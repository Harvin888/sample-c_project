/****************************************************************************
       Module: debug.c
     Engineer: Nikolay Chokoev
  Description: 
Date           Initials    Description
08-Oct-2007    NCH         Initial
****************************************************************************/

#include <stdio.h>
#include <windows.h>

void dbgprint(const char *pszMes, ...)
{
#ifdef _DEBUG
   char szDbgStr[20];
   char szDbgStr1[260];
   va_list ArgList;
   FILE *pF;
   
   pF=fopen("_debugf.dbg","a");
   if(pF == NULL) return;
   
   sprintf(szDbgStr,"dbg::=>");
   va_start(ArgList, pszMes);
   vsprintf(szDbgStr1, pszMes, ArgList ); 
   va_end  (ArgList);

   fprintf(pF,"%s%s",szDbgStr,szDbgStr1);
   fclose(pF);
#endif
}