/****************************************************************************
       Module: ml_ash510.cpp
     Engineer: Nikolay Chokoev
  Description: Implementation of ASH510 driver (midlayer) for Opella-XD
Date           Initials    Description
08-Oct-2007    NCH         Initial
****************************************************************************/
#include <stdio.h>
#include "ml_ash510.h"
#include "..\..\..\opellaxd\protocol\common\drvopxd.h"

// Win specific defines
#include "windows.h"      
//NCH todo
#include "..\..\..\opellaxd\protocol\common\drvopxd.cpp"
#include "debug.h"

#define DEFAULT_ARC_RTCK_TIMEOUT                            50                // default timeout is 50 ms
#define MIN_ARC_RTCK_TIMEOUT                                5                 // minimum timeout is 5 ms
#define MAX_ARC_RTCK_TIMEOUT                                20000             // maximum timeout is 20 s


// Opella-XD ARC frequency limits and default values
#define MAX_ARC_JTAG_AND_BLAST_FREQUENCY                    100.0             // maximum frequency is 100 MHz
#define MIN_ARC_JTAG_AND_BLAST_FREQUENCY                    0.001              // minimum frequency is 1 kHz
#define DEFAULT_ARC_JTAG_FREQUENCY                          12.0              // 12 MHz as default JTAG clock (compatibility with OpellaUSB)
/****************************************************************************
*** Local defines                                                         ***
****************************************************************************/
#define XD510_SEND_TIMEOUT                     2000                    // timeout to send command into Opella-XD (2 seconds)
#define XD510_RECEIVE_SHORT_TIMEOUT            2000                    // timeout to receive response from Opella-XD (2 seconds)
#define XD510_RECEIVE_LONG_TIMEOUT             10000                   // timeout to receive response from Opella-XD (10 seconds)
#define MAX_XD510_HWORDS_ACCESS                         16384

TyArcTargetParams *ptyMLHandle;		// pointer to structure used in MidLayer

// local variables
#define DLLPATH_LENGTH                                      (_MAX_PATH)
char pszGlobalDllPath[DLLPATH_LENGTH];                                        // contains DLL path


/****************************************************************************
     Function: StringToLwr
     Engineer: Nikolay Chokoev
        Input: char *pszString - string to convert
       Output: none
  Description: Convert string to lower case.
Date           Initials    Description
07-Dec-2007    NCH         Initial
*****************************************************************************/
void StringToLwr(char *pszString)
{
   while(*pszString)
      {
      if ((*pszString >= 'A') && (*pszString <= 'Z')) 
          *pszString += 'a'-'A';
      pszString++;
      }
}

/****************************************************************************
     Function: ML_GetListOfConnectedProbes
     Engineer: Nikolay Chokoev
        Input: char ppszInstanceNumbers[][16] - pointer to list for instance numbers

       Output: none
  Description: get list of connected Opella-XD probe (list of serial numbers)
Date           Initials    Description
16-Jul-2006    NCH         Initial
****************************************************************************/
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], 
								 unsigned short usMaxInstances, 
								 unsigned short *pusConnectedInstances)
{
   unsigned short usIndex;
   unsigned short usNumberOfDevices = 0;
   char ppszLocalInstanceNumbers[MAX_DEVICES_SUPPORTED][16];

   // check connected Opella-XDs
   DL_OPXD_GetConnectedDevices(ppszLocalInstanceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);

   // copy list of devices up to maximum number
   if (usNumberOfDevices > usMaxInstances)
      usNumberOfDevices = usMaxInstances;

   for (usIndex=0; usIndex < usNumberOfDevices; usIndex++)
      strcpy(ppszInstanceNumbers[usIndex], ppszLocalInstanceNumbers[usIndex]);

   if (pusConnectedInstances != NULL)
      *pusConnectedInstances = usNumberOfDevices;
}

/****************************************************************************
     Function: ML_OpenConnection
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int - error code ERR_ML_ARC_xxx
  Description: open connection to debugger (initialize Opella-XD, initialize all 
               data structures, etc.)
               Must be called before any other ML_ functions.
               Opella-XD instance number has to be already filled in the structure
               and initilization flag should be 0 when calling this function.
Date           Initials    Description
10-Oct-2007    NCH         Initial
****************************************************************************/
int ML_OpenConnection(TyArcTargetParams *ptyArcParams)
{
   TyDeviceInfo tyLocDeviceInfo;
   TyDevHandle tyLocHandle = MAX_DEVICES_SUPPORTED;
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;

   // check parameters
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->bConnectionInitialized)
      return ERR_ML_ARC_ALREADY_INITIALIZED;
   // let try to open device with serial number from structure to get a handle
   if (DL_OPXD_OpenDevice(ptyArcParams->pszProbeInstanceNumber, OPELLAXD_USB_PID, 0, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_PROBE_NOT_AVAILABLE;
   // got handle so do configuration steps
//   tyLocDeviceInfo.tyTpaConnected = TPA_NONE;
   
tyLocDeviceInfo.tyTpaConnected = TPA_ARC20;   
   //if ((DL_OPXD_UnconfigureDevice(tyLocHandle) != DRVOPXD_ERROR_NO_ERROR) || 
   //    (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyLocDeviceInfo) != DRVOPXD_ERROR_NO_ERROR)
   //    )
   //   {
   //   (void)DL_OPXD_CloseDevice(tyLocHandle);
   //   return ERR_ML_ARC_USB_DRIVER_ERROR;
   //   }
      
   if (tyLocDeviceInfo.tyTpaConnected != TPA_ARC20)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_PROBE_INVALID_TPA;
      }
   // ok, we got probe with TPA for ARC so let configure FPGA and diskware
   // configuration files MUST be in same folder as .dll so we can use that path
   if (!strcmp(pszGlobalDllPath, ""))
      tyRes = DL_OPXD_ConfigureDevice(tyLocHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ASH510_FILENAME, OPXD_FPGAWARE_ASH510_FILENAME);
   else
      {     // there is stored some path so append \ and filenames
      char pszLocDiskwareName[_MAX_PATH];
      char pszLocFpgawareName[_MAX_PATH];
      // create full diskware filename
      strcpy(pszLocDiskwareName, pszGlobalDllPath);
      strcat(pszLocDiskwareName, "\\");
      strcat(pszLocDiskwareName, OPXD_DISKWARE_ASH510_FILENAME);
      // create full fpgaware filename
      strcpy(pszLocFpgawareName, pszGlobalDllPath);
      strcat(pszLocFpgawareName, "\\");
      strcat(pszLocFpgawareName, OPXD_FPGAWARE_ASH510_FILENAME);
      tyRes = DL_OPXD_ConfigureDevice(tyLocHandle, DW_ARC, FPGA_ARC, pszLocDiskwareName, pszLocFpgawareName);
      }
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_PROBE_CONFIG_ERROR;
      }
   // obtain information about device
   if (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyLocDeviceInfo) == DRVOPXD_ERROR_NO_ERROR)
      {  // fill information to local structure
      ptyArcParams->tyProbeFirmwareInfo.ulVersion  = tyLocDeviceInfo.ulFirmwareVersion;
      ptyArcParams->tyProbeFirmwareInfo.ulDate     = tyLocDeviceInfo.ulFirmwareDate;
      ptyArcParams->tyProbeDiskwareInfo.ulVersion  = tyLocDeviceInfo.ulDiskwareVersion;
      ptyArcParams->tyProbeDiskwareInfo.ulDate     = tyLocDeviceInfo.ulDiskwareDate;
      ptyArcParams->tyProbeFPGAwareInfo.ulVersion  = tyLocDeviceInfo.ulFpgawareVersion;
      ptyArcParams->tyProbeFPGAwareInfo.ulDate     = tyLocDeviceInfo.ulFpgawareDate;
      }
   else
      {
      ptyArcParams->tyProbeFirmwareInfo.ulVersion  = 0x0;
      ptyArcParams->tyProbeFirmwareInfo.ulDate     = 0x0;
      ptyArcParams->tyProbeDiskwareInfo.ulVersion  = 0x0;
      ptyArcParams->tyProbeDiskwareInfo.ulDate     = 0x0;
      ptyArcParams->tyProbeFPGAwareInfo.ulVersion  = 0x0;
      ptyArcParams->tyProbeFPGAwareInfo.ulDate     = 0x0;
      }
   // ok, probe is configured, with correct TPA, etc. so let prepare for debug session with default settings
   // set target type, TMS sequence, default JTAG frequency, no multicore, fixed voltage of 3.3V
   ptyArcParams->dJtagFrequency = DEFAULT_ARC_JTAG_FREQUENCY;
   tyRes = DL_OPXD_JtagSetFrequency(tyLocHandle, ptyArcParams->dJtagFrequency, NULL, NULL);              // set default fixed frequency
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_PROBE_CONFIG_ERROR;
      }

   // depending on target type
   tyRes = DRVOPXD_ERROR_NO_ERROR;

   // set Vtpa voltage depending on target
   
   // use Vtref from target connector for other targets
   tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 1, 1);
//   tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 1.8, 0, 1);
tyRes = DRVOPXD_ERROR_NO_ERROR;

   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_PROBE_CONFIG_ERROR;
      }
   // wait to stabilize voltage
   ML_Sleep(ptyArcParams, 400);// 400 ms
   // we came up here so initialization has been successful
   ptyArcParams->bConnectionInitialized = 1;
   ptyArcParams->iCurrentHandle = (int)tyLocHandle;
   // initialize last error
   ptyArcParams->iLastErrorCode = ERR_ML_ARC_NO_ERROR;
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_UpgradeFirmware
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               PfPrintFunc pfPrintFunc - print message function
       Output: int - error code ERR_ML_ARC_xxx
  Description: open connection and verify firmware version
               automatically upgrade if necessary
Date           Initials    Description
31-Oct-2007    NCH         Initial
****************************************************************************/
int ML_UpgradeFirmware(TyArcTargetParams *ptyArcParams, PfPrintFunc pfPrintFunc)
{
   char pszLocFirmwareName[_MAX_PATH];
   int iFirmwareDiff = 0;
   unsigned char bReconnectNeeded = 0;
   TyDevHandle tyLocHandle = MAX_DEVICES_SUPPORTED;
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->bConnectionInitialized)
      return ERR_ML_ARC_ALREADY_INITIALIZED;
   // let try to open device with serial number from structure to get a handle
   if (DL_OPXD_OpenDevice(ptyArcParams->pszProbeInstanceNumber, OPELLAXD_USB_PID, 0, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_PROBE_NOT_AVAILABLE;
   // got handle so do configuration steps
   if (DL_OPXD_UnconfigureDevice(tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      dbgprint("ERROR:DL_OPXD_UnconfigureDevice\n");
      return ERR_ML_ARC_USB_DRIVER_ERROR;
      }
   // get firmware full path
   if (!strcmp(pszGlobalDllPath, ""))
      strcpy(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);
   else
      {     // there is stored some path so append \ and filenames
      strcpy(pszLocFirmwareName, pszGlobalDllPath);
      strcat(pszLocFirmwareName, "\\");
      strcat(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);
      }
   // check firmware version if we need to upgrade
   if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_CHECK_ONLY, pszLocFirmwareName, &bReconnectNeeded, &iFirmwareDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      dbgprint("ERROR:DL_OPXD_UpgradeDevice1\n");
      return ERR_ML_ARC_USB_DRIVER_ERROR;
      }
   if (iFirmwareDiff >= 0)
      {
      // great, probe has same or newer firmware than required, so close handle and return
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_NO_ERROR;
      }
   // we need to upgrade firmware
   if (pfPrintFunc != NULL)
      pfPrintFunc("Updating Opella-XD firmware, please wait ...\n");
   bReconnectNeeded = 0;
   if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_NEWER_VERSION, pszLocFirmwareName, &bReconnectNeeded, &iFirmwareDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      dbgprint("ERROR:DL_OPXD_UpgradeDevice2\n");
      return ERR_ML_ARC_USB_DRIVER_ERROR;
      }
   if (bReconnectNeeded)
      {
      unsigned int uiCnt;
      // wait for 7 seconds to reconnect probe
      for (uiCnt=0; uiCnt < 7; uiCnt++)
         ML_Sleep(ptyArcParams, 1000);                                        // 100 ms
      }
   else
      (void)DL_OPXD_CloseDevice(tyLocHandle);                                 // close handle

   if (pfPrintFunc != NULL)
      pfPrintFunc("Opella-XD firmware has been updated\n");
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_CloseConnection
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int - error code ERR_ML_ARC_xxx
  Description: close connection to debugger
               Must be called as last of ML_ functions.
Date           Initials    Description
16-Jul-2006    NCH         Initial
****************************************************************************/
int ML_CloseConnection(TyArcTargetParams *ptyArcParams)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // if device is already closed, just return
   if (!ptyArcParams->bConnectionInitialized || 
      (ptyArcParams->iCurrentHandle >= MAX_DEVICES_SUPPORTED))
      return ERR_ML_ARC_NO_ERROR;   
   // so lets do deinitialization sequence for Opella-XD, do not bother with errors
   // set fixed frequency to default value
   (void)DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, DEFAULT_ARC_JTAG_FREQUENCY, NULL, NULL);
   // disable TPA and unconfigure Opella-XD (FPGA and diskware)
   (void)DL_OPXD_SetVtpaVoltage(ptyArcParams->iCurrentHandle, 0.0, 1, 0);
   (void)DL_OPXD_UnconfigureDevice(ptyArcParams->iCurrentHandle);
   // close device
   (void)DL_OPXD_CloseDevice(ptyArcParams->iCurrentHandle);
   ptyArcParams->bConnectionInitialized = 0;                            // connection is closed
   ptyArcParams->iCurrentHandle = MAX_DEVICES_SUPPORTED;                // handle to usb is now invalid
   return ERR_ML_ARC_NO_ERROR;
}


/****************************************************************************
     Function: ML_WriteBlock
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               TyArcAccessType tyAccessType - ARC core access type
               unsigned long ulAddress - address to write
               unsigned long ulCount - number of words to write (max. 0x4000 of words)
               unsigned long *pulData - pointer to data
       Output: int - error code ERR_ML_ARC_xxx
  Description: Write block of data from ARC core.
Date           Initials    Description
11-Oct-2007    NCH         Initial
****************************************************************************/
int ML_WriteBlock(TyArcTargetParams *ptyArcParams, 
				      unsigned long ulAddress, 
				      unsigned long ulCount, 
				      unsigned long *pulData)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int iResult = ERR_ML_ARC_NO_ERROR;
   unsigned short usAccessType = 0; //NCH todo remove

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
//NCH todo implement me
   // call diskware function
//   tyRes = DL_OPXD_Arc_WriteBlock(ptyArcParams->iCurrentHandle,0,     // core number
//                                  usAccessType, ulAddress, ulCount, pulData);                                        // other params

   return 0;
}

/****************************************************************************
     Function: ML_ReadBlock
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               TyArcAccessType tyAccessType - ARC core access type
               unsigned long ulAddress - address to read
               unsigned long ulCount - number of words to read (max. 0x4000 of words)
               unsigned long *pulData - pointer to data
       Output: int - error code ERR_ML_ARC_xxx
  Description: Read block of data from ARC core.
Date           Initials    Description
11-Oct-2007    NCH         Initial
****************************************************************************/
int ML_ReadBlock( TyArcTargetParams *ptyArcParams, 
                  unsigned long ulAddress, 
				      unsigned long ulCount, 
				      unsigned long *pulData)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int iResult = ERR_ML_ARC_NO_ERROR;
   unsigned short usAccessType = 0;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // call diskware function
   //NCH todo implement me
//   tyRes = DL_OPXD_Arc_ReadBlock(ptyArcParams->iCurrentHandle, 0, 
//                                 usAccessType, ulAddress, ulCount, pulData);
   return (0);
}

/****************************************************************************
     Function: ML_ConvertFrequencySelection
     Engineer: Nikolay Chokoev
        Input: const char *pszSelection - string with frequency selection
               double *pdFrequency - pointer to converted frequency
       Output: unsigned char - TRUE if frequency is valid and in supported range
  Description: return information if target is ARCangel
Date           Initials    Description
16-Jul-2006    NCH         Initial
****************************************************************************/
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency)
{
   double dFrequency = 0.0;
   int iValue = 0;
   char pszLocSelection[20];

   if ((pszSelection == NULL) || (strlen(pszSelection) >= 20)) 
      return FALSE;
   else
      {  // convert string to lower case in local buffer
      unsigned int uiIndex = 0;
      while (uiIndex < strlen(pszSelection))
         {
         if ((pszSelection[uiIndex] >= 'A') && (pszSelection[uiIndex] <= 'Z'))
            pszLocSelection[uiIndex] = pszSelection[uiIndex] + ('a' - 'A');
         else
            pszLocSelection[uiIndex] = pszSelection[uiIndex];
         uiIndex++;
         }
      pszLocSelection[uiIndex] = 0;        // terminate string
      }
   if (pdFrequency != NULL)
      *pdFrequency = 0.0;

   // first check frequency selection as index (backward compatibility with OpellaUSB)
   if (strlen(pszLocSelection) == 1)
      {  // selection made by index so check value
      switch (pszLocSelection[0])
         {
         case '0':               // 24 MHz
            dFrequency = 24.0;
            break;
         case '1':               // 12 MHz
            dFrequency = 12.0;
            break;
         case '2':               //  8 MHz
            dFrequency = 8.0;
            break;
         case '3':               //  6 MHz
            dFrequency = 6.0;
            break;
         case '4':               //  4 MHz
            dFrequency = 4.0;
            break;
         case '5':               //  3 MHz
            dFrequency = 3.0;
            break;
         case '6':               //  2 MHz
            dFrequency = 2.0;
            break;
         case '7':               //  1 MHz
            dFrequency = 1.0;
            break;
         default:
            return FALSE;        // error in selection
         }
      }
   else if ((strstr(pszLocSelection, "khz") != NULL) && (sscanf(pszLocSelection, "%dkhz", &iValue) == 1))
      {  // value specified in kHz
      dFrequency = (double)iValue;                    // frequency in kHz
      dFrequency /= 1000.0;                           // convert to MHz
      }
   else if ((strstr(pszLocSelection, "mhz") != NULL) && (sscanf(pszLocSelection, "%dmhz", &iValue) == 1))
      {  // value in MHz
      dFrequency = (double)iValue;                    // frequency in MHz, no need to further conversion
      }
   else
      return FALSE;

   // otherwise frequency has been correctly parsed
   if (pdFrequency != NULL)
      *pdFrequency = dFrequency;

   return TRUE;
}

/****************************************************************************
     Function: ML_GetErrorMessage
     Engineer: Nikolay Chokoev
        Input: int iReturnCode - error code ML_xxx
       Output: char * - pointer to string with message
  Description: get error message
Date           Initials    Description
05-Nov-2007    NCH         Initial
****************************************************************************/
char* ML_GetErrorMessage(int iReturnCode)
{
   char *pszLocMsg;
   switch (iReturnCode)
      {
      case ERR_ML_ARC_NO_ERROR:                       // no error
         pszLocMsg = MSG_ML_NO_ERROR;                 break;
      case ERR_ML_ARC_INVALID_HANDLE:                 // invalid driver handle
         pszLocMsg = MSG_ML_INVALID_DRIVER_HANDLE;    break;
      case ERR_ML_ARC_TARGET_NOT_SUPPORTED:           // target does not support selected feature
         pszLocMsg = MSG_ML_TARGET_NOT_SUPPORTED;     break;
      case ERR_ML_ARC_USB_DRIVER_ERROR:               // USB driver error (Opella-XD did not respond or USB error)
         pszLocMsg = MSG_ML_USB_DRIVER_ERROR;         break;
      case ERR_ML_ARC_PROBE_NOT_AVAILABLE:            // Opella-XD is not available for debug session
         pszLocMsg = MSG_ML_PROBE_NOT_AVAILABLE;      break;
      case ERR_ML_ARC_PROBE_INVALID_TPA:              // missing or invalid TPA
         pszLocMsg = MSG_ML_INVALID_TPA;              break;
      case ERR_ML_ARC_PROBE_CONFIG_ERROR:             // probe configuration error (missing diskware or FPGAware)
         pszLocMsg = MSG_ML_PROBE_CONFIG_ERROR;       break;
      case ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED:           // RTCK timeout expired (problem with RTCK)
         pszLocMsg = MSG_ML_RTCK_TIMEOUT_EXPIRED;     break;
      case ERR_ML_ARC_DEBUG_ACCESS:                   // access to ARC JTAG debug register failed
         pszLocMsg = MSG_ML_DEBUG_ACCESS_FAILED;      break;
      case ERR_ML_ARC_INVALID_FREQUENCY:              // invalid frequency format
         pszLocMsg = MSG_ML_INVALID_FREQUENCY;        break;
      case ERR_ML_ARC_ANGEL_BLASTING_FAILED:          // blasting FPGA into ARCangel failed
         pszLocMsg = MSG_ML_AA_BLASTING_FAILED;       break;
      case ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE:  // invalid response to certain extended commands (setting clocks on ARCangel4)
         pszLocMsg = MSG_ML_AA_INVALID_EXTCMD_RESPONSE;  break;
      case ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY:    // cannot set PLL frequency on ARCangel4 (requested frequency is not supported)
         pszLocMsg = MSG_ML_AA_INVALID_PLL_FREQUENCY; break;
      case ERR_ML_ARC_TEST_SCANCHAIN_FAILED:          // failing scanchain loopback test
         pszLocMsg = MSG_ML_SCANCHAIN_TEST_FAILED;    break;
      case ERR_ML_ARC_TARGET_RESET_DETECTED:          // target reset detected
         pszLocMsg = MSG_ML_TARGET_RESET_DETECTED;    break;
      default:                                        // unspecified error
         pszLocMsg = MSG_ML_UNKNOWN_ERROR;            break;
      }
   return pszLocMsg;
}

/****************************************************************************
*** Other MidLayer Functions                                              ***
****************************************************************************/

/****************************************************************************
     Function: ML_SetDllPath
     Engineer: Nikolay Chokoev
        Input: const char *pszDllPath - DLL full path
       Output: none
  Description: extract path to DLL folder and store into global variable
               (to be used with ML_CreateFullFilename and others)
Date           Initials    Description
09-Oct-2007    NCH         Initial
****************************************************************************/
void ML_SetDllPath(const char *pszDllPath)
{
   char *pszPtr;

   pszGlobalDllPath[0] = 0;
   // copy string to buffer and convert to lower case
   strncpy(pszGlobalDllPath, pszDllPath, DLLPATH_LENGTH - 1);
   pszGlobalDllPath[DLLPATH_LENGTH - 1] = 0;

#ifdef __LINUX
   // try to find .so extension
   pszPtr = strstr(pszGlobalDllPath,".so");
   if (pszPtr != NULL)
      {     // found .so so go backwards to nearest '/' (or to start of string)
      *pszPtr=0;
      while ((*pszPtr != '/') && (pszPtr > pszGlobalDllPath))
         pszPtr--;
      *pszPtr = 0;   // cut string (remove dll name+extension and \ character
      }
#else
   StringToLwr(pszGlobalDllPath);
   // try to find .dll extension
   pszPtr = strstr(pszGlobalDllPath,".dll");
   if (pszPtr != NULL)
      {     // found .dll so go backwards to nearest '\\' (or to start of string)
      *pszPtr=0;
      while ((*pszPtr != '\\') && (pszPtr > pszGlobalDllPath))
         pszPtr--;
      *pszPtr = 0;   // cut string (remove dll name+extension and \ character
      }
#endif
   else
      pszGlobalDllPath[0] = 0;   // wrong format, so use empty string
}

/****************************************************************************
     Function: ML_GetDllPath
     Engineer: Nikolay Chokoev
        Input: char *pszDllPath - full path to DLL
               unsigned int uiSize - maximum number of bytes in pszDllPath
       Output: none
  Description: copy full DLL path into provided string
Date           Initials    Description
15-Oct-2007    NCH         Initial
****************************************************************************/
void ML_GetDllPath(char *pszDllPath, unsigned int uiSize)
{
   if (uiSize)
      {
      strncpy(pszDllPath, pszGlobalDllPath, uiSize);
      pszDllPath[uiSize-1] = 0;
      }
}

/****************************************************************************
     Function: ML_Sleep
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned long ulMiliseconds - number of miliseconds to sleep
       Output: none
  Description: sleep version in MidLayer, calling debugger implementation if provided
Date           Initials    Description
09-Oct-2007    NCH         Initial
****************************************************************************/
void ML_Sleep(TyArcTargetParams *ptyArcParams, unsigned long ulMiliseconds)
{
   if ((ptyArcParams == NULL) || (ptyArcParams->pfSleepFunc == NULL))
      {
      Sleep((DWORD)ulMiliseconds);  // we must use own implementation
      }
   else
      ptyArcParams->pfSleepFunc((unsigned)ulMiliseconds);
}

//----------------------------------------------------------------------------
//--- Local functions                                                      ---
//----------------------------------------------------------------------------

//NCH todo

TyError memReadTBCblk_Reg( unsigned short usTBCPort,
                           unsigned short *pDataBuff,
                           unsigned long  ulDataCount,
                           int            iReadOptions)
{

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int iPacketLen;

   if (ptyMLHandle->iCurrentHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (  (pDataBuff == NULL) || 
         (ulDataCount > MAX_XD510_HWORDS_ACCESS))
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
   // synchronization not used, assuming single thread uses this function   
   ptyUsbDevHandle = ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].pulTransferBuffer;
   // create packet
//   dbgprint("memReadTBCblk_Reg(cnt)(reg):%d\t%08lX\n", ulDataCount, usTBCPort);   
   *((unsigned long *)(pucBuffer + 0x0)) = CMD_CODE_ASH510_READ; // command code
   *((unsigned long *)(pucBuffer + 0x04)) = ulDataCount;          // register count   (read 1 register only)
   *((unsigned long *)(pucBuffer + 0x08)) = usTBCPort;            // register address
   *((unsigned long *)(pucBuffer + 0x0C)) = iReadOptions;         // read options
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, XD510_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, XD510_RECEIVE_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));
      }
   // some registers can be read during powerdown state
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR))
      {
      unsigned long ulWordsRead = *((unsigned long *)(pucBuffer + 0x04));
      if (ulWordsRead != ulDataCount)
         tyResult = DRVOPXD_ERROR_TARGET_READ_MEMORY;                            // error when writing to target memory
      else
         memcpy((void *)pDataBuff, (void *)(pucBuffer + 0x08), ulDataCount*4); // copy all words
      }
//   dbgprint("memReadTBCblk_Reg(+0)(+4)(+8)=>%08lX\t%08lX\t%08lX\n",
//            *((unsigned long *)(pucBuffer + 0x00)),
//            *((unsigned long *)(pucBuffer + 0x04)),
//            *((unsigned long *)(pucBuffer + 0x08)));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
   
}

void memWriteTBCblk_Reg(unsigned short usTBCPort,
				            unsigned short *pDataBuff,
                        unsigned long  ulDataCount,
                        int            iWriteOptions)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int iPacketLen;

   if (ptyMLHandle->iCurrentHandle >= MAX_DEVICES_SUPPORTED)
      return;// DRVOPXD_ERROR_INVALID_HANDLE;
   if ((pDataBuff == NULL) || 
	   (ulDataCount > MAX_XD510_HWORDS_ACCESS))
      return;// DRVOPXD_ERROR_INVALID_DATA_FORMAT;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].pulTransferBuffer;
   // create packet   
   *((unsigned long *)(pucBuffer + 0x00)) = CMD_CODE_ASH510_WRITE;		// command code
   *((unsigned long *)(pucBuffer + 0x04)) = ulDataCount;				// data count
   *((unsigned long *)(pucBuffer + 0x08)) = usTBCPort;					// start address
   *((unsigned long *)(pucBuffer + 0x0C)) = iWriteOptions;				// write options
   memcpy((void *)(pucBuffer + 0x10), (void *)pDataBuff, ulDataCount*4);// copy all words
   iPacketLen = TRANSMIT_LEN(16 + ((int)ulDataCount*4));
   //dbgprint("memWriteTBCblk_Reg(cnt)(reg)(dat):%d\t%08lX\t%08lX\n", 
   //         ulDataCount, usTBCPort,(unsigned long) *(pucBuffer + 0x10));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, XD510_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, XD510_RECEIVE_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      unsigned long ulWordsWritten = *((unsigned long *)(pucBuffer + 0x04));
      if (ulWordsWritten != ulDataCount)
         tyResult = DRVOPXD_ERROR_TARGET_WRITE_MEMORY;                           // error when writing to target memory
      }
   //dbgprint("memWriteTBCblk_Reg=>%08lX\t%08lX\n",
   //         tyResult, *((unsigned long *)(pucBuffer + 0x04)));
   // synchronization not used, assuming single thread uses this function
   return;//(tyResult);
}

unsigned short memReadTBC_Reg(unsigned short usTBCPort)
{
	static unsigned short usData;
	unsigned short usLocTBCPort;
   //NCH todo subtract default port.....
   usLocTBCPort=usTBCPort;// - 240;
//   dbgprint("memReadTBC_Reg:%08lX\n", usLocTBCPort);
	(void)memReadTBCblk_Reg(usLocTBCPort, &usData, 1, 0);

	return usData;
}


void memWriteTBC_Reg(unsigned short usTBCPort, unsigned short usValue)
{
   //NCH todo subtract default port.....
   unsigned short usLocTBCPort;
//   dbgprint("memWriteTBC_Reg:%08lX\t%08lX\n", usTBCPort,usValue);
   usLocTBCPort = usTBCPort;// - 0x240;
//   dbgprint("memWriteTBC_Reg(port)(value):%08lX\t%08lX\n", usLocTBCPort,usValue);
	(void)memWriteTBCblk_Reg(usLocTBCPort, &usValue, 1, 0);
}

int SetJtagFreq ( double dFreq,
                  double *pdPllFrequency, 
                  double *pdFpgaDivideRatio)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (ptyMLHandle->iCurrentHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].pulTransferBuffer;
   // create packet
   *((unsigned long *)(pucBuffer + 0x00)) = CMD_CODE_ASH510_SET_JTAG_FREQUENCY;
   *((unsigned long *)(pucBuffer + 0x04)) = (unsigned long)(dFreq * 1000000);              // frequency value in Hz
   pucBuffer[0x08] = 0x01; // PLL                                                                      // using selectable frequency from PLL
//   pucBuffer[0x08] = 0x00; // XSYSCLK
   pucBuffer[0x09] = 0x00;                                                                      // disabling adaptive clock
   pucBuffer[0x0A] = 0x00;    
   pucBuffer[0x0B] = 0x00;                                           // reserved
   *((unsigned short *)(pucBuffer + 0x0C)) = 0x0;                                               // timeout in ms
   pucBuffer[0x0E] = 0x00;                                                                      // not using reset occured to stop adaptive clock
   pucBuffer[0x0F] = 0x00;                                                                      // reserved
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      if (pdPllFrequency != NULL)
         {
         *pdPllFrequency = (double)(*((unsigned long *)(pucBuffer + 0x08)));
         *pdPllFrequency /= 1000000;                                                               // convert to MHz
         }
      if (pdFpgaDivideRatio != NULL)
         *pdFpgaDivideRatio = (double)(*((unsigned long *)(pucBuffer + 0x0C)));
      if (*((unsigned long *)(pucBuffer + 0x0C)) >= 1000)
         ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].iDataTimeout = DRV_TIMEOUT * 10;                           // set longer timeout for frequency < 10 kHz
      else
         ptyDrvDeviceTable[ptyMLHandle->iCurrentHandle].iDataTimeout = DRV_TIMEOUT;
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}