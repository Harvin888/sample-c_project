/****************************************************************************
       Module: ml_arc.h
     Engineer: Nikolay Chokoev
  Description: Headers for ARC driver functions (mid layer).
Date           Initials    Description
08-Oct-2007    NCH         Initial
****************************************************************************/
#ifndef ML_ASH510_H_
#define ML_ASH510_H_

#ifdef __LINUX
// Linux
typedef unsigned long       DWORD;
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#endif //__LINUX

// error codes for ARC midlayer
#define ERR_ML_ARC_NO_ERROR                                 0           // no error occured
#define ERR_ML_ARC_INVALID_HANDLE                           1           // invalid handle with parameters
#define ERR_ML_ARC_ALREADY_INITIALIZED                      2           // connection already initialized
#define ERR_ML_ARC_TARGET_NOT_SUPPORTED                     3           // functionality not supported for selected target
#define ERR_ML_ARC_USB_DRIVER_ERROR                         4           // driver internal error
#define ERR_ML_ARC_PROBE_NOT_AVAILABLE                      5           // cannot open connection with probe
#define ERR_ML_ARC_PROBE_INVALID_TPA                        6           // no or invalid TPA used
#define ERR_ML_ARC_PROBE_CONFIG_ERROR                       7           // cannot configure probe
#define ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED                     10          // RTCK timeout expired
#define ERR_ML_ARC_INVALID_FREQUENCY                        50          // invalid frequency selection
#define ERR_ML_ARC_ANGEL_BLASTING_FAILED                    100         // blasting to ARCangel failed
#define ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE            101         // invalid response to extended command
#define ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY              102         // invalid PLL frequency
#define ERR_ML_ARC_TEST_SCANCHAIN_FAILED                    200         // failing test scanchain
#define ERR_ML_ARC_DETECT_SCANCHAIN_FAILED                  201         // detection of scanchain structure failed
#define ERR_ML_ARC_INVALID_SCANCHAIN_STRUCTURE              202         // invalid scanchain structure
#define ERR_ML_ARC_TARGET_RESET_DETECTED                    300         // target reset occured
#define ERR_ML_ARC_DEBUG_ACCESS                             400         // access to ARC debug failed
#define ERR_ML_ARC_DEBUG_PDOWN                              401         // access to ARC debug failed due to powerdown

// filenames for ARC diskware and FPGAware
#define OPXD_DISKWARE_ASH510_FILENAME                          "opxdd510.bin"
#define OPXD_FPGAWARE_ASH510_FILENAME                          "xds510_fpga.bin"
#define OPXD_FIRMWARE_FILENAME                                 "opxdfw.bin"

// error messages
#define MSG_ML_NO_ERROR                                     ""
#define MSG_ML_INVALID_DISKWARE_FILENAME                    "Cannot find Opella-XD ARC diskware file\n"
#define MSG_ML_INVALID_FPGAWARE_FILENAME                    "Cannot find Opella-XD ARC FPGAware file\n"
#define MSG_ML_INVALID_FIRMWARE_FILENAME                    "Cannot find Opella-XD firmware file\n"
#define MSG_ML_INVALID_TPA_CONNECTED                        "Missing or invalid type of TPA connected to Opella-XD\n"
#define MSG_ML_USB_DRIVER_ERROR                             "USB driver error occured\n"
#define MSG_ML_INVALID_DRIVER_HANDLE                        "Invalid driver handle used\n"
#define MSG_ML_PROBE_NOT_AVAILABLE                          "Selected Opella-XD is not available for debug session\n"
#define MSG_ML_INVALID_TPA                                  "Missing or invalid TPA type connected to Opella-XD\n"
#define MSG_ML_PROBE_CONFIG_ERROR                           "Cannot configure Opella-XD with ARC diskware and FPGAware\n"
#define MSG_ML_TARGET_NOT_SUPPORTED                         "Requested feature is not supported for current target\n"
#define MSG_ML_INVALID_FREQUENCY                            "Invalid frequency format used\n"
#define MSG_ML_AA_BLASTING_FAILED                           "Cannot blast FPGA into ARCangel\n"
#define MSG_ML_AA_INVALID_EXTCMD_RESPONSE                   "Unexpected response from ARCangel\n"
#define MSG_ML_AA_INVALID_PLL_FREQUENCY                     "Invalid PLL frequency requested for ARCangel\n"
#define MSG_ML_RTCK_TIMEOUT_EXPIRED                         "Cannot detect RTCK signal from target within specified timeout\n"
#define MSG_ML_DEBUG_ACCESS_FAILED                          "Access to ARC debug interface has failed\n"
#define MSG_ML_SCANCHAIN_TEST_FAILED                        "Scanchain loopback test has failed\n"
#define MSG_ML_INVALID_PROPERTY_FORMAT                      "Invalid property format\n"
#define MSG_ML_UNKNOWN_ERROR                                "Unknown error has occured\n"
#define MSG_ML_TARGET_RESET_DETECTED                        "\nA target initiated reset has been detected\n"

// maximum memory block size
#define MAX_ARC_READ_MEMORY                                 0x4000      // in words (total 64 kB)
#define MAX_ARC_WRITE_MEMORY                                0x4000      // in words (total 64 kB)

#define MAX_ARC_CORES                                       16
#define MAX_ON_TARGET_RESET_ACTIONS                         16

// sleep function prototype
typedef void (*TySleepFunc) (unsigned miliseconds);                     // sleep function prototype
typedef void (*TyProgressBarFunc) (void);                               // callback prototype for progress bar (blasting, etc.)   
typedef void (*PfPrintFunc)(const char *);                              // callback for general print messages

// firmware/diskware version
typedef struct _TyProbeVersion {
   unsigned long ulVersion;                                             // version
   unsigned long ulDate;                                                // date
} TyProbeVersion;


typedef struct _TyArcTargetParams {
   unsigned char bConnectionInitialized;

   double dJtagFrequency;                                               // JTAG frequency in MHz

   // function pointers
   TySleepFunc pfSleepFunc;                                             // pointer to sleep function
   TyProgressBarFunc pfProgressBarFunc;                                 // pointer to progress bar function

   // probe information
   char pszProbeInstanceNumber[16];                                     // probe instance serial number
   int iCurrentHandle;                                                  // current probe handle
   TyProbeVersion tyProbeDiskwareInfo;                                  // probe diskware info
   TyProbeVersion tyProbeFirmwareInfo;                                  // probe firmware info
   TyProbeVersion tyProbeFPGAwareInfo;                                  // probe FPGAware info
   // other information
   int iLastErrorCode;                                                  // last error code (if any error happened)  
} TyArcTargetParams, *PTyArcTargetParams;

// function prototypes in ARC midlayer
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances);
int ML_UpgradeFirmware(TyArcTargetParams *ptyArcParams, PfPrintFunc pfPrintFunc);
int ML_OpenConnection(TyArcTargetParams *ptyArcParams);
int ML_CloseConnection(TyArcTargetParams *ptyArcParams);
int ML_TestScanchain(TyArcTargetParams *ptyArcParams, unsigned char bVerifyNumberOfCores);
int ML_DetectScanchain(TyArcTargetParams *ptyArcParams, unsigned int uiMaxCores, unsigned int *puiNumberOfCores, 
                       unsigned long *pulIRWidth, unsigned char *pbARCCores);
int ML_UpdateScanchain(TyArcTargetParams *ptyArcParams);

// function supporting target reset
int ML_SetResetProperty(TyArcTargetParams *ptyArcParams, unsigned char bSetResetDelay, unsigned int *piValue);
int ML_ResetTarget(TyArcTargetParams *ptyArcParams);
// function setting frequency
int ML_SetJtagFrequency(TyArcTargetParams *ptyArcParams, double dFrequency);
int ML_SetBlastFrequency(TyArcTargetParams *ptyArcParams, double dFrequency);
unsigned char ML_UsingAdaptiveJtagClock(TyArcTargetParams *ptyArcParams);
// read/write functions
int ML_WriteBlock(TyArcTargetParams *ptyArcParams, unsigned long ulAddress, unsigned long ulCount, unsigned long *pulData);
int ML_ReadBlock( TyArcTargetParams *ptyArcParams, unsigned long ulAddress, unsigned long ulCount, unsigned long *pulData);
// function setting target parameters
int ML_IndicateTargetStatusChange(TyArcTargetParams *ptyArcParams);
int ML_UpdateProbeSettings(TyArcTargetParams *ptyArcParams);

// convert functions
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency);
char* ML_GetErrorMessage(int iReturnCode);

// other functions
void ML_SetDllPath(const char *pszDllPath);
void ML_GetDllPath(char *pszDllPath, unsigned int uiSize);
void ML_Sleep(TyArcTargetParams *ptyArcParams, unsigned long ulMiliseconds);

unsigned short memReadTBC_Reg(unsigned short usTBCPort);
void memWriteTBC_Reg(unsigned short usTBCPort, unsigned short usValue);
int  SetJtagFreq( double dFreq,
                  double *pdPllFrequency, 
                  double *pdFpgaDivideRatio);
                  
#endif   // ML_ASH510_H_

