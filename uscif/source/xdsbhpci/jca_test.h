/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_test.h
*
* DESCRIPTION
*   The header file for use by jca_test.c.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_test_h
#define jca_test_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The function declarations.
*******************************************************************************/

extern JTI_Error
jca_SetupJcaTest(JCA_Handle pAdapterInform);

/*******************************************************************************
* Test parameters.
*******************************************************************************/

/* Value to indicate that unittest is active */
#define UNIT_TEST_FLAG  (uint32_t)1

/* Maximum size of the TBC address space */
#define FAKE_SIZE       (uint32_t)0x100

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_test_h */

/* -- END OF FILE -- */