/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_program.h
*
* DESCRIPTION
*   Program an FPGA/CPLD via the JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_api_program_h
#define jca_api_program_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern JTI_Error
jca_ProgramStart(
    JCA_Inform     *pAdapterInform, /* Communications adapter handle. */
    JCA_ProgramType hardwareType);  /* The FPGA/CPLD type. */

extern JTI_Error
jca_ProgramTransfer(
    JCA_Inform     *pAdapterInform,  /* Communications adapter handle. */
    JCA_ProgramType hardwareType,    /* The FPGA/CPLD type. */
    unsigned char  *pProgramData,    /* The FPGA/CPLD programming data. */
    uint32_t        programNumber,   /* The number of data values. */
    uint32_t       *pProgramStatus); /* The result status. */

extern JTI_Error
jca_ProgramFinish(
    JCA_Inform     *pAdapterInform,  /* Communications adapter handle. */
    JCA_ProgramType hardwareType,    /* The FPGA/CPLD type. */
    uint32_t       *pProgramStatus); /* The result status. */


extern JTI_Error
jca_ProgramVersion(
    JCA_Inform     *pAdapterInform,    /* Communications adapter handle. */
    JCA_ProgramType hardwareType,      /* The FPGA/CPLD type. */
    uint32_t       *pHardwareVersion); /* The version status. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_api_program_h */

/* -- END OF FILE -- */
