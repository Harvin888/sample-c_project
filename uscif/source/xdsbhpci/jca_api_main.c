/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_main.c
*
* DESCRIPTION
*   Open and close the JCA.
*
* PUBLIC FUNCTIONS
*
*   POD_GetParent()  - Report the parent library of this adapter.
*
*   jca_Create()     - Create a communications adapter structure.
*   jca_Delete()     - Delete a communications adapter structure.
*
*   jca_Init()       - Software open a communications adapter.
*   jca_Quit()       - Software close a communications adapter.
*
*   jca_Connect()    - Hardware connect to a communications adapter.
*   jca_Disconnect() - Hardware disconnect from a communications adapter.
*
* PRIVATE FUNCTIONS
*
*   GetPortAddress() - Fetch the port address for the adapter.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_main_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The legacy header file.
 */
#include "xdsfast3.h"

/*
 * The other header files.
 */
#include "reg_base560.h"

#include "xds_memory.h"
#include "xds_file_stuff.h"
#include "xds_parameter.h"
#include "xds_variable.h"

#include "cfg_public.h"
#include "jca_public.h"

#include "jca_internal.h"
#include "jca_api_main.h"
#include "jca_test.h"

#include "bhpci_mmap.h"

#include "read_sepk_variable.h"

#include "cfg_import.h"

#include "..\..\debug.h"
#include "..\..\ml_ash510.h"

#include "jca_api_register.h" //nch dbg

#define CONNECT_TO_OPELLA_NR "22222"

extern TyArcTargetParams *ptyMLHandle;		// pointer to structure used in MidLayer

static int bFPGALoaded = 0;

/*******************************************************************************
* The private functions.
*******************************************************************************/

static JTI_Error
GetPortAddress(
    CFG_Handle cfgHandle,     /* The board configuration handle. */
    logic_t   *pUsePort,      /* Returns a flag that the address was used. */
    uint32_t  *pPortAddress); /* Returns the value of the port address. */

/*******************************************************************************
* NAME: POD_GetParent()
*
* DESCRIPTION
*   Report the parent library of this adapter.
*
* NOTES
*
*******************************************************************************/

char *
POD_GetParent(void)
{
    return LS_MODERN_DRIVER_NAME;
}

/*******************************************************************************
* NAME: jca_Create()
*
* DESCRIPTION
*   Create a communications adapter structure.
*   
* NOTES
*   It returns an opaque handle that references the data structure.
*
*******************************************************************************/

JTI_Error
jca_Create(
    JCA_Inform **ppAdapterInform) /* Pointer to comm�s adapter handle. */
{
    JCA_Inform *pAdapterInform = NULL;
    JTI_Error   error  = SC_ERR_NONE;
    JTI_Error   error1 = SC_ERR_NONE;
    dbgprint("jca_Create\n");
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == ppAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }


    if (SC_ERR_NONE == error)
    {
    /* Erase the handle in case of error. */
    *ppAdapterInform = NULL;

        /* Allocate the new information structure. */
        pAdapterInform = (JCA_Inform*)fast_malloc(sizeof(JCA_Inform));
        if (NULL == pAdapterInform)
        {
            error1 = SC_ERR_SYS_MEM_ALLOC;
        }

        if (SC_ERR_NONE == error1)
        {
            /* Initialise the new JCA_Inform structure. */
            pAdapterInform->mapHandle = NULL;
            pAdapterInform->pCpld  = NULL;
            pAdapterInform->pPll   = NULL;
            pAdapterInform->pCable = NULL;
            pAdapterInform->pTimer = NULL;
            pAdapterInform->pTbc0  = NULL;
            pAdapterInform->pTbc1  = NULL;
            pAdapterInform->pTbc2  = NULL;
            pAdapterInform->pTbc3  = NULL;
            pAdapterInform->pEmif  = NULL;

            pAdapterInform->bufferSize    = (uint32_t)0x4000;
            pAdapterInform->pReadBuffer1  = NULL;
            pAdapterInform->pWriteBuffer1 = NULL;
            pAdapterInform->pReadBuffer2  = NULL;

            pAdapterInform->dataTransfer = (JCA_Timeout)0;
            pAdapterInform->statusPoll   = (JCA_Timeout)0;
            pAdapterInform->testMode      = (uint32_t)0;

            /* Return the new value. */
            *ppAdapterInform = pAdapterInform;
        }
    }

    if (SC_ERR_NONE != error1) 
    {
        /* Call jca_Delete() to clean up */
        error = jca_Delete(&pAdapterInform);
    }

    return error;
}

/*******************************************************************************
* NAME: jca_Delete()
*
* DESCRIPTION
*   Delete a communications adapter structure.
*
* NOTES
*   It erases an opaque handle that references the data structure.
*
*******************************************************************************/

JTI_Error
jca_Delete(
    JCA_Inform **ppAdapterInform) /* Pointer to comm�s adapter handle. */
{
    JTI_Error   error = SC_ERR_NONE;
    dbgprint("jca_Delete\n");    
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == ppAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    if (SC_ERR_NONE == error)
    {
        /* Free the old information structure. */
        {
            uint32_t size = sizeof(JCA_Inform);
            void * pntr = *ppAdapterInform;

            uscif_free(&pntr, &size);
        }
    }

    /* Erase the handle. */
    ppAdapterInform = NULL;

    return error;
}

/*******************************************************************************
* NAME: jca_Init()
*
* DESCRIPTION
*   Software open a communications adapter.
*     
* NOTES
*   This function performs the software-only actions that are required
*   to prepare for access to the scan-controller and utility-controller
*   from the location at which the JSC/JUC API's are executing, through
*   all communications infrastructure, up to (but not including)
*   the scan-controller and utility-controller.
*
*   The adapter configuration is memory-mapped text from which adapter specific 
*   variables may be read and stored in the opaque data structure referenced 
*   by the handle.
*
*   The client type is an enumeration indicating the type of the client
*   that is using the communications-adapter through the JTI API's.
*
*******************************************************************************/

JTI_Error
jca_Init(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    CFG_Handle  adapterConfig,  /* Variables to configure adapter. */
    JTI_Client  jtiClientType)  /* Type of the JTI client using JCA. */
{
    JTI_Error   error  = SC_ERR_NONE;
    JTI_Error   error1 = SC_ERR_NONE;

   double dTestFreq, dTestDiv;
   int res;
   
    uint32_t portAddress = LS_INVALID_ADDRESS;
    logic_t  usePort     = FALSE;
    dbgprint("jca_Init\n");
    /* A NULL pointer value means a stupid coding error. */
    if ((NULL == pAdapterInform) || (NULL == adapterConfig))
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }
    
    if (SC_ERR_NONE == error)
    {
        /* Range check for valid client type.  */
        if ((jtiClientType <= eJTI_CLIENT_INVALID_FIRST) || 
            (jtiClientType >= eJTI_CLIENT_INVALID_LAST))
        {
                error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_CLIENT
        }
    }

    /*
     *  TODO:mld 
     *  add ability to override default buffer size via cfg variable #############
     */

    /*
     *  Initialize the two read and one write buffers.  
     */
    if (SC_ERR_NONE == error)
    {
        pAdapterInform->pReadBuffer1 = (void*)fast_malloc(pAdapterInform->bufferSize);
        if(NULL == pAdapterInform->pReadBuffer1)
        {
            error1 = SC_ERR_SYS_MEM_ALLOC;
        }

        if (SC_ERR_NONE == error1)
        {
            pAdapterInform->pWriteBuffer1 = (uint32_t*)fast_malloc(pAdapterInform->bufferSize);
            if(NULL == pAdapterInform->pWriteBuffer1)
            {
                error1 = SC_ERR_SYS_MEM_ALLOC;
            }
        }

        if (SC_ERR_NONE == error1)
        {
            pAdapterInform->pReadBuffer2 = (uint32_t*)fast_malloc(pAdapterInform->bufferSize);
            if(NULL == pAdapterInform->pReadBuffer2)
            {
                error1 = SC_ERR_SYS_MEM_ALLOC;
            }
        }
    }

    if ((SC_ERR_NONE == error)  && 
        (SC_ERR_NONE == error1) &&
        (eJTI_CLIENT_TEST_SOFTWARE == jtiClientType))
    {
        error = jca_SetupJcaTest(pAdapterInform);
    }

    if ((SC_ERR_NONE == error)  && 
        (SC_ERR_NONE == error1) && 
        ((uint32_t)0 == pAdapterInform->testMode))
    {
        /* Load the dynamic libraries.  */
        error = CFG_LoadLibrary();

        if (SC_ERR_NONE == error)
        {
            /* Read the port address for the adapter. */
            error = GetPortAddress(adapterConfig, &usePort, &portAddress);
            if (SC_ERR_NONE != error)
            {
                error = SC_ERR_CMD_PARM;
            }
        }

        if ((SC_ERR_NONE == error) &&
            (0 == bFPGALoaded))
        /* Initialise the Opella-XD device driver */
        {//NCH todo open connection
	         int iRes;
             TyArcTargetParams *ptyLocMLHandle = NULL;
	         char ppszLocInstanceNumber[16][16];
	         unsigned short usConnectedOpellaXD;
	         unsigned char bConnectionOk = 0;
	         
            dbgprint("jca_Init:ML\n");
            
             // Prepare pointer to ML handle for target params
             ptyLocMLHandle = (TyArcTargetParams *)malloc(sizeof(TyArcTargetParams));
             if (ptyLocMLHandle == NULL)
                {// we can do anything but return with error
                return SC_ERR_OCS_OPEN;
                }

             (void)memset(ptyLocMLHandle, 0, sizeof(TyArcTargetParams));
             ptyMLHandle = ptyLocMLHandle;

             // get connected Opella-XD and pick first
             ML_GetListOfConnectedProbes(ppszLocInstanceNumber, 16, &usConnectedOpellaXD);
             if (usConnectedOpellaXD)
                {
                strncpy(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0], 15);
                ptyMLHandle->pszProbeInstanceNumber[15] = 0;
                }

             // now it is time to initialize probe settings
             // note we do not bother with default values since it will be set in ML_OpenConnection
             // we need only specify Opella-XD instance number and connection flag should be 0
             ptyMLHandle->bConnectionInitialized = 0;
         	sprintf(ptyMLHandle->pszProbeInstanceNumber,CONNECT_TO_OPELLA_NR);
	         dbgprint("Opella: %s\n",ptyMLHandle->pszProbeInstanceNumber);

             // first check probe firmware
             // now there must be valid Opella-XD serial number, so try opening connection
             iRes = ML_UpgradeFirmware(ptyMLHandle, NULL); //NCH todo - remove NULL function
	         dbgprint("ML_UpgradeFirmware: %d\n",iRes);

             if ((iRes == ERR_ML_ARC_NO_ERROR) || 
                 (iRes == ERR_ML_ARC_ALREADY_INITIALIZED))
                {
                iRes = ML_OpenConnection(ptyMLHandle);
	            dbgprint("ML_OpenConnection: %d\n",iRes);
                if ((iRes == ERR_ML_ARC_NO_ERROR) || 
                    (iRes == ERR_ML_ARC_ALREADY_INITIALIZED))
                   bConnectionOk = 1;
         //       else
         //          GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));
                }
         //    else
         //       GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));
             // check if connection is ok
             if (!bConnectionOk)
                {  // there is no probe or error occured when openning connection
                return SC_ERR_OCS_OPEN;
                }
        }
    }
    if (SC_ERR_NONE != error1)
    {
        error = jca_Quit(pAdapterInform);
    }
    else
    {
        // Open the log file.
        //pAdapterInform->logFile = fopen ("jcaLogFile.txt", "a");
#if (MDEBUG == 1)
        pAdapterInform->logFile = fopen ("jcaLogFile.txt", "w");
        fprintf(pAdapterInform->logFile,"Open--------------------------\n");
#endif        
    }
    bFPGALoaded = 1;
    pAdapterInform->lastAddr     = 0; 
    pAdapterInform->lastCpldStat = 0x0720;
    dTestFreq=0;
    dTestDiv=0;
   
    res=SetJtagFreq(1.00, &dTestFreq, &dTestDiv);
    jca_SingleRegisterWrite(pAdapterInform,0x01630000 , 0x002F);
    Sleep(100);
    dbgprint("Res:%d\tPLL Freq:%lf\tDiv:%lf\n",res,dTestFreq,dTestDiv);
   //{//dbg
   //uint32_t tdbgdata;
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x2C<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x30<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x34<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x38<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x3C<<0), &tdbgdata);
   //
   //jca_SingleRegisterWrite(pAdapterInform,0x016B0000 | (0x2C<<0), 0xAA55);
   //jca_SingleRegisterWrite(pAdapterInform,0x016B0000 | (0x30<<0), 0x55AA);
   //jca_SingleRegisterWrite(pAdapterInform,0x016B0000 | (0x34<<0), 0xA5A5);
   //jca_SingleRegisterWrite(pAdapterInform,0x016B0000 | (0x38<<0), 0x5A5A);
   //jca_SingleRegisterWrite(pAdapterInform,0x016B0000 | (0x3C<<0), 0x1234);
   //   
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x2C<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x30<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x34<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x38<<0), &tdbgdata);
   //jca_SingleRegisterRead(pAdapterInform,0x016B0000 | (0x3C<<0), &tdbgdata);
   //
   //jca_SingleRegisterRead(pAdapterInform,0x01680038, &tdbgdata);
   //}
    return error;
}

/*******************************************************************************
* NAME: jca_Quit()
*
* DESCRIPTION
*   Software close a communications adapter.
*
* NOTES
*   This function undoes the effects of the jca_Init() function.    
*
*******************************************************************************/

JTI_Error
jca_Quit(
    JCA_Inform *pAdapterInform) /* Communications adapter handle. */
{
    JTI_Error error  = SC_ERR_NONE;
    JTI_Error error1 = SC_ERR_NONE;
    dbgprint("jca_Quit\n");
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }
    
    if (NULL != pAdapterInform)
    {
        pAdapterInform->mapHandle = INVALID_HANDLE_VALUE;
        /* Free the second read buffer  */
        {
            uint32_t size = pAdapterInform->bufferSize;
            void    *pntr = pAdapterInform->pReadBuffer2;

            uscif_free(&pntr, &size);
        }

        /* Free the write buffer  */
        {
            uint32_t size = pAdapterInform->bufferSize;
            void    *pntr = pAdapterInform->pWriteBuffer1;

            uscif_free(&pntr, &size);
        }

        /* Free the first read buffer  */
        {
            uint32_t size = pAdapterInform->bufferSize;
            void    *pntr = pAdapterInform->pReadBuffer1;

            uscif_free(&pntr, &size);
        }
        pAdapterInform->pReadBuffer2  = NULL;
        pAdapterInform->pReadBuffer1  = NULL;
        pAdapterInform->pWriteBuffer1 = NULL;
        pAdapterInform->bufferSize    = 0;

    }

    /* Free the dynamic libraries. */
    error1 = CFG_FreeLibrary();
    if (SC_ERR_NONE == error)
    {
        error = error1;
#if (MDEBUG == 1)        
        // Close the log file.
        if(NULL != pAdapterInform->logFile)
        {
            fflush(pAdapterInform->logFile);
            fclose(pAdapterInform->logFile);
        }
#endif
    }
    
    /* Close the handle. */
    if (ptyMLHandle != NULL)
		{  // close probe connection
		(void)ML_CloseConnection(ptyMLHandle);
		free(ptyMLHandle);
		bFPGALoaded = 0;
		}
		
    return error;
}

/*******************************************************************************
* NAME: jca_Connect()
*
* DESCRIPTION
*   Hardware connect to a communications adapter.
*
* NOTES
*   This performs the communications-adapter hardware actions required to 
*   access to the scan-controller and utility-controller hardware from the 
*   location at which the JSC/JUC API's are executing, through all 
*   communications infrastructure, up to (but not including) the 
*   scan-controller and utility-controller. This specifically excludes the 
*   scan-controller and utility-controller. It also excludes the JTAG 
*   connection to the target devices. This API does not perform any actions 
*   that query or test the scan-controller and utility-controller, or the 
*   JTAG connection, or communicate with target devices.    
*
*******************************************************************************/

JTI_Error
jca_Connect(
    JCA_Inform *pAdapterInform) /* Communications adapter handle. */
{
    JTI_Error error  = SC_ERR_NONE;
    dbgprint("jca_Connect\n");
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    return error;
}

/*******************************************************************************
* NAME: jca_Disconnect()
*
* DESCRIPTION
*   Hardware disconnect from a communications adapter.
*
* NOTES
*   This function undoes the effects of the jca_Connect() function.
*
*******************************************************************************/

JTI_Error
jca_Disconnect(
    JCA_Inform *pAdapterInform) /* Communications adapter handle. */
{
    JTI_Error error  = SC_ERR_NONE;
    dbgprint("jca_Disconnect\n");
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    return error;
}

/*******************************************************************************
* NAME: GetPortAddress()
*
* DESCRIPTION
*   Fetch the port address for the adapter.
*
* NOTES
*
*******************************************************************************/

JTI_Error
GetPortAddress(
    CFG_Handle cfgHandle,    /* The board configuration handle. */
    logic_t   *pUsePort,     /* Returns a flag that the address was used. */
    uint32_t  *pPortAddress) /* Returns the value of the port address. */
{
    JTI_Error error  = SC_ERR_NONE;

    SEPK_Variable *pSepkVariable;
    logic_t  usePort;
    uint32_t portAddress;
    dbgprint("GetPortAddress\n");
    /* A NULL pointer value means a stupid coding error. */
    if ((NULL == cfgHandle) || (NULL == pUsePort) || (NULL == pPortAddress))
    {
        error = SC_ERR_CMD_PARM;
    }
    if (SC_ERR_NONE == error)
    {
        /* Create and initialise the structure of configuration options. */
        if (READ_SepkCreate(&pSepkVariable))
        {
            error = SC_ERR_SYS_MEM_ALLOC;
        }
        if (SC_ERR_NONE == error)
        {
            READ_SepkInitial(pSepkVariable);
        }
        if (SC_ERR_NONE == error)
        {
            /* Read the variables for the configuration options. */
            error = READ_SepkVariable(cfgHandle, pSepkVariable);
            if (SC_ERR_NONE == error)
            /* Save the configuration options. */
            {
               /*
                * This configure variable is the value
                * of the sourceless EPK address or IP port.
                */
                portAddress = pSepkVariable->portAddress;
                usePort     = pSepkVariable->usePort;

                if (SC_ERR_NONE == error)
                {
                    /* Delete the structure of configuration options. */
                    if (READ_SepkDelete(&pSepkVariable))
                    {
                        error = SC_ERR_SYS_MEM_FREE;
                    }
                }

                if (SC_ERR_NONE == error)
                {
                    /* Return the results. */
                    *pUsePort      = usePort;
                    *pPortAddress  = portAddress;
                }
            }
        }
    }

    return error;
}

/* -- END OF FILE -- */
