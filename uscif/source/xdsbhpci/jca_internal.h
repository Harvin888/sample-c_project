/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_internal.h
*
* DESCRIPTION
*   The private (internal) header file for use by JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_internal_h
#define jca_internal_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The private typedef's.
*******************************************************************************/

/*
 * This structure is used to store the client specific JCA state.
 * Its details are hidden from clients behind the opaque JCA_Handle.
 */
typedef struct JCA_Inform
{
    HANDLE mapHandle;

    volatile DWORD *pCpld;
    volatile DWORD *pPll;
    volatile DWORD *pCable;
    volatile DWORD *pTimer;
    volatile DWORD *pTbc0;
    volatile DWORD *pTbc1;
    volatile DWORD *pTbc2;
    volatile DWORD *pTbc3;
    volatile DWORD *pEmif;

    uint32_t  bufferSize;
    void     *pReadBuffer1;
    void     *pWriteBuffer1;
    void     *pReadBuffer2;

    JCA_Timeout dataTransfer;
    JCA_Timeout statusPoll;

    uint32_t     testMode;
    FILE        *logFile; 

    uint32_t     lastAddr;
    uint32_t     lastCpldStat;

} JCA_Inform;

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_internal_h */

/* -- END OF FILE -- */
