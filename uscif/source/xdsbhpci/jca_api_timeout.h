/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_timeout.h
*
* DESCRIPTION
*   Timeout and delay functions for the JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_api_timeout_h
#define jca_api_timeout_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern JTI_Error
jca_WaitForTime(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    timeDelay);     /* A delay measured in micro-seconds. */

extern JTI_Error
jca_WaitForTclk(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    periodDelay);   /* A delay measured in TCLK periods. */

extern JTI_Error
jca_TimeOut(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    JCA_Timeout dataTransfer,   /* Select the data transfer timeout. */
    JCA_Timeout statusPoll);    /* Select the status poll timeout. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_api_timeout_h */

/* -- END OF FILE -- */
