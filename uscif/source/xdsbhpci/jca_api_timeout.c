/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_timeout.c
*
* DESCRIPTION
*   Timeout and delay functions for the JCA.
*
* PUBLIC FUNCTIONS
*
*   jca_WaitForTime() - Wait until a specified delay in time has occurred.
*   jca_WaitForTclk() - Wait until a specified delay in TCLK's has occurred.
*   jca_TimeOut()     - Select the timeout for data transfers and status pollings.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_timeout_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "xds_variable.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_timeout.h"

/*******************************************************************************
* NAME: jca_WaitForTime()
*
* DESCRIPTION
*   Wait until a specified delay in time has occurred.
*
* NOTES
*   This function waits until a time delay of at least
*   the requested number of micro-seconds has occurred.  
*
*******************************************************************************/

JTI_Error
jca_WaitForTime(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    timeDelay)      /* A delay measured in micro-seconds. */
{
    LARGE_INTEGER startTime;
    LARGE_INTEGER endTime;
    LARGE_INTEGER frequency;

    uint32_t deltaTime;

    if (NULL == pAdapterInform)
    {
        return SC_ERR_CMD_PARM;
    }

    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&startTime);

    do
    {
        QueryPerformanceCounter(&endTime);

        deltaTime = (uint32_t)((endTime.QuadPart - startTime.QuadPart) /
                                   (frequency.QuadPart));
    }
    while (deltaTime < timeDelay);

    return SC_ERR_NONE;
}

/*******************************************************************************
* NAME: jca_WaitForTclk()
*
* DESCRIPTION
*   Wait until a specified delay in TCLK's has occurred.
*
* NOTES
*   This function waits until a time delay of at least
*   the requested number of TCLK periods has occurred.   
*
*******************************************************************************/

JTI_Error
jca_WaitForTclk(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    periodDelay)    /* A delay measured in TCLK periods. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }
    UNUSED_PARAM(periodDelay);

    return error;
}

/*******************************************************************************
* NAME: jca_TimeOut()
*
* DESCRIPTION
*   Select the timeout for data transfers and status pollings.
*
* NOTES
*   This function specifies the timeout values used by
*   the jca_BufferDataTransfer() and jca_PollStatus() commands. 
*
*******************************************************************************/

JTI_Error
jca_TimeOut(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    JCA_Timeout dataTransfer,   /* Select the data transfer timeout. */
    JCA_Timeout statusPoll)     /* Select the status poll timeout. */
{
    if (NULL == pAdapterInform)
    {
        return SC_ERR_CMD_PARM;
    }

    /* Decode data transfer timeout and check for valid value.  */
    switch (dataTransfer)
    {
        case eJCA_TIMEOUT_ZERO:  /* The timeout occurs if first test fails. */
            pAdapterInform->dataTransfer = (int32_t)0;
            break;

        case eJCA_TIMEOUT_TINY:  /* The timeout is 1/256 second. */
            pAdapterInform->dataTransfer = (int32_t)3906;
            break;

        case eJCA_TIMEOUT_SHORT: /* The timeout is 1/16 second. */
            pAdapterInform->dataTransfer = (int32_t)62500;
            break;

        case eJCA_TIMEOUT_LONG:  /* The timeout is 1 second. */
            pAdapterInform->dataTransfer = (int32_t)1000000;
            break;

        case eJCA_TIMEOUT_NEVER: /* The timeout never occurs. */
            pAdapterInform->dataTransfer = (int32_t)0xFFFFFFFF;
            break;

        default:
            return SC_ERR_CMD_PARM;
            break;
    }

    /* Decode status poll timeout and check for valid value.  */
    switch (statusPoll)
    {
        case eJCA_TIMEOUT_ZERO:  /* The timeout occurs if first test fails. */
            pAdapterInform->statusPoll = (JCA_Timeout)0;
            break;

        case eJCA_TIMEOUT_TINY:  /* The timeout is 1/256 second. */
            pAdapterInform->statusPoll = (JCA_Timeout)3906;
            break;

        case eJCA_TIMEOUT_SHORT: /* The timeout is 1/16 second. */
            pAdapterInform->statusPoll = (JCA_Timeout)62500;
            break;

        case eJCA_TIMEOUT_LONG:  /* The timeout is 1 second. */
            pAdapterInform->statusPoll = (JCA_Timeout)1000000;
            break;

        case eJCA_TIMEOUT_NEVER: /* The timeout never occurs. */
            pAdapterInform->statusPoll = (JCA_Timeout)0xFFFFFFFF;
            break;

        default:
            return SC_ERR_CMD_PARM;
            break;
    }

    return SC_ERR_NONE;
}

/* -- END OF FILE -- */
