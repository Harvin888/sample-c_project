/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_main.h
*
* DESCRIPTION
*   Open and close the JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_api_main_h
#define jca_api_main_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern JTI_Error
jca_Create(
    JCA_Inform **pAdapterInform); /* Pointer to comm�s adapter handle. */

extern JTI_Error
jca_Delete(
    JCA_Inform **pAdapterInform); /* Pointer to comm�s adapter handle. */

extern JTI_Error
jca_Init(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    CFG_Handle  adapterConfig,  /* Variables to configure adapter. */
    JTI_Client  jtiClientType); /* Type of the JTI client using JCA. */

extern JTI_Error
jca_Quit(
    JCA_Inform *pAdapterInform); /* Communications adapter handle. */

extern JTI_Error
jca_Connect(
    JCA_Inform *pAdapterInform); /* Communications adapter handle. */

extern JTI_Error
jca_Disconnect(
    JCA_Inform *pAdapterInform); /* Communications adapter handle. */

extern char *
POD_GetParent(void);

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_api_main */

/* -- END OF FILE -- */
