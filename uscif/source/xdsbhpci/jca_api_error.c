/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_error.c
*
* DESCRIPTION
*   Read the error values from the JCA.
*
* PUBLIC FUNCTIONS
*
*   jca_ReadError() - Read an error value from the communications adapter.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_error_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "xds_variable.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_error.h"

/*******************************************************************************
* NAME: jca_ReadError()
*
* DESCRIPTION
*   Read an error value from the communications adapter.
*
* NOTES
*
*******************************************************************************/

JTI_Error
jca_ReadError(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    JTI_Error  *pErrorReturn)   /* Returned error value. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(pErrorReturn);

    return error;
}

/* -- END OF FILE -- */
