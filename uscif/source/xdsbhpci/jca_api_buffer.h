/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_buffer.h
*
* DESCRIPTION
*   Buffer read and write functions for the JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_api_buffer_h
#define jca_api_buffer_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern JTI_Error
jca_BufferDataTransfer( 
    JCA_Inform  *pAdapterInform, /* Communications adapter handle. */
    JCA_XferType accessType,     /* The type of data transfer. */
    uint32_t     bitCount);      /* The number of bits. */

extern JTI_Error
jca_BufferDataRead08(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint8_t    *pAccessData,    /* Array of buffer values. */
    uint32_t    countData);     /* Number of buffer values. */

extern JTI_Error
jca_BufferDataWrite08(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint8_t    *pAccessData,    /* Array of buffer values. */
    uint32_t    countData);     /* Number of buffer values. */

extern JTI_Error
jca_BufferDataRead16(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint16_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData);     /* Number of buffer values. */

extern JTI_Error
jca_BufferDataWrite16(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint16_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData);     /* Number of buffer values. */

extern JTI_Error
jca_BufferDataRead32(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint32_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData);     /* Number of buffer values. */

extern JTI_Error
jca_BufferDataWrite32(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint32_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData);     /* Number of buffer values. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_api_buffer_h */

/* -- END OF FILE -- */
