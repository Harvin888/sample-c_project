/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_poll.c
*
* DESCRIPTION
*   Poll register status via the JCA.
*
* PUBLIC FUNCTIONS
*
*   jca_StatusPoll() - Poll for predefined status in one or more registers.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_poll_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "xds_variable.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_poll.h"

/*******************************************************************************
* NAME: jca_StatusPoll()
*
* DESCRIPTION
*   Poll for predefined status in one or more registers.
*
* NOTES
*   This function polls for predefined status conditions in one or more 
*   hardware register addresses. This function uses the JCA_StatusPoll 
*   enumerations for status conditions that are evaluated from one or more 
*   register fields and a test value.
*
*   If the status condition does not occur within a reasonable time then a 
*   timeout error will be returned. The associated jca_Timeout() function 
*   configures the 'reasonable time'.
*
*******************************************************************************/

JTI_Error
jca_StatusPoll(
    JCA_Inform    *pAdapterInform, /* Communications adapter handle. */
    JCA_StatusType statusType,    /* The status being polled. */
    uint32_t       testValue)      /* The value being tested. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(statusType);
    UNUSED_PARAM(testValue);

    return error;
}

/* -- END OF FILE -- */
