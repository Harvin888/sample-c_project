/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_semaphore.c
*
* DESCRIPTION
*   Access multiple-client semaphores via the JCA.
*
* NOTES
*   This sub-set of the JCA API's are intended to allow multiple clients to 
*   share the same underlying scan-controller and utility-controller by 
*   acquiring and releasing a semaphore. The generation of semaphore names 
*   appropriate for the operating environment and the creation and deletion 
*   of semaphores occurs within the jca_Connect() and jca_Disconnect() 
*   functions. These acquire and release API's do not perform any actions 
*   that test the JTAG connection and/or communicate with target devices.
*
* PUBLIC FUNCTIONS
*
*   jca_AcquireSemaphore() - Acquire the communications adapter semaphore.
*   jca_ReleaseSemaphore() - Release the communications adapter semaphore.
*   jca_QuerySemaphore()   - Query the communications adapter client count.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_semaphore_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "reg_base560.h"

#include "xds_variable.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_semaphore.h"

/*******************************************************************************
* NAME: jca_AcquireSemaphore()
*
* DESCRIPTION
*   Acquire the communications adapter semaphore.
*
* NOTES
*   This function acquires ownership of the semaphore for the client. In an 
*   environment that permits multiple tasks or processes this API will block 
*   until all other clients have released ownership of the semaphore.
*
*******************************************************************************/

JTI_Error
jca_AcquireSemaphore(
    JCA_Inform *pAdapterInform) /* Communication adapter handle. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    return error;
}

/*******************************************************************************
* NAME: jca_ReleaseSemaphore()
*
* DESCRIPTION
*   Release the communications adapter semaphore.
*
* NOTES
*   This function releases ownership of the semaphore by the client. In an 
*   environment that permits multiple tasks or processes this API will allow 
*   other clients to obtain ownership of the semaphore.
*
*******************************************************************************/

JTI_Error
jca_ReleaseSemaphore(
    JCA_Inform *pAdapterInform) /* Communication adapter handle. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    return error;
}

/*******************************************************************************
* NAME: jca_QuerySemaphore()
*
* DESCRIPTION
*   Query the communications adapter client count.
*
* NOTES
*   This API is most commonly used to determine if the current client is the 
*   'first opened' or 'last closed' and perform custom start-up and shut-down 
*   actions on the first connect and last disconnect of the scan-controller
*   and  utility-controller.
*
*******************************************************************************/

JTI_Error
jca_QuerySemaphore(
    JCA_Inform *pAdapterInform, /* Communication adapter handle. */
    uint32_t   *pClientNumber)  /* Number of JCA clients connected. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }
    UNUSED_PARAM(pClientNumber);

    return error;
}

/* -- END OF FILE -- */
