/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_register.h
*
* DESCRIPTION
*   Read and write registers via the JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_api_register_h
#define jca_api_register_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern JTI_Error
jca_SingleRegisterRead( 
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t   *pAccessData);   /* The pointer to the register value. */

extern JTI_Error
jca_SingleRegisterWrite(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t    accessData);    /* The register value. */

extern JTI_Error
jca_MultipleRegisterRead(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t   *pAccessAddr,    /* Array of register addresses. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData);     /* Number of addresses and values. */


extern JTI_Error
jca_MultipleRegisterWrite(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t   *pAccessAddr,    /* Array of register addresses. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData);     /* Number of addresses and values. */

extern JTI_Error
jca_ArrayRegisterRead(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData);     /* Number of register values. */

extern JTI_Error
jca_ArrayRegisterWrite(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData);     /* Number of register values. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_api_register_h */

/* -- END OF FILE -- */
