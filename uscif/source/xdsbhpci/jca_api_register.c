/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_register.c
*
* DESCRIPTION
*   Read and write registers via the JCA.
*
* PUBLIC FUNCTIONS
*
*   jca_SingleRegisterRead()   - Read a single hardware register.
*   jca_SingleRegisterWrite()  - Write a single hardware register.
*
*   jca_MultipleRegisterRead() - Read many registers at different addresses.
*   jca_MultipleRegisterWrite()- Write many registers at different addresses.
*
*   jca_ArrayRegisterRead()    - Read many registers at a single address.
*   jca_ArrayRegisterWrite()   - Write many registers at a single address.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_register_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "reg_base560.h"

#include "xds_variable.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_register.h"
#include "..\..\debug.h"
#include "..\..\ml_ash510.h"
#include "reg_nanotbc.h"

/*******************************************************************************
* NAME: jca_SingleRegisterRead()
*
* DESCRIPTION
*   Read a single hardware register.
*
* NOTES
*   This function reads a single 32-bit value from a hardware register.
*   If the register is less than 32-bits wide the unused bits are undefined.
*
*******************************************************************************/

JTI_Error  
jca_SingleRegisterRead( 
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t   *pAccessData)    /* The pointer to the register value. */
{
    JTI_Error   error  = SC_ERR_NONE;
    uint32_t data   = 0;
    uint32_t offset = 0;
    uint32_t block  = 0;

    static int bProgrammingInProgress = 0;
    static int iAttempt = 0;
    
#if (MDEBUG == 1)    
    fprintf(pAdapterInform->logFile,"Rd 0x%08X=",accessAddr);
    fflush(pAdapterInform->logFile);
#endif    
    if ((NULL == pAdapterInform) || (NULL == pAccessData))
    {
        error = SC_ERR_CMD_PARM;
    }

    offset = (accessAddr & (uint32_t)0xFF) >> 1;
    block  = (accessAddr & 0x000F0000) >> 11; //( >>12 <<1)
    
   //nch todo: this is not needed. Remove.
   //nch leave for debug.
    if (SC_ERR_NONE == error)
    {
        switch (accessAddr & 0xFFF00000)
        {
            case SC_BLCK_CPLD:
                ////data = (uint32_t)pAdapterInform->pCpld[offset];
                if(0x01600000 != pAdapterInform->lastAddr)
                {
                    data = pAdapterInform->lastCpldStat;
                }
                else
                {
                    if(0x0720 == pAdapterInform->lastCpldStat)
                    {
                        data = 0x0420;
                        pAdapterInform->lastCpldStat = 0x0420;
                    }
                    else if(0x0420 == pAdapterInform->lastCpldStat)
                    {
                        data = 0x0520;
                        pAdapterInform->lastCpldStat = 0x0520;
                    }
                    else if(0x0520 == pAdapterInform->lastCpldStat)
                    {
                        data = 0x0720;
                        pAdapterInform->lastCpldStat = 0x0720;
                    }
                }
                break;
                
            case SC_BLCK_FPGA:
                //nch todo: data types
                bProgrammingInProgress = 0;
                if ((offset == 0x00) && 
                    (block  == 0x00))
				    {
				        bProgrammingInProgress = 1;
                    data = 0;
                    error = SC_ERR_CMD_PARM;
				    }
#if (MIKEMOD == 1)
				    else if(0x01650000 == accessAddr)
                {
                    data=0x00000014;
                }
                else if(0x01650004 == accessAddr)
                {
                    data=0x00003400;
                }
#endif               
                else if(0x01670018 == accessAddr)
                {
//                    data &= ~0x00000800;
                    data = 0x00001003;
                }
/*                else if(0x01680020 == accessAddr)
                {
                    data |= 0x00000410;
                }*/                
                else
				    {
                    data = memReadTBC_Reg(0x400 | block | offset);//0x400 is (0x200 << 1)
				    }
                break;

            default:
                error = SC_ERR_CMD_PARM;
                break;
        }
    }

    if (SC_ERR_NONE == error)
    {
        *pAccessData = data;
//        dbgprint("Rd %08lX=%08lX\n",accessAddr, data);
    }

    pAdapterInform->lastAddr = accessAddr;
#if (MDEBUG == 1)
    fprintf(pAdapterInform->logFile,"0x%04X  %d\n", data & 0xFFFF, error);
    fflush(pAdapterInform->logFile);
#endif
    return error;
}

/*******************************************************************************
* NAME: jca_SingleRegisterWrite()
*
* DESCRIPTION
*   Write a single hardware register.
*
* NOTES
*   This function writes a single 32-bit value to a hardware register.
*   If the register is less than 32-bits wide the unused bits are ignored.  
*
*******************************************************************************/

JTI_Error
jca_SingleRegisterWrite(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t    accessData)     /* The register value. */
{
    JTI_Error   error  = SC_ERR_NONE;
    uint32_t adrs      = accessAddr;
    uint32_t data      = accessData;
    uint32_t offset    = 0;
    uint32_t block  = 0;
#if (MDEBUG == 1)
    fprintf(pAdapterInform->logFile,"Wr 0x%08X=",accessAddr);
    fflush(pAdapterInform->logFile);
#endif
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;;
    }

    offset = (adrs & (uint32_t)0xFF) >> 1;      //( >>2 <<1)
    block  = (adrs & 0x000F0000) >> 11;   //( >>12 <<1)
       
   //nch todo: this is not needed. Remove.
   //nch leave for debug.
    if (SC_ERR_NONE == error)
    {
#if (MDEBUG == 1)    
        fprintf(pAdapterInform->logFile,"0x%04X  ", accessData & 0xFFFF);
        fflush(pAdapterInform->logFile);
#endif        
        switch (adrs & (uint32_t)0xFFF00000)
        {
            case SC_BLCK_CPLD:
//                dbgprint("jca_SingleRegisterWrite:CPLD:%08lX\n",offset);
                break;
                
            case SC_BLCK_FPGA:
                //nch todo: data types
#if (MIKEMOD == 1)
                if(0x016B0014 == accessAddr)
                {
                    data |= 0x0000C000;
                }
#endif
               if(0x01630000 == accessAddr)
                {
                    data = 0x2F;
                }
                
                memWriteTBC_Reg(0x400 | block | offset, (unsigned short)data);
                break;

            default:
                error = SC_ERR_CMD_PARM;
                break;
        }
    } 
//   dbgprint("Wr %08lX=%08lX\n",adrs, data);
    pAdapterInform->lastAddr = accessAddr;
#if (MDEBUG == 1)
    fprintf(pAdapterInform->logFile,"%d\n", error);
    fflush(pAdapterInform->logFile);
#endif
    return error;;
}

/*******************************************************************************
* NAME: jca_MultipleRegisterRead()
*
* DESCRIPTION
*   Read many registers at different addresses.
*
* NOTES
*   This function reads a list of one or more 32-bit values from to one
*   or more sequential or non-sequential hardware register addresses.
*   If the registers are less than 32-bits wide the unused bits are undefined.
*
*******************************************************************************/

JTI_Error
jca_MultipleRegisterRead(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t   *pAccessAddr,    /* Array of register addresses. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData)      /* Number of addresses and values. */
{
    JTI_Error error = SC_ERR_NONE;
    uint32_t  currentCount;
    uint32_t  currentAddr;

    if (NULL == pAdapterInform || 
        NULL == pAccessAddr    || 
        NULL == pAccessData    ||
        0    == countData)
    {
        error = SC_ERR_CMD_PARM;;
    }

    if (SC_ERR_NONE == error)
    {
        for (currentCount = 0; currentCount < countData; currentCount++)
        {
            currentAddr = *pAccessAddr++;
            error = jca_SingleRegisterRead(
                        pAdapterInform, 
                        currentAddr, 
                        pAccessData);
            if (error)
            {
                break;
            }
            pAccessData++;
        }
    }

    return error;
}

/*******************************************************************************
* NAME: jca_MultipleRegisterWrite()
*
* DESCRIPTION
*   Write many registers at different addresses.
*
* NOTES
*   This function writes a list of one or more 32-bit values to one or
*   more sequential or non- sequential hardware register addresses.
*   If the registers are less than 32-bits wide the unused bits are ignored.
*
*******************************************************************************/

JTI_Error
jca_MultipleRegisterWrite(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t   *pAccessAddr,    /* Array of register addresses. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData)      /* Number of addresses and values. */
{
    JTI_Error error      = SC_ERR_NONE;
    uint32_t  currentCount;
    uint32_t  currentAddr;
    uint32_t  currentData;

    if (NULL == pAdapterInform || 
        NULL == pAccessAddr    || 
        NULL == pAccessData    ||
        0    == countData)
    {
        error = SC_ERR_CMD_PARM;
    }

    if (SC_ERR_NONE == error)
    {
        for (currentCount = 0; currentCount < countData; currentCount++)
        {
            currentAddr = *pAccessAddr++;
            currentData = *pAccessData++;
            error = jca_SingleRegisterWrite(
                        pAdapterInform, 
                        currentAddr, 
                        currentData);
            if (error)
            {
                break;
            }
        }
    }

    return error;
}

/*******************************************************************************
* NAME: jca_ArrayRegisterRead()
*
* DESCRIPTION
*   Read many registers at a single address.
*
* NOTES
*   This function reads an array of one or more 32-bit
*   values from a single hardware register address.
*   If the register is less than 32-bits wide the unused bits are undefined.
*
*******************************************************************************/

JTI_Error
jca_ArrayRegisterRead(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData)      /* Number of register values. */
{
    JTI_Error error = SC_ERR_NONE;
    uint32_t  currentCount;

    if (NULL == pAdapterInform || 
        NULL == pAccessData    ||
        0    == countData)
    {
        error = SC_ERR_CMD_PARM;
    }

    if (SC_ERR_NONE == error)
    {
        for (currentCount = 0; currentCount < countData; currentCount++)
        {
            error = jca_SingleRegisterRead(
                        pAdapterInform, 
                        accessAddr, 
                        pAccessData);
            if (error)
            {
                break;
            }
            pAccessData++;
        }
    }

    return error;
}

/*******************************************************************************
* NAME: jca_ArrayRegisterWrite()
*
* DESCRIPTION
*   Write many registers at a single address.
*
* NOTES
*   This function writes an array of one or more 32-bit
*   values to a single hardware register address.
*   If the register is less than 32-bits wide the unused bits are ignored.
*
*******************************************************************************/

JTI_Error
jca_ArrayRegisterWrite(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessAddr,     /* The register address. */
    uint32_t   *pAccessData,    /* Array of register values. */
    uint32_t    countData)      /* Number of register values. */
{
    JTI_Error error      = SC_ERR_NONE;
    uint32_t  currentCount;
    uint32_t  currentData;

    if (NULL == pAdapterInform || 
        NULL == pAccessData    ||
        0    == countData)
    {
        error = SC_ERR_CMD_PARM;
    }

    if (SC_ERR_NONE == error)
    {
        for (currentCount = 0; currentCount < countData; currentCount++)
        {
            currentData = *pAccessData++;
            error = jca_SingleRegisterWrite(
                        pAdapterInform, 
                        accessAddr, 
                        currentData);
            if (error)
            {
                break;
            }
        }
    }

    return error;
}

/* -- END OF FILE -- */
