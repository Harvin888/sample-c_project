/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_program.c
*
* DESCRIPTION
*   Program an FPGA/CPLD via the JCA.
*
* PUBLIC FUNCTIONS
*
*   jca_ProgramStart()    - Start programming the controller and utility FPGA/CPLD�s.
*   jca_ProgramTransfer() - Transfer data to program the controller and utility FPGA/CPLD�s.
*   jca_ProgramFinish()   - Finish programming the controller and utility FPGA/CPLD�s.
*   jca_ProgramVersion()  - Query the version of the controller and utility FPGA/CPLD�s.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_program_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The legacy header file.
 */
#include "xdsfast3.h"

/*
 * The other header files.
 */
#include "xds_variable.h"

#include "reg_base560.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_program.h"

/*******************************************************************************
* NAME: jca_ProgramStart()
*
* DESCRIPTION
*   Start programming the controller and utility FPGA/CPLD's. 
*
* NOTES
*
*******************************************************************************/

JTI_Error
jca_ProgramStart(
    JCA_Inform     *pAdapterInform, /* Communications adapter handle. */
    JCA_ProgramType hardwareType)   /* The FPGA/CPLD type. */
{
    JTI_Error error  = SC_ERR_NONE;


    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(hardwareType);
#if 0
    uint16_t        fpgaType = 0;
    uint32_t        fpgaVersion = 0, fpgaCheckSum = 0;
    uint32_t        fpgaDataSize = 0, fpgaXfer = 0;
    unsigned char   fpgaDataValue = 0;
    uint16_t        fpgaMode = SC_FPGA_LOAD_BEFOR;
    
    switch (hardwareType)
    {
    case eJCA_PROGRAM_TYPE_EMULATOR:
    case eJCA_PROGRAM_TYPE_CLIENT:
        error = fpga_client_program(pAdapterInform, fpgaType, 
                    fpgaMode, fpgaVersion, fpgaCheckSum, fpgaDataSize, 
                    &fpgaDataValue, &fpgaXfer);
        break;

    case eJCA_PROGRAM_TYPE_SERVER:
        error = fpga_server_program(pAdapterInform, fpgaType, 
                    fpgaMode, fpgaVersion, fpgaCheckSum, fpgaDataSize, 
                    &fpgaDataValue, &fpgaXfer);
        break;

    case eJCA_PROGRAM_TYPE_POD:
    default:
        error = SC_ERR_CMD_PARM;
    }
#endif

    return error;
}

/*******************************************************************************
* NAME: jca_ProgramTransfer()
*
* DESCRIPTION
*   Transfer data to program the controller and utility FPGA/CPLD's.  
*
* NOTES
*
*******************************************************************************/

JTI_Error
jca_ProgramTransfer(
    JCA_Inform     *pAdapterInform, /* Communications adapter handle. */
    JCA_ProgramType hardwareType,   /* The FPGA/CPLD type. */
    unsigned char  *pProgramData,   /* The FPGA/CPLD programming data. */
    uint32_t        programNumber,  /* The number of data values. */
    uint32_t       *pProgramStatus) /* The result status. */
{

    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(hardwareType);
    UNUSED_PARAM(pProgramData);
    UNUSED_PARAM(programNumber);
    UNUSED_PARAM(pProgramStatus);
#if 0
    uint16_t        fpgaType = 0;
    uint16_t        fpgaMode = SC_FPGA_LOAD_DATA;
    uint32_t        fpgaVersion = 0, fpgaCheckSum = 0;

    switch (hardwareType)
    {
    case eJCA_PROGRAM_TYPE_EMULATOR:
    case eJCA_PROGRAM_TYPE_CLIENT:
        error = fpga_client_program(pAdapterInform, fpgaType, 
                    fpgaMode, fpgaVersion, fpgaCheckSum, programNumber, 
                    pProgramData, pProgramStatus);
        break;

    case eJCA_PROGRAM_TYPE_SERVER:
        error = fpga_server_program(pAdapterInform, fpgaType, 
                    fpgaMode, fpgaVersion, fpgaCheckSum, programNumber, 
                    pProgramData, pProgramStatus);
        break;

    case eJCA_PROGRAM_TYPE_POD:
    default:
        error = SC_ERR_CMD_PARM;
    }
#endif

    return error;
}

/*******************************************************************************
* NAME: jca_ProgramFinish()
*
* DESCRIPTION
*   Finish programming the controller and utility FPGA/CPLD's.  
*
* NOTES
*
*******************************************************************************/

JTI_Error
jca_ProgramFinish(
    JCA_Inform     *pAdapterInform, /* Communications adapter handle. */
    JCA_ProgramType hardwareType,   /* The FPGA/CPLD type. */
    uint32_t       *pProgramStatus) /* The result status. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }
    UNUSED_PARAM(hardwareType);
    UNUSED_PARAM(pProgramStatus);
#if 0
    uint16_t        fpgaType = 0;
    uint32_t        fpgaVersion   = 0, fpgaCheckSum = 0;
    uint32_t        fpgaDataSize  = 0, fpgaXfer = 0;
    unsigned char   fpgaDataValue = 0;
    uint16_t        fpgaMode = SC_FPGA_LOAD_AFTER;
    
    UNUSED_PARAM(pProgramStatus);

    switch (hardwareType)
    {
    case eJCA_PROGRAM_TYPE_EMULATOR:
    case eJCA_PROGRAM_TYPE_CLIENT:
        error = fpga_client_program(pAdapterInform, fpgaType, 
                    fpgaMode, fpgaVersion, fpgaCheckSum, fpgaDataSize, 
                    &fpgaDataValue, &fpgaXfer);
        break;

    case eJCA_PROGRAM_TYPE_SERVER:
        error = fpga_server_program(pAdapterInform, fpgaType, 
                    fpgaMode, fpgaVersion, fpgaCheckSum, fpgaDataSize, 
                    &fpgaDataValue, &fpgaXfer);
        break;

    case eJCA_PROGRAM_TYPE_POD:
    default:
        error = SC_ERR_CMD_PARM;
    }
#endif

    return error;
}

/*******************************************************************************
* NAME: jca_ProgramVersion()
*
* DESCRIPTION
*   Query the version of the controller and utility FPGA/CPLD's
*
* NOTES
*
*******************************************************************************/

JTI_Error
jca_ProgramVersion(
    JCA_Inform     *pAdapterInform,   /* Communications adapter handle. */
    JCA_ProgramType hardwareType,     /* The FPGA/CPLD type. */
    uint32_t       *pHardwareVersion) /* The version status. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(hardwareType);
    UNUSED_PARAM(pHardwareVersion);
#if 0
    uint16_t    fpgaType = 0;
    uint16_t    hwRev = 0;
    
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        return SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    /* Apply the default version number. */
    *pHardwareVersion = (uint32_t)0x0000;
    
    switch (hardwareType)
    {
    case eJCA_PROGRAM_TYPE_EMULATOR:
    case eJCA_PROGRAM_TYPE_CLIENT:
        error = fpga_client_version(pAdapterInform, fpgaType, &hwRev);
        break;

    case eJCA_PROGRAM_TYPE_SERVER:
        error = fpga_server_version(pAdapterInform, fpgaType, &hwRev);
        break;

    case eJCA_PROGRAM_TYPE_POD:
    default:
        return SC_ERR_CMD_PARM;
    }
#endif


    return error;

}


/* -- END OF FILE -- */
