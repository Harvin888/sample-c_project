/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_poll.h
*
* DESCRIPTION
*   Poll register status via the JCA.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_api_poll_h
#define jca_api_poll_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern JTI_Error
jca_StatusPoll(
    JCA_Inform    *pAdapterInform, /* Communications adapter handle. */
    JCA_StatusType statusType,    /* The status being polled. */
    uint32_t       testValue);     /* The value being tested. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_api_poll_h */

/* -- END OF FILE -- */
