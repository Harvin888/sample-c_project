/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_api_buffer.c
*
* DESCRIPTION
*   Buffer read and write functions for the JCA.
*
* PUBLIC FUNCTIONS
*
*   jca_BufferDataTransfer() - Perform a buffer data transfer via the controller.
*
*   jca_BufferDataRead08()   - Read 8-bit data earlier taken from the controller.
*   jca_BufferDataWrite08()  - Write 8-bit data later given to the controller.
*
*   jca_BufferDataRead16()   - Read 16-bit data earlier taken from the controller.
*   jca_BufferDataWrite16()  - Write 16-bit data later given to the controller.
*
*   jca_BufferDataRead32()   - Read 32-bit data earlier taken from the controller.
*   jca_BufferDataWrite32()  - Write 32-bit data later given to the controller.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_api_buffer_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "reg_base560.h"

#include "xds_variable.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_api_buffer.h"

/*******************************************************************************
* NAME: jca_BufferDataTransfer()
*
* DESCRIPTION
*   Perform a buffer data transfer via the controller.
*
* NOTES
*   The data is output from write buffers within the JCA and written to the 
*   controller hardware.  The data is read to the controller hardware and 
*   input to read buffers within the JCA.
*   
*   If the data transfer does not occur within a reasonable time then a 
*   timeout error will be returned. The associated jca_Timeout() function 
*   configures the 'reasonable time'. 
*
*******************************************************************************/

JTI_Error
jca_BufferDataTransfer( 
    JCA_Inform  *pAdapterInform, /* Communications adapter handle. */
    JCA_XferType accessType,     /* The type of data transfer. */
    uint32_t     bitCount)       /* The number of bits. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessType);
    UNUSED_PARAM(bitCount);

    return error;
}

/*******************************************************************************
* NAME: jca_BufferDataRead08()
*
* DESCRIPTION
*   Read 8-bit data earlier taken from the controller.
*
* NOTES
*   This function reads data from a selected buffer within the JCA 8 bits at 
*   a time.  The buffer is used to store data that was earlier read from the 
*   controller hardware.
*
*******************************************************************************/

JTI_Error
jca_BufferDataRead08(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint8_t    *pAccessData,    /* Array of buffer values. */
    uint32_t    countData)      /* Number of buffer values. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessSelect);
    UNUSED_PARAM(pAccessData);
    UNUSED_PARAM(countData);

    return error;
}

/*******************************************************************************
* NAME: jca_BufferDataWrite08()
*
* DESCRIPTION
*   Write 8-bit data later given to the controller.
*
* NOTES
*   This function writes data to a selected buffer within the JCA software 8 
*   bits at a time.  The buffer is used to store data that will be later 
*   written to the controller hardware
*
*******************************************************************************/

JTI_Error
jca_BufferDataWrite08(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint8_t    *pAccessData,    /* Array of buffer values. */
    uint32_t    countData)      /* Number of buffer values. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessSelect);
    UNUSED_PARAM(pAccessData);
    UNUSED_PARAM(countData);

    return error;
}

/*******************************************************************************
* NAME: jca_BufferDataRead16()
*
* DESCRIPTION
*   Read 16-bit data earlier taken from the controller.
*
* NOTES
*   This function reads data from a selected buffer within the JCA 16 bits at 
*   a time.  The buffer is used to store data that was earlier read from the 
*   controller hardware.
*
*******************************************************************************/

JTI_Error
jca_BufferDataRead16(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint16_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData)      /* Number of buffer values. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessSelect);
    UNUSED_PARAM(pAccessData);
    UNUSED_PARAM(countData);

    return error;
}

/*******************************************************************************
* NAME: jca_BufferDataWrite16()
*
* DESCRIPTION
*   Write 16-bit data later given to the controller.
*
* NOTES
*   This function writes data to a selected buffer within the JCA software 16 
*   bits at a time.  The buffer is used to store data that will be later 
*   written to the controller hardware
*
*******************************************************************************/

JTI_Error
jca_BufferDataWrite16(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint16_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData)      /* Number of buffer values. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessSelect);
    UNUSED_PARAM(pAccessData);
    UNUSED_PARAM(countData);

    return error;
}

/*******************************************************************************
* NAME: jca_BufferDataRead32()
*
* DESCRIPTION
*   Read 32-bit data earlier taken from the controller.
*
* NOTES
*   This function reads data from a selected buffer within the JCA 32 bits at 
*   a time.  The buffer is used to store data that was earlier read from the 
*   controller hardware.
*
*******************************************************************************/

JTI_Error
jca_BufferDataRead32(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint32_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData)      /* Number of buffer values. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessSelect);
    UNUSED_PARAM(pAccessData);
    UNUSED_PARAM(countData);

    return error;
}

/*******************************************************************************
* NAME: jca_BufferDataWrite32()
*
* DESCRIPTION
*   Write 32-bit data later given to the controller.
*
* NOTES
*   This function writes data to a selected buffer within the JCA software 32 
*   bits at a time.  The buffer is used to store data that will be later 
*   written to the controller hardware
*
*******************************************************************************/

JTI_Error
jca_BufferDataWrite32(
    JCA_Inform *pAdapterInform, /* Communications adapter handle. */
    uint32_t    accessSelect,   /* Select the buffer. */
    uint32_t   *pAccessData,    /* Array of buffer values. */
    uint32_t    countData)      /* Number of buffer values. */
{
    JTI_Error error  = SC_ERR_NONE;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    UNUSED_PARAM(accessSelect);
    UNUSED_PARAM(pAccessData);
    UNUSED_PARAM(countData);

    return error;
}

/* -- END OF FILE -- */
