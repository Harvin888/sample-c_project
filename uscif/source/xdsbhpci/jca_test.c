/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_test.c
*
* DESCRIPTION
*   The logic to bypass hardware for unit testing.
*
* PUBLIC FUNCTIONS
*
*   jca_setupJcaTest()  configure memory to fake register reads and writes.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_test_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

#include "xds_include.h"

#include "jca_public.h"
#include "jca_internal.h"
#include "jca_test.h"

/*******************************************************************************
* NAME: jca_setupJcaTest()
*
* DESCRIPTION
*   Allocates memory to subtitute for memory mapped nano TBC registers.
*
* NOTES
*
*******************************************************************************/

JTI_Error
jca_SetupJcaTest(JCA_Inform *pAdapterInform)
{ 
    JTI_Error error  = SC_ERR_NONE;

    /* 
     *  Allocate memory to substitute for memory mapped registers. 
     *  DWORD is used for compatibility
     */

    static DWORD aCpld[FAKE_SIZE]   = {0};
    static DWORD aPll[FAKE_SIZE]    = {0};
    static DWORD aCable[FAKE_SIZE]  = {0};
    static DWORD aTimer[FAKE_SIZE]  = {0};
    static DWORD aTbc0[FAKE_SIZE]   = {0};
    static DWORD aTbc1[FAKE_SIZE]   = {0};
    static DWORD aTbc2[FAKE_SIZE]   = {0};
    static DWORD aTbc3[FAKE_SIZE]   = {0};

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pAdapterInform)
    {
        error = SC_ERR_CMD_PARM;  ///JTI_ERR_JCA_BAD_HANDLE
    }

    /* Map the memory to the register pointers */

    pAdapterInform->testMode = UNIT_TEST_FLAG;
    pAdapterInform->pCpld    = aCpld;
    pAdapterInform->pPll     = aPll;
    pAdapterInform->pCable   = aCable;
    pAdapterInform->pTimer   = aTimer;
    pAdapterInform->pTbc0    = aTbc0;
    pAdapterInform->pTbc1    = aTbc1;
    pAdapterInform->pTbc2    = aTbc2;
    pAdapterInform->pTbc3    = aTbc3;

    return error;
}

/* -- END OF FILE -- */

