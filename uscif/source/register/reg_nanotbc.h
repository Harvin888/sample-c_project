/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2007, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   reg_nanotbc.h
*
* DESCRIPTION
*   The register definitions for the Nano-TBC.
*
* NOTES
*   These mnemonics are derived from the 'Nano-TBC Implementation Version 2.1'
*   document by Lee Larson and Yihan Jiang that is dated 4th September 2007.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef reg_nanotbc_h
#define reg_nanotbc_h /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* These are the Nano-TBC Register and Buffer Mnemonics
*******************************************************************************/

#define TBC_REG_COMMAND0     (SC_BLCK_NANO0 + (0x00<<2)) /* Block 0, Write Only */
#define TBC_REG_COMMAND1     (SC_BLCK_NANO0 + (0x01<<2)) /* Block 0, Write Only */
#define TBC_REG_MODIFIER0    (SC_BLCK_NANO0 + (0x02<<2)) /* Block 0, Write Only */
#define TBC_REG_MODIFIER1    (SC_BLCK_NANO0 + (0x03<<2)) /* Block 0, Write Only */

#define TBC_REG_CONFIGURE0   (SC_BLCK_NANO3 + (0x04<<2)) /* Block 3, Write Only */
#define TBC_REG_CONFIGURE1   (SC_BLCK_NANO3 + (0x05<<2)) /* Block 3, Write Only */
#define TBC_REG_INTERRUPT    (SC_BLCK_NANO3 + (0x08<<2)) /* Block 3, Read/Write */

#define TBC_REG_NSEQ_CTRL    (SC_BLCK_NANO3 + (0x09<<2)) /* Block 3, Write Only */
#define TBC_REG_NSEQ_STORE   (SC_BLCK_NANO3 + (0x0A<<2)) /* Block 3, Write Only */
#define TBC_REG_NSEQ_PARM0   (SC_BLCK_NANO3 + (0x0B<<2)) /* Block 3, Write Only */

#define TBC_REG_STORAGE0     (SC_BLCK_NANO3 + (0x0C<<2)) /* Block 3, Read/Write */
#define TBC_REG_STORAGE1     (SC_BLCK_NANO3 + (0x0D<<2)) /* Block 3, Read/Write */
#define TBC_REG_STORAGE2     (SC_BLCK_NANO3 + (0x0E<<2)) /* Block 3, Read/Write */
#define TBC_REG_STORAGE3     (SC_BLCK_NANO3 + (0x0F<<2)) /* Block 3, Read/Write */

#define TBC_REG_SYS_STATUS0  (SC_BLCK_NANO0 + (0x08<<2)) /* Block 0, Read Only  */
#define TBC_REG_SYS_STATUS1  (SC_BLCK_NANO0 + (0x09<<2)) /* Block 0, Read Only  */

#define TBC_REG_PUT_STATUS   (SC_BLCK_NANO0 + (0x0B<<2)) /* Block 0, Read Only  */
#define TBC_REG_GET0_STATUS  (SC_BLCK_NANO0 + (0x0C<<2)) /* Block 0, Read Only  */
#define TBC_REG_GET1_STATUS  (SC_BLCK_NANO0 + (0x0D<<2)) /* Block 0, Read Only  */

#define TBC_REG_GET_VERSION  (SC_BLCK_NANO0 + (0x0E<<2)) /* Block 0, Read Only  */

#define TBC_REG_INST_MAIN0   (SC_BLCK_NANO0 + (0x08<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN0   (SC_BLCK_NANO0 + (0x09<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN1   (SC_BLCK_NANO0 + (0x0A<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN2   (SC_BLCK_NANO0 + (0x0B<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN3   (SC_BLCK_NANO0 + (0x0C<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN4   (SC_BLCK_NANO0 + (0x0D<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN5   (SC_BLCK_NANO0 + (0x0E<<2)) /* Block 0, Write Only */
#define TBC_REG_DATA_MAIN6   (SC_BLCK_NANO0 + (0x0F<<2)) /* Block 0, Write Only */

#define TBC_REG_DATA_PRE     (SC_BLCK_NANO1 + (0x00<<2)) /* Block 1, Write Only */
#define TBC_REG_DATA_POS     (SC_BLCK_NANO1 + (0x01<<2)) /* Block 1, Write Only */
#define TBC_REG_INST_PRE     (SC_BLCK_NANO1 + (0x02<<2)) /* Block 1, Write Only */
#define TBC_REG_INST_POS     (SC_BLCK_NANO1 + (0x03<<2)) /* Block 1, Write Only */

#define TBC_REG_END_DELAY0   (SC_BLCK_NANO0 + (0x04<<2)) /* Block 0, Write Only */
#define TBC_REG_END_DELAY1   (SC_BLCK_NANO0 + (0x05<<2)) /* Block 0, Write Only */
#define TBC_REG_END_DELAY2   (SC_BLCK_NANO0 + (0x06<<2)) /* Block 0, Write Only */
#define TBC_REG_END_DELAY3   (SC_BLCK_NANO0 + (0x07<<2)) /* Block 0, Write Only */

#define TBC_REG_COUNT_REDO0  (SC_BLCK_NANO1 + (0x04<<2)) /* Block 1, Write Only */
#define TBC_REG_COUNT_REDO1  (SC_BLCK_NANO1 + (0x05<<2)) /* Block 1, Write Only */
#define TBC_REG_SHORT_POLL0  (SC_BLCK_NANO1 + (0x06<<2)) /* Block 1, Write Only */
#define TBC_REG_SHORT_POLL1  (SC_BLCK_NANO1 + (0x07<<2)) /* Block 1, Write Only */

#define TBC_REG_PUT_DATA_A0  (SC_BLCK_NANO1 + (0x08<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_A1  (SC_BLCK_NANO1 + (0x09<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_A2  (SC_BLCK_NANO1 + (0x0A<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_A3  (SC_BLCK_NANO1 + (0x0B<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_B0  (SC_BLCK_NANO1 + (0x0C<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_B1  (SC_BLCK_NANO1 + (0x0D<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_B2  (SC_BLCK_NANO1 + (0x0E<<2)) /* Block 1, Write Only */
#define TBC_REG_PUT_DATA_B3  (SC_BLCK_NANO1 + (0x0F<<2)) /* Block 1, Write Only */

#define TBC_REG_EXPECT_A0    (SC_BLCK_NANO2 + (0x00<<2)) /* Block 2, Write Only */
#define TBC_REG_EXPECT_A1    (SC_BLCK_NANO2 + (0x01<<2)) /* Block 2, Write Only */
#define TBC_REG_EXPECT_A2    (SC_BLCK_NANO2 + (0x02<<2)) /* Block 2, Write Only */
#define TBC_REG_EXPECT_A3    (SC_BLCK_NANO2 + (0x03<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_A0    (SC_BLCK_NANO2 + (0x04<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_A1    (SC_BLCK_NANO2 + (0x05<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_A2    (SC_BLCK_NANO2 + (0x06<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_A3    (SC_BLCK_NANO2 + (0x07<<2)) /* Block 2, Write Only */

#define TBC_REG_EXPECT_B0    (SC_BLCK_NANO2 + (0x08<<2)) /* Block 2, Write Only */
#define TBC_REG_EXPECT_B1    (SC_BLCK_NANO2 + (0x09<<2)) /* Block 2, Write Only */
#define TBC_REG_EXPECT_B2    (SC_BLCK_NANO2 + (0x0A<<2)) /* Block 2, Write Only */
#define TBC_REG_EXPECT_B3    (SC_BLCK_NANO2 + (0x0B<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_B0    (SC_BLCK_NANO2 + (0x0C<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_B1    (SC_BLCK_NANO2 + (0x0D<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_B2    (SC_BLCK_NANO2 + (0x0E<<2)) /* Block 2, Write Only */
#define TBC_REG_SELECT_B3    (SC_BLCK_NANO2 + (0x0F<<2)) /* Block 2, Write Only */

#define TBC_REG_GET_DATA_A0  (SC_BLCK_NANO2 + (0x08<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_A1  (SC_BLCK_NANO2 + (0x09<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_A2  (SC_BLCK_NANO2 + (0x0A<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_A3  (SC_BLCK_NANO2 + (0x0B<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_B0  (SC_BLCK_NANO2 + (0x0C<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_B1  (SC_BLCK_NANO2 + (0x0D<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_B2  (SC_BLCK_NANO2 + (0x0E<<2)) /* Block 2, Read Only  */
#define TBC_REG_GET_DATA_B3  (SC_BLCK_NANO2 + (0x0F<<2)) /* Block 2, Read Only  */

#define TBC_REG_PUT_BUFFER   (SC_BLCK_NANO3 + (0x00<<2)) /* Block 3, Write Only */
#define TBC_REG_GET_BUFFER0  (SC_BLCK_NANO3 + (0x00<<2)) /* Block 3, Read Only  */
#define TBC_REG_GET_BUFFER1  (SC_BLCK_NANO3 + (0x04<<2)) /* Block 3, Read Only  */

#define TBC_REG_OBSERVE0     (SC_BLCK_NANO1 + (0x00<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE1     (SC_BLCK_NANO1 + (0x01<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE2     (SC_BLCK_NANO1 + (0x02<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE3     (SC_BLCK_NANO1 + (0x03<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE4     (SC_BLCK_NANO1 + (0x04<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE5     (SC_BLCK_NANO1 + (0x05<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE6     (SC_BLCK_NANO1 + (0x06<<2)) /* Block 1, Read Only  */
#define TBC_REG_OBSERVE7     (SC_BLCK_NANO1 + (0x07<<2)) /* Block 1, Read Only  */

/*******************************************************************************
* These are the Command #0 and Command #1 Mnemonics for TBC_REG_COMMAND0/1.
*
*         15-13     12-10      9-8     7-6     5-4    3     2    1    0
*   +---+---+---+---+---+---+---+---+---+---+---+---+----+----+-----+----+ 
*   |    End    |   Scan    |  End  |  Via  |  Put  |Get |Poll|Match|Bank|
*   |   State   |   Type    | Delay | State |  Type |Type|Mode|     |Slct|
*   +---+---+---+---+---+---+---+---+---+---+---+---+----+----+-----+----+ 
*
*******************************************************************************/

/* These are the Command Field Mask Mnenonics. */
#define CMND_XTRA_MASK          0x0003
#define CMND_TYPE_MASK          0x003C
#define CMND_VIA_MASK           0x00C0
#define CMND_USE_MASK           0x1C00
#define CMND_GOTO_MASK          0xE000

/* These are the Command Extra Mnemonics. */
#define CMND_USE_SETA           0x0000 /* Extra Zero, Use Register Set #A */
#define CMND_USE_SETB           0x0001 /* Extra Zero, Use Register Set #B */

#define CMND_TST_DIFFER         0x0000 /* Extra Zero, Use Mismatch Test */
#define CMND_TST_EQUAL          0x0002 /* Extra Zero, Use Match Test    */

#define CMND_USE_ZERO           0x0000 /* Extra Zero, Use Constant Zero */
#define CMND_USE_ONES           0x0002 /* Extra Zero, Use Constant Zeros */

/* These are the Command Type Mnemonics For Extra Commands. */
#define CMND_DO_GOTO            0x0004 /* Type Zero, Do Go-To  */
#define CMND_DO_BYPASS          0x000C /* Type Zero, Do Bypass */
#define CMND_CIRCLE_2REG        0x0000 /* Type Zero, Circulate to Get Reg' */
#define CMND_CIRCLE_2BUF        0x0008 /* Type Zero, Circulate to Get Buf' */

/* These are the Command Type Mnemonics For Data Scan Commands. */
#define CMND_SCAN_ONE2REG       0x0000 /* Type Zero, Put Ones to Get Reg' */
#define CMND_SCAN_ONE2BUF       0x0008 /* Type Zero, Put Ones to Get Buf' */
#define CMND_SCAN_REG2REG       0x0010 /* Type Zero, Put Reg' to Get Reg' */
#define CMND_SCAN_REG2BUF       0x0018 /* Type Zero, Put Reg' to Get Buf' */
#define CMND_SCAN_BUF2REG       0x0020 /* Type Zero, Put Buf' to Get Reg' */
#define CMND_SCAN_BUF2BUF       0x0028 /* Type Zero, Put Buf' to Get Buf' */
#define CMND_SCAN_MIX2REG       0x0030 /* Type Zero, Put Mix' to Get Reg' */
#define CMND_SCAN_MIX2BUF       0x0038 /* Type Zero, Put Mix' to Get Buf' */

/* These are the Command Type Mnemonics For Quick Poll Commands. */
#define CMND_QUIK_ONE2REG       0x0002 /* Type Zero, Put Ones to Get Reg' */
#define CMND_QUIK_ONE2BUF       0x000A /* Type Zero, Put Ones to Get Buf' */
#define CMND_QUIK_REG2REG       0x0012 /* Type Zero, Put Reg' to Get Reg' */
#define CMND_QUIK_REG2BUF       0x001A /* Type Zero, Put Reg' to Get Buf' */
#define CMND_QUIK_BUF2REG       0x0022 /* Type Zero, Put Buf' to Get Reg' */
#define CMND_QUIK_BUF2BUF       0x002A /* Type Zero, Put Buf' to Get Buf' */
#define CMND_QUIK_MIX2REG       0x0032 /* Type Zero, Put Mix' to Get Reg' */
#define CMND_QUIK_MIX2BUF       0x003A /* Type Zero, Put Mix' to Get Buf' */

/* These are the Command Type Mnemonics For Flexible Poll Commands. */
#define CMND_FLEX_ONE2REG       0x0004 /* Type Zero, Put Ones to Get Reg' */
#define CMND_FLEX_ONE2BUF       0x000C /* Type Zero, Put Ones to Get Buf' */
#define CMND_FLEX_REG2REG       0x0014 /* Type Zero, Put Reg' to Get Reg' */
#define CMND_FLEX_REG2BUF       0x001C /* Type Zero, Put Reg' to Get Buf' */
#define CMND_FLEX_BUF2REG       0x0024 /* Type Zero, Put Buf' to Get Reg' */
#define CMND_FLEX_BUF2BUF       0x002C /* Type Zero, Put Buf' to Get Buf' */
#define CMND_FLEX_MIX2REG       0x0034 /* Type Zero, Put Mix' to Get Reg' */
#define CMND_FLEX_MIX2BUF       0x003C /* Type Zero, Put Mix' to Get Buf' */

/* These are the Command Via State Mnemonics. */
#define CMND_VIA_IDLE           0x0040 /* Via Idle Zero, go via the idle state   */
#define CMND_VIA_CAPT           0x0080 /* Via Capture Zero, go via capture state */

/* These are the End State Delay Count Select Choice Mnemonics. */
#define CMND_USE_DELAY0         0x0000 /* State Choice Zero, Use i-shift state */
#define CMND_USE_DELAY1         0x0100 /* State Choice Zero, Use i-shift state */
#define CMND_USE_DELAY2         0x0200 /* State Choice Zero, Use i-shift state */
#define CMND_USE_DELAY3         0x0300 /* State Choice Zero, Use i-shift state */

/* These are the Command State Choice Mnemonics. */
#define CMND_USE_INST0          0x0000 /* State Choice Zero, Use i-shift state */
#define CMND_USE_DATA0          0x0400 /* State Choice Zero, Use d-shift state */
#define CMND_USE_DATA1          0x0800 /* State Choice Zero, Use d-shift state */
#define CMND_USE_DATA2          0x0C00 /* State Choice Zero, Use d-shift state */
#define CMND_USE_DATA3          0x1000 /* State Choice Zero, Use d-shift state */
#define CMND_USE_DATA4          0x1400 /* State Choice Zero, Use d-shift state */
#define CMND_USE_DATA5          0x1800 /* State Choice Zero, Use d-shift state */
#define CMND_USE_DATA6          0x1C00 /* State Choice Zero, Use d-shift state */

/* These are the Command End State Mnemonics. */
#define CMND_GOTO_RESET         0x0000 /* Go to test-logic-reset    */
#define CMND_GOTO_IDLE          0x2000 /* Go to run-test/idle       */
#define CMND_GOTO_IPAUSE        0x4000 /* Go to i-pause             */
#define CMND_GOTO_DPAUSE        0x6000 /* Go to d-pause             */
#define CMND_GOTO_CAPT_IPAUSE   0x8000 /* Go via capture to i-pause */
#define CMND_GOTO_CAPT_DPAUSE   0xA000 /* Go via capture to d-pause */
#define CMND_GOTO_IDLE_IPAUSE   0xC000 /* Go via idle to i-pause    */
#define CMND_GOTO_IDLE_DPAUSE   0xE000 /* Go via idle to d-pause    */

/*******************************************************************************
* These are the Configure #0 Mnemonics for TBC_REG_CONFIGURE0.
*******************************************************************************/

#define CFG0_DEV_RESET        0x0000 /* Device Reset  */
#define CFG0_DEV_ENABLE       0x0001 /* Device Enable */

#define CFG0_CMD_RESET        0x0000 /* Command Reset  */
#define CFG0_CMD_ENABLE       0x0002 /* Command Enable */

#define CFG0_BFR_RESET        0x0000 /* Buffer Reset  */
#define CFG0_BFR_ENABLE       0x0004 /* Buffer Enable */

#define CFG0_ENABLE_LOCKED    0x0000 /* Enable field locked */
#define CFG0_ENABLE_ACCESS    0x0008 /* Enable field access */

#define CFG0_LINK_EXPECT_MASK     0x00F0           /* Link Delay Expect Mask  */
#define CFG0_LINK_EXPECT_VALU(n) (0x00F0&((n)<<4)) /* Link Delay Expect Value */

#define CFG0_LINK_MANUAL      0x0000 /* Link Delay Manual    */
#define CFG0_LINK_AUTO        0x0100 /* Link Delay Automatic */

#define CFG0_LINK_LOCKED      0x0000 /* Link Delay field locked */
#define CFG0_LINK_ACCESS      0x0200 /* Link Delay field access */

#define CFG0_SEQ_INT_DISABLE  0x0000 /* Sequencer Interrupt Disable */
#define CFG0_SEQ_INT_ENABLE   0x0400 /* Sequencer Interrupt Enable  */

#define CFG0_GET0_INT_DISABLE 0x0000 /* Buffer Interrupt Disable */
#define CFG0_GET0_INT_ENABLE  0x0800 /* Buffer Interrupt Enable  */

#define CFG0_GET1_INT_DISABLE 0x0000 /* Buffer Interrupt Disable */
#define CFG0_GET1_INT_ENABLE  0x1000 /* Buffer Interrupt Enable  */

#define CFG0_PUT_INT_DISABLE  0x0000 /* Buffer Interrupt Disable */
#define CFG0_PUT_INT_ENABLE   0x2000 /* Buffer Interrupt Enable  */

#define CFG0_CMD_INT_DISABLE  0x0000 /* Command Interrupt Disable */
#define CFG0_CMD_INT_ENABLE   0x4000 /* Command Interrupt Enable  */

#define CFG0_INT_LOCKED       0x0000 /* Interrupt field locked */
#define CFG0_INT_ACCESS       0x8000 /* Interrupt field access */

/*******************************************************************************
* These are the Configure #1 Mnemonics for TBC_REG_CONFIGURE1.
*******************************************************************************/

#define CFG1_TDO_ZERO         0x0000 /* TDO Pin Value Is 0 */
#define CFG1_TDO_ONE          0x0001 /* TDO Pin Value Is 1 */

#define CFG1_TMS_ZERO         0x0000 /* TMS Pin Value Is 0 */
#define CFG1_TMS_ONE          0x0002 /* TMS Pin Value Is 1 */

#define CFG1_FIXED_TMS        0x0000 /* Fixed value on the TMS Bit I/O. */
#define CFG1_PULSE_TMS        0x0004 /* Pulse value on the TMS Bit I/O. */

#define CFG1_USE_JTAG         0x0000 /* Select JTAG Signals  */
#define CFG1_USE_BITS         0x0008 /* Select Register Bits */

#define CFG1_BITIO_LOCKED     0x0000 /* Bit I/O field locked */
#define CFG1_BITIO_ACCESS     0x0010 /* Bit I/O field access */

#define CFG1_CONNECT_TDO      0x0000 /* Connect Signal or Bit on TDO Pin */
#define CFG1_FREEZE_TDO       0x0020 /* Freeze Value on TDO Pin          */

#define CFG1_CONNECT_TMS      0x0000 /* Connect Signal or Bit on TMS Pin */
#define CFG1_FREEZE_TMS       0x0040 /* Freeze Value on TMS Pin          */

#define CFG1_CONNECT_TRST     0x0000 /* Connect Signal or Bit on TRST Pin */
#define CFG1_FREEZE_TRST      0x0080 /* Freeze Value on TRST Pin          */

#define CFG1_FREEZE_LOCKED    0x0000 /* Freeze field locked */
#define CFG1_FREEZE_ACCESS    0x0100 /* Freeze field access */

#define CFG1_STREAM_DISABLE   0x0000 /* Streaming mode disabled */
#define CFG1_STREAM_ENABLE    0x0200 /* Streaming mode enabled  */

#define CFG1_STREAM_LOCKED    0x0000 /* Stream field locked */
#define CFG1_STREAM_ACCESS    0x0400 /* Stream field access */

#define CFG1_SAMPLE_NOT       0x0000 /* Don't Sample JTAG State */
#define CFG1_SAMPLE_YES       0x0800 /* Do Sample JTAG State    */

#define CFG1_TRST_ZERO        0x0000 /* TRST Pin Value Is 0 */
#define CFG1_TRST_ONE         0x1000 /* TRST Pin Value Is 1 */

#define CFG1_TRST_LOCKED      0x0000 /* TRST Pin field locked */
#define CFG1_TRST_ACCESS      0x2000 /* TRST Pin field access */

#define CFG1_DATA_NORMAL      0x0000 /* Test Data Normal    */
#define CFG1_DATA_LOOPBACK    0x4000 /* Test Data Loop-Back */

#define CFG1_TEST_LOCKED      0x0000 /* Test field locked */
#define CFG1_TEST_ACCESS      0x8000 /* Test field access */

/*******************************************************************************
* These are the Modifier Mnemonics for TBC_REG_MODIFIER.
*******************************************************************************/

#define MOD_CMND0_EVERY_PASS  0x0000 /* Apply Command0 Every Pass       */
#define MOD_CMND0_JUST_ONCE   0x0001 /* Inhibit Command0 After One Pass */

#define MOD_CMND0_APPLY_PRE   0x0000 /* Apply Command0 Pre-amble   */
#define MOD_CMND0_INHIBIT_PRE 0x0002 /* Inhibit Command0 Pre-amble */

#define MOD_CMND0_APPLY_PST   0x0000 /* Apply Command0 Post-amble   */
#define MOD_CMND0_INHIBIT_POS 0x0004 /* Inhibit Command0 Post-amble */

#define MOD_CMND1_EVERY_PASS  0x0000 /* Apply Command1 Every Pass       */
#define MOD_CMND1_JUST_ONCE   0x0100 /* Inhibit Command1 After One Pass */

#define MOD_CMND1_APPLY_PRE   0x0000 /* Apply Command1 Pre-amble   */
#define MOD_CMND1_INHIBIT_PRE 0x0200 /* Inhibit Command1 Pre-amble */

#define MOD_CMND1_APPLY_PST   0x0000 /* Apply Command1 Post-amble   */
#define MOD_CMND1_INHIBIT_POS 0x0400 /* Inhibit Command1 Post-amble */

#define MOD_CMND_APPLY_REDO   0x0000 /* Apply the Command Repeat Count   */
#define MOD_CMND_INHIBIT_REDO 0x8000 /* Inhibit the Command Repeat Count */

/*******************************************************************************
* These are the System Status #0 Mnemonics for TBC_REG_SYS_STATUS0.
*******************************************************************************/

#define STS0_CMD_DEAD         0x0000 /* Command Is Dead  */
#define STS0_CMD_ALIVE        0x0001 /* Command Is Alive */

#define STS0_STRM_PAUSE       0x0000 /* Streaming Is Pause */
#define STS0_STRM_ALIVE       0x0002 /* Streaming Is Alive */

#define STS0_UNUSED_20        0x0000 /* Unused.      */
#define STS0_UNUSED_21        0x0004 /* Unused.      */

#define STS0_UNUSED_30        0x0000 /* Unused.      */
#define STS0_UNUSED_31        0x0008 /* Unused.      */

#define STS0_POLL_NOSUCCESS   0x0000 /* Poll Command In Progress */
#define STS0_POLL_SUCCESS     0x0010 /* Poll Command Succeeded   */

#define STS0_POLL_NOTIMEOUT   0x0000 /* Poll Command In Progress */
#define STS0_POLL_TIMEOUT     0x0020 /* Poll Command Timeout     */

#define STS0_SEQ_FAIL_NOT     0x0000 /* Sequencer Fail Flag Clear */
#define STS0_SEQ_FAIL_YES     0x0040 /* Sequencer Fail Flag Set   */

#define STS0_SEQ_BUSY_NOT     0x0000 /* Sequencer Busy Flag Clear */
#define STS0_SEQ_BUSY_YES     0x0080 /* Sequencer Busy Flag Set   */

#define STS0_GET0_IS_IN_USE   0x0000 /* Get Buffer0 Not Almost Full */
#define STS0_GET0_ALMOST_FULL 0x0100 /* Get Buffer0 Almost Full     */

#define STS0_GET1_IS_IN_USE   0x0000 /* Get Buffer1 Not Almost Full */
#define STS0_GET1_ALMOST_FULL 0x0200 /* Get Buffer1 Almost Full     */

#define STS0_PUT_IS_IN_USE    0x0000 /* Put Buffer Not Almost Empty */
#define STS0_PUT_ALMOST_MPTY  0x0400 /* Put Buffer Almost Empty     */

#define STS0_JTAG_TCLK_DONE   0x0000 /* The JTAG TCLK delay is done. */
#define STS0_JTAG_TCLK_BUSY   0x0800 /* The JTAG TCLK delay is busy. */

/*******************************************************************************
* These are the System Status #0 Mnemonics for TBC_REG_SYS_STATUS0.
*******************************************************************************/

#define NANO_JTAG_STATE_RESET    0x0000 /* Reset */
#define NANO_JTAG_STATE_DSELECT  0x0001
#define NANO_JTAG_STATE_DCAPTURE 0x0002
#define NANO_JTAG_STATE_DSHIFT   0x0003 /* Shift-DR */
#define NANO_JTAG_STATE_DEXIT1   0x0004
#define NANO_JTAG_STATE_DPAUSE   0x0005 /* Pause-DR */
#define NANO_JTAG_STATE_DEXIT2   0x0006
#define NANO_JTAG_STATE_DUPDATE  0x0007
#define NANO_JTAG_STATE_IDLE     0x0008 /* Idle */
#define NANO_JTAG_STATE_ISELECT  0x0009
#define NANO_JTAG_STATE_ICAPTURE 0x000A
#define NANO_JTAG_STATE_ISHIFT   0x000B /* Shift-IR */
#define NANO_JTAG_STATE_IEXIT1   0x000C
#define NANO_JTAG_STATE_IPAUSE   0x000D /* Pause-IR */
#define NANO_JTAG_STATE_IEXIT2   0x000E
#define NANO_JTAG_STATE_IUPDATE  0x000F

#define STS0_JTAG_STATE_MASK                 0xF000     /* The JTAG state Mask. */
#define STS0_JTAG_STATE_VALU(r)   (((r)>>12)&0xF)       /* The JTAG state value. */
#define STS0_JTAG_STATE_EQU(r,n) ((((r)>>12)&0xF)==(n)) /* The JTAG state equals n. */

#define STS0_JTAG_STATE_RESET(r)  (STS1_JTAG_STATE_EQU((r),NANO_JTAG_STATE_RESET)
#define STS0_JTAG_STATE_IDLE(r)   (STS1_JTAG_STATE_EQU((r),NANO_JTAG_STATE_IDLE)
#define STS0_JTAG_STATE_DSHIFT(r) (STS1_JTAG_STATE_EQU((r),NANO_JTAG_STATE_DSHIFT)
#define STS0_JTAG_STATE_ISHIFT(r) (STS1_JTAG_STATE_EQU((r),NANO_JTAG_STATE_ISHIFT)
#define STS0_JTAG_STATE_DPAUSE(r) (STS1_JTAG_STATE_EQU((r),NANO_JTAG_STATE_DPAUSE)
#define STS0_JTAG_STATE_IPAUSE(r) (STS1_JTAG_STATE_EQU((r),NANO_JTAG_STATE_IPAUSE)

#define STS0_JTAG_STATE_STABLE(r) (STS1_JTAG_STATE_RESET(r)  || \
                                   STS1_JTAG_STATE_IDLE(r)   || \
                                   STS1_JTAG_STATE_DSHIFT(r) || \
                                   STS1_JTAG_STATE_ISHIFT(r) || \
                                   STS1_JTAG_STATE_DPAUSE(r) || \
                                   STS1_JTAG_STATE_IPAUSE(r))

/*******************************************************************************
* These are the System Status #1 Mnemonics for TBC_REG_SYS_STATUS1.
*******************************************************************************/

#define STS1_GETREG_ADRS_MASK     0x0003        /* Get Register Address Mask */
#define STS1_GETREG_ADRS_VALU(r) (0x3&((r)>>0)) /* Get Register Address Value */

#define STS1_CMND_SELECT0         0x0000 /* Command Select #0 */
#define STS1_CMND_SELECT1         0x0004 /* Command Select #1 */

#define STS1_BANK_SELECT0         0x0000 /* Bank Select #0 */
#define STS1_BANK_SELECT1         0x0008 /* Bank Select #1 */

#define STS1_STALL_SCAN_NOT       0x0000 /* Stall Scan Not */
#define STS1_STALL_SCAN_YES       0x0010 /* Stall Scan Yes */

#define STS1_TDI_PIN_MASK         0x0020        /* TDI Pin Mask  */
#define STS1_TDI_PIN_VALU(r)     (0x1&((r)>>5)) /* TDI Pin Value */

#define STS1_TRST_PIN_MASK        0x0040        /* TRST Pin Mask  */
#define STS1_TRST_PIN_VALU(r)    (0x1&((r)>>6)) /* TRST Pin Value */

#define STS1_SEQ_DONE_NOT         0x0000 /* Sequencer Is Not Done */
#define STS1_SEQ_DONE_YES         0x0080 /* Sequencer Is Done     */

#define STS1_GET0_NOT_INT         0x0000 /* Get Buffer0 Not Almost Full */
#define STS1_GET0_ALMOST_FULL_INT 0x0100 /* Get Buffer0 Almost Full     */

#define STS1_GET1_NOT_INT         0x0000 /* Get Buffer1 Not Almost Full */
#define STS1_GET1_ALMOST_FULL_INT 0x0200 /* Get Buffer1 Almost Full     */

#define STS1_PUT_NOT_INT          0x0000 /* Put Buffer Not Almost Empty */
#define STS1_PUT_ALMOST_MPTY_INT  0x0400 /* Put Buffer Almost Empty     */

#define STS1_CMD_NOT_INT          0x0000 /* Command Not Alive */
#define STS1_CMD_ALIVE_INT        0x0800 /* Command Alive     */

#define STS1_LINK_ACTUAL_MASK     0x7000         /* Link Delay Actual Mask  */
#define STS1_LINK_ACTUAL_VALU(r) (0x7&((r)>>12)) /* Link Delay Actual Value */

#define STS1_LINK_SUCCESS         0x0000 /* Link Delay Calibration Success */
#define STS1_LINK_FAILURE         0x8000 /* Link Delay Calibration Failure */

/*******************************************************************************
* These are the Buffer/Register Capacity mnemonics.
*******************************************************************************/

#define NANO_REGISTER_SIZE        4
#define NANO_BUFFER_SIZE          256

/*******************************************************************************
* These are the Buffer Count Mnemonics for TBC_REG_PUT_STATUS.
*******************************************************************************/

#define STS_PUT_BFR_MASK        0x01FF /* Put Buffer Mask */
#define STS_PUT_BFR_STUFF(r)   (0x01FF&((r)>>0))       /* Put Buffer has x words           */
#define STS_PUT_BFR_EQU(r,n)  ((0x01FF&((r)>>0))==(n)) /* Put Buffer has exactly n words   */
#define STS_PUT_BFR_LSS(r,n)  ((0x01FF&((r)>>0))< (n)) /* Put Buffer has less than n words */

#define STS_PUT_BFFR_MAX_WORD  (0x0100 - 0x0020)
#define STS_PUT_BFFR_ONE_WORD   0x0001
#define STS_PUT_BFFR_EMPTY      0x0000

/*******************************************************************************
* These are the Buffer Count Mnemonics for
* TBC_REG_GET0_STATUS and TBC_REG_GET1_STATUS.
*******************************************************************************/

#define STS_GET_BFR_MASK        0x01FF /* Get Buffer Mask */
#define STS_GET_BFR_STUFF(r)   (0x01FF&((r)>>0))       /* Get Buffer has x words         */
#define STS_GET_BFR_EQU(r,n)  ((0x01FF&((r)>>0))==(n)) /* Get Buffer has exactly n words */
#define STS_GET_BFR_GEQ(r,n)  ((0x01FF&((r)>>0))>=(n)) /* Get Buffer at least n words    */

#define STS_GET_BFFR_MAX_WORD  (0x0100 - 0x0020)
#define STS_GET_BFFR_ONE_WORD   0x0001
#define STS_GET_BFFR_EMPTY      0x0000

/*******************************************************************************
* These are the Control Register Mnemonics for TBC_REG_NSEQ_CTRL.
*******************************************************************************/

#define NSEQ_CTRL_ADDR_MASK     0x00FF  /* nSeq Control store address mask */
#define NSEQ_CTRL_LOAD_ENABLE   0x0100  /* nSeq is able to load nano code, the nSeq PC is reset to 0. */
#define NSEQ_CTRL_LOAD_DISABLE  0x0000  /* nSeq is unable to load nano code. */
#define NSEQ_CTRL_RUN_ENABLE    0x0200  /* Enable nSeq to Run. */
#define NSEQ_CTRL_RUN_DISABLE   0x0000  /* Disable nSeq to Run, the execution is halted. */
#define NSEQ_CTRL_SEL_PAGE_HI   0x0400  /* Select the upper 64 words of nSeq RAM. */
#define NSEQ_CTRL_SEL_PAGE_LO   0x0000  /* Select the lower 64 words of nSeq RAM. */
#define NSEQ_CTRL_ABORT         0x0800  /* Signal to the nSeq to terminate the command. */
#define NSEQ_CTRL_MODE_ENABLE   0x1000  /* start of scan command is controlled by nSeq */
#define NSEQ_CTRL_MODE_DISABLE  0x0000  /* start of scan command is not controlled by nSeq */
#define NSEQ_CTRL_STEP_ENABLE   0x2000  /* nSeq will execute a single instruction and stop. */
#define NSEQ_CTRL_STEP_DISABLE  0x0000  /* nSeq is unable to step code. */
#define NSEQ_CTRL_OBSV_PC       0x0000  /* Observe PC value. */
#define NSEQ_CTRL_OBSV_A        0x4000  /* Observe A register value. */
#define NSEQ_CTRL_OBSV_B        0x8000  /* Observe B register value. */
#define NSEQ_CTRL_OBSV_Q        0xC000  /* Observe Q register value. */

/*******************************************************************************
* Define the PLL Write Register/PLL Data Shifter address.
*******************************************************************************/

#define SC_REG_PLL_OLD_LO      (SC_BLCK_PLL   + (0x00<<2))
#define SC_REG_PLL_OLD_HI      (SC_BLCK_PLL   + (0x01<<2))
#define SC_REG_PLL_NEW_PLL     (SC_BLCK_PLL   + (0x02<<2))
#define SC_REG_PLL_CPLD_WR     (SC_BLCK_PLL   + (0x03<<2))
#define SC_REG_PLL_CPLD_RD     (SC_BLCK_PLL   + (0x04<<2))

/*******************************************************************************
* Define the Support Logic Control/Status Register address.
*******************************************************************************/

#define SC_REG_SUPPORT_CNTL    (SC_BLCK_CABLE + (0x00<<2))
#define SC_REG_SUPPORT_STAT    (SC_BLCK_CABLE + (0x01<<2))
#define SC_REG_SUPPORT_EMUIO   (SC_BLCK_CABLE + (0x02<<2))

/*******************************************************************************
* Define the Support Logic Test Pattern Register address.
*******************************************************************************/

#define SC_REG_SUPPORT_TEST0   (SC_BLCK_CABLE + (0x08<<2))
#define SC_REG_SUPPORT_TEST1   (SC_BLCK_CABLE + (0x09<<2))
#define SC_REG_SUPPORT_TEST2   (SC_BLCK_CABLE + (0x0A<<2))
#define SC_REG_SUPPORT_TEST3   (SC_BLCK_CABLE + (0x0B<<2))

/*******************************************************************************
* Define the FPGA Program Revision Register address.
*******************************************************************************/

#define SC_REG_SUPPORT_REVISE  (SC_BLCK_CABLE + (0x0C<<2))

/*******************************************************************************
* Define the Timer Register Write addresses.
*******************************************************************************/

#define SC_REG_INTR_ENABLE     (SC_BLCK_TIMER + (0x00<<2))
#define SC_REG_INTR_CLEAR      (SC_BLCK_TIMER + (0x01<<2))
#define SC_REG_TIMER_EARLY     (SC_BLCK_TIMER + (0x03<<2))

#define SC_REG_TIMER_UPDT_LO   (SC_BLCK_TIMER + (0x04<<2))
#define SC_REG_TIMER_UPDT_HI   (SC_BLCK_TIMER + (0x05<<2))

#define SC_REG_TIMER_CNTL_LO   (SC_BLCK_TIMER + (0x06<<2))
#define SC_REG_TIMER_CNTL_HI   (SC_BLCK_TIMER + (0x07<<2))

/*******************************************************************************
* Define the Timer Register Read addresses.
*******************************************************************************/

#define SC_REG_TIMER_WC_LO     (SC_BLCK_TIMER + (0x00<<2))
#define SC_REG_TIMER_WC_HI     (SC_BLCK_TIMER + (0x01<<2))
#define SC_REG_TIMER_BM_LO     (SC_BLCK_TIMER + (0x02<<2))
#define SC_REG_TIMER_BM_HI     (SC_BLCK_TIMER + (0x03<<2))

#define SC_REG_TIMER_FQ_LO     (SC_BLCK_TIMER + (0x04<<2))
#define SC_REG_TIMER_FQ_HI     (SC_BLCK_TIMER + (0x05<<2))

#define SC_REG_SPRT_STATUS     (SC_BLCK_TIMER + (0x06<<2))

/*******************************************************************************
* These are the Support Logic Control Mnemonics for SC_REG_SUPPORT_CNTL.
*******************************************************************************/

#define SC_CNTL_UNUSED_0    0x0001 /* Unused.      */
#define SC_CNTL_UNUSED_1    0x0002 /* Unused.      */
#define SC_CNTL_PREL        0x0004 /* Pod Release. */    
#define SC_CNTL_CLK_DSB     0x0008 /* Pod DCLK Signal Disable */

#define SC_CNTL_OTES        0x0010 /* Outgoing Target Edge Select - 0 rise, 1 fall */
#define SC_CNTL_ITES        0x0020 /* Incoming Target Edge Select - 0 rise, 1 fall */
#define SC_CNTL_PINT_EN     0x0040 /* Pod Error Interrupt Enable  */
#define SC_CNTL_PLL_DSB     0x0080 /* Pod PLL Signal Disable */

#define SC_CNTL_FAST_RATE   0x0100 /* Counter Fast Rate (Test Mode) */
#define SC_CNTL_UNUSED_9    0x0200 /* Unused. */
#define SC_CNTL_UNUSED_A    0x0400 /* Unused. */
#define SC_CNTL_UNUSED_B    0x0800 /* Unused. */

#define SC_CNTL_UNUSED_C    0x1000 /* Unused. */
#define SC_CNTL_UNUSED_D    0x2000 /* Unused. */
#define SC_CNTL_UNUSED_E    0x4000 /* Unused. */
#define SC_CNTL_UNUSED_F    0x8000 /* Unused. */

/*******************************************************************************
* These are the Support Logic Status Mnemonics for SC_REG_SUPPORT_STAT.
*******************************************************************************/

#define SC_STAT_UNUSED_0    0x0001 /* Unused.                 */
#define SC_STAT_UNUSED_1    0x0002 /* Unused.                 */
#define SC_STAT_UNUSED_2    0x0004 /* Unused.                 */
#define SC_STAT_TDIS        0x0008 /* Real Target Disconnect. */

#define SC_STAT_POWER       0x0010 /* Real Target Power Loss.    */
#define SC_STAT_PDIS        0x0020 /* Real Pod Disconnect.       */
#define SC_STAT_LATCH_TDIS  0x0040 /* Latched Target Disconnect. */
#define SC_STAT_LATCH_POWER 0x0080 /* Latched Target Power Loss. */

#define SC_STAT_LATCH_PDIS  0x0100 /* Latched Pod Disconnect. */
#define SC_STAT_PLL         0x0200 /* PLL Programmer Status.  */
#define SC_STAT_UNUSED_A    0x0400 /* Unused. */
#define SC_STAT_UNUSED_B    0x0800 /* Unused. */

#define SC_STAT_POD_REV0    0x1000 /* Pod Revision Status 0. */
#define SC_STAT_POD_REV1    0x2000 /* Pod Revision Status 1. */
#define SC_STAT_UNUSED_E    0x4000 /* Unused. */
#define SC_STAT_UNUSED_F    0x8000 /* Unused. */

/*******************************************************************************
* These are the Support Logic EMU pin Mnemonics for SC_REG_SUPPORT_EMUIO.
*
* .._EMUx_OUT   - The value that may be output on the pin.
* .._EMUx_BACK  - The read back value generated by termination resistors.
* .._EMUx_ENB   - The output enable - a 0 disables the pin.
*                                   - a 1 enables the pin and outputs the EMUx_OUT value.
* .._EMUx_MODE  - The mode control  - a 0 enables HS-RTDX control of the pin.
*                                   - a 1 enables bit-i/o control of the pin.
* .._EMUx_IN    - The value currently input the EMU pin.
* .._EMUx_FIELD - The field access bit for this specific pin.
*
*******************************************************************************/

#define SC_EMUIO_EMU0_OUT   0x0001 /* The EMU0 write-only output data bit.   */
#define SC_EMUIO_EMU0_BACK  0x0001 /* The EMU0 read-only  output read back bit. */
#define SC_EMUIO_EMU0_ENB   0x0002 /* The EMU0 read-write output enable bit. */
#define SC_EMUIO_EMU0_MODE  0x0004 /* The EMU0 read-write bit 1/0 mode bit.  */
#define SC_EMUIO_EMU0_IN    0x0008 /* The EMU0 read-only  input data bit.    */
#define SC_EMUIO_EMU0_FIELD 0x0008 /* The EMU0 write-only field access bit. */

#define SC_EMUIO_EMU1_OUT   0x0010 /* The EMU1 write-only output data bit.   */
#define SC_EMUIO_EMU1_BACK  0x0010 /* The EMU1 read-only  output read back bit. */
#define SC_EMUIO_EMU1_ENB   0x0020 /* The EMU1 read-write output enable bit. */
#define SC_EMUIO_EMU1_MODE  0x0040 /* The EMU1 read-write bit 1/0 mode bit.  */
#define SC_EMUIO_EMU1_IN    0x0080 /* The EMU1 read-only  input data bit.    */
#define SC_EMUIO_EMU1_FIELD 0x0080 /* The EMU1 write-only field access bit. */

#define SC_EMUIO_EMU2_OUT   0x0100 /* The EMU2 write-only output data bit.   */
#define SC_EMUIO_EMU2_BACK  0x0100 /* The EMU2 read-only  output read back bit. */
#define SC_EMUIO_EMU2_ENB   0x0200 /* The EMU2 read-write output enable bit. */
#define SC_EMUIO_EMU2_MODE  0x0400 /* The EMU2 read-write bit 1/0 mode bit.  */
#define SC_EMUIO_EMU2_IN    0x0800 /* The EMU2 read-only  input data bit.    */
#define SC_EMUIO_EMU2_FIELD 0x0800 /* The EMU2 write-only field access bit. */

#define SC_EMUIO_EMU3_OUT   0x1000 /* The EMU3 write-only output data bit.   */
#define SC_EMUIO_EMU3_BACK  0x1000 /* The EMU3 read-only  output read back bit. */
#define SC_EMUIO_EMU3_ENB   0x2000 /* The EMU3 read-write output enable bit. */
#define SC_EMUIO_EMU3_MODE  0x4000 /* The EMU3 read-write bit 1/0 mode bit.  */
#define SC_EMUIO_EMU3_IN    0x8000 /* The EMU3 read-only  input data bit.    */
#define SC_EMUIO_EMU3_FIELD 0x8000 /* The EMU3 write-only field access bit. */

/*******************************************************************************
* Define the Timer Control Register fields.
*******************************************************************************/

#define SC_TIMER_MASK_BM0           0x003F /* Timer BM0 bit field. */
#define SC_TIMER_MASK_BM1           0x3F00 /* Timer BM1 bit field. */
#define SC_TIMER_MASK_FQ            0x00FF /* Timer FQ bit field. */
#define SC_TIMER_MASK_WC            0x0100 /* Timer WC bit field. */

/*******************************************************************************
* Define the Timer Interrupt Status Register Bits.
*******************************************************************************/

#define SC_TIMER_FLAG_FQ_VALUE      0x000F /* Timer FQ wallclock counter value. */
#define SC_TIMER_FLAG_FQ_GATE       0x0200 /* Timer FQ hardware gate is active.   */
#define SC_TIMER_FLAG_FQ_TOOHI      0x0400 /* Timer FQ clock is too high (>50Mhz) */
#define SC_TIMER_FLAG_FQ_ZERO       0x0800 /* Timer FQ wallclock zero flag.  */
#define SC_TIMER_FLAG_FQ_EARLY      0x1000 /* Timer FQ wallclock early flag. */

/*******************************************************************************
* Define the Timer Interrupt Enable Register Bits.
*******************************************************************************/

#define SC_TIMER_ENABLE_FQ_ZERO     0x0800 /* Timer FQ wallclock zero enable.  */
#define SC_TIMER_ENABLE_FQ_EARLY    0x1000 /* Timer FQ wallclock early enable. */

/*******************************************************************************
* Define the Timer Interrupt Clear Register Bits.
*******************************************************************************/

#define SC_TIMER_CLEAR_FQ_ZERO      0x0800 /* Timer FQ wallclock zero enable.  */
#define SC_TIMER_CLEAR_FQ_EARLY     0x1000 /* Timer FQ wallclock early enable. */

/*******************************************************************************
* Define the Benchmark Timer0 Control Register Bits.
*******************************************************************************/

/* Bit 0 */
#define SC_TIMER_BM0_INCREMENT      0x0000 /* Timer increment.  */
#define SC_TIMER_BM0_LOADZERO       0x0001 /* Timer load zeros. */

/* Bit 1 */
#define SC_TIMER_BM0_EMU_FALLING    0x0000 /* Timer operate on falling edge. */
#define SC_TIMER_BM0_EMU_RISING     0x0002 /* Timer operate on rising edge.  */

/* Bit 3 */
#define SC_TIMER_BM0_TEST_MODE_NOT  0x0000 /* Timer test mode disable. */
#define SC_TIMER_BM0_TEST_MODE_YES  0x0008 /* Timer test mode enable.  */

/* Bit 4 */
#define SC_TIMER_BM0_COUNT_DSB      0x0000 /* Timer use disable. */
#define SC_TIMER_BM0_COUNT_ENB      0x0010 /* Timer use enable.  */

/* Bit 5 */
#define SC_TIMER_BM0_SNGL_STEP_NOT  0x0000 /* Timer single step disable. */
#define SC_TIMER_BM0_SNGL_STEP_YES  0x0020 /* Timer single step enable.  */

/*******************************************************************************
* Define the Benchmark Timer1/0 Control Register Bits.
*******************************************************************************/

/* Bit 7 */
#define SC_TIMER_BM0_READ_SELECT    0x0000 /* Timer BM0 Read Select. */
#define SC_TIMER_BM1_READ_SELECT    0x0080 /* Timer BM1 Read Select. */

/*******************************************************************************
* Define the Benchmark Timer1 Control Register Bits.
*******************************************************************************/

/* Bit 8 */
#define SC_TIMER_BM1_INCREMENT      0x0000 /* Timer increment.  */
#define SC_TIMER_BM1_LOADZERO       0x0100 /* Timer load zeros. */

/* Bit 9 */
#define SC_TIMER_BM1_EMU_FALLING    0x0000 /* Timer operate on falling edge. */
#define SC_TIMER_BM1_EMU_RISING     0x0200 /* Timer operate on rising edge.  */

/* Bit B */
#define SC_TIMER_BM1_TEST_MODE_NOT  0x0000 /* Timer test mode disable. */
#define SC_TIMER_BM1_TEST_MODE_YES  0x0800 /* Timer test mode enable.  */

/* Bit C */
#define SC_TIMER_BM1_COUNT_DSB      0x0000 /* Timer use disable. */
#define SC_TIMER_BM1_COUNT_ENB      0x1000 /* Timer use enable.  */

/* Bit D */
#define SC_TIMER_BM1_SNGL_STEP_NOT  0x0000 /* Timer single step disable. */
#define SC_TIMER_BM1_SNGL_STEP_YES  0x2000 /* Timer single step enable.  */

/*******************************************************************************
* Define the Frequency Measurement Control Register Bits.
*******************************************************************************/

/* Bit 1, a minor command bit */
#define SC_TIMER_FQ_LOAD_NOTNOW   0x0000 /* Timer Don't load data.     */
#define SC_TIMER_FQ_LOAD_RITENOW  0x0002 /* Timer Do load immediately. */

/* Bit 2, a sticky bit */
#define SC_TIMER_FQ_COUNT_DSB     0x0000 /* Timer use disable. */
#define SC_TIMER_FQ_COUNT_ENB     0x0004 /* Timer use enable.  */

/* Bit 3, a sticky bit */
#define SC_TIMER_FQ_MODE_FRQNCY   0x0000 /* Timer frequency measurement mode. */
#define SC_TIMER_FQ_MODE_PERIOD   0x0008 /* Timer period measurement mode.    */

/* Bit 4, a sticky bit */
#define SC_TIMER_FQ_MODE_WATCHDOG 0x0000 /* Timer Watchdog mode.    */
#define SC_TIMER_FQ_MODE_MEASURE  0x0010 /* Timer Measurement mode. */

/* Bit 5, a sticky bit */
#define SC_TIMER_FQ_TEST_MODE_NOT 0x0000 /* Timer test mode disable. */
#define SC_TIMER_FQ_TEST_MODE_YES 0x0020 /* Timer test mode enable.  */

/* Bit 6, a sticky bit */
#define SC_TIMER_FQ_GATE_SHRT     0x0000 /* Timer Use short gate (8192 F, 1 T). */
#define SC_TIMER_FQ_GATE_LONG     0x0040 /* Timer Use long gate (128K F, 16 T). */

/* Bit 7, a sticky bit */
#define SC_TIMER_FQ_GATE_SOFT     0x0000 /* Timer Use Software Gate. */
#define SC_TIMER_FQ_GATE_HARD     0x0080 /* Timer Use Hardware Gate. */

/*******************************************************************************
* Define the Wall-Clock Control Register Bits.
*******************************************************************************/

/* Bit 0, a sticky bit */
#define SC_TIMER_WC_TEST_MODE_NOT 0x0000 /* Timer test mode disable. */
#define SC_TIMER_WC_TEST_MODE_YES 0x0001 /* Timer test mode enable.  */

/* Bit 8, a sticky bit */
#define SC_TIMER_WC_COUNTUP       0x0000 /* Timer Increment.      */
#define SC_TIMER_WC_LOADZERO      0x0100 /* Timer Load all-zerso. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#endif /* reg_nanotbc_h */

/* -- END OF FILE -- */
