/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2002-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   reg_base560.h
*
* DESCRIPTION
*   The base register addresses for the XDS560.
*
* AUTHORS
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef reg_base560_h
#define reg_base560_h /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* These are the Block Address Mnemonics.
*******************************************************************************/

#define SC_BLCK_CPLD       0x01400000
#define SC_BLCK_FPGA       0x01600000
#define SC_BLCK_EMIF       0x01800000

/*******************************************************************************
* These are the FPGA Block Address Mnemonics.
*******************************************************************************/

/*
 * These are the standard emulation addresses.
 */
#define SC_BLCK_UNUSED_0  (SC_BLCK_FPGA + 0x00000000) /* Also used by legacy-TBC.    */
#define SC_BLCK_UNUSED_1  (SC_BLCK_FPGA + 0x00010000) /* Also used by TI HS-RTDX.    */
#define SC_BLCK_UNUSED_2  (SC_BLCK_FPGA + 0x00020000) /* Also used by TI 560-Trace.  */
#define SC_BLCK_PLL       (SC_BLCK_FPGA + 0x00030000) /* Also used by TI 560-Trace.  */

#define SC_BLCK_UNUSED_4  (SC_BLCK_FPGA + 0x00040000) /* Also used by SD 560-Remote. */
#define SC_BLCK_CABLE     (SC_BLCK_FPGA + 0x00050000)
#define SC_BLCK_UNUSED_6  (SC_BLCK_FPGA + 0x00060000)
#define SC_BLCK_TIMER     (SC_BLCK_FPGA + 0x00070000)

#define SC_BLCK_NANO0     (SC_BLCK_FPGA + 0x00080000)
#define SC_BLCK_NANO1     (SC_BLCK_FPGA + 0x00090000)
#define SC_BLCK_NANO2     (SC_BLCK_FPGA + 0x000A0000)
#define SC_BLCK_NANO3     (SC_BLCK_FPGA + 0x000B0000)

#define SC_BLCK_UNUSED_C  (SC_BLCK_FPGA + 0x000C0000)
#define SC_BLCK_UNUSED_D  (SC_BLCK_FPGA + 0x000D0000)
#define SC_BLCK_UNUSED_E  (SC_BLCK_FPGA + 0x000E0000)
#define SC_BLCK_UNUSED_F  (SC_BLCK_FPGA + 0x000F0000)

/*
 * These are the legacy-tbc support addresses.
 */
#define SC_BLCK_LEGACY    (SC_BLCK_FPGA + 0x00000000)

/*
 * These are the hs-rtdx support addresses.
 */
#define SC_BLCK_HSRTDX    (SC_BLCK_FPGA + 0x00010000)

/*
 * These are the trace support addresses.
 */
#define SC_BLCK_TRACE0    (SC_BLCK_FPGA + 0x00020000)
#define SC_BLCK_TRACE1    (SC_BLCK_FPGA + 0x00030000)

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#endif /* reg_base560_h */

/* -- END OF FILE -- */

