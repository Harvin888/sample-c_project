/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2003-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   read_sepk_variable.h
*
* DESCRIPTION
*   The functions that read SEPK configure variables.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef read_sepk_variable_h
#define read_sepk_variable_h /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The public definitions.
*******************************************************************************/

#define SEPK_LOGFILE_DEFAULT "sepk_log_file.txt"
#define SEPK_LOGTYPE_VERBOSE "verbose"
#define SEPK_LOGTYPE_BRIEF   "brief"

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern logic_t
READ_SepkCreate(SEPK_Variable **ppSepkVariable);

extern logic_t
READ_SepkDelete(SEPK_Variable **ppSepkVariable);

extern void
READ_SepkInitial(SEPK_Variable *pSepkVariable);

extern SC_ERROR
READ_SepkVariable(CFG_Handle     cfgHandle,
                  SEPK_Variable *pSepkVariable);

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#endif /* read_sepk_variable_h */

/* -- END OF FILE -- */
