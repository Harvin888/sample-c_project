/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   read_sepk_variable.c
*
* DESCRIPTION
*   The functions that read SEPK configure variables.
*
* PUBLIC FUNCTIONS
*
*   READ_SepkCreate() - Create the SEPK configure variable structure.
*   READ_SepkDelete() - Delete the SEPK configure variable structure.
*
*   READ_SepkInitial()  - Initialise the SEPK configure variable structure.
*   READ_SepkVariable() - Read the SEPK configure variables.
*
* AUTHORS
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define read_sepk_variable_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The legacy header file.
 */
#include "xdsfast3.h"

/*
 * The other header files.
 */
#include "xds_memory.h"
#include "xds_simple.h"
#include "xds_variable.h"

#include "cfg_public.h"
#include "cfg_import.h"

#include "read_sepk_variable.h"

/*******************************************************************************
* NAME: READ_SepkCreate()
*
* DESCRIPTION
*   Create the SEPK configure variables structure.
*
* NOTES
*
*******************************************************************************/

logic_t
READ_SepkCreate(SEPK_Variable **ppSepkVariable)
{
    SEPK_Variable *pSepkVariable = NULL;

    if (NULL == ppSepkVariable)
    {
        return TRUE;
    }

    /* Create the structure used to record configure variables.*/
    {
        uint32_t size;
        void    *pntr;

        size = sizeof(SEPK_Variable);
        pntr = (SEPK_Variable*)slow_malloc(size);
        if (NULL == pntr)
        {
            return TRUE;
        }

        pSepkVariable = pntr;
    }

    /* Return the successfully initialised structure pointer. */ 
    *ppSepkVariable = pSepkVariable;

    return FALSE;
}

/*******************************************************************************
* NAME: READ_SepkDelete()
*
* DESCRIPTION
*   Delete the SEPK configure variables structure.
*
* NOTES
*
*******************************************************************************/

logic_t
READ_SepkDelete(SEPK_Variable **ppSepkVariable)
{
    SEPK_Variable *pSepkVariable = NULL;

    if ((NULL == ppSepkVariable) || (NULL == *ppSepkVariable))
    {
        return TRUE;
    }

    pSepkVariable = *ppSepkVariable;

    /* Erase the structure pointer. */
    *ppSepkVariable = NULL;

    /* Delete the structure used to record configure variables.*/
    {
        void    *pntr;
        uint32_t size;

        pntr = pSepkVariable;
        size = sizeof(SEPK_Variable);
        uscif_free(&pntr, &size);
    }

    return FALSE;
}

/*******************************************************************************
* NAME: READ_SepkInitial()
*
* DESCRIPTION
*   Initialise the SEPK configure variable structure.
*
* NOTES
*
*******************************************************************************/

void
READ_SepkInitial(SEPK_Variable *pSepkVariable)
{
   /*
    * This  configure variable is used to suppress
    * dead-clock and timeout errors when TCLKR stalls.
    */
    pSepkVariable->okStall = FALSE;

   /*
    * This configure variable is the
    * name of the sourceless EPK driver.
    */
    pSepkVariable->driverName[0] = '\0';
    pSepkVariable->useDrvr = FALSE;

   /*
    * This configure variable is the value
    * of the sourceless EPK address or IP port.
    */
    pSepkVariable->portAddress = 0;
    pSepkVariable->usePort = FALSE;

   /*
    * This configure variable is the operating
    * mode of the Sourceless EPK driver.
    */
    pSepkVariable->blockMode = FALSE;
    pSepkVariable->useMode = FALSE;

   /*
    * This configure variable is the domain
    * name or IP address of the host computer.
    */
    pSepkVariable->hostName[0] = '\0';
    pSepkVariable->useHost = FALSE;

   /*
    * This configure variable is the
    * value of the remote IP port.
    */
    pSepkVariable->remotePort = 0;
    pSepkVariable->useRemote = FALSE;

   /*
    * This configure variable is the direct connection
    * (no cable) flag of the Sourceless EPK driver.
    */
    pSepkVariable->directConnect = FALSE;

   /*
    * This configure variable is used to enable
    * or disable logging the Sourceless EPK API's.
    */
    pSepkVariable->logFileEnable = FALSE;
    pSepkVariable->useEnable = FALSE;

   /*
    * This configure variable is used to configure
    * the style of logging the Sourceless EPK API's.
    */
    pSepkVariable->logFileVerbose = FALSE;

   /*
    * This configure variable is used to name the
    * log-file for logging the Sourceless EPK API's.
    */
    pSepkVariable->logFileName[0] = '\0';

    return;
}

/*******************************************************************************
* NAME: READ_SepkVariable()
*
* DESCRIPTION
*    Read the SEPK configure variables.
*
* NOTES
*
*******************************************************************************/

SC_ERROR
READ_SepkVariable(CFG_Handle     cfgHandle,
                  SEPK_Variable *pSepkVariable)
{
    SC_ERROR error;

    char *name  = NULL;
    char *value = NULL;

    /* A NULL pointer value means a stupid coding error. */
    if ((NULL == cfgHandle) || (NULL == pSepkVariable))
    {
        return SC_ERR_CMD_PARM;
    }

    {
       /*
        * Check the configuration variable `USCIF.OKSTALL'.
        * If set to 'TRUE' then don't generate dead-clock
        * errors or timeout errors when TCLKR stalls.
        */
        name = "USCIF.OKSTALL";
        error = LS_GetConfigValue(cfgHandle, name, NULL, &value, NULL);
        if (SC_ERR_NONE != error)
        {
            return error;
        }
        error = LS_FromValue2Logic(value, &pSepkVariable->okStall);
        if (SC_ERR_NONE != error)
        {
            return error;
        }
    }

    /* Read the configuration variables for the adapter's identificaion. */
    {
       /*
        * Look for an emulator adapter file name provided by a variable.
        * In this case we don't know if it exists and is readable.
        */
        name = "SEPK.POD_DRVR";
        error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &pSepkVariable->useDrvr);
        if (SC_ERR_NONE != error)
        {
            return error;
        }
        error = LS_FromValue2String(value, pSepkVariable->driverName, LS_MAX_CFG_VARY_LONG + 1);
        if (SC_ERR_NONE != error)
        {
            return error;
        }

        if (pSepkVariable->useDrvr)
        {
           /*
            * Look for an emulator port address provided by a variable.
            * In this case we don't know if it exists and is readable.
            */
            name = "SEPK.POD_PORT";
            error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &pSepkVariable->usePort);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
            error = LS_FromValue2Unsign(value, &pSepkVariable->portAddress);
            if (SC_ERR_NONE != error)
            {
                return error;
            }

            name = "SEPK.POD_BLKMODE";
            error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &pSepkVariable->useMode);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
            error = LS_FromValue2Logic(value, &pSepkVariable->blockMode);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
        }
    }

    /* Read the configuration variables for the remote server's identificaion. */
    {
       /*
        * Look for an remote emulator IP name provided by a variable.
        * In this case we don't know if it exists and is readable.
        */
        name = "SEPK.RMT_HOST";
        error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &pSepkVariable->useHost);
        if (SC_ERR_NONE != error)
        {
            return error;
        }
        error = LS_FromValue2String(value, pSepkVariable->hostName, LS_MAX_CFG_VARY_LONG + 1);
        if (SC_ERR_NONE != error)
        {
            return error;
        }

        if (pSepkVariable->useHost)
        {
           /*
            * Look for an remote emulator IP port provided by a variable.
            * In this case we don't know if it exists and is readable.
            */
            name = "SEPK.RMT_PORT";
            error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &pSepkVariable->useRemote);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
            error = LS_FromValue2Unsign(value, &pSepkVariable->remotePort);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
        }
    }

   /*
    * Read the configuration variables for the direct
    * connection (no cable) flag of the Sourceless EPK driver.
    */
    {
        name = "SEPK.POD_NOCABLE";
        error = LS_GetConfigValue(cfgHandle, name, NULL, &value, NULL);
        if (SC_ERR_NONE != error)
        {
            return error;
        }
        error = LS_FromValue2Logic(value, &pSepkVariable->directConnect);
        if (SC_ERR_NONE != error)
        {
            return error;
        }
    }

    /* Read the configuration variables for the adapter's API logging. */
    {
       /*
        * Check the configuration variable 'SEPK.POD_LOGMODE'.
        * If it exists then use it to enable or disable logging.
        */
        {
            name = "SEPK.POD_LOGMODE";
            error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &pSepkVariable->useEnable);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
            error = LS_FromValue2Logic(value, &pSepkVariable->logFileEnable);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
        }

        if (pSepkVariable->useEnable)
        {
            logic_t found = FALSE;

            char logFileType[LS_MAX_CFG_VARY_SHORT + 1] = "";
           /*
            * Check the configuration variable 'SEPK.POD_LOGTYPE'.
            * If it exists then use it to enable or disable logging.
            */
            name = "SEPK.POD_LOGTYPE";
            error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &found);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
            if (found)
            {
                error = LS_FromValue2String(value, logFileType, LS_MAX_CFG_VARY_SHORT + 1);
                if (SC_ERR_NONE != error)
                {
                    return error;
                }

                if (!strcmp_nocase(SEPK_LOGTYPE_VERBOSE, logFileType))
                {
                    pSepkVariable->logFileVerbose = TRUE;
                }
                else if (!strcmp_nocase(SEPK_LOGTYPE_BRIEF, logFileType))
                {
                    pSepkVariable->logFileVerbose = FALSE;
                }
            }

           /*
            * Check the configuration variable 'SEPK.POD_LOGFILE'.
            * If it exists then use it as the log-file name.
            */
            name = "SEPK.POD_LOGFILE";
            error = LS_GetConfigValue(cfgHandle, name, NULL, &value, &found);
            if (SC_ERR_NONE != error)
            {
                return error;
            }
            if (found)
            {
                error = LS_FromValue2String(value, pSepkVariable->logFileName, LS_MAX_CFG_VARY_LONG + 1);
                if (SC_ERR_NONE != error)
                {
                    return error;
                }
            }
            else
            {
                strcpy(pSepkVariable->logFileName, SEPK_LOGFILE_DEFAULT);
            }
        }
    }

    return SC_ERR_NONE;
}

/* -- END OF FILE -- */

