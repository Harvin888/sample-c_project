/*
 * bhpci_mmap.cpp : BH-PCI memory mapped access library
 *
 * Copyright 2004  EWA Technologies, Inc.
 * All Right Reserved.
 */

#include <windows.h>
#include <setupapi.h>
#include <initguid.h>

/* 
 * Hide warning from the Windows winioctl.h header file:
 * "nonstandard extension used : nameless struct/union"
 */
#pragma warning(disable: 4201)
#include <winioctl.h>
#pragma warning(default: 4201)

#include "bhpci_mmap.h"

// {09443EFE-9423-41e3-9F4B-8FEAA3BCBBE9}
DEFINE_GUID(GUID_PCI560_INTERFACE, 0x9443efe, 0x9423, 0x41e3, 0x9f, 0x4b, 0x8f, 0xea, 0xa3, 0xbc, 0xbb, 0xe9);

/*
 * Map memory io-controls
 */
#define MDPJTAG_IOCTL_BASE  0x0800

#define IOCTL_MDPJTAG_MAP_MEMORY           \
            CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                    MDPJTAG_IOCTL_BASE+22, \
                    METHOD_BUFFERED,       \
                    FILE_ANY_ACCESS)

#define IOCTL_MDPJTAG_UNMAP_MEMORY         \
            CTL_CODE(FILE_DEVICE_UNKNOWN,  \
                    MDPJTAG_IOCTL_BASE+23, \
                    METHOD_BUFFERED,       \
                    FILE_ANY_ACCESS)

HANDLE
BHPCI_OpenDevice(int idx)
{
    HDEVINFO hardwareDeviceInfo;
    HANDLE hnd;
    SP_INTERFACE_DEVICE_DATA deviceInfoData;

    hardwareDeviceInfo = SetupDiGetClassDevs(
            (struct _GUID *)&GUID_PCI560_INTERFACE,
            NULL,                // Define no enumerator (global)
            NULL,                // Define no
            DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

    if (hardwareDeviceInfo == INVALID_HANDLE_VALUE)
    {
        return INVALID_HANDLE_VALUE;
    }

    hnd = INVALID_HANDLE_VALUE;

    deviceInfoData.cbSize = sizeof( SP_INTERFACE_DEVICE_DATA);

    if (SetupDiEnumDeviceInterfaces(
            hardwareDeviceInfo,
            0,                   // We don't care about specific PDOs
            (struct _GUID *)&GUID_PCI560_INTERFACE,
            idx,
            &deviceInfoData)) 
    {
        ULONG requiredLength = 0;
        PSP_INTERFACE_DEVICE_DETAIL_DATA functionClassDeviceData;

        SetupDiGetInterfaceDeviceDetail(
                hardwareDeviceInfo,
                &deviceInfoData,
                NULL,            // probing so no output buffer yet
                0,               // probing so output buffer length of zero
                &requiredLength,
                NULL);           // not interested in the specific dev-node

        functionClassDeviceData =
                (PSP_INTERFACE_DEVICE_DETAIL_DATA)malloc( requiredLength);
        functionClassDeviceData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

        if ( SetupDiGetInterfaceDeviceDetail(
                hardwareDeviceInfo,
                &deviceInfoData,
                functionClassDeviceData,
                requiredLength,
                &requiredLength,
                NULL))
        {
               hnd = CreateFile(
                    functionClassDeviceData->DevicePath,
                    GENERIC_WRITE | GENERIC_READ,
                    FILE_SHARE_READ | FILE_SHARE_WRITE,
                    NULL,
                    OPEN_EXISTING,
                    0, 
                    NULL);
        }

        free( functionClassDeviceData );
    }

    SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
    return hnd;
}

volatile void *
BHPCI_MapMemory(HANDLE hnd, DWORD c6202Addr, DWORD length)
{
    DWORD inbuf[2] = {0};
    volatile void *outbuf[1];
    DWORD nBytes;

    BOOL Success;

    inbuf[0] = c6202Addr;
    inbuf[1] = length;

    Success = DeviceIoControl( hnd,
            IOCTL_MDPJTAG_MAP_MEMORY,
            inbuf, sizeof( inbuf),
            (void *)outbuf, sizeof( outbuf),
            &nBytes, NULL);

    if (Success)
    {
        if ( nBytes >= sizeof( outbuf))
        {
            return outbuf[0];
        }
    }

    return NULL;
}

BOOL
BHPCI_UnmapMemory(HANDLE hnd, volatile void *vaddr)
{
    DWORD nBytes;

    BOOL Success;

    Success = DeviceIoControl( hnd,
                      IOCTL_MDPJTAG_UNMAP_MEMORY,
                      (void *)&vaddr, sizeof( vaddr),
                      NULL, 0,
                      &nBytes, NULL);

    if (Success)
    {
        return TRUE;
    }

    return FALSE;
}
