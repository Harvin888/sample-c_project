/*
 * bhpci_mmap.h : BH-PCI memory mapped access declarations
 *
 * Copyright 2004  EWA Technologies, Inc.
 * All Right Reserved.
 */
#include <windows.h>

#define BHPCI_MMAP_PAGE_SIZE 0x00400000

#ifdef __cplusplus
extern "C" {
#endif

HANDLE
BHPCI_OpenDevice( int idx);

volatile void *
BHPCI_MapMemory( HANDLE hnd, DWORD c6202Addr, DWORD length);

BOOL
BHPCI_UnmapMemory( HANDLE hnd, volatile void *vaddr);

#ifdef __cplusplus
};
#endif

