/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   cfg_import.h
*
* DESCRIPTION
*   Wrapper to access the board-configuration dynamic library.
*
* AUTHOR
*   Edward Fewell
*   efewell@ti.com
*   281.274.3922
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef cfg_import_h
#define cfg_import_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* Include the header only if explicitly loading a dynamic library.
* The double test comprehends source inclusion and explicitly loading.
*******************************************************************************/

#if (defined(USE_DYNAMIC_LIBS) && !defined(XDSBOARD_LIBRARY))

/*******************************************************************************
* The type definitions.
*******************************************************************************/

#if (USE_FILE_SYSTEM)
#include <stdio.h>

typedef SC_ERROR 
(*LS_BoardFileType_t)(FILE *textio_hndl, uint32_t *pType);

typedef SC_ERROR 
(*LS_File2Struct_t)(CFG_Handle cfgHandle, uint32_t option, uint32_t *pType,
                    FILE *textio_hndl, FILE *excess_hndl);
typedef SC_ERROR 
(*LS_Struct2File_t)(CFG_Handle cfgHandle, uint32_t option, uint32_t *pType,
                    FILE *textio_hndl, FILE *excess_hndl);
#endif /* (USE_FILE_SYSTEM) */

typedef SC_ERROR 
(*LS_ConvertFamilyName_t)(CFG_Handle cfgHandle,
                          const uint32_t *pNumber, const char *pString,
                          char *LegacyName, char *ModernName, char *FamilyName);

typedef SC_ERROR 
(*LS_ConvertVaryName_t)(char *Name, logic_t legacy);

typedef SC_ERROR 
(*LS_CreateAlias_t)(CFG_Handle cfgHandle,
                    LS_ALIAS *pAliasData, uint32_t *pAliasId);

typedef SC_ERROR 
(*LS_CreateDevice_t)(CFG_Handle cfgHandle,
                     char *pDeviceName, char *pFamilyName, char *pPartnerName,
                     uint32_t drBits, uint32_t irBits, uint32_t parentSubpath,
                     uint32_t subpathFirst, uint32_t subpathNum,
                     uint32_t address, uint32_t identify,
                     uint32_t *pDeviceId);

typedef SC_ERROR 
(*LS_CreateFamily_t)(CFG_Handle cfgHandle,
                     LS_FAMILY *pFamilyData, uint32_t *pFamilyId);

typedef SC_ERROR 
(*LS_CreateMasquerade_t)(CFG_Handle *pMasquerade, logic_t *pItsaMasquerade,
                         CFG_Handle original, uint32_t option);

typedef SC_ERROR 
(*LS_CreateStruct_t)(CFG_Handle *pCnfgr);

typedef SC_ERROR 
(*LS_CreateSubpath_t)(CFG_Handle cfgHandle, char *pSubpathName,
                      uint32_t address, uint32_t itsaPseudo,
                      uint32_t itsaDefault, uint32_t itsaCustom,
                      uint32_t parentId, uint32_t deviceFirst, uint32_t deviceNum,
                      uint32_t *pSubpathId);

typedef SC_ERROR 
(*LS_CreateVariable_t)(CFG_Handle cfgHandle, char *Name, char *Value,
                       uint32_t *pVariableId);

typedef SC_ERROR 
(*LS_DeleteAlias_t)(CFG_Handle cfgHandle, char *Name);

typedef SC_ERROR 
(*LS_DeleteDevice_t)(CFG_Handle cfgHandle, char *Name);

typedef SC_ERROR 
(*LS_DeleteFamily_t)(CFG_Handle cfgHandle, char *Name);

typedef SC_ERROR 
(*LS_DeleteMarshall_t)(MSH_Handle *pMarsh, uint32_t *pSize);

typedef SC_ERROR 
(*LS_DeleteMasquerade_t)(CFG_Handle *pMasquerade, logic_t *pItsaMasquerade);

typedef SC_ERROR 
(*LS_DeleteStruct_t)(CFG_Handle *pCnfgr);

typedef SC_ERROR 
(*LS_DeleteSubpath_t)(CFG_Handle cfgHandle);

typedef SC_ERROR 
(*LS_DeleteVariable_t)(CFG_Handle cfgHandle, char *Name);

typedef SC_ERROR 
(*LS_FromValue2Frequency_t)(const char *Value, coord_t *Coord);

typedef SC_ERROR 
(*LS_FromValue2Logic_t)(const char *Value, logic_t *Logic);

typedef SC_ERROR 
(*LS_FromValue2Signed_t)(const char *Value, int32_t *Number);

typedef SC_ERROR 
(*LS_FromValue2String_t)(const char *Value, char *String, uint32_t length);

typedef SC_ERROR 
(*LS_FromValue2Unsign_t)(const char *Value, uint32_t *Number);

typedef SC_ERROR 
(*LS_GetAliasByIndex_t)(CFG_Handle cfgHandle,
                        uint32_t AliasId, LS_ALIAS *pAliasData);

typedef SC_ERROR 
(*LS_GetConfigValue_t)(CFG_Handle cfgHandle, const char *Name,
                       uint32_t *pVariableId, char **pValue, logic_t *pFound);

typedef SC_ERROR 
(*LS_GetDeviceByIndex_t)(CFG_Handle cfgHandle, uint32_t DeviceId,
                    char *pDeviceName, char *pFamilyName, char *pPartnerName,
                    uint32_t *pDrBits, uint32_t *pIrBits, uint32_t *pParentId,
                    uint32_t *pSubpathFirst, uint32_t *pSubpathNum,
                    uint32_t *pAddress, uint32_t *pIdentify,
                    uint32_t *pFamilyId);

typedef SC_ERROR 
(*LS_GetErrorStatus_t)(CFG_Handle cfgHandle, uint32_t *pLine, uint32_t *pError);

typedef SC_ERROR 
(*LS_GetFamilyByIndex_t)(CFG_Handle cfgHandle,
                         uint32_t FamilyId, LS_FAMILY *pFamilyData);

typedef SC_ERROR 
(*LS_GetItemNumbers_t)(CFG_Handle cfgHandle,
                       uint32_t *pVariableNum, uint32_t *pPackageNum, uint32_t *pFamilyNum,
                       uint32_t *pAliasNum, uint32_t *pDeviceNum, uint32_t *pSubpathNum);

typedef SC_ERROR 
(*LS_GetSubpathByIndex_t)(CFG_Handle cfgHandle, uint32_t subpathId, char *pSubpathName,
                          uint32_t *pAddress, uint32_t *pItsaPseudo, 
                          uint32_t *pItsaDefault, uint32_t *pItsaCustom,
                          uint32_t *pParentId, uint32_t *pDeviceFirst, uint32_t *pDeviceNum);

typedef SC_ERROR 
(*LS_GetTargetLengths_t)(CFG_Handle cfgHandle,
                         uint32_t DeviceId, uint32_t *pLengthArray, uint32_t lengthMax);

typedef SC_ERROR 
(*LS_GetVariableByIndex_t)(CFG_Handle cfgHandle, uint32_t VariableId,
                           char **pName, char **pValue);

typedef SC_ERROR 
(*LS_IsAliasGood_t)(CFG_Handle cfgHandle,
                     char *Name, uint32_t length, uint32_t *pAliasId);

typedef SC_ERROR 
(*LS_IsDeviceGood_t)(CFG_Handle cfgHandle,
                char *pDeviceName, uint32_t length, uint32_t *pDeviceId);

typedef SC_ERROR 
(*LS_IsFamilyGood_t)(CFG_Handle cfgHandle,
                     char *Name, uint32_t length, uint32_t *pFamilyId);

typedef SC_ERROR 
(*LS_IsSubpathGood_t)(CFG_Handle cfgHandle,
                      char *pSubpathName, uint32_t length, uint32_t *pSubpathId);

typedef SC_ERROR 
(*LS_IsVariableGood_t)(CFG_Handle cfgHandle,
                       char *Name, uint32_t length, logic_t StandAlone, uint32_t *pVariableId);

typedef SC_ERROR 
(*LS_Marshall2Struct_t)(CFG_Handle *pCnfgr, uint32_t option,
                        MSH_Handle *pMarsh, uint32_t *pSize);

typedef SC_ERROR 
(*LS_PutErrorStatus_t)(CFG_Handle cfgHandle, uint32_t *pLine, uint32_t *pError);

typedef SC_ERROR 
(*LS_Struct2Marshall_t)(CFG_Handle *pCnfgr, uint32_t option,
                        MSH_Handle *pMarsh, uint32_t *pSize);

/*******************************************************************************
* The function pointer declarations.
*******************************************************************************/

#if (!defined(cfg_import_c))

#if (USE_FILE_SYSTEM)
extern LS_BoardFileType_t               LS_BoardFileType;
extern LS_File2Struct_t                 LS_File2Struct;
extern LS_Struct2File_t                 LS_Struct2File;
#endif /* (USE_FILE_SYSTEM) */

extern LS_ConvertFamilyName_t           LS_ConvertFamilyName;
extern LS_ConvertVaryName_t             LS_ConvertVaryName;
extern LS_CreateAlias_t                 LS_CreateAlias;
extern LS_CreateDevice_t                LS_CreateDevice;
extern LS_CreateFamily_t                LS_CreateFamily;
extern LS_CreateMasquerade_t            LS_CreateMasquerade;
extern LS_CreateStruct_t                LS_CreateStruct;
extern LS_CreateSubpath_t               LS_CreateSubpath;
extern LS_CreateVariable_t              LS_CreateVariable;
extern LS_DeleteAlias_t                 LS_DeleteAlias;
extern LS_DeleteDevice_t                LS_DeleteDevice;
extern LS_DeleteFamily_t                LS_DeleteFamily;
extern LS_DeleteMarshall_t              LS_DeleteMarshall;
extern LS_DeleteMasquerade_t            LS_DeleteMasquerade;
extern LS_DeleteStruct_t                LS_DeleteStruct;
extern LS_DeleteSubpath_t               LS_DeleteSubpath;
extern LS_DeleteVariable_t              LS_DeleteVariable;
extern LS_FromValue2Frequency_t         LS_FromValue2Frequency;
extern LS_FromValue2Logic_t             LS_FromValue2Logic;
extern LS_FromValue2Signed_t            LS_FromValue2Signed;
extern LS_FromValue2String_t            LS_FromValue2String;
extern LS_FromValue2Unsign_t            LS_FromValue2Unsign;
extern LS_GetAliasByIndex_t             LS_GetAliasByIndex;
extern LS_GetConfigValue_t              LS_GetConfigValue;
extern LS_GetDeviceByIndex_t            LS_GetDeviceByIndex;
extern LS_GetErrorStatus_t              LS_GetErrorStatus;
extern LS_GetFamilyByIndex_t            LS_GetFamilyByIndex;
extern LS_GetItemNumbers_t              LS_GetItemNumbers;
extern LS_GetSubpathByIndex_t           LS_GetSubpathByIndex;
extern LS_GetTargetLengths_t            LS_GetTargetLengths;
extern LS_GetVariableByIndex_t          LS_GetVariableByIndex;
extern LS_IsAliasGood_t                 LS_IsAliasGood;
extern LS_IsDeviceGood_t                LS_IsDeviceGood;
extern LS_IsFamilyGood_t                LS_IsFamilyGood;
extern LS_IsSubpathGood_t               LS_IsSubpathGood;
extern LS_IsVariableGood_t              LS_IsVariableGood;
extern LS_Marshall2Struct_t             LS_Marshall2Struct;
extern LS_PutErrorStatus_t              LS_PutErrorStatus;
extern LS_Struct2Marshall_t             LS_Struct2Marshall;

#endif /* (!defined(cfg_import_c)) */ 

/*******************************************************************************
* The function declarations.
*******************************************************************************/

extern SC_ERROR
CFG_LoadLibrary(void);

extern SC_ERROR
CFG_FreeLibrary(void);

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#endif /* (defined(USE_DYNAMIC_LIBS) && !defined(XDSBOARD_LIBRARY)) */

#ifdef  __cplusplus
};
#endif

#endif /* cfg_import_h */

/* -- END OF FILE -- */
