/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   cfg_import.c            
*
* DESCRIPTION
*   Wrapper to access the board-configuration dynamic library.
*
* PUBLIC FUNCTIONS
*
*   CFG_LoadLibrary() - Load the xdsboard.dll dynamic library.
*   CFG_FreeLibrary() - Free the xdsboard.dll dynamic library.
*
* PRIVATE FUNCTIONS
*
*   GetFunction() - Get a pointer for a named function from a library.
*   GetMissing()  - Get a pointer for a missing or unloaded function.
*   ApiLoad()     - Load the API's function pointers in the interface. 
*   ApiCheck()    - Check the API's function pointers in the interface.
*
* AUTHOR
*   Edward Fewell
*   efewell@ti.com
*   281.274.3922
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define cfg_import_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The legacy header file.
 */
#include "xdsfast3.h"

/*
 * The other header files.
 */
#include "xds_loadlib.h"
#include "xds_file_stuff.h"
#include "xds_variable.h"

#include "cfg_public.h"

#include "cfg_import.h"

/*******************************************************************************
* The private functions.
*******************************************************************************/

static FNC_HANDLE
GetFunction(
    LIB_HANDLE library,      /* The library handle. */
    char      *pIdentifier); /* A string for name of the function. */

static SC_ERROR
GetMissing(
    void); /* There are no arguments. */

static SC_ERROR
ApiLoad(
    LIB_HANDLE library); /* The library handle. */

static SC_ERROR
ApiCheck(
    logic_t *pNotValid);  /* A pointer to the 'not initialised' status flag. */  

/*******************************************************************************
* The private definitions.
*******************************************************************************/

#if (USE_FILE_SYSTEM)
#include <stdio.h>
LS_BoardFileType_t          LS_BoardFileType        = (LS_BoardFileType_t)      GetMissing;
LS_File2Struct_t            LS_File2Struct          = (LS_File2Struct_t)        GetMissing;
LS_Struct2File_t            LS_Struct2File          = (LS_Struct2File_t)        GetMissing;
#endif /* (USE_FILE_SYSTEM) */

LS_ConvertFamilyName_t      LS_ConvertFamilyName    = (LS_ConvertFamilyName_t)  GetMissing;
LS_ConvertVaryName_t        LS_ConvertVaryName      = (LS_ConvertVaryName_t)    GetMissing;
LS_CreateAlias_t            LS_CreateAlias          = (LS_CreateAlias_t)        GetMissing;
LS_CreateDevice_t           LS_CreateDevice         = (LS_CreateDevice_t)       GetMissing;
LS_CreateFamily_t           LS_CreateFamily         = (LS_CreateFamily_t)       GetMissing;
LS_CreateMasquerade_t       LS_CreateMasquerade     = (LS_CreateMasquerade_t)   GetMissing;
LS_CreateStruct_t           LS_CreateStruct         = (LS_CreateStruct_t)       GetMissing;
LS_CreateSubpath_t          LS_CreateSubpath        = (LS_CreateSubpath_t)      GetMissing;
LS_CreateVariable_t         LS_CreateVariable       = (LS_CreateVariable_t)     GetMissing;
LS_DeleteAlias_t            LS_DeleteAlias          = (LS_DeleteAlias_t)        GetMissing;
LS_DeleteDevice_t           LS_DeleteDevice         = (LS_DeleteDevice_t)       GetMissing;
LS_DeleteFamily_t           LS_DeleteFamily         = (LS_DeleteFamily_t)       GetMissing;
LS_DeleteMarshall_t         LS_DeleteMarshall       = (LS_DeleteMarshall_t)     GetMissing;
LS_DeleteMasquerade_t       LS_DeleteMasquerade     = (LS_DeleteMasquerade_t)   GetMissing;
LS_DeleteStruct_t           LS_DeleteStruct         = (LS_DeleteStruct_t)       GetMissing;
LS_DeleteSubpath_t          LS_DeleteSubpath        = (LS_DeleteSubpath_t)      GetMissing;
LS_DeleteVariable_t         LS_DeleteVariable       = (LS_DeleteVariable_t)     GetMissing;
LS_FromValue2Frequency_t    LS_FromValue2Frequency  = (LS_FromValue2Frequency_t)GetMissing;
LS_FromValue2Logic_t        LS_FromValue2Logic      = (LS_FromValue2Logic_t)    GetMissing;
LS_FromValue2Signed_t       LS_FromValue2Signed     = (LS_FromValue2Signed_t)   GetMissing;
LS_FromValue2String_t       LS_FromValue2String     = (LS_FromValue2String_t)   GetMissing;
LS_FromValue2Unsign_t       LS_FromValue2Unsign     = (LS_FromValue2Unsign_t)   GetMissing;
LS_GetAliasByIndex_t        LS_GetAliasByIndex      = (LS_GetAliasByIndex_t)    GetMissing;
LS_GetConfigValue_t         LS_GetConfigValue       = (LS_GetConfigValue_t)     GetMissing;
LS_GetDeviceByIndex_t       LS_GetDeviceByIndex     = (LS_GetDeviceByIndex_t)   GetMissing;
LS_GetErrorStatus_t         LS_GetErrorStatus       = (LS_GetErrorStatus_t)     GetMissing;
LS_GetFamilyByIndex_t       LS_GetFamilyByIndex     = (LS_GetFamilyByIndex_t)   GetMissing;
LS_GetItemNumbers_t         LS_GetItemNumbers       = (LS_GetItemNumbers_t)     GetMissing;
LS_GetSubpathByIndex_t      LS_GetSubpathByIndex    = (LS_GetSubpathByIndex_t)  GetMissing;
LS_GetTargetLengths_t       LS_GetTargetLengths     = (LS_GetTargetLengths_t)   GetMissing;
LS_GetVariableByIndex_t     LS_GetVariableByIndex   = (LS_GetVariableByIndex_t) GetMissing;
LS_IsAliasGood_t            LS_IsAliasGood          = (LS_IsAliasGood_t)        GetMissing;
LS_IsDeviceGood_t           LS_IsDeviceGood         = (LS_IsDeviceGood_t)       GetMissing;
LS_IsFamilyGood_t           LS_IsFamilyGood         = (LS_IsFamilyGood_t)       GetMissing;
LS_IsSubpathGood_t          LS_IsSubpathGood        = (LS_IsSubpathGood_t)      GetMissing;
LS_IsVariableGood_t         LS_IsVariableGood       = (LS_IsVariableGood_t)     GetMissing;
LS_Marshall2Struct_t        LS_Marshall2Struct      = (LS_Marshall2Struct_t)    GetMissing;
LS_PutErrorStatus_t         LS_PutErrorStatus       = (LS_PutErrorStatus_t)     GetMissing;
LS_Struct2Marshall_t        LS_Struct2Marshall      = (LS_Struct2Marshall_t)    GetMissing;

LIB_HANDLE XdsBoardHandle = NULL;
uint32_t   XdsBoardCount  = 0;

/*!*****************************************************************************
 * Function Name: CFG_LoadLibrary()
 *
 *   Description: Load the dynamic library and initialise its function pointers.
 *
 *    Parameters: none
 * 
 *  Return Value: SC_ERR_NONE on success, otherwise an appropriate error number.
 *
 ******************************************************************************/

SC_ERROR
CFG_LoadLibrary(void)
{
    SC_ERROR error = SC_ERR_NONE;
    logic_t  fail  = FALSE;

    if (NULL != XdsBoardHandle)
    {
        /* Already loaded */
        XdsBoardCount++;

        return SC_ERR_NONE;
    }

    XdsBoardHandle = xds_LoadLibrary(LS_USCIF_BOARD_LIB);
    if (NULL == XdsBoardHandle)
    {
        /* Library failed to load. */
        return SC_ERR_LIB_BRD_LOAD;
    }

    XdsBoardCount = 1;

    /* Initialise the function pointers from the library. */
    error = ApiLoad(XdsBoardHandle);
    if (SC_ERR_NONE != error)
    {
        return error;
    }

    /* Check all the function pointers are valid. */
    error = ApiCheck(&fail);
    if (SC_ERR_NONE != error)
    {
        return error;
    }
    if (FALSE != fail)
    {
        /* One or more of the functions were not found. */
        CFG_FreeLibrary();

        return SC_ERR_LIB_BRD_FUNCTION;
    }

    return SC_ERR_NONE;
}

/*!*****************************************************************************
 * Function Name: CFG_FreeLibrary()
 *
 *   Description: Free the dynamic library and erase its function pointers.
 *
 *    Parameters: none
 * 
 *  Return Value: none.
 *
 ******************************************************************************/

SC_ERROR
CFG_FreeLibrary(void)
{    
    if (NULL != XdsBoardHandle)
    {
        XdsBoardCount--;
        
        if (0 == XdsBoardCount)
        {
            SC_ERROR error;

            /* Erase the function pointers. */
            error = ApiLoad(NULL);
            if (SC_ERR_NONE != error)
            {
                return error;
            }

            /* Free the library. */
            if (FALSE != xds_FreeLibrary(XdsBoardHandle))
            {
                /* Library failed to free. */
                return SC_ERR_LIB_BRD_FREE;
            }
            XdsBoardHandle = NULL;
        }
    }

    return SC_ERR_NONE;
}

/*******************************************************************************
* NAME: GetFunction()
*
* DESCRIPTION
*   Get a pointer for a named function from a library.
*
* NOTES
*   If the library handle is initialised then attempt to return a
*   function pointer to a named function in a previously loaded library.
*   If that fails (likely an invalid name) then return a NULL pointer value
*   that is distinct from the pointer value for the 'missing function' stub.
*
*   If the library handle is NOT initialised then return
*   a function pointer to the 'missing function' stub.
*
*******************************************************************************/

FNC_HANDLE
GetFunction(
    LIB_HANDLE library,     /* The library handle. */
    char      *pIdentifier) /* A string for name of the function. */
{   
    FNC_HANDLE procedure;

    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pIdentifier)
    {
        return NULL;
    }

    if (NULL != library)
    {
        procedure = xds_GetProcAddress(library, pIdentifier);
    }
    else
    {
        procedure = (FNC_HANDLE)GetMissing;
    }

    return procedure;
}

/*******************************************************************************
* NAME: GetMissing()
*
* DESCRIPTION
*   Get a pointer for a missing or un-loaded function.
*
* NOTES
*   This function returns a function pointer for a local function that has
*   no arguments and simply returns the error number SC_ERR_LIB_RTR_ACCESS.
*
*******************************************************************************/

SC_ERROR
GetMissing(
    void) /* There are no arguments. */
{    
    return SC_ERR_LIB_BRD_ACCESS;
}

/*******************************************************************************
* NAME: ApiLoad()
*
* DESCRIPTION
*   Load the API's function pointers in the interface.
*
* NOTES
*   This function initialises or erases the interface structure.
*
*   If the library handle is initialised then attempt to return a
*   complete set of function pointers from previously loaded library.
*
*   If the library handle is NOT initialised then empty the complete
*   set of function pointers from a previously loaded library.
*
*******************************************************************************/

SC_ERROR
ApiLoad(
    LIB_HANDLE library) /* The library handle. */
{
#if (USE_FILE_SYSTEM)
    LS_BoardFileType       = (LS_BoardFileType_t)       GetFunction(library, "LS_BoardFileType");
    LS_File2Struct         = (LS_File2Struct_t)         GetFunction(library, "LS_File2Struct");
    LS_Struct2File         = (LS_Struct2File_t)         GetFunction(library, "LS_Struct2File");
#endif /* (USE_FILE_SYSTEM) */

    LS_ConvertFamilyName   = (LS_ConvertFamilyName_t)   GetFunction(library, "LS_ConvertFamilyName");
    LS_ConvertVaryName     = (LS_ConvertVaryName_t)     GetFunction(library, "LS_ConvertVaryName");
    LS_CreateAlias         = (LS_CreateAlias_t)         GetFunction(library, "LS_CreateAlias");
    LS_CreateDevice        = (LS_CreateDevice_t)        GetFunction(library, "LS_CreateDevice");
    LS_CreateFamily        = (LS_CreateFamily_t)        GetFunction(library, "LS_CreateFamily");
    LS_CreateMasquerade    = (LS_CreateMasquerade_t)    GetFunction(library, "LS_CreateMasquerade");
    LS_CreateStruct        = (LS_CreateStruct_t)        GetFunction(library, "LS_CreateStruct");
    LS_CreateSubpath       = (LS_CreateSubpath_t)       GetFunction(library, "LS_CreateSubpath");
    LS_CreateVariable      = (LS_CreateVariable_t)      GetFunction(library, "LS_CreateVariable");
    LS_DeleteAlias         = (LS_DeleteAlias_t)         GetFunction(library, "LS_DeleteAlias");
    LS_DeleteDevice        = (LS_DeleteDevice_t)        GetFunction(library, "LS_DeleteDevice");
    LS_DeleteFamily        = (LS_DeleteFamily_t)        GetFunction(library, "LS_DeleteFamily");
    LS_DeleteMarshall      = (LS_DeleteMarshall_t)      GetFunction(library, "LS_DeleteMarshall");
    LS_DeleteMasquerade    = (LS_DeleteMasquerade_t)    GetFunction(library, "LS_DeleteMasquerade");
    LS_DeleteSubpath       = (LS_DeleteSubpath_t)       GetFunction(library, "LS_DeleteSubpath");
    LS_DeleteStruct        = (LS_DeleteStruct_t)        GetFunction(library, "LS_DeleteStruct");
    LS_DeleteVariable      = (LS_DeleteVariable_t)      GetFunction(library, "LS_DeleteVariable");
    LS_FromValue2Frequency = (LS_FromValue2Frequency_t) GetFunction(library, "LS_FromValue2Frequency");
    LS_FromValue2Logic     = (LS_FromValue2Logic_t)     GetFunction(library, "LS_FromValue2Logic");
    LS_FromValue2Signed    = (LS_FromValue2Signed_t)    GetFunction(library, "LS_FromValue2Signed");
    LS_FromValue2String    = (LS_FromValue2String_t)    GetFunction(library, "LS_FromValue2String");
    LS_FromValue2Unsign    = (LS_FromValue2Unsign_t)    GetFunction(library, "LS_FromValue2Unsign");
    LS_GetAliasByIndex     = (LS_GetAliasByIndex_t)     GetFunction(library, "LS_GetAliasByIndex");
    LS_GetConfigValue      = (LS_GetConfigValue_t)      GetFunction(library, "LS_GetConfigValue");
    LS_GetDeviceByIndex    = (LS_GetDeviceByIndex_t)    GetFunction(library, "LS_GetDeviceByIndex");
    LS_GetErrorStatus      = (LS_GetErrorStatus_t)      GetFunction(library, "LS_GetErrorStatus");
    LS_GetFamilyByIndex    = (LS_GetFamilyByIndex_t)    GetFunction(library, "LS_GetFamilyByIndex");
    LS_GetItemNumbers      = (LS_GetItemNumbers_t)      GetFunction(library, "LS_GetItemNumbers");
    LS_GetSubpathByIndex   = (LS_GetSubpathByIndex_t)   GetFunction(library, "LS_GetSubpathByIndex");
    LS_GetTargetLengths    = (LS_GetTargetLengths_t)    GetFunction(library, "LS_GetTargetLengths");
    LS_GetVariableByIndex  = (LS_GetVariableByIndex_t)  GetFunction(library, "LS_GetVariableByIndex");
    LS_IsAliasGood         = (LS_IsAliasGood_t)         GetFunction(library, "LS_IsAliasGood");
    LS_IsDeviceGood        = (LS_IsDeviceGood_t)        GetFunction(library, "LS_IsDeviceGood");
    LS_IsFamilyGood        = (LS_IsFamilyGood_t)        GetFunction(library, "LS_IsFamilyGood");
    LS_IsSubpathGood       = (LS_IsSubpathGood_t)       GetFunction(library, "LS_IsSubpathGood");
    LS_IsVariableGood      = (LS_IsVariableGood_t)      GetFunction(library, "LS_IsVariableGood");
    LS_Marshall2Struct     = (LS_Marshall2Struct_t)     GetFunction(library, "LS_Marshall2Struct");
    LS_PutErrorStatus      = (LS_PutErrorStatus_t)      GetFunction(library, "LS_PutErrorStatus");
    LS_Struct2Marshall     = (LS_Struct2Marshall_t)     GetFunction(library, "LS_Struct2Marshall");

    return SC_ERR_NONE;
}

/*******************************************************************************
* NAME: ApiCheck()
*
* DESCRIPTION
*   Check the API's function pointers in the interface.
*
* NOTES
*   This function sets a status flag if one or more
*   function pointers are not-valid or 'not initialised'.
*
*******************************************************************************/

SC_ERROR
ApiCheck(
    logic_t *pNotValid)  /* A pointer to the 'not initialised' status flag. */  
{    
    /* A NULL pointer value means a stupid coding error. */
    if (NULL == pNotValid)
    {
        return SC_ERR_CMD_PARM;
    }

    if (
#if (USE_FILE_SYSTEM)
        NULL == LS_BoardFileType       || 
        NULL == LS_File2Struct         ||
        NULL == LS_Struct2File         ||
#endif /* (USE_FILE_SYSTEM) */

        NULL == LS_ConvertFamilyName   ||
        NULL == LS_ConvertVaryName     ||
        NULL == LS_CreateAlias         ||
        NULL == LS_CreateDevice        ||
        NULL == LS_CreateFamily        ||
        NULL == LS_CreateMasquerade    ||
        NULL == LS_CreateStruct        ||
        NULL == LS_CreateSubpath       ||
        NULL == LS_CreateVariable      ||
        NULL == LS_DeleteAlias         ||
        NULL == LS_DeleteDevice        ||
        NULL == LS_DeleteFamily        ||
        NULL == LS_DeleteMarshall      ||
        NULL == LS_DeleteMasquerade    ||
        NULL == LS_DeleteStruct        ||
        NULL == LS_DeleteSubpath       ||
        NULL == LS_DeleteVariable      ||
        NULL == LS_FromValue2Frequency ||
        NULL == LS_FromValue2Logic     ||
        NULL == LS_FromValue2Signed    ||
        NULL == LS_FromValue2String    ||
        NULL == LS_FromValue2Unsign    ||
        NULL == LS_GetAliasByIndex     ||
        NULL == LS_GetConfigValue      ||
        NULL == LS_GetDeviceByIndex    ||
        NULL == LS_GetErrorStatus      ||
        NULL == LS_GetFamilyByIndex    ||
        NULL == LS_GetItemNumbers      ||
        NULL == LS_GetSubpathByIndex   ||
        NULL == LS_GetTargetLengths    ||
        NULL == LS_GetVariableByIndex  ||
        NULL == LS_IsAliasGood         ||
        NULL == LS_IsDeviceGood        ||
        NULL == LS_IsFamilyGood        ||
        NULL == LS_IsSubpathGood       ||
        NULL == LS_IsVariableGood      ||
        NULL == LS_Marshall2Struct     ||
        NULL == LS_PutErrorStatus      ||
        NULL == LS_Struct2Marshall            
        )
    {
        *pNotValid = TRUE;
    }
    else
    {
        *pNotValid = FALSE;
    }

    return SC_ERR_NONE;
}

/* -- END OF FILE -- */
