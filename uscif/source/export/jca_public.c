/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_public.c
*
* DESCRIPTION
*   The public functions that are the documented JCA API. 
*
* PUBLIC FUNCTIONS
*
*   JCA_Create()     - Create a communications adapter structure.
*   JCA_Delete()     - Delete a communications adapter structure.
*   JCA_Init()       - Software open a communications adapter.
*   JCA_Quit()       - Software close a communications adapter.
*   JCA_Connect()    - Hardware connect to a communications adapter.
*   JCA_Disconnect() - Hardware disconnect from a communications adapter.
*
*   JCA_AcquireSemaphore() - Acquire the communications adapter semaphore.
*   JCA_ReleaseSemaphore() - Release the communications adapter semaphore.
*   JCA_QuerySemaphore()   - Query the communications adapter client count.
*
*   JCA_SingleRegisterRead()   - Read a single hardware register.
*   JCA_SingleRegisterWrite()  - Write a single hardware register.
*   JCA_MultipleRegisterRead() - Read many registers at different addresses.
*   JCA_MultipleRegisterWrite()- Write many registers at different addresses.
*   JCA_ArrayRegisterRead()    - Read many registers at a single address.
*   JCA_ArrayRegisterWrite()   - Write many registers at a single address.
*
*   JCA_BufferDataTransfer() - Perform a buffer data transfer via the controller.
*   JCA_BufferDataRead08()   - Read 8-bit data earlier taken from the controller.
*   JCA_BufferDataWrite08()  - Write 8-bit data later given to the controller.
*   JCA_BufferDataRead16()   - Read 16-bit data earlier taken from the controller.
*   JCA_BufferDataWrite16()  - Write 16-bit data later given to the controller.
*   JCA_BufferDataRead32()   - Read 32-bit data earlier taken from the controller.
*   JCA_BufferDataWrite32()  - Write 32-bit data later given to the controller.
*
*   JCA_StatusPoll()  - Poll for predefined status in one or more registers.
*   JCA_WriteFlag()   - Write an action flag to the communications adapter.
*   JCA_ReadError()   - Read an error value from the communications adapter.
*
*   JCA_WaitForTime() - Wait until a specified delay in time has occurred.
*   JCA_WaitForTclk() - Wait until a specified delay in TCLK's has occurred.
*   JCA_TimeOut()     - Select the timeout for data transfers and status pollings.
*
*   JCA_ProgramStart()    - Start programming the controller and utility FPGA/CPLD�s.
*   JCA_ProgramTransfer() - Transfer data to program the controller and utility FPGA/CPLD�s.
*   JCA_ProgramFinish()   - Finish programming the controller and utility FPGA/CPLD�s.
*   JCA_ProgramVersion()  - Query the version of the controller and utility FPGA/CPLD�s.
*
* NOTES
*   Each of these public functions (with upper-case JCA_ prefix)
*   simply hide the private equivalent (with lower-case jca_ prefix) and
*   hide the private JCA_Inform* structure behind the opaque JCA_Handle handle.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define jca_public_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "xds_variable.h"

#include "cfg_public.h"

#include "jca_public.h"
#include "jca_internal.h"

#include "jca_api_buffer.h"
#include "jca_api_error.h"
#include "jca_api_flag.h"
#include "jca_api_main.h"
#include "jca_api_poll.h"
#include "jca_api_program.h"
#include "jca_api_register.h"
#include "jca_api_semaphore.h"
#include "jca_api_timeout.h"
#include "..\..\debug.h"
/*******************************************************************************
* Functions located in jca_api_main.c  [2.1.1]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_Create()
*
* DESCRIPTION
*   Create a communications adapter structure.
*
* NOTES
*   This wrapper simply hides jca_Create() behind JCA_Create()
*   and hides JCA_Inform* behind JCA_Handle.
*
*******************************************************************************/

JTI_Error 
JCA_Create(
    JCA_Handle *pAdapterHandle) /* Pointer to comm�s adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_Create\n");
    error = jca_Create(
                (JCA_Inform**)pAdapterHandle);

    return error;
}

/*******************************************************************************
* NAME: JCA_Delete()
*
* DESCRIPTION
*   Delete a communications adapter structure.
*
* NOTES
*   This wrapper simply hides jca_Delete() behind JCA_Delete()
*   and hides JCA_Inform* behind JCA_Handle.
*
*******************************************************************************/

JTI_Error
JCA_Delete(
    JCA_Handle *pAdapterHandle) /* Pointer to comm�s adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_Delete\n");
    error = jca_Delete(
                (JCA_Inform**)pAdapterHandle);

    return error;
}

/*******************************************************************************
* NAME: JCA_Init()
*
* DESCRIPTION
*   Software open a communications adapter.
*
* NOTES
*   This wrapper simply hides jca_Init() behind JCA_Init()
*   and hides JCA_Inform* behind JCA_Handle.
*
*******************************************************************************/

JTI_Error
JCA_Init(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    CFG_Handle adapterConfig, /* Variables to configure adapter. */
    JTI_Client jtiClientType) /* Type of the JTI client using JCA. */
{
    JTI_Error error;
    dbgprint("JCA_Init\n");
    error = jca_Init(
                (JCA_Inform*)adapterHandle,
                adapterConfig,
                jtiClientType);

    return error;
}

/*******************************************************************************
* NAME: JCA_Quit()
*
* DESCRIPTION
*   Software close a communications adapter.
*
* NOTES
*   This wrapper simply hides jca_Quit() behind JCA_Quit()
*   and hides JCA_Inform* behind JCA_Handle.
*
*******************************************************************************/

JTI_Error
JCA_Quit(
    JCA_Handle adapterHandle)  /* Communications adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_Quit\n");
    error = jca_Quit(
                (JCA_Inform*)adapterHandle);

    return error;
}

/*******************************************************************************
* NAME: JCA_Connect()
*
* DESCRIPTION
*   Hardware connect to a communications adapter.
*
* NOTES
*   This wrapper simply hides jca_Connect() behind JCA_Connect()
*   and hides JCA_Inform* behind JCA_Handle.
*
*******************************************************************************/

JTI_Error
JCA_Connect(
    JCA_Handle adapterHandle)  /* Communications adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_Connect\n");
    error = jca_Connect(
                (JCA_Inform*)adapterHandle);

    return error;
}

/*******************************************************************************
* NAME: JCA_Disconnect()
*
* DESCRIPTION
*   Hardware disconnect from a communications adapter.
*
* NOTES
*   This wrapper simply hides jca_Disconnect() behind JCA_Disconnect()
*   and hides JCA_Inform* behind JCA_Handle.
*
*******************************************************************************/

JTI_Error
JCA_Disconnect(
    JCA_Handle adapterHandle)  /* Communications adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_Disconnect\n");
    error = jca_Disconnect(
                (JCA_Inform*)adapterHandle);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_semaphore.c  [2.1.2]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_AcquireSemaphore()
*
* DESCRIPTION
*   Acquire the communications adapter semaphore.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_AcquireSemaphore(
    JCA_Handle adapterHandle)  /* Communication adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_AcquireSemaphore\n");
    error = jca_AcquireSemaphore(
                (JCA_Inform*)adapterHandle);

    return error;
}

/*******************************************************************************
* NAME: JCA_ReleaseSemaphore()
*
* DESCRIPTION
*   Release the communications adapter semaphore.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ReleaseSemaphore(
    JCA_Handle adapterHandle)  /* Communication adapter handle. */
{
    JTI_Error error;
    dbgprint("JCA_ReleaseSemaphore\n");
    error = jca_ReleaseSemaphore(
                (JCA_Inform*)adapterHandle);

    return error;
}

/*******************************************************************************
* NAME: JCA_QuerySemaphore()
*
* DESCRIPTION
*   Query the communications adapter client count.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_QuerySemaphore(
    JCA_Handle adapterHandle,  /* Communication adapter handle. */
    uint32_t  *pClientNumber)  /* Number of JCA clients connected. */
{
    JTI_Error error;
    dbgprint("JCA_QuerySemaphore\n");
    error = jca_QuerySemaphore(
                (JCA_Inform*)adapterHandle,
                pClientNumber);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_register.c  [2.1.3]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_SingleRegisterRead()
*
* DESCRIPTION
*   Read a single hardware register.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_SingleRegisterRead(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t  *pAccessData)   /* The pointer to the register value. */
{
    JTI_Error error;
//    dbgprint("JCA_SingleRegisterRead\n");
    error = jca_SingleRegisterRead(
                (JCA_Inform*)adapterHandle,
                accessAddr,
                pAccessData);

    return error;
}

/*******************************************************************************
* NAME: JCA_SingleRegisterWrite()
*
* DESCRIPTION
*   Write a single hardware register. 
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_SingleRegisterWrite(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t   accessData)    /* The register value. */
{
    JTI_Error error;
//    dbgprint("JCA_SingleRegisterWrite\n");
    error = jca_SingleRegisterWrite(
                (JCA_Inform*)adapterHandle,
                accessAddr,
                accessData);

    return error;
}

/*******************************************************************************
* NAME: JCA_MultipleRegisterRead()
*
* DESCRIPTION
*   Read many registers at different addresses.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_MultipleRegisterRead(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t  *pAccessAddr,   /* Array of register addresses. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData)     /* Number of addresses and values. */
{
    JTI_Error error;
    dbgprint("JCA_MultipleRegisterRead\n");
    error = jca_MultipleRegisterRead(
                (JCA_Inform*)adapterHandle, 
                pAccessAddr, 
                pAccessData,
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_MultipleRegisterWrite()
*
* DESCRIPTION
*   Write many registers at different addresses.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_MultipleRegisterWrite(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t  *pAccessAddr,   /* Array of register addresses. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData)     /* Number of addresses and values. */
{
    JTI_Error error;
    dbgprint("JCA_MultipleRegisterWrite\n");
    error = jca_MultipleRegisterWrite(
                (JCA_Inform*)adapterHandle, 
                pAccessAddr, 
                pAccessData,
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_ArrayRegisterRead()
*
* DESCRIPTION
*   Read many registers at a single address.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ArrayRegisterRead(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData)     /* Number of register values. */
{
    JTI_Error error;
    dbgprint("JCA_ArrayRegisterRead\n");
    error = jca_ArrayRegisterRead(
                (JCA_Inform*)adapterHandle, 
                accessAddr, 
                pAccessData,
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_ArrayRegisterWrite()
*
* DESCRIPTION
*   Write many registers at a single address.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ArrayRegisterWrite(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData)     /* Number of register values. */ 
{
    JTI_Error error;
    dbgprint("JCA_ArrayRegisterWrite\n");
    error = jca_ArrayRegisterWrite(
                (JCA_Inform*)adapterHandle, 
                accessAddr, 
                pAccessData,
                countData);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_io.c  [2.1.4]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_BufferDataTransfer()
*
* DESCRIPTION
*   Perform a buffer data transfer via the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataTransfer(
    JCA_Handle   adapterHandle, /* Communications adapter handle. */
    JCA_XferType accessType,    /* The type of data transfer. */
    uint32_t     bitCount)      /* The number of bits. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataTransfer\n");
    error = jca_BufferDataTransfer(
                (JCA_Inform*)adapterHandle,
                accessType,
                bitCount);

    return error;
}

/*******************************************************************************
* NAME: JCA_BufferDataRead08()
*
* DESCRIPTION
*   Read 8-bit data earlier taken from the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataRead08(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint8_t   *pAccessData,   /* Array of buffer values. */
    uint32_t   countData)     /* Number of buffer values. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataRead08\n");
    error = jca_BufferDataRead08(
                (JCA_Inform*)adapterHandle, 
                accessSelect, 
                pAccessData, 
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_BufferDataWrite08()
*
* DESCRIPTION
*   Write 8-bit data later given to the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataWrite08(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint8_t   *pAccessData,   /* Array of buffer values. */
    uint32_t   countData)     /* Number of buffer values. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataWrite08\n");
    error = jca_BufferDataWrite08(
                (JCA_Inform*)adapterHandle, 
                accessSelect, 
                pAccessData, 
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_BufferDataRead16()
*
* DESCRIPTION
*   Read 16-bit data earlier taken from the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataRead16(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint16_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData)     /* Number of buffer values. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataRead16\n");
    error = jca_BufferDataRead16(
                (JCA_Inform*)adapterHandle, 
                accessSelect, 
                pAccessData, 
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_BufferDataWrite16()
*
* DESCRIPTION
*   Write 16-bit data later given to the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataWrite16(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint16_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData)     /* Number of buffer values. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataWrite16\n");
    error = jca_BufferDataWrite16(
                (JCA_Inform*)adapterHandle, 
                accessSelect, 
                pAccessData, 
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_BufferDataRead32()
*
* DESCRIPTION
*   Read 32-bit data earlier taken from the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataRead32(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint32_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData)     /* Number of buffer values. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataRead32\n");
    error = jca_BufferDataRead32(
                (JCA_Inform*)adapterHandle, 
                accessSelect, 
                pAccessData, 
                countData);

    return error;
}

/*******************************************************************************
* NAME: JCA_BufferDataWrite32()
*
* DESCRIPTION
*   Write 32-bit data later given to the controller.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_BufferDataWrite32(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint32_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData)     /* Number of buffer values. */
{
    JTI_Error error;
    dbgprint("JCA_BufferDataWrite32\n");
    error = jca_BufferDataWrite32(
                (JCA_Inform*)adapterHandle, 
                accessSelect, 
                pAccessData, 
                countData);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_poll.c  [2.1.5]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_StatusPoll()
*
* DESCRIPTION
*   Poll for predefined status in one or more registers.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_StatusPoll(
    JCA_Handle     adapterHandle, /* Communication adapter handle. */
    JCA_StatusType statusType,    /* The status being polled. */
    uint32_t       testValue)     /* The value being tested. */
{
    JTI_Error error;
    dbgprint("JCA_StatusPoll\n");
    error = jca_StatusPoll(
                (JCA_Inform*)adapterHandle,
                statusType,
                testValue);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_error.c  [2.1.6]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_WriteFlag()
*
* DESCRIPTION
*   Write an action flag to the communications adapter.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_WriteFlag(
    JCA_Handle     adapterHandle, /* Communication adapter handle. */
    JCA_ActionType actionType)    /* The type of action. */
{
    JTI_Error error;
    dbgprint("JCA_WriteFlag\n");
    error = jca_WriteFlag(
                (JCA_Inform*)adapterHandle,
                actionType);

    return error;
}

/*******************************************************************************
* NAME: JCA_ReadError()
*
* DESCRIPTION
*   Read an error value from the communications adapter.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ReadError(
    JCA_Handle  adapterHandle, /* Communications adapter handle. */
    JTI_Error  *pErrorReturn)  /* Returned error value. */
{
    JTI_Error error;
    dbgprint("JCA_ReadError\n");
   error = jca_ReadError(
                (JCA_Inform*)adapterHandle,
                pErrorReturn);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_timeout.c [2.1.7]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_WaitForTime()
*
* DESCRIPTION
*   Wait until a specified delay in time has occurred.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_WaitForTime(
    JCA_Handle  adapterHandle, /* Communications adapter handle. */
    uint32_t    timeDelay)     /* A delay measured in micro-seconds. */
{
    JTI_Error error;
    dbgprint("JCA_WaitForTime\n");
    error = jca_WaitForTime(
                (JCA_Inform*)adapterHandle,
                timeDelay);

    return error;
}

/*******************************************************************************
* NAME: JCA_WaitForTclk()
*
* DESCRIPTION
*   Wait until a specified delay in TCLK's has occurred.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_WaitForTclk(
    JCA_Handle  adapterHandle, /* Communications adapter handle. */
    uint32_t    periodDelay)   /* A delay measured in TCLK periods. */
{
    JTI_Error error;
    dbgprint("JCA_WaitForTclk\n");
    error = jca_WaitForTclk(
                (JCA_Inform*)adapterHandle,
                periodDelay);

    return error;
}

/*******************************************************************************
* NAME: JCA_TimeOut()
*
* DESCRIPTION
*   Select the timeout for data transfers and status pollings.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_TimeOut(
    JCA_Handle  adapterHandle, /* Communication adapter handle. */
    JCA_Timeout dataTransfer,  /* Select the data transfer timeout. */
    JCA_Timeout statusPoll)    /* Select the status poll timeout. */
{
    JTI_Error error;
    dbgprint("JCA_TimeOut\n");
    error = jca_TimeOut(
                (JCA_Inform*)adapterHandle,
                dataTransfer,
                statusPoll);

    return error;
}

/*******************************************************************************
* Functions located in jca_api_program.c [2.1.8]
*******************************************************************************/

/*******************************************************************************
* NAME: JCA_ProgramStart()
*
* DESCRIPTION
*   Start programming the controller and utility FPGA/CPLD�s.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ProgramStart(
    JCA_Handle      adapterHandle, /* Communications adapter handle. */
    JCA_ProgramType hardwareType)  /* The FPGA/CPLD type. */
{
    JTI_Error error;
    dbgprint("JCA_ProgramStart\n");
    error = jca_ProgramStart(
                (JCA_Inform*)adapterHandle,
                hardwareType);

    return error;
}

/*******************************************************************************
* NAME: JCA_ProgramTransfer()
*
* DESCRIPTION
*   Transfer data to program the controller and utility FPGA/CPLD�s.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ProgramTransfer(
    JCA_Handle      adapterHandle,  /* Communications adapter handle. */
    JCA_ProgramType hardwareType,   /* The FPGA/CPLD type. */
    unsigned char  *pProgramData,   /* The FPGA/CPLD programming data. */
    uint32_t        programNumber,  /* The number of data values. */
    uint32_t       *pProgramStatus) /* The result status. */
{
    JTI_Error error;
    dbgprint("JCA_ProgramTransfer\n");
    error = jca_ProgramTransfer(
                (JCA_Inform*)adapterHandle, 
                hardwareType, 
                pProgramData,
                programNumber,
                pProgramStatus);

    return error;
}

/*******************************************************************************
* NAME: JCA_ProgramFinish()
*
* DESCRIPTION
*   Finish programming the controller and utility FPGA/CPLD�s.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ProgramFinish(
    JCA_Handle      adapterHandle,  /* Communications adapter handle. */
    JCA_ProgramType hardwareType,   /* The FPGA/CPLD type. */
    uint32_t       *pProgramStatus) /* The result status. */
{
    JTI_Error error;
    dbgprint("JCA_ProgramFinish\n");
    error = jca_ProgramFinish(
                (JCA_Inform*)adapterHandle, 
                hardwareType, 
                pProgramStatus);

    return error;
}

/*******************************************************************************
* NAME: JCA_ProgramVersion()
*
* DESCRIPTION
*   Query the version of the controller and utility FPGA/CPLD�s.
*
* NOTES
*
*******************************************************************************/

JTI_Error
JCA_ProgramVersion(
    JCA_Handle      adapterHandle,    /* Communications adapter handle. */
    JCA_ProgramType hardwareType,     /* The FPGA/CPLD type. */
    uint32_t       *pHardwareVersion) /* The version status. */
{
    JTI_Error error;
    dbgprint("JCA_ProgramVersion\n");
    error = jca_ProgramVersion(
                (JCA_Inform*)adapterHandle,
                hardwareType,
                pHardwareVersion);

    return error;
}

/* -- END OF FILE -- */
