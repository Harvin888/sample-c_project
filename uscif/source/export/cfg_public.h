/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2003-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   cfg_public.h
*
* DESCRIPTION
*   USCIF exported functions for the board configure file.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef cfg_public_h
#define cfg_public_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* Define the root object names.
*******************************************************************************/

#define LS_ROOT_FAMILY_NAME    "root_family"
#define LS_ROOT_DEVICE_NAME    "root_device"
#define LS_ROOT_SUBPATH_NAME   "root_subpath"

/*******************************************************************************
* Define the format strings for board configuration.
*******************************************************************************/

#define LS_BOARD_ERRORS_TITLE  "errors"
#define LS_BOARD_CONFIG_TITLE  "config"

#define LS_BOARD_MODERN_NAME   "version"
#define LS_BOARD_MODERN_VALUE  "3.5"

#define LS_BOARD_XEMEL_VALUE   "version='1.0' ?>"

#define LS_BOARD_XEMEL_LABEL   "<?xml"
#define LS_BOARD_LEGACY_LABEL  ";cfg-2.0"
#define LS_BOARD_MODERN_LABEL   "# config version=3.5"
#define LS_BOARD_ERRORS_LABEL   "# errors version=3.5"

#define LS_BOARD_XEMEL_SIZE     5
#define LS_BOARD_LEGACY_SIZE    8
#define LS_BOARD_MODERN_SIZE    20

/*******************************************************************************
* Define the format flags for board configuration.
*******************************************************************************/

#define LS_BOARD_FILE_UNKNOWN 0x00000000
#define LS_BOARD_FILE_XEMEL   0x00000001
#define LS_BOARD_FILE_LEGACY  0x00000002
#define LS_BOARD_FILE_MODERN  0x00000003
#define LS_BOARD_FILE_ERRORS  0x00000004
#define LS_BOARD_FILE_HEADER  0x00000005

/*******************************************************************************
* Define the option flags for board configuration.
*******************************************************************************/

#define LS_TEXTIO_OPT_USEMASK      0x000000FF
#define LS_TEXTIO_OPT_USEOFFSET    0x00000001
#define LS_TEXTIO_OPT_USETITLE     0x00000002
#define LS_TEXTIO_OPT_USEVARIABLE  0x00000004
#define LS_TEXTIO_OPT_USEPACKAGE   0x00000008
#define LS_TEXTIO_OPT_USEFAMILY    0x00000010
#define LS_TEXTIO_OPT_USEALIAS     0x00000020
#define LS_TEXTIO_OPT_USEDEVICE    0x00000040
#define LS_TEXTIO_OPT_USESUBPATH   0x00000080

#define LS_TEXTIO_OPT_INMASK       0x0000FF00
#define LS_TEXTIO_OPT_INSTREAM     0x00000100
#define LS_TEXTIO_OPT_INMEMORY     0x00000200
#define LS_TEXTIO_OPT_INEXPAND     0x00000400
#define LS_TEXTIO_OPT_IN03         0x00000800
#define LS_TEXTIO_OPT_IN04         0x00001000
#define LS_TEXTIO_OPT_IN05         0x00002000
#define LS_TEXTIO_OPT_IN06         0x00004000
#define LS_TEXTIO_OPT_INDBGJTAG    0x00008000

#define LS_TEXTIO_OPT_OUTMASK      0x00FF0000
#define LS_TEXTIO_OPT_OUTSTREAM    0x00010000
#define LS_TEXTIO_OPT_OUTMEMORY    0x00020000
#define LS_TEXTIO_OPT_OUTCOMMENT   0x00040000
#define LS_TEXTIO_OPT_OUTINDENT    0x00080000
#define LS_TEXTIO_OPT_OUTXPLICIT   0x00100000
#define LS_TEXTIO_OPT_OUTCRUSHED   0x00200000
#define LS_TEXTIO_OPT_OUTEXCLUDE   0x00400000
#define LS_TEXTIO_OPT_OUT07        0x00800000

#define LS_TEXTIO_OPT_DBGMASK      0xFF000000
#define LS_TEXTIO_OPT_DBGSTREAM    0x01000000
#define LS_TEXTIO_OPT_DBG01        0x02000000
#define LS_TEXTIO_OPT_DBG02        0x04000000
#define LS_TEXTIO_OPT_DBG03        0x08000000

/*******************************************************************************
* Constants for both board configuration and scan-path management.
*******************************************************************************/

#define LS_UNUSED_DR_BITS  (uint32_t)-1
#define LS_UNUSED_IR_BITS  (uint32_t)-1
#define LS_UNUSED_ADDRESS  (uint32_t)-1
#define LS_UNUSED_IDENTIFY (uint32_t)-1

#define LS_ZERO_DR_BITS    (uint32_t)0
#define LS_ZERO_IR_BITS    (uint32_t)0
#define LS_ZERO_ADDRESS    (uint32_t)0
#define LS_ZERO_IDENTIFY   (uint32_t)0

#define LS_UNUSED_ERROR    (uint32_t)-1
#define LS_UNUSED_OFFSET   (uint32_t)-1
#define LS_UNUSED_TITLE    (uint32_t)-1
#define LS_UNUSED_VARIABLE (uint32_t)-1
#define LS_UNUSED_PACKAGE  (uint32_t)-1
#define LS_UNUSED_FAMILY   (uint32_t)-1
#define LS_UNUSED_ALIAS    (uint32_t)-1
#define LS_UNUSED_DEVICE   (uint32_t)-1
#define LS_UNUSED_SUBPATH  (uint32_t)-1

#define LS_ZERO_OFFSETS    (uint32_t)0
#define LS_ZERO_TITLES     (uint32_t)0
#define LS_ZERO_PACKAGES   (uint32_t)0
#define LS_ZERO_FAMILIES   (uint32_t)0
#define LS_ZERO_ALIASES    (uint32_t)0
#define LS_ZERO_DEVICES    (uint32_t)0
#define LS_ZERO_SUBPATHS   (uint32_t)0
#define LS_ZERO_VARIABLES  (uint32_t)0

/*******************************************************************************
* Constants for both board configuration and scan-path management.
*******************************************************************************/

#define LS_ROOT_PACKAGE   (uint32_t)0
#define LS_ROOT_FAMILY    (uint32_t)0
#define LS_ROOT_DEVICE    (uint32_t)0
#define LS_ROOT_SUBPATH   (uint32_t)0

#define LS_FIRST_OFFSET   (uint32_t)0 /* There is no root offset. */
#define LS_FIRST_TITLE    (uint32_t)0 /* There is no root title. */

#define LS_FIRST_VARIABLE (uint32_t)0 /* There is no root variable. */
#define LS_FIRST_PACKAGE  (uint32_t)1
#define LS_FIRST_FAMILY   (uint32_t)1
#define LS_FIRST_ALIAS    (uint32_t)0 /* There is no root alias. */
#define LS_FIRST_DEVICE   (uint32_t)1
#define LS_FIRST_SUBPATH  (uint32_t)1

/*******************************************************************************
* Define the Unified-SCIF constants for board configuration.
*******************************************************************************/

/* The structure names. */
#define LS_BOARD_STRCT_LABEL   "-itsa-config-struct-"
#define LS_BOARD_ERASE_LABEL   "~erase~config~stuff~"

/* The maximum number of characters in structure names. */
#define LS_BOARD_STRCT_SIZE     20
#define LS_BOARD_ERASE_SIZE     20

/*
 * The maximum number of characters in a config variable name.
 * Allow for config variable names to be a 'section' plus a '.' plus a 'variable'.
 *
 * These mnemonics are defined in both "cfg_public.h" and "xds_variable.h".
 */
#ifndef LS_MAX_CFG
#define LS_MAX_CFG
  /* The maximum number of characters in identifier names. */
  #define LS_MAX_IDENTIFIER_NAME  16

  #define LS_MAX_CFG_VARY_NAME   (LS_MAX_IDENTIFIER_NAME + 1 + LS_MAX_IDENTIFIER_NAME)
  /* The maximum number of characters in a short config variable value. */
  #define LS_MAX_CFG_VARY_SHORT   32
  /* The maximum number of characters in a long config variable value. */
  #define LS_MAX_CFG_VARY_LONG    256
  /* The maximum length of the customized text string. */
  #define LS_MAX_TEXT_LENGTH      128
#endif

/* The maximum number of characters in a section name. */
#define LS_MAX_SECTION_NAME     LS_MAX_IDENTIFIER_NAME
/* The meta-character for section names. */
#define LS_METACHAR_SECTION     "."

/* The maximum number of characters in an error description. */
#define LS_MAX_ERROR_TITLE     (LS_MAX_IDENTIFIER_NAME * 2)
#define LS_MAX_ERROR_BRIEF     (LS_MAX_CFG_VARY_SHORT * 3)
#define LS_MAX_ERROR_EXPLAIN   (LS_MAX_CFG_VARY_LONG * 4)

/* The fixed names and values for offset and title descriptions. */
#define LS_FIXED_OFFSET_NAME   "offset"
#define LS_FIXED_ORIGIN_VALUE  "uscif"

/* The maximum number of characters in an offset and title name. */
#define LS_MAX_OFFSET_NAME     (LS_MAX_IDENTIFIER_NAME * 2)
#define LS_MAX_TITLE_NAME      (LS_MAX_IDENTIFIER_NAME * 2)

/* The maximum number of characters in a package name. */
#define LS_MAX_PACKAGE_NAME     LS_MAX_IDENTIFIER_NAME

/* The maximum number of characters in a family name. */
#define LS_MAX_FAMILY_NAME      LS_MAX_IDENTIFIER_NAME

/* The maximum number of characters in an alias name. */
#define LS_MAX_ALIAS_NAME       LS_MAX_IDENTIFIER_NAME

/* The maximum number of characters in a device name. */
#define LS_MAX_DEVICE_NAME      LS_MAX_IDENTIFIER_NAME

/* The maximum number of characters in a subpath name. */
#define LS_MAX_SUBPATH_NAME     LS_MAX_IDENTIFIER_NAME

/*******************************************************************************
* Declare the typedefs for error information files.
* These are exactly equivalent to the description in the configuration file.
*******************************************************************************/

/*
 * The LS_OFFSET structure.
 * This is used to hold an offset description from the board config file.
 */
typedef struct LS_OFFSET
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The offset name. */
    char     offset[LS_MAX_OFFSET_NAME + 1];
    /* The offset number. */
    int32_t  number;

} LS_OFFSET;

/*
 * The LS_TITLE structure.
 * This is used to hold a title description from the board config file.
 */
typedef struct LS_TITLE
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The title name. */
    char     name[LS_MAX_TITLE_NAME + 1];

    /* The offset name. */
    char     offset[LS_MAX_OFFSET_NAME + 1];
    /* The offset number. */
    int32_t  number;

    /* The array of pointers to alias strings. */
    char   **aliasString;
    uint32_t countString;

    /* Pointer to a brief explanation string. */
    char *brief;
    /* Pointer to a full explanation string.  */
    char *explain;

    /* The error value. */
    int32_t value;

} LS_TITLE;

/*******************************************************************************
* Structure declarations for scan-path management.
* These are derived from the description in the configuration file.
*******************************************************************************/

/*
 * The LS_VARIABLE structure.
 * This is used to hold a variable description from the board config file.
 */
typedef struct LS_VARIABLE
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* Pointer to a configuration variable name.  */
    char *name;
    /* Pointer to a configuration variable value. */
    char *value;

} LS_VARIABLE;

/*
 * The LS_PACKAGE structure.
 * This is used to hold a package description from the board config file.
 */
typedef struct LS_PACKAGE
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The package name. */
    char     name[LS_MAX_PACKAGE_NAME + 1];

} LS_PACKAGE;

/*
 * The LS_FAMILY structure.
 * This is used to hold a family description from the board config file.
 */
typedef struct LS_FAMILY
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The family name. */
    char     name[LS_MAX_FAMILY_NAME + 1];

    /* Properties of the jtag device. */
    uint32_t drBits;  /* The length of the JTAG Bypass Register of the device.      */
    uint32_t irBits;  /* The length of the JTAG Instruction Register of the device. */

    /* Properties of the end delays. */
    uint32_t onceEnd; /* The length of the end delay parameter after a single scan. */
    uint32_t manyEnd; /* The length of the end delay parameter after a repeat scan. */

    /* Properties of the fixed memory. */
    uint32_t fixNumber; /* The number of fixed memory spaces needed for the device.  */
    uint32_t fixedSize; /* The size of fixed memory spaces allocated for the device. */

    /* Properties of the family name. */
    uint32_t scanPaths; /* The actual number of IR/DR scan-paths inside the device. */
    uint32_t unused;    /* Available for future use.                                */

    /* Properties of the router device. */
#if 0
    logic_t  itsaRouter;       /* The family is/is-not a router device. */
    logic_t  incrementTowards; /* The addresses increment/decrement towards the emulator data input. */
    logic_t  locationNearSide; /* The subpaths are on the side near/far the emulator data input. */
    uint32_t maximumSubpaths;  /* The maximum number of router sub-paths. */
#else
    uint32_t routerFlags; /* The flags for router properties of the device.        */
    uint32_t routerPaths; /* The maximum number of router sub-paths of the device. */
#endif

} LS_FAMILY;

/*
 * The LS_ALIAS structure.
 * This is used to hold an alias description from the board config file.
 */
typedef struct LS_ALIAS
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The family name. */
    char     name[LS_MAX_FAMILY_NAME + 1];

    /* The family ID value. */
    uint32_t familyId;

    /* The alias type. */
    char     type[LS_MAX_FAMILY_NAME + 1];

    /* The modern name used by version 3.5 files. */
    char     modernName[LS_MAX_FAMILY_NAME + 1];

    /* The legacy name used by version 2.0 files. */
    char     legacyName[LS_MAX_FAMILY_NAME + 1];

    /* The array of pointers to alias numbers. */
    uint32_t **aliasNumber;
    uint32_t   countNumber;

    /* The array of pointers to alias strings. */
    char   **aliasString;
    uint32_t countString;

} LS_ALIAS;

/*
 * The LS_DEVICE structure.
 * This is used to hold a device description from the board config file.
 */
typedef struct LS_DEVICE
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The device name.  */
    char   name[LS_MAX_DEVICE_NAME + 1];

    /* The partner name.  */
    char   partner[LS_MAX_DEVICE_NAME + 1];

    /* The numerical equivalents of the device value.  */
    uint32_t familyId; /* The device family ID value. */
    uint32_t drBits;   /* The device DR register length. */
    uint32_t irBits;   /* The device IR register length. */

    /* The subpath ID of the parent subpath for the device. */
    uint32_t parentId; /* The device's parent subpath ID. */

    /* The range of sub-path ID's owned by this device.  */
    uint32_t subpathId;  /* The first sub-path ID.   */
    uint32_t subpathNum; /* The number of sub-paths. */

    /* The extra device characteristics of the device. */
    uint32_t address;  /* The physical address. */
    uint32_t identify; /* The identity value.   */

} LS_DEVICE;

/*
 * The LS_SUBPATH structure.
 * This is used to hold a sub-path description from the board config file.
 */
typedef struct LS_SUBPATH
{
    /* The version size of this structure. */
    uint32_t versionSize;

    /* The subpath name.  */
    char   name[LS_MAX_SUBPATH_NAME + 1];

    /* The port address used to select the subpath. */
    uint32_t address; /* The sub-path's port address. */

    /* The flags used to address and select the subpaths. */
    uint32_t itsaPseudo;  /* The sub-path's pseudo-address flag. */
    uint32_t itsaDefault; /* The sub-path's default flag. */
    uint32_t itsaCustom;  /* The sub-path's custom flag.  */

    /* The device ID of the parent device for the subpath. */
    uint32_t parentId; /* The subpath's parent device ID. */

    /* The range of device ID's owned by this sub-path.  */
    uint32_t deviceId;  /* The first device ID.   */
    uint32_t deviceNum; /* The number of devices. */

} LS_SUBPATH;

/*******************************************************************************
* Structure declarations for scan-path management.
* These are derived from the description in the configuration file.
*******************************************************************************/

/*
 * The LS_GLOBAL structure.
 * This is used to hold a global description from the board config file.
 */
typedef struct LS_GLOBAL
{
    uint32_t totalTargetNum;    /* The total number of targets.   */
    uint32_t totalRouterNum;    /* The total number of routers.   */
    uint32_t selectDeviceNum;   /* The total number of selected devices.  */
    uint32_t selectSubpathNum;  /* The total number of selected subpaths. */

    /* The default and custom sub-path values. */
    uint32_t subpathDefaultNum;
    uint32_t subpathCustomNum;

    /* This enables/disables no-timeout operations. */
    logic_t  noTimeOut;

    /* The global resource status. */
    uint32_t resourceStatus;

    /* The JTAG config variables. */
    JTAG_Variable jtagVariable;

    /* The router config variables. */
    RTR_Variable rtrVariable;

} LS_GLOBAL;

/*
 * The LS_REVISE structure.
 * This is used to hold revision dependant device characteristics.
 * There may be an sub-array of these for each family of device.
 */
typedef struct LS_REVISE
{
    logic_t  fixUsed;  /* The offset ID in the revise_data for each family. */

    uint32_t fixOffset;  /* The revision offset of the fixed space. */
    uint32_t fixSize;    /* The length of the fixed space.  */
    uint32_t fixAddress; /* The address of the fixed space. */

} LS_REVISE;

/*
 * The LS_USAGE structure.
 * This is used to describe the sub-array of revision dependant
 * device characteristics for specific family of device.
 * There will be just one of these for each family of device.
 */
typedef struct LS_USAGE
{
    uint32_t useBase;  /* The base ID in the revise_data for each family. */
    uint32_t useCount; /* Number of usages of each family. */

} LS_USAGE;

/*
 * The LS_STATIC structure.
 * This is used to hold static device characteristics.
 */
typedef struct LS_STATIC
{
    /* These values describe the device's location on the sub-path. */
    uint32_t preDr; /* Number of DR bits preceeding this device. */
    uint32_t posDr; /* Number of DR bits succeeding this device. */
    uint32_t preIr; /* Number of IR bits preceeding this device. */
    uint32_t posIr; /* Number of IR bits succeeding this device. */

   /*
    * Roland Hoar, 9th August 2005.
    * These four members of LS_STATIC are potentially redundant and
    * could be replaced by the equivalent members of LS_FAMILY.
    */

    /* These values describe the device itself. */
    uint32_t mainDr;  /* The length of the JTAG Bypass Register of the device.      */
    uint32_t mainIr;  /* The length of the JTAG Instruction Register of the device. */

    /* These values describe the device itself. */
    uint32_t onceEnd; /* The length of the end delay parameter after a single scan. */
    uint32_t manyEnd; /* The length of the end delay parameter after a repeat scan. */

} LS_STATIC;

/*
 * The LS_DYNAMIC structure.
 * This is used to hold dynamic device characteristics.
 */
typedef struct LS_DYNAMIC
{
    /* The global status for this device. */
    uint32_t globalStatus;
    /* The resource status for this device. */
    uint32_t resourceStatus;

    uint32_t instBase; /* The base of the global IR value in their array. */
    uint32_t instNum;  /* The number of global IR values. */

    uint32_t pathBase; /* The base of the scan path values in their array. */
    uint32_t pathNum;  /* The number of scan path values. */

    /* The number of default sub-paths for this device. */
    uint32_t subpathDefaultNum;
    /* The number of custom sub-paths for this device. */
    uint32_t subpathCustomNum;

    /* The default usage device flag for this device. */ 
    logic_t  defaultUsageDeviceFlag;
    /* The custom usage device flag for this device. */ 
    logic_t  customUsageDeviceFlag;
    /* The client usage device flag for this device. */ 
    logic_t  clientUsageDeviceFlag;

    /* The selected device count for this device. */ 
    uint32_t selectDeviceCount;

    /* The root of the ordered list of actual sub-paths of router, is sub-path array index. */ 
    uint32_t actualSubpathRoot;
    /* An item in the ordered list of actual devices on sub-path, is device array index. */ 
    uint32_t actualDeviceList;

} LS_DYNAMIC;

/*
 * The LS_CONNECT structure.
 * This is used to hold dynamic sub-path characteristics.
 */
typedef struct LS_CONNECT
{
    /* The isolated sub-path count for this sub-path. */
    uint32_t isolateSubpathCount;
    /* The selected sub-path count for this sub-path. */
    uint32_t selectSubpathCount;

    /* The root of the ordered list of actual devices on sub-path, is device array index. */ 
    uint32_t actualDeviceRoot;
    /* An item in the ordered list of actual sub-paths of router, is sub-path array index. */ 
    uint32_t actualSubpathList;

} LS_CONNECT;

/*
 * The LS_INST typedef.
 * This is a variable length array of
 * subsiduary data for the LS_DYNAMIC structure.
 */
typedef uint16_t LS_INST;

/*
 * The LS_PATH typedef.
 * This is a variable length array of subsiduary
 * data for the LS_DYNAMIC structure.
 */
typedef uint32_t LS_PATH;

/*******************************************************************************
* Declaration for the root structure for both
* board configuration and scan-path management.
*******************************************************************************/

/*
 * The CFG_Inform structure.
 * This is the root structure for both the
 * board configuration and scan-path management.
 */
typedef struct CFG_Inform
{
    /* This structure label distinguishes structure data from memory mapped data. */
    char label[LS_BOARD_STRCT_SIZE];

    /* These values indicate the status of board configure files. */
    uint32_t text_line;  /* The line number for text lines.  */
    uint32_t text_error; /* The error number for text lines. */

    /* These values are the number of error description structures. */
    uint32_t offset_num; /* The number of offset descriptions. */
    uint32_t title_num;  /* The number of title descriptions.  */

    /* These values are the error description structures. */
    LS_OFFSET *offset_data; /* The array of offset descriptions. */
    LS_TITLE  *title_data;  /* The array of title descriptions.  */

    /* These values are the number of board configure structures. */
    uint32_t variable_num; /* The number of variable descriptions. */
    uint32_t package_num;  /* The number of package descriptions.  */
    uint32_t family_num;   /* The number of family descriptions.   */
    uint32_t alias_num;    /* The number of alias descriptions.    */
    uint32_t device_num;   /* The number of device descriptions.   */
    uint32_t subpath_num;  /* The number of subpath descriptions.  */

    /* These values are the board configure structures. */
    LS_VARIABLE *variable_data; /* The array of variable descriptions. */
    LS_PACKAGE  *package_data;  /* The array of package descriptions.  */
    LS_FAMILY   *family_data;   /* The array of family descriptions.   */
    LS_ALIAS    *alias_data;    /* The array of alias descriptions.    */
    LS_DEVICE   *device_data;   /* The array of device descriptions.   */
    LS_SUBPATH  *subpath_data;  /* The array of subpath descriptions.  */

    /* These values are the number of scan-path management structures. */
    uint32_t global_num;  /* The number of global descriptions.  */
    uint32_t revise_num;  /* The number of revise descriptions.  */
    uint32_t usage_num;   /* The number of usage descriptions.   */
    uint32_t inst_num;    /* The number of inst descriptions.    */
    uint32_t path_num;    /* The number of path descriptions.    */
    uint32_t static_num;  /* The number of static descriptions.  */
    uint32_t dynamic_num; /* The number of dynamic descriptions. */
    uint32_t connect_num; /* The number of connect descriptions. */

    /* These values are the scan-path management structures. */
    LS_GLOBAL  *global_data;  /* The array of global descriptions.  */
    LS_REVISE  *revise_data;  /* The array of revise descriptions.  */
    LS_USAGE   *usage_data;   /* The array of usage descriptions.   */
    LS_INST    *inst_data;    /* The array of inst descriptions.    */
    LS_PATH    *path_data;    /* The array of path descriptions.    */
    LS_STATIC  *static_data;  /* The array of static descriptions.  */
    LS_DYNAMIC *dynamic_data; /* The array of dynamic descriptions. */
    LS_CONNECT *connect_data; /* The array of connect descriptions. */

} CFG_Inform;

/*******************************************************************************
* Declare the public functions.
*******************************************************************************/

#if (!defined(USE_DYNAMIC_LIBS) || (defined(XDSBOARD_LIBRARY) && defined(cfg_public_c)))
#if (USE_FILE_SYSTEM)

#include <stdio.h>

extern BOARD_API SC_ERROR
LS_BoardFileType(FILE *textio_hndl, uint32_t *pType);

extern BOARD_API SC_ERROR
LS_Struct2File(CFG_Handle cfgHandle, uint32_t option, uint32_t *pType,
               FILE *textio_hndl, FILE *excess_hndl);

extern BOARD_API SC_ERROR
LS_File2Struct(CFG_Handle cfgHandle, uint32_t option, uint32_t *pType,
               FILE *textio_hndl, FILE *excess_hndl);

#endif /* (USE_FILE_SYSTEM) */

extern BOARD_API SC_ERROR
LS_CreateStruct(CFG_Handle *pCnfgr);

extern BOARD_API SC_ERROR
LS_DeleteStruct(CFG_Handle *pCnfgr);

extern BOARD_API SC_ERROR
LS_CreateMasquerade(CFG_Handle *pMasquerade, logic_t *pItsaMasquerade,
                    CFG_Handle original, uint32_t option);

extern BOARD_API SC_ERROR
LS_DeleteMasquerade(CFG_Handle *pMasquerade, logic_t *pItsaMasquerade);

extern BOARD_API SC_ERROR
LS_Struct2Marshall(CFG_Handle *pCnfgr, uint32_t option,
                   MSH_Handle *pMarsh, uint32_t *pSize);

extern BOARD_API SC_ERROR
LS_Marshall2Struct(CFG_Handle *pCnfgr, uint32_t option,
                   MSH_Handle *pMarsh, uint32_t *pSize);

extern BOARD_API SC_ERROR
LS_DeleteMarshall(MSH_Handle *pMarsh, uint32_t *pSize);

extern BOARD_API SC_ERROR
LS_CreateFamily(CFG_Handle cfgHandle,
                LS_FAMILY *pFamilyData, uint32_t *pFamilyId);

extern BOARD_API SC_ERROR
LS_DeleteFamily(CFG_Handle cfgHandle, char *Name);

extern BOARD_API SC_ERROR
LS_GetFamilyByIndex(CFG_Handle cfgHandle,
                    uint32_t FamilyId, LS_FAMILY *pFamilyData);

extern BOARD_API SC_ERROR
LS_IsFamilyGood(CFG_Handle cfgHandle,
                char *Name, uint32_t length, uint32_t *pFamilyId);

extern BOARD_API SC_ERROR
LS_CreateAlias(CFG_Handle cfgHandle,
               LS_ALIAS *pAliasData, uint32_t *pAliasId);

extern BOARD_API SC_ERROR
LS_DeleteAlias(CFG_Handle cfgHandle, char *Name);

extern BOARD_API SC_ERROR
LS_GetAliasByIndex(CFG_Handle cfgHandle,
                   uint32_t AliasId, LS_ALIAS *pAliasData);

extern BOARD_API SC_ERROR
LS_IsAliasGood(CFG_Handle cfgHandle,
                char *Name, uint32_t length, uint32_t *pAliasId);

extern BOARD_API SC_ERROR
LS_CreateDevice(CFG_Handle cfgHandle,
                char *pDeviceName, char *pFamilyName, char *pPartnerName,
                uint32_t drBits, uint32_t irBits, uint32_t parentSubpath,
                uint32_t subpathFirst, uint32_t subpathNum,
                uint32_t address, uint32_t identify,
                uint32_t *pDeviceId);

extern BOARD_API SC_ERROR
LS_DeleteDevice(CFG_Handle cfgHandle, char *Name);

extern BOARD_API SC_ERROR
LS_GetDeviceByIndex(CFG_Handle cfgHandle, uint32_t DeviceId,
                    char *pDeviceName, char *pFamilyName, char *pPartnerName,
                    uint32_t *pDrBits, uint32_t *pIrBits, uint32_t *pParentId,
                    uint32_t *pSubpathFirst, uint32_t *pSubpathNum,
                    uint32_t *pAddress, uint32_t *pIdentify,
                    uint32_t *pFamilyId);

extern BOARD_API SC_ERROR
LS_IsDeviceGood(CFG_Handle cfgHandle,
                char *pDeviceName, uint32_t length, uint32_t *pDeviceId);

extern BOARD_API SC_ERROR
LS_CreateSubpath(CFG_Handle cfgHandle, char *pSubpathName,
                 uint32_t address, uint32_t itsaPseudo,
                 uint32_t itsaDefault, uint32_t itsaCustom,
                 uint32_t parentId, uint32_t deviceFirst, uint32_t deviceNum,
                 uint32_t *pSubpathId);

extern BOARD_API SC_ERROR
LS_DeleteSubpath(CFG_Handle cfgHandle);

extern BOARD_API SC_ERROR
LS_GetSubpathByIndex(CFG_Handle cfgHandle, uint32_t subpathId, char *pSubpathName,
                     uint32_t *pAddress, uint32_t *pItsaPseudo, 
                     uint32_t *pItsaDefault, uint32_t *pItsaCustom,
                     uint32_t *pParentId, uint32_t *pDeviceFirst, uint32_t *pDeviceNum);

extern BOARD_API SC_ERROR
LS_IsSubpathGood(CFG_Handle cfgHandle,
                 char *pSubpathName, uint32_t length, uint32_t *pSubpathId);

extern BOARD_API SC_ERROR
LS_GetItemNumbers(CFG_Handle cfgHandle,
                  uint32_t *pVariableNum, uint32_t *pPackageNum, uint32_t *pFamilyNum,
                  uint32_t *pAliasNum, uint32_t *pDeviceNum, uint32_t *pSubpathNum);

extern BOARD_API SC_ERROR
LS_PutErrorStatus(CFG_Handle cfgHandle, uint32_t *pLine, uint32_t *pError);

extern BOARD_API SC_ERROR
LS_GetErrorStatus(CFG_Handle cfgHandle, uint32_t *pLine, uint32_t *pError);

extern BOARD_API SC_ERROR
LS_GetTargetLengths(CFG_Handle cfgHandle,
                    uint32_t DeviceId, uint32_t *pLengthArray, uint32_t lengthMax);

extern BOARD_API SC_ERROR
LS_ConvertFamilyName(CFG_Handle cfgHandle,
                     const uint32_t *pNumber, const char *pString,
                     char *LegacyName, char *ModernName, char *FamilyName);

extern BOARD_API SC_ERROR
LS_ConvertVaryName(char *Name, logic_t legacy);

extern BOARD_API SC_ERROR
LS_CreateVariable(CFG_Handle cfgHandle, char *Name, char *Value,
                  uint32_t *pVariableId);

extern BOARD_API SC_ERROR
LS_DeleteVariable(CFG_Handle cfgHandle, char *Name);

extern BOARD_API SC_ERROR
LS_GetVariableByIndex(CFG_Handle cfgHandle, uint32_t VariableId,
                  char **pName, char **pValue);

extern BOARD_API SC_ERROR
LS_IsVariableGood(CFG_Handle cfgHandle,
                  char *Name, uint32_t length, logic_t StandAlone, uint32_t *pVariableId);

extern BOARD_API SC_ERROR
LS_GetConfigValue(CFG_Handle cfgHandle, const char *Name,
                  uint32_t *pVariableId, char **pValue, logic_t *pFound);

extern BOARD_API SC_ERROR
LS_FromValue2String(const char *Value, char *String, uint32_t length);

extern BOARD_API SC_ERROR
LS_FromValue2Logic(const char *Value, logic_t *Logic);

extern BOARD_API SC_ERROR
LS_FromValue2Unsign(const char *Value, uint32_t *Number);

extern BOARD_API SC_ERROR
LS_FromValue2Signed(const char *Value, int32_t *Number);

extern BOARD_API SC_ERROR
LS_FromValue2Frequency(const char *Value, coord_t *Coord);

#endif /* (!defined(USE_DYNAMIC_LIBS) || (defined(XDSBOARD_LIBRARY) && defined(cfg_public_c))) */

/*******************************************************************************
* Declare the public stub functions.
*******************************************************************************/

#if (!defined(USE_DYNAMIC_LIBS))

extern BOARD_API SC_ERROR
CFG_LoadLibrary(void);

extern BOARD_API SC_ERROR
CFG_FreeLibrary(void);

#endif /* (!defined(USE_DYNAMIC_LIBS)) */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* cfg_public_h */

/* -- END OF FILE -- */
