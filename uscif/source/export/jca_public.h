/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jca_public.h
*
* DESCRIPTION
*   The public functions that are the documented JCA API.
*
* AUTHOR
*   mikedunn
*   mikedunn@ti.com
*   281.274.2855
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jca_public_h
#define jca_public_h /* Defines a mnemonic specific to this file. */

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
* Functions located in jca_api_main.c  [2.1.1]
*******************************************************************************/

extern JTI_Error 
JCA_Create(
    JCA_Handle *pAdapterHandle); /* Pointer to comm�s adapter handle. */

extern JTI_Error
JCA_Delete(
    JCA_Handle *pAdapterHandle); /* Pointer to comm�s adapter handle. */

/*
 * The type the client that is using the JTI API�s.
 */
typedef enum
{
    eJTI_CLIENT_INVALID_FIRST = 0, /* The first enumeration is invalid. */

    /* Anonymous client that doesn�t fit the defined types. */
    eJTI_CLIENT_ANONYMOUS,

    /* The JCA is acting as an independent client. */
    eJTI_CLIENT_JCA,
    /* The JSC is acting as an independent client. */
    eJTI_CLIENT_JSC,
    /* The JUC is acting as an independent client. */
    eJTI_CLIENT_JUC,
    /* The JSM is acting as an independent client. */
    eJTI_CLIENT_JSM,

    /* A reset utility (emulator or scan-path) is the external client. */
    eJTI_CLIENT_RESET_UTILITY,
    /* A diagnostic utility (debug or probe) is the external client. */
    eJTI_CLIENT_DEBUG_UTILITY,
    /* A configuration utility (FPGA or CPLD) is the external client. */
    eJTI_CLIENT_CONFIG_UTILITY,

    /* An ARM driver is the external client. */
    eJTI_CLIENT_ARM_DRIVER,
    /* An DSP driver is the external client. */
    eJTI_CLIENT_DSP_DRIVER,
    /* A software only test is the external client */
    eJTI_CLIENT_TEST_SOFTWARE,

    eJTI_CLIENT_INVALID_LAST /* The last enumeration is invalid. */

} JTI_Client;

extern JTI_Error
JCA_Init(
    JCA_Handle adapterHandle,  /* Communications adapter handle. */
    CFG_Handle adapterConfig,  /* Variables to configure adapter. */
    JTI_Client jtiClientType); /* Type of the JTI client using JCA. */

extern JTI_Error
JCA_Quit(
    JCA_Handle adapterHandle); /* Communications adapter handle. */

extern JTI_Error
JCA_Connect(
    JCA_Handle adapterHandle); /* Communications adapter handle. */

extern JTI_Error
JCA_Disconnect(
    JCA_Handle adapterHandle); /* Communications adapter handle. */

/*******************************************************************************
* Functions located in jca_api_semaphore.c  [2.1.2]
*******************************************************************************/

extern JTI_Error
JCA_AcquireSemaphore(
    JCA_Handle adapterHandle); /* Communication adapter handle. */

extern JTI_Error
JCA_ReleaseSemaphore(
    JCA_Handle adapterHandle); /* Communication adapter handle. */

extern JTI_Error
JCA_QuerySemaphore(
    JCA_Handle adapterHandle,  /* Communication adapter handle. */
    uint32_t  *pClientNumber); /* Number of JCA clients connected. */

/*******************************************************************************
* Functions located in jca_api_register.c  [2.1.3]
*******************************************************************************/

extern JTI_Error
JCA_SingleRegisterRead(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t  *pAccessData);  /* The pointer to the register value. */

extern JTI_Error
JCA_SingleRegisterWrite(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t   accessData);   /* The register value. */

extern JTI_Error
JCA_MultipleRegisterRead(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t  *pAccessAddr,   /* Array of register addresses. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData);    /* Number of addresses and values. */

extern JTI_Error
JCA_MultipleRegisterWrite(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t  *pAccessAddr,   /* Array of register addresses. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData);    /* Number of addresses and values. */

extern JTI_Error
JCA_ArrayRegisterRead(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData);    /* Number of register values. */

extern JTI_Error
JCA_ArrayRegisterWrite(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessAddr,    /* The register address. */
    uint32_t  *pAccessData,   /* Array of register values. */
    uint32_t   countData);    /* Number of register values. */ 

/*******************************************************************************
* Functions located in jca_api_io.c  [2.1.4]
*******************************************************************************/

typedef enum
{
    eJCA_XFER_TYPE_INVALID_FIRST = 0, /* The first enumeration is invalid. */

    /* Send fixed. Receive data ignored. */
    eJCA_XFER_TYPE_SEND_ONLY_FIXED,
    /* Send fixed. Receive data into read buffer #1. */
    eJCA_XFER_TYPE_SEND_RECV_FIXED,

    /* Send data from write buffer #1. Receive data ignored. */
    eJCA_XFER_TYPE_SEND_ONLY_BUFFER,
    /* Send data from write buffer #1. Receive data into read buffer #1. */
    eJCA_XFER_TYPE_SEND_RECV_BUFFER,

    /* Send data from write buffer #1. Receive data polled. */
    eJCA_XFER_TYPE_SEND_RECV_LONG_POLL,
    /* Send data from write buffer #1. Receive data polled. */
    eJCA_XFER_TYPE_SEND_RECV_SHORT_POLL,

    /* Send data from write buffer #1. Receive data ignored. */
    eJCA_XFER_TYPE_SEND_ONLY_MIXED,
    /* Send data from write buffer #1. Receive data ignored. */
    eJCA_XFER_TYPE_SEND_ONLY_LIST,

    eJCA_XFER_TYPE_INVALID_LAST /* The last enumeration is invalid. */

} JCA_XferType;

extern JTI_Error
JCA_BufferDataTransfer(
    JCA_Handle   adapterHandle, /* Communications adapter handle. */
    JCA_XferType accessType,    /* The type of data transfer. */
    uint32_t     bitCount);     /* The number of bits. */

extern JTI_Error
JCA_BufferDataRead08(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint8_t   *pAccessData,   /* Array of buffer values. */
    uint32_t   countData);    /* Number of buffer values. */

extern JTI_Error
JCA_BufferDataWrite08(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint8_t   *pAccessData,   /* Array of buffer values. */
    uint32_t   countData);    /* Number of buffer values. */

extern JTI_Error
JCA_BufferDataRead16(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint16_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData);    /* Number of buffer values. */

extern JTI_Error
JCA_BufferDataWrite16(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint16_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData);    /* Number of buffer values. */

extern JTI_Error
JCA_BufferDataRead32(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint32_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData);    /* Number of buffer values. */

extern JTI_Error
JCA_BufferDataWrite32(
    JCA_Handle adapterHandle, /* Communications adapter handle. */
    uint32_t   accessSelect,  /* Select the buffer. */
    uint32_t  *pAccessData,   /* Array of buffer values. */
    uint32_t   countData);    /* Number of buffer values. */

/*******************************************************************************
* Functions located in jca_api_poll.c  [2.1.5]
*******************************************************************************/

typedef enum
{
    eJCA_STATUS_TYPE_INVALID_FIRST = 0, /* The first enumeration is invalid. */

    eJCA_STATUS_TYPE_HARMLESS,     /* A harmless action for debug. */

    eJCA_STATUS_TYPE_CABLE_VALID,  /* Wait for cable status is valid. */

    eJCA_STATUS_TYPE_WR_BFR_ONE,   /* Wait for write buffer #1  has >=1 spaces. */
    eJCA_STATUS_TYPE_WR_BFR_MANY,  /* Wait for write buffer #1  has >=N spaces. */
    eJCA_STATUS_TYPE_RD_BFR1_ONE,  /* Wait for read buffer #1 has >=1 words. */
    eJCA_STATUS_TYPE_RD_BFR1_MANY, /* Wait for read buffer #1 has >=N words. */
    eJCA_STATUS_TYPE_RD_BFR2_ONE,  /* Wait for read buffer #2 has >=1 words. */
    eJCA_STATUS_TYPE_RD_BFR2_MANY, /* Wait for read buffer #2 has >=N words. */

    eJCA_STATUS_TYPE_COMMAND_START,       /* Wait for scan command start. */
    eJCA_STATUS_TYPE_COMMAND_FINISH,      /* Wait for scan command finish. */
    eJCA_STATUS_TYPE_FREQ_PROGRAM_START,  /* Wait for TCLKO programming start. */
    eJCA_STATUS_TYPE_FREQ_PROGRAM_FINISH, /* Wait for TCLKO programming finish. */
    eJCA_STATUS_TYPE_FREQ_MEASURE_START,  /* Wait for TCLKR measuring start. */
    eJCA_STATUS_TYPE_FREQ_MEASURE_FINISH, /* Wait for TCLKR measuring finish. */

    eJCA_STATUS_TYPE_INVALID_LAST /* The last enumeration is invalid. */

} JCA_StatusType;

#define JCA_STATUS_TYPE_UNUSED (uint32_t)-1

extern JTI_Error
JCA_StatusPoll(
    JCA_Handle     adapterHandle, /* Communication adapter handle. */
    JCA_StatusType statusType,    /* The status being polled. */
    uint32_t       testValue);    /* The value being tested. */

/*******************************************************************************
* Functions located in jca_api_flag.c  [2.1.6]
*******************************************************************************/

typedef enum
{
    eJCA_ACTION_TYPE_INVALID_FIRST = 0, /* The first enum' is invalid. */

    eJCA_ACTION_TYPE_HARMLESS, /* A harmless action for debug. */

    eJCA_ACTION_TYPE_QUEUE_START,  /* Start queuing commands. */
    eJCA_ACTION_TYPE_QUEUE_FINISH, /* Finish queuing commands. */
    eJCA_ACTION_TYPE_QUEUE_APPLY,  /* Apply all queued commands. */
    eJCA_ACTION_TYPE_QUEUE_ERASE,  /* Erase all queued commands. */

    eJCA_ACTION_TYPE_INTERRUPT_ENABLE,  /* Start interrupt protected code.*/
    eJCA_ACTION_TYPE_INTERRUPT_DISABLE, /* Finish interrupt protected code. */

    eJCA_ACTION_TYPE_COMMAND_START,  /* Start a command. */
    eJCA_ACTION_TYPE_COMMAND_FINISH, /* Finish a command. */

    eJCA_ACTION_TYPE_FREQ_PROGRAM_START,  /* Start TCLKO programming. */
    eJCA_ACTION_TYPE_FREQ_PROGRAM_FINISH, /* Finish TCLKO programming. */
    eJCA_ACTION_TYPE_FREQ_MEASURE_START,  /* Start TCLKR measurement. */
    eJCA_ACTION_TYPE_FREQ_MEASURE_FINISH, /* Finish TCLKR measurement. */

    eJCA_ACTION_TYPE_INVALID_LAST /* The last enum' is invalid. */

} JCA_ActionType;

extern JTI_Error
JCA_WriteFlag(
    JCA_Handle     adapterHandle, /* Communication adapter handle. */
    JCA_ActionType actionType);   /* The type of action. */

/*******************************************************************************
* Functions located in jca_api_error.c  [2.1.6]
*******************************************************************************/

extern JTI_Error
JCA_ReadError(
    JCA_Handle  adapterHandle, /* Communications adapter handle. */
    JTI_Error  *pErrorReturn); /* Returned error value. */

/*******************************************************************************
* Functions located in jca_api_timeout.c [2.1.7]
*******************************************************************************/

extern JTI_Error
JCA_WaitForTime(
    JCA_Handle  adapterHandle, /* Communications adapter handle. */
    uint32_t    timeDelay);    /* A delay measured in micro-seconds. */

extern JTI_Error
JCA_WaitForTclk(
    JCA_Handle  adapterHandle, /* Communications adapter handle. */
    uint32_t    periodDelay);  /* A delay measured in TCLK periods. */

typedef enum
{
    eJCA_TIMEOUT_INVALID_FIRST = 0, /* The first enum' is invalid. */

    eJCA_TIMEOUT_ZERO,  /* The timeout occurs if first test fails. */
    eJCA_TIMEOUT_TINY,  /* The timeout is 1/256 second. */
    eJCA_TIMEOUT_SHORT, /* The timeout is 1/16 second. */
    eJCA_TIMEOUT_LONG,  /* The timeout is 1 second. */
    eJCA_TIMEOUT_NEVER, /* The timeout never occurs. */

    eJCA_TIMEOUT_INVALID_LAST /* The last enum' is invalid. */

} JCA_Timeout;

extern JTI_Error
JCA_TimeOut(
    JCA_Handle  adapterHandle, /* Communication adapter handle. */
    JCA_Timeout dataTransfer,  /* Select the data transfer timeout. */
    JCA_Timeout statusPoll);   /* Select the status poll timeout. */

/*******************************************************************************
* Functions located in jca_api_program.c [2.1.8]
*******************************************************************************/

typedef enum
{
    eJCA_PROGRAM_TYPE_INVALID_FIRST = 0, /* The first enumeration is invalid. */

    eJCA_PROGRAM_TYPE_EMULATOR, /* Program the emulator FPGA/CPLD. */
    eJCA_PROGRAM_TYPE_CLIENT,   /* Program the client FPGA/CPLD. */
    eJCA_PROGRAM_TYPE_SERVER,   /* Program the server FPGA/CPLD. */
    eJCA_PROGRAM_TYPE_POD,      /* Program the pod FPGA/CPLD. */

    eJCA_PROGRAM_TYPE_INVALID_LAST /* The last enumeration is invalid. */

} JCA_ProgramType;

extern JTI_Error
JCA_ProgramStart(
    JCA_Handle      adapterHandle,     /* Communications adapter handle. */
    JCA_ProgramType hardwareType);     /* The FPGA/CPLD type. */

extern JTI_Error
JCA_ProgramTransfer(
    JCA_Handle      adapterHandle,    /* Communications adapter handle. */
    JCA_ProgramType hardwareType,     /* The FPGA/CPLD type. */
    unsigned char  *pProgramData,     /* The FPGA/CPLD programming data. */
    uint32_t        programNumber,    /* The number of data values. */
    uint32_t       *pProgramStatus);  /* The result status. */

extern JTI_Error
JCA_ProgramFinish(
    JCA_Handle      adapterHandle,    /* Communications adapter handle. */
    JCA_ProgramType hardwareType,     /* The FPGA/CPLD type. */
    uint32_t       *pProgramStatus);  /* The result status. */

extern JTI_Error
JCA_ProgramVersion(
    JCA_Handle      adapterHandle,     /* Communications adapter handle. */
    JCA_ProgramType hardwareType,      /* The FPGA/CPLD type. */
    uint32_t       *pHardwareVersion); /* The version status. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef __cplusplus
};
#endif

#endif /* jca_public_h */

/* -- END OF FILE -- */
