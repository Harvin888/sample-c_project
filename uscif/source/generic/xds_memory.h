/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_memory.h
*
* DESCRIPTION
*   A set of memory allocation functions.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xds_memory_h
#define xds_memory_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern void *
fast_malloc(uint32_t size);
extern void *
slow_malloc(uint32_t size);

extern void *
fast_realloc(void *pMemory, uint32_t oldSize, uint32_t newSize);
extern void *
slow_realloc(void *pMemory, uint32_t oldSize, uint32_t newSize);

extern logic_t
string_malloc(char **ppString, uint32_t *pSize, char *pText);
extern void
uscif_free(void **ppMemory, uint32_t *pSize);

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* xds_memory_h */

/* -- END OF FILE -- */
