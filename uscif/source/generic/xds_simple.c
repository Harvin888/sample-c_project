/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_simple.c
*
* DESCRIPTION
*   A set of simple character string and numeric functions.
*
* PUBLIC FUNCTIONS
*
*   unsign2true()  - Convert an unsigned number to true logic.
*   unsign2false() - Convert an unsigned number to false logic.
*
*   strncpy_safe() - Do a string copy immune to bufffer overrun.
*   strncat_safe() - Do a string concatenate immune to bufffer overrun.
*
*   str_2_upper() - Convert a string to upper-case.
*   str_2_lower() - Convert a string to lower-case.
*
*   strcmp_nocase()  - Do a string comparison that is case insensitive.
*   strncmp_nocase() - Do a string comparison that is case insensitive.
*
*   arrcmp_nocase()  - Do an array comparison that is case insensitive.
*   arrncmp_nocase() - Do an array comparison that is case insensitive.
*
*   format_unsign() - Convert a 'uint32_t' to a fixed text string.
*   format_signed() - Convert an 'int32_t' to a signed fixed text string.
*
*   field_insert()  - Get a field from a list and insert it into a buffer.
*   field_extract() - Extract a field from a buffer and put it into a list.
*
*   illegal_identifier() - Test if it is an illegal identifier.
*
*   convert_to_text() - Convert an error number to a string.
*
* NOTES
*   These are *very simple* functions:
*
*   1) No USCIF typedef's are involved.
*   2) No file access is involved.
*   3) No memory allocation is involved.
*   4) No dependancy on the build environment.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define xds_simple_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The legacy header file.
 */
#include "xdsfast3.h"

/*
 * The other header files.
 */
#include "xds_simple.h"

/*******************************************************************************
* NAME: unsign2true()
*
* DESCRIPTION
*   Convert an unsigned number to true logic.
*
* NOTES
*   This function provides a simple method of generating
*   an honest value of type logic_t while avoiding
*   warnings from Microsoft and TI compiler tools.
*
*   The parameter 'number' can be almost any sort
*   of bit-wise, logical or arithmetic value.
*
*******************************************************************************/

logic_t
unsign2true(uint32_t number)
{
    if (0 != number)
    {
        return TRUE;
    }

    return FALSE;
}

/*******************************************************************************
* NAME: unsign2false()
*
* DESCRIPTION
*   Convert an unsigned number to false logic.
*
* NOTES
*   This function provides a simple method of generating
*   an honest value of type logic_t while avoiding
*   warnings from Microsoft and TI compiler tools.
*
*   The parameter 'number' can be almost any sort
*   of bit-wise, logical or arithmetic value.
*
*******************************************************************************/

logic_t
unsign2false(uint32_t number)
{
    if (0 == number)
    {
        return TRUE;
    }

    return FALSE;
}

/*******************************************************************************
* NAME: strncpy_safe()
*
* DESCRIPTION
*   Do a string copy immune to bufffer overrun.
*
* NOTES
*   The parameter 'size' represents the total capacity of the dest' array.
*
*******************************************************************************/

logic_t
strncpy_safe(char *dest, const char *source, uint32_t size)
{
    /* Check the parameters. */
    if ((NULL == source) || (NULL == dest) | (0 == size))
    {
        return TRUE;
    }

    /* Erase the last character. */
    dest[size - 1] = '\0';

    /* Only now copy the data. */
    strncpy(dest, source, size);

    /* Fail if the last character over-written. */
    if ('\0' == dest[size - 1])
    {
        return TRUE;
    }

    return FALSE;
}


/*******************************************************************************
* NAME: strncat_safe()
*
* DESCRIPTION
*   Do a string concatenate immune to bufffer overrun.
*
* NOTES
*   The parameter 'size' represents the total capacity of the dest' array.
*
*******************************************************************************/

logic_t
strncat_safe(char *dest, const char *source, uint32_t size)
{
    uint32_t length;

    /* Check the parameters. */
    if ((NULL == source) || (NULL == dest) | (0 == size))
    {
        return TRUE;
    }

    /* The destination cannot be full. */
    length = strlen(dest);
    if (length + 1 >= size)
    {
        return TRUE;
    }

    /* Erase the last character. */
    dest[size - 1] = '\0';

    /* Only now copy the data. */
    strncpy(dest + length, source, size - length);

    /* Fail if the last character over-written. */
    if ('\0' == dest[size - 1])
    {
        return TRUE;
    }

    return FALSE;
}

/*******************************************************************************
* NAME: str_2_upper()
*
* DESCRIPTION
*   Convert a string to upper-case.
*
* NOTES
*
*******************************************************************************/

char *
str_2_upper(char *string)
{
    char c, *p;

    for (p = string; '\0' != (c = *p); p++)
    {
        *p = (char)toupper(c);
    }

    return string;
}

/*******************************************************************************
* NAME: str_2_lower()
*
* DESCRIPTION
*   Convert a string to lower-case.
*
* NOTES
*
*******************************************************************************/

char *
str_2_lower(char *string)
{
    char c, *p;

    for (p = string; '\0' != (c = *p); p++) 
    {
        *p = (char)tolower(c);
    }

    return string;
}

/*******************************************************************************
* NAME: strcmp_nocase()
*
* DESCRIPTION
*   Do a string comparison that is case insensitive.
*   The comparison is terminated by null characters.
*
* NOTES
*   We return an integer value +1, 0, -1 to indicate the final comparison
*   as is done by ANSI 'C' strcmp() for use in binary searches.
*
*******************************************************************************/

int
strcmp_nocase(const char *string1, const char *string2)
{
    int result;

    if ((NULL == string1) || (NULL == string2))
    {
        return 1;
    }

    while (0 == (result = toupper(*string1) - toupper(*string2)))
    {
        if ((*string1 == '\0') && (*string2 == '\0'))
        {
            return 0;
        }
        string1++; string2++;
    }

    if (result > 0)
    {
        result = 1;
    }
    if (result < 0)
    {
        result = -1;
    }

    return result;
}

/*******************************************************************************
* NAME: strncmp_nocase()
*
* DESCRIPTION
*   Do a string comparison that is case insensitive.
*   The comparison is terminated by null characters or the count of characters.
*
* NOTES
*   We return an integer value +1, 0, -1 to indicate the final comparison
*   as is done by ANSI 'C' strcmp() for use in binary searches.
*
*******************************************************************************/

int
strncmp_nocase(const char *string1, const char *string2, uint32_t number)
{
    int result;

    if ((NULL == string1) || (NULL == string2))
    {
        return 1;
    }

    while (0 == (result = toupper(*string1) - toupper(*string2)))
    {
        if (--number == 0)
        {
            return 0;
        }
        if ((*string1 == '\0') && (*string2 == '\0'))
        {
            return 0;
        }
        string1++; string2++;
    }

    if (result > 0)
    {
        result = 1;
    }
    if (result < 0)
    {
        result = -1;
    }

    return result;
}

/*******************************************************************************
* NAME: arrcmp_nocase()
*
* DESCRIPTION
*   Do an array comparison that is case insensitive.
*   The comparison is terminated by null strings.
*
*   The last element of the array must be a NULL pointer so
*   that if no match occurs then the NULL value is returned.
*
* NOTES
*   Because a full match is always required
*   the parameter 'string' is not updated.
*
*******************************************************************************/

const char *
arrcmp_nocase(char *string, const char *array[], uint32_t *index)
{
    uint32_t i;

    if ((NULL == string) || (NULL == array))
    {
        return NULL;
    }

    for (i = 0; NULL != array[i]; i++)
    {
        if (!strcmp_nocase(string, array[i]))
        {
            break;
        }
    }

    if (NULL != index)
    {
        *index = i;
    }

    return array[i];
}

/*******************************************************************************
* NAME: arrncmp_nocase()
*
* DESCRIPTION
*   Do a string comparison that is case insensitive.
*   The comparison is terminated by null strings or the count of characters.
*
*   The last element of the array must be a NULL pointer so
*   that if no match occurs then the NULL value is returned.
*
* NOTES
*   Because only a partial match is required the parameter 'string'
*   is always updated with the match in the reference 'array'.
*
*******************************************************************************/

const char *
arrncmp_nocase(char **string, const char *array[], uint32_t *index, uint32_t number)
{
    uint32_t i;

    if ((NULL == string) || (NULL == *string) || (NULL == array))
    {
        return NULL;
    }

    for (i = 0; NULL != array[i]; i++)
    {
        if (!strncmp_nocase(*string, array[i], number))
        {
            *string = (char*)array[i];
            break;
        }
    }

    if (NULL != index)
    {
        *index = i;
    }

    return array[i];
}


/*******************************************************************************
* NAME: format_unsign()
*
* DESCRIPTION
*   Convert a 'uint32_t' to a fixed text string.
* 
* NOTES
*   The text string is stored in a small static array.
*
*******************************************************************************/

char *
format_unsign(uint32_t number)
{
    static char text[16 + 1] = ""; 

    char *format;
    
    if (number < 256)
    {
        format = "%u";
    }
    else if (number < 256)
    {
        format = "0x%02X";
    }
    else if (number < 256*256)
    {
        format = "0x%04X";
    }
    else if (number < 256*256*256)
    {
        format = "0x%06X";
    }
    else
    {
        format = "0x%08X";
    }

    sprintf(text, format, number);

    return text;
}

/*******************************************************************************
* NAME: format_signed()
*
* DESCRIPTION
*   Convert an 'int32_t' to a signed fixed text string.
* 
* NOTES
*   The text string is stored in a small static array.
*
*******************************************************************************/

char *
format_signed(int32_t number)
{
    static char text[16 + 1] = ""; 

    char *format;

    logic_t itsNegative = unsign2true(number < 0);

    if (itsNegative)
    {
        format = "%d";
    }
    else
    {  
        if (number < 256)
        {
            format = "%u";
        }
        else if (number < 256)
        {
            format = "0x%02X";
        }
        else if (number < 256*256)
        {
            format = "0x%04X";
        }
        else if (number < 256*256*256)
        {
            format = "0x%06X";
        }
        else
        {
            format = "0x%08X";
        }
    }

    sprintf(text, format, number);

    return text;
}

/*******************************************************************************
* NAME: field_insert()
*
* DESCRIPTION
*   Get a field from a 32-bit wide list and insert it into a 16-bit wide buffer.
*
* NOTES
*   The field's size in bits within the list are provided.
*   The field's base bit-address within the buffer are provided.
*
*   Unused bits in partially used words of the buffer will be unchanged.
*
*******************************************************************************/

void
field_insert(uint32_t *List, uint32_t Size, uint32_t Base, uint16_t *Buffer)
{
    uint32_t Value;  /* Current value.           */
    uint32_t Length; /* Length of current value. */

    uint32_t ListBit = 0; /* Bit offset into the list.  */
    uint32_t ListWrd = 0; /* Word offset into the list. */

    uint32_t BufferBit = Base & 0xF; /* Bit offset into the buffer.  */
    uint32_t BufferWrd = Base >> 4;  /* Word offset into the buffer. */

   /*
    * With each pass through this loop we insert up-to 16-bits
    * in a single word of the buffer, using a value extracted
    * from either one or two words of the list.
    */
    while (Size)
    {
       /*
        * Determine the length of the value to extract
        * from the list and to insert into the buffer.
        */
        Length = 16 - BufferBit;
        if (Length > Size)
        {
            Length = Size;
        }
        
       /*
        * Fetch some or all of the value from
        * the current word in the list.
        */
        Value = (List[ListWrd] >> ListBit) & ~(~0 << Length);
        ListBit += Length;

       /*
        * Decide if we need to fetch some the
        * value from the next word in the list.
        */
        if (ListBit > 32)
        {
            uint32_t Missing = ListBit - 32;
            uint32_t Already = 32 - (ListBit - Length);

            ListBit = Missing;
            ListWrd++;

            Value |= (List[ListWrd] & ~(~0 << Missing)) << Already;
        }
        /* Decide if we are on the border-line. */
        else if (ListBit == 32)
        {
            ListBit = 0;
            ListWrd++;
        }

        /* Insert the value into the buffer. */
        {
            uint16_t Temp = Buffer[BufferWrd];

            Temp &= ~(~(~0 << Length) << BufferBit);
            Temp |=   Value << BufferBit;

            Buffer[BufferWrd] = Temp;
        }

        /* The current word in the buffer is now completed. */
        BufferBit = 0;
        BufferWrd++;

        /* There's now less of the field to extract and insert. */
        Size -= Length;
    }

    return;
}

/*******************************************************************************
* NAME: field_extract()
*
* DESCRIPTION
*   Extract a field from a 16-bit wide buffer and put it into a 32-bit wide list.
*
* NOTES
*   The field's size in bits within the list are provided.
*   The field's base bit-address within the Buffer are provided.
*
*   Unused bits in partially used words of the list will be zeroed.
*
*******************************************************************************/

void
field_extract(uint32_t *List, uint32_t Size, uint32_t Base, uint16_t *Buffer)
{
    uint32_t Value;  /* Current value.           */
    uint32_t Length; /* Length of current value. */

    uint32_t ListBit = 0; /* Bit offset into the list.  */
    uint32_t ListWrd = 0; /* Word offset into the list. */

    uint32_t BufferBit = Base & 0xF; /* Bit offset into the buffer.  */
    uint32_t BufferWrd = Base >> 4;  /* Word offset into the buffer. */

    List[ListWrd] = 0;

   /*
    * With each pass through this loop we extract up-to 16-bits 
    * from a single word of the buffer, then insert the
    * value into either one or two words of the list.
    */
    while (Size)
    {
       /*
        * Determine the length of the value to extract
        * from the buffer and to insert into the list.
        */
        Length = 16 - BufferBit;
        if (Length > Size)
            Length = Size;
    
        /* Fetch the value from the buffer. */
        Value = (Buffer[BufferWrd] >> BufferBit) & ~(~0 << Length);

        /* Insert the value into the list */
        {
            uint32_t Temp = List[ListWrd];

            Temp &= ~(~(~0 << Length) << ListBit);
            Temp |=  Value << ListBit;

            List[ListWrd] = Temp;
        }
        ListBit += Length;

       /*
        * Decide if we need to insert some the
        * value into the next word in the list.
        */
        if (ListBit > 32)
        {
            uint32_t Missing = ListBit - 32;
            uint32_t Already = 32 - (ListBit - Length);

            ListBit = Missing;
            ListWrd++;

            List[ListWrd] = (Value >> Already) & ~(~0 << Missing);
        }
        /* Decide if we are on the border-line. */
        else if (ListBit == 32)
        {
            ListBit = 0;
            ListWrd++;
        }

        /* The current word in the buffer is now completed. */
        BufferBit = 0;
        BufferWrd++;

        /* There's now less of the field to extract and insert. */
        Size -= Length;
    }

    return;
}

/*******************************************************************************
* NAME: legal_identifier()
*
* DESCRIPTION
*   Test if it is an illegal identifier.
*
* NOTES
*   Apply identifier rules similar to 'C' language.
*   Check the length is acceptable, plus the first character is
*   alphabetic and the later ones are alphanumeric or underscore.
*
*   If 'max' is -1 then be more flexible to allow several characters
*   used by numbers and typical filenames. There is no length limit,
*   plus the first character is alphanumeric and later characters
*   can be alphanumeric, underscore, hyphen, decimal point,
*   forward or backward slash.
*
*******************************************************************************/

uint8_t
illegal_identifier(char *name, uint32_t length, uint32_t max)
{
    uint32_t i;

    if (0 == length)
    {
        return 1;
    }
    if (max < length)
    {
        return 2;
    }

    for (i = 0; i < length; i++)
    {
        char    byte  = name[i];
        logic_t relax = unsign2true((uint32_t)-1 == max);
        logic_t first = unsign2false(i);

        if (relax)
        /* Relaxed rules. */
        {
            if (first)
            {
                if (!isalnum(byte))
                {
                    return 3;
                }
            }
            else if (!isalnum(byte))
            {
                /* Permit numbers and typical filenames. */
                char *ok = "_-:./\\";

                if (NULL == strchr(ok, byte))
                {
                    return 4;
                }
            }
        }
        else
        /* 'C' language identifier rules. */
        {
            if (first)
            {
                if (!isalpha(byte))
                {
                    return 3;
                }
            }
            else if (!isalnum(byte))
            {
                /* Permit only the under-score and hyphen. */
                char *ok = "_"; 

                if (NULL == strchr(ok, byte))
                {
                    return 4;
                }
            }
        }
    }

    return 0;
}

/*******************************************************************************
* NAME: convert_to_text()
*
* DESCRIPTION
*   Convert an error number to a string.  
*
* NOTES
*
*******************************************************************************/

char *
convert_to_text(int32_t number, uint32_t base)
{
   /*
    * Allow for corrupted 32 bit error numbers thus a
    * minus sign + 10 decimal digits + null character.
    */
    static char textArray[1 + 10 + 1];

    if (SMG_BASE10 == base)
    {
        if (number > 0)
        {
             sprintf(textArray, "+%d",  number);
        }
        if (number < 0)
        {
             sprintf(textArray, "%d", number);
        }
        if (number == 0)
        {
             sprintf(textArray, "0");
        }
    }
    else if (SMG_BASE16 == base)
    {
        sprintf(textArray, "0x%04x%04x",
            0xFFFF & (number >> 16), 0xFFFF & number);
    }
    else
    {
        textArray[0] = '\0';
    }

    return textArray;
}

/* -- END OF FILE -- */
