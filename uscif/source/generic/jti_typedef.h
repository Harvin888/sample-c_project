/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jti_typedef.h
*
* DESCRIPTION
*   The definitions for the standard JTI types and its build environments.
*
* NOTES
*   This header file does NOT require any definitions to preceed it.
*
*   This header file should be included in all JTI
*   source files immediately after "jti_typedef.h".
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jti_typedef_h
#define jti_typedef_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The Unified-SCIF private MS_WIN32, DSP_BIOS, and POSIX 
* macros that describe the build environment are derived 
* from pre-defined macro names supported by the build tools.
*******************************************************************************/

#if !defined(_WIN32) && !defined(_TMS320C6X) && !defined(_POSIX)
#error The _WIN32/_TMS320C6X/_POSIX preprocessor macros are undefined.
#endif

#undef  MS_WIN32
#ifdef _WIN32 /* The MS Visual C++ tools maintain this macro. */
#define MS_WIN32 1
#else
#define MS_WIN32 0
#endif

#undef  DSP_BIOS
#ifdef _TMS320C6X /* The TI C6x Code-Gen tools maintain this macro. */
#define DSP_BIOS 1
#else
#define DSP_BIOS 0
#endif

#undef  POSIX
#ifdef _POSIX /* Set in makefile when building on POSIX compliant UNIX */
#define POSIX 1
#else
#define POSIX 0
#endif

/*******************************************************************************
* The JTI generic declarations.
*******************************************************************************/

#if (MS_WIN32) || (DSP_BIOS)

/*
 * The integer types with exactly the specified number of bits.
 */
typedef signed char int8_t;
typedef unsigned char uint8_t;

typedef short int16_t;
typedef unsigned short uint16_t;

typedef int int32_t;
typedef unsigned uint32_t;

/*
 * The integer types with at least the specified number of bits.
 */
typedef signed char int_least8_t;
typedef unsigned char uint_least8_t;

typedef short int_least16_t;
typedef unsigned short uint_least16_t;

typedef int int_least32_t;
typedef unsigned uint_least32_t;

#endif /* (MS_WIN32) || (DSP_BIOS) */

#if (POSIX)

#include <inttypes.h>

#endif /* (POSIX) */

/*
 * The logic type.
 */
typedef uint32_t logic_t;
#undef  FALSE
#define FALSE  ((logic_t)0)
#undef  TRUE
#define TRUE   ((logic_t)1)

/*******************************************************************************
* The JTI specific declarations.
*******************************************************************************/

/*
 * The integer types for PLL programming and measuring frequency
 */
typedef uint32_t  hertz_t; /* A frequency value in literal Hertz. */
typedef uint32_t  coord_t; /* A frequency value in a coordinate pair. */
typedef  int8_t y_coord_t; /* A frequency value's y coordinate. */
typedef uint8_t z_coord_t; /* A frequency value's z coordinate. */

/*
 * The integer type for measuring time.
 */
typedef uint32_t wallclock_t;

/*
 * The types specifically for the JTI errors.
 */
typedef int32_t  JTI_Error;   /* The error value. */
typedef logic_t  JTI_Failure; /* The fail flag. */

/*
 * The types specifically for the scan-control functions.
 */
typedef uint32_t JSC_OPCODE; /* The command opcode. */
typedef uint32_t JSC_MODIFY; /* The command modifier. */

/*
 * The opaque types for library handles.
 */
#define JTI_INVALID NULL

typedef void *JSM_Handle;    /* The scan manager handle. */
typedef void *JSC_Handle;    /* The scan controller handle. */
typedef void *JUC_Handle;    /* The utility controller handle. */
typedef void *JCA_Handle;    /* The communication adapter handle. */
typedef void *JIO_Handle;    /* The input output handle. */

typedef void *JBD_Handle;    /* The board description handle. */
typedef void *JEI_Handle;    /* The error description handle. */
typedef void *JRM_Handle;    /* The route manager handle. */
typedef void *JRC_Handle;    /* The route controller handle. */

typedef void *JSP_Semaphore; /* The semaphore handle. */
typedef void *JSP_Sharedmem; /* The shared memory handle. */

/*
 * The opaque types for legacy library handles.
 */
typedef void *CFG_Handle; /* A Unified-SCIF board configuration handle. */
typedef void *MSH_Handle; /* A Unified-SCIF marshaled data handle. */

typedef void *TXT_Handle; /* A Unified-SCIF text i/o handle. */
typedef void *PRS_Handle; /* A Unified-SCIF parser handle. */

typedef void *RTR_Handle; /* A Unified-SCIF route manager handle. */
typedef void *ICE_Handle; /* A Unified-SCIF route controller handle. */

typedef void *SMG_Handle;  /* A Unified-SCIF manager handle.    */
typedef void *TBC_Handle;  /* A Unified-SCIF controller handle. */
typedef void *POD_Handle;  /* A Unified-SCIF adapter handle.    */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* jti_typedef_h */

/* -- END OF FILE -- */
