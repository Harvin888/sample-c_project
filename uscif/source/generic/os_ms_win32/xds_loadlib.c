/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_loadlib.c            
*
* DESCRIPTION
*   OS independent dynamic library access. 
*   This version supports MS_WIN32 builds.
*
* INTERFACE FUNCTIONS
*
*   xds_LoadLibrary()    - Load a dynamic library.
*   xds_FreeLibrary()    - Free a dynamic library.
*   xds_GetProcAddress() - Get a function pointer from a dynamic library.
*
* AUTHOR
*   Edward Fewell
*   efewell@ti.com
*   281.274.3922
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define xds_loadlib_c

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "xds_loadlib.h"

/*!*****************************************************************************
 * Function Name: xds_LoadLibrary()
 *
 *   Description: Load a dynamic library.
 *
 *    Parameters: pLibrary      Text name of dynamic library to open.
 * 
 *  Return Value: Handle to library module if no errors, otherwise returns NULL.
 *
 ******************************************************************************/

LIB_HANDLE
xds_LoadLibrary(char *pLibrary)
{
    return LoadLibrary(pLibrary);
}

/*!*****************************************************************************
 * Function Name: xds_FreeLibrary()
 *
 *   Description: Free a dynamic library.
 *
 *    Parameters: library      Handle to dynamic library module.
 * 
 *  Return Value: TRUE if failure, FALSE if success.
 *
 ******************************************************************************/

logic_t
xds_FreeLibrary(LIB_HANDLE library)
{
    BOOL result = FreeLibrary((HMODULE)library);

    logic_t failure;

   /*
    * Microsoft states that FreeLibrary() returns 'a non-zero BOOL value'
    * on success and the 'zero BOOL value' on failure.
    *
    * Our convention is similar to JCA_Error,
    * where the zero value indicates no error.
    */
    if (0 == result)
    {
        failure = TRUE;
    }
    else
    {
        failure = FALSE;
    }

    return failure;
}

/*!*****************************************************************************
 * Function Name: xds_GetProcAddress()
 *
 *   Description: Get a function pointer from a dynamic library.
 *
 *    Parameters: libraryHandle      Handle for dynamic library module.
 *                pFunction          Text name of function to retrieve.
 * 
 *  Return Value: Handle to function if no errors, otherwise returns NULL.
 *
 ******************************************************************************/

FNC_HANDLE
xds_GetProcAddress(LIB_HANDLE library,
                   char      *pFunction)
{
    return GetProcAddress((HMODULE)library, pFunction);
}

/* -- END OF FILE -- */
