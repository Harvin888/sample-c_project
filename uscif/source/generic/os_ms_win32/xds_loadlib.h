/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_loadlib.h
*
* DESCRIPTION
*   OS independent dynamic library access.
*   This version supports MS_WIN32 builds.
*
*   LIB_HANDLE is a handle for the load library routine.
*   FNC_HANDLE is a handle for the get function routine.
*
* AUTHOR
*   Edward Fewell
*   efewell@ti.com
*   281.274.3922
*
*******************************************************************************/

#ifndef xds_loadlib_h
#define xds_loadlib_h

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The type definitions.
*******************************************************************************/

typedef HMODULE LIB_HANDLE;
typedef FARPROC FNC_HANDLE;

/*******************************************************************************
* The function declarations.
*******************************************************************************/

LIB_HANDLE
xds_LoadLibrary(char *pLibrary);

logic_t
xds_FreeLibrary(LIB_HANDLE library);

FNC_HANDLE
xds_GetProcAddress(LIB_HANDLE library,
                   char      *pFunction);

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* xds_loadlib_h */

/* -- END OF FILE -- */
