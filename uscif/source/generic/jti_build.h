/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jti_build.h
*
* DESCRIPTION
*   The recipies for building JTI that are system independent. 
*
* NOTES
*   This header file shall include NO references to the operating system.
*
*   This header file should be included
*   first in all JTI source files.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jti_build_h
#define jti_build_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The following primary mnemonics must be provided by the make file
* or project file to indicate the build recipie for the executable
* or library being built.
*
* The JTI code is now sufficiently well structured that the
* same primary mnemonic is used independantly of the environment
* choice (win32/posix/bios) or build choice (development/release).
*
* Some of these primary mnemonics have a secondary mnemonc defined in
* their build recipie to indicate they share significant characteristics
* with other executables or libraries.
*
*   Primary Mnemonic       Secondary Mnemonic
*   ----------------       ------------------
*   XDSRESET_EXECUTABLE    CLI_EXECUTABLE
*   XDSPROBE_EXECUTABLE    CLI_EXECUTABLE
*   DBGJTAG_EXECUTABLE     CLI_EXECUTABLE
*   EASYSCAN_LIBRARY
*   JTAGPROBE_LIBRARY
*   LUASCRIPT_LIBRARY
*   JTAGDATA_LIBRARY
*   XDSBOARD_LIBRARY
*   XDSERROR_LIBRARY
*   XDSROUTE_LIBRARY
*   XDSICICLE_LIBRARY
*   USCIF_LIBRARY
*   MANAGE_LIBRARY
*   LEGACYTBC_LIBRARY      CONTROL_LIBRARY
*   NANOTBC_LIBRARY        CONTROL_LIBRARY
*   ZEROTBC_LIBRARY        CONTROL_LIBRARY
*   MODEL_LIBRARY
*   XDS510ISA_LIBRARY      ADAPTER_LIBRARY
*   XDSBHPCI_LIBRARY       ADAPTER_LIBRARY
*   XDS100PP_LIBRARY       ADAPTER_LIBRARY
*   XDS100USB_LIBRARY      ADAPTER_LIBRARY
*   ECOM_LIBRARY
*
*******************************************************************************/

/*******************************************************************************
*
* USE_FILE_SYSTEM == 0
*   Build software that does not require access to a file-system.
*
* USE_FILE_SYSTEM == 1
*   Build software that does require access to a file-system.
*
*******************************************************************************/

#undef USE_FILE_SYSTEM

/*******************************************************************************
* The following mnemonic identifies features that may enhance performance
* by in-lining the reads from and writes to hardware instead
* of using shared function calls.
*
* Setting this mnemonic to zero may result in significantly smaller binary.
*
* USE_INLINE_CODE == 0
*   Use function calls to access Legacy-TBC registers.
*
* USE_INLINE_CODE == 1
*   Use in-line code to access Legacy-TBC registers.
*
*******************************************************************************/

#undef  USE_INLINE_CODE

/*******************************************************************************
* The following mnemonics identify functions that
* allow use of features specific to the XDS510 SEPK.
*
* USE_CUSTOM_RDWR, USE_CUSTOM_SPRT, USE_CUSTOM_MODE == 0
*   Disable the use of certain XDS510 SEPK features.
*
* USE_CUSTOM_RDWR, USE_CUSTOM_SPRT, USE_CUSTOM_MODE == 1
*   Enable the use of certain XDS510 SEPK features.
*
*******************************************************************************/

#undef USE_CUSTOM_RDWR
#undef USE_CUSTOM_SPRT
#undef USE_CUSTOM_MODE

/*******************************************************************************
* The following mnemonic identifies functions that will allow
* support for generating log-files of processor-ids used by CC-Setup.
*
* USE_PROCID_LOGFILE == 0
*   Exclude the processor-id log-file support.
*
* USE_PROCID_LOGFILE == 1
*   Include the processor-id log-file support.
*
*******************************************************************************/

#undef USE_PROCID_LOGFILE

/*******************************************************************************
* The following mnemonic identifies functions that will
* allow support for working-around BoundsChecker limitations.
*
* One example is avoiding the gross BoundsChecker failure after 159
* repeated uses of realloc() to incrementally grow the LS_TITLE array.
*
* USE_BC_WORKAROUND == 0
*   Exclude the BoundsChecker work-around support.
*
* USE_BC_WORKAROUND == 1
*   Include the BoundsChecker lwork-around support.
*
*******************************************************************************/

#undef USE_BC_WORKAROUND

/*******************************************************************************
* The following temporary mnemonic identifies functions that will
* allow support for explicit loading of liraries.
*******************************************************************************/

#undef  USE_DYNAMIC_LIBS

/*******************************************************************************
* The following project is built with the TI C6x compiler.
* It is intended for the XDS560 board using a Nano-TBC.
*
* USCIF_LIBRARY
*   This builds a library named 'xdsfast3_ts.lib'
*   containing Unified-SCIF and supporting the Nano-TBC.
*
*   Note that code build into ths library is dependant
*   on TI_BUILD_TOOLS_CCS21 and TI_BUILD_TOOLS_CCS33
*   in the makefile.
*
*******************************************************************************/

#if (defined(USCIF_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      0

        #define USE_INLINE_CODE      0

        #define USE_BC_WORKAROUND    0
        #define USE_BC_SIZE          0

    #else

        #define USE_FILE_SYSTEM      0

        #define USE_INLINE_CODE      0

        #define USE_BC_WORKAROUND    0
        #define USE_BC_SIZE          0

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* The following projects are built with 'gmake/gcc'
* for use on POSIX compliant UNIX (Solaris/Linux/MacOSX).
*
* MANAGE_LIBRARY
*   This builds a library named 'xdsfast3.dll/.so' as the scan-manager.
*
*******************************************************************************/

#if (defined(MANAGE_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1

        #define USE_INLINE_CODE      0

        #define USE_XML_PARSER       0
        #define USE_BC_WORKAROUND    0
        #define USE_BC_SIZE          0

    #else

        #define USE_FILE_SYSTEM      1

        #define USE_INLINE_CODE      0

        #define USE_XML_PARSER       0
        #define USE_BC_WORKAROUND    1
        #define USE_BC_SIZE          512

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* The following projects are built with 'gmake/gcc'
* for use on POSIX compliant UNIX (Solaris/Linux/MacOSX).
*
* LEGACYTBC_LIBRARY
*   This builds a controller library for the Legacy-TBC.
*
* NANOTBC_LIBRARY
*   This builds a controller library for the Nano-TBC.
*
* ZEROTBC_LIBRARY
*   This builds a controller library for the Zero-TBC.
*
*******************************************************************************/

#if (defined(LEGACYTBC_LIBRARY) || \
     defined(NANOTBC_LIBRARY) || \
     defined(ZEROTBC_LIBRARY))

    #define CONTROL_LIBRARY

    #if (defined(NDEBUG))

        #define CONTROL_LIBRARY

        #define USE_FILE_SYSTEM      1

        #define USE_INLINE_CODE      1

        #define USE_BC_WORKAROUND    0
        #define USE_BC_SIZE          0

    #else

        #define USE_FILE_SYSTEM      1

        #define USE_INLINE_CODE      0

        #define USE_BC_WORKAROUND    1
        #define USE_BC_SIZE          512

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* The following projects are built with 'gmake/gcc'
* for use on POSIX compliant UNIX (Solaris/Linux/MacOSX).
*
* XDSBOARD_LIBRARY
*   This builds a library named 'xdsconfig.dll/.so' containing config handling.
*
* XDSERROR_LIBRARY
*   This builds a library named 'xdserror.dll/.so' containing error handling.
*
* XDSROUTE_LIBRARY
*   This builds a library named 'xdsroute.dll/.so' containing route management.
*
* XDSICICLE_LIBRARY
*   This builds a library named 'xdsicicle.dll/.so' containing route control.
*
*******************************************************************************/

#if (defined(XDSBOARD_LIBRARY) || \
     defined(XDSERROR_LIBRARY) || \
     defined(XDSROUTE_LIBRARY) || \
     defined(XDSICICLE_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1

        #define USE_XML_PARSER       0
        #define USE_BC_WORKAROUND    0
        #define USE_BC_SIZE          0

    #else

        #define USE_FILE_SYSTEM      1

        #define USE_XML_PARSER       0
        #define USE_BC_WORKAROUND    1
        #define USE_BC_SIZE          512

    #endif

#endif

/*******************************************************************************
* The following project is built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* ECOM_LIBRARY
*   This builds the libray named 'xdsecom3.dll' that provides the
*   USCIF-over-ECOM connection between 510 USCIF and 560 USCIF.
*
*******************************************************************************/

#if (defined(ECOM_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1

    #else

        #define USE_FILE_SYSTEM      1

    #endif

#endif

/*******************************************************************************
* The following projects are all built with `Visual C++ 6.0'
* and intended for Win95/98/ME and WinNT/2K/XP.
*
* JTAGDATA_LIBRARY
*   This builds a library named 'jtagdata.dll' that interprets CC-Setup
*   instructions for board configuration functions in 'xdsfast3.dll.
*
*******************************************************************************/

#if (defined(JTAGDATA_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1
        #define USE_PROCID_LOGFILE   0

    #else

        #define USE_FILE_SYSTEM      1
        #define USE_PROCID_LOGFILE   0

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* The following projects are built with 'gmake/gcc'
* for use on POSIX compliant UNIX (Solaris/Linux/MacOSX).
*
* XDSRESET_EXECUTABLE
*   This builds an executable named `xdsreset.exe/xdsreset'.
*   It is a command-line parser and output
*   formatter for the jtagprobe library.
*
* XDSPROBE_EXECUTABLE
*   This builds an executable named `xdsprobe.exe/xdsprobe'.
*   It is a command-line parser and output
*   formatter for the jtagprobe library.
*
* DBGJTAG_EXECUTABLE
*   This builds an executable named `dbgjtag.exe/dbgjtag'.
*   It is a command-line parser and output
*   formatter for the jtagprobe library.
*
*******************************************************************************/

#if (defined(XDSRESET_EXECUTABLE) || \
     defined(XDSPROBE_EXECUTABLE) || \
     defined(DBGJTAG_EXECUTABLE))

    #define CLI_EXECUTABLE

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1
        #define USE_PARSE_DEBUG      0

    #else

        #define USE_FILE_SYSTEM      1
        #define USE_PARSE_DEBUG      0

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* The following projects are built with 'gmake/gcc'
* for use on POSIX compliant UNIX (Solaris/Linux/MacOSX).
*
* EASYSCAN_LIBRARY
*   This builds a library named 'eswrap.dll/.so' that implements
*   the experimental Easy Scan wrapper.
*
* JTAGPROBE_LIBRARY
*   This builds a library named 'jtagprobe.dll/.so' that implements
*   the core of the xdsreset/xdsprobe/dbgjtag utilities.
*
* LUASCRIPT_LIBRARY
*   This builds a library named 'luascript.dll/.so' that implements
*   a Lua-script interactive shell library for the dbgjtag utility.
*
*******************************************************************************/

#if (defined(EASYSCAN_LIBRARY)  || \
     defined(JTAGPROBE_LIBRARY) || \
     defined(LUASCRIPT_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1

    #else

        #define USE_FILE_SYSTEM      1

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* MODEL_LIBRARY
*   This builds a library that implements a Nano-TBC C-Model.
*
*******************************************************************************/

#if (defined(MODEL_LIBRARY))

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1
        #define USE_INLINE_CODE      0

    #else

        #define USE_FILE_SYSTEM      1
        #define USE_INLINE_CODE      0

    #endif

#endif

/*******************************************************************************
* The following project is built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* XDS510ISA_LIBRARY
*   This builds an adapter library for the TI XDS510 ISA-bus.
*
*******************************************************************************/

#if (defined(XDS510ISA_LIBRARY))

    #define ADAPTER_LIBRARY

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1
        #define USE_INLINE_CODE      0

        #define USE_CUSTOM_RDWR      1 /* optional - built-in and custom code provided. */
        #define USE_CUSTOM_SPRT      1 /* compulsory - built-in code no longer exists.  */
        #define USE_CUSTOM_MODE      0 /* optional - built-in and custom code provided. */

    #else

        #define USE_FILE_SYSTEM      1
        #define USE_INLINE_CODE      0

        #define USE_CUSTOM_RDWR      1 /* optional - built-in and custom code provided. */
        #define USE_CUSTOM_SPRT      1 /* compulsory - built-in code no longer exists.  */
        #define USE_CUSTOM_MODE      0 /* optional - built-in and custom code provided. */

    #endif

#endif

/*******************************************************************************
* The following projects are built with `Visual C++ 6.0'
* for use on Win95/98/ME and WinNT/2K/XP.
*
* XDSBHPCI_LIBRARY
*   This builds an adapter library for the BlackHawk 560 PCI-bus.
*
* XDS100PP_LIBRARY
*   This builds an adapter library for the TI MSP430 PP Hardware.
*
* XDS100USB_LIBRARY
*   This builds an adapter library for the TI MSP430 USB Hardware.
*
*******************************************************************************/

#if (defined(XDSBHPCI_LIBRARY) || \
     defined(XDS100PP_LIBRARY) || \
     defined(XDS100USB_LIBRARY))

    #define ADAPTER_LIBRARY

    #if (defined(NDEBUG))

        #define USE_FILE_SYSTEM      1
        #define USE_INLINE_CODE      0

    #else

        #define USE_FILE_SYSTEM      1
        #define USE_INLINE_CODE      0

    #endif

#endif

/*******************************************************************************
* Most projects use explict loading of dynamic libraries.
*******************************************************************************/

#if (defined(USCIF_LIBRARY))

    #undef  USE_DYNAMIC_LIBS

#else

    #define USE_DYNAMIC_LIBS

#endif

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* jti_build_h */

/* -- END OF FILE -- */

