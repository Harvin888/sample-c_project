/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2007, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_variable.h
*
* DESCRIPTION
*   The structures used to store configure variables.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xds_variable_h
#define xds_variable_h /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The definitions.
*******************************************************************************/

/*
 * The maximum number of characters in a config variable name.
 * Allow for config variable names to be a 'section' plus a '.' plus a 'variable'.
 *
 * These mnemonics are defined in both "cfg_public.h" and "xds_variable.h".
 */
#ifndef LS_MAX_CFG
#define LS_MAX_CFG
  /* The maximum number of characters in identifier names. */
  #define LS_MAX_IDENTIFIER_NAME  16

  #define LS_MAX_CFG_VARY_NAME   (LS_MAX_IDENTIFIER_NAME + 1 + LS_MAX_IDENTIFIER_NAME)
  /* The maximum number of characters in a short config variable value. */
  #define LS_MAX_CFG_VARY_SHORT   32
  /* The maximum number of characters in a long config variable value. */
  #define LS_MAX_CFG_VARY_LONG    256
  /* The maximum length of the customized text string. */
  #define LS_MAX_TEXT_LENGTH      128
#endif

/*******************************************************************************
* This structure is used by "read_ecom_variable.c/h".
*******************************************************************************/

typedef struct COM_Variable
{
   /*
    * This configure variable is the name
    * of the ECOM accessed emulator driver.
    */
    char    driverName[LS_MAX_CFG_VARY_LONG + 1];
    logic_t useDrvr;

   /*
    * This configure variable is the value
    * of the ECOM accessed emulator address.
    */
    uint32_t portAddress;
    logic_t  usePort;

} COM_Variable;

/*******************************************************************************
* This structure is used by "read_jtag_variable.c/h".
*******************************************************************************/

typedef struct JTAG_Variable
{
   /*
    * This configure variable is the version number
    * of USCIF's software support for advanced JTAG.
    */
    uint32_t jtagVersion;

   /*
    * This configure variable is the type of DTS hardware
    * that translates between JTAG and advanced JTAG.
    */
    uint32_t jtagAdapter;

   /*
    * This configure variable is the choice of
    * operating format for the DTS hardware.
    */
    uint32_t jtagFormat;

   /*
    * This configure variable is the change in link-delay
    * caused by the presence of the DTS hardware.
    */

    uint32_t jtagDelta;
   /*
    * This configure variable is the choice of
    * edge signalling for the DTS hardware.
    */
    uint32_t jtagEdge;

} JTAG_Variable;

/*******************************************************************************
* This structure is used by "read_pll_variable.c/h".
*******************************************************************************/

typedef struct PLL_Variable
{
   /*
    * These configure variables select
    * the PLL programming algorithm.
    */
    /* The PLL programming command.        */
    uint32_t tclkProgram;
    /* The PLL programming frequency.      */
    uint32_t tclkBeginning;
    /* The PLL programming frequency.      */
    uint32_t tclkFrequency;
    /* The PLL scan-path test-mode option. */
    uint32_t tclkTestMode;
    /* The PLL scan-path over-flow option. */
    uint32_t tclkOverFlow;

   /*
    * These configure variables select
    * the pll programming scan-test length.
    */
    /* The scan-test length knowledge. */
    logic_t  bitsKnown;
    /* The scan-test length value.     */
    uint32_t bitsLength;

   /*
    * These configure variables select
    * the pll programming path length.
    */
    /* The IR path length knowledge. */
    logic_t  irKnown;
    /* The IR path length value.     */
    uint32_t irLength;
    /* The DR path length knowledge. */
    logic_t  drKnown;
    /* The DR path length value.     */
    uint32_t drLength;

} PLL_Variable;

/*******************************************************************************
* This structure is used by "read_rtr_variable.c/h".
*******************************************************************************/

typedef struct RTR_Variable
{
   /*
    * This configure variable customises the software
    * to support extreme low frequencies by suppressing
    * dead-clock errors.
    */
    logic_t  okStall;

   /*
    * These configure variables customise the software
    * to modify the common behaviour of all routers.
    */
    logic_t  controlScanEnb;
    logic_t  subpathDefaultEnb;
    logic_t  subpathCustomEnb;

   /*
    * These configure variables customise the software to
    * modify the behaviour of IcePick-A & -B routers.
    */
    logic_t  routerSupport;
    uint32_t routerSysOpen;
    uint32_t routerSysClose;
    uint32_t routerPinOpen;
    uint32_t routerPinClose;

   /*
    * These configure variables customise the software to
    * modify the behaviour of IcePick-A & -B & -C routers.
    */
    uint32_t routerIdleDelay;
    logic_t  routerSkipPoll;

   /*
    * These configure variables customise the software
    * to modify the behaviour of IcePick-C routers.
    */
    logic_t  routerPrivate;
    logic_t  routerAdaptive;

   /*
    * These configure variables customise the software
    * to modify the behaviour of IcePick-C routers.
    */
    uint32_t routerTclkProgram;
    logic_t  routerTdoEdge;

} RTR_Variable;

/*******************************************************************************
* This structure is used by "read_tbc_variable.c/h".
*******************************************************************************/

typedef struct TBC_Variable
{
   /*
    * This configure variable overrides the calculated
    * pre-ambles and post-ambles with a zero value.
    */
    logic_t noAmble;

   /*
    * This configure variable disables the double-check
    * function used to evaulate the root cause of errors.
    */
    logic_t noCheck;

   /*
    * These configure variables customise the software
    * to support relatively high and low frequencies.
    */
    logic_t  fastClock;
    logic_t  slowClock;
    uint32_t slowFreq;
    logic_t  okStall;

   /*
    * These configure variables modify operation
    * to support TMD/TDO output and TDI input on
    * the rising and falling edges of TCLKR.
    */
    uint32_t tdoEdge;
    uint32_t tdiEdge;

   /*
    * These configure variables modify operation
    * to support hardware link-delays between
    * the TMD/TDO output and TDI input.
    */
    logic_t  linkAutomatic;
    uint32_t linkDelay;

   /*
    * These configure variables override the calculated
    * end-delays applied at the end of scan commands.
    */
    uint32_t forceOnceEnd;
    uint32_t forceManyEnd;

   /*
    * These configure variables are a sub-set of the
    * PLL configure variables recorded in PLL_Variable.
    */
    uint32_t pllProgram;
    coord_t  pllBeginning;
    coord_t  pllFrequency;
    coord_t  pllReference;
    logic_t  pllInitial;
    logic_t  pllTerminal;
    coord_t  pllOmap3430;

   /*
    * These configure variables control the timing delays
    * supported by the CPLD in the Rev-C and Rev-D pods.
    */
    uint32_t delay_tdi_pod2emu;
    uint32_t delay_tdi_tgt2pod;
    uint32_t delay_tck_emu2pod;
    uint32_t delay_tck_pod2emu;

   /*
    * These configure variables configure the JTAG nTRST boot-mode
    * supported by the CPLD in the Rev-C and Rev-D pods.
    */
    uint32_t jtagBootMode;
    uint32_t jtagBootValue;

   /*
    * These configure variables configure the
    * Power-on-Reset boot-mode supported by
    * the CPLD in the Rev-C and Rev-D pods.
    */
    uint32_t powerBootMode;
    uint32_t powerBootValue;

   /*
    * These configure variables configure the
    * background values for the boot-modes supported
    * by the CPLD in the Rev-C and Rev-D pods.
    */
    uint32_t emuOutputMode;
    uint32_t emuOutputValue;

   /*
    * These configure variable configure the
    * loopback values for TCLKO/TCLKR and TDO/TDI.
    */
    uint32_t podLoopMode;
    uint32_t podLoopValue;

   /*
    * This configure variable is the direct connection
    * (no cable) flag of the Sourceless EPK driver.
    */
    logic_t directConnect;

   /*
    * This configure variable enables the
    * buffer test code for the scan-controller.
    */
    logic_t bufferTest;

} TBC_Variable;

/*******************************************************************************
* This structure is used by "read_sepk_variable.c/h".
*******************************************************************************/

typedef struct SEPK_Variable
{
   /*
    * This  configure variable is used to suppress
    * dead-clock and timeout errors when TCLKR stalls.
    */
    logic_t okStall;

   /*
    * This configure variable is the
    * name of the sourceless EPK driver.
    */
    char    driverName[LS_MAX_CFG_VARY_LONG + 1];
    logic_t useDrvr;

   /*
    * This configure variable is the value
    * of the sourceless EPK address or IP port.
    */
    uint32_t portAddress;
    logic_t  usePort;

   /*
    * This configure variable is the operating
    * mode of the Sourceless EPK driver.
    */
    logic_t  blockMode;
    logic_t  useMode;

   /*
    * This configure variable is the domain
    * name or IP address of the host computer.
    */
    char    hostName[LS_MAX_CFG_VARY_SHORT + 1];
    logic_t useHost;

   /*
    * This configure variable is the
    * value of the remote IP port.
    */
    uint32_t remotePort;
    logic_t  useRemote;

   /*
    * This configure variable is the direct connection
    * (no cable) flag of the Sourceless EPK driver.
    */
    logic_t directConnect;

   /*
    * This configure variable is flag used to enable
    * or disable logging the Sourceless EPK API's.
    */
    logic_t logFileEnable;
    logic_t useEnable;

   /*
    * This configure variable is used to configure
    * the style of logging the Sourceless EPK API's.
    */
    logic_t logFileVerbose;

   /*
    * This configure variable is used to name the
    * log-file for logging the Sourceless EPK API's.
    */
    char logFileName[LS_MAX_CFG_VARY_LONG + 1];

} SEPK_Variable;

/*******************************************************************************
* This structure is used by "read_xds_variable.c/h".
*******************************************************************************/

typedef struct XDS_Variable
{
   /*
    * This configure variable is the
    * name of the sourceless EPK driver.
    */
    char    driverNameSepk[LS_MAX_CFG_VARY_LONG + 1];
    logic_t useDrvrSepk;

   /*
    * This configure variable is the value
    * of the sourceless EPK address or IP port.
    */
    uint32_t portAddressSepk;
    logic_t  usePortSepk;

   /*
    * This configure variable is the operating
    * mode of the Sourceless EPK driver.
    */
    logic_t  blockModeSepk;
    logic_t  useModeSepk;

   /*
    * This configure variable is the name
    * of the ECOM accessed emulator driver.
    */
    char    driverNameEcom[LS_MAX_CFG_VARY_LONG + 1];
    logic_t useDrvrEcom;

   /*
    * This configure variable is the value
    * of the ECOM accessed emulator address.
    */
    uint32_t portAddressEcom;
    logic_t  usePortEcom;

} XDS_Variable;

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#endif /* xds_variable_h */

/* -- END OF FILE -- */
