/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_memory.c
*
* DESCRIPTION
*   A set of convienent memory allocation functions.
*
* PUBLIC FUNCTIONS
*
*   fast_malloc() - Do a memory allocation that requests faster memory.
*   slow_malloc() - Do a memory allocation that requests slower memory.
*
*   fast_realloc() - Do a memory re-allocation that requests faster memory.
*   slow_realloc() - Do a memory re-allocation that requests slower memory.
*
*   string_malloc() - Create a dynamically allocated string.
*   uscif_free()    - Free allocated memory, plus erase the pointer and size.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#define xds_memory_c /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* The build options and header files.
*******************************************************************************/

/*
 * The standard header files for the
 * transition from USCIF35 to JTI.
 */
#include "xds_include.h"

/*
 * The other header files.
 */
#include "xds_memory.h"

/*******************************************************************************
* NAME: fast_malloc()
*
* DESCRIPTION
*   Do a memory allocation that requests faster memory.
*
* NOTES
*
*******************************************************************************/

void *
fast_malloc(uint32_t size)
{
    return malloc(size);
}

/*******************************************************************************
* NAME: slow_malloc()
*
* DESCRIPTION
*   Do a memory allocation that requests slower memory.
*
* NOTES
*
*******************************************************************************/

void *
slow_malloc(uint32_t size)
{
    return malloc(size);
}

/*******************************************************************************
* NAME: fast_realloc()
*
* DESCRIPTION
*   Do a memory re-allocation that requests faster memory.
*
* NOTES
*
*******************************************************************************/

void *
fast_realloc(void *pMemory, uint32_t oldSize, uint32_t newSize)
{
    UNUSED_PARAM(oldSize);

    return realloc(pMemory, newSize);
}

/*******************************************************************************
* NAME: slow_realloc()
*
* DESCRIPTION
*   Do a memory re-allocation that requests slower memory.
*
* NOTES
*
*******************************************************************************/

void *
slow_realloc(void *pMemory, uint32_t oldSize, uint32_t newSize)
{
    UNUSED_PARAM(oldSize);

    return realloc(pMemory, newSize);
}

/*******************************************************************************
* NAME: string_malloc()
*
* DESCRIPTION
*   Create a dynamically allocated string.
*
* NOTES
*
*******************************************************************************/

logic_t
string_malloc(char **ppString, uint32_t *pSize, char *pText)
{
    char    *pString;
    uint32_t size;

    if ((NULL == ppString) || (NULL == pSize))
    {
        return TRUE;
    }

    /* Delete the old string. */
    size = *pSize;
    uscif_free((void**)ppString, &size);

    /* Create the new string. */
    size    = strlen(pText) + 1;
    pString = slow_malloc(size);
    if (NULL == pString)
    {
        return TRUE;
    }

    *ppString = strcpy(pString, pText);
    *pSize    = size;

    return FALSE;
}

/*******************************************************************************
* NAME: uscif_free()
*
* DESCRIPTION
*   Free allocated memory, plus erase the pointer and size.
*
* NOTES
*
*******************************************************************************/

void
uscif_free(void **ppMemory, uint32_t *pSize)
{
    if ((NULL == ppMemory) || (NULL == *ppMemory))
    {
        return;
    }
    if ((NULL == pSize) || (0 == *pSize))
    {
        return;
    }

    free(*ppMemory);

    *ppMemory = NULL;
    *pSize = 0;

    return;
}

/* -- END OF FILE -- */
