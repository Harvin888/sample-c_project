/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   jti_header.h
*
* DESCRIPTION
*   The definitions and includes for building JTI that are system dependent. 
*
* NOTES
*   This header file does include references to the operating system.
*
*   This header file should be included in all JTI
*   source files immediately after "jti_typedef.h".
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef jti_header_h
#define jti_header_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The ANSI-C header files.
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*******************************************************************************
* The operating system specific mnemonics and header files.
*******************************************************************************/

/*
 * This is where almost every executable and library acquires
 * its operating system specifc mnemonics and header files.

 * The open-source Lua source code is an exception.
 * It makes its own choices about such matters. 
 */

#if !defined(LUASCRIPT_LIBRARY)

    #if (MS_WIN32)

        #define VC_EXTRALEAN
        #define RPC_NO_WINDOWS_H

        #include <windows.h>
        #include <conio.h>
        #include <signal.h>
        #include <process.h>
        #include <winbase.h>
        #include <winsock.h>

    #endif /* (MS_WIN32) */

    #if (POSIX)

        #include <fcntl.h>
        #include <errno.h>
        #include <netdb.h>
        #include <pthread.h>
        #include <semaphore.h>
        #include <signal.h>
        #include <stdarg.h>
        #include <stdlib.h>
        #include <sys/mman.h>
        #include <sys/time.h>
        #include <sys/socket.h>
        #include <termios.h>
        #include <unistd.h>

        #if defined(__sun) || defined(__solaris__)
        #include <sys/filio.h>
        #endif /* defined(__sun) || defined(__solaris__) */

    #endif /* (POSIX) */

#endif /* !defined(LUASCRIPT_LIBRARY) */

/*******************************************************************************
* The operating system specific pragmas for suppressing error warnings.
*******************************************************************************/

/*
 * The following Microsoft tools warning are merely information:
 *
 *   C4711 - function selected for automatic inline expansion.
 *   C4702 - unreachable code as result of inline expansion.
 *   C4505 - unreferenced local function removed as result of inline expansion.
 *   C4100 - function has an unreferenced formal parameter.
 */
#if (MS_WIN32)
#pragma warning(disable : 4711 4702 4505)
#endif /* (MS_WIN32) */

/*
 * The following macro is used to suppress C4100.
 * The unreferenced parameter is used in an expression with no value.
 */
#if (MS_WIN32)
#define UNUSED_PARAM(x) ((void)(x))
#endif /* (MS_WIN32) */

#if (DSP_BIOS)
#define UNUSED_PARAM(x)
#endif /* (DSP_BIOS) */

#if (POSIX)
#define UNUSED_PARAM(x)
#endif /* (POSIX) */

/*******************************************************************************
* The operating system specific export and import mnemonics.
*******************************************************************************/

#undef JTAGDATA_API
#undef ESWRAP_API
#undef PROBE_API
#undef SCRIPT_API
#undef BOARD_API
#undef ERROR_API
#undef ROUTE_API
#undef ICICLE_API
#undef ECOM_API
#undef MANAGE_API
#undef CONTROL_API
#undef POD_API

#if (DSP_BIOS || POSIX)
    #define JTAGDATA_API
    #define ESWRAP_API
    #define PROBE_API
    #define SCRIPT_API
    #define BOARD_API
    #define ERROR_API
    #define ROUTE_API
    #define ICICLE_API
    #define ECOM_API
    #define MANAGE_API
    #define CONTROL_API
    #define POD_API
#endif /* (DSP_BIOS || POSIX) */

#if (MS_WIN32)
    #if defined(SWIG)
        /* We are building test infrastructure. */
        #define JTAGDATA_API
        #define ESWRAP_API
        #define PROBE_API
        #define SCRIPT_API
        #define BOARD_API
        #define ERROR_API
        #define ROUTE_API
        #define ICICLE_API
        #define ECOM_API
        #define MANAGE_API
        #define CONTROL_API
        #define POD_API
    #else
        /* We are building applications. */
        #define JTAGDATA_API __declspec(dllimport)
        #define ESWRAP_API   __declspec(dllimport)
        #define PROBE_API    __declspec(dllimport)
        #define SCRIPT_API   __declspec(dllimport)
        #define BOARD_API    __declspec(dllimport)
        #define ERROR_API    __declspec(dllimport)
        #define ROUTE_API    __declspec(dllimport)
        #define ICICLE_API   __declspec(dllimport)
        #define ECOM_API     __declspec(dllimport)
        #define MANAGE_API   __declspec(dllimport)
        #define CONTROL_API  __declspec(dllimport)
        #define POD_API      __declspec(dllimport)
    #endif

    #if defined(JTAGDATA_LIBRARY)
        /* We are inside the JTAGDATA library. */
        #undef  JTAGDATA_API
        #define JTAGDATA_API __declspec(dllexport) 
    #elif defined(EASYSCAN_LIBRARY)
        /* We are inside the ESWRAP library. */
        #undef  ESWRAP_API
        #define ESWRAP_API __declspec(dllexport) 
    #elif defined(JTAGPROBE_LIBRARY)
        /* We are inside the PROBE library. */
        #undef  PROBE_API
        #define PROBE_API __declspec(dllexport) 
    #elif defined(LUASCRIPT_LIBRARY)
        /* We are inside the SCRIPT library. */
        #undef  SCRIPT_API
        #define SCRIPT_API __declspec(dllexport) 
    #elif defined(XDSBOARD_LIBRARY)
        /* We are inside the CONFIG library. */
        #undef  BOARD_API
        #define BOARD_API __declspec(dllexport)
    #elif defined(XDSERROR_LIBRARY)
        /* We are inside the ERROR library. */
        #undef  ERROR_API
        #define ERROR_API __declspec(dllexport)
    #elif defined(XDSROUTE_LIBRARY)
        /* We are inside the ROUTE library. */
        #undef  ROUTE_API
        #define ROUTE_API __declspec(dllexport)
    #elif defined(XDSICICLE_LIBRARY)
        /* We are inside the ICICLE library. */
        #undef  ICICLE_API
        #define ICICLE_API __declspec(dllexport)
    #elif defined(MANAGE_LIBRARY)
        /* We are inside the SMG library. */
        #undef  MANAGE_API
        #define MANAGE_API __declspec(dllexport)
    #elif defined(CONTROL_LIBRARY)
        /* We are inside the TBC library. */
        #undef  CONTROL_API
        #define CONTROL_API __declspec(dllexport)
    #elif defined(ECOM_LIBRARY)
        /* We are inside the ECOM library. */
        #undef  ECOM_API
        #define ECOM_API __declspec(dllexport)
    #elif defined(ADAPTER_LIBRARY)
        /* We are inside the ADAPTER library. */
        #undef  POD_API
        #define POD_API __declspec(dllexport)
    #endif
#endif /* (MS_WIN32) */

/*******************************************************************************
* The operating system specific variable attributes.
*******************************************************************************/

#if (MS_WIN32)
#define XDS_FAR
#define XDS_NEAR
#endif /* (MS_WIN32) */

#if (DSP_BIOS)
#define XDS_FAR  far
#define XDS_NEAR near
#endif /* (DSP_BIOS) */

#if (POSIX)
#define XDS_FAR
#define XDS_NEAR
#endif /* (POSIX) */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* jti_header_h */

/* -- END OF FILE -- */
