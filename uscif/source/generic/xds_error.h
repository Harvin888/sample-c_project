/*******************************************************************************
* This is the start of the automatically generated error offset and
* title descriptions that are derived from the 'xdserror.cfg' file.
*******************************************************************************/

/*******************************************************************************
* Define the blocks of Unified-SCIF driver and utility errors.
*******************************************************************************/

/* There are 70 offset descriptions. */

#define OFFSET_NONE      0
#define OFFSET_010       -10
#define OFFSET_020       -20
#define OFFSET_030       -30
#define OFFSET_040       -40
#define OFFSET_050       -50
#define OFFSET_060       -60
#define OFFSET_070       -70
#define OFFSET_080       -80
#define OFFSET_090       -90
#define OFFSET_BAD       -100
#define OFFSET_OCS       -110
#define OFFSET_CMD       -120
#define OFFSET_SYS       -130
#define OFFSET_KNL       -140
#define OFFSET_POD       -150
#define OFFSET_MEM       -160
#define OFFSET_REG       -170
#define OFFSET_CTL       -180
#define OFFSET_RST       -190
#define OFFSET_TBC       -200
#define OFFSET_LCK       -210
#define OFFSET_BOOT      -220
#define OFFSET_PATH      -230
#define OFFSET_ROUTER    -240
#define OFFSET_ECOM      -250
#define OFFSET_260       -260
#define OFFSET_SCAN      -270
#define OFFSET_CLK       -280
#define OFFSET_290       -290
#define OFFSET_PLL       -300
#define OFFSET_PGM       -310
#define OFFSET_IDNT      -320
#define OFFSET_CNFG      -330
#define OFFSET_HUNG      -340
#define OFFSET_ITSABUG   -350
#define OFFSET_EMULOG    -360
#define OFFSET_NSEQ      -370
#define OFFSET_LIST      -380
#define OFFSET_390       -390
#define OFFSET_BOARD1    -400
#define OFFSET_BOARD2    -410
#define OFFSET_OFFSET    -420
#define OFFSET_TITLE     -430
#define OFFSET_VARIABLE  -440
#define OFFSET_PACKAGE   -450
#define OFFSET_FAMILY    -460
#define OFFSET_ALIAS     -470
#define OFFSET_DEVICE    -480
#define OFFSET_SUBPATH   -490
#define OFFSET_TEST1     -500
#define OFFSET_TEST2     -510
#define OFFSET_LOGFILE   -520
#define OFFSET_AJTAG0    -530
#define OFFSET_AJTAG1    -540
#define OFFSET_TCPIP     -550
#define OFFSET_THREAD    -560
#define OFFSET_LUA       -570
#define OFFSET_580       -580
#define OFFSET_590       -590
#define OFFSET_LIB_ANY   -600
#define OFFSET_LIB_PRB   -610
#define OFFSET_LIB_ECOM  -620
#define OFFSET_LIB_SMG   -630
#define OFFSET_LIB_RTR   -640
#define OFFSET_LIB_BRD   -650
#define OFFSET_LIB_ERR   -660
#define OFFSET_LIB_TBC   -670
#define OFFSET_LIB_LUA   -680
#define OFFSET_690       -690

/*******************************************************************************
* Define the individual Unified-SCIF driver and utility error values.
*
* The following errors are explicitly coded in many PTI's.
* Their numerical values must not change.
*
*   SC_ERR_OCS_ALREADY_OPEN
*
*   SC_ERR_CTL_NO_TRG_POWER
*   SC_ERR_CTL_NO_TRG_CLOCK
*   SC_ERR_CTL_CBL_BREAK_NEAR
*   SC_ERR_CTL_CBL_BREAK_FAR
*
*******************************************************************************/

/* There are 0x0174 title descriptions. */

/* +1 This '+1' value should be returned only during code development. */
#define SC_ERR_TRIVIAL_POS               (OFFSET_NONE + 1)
/* -1 This '-1' value should be returned only during code development. */
#define SC_ERR_TRIVIAL_NEG               (OFFSET_NONE - 1)

/*  0 This error number is used when no error has occured. */
#define SC_ERR_NONE                      (OFFSET_NONE - 0)

/*  0 This error value '0' is provided only as an example. */
#define SC_ERR_USCIF_START               (OFFSET_NONE - 0)
/* -999 This error value '-999' is provided only as an example. */
#define SC_ERR_USCIF_FINISH              (OFFSET_NONE - 999)
/* -100 This error value '-100' is provided only as an example. */
#define SC_ERR_DRIVER_POS                (OFFSET_NONE - 100)
/* -699 This error value '-599' is provided only as an example. */
#define SC_ERR_DRIVER_NEG                (OFFSET_NONE - 699)
/* -700 This error value '-700' is provided only as an example. */
#define SC_ERR_ADAPT_POS                 (OFFSET_NONE - 700)
/* -899 This error value '-899' is provided only as an example. */
#define SC_ERR_ADAPT_NEG                 (OFFSET_NONE - 899)

/* -100 Something bad happened and we don't know why. */
#define SC_ERR_BAD_GENERIC               (OFFSET_BAD - 0)

/* -110 The controller is already in use by another client. */
#define SC_ERR_OCS_ALREADY_OPEN          (OFFSET_OCS - 0)
/* -111 Generic failure when opening the controller. */
#define SC_ERR_OCS_OPEN                  (OFFSET_OCS - 1)
/* -112 Generic failure when closing the controller. */
#define SC_ERR_OCS_CLOSE                 (OFFSET_OCS - 2)
/* -113 Failure due to controller being hung and not responding. */
#define SC_ERR_OCS_HUNG                  (OFFSET_OCS - 3)
/* -114 Failure due to controller operations are taking too long. */
#define SC_ERR_OCS_BUSY                  (OFFSET_OCS - 4)
/* -115 Failure due to controller operation having bad parameters. */
#define SC_ERR_OCS_PARM                  (OFFSET_OCS - 5)
/* -116 Failure due to controller data-structures are corrupted. */
#define SC_ERR_OCS_TRASH                 (OFFSET_OCS - 6)
/* -117 Failure due to lack of controller data-structures. */
#define SC_ERR_OCS_NUMBER                (OFFSET_OCS - 7)
/* -118 Failure due to invalid port address. */
#define SC_ERR_OCS_PORT                  (OFFSET_OCS - 8)

/* -120 An invalid command was used in a SMG_call(). */
#define SC_ERR_CMD_INVALID               (OFFSET_CMD - 0)
/* -121 An invalid controller handle was used in a SMG_call(). */
#define SC_ERR_CMD_HANDLE                (OFFSET_CMD - 1)
/* -122 An invalid command arguments structure in SMG_open or SMG_call(). */
#define SC_ERR_CMD_PARM                  (OFFSET_CMD - 2)
/* -123 An invalid controller index was used in a SMG_call() or SMG_open(). */
#define SC_ERR_CMD_TBC_INDEX             (OFFSET_CMD - 3)
/* -124 An invalid command index was used in a SMG_call(). */
#define SC_ERR_CMD_INDEX                 (OFFSET_CMD - 4)
/* -125 An invalid count value was used in a SMG_call(). */
#define SC_ERR_CMD_COUNT                 (OFFSET_CMD - 5)
/* -126 An operation was attempted that is invalid in zero client mode. */
#define SC_ERR_CMD_INVALID_ZERO          (OFFSET_CMD - 6)
/* -127 An operation was attempted that is invalid in single client mode. */
#define SC_ERR_CMD_INVALID_SNGL          (OFFSET_CMD - 7)
/* -128 An operation was attempted that is invalid in multiple client mode. */
#define SC_ERR_CMD_INVALID_MULT          (OFFSET_CMD - 8)

/* -130 The OS or its version is not known to Unified-SCIF. */
#define SC_ERR_SYS_UNKNOWN_OS            (OFFSET_SYS - 0)
/* -131 The OS has reported an error when allocating memory. */
#define SC_ERR_SYS_MEM_ALLOC             (OFFSET_SYS - 1)
/* -132 The OS has reported an error when freeing memory. */
#define SC_ERR_SYS_MEM_FREE              (OFFSET_SYS - 2)
/* -133 The OS has reported an error when opening a file. */
#define SC_ERR_SYS_FILE_OPEN             (OFFSET_SYS - 3)
/* -134 The OS has reported an error when closing a file. */
#define SC_ERR_SYS_FILE_CLOSE            (OFFSET_SYS - 4)
/* -135 The OS has reported an error when reading a file. */
#define SC_ERR_SYS_FILE_READ             (OFFSET_SYS - 5)
/* -136 The OS has reported an error when writing a file. */
#define SC_ERR_SYS_FILE_WRITE            (OFFSET_SYS - 6)
/* -137 The OS has reported an error in the file statistics. */
#define SC_ERR_SYS_FILE_STAT             (OFFSET_SYS - 7)

/* -140 An attempt to call the kernel driver failed. */
#define SC_ERR_KNL_FAIL                  (OFFSET_KNL - 0)
/* -141 An attempt to open the kernel driver failed. */
#define SC_ERR_KNL_OPEN                  (OFFSET_KNL - 1)
/* -142 An attempt to close the kernel driver failed. */
#define SC_ERR_KNL_CLOSE                 (OFFSET_KNL - 2)
/* -143 An attempt to start the kernel driver failed. */
#define SC_ERR_KNL_START                 (OFFSET_KNL - 3)
/* -144 An attempt to stop the kernel driver failed. */
#define SC_ERR_KNL_STOP                  (OFFSET_KNL - 4)

/* -150 An attempt to call the adapter failed. */
#define SC_ERR_POD_FAIL                  (OFFSET_POD - 0)
/* -151 An attempt to open the adapter failed. */
#define SC_ERR_POD_OPEN                  (OFFSET_POD - 1)
/* -152 An attempt to close the adapter failed. */
#define SC_ERR_POD_CLOSE                 (OFFSET_POD - 2)
/* -153 An attempt to call the adapter to read the TBC failed. */
#define SC_ERR_POD_TBC_READ              (OFFSET_POD - 3)
/* -154 An attempt to call the adapter to write the TBC failed. */
#define SC_ERR_POD_TBC_WRITE             (OFFSET_POD - 4)
/* -155 An adapter library itself cannot be found. */
#define SC_ERR_POD_NO_LIBRARY            (OFFSET_POD - 5)
/* -156 An adapter library function cannot be found. */
#define SC_ERR_POD_NO_FUNCTION           (OFFSET_POD - 6)
/* -157 An attempt to start-up an adapter call failed. */
#define SC_ERR_POD_STARTUP               (OFFSET_POD - 7)
/* -158 An attempt to shut-down an adapter call failed. */
#define SC_ERR_POD_SHUTDOWN              (OFFSET_POD - 8)
/* -159 The adapter version is in-compatible with the USCIF version. */
#define SC_ERR_POD_VERSION               (OFFSET_POD - 9)

/* -160 An error occurred while allocating controller memory. */
#define SC_ERR_MEM_ALLOC                 (OFFSET_MEM - 0)
/* -161 An error occurred while freeing controller memory. */
#define SC_ERR_MEM_FREE                  (OFFSET_MEM - 1)
/* -162 An error occurred while allocating `fixed' controller memory. */
#define SC_ERR_MEM_FIX_ALLOC             (OFFSET_MEM - 2)
/* -163 An error occurred while freeing `fixed' controller memory. */
#define SC_ERR_MEM_FIX_FREE              (OFFSET_MEM - 3)
/* -164 An error occurred while allocating `shared' memory. */
#define SC_ERR_MEM_SHARE_ALLOC           (OFFSET_MEM - 4)
/* -165 An error occurred while freeing `shared' memory. */
#define SC_ERR_MEM_SHARE_FREE            (OFFSET_MEM - 5)

/* -170 An invalid address for the test bus controller or support logic. */
#define SC_ERR_REG_INVALID               (OFFSET_REG - 0)
/* -171 No test bus controller was found at the specified address. */
#define SC_ERR_REG_DETECT_CTLR           (OFFSET_REG - 1)
/* -172 No support-logic was found at the specified address. */
#define SC_ERR_REG_DETECT_SPRT           (OFFSET_REG - 2)
/* -173 The high half of the test bus controller is stuck at all-ones. */
#define SC_ERR_REG_DETECT_HIONES         (OFFSET_REG - 3)
/* -174 The low half of the test bus controller is stuck at all-ones. */
#define SC_ERR_REG_DETECT_LOONES         (OFFSET_REG - 4)
/* -175 The high half of the test bus controller is stuck at all-zeros. */
#define SC_ERR_REG_DETECT_HIZERO         (OFFSET_REG - 5)
/* -176 The low half of the test bus controller is stuck at all-zeros. */
#define SC_ERR_REG_DETECT_LOZERO         (OFFSET_REG - 6)

/* -180 The target system has lost its power supply. */
#define SC_ERR_CTL_NO_TRG_POWER          (OFFSET_CTL - 0)
/* -181 The target system has lost its JTAG clock. */
#define SC_ERR_CTL_NO_TRG_CLOCK          (OFFSET_CTL - 1)
/* -182 The target cable is disconnected near-to the controller. */
#define SC_ERR_CTL_CBL_BREAK_NEAR        (OFFSET_CTL - 2)
/* -183 The target cable is disconnected far-from the controller. */
#define SC_ERR_CTL_CBL_BREAK_FAR         (OFFSET_CTL - 3)
/* -184 The controller has detected a failure in its connection to the target. */
#define SC_ERR_CTL_CONNECT_FAIL          (OFFSET_CTL - 4)
/* -185 The target cable has an invalid version number. */
#define SC_ERR_CTL_POD_VERSION           (OFFSET_CTL - 5)

/* -190 An attempt to software reset the test bus controller failed. */
#define SC_ERR_RST_SERIAL_RESET          (OFFSET_RST - 0)
/* -191 An attempt to hardware reset the test bus controller failed. */
#define SC_ERR_RST_MASTER_RESET          (OFFSET_RST - 1)
/* -192 An attempt to access the test bus controller during a reset has failed. */
#define SC_ERR_RST_TBC_ACCESS            (OFFSET_RST - 2)

/* -200 The version number of the test bus controller is invalid. */
#define SC_ERR_TBC_VERSION               (OFFSET_TBC - 0)
/* -201 The controller configuration being attempted is not valid. */
#define SC_ERR_TBC_CONFIGURE             (OFFSET_TBC - 1)
/* -202 A bad test bus controller or support logic address has been used. */
#define SC_ERR_TBC_ADDRESS               (OFFSET_TBC - 2)
/* -203 The test bus controller is not compatible with the installed software. */
#define SC_ERR_TBC_COMPATIBLE            (OFFSET_TBC - 3)

/* -210 Failed to open the semaphore for the test bus controller. */
#define SC_ERR_LCK_OPEN                  (OFFSET_LCK - 0)
/* -211 Failed to close the semaphore for the test bus controller. */
#define SC_ERR_LCK_CLOSE                 (OFFSET_LCK - 1)
/* -212 Failed to acquire the semaphore for the test bus controller. */
#define SC_ERR_LCK_ACQUIRE               (OFFSET_LCK - 2)
/* -213 Failed to release the semaphore for the test bus controller. */
#define SC_ERR_LCK_RELEASE               (OFFSET_LCK - 3)
/* -214 The semaphore for the test bus controller is in an abandoned state. */
#define SC_ERR_LCK_DIED                  (OFFSET_LCK - 4)
/* -215 The semaphore for the test bus controller is in a time-out state. */
#define SC_ERR_LCK_TIME                  (OFFSET_LCK - 5)
/* -216 The semaphore for the test bus controller is in an abused state. */
#define SC_ERR_LCK_OWNED                 (OFFSET_LCK - 6)

/* -220 Invalid JTAG nTRST boot-mode command. */
#define SC_ERR_BOOT_BAD_JTAGBOOT_MODE    (OFFSET_BOOT - 0)
/* -221 Invalid JTAG nTRST boot-mode value. */
#define SC_ERR_BOOT_BAD_JTAGBOOT_VALUE   (OFFSET_BOOT - 1)
/* -222 Invalid Power-on-Reset boot-mode command. */
#define SC_ERR_BOOT_BAD_POWERBOOT_MODE   (OFFSET_BOOT - 2)
/* -223 Invalid Power-on-Reset boot-mode value. */
#define SC_ERR_BOOT_BAD_POWERBOOT_VALUE  (OFFSET_BOOT - 3)
/* -224 Invalid background boot-mode command. */
#define SC_ERR_BOOT_BAD_EMUOUTPUT_MODE   (OFFSET_BOOT - 4)
/* -225 Invalid background boot-mode value. */
#define SC_ERR_BOOT_BAD_EMUOUTPUT_VALUE  (OFFSET_BOOT - 5)
/* -226 Invalid clock/data loopback command. */
#define SC_ERR_BOOT_BAD_LOOPBACK_MODE    (OFFSET_BOOT - 6)
/* -227 Invalid clock/data loopback value. */
#define SC_ERR_BOOT_BAD_LOOPBACK_VALUE   (OFFSET_BOOT - 7)

/* -230 The measured lengths of the JTAG IR and DR scan-paths are invalid. */
#define SC_ERR_PATH_MEASURE              (OFFSET_PATH - 0)
/* -231 The measured length of the JTAG IR instruction path is invalid. */
#define SC_ERR_PATH_IR_MEASURE           (OFFSET_PATH - 1)
/* -232 The measured length of the JTAG DR data path is invalid. */
#define SC_ERR_PATH_DR_MEASURE           (OFFSET_PATH - 2)
/* -233 The JTAG IR and DR scan-paths cannot circulate bits, they may be broken. */
#define SC_ERR_PATH_BROKEN               (OFFSET_PATH - 3)
/* -234 The instruction scan-path cannot circulate bits, it may be broken. */
#define SC_ERR_PATH_IR_BROKEN            (OFFSET_PATH - 4)
/* -235 The data scan-path cannot circulate bits, it may be broken. */
#define SC_ERR_PATH_DR_BROKEN            (OFFSET_PATH - 5)

/* -240 A router family in the board config file has no routing software. */
#define SC_ERR_ROUTER_NO_MINIDRIVER      (OFFSET_ROUTER - 0)
/* -241 A router subpath could not be accessed. */
#define SC_ERR_ROUTER_SECURE_SUBPATH     (OFFSET_ROUTER - 1)
/* -242 A router subpath could not be accessed. */
#define SC_ERR_ROUTER_ACCESS_SUBPATH     (OFFSET_ROUTER - 2)

/* -250 An attempt to access the named emulator via USCIF ECOM has failed. */
#define SC_ERR_ECOM_EMUNAME              (OFFSET_ECOM - 0)
/* -251 An attempt to access the USCIF interface via USCIF ECOM has failed. */
#define SC_ERR_ECOM_NO_USCIF             (OFFSET_ECOM - 1)
/* -252 An attempt to operate the USCIF interface via USCIF ECOM has failed. */
#define SC_ERR_ECOM_OPERATE              (OFFSET_ECOM - 2)

/* -270 The length of a scan command was too large for the test bus controller. */
#define SC_ERR_SCAN_LENGTH_CONTROL       (OFFSET_SCAN - 0)
/* -271 The length of the scan command was too large for the test bus controller's memory. */
#define SC_ERR_SCAN_LENGTH_MEMORY        (OFFSET_SCAN - 1)
/* -272 The requested length of the scan-path test was outside the acceptable range. */
#define SC_ERR_SCAN_LENGTH_SCANTEST      (OFFSET_SCAN - 2)
/* -273 The specified length of the JTAG IR scan-path is too large. */
#define SC_ERR_SCAN_LENGTH_IRPATH        (OFFSET_SCAN - 3)
/* -274 The specified length of the JTAG DR scan-path is too large. */
#define SC_ERR_SCAN_LENGTH_DRPATH        (OFFSET_SCAN - 4)
/* -275 The attempt to poll a target device exceeded its timeout limit. */
#define SC_ERR_SCAN_POLL_BUSY            (OFFSET_SCAN - 5)

/* -280 The requested TCLK frequency isn't supported by the the hardware. */
#define SC_ERR_CLK_SELECT_HW             (OFFSET_CLK - 0)
/* -281 The requested TCLK frequency value is out of the supported range. */
#define SC_ERR_CLK_SELECT_OOR            (OFFSET_CLK - 1)
/* -282 The measurement of TCLK frequency isn't supported by the hardware. */
#define SC_ERR_CLK_MEASURE_HW            (OFFSET_CLK - 2)
/* -283 The measured TCLK frequency value is out of the supported range. */
#define SC_ERR_CLK_MEASURE_OOR           (OFFSET_CLK - 3)
/* -284 The PLL auto-ranging function has exceeded its over-flow limit. */
#define SC_ERR_CLK_OVER_FLOW             (OFFSET_CLK - 4)
/* -285 The PLL programming function has detected invalid coordinates. */
#define SC_ERR_CLK_PLL_PARM              (OFFSET_CLK - 5)
/* -286 The PLL programming function has suffered a timeout. */
#define SC_ERR_CLK_PLL_HUNG              (OFFSET_CLK - 6)
/* -287 The frequency measurement function has suffered a timeout. */
#define SC_ERR_CLK_FQ_HUNG               (OFFSET_CLK - 7)

/* -300 The requested TCLK PLL programming option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_PROGRAM      (OFFSET_PLL - 0)
/* -301 The requested TCLK PLL frequency option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_BEGINNING    (OFFSET_PLL - 1)
/* -302 The requested TCLK PLL frequency option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_FREQUENCY    (OFFSET_PLL - 2)
/* -303 The requested TCLK PLL reference option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_REFERENCE    (OFFSET_PLL - 3)
/* -304 The requested TCLK PLL initial option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_INITIAL      (OFFSET_PLL - 4)
/* -305 The requested TCLK PLL terminal option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_TERMINAL     (OFFSET_PLL - 5)
/* -306 The requested TCLK PLL scan-test test-mode option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_TESTMODE     (OFFSET_PLL - 6)
/* -307 The requested TCLK PLL scan-test over-flow option is invalid. */
#define SC_ERR_PLL_BAD_TCLK_OVERFLOW     (OFFSET_PLL - 7)

/* -310 The programming of FPGA's isn't supported by the hardware. */
#define SC_ERR_PGM_FPGA_NOT_USED         (OFFSET_PGM - 0)
/* -311 An attempt to program the emulator FPGA failed. */
#define SC_ERR_PGM_FPGA_PROTECTED        (OFFSET_PGM - 1)
/* -312 The emulator FPGA programming failed before loading data. */
#define SC_ERR_PGM_FPGA_FAIL_BEFORE      (OFFSET_PGM - 2)
/* -313 The emulator FPGA programming failed while loading data. */
#define SC_ERR_PGM_FPGA_FAIL_DURING      (OFFSET_PGM - 3)
/* -314 The emulator FPGA programming failed after loading data. */
#define SC_ERR_PGM_FPGA_FAIL_AFTER       (OFFSET_PGM - 4)
/* -315 The emulator FPGA programming failed with a bad check-sum. */
#define SC_ERR_PGM_FPGA_CHECKSUM_BAD     (OFFSET_PGM - 5)
/* -316 The emulator FPGA programming failed for unknown reasons. */
#define SC_ERR_PGM_FPGA_UNKNOWN          (OFFSET_PGM - 6)
/* -317 The emulator FPGA programming failed because it is erased. */
#define SC_ERR_PGM_FPGA_ERASED           (OFFSET_PGM - 7)

/* -320 The variable identifier is invalid. */
#define SC_ERR_IDNT_VARIABLE_ID          (OFFSET_IDNT - 0)
/* -321 The package identifier is invalid. */
#define SC_ERR_IDNT_PACKAGE_ID           (OFFSET_IDNT - 1)
/* -322 The family identifier is invalid. */
#define SC_ERR_IDNT_FAMILY_ID            (OFFSET_IDNT - 2)
/* -323 The alias  identifier is invalid. */
#define SC_ERR_IDNT_ALIAS_ID             (OFFSET_IDNT - 3)
/* -324 The device identifier is invalid. */
#define SC_ERR_IDNT_DEVICE_ID            (OFFSET_IDNT - 4)
/* -325 The sub-path identifier is invalid. */
#define SC_ERR_IDNT_SUBPATH_ID           (OFFSET_IDNT - 5)
/* -326 The revise identifier is invalid. */
#define SC_ERR_IDNT_REVISE_ID            (OFFSET_IDNT - 6)
/* -327 The usage identifier is invalid. */
#define SC_ERR_IDNT_USAGE_ID             (OFFSET_IDNT - 7)
/* -328 The inst identifier is invalid. */
#define SC_ERR_IDNT_INST_ID              (OFFSET_IDNT - 8)
/* -329 The path identifier is invalid. */
#define SC_ERR_IDNT_PATH_ID              (OFFSET_IDNT - 9)

/* -330 The configure file's contents are missing. */
#define SC_ERR_CNFG_FILE_EMPTY           (OFFSET_CNFG - 0)
/* -331 The configure file's contents are too large. */
#define SC_ERR_CNFG_FILE_SIZE            (OFFSET_CNFG - 1)
/* -332 The configure file's end was reached too soon. */
#define SC_ERR_CNFG_FILE_END             (OFFSET_CNFG - 2)
/* -333 The configure file syntax is bad, there are invalid tokens. */
#define SC_ERR_CNFG_FILE_SYNTAX          (OFFSET_CNFG - 3)
/* -334 The configure file format is unknown. */
#define SC_ERR_CNFG_FILE_FORMAT          (OFFSET_CNFG - 4)
/* -335 The configure file version is invalid. */
#define SC_ERR_CNFG_FILE_VERSION         (OFFSET_CNFG - 5)
/* -336 The configure file has lines that are too long. */
#define SC_ERR_CNFG_LINE_LENGTH          (OFFSET_CNFG - 6)
/* -337 The configure file has too many lines. */
#define SC_ERR_CNFG_LINE_NUMBER          (OFFSET_CNFG - 7)
/* -338 The configure file has a record name that is bad. */
#define SC_ERR_CNFG_NAME_BAD             (OFFSET_CNFG - 8)
/* -339 The configure file has a record value that is bad. */
#define SC_ERR_CNFG_ITEM_BAD             (OFFSET_CNFG - 9)

/* -340 Failure due to the controller not-ready signal taking too long. */
#define SC_ERR_HUNG_NOREADY              (OFFSET_HUNG - 0)
/* -341 Failure due to the controller test clock signal taking too long. */
#define SC_ERR_HUNG_NOTESTCLK            (OFFSET_HUNG - 1)
/* -342 Failure due to the controller command finish taking too long. */
#define SC_ERR_HUNG_COMMAND              (OFFSET_HUNG - 2)
/* -343 Failure due to the controller buffer reads are taking too long. */
#define SC_ERR_HUNG_RDBUFFER             (OFFSET_HUNG - 3)
/* -344 Failure due to the controller buffer writes are taking too long. */
#define SC_ERR_HUNG_WRBUFFER             (OFFSET_HUNG - 4)

/* -350 Failure due to bug of type 'A' detected in the emulator VHDL. */
#define SC_ERR_ITSABUG_A                 (OFFSET_ITSABUG - 0)
/* -351 Failure due to bug of type 'B' detected in the emulator VHDL. */
#define SC_ERR_ITSABUG_B                 (OFFSET_ITSABUG - 1)
/* -352 Failure due to bug of type 'C' detected in the emulator VHDL. */
#define SC_ERR_ITSABUG_C                 (OFFSET_ITSABUG - 2)
/* -353 Failure due to bug of type 'C' detected in the emulator VHDL. */
#define SC_ERR_ITSABUG_D                 (OFFSET_ITSABUG - 3)

/* -360 The emulator software has exceeded the capacity of its internal log-file. */
#define SC_ERR_EMULOG_FULL               (OFFSET_EMULOG - 0)
/* -361 The emulator software have put an invalid value in its internal log-file. */
#define SC_ERR_EMULOG_VALUE              (OFFSET_EMULOG - 1)
/* -362 Failed to read the values stored in the internal internal log-file. */
#define SC_ERR_EMULOG_GETARRAY           (OFFSET_EMULOG - 2)

/* -370 The nano-sequencer operation has failed. */
#define SC_ERR_NSEQ_FAIL                 (OFFSET_NSEQ - 0)
/* -371 The nano-sequencer operation has timed out. */
#define SC_ERR_NSEQ_TIMEOUT              (OFFSET_NSEQ - 1)

/* -380 An error occurred when deleting a device from the device select list. */
#define SC_ERR_LIST_DEVICE_DELETE        (OFFSET_LIST - 0)
/* -381 An error occurred when inserting a device into the device select list. */
#define SC_ERR_LIST_DEVICE_INSERT        (OFFSET_LIST - 1)
/* -382 An error occurred when deleting a sub-path from the sub-path select list. */
#define SC_ERR_LIST_SUBPATH_DELETE       (OFFSET_LIST - 2)
/* -383 An error occurred when inserting a sub-path into the sub-path select list. */
#define SC_ERR_LIST_SUBPATH_INSERT       (OFFSET_LIST - 3)

/* -400 Failed to find the specified offset name in the board config info'. */
#define SC_ERR_BOARD_OFFSET_NAME         (OFFSET_BOARD1 - 0)
/* -401 Failed to find the specified offset ID in the board config info'. */
#define SC_ERR_BOARD_OFFSET_ID           (OFFSET_BOARD1 - 1)
/* -402 Failed to find the specified title name in the board config info'. */
#define SC_ERR_BOARD_TITLE_NAME          (OFFSET_BOARD1 - 2)
/* -403 Failed to find the specified title ID in the board config info'. */
#define SC_ERR_BOARD_TITLE_ID            (OFFSET_BOARD1 - 3)
/* -404 Failed to find the specified variable name in the board config info'. */
#define SC_ERR_BOARD_VARIABLE_NAME       (OFFSET_BOARD1 - 4)
/* -405 Failed to find the specified variable ID in the board config info'. */
#define SC_ERR_BOARD_VARIABLE_ID         (OFFSET_BOARD1 - 5)
/* -406 Failed to find the specified package name in the board config info'. */
#define SC_ERR_BOARD_PACKAGE_NAME        (OFFSET_BOARD1 - 6)
/* -407 Failed to find the specified package identifier in the board config info'. */
#define SC_ERR_BOARD_PACKAGE_ID          (OFFSET_BOARD1 - 7)

/* -410 Failed to find the specified family name in the board config info'. */
#define SC_ERR_BOARD_FAMILY_NAME         (OFFSET_BOARD2 - 0)
/* -411 Failed to find the specified family identifier in the board config info'. */
#define SC_ERR_BOARD_FAMILY_ID           (OFFSET_BOARD2 - 1)
/* -412 Failed to find the specified alias  name in the board config info'. */
#define SC_ERR_BOARD_ALIAS_NAME          (OFFSET_BOARD2 - 2)
/* -413 Failed to find the specified alias  identifier in the board config info'. */
#define SC_ERR_BOARD_ALIAS_ID            (OFFSET_BOARD2 - 3)
/* -414 Failed to find the specified device name in the board config info'. */
#define SC_ERR_BOARD_DEVICE_NAME         (OFFSET_BOARD2 - 4)
/* -415 Failed to find the specified device identifier in the board config info'. */
#define SC_ERR_BOARD_DEVICE_ID           (OFFSET_BOARD2 - 5)
/* -416 Failed to find the specified subpath name in the board config info'. */
#define SC_ERR_BOARD_SUBPATH_NAME        (OFFSET_BOARD2 - 6)
/* -417 Failed to find the specified subpath ID in the board config info'. */
#define SC_ERR_BOARD_SUBPATH_ID          (OFFSET_BOARD2 - 7)

/* -420 In the configure file an offset name is empty. */
#define SC_ERR_OFFSET_NAME_EMPTY         (OFFSET_OFFSET - 0)
/* -421 In the configure file an offset name is too long. */
#define SC_ERR_OFFSET_NAME_LENGTH        (OFFSET_OFFSET - 1)
/* -422 In the configure file an offset name has invalid spelling. */
#define SC_ERR_OFFSET_NAME_SPELL         (OFFSET_OFFSET - 2)
/* -423 In the configure file an offset name must be unique, but its already used. */
#define SC_ERR_OFFSET_NAME_UNIQUE        (OFFSET_OFFSET - 3)
/* -424 In the configure file an offset name=value is absent. */
#define SC_ERR_OFFSET_ITEM_ABSENT        (OFFSET_OFFSET - 4)
/* -425 In the configure file an offset name=value is too long. */
#define SC_ERR_OFFSET_ITEM_LENGTH        (OFFSET_OFFSET - 5)
/* -426 In the configure file an offset name=value has a bad name or value. */
#define SC_ERR_OFFSET_ITEM_BAD           (OFFSET_OFFSET - 6)
/* -427 In the configure file an offset has too many name=values. */
#define SC_ERR_OFFSET_ITEM_2MANY         (OFFSET_OFFSET - 7)
/* -428 Invalid attempt to close an offset when not yet in an offset. */
#define SC_ERR_OFFSET_BAD_FINISH         (OFFSET_OFFSET - 8)

/* -430 In the configure file an title name is empty. */
#define SC_ERR_TITLE_NAME_EMPTY          (OFFSET_TITLE - 0)
/* -431 In the configure file an title name is too long. */
#define SC_ERR_TITLE_NAME_LENGTH         (OFFSET_TITLE - 1)
/* -432 In the configure file an title name has invalid spelling. */
#define SC_ERR_TITLE_NAME_SPELL          (OFFSET_TITLE - 2)
/* -433 In the configure file an title name must be unique, but its already used. */
#define SC_ERR_TITLE_NAME_UNIQUE         (OFFSET_TITLE - 3)
/* -434 In the configure file an title name=value is absent. */
#define SC_ERR_TITLE_ITEM_ABSENT         (OFFSET_TITLE - 4)
/* -435 In the configure file an title name=value is too long. */
#define SC_ERR_TITLE_ITEM_LENGTH         (OFFSET_TITLE - 5)
/* -436 In the configure file an title name=value has a bad name or value. */
#define SC_ERR_TITLE_ITEM_BAD            (OFFSET_TITLE - 6)
/* -437 In the configure file an title has too many name=values. */
#define SC_ERR_TITLE_ITEM_2MANY          (OFFSET_TITLE - 7)
/* -438 Invalid attempt to close a title when not yet in a title. */
#define SC_ERR_TITLE_BAD_FINISH          (OFFSET_TITLE - 8)

/* -440 In the configure file a variable name is empty. */
#define SC_ERR_VARIABLE_NAME_EMPTY       (OFFSET_VARIABLE - 0)
/* -441 In the configure file a variable name is too long. */
#define SC_ERR_VARIABLE_NAME_LENGTH      (OFFSET_VARIABLE - 1)
/* -442 In the configure file a variable name has invalid spelling. */
#define SC_ERR_VARIABLE_NAME_SPELL       (OFFSET_VARIABLE - 2)
/* -443 In the configure file a variable name must be unique, but its already used. */
#define SC_ERR_VARIABLE_NAME_UNIQUE      (OFFSET_VARIABLE - 3)
/* -444 In the configure file a variable name=value is absent. */
#define SC_ERR_VARIABLE_ITEM_ABSENT      (OFFSET_VARIABLE - 4)
/* -445 In the configure file a variable value is too long. */
#define SC_ERR_VARIABLE_ITEM_LENGTH      (OFFSET_VARIABLE - 5)
/* -446 In the configure file a variable name=value has a bad name or value. */
#define SC_ERR_VARIABLE_ITEM_BAD         (OFFSET_VARIABLE - 6)
/* -447 Invalid attempt to close a section when not yet in a section. */
#define SC_ERR_VARIABLE_BAD_FINISH       (OFFSET_VARIABLE - 7)
/* -448 Invalid spelling of the value of a configure variable. */
#define SC_ERR_VARIABLE_VALUE_SPELL      (OFFSET_VARIABLE - 8)
/* -449 Invalid spelling of the name of a configure variable. */
#define SC_ERR_VARIABLE_NAME_SEPARATE    (OFFSET_VARIABLE - 9)

/* -450 In the configure file a package name is empty. */
#define SC_ERR_PACKAGE_NAME_EMPTY        (OFFSET_PACKAGE - 0)
/* -451 In the configure file a package name is too long. */
#define SC_ERR_PACKAGE_NAME_LENGTH       (OFFSET_PACKAGE - 1)
/* -452 In the configure file a package name has invalid spelling. */
#define SC_ERR_PACKAGE_NAME_SPELL        (OFFSET_PACKAGE - 2)
/* -453 In the configure file a package name must be unique, but its already used. */
#define SC_ERR_PACKAGE_NAME_UNIQUE       (OFFSET_PACKAGE - 3)
/* -454 In the configure file a package name=value is absent. */
#define SC_ERR_PACKAGE_ITEM_ABSENT       (OFFSET_PACKAGE - 4)
/* -455 In the configure file a package name=value is too long. */
#define SC_ERR_PACKAGE_ITEM_LENGTH       (OFFSET_PACKAGE - 5)
/* -456 In the configure file a package name=value has a bad name or value. */
#define SC_ERR_PACKAGE_ITEM_BAD          (OFFSET_PACKAGE - 6)
/* -457 In the configure file a package has too many name=values. */
#define SC_ERR_PACKAGE_ITEM_2MANY        (OFFSET_PACKAGE - 7)

/* -460 In the configure file a family name is empty. */
#define SC_ERR_FAMILY_NAME_EMPTY         (OFFSET_FAMILY - 0)
/* -461 In the configure file a family name is too long. */
#define SC_ERR_FAMILY_NAME_LENGTH        (OFFSET_FAMILY - 1)
/* -462 In the configure file a family name has invalid spelling. */
#define SC_ERR_FAMILY_NAME_SPELL         (OFFSET_FAMILY - 2)
/* -463 In the configure file a family name must be unique, but its already used. */
#define SC_ERR_FAMILY_NAME_UNIQUE        (OFFSET_FAMILY - 3)
/* -464 In the configure file a family name=value is absent. */
#define SC_ERR_FAMILY_ITEM_ABSENT        (OFFSET_FAMILY - 4)
/* -465 In the configure file a family name=value is too long. */
#define SC_ERR_FAMILY_ITEM_LENGTH        (OFFSET_FAMILY - 5)
/* -466 In the configure file a family name=value has a bad name or value. */
#define SC_ERR_FAMILY_ITEM_BAD           (OFFSET_FAMILY - 6)
/* -467 In the configure file a family has too many name=values. */
#define SC_ERR_FAMILY_ITEM_2MANY         (OFFSET_FAMILY - 7)
/* -468 Invalid attempt to close a family when not yet in a family. */
#define SC_ERR_FAMILY_BAD_FINISH         (OFFSET_FAMILY - 8)

/* -470 In the configure file an alias  name is empty. */
#define SC_ERR_ALIAS_NAME_EMPTY          (OFFSET_ALIAS - 0)
/* -471 In the configure file an alias  name is too long. */
#define SC_ERR_ALIAS_NAME_LENGTH         (OFFSET_ALIAS - 1)
/* -472 In the configure file an alias  name has invalid spelling. */
#define SC_ERR_ALIAS_NAME_SPELL          (OFFSET_ALIAS - 2)
/* -473 In the configure file an alias name must be unique, but its already used. */
#define SC_ERR_ALIAS_NAME_UNIQUE         (OFFSET_ALIAS - 3)
/* -474 In the configure file an alias name=value is absent. */
#define SC_ERR_ALIAS_ITEM_ABSENT         (OFFSET_ALIAS - 4)
/* -475 In the configure file an alias name=value is too long. */
#define SC_ERR_ALIAS_ITEM_LENGTH         (OFFSET_ALIAS - 5)
/* -476 In the configure file an alias name=value has a bad name or value. */
#define SC_ERR_ALIAS_ITEM_BAD            (OFFSET_ALIAS - 6)
/* -477 In the configure file an alias has too many name=values. */
#define SC_ERR_ALIAS_ITEM_2MANY          (OFFSET_ALIAS - 7)
/* -478 Invalid attempt to close an alias when not yet in an alias. */
#define SC_ERR_ALIAS_BAD_FINISH          (OFFSET_ALIAS - 8)
/* -479 In the configure file an alias has no matching family. */
#define SC_ERR_ALIAS_NO_FAMILY           (OFFSET_ALIAS - 9)

/* -480 In the configure file a device name is empty. */
#define SC_ERR_DEVICE_NAME_EMPTY         (OFFSET_DEVICE - 0)
/* -481 In the configure file a device name is too long. */
#define SC_ERR_DEVICE_NAME_LENGTH        (OFFSET_DEVICE - 1)
/* -482 In the configure file a device name has invalid spelling. */
#define SC_ERR_DEVICE_NAME_SPELL         (OFFSET_DEVICE - 2)
/* -483 In the configure file a device name must be unique, but its already used. */
#define SC_ERR_DEVICE_NAME_UNIQUE        (OFFSET_DEVICE - 3)
/* -484 In the configure file a device name=value is absent. */
#define SC_ERR_DEVICE_ITEM_ABSENT        (OFFSET_DEVICE - 4)
/* -485 In the configure file a device name=value is too long. */
#define SC_ERR_DEVICE_ITEM_LENGTH        (OFFSET_DEVICE - 5)
/* -486 In the configure file a device name=value has a bad name or value. */
#define SC_ERR_DEVICE_ITEM_BAD           (OFFSET_DEVICE - 6)
/* -487 In the configure file a device has too many name=values. */
#define SC_ERR_DEVICE_ITEM_2MANY         (OFFSET_DEVICE - 7)
/* -488 The expected and actual number of devices don't match. */
#define SC_ERR_DEVICE_BAD_NUMBER         (OFFSET_DEVICE - 8)

/* -490 In the configure file a subpath name is empty. */
#define SC_ERR_SUBPATH_NAME_EMPTY        (OFFSET_SUBPATH - 0)
/* -491 In the configure file a subpath name is too long. */
#define SC_ERR_SUBPATH_NAME_LENGTH       (OFFSET_SUBPATH - 1)
/* -492 In the configure file a subpath name has invalid spelling. */
#define SC_ERR_SUBPATH_NAME_SPELL        (OFFSET_SUBPATH - 2)
/* -493 In the configure file a subpath name must be unique, but its already used. */
#define SC_ERR_SUBPATH_NAME_UNIQUE       (OFFSET_SUBPATH - 3)
/* -494 In the configure file a subpath name=value is absent. */
#define SC_ERR_SUBPATH_ITEM_ABSENT       (OFFSET_SUBPATH - 4)
/* -495 In the configure file a subpath name=value is too long. */
#define SC_ERR_SUBPATH_ITEM_LENGTH       (OFFSET_SUBPATH - 5)
/* -496 In the configure file a subpath name=value has a bad name or value. */
#define SC_ERR_SUBPATH_ITEM_BAD          (OFFSET_SUBPATH - 6)
/* -497 In the configure file a subpath has too many name=values. */
#define SC_ERR_SUBPATH_ITEM_2MANY        (OFFSET_SUBPATH - 7)
/* -498 The expected and actual number of subpaths don't match. */
#define SC_ERR_SUBPATH_BAD_NUMBER        (OFFSET_SUBPATH - 8)
/* -499 Invalid attempt to close a sub-path when not yet on a sub-path. */
#define SC_ERR_SUBPATH_BAD_FINISH        (OFFSET_SUBPATH - 9)

/* -500 The hardware selected external frequency failed the scan-path test. */
#define SC_ERR_TEST_EXTERNAL             (OFFSET_TEST1 - 0)
/* -501 The built-in scan-path length measurement failed. */
#define SC_ERR_TEST_MEASURE              (OFFSET_TEST1 - 1)
/* -502 The built-in PLL frequency selection algorithm has failed. */
#define SC_ERR_TEST_PLLNEXT              (OFFSET_TEST1 - 2)
/* -503 The user selected phase-one auto-range frequency failed the scan-path test. */
#define SC_ERR_TEST_AUTOMATIC1           (OFFSET_TEST1 - 3)
/* -504 The user selected phase-two auto-range frequency failed the scan-path test. */
#define SC_ERR_TEST_AUTOMATIC2           (OFFSET_TEST1 - 4)
/* -505 The user selected phase-three auto-range frequency failed the scan-path test. */
#define SC_ERR_TEST_AUTOMATIC3           (OFFSET_TEST1 - 5)
/* -506 The user selected synchronous adaptve frequency failed the scan-path test. */
#define SC_ERR_TEST_ADAPTIVE             (OFFSET_TEST1 - 6)
/* -507 The user selected asynchronous adaptve frequency failed the scan-path test. */
#define SC_ERR_TEST_INVERTER             (OFFSET_TEST1 - 7)

/* -510 The pre-defined 'reference' frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_REFERENCE            (OFFSET_TEST2 - 0)
/* -511 The pre-defined 'minimum' frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_MINIMUM              (OFFSET_TEST2 - 1)
/* -512 The pre-defined 'legacy' frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_LEGACY               (OFFSET_TEST2 - 2)
/* -513 The pre-defined 'exchange' frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_EXCHANGE             (OFFSET_TEST2 - 3)
/* -514 The pre-defined 'standard' frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_STANDARD             (OFFSET_TEST2 - 4)
/* -515 The pre-defined 'maximum' frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_MAXIMUM              (OFFSET_TEST2 - 5)
/* -516 The user-selected specific frequency failed the scan-path reliability test. */
#define SC_ERR_TEST_SPECIFIC             (OFFSET_TEST2 - 6)

/* -520 A test report has exceeded the capacity of its internal log-file. */
#define SC_ERR_LOGFILE_FULL              (OFFSET_LOGFILE - 0)
/* -521 A test report has put an invalid value in its internal log-file. */
#define SC_ERR_LOGFILE_VALUE             (OFFSET_LOGFILE - 1)

/* -530 The requested advanced JTAG version option is invalid. */
#define SC_ERR_AJTAG_BAD_JTAG_VERSION    (OFFSET_AJTAG0 - 0)
/* -531 The requested advanced JTAG adapter option is invalid. */
#define SC_ERR_AJTAG_BAD_JTAG_ADAPTER    (OFFSET_AJTAG0 - 1)
/* -532 The requested advanced JTAG format option is invalid. */
#define SC_ERR_AJTAG_BAD_JTAG_FORMAT     (OFFSET_AJTAG0 - 2)
/* -533 The requested advanced JTAG delta option is invalid. */
#define SC_ERR_AJTAG_BAD_JTAG_DELTA      (OFFSET_AJTAG0 - 3)
/* -534 The requested advanced JTAG edge option is invalid. */
#define SC_ERR_AJTAG_BAD_JTAG_EDGE       (OFFSET_AJTAG0 - 4)

/* -540 A protocol error has occurred - the advanced JTAG state is invalid. */
#define SC_ERR_AJTAG_CORRUPT             (OFFSET_AJTAG1 - 0)

/* -550 An attempt to start-up the sockets interface failed. */
#define SC_ERR_TCPIP_OPEN                (OFFSET_TCPIP - 0)
/* -551 An attempt to shut-down the sockets interface failed. */
#define SC_ERR_TCPIP_CLOSE               (OFFSET_TCPIP - 1)
/* -552 An attempt to connect via the sockets interface failed. */
#define SC_ERR_TCPIP_CONNECT             (OFFSET_TCPIP - 2)
/* -556 An attempt to dis-connect via the sockets interface failed. */
#define SC_ERR_TCPIP_SHUTDOWN            (OFFSET_TCPIP - 6)
/* -553 An attempt to send data via the sockets interface failed. */
#define SC_ERR_TCPIP_SEND                (OFFSET_TCPIP - 3)
/* -554 An attempt to receive data via the sockets interface failed. */
#define SC_ERR_TCPIP_RECV                (OFFSET_TCPIP - 4)
/* -555 An attempt to start-up a listener via the sockets interface failed. */
#define SC_ERR_TCPIP_LISTENER            (OFFSET_TCPIP - 5)
/* -557 The remote server host name or IP address is invalid or missing. */
#define SC_ERR_TCPIP_HOST                (OFFSET_TCPIP - 7)

/* -560 An attempt to create a thread caused a system error. */
#define SC_ERR_THREAD_OPEN               (OFFSET_THREAD - 0)
/* -561 An attempt to destroy a thread caused a system error. */
#define SC_ERR_THREAD_CLOSE              (OFFSET_THREAD - 1)
/* -562 An attempt to create a mutex caused a system error. */
#define SC_ERR_MUTEX_OPEN                (OFFSET_THREAD - 2)
/* -563 An attempt to destroy a mutex caused a system error. */
#define SC_ERR_MUTEX_CLOSE               (OFFSET_THREAD - 3)
/* -564 An attempt to lock a mutex caused a system error. */
#define SC_ERR_MUTEX_LOCK                (OFFSET_THREAD - 4)
/* -565 An attempt to unlock a mutex caused a system error. */
#define SC_ERR_MUTEX_UNLOCK              (OFFSET_THREAD - 5)

/* -570 The Lua-script interactive shell has failed and we don't know why. */
#define SC_ERR_LUA_FAILURE               (OFFSET_LUA - 0)

/* -600 A required dynamic library could not be located. */
#define SC_ERR_LIB_ANY_LOCATE            (OFFSET_LIB_ANY - 0)
/* -601 A required dynamic library could not be accessed. */
#define SC_ERR_LIB_ANY_ACCESS            (OFFSET_LIB_ANY - 1)
/* -602 A required dynamic library could not be used. */
#define SC_ERR_LIB_ANY_VERSION           (OFFSET_LIB_ANY - 2)
/* -603 A required dynamic library could not be resolved. */
#define SC_ERR_LIB_ANY_FUNCTION          (OFFSET_LIB_ANY - 3)
/* -604 A required dynamic library could not be installed. */
#define SC_ERR_LIB_ANY_LOAD              (OFFSET_LIB_ANY - 4)
/* -605 A required dynamic library could not be removed. */
#define SC_ERR_LIB_ANY_FREE              (OFFSET_LIB_ANY - 5)
/* -606 A required dynamic library could not be initialised. */
#define SC_ERR_LIB_ANY_INITIALISE        (OFFSET_LIB_ANY - 6)
/* -607 A required dynamic library could not be terminated. */
#define SC_ERR_LIB_ANY_TERMINATE         (OFFSET_LIB_ANY - 7)

/* -610 The jtag-probe dynamic library could not be located. */
#define SC_ERR_LIB_PRB_LOCATE            (OFFSET_LIB_PRB - 0)
/* -611 The jtag-probe dynamic library could not be accessed. */
#define SC_ERR_LIB_PRB_ACCESS            (OFFSET_LIB_PRB - 1)
/* -612 The jtag-probe dynamic library could not be used. */
#define SC_ERR_LIB_PRB_VERSION           (OFFSET_LIB_PRB - 2)
/* -613 The jtag-probe dynamic library could not be resolved. */
#define SC_ERR_LIB_PRB_FUNCTION          (OFFSET_LIB_PRB - 3)
/* -614 The jtag-probe dynamic library could not be installed. */
#define SC_ERR_LIB_PRB_LOAD              (OFFSET_LIB_PRB - 4)
/* -615 The jtag-probe dynamic library could not be removed. */
#define SC_ERR_LIB_PRB_FREE              (OFFSET_LIB_PRB - 5)
/* -616 The jtag-probe dynamic library could not be initialised. */
#define SC_ERR_LIB_PRB_INITIALISE        (OFFSET_LIB_PRB - 6)
/* -617 The jtag-probe dynamic library could not be terminated. */
#define SC_ERR_LIB_PRB_TERMINATE         (OFFSET_LIB_PRB - 7)

/* -600 One of the ECOM dynamic libraries could not be located. */
#define SC_ERR_LIB_ECOM_LOCATE           (OFFSET_LIB_ANY - 0)
/* -601 One of the ECOM dynamic libraries could not be accessed. */
#define SC_ERR_LIB_ECOM_ACCESS           (OFFSET_LIB_ANY - 1)
/* -602 One of the ECOM dynamic libraries could not be used. */
#define SC_ERR_LIB_ECOM_VERSION          (OFFSET_LIB_ANY - 2)
/* -603 One of the ECOM dynamic libraries could not be resolved. */
#define SC_ERR_LIB_ECOM_FUNCTION         (OFFSET_LIB_ANY - 3)
/* -604 One of the ECOM dynamic libraries could not be installed. */
#define SC_ERR_LIB_ECOM_LOAD             (OFFSET_LIB_ANY - 4)
/* -605 One of the ECOM dynamic libraries could not be removed. */
#define SC_ERR_LIB_ECOM_FREE             (OFFSET_LIB_ANY - 5)
/* -606 One of the ECOM dynamic libraries could not be initialised. */
#define SC_ERR_LIB_ECOM_INITIALISE       (OFFSET_LIB_ANY - 6)
/* -607 One of the ECOM dynamic libraries could not be terminated. */
#define SC_ERR_LIB_ECOM_TERMINATE        (OFFSET_LIB_ANY - 7)

/* -630 The scan-manager dynamic library could not be located. */
#define SC_ERR_LIB_SMG_LOCATE            (OFFSET_LIB_SMG - 0)
/* -631 The scan-manager dynamic library could not be accessed. */
#define SC_ERR_LIB_SMG_ACCESS            (OFFSET_LIB_SMG - 1)
/* -632 The scan-manager dynamic library could not be used. */
#define SC_ERR_LIB_SMG_VERSION           (OFFSET_LIB_SMG - 2)
/* -633 The scan-manager dynamic library could not be resolved. */
#define SC_ERR_LIB_SMG_FUNCTION          (OFFSET_LIB_SMG - 3)
/* -634 The scan-manager dynamic library could not be installed. */
#define SC_ERR_LIB_SMG_LOAD              (OFFSET_LIB_SMG - 4)
/* -635 The scan-manager dynamic library could not be removed. */
#define SC_ERR_LIB_SMG_FREE              (OFFSET_LIB_SMG - 5)
/* -636 The scan-manager dynamic library could not be initialised. */
#define SC_ERR_LIB_SMG_INITIALISE        (OFFSET_LIB_SMG - 6)
/* -637 The scan-manager dynamic library could not be terminated. */
#define SC_ERR_LIB_SMG_TERMINATE         (OFFSET_LIB_SMG - 7)

/* -640 The route-manager dynamic library could not be located. */
#define SC_ERR_LIB_RTR_LOCATE            (OFFSET_LIB_RTR - 0)
/* -641 The route-manager dynamic library could not be accessed. */
#define SC_ERR_LIB_RTR_ACCESS            (OFFSET_LIB_RTR - 1)
/* -642 The route-manager dynamic library could not be used. */
#define SC_ERR_LIB_RTR_VERSION           (OFFSET_LIB_RTR - 2)
/* -643 The route-manager dynamic library could not be resolved. */
#define SC_ERR_LIB_RTR_FUNCTION          (OFFSET_LIB_RTR - 3)
/* -644 The route-manager dynamic library could not be installed. */
#define SC_ERR_LIB_RTR_LOAD              (OFFSET_LIB_RTR - 4)
/* -645 The route-manager dynamic library could not be removed. */
#define SC_ERR_LIB_RTR_FREE              (OFFSET_LIB_RTR - 5)
/* -646 The route-manager dynamic library could not be initialised. */
#define SC_ERR_LIB_RTR_INITIALISE        (OFFSET_LIB_RTR - 6)
/* -647 The route-manager dynamic library could not be terminated. */
#define SC_ERR_LIB_RTR_TERMINATE         (OFFSET_LIB_RTR - 7)

/* -650 The board-confgure dynamic library could not be located. */
#define SC_ERR_LIB_BRD_LOCATE            (OFFSET_LIB_BRD - 0)
/* -651 The board-confgure dynamic library could not be accessed. */
#define SC_ERR_LIB_BRD_ACCESS            (OFFSET_LIB_BRD - 1)
/* -652 The board-confgure dynamic library could not be used. */
#define SC_ERR_LIB_BRD_VERSION           (OFFSET_LIB_BRD - 2)
/* -653 The board-confgure dynamic library could not be resolved. */
#define SC_ERR_LIB_BRD_FUNCTION          (OFFSET_LIB_BRD - 3)
/* -654 The board-confgure dynamic library could not be installed. */
#define SC_ERR_LIB_BRD_LOAD              (OFFSET_LIB_BRD - 4)
/* -655 The board-confgure dynamic library could not be removed. */
#define SC_ERR_LIB_BRD_FREE              (OFFSET_LIB_BRD - 5)
/* -656 The board-confgure dynamic library could not be initialised. */
#define SC_ERR_LIB_BRD_INITIALISE        (OFFSET_LIB_BRD - 6)
/* -657 The board-confgure dynamic library could not be terminated. */
#define SC_ERR_LIB_BRD_TERMINATE         (OFFSET_LIB_BRD - 7)

/* -660 The error-handler dynamic library could not be located. */
#define SC_ERR_LIB_ERR_LOCATE            (OFFSET_LIB_ERR - 0)
/* -661 The error-handler dynamic library could not be accessed. */
#define SC_ERR_LIB_ERR_ACCESS            (OFFSET_LIB_ERR - 1)
/* -662 The error-handler dynamic library could not be used. */
#define SC_ERR_LIB_ERR_VERSION           (OFFSET_LIB_ERR - 2)
/* -663 The error-handler dynamic library could not be resolved. */
#define SC_ERR_LIB_ERR_FUNCTION          (OFFSET_LIB_ERR - 3)
/* -664 The error-handler dynamic library could not be installed. */
#define SC_ERR_LIB_ERR_LOAD              (OFFSET_LIB_ERR - 4)
/* -665 The error-handler dynamic library could not be removed. */
#define SC_ERR_LIB_ERR_FREE              (OFFSET_LIB_ERR - 5)
/* -666 The error-handler dynamic library could not be initialised. */
#define SC_ERR_LIB_ERR_INITIALISE        (OFFSET_LIB_ERR - 6)
/* -667 The error-handler dynamic library could not be terminated. */
#define SC_ERR_LIB_ERR_TERMINATE         (OFFSET_LIB_ERR - 7)

/* -670 The scan-controller dynamic library could not be located. */
#define SC_ERR_LIB_TBC_LOCATE            (OFFSET_LIB_TBC - 0)
/* -671 The scan-controller dynamic library could not be accessed. */
#define SC_ERR_LIB_TBC_ACCESS            (OFFSET_LIB_TBC - 1)
/* -672 The scan-controller dynamic library could not be used. */
#define SC_ERR_LIB_TBC_VERSION           (OFFSET_LIB_TBC - 2)
/* -673 The scan-controller dynamic library could not be resolved. */
#define SC_ERR_LIB_TBC_FUNCTION          (OFFSET_LIB_TBC - 3)
/* -674 The scan-controller dynamic library could not be installed. */
#define SC_ERR_LIB_TBC_LOAD              (OFFSET_LIB_TBC - 4)
/* -675 The scan-controller dynamic library could not be removed. */
#define SC_ERR_LIB_TBC_FREE              (OFFSET_LIB_TBC - 5)
/* -676 The scan-controller dynamic library could not be initialised. */
#define SC_ERR_LIB_TBC_INITIALISE        (OFFSET_LIB_TBC - 6)
/* -677 The scan-controller dynamic library could not be terminated. */
#define SC_ERR_LIB_TBC_TERMINATE         (OFFSET_LIB_TBC - 7)

/* -680 The lua-script dynamic library could not be located. */
#define SC_ERR_LIB_LUA_LOCATE            (OFFSET_LIB_LUA - 0)
/* -681 The lua-script dynamic library could not be accessed. */
#define SC_ERR_LIB_LUA_ACCESS            (OFFSET_LIB_LUA - 1)
/* -682 The lua-script dynamic library could not be used. */
#define SC_ERR_LIB_LUA_VERSION           (OFFSET_LIB_LUA - 2)
/* -683 The lua-script dynamic library could not be resolved. */
#define SC_ERR_LIB_LUA_FUNCTION          (OFFSET_LIB_LUA - 3)
/* -684 The lua-script dynamic library could not be installed. */
#define SC_ERR_LIB_LUA_LOAD              (OFFSET_LIB_LUA - 4)
/* -685 The lua-script dynamic library could not be removed. */
#define SC_ERR_LIB_LUA_FREE              (OFFSET_LIB_LUA - 5)
/* -686 The lua-script dynamic library could not be initialised. */
#define SC_ERR_LIB_LUA_INITIALISE        (OFFSET_LIB_LUA - 6)
/* -687 The lua-script dynamic library could not be terminated. */
#define SC_ERR_LIB_LUA_TERMINATE         (OFFSET_LIB_LUA - 7)

/*******************************************************************************
* Define the aliases that support Unified-SCIF driver and utility legacy errors.
*******************************************************************************/

/* There are 9 title descriptions. */

/* Failure due to controller being hung and not responding. */
#define SC_ERR_CTL_HUNG                  SC_ERR_OCS_HUNG

/* Failure due to controller operations are taking too long. */
#define SC_ERR_CTL_BUSY                  SC_ERR_OCS_BUSY

/* Failure due to controller operation having bad parameters. */
#define SC_ERR_CTL_PARM                  SC_ERR_OCS_PARM

/* Failure due to controller data-structures are corrupted. */
#define SC_ERR_CTL_TRASH                 SC_ERR_OCS_TRASH

/* Failure due to lack of controller data-structures. */
#define SC_ERR_CTL_NUMBER                SC_ERR_OCS_NUMBER

/* Failure due to invalid port address. */
#define SC_ERR_CTL_PORT                  SC_ERR_OCS_PORT

/* The version number of the test bus controller is invalid. */
#define SC_ERR_CTL_TBC_VERSION           SC_ERR_TBC_VERSION

/* Failed to find the specified variable name in the board config info'. */
#define SC_ERR_BRD_VARIABLE_NAME         SC_ERR_BOARD_VARIABLE_NAME

/* Failed to find the specified device name in the board config info'. */
#define SC_ERR_BRD_DEVICE_NAME           SC_ERR_BOARD_DEVICE_NAME

/*******************************************************************************
* This is the finish of the automatically generated error offset and
* title descriptions that are derived from the 'xdserror.cfg' file.
*******************************************************************************/
