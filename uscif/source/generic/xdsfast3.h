/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xdsfast3.h
*
* DESCRIPTION
*   The public header file for Unified-SCIF-35 itself.
*
*   This file contains defines and typedef's required
*   to build debuggers, utilities and adapters.
*
*   It *no-longer* requires any of the following to be defined:
*
*     MS_WIN32
*     DSP_BIOS
*     XDS510
*     XDS560
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xdsfast3_h
#define xdsfast3_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* This header file includes additional header
* files during the transtion from USCIF35 to JTI.
*******************************************************************************/

#include "xds_error.h"

/*******************************************************************************
* The Unified-SCIF private MS_WIN32, DSP_BIOS, and POSIX 
* macros that describe the build environment are derived 
* from pre-defined macro names supported by the build tools.
*******************************************************************************/

#if !defined(_WIN32) && !defined(_TMS320C6X) && !defined(_POSIX)
#error The _WIN32/_TMS320C6X/_POSIX preprocessor macros are undefined.
#endif

#undef  MS_WIN32
#ifdef _WIN32 /* The MS Visual C++ tools maintain this macro. */
#define MS_WIN32 1
#else
#define MS_WIN32 0
#endif

#undef  DSP_BIOS
#ifdef _TMS320C6X /* The TI C6x Code-Gen tools maintain this macro. */
#define DSP_BIOS 1
#else
#define DSP_BIOS 0
#endif

#undef  POSIX
#ifdef _POSIX /* Set in makefile when building on POSIX compliant UNIX */
#define POSIX 1
#else
#define POSIX 0
#endif

/*******************************************************************************
* Enable the definition of Unified-SCIF signed and usigned integer types.
*******************************************************************************/

#define TI_SINT_TYPEDEF
#define TI_CINT_TYPEDEF

/*******************************************************************************
* Define the Unified-SCIF basic types for MS Visual C++ tools:
*
* The signed and unsigned char are 8-bits.
* The signed and unsigned short are 16-bits.
* The signed and unsigned int are 32-bits.
* The signed and unsigned long are 32-bits.
*******************************************************************************/

#if (MS_WIN32)
#if defined(TI_SINT_TYPEDEF)
typedef   signed char  SINT8;
typedef   signed short SINT16;
typedef   signed long  SINT32;
#endif /* defined(TI_SINT_TYPEDEF) */

#if defined(TI_CINT_TYPEDEF)
typedef unsigned char  CINT8;
typedef unsigned short CINT16;
typedef unsigned long  CINT32;
#endif /* defined(TI_CINT_TYPEDEF) */
#endif /* (MS_WIN32) */

/*******************************************************************************
* Define the Unified-SCIF basic types for TI C6x Code-Gen tools:
*
* The signed and unsigned char are 8-bits.
* The signed and unsigned short are 16-bits.
* The signed and unsigned int are 32-bits.
* The signed and unsigned long are 40-bit numbers in 64-bits of storage.
*******************************************************************************/

#if (DSP_BIOS)
#if defined(TI_SINT_TYPEDEF)
typedef   signed char  SINT8;
typedef   signed short SINT16;
typedef   signed int   SINT32;
#endif /* defined(TI_SINT_TYPEDEF) */

#if defined(TI_CINT_TYPEDEF)
typedef unsigned char  CINT8;
typedef unsigned short CINT16;
typedef unsigned int   CINT32;
#endif /* defined(TI_CINT_TYPEDEF) */
#endif /* (DSP_BIOS) */

/*******************************************************************************
* Define the Unified-SCIF basic types for POSIX tools:
*
* Use the standard integer types from the inttypes.h system header
* which should already be correct for the specific OS and processor.
*******************************************************************************/

#if (POSIX)
#include <inttypes.h>
#if defined(TI_SINT_TYPEDEF)
typedef int8_t  SINT8;
typedef int16_t SINT16;
typedef int32_t SINT32;
#endif /* defined(TI_SINT_TYPEDEF) */

#if defined(TI_CINT_TYPEDEF)
typedef uint8_t  CINT8;
typedef uint16_t CINT16;
typedef uint32_t CINT32;
#endif /* defined(TI_CINT_TYPEDEF) */
#endif /* (POSIX) */

/*******************************************************************************
* Define the Unified-SCIF basic types.
*******************************************************************************/

/*
 * The old logic type.
 */
typedef short  LOGIC;
#define NOT 0 /* Must be a define and not an enumeration. */
#define YES 1 /* Must be a define and not an enumeration. */

/*******************************************************************************
* The platform-specific macros for pathname attributes.
*******************************************************************************/

#if (MS_WIN32)
#define MAX_PATH_NAME_SIZE  _MAX_PATH  /* Max length of full pathname.       */
#define MAX_DRIVE_NAME_SIZE _MAX_DRIVE /* Max length of drive component.     */
#define MAX_DIR_NAME_SIZE   _MAX_DIR   /* Max length of path component.      */
#define MAX_FILE_NAME_SIZE  _MAX_FNAME /* Max length of file name component. */
#define MAX_EXT_NAME_SIZE   _MAX_EXT   /* Max length of extension component. */
#endif /* (MS_WIN32) */

#if (DSP_BIOS)
#define MAX_PATH_NAME_SIZE  256 /* Max length of full pathname.       */
#define MAX_DRIVE_NAME_SIZE 256 /* Max length of drive component.     */
#define MAX_DIR_NAME_SIZE   256 /* Max length of path component.      */
#define MAX_FILE_NAME_SIZE  256 /* Max length of file name component. */
#define MAX_EXT_NAME_SIZE   256 /* Max length of extension component. */
#endif /* (DSP_BIOS) */

#if (POSIX)
#define MAX_PATH_NAME_SIZE  256 /* Max length of full pathname.       */
#define MAX_DRIVE_NAME_SIZE 256 /* Max length of drive component.     */
#define MAX_DIR_NAME_SIZE   256 /* Max length of path component.      */
#define MAX_FILE_NAME_SIZE  256 /* Max length of file name component. */
#define MAX_EXT_NAME_SIZE   256 /* Max length of extension component. */
#endif /* (POSIX) */

/*******************************************************************************
* The intrinsic object length definitions.
*******************************************************************************/

#if (MS_WIN32)
/*
 * The intrinsic length of driver names.
 * Big enough for a full file/path name.
 */
#define SC_DRIVER_LENGTH    MAX_PATH_NAME_SIZE
#endif /* (MS_WIN32) */

#if (DSP_BIOS)
/*
 * The intrinsic length of driver names.
 * Big enough for a named feature hyphenated with a short file name
 * Lacks the much larger space required for full file/path names.
 */
#define SC_DRIVER_LENGTH   (16 + 1 + 8)
#endif /* (DSP_BIOS) */

#if (POSIX)
/*
 * The intrinsic length of driver names.
 * Big enough for a full file/path name.
 */
#define SC_DRIVER_LENGTH    MAX_PATH_NAME_SIZE
#endif /* (POSIX) */

 /*
 * The intrinsic length of controller, semaphore and share object names.
 * Consists of USCIF prefix, code prefix, driver name and port address
 * all hyphenated together to provide an object name unique across
 * installations of TI and 3rd party 510 and 560 emulators.
 */
#define SC_OBJECT_LENGTH   (8 + 1 + 8 + 1 + SC_DRIVER_LENGTH + 1 + 8)

/*******************************************************************************
* Standard macros to be included in all files.
*******************************************************************************/

/* Convert two 16-bit incrementing counts to one 32-bit incrementing count. */
#define SC_UPCOUNT_32BIT(reg1, reg0) \
    (((CINT32)(reg1) << 16) | (CINT32)(reg0))

/* Convert two 16-bit decrementing counts to one 32-bit incrementing count. */
#define SC_DNCOUNT_32BIT(max1, max0, reg1, reg0) \
    (SC_UPCOUNT_32BIT(max1, max0) - SC_UPCOUNT_32BIT((reg1 & max1), (reg0 & max0)))

enum UscifOsTypes
{
    SC_OS_UNKNOWN,
    SC_OS_WIN16, SC_OS_WIN32S, SC_OS_WIN95, SC_OS_WIN98,
    SC_OS_WINNT, SC_OS_WIN2K,  SC_OS_WINME, SC_OS_WINXP,
    SC_OS_WINVISTA
};

#define SMG_BITS08    8

#define SMG_SIZE08    1
#define SMG_SIZE16    2
#define SMG_SIZE32    4

#define SMG_BASE00    0
#define SMG_BASE08    8
#define SMG_BASE10   10
#define SMG_BASE16   16

#define SC_WORD_SIZE  2

#define SC_SUCCESS FALSE
#define SC_FAILURE TRUE

enum UscifPathTypes
{
    DR_PATH, IR_PATH
};

enum UscifEmulatorTypes
{
    SMG_EMULATE_UNKNOWN = 0,
    SMG_EMULATE_ADAPTER, /* Use 510 style emulator adapter.  */
    SMG_EMULATE_PROGRAM  /* Use 560 style emulator program.  */
};

/*******************************************************************************
* Enum definitions for specific functions.
*******************************************************************************/

/* Mnemonics for '*pEmulatorType' in SMG_GetEmulatorInfo(). */
enum UscifEmuType
{
    SC_EMU_TYPE_UNKNOWN = 0,
    SC_EMU_TYPE_XDS510,
    SC_EMU_TYPE_XDS560
};

/* Mnemonics for '*pDeviceType' in SMG_GetFamilyInfo(). */
enum UscifDeviceTypes
{
    SC_DEV_TYPE_ROOT = 0,
    SC_DEV_TYPE_SIMPLE,
    SC_DEV_TYPE_ROUTER_NEAR,
    SC_DEV_TYPE_ROUTER_FAR
};

/*******************************************************************************
* Define the Unified-SCIF basic types.
*******************************************************************************/

typedef SINT32 SC_ERROR;   /* A command error value.            */
typedef CINT16 SC_OPT;     /* A command option value.           */
typedef CINT16 SC_CMD;     /* A command index value.            */
typedef CINT16 SC_STATUS;  /* A controller status value.        */
typedef CINT16 SC_INDEX;   /* A controller index value.         */
typedef CINT16 SC_MVAL;    /* A controller memory value.        */
typedef CINT32 SC_MADD;    /* A controller memory address.      */
typedef CINT16 SC_REGVAL;  /* A controller register value.      */
typedef CINT32 SMG_PORT;   /* A Unified-SCIF port address.      */

typedef void *SMG_HANDLE;  /* A Unified-SCIF manager handle.    */
typedef void *TBC_HANDLE;  /* A Unified-SCIF controller handle. */
typedef void *POD_HANDLE;  /* A Unified-SCIF adapter handle.    */

#define MVAL_BITS  16  /* Number of bits in SC_MVAL. */
#define MVAL_DIV    4  /* Use `x >> MVAL_DIV' instead of `x / MVAL_BITS'. */
#define MVAL_MOD  0xF  /* Use `x &  MVAL_DIV' instead of `x % MVAL_BITS'. */

/*******************************************************************************
* Define the Unified-SCIF argument type.
*******************************************************************************/

typedef struct sc_args
{
    SMG_HANDLE smg_handle;  /* Manager handle.    */
    TBC_HANDLE tbc_handle;  /* Controller handle. */
    POD_HANDLE pod_handle;  /* Adapter handle.    */

    CINT32     sc_did;      /* Target device index.   */
    CINT32     sc_client;   /* Client utility or debugger index. */

    SC_OPT     sc_cmdopt;   /* Command options. */
    SC_CMD     sc_cmdind;   /* Command index.   */

    CINT32     sc_count;    /* Interest count for scans.   */
    CINT32     sc_preamble; /* Pre-amble count for scans.  */
    CINT32     sc_pstamble; /* Post-amble count for scans. */

    SMG_PORT   smg_port;    /* Port address for utilities and debuggers. */
    SMG_PORT   smg_adrs_lo; /* Port address (low bank) for adapters.     */
    SMG_PORT   smg_adrs_hi; /* Port address (high bank) for adapters.    */

    SC_MADD    sc_srcadd;   /* Address of a source string.      */
    SC_MADD    sc_dstadd;   /* Address of a destination string. */

    CINT32     sc_drlength; /* DR path length for utility scans. */
    CINT32     sc_irlength; /* IR path length for utility scans. */

    SC_MADD    sc_stabadd;  /* Address of a source translate table.      */
    SC_MADD    sc_dtabadd;  /* Address of a destination translate table. */

    SC_INDEX   sc_index;    /* Generic index.   */

    LOGIC      sc_boolean;  /* Generic boolean. */

    CINT32     sc_size;     /* Size of the legacy buffer.    */
    void      *sc_hostbuf;  /* Pointer to the legacy buffer. */

    CINT32     sc_value1;   /* Values for the extra arguments.   */
    CINT32     sc_value2;
    CINT32     sc_value3;
    CINT32     sc_value4;

    void      *sc_buffer1;  /* Pointers for the extra arguments. */
    void      *sc_buffer2;
    void      *sc_buffer3;
    void      *sc_buffer4;

} SC_ARGS;

/*******************************************************************************
* Define the Unified-SCIF export/import directions.
*******************************************************************************/

#if (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY))

#undef MANAGE_API

#if (DSP_BIOS || POSIX)
    #define MANAGE_API
#endif /* (DSP_BIOS || POSIX) */

#if (MS_WIN32)
    #if defined(SWIG)
        #define MANAGE_API
    #else
        #if defined(MANAGE_LIBRARY)
            #define MANAGE_API __declspec(dllexport)
        #else
            #define MANAGE_API __declspec(dllimport)
        #endif
    #endif
#endif /* (MS_WIN32) */

#endif /* (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY)) */

/*******************************************************************************
* Declare the Unified-SCIF functions.
*******************************************************************************/

#if (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY))

extern MANAGE_API SC_ERROR
SMG_open(SMG_HANDLE *pSmgHandle, SC_ARGS *pCmdArgs);

extern MANAGE_API SC_ERROR
SMG_close(SMG_HANDLE *pSmgHandle);

extern MANAGE_API SC_ERROR
SMG_call(SC_CMD cmdIndex, SC_ARGS *pCmdArgs);

extern MANAGE_API SC_ERROR
SMG_Version(SC_ARGS *pCmdArgs);

extern MANAGE_API SC_ERROR
SMG_OsInform(SC_ARGS *pCmdArgs);

extern MANAGE_API SC_ERROR
SMG_acquire(SMG_HANDLE smgHandle);

extern MANAGE_API SC_ERROR
SMG_release(SMG_HANDLE smgHandle);

#endif /* (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY)) */

/*******************************************************************************
* Declare the Unified-SCIF functions.
*******************************************************************************/

/* Mnemonics for ErrorType in SMG_ErrorInfo(). */
enum UscifExplainTypes
{
    /* The error number is unused. */
    SC_EXPLAIN_INVALID,
    /* The error number is used by a TI or a third-party adapter. */
    SC_EXPLAIN_ADAPT,
    /* The error number is used by USCIF or one of its utilities. */
    SC_EXPLAIN_USCIF
    /* The error numbers used by GTI/OTI/PTI are not yet supported. */
};

#define SC_EXPLAIN_TYPE        0x000F /* Mask for the error types.          */
#define SC_EXPLAIN_VERBOSE     0x0010 /* Flag to select verbose GTI output. */

/* Mnemonics for ErrorOption in SMG_ErrorInfo(). */
#define SC_EXPLAIN_DO_UNLOAD   0x0 /* Get the explanation. */
#define SC_EXPLAIN_DO_LOAD     0x1 /* Free resources used. */

#define SC_EXPLAIN_USE_NUMBER  0x0 /* Use number to lookup explanation. */
#define SC_EXPLAIN_USE_TITLE   0x2 /* Use title instead - not yet implemented. */

/* The actual SC_EXPLAIN structure in SMG_ErrorInfo(). */
typedef struct SC_EXPLAIN
{
    CINT32   ErrorType;
    CINT32   ErrorOption;

    SC_ERROR ErrorNumber;

    char *StringTitle;    /* String pointer to the error title. */
    char *StringExplain;  /* String pointer to the error explanation. */

    CINT32 LengthTitle;   /* Length of the error title.       */
    CINT32 LengthExplain; /* Length of the error explanation. */

} SC_EXPLAIN;

#if (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY))

extern MANAGE_API SC_ERROR
SMG_ErrorInfo(CINT32 *pMarshall, SC_EXPLAIN *Explain);

#endif /* (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY)) */

/*******************************************************************************
* Declare the Unified-SCIF functions.
*******************************************************************************/

#if (MS_WIN32 || POSIX)
#if (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY))

extern MANAGE_API SC_ERROR
SMG_LoadBoardData(const char *pFileName, LOGIC extraFlag,
                  CINT32 **ppMarshall, CINT32 *pSize);

extern MANAGE_API SC_ERROR
SMG_UnloadBoardData(CINT32 **ppMarshall);

extern MANAGE_API SC_ERROR
SMG_GetTargetDevice(CINT32 *pMarshall, const char *DeviceName,
                    CINT32 *pDeviceId, char *pFamilyName,
                    CINT32 *pFamilyId, CINT32 *pMainIr);

extern MANAGE_API SC_ERROR
SMG_GetDeviceByIndex(CINT32 *pMarshall, char *DeviceName,
                     CINT32  deviceId,  char *pFamilyName,
                     CINT32 *pFamilyId, CINT32 *pMainIr);

extern MANAGE_API SC_ERROR
SMG_GetEmulatorInfo(CINT32 *pMarshall, CINT32 *pEmulatorType,
                    char *pDriverString, CINT32 driverLength,
                    CINT32 *pEmulatorPort);

#endif /* (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY)) */
#endif /* (MS_WIN32 || POSIX) */

/*******************************************************************************
* Declare the Unified-SCIF functions.
*******************************************************************************/

#if (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY))

/* This value represents invalid device and subpath IDs. */
#define SMG_INVALID_ID (CINT32)-1

extern MANAGE_API SC_ERROR
SMG_CreateMasquerade(CINT32 **ppMasquerade,
                     LOGIC *pItsaMasquerade, CINT32 *pMarshall);

extern MANAGE_API SC_ERROR
SMG_DeleteMasquerade(CINT32 **ppMasquerade,
                     LOGIC *pItsaMasquerade);

extern MANAGE_API SC_ERROR
SMG_GetConfigVariable(CINT32 *pMarshall, const char *pObsolete,
                      const char *pVaryName, LOGIC *pFound,
                      LOGIC *pLogic, CINT32 *pInteger,
                      char *pString, CINT32 stringLength);

extern MANAGE_API SC_ERROR
SMG_GetFamilyFromAlias(CINT32 *pMarshall,
                       CINT32 *pAliasNumber, char *pAliasString,
                       char *pFamilyName, CINT32 familySize);

extern MANAGE_API SC_ERROR
SMG_GetDeviceBasicOnSubpath(CINT32 *pMarshall, CINT32 deviceId,
                            CINT32 *pFirstDeviceId, CINT32 *pNumberOfDevices,
                            CINT32 *pUnused1, CINT32 *pUnused2,
                            CINT32 *pParentRouterId, LOGIC *pEndOfHierarchy);

extern MANAGE_API SC_ERROR
SMG_GetDeviceInfoOnSubpath(CINT32 *pMarshall, CINT32 deviceId,
                           CINT32 *pAddress, CINT32 *pIdentify,
                           CINT32 *pPartnerId, CINT32 *pUnused2,
                           CINT32 *pNextDeviceId, LOGIC *pEndOfSubPath);

extern MANAGE_API SC_ERROR
SMG_GetSubpathInfo(CINT32 *pMarshall, CINT32 deviceId,
                   CINT32 *pAddress, LOGIC *pItsaPseudo,
                   LOGIC *pItsaDefault, LOGIC *pItsaCustom,
                   CINT32 *pUnused1,  CINT32 *pUnused2);

extern MANAGE_API SC_ERROR
SMG_GetFamilyInfo(CINT32 *pMarshall, CINT32 deviceId,
                  CINT32 *pDeviceType, CINT32 *pIrLength,
                  CINT32 *pUnused1, CINT32 *pUnused2,
                  char *pFamilyName, CINT32 familySize);

#endif /* (!defined(USE_DYNAMIC_LIBS) || defined(MANAGE_LIBRARY)) */

/*******************************************************************************
* Declare the Unified-SCIF functions.
*******************************************************************************/

#if (DSP_BIOS)

/* This function programs the 560/560Trace FPGA. */
extern SC_ERROR
TBC_FpgaProgram(
    void   *tbcHandle,
    LOGIC   fpgaForce,
    CINT32 *pFpgaLoad);

/* This function fetches the 560/560Trace cable version number. */
extern SC_ERROR
TBC_CableVersion(
    void   *tbcHandle,
    CINT32 *pCableVersion,
    LOGIC  *pFpgaErased,
    LOGIC  *pCableDisconnected,
    LOGIC  *pTraceSupported);

#endif /* (DSP_BIOS) */

/*******************************************************************************
* Define the Unified-SCIF manager command mnemonics.
*******************************************************************************/

#define SC_CMD_INDEX_MASK     0xFF
#define SC_CMD_INDEX_INVALID  0xFF

#define SC_CMD_FUNC_OPEN         0
#define SC_CMD_FUNC_CLOSE        1
#define SC_CMD_02                2
#define SC_CMD_03                3
#define SC_CMD_04                4
#define SC_CMD_05                5
#define SC_CMD_06                6
#define SC_CMD_07                7

#define SC_CMD_RESET             8
#define SC_CMD_GO_TO             9
#define SC_CMD_LOAD_FPGA        10
#define SC_CMD_STATUS           11
#define SC_CMD_REG_POLL         12
#define SC_CMD_BENCHMARK        13
#define SC_CMD_REG_READ         14
#define SC_CMD_REG_WRITE        15

#define SC_CMD_MEM_INFO         16
#define SC_CMD_17               17
#define SC_CMD_MEM_READ         18
#define SC_CMD_MEM_WRITE        19
#define SC_CMD_MEM_FILL         20
#define SC_CMD_MEM_COPY         21
#define SC_CMD_MEM_XLAT         22
#define SC_CMD_23               23

#define SC_CMD_DO_BYPASS_3      24
#define SC_CMD_DO_STATUS_3      25
#define SC_CMD_SCAN_POLL_3      26
#define SC_CMD_SCAN_FULL_3      27
#define SC_CMD_SET_PATH         28
#define SC_CMD_GET_PATH         29
#define SC_CMD_SET_FIXED        30
#define SC_CMD_31               31

#define SC_CMD_RSC_ACQUIRE      32
#define SC_CMD_RSC_RELEASE      33
#define SC_CMD_GLB_EXEC         34
#define SC_CMD_GLB_STATUS       35
#define SC_CMD_SCAN_LIST_3      36
#define SC_CMD_SCAN_MIXED_3     37
#define SC_CMD_RSC_READ         38
#define SC_CMD_RSC_WRITE        39

#define SC_CMD_DO_DEVID_3       40
#define SC_CMD_DO_AMBLE_3       41
#define SC_CMD_DO_TEST          42
#define SC_CMD_GET_REVISION     43
#define SC_CMD_GET_ERROR        44
#define SC_CMD_POD_FIDDLE       45
#define SC_CMD_POD_STATUS       46
#define SC_CMD_REVISION_STATUS  47

#define SC_CMD_PLL_INIT         48
#define SC_CMD_PLL_EXIST        49
#define SC_CMD_50               50
#define SC_CMD_PLL_CONFIG       51
#define SC_CMD_CLK_MEASURE      52
#define SC_CMD_CLK_RECORD       53
#define SC_CMD_CONFIG_MODE      54
#define SC_CMD_CONFIG_FREQ      55

#define SC_CMD_CONFIG_DATA      56
#define SC_CMD_CONFIG_OKSTALL   57
#define SC_CMD_LOGFILE_READ     58
#define SC_CMD_59               59
#define SC_CMD_60               60
#define SC_CMD_61               61
#define SC_CMD_62               62
#define SC_CMD_63               63

#define SC_CMD_MAX              64

/*******************************************************************************
* Define the controller version numbers.
*******************************************************************************/

enum UscifVersionControllerType
{
   /*
    * 0x0 = Sample TBC's that are no longer built or supported.
    *       Used in very old ISA-bus EVM's for MPSD style C30's
    *       and possibly in some very old XDS510 boards.
    * 0x1 = Standard TBC sold as catalogue parts.
    *       They are used on some XDS510 boards and in DSK's and EVM's.
    * 0x2 = Proprietary TBC-XL with a single-port memory interface.
    *       They are used in all recent XDS510 boards.
    * 0x3 = Proprietary TBC-XL with a dual-port memory interface.
    *       They are used in the initial XDS560 boards.
    */
    TBC_ITS_OBSOLETE = 0,
    TBC_ITSA_TBC,
    TBC_ITSA_EXL,
    TBC_ITSA_DPM,

   /*
    * Proprietary Nano-TBC hardware implemented using VHDL.
    * Proprietary Nano-TBC simulation implemented using C language.
    * Proprietary Femto-TBC hardware implemented using VHDL.
    * Proprietary Femto-TBC simulation implemented using C language.
    * Proprietary Tiny-SerDes hardware implemented using VHDL.
    * Proprietary Tiny-SerDes simulation implemented using C language.
    */
    TBC_ITSA_NANO_VHDL,
    TBC_ITSA_NANO_CMODEL,
    TBC_ITSA_FEMTO_VHDL,
    TBC_ITSA_FEMTO_CMODEL,
    TBC_ITSA_SERDES_VHDL,
    TBC_ITSA_SERDES_CMODEL,

    /* Reserved for future use by TI. */
    TBC_ITSA_TI_CTLR_A = 0xA,
    TBC_ITSA_TI_CTLR_B,
    TBC_ITSA_TI_CTLR_C,
    TBC_ITSA_TI_CTLR_D,
    TBC_ITSA_TI_CTLR_E,
    TBC_ITSA_TI_CTLR_F = 15,

    /* Reserved for use by third-parties. */
    TBC_ITSA_3RD_PARTY_LO = 16,
    TBC_ITSA_3RD_PARTY_HI = 31
};

/*******************************************************************************
* Define the 560-class cable version numbers.
*******************************************************************************/

enum UscifPodVersionType
{
    POD_VERSION_UNKNOWN,      /* Unknown connection or cable.               */
    POD_VERSION_DIRECT,       /* Direct connection without cable.           */
    POD_VERSION_FIXED_OLD,    /* XDS560 Revision-B fixed-use old-pll cable. */
    POD_VERSION_HYBRIDPOD,    /* XDS560 Revision-C/D Hybrid cable.          */
    POD_VERSION_MULTI_REV_C,  /* XDS560 Revision-C multi-purpose cable.     */
    POD_VERSION_TRACEPOD,     /* XDS560 Trace cable.                        */
    POD_VERSION_MULTI_REV_D,  /* XDS560 Revision-D multi-purpose cable.     */
    POD_VERSION_FIXED_NEW     /* XDS560 Revision-B fixed-use new-pll cable. */
};

/*******************************************************************************
* Define the 560-class cable sub-code numbers.
*******************************************************************************/

enum UscifPodSubCodeType
{
    POD_VERSION_SUBCODE_0, /* Used by Revision-C multi-purpose cable. */
    POD_VERSION_SUBCODE_1, /* Used by Revision-D multi-purpose cable. */
    POD_VERSION_SUBCODE_2, /* Unused. */
    POD_VERSION_SUBCODE_3  /* Unused. */
};

/*******************************************************************************
* Define the SC_CMD_GET_REVISION request numbers.
*******************************************************************************/

enum UscifGetHardwareVersionType
{
    LS_VERSION_READ_TBC_VERSION,   /* Read the controller version number.   */
    LS_VERSION_READ_TBC_LENGTH,    /* Read the controller internal length.  */
    LS_VERSION_READ_POD_VERSION,   /* Read the cable+pod version number.    */
    LS_VERSION_READ_POD_CAPABLE,   /* Read the cable+pod capability number. */
    LS_VERSION_READ_FPGA_EMULATOR, /* Read the controller emulator FPGA version. */
    LS_VERSION_READ_FPGA_CLIENT,   /* Read the controller client FPGA version.   */
    LS_VERSION_READ_FPGA_SERVER,   /* Read the controller server FPGA version.   */

    LS_VERSION_READ_MAX_INDEX /* The total number of version read indexes. */
};

/*******************************************************************************
* Define the version number reserved for erased FPGA/CPLD.
*******************************************************************************/

#define LS_FPGA_VERSION_ERASED (uint16_t)0

/*******************************************************************************
* The Unified-SCIF other stuff.
*******************************************************************************/

#define SMG_BAD_HANDLE   NULL
#define TBC_BAD_HANDLE   NULL
#define POD_BAD_HANDLE   NULL

/*******************************************************************************
* The Unified-SCIF operating modes.
*******************************************************************************/

/* Options that control the operating mode during SMG_open(). */
#define SMG_CLIENT_TYPE_MASK (SC_OPT)0x000F
#define SMG_RECIPE_CLIENT    (SC_OPT)0x0000
#define SMG_MANY_CLIENT      (SC_OPT)0x0001
#define SMG_SNGL_CLIENT      (SC_OPT)0x0002
#define SMG_ZERO_CLIENT      (SC_OPT)0x0004
#define SMG_UNUSED_CLIENT    (SC_OPT)0x0008

/* Options that control the operating mode during SMG_open(). */
#define SMG_HNDL_ALWAYS     (SC_OPT)0x0100

/* Options that control the operating mode during SMG_open(). */
#define SMG_ONLY_MASK       (SC_OPT)0x0600
#define SMG_ONLY_HARD       (SC_OPT)0x0200
#define SMG_ONLY_SOFT       (SC_OPT)0x0400

/* Options that control programming an FPGA(). */
#define SMG_FPGA_MASK       (SC_OPT)0x00F0
#define SMG_FPGA_PROGRAM    (SC_OPT)0x0010
#define SMG_FPGA_FORCE      (SC_OPT)0x0020
#define SMG_FPGA_EXTERNAL   (SC_OPT)0x0040
#define SMG_FPGA_ERASE      (SC_OPT)0x0080

/*******************************************************************************
* The Unified-SCIF client indexes.
*******************************************************************************/

enum UscifClientNames
{
    SC_OTIS_CLIENT = 0, /* Generic value for PTI's. */
    SC_USCIF_CLIENT,    /* API calls generated by USCIF itself. */
    SC_TI_CUI_CLIENT,   /* API calls generated by TI's CUI. */
    SC_TI_GUI_CLIENT,   /* API calls generated by TI's GUI. */
    SC_TI_CCS_CLIENT,   /* API calls generated by TI's CCS. */
    SC_GTIRESET_CLIENT, /* Generic value for GTI reset's.   */
    SC_DBGJTAG_CLIENT,  /* API calls generated by DBGJTAG.  */
    SC_RSTJTAG_CLIENT   /* API calls generated by RSTJTAG.  */
};

#define SMG_CLIENT_NAME_MASK       (CINT32)0x0000000F

#define SMG_RECIPE_HARD_OPEN       (CINT32)0x00000010
#define SMG_RECIPE_SOFT_OPEN       (CINT32)0x00000020

#define SMG_RECIPE_MEMORY_INIT     (CINT32)0x00000040
#define SMG_RECIPE_LOCK_ACQU_RELE  (CINT32)0x00000080
#define SMG_RECIPE_LOCK_WAIT_FIXED (CINT32)0x00000100
#define SMG_RECIPE_CONFIG_FREQ     (CINT32)0x00000200

#define SMG_RECIPE_DEFAULT_OPEN    (CINT32)0x00000400
#define SMG_RECIPE_DEFAULT_CLOSE   (CINT32)0x00000800
#define SMG_RECIPE_CUSTOM_OPEN     (CINT32)0x00001000
#define SMG_RECIPE_CUSTOM_CLOSE    (CINT32)0x00002000
#define SMG_RECIPE_ROUTER_OPEN     (CINT32)0x00004000
#define SMG_RECIPE_ROUTER_CLOSE    (CINT32)0x00008000
#define SMG_RECIPE_ROUTER_REOPEN   (CINT32)0x00010000

#define SMG_RECIPE_PLL_INIT        (CINT32)0x00020000
#define SMG_RECIPE_PLL_TERM        (CINT32)0x00040000
#define SMG_RECIPE_JTAG_INIT       (CINT32)0x00080000
#define SMG_RECIPE_JTAG_TERM       (CINT32)0x00100000

#define SMG_RECIPE_AUTO_LOAD_FPGA  (CINT32)0x00200000
#define SMG_RECIPE_HARDWARE_INIT   (CINT32)0x00400000
#define SMG_RECIPE_MODIFY_CLKMODE  (CINT32)0x00800000
#define SMG_RECIPE_SCAN_2CHECK     (CINT32)0x01000000
#define SMG_RECIPE_MMRY_2CHECK     (CINT32)0x02000000
#define SMG_RECIPE_CALL_2CHECK     (CINT32)0x04000000
#define SMG_RECIPE_SELECT_DEVICE   (CINT32)0x08000000
#define SMG_RECIPE_HONEST_RESET    (CINT32)0x10000000
#define SMG_RECIPE_LOG_FILES       (CINT32)0x20000000

/*******************************************************************************
* The SCIF controller, cable and pod status codes.
*******************************************************************************/
#define SC_BUSY_STATUS        0x0003 /* The controller is busy.    */
#define SC_POWER_STATUS       0x0010 /* The target has power.      */
#define SC_EVT0_STATUS        0x0020 /* The value of the EMU0 pin. */
#define SC_EVT1_STATUS        0x0040 /* The value of the EMU1 pin. */

#define SC_CLOCK_STATUS       0x0080 /* The target has a clock.    */
#define SC_NEAR_STATUS        0x0100 /* The cable is connected to the emulator. */
#define SC_FAR_STATUS         0x0200 /* The cable is connected to the target.   */

#define SC_CLOCK_UNKNOWN      0x0400 /* Don't know if target has a clock. */
#define SC_NEAR_UNKNOWN       0x0800 /* Don't know if cable is connected to emulator. */
#define SC_FAR_UNKNOWN        0x1000 /* Don't know if cable is connected to target.   */

/*******************************************************************************
* The SCIF JTAG status codes returned by go-to commands.
*******************************************************************************/

#define SC_JTAG_STATUS_RESET  0x0000
#define SC_JTAG_STATUS_IDLE   0x0002

#define SC_JTAG_STATUS_DSHIFT 0x0004
#define SC_JTAG_STATUS_ISHIFT 0x0005

#define SC_JTAG_STATUS_DPAUSE 0x0006
#define SC_JTAG_STATUS_IPAUSE 0x0007

#define SC_JTAG_STATUS_DTEMP  0x0001
#define SC_JTAG_STATUS_ITEMP  0x0003

/*******************************************************************************
* The Unified-SCIF command options.
*******************************************************************************/

/* Major command wake state options */
/* This applies only to do-bypass, do-status */
/* scan-poll and scan-full commands.         */
#define SC_STATE_WAKE_MASK    (SC_OPT)0x0008

#define SC_WAKE_DSCAN_DPAUSE  (SC_OPT)0x0000
#define SC_WAKE_ISCAN_IPAUSE  (SC_OPT)0x0008

/* Major command end state options */
/* This applies only to go-to do-bypass, do-status */
/* scan-poll and scan-full commands.               */
#define SC_STATE_END_MASK     (SC_OPT)0x0007

#define SC_STATE_RESET        (SC_OPT)0x0000
#define SC_STATE_IDLE         (SC_OPT)0x0002
#define SC_STATE_DPAUSE       (SC_OPT)0x0006
#define SC_STATE_IPAUSE       (SC_OPT)0x0007

/*******************************************************************************
* The Unified-SCIF command options that apply to many commands.
*******************************************************************************/

/* This applies only to memory-rd/wr/fill/copy, memory-xlat and scan-full. */
/* This does not apply to do-bypass, do-status and scan-poll.              */
#define SC_MEM_VOLATILE       (SC_OPT)0x0000
#define SC_MEM_FIXED          (SC_OPT)0x0010

/* This currently applies only to global-add. */
#define SC_MEM_LOCAL          (SC_OPT)0x0000
#define SC_MEM_HOST           (SC_OPT)0x0020

/* This applies only to version-info and reset. */
#define SC_CMD_VERBOSE        (SC_OPT)0x0020

/* These apply only to go-to and reset. */
#define SC_TRST_PIN_ASSERT    (SC_OPT)0x0040
#define SC_TRST_PIN_NEGATE    (SC_OPT)0x0080

/*******************************************************************************
* The Unified-SCIF bypass, status, poll and scan command options.
*******************************************************************************/

/*
 * Command options used only by the
 * one scan command: SC_CMD_DO_AMBLE_3.
 */
#define SC_AMBLE_GET_INFO     (SC_OPT)0x1000

#define SC_AMBLE_USE_ZERO     (SC_OPT)0x0001
#define SC_AMBLE_USE_AMBLE    (SC_OPT)0x0002
#define SC_AMBLE_USE_MAIN     (SC_OPT)0x0004
#define SC_AMBLE_USE_OTHER    (SC_OPT)0x0008

enum UscifAmbleInfo
{
    SC_AMBLE_PRE_DR,   SC_AMBLE_POS_DR,
    SC_AMBLE_PRE_IR,   SC_AMBLE_POS_IR,
    SC_AMBLE_MAIN_DR,  SC_AMBLE_MAIN_IR,
    SC_AMBLE_ONCE_END, SC_AMBLE_MANY_END,
    SC_AMBLE_NUMBER
};

/*
 * Command options used only by the
 * one scan command: SC_CMD_DO_DEVID_3.
 */
#define SC_DEVID_GET_INFO     (SC_OPT)0x1000
#define SC_DEVID_ROUTE_LIST   (SC_OPT)0x2000

/*
 * Command options used only by the
 * one scan command: SC_CMD_BYPASS_3.
 */
#define SC_BYPASS_MASK        (SC_OPT)0x0100

#define SC_BYPASS_ZERO        (SC_OPT)0x0000 /* All-zeros during main scan. */
#define SC_BYPASS_ONES        (SC_OPT)0x0100 /* All-ones during main scan.  */

/*
 * Command options used only by the
 * one scan command: SC_CMD_SCAN_POLL_3.
 */
#define SC_POLL_SUCCESS_MASK  (SC_OPT)0x0100

#define SC_POLL_HIT           (SC_OPT)0x0000 /* Finish poll when match is found.  */
#define SC_POLL_MISS          (SC_OPT)0x0100 /* Finish poll when mis-match found. */

#define SC_POLL_TIMEOUT_MASK  (SC_OPT)0x0600

#define SC_POLL_ONCE          (SC_OPT)0x0000 /* Instant timeout - after one command. */
#define SC_POLL_EVER          (SC_OPT)0x0200 /* No timeout - poll forever */
#define SC_POLL_FAST          (SC_OPT)0x0400 /* Fast timeout - 1/50 second typical. */
#define SC_POLL_SLOW          (SC_OPT)0x0600 /* Slow timeout - 1/2 second typical.  */

#define SC_POLL_RATIO         25 /* Relative timeouts for slow and fast polling. */

/*
 * Command options used only by the
 * one scan command: SC_CMD_SCAN_FULL_3.
 */
#define SC_SCAN_SEND_MASK     (SC_OPT)0x0300
#define SC_SCAN_RECV_MASK     (SC_OPT)0x0400
#define SC_SCAN_FULL_MASK     (SC_OPT)0x0700

#define SC_SEND_ZERO          (SC_OPT)0x0000
#define SC_SEND_ONES          (SC_OPT)0x0100
#define SC_SEND_CIRCULATE     (SC_OPT)0x0200
#define SC_SEND               (SC_OPT)0x0300

#define SC_RECEIVE_SEND_ZERO  (SC_OPT)0x0400
#define SC_RECEIVE_SEND_ONES  (SC_OPT)0x0500
#define SC_RECEIVE_CIRCULATE  (SC_OPT)0x0600
#define SC_RECEIVE_SEND       (SC_OPT)0x0700

/*
 * Command options used only by the
 * one scan command: SC_CMD_SCAN_LIST_3.
 */
#define SC_GLOBAL_SEND_MASK   (SC_OPT)0x0100

#define SC_GLOBAL_DATA_ZERO   (SC_OPT)0x0000
#define SC_GLOBAL_DATA_ONES   (SC_OPT)0x0100

/*
 * Scan command options usable by several commands.
 */
#define SC_SCAN_OPTION_MASK   (SC_OPT)0xF8C0

/* Don't use pre-ambles.   */
#define SC_SCAN_NO_PRE        (SC_OPT)0x0040
/* Don't use post-ambles.   */
#define SC_SCAN_NO_POS        (SC_OPT)0x0080
/* Don't use pre/post-ambles.   */
#define SC_SCAN_NO_AMBLE      (SC_OPT)0x1000

/* Don't use guaranteed capture. */
#define SC_SCAN_NO_CAPTURE    (SC_OPT)0x0800
/* Do use ice-maker flushing. */
#define SC_SCAN_FLUSH         (SC_OPT)0x2000
/* Disable buffer adjustments.  */
#define SC_SCAN_DIY_ADJUST    (SC_OPT)0x4000

/* Data may be returned. */
#define SC_SCAN_RECV_ALSO     (SC_OPT)0x0000
/* Data is not returned. */
#define SC_SCAN_SEND_ONLY     (SC_OPT)0x8000

/*******************************************************************************
* The Unified-SCIF other command options.
*******************************************************************************/

/* The fpga load command options. */
#define SC_FPGA_LOAD_MASK     (SC_OPT)0x3000

#define SC_FPGA_LOAD_TOTAL    (SC_OPT)0x0000
#define SC_FPGA_LOAD_BEFOR    (SC_OPT)0x1000
#define SC_FPGA_LOAD_DATA     (SC_OPT)0x2000
#define SC_FPGA_LOAD_AFTER    (SC_OPT)0x3000

/* The get error command options. */
#define SC_ERROR_ERASE        (SC_OPT)0x0000 /* Get the error then erase it. */
#define SC_ERROR_RETAIN       (SC_OPT)0x1000 /* Get the error and retain it. */

/* The go-to command options. */
#define SC_STATE_MODE_MASK    (SC_OPT)0x7000

#define SC_STATE_RUN_1_CLK    (SC_OPT)0x1000
#define SC_STATE_GET_STATE    (SC_OPT)0x2000

/* The reset command options. */
#define SC_RESET_MASK         (SC_OPT)0x7000

#define SC_RESET_SOFT         (SC_OPT)0x1000 /* Only perform a soft reset. */
#define SC_RESET_HARD         (SC_OPT)0x2000 /* Only perform a hard reset. */
#define SC_RESET_HIDDEN       (SC_OPT)0x4000 /* Hard reset does not affect TRST. */

/* The global status command options. */
#define SC_GLB_STATUS_MASK    (SC_OPT)0x7000 /* Already used by PTI. */

#define SC_STATUS_PUT         (SC_OPT)0x1000 /* Not yet used by PTI. */
#define SC_STATUS_GET         (SC_OPT)0x2000 /* Not yet used by PTI. */
#define SC_STATUS_GLOBAL      (SC_OPT)0x4000 /* Already used by PTI. */

/* The global execute command options. */
#define SC_GLB_EXEC_MASK      (SC_OPT)0x7000 /* Already used by PTI. */

#define SC_GLB_CLR            (SC_OPT)0x1000 /* Not yet used by PTI. */
#define SC_GLB_ADD            (SC_OPT)0x2000 /* Already used by PTI. */
#define SC_GLB_START          (SC_OPT)0x4000 /* Already used by PTI. */

/* The resource acquire and release command options. */
#define SC_RSC_SHARE          (SC_OPT)0x8000
#define SC_RSC_EXTEND         (SC_OPT)0x4000
#define SC_RSC_OWNER          (SC_OPT)0x2000

/*******************************************************************************
* The scan-loop options.
* These are used with sc_size instead of sc_cmdopt.
*
* Typically do this to repeatidly scan many differing values:
*   sc_size  = (repeats << SMG_LOOP_SHIFT);
*   sc_size |= SMG_LOOP_ENABLE | SMG_LOOP_DIFFER | SMG_SIZExx;
*
* Typically do this to repeatidly scan a single constant value:
*   sc_size  = (repeats << SMG_LOOP_SHIFT);
*   sc_size |= SMG_LOOP_ENABLE | SMG_LOOP_CONST | SMG_SIZExx;
*
*******************************************************************************/

#define SMG_LOOP_MASK    0x0018
#define SMG_SIZE_MASK    0x0007

#define SMG_LOOP_CONST   0x0010
#define SMG_LOOP_DIFFER  0x0000

#define SMG_LOOP_ENABLE  0x0008
#define SMG_LOOP_DISABLE 0x0000

#define SMG_LOOP_SHIFT   16
#define SMG_LOOP_MAX     65535

/*******************************************************************************
* The Unified-SCIF log-file command options.
*******************************************************************************/

#define SC_LOGFILE_SELECT_RESET  0x0000000
#define SC_LOGFILE_SELECT_PLL    0x0000001
#define SC_LOGFILE_SELECT_TEST   0x0000002

/*******************************************************************************
* The Unified-SCIF pod fiddle options.
*******************************************************************************/

#define SC_SIGNAL_FIDDLE         0x0000000
#define SC_EMUPIN_FIDDLE         0x0000001
#define SC_SYSRST_FIDDLE         0x0000002

/*******************************************************************************
* The Unified-SCIF loop-back command options.
*******************************************************************************/

#define SC_PODLOOP_BACK_MASK     0x0000001F
#define SC_PODLOOP_BACK_SHIFT             0

#define SC_PODLOOP_BACK_NONE     0x00000000 /* The loopback is disabled.               */
#define SC_PODLOOP_BACK_DATA     0x00000001 /* The loopback of TDO/TDI is enabled.     */
#define SC_PODLOOP_BACK_CLOCK    0x00000002 /* The loopback of TCLKO/TCLKR is enabled. */
#define SC_PODLOOP_BACK_TOTAL    0x00000003 /* The loopback of both data and clock is enabled and the
                                             detection of power-loss and cable-break is disabled. */
#define SC_PODLOOP_BACK_ACCESS   0x00000010

/*******************************************************************************
* The Unified-SCIF boot-mode command options.
*******************************************************************************/

/*
 * Note that Within the JTAG nTRST boot-mode and Power-on-Reset bootmode  
 * there is no ability to separately set/clear an event flag,
 * or enable/disable boot-mode output, or apply boot-mode values.
 * Application software must remember the prior state so that
 * un-modified old values can be re-applied with each modified item.
 */

#define SC_JTAGBOOT_MODE_MASK    0x0000001F
#define SC_JTAGBOOT_MODE_SHIFT            0

#define SC_JTAGBOOT_MODE_EMU0    0x00000001 /* EMU0 value output during boot-mode.  */
#define SC_JTAGBOOT_MODE_EMU1    0x00000002 /* EMU1 value output during boot-mode.  */
#define SC_JTAGBOOT_MODE_FORCE   0x00000004 /* 1 forces and 0 clears event flag.    */
#define SC_JTAGBOOT_MODE_ENABLE  0x00000008 /* 1 enables, 0 disables + applies hiz. */
#define SC_JTAGBOOT_MODE_ACCESS  0x00000010

#define SC_POWERBOOT_MODE_MASK   0x00001F00
#define SC_POWERBOOT_MODE_SHIFT           8

#define SC_POWERBOOT_MODE_EMU0   0x00000100 /* EMU0 value output during boot-mode.  */
#define SC_POWERBOOT_MODE_EMU1   0x00000200 /* EMU1 value output during boot-mode.  */
#define SC_POWERBOOT_MODE_FORCE  0x00000400 /* 1 forces and 0 clears event flag.    */
#define SC_POWERBOOT_MODE_ENABLE 0x00000800 /* 1 enables, 0 disables + applies hiz. */
#define SC_POWERBOOT_MODE_ACCESS 0x00001000

#define SC_EMUOUTPUT_MODE_MASK   0x001F0000
#define SC_EMUOUTPUT_MODE_SHIFT          16

#define SC_EMUOUTPUT_MODE_EMU0   0x00010000 /* EMU0 value output during background. */
#define SC_EMUOUTPUT_MODE_EMU1   0x00020000 /* EMU1 value output during background. */
/*      SC_EMUOUTPUT_MODE_FORCE                background does not have event flag. */
#define SC_EMUOUTPUT_MODE_ENABLE 0x00080000 /* 1 enables, 0 disables + applies hiz. */
#define SC_EMUOUTPUT_MODE_ACCESS 0x00100000

/*******************************************************************************
* The Unified-SCIF system-reset command options.
*******************************************************************************/

#define SC_SYSTEM_RESET_MASK     0x0000001F
#define SC_SYSTEM_RESET_SHIFT             0

#define SC_SYSTEM_RESET_NOTHING  0x00000000
#define SC_SYSTEM_RESET_ASSERT   0x00000001
#define SC_SYSTEM_RESET_NEGATE   0x00000002
#define SC_SYSTEM_RESET_PULSE    0x00000003

#define SC_SYSTEM_RESET_ACCESS   0x00000010

/*******************************************************************************
* The Unified-SCIF register and block-mode command options.
*******************************************************************************/

/* The register polling options. */
#define SC_POLL_TBC_READY     (SC_OPT)0x0001 /* The TBC is ready for access.  */

#define SC_POLL_WR_BFR_OK     (SC_OPT)0x0100 /* Write buffer has 1 or 2 gaps. */
#define SC_POLL_WR_BFR_MPTY   (SC_OPT)0x0200 /* Write buffer has 2 gaps.      */
#define SC_POLL_RD_BFR_OK     (SC_OPT)0x1000 /* Read buffer has 1 or 2 words. */
#define SC_POLL_RD_BFR_FULL   (SC_OPT)0x2000 /* Read buffer has 2 words.      */

#define SC_POLL_TBC_CMD_DEAD  (SC_OPT)0x0010 /* All TBC commands are dead.    */
#define SC_POLL_EXL_CMD_DEAD  (SC_OPT)0x0020 /* All EXL commands are dead.    */
#define SC_POLL_MOVE_CMD_DEAD (SC_OPT)0x0040 /* All SM commands are dead.     */
#define SC_POLL_XLAT_CMD_DEAD (SC_OPT)0x0080 /* All SP commands are dead.     */

/* The register access options. */
#define SC_REG_ACCESS_MASK    (SC_OPT)0x3000

#define SC_REG_RGST_SINGLE    (SC_OPT)0x0000 /* Multiple using just one register.  */
#define SC_REG_RGST_ARRAY     (SC_OPT)0x1000 /* Multiple using adjacent registers. */

#define SC_REG_DATA_SINGLE    (SC_OPT)0x0000 /* Multiple using just one data value.  */
#define SC_REG_DATA_ARRAY     (SC_OPT)0x2000 /* Multiple using adjacent data values. */

/* The down-load and up-load options. */
#define SC_LOAD_MASK          (SC_OPT)0x1000

#define SC_LOAD_DATA_ONLY     (SC_OPT)0x0000 /* Data part of an upload or download. */
#define SC_LOAD_DATA_PLUS     (SC_OPT)0x1000 /* Data part of an upload or download. */

/* The buffer transfer options. */
#define SC_XFER_SIZE_MASK     (SC_OPT)0x0FFF
#define SC_XFER_DATA_MASK     (SC_OPT)0x3000
#define SC_XFER_CMND_MASK     (SC_OPT)0xC000

#define SC_XFER_DATA_NONE     (SC_OPT)0x0000 /* Don't access any scan-path data.  */
#define SC_XFER_DATA_SEND     (SC_OPT)0x1000 /* Write scan-path data to buffers.  */
#define SC_XFER_DATA_RECV     (SC_OPT)0x2000 /* Read scan-path data from buffers. */
#define SC_XFER_DATA_BOTH     (SC_OPT)0x3000 /* Write and read scan-path data.    */

#define SC_XFER_CMND_BYPS     (SC_OPT)0x0000 /* Its data for a bypass command. */
#define SC_XFER_CMND_STTS     (SC_OPT)0x4000 /* Its data for a status command. */
#define SC_XFER_CMND_POLL     (SC_OPT)0x8000 /* Its data for a poll command.   */
#define SC_XFER_CMND_SCAN     (SC_OPT)0xC000 /* Its data for a scan command.   */

/*******************************************************************************
* EMU[1] and EMU[0] pin definitions.
*******************************************************************************/

/* Event pin edge selection. */
#define SC_EVT_FALLING_EDGE   (SC_OPT)0x0000 /* Select falling edges. */
#define SC_EVT_RISING_EDGE    (SC_OPT)0x0040 /* Select rising edges.  */

/* Event pin selection. */
#define SC_SEL_EVT0                        0 /* Select the EMU0 pin. */
#define SC_SEL_EVT1                        1 /* Select the EMU1 pin. */

/* Benchmark timer selection for XDS560. */
/* This is now ignored as timer selection is via event pin selection. */
#define SC_SEL_TIMER0                      0 /* Select Timer 0. */
#define SC_SEL_TIMER1                      1 /* Select Timer 1. */

/*******************************************************************************
* Link-Delay and Link-Delta definitions.
* The maximum acceptable delay is reduced from
* the Legacy-TBC's 32 to the Nano-TBC's 10.
* The maximum delta is the maximum amount
* by with the TBC modifies a fixed link-delay.
*******************************************************************************/

#define TBC_CABLE_DELTA_MAXIMUM     1

#define TBC_CABLE_DELAY_MAXIMUM    10
#define TBC_CABLE_DELAY_INVALID    -1
#define TBC_CABLE_DELAY_LIMITS(x) \
        ((x) <= (TBC_CABLE_DELAY_MAXIMUM - TBC_CABLE_DELTA_MAXIMUM))

/*******************************************************************************
* End-Delay definitions.
*******************************************************************************/

#define TBC_END_DELAY_MAXIMUM     0xFFFF
#define TBC_END_DELAY_INVALID    -1
#define TBC_END_DELAY_LIMITS(x) ((x) <= TBC_END_DELAY_MAXIMUM)

/*******************************************************************************
* The resource acquire and release meemonics.
*******************************************************************************/

#define SC_RESOURCE_TOTAL  32     /* The total number of resource items.  */

#define SC_RESOURCE_ITEM(x) ((SC_INDEX)(1<<(x))) /* Lock or Unlock resource X. */

/*******************************************************************************
* Provide compatibility with obsolete nmemonics used in PTI code.
*******************************************************************************/

#ifndef SIZE16
#define SIZE16              SMG_SIZE16
#endif
#ifndef SIZE32
#define SIZE32              SMG_SIZE32
#endif

#ifndef BASE10
#define BASE10              SMG_BASE10
#endif
#ifndef BASE16
#define BASE16              SMG_BASE16
#endif

#define SC_LCK_EVT0         SC_RESOURCE_ITEM(0)
#define SC_LCK_EVT1         SC_RESOURCE_ITEM(1)
#define SC_LCK_EVT2         SC_RESOURCE_ITEM(2)
#define SC_LCK_EVT3         SC_RESOURCE_ITEM(3)

#define SC_SCAN_NAKED       SC_SCAN_NO_AMBLE

#define SC_CMD_STATE        SC_CMD_GO_TO
#define SC_CMD_CNT2_RW      SC_CMD_BENCHMARK
#define SC_CMD_CNT_WRITE    SC_CMD_BENCHMARK

#define SC_TPOWER_STATUS    SC_POWER_STATUS
#define SC_USER_EVT0_STATUS SC_EVT0_STATUS
#define SC_USER_EVT1_STATUS SC_EVT1_STATUS

#define SC_CLK_STATUS       SC_CLOCK_STATUS
#define SC_NEAR_CBL_STATUS  SC_NEAR_STATUS
#define SC_FAR_CBL_STATUS   SC_FAR_STATUS

#define SC_CLK_UNKNOWN      SC_CLOCK_UNKNOWN
#define SC_NEAR_CBL_UNKNOWN SC_NEAR_UNKNOWN
#define SC_FAR_CBL_UNKNOWN  SC_FAR_UNKNOWN

#define SC_XDSRESET_CLIENT  SC_GTIRESET_CLIENT

/*
 * These are references in OTI and PTI of legacy mnemonics.
 */
#define TBC_CMD_REVISION_STATUS SC_CMD_REVISION_STATUS
#define TBC_CMD_RESET           SC_CMD_RESET

/*
 * These are referenced in OTI's UTL_smg_database(()
 * but no-longer generated by USCIF35.
 */
#define SC_ERR_SHM_VERSION  (SC_ERR_BAD_GENERIC - 1)
#define SC_ERR_SHM_TRASH    (SC_ERR_BAD_GENERIC - 2)
#define SC_ERR_SHM_SIZE     (SC_ERR_BAD_GENERIC - 3)

/*
 * Define the mnemonics for the sections of the board configuration.
 */
#define ADAPTER_SECTION "ADAPT"   /* Sourceless-EPK features. */
#define CONTROL_SECTION "USCIF"   /* Unified-SCIF features.   */
#define UTILITY_SECTION "UTILITY" /* xdsprobe, xdsreset, ...  */
#define DRIVERS_SECTION "OTIS"    /* GTI, OTI, PTI drivers.   */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* xdsfast3_h */

/* -- End Of File -- */
