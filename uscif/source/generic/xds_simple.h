/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_simple.h
*
* DESCRIPTION
*   A set of simple character string and numeric functions.
*
* NOTES
*   These are *very simple* functions:
*
*   1) No USCIF typedef's are involved.
*   2) No file access is involved.
*   3) No memory allocation is involved.
*   4) No dependancy on the build environment.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xds_simple_h
#define xds_simple_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* These are the characters and strings used to
* parse config' files and command line options.
*******************************************************************************/

#define META_COMMENT    '#'

#define META_OFFSET     '^'
#define META_TITLE      '!'
#define META_VARIABLE   '$'
#define META_PACKAGE    '?'
#define META_FAMILY     '%'
#define META_ALIAS      '~'
#define META_DEVICE     '@'
#define META_SUBPATH    '&'

#define META_ANYNAME    "^!?$%~@&"

#define META_SEPARATOR  '.'
#define META_TERMINATOR '/'

#define META_START      '<'
#define META_JOIN       '|'
#define META_FINISH     '>'

#define META_INCLUDE    '+'
#define META_EXCLUDE    '-'
#define META_LISTING    ':'

#define META_PLUSMINUS      "+-"
#define META_PLUSMINUSCOLON "+-:"

/*******************************************************************************
* The public functions.
*******************************************************************************/

extern logic_t
unsign2true(uint32_t number);
extern logic_t
unsign2false(uint32_t number);

extern logic_t
strncpy_safe(char *dest, const char *source, uint32_t size);
extern logic_t
strncat_safe(char *dest, const char *source, uint32_t size);

extern char *
str_2_upper(char *string);
extern char *
str_2_lower(char *string);

extern int
strcmp_nocase(const char *string1, const char *string2);
extern int
strncmp_nocase(const char *string1, const char *string2, uint32_t number);

extern const char *
arrcmp_nocase(char *string, const char *array[], uint32_t *index);
extern const char *
arrncmp_nocase(char **string, const char *array[], uint32_t *index, uint32_t number);

extern char *
format_unsign(uint32_t number);
extern char *
format_signed(int32_t number);

extern void
field_insert(uint32_t *List, uint32_t Size, uint32_t Base, uint16_t *Buffer);
extern void
field_extract(uint32_t *List, uint32_t Size, uint32_t Base, uint16_t *Buffer);

extern uint8_t
illegal_identifier(char *name, uint32_t length, uint32_t max);

extern char *
convert_to_text(int32_t number, uint32_t base);

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* xds_simple_h */

/* -- END OF FILE -- */
