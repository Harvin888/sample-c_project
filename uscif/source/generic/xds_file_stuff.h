/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_file_stuff.h
*
* DESCRIPTION
*   The header file for miscellaneous file name related mnemonics.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xds_file_stuff_h
#define xds_file_stuff_h /* Defines a mnemonic specific to this file. */

/*******************************************************************************
* Define the Unified-SCIF constants for library file extensions.
*******************************************************************************/

#if (MS_WIN32)
#define LS_LIB ".dll"
#endif /* (MS_WIN32) */

#if (DSP_BIOS)
#define LS_LIB ".dll"
#endif /* (DSP_BIOS) */

#if (POSIX)
#define LS_LIB ".so"
#endif /* (POSIX) */

/*******************************************************************************
* Define the Unified-SCIF constants for file names.
*******************************************************************************/

#define LS_TEXT_FILE_READ        "r"
#define LS_TEXT_FILE_WRITE       "w"
#define LS_TEXT_FILE_APPEND      "a"

#define LS_BINARY_FILE_READ      "rb"
#define LS_BINARY_FILE_WRITE     "wb"
#define LS_BINARY_FILE_APPEND    "ab"

#define LS_DEFAULT_TARGET_NAME   "cpu"

#define LS_SOURCE_EXTENSION      ".c"
#define LS_HEADER_EXTENSION      ".h"
#define LS_TEXT_EXTENSION        ".txt"

#define LS_ALTERA_TEXT_EXTENSION ".ttf"
#define LS_ALTERA_BNRY_EXTENSION ".rbf"

#define LS_PROGRAM_EXTENSION     ".out"
#define LS_ADAPTER_EXTENSION     LS_LIB

#define LS_REDIRECT_EXTENSION    ".txt"
#define LS_BOARDCFNGR_EXTENSION  ".cfg"
#define LS_BOARDMARSH_EXTENSION  ".dat"
#define LS_BOARDEXTRA_EXTENSION  ".cfg"
#define LS_LUASCRIPT_EXTENSION   ".lua"

#define LS_DEFAULT_REDIRECT_NAME    "" /* An empty string. */
#define LS_DEFAULT_LUASTART_NAME    "start.lua"
#define LS_DEFAULT_LUAFINISH_NAME   "finish.lua"

#define LS_DEFAULT_FPGASRC_NAME     "fpgadata.ttf"
#define LS_DEFAULT_FPGADST_NAME     "fpgadata.c"

#define LS_DEFAULT_INPUT_NAME       "input.txt"
#define LS_DEFAULT_OUTPUT_NAME      "output.txt"

#define LS_DEFAULT_CONFIGURE_NAME   "board.cfg" /* The legacy name. */
#define LS_DEFAULT_PROGRAM_NAME     "xds560.out"
#define LS_DEFAULT_KERNEL_NAME      "xdsfast1"

#define LS_DEFAULT_PORT_ADDRESS     "0x0"

#define LS_LEGACY_ADAPTER_NAME      ("xds510"   LS_LIB)
#define LS_MODERN_ADAPTER_NAME      ("xdsbhpci" LS_LIB)
#define LS_CMODEL_ADAPTER_NAME      ("xds100pp" LS_LIB)

#define LS_LEGACY_DRIVER_NAME       ("xds8990tbc" LS_LIB)
#define LS_MODERN_DRIVER_NAME       ("xdsnanotbc" LS_LIB)
#define LS_CMODEL_DRIVER_NAME       ("xdszerotbc" LS_LIB)

#define LS_EMPTY_VARIABLE_NAME       "" /* An empty string. */
#define LS_EMPTY_CONFIGURE_NAME      "" /* An empty string. */

#define LS_REQUEST_SKIP_NAME         "0"

#define LS_DEFAULT_FILENAME_IN  "input.txt"
#define LS_DEFAULT_FILENAME_OUT "output.txt"

#define LS_USCIF_ERROR_CFG   "xdserror.cfg"  /* The error configuration file.  */
#define LS_USCIF_FAMILY_CFG  "xdsfamily.cfg" /* The family configuration file. */
#define LS_USCIF_ALIAS_CFG   "xdsalias.cfg"  /* The alias configuration file.  */

#define LS_USCIF_PROBE_LIB   ("jtagprobe"  LS_LIB) /* The JTAG-probe library.                  */
#define LS_USCIF_CORE_LIB    ("xdsfast3"   LS_LIB) /* The core library.                        */
#define LS_USCIF_ERROR_LIB   ("xdserror"   LS_LIB) /* The error handling library.              */
#define LS_USCIF_BOARD_LIB   ("xdsboard"   LS_LIB) /* The board config' library static API's.  */
#define LS_USCIF_DYNAMIC_LIB ("xdsboard"   LS_LIB) /* The board config' library dynamic API's. */
#define LS_USCIF_ROUTE_LIB   ("xdsroute"   LS_LIB) /* The route management library.            */
#define LS_USCIF_ICICLE_LIB  ("xdsicicle"  LS_LIB) /* The route control library.               */
#define LS_USCIF_SCRIPT_LIB  ("luascript"  LS_LIB) /* The Lua-script library.                  */
#define LS_USCIF_ECOM_LIB    ("xdsecom3"   LS_LIB) /* The ECOM library.                        */

#define LS_XDS560_ECOM_LIB   ("xds560_ecom" LS_LIB) /* The XDS560 ECOM library. */
#define LS_XDS560_RTDX_LIB   ("rtdx"        LS_LIB) /* The XDS560 RTDX library. */

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#endif /* xds_file_stuff_h */

/* -- End Of File -- */
