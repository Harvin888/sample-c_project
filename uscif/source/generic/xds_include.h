/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 2006, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_include.h
*
* DESCRIPTION
*   The standard header files for the transition from USCIF35 to JTI.
*
* NOTES
*   This header file is used by the first "#include" statement in all
*   USCIF35 and JTI source files during the transiton from USCIF35 to JTI.
*
    It contains nothing more than nested "#include" statements
*   that allow the ordering and selection of header files to
*   be easily changed without editing 100+ source files.
*
*   The .._LIBRARY and .._EXECUTABLE mnenonics may be used to
*   selectively include files for specific USCIF35 and JTI components.
*
*   The MS_WIN32, POSIX and DSP_BIOS mnenonics shall NOT be used
*   to selectively include files based on the operating system.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xds_include_h
#define xds_include_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The standard header files.
*******************************************************************************/

/* The operating system independant build options. */
#include "jti_build.h"

/* The operating system dependant typedef's and mnemoncs. */
#include "jti_typedef.h"
#include "jti_header.h"

/* The standard error mnemnonics. */
#include "xds_error.h"

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* xds_include_h */

/* -- End Of File -- */
