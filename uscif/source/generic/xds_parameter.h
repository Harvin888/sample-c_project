/*******************************************************************************
* $Source$
* $Revision$
* $Date$
*
* TI Proprietary Information - Internal Data.
* Copyright (c) 1990-2005, Texas Instruments Incorporated.
* All rights reserved.
*******************************************************************************/

/*******************************************************************************
* FILENAME
*   xds_parameter.h
*
* DESCRIPTION
*   The titles and values for command-line options and config variables.
*
* AUTHOR
*   Roland Hoar
*   rhoar@ti.com
*   281.274.3612
*
*******************************************************************************/

/*******************************************************************************
*        1         2         3         4         5         6         7         8
*2345678901234567890123456789012345678901234567890123456789012345678901234567890
*******************************************************************************/

#ifndef xds_parameter_h
#define xds_parameter_h /* Defines a mnemonic specific to this file. */

#ifdef  __cplusplus
extern "C" {
#endif

/*******************************************************************************
* The definitions for the matching command-line option strings.
*******************************************************************************/

#define LS_MIN_OPTION_SIZE  4

/*******************************************************************************
* The definitions for the 'forever' and 'once' count values.
* These mnemonics are located here until a better location is found.
*******************************************************************************/

/* The request to perform a test forever. */
#define LS_REPEAT_FOREVER   (uint32_t)0
/* The request to perform a test just once. */
#define LS_REPEAT_JUSTONCE  (uint32_t)1

/*******************************************************************************
* The definitions for the 'invalid' and 'default' port addresses.
* These mnemonics are located here until a better location is found.
*******************************************************************************/

/* The invalid port address. */
#define LS_INVALID_ADDRESS (uint32_t)-1
/* The default port address. */
#define LS_DEFAULT_ADDRESS (uint32_t)0

/*******************************************************************************
* The definitions for command-line enumerations.
*******************************************************************************/

enum LsUscifJtagVersion
{
    LS_JTAG_VERSION_UNKNOWN, /* The version of JTAG support requested is unknown.   */
    LS_JTAG_VERSION_ALPHA1,  /* The version of JTAG support requested is "alpha 1". */
    LS_JTAG_VERSION_ALPHA2   /* The version of JTAG support requested is "alpha 2". */
};

enum LsUscifJtagAdapter
{
    LS_JTAG_ADAPTER_UNINSTALL, /* The JTAG adapter is not installed.          */
    LS_JTAG_ADAPTER_DISABLE,   /* The JTAG adapter is installed but disabled. */
    LS_JTAG_ADAPTER_ENABLE     /* The JTAG adapter is installed and enabled.  */
};

enum LsUscifJtagFormat
{
   /*
    * Except for LS_JTAG_FORMAT_MULTIPLE the values of this
    * enumeration match the value applied with JTAG's SSF command.
    */
    LS_JTAG_FORMAT_JTAG0 = 0x0,     /* JTAG scan mode #0. */
    LS_JTAG_FORMAT_JTAG1 = 0x1,     /* JTAG scan mode #1. */
    LS_JTAG_FORMAT_JTAG2 = 0x2,     /* JTAG scan mode #2. */
    LS_JTAG_FORMAT_JTAG3 = 0x3,     /* JTAG scan mode #3. */

    LS_JTAG_FORMAT_SEGMENT0 = 0x4,  /* Segmented scan mode #0. */
    LS_JTAG_FORMAT_SEGMENT1 = 0x5,  /* Segmented scan mode #1. */
    LS_JTAG_FORMAT_SEGMENT2 = 0x6,  /* Segmented scan mode #2. */
    LS_JTAG_FORMAT_SEGMENT3 = 0x7,  /* Segmented scan mode #3. */

    LS_JTAG_FORMAT_OPTIMISE0 = 0x8, /* Optimised scan mode #0. */
    LS_JTAG_FORMAT_OPTIMISE1 = 0x9, /* Optimised scan mode #1. */
    LS_JTAG_FORMAT_OPTIMISE2 = 0xA, /* Optimised scan mode #2. */
    LS_JTAG_FORMAT_OPTIMISE3 = 0xB, /* Optimised scan mode #3. */
    LS_JTAG_FORMAT_OPTIMISE4 = 0xC, /* Optimised scan mode #4. */
    LS_JTAG_FORMAT_OPTIMISE5 = 0xD, /* Optimised scan mode #5. */
    LS_JTAG_FORMAT_OPTIMISE6 = 0xE, /* Optimised scan mode #6. */
    LS_JTAG_FORMAT_OPTIMISE7 = 0xF, /* Optimised scan mode #7. */

    LS_JTAG_FORMAT_MULTIPLE = 0x10  /* Mutli-device scan mode. */
};

enum LsUscifJtagDelta
{

    LS_JTAG_DELTA_NONE = 0 /* The JTAG adapter adds no link-delay. */
};

enum LsUscifJtagEdge
{

    LS_JTAG_RISE_EDGE,
    LS_JTAG_FALL_EDGE
};

enum LsUscifProgTypes
{
    LS_TCLK_PROG_NOTHING,   /* Don't touch the PLL at all.               */
    LS_TCLK_PROG_EXTERNAL,  /* Assume an external clock source.          */
    LS_TCLK_PROG_DEFAULT,   /* Use the default clock program.            */
    LS_TCLK_PROG_REFERENCE, /* Use the PLL reference frequency.          */
    LS_TCLK_PROG_SPECIFIC,  /* Use a specific (user-selected) frequency. */
    LS_TCLK_PROG_AUTOMATIC, /* Use an automatic frequency selection.     */
    LS_TCLK_PROG_ADAPTIVE,  /* Use an adaptive frequency selection.      */
    LS_TCLK_PROG_INVERTER,  /* Use an inverter frequency selection.      */

    LS_TCLK_PROG_SUPERLOW,  /* Use the superlow internal frequency. */
    LS_TCLK_PROG_MINIMUM,   /* Use the minimum internal frequency.  */
    LS_TCLK_PROG_LEGACY,    /* Use the legacy XDS510 frequency.     */
    LS_TCLK_PROG_EXCHANGE,  /* Use the standard HS-RTDX frequency.  */
    LS_TCLK_PROG_MAXIMUM    /* Use the maximum internal frequency.  */
};

enum LsUscifTestModeTypes
{
    LS_TCLK_TESTMODE_DEFAULT,  /* Use the default scan-test option. */
    LS_TCLK_TESTMODE_NOTHING,  /* Skip the scan-test entirely.      */
    LS_TCLK_TESTMODE_CONTINUE, /* Do scan-test, log result, and continue after failures.  */
    LS_TCLK_TESTMODE_SURRENDER /* Do scan-test, log result, and surrender after failures. */
};

enum LsUscifOverFlowTypes
{
    LS_TCLK_OVERFLOW_DEFAULT, /* The PLL log-file default behaviour when full.  */
    LS_TCLK_OVERFLOW_FAILURE, /* The PLL log-file generates an error when full. */
    LS_TCLK_OVERFLOW_SILENT,  /* The PLL log-file is silent when full.          */
    LS_TCLK_OVERFLOW_INITIAL  /* The PLL log-file initialise command.           */
};

enum LsUscifFpgaType
{
    LS_FPGA_SELECT_UNKNOWN,  /* Select an unknown FPGA. */
    LS_FPGA_SELECT_EMULATOR, /* Select the standard emulation 560 FPGA. */
    LS_FPGA_SELECT_MULTIPOD, /* Select the multi-purpose pod 560 FPGA.  */
    LS_FPGA_SELECT_CLIENT,   /* Select the client trace-enabled 560 FPGA. */
    LS_FPGA_SELECT_SERVER,   /* Select the server trace-enabled 560 FPGA. */

    LS_FPGA_SELECT_MAX_INDEX /* The total number of fpga indexes. */
};

enum LsUscifFpgaAction
{
    LS_FPGA_ACTION_HUFFMAN, /* Select the Huffman algorithm. */
    LS_FPGA_ACTION_CRC32    /* Select the CRC32 algorithm.   */
};

enum LsUscifFpgaDirect
{
    LS_FPGA_DIRECT_ENCODE, /* Select the action to encode. */
    LS_FPGA_DIRECT_DECODE  /* Select the action to decode. */
};

enum LsHierarchyValue
{
    LS_HIERARCHY_SIBLING,    /* Access only the named items.                            */
    LS_HIERARCHY_CHILDREN,   /* Access named items and their children.                  */
    LS_HIERARCHY_DESCENDANTS /* Access named items plus their children and descendants. */
};

enum LsMethodValue
{
    LS_METHOD_PLURAL, /* Select and de-select items on the lists all together.  */
    LS_METHOD_SINGLE  /* Select and de-select items on the lists one at a time. */
};

enum LsReorderValue
{
    LS_REORDER_FORWARD,   /* Apply route-lists in forward order - n items. */
    LS_REORDER_REVERSE,   /* Apply route-lists in reverse order - n items. */
    LS_REORDER_COMBINE,   /* Apply route-lists in every combination of two. */
    LS_REORDER_SHUFFLE,   /* Apply route-lists in random permutation. */
    LS_REORDER_PERMUTE    /* Apply route-lists in every permutation. */
};

enum LsSystemReset
{
    LS_SYSTEM_RESET_NOTHING, /* Do nothing to the signal. */
    LS_SYSTEM_RESET_ASSERT,  /* Assert the signal - its active low so output a low voltage.  */
    LS_SYSTEM_RESET_NEGATE,  /* Negate the signal - its active low so output a high voltage. */
    LS_SYSTEM_RESET_PULSE    /* Pulse the signal  - invert then wait 500us then restore.     */
};

enum LsErrorFileType
{
    LS_ERROR_FILETYPE_MODERN, /* Full error descriptions in the modern file format.  */
    LS_ERROR_FILETYPE_HEADER  /* Full error mnemonics in the 'C' header file format. */
};

enum LsBootModeCommand
{
    LS_BOOT_MODE_DISABLE, /* Both EMU1-0 pins remain hi-z. */
    LS_BOOT_MODE_ENABLE   /* Both EMU1-0 pins may turn-on. */
};

enum LsBootModeValue
{
    LS_BOOT_VALUE_HIZ, /* Both EMU1-0 pins remain hi-z. */
    LS_BOOT_VALUE_00,  /* Both EMU1-0 pins are low.     */
    LS_BOOT_VALUE_01,  /* EMU1 is low, EMU0 is high.    */
    LS_BOOT_VALUE_10,  /* EMU1 is high, EMU0 is low.    */
    LS_BOOT_VALUE_11   /* Both EMU1-0 pins are high.    */
};

enum LsLoopBackCommand
{
    LS_LOOP_MODE_DISABLE, /* The loopback of clock and/or data is disabled. */
    LS_LOOP_MODE_ENABLE   /* The loopback of clock and/or data may occur.   */
};

enum LsLoopBackValue
{
    LS_LOOP_VALUE_DISABLE, /* The loopback of clock and/or data is disabled. */
    LS_LOOP_VALUE_DATA,    /* The loopback of TDO/TDI is enabled.            */
    LS_LOOP_VALUE_CLOCK,   /* The loopback of TCLKO/TCLKR is enabled.        */
    LS_LOOP_VALUE_TOTAL    /* The loopback of both data and clock is enabled and the
                              detection of power-loss and cable-break is disabled. */
};

enum LsLinkDelayCommand
{
    LS_LINK_MODE_DISABLE, /* The link-delay for TMS, TDO and TDI is disabled. */
    LS_LINK_MODE_ENABLE   /* The link-delay for TMS, TDO and TDI is enabled.  */
};

enum LsLinkDelayValue
{
    LS_LINK_VALUE_DEFAULT   = -2, /* The default link-delay for TMS, TDO and TDI is used.   */
    LS_LINK_VALUE_AUTOMATIC = -1, /* The automatic link-delay for TMS, TDO and TDI is used. */
    LS_LINK_VALUE_SPECIFIC  =  0  /* A specific link-delay for TMS, TDO and TDI is used.
                                     Any zero or positive value is a specific link-delay. */
};

enum LsJtagSignalEdgeDataOut
{
    TDO_FALL_EDGE, /* The falling edge is JTAG standard for TMS and TDO. */
    TDO_RISE_EDGE
};

enum LsJtagSignalEdgeDataIn
{
    TDI_RISE_EDGE, /* The rising edge is JTAG standard for TDI. */
    TDI_FALL_EDGE
};

/*******************************************************************************
* The declarations for config variables and command-line parameters.
*******************************************************************************/

/* The acceptable values of USCIF.JTAG_VERSION. */
extern XDS_FAR const char *JtagVersionTitle[];

/* The acceptable values of USCIF.JTAG_ADAPTER. */
extern XDS_FAR const char *JtagAdapterTitle[];

/* The acceptable values of USCIF.JTAG_FORMAT. */
extern XDS_FAR const char *JtagFormatTitle[];

/* The acceptable values of USCIF.JTAG_DELTA. */
extern XDS_FAR const char *JtagDeltaTitle[];

/* The acceptable values of USCIF.JTAG_EDGE. */
extern XDS_FAR const char *JtagEdgeTitle[];

/* The acceptable values of USCIF.TCLK_PROGRAM and the parameter 'program'. */
extern XDS_FAR const char *TclkProgTitle[];

/* The acceptable values of USCIF.TCLK_FREQUENCY and the parameter 'frequency'. */
/* The acceptable values of USCIF.TCLK_BEGINNING and the parameter 'beginning'. */
extern XDS_FAR const char *TclkFreqTitle[];

/* The acceptable values of USCIF.TCLK_TESTMODE and the parameter 'testmode'. */
extern XDS_FAR const char *TestModeTitle[];

/* The acceptable values of USCIF.TCLK_OVERFLOW and the parameter 'overflow'. */
extern XDS_FAR const char *OverFlowTitle[];

/* The acceptable values of USCIF.TCLK_REFERENCE and the parameter 'reference'. */
extern XDS_FAR const char *TclkReferTitle[];

/* The acceptable values of USCIF.TCLK_INITIAL and the parameter 'initial'. */
extern XDS_FAR const char *InitialTitle[];

/* The acceptable values of USCIF.TCLK_TERMINAL and the parameter 'terminal'. */
extern XDS_FAR const char *TerminalTitle[];

/* The acceptable values of USCIF.TCLK_SCAN_BITS and the parameter 'bits'. */
extern XDS_FAR const char *BitsTitle[];

/* The acceptable values of USCIF.TCLK_PATH_IRSIZE and the parameter 'irsize'. */
extern XDS_FAR const char *IrTitle[];

/* The acceptable values of USCIF.TCLK_PATH_DRSIZE and the parameter 'drsize'. */
extern XDS_FAR const char *DrTitle[];

/* The acceptable values of the parameters 'expand' and 'literal'. */
extern XDS_FAR const char *DataTitle[];

/* The acceptable values of the parameter 'goto'. */
extern XDS_FAR const char *GotoTitle[];

/* The acceptable values of the parameter 'goto'. */
extern XDS_FAR const char *StateTitle[];

/* The acceptable values of the parameter 'type'. */
extern XDS_FAR const char *FpgaTitleType[];

/* The acceptable values of the parameter 'action'. */
extern XDS_FAR const char *FpgaTitleAction[];

/* The acceptable values of the parameter 'direction'. */
extern XDS_FAR const char *FpgaTitleDirect[];

/* The acceptable values of the parameters 'center', 'lowest' and 'highest'. */
extern XDS_FAR const char *LimitTitle[];

/* The acceptable values of the parameters 'repeat'. */
extern XDS_FAR const char *RepeatTitle[];

/* The acceptable values of the parameter 'hierarchy'. */
extern XDS_FAR const char *HierarchyTitle[];

/* The acceptable values of the parameter 'method'. */
extern XDS_FAR const char *MethodTitle[];

/* The acceptable values of the parameter 'systemReset'. */
extern XDS_FAR const char *SystemResetTitle[];

/* The acceptable values of the parameter 'reorder'. */
extern XDS_FAR const char *ReorderTitle[];

/* The acceptable values of USCIF.JTAGBOOT_MODE and the parameter 'jtagBootMode'.   */
/* The acceptable values of USCIF.POWERBOOT_MODE and the parameter 'powerBootMode'. */
/* The acceptable values of USCIF.EMUOUTPUT_MODE and the parameter 'emuOutputMode'. */
extern XDS_FAR const char *BootModeTitle[];

/* The acceptable values of USCIF.JTAGBOOT_VALUE and the parameter 'jtagBootValue'.   */
/* The acceptable values of USCIF.POWERBOOT_VALUE and the parameter 'powerBootValue'. */
/* The acceptable values of USCIF.EMUOUTPUT_VALUE and the parameter 'emuOutputValue'. */
extern XDS_FAR const char *BootValueTitle[];

/* The acceptable values of USCIF.LOOPBACK_MODE and the parameter 'podLoopMode'. */
extern XDS_FAR const char *LoopModeTitle[];

/* The acceptable values of USCIF.LOOPBACK_VALUE and the parameter 'podLoopValue'. */
extern XDS_FAR const char *LoopValueTitle[];

/* The acceptable values of USCIF.LINKDELAY_MODE and the parameter 'podLinkMode'. */
extern XDS_FAR const char *LinkModeTitle[];

/* The acceptable values of USCIF.LINKDELAY_VALUE and the parameter 'podLinkValue'. */
extern XDS_FAR const char *LinkValueTitle[];

/* The acceptable values of USCIF.TDOEDGE and the parameter 'xxx'.   */
extern XDS_FAR const char *TdoEdgeTitle[];

/* The acceptable values of USCIF.TDIEDGE and the parameter 'xxx'.   */
extern XDS_FAR const char *TdiEdgeTitle[];

/* The acceptable values of the parameter 'FileType'. */
extern XDS_FAR const char *ErrorFileTypeTitle[];

/*******************************************************************************
* The public declarations for config variables and command-line parameters.
*******************************************************************************/

/* The acceptable values of USCIF.JTAG_VERSION. */
extern XDS_FAR const uint32_t JtagVersionValue[];

/* The acceptable values of USCIF.JTAG_ADAPTER. */
extern XDS_FAR const uint32_t JtagAdapterValue[];

/* The acceptable values of USCIF.JTAG_FORMAT. */
extern XDS_FAR const uint32_t JtagFormatValue[];

/* The acceptable values of USCIF.JTAG_DELTA. */
extern XDS_FAR const uint32_t JtagDeltaValue[];

/* The acceptable values of USCIF.JTAG_EDGE. */
extern XDS_FAR const uint32_t JtagEdgeValue[];

/* The acceptable values of USCIF.TCLK_PROGRAM and the parameter 'program'. */
extern XDS_FAR const uint32_t TclkProgValue[];

/* The acceptable values of USCIF.TCLK_FREQUENCY and the parameter 'frequency'. */
/* The acceptable values of USCIF.TCLK_BEGINNING and the parameter 'beginning'. */
extern XDS_FAR const uint32_t TclkFreqValue[];

/* The acceptable values of USCIF.TCLK_REFERENCE and the parameter 'reference'. */
extern XDS_FAR const uint32_t TclkReferValue[];

/* The acceptable values of USCIF.TCLK_INITIAL and the parameter 'initial'. */
extern XDS_FAR const uint32_t InitialValue[];

/* The acceptable values of USCIF.TCLK_TERMINAL and the parameter 'terminal'. */
extern XDS_FAR const uint32_t TerminalValue[];

/* The acceptable values of USCIF.TCLK_SCAN_BITS and the parameter 'bits'. */
extern XDS_FAR const uint32_t BitsValue[];

/* The acceptable values of the parameters 'expand' and 'literal'. */
extern XDS_FAR const uint32_t DataValue[];

/* The acceptable values of the parameter 'goto'. */
extern XDS_FAR const uint32_t GotoValue[];

/* The acceptable values of the parameter 'goto'. */
extern XDS_FAR const uint32_t StateValue[];

/* The acceptable values of the parameter 'type'. */
extern XDS_FAR const uint32_t FpgaValueType[];

/* The acceptable values of the parameter 'action'. */
extern XDS_FAR const uint32_t FpgaValueAction[];

/* The acceptable values of the parameter 'direction'. */
extern XDS_FAR const uint32_t FpgaValueDirect[];

/* The acceptable values of the parameters 'center', 'lowest' and 'highest'. */
extern XDS_FAR const uint32_t LimitValue[];

/* The acceptable values of the parameter 'repeat'. */
extern XDS_FAR const uint32_t RepeatValue[];

/* The acceptable values of the parameter 'hierarchy'. */
extern XDS_FAR const uint32_t HierarchyValue[];

/* The acceptable values of the parameter 'method'. */
extern XDS_FAR const uint32_t MethodValue[];

/* The acceptable values of the parameter 'systemReset'. */
extern XDS_FAR const uint32_t SystemResetValue[];

/* The acceptable values of the parameter 'reorder'. */
extern XDS_FAR const uint32_t ReorderValue[];

/* The acceptable values of USCIF.JTAGBOOT_MODE and the parameter 'jtagBootMode'.   */
/* The acceptable values of USCIF.POWERBOOT_MODE and the parameter 'powerBootMode'. */
/* The acceptable values of USCIF.EMUOUTPUT_MODE and the parameter 'emuOutputMode'. */
extern XDS_FAR const uint32_t BootModeValue[];

/* The acceptable values of USCIF.JTAGBOOT_VALUE and the parameter 'jtagBootValue'.   */
/* The acceptable values of USCIF.POWERBOOT_VALUE and the parameter 'powerBootValue'. */
/* The acceptable values of USCIF.EMUOUTPUT_VALUE and the parameter 'emuOutputValue'. */
extern XDS_FAR const uint32_t BootValueValue[];

/* The acceptable values of USCIF.LOOPBACK_MODE and the parameter 'podLoopMode'. */
extern XDS_FAR const uint32_t LoopModeValue[];

/* The acceptable values of USCIF.LOOPBACK_VALUE and the parameter 'podLoopValue'. */
extern XDS_FAR const uint32_t LoopValueValue[];

/* The acceptable values of USCIF.LINKDELAY_MODE and the parameter 'podLinkMode'. */
extern XDS_FAR const uint32_t LinkModeValue[];

/* The acceptable values of USCIF.LINKDELAY_VALUE and the parameter 'podLinkValue'. */
extern XDS_FAR const uint32_t LinkValueValue[];

/* The acceptable values of USCIF.TDOEDGE and the parameter 'xxx'.   */
extern XDS_FAR const uint32_t TdoEdgeValue[];

/* The acceptable values of USCIF.TDIEDGE and the parameter 'xxx'.   */
extern XDS_FAR const uint32_t TdiEdgeValue[];

/* The acceptable values of the parameter 'FileType'. */
extern XDS_FAR const uint32_t ErrorFileTypeValue[];

/*******************************************************************************
* The end of the header file.
*******************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* xds_parameter_h */

/* -- END OF FILE -- */
