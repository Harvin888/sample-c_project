# Microsoft Developer Studio Project File - Name="xdsbhpci_ms" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=xdsbhpci_ms - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xdsbhpci_ms.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xdsbhpci_ms.mak" CFG="xdsbhpci_ms - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xdsbhpci_ms - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "xdsbhpci_ms - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "xdsbhpci_ms"
# PROP Scc_LocalPath "..\.."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xdsbhpci_ms - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\Output\Release"
# PROP Intermediate_Dir ".\Intermediate\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W4 /GX /O2 /Ob2 /I "..\..\source\read" /I "..\..\source\generic\os_ms_win32" /I "..\..\source\generic" /I "..\..\source\export" /I "..\..\source\import" /I "..\..\source\register" /I "..\..\source\blackhawk" /I "..\..\source\xdsbhpci" /I "..\..\source\fpga" /D "NDEBUG" /D "_WINDOWS" /D "XDSBHPCI_LIBRARY" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /o "NUL" /win32
# SUBTRACT MTL /mktyplib203
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /fo".\Resource\Release\xdsbhpci_ms.res" /i "..\..\source\generic" /d "NDEBUG" /d "_WINDOWS" /d "XDSBHPCI_LIBRARY"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib advapi32.lib setupapi.lib /nologo /subsystem:windows /dll /pdb:none /machine:I386 /out:".\Output\Release\xdsbhpci.dll"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy /y Output\Release\xdsbhpci.dll ..\..\export
# End Special Build Tool

!ELSEIF  "$(CFG)" == "xdsbhpci_ms - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\Output\Debug"
# PROP Intermediate_Dir ".\Intermediate\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MDd /W4 /Gm /GX /ZI /Od /I "..\..\source\read" /I "..\..\..\..\..\opellaxd" /I "..\..\source\generic\os_ms_win32" /I "..\..\source\generic" /I "..\..\source\export" /I "..\..\source\import" /I "..\..\source\register" /I "..\..\source\blackhawk" /I "..\..\source\xdsbhpci" /I "..\..\source\fpga" /D "_DEBUG" /D "_WINDOWS" /D "XDSBHPCI_LIBRARY" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /o "NUL" /win32
# SUBTRACT MTL /mktyplib203
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /fo".\Resource\Debug\xdsbhpci_ms.res" /i "..\..\source\generic" /d "_DEBUG" /d "_WINDOWS" /d "XDSBHPCI_LIBRARY"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib advapi32.lib setupapi.lib /nologo /subsystem:windows /dll /map:".\Output\Debug/xdsbhpci_ms.map" /debug /machine:I386 /out:".\Output\Debug\xdsbhpci.dll"
# SUBTRACT LINK32 /profile
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy /y Output\Debug\xdsbhpci.dll ..\..\export
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "xdsbhpci_ms - Win32 Release"
# Name "xdsbhpci_ms - Win32 Debug"
# Begin Source File

SOURCE=..\..\source\blackhawk\bhpci_mmap.cpp
# End Source File
# Begin Source File

SOURCE=..\..\source\import\cfg_import.c
# End Source File
# Begin Source File

SOURCE=..\..\debug.c
# End Source File
# Begin Source File

SOURCE=..\..\debug.h
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_buffer.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_error.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_flag.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_main.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_poll.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_program.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_register.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_semaphore.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_api_timeout.c
# End Source File
# Begin Source File

SOURCE=..\..\source\export\jca_public.c
# End Source File
# Begin Source File

SOURCE=..\..\source\xdsbhpci\jca_test.c
# End Source File
# Begin Source File

SOURCE=..\..\libusb_dyn.cpp
# End Source File
# Begin Source File

SOURCE=..\..\ml_ash510.c
# End Source File
# Begin Source File

SOURCE=..\..\ml_ash510.h
# End Source File
# Begin Source File

SOURCE=..\..\source\read\read_sepk_variable.c
# End Source File
# Begin Source File

SOURCE=.\ti_icon.ico
# End Source File
# Begin Source File

SOURCE=..\..\source\generic\os_ms_win32\xds_loadlib.c
# End Source File
# Begin Source File

SOURCE=..\..\source\generic\xds_memory.c
# End Source File
# Begin Source File

SOURCE=..\..\source\generic\xds_simple.c
# End Source File
# Begin Source File

SOURCE=.\xdsbhpci_ms.def
# End Source File
# Begin Source File

SOURCE=.\xdsbhpci_ms.rc
# End Source File
# End Target
# End Project
