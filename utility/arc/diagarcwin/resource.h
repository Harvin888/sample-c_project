//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by diagarcwin.rc
//
#define IDD_DIAGARCWIN_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDD_ABOUT_DIALOG                129
#define IDB_BITMAPLOGO                  130
#define IDABOUT                         1000
#define IDC_STATUS_LIST                 1001
#define IDC_INTERFACE_STATIC            1002
#define IDC_SCANCHAIN_STATIC            1003
#define IDC_TARGET_STATIC               1004
#define IDC_BUTTON_CONNECT              1005
#define IDC_BUTTON_DISCONNECT           1006
#define IDC_BUTTON_RESET                1007
#define IDC_COMBO_JTAGFREQ              1008
#define IDC_STATIC_JTAGFREQ             1009
#define IDC_CHECK_OPTACCESS             1010
#define IDC_CHECK_AUTOINC               1011
#define IDC_BUTTON_BROWSEDLL            1012
#define IDC_BUTTON_BROWSEXBF            1013
#define IDC_STATIC_DLL_LABEL            1014
#define IDC_STATIC_DLL_PATH             1015
#define IDC_STATIC_XBF_PATH             1016
#define IDC_CHECK_XBF                   1017
#define IDC_BUTTON_SCANTEST             1018
#define IDC_BUTTON_READID               1019
#define IDC_BUTTON_MEM_TEST             1020
#define IDC_EDIT_MEM_ENDADDR            1021
#define IDC_STATIC_SCAN_LABEL1          1022
#define IDC_STATIC_SCAN_LABEL2          1023
#define IDC_STATIC_SCAN_ARCS            1024
#define IDC_STATIC_SCAN_LENGTH          1025
#define IDC_STATIC_MEM_CORE             1026
#define IDC_STATIC_MEM_STARTADDR        1027
#define IDC_BUTTON_BROWSEINI            1028
#define IDC_PROGRESS1                   1029
#define IDC_COMBO_MEM_CORE              1030
#define IDC_EDIT_MEM_STARTADDR          1031
#define IDC_STATIC_MEM_ENDADDR          1032
#define IDC_COMBO_MEM_BLOCKSIZE         1033
#define IDC_STATIC_INI_PATH             1033
#define IDC_STATIC_INI_LABEL            1034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
