/******************************************************************************
       Module: diagarcwin.h
     Engineer: Vitezslav Hola
  Description: Header for main dialog application of ARC diagnostic utility
  Date           Initials    Description
  24-Oct-2007    VH          Initial
******************************************************************************/
#if !defined(AFX_DIAGARCWIN_H__A401FAD9_5219_4C21_BB8C_6E4D68DEDA41__INCLUDED_)
#define AFX_DIAGARCWIN_H__A401FAD9_5219_4C21_BB8C_6E4D68DEDA41__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDiagarcwinApp:
// See diagarcwin.cpp for the implementation of this class
//

class CDiagarcwinApp : public CWinApp
{
public:
	CDiagarcwinApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDiagarcwinApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDiagarcwinApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIAGARCWIN_H__A401FAD9_5219_4C21_BB8C_6E4D68DEDA41__INCLUDED_)

