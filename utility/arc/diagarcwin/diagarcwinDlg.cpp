/******************************************************************************
       Module: diagarcwinDlg.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of main dialog in Opella-XD ARC diagnostic utility
  Date           Initials    Description
  24-Oct-2007    VH          Initial
  19-Jul-2012    SPT         Added TAP reset using TMS sequence during connection
  16-Nov-2014    RR          Added new function to verify the interface DLL
******************************************************************************/
#include "stdafx.h"
#include <direct.h>
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "protocol\common\types.h"
#include "arcint.h"
#include "diagarcwin.h"
#include "diagarcwinDlg.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CDiagarcwinDlg dialog
CDiagarcwinDlg::CDiagarcwinDlg(CWnd* pParent /*=NULL*/) : CDialog(CDiagarcwinDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDiagarcwinDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
   m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
   ptyArcInstance = NULL;
   pfget_ARC_interface = NULL;
   pfAshTestScanchain = NULL;
   pfAshGetSetupItem = NULL;
   pfAshGetLastErrorMessage = NULL;
   pCallback = NULL;
   ulNumberOfARCCores = 1;
   ulTotalIRLength = 4;
   // init memory block size (64kB, 16kB and 2kB)
   pulMemoryBlockSize[0] = 64;
   pulMemoryBlockSize[1] = 16;
   pulMemoryBlockSize[2] = 2;

	strcpy(pszGlobalDirPath, "");
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnDestroy
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnDestroy event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnDestroy() 
{
   CDialog::OnDestroy();
   char szText[_MAX_PATH];
   uint32_t ulValue;

   // save target options
   GetDlgItem(IDC_STATIC_DLL_PATH)->GetWindowText(szText, _MAX_PATH);
   LocalWriteProfileString("IDC_STATIC_DLL_PATH", szText);
   GetDlgItem(IDC_STATIC_INI_PATH)->GetWindowText(szText, _MAX_PATH);
   LocalWriteProfileString("IDC_STATIC_INI_PATH", szText);
   GetDlgItem(IDC_STATIC_XBF_PATH)->GetWindowText(szText, _MAX_PATH);
   LocalWriteProfileString("IDC_STATIC_XBF_PATH", szText);
   ulValue = ((CButton *)GetDlgItem(IDC_CHECK_XBF))->GetCheck();
   LocalWriteProfileValue("IDC_CHECK_XBF", ulValue);
   ulValue = ((CButton *)GetDlgItem(IDC_CHECK_OPTACCESS))->GetCheck();
   LocalWriteProfileValue("IDC_CHECK_OPTACCESS", ulValue);
   ulValue = ((CButton *)GetDlgItem(IDC_CHECK_AUTOINC))->GetCheck();
   LocalWriteProfileValue("IDC_CHECK_AUTOINC", ulValue);
   ulValue = ((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->GetCurSel();
   LocalWriteProfileValue("IDC_COMBO_JTAGFREQ", ulValue);
   // save memory test options
   ulValue = ((CComboBox *)GetDlgItem(IDC_COMBO_MEM_CORE))->GetCurSel();
   LocalWriteProfileValue("IDC_COMBO_MEM_CORE", ulValue);
   GetDlgItem(IDC_EDIT_MEM_STARTADDR)->GetWindowText(szText, _MAX_PATH);
   LocalWriteProfileString("IDC_EDIT_MEM_STARTADDR", szText);
   GetDlgItem(IDC_EDIT_MEM_ENDADDR)->GetWindowText(szText, _MAX_PATH);
   LocalWriteProfileString("IDC_EDIT_MEM_ENDADDR", szText);
}

void CDiagarcwinDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_PROGRESS1, m_ProgressBar);
	//{{AFX_DATA_MAP(CDiagarcwinDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDiagarcwinDlg, CDialog)
	//{{AFX_MSG_MAP(CDiagarcwinDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDABOUT, OnAbout)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_BROWSEDLL, OnButtonBrowsedll)
	ON_BN_CLICKED(IDC_BUTTON_BROWSEINI, OnButtonBrowseIni)
	ON_BN_CLICKED(IDC_BUTTON_BROWSEXBF, OnButtonBrowsexbf)
	ON_BN_CLICKED(IDC_CHECK_XBF, OnCheckXbf)
	ON_CBN_SELCHANGE(IDC_COMBO_JTAGFREQ, OnJtagFreq)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, OnButtonDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_RESET, OnButtonReset)
	ON_BN_CLICKED(IDC_BUTTON_SCANTEST, OnButtonScantest)
	ON_BN_CLICKED(IDC_BUTTON_READID, OnButtonReadid)
	ON_BN_CLICKED(IDC_BUTTON_MEM_TEST, OnButtonMemTest)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDiagarcwinDlg message handlers

/****************************************************************************
     Function: GetExecutablePath
     Engineer: Andre Schmiel
        Input: none
       Output: library path
  Description: get library path
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
char *CDiagarcwinDlg::GetExecutablePath()
{
   if (!_getcwd(pszGlobalDirPath, sizeof(pszGlobalDirPath)))
      strcpy(pszGlobalDirPath, "");

   return pszGlobalDirPath;
}

/****************************************************************************
     Function: GetINIFileLocation
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: get INI file location
Date           Initials    Description
05-May-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::GetINIFileLocation()
{
   char          pszFullFilename[_MAX_PATH];
   char         *pszLibraryPath;

   if (!strcmp(pszGlobalDirPath, ""))
      pszLibraryPath = GetExecutablePath();
   else
      pszLibraryPath = pszGlobalDirPath;

   strncpy(pszFullFilename, pszLibraryPath, sizeof(pszFullFilename));
   strncat(pszFullFilename, "/", _MAX_PATH - strlen(pszFullFilename));
   strncat(pszFullFilename, ARC_STD_INIFILE_NAME, _MAX_PATH - strlen(pszFullFilename));
	strcpy(pszIniFileLocation, pszFullFilename);
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnInitDialog
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: initialize dialog event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
BOOL CDiagarcwinDlg::OnInitDialog()
{
   char pszLocBuffer[15];
   uint32_t uiIndex;

   CDialog::OnInitDialog();
   // Set the icon for this dialog.  The framework does this automatically when the application's main window is not a dialog
   SetIcon(m_hIcon, TRUE);			   // Set big icon
   SetIcon(m_hIcon, FALSE);		       // Set small icon
   // initialize dialog items
   // init frequency combo box
   ((CListBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->ResetContent();
   for (uiIndex=0; uiIndex<NUMBER_OF_ARC_FREQ; uiIndex++)
      {
      GetARCJtagFrequency(0, uiIndex, pszLocBuffer, 15);
      ((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->AddString(pszLocBuffer);
      }
   // set 12 MHz as initial
   ((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->SetCurSel(LocalReadProfileValue("IDC_COMBO_JTAGFREQ", 12));
   // initialize path controls
   GetDlgItem(IDC_STATIC_DLL_PATH)->SetWindowText(LocalReadProfileString("IDC_STATIC_DLL_PATH", "C:\\AshlingOpellaXDforARC\\opxdarc.dll"));
	GetINIFileLocation();
   GetDlgItem(IDC_STATIC_INI_PATH)->SetWindowText(pszIniFileLocation);
   GetDlgItem(IDC_STATIC_XBF_PATH)->SetWindowText(LocalReadProfileString("IDC_STATIC_XBF_PATH", "C:\\OPXDARC.XBF"));
   // initialize target options
   ((CButton *)GetDlgItem(IDC_CHECK_OPTACCESS))->SetCheck(LocalReadProfileValue("IDC_CHECK_OPTACCESS", TRUE));
   ((CButton *)GetDlgItem(IDC_CHECK_AUTOINC))->SetCheck(LocalReadProfileValue("IDC_CHECK_AUTOINC", TRUE));
   ((CButton *)GetDlgItem(IDC_CHECK_XBF))->SetCheck(LocalReadProfileValue("IDC_CHECK_XBF", FALSE));
   if (((CButton *)GetDlgItem(IDC_CHECK_XBF))->GetCheck())
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(TRUE);     
      GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(TRUE);
      }
   else
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(FALSE);
      GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(FALSE);
      }
   // initialize memory test section
   ((CComboBox *)GetDlgItem(IDC_COMBO_MEM_CORE))->SetCurSel(LocalReadProfileValue("IDC_COMBO_MEM_CORE", 0));
   GetDlgItem(IDC_EDIT_MEM_STARTADDR)->SetWindowText(LocalReadProfileString("IDC_EDIT_MEM_STARTADDR", "0x0"));
   GetDlgItem(IDC_EDIT_MEM_ENDADDR)->SetWindowText(LocalReadProfileString("IDC_EDIT_MEM_ENDADDR", "0xFFFF"));
   m_ProgressBar.SetRange(0,100);
   m_ProgressBar.SetPos(0);

   return TRUE;                        // return TRUE  unless you set the focus to a control
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnPaint
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnPaint event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnPaint() 
{
   // If you add a minimize button to your dialog, you will need the code below
   //  to draw the icon.  For MFC applications using the document/view model,
   //  this is automatically done for you by the framework.
   if (IsIconic())
      {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;
      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
      }
   else
      {
      CDialog::OnPaint();
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnQueryDragIcon
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnQueryDragIcon event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
HCURSOR CDiagarcwinDlg::OnQueryDragIcon()
{
   // The system calls this to obtain the cursor to display while the user drags
   //  the minimized window.
   return (HCURSOR) m_hIcon;
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnAbout
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnAbout event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnAbout() 
{
   AboutDlg cAboutDlg;
   // show about dialog as modal
   cAboutDlg.DoModal();
}

/****************************************************************************
     Function: CDiagarcwinDlg::GetARCJtagFrequency
     Engineer: Vitezslav Hola
        Input: unsigned char bDriverFormat - use format for driver or combo box
               uint32_t uiFreqIndex - frequency index (from 0 to NUMBER_OF_ARC_FREQ-1)
               char *pszString - pointer to string for frequency value
               uint32_t uiSize - number of bytes in buffer
       Output: none
  Description: get string with frequency value for particular frequency index
               as in combo box
Date           Initials    Description
24-Oct-2007    VH          Initial
22-Feb-2008    NCH         Added to 1 kHz values
****************************************************************************/
void CDiagarcwinDlg::GetARCJtagFrequency(unsigned char bDriverFormat, uint32_t uiFreqIndex, char *pszString, uint32_t uiSize)
{
   uint32_t uiFreqValue = 1;
   unsigned char bMHzUnit = 1 ;

   if ((uiSize == 0) || (pszString == NULL))
      return;

   switch (uiFreqIndex)
      {
      case 0:     // 100 MHz
         uiFreqValue = 100;      bMHzUnit = 1;        break;
      case 1:     // 90 MHz
         uiFreqValue =  90;      bMHzUnit = 1;        break;
      case 2:     // 80 MHz
         uiFreqValue =  80;      bMHzUnit = 1;        break;
      case 3:     // 70 MHz
         uiFreqValue =  70;      bMHzUnit = 1;        break;
      case 4:     // 60 MHz
         uiFreqValue =  60;      bMHzUnit = 1;        break;
      case 5:     // 50 MHz
         uiFreqValue =  50;      bMHzUnit = 1;        break;
      case 6:     // 40 MHz
         uiFreqValue =  40;      bMHzUnit = 1;        break;
      case 7:     // 30 MHz
         uiFreqValue =  30;      bMHzUnit = 1;        break;
      case 8:     // 25 MHz
         uiFreqValue =  25;      bMHzUnit = 1;        break;
      case 9:     // 20 MHz
         uiFreqValue =  20;      bMHzUnit = 1;        break;
      case 10:    // 18 MHz
         uiFreqValue =  18;      bMHzUnit = 1;        break;
      case 11:    // 15 MHz
         uiFreqValue =  15;      bMHzUnit = 1;        break;
      case 12:    // 12 MHz
         uiFreqValue =  12;      bMHzUnit = 1;        break;
      case 13:    // 10 MHz
         uiFreqValue =  10;      bMHzUnit = 1;        break;
      case 14:    // 9 MHz
         uiFreqValue =   9;      bMHzUnit = 1;        break;
      case 15:    // 8 MHz
         uiFreqValue =   8;      bMHzUnit = 1;        break;
      case 16:    // 7 MHz
         uiFreqValue =   7;      bMHzUnit = 1;        break;
      case 17:    // 6 MHz
         uiFreqValue =   6;      bMHzUnit = 1;        break;
      case 18:    // 5 MHz
         uiFreqValue =   5;      bMHzUnit = 1;        break;
      case 19:    // 4 MHz
         uiFreqValue =   4;      bMHzUnit = 1;        break;
      case 20:    // 3 MHz
         uiFreqValue =   3;      bMHzUnit = 1;        break;
      case 21:    // 2.5 MHz
         uiFreqValue =  2500;    bMHzUnit = 0;        break;
      case 22:    // 2 MHz
         uiFreqValue =   2;      bMHzUnit = 1;        break;
      case 23:    // 1.8 MHz
         uiFreqValue =  1800;    bMHzUnit = 0;        break;
      case 24:    // 1.5 MHz
         uiFreqValue =  1500;    bMHzUnit = 0;        break;
      case 25:    // 1.2 MHz
         uiFreqValue =  1200;    bMHzUnit = 0;        break;
      case 26:    // 1 MHz
         uiFreqValue =   1;      bMHzUnit = 1;        break;
      case 27:    // 900 kHz
         uiFreqValue = 900;      bMHzUnit = 0;        break;
      case 28:    // 800 kHz
         uiFreqValue = 800;      bMHzUnit = 0;        break;
      case 29:    // 700 kHz
         uiFreqValue = 700;      bMHzUnit = 0;        break;
      case 30:    // 600 MHz
         uiFreqValue = 600;      bMHzUnit = 0;        break;
      case 31:    // 500 kHz
         uiFreqValue = 500;      bMHzUnit = 0;        break;
      case 32:    // 400 kHz
         uiFreqValue = 400;      bMHzUnit = 0;        break;
      case 33:    // 300 kHz
         uiFreqValue = 300;      bMHzUnit = 0;        break;
      case 34:    // 250 kHz
         uiFreqValue = 250;      bMHzUnit = 0;        break;
      case 35:    // 200 kHz
         uiFreqValue = 200;      bMHzUnit = 0;        break;
      case 36:    // 180 kHz
         uiFreqValue =  180;     bMHzUnit = 0;        break;
      case 37:    // 150 kHz
         uiFreqValue = 150;      bMHzUnit = 0;        break;
      case 38:    // 120 kHz
         uiFreqValue = 120;      bMHzUnit = 0;        break;
      case 39:    // 100 kHz
         uiFreqValue = 100;      bMHzUnit = 0;        break;
      case 40:    // 90 kHz
         uiFreqValue =  90;      bMHzUnit = 0;        break;
      case 41:    // 80 kHz
         uiFreqValue =  80;      bMHzUnit = 0;        break;
      case 42:    // 70 kHz
         uiFreqValue =  70;      bMHzUnit = 0;        break;
      case 43:    // 60 MHz
         uiFreqValue =  60;      bMHzUnit = 0;        break;
      case 44:    // 50 kHz
         uiFreqValue =  50;      bMHzUnit = 0;        break;
      case 45:    // 40 kHz
         uiFreqValue =  40;      bMHzUnit = 0;        break;
      case 46:    // 30 kHz
         uiFreqValue =  30;      bMHzUnit = 0;        break;
      case 47:    // 25 kHz
         uiFreqValue =  25;      bMHzUnit = 0;        break;
      case 48:    // 20 kHz
         uiFreqValue =  20;      bMHzUnit = 0;        break;
      case 49:    // 18 kHz
         uiFreqValue =  18;      bMHzUnit = 0;        break;
      case 50:    // 15 kHz
         uiFreqValue =  15;      bMHzUnit = 0;        break;
      case 51:    // 12 kHz
         uiFreqValue =  12;      bMHzUnit = 0;        break;
      case 52:    // 10 kHz
         uiFreqValue =  10;      bMHzUnit = 0;        break;
      case 53:    // 9 kHz
         uiFreqValue =  9;       bMHzUnit = 0;        break;
      case 54:    // 8 kHz
         uiFreqValue =  8;       bMHzUnit = 0;        break;
      case 55:    // 7 kHz
         uiFreqValue =  7;       bMHzUnit = 0;        break;
      case 56:    // 6 kHz
         uiFreqValue =  6;       bMHzUnit = 0;        break;
      case 57:    // 5 kHz
         uiFreqValue =  5;       bMHzUnit = 0;        break;
      case 58:    // 4 kHz
         uiFreqValue =  4;       bMHzUnit = 0;        break;
      case 59:    // 3 kHz
         uiFreqValue =  3;       bMHzUnit = 0;        break;
      case 60:    // 2 kHz
         uiFreqValue =  2;       bMHzUnit = 0;        break;
      case 61:    // 1 kHz
         uiFreqValue =  1;       bMHzUnit = 0;        break;
      default:    // 1 MHz as default
         uiFreqValue = 1;        bMHzUnit = 1;        break;
      }
   strcpy(pszString, "");
   if (bDriverFormat)
      {  // print string as driver requires (integer + units without space between)
      if (bMHzUnit)
         _snprintf(pszString, uiSize, "%uMHz", uiFreqValue);
      else
         _snprintf(pszString, uiSize, "%ukHz", uiFreqValue);
      }
   else
      {  // print in user friendly format for combo list
      if (bMHzUnit)
         _snprintf(pszString, uiSize, "%u MHz", uiFreqValue);                    // just MHz value
      else if (uiFreqValue < 1000)
         _snprintf(pszString, uiSize, "%u kHz", uiFreqValue);                    // just kHz value
      else
         {  // frequency > 1MHz but using kHz units
         float fFrequency = (float)uiFreqValue;
         fFrequency /= 1000.0;                                                   // convert to MHz
         _snprintf(pszString, uiSize, "%.1f MHz", fFrequency);
         }
      }
   pszString[uiSize-1] = 0;            // terminate string
}

/****************************************************************************
     Function: CDiagarcwinDlg::LocalReadProfileValue
     Engineer: Vitezslav Hola
        Input: char *pszKey - key name
               uint32_t ulValue - default value
       Output: uint32_t - result
  Description: read value from registry
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
uint32_t CDiagarcwinDlg::LocalReadProfileValue(char *pszKey, uint32_t ulDefault)
{
   char szDefault[0x100];
   char szReturned[0x100];
   uint32_t ulReturned;
   sprintf(szDefault, "%d", ulDefault);
   strcpy(szReturned, AfxGetApp()->GetProfileString(APPLICATION_NAME, pszKey, szDefault));
   sscanf(szReturned, "%d", &ulReturned);
   return ulReturned;
}

/****************************************************************************
     Function: CDiagarcwinDlg::LocalWriteProfileValue
     Engineer: Vitezslav Hola
        Input: char *pszKey - key name
               uint32_t ulValue - value
       Output: none
  Description: write value to registry
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::LocalWriteProfileValue(char *pszKey, uint32_t ulValue)
{
   char szValue[0x100];
   sprintf(szValue, "%d", ulValue);
   AfxGetApp()->WriteProfileString(APPLICATION_NAME, pszKey, szValue);
}

/****************************************************************************
     Function: CDiagarcwinDlg::LocalReadProfileString
     Engineer: Vitezslav Hola
        Input: char *pszKey - key name
               char *pszValue - default value
       Output: CString - result
  Description: read string from registry
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
CString CDiagarcwinDlg::LocalReadProfileString(char *pszKey, char *pszDefault)
{
   return AfxGetApp()->GetProfileString(APPLICATION_NAME, pszKey, pszDefault);
}

/****************************************************************************
     Function: CDiagarcwinDlg::LocalWriteProfileString
     Engineer: Vitezslav Hola
        Input: char *pszKey - key name
               char *pszValue - value
       Output: none
  Description: write string to registry
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::LocalWriteProfileString(char *pszKey, char *pszValue)
{
   AfxGetApp()->WriteProfileString(APPLICATION_NAME, pszKey, pszValue);
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonBrowsedll
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: browse path for DLL
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonBrowsedll() 
{
   CFileDialog cFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_EXPLORER, "DLL Files (*.dll)|*.dll|All Files (*.*)|*.*||", NULL);
   // show dialog to browse DLL
   if (cFileDialog.DoModal() == IDOK)
      GetDlgItem(IDC_STATIC_DLL_PATH)->SetWindowText(cFileDialog.GetPathName());
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonBrowseIni
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: browse path for DLL
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonBrowseIni() 
{
   CFileDialog cFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_EXPLORER, "INI Files (*.ini)|*.ini|All Files (*.*)|*.*||", NULL);

   // show dialog to browse DLL
   if (cFileDialog.DoModal() == IDOK)
	{
      GetDlgItem(IDC_STATIC_INI_PATH)->SetWindowText(cFileDialog.GetPathName());
		GetDlgItem(IDC_STATIC_INI_PATH)->GetWindowText(pszIniFileLocation, _MAX_PATH);

		LocalWriteProfileString("ARC_INI_ENV_VARIABLE", pszIniFileLocation);
	}
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonBrowsexbf
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: browse path for XBF
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonBrowsexbf() 
{
   CFileDialog cFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_EXPLORER, "XBF Files (*.xbf)|*.xbf|All Files (*.*)|*.*||", NULL);
   // show dialog to browse XBF file
   if (cFileDialog.DoModal() == IDOK)
      GetDlgItem(IDC_STATIC_XBF_PATH)->SetWindowText(cFileDialog.GetPathName());
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnCheckXbf
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: changing blast checkbox
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnCheckXbf() 
{
   if (((CButton *)GetDlgItem(IDC_CHECK_XBF))->GetCheck() != 0)
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(TRUE);     
      GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(TRUE);
      }
   else 
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(FALSE);     
      GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(FALSE);
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnJtagFreq
     Engineer: 
        Input: default
       Output: default
  Description: 
Date           Initials    Description
22-Feb-2008    NCH         Initial
****************************************************************************/
void CDiagarcwinDlg::OnJtagFreq() 
{
#if 0000
   if (((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->GetCurSel() < 53)
      {
      GetDlgItem(IDC_CHECK_OPTACCESS)->EnableWindow(TRUE);
      GetDlgItem(IDC_CHECK_AUTOINC)->EnableWindow(TRUE);
      }
   else
      {
      ((CButton *)GetDlgItem(IDC_CHECK_OPTACCESS))->SetCheck(TRUE);
      ((CButton *)GetDlgItem(IDC_CHECK_AUTOINC))->SetCheck(TRUE);
      GetDlgItem(IDC_CHECK_OPTACCESS)->EnableWindow(FALSE);
      GetDlgItem(IDC_CHECK_AUTOINC)->EnableWindow(FALSE);
      }
#endif
}

/****************************************************************************
     Function: CDiagarcwinDlg::VerifyInterfaceDLL
     Engineer: Roshith R
        Input: void 
       Output: true or false
  Description: Verifies the given interface DLL is a valid one by searching
               for the string 'Ashling' in the ID returned by DLL
Date           Initials    Description
16-Nov-2014    RR         Initial
****************************************************************************/
bool CDiagarcwinDlg::VerifyInterfaceDLL()
{
	char *pcInterfaceID;

	pcInterfaceID = pfAshGetInterfaceId();
	if (NULL == pcInterfaceID)
		return false;

	/* Search for the string "Ashling" in the interface ID returned from DLL */
	if (NULL != strstr(pcInterfaceID, "Ashling"))
	{
		/* String 'Ashling' found, valid DLL */
		return true;
	}
	else
	{
		/* String 'Ashling' not found, invalid DLL */
		return false;
	}
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonConnect
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnButtonConnect event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonConnect() 
{
   char pszLocString[_MAX_PATH];
   uint32_t ulIndex;
   char pszValue [20];
   AfxGetApp()->DoWaitCursor(1);
   m_ProgressBar.SetPos(0);
   // initialize connection
   ptyArcInstance = NULL;
   ((CListBox *)GetDlgItem(IDC_STATUS_LIST))->ResetContent();
   // get DLL path
   GetDlgItem(IDC_STATIC_DLL_PATH)->GetWindowText(pszLocString, _MAX_PATH);
   if (pszLocString[0] == '\0')
      {
			SetupMessage("Opella-XD Interface DLL location has not been specified");
			return;
      }
   // load DLL
   hDllInst = LoadLibrary(pszLocString);
   if (hDllInst == NULL)
      {
      SetupMessage("Cannot load specified Opella-XD Interface DLL");
      return;
      }
   GetDlgItem(IDC_STATIC_INI_PATH)->GetWindowText(pszLocString, _MAX_PATH);
   if (pszLocString[0] == '\0')
      {
			SetupMessage("Opella-XD INI File location has not been specified");
			return;
      }

   // get pointers to functions
   pfget_ARC_interface      = (Tyget_ARC_interface)GetProcAddress(hDllInst, "get_ARC_interface");
   pfAshTestScanchain       = (TyAshTestScanchain)GetProcAddress(hDllInst, "ASH_TestScanchain");
   pfAshSetSetupItem        = (TyAshSetSetupItem)GetProcAddress(hDllInst, "ASH_SetSetupItem");
   pfAshGetSetupItem        = (TyAshGetSetupItem)GetProcAddress(hDllInst, "ASH_GetSetupItem");
   pfAshGetLastErrorMessage = (TyAshGetLastErrorMessage)GetProcAddress(hDllInst, "ASH_GetLastErrorMessage");
   pfAshTMSReset            = (TyAshTMSReset)GetProcAddress(hDllInst, "ASH_TMSReset");
   
   // *** Added for v105-D
	pfAshGetInterfaceId = (TyAshGetInterfaceID)GetProcAddress(hDllInst, "ASH_GetInterfaceID");

	
   // check if pointers are valid
   if (   (pfget_ARC_interface      == NULL)
		 || (pfAshTestScanchain       == NULL)
		 || (pfAshGetSetupItem        == NULL)
		 || (pfAshSetSetupItem        == NULL)
		 || (pfAshGetLastErrorMessage == NULL)
		 || (pfAshTMSReset            == NULL)
		 || (pfAshGetInterfaceId      == NULL))
      {
      SetupMessage("Cannot find exported functions within specified Opella-XD Interface DLL");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
      }

	// Check the interface DLL is a valid Ashling DLL
	if (true != VerifyInterfaceDLL ())
	{
      SetupMessage("Ashing Opella-XD interface DLL is not a valid one!");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
	}

	pfAshSetSetupItem(ptyArcInstance, ARC_SETUP_SECT_INIFILE, ARC_SETUP_SECT_INIFILE, pszIniFileLocation);

   // ok, let open debug connection
   SetupMessage("Initializing connection ...");
   ptyArcInstance = pfget_ARC_interface();
   if (ptyArcInstance == NULL)
      {
      SetupMessage("Cannot open debug connection to Opella-XD or connection has been canceled");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
      }

   // set callback function
   if (pCallback != NULL)
      ptyArcInstance->ptyFunctionTable->receive_callback(ptyArcInstance, pCallback);
   // get info about cores and set controls
   GetARCDetails();

   sprintf(pszLocString, "%d", ulTotalIRLength);
   GetDlgItem(IDC_STATIC_SCAN_LENGTH)->SetWindowText(pszLocString);
   sprintf(pszLocString, "%d", ulNumberOfARCCores);
   GetDlgItem(IDC_STATIC_SCAN_ARCS)->SetWindowText(pszLocString);
   ((CComboBox *)GetDlgItem(IDC_COMBO_MEM_CORE))->ResetContent();
   for (ulIndex=0; ulIndex < ulNumberOfARCCores; ulIndex++)
      {
      sprintf(pszLocString, "%d", ulIndex+1);
      ((CComboBox *)GetDlgItem(IDC_COMBO_MEM_CORE))->AddString(pszLocString);
      }
   ((CComboBox *)GetDlgItem(IDC_COMBO_MEM_CORE))->SetCurSel(0);
   // set JTAG frequency for probe
   GetARCJtagFrequency(1, ((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->GetCurSel(), pszLocString, _MAX_PATH);
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_frequency", pszLocString);
   // now blast XBF file if enabled
   if (((CButton *)GetDlgItem(IDC_CHECK_XBF))->GetCheck())
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->GetWindowText(pszLocString, _MAX_PATH);
      if (!strcmp(pszLocString, ""))
         {
         SetupMessage("Target XBF location has not been specified");
		 if (ptyArcInstance)
			ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
         FreeLibrary(hDllInst);
         hDllInst = NULL;
		 ptyArcInstance = NULL;
         return;
         }
      // so let blast target FPGA
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "blast", pszLocString);
      }
   // now set probe configuration
   if (((CButton *)GetDlgItem(IDC_CHECK_OPTACCESS))->GetCheck())
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_optimise", "1");
   else
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_optimise", "0");
   if (((CButton *)GetDlgItem(IDC_CHECK_AUTOINC))->GetCheck())
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "auto_address", "1");
   else
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "auto_address", "0");
   
	if (ulNumberOfARCCores == 0)
      {
      SetupMessage("Cannot open debug connection to Opella-XD or connection has been canceled");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
      }

   pfAshGetSetupItem(ptyArcInstance, "TargetConfiguration", "TargetOptions", pszValue);
   if (pszValue[0] == '0' || pszValue[0] == '1') 
      pfAshTMSReset(ptyArcInstance);

   SetupMessage("Connected successfully.");
   // connection has been successful, so disable some controls
   GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_BROWSEDLL)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_BROWSEINI)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_DLL_LABEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_STATIC_INI_LABEL)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_DLL_PATH)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_INI_PATH)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(FALSE);
   GetDlgItem(IDC_CHECK_XBF)->EnableWindow(FALSE);
   GetDlgItem(IDC_COMBO_JTAGFREQ)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_JTAGFREQ)->EnableWindow(FALSE);
   GetDlgItem(IDC_CHECK_OPTACCESS)->EnableWindow(FALSE);
   GetDlgItem(IDC_CHECK_AUTOINC)->EnableWindow(FALSE);
   GetDlgItem(IDC_TARGET_STATIC)->EnableWindow(FALSE);
   // and enable others
   GetDlgItem(IDC_BUTTON_DISCONNECT)->EnableWindow(TRUE);
   GetDlgItem(IDC_BUTTON_RESET)->EnableWindow(TRUE);
   GetDlgItem(IDC_SCANCHAIN_STATIC)->EnableWindow(TRUE);
   GetDlgItem(IDC_BUTTON_SCANTEST)->EnableWindow(TRUE);
   GetDlgItem(IDC_BUTTON_READID)->EnableWindow(TRUE);
   GetDlgItem(IDC_INTERFACE_STATIC)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_SCAN_LABEL1)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_SCAN_LABEL2)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_SCAN_ARCS)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_SCAN_LENGTH)->EnableWindow(TRUE);
   GetDlgItem(IDC_BUTTON_MEM_TEST)->EnableWindow(TRUE);
   GetDlgItem(IDC_EDIT_MEM_STARTADDR)->EnableWindow(TRUE);
   GetDlgItem(IDC_EDIT_MEM_ENDADDR)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_MEM_CORE)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_MEM_STARTADDR)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_MEM_ENDADDR)->EnableWindow(TRUE);
   GetDlgItem(IDC_COMBO_MEM_CORE)->EnableWindow(TRUE);

   return ;
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonDisconnect
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnButtonDisconnect event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonDisconnect() 
{
   AfxGetApp()->DoWaitCursor(1);
   // enable some controls
   GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
   GetDlgItem(IDC_BUTTON_BROWSEDLL)->EnableWindow(TRUE);
   GetDlgItem(IDC_BUTTON_BROWSEINI)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_DLL_LABEL)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_INI_LABEL)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_DLL_PATH)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_INI_PATH)->EnableWindow(TRUE);
   GetDlgItem(IDC_CHECK_XBF)->EnableWindow(TRUE);
   if (((CButton *)GetDlgItem(IDC_CHECK_XBF))->GetCheck() != 0)
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(TRUE);     
      GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(TRUE);
      }
   else 
      {
      GetDlgItem(IDC_STATIC_XBF_PATH)->EnableWindow(FALSE);     
      GetDlgItem(IDC_BUTTON_BROWSEXBF)->EnableWindow(FALSE);
      }
   GetDlgItem(IDC_COMBO_JTAGFREQ)->EnableWindow(TRUE);
   GetDlgItem(IDC_STATIC_JTAGFREQ)->EnableWindow(TRUE);
   GetDlgItem(IDC_CHECK_OPTACCESS)->EnableWindow(TRUE);
   GetDlgItem(IDC_CHECK_AUTOINC)->EnableWindow(TRUE);
   GetDlgItem(IDC_TARGET_STATIC)->EnableWindow(TRUE);
   // and disable other
   GetDlgItem(IDC_BUTTON_DISCONNECT)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_RESET)->EnableWindow(FALSE);
   GetDlgItem(IDC_SCANCHAIN_STATIC)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_SCANTEST)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_READID)->EnableWindow(FALSE);
   GetDlgItem(IDC_INTERFACE_STATIC)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_SCAN_LABEL1)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_SCAN_LABEL2)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_SCAN_ARCS)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_SCAN_LENGTH)->EnableWindow(FALSE);
   GetDlgItem(IDC_BUTTON_MEM_TEST)->EnableWindow(FALSE);
   GetDlgItem(IDC_EDIT_MEM_STARTADDR)->EnableWindow(FALSE);
   GetDlgItem(IDC_EDIT_MEM_ENDADDR)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_MEM_CORE)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_MEM_STARTADDR)->EnableWindow(FALSE);
   GetDlgItem(IDC_STATIC_MEM_ENDADDR)->EnableWindow(FALSE);
   GetDlgItem(IDC_COMBO_MEM_CORE)->EnableWindow(FALSE);
   // close connection for probe
   if (ptyArcInstance != NULL)
      {
      ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
      ptyArcInstance = NULL;
      }
   FreeLibrary(hDllInst);
   hDllInst = NULL;
}

/****************************************************************************
     Function: CDialog::OnCancel
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnCancel event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnCancel() 
{
   // check if DLL has been loaded, we must close connection
   if (hDllInst != NULL)
      {
      if (ptyArcInstance != NULL)
         {
         ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
         ptyArcInstance = NULL;
         }
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      }
   CDialog::OnCancel();
}

/****************************************************************************
     Function: CDiagarcwinDlg::SetupMessage
     Engineer: Vitezslav Hola
        Input: char *pszMessage - message string
       Output: none
  Description: add message to status list
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::SetupMessage(char *pszMessage)
{
   CListBox *pList;
   // add string to message list and go to next
   pList = (CListBox *)GetDlgItem(IDC_STATUS_LIST);
   pList->AddString(pszMessage);
   pList->SetCurSel(pList->GetCount()-1);
}

/****************************************************************************
     Function: CDiagarcwinDlg::SetupGeneralMessage
     Engineer: Vitezslav Hola
        Input: const char *pszMessage - message string
       Output: none
  Description: add message to status list
Date           Initials    Description
01-Nov-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::SetupGeneralMessage(const char *pszMessage)
{

}

/****************************************************************************
     Function: CDiagarcwinDlg::ModifyMessage
     Engineer: Vitezslav Hola
        Input: char *pszMessage - message string
       Output: none
  Description: modify message string to remove \n and \r characters
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::ModifyMessage(char *pszMessage)
{
   if (pszMessage != NULL)
      {
      char *pszPtr;
      pszPtr = pszMessage;
      while (pszPtr != NULL)
         {
         pszPtr = strchr(pszMessage, '\n');
         if (pszPtr != NULL)
            *pszPtr = ' ';                               // replace \n with space
         }
      pszPtr = pszMessage;
      while (pszPtr != NULL)
         {
         pszPtr = strchr(pszMessage, '\r');
         if (pszPtr != NULL)
            *pszPtr = ' ';                               // replace \r with space
         }
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonReset
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: OnButtonReset event
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonReset() 
{
	if (ulNumberOfARCCores == 0)
	{
      SetupMessage("No core available on scan chain.");
		return;
	}

   // call property to reset target
   if (ptyArcInstance != NULL)
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "reset_board", "1");

   SetupMessage("Resetting target.");
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonScantest
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: OnButtonScantest event
Date           Initials    Description
25-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonScantest() 
{
   AfxGetApp()->DoWaitCursor(1);

	if (ulNumberOfARCCores == 0)
	{
      SetupMessage("No core available on scan chain.");
		return;
	}

   // just run scanchain loopback test
   if (pfAshTestScanchain(ptyArcInstance))
      SetupMessage("Scan chain loopback test passed.");
   else
      SetupMessage("Scan chain loopback test failed.");
}

/****************************************************************************
     Function: CDiagarcwinDlg::GetARCDetails
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: obtain details about target configuration from DLL
Date           Initials    Description
25-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::GetARCDetails(void)
{
   if ((ptyArcInstance != NULL) && (pfAshGetSetupItem != NULL))
      {
      char pszValue[20];
      uint32_t ulIndex;
      uint32_t ulNumberOfCores = 0;
      uint32_t ulNumberOfARC = 0;
      uint32_t ulTotalIR = 0;
      pfAshGetSetupItem(ptyArcInstance, "MultiCoreDebug", "NumberOfCores", pszValue);
//      if (!strcmp(pszValue, "") || (sscanf(pszValue, "%u", &ulNumberOfCores) != 1) || (ulNumberOfCores > MAX_NUMBER_OF_ARC) || (ulNumberOfCores < 1))
		//changed with introduction of auto detect scan chain
      if (!strcmp(pszValue, "") || (sscanf(pszValue, "%u", &ulNumberOfCores) != 1) || (ulNumberOfCores > MAX_NUMBER_OF_ARC))
         ulNumberOfCores = 1;

      // for each core, get detail info
      for (ulIndex=0; ulIndex < ulNumberOfCores; ulIndex++)
         {
         char pszDeviceStr[20];
         sprintf(pszDeviceStr, "Device%d", ulIndex+1);         // get section name
         pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "ARC", pszValue);
         if (strcmp(pszValue, "0"))
            {
            ulNumberOfARC++;                       // it is ARC core
            ulTotalIR += 4;                        // ARC has 4 bits in IR
            }
         else
            {
            uint32_t ulCoreWidth = 0;
            pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "IRWidth", pszValue);
            if (!strcmp(pszValue, "") || (sscanf(pszValue, "%u", &ulCoreWidth) != 1) || (ulCoreWidth > MAX_CORE_WIDTH))
               ulTotalIR += 4;
            else
               ulTotalIR += ulCoreWidth;
            }
         }
      // assign result
      ulNumberOfARCCores = ulNumberOfARC;
      ulTotalIRLength = ulTotalIR;
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonReadid
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: OnButtonReadid event
Date           Initials    Description
25-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::OnButtonReadid() 
{
   int iRet;

   AfxGetApp()->DoWaitCursor(1);

	if (ulNumberOfARCCores == 0)
	{
      SetupMessage("No core available on scan chain.");
		return;
	}

   // read ID for each ARC core
   if (ptyArcInstance != NULL)
      {
      uint32_t ulIndex;
      for (ulIndex=0; ulIndex < ulNumberOfARCCores; ulIndex++)
         {
         char pszLocString[100];
         uint32_t ulARCID = 0;
         // select ARC core
         sprintf(pszLocString, "%d", ulIndex+1);
         ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "cpunum", pszLocString);
         // read ID (aux register, 0x4)
         iRet = ptyArcInstance->ptyFunctionTable->read_banked_reg(ptyArcInstance, reg_bank_aux, 0x4, &ulARCID);
         switch (iRet)
            {
            case 0x04: //target powered-off
               {
                  sprintf(pszLocString, "\tTarget is Powered-off.");
                  SetupMessage(pszLocString);
               }
               break;
            case 0x02: //target powered-down
               {
                  sprintf(pszLocString, "\tTarget is Powered-down.");
                  SetupMessage(pszLocString);
               }
               break;
            case 0x01: // successful read
               {  
                  sprintf(pszLocString, "\tARC: %3.3d, ID register: %8.8X", ulIndex+1, ulARCID);
                  SetupMessage(pszLocString);
               }
               break;
            case 0x00: // failed to read ID
               {  
                  SetupMessage("\tFailed to read ID register.");
               }
               break;
            }
         }

         SetupMessage("Reading of ARC ID register(s) completed.");
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonMemTest
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: OnButtonMemTest event
Date           Initials    Description
25-Oct-2007    VH          Initial
19-Feb-2008    NCH         Bug fix - print message
25-Feb-2008    NCH         Add progress bar
****************************************************************************/
void CDiagarcwinDlg::OnButtonMemTest() 
{
   int iResult;

   AfxGetApp()->DoWaitCursor(1);

	if (ulNumberOfARCCores == 0)
	{
      SetupMessage("No core available on scan chain.");
		return;
	}

   // test given memory range via selected core
   if (ptyArcInstance != NULL)
      {
      char pszLocBuffer1[_MAX_PATH];
      char pszLocBuffer2[_MAX_PATH];
      uint32_t ulStartAddress, ulEndAddress, ulBlockSize, ulTransferSize, ulTotalSize;
      double dReadStartCount, dWriteStartCount, dReadTotal, dWriteTotal;
	  unsigned char bVerificationFailed = 0;
      // get range parameters and check validity
      GetDlgItem(IDC_EDIT_MEM_STARTADDR)->GetWindowText(pszLocBuffer1, _MAX_PATH);
      GetDlgItem(IDC_EDIT_MEM_ENDADDR)->GetWindowText(pszLocBuffer2, _MAX_PATH);
      if ((sscanf(pszLocBuffer1, "%X", &ulStartAddress) != 1) || 
          (sscanf(pszLocBuffer2, "%X", &ulEndAddress) != 1) ||
          (ulEndAddress <= ulStartAddress) || (ulStartAddress & 0x3) || ((ulEndAddress & 0x3) != 0x3))
         {
         SetupMessage("Memory range is not valid or alligned to words");
         return;
         }
      // select processor
      sprintf(pszLocBuffer1, "%d", ((CComboBox *)GetDlgItem(IDC_COMBO_MEM_CORE))->GetCurSel() + 1);
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "cpunum", pszLocBuffer1);
      // get block size (2kB if frequency < 1 MHz or using adaptive clock or 64 kB if frequency >= 1MHz)
      if (((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->GetCurSel() > 52)
         ulBlockSize = 512;                  // 0.5 kB block size
      else if (((CComboBox *)GetDlgItem(IDC_COMBO_JTAGFREQ))->GetCurSel() > 26)
         ulBlockSize = 2*1024;               // 2 kB block size
      else
         ulBlockSize = 64*1024;              // 64 kB block size

      // do transfer
      ulTransferSize = (ulEndAddress - ulStartAddress) + 1;
      ulTotalSize = ulTransferSize;
      // initialize performance measurement
      dReadTotal = 0.0;
      dWriteTotal = 0.0;
      m_ProgressBar.SetPos(0);
      if (ulTransferSize > ulBlockSize)
         {
         m_ProgressBar.SetRange(0, 2*(int)(ulTransferSize/ulBlockSize));
         m_ProgressBar.SetStep(1);
         }
      else
         {
         m_ProgressBar.SetRange(0, 2*(int)(ulTransferSize/ulBlockSize));
         m_ProgressBar.SetStep(1);
         }
      while (ulTransferSize)
         {
         uint32_t ulIndex;
         uint32_t ulCurrentSize = ulTransferSize;
         if (ulCurrentSize > ulBlockSize)
            ulCurrentSize = ulBlockSize;
         // first write memory
         m_ProgressBar.StepIt();
         SetupMessage("Writing Memory...");
//         Invalidate();
//         UpdateWindow();
         for (ulIndex = 0; ulIndex < (ulBlockSize/4); ulIndex++)
            {
            pulMemoryAddress[ulIndex] = ulStartAddress + ulIndex*4;
            pulMemoryWrite[ulIndex]   = (rand() & 0xFFFF) << 16;
            pulMemoryWrite[ulIndex]  |= (rand() & 0xFFFF);
            pulMemoryRead[ulIndex]    = 0x0;
            }
         dWriteStartCount = GetAccurateTickCount();
         
         iResult = ptyArcInstance->ptyFunctionTable->write_memory(ptyArcInstance, ulStartAddress, (void *)pulMemoryWrite, ulCurrentSize, 0);
         if (iResult != (int)ulCurrentSize)
            {
				if (iResult == 2)
					SetupMessage("Target is either Powered-down or Powered-off.");
				else
               SetupMessage("Failed to write to specified memory address range");

            return;
            }
         dWriteTotal += (GetAccurateTickCount() - dWriteStartCount);
         // now read memory
         m_ProgressBar.StepIt();
         SetupMessage("Read Memory...");
//         Invalidate();
//         UpdateWindow();
         dReadStartCount = GetAccurateTickCount();

         iResult = ptyArcInstance->ptyFunctionTable->read_memory(ptyArcInstance, ulStartAddress, (void *)pulMemoryRead, ulCurrentSize, 0);
         if (iResult != (int)ulCurrentSize)
            {
				if (iResult == 2)
					SetupMessage("Target is either Powered-down or Powered-off.");
				else
               SetupMessage("Failed to read from specified memory address range");

            return;
            }
         dReadTotal += (GetAccurateTickCount() - dReadStartCount);
         // and verify memory
         SetupMessage("Verifying Memory...");
//         Invalidate();
//         UpdateWindow();
         for (ulIndex = 0; ulIndex < (ulCurrentSize/4); ulIndex++)
            {
            if (pulMemoryWrite[ulIndex] != pulMemoryRead[ulIndex])
               {
               DisplayMemoryFailure(pulMemoryAddress[ulIndex], ulIndex, ulBlockSize);
			      bVerificationFailed = 1;
			      break;
               }
            }
         // update next address
         ulStartAddress += ulCurrentSize;
         ulTransferSize -= ulCurrentSize;
         }
//      m_ProgressBar.SetPos(0);
	  if (!bVerificationFailed)
         SetupMessage("Memory access tested successfully");
      // calculate overall performance
      if (dReadTotal < 0.1)
         dReadTotal = 0.1;
      dReadTotal /= 1000;                 // convert to seconds
      if (dWriteTotal < 0.1)
         dWriteTotal = 0.1;
      dWriteTotal /= 1000;                // convert to seconds
      // calculate kB per second
      dReadTotal = ((double)ulTotalSize / 1024.0) / dReadTotal;
      dWriteTotal = ((double)ulTotalSize / 1024.0) / dWriteTotal;
      // show results
      sprintf(pszLocBuffer1, "Read performance is %.1f kBytes/second", dReadTotal);
      sprintf(pszLocBuffer2, "Write performance is %.1f kBytes/second", dWriteTotal);
      SetupMessage(pszLocBuffer1);
      SetupMessage(pszLocBuffer2);
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::GetAccurateTickCount
     Engineer: Vitezslav Hola
        Input: None
       Output: double - count
  Description: return accurate tick count (in milliseconds)
Date           Initials    Description
25-Oct-2007    VH          Initial
****************************************************************************/
double CDiagarcwinDlg::GetAccurateTickCount()
{
   LARGE_INTEGER liFrequency, liCurrentValue;
   double dCurrentTime, dTicksPerSecond;

   if (!QueryPerformanceFrequency(&liFrequency))
      return (double)GetTickCount();      // no accurate timer available

   (void)QueryPerformanceCounter(&liCurrentValue);
   dCurrentTime    = (double)(((double)liCurrentValue.HighPart * (double)0x100000000) + (double)liCurrentValue.LowPart);
   dTicksPerSecond = (((double)liFrequency.HighPart * (double)0x100000000) + (double)liFrequency.LowPart);
   return (dCurrentTime / dTicksPerSecond) * 1000;       // return value in milliseconds
}

/****************************************************************************
     Function: CDiagarcwinDlg::DisplayMemoryFailure
     Engineer: Vitezslav Hola
        Input: uint32_t ulAddress - address that failed verification
               uint32_t ulOffset - offset in buffer failing verification
               uint32_t ulBlockSize - size of current memory block
       Output: none
  Description: show information about memory region failing verification
Date           Initials    Description
30-Oct-2007    VH          Initial
****************************************************************************/
void CDiagarcwinDlg::DisplayMemoryFailure(uint32_t ulAddress, uint32_t ulOffset, uint32_t ulBlockSize)
{
   char pszLocString[_MAX_PATH];
   uint32_t ulCnt, ulIndex;
   
   ulBlockSize /= 4;                                                          // convert to words
   // show error message
   sprintf(pszLocString, "Failed to verify memory contents at address 0x%8.8X", ulAddress);
   SetupMessage(pszLocString);
   // check offset
   ulCnt = 5;
   if (ulBlockSize < ulCnt)
      {
      ulCnt = ulBlockSize;
      ulOffset = 0;
      }
   else if (ulOffset < 2)
      ulOffset = 0;
   else if ((ulOffset + 2) >= ulBlockSize)
      ulOffset = ulBlockSize - 3;
   else
      ulOffset -= 2;

   // show content of memory
   SetupMessage("Address    | Data Wrote | Data Read");
   SetupMessage("-----------------------------------");
   for (ulIndex=0; ulIndex < ulCnt; ulIndex++)
      {
      sprintf(pszLocString, "0x%8.8X | 0x%8.8X | 0x%8.8X", pulMemoryAddress[ulOffset + ulIndex],
              pulMemoryWrite[ulOffset + ulIndex], pulMemoryRead[ulOffset + ulIndex]);
      SetupMessage(pszLocString);
      }
}

