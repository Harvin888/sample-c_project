; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=AboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "diagarcwin.h"

ClassCount=3
Class1=CDiagarcwinApp
Class2=CDiagarcwinDlg

ResourceCount=4
Resource2=IDD_DIAGARCWIN_DIALOG
Resource3=IDD_ABOUT_DIALOG (English (Ireland))
Resource1=IDR_MAINFRAME
Class3=AboutDlg
Resource4=IDD_ABOUT_DIALOG

[CLS:CDiagarcwinApp]
Type=0
HeaderFile=diagarcwin.h
ImplementationFile=diagarcwin.cpp
Filter=N

[CLS:CDiagarcwinDlg]
Type=0
HeaderFile=diagarcwinDlg.h
ImplementationFile=diagarcwinDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CDiagarcwinDlg



[DLG:IDD_DIAGARCWIN_DIALOG]
Type=1
Class=CDiagarcwinDlg
ControlCount=37
Control1=IDCANCEL,button,1342242816
Control2=IDABOUT,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATUS_LIST,listbox,1352728961
Control5=IDC_INTERFACE_STATIC,button,1476395015
Control6=IDC_SCANCHAIN_STATIC,button,1476395015
Control7=IDC_TARGET_STATIC,button,1342177287
Control8=IDC_BUTTON_CONNECT,button,1342242816
Control9=IDC_BUTTON_DISCONNECT,button,1476460544
Control10=IDC_BUTTON_RESET,button,1476460544
Control11=IDC_COMBO_JTAGFREQ,combobox,1344339971
Control12=IDC_STATIC_JTAGFREQ,static,1342308352
Control13=IDC_CHECK_OPTACCESS,button,1342242819
Control14=IDC_CHECK_AUTOINC,button,1342242819
Control15=IDC_BUTTON_BROWSEDLL,button,1342242816
Control16=IDC_BUTTON_BROWSEXBF,button,1342242816
Control17=IDC_STATIC_DLL_PATH,static,1342308352
Control18=IDC_STATIC_XBF_PATH,static,1342308352
Control19=IDC_STATIC_DLL_LABEL,static,1342308352
Control20=IDC_CHECK_XBF,button,1342242819
Control21=IDC_BUTTON_SCANTEST,button,1476460544
Control22=IDC_BUTTON_READID,button,1476460544
Control23=IDC_BUTTON_MEM_TEST,button,1476460544
Control24=IDC_STATIC_SCAN_LABEL1,static,1476526080
Control25=IDC_STATIC_SCAN_LABEL2,static,1476526080
Control26=IDC_STATIC_SCAN_ARCS,static,1476526080
Control27=IDC_STATIC_SCAN_LENGTH,static,1476526080
Control28=IDC_STATIC_MEM_CORE,static,1476526080
Control29=IDC_STATIC_MEM_STARTADDR,static,1476526080
Control30=IDC_COMBO_MEM_CORE,combobox,1478557699
Control31=IDC_EDIT_MEM_ENDADDR,edit,1484849280
Control32=IDC_EDIT_MEM_STARTADDR,edit,1484849280
Control33=IDC_STATIC_MEM_ENDADDR,static,1476526080
Control34=IDC_PROGRESS1,msctls_progress32,1350565888
Control35=IDC_BUTTON_BROWSEINI,button,1342242816
Control36=IDC_STATIC_INI_PATH,static,1342308352
Control37=IDC_STATIC_INI_LABEL,static,1342308352

[DLG:IDD_ABOUT_DIALOG]
Type=1
Class=AboutDlg
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308353
Control3=IDC_STATIC,static,1342308865
Control4=IDC_STATIC,static,1342308865
Control5=IDC_STATIC,static,1342308353
Control6=IDC_STATIC,static,1342308353
Control7=IDC_STATIC,static,1342177294

[CLS:AboutDlg]
Type=0
HeaderFile=AboutDlg.h
ImplementationFile=AboutDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=AboutDlg

[DLG:IDD_ABOUT_DIALOG (English (Ireland))]
Type=1
Class=?
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308865
Control4=IDC_STATIC,static,1342308865
Control5=IDC_STATIC,static,1342308353
Control6=IDC_STATIC,static,1342308353
Control7=IDC_STATIC,static,1342177294

