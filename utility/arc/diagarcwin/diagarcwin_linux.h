#ifndef DIAGARCWIN_LINUX_H
#define DIAGARCWIN_LINUX_H

#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1

#include <QMainWindow>
#include "arcint.h"

#include <iostream>
#include <dlfcn.h>

#define NUMBER_OF_ARC_MEMBLOCK      3
#define MEMORY_BLOCK_WORDS          (16384)                 // 64kB

#define _MAX_PATH                   260

#define  LoadLibrary(lib)                 dlopen(lib, RTLD_LAZY)
#define  GetProcAddress(handle, proc)     dlsym(handle, proc)
#define  FreeLibrary(handle)              dlclose(handle)

typedef void* HINSTANCE;

#define SECT_DLL_PATH         "DLL_PATH"
#define SECT_XBF_PATH         "XBF_PATH"
#define SECT_XBF_CHECK        "XBF_CHECK"
#define SECT_JTAG_FREQ        "JTAG_FREQUENCY"
#define SECT_OPT_ACCESS       "OPT_ACCESS"
#define SECT_AUTO_INC         "AUTO_INCREMENT"
#define SECT_MEM_CORE         "MEM_CORE"
#define SECT_MEM_START_ADDR   "MEM_START_ADDRESS"
#define SECT_MEM_END_ADDR     "MEM_END_ADDRESS"

#define DEFAULT_DIR_PATH      ""
#define DEFAULT_DLL_PATH      " "
#define DEFAULT_XBF_PATH      " "
#define DEFAULT_JTAG_FREQ     "12 MHz"
#define DEFAULT_AUTO_INC      1
#define DEFAULT_XBF_CHECK     0
#define DEFAULT_OPT_ACCESS    1
#define DEFAULT_MEM_COMBO     "1"
#define DEFAULT_MEM_START_ADDRESS "0"
#define DEFAULT_MEM_END_ADDRESS   "0xFFFF"

struct TyFunctionTable
{
   int            (*DummyFunction                )(struct TyArcInstance*);
   int            (*version                      )(struct TyArcInstance*);
   const char *   (*id                           )(struct TyArcInstance*);
   void           (*destroy                      )(struct TyArcInstance*);
   const char *   (*additional_possibilities     )(struct TyArcInstance*);
   void*          (*additional_information       )(struct TyArcInstance*, unsigned);
   int            (*prepare_for_new_program      )(struct TyArcInstance*, int);
   int            (*process_property             )(struct TyArcInstance*, const char *, const char *);
   int            (*is_simulator                 )(struct TyArcInstance*);
   int            (*step                         )(struct TyArcInstance*);
   int            (*run                          )(struct TyArcInstance*);
   int            (*read_memory                  )(struct TyArcInstance*, unsigned long, void *, unsigned long, int);
   int            (*write_memory                 )(struct TyArcInstance*, unsigned long, void *, unsigned long, int );
   int            (*read_reg                     )(struct TyArcInstance*, int, unsigned long *);
   int            (*write_reg                    )(struct TyArcInstance*, int, unsigned long);
   unsigned       (*memory_size                  )(struct TyArcInstance*);
   int            (*set_memory_size              )(struct TyArcInstance*, unsigned);
   int            (*set_reg_watchpoint           )(struct TyArcInstance*, int, int);
   int            (*remove_reg_watchpoint        )(struct TyArcInstance*, int, int);
   int            (*set_mem_watchpoint           )(struct TyArcInstance*, unsigned long, int);
   int            (*remove_mem_watchpoint        )(struct TyArcInstance*, unsigned long, int);
   int            (*stopped_at_watchpoint        )(struct TyArcInstance*);
   int            (*stopped_at_exception         )(struct TyArcInstance*);
   int            (*set_breakpoint               )(struct TyArcInstance*, unsigned, void*);
   int            (*remove_breakpoint            )(struct TyArcInstance*, unsigned, void*);
   int            (*retrieve_breakpoint_code     )(struct TyArcInstance*, unsigned, char *, unsigned, void *);
   int            (*breakpoint_cookie_len        )(struct TyArcInstance*);
   int            (*at_breakpoint                )(struct TyArcInstance*);
   int            (*define_displays              )(struct TyArcInstance*, struct Register_display *);
   int            (*fill_memory                  )(struct TyArcInstance*, unsigned long, void *, unsigned long, unsigned long, int);
   int            (*instruction_trace_count      )(struct TyArcInstance*);
   void           (*get_instruction_traces       )(struct TyArcInstance*, unsigned long *);
   void           (*receive_callback             )(struct TyArcInstance*, ARC_callback*);
   int            (*supports_feature             )(struct TyArcInstance*);
   unsigned long  (*data_exchange                )(struct TyArcInstance*, unsigned long, unsigned long, unsigned long, void *, unsigned long, void *);
   int            (*in_same_process_as_debugger  )(struct TyArcInstance*);
   unsigned long  (*max_data_exchange_transfer   )(struct TyArcInstance*);
   int            (*read_banked_reg              )(struct TyArcInstance*, int, int, unsigned long *);
   int            (*write_banked_reg             )(struct TyArcInstance*, int, int, unsigned long *);
};

struct TyArcInstance
{
   struct TyFunctionTable *ptyFunctionTable;
};

typedef void (*PfPrintFunc)(const char *);
typedef struct TyArcInstance *(*Tyget_ARC_interface)(void);
typedef int (*TyAshTestScanchain)(struct TyArcInstance *);
typedef void (*TyAshSetSetupItem)(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue);
typedef void (*TyAshGetSetupItem)(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue);
typedef void (*TyAshGetLastErrorMessage)(struct TyArcInstance *p, char *pszMessage, unsigned int uiSize);
typedef void (*TyAshSetGeneralMessagePrint)(PfPrintFunc pfFunc);
typedef int (*TyAshTMSReset)(struct TyArcInstance *p);

// **** Added for v105-D
typedef char* (*TyAshGetInterfaceID) (void);

namespace Ui {
class CDiagarcwinDlg;
}

class CDiagarcwinDlg : public QMainWindow
{
   Q_OBJECT

public:
   explicit CDiagarcwinDlg(QWidget *parent = 0);
   ~CDiagarcwinDlg();

   HINSTANCE hDllInst;

   TyArcInstance           *ptyArcInstance;
   Tyget_ARC_interface      pfget_ARC_interface;
   TyAshTestScanchain       pfAshTestScanchain;
   TyAshGetSetupItem        pfAshSetSetupItem;
   TyAshGetSetupItem        pfAshGetSetupItem;
   TyAshGetLastErrorMessage pfAshGetLastErrorMessage;
   ARC_callback            *pCallback;
   TyAshTMSReset            pfAshTMSReset;

   TyAshGetInterfaceID      pfAshGetInterfaceId;

   char pszGlobalDirPath[_MAX_PATH];
   char pszDllPath[_MAX_PATH];
   char pszXBFPath[_MAX_PATH];
   char pszJtagFreq[20];
   char pszCMemCore[20];
   char pszMemStartAddr[20];
   char pszMemEndAddr[20];
   unsigned char bXBFChecked;
   unsigned char bOptAccess;
   unsigned char bAutoIncr;

   // target details
   unsigned long ulNumberOfARCCores;
   unsigned long ulTotalIRLength;
   // memory block size
   unsigned long pulMemoryBlockSize[NUMBER_OF_ARC_MEMBLOCK];
   // buffers for memory verification
   unsigned long pulMemoryAddress[MEMORY_BLOCK_WORDS];
   unsigned long pulMemoryWrite[MEMORY_BLOCK_WORDS];
   unsigned long pulMemoryRead[MEMORY_BLOCK_WORDS];

   char *GetExecutablePath();
   bool OnInitDialog();
   void GetINIFileLocation();
   void StoreDiagINIFile();
   void ParseINIData(const char *pszCurrentSection, const char *pszIniLine);
   void LoadDiagIniFile();
   void GetARCJtagFrequency(unsigned char bDriverFormat, unsigned int uiFreqIndex, char *pszString, unsigned int uiSize);
   void GetARCDetails(void);
   void SetupMessage(const char *pszMessage);
   void ModifyMessage(const char *pszMessage);
   void SetupGeneralMessage(const char *pszMessage);
   double GetAccurateTickCount();
   void DisplayMemoryFailure(unsigned long ulAddress, unsigned long ulOffset, unsigned long ulBlockSize);
   void OnJtagFreq(void);
   bool VerifyInterfaceDLL(void);

private slots:
   void on_IDABOUT_clicked();
   void on_IDC_BUTTON_BROWSEDLL_clicked();
   void on_IDC_BUTTON_BROWSEINI_clicked();
   void on_IDC_BUTTON_BROWSEXBF_clicked();
   void on_IDC_CHECK_XBF_clicked(bool checked);
   void on_IDC_BUTTON_CONNECT_clicked();
   void on_IDC_BUTTON_DISCONNECT_clicked();
   void on_IDCANCEL_clicked();
   void on_IDC_BUTTON_RESET_clicked();
   void on_IDC_BUTTON_SCANTEST_clicked();
   void on_IDC_BUTTON_READID_clicked();

   void on_IDC_BUTTON_MEM_TEST_clicked();

private:
   Ui::CDiagarcwinDlg *ui;
};

#endif // DIAGARCWIN_LINUX_H
