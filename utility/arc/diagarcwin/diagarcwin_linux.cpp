#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

#include <QFileDialog>
#include <QPlainTextEdit>

#include "AboutDlg_Linux.h"
#include "diagarcwin_linux.h"
#include "ui_diagarcwin.h"

#define MAX_NUMBER_OF_ARC                       256
#define MAX_CORE_WIDTH                          32
#define NUMBER_OF_ARC_FREQ                      62
#define APPLICATION_NAME                        "OPXDARC.EXE"
#define ARC_STD_LIB_NAME                        "opxdarc.so"
#define ARC_STD_INIFILE_NAME                    "opxdarc.ini"
#define DEFAULT_INI_FILENAME                    "diagarcwin.ini"

//global variables for INI file handling
#define ARC_SETUP_SECT_INIFILE               "InitFileLocation"

char pszIniFileLocation[_MAX_PATH];

CDiagarcwinDlg::CDiagarcwinDlg(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::CDiagarcwinDlg)
{
   ui->setupUi(this);

   ptyArcInstance = NULL;
   pfget_ARC_interface = NULL;
   pfAshTestScanchain = NULL;
   pfAshGetSetupItem = NULL;
   pfAshGetLastErrorMessage = NULL;
   pCallback = NULL;
   ulNumberOfARCCores = 1;
   ulTotalIRLength = 4;
   // init memory block size (64kB, 16kB and 2kB)
   pulMemoryBlockSize[0] = 64;
   pulMemoryBlockSize[1] = 16;
   pulMemoryBlockSize[2] = 2;

   strcpy(pszDllPath,     "");
   strcpy(pszXBFPath,     "");
   strcpy(pszJtagFreq,    DEFAULT_JTAG_FREQ);
   strcpy(pszCMemCore,    DEFAULT_MEM_COMBO);
   strcpy(pszMemStartAddr,DEFAULT_MEM_START_ADDRESS);
   strcpy(pszMemEndAddr,  DEFAULT_MEM_END_ADDRESS);
   bXBFChecked = DEFAULT_XBF_CHECK;
   bOptAccess  = DEFAULT_OPT_ACCESS;
   bAutoIncr   = DEFAULT_AUTO_INC;

   strcpy(pszGlobalDirPath, DEFAULT_DIR_PATH);
}

CDiagarcwinDlg::~CDiagarcwinDlg()
{
   QString qString;

   // save target options
   qString = ui->IDC_STATIC_DLL_PATH->text();   
   strncpy(pszDllPath, qString.toStdString().data(), sizeof(pszDllPath));
   //strcpy(pszDllPath, qString.toStdString().data());
   qString = ui->IDC_STATIC_XBF_PATH->text();
   strncpy(pszXBFPath, qString.toStdString().data(), sizeof(pszXBFPath));
   //strcpy(pszXBFPath, qString.toStdString().data());
   bXBFChecked = ui->IDC_CHECK_XBF->isChecked();
   bOptAccess = ui->IDC_CHECK_OPTACCESS->isChecked();
   bAutoIncr = ui->IDC_CHECK_AUTOINC->isChecked();
    qString= ui->IDC_COMBO_JTAGFREQ->currentText();
   strncpy(pszJtagFreq, qString.toStdString().data(), sizeof(pszJtagFreq));
   //strcpy(pszJtagFreq, qString.toStdString().data());
   // save memory test options
   qString = ui->IDC_COMBO_MEM_CORE->currentText();
   strncpy(pszCMemCore, qString.toStdString().data(), sizeof(pszCMemCore));
   //strcpy(pszCMemCore, qString.toStdString().data());
   qString = ui->IDC_EDIT_MEM_STARTADDR->text();
   strncpy(pszMemStartAddr, qString.toStdString().data(), sizeof(pszMemStartAddr));
   //strcpy(pszMemStartAddr, qString.toStdString().data());
   qString = ui->IDC_EDIT_MEM_ENDADDR->text();
   strncpy(pszMemEndAddr, qString.toStdString().data(), sizeof(pszMemEndAddr));
   //strcpy(pszMemEndAddr, qString.toStdString().data());

   StoreDiagINIFile();

   delete ui;
}

/****************************************************************************
     Function: GetExecutablePath
     Engineer: Andre Schmiel
        Input: none
       Output: library path
  Description: get library path
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
char *CDiagarcwinDlg::GetExecutablePath()
{
   if (!getcwd(pszGlobalDirPath, sizeof(pszGlobalDirPath)))
      strcpy(pszGlobalDirPath, "");

   return pszGlobalDirPath;
}

/****************************************************************************
     Function: GetINIFileLocation
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: get INI file location
Date           Initials    Description
05-May-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::GetINIFileLocation()
{
   char          pszFullFilename[_MAX_PATH];
   char         *pszLibraryPath;

   if (!strcmp(pszGlobalDirPath, ""))
      pszLibraryPath = GetExecutablePath();
   else
      pszLibraryPath = pszGlobalDirPath;

   strncpy(pszFullFilename, pszLibraryPath, sizeof(pszFullFilename));
   strncat(pszFullFilename, "/", _MAX_PATH - strlen(pszFullFilename));
   strncat(pszFullFilename, ARC_STD_INIFILE_NAME, _MAX_PATH - strlen(pszFullFilename));
	strcpy(pszIniFileLocation, pszFullFilename);
}

/****************************************************************************
     Function: StoreDiagINIFile
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: store diagarcwin configuration into INI file
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::StoreDiagINIFile()
{
   FILE *pIniFile;
   char  pszValue[_MAX_PATH];
   char  pszFullFilename[_MAX_PATH];
   char *pszLibraryPath;

   if (!strcmp(pszGlobalDirPath, ""))
      pszLibraryPath = GetExecutablePath();
   else
      pszLibraryPath = pszGlobalDirPath;

   strncpy(pszFullFilename, pszLibraryPath, sizeof(pszFullFilename));
   strncat(pszFullFilename, "/", _MAX_PATH - strlen(pszFullFilename));
   strncat(pszFullFilename, DEFAULT_INI_FILENAME, _MAX_PATH - strlen(pszFullFilename));

   pIniFile = fopen(pszFullFilename, "wt");

   if (pIniFile == NULL)
      return;

   // writing target configuration section
   fprintf(pIniFile, "[%s]\n", SECT_DLL_PATH);              // section name
   strncpy(pszValue, pszFullFilename, sizeof(pszValue));

   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n",  pszValue);                 // store target selection

   fprintf(pIniFile, "[%s]\n", SECT_XBF_PATH);              // section name
   strncpy(pszValue, pszXBFPath, sizeof(pszValue));
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection

   fprintf(pIniFile, "[%s]\n", SECT_XBF_CHECK);             // section name
   sprintf(pszValue, "%d", bXBFChecked);
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection

   fprintf(pIniFile, "[%s]\n", SECT_JTAG_FREQ);             // section name
   strncpy(pszValue, pszJtagFreq, sizeof(pszValue));
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection

   fprintf(pIniFile, "[%s]\n", SECT_OPT_ACCESS);            // section name
   sprintf(pszValue, "%d", bOptAccess);
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection

   fprintf(pIniFile, "[%s]\n", SECT_AUTO_INC);              // section name
   sprintf(pszValue, "%d", bAutoIncr);
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection

   // memory options
   fprintf(pIniFile, "[%s]\n", SECT_MEM_START_ADDR);        // section name
   strncpy(pszValue, pszMemStartAddr, sizeof(pszValue));
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection
   fprintf(pIniFile, "[%s]\n", SECT_MEM_END_ADDR);          // section name
   strncpy(pszValue, pszMemEndAddr, sizeof(pszValue));
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection
   fprintf(pIniFile, "[%s]\n", SECT_MEM_CORE);              // section name
   strncpy(pszValue, pszCMemCore, sizeof(pszValue));
   if (strcmp(pszValue, ""))
      fprintf(pIniFile, "%s\n", pszValue);                  // store adaptive clock selection

   fclose(pIniFile);                                        // close INI file
}

/****************************************************************************
     Function: ParseINIData
     Engineer: Nikolay Chokoev
        Input: const char *pszCurrentSection -
               const char *pszIniLine -
       Output: none
  Description: Parse data from INI file.
Date           Initials    Description
22-Jan-2008    NCH         Initial
****************************************************************************/
void CDiagarcwinDlg::ParseINIData(const char *pszCurrentSection, const char *pszIniLine)
{
   if(strncmp(pszCurrentSection, SECT_XBF_PATH, sizeof(SECT_XBF_PATH))==0)
   {
      if(strlen(pszIniLine) > 0)
      {
         strcpy(pszXBFPath, pszIniLine);
         return;
      }

      strcpy(pszXBFPath, DEFAULT_XBF_PATH);
      return;
   }
   else if(strncmp(pszCurrentSection, SECT_XBF_CHECK, sizeof(SECT_XBF_CHECK))==0)
   {
      if(strncmp(pszIniLine,"0",1) == 0)
         bXBFChecked = 0;
      else if(strncmp(pszIniLine,"1",1) == 0)
         bXBFChecked = 1;
      else
         bXBFChecked = DEFAULT_XBF_CHECK;

      return;
      }
   else if(strncmp(pszCurrentSection, SECT_JTAG_FREQ, sizeof(SECT_JTAG_FREQ))==0)
   {
      if(strlen(pszIniLine) > 0)
      {
         strcpy(pszJtagFreq, pszIniLine);
         return;
      }

      strcpy(pszJtagFreq, DEFAULT_JTAG_FREQ);
   }
   else if(strncmp(pszCurrentSection, SECT_OPT_ACCESS, sizeof(SECT_OPT_ACCESS))==0)
   {
      if(strncmp(pszIniLine,"0",1) == 0)
         bOptAccess = 0;
      else if(strncmp(pszIniLine,"1",1) == 0)
         bOptAccess = 1;
      else
         bOptAccess = DEFAULT_OPT_ACCESS;

      return;
   }
   else if(strncmp(pszCurrentSection, SECT_AUTO_INC, sizeof(SECT_AUTO_INC))==0)
   {
      if(strncmp(pszIniLine,"0",1) == 0)
         bAutoIncr = 0;
      else if(strncmp(pszIniLine,"1",1) == 0)
         bAutoIncr = 1;
      else
         bAutoIncr = DEFAULT_AUTO_INC;

      return;
   }
   else if(strncmp(pszCurrentSection, SECT_MEM_CORE, sizeof(SECT_MEM_CORE))==0)
   {
      if(strlen(pszIniLine) > 0)
      {
         strcpy(pszCMemCore, pszIniLine);
         return;
      }

      strcpy(pszCMemCore, DEFAULT_MEM_COMBO);
   }
   else if(strncmp(pszCurrentSection, SECT_MEM_START_ADDR, sizeof(SECT_MEM_START_ADDR))==0)
   {
      if(strlen(pszIniLine) > 0)
      {
         strcpy(pszMemStartAddr, pszIniLine);
         return;
      }

      strcpy(pszMemStartAddr, DEFAULT_MEM_START_ADDRESS);
      return;
   }
   else if(strncmp(pszCurrentSection, SECT_MEM_END_ADDR, sizeof(SECT_MEM_END_ADDR))==0)
   {
      if(strlen(pszIniLine) > 0)
      {
         strcpy(pszMemEndAddr, pszIniLine);
         return;
      }

      strcpy(pszMemEndAddr, DEFAULT_MEM_END_ADDRESS);
      return;
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::LoadDiagIniFile
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: load diagarcwin configuration from INI file
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::LoadDiagIniFile()
{
   FILE *pIniFile;
   char pszCurrentSection[32];
   char pszIniLine[_MAX_PATH];
   char pszFullFilename[_MAX_PATH];
   char *pszLibraryPath;
   static int bSectionReaded = true;

   if (!strcmp(pszGlobalDirPath, ""))
      pszLibraryPath = GetExecutablePath();
   else
      pszLibraryPath = pszGlobalDirPath;

   strncpy(pszFullFilename, pszLibraryPath, sizeof(pszFullFilename));
   strncat(pszFullFilename, "/", _MAX_PATH - strlen(pszFullFilename));
   strncat(pszFullFilename, ARC_STD_LIB_NAME, _MAX_PATH - strlen(pszFullFilename));
   strcpy(pszDllPath, pszFullFilename);

   strncpy(pszFullFilename, pszLibraryPath, sizeof(pszFullFilename));
   strncat(pszFullFilename, "/", _MAX_PATH - strlen(pszFullFilename));
   strncat(pszFullFilename, DEFAULT_INI_FILENAME, _MAX_PATH - strlen(pszFullFilename));

   pIniFile = fopen(pszFullFilename, "rt");

   if (pIniFile == NULL)
      return;

   strcpy(pszCurrentSection, "");

   // read INI file line by line
   while (fgets(pszIniLine, _MAX_PATH, pIniFile) != NULL)
   {  // got line so try to parse it, first we need to remove any '\r' and '\n' characters (just strap them)
      char *pszLabelEnd = NULL;

      pszLabelEnd = strchr(pszIniLine, '\n');

      if (pszLabelEnd != NULL)
         *pszLabelEnd = 0;

      pszLabelEnd = strchr(pszIniLine, '\r');

      if (pszLabelEnd != NULL)
         *pszLabelEnd = 0;

      if ((pszIniLine[0] == '[') && (strchr(pszIniLine, ']') != NULL))
      {  // got section label
         pszLabelEnd = strchr(pszIniLine, ']');
         if (pszLabelEnd != NULL)
            *pszLabelEnd = 0;                                           // cut end of label
         strncpy(pszCurrentSection, &(pszIniLine[1]), 32);                // copy label name into local
         pszCurrentSection[31] = 0;
         bSectionReaded = false;
      }
      else if (strstr(pszIniLine, "") != NULL)
      {  // got variable assignment
         if(!bSectionReaded)
         {
            ParseINIData(pszCurrentSection, pszIniLine);
            bSectionReaded = true;
         }
      }
    }

   fclose(pIniFile);
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnInitDialog
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: initialize dialog event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
bool CDiagarcwinDlg::OnInitDialog()
{
   char         pszLocBuffer[15];
   unsigned int uiIndex;

   // initialize dialog items
   LoadDiagIniFile();

   // init frequency combo box
   ui->IDC_COMBO_JTAGFREQ->clear();
   for (uiIndex=0; uiIndex<NUMBER_OF_ARC_FREQ; uiIndex++)
   {
      GetARCJtagFrequency(0, uiIndex, pszLocBuffer, 15);
      ui->IDC_COMBO_JTAGFREQ->addItem(pszLocBuffer);
   }

   // set 12 MHz as initial
   ui->IDC_COMBO_JTAGFREQ->setCurrentText(pszJtagFreq);

   // initialize path controls
   ui->IDC_STATIC_DLL_PATH->insert(pszDllPath);
   GetINIFileLocation();
   ui->IDC_STATIC_INI_PATH->insert(pszIniFileLocation);
   ui->IDC_STATIC_XBF_PATH->insert(pszXBFPath);

   // initialize target options
   ui->IDC_CHECK_OPTACCESS->setChecked(bOptAccess);
   ui->IDC_CHECK_AUTOINC->setChecked(bAutoIncr);
   ui->IDC_CHECK_XBF->setChecked(bXBFChecked);
   if (ui->IDC_CHECK_XBF->isChecked())
   {
      ui->IDC_STATIC_XBF_PATH->setDisabled(false);
      ui->IDC_BUTTON_BROWSEXBF->setDisabled(false);
   }
   else
   {
      ui->IDC_STATIC_XBF_PATH->setDisabled(true);
      ui->IDC_BUTTON_BROWSEXBF->setDisabled(true);
   }

   // initialize memory test section
   ui->IDC_COMBO_MEM_CORE->setCurrentText(pszCMemCore);
   ui->IDC_EDIT_MEM_STARTADDR->insert(pszMemStartAddr);
   ui->IDC_EDIT_MEM_ENDADDR->insert(pszMemEndAddr);
   ui->IDC_PROGRESS1->setRange(0,100);
   ui->IDC_PROGRESS1->setValue(0);

   ui->IDC_STATIC_DLL_PATH->setReadOnly(true);
   ui->IDC_STATIC_INI_PATH->setReadOnly(true);
   ui->IDC_STATIC_XBF_PATH->setReadOnly(true);

   return true;                        // return TRUE  unless you set the focus to a control
}

void CDiagarcwinDlg::on_IDABOUT_clicked()
{
   CAboutDlg cAboutDlg;

   cAboutDlg.exec();
}

/****************************************************************************
     Function: CDiagarcwinDlg::GetARCJtagFrequency
     Engineer: Andre Schmiel
        Input: unsigned char bDriverFormat - use format for driver or combo box
               unsigned int uiFreqIndex - frequency index (from 0 to NUMBER_OF_ARC_FREQ-1)
               char *pszString - pointer to string for frequency value
               unsigned int uiSize - number of bytes in buffer
       Output: none
  Description: get string with frequency value for particular frequency index
               as in combo box
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::GetARCJtagFrequency(unsigned char bDriverFormat, unsigned int uiFreqIndex, char *pszString, unsigned int uiSize)
{
   unsigned int uiFreqValue = 1;
   unsigned char bMHzUnit = 1 ;

   if ((uiSize == 0) || (pszString == NULL))
      return;

   switch (uiFreqIndex)
      {
      case 0:     // 100 MHz
         uiFreqValue = 100;      bMHzUnit = 1;        break;
      case 1:     // 90 MHz
         uiFreqValue =  90;      bMHzUnit = 1;        break;
      case 2:     // 80 MHz
         uiFreqValue =  80;      bMHzUnit = 1;        break;
      case 3:     // 70 MHz
         uiFreqValue =  70;      bMHzUnit = 1;        break;
      case 4:     // 60 MHz
         uiFreqValue =  60;      bMHzUnit = 1;        break;
      case 5:     // 50 MHz
         uiFreqValue =  50;      bMHzUnit = 1;        break;
      case 6:     // 40 MHz
         uiFreqValue =  40;      bMHzUnit = 1;        break;
      case 7:     // 30 MHz
         uiFreqValue =  30;      bMHzUnit = 1;        break;
      case 8:     // 25 MHz
         uiFreqValue =  25;      bMHzUnit = 1;        break;
      case 9:     // 20 MHz
         uiFreqValue =  20;      bMHzUnit = 1;        break;
      case 10:    // 18 MHz
         uiFreqValue =  18;      bMHzUnit = 1;        break;
      case 11:    // 15 MHz
         uiFreqValue =  15;      bMHzUnit = 1;        break;
      case 12:    // 12 MHz
         uiFreqValue =  12;      bMHzUnit = 1;        break;
      case 13:    // 10 MHz
         uiFreqValue =  10;      bMHzUnit = 1;        break;
      case 14:    // 9 MHz
         uiFreqValue =   9;      bMHzUnit = 1;        break;
      case 15:    // 8 MHz
         uiFreqValue =   8;      bMHzUnit = 1;        break;
      case 16:    // 7 MHz
         uiFreqValue =   7;      bMHzUnit = 1;        break;
      case 17:    // 6 MHz
         uiFreqValue =   6;      bMHzUnit = 1;        break;
      case 18:    // 5 MHz
         uiFreqValue =   5;      bMHzUnit = 1;        break;
      case 19:    // 4 MHz
         uiFreqValue =   4;      bMHzUnit = 1;        break;
      case 20:    // 3 MHz
         uiFreqValue =   3;      bMHzUnit = 1;        break;
      case 21:    // 2.5 MHz
         uiFreqValue =  2500;    bMHzUnit = 0;        break;
      case 22:    // 2 MHz
         uiFreqValue =   2;      bMHzUnit = 1;        break;
      case 23:    // 1.8 MHz
         uiFreqValue =  1800;    bMHzUnit = 0;        break;
      case 24:    // 1.5 MHz
         uiFreqValue =  1500;    bMHzUnit = 0;        break;
      case 25:    // 1.2 MHz
         uiFreqValue =  1200;    bMHzUnit = 0;        break;
      case 26:    // 1 MHz
         uiFreqValue =   1;      bMHzUnit = 1;        break;
      case 27:    // 900 kHz
         uiFreqValue = 900;      bMHzUnit = 0;        break;
      case 28:    // 800 kHz
         uiFreqValue = 800;      bMHzUnit = 0;        break;
      case 29:    // 700 kHz
         uiFreqValue = 700;      bMHzUnit = 0;        break;
      case 30:    // 600 MHz
         uiFreqValue = 600;      bMHzUnit = 0;        break;
      case 31:    // 500 kHz
         uiFreqValue = 500;      bMHzUnit = 0;        break;
      case 32:    // 400 kHz
         uiFreqValue = 400;      bMHzUnit = 0;        break;
      case 33:    // 300 kHz
         uiFreqValue = 300;      bMHzUnit = 0;        break;
      case 34:    // 250 kHz
         uiFreqValue = 250;      bMHzUnit = 0;        break;
      case 35:    // 200 kHz
         uiFreqValue = 200;      bMHzUnit = 0;        break;
      case 36:    // 180 kHz
         uiFreqValue =  180;     bMHzUnit = 0;        break;
      case 37:    // 150 kHz
         uiFreqValue = 150;      bMHzUnit = 0;        break;
      case 38:    // 120 kHz
         uiFreqValue = 120;      bMHzUnit = 0;        break;
      case 39:    // 100 kHz
         uiFreqValue = 100;      bMHzUnit = 0;        break;
      case 40:    // 90 kHz
         uiFreqValue =  90;      bMHzUnit = 0;        break;
      case 41:    // 80 kHz
         uiFreqValue =  80;      bMHzUnit = 0;        break;
      case 42:    // 70 kHz
         uiFreqValue =  70;      bMHzUnit = 0;        break;
      case 43:    // 60 MHz
         uiFreqValue =  60;      bMHzUnit = 0;        break;
      case 44:    // 50 kHz
         uiFreqValue =  50;      bMHzUnit = 0;        break;
      case 45:    // 40 kHz
         uiFreqValue =  40;      bMHzUnit = 0;        break;
      case 46:    // 30 kHz
         uiFreqValue =  30;      bMHzUnit = 0;        break;
      case 47:    // 25 kHz
         uiFreqValue =  25;      bMHzUnit = 0;        break;
      case 48:    // 20 kHz
         uiFreqValue =  20;      bMHzUnit = 0;        break;
      case 49:    // 18 kHz
         uiFreqValue =  18;      bMHzUnit = 0;        break;
      case 50:    // 15 kHz
         uiFreqValue =  15;      bMHzUnit = 0;        break;
      case 51:    // 12 kHz
         uiFreqValue =  12;      bMHzUnit = 0;        break;
      case 52:    // 10 kHz
         uiFreqValue =  10;      bMHzUnit = 0;        break;
      case 53:    // 9 kHz
         uiFreqValue =  9;       bMHzUnit = 0;        break;
      case 54:    // 8 kHz
         uiFreqValue =  8;       bMHzUnit = 0;        break;
      case 55:    // 7 kHz
         uiFreqValue =  7;       bMHzUnit = 0;        break;
      case 56:    // 6 kHz
         uiFreqValue =  6;       bMHzUnit = 0;        break;
      case 57:    // 5 kHz
         uiFreqValue =  5;       bMHzUnit = 0;        break;
      case 58:    // 4 kHz
         uiFreqValue =  4;       bMHzUnit = 0;        break;
      case 59:    // 3 kHz
         uiFreqValue =  3;       bMHzUnit = 0;        break;
      case 60:    // 2 kHz
         uiFreqValue =  2;       bMHzUnit = 0;        break;
      case 61:    // 1 kHz
         uiFreqValue =  1;       bMHzUnit = 0;        break;
      default:    // 1 MHz as default
         uiFreqValue = 1;        bMHzUnit = 1;        break;
      }
   strcpy(pszString, "");
   if (bDriverFormat)
      {  // print string as driver requires (integer + units without space between)
      if (bMHzUnit)
         sprintf(pszString, "%uMHz", uiFreqValue);
      else
         sprintf(pszString, "%ukHz", uiFreqValue);
      }
   else
      {  // print in user friendly format for combo list
      if (bMHzUnit)
         sprintf(pszString, "%u MHz", uiFreqValue);                    // just MHz value
      else if (uiFreqValue < 1000)
         sprintf(pszString, "%u kHz", uiFreqValue);                    // just kHz value
      else
         {  // frequency > 1MHz but using kHz units
         float fFrequency = (float)uiFreqValue;
         fFrequency /= 1000.0;                                                   // convert to MHz
         sprintf(pszString, "%.1f MHz", fFrequency);
         }
      }
   pszString[uiSize-1] = 0;            // terminate string
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonBrowsedll
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: browse path for DLL
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_BROWSEDLL_clicked()
{
   QString     qsCaption, qsFilter, qsFileName;
   QString     qsDirectory = "";

   QFileDialog qFileDialog(this);

   qsCaption   = "Load";
   qsFilter    = "*.so";

   // show dialog to browse DLL
   qsFileName = qFileDialog.getOpenFileName(NULL, qsCaption, qsDirectory, qsFilter);

   if (qsFileName.count() == 0)
      return;

   if (qsFileName.contains(".so") == false)
      qsFileName.append(".so");

   ui->IDC_STATIC_DLL_PATH->insert(qsFileName);
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonBrowseIni
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: browse path for DLL
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_BROWSEINI_clicked()
{
   QString     qsCaption, qsFilter, qsFileName;
   QString     qsDirectory = "";

   QFileDialog qFileDialog(this);

   qsCaption   = "Load";
   qsFilter    = "*.ini";

   // show dialog to browse INI
   qsFileName = qFileDialog.getOpenFileName(NULL, qsCaption, qsDirectory, qsFilter);


   if (qsFileName.count() == 0)
      return;

   if (qsFileName.contains(".ini") == false)
      qsFileName.append(".ini");

   ui->IDC_STATIC_INI_PATH->insert(qsFileName);
   strcpy(pszIniFileLocation, qsFileName.toStdString().data());

   if (!strcmp(pszIniFileLocation, ""))
   {
      SetupMessage("INI file location empty! Using standard file location.");
      GetINIFileLocation();
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonBrowsexbf
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: browse path for XBF
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_BROWSEXBF_clicked()
{
   QString     qsCaption, qsFilter, qsFileName;
   QString     qsDirectory = "";

   QFileDialog qFileDialog(this);

   qsCaption   = "Load";
   qsFilter    = "*.xbf";

   // show dialog to browse XBF
   qsFileName = qFileDialog.getOpenFileName(NULL, qsCaption, qsDirectory, qsFilter);

   if (qsFileName.count() == 0)
      return;

   if (qsFileName.contains(".xbf") == false)
      qsFileName.append(".xbf");

   ui->IDC_STATIC_XBF_PATH->insert(qsFileName);
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnCheckXbf
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: changing blast checkbox
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_CHECK_XBF_clicked(bool checked)
{
   if (checked == true)
   {
      ui->IDC_STATIC_XBF_PATH->setDisabled(false);
      ui->IDC_BUTTON_BROWSEXBF->setDisabled(false);
   }
   else
   {
      ui->IDC_STATIC_XBF_PATH->setDisabled(true);
      ui->IDC_BUTTON_BROWSEXBF->setDisabled(true);
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::VerifyInterfaceDLL
     Engineer: Roshith R
        Input: void
       Output: true or false
  Description: Verifies the given interface DLL is a valid one by searching
               for the string 'Ashling' in the ID returned by DLL
Date           Initials    Description
16-Nov-2014    RR         Initial
****************************************************************************/
bool CDiagarcwinDlg::VerifyInterfaceDLL()
{
   char *pcInterfaceID;

   pcInterfaceID = pfAshGetInterfaceId();
   if (NULL == pcInterfaceID)
      return false;

   /* Search for the string "Ashling" in the interface ID returned from DLL */
   if (NULL != strstr(pcInterfaceID, "Ashling"))
   {
      /* String 'Ashling' found, valid DLL */
      return true;
   }
   else
   {
      /* String 'Ashling' not found, invalid DLL */
      return false;
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonConnect
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: OnButtonConnect event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_CONNECT_clicked()
{
   char pszLocString[_MAX_PATH];
   unsigned long ulIndex;
   char pszValue[20];

   QString qString;

   ui->IDC_PROGRESS1->setValue(0);

   // initialize connection
   ptyArcInstance = NULL;
   ui->IDC_STATUS_LIST->clear();
   // get DLL path
   qString = ui->IDC_STATIC_DLL_PATH->text();
   strcpy(pszLocString, qString.toStdString().data());
   if (pszLocString[0] == '\0')
   {
      SetupMessage("Opella-XD Interface DLL location has not been specified");
      return;
   }

   // load DLL
   hDllInst = LoadLibrary(pszLocString);
   if (hDllInst == NULL)
   {
      SetupMessage("Cannot load specified Opella-XD Interface DLL");
      return;
   }

   qString = ui->IDC_STATIC_INI_PATH->text();
   strcpy(pszLocString, qString.toStdString().data());
   if (pszLocString[0] == '\0')
   {
      SetupMessage("Opella-XD INI File location has not been specified");
      return;
   }

   // get pointers to functions
   pfget_ARC_interface      = (Tyget_ARC_interface)GetProcAddress(hDllInst, "get_ARC_interface");
   pfAshTestScanchain       = (TyAshTestScanchain)GetProcAddress(hDllInst, "ASH_TestScanchain");
   pfAshSetSetupItem        = (TyAshSetSetupItem)GetProcAddress(hDllInst, "ASH_SetSetupItem");
   pfAshGetSetupItem        = (TyAshGetSetupItem)GetProcAddress(hDllInst, "ASH_GetSetupItem");
   pfAshGetLastErrorMessage = (TyAshGetLastErrorMessage)GetProcAddress(hDllInst, "ASH_GetLastErrorMessage");
   pfAshTMSReset            = (TyAshTMSReset)GetProcAddress(hDllInst, "ASH_TMSReset");

   // *** Added for v105-D
   pfAshGetInterfaceId = (TyAshGetInterfaceID)GetProcAddress(hDllInst, "ASH_GetInterfaceID");

   // check if pointers are valid
   if (   (pfget_ARC_interface      == NULL)
       || (pfAshTestScanchain       == NULL)
       || (pfAshGetSetupItem        == NULL)
       || (pfAshSetSetupItem        == NULL)
       || (pfAshGetLastErrorMessage == NULL)
       || (pfAshTMSReset            == NULL)
       || (pfAshGetInterfaceId      == NULL))
   {
      SetupMessage("Cannot find exported functions within specified Opella-XD Interface DLL");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
   }

   // Check the interface DLL is a valid Ashling DLL
   if (true != VerifyInterfaceDLL ())
   {
      SetupMessage("Ashing Opella-XD interface DLL is not a valid one!");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
   }
   
   pfAshSetSetupItem(ptyArcInstance, ARC_SETUP_SECT_INIFILE, ARC_SETUP_SECT_INIFILE, pszIniFileLocation);

   // ok, let open debug connection
   SetupMessage("Initializing connection ...");
   ptyArcInstance = pfget_ARC_interface();
   if (ptyArcInstance == NULL)
      {
      SetupMessage("Cannot open debug connection to Opella-XD or connection has been canceled");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      return;
      }

   // set callback function
   if (pCallback != NULL)
      ptyArcInstance->ptyFunctionTable->receive_callback(ptyArcInstance, pCallback);
   // get info about cores and set controls
   GetARCDetails();

   sprintf(pszLocString, "%ld", ulTotalIRLength);
   ui->IDC_STATIC_SCAN_LENGTH->insert(pszLocString);
   sprintf(pszLocString, "%ld", ulNumberOfARCCores);
   ui->IDC_STATIC_SCAN_ARCS->insert(pszLocString);
   ui->IDC_COMBO_MEM_CORE->clear();

   for (ulIndex=0; ulIndex < ulNumberOfARCCores; ulIndex++)
   {
      sprintf(pszLocString, "%ld", ulIndex+1);
      ui->IDC_COMBO_MEM_CORE->addItem(pszLocString);
   }

   ui->IDC_COMBO_MEM_CORE->setCurrentIndex(0);

   // set JTAG frequency for probe
   GetARCJtagFrequency(1, ui->IDC_COMBO_JTAGFREQ->currentIndex(), pszLocString, _MAX_PATH);
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_frequency", pszLocString);
   // now blast XBF file if enabled
   if (ui->IDC_CHECK_XBF->isChecked())
   {
      qString = ui->IDC_STATIC_XBF_PATH->text();
      strcpy(pszLocString, qString.toStdString().data());
      if (!strcmp(pszLocString, ""))
      {
         SetupMessage("Target XBF location has not been specified");

         if (ptyArcInstance)
            ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);

         FreeLibrary(hDllInst);
         hDllInst = NULL;
         ptyArcInstance = NULL;
         return;
      }

      // so let blast target FPGA
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "blast", pszLocString);
   }

   // now set probe configuration
   if (ui->IDC_CHECK_OPTACCESS->isChecked())
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_optimise", "1");
   else
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_optimise", "0");

   if (ui->IDC_CHECK_AUTOINC->isChecked())
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "auto_address", "1");
   else
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "auto_address", "0");

   if (ulNumberOfARCCores == 0)
   {
      SetupMessage("Cannot open debug connection to Opella-XD or connection has been canceled");
      //FreeLibrary(hDllInst);
      //hDllInst = NULL;
      return;
   }

   pfAshGetSetupItem(ptyArcInstance, "TargetConfiguration", "TargetOptions", pszValue);
   if (pszValue[0] == '0' || pszValue[0] == '1')
      pfAshTMSReset(ptyArcInstance);

   SetupMessage("Connected successfully.");

   // connection has been successful, so disable some controls
   ui->IDC_BUTTON_CONNECT->setDisabled(true);
   ui->IDC_BUTTON_BROWSEDLL->setDisabled(true);
   ui->IDC_BUTTON_BROWSEINI->setDisabled(true);
   ui->IDC_BUTTON_BROWSEXBF->setDisabled(true);
   ui->IDC_STATIC_DLL_LABEL->setDisabled(true);
   ui->IDC_STATIC_INI_LABEL->setDisabled(true);
   ui->IDC_STATIC_DLL_PATH->setDisabled(true);
   ui->IDC_STATIC_INI_PATH->setDisabled(true);
   ui->IDC_STATIC_XBF_PATH->setDisabled(true);
   ui->IDC_CHECK_XBF->setDisabled(true);
   ui->IDC_STATIC_JTAGFREQ->setDisabled(true);
   ui->IDC_COMBO_JTAGFREQ->setDisabled(true);
   ui->IDC_CHECK_OPTACCESS->setDisabled(true);
   ui->IDC_CHECK_AUTOINC->setDisabled(true);

   // and enable others
   ui->IDC_BUTTON_DISCONNECT->setDisabled(false);
   ui->IDC_BUTTON_RESET->setDisabled(false);
   ui->IDC_BUTTON_SCANTEST->setDisabled(false);
   ui->IDC_BUTTON_READID->setDisabled(false);
   ui->IDC_BUTTON_MEM_TEST->setDisabled(false);
   ui->IDC_EDIT_MEM_STARTADDR->setDisabled(false);
   ui->IDC_EDIT_MEM_ENDADDR->setDisabled(false);;
   ui->IDC_COMBO_MEM_CORE->setDisabled(false);;

   return ;
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonDisconnect
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: OnButtonDisconnect event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_DISCONNECT_clicked()
{
   // enable some controls
   ui->IDC_BUTTON_CONNECT->setDisabled(false);
   ui->IDC_BUTTON_BROWSEDLL->setDisabled(false);
   ui->IDC_BUTTON_BROWSEINI->setDisabled(false);
   ui->IDC_STATIC_DLL_LABEL->setDisabled(false);
   ui->IDC_STATIC_INI_LABEL->setDisabled(false);
   ui->IDC_STATIC_DLL_PATH->setDisabled(false);
   ui->IDC_STATIC_INI_PATH->setDisabled(false);
   ui->IDC_CHECK_XBF->setDisabled(false);

   if (ui->IDC_CHECK_XBF->isChecked())
   {
      ui->IDC_STATIC_XBF_PATH->setDisabled(false);
      ui->IDC_BUTTON_BROWSEXBF->setDisabled(false);
   }
   else
   {
      ui->IDC_STATIC_XBF_PATH->setDisabled(true);
      ui->IDC_BUTTON_BROWSEXBF->setDisabled(true);
   }

   ui->IDC_STATIC_JTAGFREQ->setDisabled(false);
   ui->IDC_COMBO_JTAGFREQ->setDisabled(false);
   ui->IDC_CHECK_OPTACCESS->setDisabled(false);
   ui->IDC_CHECK_AUTOINC->setDisabled(false);

   // and disable other
   ui->IDC_BUTTON_DISCONNECT->setDisabled(true);
   ui->IDC_BUTTON_RESET->setDisabled(true);
   ui->IDC_BUTTON_SCANTEST->setDisabled(true);
   ui->IDC_BUTTON_READID->setDisabled(true);
   ui->IDC_BUTTON_MEM_TEST->setDisabled(true);
   ui->IDC_EDIT_MEM_STARTADDR->setDisabled(true);
   ui->IDC_EDIT_MEM_ENDADDR->setDisabled(true);
   ui->IDC_EDIT_MEM_STARTADDR->setDisabled(true);
   ui->IDC_EDIT_MEM_ENDADDR->setDisabled(true);
   ui->IDC_COMBO_MEM_CORE->setDisabled(true);

   // close connection for probe
   if (ptyArcInstance != NULL)
   {
      ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
      ptyArcInstance = NULL;
   }

   ui->IDC_STATUS_LIST->clear();

   FreeLibrary(hDllInst);
   hDllInst = NULL;
}

/****************************************************************************
     Function: CDialog::OnCancel
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: OnCancel event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDCANCEL_clicked()
{
   // check if DLL has been loaded, we must close connection
   if (hDllInst != NULL)
      {
      if (ptyArcInstance != NULL)
         {
         ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
         ptyArcInstance = NULL;
         }
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      }

   this->close();
}

/****************************************************************************
     Function: CDiagarcwinDlg::SetupMessage
     Engineer: Andre Schmiel
        Input: char *pszMessage - message string
       Output: none
  Description: add message to status list
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::SetupMessage(const char *pszMessage)
{
   ui->IDC_STATUS_LIST->appendPlainText(pszMessage);
}

/****************************************************************************
     Function: CDiagarcwinDlg::ModifyMessage
     Engineer: Andre Schmiel
        Input: char *pszMessage - message string
       Output: none
  Description: modify message string to remove \n and \r characters
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::ModifyMessage(const char *pszMessage)
{
   if (pszMessage != NULL)
      {
      char *pszPtr;
      pszPtr = (char*)pszMessage;
      while (pszPtr != NULL)
         {
         pszPtr = (char*)strchr(pszMessage, '\n');
         if (pszPtr != NULL)
            *pszPtr = ' ';                               // replace \n with space
         }
      pszPtr = (char*)pszMessage;
      while (pszPtr != NULL)
         {
         pszPtr = (char*)strchr(pszMessage, '\r');
         if (pszPtr != NULL)
            *pszPtr = ' ';                               // replace \r with space
         }
      }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonReset
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: OnButtonReset event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_RESET_clicked()
{
   if (ulNumberOfARCCores == 0)
   {
      SetupMessage("No core available on scan chain.");
      return;
   }

   // call property to reset target
   if (ptyArcInstance != NULL)
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "reset_board", "1");

   SetupMessage("Resetting target.");
}

void CDiagarcwinDlg::on_IDC_BUTTON_SCANTEST_clicked()
{
   if (ulNumberOfARCCores == 0)
   {
      SetupMessage("No core available on scan chain.");
      return;
   }

   // just run scanchain loopback test
   if (pfAshTestScanchain(ptyArcInstance))
      SetupMessage("Scan chain loopback test passed.");
   else
      SetupMessage("Scan chain loopback test failed.");
}

/****************************************************************************
     Function: CDiagarcwinDlg::GetARCDetails
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: obtain details about target configuration from DLL
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::GetARCDetails(void)
{
   if ((ptyArcInstance != NULL) && (pfAshGetSetupItem != NULL))
   {
      char pszValue[20];
      unsigned long ulIndex;
      unsigned long ulNumberOfCores = 0;
      unsigned long ulNumberOfARC = 0;
      unsigned long ulTotalIR = 0;
      pfAshGetSetupItem(ptyArcInstance, "MultiCoreDebug", "NumberOfCores", pszValue);

      if (!strcmp(pszValue, "") || (sscanf(pszValue, "%lu", &ulNumberOfCores) != 1) || (ulNumberOfCores > MAX_NUMBER_OF_ARC))
         ulNumberOfCores = 1;

      // for each core, get detail info
      for (ulIndex=0; ulIndex < ulNumberOfCores; ulIndex++)
      {
         char pszDeviceStr[20];
         sprintf(pszDeviceStr, "Device%ld", ulIndex+1);         // get section name
         pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "ARC", pszValue);
         if (strcmp(pszValue, "0"))
         {
            ulNumberOfARC++;                       // it is ARC core
            ulTotalIR += 4;                        // ARC has 4 bits in IR
         }
         else
         {
            unsigned long ulCoreWidth = 0;
            pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "IRWidth", pszValue);
            if (!strcmp(pszValue, "") || (sscanf(pszValue, "%lu", &ulCoreWidth) != 1) || (ulCoreWidth > MAX_CORE_WIDTH))
               ulTotalIR += 4;
            else
               ulTotalIR += ulCoreWidth;
         }
      }
      // assign result
      ulNumberOfARCCores = ulNumberOfARC;
      ulTotalIRLength = ulTotalIR;
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonReadid
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: OnButtonReadid event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_READID_clicked()
{
   char          pszLocString[100];
   unsigned long ulIndex;
   unsigned long ulARCID = 0;
   int           iRet;

   if (ulNumberOfARCCores == 0)
   {
      SetupMessage("No core available on scan chain.");
      return;
   }

   // read ID for each ARC core
   if (ptyArcInstance != NULL)
   {
      for (ulIndex=0; ulIndex < ulNumberOfARCCores; ulIndex++)
      {
         // select ARC core
         sprintf(pszLocString, "%ld", ulIndex+1);
         ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "cpunum", pszLocString);
         // read ID (aux register, 0x4)
         iRet = ptyArcInstance->ptyFunctionTable->read_banked_reg(ptyArcInstance, reg_bank_aux, 0x4, &ulARCID);
         switch (iRet)
         {
            case 0x04: //target powered-off
            {
               sprintf(pszLocString, "\tTarget is Powered-off.");
               SetupMessage(pszLocString);
            }
            break;
            case 0x02: //target powered-down
            {
               sprintf(pszLocString, "\tTarget is Powered-down.");
               SetupMessage(pszLocString);
            }
            break;
            case 0x01: // successful read
            {
               sprintf(pszLocString, "\tARC: %3.3ld, ID register: %8.8lX", ulIndex+1, ulARCID);
               SetupMessage(pszLocString);
            }
               break;
            case 0x00: // failed to read ID
            {
               SetupMessage("\tFailed to read ID register.");
            }
            break;
         }


         SetupMessage("Reading of ARC ID register(s) completed.");
      }
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::OnButtonMemTest
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: OnButtonMemTest event
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::on_IDC_BUTTON_MEM_TEST_clicked()
{
   char          pszLocBuffer1[_MAX_PATH];
   char          pszLocBuffer2[_MAX_PATH];
   unsigned char bVerificationFailed = 0;
   unsigned long ulIndex, ulStartAddress, ulEndAddress, ulBlockSize, ulTransferSize, ulTotalSize;
   unsigned long ulCurrentSize;
   double        dReadStartCount, dWriteStartCount, dReadTotal, dWriteTotal;
   int           iResult;

   QString       qString;

   if (ulNumberOfARCCores == 0)
   {
      SetupMessage("No core available on scan chain.");
      return;
   }

   // test given memory range via selected core
   if (ptyArcInstance != NULL)
   {
      // get range parameters and check validity
      qString = ui->IDC_EDIT_MEM_STARTADDR->text();
      strcpy(pszLocBuffer1, qString.toStdString().data());
      qString = ui->IDC_EDIT_MEM_ENDADDR->text();
      strcpy(pszLocBuffer2, qString.toStdString().data());
      if ((sscanf(pszLocBuffer1, "%lX", &ulStartAddress) != 1) ||
          (sscanf(pszLocBuffer2, "%lX", &ulEndAddress) != 1) ||
          (ulEndAddress <= ulStartAddress) || (ulStartAddress & 0x3) || ((ulEndAddress & 0x3) != 0x3))
      {
         SetupMessage("Memory range is not valid or alligned to words");
         return;
      }

      // select processor
      sprintf(pszLocBuffer1, "%d", ui->IDC_COMBO_MEM_CORE->currentIndex() + 1);
      ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "cpunum", pszLocBuffer1);
      // get block size (2kB if frequency < 1 MHz or using adaptive clock or 64 kB if frequency >= 1MHz)
      if (ui->IDC_COMBO_JTAGFREQ->currentIndex() > 52)
         ulBlockSize = 512;                  // 0.5 kB block size
      else if (ui->IDC_COMBO_JTAGFREQ->currentIndex() > 26)
         ulBlockSize = 2*1024;               // 2 kB block size
      else
         ulBlockSize = 64*1024;              // 64 kB block size

      // do transfer
      ulTransferSize = (ulEndAddress - ulStartAddress) + 1;
      ulTotalSize = ulTransferSize;
      // initialize performance measurement
      dReadTotal = 0.0;
      dWriteTotal = 0.0;
      ui->IDC_PROGRESS1->setValue(0);

      ui->IDC_PROGRESS1->setRange(0, 2*(int)(ulTransferSize/ulBlockSize));

      while (ulTransferSize)
      {
         ulCurrentSize = ulTransferSize;

         if (ulCurrentSize > ulBlockSize)
            ulCurrentSize = ulBlockSize;

         // first write memory
         ui->IDC_PROGRESS1->setValue(ui->IDC_PROGRESS1->value() + 1);

         SetupMessage("Writing Memory...");

         for (ulIndex = 0; ulIndex < (ulBlockSize/4); ulIndex++)
         {
            pulMemoryAddress[ulIndex] = ulStartAddress + ulIndex*4;
            pulMemoryWrite[ulIndex]   = (rand() & 0xFFFF) << 16;
            pulMemoryWrite[ulIndex]  |= (rand() & 0xFFFF);
            pulMemoryRead[ulIndex]    = 0x0;
         }

         dWriteStartCount = GetAccurateTickCount();

         iResult = ptyArcInstance->ptyFunctionTable->write_memory(ptyArcInstance, ulStartAddress, (void *)pulMemoryWrite, ulCurrentSize, 0);
         if (iResult != (int)ulCurrentSize)
         {
            if (iResult == 2)
               SetupMessage("Target is either Powered-down or Powered-off.");
            else
               SetupMessage("Failed to write to specified memory address range");

            return;
         }

         dWriteTotal += (GetAccurateTickCount() - dWriteStartCount);

         // now read memory
         ui->IDC_PROGRESS1->setValue(ui->IDC_PROGRESS1->value() + 1);

         SetupMessage("Read Memory...");

         dReadStartCount = GetAccurateTickCount();

         iResult = ptyArcInstance->ptyFunctionTable->read_memory(ptyArcInstance, ulStartAddress, (void *)pulMemoryRead, ulCurrentSize, 0);
         if (iResult != (int)ulCurrentSize)
         {
            if (iResult == 2)
               SetupMessage("Target is either Powered-down or Powered-off.");
            else
               SetupMessage("Failed to read from specified memory address range");

            return;
         }

         dReadTotal += (GetAccurateTickCount() - dReadStartCount);

         // and verify memory
         SetupMessage("Verifying Memory...");

         for (ulIndex = 0; ulIndex < (ulCurrentSize/4); ulIndex++)
         {
            if (pulMemoryWrite[ulIndex] != pulMemoryRead[ulIndex])
               {
               DisplayMemoryFailure(pulMemoryAddress[ulIndex], ulIndex, ulBlockSize);
               bVerificationFailed = 1;
               break;
               }
         }

         // update next address
         ulStartAddress += ulCurrentSize;
         ulTransferSize -= ulCurrentSize;
      }

      if (!bVerificationFailed)
         SetupMessage("Memory access tested successfully");

      // calculate overall performance
      if (dReadTotal < 0.1)
         dReadTotal = 0.1;
      dReadTotal /= 1000;                 // convert to seconds
      if (dWriteTotal < 0.1)
         dWriteTotal = 0.1;
      dWriteTotal /= 1000;                // convert to seconds

      // calculate kB per second
      dReadTotal = ((double)ulTotalSize / 1024.0) / dReadTotal;
      dWriteTotal = ((double)ulTotalSize / 1024.0) / dWriteTotal;

      // show results
      sprintf(pszLocBuffer1, "Read performance is %.1f kBytes/second", dReadTotal);
      sprintf(pszLocBuffer2, "Write performance is %.1f kBytes/second", dWriteTotal);
      SetupMessage(pszLocBuffer1);
      SetupMessage(pszLocBuffer2);
   }
}

/****************************************************************************
     Function: CDiagarcwinDlg::GetAccurateTickCount
     Engineer: Andre Schmiel
        Input: None
       Output: double - count
  Description: return accurate tick count (in milliseconds)
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
double CDiagarcwinDlg::GetAccurateTickCount()
{
   struct timeval tyTime;
   struct timezone tyTz;

   gettimeofday(&tyTime, &tyTz);

   return((tyTime.tv_sec*1000)+(tyTime.tv_usec/1000));
}

/****************************************************************************
     Function: CDiagarcwinDlg::DisplayMemoryFailure
     Engineer: Andre Schmiel
        Input: unsigned long ulAddress - address that failed verification
               unsigned long ulOffset - offset in buffer failing verification
               unsigned long ulBlockSize - size of current memory block
       Output: none
  Description: show information about memory region failing verification
Date           Initials    Description
26-Apr-2017    AS          Initial
****************************************************************************/
void CDiagarcwinDlg::DisplayMemoryFailure(unsigned long ulAddress, unsigned long ulOffset, unsigned long ulBlockSize)
{
   char pszLocString[_MAX_PATH];
   unsigned long ulCnt, ulIndex;

   ulBlockSize /= 4;                                                          // convert to words

   // show error message
   sprintf(pszLocString, "Failed to verify memory contents at address 0x%8.8lX", ulAddress);
   SetupMessage(pszLocString);

   // check offset
   ulCnt = 5;

   if (ulBlockSize < ulCnt)
      {
      ulCnt = ulBlockSize;
      ulOffset = 0;
      }
   else if (ulOffset < 2)
      ulOffset = 0;
   else if ((ulOffset + 2) >= ulBlockSize)
      ulOffset = ulBlockSize - 3;
   else
      ulOffset -= 2;

   // show content of memory
   SetupMessage("Address    | Data Wrote | Data Read");
   SetupMessage("-----------------------------------");

   for (ulIndex=0; ulIndex < ulCnt; ulIndex++)
   {
      sprintf(pszLocString, "0x%8.8lX | 0x%8.8lX | 0x%8.8lX", pulMemoryAddress[ulOffset + ulIndex],
              pulMemoryWrite[ulOffset + ulIndex], pulMemoryRead[ulOffset + ulIndex]);
      SetupMessage(pszLocString);
   }
}
