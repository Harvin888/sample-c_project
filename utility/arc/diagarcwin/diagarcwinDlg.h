/******************************************************************************
       Module: diagarcwinDlg.h
     Engineer: Vitezslav Hola
  Description: Header for main dialog in ARC diagnostic utility
  Date           Initials    Description
  24-Oct-2007    VH          Initial
  19-Jul-2012    SPT         Added a TAP reset using TMS sequence during connection
******************************************************************************/
#if !defined(AFX_DIAGARCWINDLG_H__92B9ABF1_6D79_48A7_B0FE_EEA74FEA0173__INCLUDED_)
#define AFX_DIAGARCWINDLG_H__92B9ABF1_6D79_48A7_B0FE_EEA74FEA0173__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "protocol\common\types.h"

#define NUMBER_OF_ARC_MEMBLOCK      3
#define MEMORY_BLOCK_WORDS          (16384)                 // 64kB

#define MAX_NUMBER_OF_ARC                       256
#define MAX_CORE_WIDTH                          32
#define NUMBER_OF_ARC_FREQ                      62
#define APPLICATION_NAME                        "OPXDARC.EXE"
#define ARC_STD_INIFILE_NAME						"opxdarc.ini"

//global variables for INI file handling
#define ARC_SETUP_SECT_INIFILE               "InitFileLocation"

struct TyFunctionTable
{
   int            (*DummyFunction                )(struct TyArcInstance*);
   int            (*version                      )(struct TyArcInstance*);
   const char *   (*id                           )(struct TyArcInstance*);
   void           (*destroy                      )(struct TyArcInstance*);
   const char *   (*additional_possibilities     )(struct TyArcInstance*);
   void*          (*additional_information       )(struct TyArcInstance*, unsigned);
   int            (*prepare_for_new_program      )(struct TyArcInstance*, int);
   int            (*process_property             )(struct TyArcInstance*, const char *, const char *);
   int            (*is_simulator                 )(struct TyArcInstance*);
   int            (*step                         )(struct TyArcInstance*);
   int            (*run                          )(struct TyArcInstance*);
   int            (*read_memory                  )(struct TyArcInstance*, uint32_t, void *, uint32_t, int);
   int            (*write_memory                 )(struct TyArcInstance*, uint32_t, void *, uint32_t, int );
   int            (*read_reg                     )(struct TyArcInstance*, int, uint32_t *);
   int            (*write_reg                    )(struct TyArcInstance*, int, uint32_t);
   unsigned       (*memory_size                  )(struct TyArcInstance*);
   int            (*set_memory_size              )(struct TyArcInstance*, unsigned);
   int            (*set_reg_watchpoint           )(struct TyArcInstance*, int, int);
   int            (*remove_reg_watchpoint        )(struct TyArcInstance*, int, int);
   int            (*set_mem_watchpoint           )(struct TyArcInstance*, uint32_t, int);
   int            (*remove_mem_watchpoint        )(struct TyArcInstance*, uint32_t, int);
   int            (*stopped_at_watchpoint        )(struct TyArcInstance*);
   int            (*stopped_at_exception         )(struct TyArcInstance*);
   int            (*set_breakpoint               )(struct TyArcInstance*, unsigned, void*);
   int            (*remove_breakpoint            )(struct TyArcInstance*, unsigned, void*);
   int            (*retrieve_breakpoint_code     )(struct TyArcInstance*, unsigned, char *, unsigned, void *);
   int            (*breakpoint_cookie_len        )(struct TyArcInstance*);
   int            (*at_breakpoint                )(struct TyArcInstance*);
   int            (*define_displays              )(struct TyArcInstance*, struct Register_display *);
   int            (*fill_memory                  )(struct TyArcInstance*, uint32_t, void *, uint32_t, uint32_t, int);
   int            (*instruction_trace_count      )(struct TyArcInstance*);
   void           (*get_instruction_traces       )(struct TyArcInstance*, uint32_t *);
   void           (*receive_callback             )(struct TyArcInstance*, ARC_callback*);
   int            (*supports_feature             )(struct TyArcInstance*);
   uint32_t  (*data_exchange                )(struct TyArcInstance*, uint32_t, uint32_t, uint32_t, void *, uint32_t, void *);
   int            (*in_same_process_as_debugger  )(struct TyArcInstance*);
   uint32_t  (*max_data_exchange_transfer   )(struct TyArcInstance*);
   int            (*read_banked_reg              )(struct TyArcInstance*, int, int, uint32_t *);
   int            (*write_banked_reg             )(struct TyArcInstance*, int, int, uint32_t *);
};

struct TyArcInstance
{
   struct TyFunctionTable *ptyFunctionTable;
};

typedef void (*PfPrintFunc)(const char *);
typedef struct TyArcInstance *(*Tyget_ARC_interface)(void);
typedef int (*TyAshTestScanchain)(struct TyArcInstance *);
typedef void (*TyAshGetSetupItem)(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue);
typedef void (*TyAshSetSetupItem)(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue);
typedef void (*TyAshGetLastErrorMessage)(struct TyArcInstance *p, char *pszMessage, uint32_t uiSize);
typedef void (*TyAshSetGeneralMessagePrint)(PfPrintFunc pfFunc);
typedef int (*TyAshTMSReset)(struct TyArcInstance *p);

// **** Added for v105-D
typedef char* (*TyAshGetInterfaceID) (void);

/////////////////////////////////////////////////////////////////////////////
// CDiagarcwinDlg dialog

class CDiagarcwinDlg : public CDialog
{
// Construction
public:
   CDiagarcwinDlg(CWnd* pParent = NULL);	// standard constructor

	char pszGlobalDirPath[_MAX_PATH];
	char pszIniFileLocation[_MAX_PATH];

	char *GetExecutablePath();
   void GetINIFileLocation();
   void GetARCJtagFrequency(unsigned char bDriverFormat, uint32_t uiFreqIndex, char *pszString, uint32_t uiSize);
   void GetARCDetails(void);
   uint32_t LocalReadProfileValue(char *pszKey, uint32_t ulDefault);
   void LocalWriteProfileValue(char *pszKey, uint32_t ulValue);
   CString LocalReadProfileString(char *pszKey, char *pszDefault);
   void LocalWriteProfileString(char *pszKey, char *pszValue);
   void SetupMessage(char *pszMessage);
   void ModifyMessage(char *pszMessage);
   void __cdecl SetupGeneralMessage(const char *pszMessage);
   double GetAccurateTickCount();
   void DisplayMemoryFailure(uint32_t ulAddress, uint32_t ulOffset, uint32_t ulBlockSize);
   void OnJtagFreq(void);
   bool VerifyInterfaceDLL(void);
	

   // public variables
   CProgressCtrl m_ProgressBar;

   HINSTANCE hDllInst;

   TyArcInstance           *ptyArcInstance;
   Tyget_ARC_interface      pfget_ARC_interface;
   TyAshTestScanchain       pfAshTestScanchain;
   TyAshGetSetupItem        pfAshSetSetupItem;
   TyAshGetSetupItem        pfAshGetSetupItem;
   TyAshGetLastErrorMessage pfAshGetLastErrorMessage;
   ARC_callback            *pCallback;
   TyAshTMSReset            pfAshTMSReset;

   TyAshGetInterfaceID      pfAshGetInterfaceId;

   // target details
   uint32_t ulNumberOfARCCores;
   uint32_t ulTotalIRLength;
   // memory block size
   uint32_t pulMemoryBlockSize[NUMBER_OF_ARC_MEMBLOCK];
   // buffers for memory verification
   uint32_t pulMemoryAddress[MEMORY_BLOCK_WORDS];
   uint32_t pulMemoryWrite[MEMORY_BLOCK_WORDS];
   uint32_t pulMemoryRead[MEMORY_BLOCK_WORDS];

// Dialog Data
	//{{AFX_DATA(CDiagarcwinDlg)
	enum { IDD = IDD_DIAGARCWIN_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDiagarcwinDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDiagarcwinDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnAbout();
	afx_msg void OnDestroy();
	afx_msg void OnButtonBrowsedll();
	afx_msg void OnButtonBrowseIni();
	afx_msg void OnButtonBrowsexbf();
	afx_msg void OnCheckXbf();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonDisconnect();
	virtual void OnCancel();
	afx_msg void OnButtonReset();
	afx_msg void OnButtonScantest();
	afx_msg void OnButtonReadid();
	afx_msg void OnButtonMemTest();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

struct My_ARC_callback : ARC_callback {
public:
   CDiagarcwinDlg *pMessageClass;
   My_ARC_callback() { pMessageClass = NULL; return;  }
   virtual int __cdecl version() { return ARCINT_BASE_VERSION;  }
   virtual int __cdecl printf(const char *format, ...) 
   { 
      char pszBuffer[_MAX_PATH];
      va_list ArgList;
      int iResult = 0;

      va_start(ArgList, format);
      iResult = vsprintf(pszBuffer, format, ArgList); 
      va_end(ArgList);
      if (pMessageClass != NULL)
         {
         pMessageClass->ModifyMessage(pszBuffer);
         pMessageClass->SetupMessage(pszBuffer);
         pMessageClass->RedrawWindow(NULL, NULL, RDW_INVALIDATE);
         }
      return iResult; 
   }
   virtual void __cdecl meminfo(unsigned addr, struct Memory_info *m) { return; }
   // end of supported interface 1
   virtual int __cdecl vprintf(const char *format, va_list ap) { return 0; }
   virtual int __cdecl get_verbose() { return 0; }
   // end of interface 2
   virtual void __cdecl sleep(unsigned milliseconds) { Sleep(milliseconds); }
   virtual int __cdecl in_same_process_as_debugger() { return 1; }
   virtual struct ARC_endian* __cdecl get_endian() { return NULL; }
   virtual void * __cdecl get_tcp_factory() { return NULL; }
   virtual void * __cdecl get_process() { return NULL; }
   // end of interface 3
   virtual struct Debugger_access * __cdecl get_debugger_access() { return NULL; }
   // end of version 4 interface
   // pointer to class
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIAGARCWINDLG_H__92B9ABF1_6D79_48A7_B0FE_EEA74FEA0173__INCLUDED_)
 
