#include "AboutDlg_Linux.h"
#include "ui_AboutDlg.h"

CAboutDlg::CAboutDlg(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::CAboutDlg)
{
   ui->setupUi(this);
}

CAboutDlg::~CAboutDlg()
{
   delete ui;
}

void CAboutDlg::on_pushButton_clicked()
{
   this->close();
}
