/******************************************************************************
       Module: AboutDlg.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of aboug dialog window for ARC diagnostic utility
  Date           Initials    Description
  24-Oct-2007    VH          Initial
******************************************************************************/
#include "stdafx.h"
#include "diagarcwin.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//---------------------------------------------------------------------------
//--- AboutDlg class                                                      ---
//---------------------------------------------------------------------------
AboutDlg::AboutDlg(CWnd* pParent /*=NULL*/)  : CDialog(AboutDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(AboutDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

void AboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AboutDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AboutDlg, CDialog)
	//{{AFX_MSG_MAP(AboutDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AboutDlg message handlers
