#include "diagarcwin_linux.h"
#include <QApplication>
#include <QSharedMemory>

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   CDiagarcwinDlg cDiagarcwinDlg;

   cDiagarcwinDlg.OnInitDialog();
   cDiagarcwinDlg.show();

   return a.exec();
}
