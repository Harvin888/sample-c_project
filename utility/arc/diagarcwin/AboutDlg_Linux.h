#ifndef ABOUTDLG_LINUX_H
#define ABOUTDLG_LINUX_H

#include <QDialog>

namespace Ui {
class CAboutDlg;
}

class CAboutDlg : public QDialog
{
   Q_OBJECT

public:
   explicit CAboutDlg(QWidget *parent = 0);
   ~CAboutDlg();

private slots:
   void on_pushButton_clicked();

private:
   Ui::CAboutDlg *ui;
};

#endif // ABOUTDLG_LINUX_H
