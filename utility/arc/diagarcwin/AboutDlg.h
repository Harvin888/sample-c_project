/******************************************************************************
       Module: AboutDlg.h
     Engineer: Vitezslav Hola
  Description: Header for about dialog in ARC diagnostic utility
  Date           Initials    Description
  24-Oct-2007    VH          Initial
******************************************************************************/
#if !defined(AFX_ABOUTDLG_H__3D6E2499_DBF3_4C17_848D_80FDECBF0967__INCLUDED_)
#define AFX_ABOUTDLG_H__3D6E2499_DBF3_4C17_848D_80FDECBF0967__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AboutDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AboutDlg dialog

class AboutDlg : public CDialog
{
// Construction
public:
   AboutDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AboutDlg)
	enum { IDD = IDD_ABOUT_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AboutDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABOUTDLG_H__3D6E2499_DBF3_4C17_848D_80FDECBF0967__INCLUDED_)

