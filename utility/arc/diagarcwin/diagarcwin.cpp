/******************************************************************************
Module: diagarcwin.cpp
Engineer: Vitezslav Hola
Description: Main file for Opella-XD ARC diagnostic utility
Date           Initials    Description
24-Oct-2007    VH          Initial
******************************************************************************/
#include "stdafx.h"
#include "protocol/common/types.h"
#include "diagarcwin.h"
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "arcint.h"
#include "diagarcwinDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDiagarcwinApp

BEGIN_MESSAGE_MAP(CDiagarcwinApp, CWinApp)
	//{{AFX_MSG_MAP(CDiagarcwinApp)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/****************************************************************************
Function: CDiagarcwinApp::CDiagarcwinApp
Engineer: Vitezslav Hola
Input: none
Output: none
Description: class constructor
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
CDiagarcwinApp::CDiagarcwinApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// Local objects
CDiagarcwinApp theApp;

/****************************************************************************
Function: CDiagarcwinApp::InitInstance
Engineer: Vitezslav Hola
Input: none
Output: BOOL
Description: initialize application instance
Date           Initials    Description
24-Oct-2007    VH          Initial
****************************************************************************/
BOOL CDiagarcwinApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
#ifdef _AFXDLL
	//Enable3dControls();			// Call this when using MFC in a shared DLL. This function is no loger required. 
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	CDiagarcwinDlg dlg;
	My_ARC_callback MyARCcallback;   // instance for callback functions

	m_pMainWnd = &dlg;
	// prepare callback pointers in both objects
	MyARCcallback.pMessageClass = &dlg;
	dlg.pCallback = &MyARCcallback;

	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
