#ifdef __LINUX
#define ASH_DLL_API __attribute__ ((visibility ("default")))
#else
#define ASH_DLL_API __declspec(dllexport)
#endif

#include "guiopxdif.h"
#include "../../protocol/common/drvopxd.h"
#include <string.h>

extern "C" ASH_DLL_API void ASH_ListConnectedProbes(tyProbeDetails* pProbeDetails, unsigned short usMaxInstances,
    unsigned short* pusNumInstances)
{
    unsigned short usIndex;
    unsigned short usNumberOfDevices = 0;
    char ppszLocalInstanceNumber[MAX_DEVICES_SUPPORTED][16];
    TyDevHandle tyHandle;
    TyDeviceInfo tyDeviceInfo;
    TyError tyRes;

    //check connected Opella-XDs
    DL_OPXD_GetConnectedDevices(ppszLocalInstanceNumber, &usNumberOfDevices, OPELLAXD_USB_PID);

    //copy list of devices up to maximum number
    if (usNumberOfDevices > usMaxInstances)
        usNumberOfDevices = usMaxInstances;

    for (usIndex = 0; usIndex < usNumberOfDevices; usIndex++) {
        tyRes = DL_OPXD_OpenDevice(ppszLocalInstanceNumber[usIndex], OPELLAXD_USB_PID, false, &tyHandle);
        if (tyRes != DRVOPXD_ERROR_NO_ERROR) {
            usNumberOfDevices = usIndex;
        }
        tyRes = DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN);
        if (tyRes != DRVOPXD_ERROR_NO_ERROR) {
            usNumberOfDevices = usIndex;
        }
        (void)DL_OPXD_CloseDevice(tyHandle);
        strncpy(pProbeDetails[usIndex].szSlNo, ppszLocalInstanceNumber[usIndex], sizeof(pProbeDetails[usIndex].szSlNo));
        if (tyDeviceInfo.ulBoardRevision < 0x03000000) //R2 Opella-XD
            strncpy(pProbeDetails[usIndex].szDeviceName, "Opella-XD R2", sizeof(pProbeDetails[usIndex].szDeviceName));
        else
            strncpy(pProbeDetails[usIndex].szDeviceName, "Opella-XD R3", sizeof(pProbeDetails[usIndex].szDeviceName));
    }
    if (pusNumInstances != NULL)
        * pusNumInstances = usNumberOfDevices;
}
