#ifndef GUIOPXDIF_H_
#define GUIOPXDIF_H_

#ifndef ASH_DLL_API
#ifdef __LINUX
#define ASH_DLL_API
#else
#define ASH_DLL_API __stdcall
#endif
#endif // !ASH_DLL_API

#define SLNO_SIZE 16
#define DEVICENAME_SIZE 50

typedef struct stProbeDetails {
    char szSlNo[SLNO_SIZE];
    char szDeviceName[DEVICENAME_SIZE];
}tyProbeDetails;

#ifdef __cplusplus
extern "C" {
#endif

/**
    Returns the connected probes list
*/
void ASH_DLL_API ASH_ListConnectedProbes(tyProbeDetails *pProbeDetails, unsigned short usMaxInstances,
    unsigned short* pusNumInstances);

#ifdef __cplusplus
}
#endif

#endif // !#ifndef GUIOPXDIF_H_
