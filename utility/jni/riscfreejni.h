#ifdef __LINUX
#else
#include "include/jni.h"
#endif
/* Header for class com_ashling_riscfree_debug_opxd_core_probe_scan_ScanUtil */

#ifndef __RISCFREE_JNI_H__
#define __RISCFREE_JNI_H__

#ifdef __cplusplus
extern "C" {
#endif

    /*
    * Class:     com_ashling_riscfree_debug_opxd_core_probe_scan_ScanUtil
    * Method:    scanOppellaXDInstance
    * Signature: ([Lcom/ashling/riscfree/debug/opxd/core/probe/scan/DebugProbe;Ljava/lang/String;)S
    */
    JNIEXPORT jshort JNICALL Java_com_ashling_riscfree_debug_opxd_core_probe_scan_ScanUtil_scanOppellaXDInstance
    (JNIEnv*, jobject, jobjectArray, jstring);

#ifdef __cplusplus
}
#endif

#endif //#ifndef __RISCFREE_JNI_H__
