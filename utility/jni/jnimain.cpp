/****************************************************************************
       Module: jnimain.cpp
     Engineer: Suresh P. C.
  Description: Implement JNI call functions to call from RiscFree

Date           Initials    Description
16-Jun-2019    SPC         Initial
****************************************************************************/

/** \file
\brief All JNI interfaces for RiscFree are implemented in this file.
\details Functionalities implemented in this file include
- Scan Debug probes.
*/

#include "riscfreejni.h"
#include "guiopxdif.h"
#include <stdio.h>
#include <string.h>

#ifdef __LINUX
#define  LoadLibrary(lib)                 dlopen(lib, RTLD_NOW)
#define  GetProcAddress(handle, proc)     dlsym(handle, proc)
#define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#else
#include <Windows.h>
#endif

#define MAX_DEV_LIST 16

/************************************************************************************************
    Function: Java_com_ashling_riscfree_debug_opxd_core_probe_scan_ScanUtil_scanOppellaXDInstance
    Engineer: Vinod appu
       Input: JNIEnv *jenv  -  pointer to the JNI runtime environment.
              jobject	    -  pointer to the object that called this function
              jobjectArray objarray	-  pointer arrray to the class that was passed as an
                               argument to this function(String array in Java application)
              jstring path  -  gdb server path for linux application to load
                               dll correctly
      Output: jshort		-  zero if Success; else returns a non zero value
 Description: loads opxdrdi.dll and scan opella-xd debug probes

Date          Initials   Description
17-Jun-2019      VA       Initial
**************************************************************************************************/
JNIEXPORT jshort JNICALL Java_com_ashling_riscfree_debug_opxd_core_probe_scan_ScanUtil_scanOppellaXDInstance
(JNIEnv * jenv, jobject job, jobjectArray objarray, jstring path)
{
    //Currently path (GDB Server path) is not used
    jstring tempSlNo;
    jstring tempDevName;
    unsigned short iNumDevs = 0;
    try {
        tyProbeDetails sOpXDslnos[MAX_DEV_LIST];
        ASH_ListConnectedProbes(sOpXDslnos, MAX_DEV_LIST, &iNumDevs);
        jclass debugPrrobe = jenv->FindClass("com/ashling/riscfree/debug/opxd/core/probe/scan/DebugProbe");
        jmethodID mthdConstructor = jenv->GetMethodID(debugPrrobe, "<init>", "()V");
        jmethodID mthdSetDevName = jenv->GetMethodID(debugPrrobe, "setDeviceName", "(Ljava/lang/String;)V");
        jmethodID mthdSetSlNo = jenv->GetMethodID(debugPrrobe, "setSerialNumber", "(Ljava/lang/String;)V");

        //Set serial number scanned in to String(object) array passed from java layer
        for (int i = 0; i < iNumDevs; i++) {
            jobject newObj = jenv->NewObject(debugPrrobe, mthdConstructor);
            tempSlNo = jenv->NewStringUTF(sOpXDslnos[i].szSlNo);
            tempDevName = jenv->NewStringUTF(sOpXDslnos[i].szDeviceName);
            jenv->CallObjectMethod(newObj, mthdSetSlNo, tempSlNo);
            jenv->CallObjectMethod(newObj, mthdSetDevName, tempDevName);
            //jenv->GetFieldID
            jenv->SetObjectArrayElement(objarray, i, newObj);
        }
    } catch (...) {
        return -1;
    }
    return (jshort)iNumDevs;
}
