/******************************************************************************
       Module: diaggen.cpp
     Engineer: Vitezslav Hola
  Description: Binary image generator for Opella-XD diagnostic utility (OpXDDiag)
  Date           Initials    Description
  25-Sep-2006    VH          Initial
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "diaggen.h"
#include "diagglob.h"
#include "diagopt.h"
#include "diagerr.h"

// defines
#define OPXD_MANUFACTURER_NAME          "Ashling Microsystems Ltd."
#define OPXD_PRODUCT_NAME               "Ashling Microsystems Opella-XD"
#define OPXD_DETAIL_PRODUCT_NAME        "Ashling Microsystems Opella-XD device"
#define OPXD_MANUF_SECTION_CODE         0x13A20111
#define OPXD_HW_CODE                    0x07E11A04
#define OPXD_FW_ID_FLAG                 0xC0DE1234
#define OPXD_FW_UPGRADE_FLAG            0x55555555

#define OPXD_MAX_LOADER_LENGTH          0x04000              // max 16kB loader
#define OPXD_MAX_FW_LENGTH              0x3C000              // max 240kB firmware
#define OPXD_MAX_SECTION_LENGTH         0x00800              // 2kB additional section (total 8 sections)

#define GEN_BUFFER_LEN                 (OPXD_MAX_LOADER_LENGTH)
#define GEN_BUFFER_UL                  (GEN_BUFFER_LEN / sizeof(uint32_t))

// local variables
static char pcDefaultManufacturerName[]  = OPXD_MANUFACTURER_NAME;
static char pcDefaultProductName[]       = OPXD_PRODUCT_NAME;
static char pcDefaultDetailProductName[] = OPXD_DETAIL_PRODUCT_NAME;
static uint32_t pulGenBuffer[GEN_BUFFER_UL];

/****************************************************************************
     Function: GenerateProductionImage
     Engineer: Vitezslav Hola
        Input: TyDiagOptions *ptyDiagOptions - program options
       Output: int32_t - error codes DIAG_xxx
  Description: generate binary image for Opella-XD device to external file
               parameters are given by input structure
Date           Initials    Description
26-Sep-2006    VH          Initial
****************************************************************************/
int32_t GenerateProductionImage(TyDiagOptions *ptyDiagOptions)
{ 
   uint32_t ulRevision = 0;
   uint32_t  uiRe, uiMe, uiEe;

   assert(ptyDiagOptions != NULL);
   assert((strlen(pcDefaultManufacturerName) + strlen(pcDefaultProductName) + strlen(pcDefaultDetailProductName) + 3) < (OPXD_MAX_SECTION_LENGTH - 0x20));

   // check serial number
   if ((ptyDiagOptions->pszSerialNumber == NULL) || 
       CheckSerialNumberFormat(ptyDiagOptions->pszSerialNumber))
      return(DIAG_INVALID_SERIAL_NUMBER);

   if ((ptyDiagOptions->pszBoardRevision == NULL) || CheckRevisionFormat(ptyDiagOptions->pszBoardRevision))
      return(DIAG_INVALID_REVISION_NUMBER);

   if (sscanf(ptyDiagOptions->pszBoardRevision, "r%u-m%u-e%u", &uiRe, &uiMe, &uiEe) != 3)
      return(DIAG_INVALID_REVISION_NUMBER);

   if ((uiRe > 255) || (uiMe > 255) || (uiEe > 255))
      return(DIAG_INVALID_REVISION_NUMBER);

   // encode revision nimber into word
   ulRevision |= (uiRe << 24);
   ulRevision |= (uiEe << 16);
   ulRevision |= (uiMe << 8);

   if (ulRevision < 0x03000000)
      return GenerateProductionImage_R2_Board(ptyDiagOptions, ulRevision);
   else
      return GenerateProductionImage_R3_Board(ptyDiagOptions, ulRevision);
}

/****************************************************************************
     Function: GenerateProductionImage_R2_Board
     Engineer: Vitezslav Hola
        Input: TyDiagOptions *ptyDiagOptions - program options
       Output: int32_t - error codes DIAG_xxx
  Description: generate binary image for Opella-XD device to external file
               parameters are given by input structure

Date           Initials    Description
26-Sep-2006    VH          Initial
****************************************************************************/
int32_t GenerateProductionImage_R2_Board(TyDiagOptions *ptyDiagOptions,
                                     uint32_t ulRevision)
{
   int32_t            iCnt;
   char          *pcTmp, *pcFwBuffer;
   uint32_t  ulBytesRead, ulBytesWritten, ulChecksum, ulFwLength, ulDate;
   uint32_t *pulFwBuffer;

   FILE *pOutputFile, *pLoaderFile, *pFwFile;

   ulDate = GetCurrentDate();

   // initialize file pointers
   pOutputFile = NULL;
   pLoaderFile = NULL;
   pFwFile     = NULL;

   // open output file
   pOutputFile = fopen(OPXD_OUTPUT_FILENAME, "wb");
   if (pOutputFile == NULL)
      return(DIAG_MANUF_FILE_ERROR);

   // open file with loader
   pLoaderFile = fopen(OPXD_LOADER_FILENAME, "rb");
   if (pLoaderFile == NULL)
      {
      fclose(pOutputFile);
      return(DIAG_INVALID_LOADER);
      }

   // open file with firmware
   pFwFile = fopen(OPXD_FIRMWARE_FILENAME, "rb");
   if (pFwFile == NULL)
      {
      fclose(pOutputFile);
      fclose(pLoaderFile);
      return(DIAG_INVALID_FIRMWARE);
      }

   // start generating binary image

   // first fill loader
   memset((void *)pulGenBuffer, 0xff, GEN_BUFFER_LEN);            // use 0xff as empty
   ulBytesRead = (uint32_t)fread((char *)pulGenBuffer, 1, GEN_BUFFER_LEN, pLoaderFile);
   if (ulBytesRead == 0)
      {
      fclose(pOutputFile);
      fclose(pLoaderFile);
      fclose(pFwFile);
      return(DIAG_INVALID_LOADER);
      }
   fclose(pLoaderFile);
   ulBytesWritten = (uint32_t)fwrite((char *)pulGenBuffer, 1, GEN_BUFFER_LEN, pOutputFile);
   if (ulBytesWritten != GEN_BUFFER_LEN)
      {
      fclose(pOutputFile);
      fclose(pFwFile);
      return(DIAG_MANUF_FILE_ERROR);
      }

   // we have written whole loader block, now write 8 sections

   // first manufacturing section
   // running on LE, no need to convert words
   memset((void *)pulGenBuffer, 0x00, OPXD_MAX_SECTION_LENGTH);
   pulGenBuffer[0] = OPXD_MANUF_SECTION_CODE;                           // fill manufacturing section code
   pulGenBuffer[1] = OPXD_HW_CODE;
   pulGenBuffer[2] = ulDate;
   pulGenBuffer[3] = ulRevision;
   // copy serial number
   strcpy((char *)&(pulGenBuffer[4]), ptyDiagOptions->pszSerialNumber);
   pcTmp = (char *)pulGenBuffer;
   pcTmp[0x19] = 0;                                                     // ensure serial number is terminated with 0x0
   // copy manufacturer name
   pcTmp += 0x20;
   strcpy(pcTmp, pcDefaultManufacturerName);
   pcTmp += (strlen(pcDefaultManufacturerName) + 1);
   // copy product name
   strcpy(pcTmp, pcDefaultProductName);
   pcTmp += (strlen(pcDefaultProductName) + 1);
   // copy detail product name
   strcpy(pcTmp, pcDefaultDetailProductName);
   pcTmp += (strlen(pcDefaultDetailProductName) + 1);
   // write section to output file
   ulBytesWritten = (uint32_t)fwrite((char *)pulGenBuffer, 1, OPXD_MAX_SECTION_LENGTH, pOutputFile);
   if (ulBytesWritten != OPXD_MAX_SECTION_LENGTH)
      {
      fclose(pOutputFile);
      fclose(pFwFile);
      return(DIAG_MANUF_FILE_ERROR);
      }

   // write other 7 empty sections
   for(iCnt=0; iCnt<7; iCnt++)
      {
      memset((void *)pulGenBuffer, 0xff, OPXD_MAX_SECTION_LENGTH);
      ulBytesWritten = (uint32_t)fwrite((char *)pulGenBuffer, 1, OPXD_MAX_SECTION_LENGTH, pOutputFile);
      if (ulBytesWritten != OPXD_MAX_SECTION_LENGTH)
         {
         fclose(pOutputFile);
         fclose(pFwFile);
         return(DIAG_MANUF_FILE_ERROR);
         }
      }

   // all sections written, now write firmware
   // allocate buffer for firmware
   pcFwBuffer = (char *)malloc(OPXD_MAX_FW_LENGTH);
   if (pcFwBuffer == NULL)
      {
      fclose(pOutputFile);
      fclose(pFwFile);
      return(DIAG_MANUF_FILE_ERROR);
      }
   memset(pcFwBuffer, 0xff, OPXD_MAX_FW_LENGTH);
   pulFwBuffer = (uint32_t *)pcFwBuffer;
   ulFwLength = 0;
   ulBytesRead = (uint32_t)fread(pcFwBuffer, 1, OPXD_MAX_FW_LENGTH, pFwFile);
   if (ulBytesRead < 0x20)          // too short firmware
      {  
      free(pcFwBuffer);
      fclose(pOutputFile);
      fclose(pFwFile);
      return(DIAG_INVALID_FIRMWARE);
      }
   fclose(pFwFile);

   // check for valid firmware and calculate checksum
   ulFwLength = pulFwBuffer[3];
   if ((pulFwBuffer[0] != OPXD_FW_ID_FLAG) ||
       (ulFwLength > OPXD_MAX_FW_LENGTH)
       )
      {
      free(pcFwBuffer);
      fclose(pOutputFile);
      return(DIAG_INVALID_FIRMWARE);
      }
   // set valid upgrade flag
   pulFwBuffer[1] = OPXD_FW_UPGRADE_FLAG;
   // calculate correct checksum
   ulChecksum = pulFwBuffer[0];
   ulChecksum += pulFwBuffer[1];
   // exclude pulFwBuffer[2] (current value of checksum)
   for(iCnt=3; iCnt < (int32_t)(ulFwLength/sizeof(uint32_t)); iCnt++)
      {
      ulChecksum += pulFwBuffer[iCnt];
      }
   // fill correct checksum (two complement)
   pulFwBuffer[2] = (0xFFFFFFFF - ulChecksum) + 0x00000001;

   // write firmware image to output file
   ulBytesWritten = (uint32_t)fwrite(pcFwBuffer, 1, OPXD_MAX_FW_LENGTH, pOutputFile);
   if (ulBytesWritten != OPXD_MAX_FW_LENGTH)
      {
      free(pcFwBuffer);
      fclose(pOutputFile);
      return(DIAG_MANUF_FILE_ERROR);
      }

   // everything ok, just free all memory and close file
   free(pcFwBuffer);
   fclose(pOutputFile);
   return(DIAG_NO_ERROR);
}

/****************************************************************************
     Function: GenerateProductionImage_R3_Board
     Engineer: Vitezslav Hola
        Input: TyDiagOptions *ptyDiagOptions - program options
       Output: int32_t - error codes DIAG_xxx
  Description: generate binary image for Opella-XD device to external file
               parameters are given by input structure

Date           Initials    Description
26-Sep-2006    VH          Initial
****************************************************************************/
int32_t GenerateProductionImage_R3_Board(TyDiagOptions *ptyDiagOptions,
                                     uint32_t ulRevision)
{
   char          *pcTmp;
   uint32_t  ulBytesWritten, ulDate;

   FILE *pOutputFile;

   ulDate = GetCurrentDate();

   // initialize file pointers
   pOutputFile = NULL;

   // open output file
   pOutputFile = fopen(OPXD_OUTPUT_FILENAME, "wb");
   if (pOutputFile == NULL)
      return(DIAG_MANUF_FILE_ERROR);

   // start generating binary image
   // first manufacturing section
   // running on LE, no need to convert words
   memset((void *)pulGenBuffer, 0x00, OPXD_MAX_SECTION_LENGTH);
   pulGenBuffer[0] = OPXD_MANUF_SECTION_CODE;                           // fill manufacturing section code
   pulGenBuffer[1] = OPXD_HW_CODE;
   pulGenBuffer[2] = ulDate;
   pulGenBuffer[3] = ulRevision;
   // copy serial number
   strcpy((char *)&(pulGenBuffer[4]), ptyDiagOptions->pszSerialNumber);
   pcTmp = (char *)pulGenBuffer;
   pcTmp[0x19] = 0;                                                     // ensure serial number is terminated with 0x0
   // copy manufacturer name
   pcTmp += 0x20;
   strcpy(pcTmp, pcDefaultManufacturerName);
   pcTmp += (strlen(pcDefaultManufacturerName) + 1);
   // copy product name
   strcpy(pcTmp, pcDefaultProductName);
   pcTmp += (strlen(pcDefaultProductName) + 1);
   // copy detail product name
   strcpy(pcTmp, pcDefaultDetailProductName);
   pcTmp += (strlen(pcDefaultDetailProductName) + 1);
   // write section to output file
   ulBytesWritten = (uint32_t)fwrite((char *)pulGenBuffer, 1, OPXD_MAX_SECTION_LENGTH, pOutputFile);
   if (ulBytesWritten != OPXD_MAX_SECTION_LENGTH)
      {
      fclose(pOutputFile);
      return(DIAG_MANUF_FILE_ERROR);
      }

   // everything ok, just free all memory and close file
   fclose(pOutputFile);
   return(DIAG_NO_ERROR);
}
