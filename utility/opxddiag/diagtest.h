/******************************************************************************
       Module: diagtest.h
     Engineer: Vitezslav Hola
  Description: Header for diagnostic tests
  Date           Initials    Description
  06-Oct-2006    VH          Initial
******************************************************************************/
#ifndef _DIAGTEST_H_
#define _DIAGTEST_H_

#include "protocol/common/drvopxd.h"

// diagnostic error messages
// general diagnostic errors
#define DIAG_TEST_DRIVER_ERROR_MSG           "An error occurred while communicating with the Opella-XD driver"
#define DIAG_TEST_I2C_ERROR                  "An error detected on I2C bus"
// CPU test errors
#define DIAG_TEST_CPU_ERROR_MSG              "An error occurred during the internal Opella-XD CPU test"
// External RAM errors
#define DIAG_TEST_RAM_INVALID_SIZE_MSG       "Invalid SRAM size detected"
#define DIAG_TEST_RAM_ERROR_ADDRESS          "An error detected on SRAM address bus line %d"
#define DIAG_TEST_RAM_ERROR_DATA             "An error detected on SRAM data bus line %d"
#define DIAG_TEST_RAM_ERROR_RANDOM           "An error detected during random pattern test"
// voltage sensing/adjusting errors
#define DIAG_TEST_VOLTAGE_USB_ERROR_MSG      "USB voltage is invalid (outside range of 4.25V to 5.25V)"
#define DIAG_TEST_VOLTAGE_ADJUST_ERROR_MSG   "TPA voltage set to %2.1fV but measured as %2.1fV"
// FPGA test errors
#define DIAG_TEST_FPGA_CONFIG_ERROR_MSG      "An error occurred during FPGA configuration"
#define DIAG_TEST_FPGA_INVALID_ID            "Invalid FPGA ID register"
#define DIAG_TEST_FPGA_ERROR_XSYSCLK         "An error detected on XSYSCLK signal"
#define DIAG_TEST_FPGA_ERROR_XBS             "An error detected on nXBS%d signal"
#define DIAG_TEST_FPGA_ERROR_FPGA_INIT       "An error detected on INIT_FPGA signal"
#define DIAG_TEST_FPGA_ERROR_FPGA_IRQ        "An error detected on FPGA_IRQ signal"
#define DIAG_TEST_FPGA_ERROR_ADDRESS         "An error detected on FPGA address bus line %d"
#define DIAG_TEST_FPGA_ERROR_DATA            "An error detected on FPGA data bus line %d"
#define DIAG_TEST_FPGA_ERROR_DMADREQ         "An error detected on DMA_DREQ signal"
#define DIAG_TEST_FPGA_ERROR_DMADREQCLR      "An error detected on DMA_DREQCLR signal"
#define DIAG_TEST_FPGA_ERROR_DMATCOUT        "An error detected on DMA_TCOUT signal"
// PLL test errors
#define DIAG_TEST_PLL_FREQ_ERROR_MSG         "PLL frequency set to %3.2fMHz but measured %3.2fMHz"
#define DIAG_TEST_PLL_INVALID_REFERENCE      "Invalid frequency reference detected on Opella-XD"
#define DIAG_TEST_PLL_ERROR_JTAGCLK          "An error detected on JTACLK%d signal"
// TPA test errors
#define DIAG_TEST_TPA_PRODUCTION_BOARD       "Cannot detect Opella-XD production test board"
#define DIAG_TEST_TPA_ERROR_DIO_P            "An error detected on TPA interface DIO%dP signal"
#define DIAG_TEST_TPA_ERROR_DIO_N            "An error detected on TPA interface DIO%dN signal"
#define DIAG_TEST_TPA_ERROR_FSIO             "An error detected on TPA interface FSIO%d signal"
#define DIAG_TEST_TPA_ERROR_CSIO             "An error detected on TPA interface CSIO%d signal"
#define DIAG_TEST_TPA_ERROR_LOOP             "An error detected on TPA interface LOOP signal"
#define DIAG_TEST_TPA_ERROR_ABSENT           "An error detected on TPA interface ABSENT signal"
#define DIAG_TEST_TPA_ERROR_VTPA             "An error detected on TPA interface Vtpa signal"
#define DIAG_TEST_TPA_ERROR_VTPA_2           "An error detected on TPA interface Vtpa2 signal"
#define DIAG_TEST_TPA_ERROR_VTPA_3V3         "An error detected on TPA interface Vtpa33 signal"
#define DIAG_TEST_TPA_ERROR_VTPA_2V5         "An error detected on TPA interface Vtpa25 signal"
#define DIAG_TEST_TPA_ERROR_LED              "An error detected on TPA interface LED signal"
#define DIAG_TEST_TPA_ERROR_VTREF            "An error detected on TPA interface Vtref signal"

// function prototypes
int32_t RunOpellaLedDiagnostic(char *pszSerialNumber);
int32_t RunOpellaCpuDiagnostic(char *pszSerialNumber);
int32_t RunOpellaExtramDiagnostic(char *pszSerialNumber);
int32_t RunOpellaVoltageDiagnostic(char *pszSerialNumber);
int32_t RunOpellaFpgaDiagnostic(char *pszSerialNumber);
int32_t RunOpellaPllDiagnostic(char *pszSerialNumber);
int32_t RunOpellaTpaDiagnostic(char *pszSerialNumber, unsigned char bProductionBoardRequired);
int32_t RunOpellaTpaLoopDiagnostic(char *pszSerialNumber, unsigned char bTpaProductionDiagnostic, unsigned char bTpaAdaptor);

int32_t RunOpellaPerformanceMeasurement(char *pszSerialNumber);

int32_t PrepareTpaSigniture(const char *pszTpaType, const char *pszTpaSerialNumber, const char *pszTpaRevision, TyTpaParameters *ptyTpaParams);
int32_t PrepareForJtagConsole(TyDevHandle tyHandle);

int32_t UpdateFirmware(char *pszSerialNumber);
int32_t CheckForFirmware(char *pszSerialNumber);

void PrintDiagnosticErrorMessage(char *pszErrorMessage, uint32_t uiSize, int32_t iErrorCode);

#endif // _DIAGTEST_H_


