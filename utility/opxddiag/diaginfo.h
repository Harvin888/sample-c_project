/******************************************************************************
       Module: diaginfo.h
     Engineer: Vitezslav Hola
  Description: Header for diagnostic information
  Date           Initials    Description
  02-Oct-2006    VH          Initial
******************************************************************************/
#ifndef _DIAGINFO_H_
#define _DIAGINFO_H_

#include "diagopt.h"
#include "protocol/common/drvopxd.h"

#define INFO_VERSION_DATE_LEN          25
#define INFO_SERIAL_NUMBER_LEN         16

typedef struct _TyFirmwareInfo {
   char pszVersion[INFO_VERSION_DATE_LEN];
   char pszDate[INFO_VERSION_DATE_LEN];
}TyFirmwareInfo;

typedef struct _TyDiskwareInfo {
   char pszVersion[INFO_VERSION_DATE_LEN];
   char pszDate[INFO_VERSION_DATE_LEN];
   char pszReqFwVersion[INFO_VERSION_DATE_LEN];
   TyDiskwareType tyDwType;
}TyDiskwareInfo;

typedef struct _TyDevInfo {
   char pszManufDate[INFO_VERSION_DATE_LEN];
   char pszBoardRevision[INFO_VERSION_DATE_LEN];
   char pszFwVersion[INFO_VERSION_DATE_LEN];
   char pszFwDate[INFO_VERSION_DATE_LEN];
   TyTpaType tyTpaType;
   char pszTpaRevision[INFO_VERSION_DATE_LEN];
   char pszTpaSerialNumber[INFO_SERIAL_NUMBER_LEN];
}TyDevInfo;

// function prototypes
int32_t GetFirmwareInfo(char *pszFirmwareFilename, TyFirmwareInfo *ptyFirmwareInfo);
int32_t GetDiskwareInfo(char *pszDiskwareFilename, TyDiskwareInfo *ptyDiskwareInfo);
int32_t GetDeviceInfo(char *pszSerialNumber, TyDevInfo *ptyDevInfo);

void ConvertVersionString(uint32_t ulVersion, char *pszVersionString);
void ConvertDateString(uint32_t ulDate, char *pszDateString);
void ConvertPcbVersionString(uint32_t ulVersion, char *pszVersionString);

#endif // _DIAGINFO_H_


