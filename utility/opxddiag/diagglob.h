/******************************************************************************
       Module: diagglob.h
     Engineer: Vitezslav Hola
  Description: Global header for OpXDDiag
  Date           Initials    Description
  25-Sep-2006    VH          Initial
******************************************************************************/
#ifndef _DIAGGLOB_H_
#define _DIAGGLOB_H_

// uncomment following lines if you want to add test commands for particular target
//#define REDPINE_CMDS
//#define ARC_CMDS
//#define ARM_CMDS
//#define RISCV_CMDS

// Program title and version number
#define OPXDDIAG_PROG_NAME              "OPXDDIAG"

#define OPXDDIAG_PROG_TITLE             "Opella-XD Diagnostic Utility (" OPXDDIAG_PROG_NAME ")."
#define OPXDDIAG_PROG_VERSION           "v1.3.0 28-Jun-2019, (c)Ashling Microsystems Ltd 2019."


// Opella-XD serial number length
#define OPXD_MAX_SN_LENGTH              15

// OpxDiag default filenames
#define OPXD_OUTPUT_FILENAME                    "opxdmanf.bin"
#define OPXD_LOADER_FILENAME                    "opxdldr.bin"
#define OPXD_FIRMWARE_FILENAME                  "opxdfw.bin"
#define OPXD_FIRMWARE_FILENAME_LPC1837          "opxdfw_nxplpc.bin"
#define OPXD_FLASHUTIL_FILENAME_LPC1837         "opxdflsh_nxplpc.bin"
#define OPXD_DISKWARE_MIPS_FILENAME             "opxddmip.bin"
#define OPXD_DISKWARE_MIPS_FILENAME_LPC1837     "opxddmip_nxplpc.bin"
#define OPXD_DISKWARE_ARM_FILENAME              "opxddarm.bin"
#define OPXD_DISKWARE_ARM_FILENAME_LPC1837      "opxddarm_nxplpc.bin"
#define OPXD_DISKWARE_ARC_FILENAME              "opxddarc.bin"
#define OPXD_DISKWARE_ARC_FILENAME_LPC1837      "opxddarc_nxplpc.bin"
#define OPXD_DISKWARE_RISCV_FILENAME            "opxddriscv.bin"
#define OPXD_DISKWARE_RISCV_LPC1837_FILENAME    "opxddriscv_r3.bin"
#define OPXD_DISKWARE_TARCH_FILENAME            "opxddtha.bin"
#define OPXD_DISKWARE_TARCH_FILENAME_LPC1837    "opxddtha_nxplpc.bin"

#define OPXD_FPGAWARE_MIPS_FILENAME             "opxdfmip.bin"
#define OPXD_FPGAWARE_MIPS_LPC1837_FILENAME     "opxdfmip_nxplpc.bin"
#define OPXD_FPGAWARE_ARC_FILENAME              "opxdfarc.bin"
#define OPXD_FPGAWARE_ARC_TPA_R1_FILENAME       "opxdfarc_tpa_r1.bin"
#define OPXD_FPGAWARE_ARC_LPC1837_FILENAME      "opxdfarc_nxplpc.bin"
#define OPXD_FPGAWARE_ARM_FILENAME              "opxdfarm.bin"
#define OPXD_FPGAWARE_RISCV_FILENAME            "opxdfriscv.bin"
#define OPXD_FPGAWARE_RISCV_TPA_R1_FILENAME     "opxdfriscv_tpa_r1.bin"
#define OPXD_FPGAWARE_RISCV_LPC1837_FILENAME    "opxdfriscv_nxplpc.bin"
#define OPXD_FPGAWARE_ARM_LPC1837_FILENAME      "opxdfarm_nxplpc.bin"
#define OPXD_FPGAWARE_DIAG_FILENAME             "opxdfdia.bin"
#define OPXD_FPGAWARE_DIAG_LPC1837_FILENAME     "opxdfdia_nxplpc.bin"

#endif // _DIAGGLOB_H_


