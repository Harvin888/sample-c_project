/******************************************************************************
       Module: diagtest.h
     Engineer: Vitezslav Hola
  Description: Implementation of diagnostic tests for Opella-XD
  Date           Initials    Description
  06-Oct-2006    VH          Initial
  11-Jan-2008    VH          Implementation for Linux
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "diagglob.h"
#include "diagtest.h"
#include "diaginfo.h"
#include "diagerr.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdtarch.h"
#include "protocol/common/drvopxdmips.h"
#include "protocol/common/drvopxdarc.h"
#include "protocol/common/drvopxdarm.h"
#include "protocol/common/drvopxdriscv.h"
#include "protocol/rdi/opxdarmdll/common.h"

#ifndef __LINUX
#include <windows.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif

// defines
#define SLEEP_TIME            1000                          // wait for 1000 ms
#define MAX_JTAG_TEST_FREQ    100.0
#define MAX_JTAG_TEST_FREQ_R2 100.0
#define MAX_JTAG_TEST_FREQ_R3 60.0

#ifdef __LINUX
#ifndef _snprintf
#define _snprintf snprintf
#endif
#endif

// LED control in Opella-XD (check schematics)
#define LED_D1_R           0xFFFEFFFE
#define LED_D2_R           0xFFF7FFF7
#define LED_D2_G           0xFFEFFFEF
#define LED_D3_Y           0xFFFDFFFD
#define LED_D3_G           0xFFFBFFFB
#define LED_D4_R           0xFFDFFFDF
#define LED_D4_G           0xFFBFFFBF
#define LED_TPA            0xFF7FFF7F

//Uncomment the below line if you want to use adaprive clocking
//#define ADAPTIVE_CLOCK
//Uncomment the below line if you want to use Multicore auto configuration
//#define ARM_CHECK_MULTI_CORE

//external Variables
extern TyDiagOptions tyDiagOptions;

TyTargetConfig tyTargetConfig;

// local variables
static char pszErrorMessage[256];

// local function prototypes
static int32_t RunTpaDiagnostic(TyDevHandle tyHandle, TyTpaType tyTpaType, unsigned char bAdaptorBoard, char *pszPinName,
                            uint32_t uiTPARev);
static void MsSleep(uint32_t uiMiliseconds);
static uint64_t UniGetTickCount(void);

/****************************************************************************
     Function: RunOpellaLedDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella LED diagnostic test
               turn on each LED for one second
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaLedDiagnostic(char *pszSerialNumber)
{
   TyDevHandle tyHandle;
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;
   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);                     // ensure firmware is running
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing LED indicators            ...");
   fflush(stdout);
   // enable diagnostic mode with all LEDs turned of
   tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, 0xFFFFFFFF);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);
   
   // turn Power/USB (G) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D4_G);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Power/USB (R) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D4_R);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Power/USB (G and R) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D4_G & LED_D4_R);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target data (G) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D3_G);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target data (Y) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D3_Y);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target data (G and Y) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D3_G & LED_D3_Y);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target status (G) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D2_G);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target status (R) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D2_R);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target status (G and R) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D2_G & LED_D2_R);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn Target reset (R) on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_D1_R);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn TPA led on
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, TRUE, LED_TPA);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   // turn all LEDs off and reenable normal mode
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_LedDiagnosticTest(tyHandle, FALSE, 0xFFFFFFFF);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(SLEEP_TIME);

   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      (void)DL_OPXD_CloseDevice(tyHandle);
      return DIAG_TEST_OPELLA_ERROR;
      }

   printf(" OK\n");
   (void)DL_OPXD_CloseDevice(tyHandle);              // device should be unconfigured
   return(DIAG_NO_ERROR);
}

/****************************************************************************
     Function: RunOpellaCpuDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella CPU diagnostic test
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaCpuDiagnostic(char *pszSerialNumber)
{
   TyDevHandle tyHandle;
   TyError tyRes;
   int32_t iRet = DIAG_NO_ERROR;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);

   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing CPU                       ...");
   fflush(stdout);

   tyRes = DL_OPXD_CpuDiagnosticTest(tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      printf(" OK\n");
   else
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      iRet = DIAG_TEST_OPELLA_ERROR;
      }

   // device should be unconfigured
   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}

/****************************************************************************
     Function: RunOpellaExtramDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella external SRAM diagnostic test
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaExtramDiagnostic(char *pszSerialNumber)
{
   TyDevHandle tyHandle;
   TyError tyRes;
   uint32_t ulMemSize;
   int32_t iRet = DIAG_NO_ERROR;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing SRAM                      ...");
   fflush(stdout);
   tyRes = DL_OPXD_ExtramDiagnosticTest(tyHandle, &ulMemSize);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      printf(" OK (%d kB detected)\n", (uint32_t)(ulMemSize/1024));
   else
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      iRet = DIAG_TEST_OPELLA_ERROR;
      }
   // device should be unconfigured
   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}

/****************************************************************************
     Function: RunOpellaVoltageDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella voltage sense/adjust diagnostic test
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaVoltageDiagnostic(char *pszSerialNumber)
{
   TyDevHandle tyHandle;
   TyError tyRes;
   double dUsbVoltage, dVtrefVoltage;
   double pdVtpaVoltage[14];
   int32_t iCnt;
   int32_t iRet = DIAG_NO_ERROR;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // initialize testing voltage values for Vtpa
   // voltage should be rising due to faster adjustment on Vtpa
   pdVtpaVoltage[0]  = 0.6;
   pdVtpaVoltage[1]  = 0.9;
   pdVtpaVoltage[2]  = 1.2;
   pdVtpaVoltage[3]  = 1.5;
   pdVtpaVoltage[4]  = 1.8;
   pdVtpaVoltage[5]  = 2.2;
   pdVtpaVoltage[6]  = 2.5;
   pdVtpaVoltage[7]  = 2.7;
   pdVtpaVoltage[8]  = 3.0;
   pdVtpaVoltage[9]  = 3.2;
   pdVtpaVoltage[10] = 3.3;
   pdVtpaVoltage[11] = 3.6;
   pdVtpaVoltage[12] = 4.9;
   pdVtpaVoltage[13] = 5.0;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   // first sense Vtref and Vusb voltage and show result
   printf("  Testing USB voltage               ...");
   fflush(stdout);
   tyRes = DL_OPXD_VoltageSense(tyHandle, VS_USB, &dUsbVoltage);
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      {
      printf(" ERROR\n");
      printf("%s\n", DIAG_TEST_DRIVER_ERROR_MSG);
      (void)DL_OPXD_CloseDevice(tyHandle);
      return(DIAG_TEST_OPELLA_ERROR);
      }
   
   if ((dUsbVoltage < 4.6) || (dUsbVoltage > 5.4))          // range is <4.75; 5.25> but let put some tolerance
      {
      printf(" ERROR (Vusb = %2.1fV)\n", dUsbVoltage);
      printf(DIAG_TEST_VOLTAGE_USB_ERROR_MSG);
      printf("\n");
      (void)DL_OPXD_CloseDevice(tyHandle);
      return (DIAG_TEST_PC_ERROR);
      }
   else
      {
      printf(" OK (Vusb = %2.1fV)\n", dUsbVoltage);
      }

   printf("  Testing Target reference voltage  ...");
   fflush(stdout);
   tyRes = DL_OPXD_VoltageSense(tyHandle, VS_VTREF, &dVtrefVoltage);
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      {
      printf(" ERROR\n");
      printf("%s\n", DIAG_TEST_DRIVER_ERROR_MSG);
      (void)DL_OPXD_CloseDevice(tyHandle);
      return(DIAG_TEST_OPELLA_ERROR);
      }

   printf(" OK (Vtref = %2.1fV)\n", dVtrefVoltage);

   printf("  Testing Voltage adjusting         ...");
   fflush(stdout);

   tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, FALSE);       // set TPA to tracking mode

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      MsSleep(1000);

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      {
      TyDeviceInfo tyDeviceInfo;

      tyRes = DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         {
         switch (tyDeviceInfo.tyTpaConnected)
            {
            case TPA_UNKNOWN:
            case TPA_INVALID:
            case TPA_NONE:
               printf(" N/A (TPA required)\n");
               (void)DL_OPXD_CloseDevice(tyHandle);
               return (iRet);

            case TPA_MIPS14:
            case TPA_ARC20:
            case TPA_ARM20:
            case TPA_DIAG:
            default:             // restrict voltage range for current TPA
               {
               for (iCnt=0; iCnt < 14; iCnt++)
                  {
                  if (tyDeviceInfo.dMinVtpa > pdVtpaVoltage[iCnt])
                     pdVtpaVoltage[iCnt] = tyDeviceInfo.dMinVtpa;
                  if (tyDeviceInfo.dMaxVtpa < pdVtpaVoltage[iCnt])
                     pdVtpaVoltage[iCnt] = tyDeviceInfo.dMaxVtpa;
                  }
               }
               break;
            }
         }
      }

   for(iCnt=0; iCnt<14; iCnt++)
      {
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         {
         double dMeasuredVoltage = 0;

         tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, pdVtpaVoltage[iCnt], FALSE, FALSE);       // set fixed voltage
         if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            MsSleep(1000);
         if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            tyRes = DL_OPXD_VoltageSense(tyHandle, VS_VTPA, &dMeasuredVoltage);
         if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            {
            // check measured voltage (use 10% tolerance from actual value)
            if ((dMeasuredVoltage > (pdVtpaVoltage[iCnt] * 1.1)) || (dMeasuredVoltage < (pdVtpaVoltage[iCnt] * 0.9)))
               {
               printf(" ERROR\n");
               printf(DIAG_TEST_VOLTAGE_ADJUST_ERROR_MSG, pdVtpaVoltage[iCnt], dMeasuredVoltage);
               printf("\n");
               (void)DL_OPXD_CloseDevice(tyHandle);
               return (DIAG_TEST_OPELLA_ERROR);
               }
            }
         else
            break;
         }
      }

   tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, FALSE);          // restore tracking voltage after diagnotics with drivers disabled
   MsSleep(1000);

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      {
      printf(" OK (%2.1fV to %2.1fV)\n", pdVtpaVoltage[0], pdVtpaVoltage[13]);      // show tested range
      }
   else
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      iRet = DIAG_TEST_OPELLA_ERROR;
      }
   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}

/****************************************************************************
     Function: RunOpellaFpgaDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella FPGA diagnostic test
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaFpgaDiagnostic(char *pszSerialNumber)
{
   int32_t     iRet = DIAG_NO_ERROR;
   TyError tyRes;

   TyDevHandle  tyHandle;
   TyDeviceInfo tyDeviceInfo;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing FPGA                      ...");
   fflush(stdout);
   // configure FPGA as diagnostic

   if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DIAG_DRIVER_ERROR;

   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
      tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_FILENAME);
   else
      tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_LPC1837_FILENAME);

   // if configured, run FPGA diagnostics
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_FpgaDiagnosticTest(tyHandle);
   (void)DL_OPXD_UnconfigureDevice(tyHandle);

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      printf(" OK\n");
   else
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      iRet = DIAG_TEST_OPELLA_ERROR;
      }

   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}

/****************************************************************************
     Function: RunOpellaPllDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella clock generator diagnostic test
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaPllDiagnostic(char *pszSerialNumber)
{
   int32_t     iRet = DIAG_NO_ERROR;
   TyError tyRes;

   TyDevHandle  tyHandle;
   TyDeviceInfo tyDeviceInfo;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);

   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing Clock generator           ...");
   fflush(stdout);

   if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DIAG_DRIVER_ERROR;

   // configure FPGA as diagnostic
   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
      tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_FILENAME);
   else
      tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_LPC1837_FILENAME);

   // if configured, run FPGA diagnostics
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_PllDiagnosticTest(tyHandle);
   (void)DL_OPXD_UnconfigureDevice(tyHandle);

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      printf(" OK\n");
   else
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      iRet = DIAG_TEST_OPELLA_ERROR;
      }

   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}

/****************************************************************************
     Function: RunOpellaTpaDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
               unsigned char bProductionBoardRequired - > 0 if production test required
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella TPA interface diagnostic test
               when produciton test board is required and not connected, this function returns error
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaTpaDiagnostic(char *pszSerialNumber, unsigned char bProductionBoardRequired)
{
   int32_t     iRet = DIAG_NO_ERROR;
   TyError tyRes;

   TyDevHandle  tyHandle;
   TyDeviceInfo tyDeviceInfo;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing TPA interface             ...");
   fflush(stdout);

   // get information about TPA, etc.
   tyRes = DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN);

   if ((tyRes == DRVOPXD_ERROR_NO_ERROR) && bProductionBoardRequired && (tyDeviceInfo.tyTpaConnected != TPA_DIAG))
      {
      printf(" ERROR\n");
      printf("%s\n", DIAG_TEST_TPA_PRODUCTION_BOARD);
      (void)DL_OPXD_CloseDevice(tyHandle);
      return DIAG_TEST_OPELLA_ERROR;
      }

   // run extensive TPA interface test if Opella-XD production test board connected
   if ((tyRes == DRVOPXD_ERROR_NO_ERROR) && (tyDeviceInfo.tyTpaConnected == TPA_DIAG))
      {
      if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
         return DIAG_DRIVER_ERROR;

      // configure FPGA as diagnostic
      if (tyDeviceInfo.ulBoardRevision < 0x03000000)
         tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_FILENAME);
      else
         tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_LPC1837_FILENAME);

      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_TpaInterfaceDiagnosticTest(tyHandle);
      
      (void)DL_OPXD_UnconfigureDevice(tyHandle);
      }

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      {
      switch (tyDeviceInfo.tyTpaConnected)
         {
         case TPA_UNKNOWN:
         case TPA_INVALID:
            printf(" ERROR\n");
            iRet = DIAG_INVALID_TPA;
            break;

         case TPA_NONE:
            printf(" N/A (TPA required)\n");
            break;

         case TPA_MIPS14:
         case TPA_ARM20:
         case TPA_ARC20:
         case TPA_DIAG:
         default:
            printf(" OK (TPA %s)\n", tyDeviceInfo.pszTpaName);
            break;
         }
      }
   else
      {
      printf(" ERROR\n");
      PrintDiagnosticErrorMessage(pszErrorMessage, 256, (int32_t)tyRes);
      printf("%s\n", pszErrorMessage);
      iRet = DIAG_TEST_OPELLA_ERROR;
      }

   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}

/****************************************************************************
     Function: RunOpellaTpaLoopDiagnostic
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
               unsigned char bTpaProductionDiagnostic - > 0x00 if testing TPA with Opella-XD Production Test Board
               unsigned char bTpaAdaptor - > 0x00 if using TPA adaptor board
       Output: int32_t - error codes DIAG_xxx
  Description: run Opella TPA loopback diagnostic test
Date           Initials    Description
07-Aug-2007    VH          Initial
23-Aug-2007    VH          Added support for Opella-XD Production Test Board
****************************************************************************/
int32_t RunOpellaTpaLoopDiagnostic(char *pszSerialNumber, unsigned char bTpaProductionDiagnostic, unsigned char bTpaAdaptor)
{
   TyDevHandle tyHandle;
   TyError tyRes;
   TyDeviceInfo tyDeviceInfo;
   int32_t iRet = DIAG_NO_ERROR;
   uint32_t uiTPARev;


   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   if (bTpaProductionDiagnostic)
      {
      char pszTpaPin[32];
      printf("  Testing TPA diagnostic            ...");
      fflush(stdout);

      // get device information (we need to know TPA type)
      tyRes = DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN);
      if (tyRes != DRVOPXD_ERROR_NO_ERROR)
         {
         printf(" ERROR\n");
         (void)DL_OPXD_CloseDevice(tyHandle);
         return DIAG_DRIVER_ERROR;
         }
      uiTPARev = (uint32_t)(((tyDeviceInfo.ulTpaRevision) & 0xFF000000) >> 24);
      switch (tyDeviceInfo.tyTpaConnected)
         {
         case TPA_MIPS14:
            {
            tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_MIPS, FPGA_MIPS, OPXD_DISKWARE_MIPS_FILENAME, OPXD_FPGAWARE_MIPS_FILENAME);
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ, NULL, NULL);                // set JTAG frequency to 100 MHz
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagSetTMS(tyHandle, NULL, NULL, NULL, NULL);                 // default TMS sequence
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagConfigMulticore(tyHandle, 0, NULL);                       // no multicore
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               {
               if (bTpaAdaptor)
                  tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 3.3, FALSE, TRUE);                // use fixed 3.3V, enable drivers
               else
                  tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, TRUE);                 // track voltage, enable drivers
               }
            }
            break;
         case TPA_ARM20:
            {
            tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_ARM, FPGA_ARM, OPXD_DISKWARE_ARM_FILENAME, OPXD_FPGAWARE_ARM_FILENAME);
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ, NULL, NULL);                // set JTAG frequency to 100 MHz
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagSetTMS(tyHandle, NULL, NULL, NULL, NULL);                 // default TMS sequence
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagConfigMulticore(tyHandle, 0, NULL);                       // no multicore
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, TRUE);                    // track voltage, enable drivers
            }
            break;
         case TPA_ARC20:
            {
            switch (uiTPARev)
            {
               case 1:
                  // configure FPGA as diagnostic
                  if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                     tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, OPXD_FPGAWARE_ARC_TPA_R1_FILENAME);
                  else
                     tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME_LPC1837, OPXD_FPGAWARE_ARC_LPC1837_FILENAME);
                  break;
               case 0:
               default:
                  if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                     tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, OPXD_FPGAWARE_ARC_FILENAME);
                  else
                     tyRes = DIAG_DRIVER_ERROR;
            }
            
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            {
               // FPGAware which includes cJTAG
               // needs to run upto 100MHz after optimization
               if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                  tyRes = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R2, NULL, NULL); //R1 TPA not supporting above this frequency
               else
                  tyRes = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R3, NULL, NULL); //R1 TPA not supporting above this frequency
            }
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagSetTMS(tyHandle, NULL, NULL, NULL, NULL);                 // default TMS sequence
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               tyRes = DL_OPXD_JtagConfigMulticore(tyHandle, 0, NULL);                       // no multicore
            if (tyRes == DRVOPXD_ERROR_NO_ERROR)
               {
               if (bTpaAdaptor)
                  {  // we need to tell diskware to emulate ARCangel
                  tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 3.3, FALSE, TRUE);                // use fixed 3.3V, enable drivers
                  if (tyRes == DRVOPXD_ERROR_NO_ERROR)
                     tyRes = DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_ARCANGEL);
                  }
               else
                  {
                  tyRes = DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, TRUE);                 // track voltage, enable drivers
                  if (tyRes == DRVOPXD_ERROR_NO_ERROR)
                     {    
                     if (uiTPARev)
                        tyRes = DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_JTAG_TPA_R1);
                     else
                        tyRes = DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_NORMAL);
                     }
                  }
               }
            }
            break;
         case TPA_UNKNOWN:
         case TPA_NONE:
         case TPA_INVALID:
         case TPA_DIAG:
         default:
            printf(" ERROR\n");
            (void)DL_OPXD_UnconfigureDevice(tyHandle);
            (void)DL_OPXD_CloseDevice(tyHandle);
            return DIAG_UNSUPPORTED_TPA;
         }
      if (tyRes != DRVOPXD_ERROR_NO_ERROR)
         {
         printf(" ERROR\n");
         (void)DL_OPXD_SetVtpaVoltage(tyHandle, 3.3, TRUE, FALSE); 
         (void)DL_OPXD_UnconfigureDevice(tyHandle);
         (void)DL_OPXD_CloseDevice(tyHandle);
         return DIAG_DRIVER_ERROR;
         }
      // we configured Opella-XD with proper diskware and FPGAware, now we can start testing TPA or TPA adaptor boards
      iRet = RunTpaDiagnostic(tyHandle, tyDeviceInfo.tyTpaConnected, bTpaAdaptor, pszTpaPin, uiTPARev);

      switch (iRet)
         {
         case DIAG_NO_ERROR:
            printf(" OK\n");
            break;
         case DIAG_TPA_TEST_PIN:
            printf(" ERROR (%s)\n", pszTpaPin);
            break;
         default:
            printf(" ERROR\n");
            break;
         }
      // disable TPA drivers
      (void)DL_OPXD_SetVtpaVoltage(tyHandle, 3.3, TRUE, FALSE); 
      }
   else
      {
      printf("  Testing TPA loopback              ...");
      fflush(stdout);

      tyRes = DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         {
         switch (tyDeviceInfo.tyTpaConnected)
            {
            case TPA_UNKNOWN:
            case TPA_INVALID:
               printf(" ERROR\n");
               iRet = DIAG_INVALID_TPA;
               break;

            case TPA_NONE:
               printf(" N/A (TPA required)\n");
               break;

            case TPA_MIPS14:
               {
               unsigned char bTpaLoopTest = 0;

               tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_MIPS, "", OPXD_FPGAWARE_MIPS_FILENAME);
               if (tyRes == DRVOPXD_ERROR_NO_ERROR)
                  tyRes = DL_OPXD_TpaLoopDiagnosticTest(tyHandle, &bTpaLoopTest);

               if (tyRes != DRVOPXD_ERROR_NO_ERROR)
                  {
                  printf(" ERROR\n");
                  iRet = DIAG_DRIVER_ERROR;
                  }
               else
                  {
                  if (bTpaLoopTest)
                     printf(" OK\n");
                  else
                     {
                     printf(" ERROR\n");
                     iRet = DIAG_TEST_TPA_ERROR;
                     }
                  }
               }
               break;

            case TPA_ARM20:
               {
               unsigned char bTpaLoopTest = 0;

               tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_ARM, "", OPXD_FPGAWARE_ARM_FILENAME);
               if (tyRes == DRVOPXD_ERROR_NO_ERROR)
                  tyRes = DL_OPXD_TpaLoopDiagnosticTest(tyHandle, &bTpaLoopTest);

               if (tyRes != DRVOPXD_ERROR_NO_ERROR)
                  {
                  printf(" ERROR\n");
                  iRet = DIAG_DRIVER_ERROR;
                  }
               else
                  {
                  if (bTpaLoopTest)
                     printf(" OK\n");
                  else
                     {
                     printf(" ERROR\n");
                     iRet = DIAG_TEST_TPA_ERROR;
                     }
                  }
               }
               break;

            case TPA_ARC20:
               {
               unsigned char bTpaLoopTest = 0;
               uiTPARev = (uint32_t)(((tyDeviceInfo.ulTpaRevision) & 0xFF000000) >> 24);
               switch (uiTPARev)
               {
               case 1:
                  // configure FPGA as diagnostic
                  if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                     tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_ARC, "", OPXD_FPGAWARE_ARC_TPA_R1_FILENAME);
                  else
                     tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_ARC, "", OPXD_FPGAWARE_ARC_LPC1837_FILENAME);
				  // Currently, RISCV also uses ARC TPA, so try loading RISCV FPGA and diskware
				  if (tyRes == DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME || tyRes == DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME)
				  {
					  if (tyDeviceInfo.ulBoardRevision < 0x03000000)
						  tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_RISCV, "", OPXD_FPGAWARE_RISCV_TPA_R1_FILENAME);
					  else
						  tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_RISCV, "", OPXD_FPGAWARE_RISCV_LPC1837_FILENAME);
				  }
				  break;
               case 0:
               default:
                  if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                     tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_ARC, "", OPXD_FPGAWARE_ARC_FILENAME);
                  else
                     tyRes = DIAG_DRIVER_ERROR;
               }
               
               if (tyRes == DRVOPXD_ERROR_NO_ERROR)
                  tyRes = DL_OPXD_TpaLoopDiagnosticTest(tyHandle, &bTpaLoopTest);

               if (tyRes != DRVOPXD_ERROR_NO_ERROR)
                  {
                  printf(" ERROR\n");
                  iRet = DIAG_DRIVER_ERROR;
                  }
               else
                  {
                  if (bTpaLoopTest)
                     printf(" OK\n");
                  else
                     {
                     printf(" ERROR\n");
                     iRet = DIAG_TEST_TPA_ERROR;
                     }
                  }
               }
               break;

            case TPA_DIAG:
            default:
               printf(" N/A (TPA not supported)\n");
               break;
            }
         }
      else
         {
         printf(" ERROR\n");
         iRet = DIAG_DRIVER_ERROR;
         }
      }
   (void)DL_OPXD_UnconfigureDevice(tyHandle);
   (void)DL_OPXD_CloseDevice(tyHandle);

   return(iRet);
}

/****************************************************************************
     Function: RunOpellaPerformanceMeasurement
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: measure Opella-XD upload/download performance
Date           Initials    Description
07-Aug-2007    VH          Initial
****************************************************************************/
int32_t RunOpellaPerformanceMeasurement(char *pszSerialNumber)
{
   int32_t           iRet = DIAG_NO_ERROR;
   int32_t           iCnt;
   double        dPerformance;
   uint64_t ulStartTime, ulStopTime;
   TyError       tyRes;

   TyDevHandle tyHandle;
   TyDeviceInfo tyDeviceInfo;

   if (pszSerialNumber == NULL)
      return DIAG_TEST_OPELLA_ERROR;

   // find device and unconfigure it
   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
   {
      if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
         return DIAG_DRIVER_ERROR;

      // configure FPGA as diagnostic
      if (tyDeviceInfo.ulBoardRevision < 0x03000000)
         tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_FILENAME);
      else
         tyRes = DL_OPXD_ConfigureDevice(tyHandle, DW_NONE, FPGA_DIAG, "", OPXD_FPGAWARE_DIAG_LPC1837_FILENAME);
   }

   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      case DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME :
         break;
      default:
         return DIAG_DRIVER_ERROR;
      }

   printf("  Testing upload performance        ...");
   fflush(stdout);
   ulStartTime = UniGetTickCount();
   for (iCnt=0; iCnt<40; iCnt++)
      {
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_TestPerformance(tyHandle, 0);
      }
   ulStopTime = UniGetTickCount();
   dPerformance = (2560.0 / (ulStopTime - ulStartTime))*1000;
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         printf(" OK (%5.1f kB/s)\n", dPerformance);
         break;
      case DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME :
         printf(" ERROR\n");
         printf("%s\n", DIAG_INVALID_FPGAWARE_MSG);
         iRet = DIAG_TEST_OPELLA_ERROR;
         break;
      case DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME :
         printf(" ERROR\n");
         printf("%s\n", DIAG_INVALID_DISKWARE_MSG);
         iRet = DIAG_TEST_OPELLA_ERROR;
         break;
      default:
         printf(" ERROR\n");
         printf("%s\n", DIAG_TEST_DRIVER_ERROR_MSG);
         iRet = DIAG_DRIVER_ERROR;
         break;
      }

   if (iRet != DIAG_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyHandle);
      return (iRet);
      }

   printf("  Testing download performance      ...");
   fflush(stdout);
   ulStartTime = UniGetTickCount();
   for (iCnt=0; iCnt<40; iCnt++)
      {
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_TestPerformance(tyHandle, 1);
      }
   ulStopTime = UniGetTickCount();
   dPerformance = (2560.0 / (ulStopTime - ulStartTime))*1000;
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         printf(" OK (%5.1f kB/s)\n", dPerformance);
         break;
      case DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME :
         printf(" ERROR\n");
         printf("%s\n", DIAG_INVALID_FPGAWARE_MSG);
         iRet = DIAG_TEST_OPELLA_ERROR;
         break;
      case DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME :
         printf(" ERROR\n");
         printf("%s\n", DIAG_INVALID_DISKWARE_MSG);
         iRet = DIAG_TEST_OPELLA_ERROR;
         break;
      default:
         printf(" ERROR\n");
         printf("%s\n", DIAG_TEST_DRIVER_ERROR_MSG);
         iRet = DIAG_DRIVER_ERROR;
         break;
      }

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      tyRes = DL_OPXD_UnconfigureDevice(tyHandle);
   (void)DL_OPXD_CloseDevice(tyHandle);
   return(iRet);
}


/****************************************************************************
     Function: PrepareTpaSigniture
     Engineer: Vitezslav Hola
        Input: const char *pszTpaType - string with TPA type
               const char *pszTpaSerialNumber - string with TPA serial number
               const char *pszTpaRevision - string with TPA revision number
               TyTpaSigniture *ptyTpaSigniture - pointer to structure to be filled
       Output: int32_t - error codes DIAG_xxx
  Description: fill 
Date           Initials    Description
06-Mar-2007    VH          Initial
****************************************************************************/
int32_t PrepareTpaSigniture(const char *pszTpaType, const char *pszTpaSerialNumber, const char *pszTpaRevision, TyTpaParameters *ptyTpaParams)
{
   uint32_t uiPcbRev, uiElRev, uiMeRev;
   TyTpaType tyTpaType = TPA_NONE;

   assert(pszTpaType != NULL);
   assert(pszTpaSerialNumber != NULL);
   assert(pszTpaRevision != NULL);
   assert(ptyTpaParams != NULL);

   if ((sscanf(pszTpaRevision, "r%d-m%d-e%d", &uiPcbRev, &uiMeRev, &uiElRev) != 3) || 
       (uiPcbRev > 255) ||  (uiElRev > 255) || (uiMeRev > 255)
       )
      return DIAG_INVALID_PARAMETERS;

   if (strlen(pszTpaSerialNumber) > 15)
      return DIAG_INVALID_PARAMETERS;

   if (!strcmp(pszTpaType, "mips14"))
      tyTpaType = TPA_MIPS14;
   if (!strcmp(pszTpaType, "arm20"))
      tyTpaType = TPA_ARM20;
   if (!strcmp(pszTpaType, "arc20"))
      tyTpaType = TPA_ARC20;
   if (!strcmp(pszTpaType, "diag"))
      tyTpaType = TPA_DIAG;

   if (tyTpaType == TPA_NONE)
      return DIAG_INVALID_PARAMETERS;              // invalid TPA type

   // fill structure for TPA signiture
   memset((void *)ptyTpaParams, 0, sizeof(TyTpaParameters));

   ptyTpaParams->tyTpaType = tyTpaType;
   strcpy(ptyTpaParams->pucTpaSerialNumber, pszTpaSerialNumber);
   ptyTpaParams->ulManufacturingDate = GetCurrentDate();
   ptyTpaParams->ulTpaRevision  = (uiPcbRev & 0xFF) << 24;
   ptyTpaParams->ulTpaRevision += (uiElRev & 0xFF) << 16;
   ptyTpaParams->ulTpaRevision += (uiMeRev & 0xFF) << 8;

   return DIAG_NO_ERROR;
}

/****************************************************************************
     Function: PrepareForJtagConsole
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyHandle - handle to open device
       Output: int32_t - error codes DIAG_xxx
  Description: prepare FPGA and set basic configuration before running JTAG console
Date           Initials    Description
25-Jan-2007    VH          Initial
****************************************************************************/
int32_t PrepareForJtagConsole(TyDevHandle tyHandle)
{
   TyDeviceInfo tyDeviceInfo;
   int32_t iRet = DIAG_NO_ERROR; 
   uint32_t uiTPARev;
   #ifdef ARM_CMDS 
   #ifdef ARM_CHECK_MULTI_CORE
   uint32_t pulBypassIRPattern[16];
   TyTAPConfig ptyCoreConfig[16];
   uint32_t ulNoOfCores = 0;
   uint32_t pulIRLengthArray[16] = {0};
   uint32_t ulI;
   #endif //ARM_CHECK_MULTI_CORE
   #endif //ARM_CMDS

   tyDeviceInfo.tyTpaConnected = TPA_NONE;

   if (DL_OPXD_UnconfigureDevice(tyHandle)!=DRVOPXD_ERROR_NO_ERROR)
      iRet = DIAG_DRIVER_ERROR;

   //TODO: Hardcoding DW_UNKNOWN as diskware type. Since DL_OPXD_GetDeviceInfo is called to get device info alone, it might be ok, still confirm that
   if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN)!=DRVOPXD_ERROR_NO_ERROR))
      iRet = DIAG_DRIVER_ERROR;

   if (iRet != DIAG_NO_ERROR)
      return iRet;

   // try to configure FPGA with proper FPGAware and diskware
   switch (tyDeviceInfo.tyTpaConnected)
      {
      case TPA_MIPS14:
         iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_MIPS, FPGA_MIPS, OPXD_DISKWARE_MIPS_FILENAME, OPXD_FPGAWARE_MIPS_FILENAME);
         break;
      case TPA_ARM20:
         // uncomment following option if you want to use test commands for ThreadArch target instead of ARM
         //iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_TARCH, FPGA_ARM, OPXD_DISKWARE_TARCH_FILENAME, OPXD_FPGAWARE_ARM_FILENAME);
         iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_ARM, FPGA_ARM, OPXD_DISKWARE_ARM_FILENAME, OPXD_FPGAWARE_ARM_FILENAME);
         break;
      case TPA_ARC20:
         uiTPARev = (uint32_t)(((tyDeviceInfo.ulTpaRevision) & 0xFF000000) >> 24);
         switch (uiTPARev)
         {
         case 1:
            if (tyDeviceInfo.ulBoardRevision < 0x03000000)
               iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, OPXD_FPGAWARE_ARC_TPA_R1_FILENAME);
            else
               iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME_LPC1837, OPXD_FPGAWARE_ARC_LPC1837_FILENAME);
			// Currently, RISCV also uses ARC TPA, so try loading RISCV FPGA and diskware
			if (iRet == DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME || iRet == DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME)
			{
				if (tyDeviceInfo.ulBoardRevision < 0x03000000)
					iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_FILENAME, OPXD_FPGAWARE_RISCV_TPA_R1_FILENAME);
				else
					iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_LPC1837_FILENAME, OPXD_FPGAWARE_RISCV_LPC1837_FILENAME);
			}
            break;
         case 0:
         default:
            if (tyDeviceInfo.ulBoardRevision < 0x03000000)
               iRet = DL_OPXD_ConfigureDevice(tyHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, OPXD_FPGAWARE_ARC_FILENAME);
            else
               iRet = DIAG_DRIVER_ERROR;
         }
         
         // set target type (uncomment following lines when using as test for ARC)
         // proper target must be specified
         if (tyDiagOptions.pszTargetDevice != NULL)
         {
            if (!strcmp(tyDiagOptions.pszTargetDevice,"arcangel"))
            {
               if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_ARCANGEL) != DRVOPXD_ERROR_NO_ERROR))
                  iRet = DIAG_DRIVER_ERROR;
            }
            else if (!strcmp(tyDiagOptions.pszTargetDevice,"siano"))
            {
               if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_RSTDETECT) != DRVOPXD_ERROR_NO_ERROR))
                  iRet = DIAG_DRIVER_ERROR;
            }
            else if (!strcmp(tyDiagOptions.pszTargetDevice,"arc-cjtag-tpa-r1"))
            {
               if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_CJTAG_TPA_R1) != DRVOPXD_ERROR_NO_ERROR))
                  iRet = DIAG_DRIVER_ERROR;
            }
            else if (!strcmp(tyDiagOptions.pszTargetDevice,"arc-jtag-tpa-r1"))
            {
               if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_JTAG_TPA_R1) != DRVOPXD_ERROR_NO_ERROR))
                  iRet = DIAG_DRIVER_ERROR;
            }
            else if (!strcmp(tyDiagOptions.pszTargetDevice,"arc"))
            {
               if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_NORMAL) != DRVOPXD_ERROR_NO_ERROR))
                  iRet = DIAG_DRIVER_ERROR;
            }
			else if (!strcmp(tyDiagOptions.pszTargetDevice, "riscv"))
			{
				if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_RISCV_SelectTarget(tyHandle, RISCV_JTAG_TPA_R1) != DRVOPXD_ERROR_NO_ERROR))
					iRet = DIAG_DRIVER_ERROR;
			}
            else
            {
               iRet = DIAG_INVALID_TARGET_DEVICE;
            }
         }
         else
         {
            if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_NORMAL) != DRVOPXD_ERROR_NO_ERROR))
                  iRet = DIAG_DRIVER_ERROR;
         }
         break;
         
      case TPA_UNKNOWN:
      case TPA_NONE:
      case TPA_INVALID:
      case TPA_DIAG:
      default:
         iRet = DIAG_NO_TPA_FOR_JTAG_CONSOLE;
         break;
      }
   #ifdef ARM_CMDS 
      #ifdef ADAPTIVE_CLOCK
         // set Adaptive clocking
         printf("Adaptive clock\n");
         if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagSetAdaptiveClock(tyHandle, 1, 1.0) != DRVOPXD_ERROR_NO_ERROR))
            iRet = DIAG_DRIVER_ERROR;
      #else
         // set default frequency (to 1MHz)
         printf("Normal clock\n");
         if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagSetFrequency(tyHandle, 1.0, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
            iRet = DIAG_DRIVER_ERROR;
      #endif //ADAPTIVE_CLOCK
   #else
      if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagSetFrequency(tyHandle, 1.0, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
         iRet = DIAG_DRIVER_ERROR;
   #endif //ARM_CMDS

   // set TMS sequence (just use default sequences)
   if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagSetTMS(tyHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
      iRet = DIAG_DRIVER_ERROR;

   // set no MC
   if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagConfigMulticore(tyHandle, 0, NULL) != DRVOPXD_ERROR_NO_ERROR))
      iRet = DIAG_DRIVER_ERROR;

   // set tracking voltage and enable drivers to target
   if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, TRUE) != DRVOPXD_ERROR_NO_ERROR))
      iRet = DIAG_DRIVER_ERROR;
   
   // set MC
   #ifdef ARM_CMDS 
   #ifdef ARM_CHECK_MULTI_CORE
   MsSleep(500);
      if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_Arm_GetMultiCoreInfo(tyHandle,&ulNoOfCores, pulIRLengthArray) != DRVOPXD_ERROR_NO_ERROR))
         iRet = DIAG_DRIVER_ERROR;
      
      printf("Number of Cores in the Scan Chain: %d\n\n", (int32_t) ulNoOfCores);
      if (ulNoOfCores < 2)
         {
         if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagConfigMulticore(tyHandle, 0, NULL) != DRVOPXD_ERROR_NO_ERROR))
            iRet = DIAG_DRIVER_ERROR;
         }
      else
         {
         for (ulI = 0; ulI < ulNoOfCores; ulI++)
            {
               pulBypassIRPattern[ulI] = 0xffffffff;
               ptyCoreConfig[ulI].pulBypassIRPattern = &pulBypassIRPattern[ulI];
               ptyCoreConfig[ulI].usDefaultDRLength = 1;
               ptyCoreConfig[ulI].usDefaultIRLength = (unsigned short) pulIRLengthArray[ulI];
            }
         if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_JtagConfigMulticore(tyHandle, ulNoOfCores, ptyCoreConfig) != DRVOPXD_ERROR_NO_ERROR))
            iRet = DIAG_DRIVER_ERROR;
         }
   #endif //ARM_CHECK_MULTI_CORE
   #endif //ARM_CMDS
   MsSleep(500);
   if (tyDiagOptions.pszTargetDevice != NULL)
   {
      if (!strcmp(tyDiagOptions.pszTargetDevice,"arc-cjtag-tpa-r1"))
         {
            iRet = DL_OPXD_cJtagInitTap7Controller(tyHandle);
         }
      if (!strcmp(tyDiagOptions.pszTargetDevice,"arc-jtag-tpa-r1"))
      {
         //tyRes = DL_OPXD_cJtagInitTap7Controller(tyLocHandle);
         //tyRes = DL_OPXD_JtagResetTAP (tyLocHandle,FALSE);
         iRet = DL_OPXD_cJtagChangeScanFormat (tyHandle,0,0,0);
         iRet = DL_OPXD_JtagResetTAP (tyHandle,FALSE);
      }
   }
   return iRet;
}

/****************************************************************************
     Function: UpdateFirmware
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: check and update firmware for Opella-XD
Date           Initials    Description
25-Jan-2007    VH          Initial
****************************************************************************/
int32_t UpdateFirmware(char *pszSerialNumber)
{
   char          pszVersion[64];
   char          pszVersion2[64];
   char          pszFileName[100];
   char          pszLocFlashUtilName[100];
   int32_t           iRet = DIAG_NO_ERROR;
   int32_t           iFwDiff;
   unsigned char bReconnect, bValidChar, bUpgradeConfirmed;
   uint32_t ulFileFw, ulDeviceFw;

   TyDevHandle  tyHandle;
   TyDeviceInfo tyDeviceInfo;

   assert(pszSerialNumber != NULL);

   switch (DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle))
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   (void)DL_OPXD_UnconfigureDevice(tyHandle);

   if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DRVOPXD_ERROR_USB_DRIVER;

   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
      strcpy(pszFileName, OPXD_FIRMWARE_FILENAME);
   else
   {
	   strcpy(pszFileName, OPXD_FIRMWARE_FILENAME_LPC1837);
	   strcpy(pszLocFlashUtilName, OPXD_FLASHUTIL_FILENAME_LPC1837);
   }

   // check current version
   if (DL_OPXD_UpgradeDevice(tyHandle, UPG_CHECK_ONLY, pszFileName, pszLocFlashUtilName, &bReconnect, &iFwDiff, &ulFileFw, &ulDeviceFw, pszSerialNumber, NULL) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyHandle);
      return DIAG_DRIVER_ERROR;
      }

   if (iFwDiff == 0)
      {
      ConvertVersionString(ulDeviceFw, pszVersion);
      printf("Opella-XD %s has the latest firmware version %s, no update necessary.\n", pszSerialNumber, pszVersion);
      return iRet;
      }

   // otherwise ask user what to do
   ConvertVersionString(ulDeviceFw, pszVersion);
   ConvertVersionString(ulFileFw, pszVersion2);
   printf("Opella-XD %s has firmware version %s.\n", pszSerialNumber, pszVersion);
   printf("Do you want to replace with %s firmware? (Yes/No) ", pszVersion2);

   bValidChar = false;
   bUpgradeConfirmed = false;
   while (!bValidChar)
      {
      int32_t iChar = getchar();
      switch(iChar)
         {
         case 'Y' :
         case 'y' :
             bUpgradeConfirmed = true;
             bValidChar = true;
             break;
         case 'N' :
         case 'n' :
             bUpgradeConfirmed = false;
             bValidChar = true;
             break;
         default:
            break;
         }
      }

   if (!bUpgradeConfirmed)
      {
      printf("\nOpella-XD firmware has not been changed.\n");
      (void)DL_OPXD_CloseDevice(tyHandle);
      return iRet;
      }
   else
      {
      printf("\nUpgrading Opella-XD firmware .");
      fflush(stdout);
      }

   // check current version
   if (DL_OPXD_UpgradeDevice(tyHandle, UPG_DIFF_VERSION, pszFileName, pszLocFlashUtilName, &bReconnect, &iFwDiff, NULL, NULL, pszSerialNumber, NULL) != DRVOPXD_ERROR_NO_ERROR)
      {
      printf("........ ERROR\n");
      (void)DL_OPXD_CloseDevice(tyHandle);
      return DIAG_UPDATE_FAILED;
      }

   if (bReconnect)
      {
      int32_t iRestartCnt;

      (void)DL_OPXD_CloseDevice(tyHandle);
      for (iRestartCnt=0; iRestartCnt<8; iRestartCnt++)
         {
         printf(".");
         fflush(stdout);
         MsSleep(1000);
         }
      printf(" ");
	  if ((DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle) == DRVOPXD_ERROR_NO_ERROR) && 
          (DL_OPXD_UpgradeDevice(tyHandle, UPG_CHECK_ONLY, pszFileName, pszLocFlashUtilName, &bReconnect, &iFwDiff, NULL, NULL, pszSerialNumber, NULL) == DRVOPXD_ERROR_NO_ERROR) &&
          (iFwDiff == 0))
         {
         printf("DONE\n");
         }
      else
         {
         printf("ERROR\n");
         iRet = DIAG_DRIVER_ERROR;
         }
      }
   else
      {
      printf("........ DONE\n");
      }
   (void)DL_OPXD_CloseDevice(tyHandle);
   return iRet;
}

/****************************************************************************
     Function: CheckForFirmware
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
       Output: int32_t - error codes DIAG_xxx
  Description: check if firmware version is compatible with current software
Date           Initials    Description
10-Aug-2007    VH          Initial
****************************************************************************/
int32_t CheckForFirmware(char *pszSerialNumber)
{
   int32_t           iFwDiff;
   unsigned char bReconnect;
   char          pszVersion[64], pszVersion2[64];
   uint32_t ulFileFw, ulDeviceFw;

   TyDevHandle  tyHandle;
   TyDeviceInfo tyDeviceInfo;

   assert(pszSerialNumber != NULL);

   switch (DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle))
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   (void)DL_OPXD_UnconfigureDevice(tyHandle);

   if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DRVOPXD_ERROR_USB_DRIVER;

   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
   {
      // check current version
      if (DL_OPXD_UpgradeDevice(tyHandle, UPG_CHECK_ONLY, OPXD_FIRMWARE_FILENAME, OPXD_FLASHUTIL_FILENAME_LPC1837, &bReconnect, &iFwDiff, &ulFileFw, &ulDeviceFw, pszSerialNumber, NULL) != DRVOPXD_ERROR_NO_ERROR)
         {
         (void)DL_OPXD_CloseDevice(tyHandle);
         return DIAG_DRIVER_ERROR;
         }
   }
   else
   {
      // check current version
      if (DL_OPXD_UpgradeDevice(tyHandle, UPG_CHECK_ONLY, OPXD_FIRMWARE_FILENAME_LPC1837, OPXD_FLASHUTIL_FILENAME_LPC1837, &bReconnect, &iFwDiff, &ulFileFw, &ulDeviceFw, pszSerialNumber, NULL) != DRVOPXD_ERROR_NO_ERROR)
         {
         (void)DL_OPXD_CloseDevice(tyHandle);
         return DIAG_DRIVER_ERROR;
         }
   }

   if (iFwDiff < 0)
      {
      // upgrade required
      ConvertVersionString(ulDeviceFw, pszVersion);
      ConvertVersionString(ulFileFw, pszVersion2);
      printf("Opella-XD requires firmware update to latest version %s.\n", pszVersion2);
      return DIAG_UPDATE_REQUIRED;
      }

   (void)DL_OPXD_CloseDevice(tyHandle);
   return DIAG_NO_ERROR;
}

/****************************************************************************
     Function: PrintDiagnosticErrorMessage
     Engineer: Vitezslav Hola
        Input: char *pszErrorMessage - pointer to string for error message
               unsigend int32_t uiSize - number of bytes in string for error message
               int32_t iErrorCode - error code
       Output: none
  Description: print error message corresponding particular diagnostic error
               into provided buffer
Date           Initials    Description
25-Jan-2007    VH          Initial
****************************************************************************/
void PrintDiagnosticErrorMessage(char *pszMessage, uint32_t uiSize, int32_t iErrorCode)
{
   if ((pszMessage == NULL) || (uiSize < 1))
      return;

   strcpy(pszMessage, "");

   if (iErrorCode == DRVOPXD_ERROR_NO_ERROR)
      return;

   switch (iErrorCode)
      {
      // CPU diagnostic error
      case DRVOPXD_ERROR_TEST_CPU:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_CPU_ERROR_MSG);  
         break;
      // FPGA diagnostic error
      case DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_CONFIG_ERROR_MSG);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_ID:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_INVALID_ID);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_XSYSCLK:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_XSYSCLK);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_XBS(0):
      case DRVOPXD_ERROR_TEST_FPGA_XBS(1):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_XBS, (iErrorCode - DRVOPXD_ERROR_TEST_FPGA_XBS(0)));  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(0):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(1):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(2):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(3):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(4):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(5):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(6):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(7):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(8):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(9):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(10):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(11):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(12):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(13):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(14):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(15):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(16):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(17):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(18):
      case DRVOPXD_ERROR_TEST_FPGA_ADDRESS(19):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_ADDRESS, (iErrorCode - DRVOPXD_ERROR_TEST_FPGA_ADDRESS(0)));  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_DATA(0):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(1):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(2):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(3):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(4):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(5):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(6):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(7):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(8):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(9):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(10):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(11):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(12):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(13):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(14):
      case DRVOPXD_ERROR_TEST_FPGA_DATA(15):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_DATA, (iErrorCode - DRVOPXD_ERROR_TEST_FPGA_DATA(0)));  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_INIT:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_FPGA_INIT);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_IRQ:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_FPGA_IRQ);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_DMADREQ:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_DMADREQ);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_DMADREQCLR:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_DMADREQCLR);  
         break;
      case DRVOPXD_ERROR_TEST_FPGA_DMATCOUT:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_FPGA_ERROR_DMATCOUT);  
         break;
      // external RAM diagnostic
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(0):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(1):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(2):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(3):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(4):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(5):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(6):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(7):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(8):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(9):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(10):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(11):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(12):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(13):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(14):
      case DRVOPXD_ERROR_TEST_EXTRAM_DATA(15):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_RAM_ERROR_DATA, (iErrorCode - DRVOPXD_ERROR_TEST_EXTRAM_DATA(0)));  
         break;
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(0):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(1):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(2):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(3):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(4):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(5):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(6):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(7):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(8):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(9):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(10):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(11):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(12):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(13):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(14):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(15):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(16):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(17):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(18):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(19):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(20):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(21):
      case DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(22):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_RAM_ERROR_ADDRESS, (iErrorCode - DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(0)));  
         break;
      case DRVOPXD_ERROR_TEST_EXTRAM_RANDOM:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_RAM_ERROR_RANDOM);  
         break;
      // TPA interface diagnostics
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(0):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(1):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(2):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(3):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(4):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(5):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(6):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(7):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(8):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_P(9):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_DIO_P, (iErrorCode - DRVOPXD_ERROR_TEST_TPAINT_DIO_P(0)));  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(0):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(1):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(2):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(3):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(4):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(5):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(6):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(7):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(8):
      case DRVOPXD_ERROR_TEST_TPAINT_DIO_N(9):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_DIO_N, (iErrorCode - DRVOPXD_ERROR_TEST_TPAINT_DIO_N(0)));  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(0):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(1):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(2):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(3):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(4):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(5):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(6):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(7):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(8):
      case DRVOPXD_ERROR_TEST_TPAINT_FSIO(9):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_FSIO, (iErrorCode - DRVOPXD_ERROR_TEST_TPAINT_FSIO(0)));  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_CSIO(0):
      case DRVOPXD_ERROR_TEST_TPAINT_CSIO(1):
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_CSIO, (iErrorCode - DRVOPXD_ERROR_TEST_TPAINT_CSIO(0)));  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_LOOP:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_LOOP);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_ABSENT:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_ABSENT);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_VTPA:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_VTPA);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_VTPA_2:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_VTPA_2);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_VTPA_3V3:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_VTPA_3V3);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_VTPA_2V5:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_VTPA_2V5);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_LED:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_LED);  
         break;
      case DRVOPXD_ERROR_TEST_TPAINT_VTREF:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_TPA_ERROR_VTREF);  
         break;

      case DRVOPXD_ERROR_TEST_PLL_REFERENCE:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_PLL_INVALID_REFERENCE);  
         break;
      case DRVOPXD_ERROR_TEST_PLL_JTAGCLK1:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_PLL_ERROR_JTAGCLK, 1);  
         break;
      case DRVOPXD_ERROR_TEST_PLL_JTAGCLK2:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_PLL_ERROR_JTAGCLK, 2);  
         break;

      case DRVOPXD_ERROR_DEVICE_I2C_ERROR:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_I2C_ERROR);  
         break;
      default:
         (void)_snprintf(pszMessage, uiSize, DIAG_TEST_DRIVER_ERROR_MSG);  
         break;
      }
}

/****************************************************************************
     Function: RunTpaDiagnostic
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyHandle - handle to device
               TyTpaType tyTpaType - type of TPA (MIPS14, etc.)
               unsigned char bAdaptorBoard - > 0x00 if using adaptor board with TPA
               char *pszPinName - string with failing pin name
       Output: int32_t - error code
  Description: run Opella-XD TPA diagnostic
Date           Initials    Description
20-Aug-2007    VH          Initial
01-Jun-2012    SPT         Updated to test TPA R1
****************************************************************************/
static int32_t RunTpaDiagnostic(TyDevHandle tyHandle, TyTpaType tyTpaType, unsigned char bAdaptorBoard, char *pszPinName,
                            uint32_t uiTPARev)
{
   uint32_t ulIDValue;
   uint32_t pulIR[1], pulDRIn[1], pulDROut[1];
   uint32_t ulTestPat0 = 0x55555555;
   uint32_t ulTestPat1 = 0xAAAAAAAA;
   uint32_t ulTestPat2 = 0x12345678;
   uint32_t ulTestPat3 = 0xFEDCBA98;
   int32_t iRet = DIAG_NO_ERROR;

   TyDeviceInfo tyDeviceInfo;

   if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DRVOPXD_ERROR_USB_DRIVER;

   if (pszPinName == NULL)
      return iRet;
   strcpy(pszPinName, "");

   // wait to be sure TPA voltage is stabilized
   MsSleep(1000);
   // first reset TAP
   (void)DL_OPXD_JtagResetTAP(tyHandle, false);

   pulIR[0] = 0xC;                  // user register
   pulDRIn[0] = 0x18000000;         // register value (display '1' on LED) Setting to JTAG mode
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
   MsSleep(500);
   (void)DL_OPXD_JtagResetTAP(tyHandle, true); //on nTRST assertion the mode change take effect
   (void) DL_OPXD_cJtagSetFPGAtocJTAGMode(tyHandle, 0); //changing from cjtag to JTAG mode
   MsSleep(100);

   // step 1 - reading ID register
   pulIR[0] = 0xC;                  // user register
   pulDRIn[0] = 0x18000000;         // register value (display '1' on LED)
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
   // read ID register
   pulIR[0] = 0x1;                  // ID register
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, NULL, pulDROut);
   ulIDValue = pulDROut[0];
   switch (tyTpaType)
      {
      case TPA_MIPS14:
         if (ulIDValue != 0x1D0001F5)
            iRet = DIAG_TPA_TEST_ID_ERROR;
         break;
      case TPA_ARM20:
      case TPA_ARC20:
         if (ulIDValue != 0x1D230A48)
            iRet = DIAG_TPA_TEST_ID_ERROR;
         break;
      case TPA_UNKNOWN:
      case TPA_NONE:
      case TPA_INVALID:
      case TPA_DIAG:
      default:
         iRet = DIAG_TPA_TEST_ID_ERROR;
         break;
      }
   if ((iRet == DIAG_TPA_TEST_ID_ERROR))
         {
         strcpy(pszPinName, "JTAG ID read");
         iRet = DIAG_TPA_TEST_PIN;                   
         }
   MsSleep(1000);

   // step 2 - test scanchain via bypasss register and several test patterns
   pulIR[0] = 0xC;                  // user register
   pulDRIn[0] = 0x28000000;         // register value (display '2' on LED)
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
   pulIR[0] = 0xF;                  // user register
   pulDRIn[0] = ulTestPat0;
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
   if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
      iRet = DIAG_TPA_TEST_SCANCHAIN_ERROR;
   pulIR[0] = 0xF;                  // user register
   pulDRIn[0] = ulTestPat1;
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
   if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
      iRet = DIAG_TPA_TEST_SCANCHAIN_ERROR;
   pulIR[0] = 0xF;                  // user register
   pulDRIn[0] = ulTestPat2;
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
   if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
      iRet = DIAG_TPA_TEST_SCANCHAIN_ERROR;
   pulIR[0] = 0xF;                  // user register
   pulDRIn[0] = ulTestPat3;
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
   if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
      iRet = DIAG_TPA_TEST_SCANCHAIN_ERROR;
   MsSleep(1000);
   // so far, we have tested TDI, TDO, TCK, TMS pins
   
   //cJTAG mode test
   if (uiTPARev == 1)
      {
      pulIR[0] = 0xC;                  // user register
      pulDRIn[0] = 0x28040000;         // register value (display '2' on LED) Setting to cJTAG mode
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
      MsSleep(500);
      (void)DL_OPXD_JtagResetTAP(tyHandle, true); //on TRST assertion the mode change take effect

      //(void)DL_OPXD_cJtagEscape(tyHandle,0); // give escape reset to target and enters cJTAG mode
      //MsSleep(1500);

      (void) DL_OPXD_cJtagSetFPGAtocJTAGMode(tyHandle, 1); //changing from jtag to cJTAG mode
      MsSleep(100);

      pulIR[0] = 0xC;                  // user register
      pulDRIn[0] = 0xC8040000;         // register value (display 'C' on LED)
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
      MsSleep(1000);

      pulIR[0] = 0xF;                  // user register
      pulDRIn[0] = ulTestPat0;
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
      if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
         {
         strcpy(pszPinName, "cJTAG mode");
         iRet = DIAG_TPA_TEST_PIN;;                   
         }

      pulIR[0] = 0xF;                  // user register
      pulDRIn[0] = ulTestPat1;
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
      if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
         {
         strcpy(pszPinName, "cJTAG mode");
         iRet = DIAG_TPA_TEST_PIN;;                   
         }

      pulIR[0] = 0xF;                  // user register
      pulDRIn[0] = ulTestPat2;
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
      if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
         {
         strcpy(pszPinName, "cJTAG mode");
         iRet = DIAG_TPA_TEST_PIN;;                   
         }

      pulIR[0] = 0xF;                  // user register
      pulDRIn[0] = ulTestPat3;
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
      if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] >> 1) != (pulDRIn[0] & 0x7FFFFFFF)))
         {
         strcpy(pszPinName, "cJTAG mode");
         iRet = DIAG_TPA_TEST_PIN;;                   
         }

      // read ID register
      pulIR[0] = 0x1;                  // ID register
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, NULL, pulDROut);
      ulIDValue = pulDROut[0];
      switch (tyTpaType)
         {
         case TPA_MIPS14:
            if (ulIDValue != 0x1D0001F5)
               {
               strcpy(pszPinName, "cJTAG mode ID CODE");
               iRet = DIAG_TPA_TEST_PIN; 
               }
            break;
         case TPA_ARM20:
         case TPA_ARC20:
            if ((iRet == DIAG_NO_ERROR) && ulIDValue != 0x1D230A48)
               {
               strcpy(pszPinName, "cJTAG mode ID CODE");
               iRet = DIAG_TPA_TEST_PIN; 
               }
            break;
         case TPA_UNKNOWN:
         case TPA_NONE:
         case TPA_INVALID:
         case TPA_DIAG:
         default:
            {
            strcpy(pszPinName, "cJTAG mode ID CODE");
            iRet = DIAG_TPA_TEST_PIN; 
            }
            break;
         }
      pulIR[0] = 0xC;
      pulDRIn[0] = 0x38000000;         // register value (display '3' on LED) setting to JTAG mode
      (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
      MsSleep(500);
      (void)DL_OPXD_JtagResetTAP(tyHandle, true); // On TRST assertion Mode change will take effect
      (void) DL_OPXD_cJtagSetFPGAtocJTAGMode(tyHandle, 0); //changing to JTAG mode
      MsSleep(100);
      }


   // step 3 - test additional pins from TPA
   //strcpy(pszPinName, "");
   pulIR[0] = 0xC;                  // user register
   pulDRIn[0] = 0x38000000;         // register value (display '3' on LED)
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
   if (iRet == DIAG_NO_ERROR)
      {
      switch (tyTpaType)
         {
         case TPA_MIPS14:
            {  // testing TPAOP-MIPS14 or ADOP-EJTAG20
            uint32_t ulDefaultMask;

            if (bAdaptorBoard)
               ulDefaultMask = 0x00000000;
            else
               ulDefaultMask = 0x00000010;

            // clear any pending flag
            pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
            // test nRST pin
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
            if ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask))
               {
               strcpy(pszPinName, "nRST");
               iRet = DIAG_TPA_TEST_PIN; 
               }
            (void)DL_OPXD_Mips_ResetProc(tyHandle, 0, 1, NULL);            // assert nRST
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
            if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000003)))
               {
               strcpy(pszPinName, "nRST");
               iRet = DIAG_TPA_TEST_PIN; 
               }
            (void)DL_OPXD_Mips_ResetProc(tyHandle, 0, 0, NULL);            // deassert nRST
            // clear any pending flag
            pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
            // test nTRST pin
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
            if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask)))
               {
               strcpy(pszPinName, "nTRST");
               iRet = DIAG_TPA_TEST_PIN; 
               }
            (void)DL_OPXD_JtagResetTAP(tyHandle, true);
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
            if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000008)))
               {
               strcpy(pszPinName, "nTRST");
               iRet = DIAG_TPA_TEST_PIN; 
               }
            // clear any pending flag
            pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
            pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
            (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);

            if (bAdaptorBoard)
               {  // we are testing in fact ADOP-EJTAG20 adaptor board together with TPA
               // we are unable to control PCSTx and DCLK pins from probe so we just check if they are still 0
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((pulDROut[0] & 0x0000FFFF) != 0x00000000)
                  strcpy(pszPinName, "PCSTx");
               }
            else
               {  // testing just TPAOP-MIPS14
               (void)DL_OPXD_Mips_DebugInterrupt(tyHandle, 0, 0);                // clear DINT
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((pulDROut[0] & 0x0000FFFF) != 0x00000000)
                  strcpy(pszPinName, "DINT");
               (void)DL_OPXD_Mips_DebugInterrupt(tyHandle, 0, 1);                // set DINT
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((pulDROut[0] & 0x0000FFFF) != 0x00000010)
                  strcpy(pszPinName, "DINT");
               // now we have tested all pins except RTCK
               // we need also test RTCK (set adaptive clock and try to read ID)
               MsSleep(1000);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x48000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               (void)DL_OPXD_JtagSetAdaptiveClock(tyHandle, 100, 100.0);                  // set adaptive clock
               pulIR[0] = 0x1;            pulDROut[0] = 0;                                // ID register
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, NULL, pulDROut);  // try to read ID register when adaptive clock is selected
               ulIDValue = pulDROut[0];
               if ((iRet == DIAG_NO_ERROR) && (ulIDValue != 0x1D0001F5))                  // if RTCK does not work, we should not be able to read ID
                  {
                  strcpy(pszPinName, "RTCK");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }

               if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                  iRet = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R2, NULL, NULL); //R1 TPA not supporting above this frequency
               else
                  iRet = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R3, NULL, NULL); //R1 TPA not supporting above this frequency
               }
            }
            break;

         case TPA_ARM20:
            {  // testing TPAOP-ARM20 or ADOP-RPINE10
            if (!bAdaptorBoard)
               {  // testing just TPAOP-ARM20
               unsigned char bLocDbgAck;
               uint32_t ulDefaultMask = 0x000000C0;
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               // test nRST pin
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask))
                  {
                  strcpy(pszPinName, "nSRST");     iRet = DIAG_TPA_TEST_PIN; 
                  }
               (void)DL_OPXD_Arm_ResetProc(tyHandle, 0, 1, NULL, NULL);                      // assert nSRST
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000003)))
                  {
                  strcpy(pszPinName, "nSRST");     iRet = DIAG_TPA_TEST_PIN; 
                  }
               (void)DL_OPXD_Arm_ResetProc(tyHandle, 0, 0, NULL, NULL);                      // assert nSRST
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               // test nTRST pin
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask)))
                  {
                  strcpy(pszPinName, "nTRST");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }
               (void)DL_OPXD_JtagResetTAP(tyHandle, true);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000008)))
                  {
                  strcpy(pszPinName, "nTRST");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               // so far, we have tested TDI, TDO, TCK, TMS, nTRST and nRST
               // we need also test RTCK (set adaptive clock and try to read ID)
               MsSleep(1000);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x48000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               (void)DL_OPXD_JtagSetAdaptiveClock(tyHandle, 100, 100.0);                  // set adaptive clock
               pulIR[0] = 0x1;            pulDROut[0] = 0;                                // ID register
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, NULL, pulDROut);  // try to read ID register when adaptive clock is selected
               ulIDValue = pulDROut[0];
               if ((iRet == DIAG_NO_ERROR) && (ulIDValue != 0x1D230A48))                  // if RTCK does not work, we should not be able to read ID
                  {
                  strcpy(pszPinName, "RTCK");      iRet = DIAG_TPA_TEST_PIN; 
                  }

               // Changed this to 70MHz to run non optimized
               // FPGAware which includes cJTAG
               // needs to run upto 100MHz after optimization
               if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                  iRet = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R2, NULL, NULL);
               else
                  iRet = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R3, NULL, NULL);

               // we have tested RTCK, so we need to test DBGRQ and DBGACK pins
               // clear any pending flag
               MsSleep(1000);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x5800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x58000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               (void)DL_OPXD_Arm_DebugRequest(tyHandle, 0, 1);                            // assert DBGRQ
               pulIR[0] = 0xC;            pulDRIn[0] = 0x58000000;      pulDROut[0] = 0;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               bLocDbgAck = 0;
               (void)DL_OPXD_Arm_DebugAcknowledge(tyHandle, &bLocDbgAck);                 // get DBGACK
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000010)) || (bLocDbgAck > 0)))
                  {
                  strcpy(pszPinName, "DBGRQ");           iRet = DIAG_TPA_TEST_PIN; 
                  }
               // test DBGACK
               pulIR[0] = 0xC;            pulDRIn[0] = 0x5800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x58000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               (void)DL_OPXD_Arm_DebugRequest(tyHandle, 0, 0);                            // deassert DBGRQ
               pulIR[0] = 0xC;            pulDRIn[0] = 0x58010000;      pulDROut[0] = 0;  // set DBGACK
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               bLocDbgAck = 0;
               (void)DL_OPXD_Arm_DebugAcknowledge(tyHandle, &bLocDbgAck);                 // get DBGACK
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask)) || (bLocDbgAck == 0)))
                  {
                  strcpy(pszPinName, "DBGACK");           iRet = DIAG_TPA_TEST_PIN; 
                  }
               // clear any pending flag
               MsSleep(1000);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x6800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x68000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               }
            else
               {  // testing ADOP-RPINE10 adaptor board
               // nothing to test, we have already tested all pins TDI, TCK, TMS and TDO
               }
            }
            break;
         case TPA_ARC20:
            {  // testing TPAOP-ARC20 or ADOP-ARC15
            if (bAdaptorBoard)
               {  // we are testing in fact ADOP-ARC15 adaptor board together with TPAOP-ARC20
               // let test only SS0, SS1, CNT, OP, D0 and D1 pins
               TyARCangelMode tyArcPins, tyArcOut;
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               MsSleep(100);
               // set SS0 pin and test it
               tyArcPins.bSS0 = 1;     tyArcPins.bSS1 = 0;     tyArcPins.bCNT = 0;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, NULL, 0);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_AAMode(tyHandle, NULL, &tyArcOut, 0);
               if (((pulDROut[0] & 0x0000FFFF) != 0x00000040) || (tyArcOut.bOP != 0))
                  {
                  strcpy(pszPinName, "SS0");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               // set SS1 pin and test it
               tyArcPins.bSS0 = 0;     tyArcPins.bSS1 = 1;     tyArcPins.bCNT = 0;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, NULL, 0);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_AAMode(tyHandle, NULL, &tyArcOut, 0);
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x00000080) || (tyArcOut.bOP != 0)))
                  {
                  strcpy(pszPinName, "SS1");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               // set CNT pin and test it
               tyArcPins.bSS0 = 0;     tyArcPins.bSS1 = 0;     tyArcPins.bCNT = 1;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, NULL, 0);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_AAMode(tyHandle, NULL, &tyArcOut, 0);
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x00000100) || (tyArcOut.bOP != 0)))
                  {
                  strcpy(pszPinName, "CNT");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               // set OP pin and test it (OP is input, so set it in FPGA)
               tyArcPins.bSS0 = 0;     tyArcPins.bSS1 = 0;     tyArcPins.bCNT = 0;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, NULL, 0);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38020000;                        // set OP to 1
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_AAMode(tyHandle, NULL, &tyArcOut, 0);
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x00000000) || (tyArcOut.bOP != 1)))
                  {
                  strcpy(pszPinName, "OP");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               // set D0 pin and test it
               tyArcPins.bSS0 = 0;     tyArcPins.bSS1 = 0;     tyArcPins.bCNT = 0;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, NULL, 0);
               (void)DL_OPXD_Arc_AAData(tyHandle, 1, 0x01);                               // set D0 and clear D1
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_AAMode(tyHandle, NULL, &tyArcOut, 0);
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x00000400) || (tyArcOut.bOP != 0)))
                  {
                  strcpy(pszPinName, "D0");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               // set D1 pin and test it
               tyArcPins.bSS0 = 0;     tyArcPins.bSS1 = 0;     tyArcPins.bCNT = 0;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, NULL, 0);
               (void)DL_OPXD_Arc_AAData(tyHandle, 1, 0x02);                               // set D1 and clear D0
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_AAMode(tyHandle, NULL, &tyArcOut, 0);
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x00000800) || (tyArcOut.bOP != 0)))
                  {
                  strcpy(pszPinName, "D1");       iRet = DIAG_TPA_TEST_PIN; 
                  }

               // clear pins
               tyArcPins.bSS0 = 0;     tyArcPins.bSS1 = 0;     tyArcPins.bCNT = 0;     tyArcPins.bOP = 0;
               (void)DL_OPXD_Arc_AAMode(tyHandle, &tyArcPins, &tyArcOut, 0);
               (void)DL_OPXD_Arc_AAData(tyHandle, 0, 0x00);
               }
            else
               {  // testing just TPAOP-ARC20
               unsigned char bLocDbgAck = 0;
               uint32_t ulDefaultMask = 0x000000C0;                // pins SS0 and SS1 expected as 1
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               // test nRST pin
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask))
                  {
                  strcpy(pszPinName, "nSRST");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }
               (void)DL_OPXD_Arc_ResetProc(tyHandle, 1, 0);             // assert nSRST
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000003)))
                  {
                  strcpy(pszPinName, "nSRST");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }
               (void)DL_OPXD_Arc_ResetProc(tyHandle, 0, 0);             // assert nSRST
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               // test nTRST pin
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask)))
                  {
                  strcpy(pszPinName, "nTRST");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }
               (void)DL_OPXD_JtagResetTAP(tyHandle, true);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != (ulDefaultMask | 0x00000008)))
                  {
                  strcpy(pszPinName, "nTRST");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }
               // clear any pending flag
               pulIR[0] = 0xC;            pulDRIn[0] = 0x3800000A;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               pulIR[0] = 0xC;            pulDRIn[0] = 0x38000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               // so far, we have tested TDI, TDO, TCK, TMS, nTRST and nRST
               // we need also test RTCK (set adaptive clock and try to read ID)
               MsSleep(1000);
               //no need to check RTCK pin with TPA R1 and cJTAG mode

               pulIR[0] = 0xC;            pulDRIn[0] = 0x48000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
               (void)DL_OPXD_JtagSetAdaptiveClock(tyHandle, 100, 100.0);                  // set adaptive clock
               pulIR[0] = 0x1;            pulDROut[0] = 0;                                // ID register
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, NULL, pulDROut);  // try to read ID register when adaptive clock is selected
               ulIDValue = pulDROut[0];
               if ((iRet == DIAG_NO_ERROR) && (ulIDValue != 0x1D230A48))                  // if RTCK does not work, we should not be able to read ID
                  {
                  strcpy(pszPinName, "RTCK");
                  iRet = DIAG_TPA_TEST_PIN; 
                  }

               // set JTAG frequency to 100 MHz
               // FPGAware which includes cJTAG
               // needs to run upto 100MHz after optimization
               if (tyDeviceInfo.ulBoardRevision < 0x03000000)
                  iRet = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R2, NULL, NULL); //R1 TPA not supporting above this frequency
               else
                  iRet = DL_OPXD_JtagSetFrequency(tyHandle, MAX_JTAG_TEST_FREQ_R3, NULL, NULL); //R1 TPA not supporting above this frequency

               MsSleep(1000);
               // test DBGRQ and DBGACK pins
               (void)DL_OPXD_Arc_DebugRequest(tyHandle, 1, NULL);                         // set DBGRQ
               pulIR[0] = 0xC;            pulDRIn[0] = 0x58000000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_DebugRequest(tyHandle, 1, &bLocDbgAck);                  // get DBGACK
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x000000D0) || (bLocDbgAck != 0)))
                  {
                  strcpy(pszPinName, "DBGRQ");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               // test DBGRQ and DBGACK pins
               (void)DL_OPXD_Arc_DebugRequest(tyHandle, 0, NULL);                         // set DBGRQ
               pulIR[0] = 0xC;            pulDRIn[0] = 0x58010000;
               (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
               (void)DL_OPXD_Arc_DebugRequest(tyHandle, 0, &bLocDbgAck);                  // get DBGACK
               if ((iRet == DIAG_NO_ERROR) && (((pulDROut[0] & 0x0000FFFF) != 0x000000C0) || (bLocDbgAck == 0)))
                  {
                  strcpy(pszPinName, "DBGACK");       iRet = DIAG_TPA_TEST_PIN; 
                  }
               MsSleep(1000);
               //no need to test D0 and D1 with TPA R1
               if (uiTPARev == 0)
                  {
                  // now we should test D0 and D1 pins (not used for ARC20 connector but with ADOP-ARC15 adaptor board)
                  // we can test these pins because there is support on production test board
                  (void)DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_ARCANGEL);                   // emulate ARCangel (to be able to control D0 and D1)
                  (void)DL_OPXD_Arc_AAData(tyHandle, 1, 0x01);                               // set D0 and clear D1
                  pulIR[0] = 0xC;            pulDRIn[0] = 0x68000000;
                  (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
                  if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != 0x000004C0))
                     {
                     strcpy(pszPinName, "D0");       iRet = DIAG_TPA_TEST_PIN; 
                     }
                  // set D1 pin and test it
                  (void)DL_OPXD_Arc_AAData(tyHandle, 1, 0x02);                               // set D1 and clear D0
                  pulIR[0] = 0xC;            pulDRIn[0] = 0x68000000;
                  (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, pulDROut);
                  if ((iRet == DIAG_NO_ERROR) && ((pulDROut[0] & 0x0000FFFF) != 0x000008C0))
                     {
                     strcpy(pszPinName, "D1");       iRet = DIAG_TPA_TEST_PIN; 
                     }
                  // clear pins
                  (void)DL_OPXD_Arc_AAData(tyHandle, 0, 0x00);
                  (void)DL_OPXD_Arc_SelectTarget(tyHandle, ARCT_NORMAL);                     // set back normal ARC
                  MsSleep(1000);
                  }
               
               }
            }
            break;
         case TPA_UNKNOWN:
         case TPA_NONE:
         case TPA_INVALID:
         case TPA_DIAG:
         default:
            break;
         }
      }
   MsSleep(1000);

   // step 4 - finish TPA test
   pulIR[0] = 0xC;                  // user register
   pulDRIn[0] = 0x00000000;         // disable LED
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, pulDRIn, NULL);
   pulIR[0] = 0xF;                  // bypass
   (void)DL_OPXD_JtagScanIRandDR(tyHandle, 0, 4, pulIR, 32, NULL, NULL);
   (void)DL_OPXD_JtagResetTAP(tyHandle, false);
   MsSleep(1000);

   return iRet;
}

/****************************************************************************
     Function: MsSleep
     Engineer: Vitezslav Hola
        Input: uint32_t uiMiliseconds - number of miliseconds to sleep
       Output: none
  Description: Implementation of sleep for both Windows and Linux
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
static void MsSleep(uint32_t uiMiliseconds)
{
   #ifndef __LINUX
   Sleep(uiMiliseconds);
   #else
   usleep(uiMiliseconds * 1000);
   #endif
}

/****************************************************************************
     Function: UniGetTickCount
     Engineer: Vitezslav Hola
        Input: none
       Output: uint32_t - number of ticks
  Description: Implementation of GetTickCount for both Windows and Linux
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
static uint64_t UniGetTickCount(void)
{
   #ifndef __LINUX
   return GetTickCount64();
   #else
   struct timeval tyTimeval;
   uint32_t ulResult;
   gettimeofday(&tyTimeval, NULL);
   ulResult = tyTimeval.tv_sec % 4000000;			// make sure miliseconds will not overflow
   ulResult *= 1000;
   ulResult += (uint32_t)(tyTimeval.tv_usec / 1000);
   return ulResult;
   #endif
}

