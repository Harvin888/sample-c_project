/******************************************************************************
       Module: diagopt.h
     Engineer: Vitezslav Hola
  Description: Header for option parsing in OpXDDiag
  Date           Initials    Description
  26-Sep-2006    VH          Initial
******************************************************************************/
#ifndef _DIAGOPT_H_
#define _DIAGOPT_H_

#include "protocol/common/types.h"

typedef struct _TyDiagOptions {
   unsigned char bShowHelp;
   unsigned char bShowVersion;
   unsigned char bShowList;                        // show list of information about connected Opellas
   unsigned char bGenerateBinaryImage;             // generate binary image for manufacturing
   unsigned char bWriteTpaSigniture;
   unsigned char bProductionBoardRequired;   
   unsigned char bProductionTpa;
   unsigned char bProductionTpaAdaptor;
   char *pszTpaSerialNumber;
   char *pszTpaType;
   char *pszTpaRevision;
   char *pszSerialNumber;
   char *pszBoardRevision;
   unsigned char bRunDiagnostic;
   unsigned char bRunJtagConsole;
   int32_t iDiagnosticSelection;
   unsigned char bUpdateFirmware;
   char *pszTargetDevice;
} TyDiagOptions;


#define MAX_CORESIGHT_COMP 960
enum TyCompClass
{
    COMPO_CLASS_START		= 0x0,
	GENERIC_VERIF_COMPO		= 0x0,
	ROM_TABLE_COMP			= 0x1,
	DEBUG_COMPO				= 0x9,
	PERI_TST_BLOCK_COMPO	= 0xB,
	DATA_ENGINE_SUBSYSTEM	= 0xD,
	GENERIC_IP_COMPO		= 0xE,
	PRIMECELL_PERIP			= 0xF,
	NOT_VALID_COM			= 0x10, 
	COMPO_CLASS_END
};

enum TyCompType
{
    CORTEX_M3_NVIC      = 0x000,
	CORTEX_M3_ITM       = 0x001,
	CORTEX_M3_DWT       = 0x002,  
	CORTEX_M3_FBP       = 0x003,
	CORESIGHT_ETM11     = 0x00d,
	TI_SDTI             = 0x120,
	TI_DAPCTL           = 0x343,
	CORTEX_M3_ETM1      = 0x4e0,
	CORESIGHT_CTI       = 0x906,
	CORESIGHT_ETB       = 0x907,
	CORESIGHT_CSTF      = 0x908,
	CORESIGHT_ETM9      = 0x910,
	CORESIGHT_TPIU      = 0x912,
	CORTEX_A8_ETM       = 0x921,
	CORTEX_A8_CTI       = 0x922,
	CORTEX_M3_TPIU      = 0x923,
	CORTEX_M3_ETM2      = 0x924,
	CORTEX_A8_DEBUG     = 0xc08,
	UNKNOWN_TYPE        = 0xFFF
};


// function prototypes
void PrintUsage(void);
void InitDiagOptions(TyDiagOptions *ptyDiagOptions);
int32_t AnalyseDiagOptions(int32_t argc, char *argv[], TyDiagOptions *ptyDiagOptions);
int32_t CheckSerialNumberFormat(char *pszSerialNumber);
int32_t CheckRevisionFormat(char *pszRevisionNumber);
uint32_t GetCurrentDate(void);

#endif // _DIAGOPT_H_


