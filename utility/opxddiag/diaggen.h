/******************************************************************************
       Module: diaggen.h
     Engineer: Vitezslav Hola
  Description: Header for binary image generator in OpXDDiag
  Date           Initials    Description
  25-Sep-2006    VH          Initial
******************************************************************************/
#ifndef _DIAGGEN_H_
#define _DIAGGEN_H_

#include "diagopt.h"

// function prototypes
int32_t GenerateProductionImage(TyDiagOptions *ptyDiagOptions);
int32_t GenerateProductionImage_R2_Board(TyDiagOptions *ptyDiagOptions, uint32_t ulRevision);
int32_t GenerateProductionImage_R3_Board(TyDiagOptions *ptyDiagOptions, uint32_t ulRevision);

#endif // _DIAGGEN_H_


