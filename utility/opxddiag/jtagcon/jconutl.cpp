/****************************************************************************
       Module: jconutl.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of utility functions in JTAG console for Opella-XD

Date           Initials    Description
18-Jan-2007    VH          initial
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include "jconutl.h"
#include "protocol/common/drvopxd.h"
#include "protocol/riscv/ml_riscv_err.h"
#include "jconmsg.h"

#define TSTSC_LENGTH          512
#define TSTSC_ULONGS          (TSTSC_LENGTH / 32)


// external variable
extern TyDevHandle tyCurrentDeviceHandle;

// local variables
uint32_t uiLastIRLength = SHIR_DEFAULT_LENGTH;       
uint32_t uiLastDRLength = SHDR_DEFAULT_LENGTH;

/****************************************************************************
     Function: Parse_SHIR_DR_Params
     Engineer: Vitezslav Hola
        Input: const char *pszParamString  : string with parameters of SHIR or SHDR command
               uint32_t *pulOutValue  : pointer to ulong buffer for value
                                             length MUST be at least SCANCHAIN_ULONG_SIZE * ulongs
               uint32_t *puiLengthBits : pointer to value for number of bits to shift
               unsigned char bIRSelect      : TRUE if IR register, FALSE for DR register
       Output: unsigned char - TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: analyse string with parameters for SHDR and SHIR commands
               SHxR <val> <length>
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
unsigned char Parse_SHIR_DR_Params(const char *pszParamString,
                                   uint32_t *pulOutValue, uint32_t ulScanChainUlongs, 
                                   uint32_t *puiLengthBits, unsigned char bIRSelect)
{
   uint32_t ulLocLength;
   char *pszLocValue, *pszLocLength;
   uint32_t uiStringPosition, uiUlongIndex, uiUlongPosition;
   unsigned char ucTempValue;
   unsigned char bValid;

   assert(pszParamString != NULL);
   assert(pulOutValue != NULL);
   assert(puiLengthBits != NULL);
   assert(ulScanChainUlongs != 0);

   ulLocLength = 1;           // at least 1 bit
   // set default values for length (use last values)
   if(bIRSelect)
      ulLocLength = uiLastIRLength;
   else
      ulLocLength = uiLastDRLength;

   // format of string must be as follows
   // "0Xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbb"
   // where a's are hex numbers for value
   // and b's are decimal value for length - this parameter is optional so it could be empty
   // optionaly, value for length could be also in hex format (0Xbbbbbbbbbb)
   // length must be less or equal as MAX_SCANCHAIN_LENGTH and at least 1
   
   pszLocLength = strstr((char*)pszParamString, " ");				// get first space (between params)

   // check for second parameter and set pszLocValue to last char of value parameter
   if(pszLocLength != NULL)
      {
      pszLocLength++;                                       // shift after space
      // check if it is last parameters
      if(strstr(pszLocLength, " ") != NULL)
         return FALSE;
      ulLocLength = StrToUlong(pszLocLength, &bValid);
	  if(!bValid)
	     return FALSE;
      // set pszLocValue before space between params
      if(pszLocLength == pszParamString)
         return FALSE;
      pszLocValue = pszLocLength-2;				// we have to shift before space !
      }
   else
      {     // just value was specified
      pszLocValue = (char *)(&(pszParamString[strlen(pszParamString)-1]));
      }
   // we have valid length, now check value
   // format is "0Xaaaaaaa", so at least 0X0
   if((strlen(pszParamString) < 3) || (pszParamString[0] != '0') || (pszParamString[1] != 'X'))
      return FALSE;

   uiStringPosition = (uint32_t)(pszLocValue - pszParamString);
   uiUlongIndex = 0;                   // index in ulong array 0..SCANCHAIN_ULONG_SIZE-1
   uiUlongPosition = 0;                // position inside ulong 0..7
   if(uiStringPosition < 2)
      return false;
   *puiLengthBits = ulLocLength;
   // set ulong array to zero
   memset((void *)pulOutValue, 0x00, sizeof(uint32_t) * ulScanChainUlongs);
   while(uiStringPosition > 0)
      {
      switch(*pszLocValue)
         {
         case '0':
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':
            ucTempValue = (unsigned char)((*pszLocValue) - '0');
            break;

         case 'A' : 
         case 'B' : 
         case 'C' : 
         case 'D' : 
         case 'E' : 
         case 'F' : 
            ucTempValue = (unsigned char)(10 + ((*pszLocValue) - 'A'));
            break;

         case 'X' :     // terminating char, must be at position 1
            if(uiStringPosition != 1)
               return FALSE;
            else
               return TRUE;               // everything done, valid parameters
         default:
               return FALSE;
         }
      
	  // check ucTempValue and position in buffer (ignore other chars)
      if((ucTempValue != 0) && (uiUlongIndex < ulScanChainUlongs))
         {
         // add next 4 bits
         pulOutValue[uiUlongIndex] |= (((uint32_t)(ucTempValue)) << (uiUlongPosition*4));
         }

      // move inside the buffer
      if((++uiUlongPosition) > 7)
         {
         uiUlongIndex++;
         uiUlongPosition = 0;
         }

      pszLocValue--;
      uiStringPosition--;
      }
   return FALSE;
}


/****************************************************************************
     Function: PrintUlongArray
     Engineer: Vitezslav Hola
        Input: char *pszString              : buffer for output string
               uint32_t uiBufferLen     : buffer size in bytes
               const uint32_t *pulData : pointer to data
               uint32_t uiNumberOfUlongs : number of ulongs to print
       Output: unsigned char - false if buffer size is insufficient
  Description: print value of array of ulongs into the string
               print 
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
unsigned char PrintUlongArray(char *pszString, uint32_t uiBufferLen, const uint32_t *pulData, uint32_t uiNumberOfUlongs)
{
   uint32_t uiIndex;
   char pszTmpString[12];           // buffer for 1 ulong (8chars + '\0')

   assert(pszString != NULL);
   assert(uiBufferLen > 0);
   assert(pulData != NULL);

   if(uiNumberOfUlongs < 1)
      uiNumberOfUlongs = 1;

   strcpy(pszString, "");
   // check if buffer len is sufficient (8 chars/ulong + "0x" + terminating char)
   if((uiNumberOfUlongs * 8 + 3) > (uint32_t)uiBufferLen)
      return FALSE;
   strcpy(pszString, "0x");
   for(uiIndex=0; uiIndex < uiNumberOfUlongs; uiIndex++)
      {
      sprintf(pszTmpString, "%08x", pulData[uiNumberOfUlongs - (uiIndex + 1)]);
      strcat(pszString, pszTmpString);
      }
   return TRUE;
}

/****************************************************************************
     Function: PrintUlongArray
     Engineer: Vitezslav Hola
        Input: uint32_t *pulUlongArray  : array of ulongs
               uint32_t uiNumberOfUlongs : number of ulongs in array
               uint32_t uiNumberOfBits   : number of bits to be shifted
               unsigned char bShiftToRight   : TRUE when shifting right (FALSE for left)
       Output: none
  Description: shift array of ulongs to right/left (maximum number of  bits to shift is 31)
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void ShiftUlongArray(uint32_t *pulUlongArray, uint32_t uiNumberOfUlongs,
                     uint32_t uiNumberOfBits, unsigned char bShiftToRight)
{
   uint32_t ulTempValue, ulTempShift;
   uint32_t uiActUlong;

   assert(pulUlongArray != NULL);

   // check range of parameters, do nothing if incorrect
   if((uiNumberOfUlongs < 1) || (uiNumberOfBits < 1) || (uiNumberOfBits > 31))
      return;

   ulTempShift = 0;
   if(bShiftToRight)
      {
      // shifting right
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
         {
         ulTempValue = pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] & (0xffffffff >> (32 - uiNumberOfBits));
         pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] = (pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] >> uiNumberOfBits) 
                                                              | ulTempShift;
         ulTempShift = ulTempValue << (32 - uiNumberOfBits);
         }
      }
   else
      {
      // shifting left
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
         {
         ulTempValue = pulUlongArray[uiActUlong] & (0xffffffff << (32 - uiNumberOfBits));
         pulUlongArray[uiActUlong] = (pulUlongArray[uiActUlong] << uiNumberOfBits) 
                                                              | ulTempShift;
         ulTempShift = ulTempValue >> (32 - uiNumberOfBits);
         }
      }
}

/****************************************************************************
     Function: TestScanChain
     Engineer: Vitezslav Hola
        Input: uint32_t *puiNumberOfTAPs - pointer to value for number of TAPs on SC
       Output: unsigned char - TRUE if scan chain is OK
  Description: test scanchain, shifts patterns 0xaa, 0x55, 0xcc, 0x33
               checks number of TAPs detected in scanchain
Date           Initials    Description
27-Mar-2006    VH          Initial
****************************************************************************/
unsigned char TestScanChain(uint32_t *puiNumberOfTAPs)
{
   uint32_t uiShiftIndex;
   uint32_t pulAAPattern[TSTSC_ULONGS];
   uint32_t pul55Pattern[TSTSC_ULONGS];
   uint32_t pulRandomPattern[TSTSC_ULONGS];
   uint32_t pulOutValue1[TSTSC_ULONGS];
   uint32_t pulOutValue2[TSTSC_ULONGS];
   uint32_t pulOutValue3[TSTSC_ULONGS];

   assert(puiNumberOfTAPs != NULL);

   // restrict SC length for this test
   // define bypass pattern, 0xAA pattern and 0x55 pattern
   memset((void *)pul55Pattern, 0x55, TSTSC_ULONGS * sizeof(uint32_t));
   memset((void *)pulAAPattern, 0xAA, TSTSC_ULONGS * sizeof(uint32_t));
   for (uiShiftIndex=0; uiShiftIndex < TSTSC_ULONGS; uiShiftIndex++)
      pulRandomPattern[uiShiftIndex] = (uint32_t)rand();

   // shift all 1s to scanchain
   (void)DL_OPXD_JtagScanIRandDR(tyCurrentDeviceHandle, 0, 256, NULL, TSTSC_LENGTH, NULL, NULL);
   // shift bypass to IR (DR == 1 bit) and shift AA pattern to DR

   (void)DL_OPXD_JtagScanIRandDR(tyCurrentDeviceHandle, 0, 256, NULL, TSTSC_LENGTH, pulAAPattern, pulOutValue1);
   (void)DL_OPXD_JtagScanIRandDR(tyCurrentDeviceHandle, 0, 256, NULL, TSTSC_LENGTH, pulRandomPattern, pulOutValue3);
   (void)DL_OPXD_JtagScanIRandDR(tyCurrentDeviceHandle, 0, 256, NULL, TSTSC_LENGTH, pul55Pattern, pulOutValue2);

   // now shift results till get patterns, number of shifts == number of TAPs
   *puiNumberOfTAPs = 0;
   for(uiShiftIndex=0; uiShiftIndex < (2*MAX_TAPS_ON_SCANCHAIN); uiShiftIndex++)
      {
      if(CompareUlongs(pulAAPattern, pulOutValue1, TSTSC_LENGTH - uiShiftIndex) &&
         CompareUlongs(pul55Pattern, pulOutValue2, TSTSC_LENGTH - uiShiftIndex) &&
         CompareUlongs(pulRandomPattern, pulOutValue3, TSTSC_LENGTH - uiShiftIndex)
         )
         {
         // both patterns shifted by the same number of bits revealed on output
         *puiNumberOfTAPs = uiShiftIndex;
         return TRUE;
         }
      // shift all arrays
      ShiftUlongArray(pulOutValue1, TSTSC_ULONGS, 1, true);
      ShiftUlongArray(pulOutValue2, TSTSC_ULONGS, 1, true);
      ShiftUlongArray(pulOutValue3, TSTSC_ULONGS, 1, true);
      }
   // we could not detect scan chain correctly
   return FALSE;
}

/****************************************************************************
     Function: CompareUlongs
     Engineer: Vitezslav Hola
        Input: uint32_t *pulUlongs1   - first array of ulongs to compare
               uint32_t *pulUlongs2   - second array of ulongs to compare
               uint32_t uiNumberOfBits - number of bits from ulongs to compare
       Output: unsigned char - TRUE if ulongs are identical in specified bits
  Description: compares to ulong arrays with specified number of bits
Date           Initials    Description
18-Mar-2006    VH          Initial
****************************************************************************/
unsigned char CompareUlongs(uint32_t *pulUlongs1, uint32_t *pulUlongs2, uint32_t uiNumberOfBits)
{
   uint32_t uiIndex;
   uint32_t ulMask;

   assert(pulUlongs1 != NULL);
   assert(pulUlongs2 != NULL);

   for(uiIndex=0; uiIndex < (uiNumberOfBits / 32); uiIndex++)
      {     // compare whole ulongs
      if(*pulUlongs1++ != *pulUlongs2++)
         return FALSE;
      }

   uiNumberOfBits = uiNumberOfBits % 32;
   // compare rest of ulong
   if(uiNumberOfBits == 0)
      return TRUE;
   ulMask = 0xffffffff >> (32 - uiNumberOfBits);
   if((*pulUlongs1 & ulMask) != (*pulUlongs2 & ulMask))
      return FALSE;

   return TRUE;
}


/****************************************************************************
     Function: PrintMessage
     Engineer: Vitezslav Hola
        Input: TyMessageType tyMessageType - type of message
               char * pszToFormat - format of message with variable number of parameters (as printf)
       Output: none
  Description: prints out messages to console, debug window, etc...
Date           Initials    Description
19-Jan-2007    VH          Initial
*****************************************************************************/
void PrintMessage(TyMessageType tyMessageType, const char * pszToFormat, ...)
{
   char     szBuffer[MAX_OUTPUT_LINE];
   char     szDisplay[MAX_OUTPUT_LINE];
   va_list  marker; // = NULL; commented for builing in linux

//lint -save -e*
   va_start(marker, pszToFormat);
   vsprintf(szBuffer, pszToFormat, marker); 
   va_end  (marker);
//lint -restore

   switch (tyMessageType)
      {
      case HELP_MESSAGE:
         strcpy(szDisplay, szBuffer);
         break;
      case INFO_MESSAGE:
         sprintf(szDisplay, "%s\n", szBuffer);
         break;
      case ERROR_MESSAGE:
         sprintf(szDisplay, "Error: %s\n", szBuffer);
         break;
      case DEBUG_MESSAGE:
         sprintf(szDisplay, "Debug: %s\n", szBuffer);
         break;
      case LOG_MESSAGE:
//         LogMessage(szBuffer, "");
         return;
      default:
         return;
      }

   printf("%s",szDisplay);
}

/****************************************************************************
     Function: StrToUlong
     Engineer: Vitezslav Hola
        Input: pszValue *: string value
               pbValid  *: storage for flag if value is valid
       Output: ulong : value represented by the string
  Description: convert string to long supports dec or hex (when starting with 0x...)
Date           Initials    Description
20-Jan-2007    VH          Initial
*****************************************************************************/
uint32_t StrToUlong(char * pszValue, unsigned char * pbValid)
{
   uint32_t ulValue;
   char * pszStop = NULL;

   // Dont allow octal, so skip leading '0's unless its 0x...
   while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] != 'X') && (pszValue[1] != '\0'))
      pszValue++;

   // strtoul supports decimal nnnn and hex 0xnnnn...
   ulValue = strtoul(pszValue, &pszStop, 0);

   // only valid if all characters were processed ok...
   if ((pszStop) && (pszStop[0] == '\0'))
      {
      if (pbValid)
         *pbValid = TRUE;
      return ulValue;
      }
   else
      {
      if (pbValid)
         *pbValid = FALSE;
      return 0;
      }
}

/****************************************************************************
     Function: StrToUlong2
     Engineer: shameerudheen PT
        Input: pszValue *: string value
               pbValid  *: storage for flag if value is valid
               pszStop  *: end character of the Value
       Output: ulong : value represented by the string
  Description: convert string to long supports dec or hex (when starting with 0x...)
Date           Initials    Description
02-Jul-2007    SPT          Initial
*****************************************************************************/
uint32_t StrToUlong2(char *pszValue, unsigned char *pbValid, const char *pszStopchar)
{
   uint32_t ulValue;
   char * pszStop = NULL;

   pszStop = (char*)pszStopchar;

   // Dont allow octal, so skip leading '0's unless its 0x...
   while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] != 'X') && (pszValue[1] != '\0'))
      pszValue++;

   // strtoul supports decimal nnnn and hex 0xnnnn...
   ulValue = strtoul(pszValue, &pszStop, 0);

   // only valid if all characters were processed ok...
   if ((pszStop != NULL) && ((pszStop[0] == '\0') || pszStop[0] == ' '))
      {
      if (pbValid)
         *pbValid = TRUE;
      return ulValue;
      }
   else
      {
      if (pbValid)
         *pbValid = FALSE;
      return 0;
      }
}

/****************************************************************************
     Function: StrToUpr
     Engineer: Vitezslav Hola
        Input: char *pszString - pointer to string
       Output: none
  Description: convert string to upper case
Date           Initials    Description
01-Feb-2007    VH          Initial
*****************************************************************************/
void StrToUpr(char *pszString)
{
   assert(pszString != NULL);

   while (*pszString)
      {
      if ((*pszString >= 'a') && (*pszString <= 'z'))
         {
         *pszString += ('A' - 'a');
         }
      pszString++;
      }
}


/****************************************************************************
     Function: Parse_ArmSHIRDR_Params
     Engineer: Shameerudheen PT
        Input: const char *pszParamString     : string with parameters of ArmSHIRDR command
               uint32_t *pulOutIRValue   : pointer to ulong buffer for IRvalue 
                                                length MUST be at least SCANCHAIN_ULONG_SIZE * ulongs
               uint32_t *pulOutDRValue   : pointer to ulong buffer for DRvalue 
                                                length MUST be at least SCANCHAIN_ULONG_SIZE * ulongs
               uint32_t *puiLengthBitsIR : pointer to IRvalue for number of bits to shift 
               uint32_t *puiLengthBitsDR : pointer to DRvalue for number of bits to shift  
       Output: unsigned char - TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: analyse string with parameters for ArmSHIRDR commands
               ArmSHIRDR <IRval> <IRlength> <DRval> <DRlength>
Date           Initials    Description
29-Jan-2007    SPT          Initial
****************************************************************************/
unsigned char Parse_ArmSHIRDR_Params(const char *pszParamString,
                                     uint32_t *pulOutIRValue, 
                                     uint32_t ulScanChainUlongsIR, 
                                     uint32_t *pulOutDRValue,
                                     uint32_t ulScanChainUlongsDR, 
                                     uint32_t *puiLengthBitsIR,
                                     uint32_t *puiLengthBitsDR)
{
   
   uint32_t ulLocIRLength;
   uint32_t ulLocDRLength;
   char *pszLocValue, *pszLocLength;
   uint32_t uiStringPosition, uiUlongIndex, uiUlongPosition;
   unsigned char ucTempValue;
   unsigned char bValid;
   const char *pszIRVal;
   char *pszIRLen,*pszDRVal,*pszDRLen; 
   //uint32_t uiTempExit = 0,

   assert(pszParamString != NULL);
   assert(pulOutIRValue != NULL);
   assert(pulOutDRValue != NULL);
   assert(puiLengthBitsIR != NULL);
   assert(puiLengthBitsDR != NULL);
   assert(ulScanChainUlongsIR != 0);
   assert(ulScanChainUlongsDR != 0);

   
   ulLocIRLength = uiLastIRLength;
   ulLocDRLength = uiLastDRLength;

   // format of string must be as follows
   // "0Xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbb 0Xaaaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbbb"
   // where a's are hex numbers for value
   // and b's are decimal value for length - this parameter is optional so it could be empty
   // optionaly, value for length could be also in hex format (0Xbbbbbbbbbb)
   // length must be less or equal as MAX_SCANCHAIN_LENGTH and at least 1

   pszIRVal = pszParamString;
   pszLocLength = strstr((char*)pszParamString, " ");
   if (pszLocLength == NULL)
   {
       return FALSE; // only one parameter
   }
   else
   {
       pszIRLen = ++pszLocLength;
       pszLocLength = strstr(pszLocLength, " ");
       if (pszLocLength == NULL)
       {
           return FALSE; // only two parameter
       }
       else
       {
           pszDRVal = ++pszLocLength;
           pszLocLength = strstr(pszLocLength, " ");
           if (pszLocLength == NULL)
           {
               return FALSE; // only three parameter
           }
           else
           {
               pszDRLen = ++pszLocLength;
               if(strstr(pszLocLength, " ") != NULL)
                   return FALSE; // more than four parameters
               //return TRUE;
           }
       }
   }
       

   //Getting DRLen into puiLengthBitsDR
   ulLocDRLength = StrToUlong(pszDRLen, &bValid);
   if(!bValid)
      return FALSE;
   *puiLengthBitsDR = ulLocDRLength;


   //working on DRVal to get extracted and to load into pulOutDRValue in exact format
   // we have valid length, now check value
   // format is "0Xaaaaaaa", so at least 0X0
   if((strlen(pszDRVal) < 3) || (pszDRVal[0] != '0') || (pszDRVal[1] != 'X'))
      return FALSE;

   //uiStringPosition = (uint32_t)(pszLocValue - pszParamString);
   uiStringPosition = (uint32_t)(pszDRLen - pszDRVal - 2);
   uiUlongIndex = 0;                   // index in ulong array 0..SCANCHAIN_ULONG_SIZE-1
   uiUlongPosition = 0;               // position inside ulong 0..7
   pszLocValue = pszDRLen - 2;
   if(uiStringPosition < 2)
      return false;
   //*puiLengthBits = ulLocLength;
   // set ulong array to zero
   memset((void *)pulOutDRValue, 0x00, sizeof(uint32_t) * ulScanChainUlongsDR);
   while(uiStringPosition > 0)
   {
      switch(*pszLocValue)
      {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
         ucTempValue = (unsigned char)((*pszLocValue) - '0');
         break;

      case 'A' : 
      case 'B' : 
      case 'C' : 
      case 'D' : 
      case 'E' : 
      case 'F' : 
         ucTempValue = (unsigned char)(10 + ((*pszLocValue) - 'A'));
         break;

      case 'X' :     // terminating char, must be at position 1
         if(uiStringPosition != 1)
            return FALSE;
         else
            goto ProcesIRLen;
            //uiTempExit = 1;               // everything done, valid parameters
            //break;
      default:
         return FALSE;
      }
      
	  // check ucTempValue and position in buffer (ignore other chars)
      if((ucTempValue != 0) && (uiUlongIndex < ulScanChainUlongsDR))
      {
          // add next 4 bits
          pulOutDRValue[uiUlongIndex] |= (((uint32_t)(ucTempValue)) << (uiUlongPosition * 4));
      }

      // move inside the buffer
      if((++uiUlongPosition) > 7)
      {
          uiUlongIndex++;
          uiUlongPosition = 0;
      }

      pszLocValue--;
      uiStringPosition--;
   }
   return FALSE;

ProcesIRLen:
   //Getting IRLen into puiLengthBitsIR
   ulLocIRLength = StrToUlong2(pszIRLen, &bValid," ");
   if(!bValid)
      return FALSE;
   *puiLengthBitsIR = ulLocIRLength;

   //working on IRVal to get extracted and to load into pulOutIRValue in exact format
   // we have valid length, now check value
   // format is "0Xaaaaaaa", so at least 0X0
   if((strlen(pszParamString) < 3) || (pszParamString[0] != '0') || (pszParamString[1] != 'X'))
      return FALSE;

   //uiStringPosition = (uint32_t)(pszLocValue - pszParamString);
   uiStringPosition = (uint32_t)(pszIRLen - pszIRVal - 2);
   uiUlongIndex = 0;                   // index in ulong array 0..SCANCHAIN_ULONG_SIZE-1
   uiUlongPosition = 0;               // position inside ulong 0..7
   pszLocValue = pszIRLen - 2;
   if(uiStringPosition < 2)
      return false;
   //*puiLengthBits = ulLocLength;
   // set ulong array to zero
   memset((void *)pulOutIRValue, 0x00, sizeof(uint32_t) * ulScanChainUlongsIR);
   while(uiStringPosition > 0)
   {
      switch(*pszLocValue)
      {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
         ucTempValue = (unsigned char)((*pszLocValue) - '0');
         break;

      case 'A' : 
      case 'B' : 
      case 'C' : 
      case 'D' : 
      case 'E' : 
      case 'F' : 
         ucTempValue = (unsigned char)(10 + ((*pszLocValue) - 'A'));
         break;

      case 'X' :     // terminating char, must be at position 1
         if(uiStringPosition != 1)
            return FALSE;
         else
            return TRUE;               // everything done, valid parameters
      default:
         return FALSE;
      }
      
	  // check ucTempValue and position in buffer (ignore other chars)
      if((ucTempValue != 0) && (uiUlongIndex < ulScanChainUlongsIR))
      {
          // add next 4 bits
          pulOutIRValue[uiUlongIndex] |= (((uint32_t)(ucTempValue)) << (uiUlongPosition * 4));
      }

      // move inside the buffer
      if((++uiUlongPosition) > 7)
      {
          uiUlongIndex++;
          uiUlongPosition = 0;
      }

      pszLocValue--;
      uiStringPosition--;
   }
   return FALSE;
}

#ifdef RISCV_CMDS
/****************************************************************************
	 Function: ConvertDwErrToOpxdlayerErr
	 Engineer: Chandrasekar
		Input: unsigned long UlError : error from diskware
	   Output: none
  Description: error from Diskware
Date           Initials    Description
03-Jun-2019    CBR          Initial
*****************************************************************************/
char* ConvertDwErrToOpxdlayerErr(unsigned long UlError)
{
	char* pszLocMsg;
	static char cTempMsg[50];

	switch (UlError)
	{
	case DRVOPXD_ERROR_NO_ERROR:                       // no error
		pszLocMsg = (char*)MSG_ML_NO_ERROR;                 break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUSY:
		pszLocMsg = (char*)MSG_ML_RISCV_ABSTRACT_REG_WRITE_BUSY;      break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED:
		pszLocMsg = (char*)MSG_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED;      break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION:
		pszLocMsg = (char*)MSG_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION;      break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_HART_STATE:
		pszLocMsg = (char*)MSG_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE;      break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR:
		pszLocMsg = (char*)MSG_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR;      break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON:
		pszLocMsg = (char*)MSG_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR;      break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED:
		pszLocMsg = (char*)MSG_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED;      break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ADDRESS_FAILED:
		pszLocMsg = (char*)MSG_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED;      break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED:
		pszLocMsg = (char*)MSG_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED;      break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED:
		pszLocMsg = (char*)MSG_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED;      break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_OTHER_REASON:
		pszLocMsg = (char*)MSG_ML_RISCV_SYSTEM_BUS_OTHER_REASON;      break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_MASTER_BUSY:
		pszLocMsg = (char*)MSG_ML_RISCV_SYSTEM_BUS_MASTER_BUSY;      break;
	case DRVOPXD_ERROR_RISCV_MEM_INVALID_LENGTH:
		pszLocMsg = (char*)MSG_ML_RISCV_MEM_INVALID_LENGTH;      break;
	case DRVOPXD_ERROR_RISCV_HALT_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_HALT_TIMEOUT;      break;
	case DRVOPXD_ERROR_RISCV_RESUME_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_RESUME_TIMEOUT;      break;
	case DRVOPXD_ERROR_RISCV_UNSUPPORT_ARCH_TYPE:
		pszLocMsg = (char*)MSG_ML_RISCV_UNSUPPORT_ARCH_TYPE;      break;
	case DRVOPXD_ERROR_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE:
		pszLocMsg = (char*)MSG_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE;      break;
	case DRVOPXD_ERROR_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION:
		pszLocMsg = (char*)MSG_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;      break;
	case DRVOPXD_ERROR_RISCV_TRIGGER_INFO_LOOP_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT;      break;
	case DRVOPXD_ERROR_RISCV_DISALE_ABSTRACT_COMMAND:
		pszLocMsg = (char*)MSG_ML_RISCV_DISALE_ABSTRACT_COMMAND;      break;
	case DRVOPXD_ERROR_RISCV_NEXT_DEBUG_MODULE_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT;      break;
	default:                                        // unspecified error
		sprintf(cTempMsg, "%s", (char*)MSG_ML_UNKNOWN_ERROR);
		pszLocMsg = cTempMsg;
		break;
	}
	return pszLocMsg;
}

/****************************************************************************
	 Function: Parse_RiscvWR_Params
	 Engineer: Chandrasekar
		Input: char *pszParams                   : params related to the command
		       unsigned long ulArchType          : to pass arch type
			   unsigned long *pulAddress         : to get address
			   unsigned long* pulMech            : to get mechanism
			   unsigned long* pulSizeOrDoublePre : to get mem_size or float mech
			   uint32_t *pulData                 : to get the data
	   Output: unsigned char - TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: write operation parse for input
Date           Initials    Description
17-Jun-2019    CBR          Initial
*****************************************************************************/
unsigned char Parse_RiscvWR_Params(char* pszParams, unsigned long ulArchType, unsigned long *pulAddress, unsigned long* pulMech,
	                       unsigned long* pulSizeOrDoublePre, uint32_t *pulData)
{
	unsigned long ulscanf = 0;
	uint32_t uExtra = 0;
	unsigned char ucType = 0;

	ulscanf = sscanf(pszParams, "%c", &ucType);

	switch (ucType)
	{
	case 'r':
	case 'R':
		if ((ulArchType == RISCV_8_BIT_ARCH) | (ulArchType == RISCV_16_BIT_ARCH) | (ulArchType == RISCV_32_BIT_ARCH))
		{
			if (((sscanf(pszParams, "%c %x %x %x", &ucType, pulAddress, &pulData[0], &uExtra) < 4) &
				(sscanf(pszParams, "%c %x %x", &ucType, pulAddress, &pulData[0]) > 2)) == 0)
			{
				return FALSE;
			}
		}
		else if (ulArchType == RISCV_64_BIT_ARCH)
		{
			if (((sscanf(pszParams, "%c %x %x %x %x ", &ucType, pulAddress, &pulData[0], &pulData[1], &uExtra) < 5) &
				(sscanf(pszParams, "%c %x %x", &ucType, pulAddress, &pulData[0]) > 2)) == 0)
			{
				return FALSE;
			}
		}
		else
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x %x", &ucType, pulAddress, &pulData[0], &pulData[1], &pulData[2], &pulData[3], &uExtra) < 7) &
				(sscanf(pszParams, "%c %x %x", &ucType, pulAddress, &pulData[0]) > 2)) == 0)
			{
				return FALSE;
			}
		}
		if ((*pulAddress < RISCV_FLOATING_POINT_REGISTER_START_Address) & (*pulAddress > RISCV_CSR_LAST_ADDR))
		{
			*pulAddress = *pulAddress - (RISCV_GPRS_START_ADDR);
		}
		else
		{
			return FALSE;
		}
		break;
	case 'f':
	case 'F':
		if ((ulArchType == RISCV_8_BIT_ARCH) | (ulArchType == RISCV_16_BIT_ARCH) | (ulArchType == RISCV_32_BIT_ARCH))
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0], &uExtra) < 6) &
				(sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0]) > 4 )) == 0)
			{

				return FALSE;
			}
		}
		else if (ulArchType == RISCV_64_BIT_ARCH)
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x %x ", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0], &pulData[1], &uExtra) < 7) &
				(sscanf(pszParams, "%c %x %x %x %x ", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0]) > 4)) == 0)
			{
				return FALSE;
			}
		}
		else 
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x %x %x %x ", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0], &pulData[1], &pulData[2], &pulData[3], &uExtra) < 9) &
				(sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0]) > 4)) == 0)
			{
				return FALSE;
			}
		}
		if ((*pulAddress < RISCV_RESERVED_START_VALUE) & (*pulAddress > RISCV_GPRS_END_VALUE) & (*pulMech < 2) & (*pulSizeOrDoublePre < 2))
		{
			//floating point, adress range is 0x1020 to 0x103f. we need to pass only 33 to 64
			*pulAddress = *pulAddress - (RISCV_CSR_LAST_ADDR);
		}
		else
		{
			return FALSE;
		}
		break;
	case 'c':
	case 'C':
		if ((ulArchType == RISCV_8_BIT_ARCH) | (ulArchType == RISCV_16_BIT_ARCH) | (ulArchType == RISCV_32_BIT_ARCH))
		{
			if (((sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, &pulData[0], &uExtra) < 5) &
				(sscanf(pszParams, "%c %x %x %x", &ucType, pulAddress, pulMech, &pulData[0]) > 3)) == 0)
			{
				return FALSE;
			}
		}
		else if (ulArchType == RISCV_64_BIT_ARCH)
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x", &ucType, pulAddress, pulMech, &pulData[0], &pulData[1], &uExtra) < 6) &
				(sscanf(pszParams, "%c %x %x %x", &ucType, pulAddress, pulMech, &pulData[0]) > 3)) == 0)
			{
				return FALSE;
			}
		}
		else 
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x %x %x", &ucType, pulAddress, pulMech, &pulData[0], &pulData[1], &pulData[2], &pulData[3], &uExtra) < 8) &
				(sscanf(pszParams, "%c %x %x %x", &ucType, pulAddress, pulMech, &pulData[0]) > 3)) == 0)
			{
				return FALSE;
			}
		}
		if ((*pulAddress == RISCV_DPC_ADDR) & (*pulMech < 2))
		{
			*pulAddress = RISCV_CSR_DPC_FROM_GDB;
		}
		else if ((*pulAddress < RISCV_GPRS_START_ADDR) & (*pulMech < 2))
		{
			*pulAddress = *pulAddress + RISCV_GDB_CSR_START_ADDRESS;
		}
		else
		{
			return FALSE;
		}
		break;
	case 'm':
	case 'M':
		if ((ulArchType == RISCV_8_BIT_ARCH) | (ulArchType == RISCV_16_BIT_ARCH) | (ulArchType == RISCV_32_BIT_ARCH))
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0], &uExtra) < 6) &
				(sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0]) > 4)) == 0)
			{
				return FALSE;
			}
		}
		else if (ulArchType == RISCV_64_BIT_ARCH)
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x %x ", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0], &pulData[1], &uExtra) < 7) &
				(sscanf(pszParams, "%c %x %x %x %x ", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0]) > 4)) == 0)
			{
				return FALSE;
			}
		}
		else 
		{
			if (((sscanf(pszParams, "%c %x %x %x %x %x %x %x %x ", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0], &pulData[1], &pulData[2], &pulData[3], &uExtra) < 9) &
				(sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &pulData[0]) > 4)) == 0)
			{
				return FALSE;
			}
		}
		if (((*pulSizeOrDoublePre == 1) | (*pulSizeOrDoublePre == 2) | (*pulSizeOrDoublePre == 4)) & (*pulMech < 3))
		{
			if (*pulMech == 2)
			{
				*pulMech = RISCV_ABSTRCT_MEM_ACCESS;
			}
			else
			{
				*pulMech = *pulMech + 1;
			}
		}
		else
		{
			return FALSE;
		}
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

/****************************************************************************
	 Function: Parse_RiscvRD_Params
	 Engineer: Chandrasekar
		Input: char *pszParams                   : params related to the command
			   unsigned long ulArchType          : to pass arch type
			   unsigned long *pulAddress         : to get address
			   unsigned long* pulMech            : to get mechanism
			   unsigned long* pulSizeOrDoublePre : to get mem_size or float mech
			   uint32_t *pulData                 : to get the data
	   Output: unsigned char - TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: read operation parse for input
Date           Initials    Description
17-Jun-2019    CBR          Initial
*****************************************************************************/
unsigned char Parse_RiscvRD_Params(char* pszParams, unsigned long ulArchType, unsigned long* pulAddress, unsigned long* pulMech,
	unsigned long* pulSizeOrDoublePre)
{
	unsigned long ulscanf = 0;
	uint32_t uExtra = 0;
	unsigned char ucType = 0;

	ulscanf = sscanf(pszParams, "%c", &ucType);

	switch (ucType)
	{
	case 'r':
	case 'R':
		if (sscanf(pszParams, "%c %x %x", &ucType, pulAddress, &uExtra) != 2)
		{
			return FALSE;
		}
		//gprs
		if ((*pulAddress < RISCV_FLOATING_POINT_REGISTER_START_Address) & (*pulAddress > RISCV_CSR_LAST_ADDR))
		{
			*pulAddress = *pulAddress - (RISCV_GPRS_START_ADDR);
		}
		else
		{
			return FALSE;
		}
		break;
	case 'f':
	case 'F':
		if (sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &uExtra) != 4)
		{
			return FALSE;
		}
		//floating point
		if ((*pulAddress < RISCV_RESERVED_START_VALUE) & (*pulAddress > RISCV_GPRS_END_VALUE) & (*pulMech < 2) & (*pulSizeOrDoublePre < 2))
		{
			//floating point, adress range is 0x1020 to 0x103f
			*pulAddress = *pulAddress - (RISCV_CSR_LAST_ADDR);
		}
		else
		{
			return FALSE;
		}
		break;
	case 'c':
	case 'C':
		if (sscanf(pszParams, "%c %x %x %x", &ucType, pulAddress, pulMech, &uExtra) != 3)
		{
			return FALSE;
		}
		if ((*pulAddress == RISCV_DPC_ADDR) & (*pulMech < 2))
		{
			*pulAddress = RISCV_CSR_DPC_FROM_GDB;
		}
		else if ((*pulAddress < RISCV_GPRS_START_ADDR) & (*pulMech < 2))
		{
			*pulAddress = *pulAddress + RISCV_GDB_CSR_START_ADDRESS;
		}
		else
		{
			return FALSE;
		}
		break;
	case 'm':
	case 'M':
		if (sscanf(pszParams, "%c %x %x %x %x", &ucType, pulAddress, pulMech, pulSizeOrDoublePre, &uExtra) != 4)
		{
			return FALSE;
		}
		if (((*pulSizeOrDoublePre == 1) | (*pulSizeOrDoublePre == 2) | (*pulSizeOrDoublePre == 4)) & (*pulMech < 3))
		{
			if (*pulMech == 2)
			{
				*pulMech = RISCV_ABSTRCT_MEM_ACCESS;
			}
			else
			{
				*pulMech = *pulMech + 1;
			}
		}
		else
		{
			return FALSE;
		}
	}
	return TRUE;
}

/****************************************************************************
	 Function: Riscv_RD_WR_Print
	 Engineer: Chandrasekar
		Input: unsigned long ulArchType          : to pass arch type
			   unsigned long pulAddress          : to pass address
			   uint32_t *puData                  : to get the data
	   Output: unsigned char - TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: print message for read and write operations
Date           Initials    Description
17-Jun-2019    CBR          Initial
*****************************************************************************/
unsigned char Riscv_RD_WR_Print(unsigned long ulArchType, unsigned long pulAddress, uint32_t* puData, uint32_t uType)
{
	
	if (uType == 1)					//for print message read GPRs,CSRs and float
	{
		switch (ulArchType)
		{
		case 8:
		case 16:
		case 32:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_ACT, puData[0], pulAddress);
			break;
		case 64:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_ACT_2, puData[1], puData[0], pulAddress);
			break;
		case 128:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_ACT_3, puData[3], puData[2], puData[1], puData[0], pulAddress);
			break;
		default:
			return FALSE;
		}
	}
	else if(uType == 2)							//for print message write GPRs,CSRs and float
	{
		switch (ulArchType)
	    {
		case 8:
		case 16:
		case 32:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_ACT, puData[0], pulAddress);
			break;
		case 64:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_ACT_2, puData[1], puData[0], pulAddress);
			break;
		case 128:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_ACT_3, puData[3], puData[2], puData[1], puData[0], pulAddress);
			break;
		default:
			return FALSE;
	    }
    }
	else if (uType == 3)						//for print message read Memory
	{
		PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_ACT, puData[0], pulAddress);
	}
	else if (uType == 4)						//for print message write Memory
	{
		PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_ACT, puData[0], pulAddress);
	}
	else
	{
		return FALSE;
	}
	return TRUE;
}
#endif
