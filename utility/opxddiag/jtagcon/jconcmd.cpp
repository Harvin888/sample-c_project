/****************************************************************************
       Module: jconcmd.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of commands in JTAG console for Opella-XD

Date           Initials    Description
18-Jan-2007    VH          initial
29-Jun-2007    SPT         added ArmShIRDR Command
27-Jul-2007    SPT         aded armrreg Command// This command will not visible in help
27-Sep-2007    VH          added test commands for ThreadArch and ARC
28-Sep-2007    VH          merged with SH changes for ARM
05-OCT-2007    SPT         added ArmTest Command
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "jconcmd.h"
#include "jconmsg.h"
#include "jconutl.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdtarch.h"
#include "protocol/common/drvopxdarc.h"
#include "protocol/rdi/opxdarmdll/common.h"
#include "protocol/common/drvopxdarm.h"
#include "protocol/common/drvopxdriscv.h"
#include "../../../firmware/export/api_def.h"

#ifndef __LINUX      // Win includes
#include <windows.h>
#else                // Linux includes
#include <unistd.h>
#endif

// uncomment following lines if you want to add test commands for particular target
//#define REDPINE_CMDS
//#define ARC_CMDS // this moved to diagglob.h
//#define ARM_CMDS // pass this define as arg to makefile
//#define RTCK_SUPPORT
//#define RISCV_CMDS

// defines
#ifdef ARC_CMDS
#ifndef __LINUX
#define S_SLEEP(x)         Sleep(x*1000)                 // sleep for win32
#else
#define S_SLEEP(x)         sleep(x)                      // sleep for linux
#endif
#endif

#define MAX_INPUT_LINE              (4096)         // maximum length of input line
#define CONSOLE_COMMAND_PROMPT      "JTAG>"        // string for command prompt
#define CFILE_COMMAND_PROMPT        "JTAG:"        // string for command files

#define MAX_JTFREQ_VALUE            166.0          // maximum JTAG frequency
#define MAX_JTFREQ_STR              "166"          // string with maximum JTAG frequency
#define MIN_JTFREQ_VALUE            0.001          // minimum JTAG frequency
#define MIN_JTFREQ_STR              "0.001"        // string with minimum JTAG frequency

// local function prototypes and definitions
void FilterInputString(char *pszInputString);
void JCMD_QuitExecute(char *pszParams);
void JCMD_QuitHelp(void);
void JCMD_CFileExecute(char *pszParams);
void JCMD_CFileHelp(void);
void JCMD_HelpExecute(char *pszParams);
void JCMD_HelpHelp(void);
void JCMD_SHIRExecute(char *pszParams);
void JCMD_SHIRHelp(void);
void JCMD_SHDRExecute(char *pszParams);
void JCMD_SHDRHelp(void);
void JCMD_TSTSCExecute(char *pszParams);
void JCMD_TSTSCHelp(void);
void JCMD_JTFREQExecute(char *pszParams);
void JCMD_JTFREQHelp(void);
void JCMD_TapResetExecute(char *pszParams);
void JCMD_TapResetHelp(void);
void JCMD_TpaExecute(char *pszParams);
void JCMD_TpaHelp(void);
void JCMD_WaitExecute(char *pszParams);
void JCMD_WaitHelp(void);
void JCMD_CHKSCExecute(char *pszParams);
void JCMD_CHKSCHelp(void);
void JCMD_InitTap7Execute(char *pszParams);
void JCMD_InitTap7Help(void);
void JCMD_SetTms(char* pszParams);
void JCMD_SetTmsHelp(void);

#ifdef RISCV_CMDS
void JCMD_RiscvInit(char *pszParams);
void JCMD_RiscvInitHelp(void);
void JCMD_RiscvHartHalt(char *pszParams);
void JCMD_RiscvHartHaltHelp(void);
void JCMD_RiscvRD(char* pszParams);
void JCMD_RiscvRDHelp(void);
void JCMD_RiscvWR(char* pszParams);
void JCMD_RiscvWRHelp(void);
#endif

#ifdef ARM_CMDS
#if defined (DEBUG) || defined (_DEBUG)
// test functions (ARM test)
void JCMD_ArmSHIRDRExecute(char *pszParams);
void JCMD_ArmSHIRDRHelp(void);
void JCMD_ArmRRegExecute(char *pszParams);
void JCMD_ArmRRegHelp(void);
void JCMD_ArmTestExecute(char *pszParams);
void JCMD_ArmTestHelp(void);
#endif //Debug build
#endif //Debug build

#ifdef REDPINE_CMDS
// test functions (RedPine test)
void JCMD_RPHOLDExecute(char *pszParams);
void JCMD_RPHOLDHelp(void);
void JCMD_RPRELExecute(char *pszParams);
void JCMD_RPRELHelp(void);
void JCMD_RPSSExecute(char *pszParams);
void JCMD_RPSSHelp(void);
void JCMD_RPRDExecute(char *pszParams);
void JCMD_RPRDHelp(void);
void JCMD_RPWRExecute(char *pszParams);
void JCMD_RPWRHelp(void);
#endif

#ifdef ARC_CMDS
// test functions (RedPine test)
void JCMD_ARCRESExecute(char *pszParams);
void JCMD_ARCRESHelp(void);
void JCMD_ARCPINExecute(char *pszParams);
void JCMD_ARCPINHelp(void);
void JCMD_ARCBLASTExecute(char *pszParams);
void JCMD_ARCBLASTHelp(void);
void JCMD_ARCEXTCMDExecute(char *pszParams);
void JCMD_ARCEXTCMDHelp(void);
void JCMD_ARCRDExecute(char *pszParams);
void JCMD_ARCRDHelp(void);
void JCMD_ARCWRExecute(char *pszParams);
void JCMD_ARCWRHelp(void);
#endif


typedef void (TyCommandExecute) (char *pszParams);
typedef void (TyCommandHelpFunc)(void);



// local type definitions
typedef struct
{
   const char           * pszCommandName;           // name of function
   TyCommandExecute     * pfCommandFunc;            // executing a command
   TyCommandHelpFunc    * pfHelpFunc;               // function to display a help
   unsigned char        bMIPSCommand;             // command is specific for MIPS
} TyConsoleCommand;


unsigned char IsCommandSupported(const TyConsoleCommand *ptyCurrentCommand);

#ifdef ARC_CMDS
#define ARCANGEL_BLAST_SIZE               (64*1024)
unsigned char pucARCangelBlast[ARCANGEL_BLAST_SIZE];
#endif

// extern variables
extern uint32_t uiLastIRLength;
extern uint32_t uiLastDRLength;

// local variables
TyDevHandle tyCurrentDeviceHandle;
double dCurrentJtagFrequency = 1.00;         // 1MHz is default frequency
unsigned char bUseAdaptiveClock = 0;         // using adaptive clock
unsigned char ucTpaTrackMode = 1;
#ifdef RISCV_CMDS
TyRiscvParam ptyRiscvParam = {0};
#endif
// --------------------------------------
// JTAG Console Command Table
// use UPPER characters to define strings
// --------------------------------------
TyConsoleCommand tyConsoleCommands[] =
{
   {                                    // command HELP
   "HELP",
   JCMD_HelpExecute,
   JCMD_HelpHelp,
   FALSE
   },
   {                                    // command SHIR
   "SHIR",
   JCMD_SHIRExecute,
   JCMD_SHIRHelp,
   FALSE
   },
   {                                    // command SHDR
   "SHDR",
   JCMD_SHDRExecute,
   JCMD_SHDRHelp,
   FALSE
   },
   {                                    // command TSTSC
   "TSTSC",
   JCMD_TSTSCExecute,
   JCMD_TSTSCHelp,
   FALSE
   },
   {
   "CHKSC",                             // command CHKSC
   JCMD_CHKSCExecute,
   JCMD_CHKSCHelp,
   FALSE
   },
   {                                    // command JTFREQ
   "JTFREQ",
   JCMD_JTFREQExecute,
   JCMD_JTFREQHelp,
   FALSE
   },
   {                                    // command TAPRES
   "TAPRES",
   JCMD_TapResetExecute,			
   JCMD_TapResetHelp,
   FALSE
   },
   {                                    // command TPA
   "TPA",
   JCMD_TpaExecute,			
   JCMD_TpaHelp,
   FALSE
   },
   {                                    // command CFILE
   "CFILE",
   JCMD_CFileExecute,
   JCMD_CFileHelp,
   FALSE
   },
   {                                    // command WAIT
   "WAIT",
   JCMD_WaitExecute,
   JCMD_WaitHelp,
   FALSE
   },
   {                                    // command INITTAP7
   "INITTAP7",
   JCMD_InitTap7Execute,
   JCMD_InitTap7Help,
   FALSE
   },
   {                                    // command settms
   "SETTMS",
   JCMD_SetTms,
   JCMD_SetTmsHelp,
   FALSE
   },
#ifdef RISCV_CMDS
   {                                    // command Opxddiag param
   "RVINIT",
   JCMD_RiscvInit,
   JCMD_RiscvInitHelp,
   FALSE
   },
   {                                    // command Opxddiag Riscv hart to halt
   "RVHALT",
   JCMD_RiscvHartHalt,
   JCMD_RiscvHartHaltHelp,
   FALSE
   },
   {                                    // command Opxddiag Riscv Read option
   "RVRD",
   JCMD_RiscvRD,
   JCMD_RiscvRDHelp,
   FALSE
   },
   {                                    // command Opxddiag Riscv Write option
   "RVWR",
   JCMD_RiscvWR,
   JCMD_RiscvWRHelp,
   FALSE
   },
#endif
#ifdef REDPINE_CMDS
   // test commands for RedPine
   {                                    // command RPHOLD
   "RPHOLD",
   JCMD_RPHOLDExecute,
   JCMD_RPHOLDHelp,
   FALSE
   },
   {                                    // command RPREL
   "RPREL",
   JCMD_RPRELExecute,
   JCMD_RPRELHelp,
   FALSE
   },
   {                                    // command RPSS
   "RPSS",
   JCMD_RPSSExecute,
   JCMD_RPSSHelp,
   FALSE
   },
   {                                    // command RPRD
   "RPRD",
   JCMD_RPRDExecute,
   JCMD_RPRDHelp,
   FALSE
   },
   {                                    // command RPWR
   "RPWR",
   JCMD_RPWRExecute,
   JCMD_RPWRHelp,
   FALSE
   },
#endif
#ifdef ARC_CMDS
   // test commands for ARC
   {                                    // command ARCRES
   "ARCRES",
   JCMD_ARCRESExecute,
   JCMD_ARCRESHelp,
   FALSE
   },
   {                                    // command ARCPIN
   "ARCPIN",
   JCMD_ARCPINExecute,
   JCMD_ARCPINHelp,
   FALSE
   },
   {                                    // command ARCBLAST
   "ARCBLAST",
   JCMD_ARCBLASTExecute,
   JCMD_ARCBLASTHelp,
   FALSE
   },
   {                                    // command ARCEXTCMD
   "ARCEXTCMD",
   JCMD_ARCEXTCMDExecute,
   JCMD_ARCEXTCMDHelp,
   FALSE
   },
   {                                    // command ARCRD
   "ARCRD",
   JCMD_ARCRDExecute,
   JCMD_ARCRDHelp,
   FALSE
   },
   {                                    // command ARCWR
   "ARCWR",
   JCMD_ARCWRExecute,
   JCMD_ARCWRHelp,
   FALSE
   },
#endif
#ifdef ARM_CMDS
#if defined (DEBUG) || defined (_DEBUG)
   {                                    // command ARMSHIRDR
   "ARMSHIRDR",
   JCMD_ArmSHIRDRExecute,
   JCMD_ArmSHIRDRHelp,
   FALSE
   },
   {                                    // command ARMRead Register
   "ARMRREG",
   JCMD_ArmRRegExecute,
   JCMD_ArmRRegHelp,
   FALSE
   },
   {                                    // command ARMTEST All cores
   "ARMTEST",
   JCMD_ArmTestExecute,
   JCMD_ArmTestHelp,
   FALSE
   },
#endif //Debug build
#endif
   {                                    // command QUIT
   "QUIT",
   JCMD_QuitExecute,
   JCMD_QuitHelp,
   FALSE
   }
};

unsigned char *pbFinishFlag = NULL;                   // pointer to flag (quit uses it)
TyCmdFiles *ptyLocCmdFiles = NULL;                    // pointer to command files structure (cfile uses it)

/****************************************************************************
     Function: JCMD_QuitExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute QUIT command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_QuitExecute(char *pszParams)
{
    assert(pszParams != NULL);

    // no parameters for quit
    if(!strcmp(pszParams, ""))
       *pbFinishFlag = TRUE;
    else
       {
       PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
       }
}

/****************************************************************************
     Function: JCMD_QuitHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for QUIT command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_QuitHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_QUIT_HELP);
}

/****************************************************************************
     Function: JCMD_CFileExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute CFILE command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_CFileExecute(char *pszParams)
{
   FILE *pFile;
   uint32_t uiIndex;
   unsigned char bFreeSlot;
    
   assert(pszParams != NULL);
   // we require 1 parameter as filename
   if(!strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   else
      {
      // open file for reading
      pFile = fopen(pszParams, "rt");
      if(pFile == NULL)
         {
         PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CFILE_EOPEN);
         return;
         }
      }
	
   // check nested files and find free pointer
   bFreeSlot = FALSE;
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      if(ptyLocCmdFiles->pCFile[uiIndex] == NULL)
         {
         bFreeSlot = TRUE;
         ptyLocCmdFiles->pCFile[uiIndex] = pFile;
         if((strlen(pszParams) + 4) > MAX_CFILE_NAME)
            {
            strncpy(ptyLocCmdFiles->pszCFileNames[uiIndex], pszParams, MAX_CFILE_NAME-4);
            ptyLocCmdFiles->pszCFileNames[uiIndex][MAX_CFILE_NAME-4] = 0;
            strcat(ptyLocCmdFiles->pszCFileNames[uiIndex], "...");
            }
         else
            strcpy(ptyLocCmdFiles->pszCFileNames[uiIndex], pszParams);
         break;
         }
      }

   if(!bFreeSlot)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CFILE_ENESTED, MAX_NESTED_CFILE);
      fclose(pFile);
      }
}


/****************************************************************************
     Function: JCMD_CFileHelp
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: show help for CFILE command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_CFileHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CFILE_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CFILE_HELP_2);
}


/****************************************************************************
     Function: JCMD_HelpExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute HELP command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_HelpExecute(char *pszParams)
{
    uint32_t uiIndex;
    TyConsoleCommand *ptyCurrentCommand;

   // assert(pszParams != NULL);

    if(strcmp(pszParams, ""))
       {
       PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
       return;
       }

    // show help header
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_HELP_1);

    // show help for all supported commands
    for(uiIndex=0; uiIndex < (sizeof(tyConsoleCommands) / sizeof(TyConsoleCommand)); uiIndex++)
        {
        // call help function for each command, add concatenate all strings
        ptyCurrentCommand = &(tyConsoleCommands[uiIndex]);
        if(IsCommandSupported(ptyCurrentCommand) && (ptyCurrentCommand->pfHelpFunc != NULL))
           (ptyCurrentCommand->pfHelpFunc)();
        }
}


/****************************************************************************
     Function: JCMD_HelpHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for HELP command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_HelpHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_HELP_HELP);
}


/****************************************************************************
     Function: IsCommandSupported
     Engineer: Vitezslav Hola
        Input: const TyConsoleCommand *ptyCurrentCommand : pointer to command description
       Output: unsigned char - TRUE if command is supported at the moment (MIPS specific, etc.)
  Description: decide whether the command is supported (given mode, parameters of gdb server, etc.)
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
unsigned char IsCommandSupported(const TyConsoleCommand *ptyCurrentCommand)
{
   if (!ptyCurrentCommand->bMIPSCommand)
      return TRUE;
   return TRUE;
}


/****************************************************************************
     Function: DoConsoleCommand
     Engineer: Vitezslav Hola
        Input: const char *pszCommandLine : input string with command
               TyCmdFiles *ptyCmdFiles    : pointer to structure with command files
               unsigned char *pbFinish    : pointer to finish variable      
       Output: none
  Description: get command line, parse it and call particular function
               for special commands (quit and cfile), fill passed structure and variable
               to signal request for reading file or to exit 
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void DoConsoleCommand(const char *pszCommandLine, TyCmdFiles *ptyCmdFiles, unsigned char *pbFinish)
{
    uint32_t uiNumberOfCommands, uiIndex;
    TyConsoleCommand *ptyCurrentCommand;
    char *pszParsedCommandLine = NULL;
    char *pszCommandParams;
    char *pszOriginCommand = NULL;
    unsigned char bValidCommand;

    assert(pbFinish != NULL);
    assert(ptyCmdFiles != NULL);
    assert(pszCommandLine != NULL);

    // initialize local pointers
    ptyLocCmdFiles = ptyCmdFiles;
    pbFinishFlag = pbFinish;
    *pbFinishFlag = FALSE;

    // split command line, find first space
    pszParsedCommandLine = strdup(pszCommandLine);              // duplicate string (free must be called at the end)
    if(pszParsedCommandLine == NULL)
        {
        // show critical error
        *pbFinishFlag = TRUE;
        return;                                   // terminate console and stop parsing command
        }
    pszCommandParams = strstr(pszParsedCommandLine, " ");
    if(pszCommandParams == NULL)
        {       // no parameters (no space), set pointer to empty string (terminating char)
        pszCommandParams = &(pszParsedCommandLine[strlen(pszParsedCommandLine)]);
        }
    else
        {       // split string
        *pszCommandParams = '\0';           // cut pszParsedCommandLine
        pszCommandParams++;                 // move after space
        }

    // save original command
    pszOriginCommand = strdup(pszParsedCommandLine);
    if(pszOriginCommand == NULL)
        {

        // show critical error
        *pbFinishFlag = TRUE;
        free(pszParsedCommandLine);
        return;                                   // terminate console and stop parsing command
        }

    // convert command string to uppercase
    StrToUpr(pszParsedCommandLine);

    // start to parse commands
    bValidCommand = FALSE;
    uiNumberOfCommands = sizeof(tyConsoleCommands) / sizeof(TyConsoleCommand);
    for(uiIndex=0; uiIndex < uiNumberOfCommands; uiIndex++)
        {
        ptyCurrentCommand = &(tyConsoleCommands[uiIndex]);
        if((!strcmp(pszParsedCommandLine, ptyCurrentCommand->pszCommandName)) && IsCommandSupported(ptyCurrentCommand))
            {
            // valid command found, now execute it
            assert(ptyCurrentCommand->pfCommandFunc != NULL);
            (ptyCurrentCommand->pfCommandFunc)(pszCommandParams);
            bValidCommand = TRUE;
            break;                          // stop cycle
            }
        }

    // display warning message for undefined command
    if((!bValidCommand) && (strcmp(pszParsedCommandLine, "")))
       {
       // restrict length of pszOriginCommand to MAX_INPUT_LINE, cut command and add "..."
       if((strlen(pszOriginCommand) + 4) > (MAX_INPUT_LINE))
          {
          pszOriginCommand[MAX_INPUT_LINE - 4] = '\0';            // cut original command
          strcat(pszOriginCommand, "...");                     // and add ...
          }
       PrintMessage(INFO_MESSAGE, JMSG_UKNOWN_COMMAND, pszOriginCommand);
       }

    // free memory after strdup
    if(pszParsedCommandLine != NULL)
        free(pszParsedCommandLine);
    if(pszOriginCommand != NULL)
        free(pszOriginCommand);
}


/****************************************************************************
     Function: JCMD_SHIRExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute SHIR command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_SHIRExecute(char *pszParams)
{
   unsigned char pszUlongString[8+SCANCHAIN_ULONG_SIZE*8];
   uint32_t pulOutBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t pulInBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t ulNumberOfBits;
   uint32_t uiUlongsToShow;

   assert(pszParams != NULL);

   // clear both buffers (fill with zero's
   memset((void*)pulOutBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
   memset((void*)pulInBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);

   // check parameters
   StrToUpr(pszParams);
   if(!Parse_SHIR_DR_Params(pszParams, pulOutBuffer, SCANCHAIN_ULONG_SIZE, &ulNumberOfBits, TRUE))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check number of bits to shift
   if((ulNumberOfBits < 1) || (ulNumberOfBits > MAX_SCANCHAIN_LENGTH))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_ELEN, MAX_SCANCHAIN_LENGTH);
      return;
      }

   switch (DL_OPXD_JtagScanIR(tyCurrentDeviceHandle, 0, ulNumberOfBits, pulOutBuffer, pulInBuffer))
      {
      case DRVOPXD_ERROR_NO_ERROR:
         {     // no error happened
         // how many ulongs to show ?
         uiUlongsToShow = ulNumberOfBits / 32;
         if(ulNumberOfBits % 32)
            uiUlongsToShow++;
         // print result (data shifted out from IR)
         (void)PrintUlongArray((char *)pszUlongString, 8+SCANCHAIN_ULONG_SIZE*8, pulInBuffer, uiUlongsToShow);
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_RESULT, pszUlongString);
         // update last used value
         uiLastIRLength = ulNumberOfBits;
         }
         break;

      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         PrintMessage(ERROR_MESSAGE, JMSG_ADAPTIVE_CLOCK_TIMEOUT);
         return;

      default:
         PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_SHIR_ERROR);
         return;
      }
}

/****************************************************************************
	 Function: JCMD_SHDRExecute
	 Engineer: Vitezslav Hola
		Input: char *pszParams          : params related to the command
	   Output: none
  Description: execute SHDR command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_SHDRExecute(char* pszParams)
{
	unsigned char pszUlongString[8 + SCANCHAIN_ULONG_SIZE * 8];
	uint32_t pulOutBuffer[SCANCHAIN_ULONG_SIZE];
	uint32_t pulInBuffer[SCANCHAIN_ULONG_SIZE];
	uint32_t ulNumberOfBits;
	uint32_t uiUlongsToShow;

	assert(pszParams != NULL);

	// clear both buffers (fill with zero's
	memset((void*)pulOutBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
	memset((void*)pulInBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
	// check parameters
	StrToUpr(pszParams);
	if (!Parse_SHIR_DR_Params(pszParams, pulOutBuffer, SCANCHAIN_ULONG_SIZE, &ulNumberOfBits, FALSE))
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	// check number of bits to shift
	if ((ulNumberOfBits < 1) || (ulNumberOfBits > MAX_SCANCHAIN_LENGTH))
	{
		PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_ELEN, MAX_SCANCHAIN_LENGTH);
		return;
	}

	switch (DL_OPXD_JtagScanDR(tyCurrentDeviceHandle, 0, ulNumberOfBits, pulOutBuffer, pulInBuffer))
	{
	case DRVOPXD_ERROR_NO_ERROR:
	{
		// how many ulongs to show ?
		uiUlongsToShow = ulNumberOfBits / 32;
		if (ulNumberOfBits % 32)
			uiUlongsToShow++;
		(void)PrintUlongArray((char*)pszUlongString, 8 + SCANCHAIN_ULONG_SIZE * 8, pulInBuffer, uiUlongsToShow);
		PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_RESULT, pszUlongString);
		// update last used value
		uiLastDRLength = ulNumberOfBits;
	}
	break;

	case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
		PrintMessage(ERROR_MESSAGE, JMSG_ADAPTIVE_CLOCK_TIMEOUT);
		return;

	default:
		PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_SHDR_ERROR);
		return;
	}
}


/****************************************************************************
     Function: JCMD_SHIRHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for SHIR command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_SHIRHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP_4);
}


/****************************************************************************
     Function: JCMD_SHDRHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for SHDR command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_SHDRHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP_4);
}


/****************************************************************************
     Function: JCMD_TapResetExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute RESET command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_TapResetExecute(char *pszParams)
{
   assert(pszParams != NULL);

   StrToUpr(pszParams);
   if(!strcmp(pszParams, ""))
      {
      // use default selection
      (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TAPRES_TMS);
      return;
      }

   if(!strcmp(pszParams, "TMS"))
      {
      // use default selection
      (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TAPRES_TMS);
      return;
      }
   
   if(!strcmp(pszParams, "TRST"))
      {
      // use default selection
      (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, true);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TAPRES_TRST);
      return;
      }

   PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
   return;
}

/****************************************************************************
     Function: JCMD_TapResetHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for reset command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_TapResetHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RESET_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RESET_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RESET_HELP_3);
}


/****************************************************************************
     Function: JCMD_TpaExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute TPA command
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void JCMD_TpaExecute(char *pszParams)
{
   double dFixedVoltage;
   TyDeviceInfo tyDeviceInfo;

   assert(pszParams != NULL);

   (void)DL_OPXD_GetDeviceInfo(tyCurrentDeviceHandle, &tyDeviceInfo, DW_UNKNOWN);

   StrToUpr(pszParams);
   if(!strcmp(pszParams, ""))
      {
      // show information about TPA
      if (tyDeviceInfo.tyTpaConnected == TPA_NONE)
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_NONE);
      else
         {
         switch(tyDeviceInfo.tyTpaConnected)
            {
            case TPA_MIPS14:
               PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_0, "MIPS14");
               break;
            case TPA_ARM20:
               PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_0, "ARM20");
               break;
            case TPA_ARC20:
               PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_0, "ARC20");
               break;
            case TPA_UNKNOWN:
            case TPA_NONE:
            case TPA_INVALID:
            case TPA_DIAG:
            default:
               PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_0, "unknown");
               break;
            }
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_1, tyDeviceInfo.dCurrentVtref);
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_INFO_2, tyDeviceInfo.dMinVtpa, tyDeviceInfo.dMaxVtpa);
         if (ucTpaTrackMode)
            PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_TRACK);
         else
            PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_FIXED, tyDeviceInfo.dCurrentVtpa);
         }
      return;
      }

   if(!strcmp(pszParams, "TRACK"))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_TRACK);
      (void)DL_OPXD_SetVtpaVoltage(tyCurrentDeviceHandle, 0.0, TRUE, TRUE);
      ucTpaTrackMode = 1;
      return;
      }

   dFixedVoltage = atof(pszParams);
   if ((dFixedVoltage == 0.0) || 
       ((tyDeviceInfo.tyTpaConnected != TPA_NONE) && ((dFixedVoltage < tyDeviceInfo.dMinVtpa) || (dFixedVoltage > tyDeviceInfo.dMaxVtpa))))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_OUT_RANGE, tyDeviceInfo.dMinVtpa, tyDeviceInfo.dMaxVtpa);
      }
   else
      {
      (void)DL_OPXD_SetVtpaVoltage(tyCurrentDeviceHandle, dFixedVoltage, FALSE, TRUE);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_FIXED, dFixedVoltage);
      ucTpaTrackMode = 0;
      }
   return;
}

/****************************************************************************
     Function: JCMD_TpaHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for tpa command
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void JCMD_TpaHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TPA_HELP_3);
}

/****************************************************************************
     Function: JCMD_JTFREQExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute JTFREQ command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_JTFREQExecute(char *pszParams)
{
   double dSelectedFreq;
   char pszFreq[20];

   assert(pszParams != NULL);

   // check parameters
   // without parameter, just display current frequency
   if(!strcmp(pszParams, ""))
      {
      if (bUseAdaptiveClock)
         {
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_ADAPTIVE);
         }
      else
         {
         sprintf(pszFreq, "%3.3f", dCurrentJtagFrequency);
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_CURRENT, pszFreq);
         }
      return;
      }
   #ifdef RTCK_SUPPORT
   // ok, let check what user wants to set
   // posibly adaptive clock
   if(!strcmp(pszParams, "adaptive") || !strcmp(pszParams, "ADAPTIVE"))
      {
      if (DL_OPXD_JtagSetAdaptiveClock(tyCurrentDeviceHandle, 100, 1.0) != DRVOPXD_ERROR_NO_ERROR)
         {
         char pszTmp[255];
         sprintf(pszTmp, JMSG_COMMAND_JTFREQ_VALUES, MIN_JTFREQ_STR, MAX_JTFREQ_STR);
         PrintMessage(INFO_MESSAGE, pszTmp);
         return;
         }
      bUseAdaptiveClock = 1;
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_ADAPTIVE);
      return;
      }
   #endif

   dSelectedFreq = atof(pszParams);
   if (dSelectedFreq < MIN_JTFREQ_VALUE)
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // try to set selected frequency
   if ((dSelectedFreq > MAX_JTFREQ_VALUE) || 
       (DL_OPXD_JtagSetFrequency(tyCurrentDeviceHandle, dSelectedFreq, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
      {
      char pszTmp[255];
      sprintf(pszTmp, JMSG_COMMAND_JTFREQ_VALUES, MIN_JTFREQ_STR, MAX_JTFREQ_STR);
      PrintMessage(INFO_MESSAGE, pszTmp);
      return;
      }

   dCurrentJtagFrequency = dSelectedFreq;
   bUseAdaptiveClock = 0;
   sprintf(pszFreq, "%3.3f", dCurrentJtagFrequency);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_SET, pszFreq);
   return;
}

/****************************************************************************
     Function: JCMD_JTFREQHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for JTFREQ command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_JTFREQHelp(void)
{
   #ifdef RTCK_SUPPORT
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP_2);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP_3);
   #else
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP_A);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP_2_A);
   #endif
}
/****************************************************************************
     Function: JCMD_CHKSCExecute
     Engineer: Amerdas S K
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute CHKSC command
Date           Initials    Description
9-OCT-2009      ADK          Initial
****************************************************************************/
void JCMD_CHKSCExecute(char *pszParams)
{
   uint32_t uiNumberOfTAPs;
   uint32_t ulTAPIndex=0;

   uint32_t pulAAPattern[MAX_CHAIN_ULONG_SIZE];
   uint32_t pul55Pattern[MAX_CHAIN_ULONG_SIZE];
   uint32_t pulCCPattern[MAX_CHAIN_ULONG_SIZE];
   uint32_t pul33Pattern[MAX_CHAIN_ULONG_SIZE];
   
   uint32_t pulOutValueAA[MAX_CHAIN_ULONG_SIZE];
   uint32_t pulOutValue55[MAX_CHAIN_ULONG_SIZE];
   uint32_t pulOutValueCC[MAX_CHAIN_ULONG_SIZE];
   uint32_t pulOutValue33[MAX_CHAIN_ULONG_SIZE];

   uint32_t ulTotalIRlengthAA = 0;
   uint32_t ulTotalIRlength55 = 0;
   uint32_t ulTotalIRlengthCC = 0;
   uint32_t ulTotalIRlength33 = 0;

   uint32_t pulIDCodeFromScan[MAX_TAPS_ON_SCANCHAIN]= {0};
   uint32_t pulIDCode[MAX_TAPS_ON_SCANCHAIN]= {0};

   uint32_t ulNonJTAGDeviceFound = 0;  
   uint32_t uiShiftIndex;
   uint32_t ulDelay;
   
   assert(pszParams != NULL);

   //there should be no parameters for tstsc
   if(strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

      // test scan chain and get number of TAPs
   uiNumberOfTAPs = 0;
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   if(!TestScanChain(&uiNumberOfTAPs))
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TSTSC_FAILED);
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_PASSED, uiNumberOfTAPs);
      }

   //OK..now find the total IR length
   memset((void *)pulAAPattern,0xaa,MAX_CHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul55Pattern,0x55,MAX_CHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pulCCPattern,0xcc,MAX_CHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul33Pattern,0x33,MAX_CHAIN_ULONG_SIZE * sizeof(uint32_t));

   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagScanIR(tyCurrentDeviceHandle, 0, MAX_CHAIN_LENGTH, pulAAPattern, pulOutValueAA);

   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagScanIR(tyCurrentDeviceHandle, 0, MAX_CHAIN_LENGTH, pul55Pattern, pulOutValue55);
  
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagScanIR(tyCurrentDeviceHandle, 0, MAX_CHAIN_LENGTH, pulCCPattern, pulOutValueCC);

   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagScanIR(tyCurrentDeviceHandle, 0, MAX_CHAIN_LENGTH, pul33Pattern, pulOutValue33);

   for(uiShiftIndex=0; uiShiftIndex < 64; uiShiftIndex++)
       {
            if(CompareUlongs(pulAAPattern, pulOutValueAA, (MAX_CHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlengthAA = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValueAA, MAX_CHAIN_ULONG_SIZE, 1, TRUE);
       }

   for(uiShiftIndex=0; uiShiftIndex < 64; uiShiftIndex++)
       {
            if(CompareUlongs(pul55Pattern, pulOutValue55, (MAX_CHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlength55 = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValue55, MAX_CHAIN_ULONG_SIZE, 1, TRUE);
       }

   for(uiShiftIndex=0; uiShiftIndex < 64; uiShiftIndex++)
       {
            if(CompareUlongs(pulCCPattern, pulOutValueCC, (MAX_CHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlengthCC = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValueCC, MAX_CHAIN_ULONG_SIZE, 1, TRUE);
       }
  
   for(uiShiftIndex=0; uiShiftIndex < 64; uiShiftIndex++)
       {
            if(CompareUlongs(pul33Pattern, pulOutValue33, (MAX_CHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlength33 = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValue33, MAX_CHAIN_ULONG_SIZE, 1, TRUE);
       }
   if(((ulTotalIRlengthAA != ulTotalIRlength55) || 
       (ulTotalIRlengthAA != ulTotalIRlengthCC) || 
       (ulTotalIRlengthAA != ulTotalIRlength33))) 
   {
       PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CHKSC_FAILED);
       return;
   } 
   else
   {
       PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_PASSED ,ulTotalIRlengthAA);
   }


   //Find the total ID code of all devices in the scan chain 
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   for(ulDelay=0; ulDelay < 0xFFFFFF; ulDelay++){}
   (void)DL_OPXD_JtagResetTAP(tyCurrentDeviceHandle, false);
   (void)DL_OPXD_JtagScanDR(tyCurrentDeviceHandle,0,(uiNumberOfTAPs * ULONG_BITS),pulAAPattern, pulIDCodeFromScan);

   //Adjust for JTAG Compliant device is also done
    for(ulTAPIndex=0; ulTAPIndex < uiNumberOfTAPs; ulTAPIndex++)
     {
       if(pulIDCodeFromScan[ulTAPIndex] & (0x00000001 << ulNonJTAGDeviceFound))
       {
           if(ulNonJTAGDeviceFound == 0)
           {
               pulIDCode[ulTAPIndex] = pulIDCodeFromScan[(uiNumberOfTAPs-1) - ulTAPIndex];
           }
           else
           {
               pulIDCode[ulTAPIndex + ulNonJTAGDeviceFound] = (((pulIDCodeFromScan[(uiNumberOfTAPs - 1) - ulTAPIndex] >> ulNonJTAGDeviceFound) & (0xFFFFFFFF >> ulNonJTAGDeviceFound)) |
                                                               ((pulIDCodeFromScan[((uiNumberOfTAPs - 1) - ulTAPIndex) + 1] & (0xFFFFFFFF >> (32 - ulNonJTAGDeviceFound))) << (32 - ulNonJTAGDeviceFound)));
           }
       }
       else
       {
           ulNonJTAGDeviceFound++;
           pulIDCode[ulTAPIndex + ulNonJTAGDeviceFound - 1] = 0x00000000;
           pulIDCode[ulTAPIndex + ulNonJTAGDeviceFound] = (((pulIDCodeFromScan[(uiNumberOfTAPs - 1) - ulTAPIndex] >> ulNonJTAGDeviceFound) & (0xFFFFFFFF >> ulNonJTAGDeviceFound)) |
                                                           ((pulIDCodeFromScan[((uiNumberOfTAPs - 1) - ulTAPIndex) + 1] & (0xFFFFFFFF >> (32 - ulNonJTAGDeviceFound))) << (32 - ulNonJTAGDeviceFound)));
       }      
     }
	// show header
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_HEADER3);

   for(ulTAPIndex=0; ulTAPIndex < uiNumberOfTAPs; ulTAPIndex++)
     {
     PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_HEADER4, 
                  ulTAPIndex,pulIDCode[ulTAPIndex]);
     }
}

/****************************************************************************
     Function: JCMD_CHKSCHelp
     Engineer: Amerdas D K
        Input: none
       Output: none
  Description: show help for TSTSC command
Date           Initials    Description
9-Oct-2009      ADK         Initial
****************************************************************************/
void JCMD_CHKSCHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_HELP);
}
/****************************************************************************
     Function: JCMD_TSTSCExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute TSTSC command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_TSTSCExecute(char *pszParams)
{
   uint32_t uiNumberOfTAPs;

   assert(pszParams != NULL);

   //there should be no parameters for tstsc
   if(strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_START);

   // test scan chain and get number of TAPs
   uiNumberOfTAPs = 0;
   if(!TestScanChain(&uiNumberOfTAPs))
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TSTSC_FAILED);
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_PASSED, uiNumberOfTAPs);
      }
}

/****************************************************************************
     Function: JCMD_TSTSCHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for TSTSC command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_TSTSCHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_HELP);
}


/****************************************************************************
     Function: JCMD_WaitExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute Wait command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_WaitExecute(char *pszParams)
{
   uint32_t uiDelayTime;

   assert(pszParams != NULL);
   // check parameters
   StrToUpr(pszParams);
   

   if(sscanf(pszParams, "%u", &uiDelayTime) != 1)
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check range
   if (uiDelayTime > MAX_WAIT_TIME_MS)
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_RANGE, MAX_WAIT_TIME_MS);
      return;
      }

   // just do wait
   if (uiDelayTime > 0)
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_WAITING, uiDelayTime);
      #ifndef __LINUX
      Sleep(uiDelayTime);
      #else
      usleep(uiDelayTime * 1000);                 // converting to microseconds !!!
      #endif
      }
}

/****************************************************************************
     Function: JCMD_WaitHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for Wait command
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
void JCMD_WaitHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_HELP_2);
}

/****************************************************************************
     Function: JCMD_InitTap7Execute
     Engineer: Shameerudheen P T
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute TAP.7 Controller initialisation
Date           Initials    Description
08-MAr-2012    SPT         Initial
****************************************************************************/
void JCMD_InitTap7Execute(char *pszParams)
{
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   tyResult = DL_OPXD_cJtagInitTap7Controller(tyCurrentDeviceHandle);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      PrintMessage(INFO_MESSAGE, "TAP.7 Initialisation done.");
   else
      PrintMessage(INFO_MESSAGE, "Error during TAP.7 Initialisation.");

   assert(pszParams != NULL);  
   return;
}

/****************************************************************************
     Function: JCMD_InitTap7Help
     Engineer: Shameerudheen P T
        Input: none
       Output: none
  Description: show help for Wait command
Date           Initials    Description
08-Mar-2012    SPT         Initial
****************************************************************************/
void JCMD_InitTap7Help(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_INITTAP7_HELP);
}

/****************************************************************************
	 Function: JCMD_SetTms
	 Engineer: Chandrasekar B R
		Input: char *pszParams          : params related to the command
	   Output: none
  Description: set the TMS value for SHIR,SHDR and SHIRDR
Date           Initials    Description
24-May-19	   CBR			  Initial
****************************************************************************/
void JCMD_SetTms(char* pszParams)
{
	TyTmsSequence ptySetTmsForScanIR, ptySetTmsForScanDR;
	uint32_t uIndex = 0;
	unsigned long ulDataArrayComplete[8] = {0};		// max input for settms 8, so array size fixed to eight
	char* cpPtr = NULL;

	assert(pszParams != NULL);

	//split the 1st position with space
	cpPtr = strtok(pszParams, " ");

	//if none of the argument is passed as a parameter
	if (cpPtr == NULL)
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	//string to unsigned long
	ulDataArrayComplete[uIndex] = strtoul(cpPtr, NULL, 0);

	//checking value should be non-zero, for alphabet value recieve would be zero other than Hexa digits
	if (ulDataArrayComplete[uIndex] == 0)
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	while (cpPtr != NULL)
	{
		uIndex++;
		//to move next space
		cpPtr = strtok(NULL, " ");
		
		if ((uIndex <= 7) && (cpPtr != NULL))
		{
			//write the value to write for TMS setting
			ulDataArrayComplete[uIndex] = strtoul(cpPtr, NULL, 0);

			//checking value should be non-zero, for alphabet value recieve would be zero other than Hexa digits
			if (ulDataArrayComplete[uIndex] == 0)
			{
				PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
				return;
			}
		}
		//space can accepted after 8th input, should work fine 
		else if ((cpPtr == NULL) && (uIndex == 8))
		{
			break;
		}
		else
		{
			//if number of input passed is greater/less than 8
			PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
			return;
		}
	}
	
	ptySetTmsForScanIR.ucPreTmsCount	= (unsigned char )ulDataArrayComplete[0];
	ptySetTmsForScanIR.usPreTmsBits		= (unsigned short)ulDataArrayComplete[1];
	ptySetTmsForScanIR.ucPostTmsCount	= (unsigned char)ulDataArrayComplete[2];
	ptySetTmsForScanIR.usPostTmsBits	= (unsigned short)ulDataArrayComplete[3];

	ptySetTmsForScanDR.ucPreTmsCount	= (unsigned char)ulDataArrayComplete[4];
	ptySetTmsForScanDR.usPreTmsBits		= (unsigned short)ulDataArrayComplete[5];
	ptySetTmsForScanDR.ucPostTmsCount	= (unsigned char)ulDataArrayComplete[6];
	ptySetTmsForScanDR.usPostTmsBits	= (unsigned short)ulDataArrayComplete[7];

    //setting tms value for shir and shdr command,return error from firmware other than expected values
	if ((DL_OPXD_JtagSetTMS(tyCurrentDeviceHandle, &ptySetTmsForScanIR, &ptySetTmsForScanDR, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)) {
		PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TMS_ERROR);
		return;
	}
}

/****************************************************************************
	 Function: JCMD_SetTmsHelp
	 Engineer: Chandrasekar B R
		Input: none
	   Output: none
  Description: show help for Settms command
Date           Initials    Description
24-May-2019    CBR         Initial
****************************************************************************/
void JCMD_SetTmsHelp(void)
{
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SETTMS_HELP);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SETTMS_HELP_2);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SETTMS_HELP_3);
}

#ifdef RISCV_CMDS
/****************************************************************************
	 Function: JCMD_RiscvInit
	 Engineer: Chandrasekar B R
		Input: char *pszParams          : params related to the command
	   Output: none
  Description: setting Architechture type and call diskware for initial
			   operations
Date           Initials    Description
12-June-19	   CBR			  Initial
****************************************************************************/
void JCMD_RiscvInit(char* pszParams)
{
	uint32_t uIndex = 0;
	unsigned long ulDataArrayComplete[3] = { 0 };		
	unsigned long ulTemp = 0;
	char* cpPtr = NULL ;

	assert(pszParams != NULL);
	//split the 1st position with space
	cpPtr = strtok(pszParams, " ");

	if (cpPtr == NULL)
	{
		//if no input
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}
	// check for other alphabets
	if (((strtoul(cpPtr, NULL, NULL) == RISCV_8_BIT_ARCH) | (strtoul(cpPtr, NULL, NULL) == RISCV_16_BIT_ARCH) |
		(strtoul(cpPtr, NULL, NULL) == RISCV_32_BIT_ARCH) | (strtoul(cpPtr, NULL, NULL) == RISCV_64_BIT_ARCH) | (strtoul(cpPtr, NULL, NULL) == RISCV_128_BIT_ARCH)) == 0)
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}
	else
	{
		ptyRiscvParam.ulArchType = strtoul(cpPtr, NULL, NULL);
	}

	while (cpPtr != NULL)
	{
		cpPtr = strtok(NULL, " ");
		uIndex++;
	}
	if (uIndex != 1)
	{
		//if input is more than one
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	if (DL_OPXD_OpxddiagRiscvInit(tyCurrentDeviceHandle,0, ulDataArrayComplete) != DRVOPXD_ERROR_NO_ERROR)
	{
		PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVINIT");
		return;
	}
	else
	{
		ptyRiscvParam.ulAbits = ulDataArrayComplete[0];
		switch (ulDataArrayComplete[2])
		{
		case 0:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVINIT_ACT, ulDataArrayComplete[1]);
			break;
		case 1:
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVINIT_ACT_2, ulDataArrayComplete[1]);
			break;
		default :
			PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVINIT_ACT_3, ulDataArrayComplete[1]);
			break;
		}	
	}
	return;
}

/****************************************************************************
	 Function: JCMD_RiscvInitHelp
	 Engineer: Chandrasekar B R
		Input: none
	   Output: none
  Description: show help for riscvinit command
Date           Initials    Description
04-Jun-2019    CBR         Initial
****************************************************************************/
void JCMD_RiscvInitHelp(void)
{
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVINIT_HELP);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVINIT_HELP_2);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVINIT_HELP_3);
}

/****************************************************************************
	 Function: JCMD_RiscvHartHalt
	 Engineer: Chandrasekar B R
		Input: char *pszParams          : params related to the command
	   Output: none
  Description: Halt the selected hart
Date           Initials    Description
05-Jun-19	   CBR			  Initial
****************************************************************************/
void JCMD_RiscvHartHalt(char* pszParams)
{
	uint32_t uIndex = 0;
	unsigned long ulHart = 0;
	unsigned long ulError = 0;
	char* cpPtr = NULL;

	assert(pszParams != NULL);

	if (ptyRiscvParam.ulArchType == RISCV_INVALID_ARCH)
	{
		PrintMessage(INFO_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVHALT ,execute RVINIT first");
		return;
	}

	if (sscanf(pszParams, "%x %x", &ulHart, &uIndex) != 1)
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	ulError = DL_OPXD_RISCV_HaltHart(tyCurrentDeviceHandle, 0, ulHart, ptyRiscvParam.ulAbits);
	if (ulError != DRVOPXD_ERROR_NO_ERROR)
	{
		PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
		return;
	}
	else
	{
		PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVHARTHALT_ACT, ulHart);
	}
}

/****************************************************************************
	 Function: JCMD_RiscvHartHaltHelp
	 Engineer: Chandrasekar B R
		Input: none
	   Output: none
  Description: show help for riscvharthalt command
Date           Initials    Description
06-Jun-2019    CBR         Initial
****************************************************************************/
void JCMD_RiscvHartHaltHelp(void)
{
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVHARTHALT_Help);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVHARTHALT_Help_2);
}

/****************************************************************************
	 Function: JCMD_RiscvRD
	 Engineer: Chandrasekar B R
		Input: char *pszParams          : params related to the command
	   Output: none
  Description: To read registers such as GPRS, Floating point, CSRs and Memory Location
Date           Initials    Description
06-June-19	   CBR			  Initial
****************************************************************************/
void JCMD_RiscvRD(char* pszParams)
{
	uint32_t uExtra = 0;
	unsigned long ulAddress = 0, ulSizeOrDoublePre = 0,ulMech = 0, ulAddressUser = 0;
	unsigned long ulError = 0;
	unsigned char ucType = 0;
	uint32_t ulResult[4] = { 0 };
	unsigned long ulConfigPara[4] = { 0 };
	uint32_t utype = 1;

	assert(pszParams != NULL);

	if (ptyRiscvParam.ulArchType == RISCV_INVALID_ARCH)
	{
		PrintMessage(INFO_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVRD ,execute RVINIT first");
		return;
	}

	if (sscanf(pszParams, "%c %x", &ucType, &ulAddressUser) != 2)
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	if (!Parse_RiscvRD_Params(pszParams, ptyRiscvParam.ulArchType, &ulAddress, &ulMech, &ulSizeOrDoublePre))
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	switch (ucType)
		{
		case 'r':
		case 'R':
			//calling read register for GPRs
			ulError = DL_OPXD_RISCV_ReadARegister(tyCurrentDeviceHandle, 0, 0, ulResult, ulAddress, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
			if(ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
				return;
			}
			break;
		case 'f':
		case 'F':
			ulConfigPara[0] = ulMech;
			ulConfigPara[1] = ulSizeOrDoublePre;
			//initialization of struct to discovery
			ulError = DL_OPXD_ConfigureOpxddiag(tyCurrentDeviceHandle, ulConfigPara);
			if (ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVRD");
				return;
			}

			ulError = DL_OPXD_RISCV_ReadARegister(tyCurrentDeviceHandle, 0, 0, ulResult, ulAddress, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
			if (ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
				return;
			}
			break;
		case 'c':
		case 'C':
			ulConfigPara[0] = ulMech;
			//initialization of struct to discovery
			ulError = DL_OPXD_ConfigureOpxddiag(tyCurrentDeviceHandle, ulConfigPara);
			if (ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVRD");
				return;
			}
			ulError = DL_OPXD_RISCV_ReadARegister(tyCurrentDeviceHandle, 0, 0, ulResult, ulAddress, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
			if (ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
				return;
			}
			break;
		case 'm':
		case 'M':
			ulConfigPara[3] = ulMech;
			//initialization of struct to discovery
			ulError = DL_OPXD_ConfigureOpxddiag(tyCurrentDeviceHandle, ulConfigPara);
			if (ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVRD");
				return;
			}
			ulError = DL_OPXD_RISCV_ReadBlock(tyCurrentDeviceHandle, 0, 0, ulSizeOrDoublePre, 1, ulAddress, (unsigned char*)ulResult, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
			if (ulError != DRVOPXD_ERROR_NO_ERROR)
			{
				PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
				return;
			}
			//for print message read memory
			utype = 3;		
			break;
		default:
			PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
			return;
		}	
	if (!Riscv_RD_WR_Print(ptyRiscvParam.ulArchType, ulAddressUser, ulResult, utype))
	{
		PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVRD");
		return;
	}
}

/****************************************************************************
	 Function: JCMD_RiscvRDHelp
	 Engineer: Chandrasekar B R
		Input: none
	   Output: none
  Description: show help for riscvrd command
Date           Initials    Description
12-Jun-2019    CBR         Initial
****************************************************************************/
void JCMD_RiscvRDHelp(void)
{
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_2);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_3);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_4);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_5);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_6);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_7);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_8);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_9);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_10);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_11);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_12);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_13);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_14);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_15);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_16);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_17);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_18);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_19);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_20);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_21);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_22);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_23);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_24);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVRD_HELP_25);
}

/****************************************************************************
	 Function: JCMD_RiscvWR
	 Engineer: Chandrasekar B R
		Input: char *pszParams          : params related to the command
	   Output: none
  Description: Write to registers such as GPRS, Floating point, CSRs and Memory Location
Date           Initials    Description
13-June-19	   CBR			  Initial
****************************************************************************/
void JCMD_RiscvWR(char* pszParams)
{
	uint32_t uExtra = 0;
	unsigned long ulAddress = 0, ulSizeOrDoublePre = 0, ulAddressUser = 0, ulMech = 0;
	unsigned long ulError = 0;
	unsigned char ucType = 0;
	unsigned long ulConfigPara[4] = { 0 };
	uint32_t ulData[4] = { 0 };
	uint32_t utype = 2;

	assert(pszParams != NULL);
	if (ptyRiscvParam.ulArchType == RISCV_INVALID_ARCH)
	{
		PrintMessage(INFO_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVWR ,execute RVINIT first");
		return;
	}
	
	if (sscanf(pszParams, "%c %x", &ucType, &ulAddressUser) != 2)
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}

	if (!Parse_RiscvWR_Params(pszParams, ptyRiscvParam.ulArchType, &ulAddress, &ulMech, &ulSizeOrDoublePre, ulData))
	{
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}
	
	switch (ucType)
	{
	case 'r':
	case 'R':			
		ulError = DL_OPXD_RISCV_WriteARegister(tyCurrentDeviceHandle, 0, 0, ulData, ulAddress, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
			return;
		}
		break;
	case 'f':
	case 'F':
		ulConfigPara[0] = ulMech;
		ulConfigPara[1] = ulSizeOrDoublePre;
		//initialization of struct to discovery
		ulError = DL_OPXD_ConfigureOpxddiag(tyCurrentDeviceHandle, ulConfigPara);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVWR");
			return;
		}
		ulError = DL_OPXD_RISCV_WriteARegister(tyCurrentDeviceHandle, 0, 0, ulData, ulAddress, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
			return;
		}
		break;
	case 'c':
	case 'C':
		ulConfigPara[0] = ulMech;
		//initialization of struct to discovery
		ulError = DL_OPXD_ConfigureOpxddiag(tyCurrentDeviceHandle, ulConfigPara);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVWR");
			return;
		}
		ulError = DL_OPXD_RISCV_WriteCSR(tyCurrentDeviceHandle, 0, 0, ulData, ulAddress, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
			return;
		}
		break;
	case 'm':
	case 'M':
		ulConfigPara[3] = ulMech;
		//initialization of struct to discovery
		ulError = DL_OPXD_ConfigureOpxddiag(tyCurrentDeviceHandle, ulConfigPara);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVWR");
			return;
		}
		ulError = DL_OPXD_RISCV_WriteBlock(tyCurrentDeviceHandle, 0, 0, ulSizeOrDoublePre, 1, ulAddress, (unsigned char*)ulData, ptyRiscvParam.ulArchType, ptyRiscvParam.ulAbits);
		if (ulError != DRVOPXD_ERROR_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, ConvertDwErrToOpxdlayerErr(ulError));
			return;
		}
		//for print message write memory
		utype = 4;
		break;
	default:
		PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
		return;
	}
	if (!Riscv_RD_WR_Print(ptyRiscvParam.ulArchType, ulAddressUser, ulData, utype))
	{
		PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RVWR");
		return;
	}
}

/****************************************************************************
	 Function: JCMD_RiscvWRHelp
	 Engineer: Chandrasekar B R
		Input: none
	   Output: none
  Description: show help for riscvwr command
Date           Initials    Description
14-June-2019    CBR         Initial
****************************************************************************/
void JCMD_RiscvWRHelp(void)
{
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_2);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_3);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_4);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_5);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_6);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_7);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_8);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_9);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_10);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_11);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_12);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_13);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_14);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_15);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_16);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_17);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_18);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_19);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_20);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_21);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_22);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_23);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_24);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_25);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_26);
	PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RISCVWR_HELP_27);
}
#endif
/****************************************************************************
     Function: GetCommandFromFile
     Engineer: Vitezslav Hola
        Input: TyCmdFiles *ptyCFiles    : pointer to structure with command files
               char *pszCommand         : output buffer for next command
               uint32_t uiBufferLen : length of output buffer in bytes
       Output: unsigned char            : true, if there is any new command
  Description: check structure for open files and read a new command
Date           Initials    Description
06-Mar-2006    VH          Initial
****************************************************************************/
unsigned char GetCommandFromFile(TyCmdFiles *ptyCFiles, char *pszCommand, uint32_t uiBufferLen)
{
   FILE **pFile;
   uint32_t uiIndex;

   assert(ptyCFiles != NULL);
   assert(pszCommand != NULL);
   assert(uiBufferLen > 0);

   strcpy(pszCommand, "");

   // get the most nested open file
   pFile = NULL;
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      if(ptyCFiles->pCFile[uiIndex] != NULL)
         {
         pFile = &(ptyCFiles->pCFile[uiIndex]);
         ptyCFiles->uiCurFileIndex = uiIndex;            // save actual file index + 1
         }
      else
         break;
      }
   if(pFile == NULL)
      return FALSE;					// no opened command file
   // read from file
   if(fgets(pszCommand, (int32_t)uiBufferLen, *pFile) == NULL)
      {   // test for end-of-file
      if(feof(*pFile))
         {
         // last reading
         fclose(*pFile);
         *pFile = NULL;
         return TRUE;
         }
      else
         {
         // error while reading file
         fclose(*pFile);
         *pFile = NULL;
         return FALSE;
         }
      }
	
   // we have read some command
   return TRUE;
}

/****************************************************************************
     Function: FilterInputString
     Engineer: Vitezslav Hola
        Input: char *pszInputString : pointer to input string
       Output: none
  Description: remove special characters ('\r', etc.) from the string,
               remove duplicated spaces
Date           Initials    Description
18-Jan-2006    VH          Initial
****************************************************************************/
void FilterInputString(char *pszInputString)
{
    char *pszString;
    unsigned char bNextSpace;
    uint32_t uiLastChar;

    assert(pszInputString != NULL);

    // check for special characters and duplicate space
    pszString = pszInputString;
    bNextSpace = TRUE;          // remove spaces on the begining
    while(*pszString)
        {
        switch(*pszString)
            {
            case ' '  :
                if(bNextSpace)
                    {
                    // remove (*pszString) from pszInputString
                    memmove((void *)pszString, (void *)(pszString+1), strlen(pszString));
                    // do not move pszString
                    }
                else
                    {
                    bNextSpace = TRUE;
                    pszString++;
                    }
                break;

            case '\r' :
            case '\n' :
            case '\t' :
            case '\b' :
                // remove (*pszString) from pszInputString
				memmove((void *)pszString, (void *)(pszString+1), strlen(pszString));
                // do not move pszString
                break;

            default:
                pszString++;                // next character
                bNextSpace = FALSE;         // correct character, next space is valid
                break;
            }
        }

    // check for space at the end of the string
    uiLastChar = (uint32_t)strlen(pszInputString);
    if((uiLastChar > 0) && (pszInputString[uiLastChar - 1] == ' '))
        pszInputString[uiLastChar - 1] = '\0';
}

/****************************************************************************
     Function: RunJTAGConsole
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDeviceHandle - handle to Opella-XD
       Output: none
  Description: execute JTAG console
Date           Initials    Description
18-Jan-2006    VH          Initial
****************************************************************************/
void RunJTAGConsole(TyDevHandle tyDeviceHandle)
{
   char szCommandLine[MAX_INPUT_LINE];
   unsigned char bFinishConsole, bCommandFromFile;
   TyCmdFiles tyCmdFiles;
   uint32_t uiIndex;

   tyCurrentDeviceHandle = tyDeviceHandle;

   // show first message
   PrintMessage(INFO_MESSAGE, JMSG_CONSOLE_INFO);

   // initialize command files structure
   tyCmdFiles.uiCurFileIndex = 0;
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      tyCmdFiles.pCFile[uiIndex] = NULL;
      strcpy(tyCmdFiles.pszCFileNames[uiIndex], "");
      }

   // main console loop
   bFinishConsole = FALSE;
   bCommandFromFile = FALSE;
   while(!bFinishConsole)
      {
      // when getting commands from command file
      // command is already in pszCommandLine
      if(!bCommandFromFile)
         {
         // show command prompt (we cannot use PrintMessage because we need stay on the same line)
         printf(CONSOLE_COMMAND_PROMPT);
			
         // read user input
         if(fgets(szCommandLine, MAX_INPUT_LINE, stdin) == NULL)
            {		// check for errors
            if(!feof(stdin))
               {
               // show error message
               bFinishConsole = TRUE;
               continue;
               }
            else
               strcpy(szCommandLine, "");
            }

         // log command line if requested
//            if (tySI.pDebugFile != NULL)
//            {
//            fprintf(tySI.pDebugFile, CONSOLE_COMMAND_PROMPT);
//            fprintf(tySI.pDebugFile, szCommandLine);
//            }
         }

      // filter and convert command line
      FilterInputString(szCommandLine);
      // show command from command file (not empty commands)
      if(bCommandFromFile && strcmp(szCommandLine, ""))
         {
         // print command to the command prompt
         PrintMessage(INFO_MESSAGE, "%s%s>%s", CFILE_COMMAND_PROMPT, 
                      tyCmdFiles.pszCFileNames[tyCmdFiles.uiCurFileIndex], 
                      szCommandLine);
         }
      // pass command line to parser
      DoConsoleCommand(szCommandLine, &tyCmdFiles, &bFinishConsole);
      // check for request for commands from files
      if(!bFinishConsole)
         bCommandFromFile = GetCommandFromFile(&tyCmdFiles, szCommandLine, MAX_INPUT_LINE);
      }
   // some files could be still open
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      if(tyCmdFiles.pCFile[uiIndex] != NULL)
         fclose(tyCmdFiles.pCFile[uiIndex]);
      }
}

//***************************************************************************
//*** Test functions for ThreadArch *****************************************
//***************************************************************************
#ifdef REDPINE_CMDS
/****************************************************************************
     Function: JCMD_RPHOLDExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute RPHOLD command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPHOLDExecute(char *pszParams)
{
   assert(pszParams != NULL);

   if (DL_OPXD_ThreadArch_HoldCore(tyCurrentDeviceHandle, 0, 1) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RPHOLD");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPHOLD_ACT);
      }
}

/****************************************************************************
     Function: JCMD_RPHOLDHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for RPHOLD command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPHOLDHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPHOLD_HELP);
}

/****************************************************************************
     Function: JCMD_RPRELExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute RPREL command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPRELExecute(char *pszParams)
{
   assert(pszParams != NULL);
   if (DL_OPXD_ThreadArch_ReleaseCore(tyCurrentDeviceHandle, 0, 1) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RPREL");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPREL_ACT);
      }
}

/****************************************************************************
     Function: JCMD_RPRELHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for RPREL command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPRELHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPREL_HELP);
}

/****************************************************************************
     Function: JCMD_RPSSExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute RPSS command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPSSExecute(char *pszParams)
{
   assert(pszParams != NULL);
   if (DL_OPXD_ThreadArch_SingleStep(tyCurrentDeviceHandle, 0, 1) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RPSS");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPSS_ACT);
      }
}

/****************************************************************************
     Function: JCMD_RPSSHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for RPSS command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPSSHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPSS_HELP);
}

/****************************************************************************
     Function: JCMD_RPRDExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute RPRD command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPRDExecute(char *pszParams)
{
   uint32_t ulAddress = 0;
   unsigned short usResult = 0;

   assert(pszParams != NULL);
   if (sscanf(pszParams, "%x", &ulAddress) != 1)
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   if (DL_OPXD_ThreadArch_ReadLocation(tyCurrentDeviceHandle, 0, ulAddress, &usResult, 0) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RPRD");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPRD_ACT, usResult, ulAddress);
      }
}

/****************************************************************************
     Function: JCMD_RPRDHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for RPRD command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPRDHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPRW_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPRW_HELP_2);
}

/****************************************************************************
     Function: JCMD_RPWRExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute RPWR command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPWRExecute(char *pszParams)
{
   assert(pszParams != NULL);

   uint32_t ulAddress, ulValue;
   unsigned short usValue = 0;


   assert(pszParams != NULL);
   if ((sscanf(pszParams, "%x %x", &ulAddress, &ulValue) != 2) || (ulValue > 0xFFFF))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   usValue = (unsigned short)(ulValue & 0xFFFF);

   if (DL_OPXD_ThreadArch_WriteLocation(tyCurrentDeviceHandle, 0, ulAddress, &usValue, 0) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "RPWR");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPWR_ACT, usValue, ulAddress);
      }
}

/****************************************************************************
     Function: JCMD_RPWRHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for RPWR command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_RPWRHelp(void)
{
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPWR_HELP);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPWR_HELP_2);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RPWR_HELP_3);
}

#endif // REDPINE_CMDS

//***************************************************************************
//*** Test functions for ARC ************************************************
//***************************************************************************
#ifdef ARC_CMDS

/****************************************************************************
     Function: JCMD_ARCRESExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute ARCRES command
Date           Initials    Description
27-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCRESExecute(char *pszParams)
{
   assert(pszParams != NULL);

   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRES_ACT);
   // assert reset
   if (DL_OPXD_Arc_ResetProc(tyCurrentDeviceHandle, 1, 0) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCRES");
      return;
      }
   S_SLEEP(1);
   // deassert reset
   if (DL_OPXD_Arc_ResetProc(tyCurrentDeviceHandle, 0, 0) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCRES");
      return;
      }
}

/****************************************************************************
     Function: JCMD_ARCRESHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for ARCRES command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCRESHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRES_HELP);
}

/****************************************************************************
     Function: JCMD_ARCPINExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute ARCPIN command
Date           Initials    Description
27-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCPINExecute(char *pszParams)
{
   uint32_t uiInput = 0;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   TyARCangelMode tyPinStatus;
   assert(pszParams != NULL);

   if (!strcmp(pszParams, ""))
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, NULL, &tyPinStatus, 0);        // just show status
   else if (sscanf(pszParams, "%x", &uiInput) == 1)
      {
      TyARCangelMode tyPinSet;
      tyPinSet.bSS0 = (uiInput & 0x1) ? 0x1 : 0x0;
      tyPinSet.bSS1 = (uiInput & 0x2) ? 0x1 : 0x0;
      tyPinSet.bCNT = (uiInput & 0x4) ? 0x1 : 0x0;
      tyPinSet.bOP = 0;
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, &tyPinSet, &tyPinStatus, 0);
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCPIN");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCPIN_ACT, tyPinStatus.bSS0, tyPinStatus.bSS1, tyPinStatus.bCNT, tyPinStatus.bOP);
      }
}

/****************************************************************************
     Function: JCMD_ARCPINHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for ARCPIN command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCPINHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCPIN_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCPIN_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCPIN_HELP_3);
}

/****************************************************************************
     Function: JCMD_ARCEXTCMDExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute ARCEXTCMD command
Date           Initials    Description
27-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCEXTCMDExecute(char *pszParams)
{
   uint32_t uiCommand = 0;
   uint32_t uiParam = 0;
   TyARCangelExtcmd tyCmd;
   assert(pszParams != NULL);

   if ((sscanf(pszParams, "%x %x", &uiCommand, &uiParam) != 2) || 
       (uiCommand > 0xFF) || (uiParam > 0xFFFF))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   tyCmd.usResponse = 0;
   tyCmd.ucCommandCode = (unsigned char)(uiCommand & 0xFF);
   tyCmd.pulParameter[0] = (uint32_t)(uiParam & 0xFFFF);
   tyCmd.pulParameter[1] = 0;

   if (DL_OPXD_Arc_AAExtendedCommand(tyCurrentDeviceHandle, &tyCmd) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCEXTCMD");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCEXTCMD_ACT, tyCmd.usResponse);
      }
}

/****************************************************************************
     Function: JCMD_ARCEXTCMDHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for ARCEXTCMD command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCEXTCMDHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCEXTCMD_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCEXTCMD_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCEXTCMD_HELP_3);
}

/****************************************************************************
     Function: JCMD_ARCBLASTExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute ARCBLAST command
Date           Initials    Description
27-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCBLASTExecute(char *pszParams)
{
   TyARCangelMode tyAAPins;
   uint32_t ulProgressLength, ulBytesRead;
   FILE *fp = NULL;
   uint32_t ulBlastingOk = 1;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   assert(pszParams != NULL);

   // try blasting ARCangel FPGA
   fp = fopen(pszParams, "rb");
   if (fp == NULL)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_ARCBLAST_FILE_ERR);
      return;
      }
   PrintMessage(HELP_MESSAGE, JMSG_COMMAND_ARCBLAST_ACT);
   // prepare for blasting (enable D0 and D1)
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 1; tyAAPins.bCNT = 0;
   tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, &tyAAPins, NULL, 1);
   tyAAPins.bSS0 = 0;   tyAAPins.bSS1 = 0; tyAAPins.bCNT = 0;
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, &tyAAPins, NULL, 1);
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 0; tyAAPins.bCNT = 0;
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, &tyAAPins, NULL, 1);
   // check OP
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, NULL, &tyAAPins,  1);
      if (tyAAPins.bOP)
         ulBlastingOk = 0;
      }
   // now we must wait 200 ms
   S_SLEEP(200);
   // start blasting
   ulProgressLength = 0;
   // read data from file and blast them into FPGA
   while ((ulBytesRead = fread(pucARCangelBlast, 1, ARCANGEL_BLAST_SIZE, fp)) != 0)
      {
      ulProgressLength += ulBytesRead;
      if (ulProgressLength > (512*1024))
         {
         PrintMessage(HELP_MESSAGE, ".");
         ulProgressLength = 0;
         }
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         tyResult = DL_OPXD_Arc_AABlast(tyCurrentDeviceHandle, ulBytesRead*8, pucARCangelBlast);
      }
   // finished, so check OP
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, NULL, &tyAAPins,  1);
      if (!tyAAPins.bOP)
         ulBlastingOk = 0;
      }
   // return to normal mode and disable blasting
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 1; tyAAPins.bCNT = 0;
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      tyResult = DL_OPXD_Arc_AAMode(tyCurrentDeviceHandle, &tyAAPins, NULL, 0);
   PrintMessage(HELP_MESSAGE, "\n");
   if (fp != NULL)
      fclose(fp);
   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCBLAST");
      return;
      }
   
   if (!ulBlastingOk)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_ARCBLAST_FPGA_ERR);
      return;
      }
}

/****************************************************************************
     Function: JCMD_ARCBLASTHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for ARCBLAST command
Date           Initials    Description
21-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCBLASTHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCBLAST_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCBLAST_HELP_2);
}

/****************************************************************************
     Function: JCMD_ARCRDExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute ARCRD command
Date           Initials    Description
28-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCRDExecute(char *pszParams)
{
   unsigned char ucType = 0;
   uint32_t uiAddress = 0;
   uint32_t ulResult = 0;

   assert(pszParams != NULL);
   if (sscanf(pszParams, "%c %x", &ucType, &uiAddress) != 2)
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   else
      {
      switch (ucType)
         {
         case 'a':
         case 'A':
            //ucType = ARC_ACCESS_AUXREG;
            break;
         case 'c':
         case 'C':
            //ucType = ARC_ACCESS_COREREG;
            break;
         case 'm':
         case 'M':
            //ucType = ARC_ACCESS_MEMORY;
            break;
         default:
            PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
            return;
         }
      }

   if (DL_OPXD_Arc_ReadBlock(tyCurrentDeviceHandle, 0, ucType, (uint32_t)uiAddress, 1, &ulResult) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCRD");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRD_ACT, ulResult, uiAddress);
      }
}

/****************************************************************************
     Function: JCMD_ARCRDHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for ARCRD command
Date           Initials    Description
28-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCRDHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRD_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRD_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRD_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCRD_HELP_4);
}

/****************************************************************************
     Function: JCMD_ARCWRExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute ARCWR command
Date           Initials    Description
28-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCWRExecute(char *pszParams)
{
   unsigned char ucType = 0;
   uint32_t uiAddress = 0;
   uint32_t uiValue = 0;

   assert(pszParams != NULL);
   if (sscanf(pszParams, "%c %x %x", &ucType, &uiAddress, &uiValue) != 3)
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   else
      {
      switch (ucType)
         {
         case 'a':
         case 'A':
            //ucType = ARC_ACCESS_AUXREG;
            break;
         case 'c':
         case 'C':
            //ucType = ARC_ACCESS_COREREG;
            break;
         case 'm':
         case 'M':
            //ucType = ARC_ACCESS_MEMORY;
            break;
         default:
            PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
            return;
         }
      }

   if (DL_OPXD_Arc_WriteBlock(tyCurrentDeviceHandle, 0, ucType, (uint32_t)uiAddress, 1, (uint32_t *)(&uiValue)) != DRVOPXD_ERROR_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_CANNOT_EXECUTE_CMD, "ARCWR");
      return;
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCWR_ACT, uiValue, uiAddress);
      }
}

/****************************************************************************
     Function: JCMD_ARCWRHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for ARCWR command
Date           Initials    Description
28-Sep-2007    VH          Initial
****************************************************************************/
void JCMD_ARCWRHelp(void)
{
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCWR_HELP);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCWR_HELP_2);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCWR_HELP_3);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCWR_HELP_4);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARCWR_HELP_5);
}
#endif // ARC_CMDS

//***************************************************************************
//*** Test functions for ARM ************************************************
//***************************************************************************
#ifdef ARM_CMDS
#if defined (DEBUG) || defined (_DEBUG)
/****************************************************************************
     Function: JCMD_ArmSHIRDRExecute
     Engineer: Shameerudheen
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute IR first then DR with out going to RUN Test/IDLE state in between
Date           Initials    Description
29-Jun-2007    SPT          Initial
****************************************************************************/
void JCMD_ArmSHIRDRExecute(char *pszParams)
{

   #define CORE 0
   unsigned char pszUlongString[8+SCANCHAIN_ULONG_SIZE*8];
   uint32_t pulOutDRBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t pulInDRBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutIRBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t ulNumberOfBitsIR, ulNumberOfBitsDR;
   uint32_t pulIRIntest[1] = {0xC};
   TyError tyResult;
   
   uint32_t uiUlongsToShow;
   TyTmsSequence tyTms_IR_IR;
   TyTmsSequence tyTms_IRDR_DR;

   assert(pszParams != NULL);

   // clear both buffers (fill with zero's
   memset((void*)pulOutDRBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
   memset((void*)pulInDRBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
   memset((void*)pulOutIRBuffer, 0xff, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
   // check parameters
   StrToUpr(pszParams);
   if (!Parse_ArmSHIRDR_Params(pszParams, pulOutIRBuffer, SCANCHAIN_ULONG_SIZE, pulOutDRBuffer, SCANCHAIN_ULONG_SIZE, &ulNumberOfBitsIR,&ulNumberOfBitsDR))
   {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
   }
   
   // check number of bits to shift IR
   if((ulNumberOfBitsIR < 1) || (ulNumberOfBitsIR > MAX_SCANCHAIN_LENGTH) )
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_ELEN, MAX_SCANCHAIN_LENGTH);
      return;
      }
   // check number of bits to shift DR
   if((ulNumberOfBitsDR < 1) || (ulNumberOfBitsDR > MAX_SCANCHAIN_LENGTH) )
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_ELEN, MAX_SCANCHAIN_LENGTH);
      return;
      }
   // set TMS IR sequence ( pre 00110, post 0001)
   // set TMS IR sequence ( pre 00111, post 0011)
   //tyTms_IRDR_IR.usPostTmsBits = 0x1;
   //tyTms_IRDR_IR.ucPostTmsCount = 0x4;
   //tyTms_IRDR_IR.usPreTmsBits = 0x6;
   //tyTms_IRDR_IR.ucPreTmsCount = 0x5;

  // tyTms_IRDR_DR.usPostTmsBits = 0x3;
  // tyTms_IRDR_DR.ucPostTmsCount = 0x4;
  // tyTms_IRDR_DR.usPreTmsBits = 0x7;
  // tyTms_IRDR_DR.ucPreTmsCount = 0x5;
  // 4, 0x02, 4, 0x01
   tyTms_IRDR_DR.usPostTmsBits = 0x01;
   tyTms_IRDR_DR.ucPostTmsCount = 0x4;
   tyTms_IRDR_DR.usPreTmsBits = 0x2;
   tyTms_IRDR_DR.ucPreTmsCount = 0x04;

   //6, 0x0F, 4, 0x03
   tyTms_IR_IR.usPostTmsBits = 0x03;
   tyTms_IR_IR.ucPostTmsCount = 0x4;
   tyTms_IR_IR.usPreTmsBits = 0x0F;
   tyTms_IR_IR.ucPreTmsCount = 0x06;

   if ((DL_OPXD_JtagSetTMS(tyCurrentDeviceHandle, NULL, NULL, NULL, &tyTms_IRDR_DR) != DRVOPXD_ERROR_NO_ERROR))   {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TMS_ERROR);
      return;
   }

   tyResult = DL_OPXD_JtagScanIRandDR(tyCurrentDeviceHandle, CORE, ulNumberOfBitsIR, pulOutIRBuffer,ulNumberOfBitsDR, pulOutDRBuffer,pulInDRBuffer);
   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
   {
      if (tyResult == DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT)
         PrintMessage(ERROR_MESSAGE, "Adaptive Clock Timeout");

      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_SHIRDR_ERROR);
      // set TMS sequence (just use default sequences)
      if ((DL_OPXD_JtagSetTMS(tyCurrentDeviceHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
      {
          PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TMS_ERROR);
          return;
      }
      return;
   }

   (void) DL_OPXD_JtagSetTMS(tyCurrentDeviceHandle, &tyTms_IR_IR, NULL, NULL , NULL);
   
   tyResult = DL_OPXD_JtagScanIR(tyCurrentDeviceHandle, CORE,4,pulIRIntest,NULL);
   

   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
   {
      if (tyResult == DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT)
         PrintMessage(ERROR_MESSAGE, "Adaptive Clock Timeout");

      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_SHIRDR_ERROR);
      // set TMS sequence (just use default sequences)
      if ((DL_OPXD_JtagSetTMS(tyCurrentDeviceHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
      {
          PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TMS_ERROR);
          return;
      }
      return;
   }
   else
   {
       // set TMS sequence (just use default sequences)
       if ((DL_OPXD_JtagSetTMS(tyCurrentDeviceHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
       {
          PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TMS_ERROR);
          return;
       }
      // how many ulongs to show ?
      uiUlongsToShow = ulNumberOfBitsDR / 32;
      if(ulNumberOfBitsDR % 32)
         uiUlongsToShow++;
      (void) PrintUlongArray((char *)pszUlongString, 8+SCANCHAIN_ULONG_SIZE*8, pulInDRBuffer, uiUlongsToShow);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_RESULT, pszUlongString);
      
      // update last used value
      uiLastDRLength = ulNumberOfBitsIR;
      uiLastDRLength = ulNumberOfBitsDR;
   }
}

/****************************************************************************
     Function: JCMD_ArmRRegExecute
     Engineer: Shameerudheen PT.
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute Arm Read Register function
Date           Initials    Description
27-Jul-2007    SPT          Initial
****************************************************************************/
void JCMD_ArmRRegExecute(char *pszParams)
{
   TyArm9Registors tyArm9Registors;

   assert(pszParams != NULL);
   StrToUpr(pszParams);

   if ((DL_OPXD_Arm_RReg(tyCurrentDeviceHandle, &tyArm9Registors, pszParams) != DRVOPXD_ERROR_NO_ERROR))
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_ARMRREG_ERROR);
      return;
      }
   
   
   return;
}

/****************************************************************************
     Function: JCMD_ArmTestExecute
     Engineer: Shameerudheen PT.
        Input: char *pszParams          : params related to the command
       Output: none
  Description: Testing all arm core, Arm core is a parameter to this function
Date           Initials    Description
05-OCT-2007    SPT          Initial
****************************************************************************/
void JCMD_ArmTestExecute(char *pszParams)
{
   assert(pszParams != NULL);
   StrToUpr(pszParams);
   if ((DL_OPXD_Arm_TEST(tyCurrentDeviceHandle, pszParams) != DRVOPXD_ERROR_NO_ERROR))
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_ARMTEST_ERROR);
      return;
      }
   return;
}

/****************************************************************************
     Function: JCMD_ARMSHIRDRHelp
     Engineer: Shameerudheen PT
        Input: none
       Output: none
  Description: show help for ARMSHIRDRHelp command
Date           Initials    Description
29-Jun-2007    SPT          Initial
****************************************************************************/
void JCMD_ArmSHIRDRHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ArmSHIRDR_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ArmSHIRDR_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ArmSHIRDR_HELP_3);
}

/****************************************************************************
     Function: JCMD_ArmRRegHelp
     Engineer: Shameerudheen PT
        Input: none
       Output: none
  Description: show help for Arm read register command. this is an internal 
               command for testing no need to display in help
Date           Initials    Description
27-Jul-2007    SPT          Initial
****************************************************************************/
void JCMD_ArmRRegHelp(void)
{
    //PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ArmRReg_HELP);
    //PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ArmRReg_HELP_2);
}

/****************************************************************************
     Function: JCMD_ArmRTestHelp
     Engineer: Shameerudheen PT
        Input: none
       Output: none
  Description: show help for Arm Test command
Date           Initials    Description
05-Oct-2007    SPT          Initial
****************************************************************************/
void JCMD_ArmTestHelp(void)
{
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_2);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_3);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_4);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_5);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_6);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_7);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_8);
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_ARMTEST_HELP_9);
}
#endif //Debug build
#endif // ARM_CMDS

