/****************************************************************************
       Module: jconmsg.h
     Engineer: Vitezslav Hola
  Description: Header for JTAG console for Opella-XD,
               JTAG console output strings and error messages 

Date           Initials    Description
17-Jan-2007    VH          initial
09-Mar-2012    SPT         added INITTAP7 command message
****************************************************************************/
#ifndef _JCONMSG_H_
#define _JCONMSG_H_

// minimum buffer length in bytes, every message must be shorter than this length
#define MSG_MIN_BUFFER_LEN          256      

// JTAG console output strings
#define JMSG_CONSOLE_INFO           "Entering JTAG low-level console mode (type \"help\" for supported commands)."
#define JMSG_UKNOWN_COMMAND         "Undefined command: \"%s\". Try \"help\"."
#define JMSG_WRONG_PARAMS           "Invalid command parameters. Type \"help\" for more information."
#define JMSG_WRONG_TPA_PARAMS       "Invalid TPA parameters. Type \"tpa\" for more information."
#define JMSG_CANNOT_EXECUTE_CMD     "Cannot execute %s command."
#define JMSG_ADAPTIVE_CLOCK_TIMEOUT "Adaptive clock does not work properly. Please check RTCK signal."

// Header for listing of supported commands
// functions JCMD_xxxHelp are responsible for dysplaying all lines
#define JMSG_COMMAND_HELP_1           "Supported commands are:"
// help descriptions
#define JMSG_COMMAND_HELP_HELP        "help                   Display supported commands."
#define JMSG_COMMAND_QUIT_HELP        "quit                   Exit program."
#define JMSG_COMMAND_SHIR_HELP        "shir <val> <len>       Shift value into IR and show value shifted out."
#define JMSG_COMMAND_SHIR_HELP_2      "                       <val> specifies value in hex format."
#define JMSG_COMMAND_SHIR_HELP_3      "                       <len> number of bits to shift (default is 5 or "
#define JMSG_COMMAND_SHIR_HELP_4      "                             previous value)."
#define JMSG_COMMAND_SHDR_HELP        "shdr <val> <len>       Shift value into DR and show value shifted out."
#define JMSG_COMMAND_SHDR_HELP_2      "                       <val> specifies value in hex format."
#define JMSG_COMMAND_SHDR_HELP_3      "                       <len> number of bits to shift (default is 32 or "
#define JMSG_COMMAND_SHDR_HELP_4      "                             previous value)."
#define JMSG_COMMAND_CFILE_HELP       "cfile <filename>       Run commands from command file."
#define JMSG_COMMAND_CFILE_HELP_2     "                       <filename> specifies command file name."
#define JMSG_COMMAND_JTFREQ_HELP      "jtfreq <freq>          Set fixed frequency in MHz or adaptive JTAG clock."
#define JMSG_COMMAND_JTFREQ_HELP_2    "                       <freq> from range <0.001 to 100.000> MHz"
#define JMSG_COMMAND_JTFREQ_HELP_3    "                              or \"adaptive\" for adaptive clock."
#define JMSG_COMMAND_JTFREQ_HELP_A    "jtfreq <freq>          Set JTAG frequency in MHz."
#define JMSG_COMMAND_JTFREQ_HELP_2_A  "                       <freq> from range <0.001 to 100.000> MHz."
#define JMSG_COMMAND_TSTSC_HELP       "tstsc                  Test scan chain by shifting out random patterns."
#define JMSG_COMMAND_CHKSC_HELP       "chksc                  Shows Total IR Length,Number of TAPs and ID Codes"
#define JMSG_COMMAND_WAIT_HELP        "wait <delayms>         Wait for given time in ms."
#define JMSG_COMMAND_WAIT_HELP_2      "                       <delayms> from range <0,9999>."
#define JMSG_COMMAND_INITTAP7_HELP    "inittap7               Initialise TAP.7 controller."
#define JMSG_COMMAND_SETTMS_HELP	  "settms                 Set the JTAG TMS pattern."
#define JMSG_COMMAND_SETTMS_HELP_2    "                       <IRPRE count> <IRPRE bits> <IRPOST count> <IRPOST bits>"
#define JMSG_COMMAND_SETTMS_HELP_3	  "                       <DRPRE count> <DRPRE bits> <DRPOST count> <DRPOST bits>"
#define JMSG_COMMAND_RESET_HELP       "tapres <trst | tms>    Reset TAPs on scanchain."
#define JMSG_COMMAND_RESET_HELP_2     "                       <trst> reset TAPs by asserting TRST."
#define JMSG_COMMAND_RESET_HELP_3     "                       <tms>  reset TAPs by holding TMS high (default)."
#define JMSG_COMMAND_TPA_HELP         "tpa <value | track>    Set target voltage."
#define JMSG_COMMAND_TPA_HELP_2       "                       <value> specifies fixed voltage for target."
#define JMSG_COMMAND_TPA_HELP_3       "                       <track> probe tracks target reference voltage."
#define JMSG_COMMAND_ArmSHIRDR_HELP   "armshirdr <irval> <len> <drval> <len> Shift irvalue into IR and drval into DR."
#define JMSG_COMMAND_ArmSHIRDR_HELP_2 "                      IR & DR Scan done without going into Run Test/Idle"
#define JMSG_COMMAND_ArmSHIRDR_HELP_3 "                      state in between."
#define JMSG_COMMAND_ArmRReg_HELP     "armrreg               Read Registers from Arm9 processor."
#define JMSG_COMMAND_ArmRReg_HELP_2   "                                      "
#define JMSG_COMMAND_ARMTEST_HELP    "armtest <armcore> <core no> <on-chip memory> Test ARM Cores."
#define JMSG_COMMAND_ARMTEST_HELP_2   "                      Testing Register RD/WR, On-Chip Memory RD/WR,"
#define JMSG_COMMAND_ARMTEST_HELP_3   "                      Execute/Software Break Point, Hardware Break Point"
#define JMSG_COMMAND_ARMTEST_HELP_4   "                      <armcore> ARM7DI, ARM7TDMI, ARM7TDMI-S, ARM740T,"
#define JMSG_COMMAND_ARMTEST_HELP_5   "                      ARM920T, ARM922T, ARM940T, ARM926EJ-S, ARM946E-S,"
#define JMSG_COMMAND_ARMTEST_HELP_6   "                      ARM966E-S, ARM968EJ-S"
#define JMSG_COMMAND_ARMTEST_HELP_7   "                      <core no> 0 for 1st, 1 for 2nd core in the chain."
#define JMSG_COMMAND_ARMTEST_HELP_8   "                      <on-chip memory> Location of onchip memory in HEX"
#define JMSG_COMMAND_ARMTEST_HELP_9   "                      if no On-Chip Memory NIL"
                                                                                     
// CFILE messages
// errors
#define JMSG_COMMAND_CFILE_EOPEN    "Cannot open command file."      
#define JMSG_COMMAND_CFILE_ENESTED  "Cannot open more than %d nested files."

// SHDR/SHIR message
#define JMSG_COMMAND_SHIR_RESULT    " IR => %s"
#define JMSG_COMMAND_SHDR_RESULT    " DR => %s"
// errors
#define JMSG_COMMAND_SHIR_ERROR     "Cannot execute SHIR command."               // error message (with prefix Error: )
#define JMSG_COMMAND_SHDR_ERROR     "Cannot execute SHDR command."               // error message (with prefix Error: )
#define JMSG_COMMAND_SHIRDR_ERROR   "Cannot execute SHIRDR command."             // error message (with prefix Error: )                                                                                 
#define JMSG_COMMAND_SHIR_ELEN      "Number of bits shifted into IR must be from range <1,%d>."
#define JMSG_COMMAND_SHDR_ELEN      "Number of bits shifted into DR must be from range <1,%d>."
#define JMSG_COMMAND_TMS_ERROR      "TMS Setting Error."

#define JMSG_COMMAND_ARMRREG_ERROR  "Cannot execute Arm Read Register Command."  // error message (with prefix Error: )
#define JMSG_COMMAND_ARMTEST_ERROR     "Cannot execute ARM Test Command. Please type Help for more Deatils"   

// JTFREQ messages
#define JMSG_COMMAND_JTFREQ_CURRENT "Current JTAG frequency is %s MHz."
#define JMSG_COMMAND_JTFREQ_ADAPTIVE "Using adaptive JTAG clock."
#define JMSG_COMMAND_JTFREQ_VALUES  "Invalid frequency value, supported range is from %s to %s MHz."
#define JMSG_COMMAND_JTFREQ_SET     "JTAG frequency set to %s MHz."
#define JMSG_COMMAND_JTFREQ_FAILED  "Cannot set new JTAG frequency."              // error message (with prefix Error: )

// TSTSC messages
#define JMSG_COMMAND_TSTSC_START    "Testing scan chain by shifting out random patterns."
#define JMSG_COMMAND_TSTSC_FAILED   "Test failed - problem on scan chain."      // error message (with prefix Error: )
#define JMSG_COMMAND_TSTSC_PASSED   "Test passed - %d TAP(s) detected on scan chain."
#define JMSG_COMMAND_TSTSC_RESULT   " %d TAP(s) detected on scan chain."

// TSTSC messages
#define JMSG_COMMAND_CHKSC_START    "Testing Total IR length by Shifting Patterns."
#define JMSG_COMMAND_CHKSC_FAILED   "Failed to get total IR Length"             // error message (with prefix Error: )
#define JMSG_COMMAND_CHKSC_PASSED   " Total IR Length = %02d " 
#define JMSG_COMMAND_CHKSC_HEADER3  " TAP number    ID Code  "
#define JMSG_COMMAND_CHKSC_HEADER4  "   %02d         0x%08X  "

// TAPRES messages
#define JMSG_COMMAND_TAPRES_TRST    "Asserting JTAG TRST signal."
#define JMSG_COMMAND_TAPRES_TMS     "Moving to JTAG Test-Logic-Reset state."

// TPA messages
#define JMSG_COMMAND_TPA_TRACK      "Probe is tracking target voltage reference."
#define JMSG_COMMAND_TPA_FIXED      "Probe set fixed target voltage %3.1fV."
#define JMSG_COMMAND_TPA_OUT_RANGE  "Current TPA supports voltage range from %3.1fV to %3.1fV."
#define JMSG_COMMAND_TPA_INFO_0     "Current TPA is %s."
#define JMSG_COMMAND_TPA_INFO_NONE  "No TPA currently connected to Opella-XD."
#define JMSG_COMMAND_TPA_INFO_1     "Target reference voltage is %3.1fV."
#define JMSG_COMMAND_TPA_INFO_2     "Probe supports voltage range from %3.1fV to %3.1fV."

// WAIT messages
#define JMSG_COMMAND_WAIT_RANGE     "Delay must be from range <0,%d> ms."
#define JMSG_COMMAND_WAIT_WAITING   "Waiting for %d ms."

//INTITAP7 messages
#define JMSG_COMMAND_INITTAP7_ESC  "Reset Escape given"

//***********************************************************************
//*** Help and messages for ThreadArch test commands ********************
//***********************************************************************
// RPHOLD
#define JMSG_COMMAND_RPHOLD_HELP    "rphold                      Hold core on ThreadArch (RedPine)."
#define JMSG_COMMAND_RPHOLD_ACT     "ThreadArch core has been halted."
// RPREL
#define JMSG_COMMAND_RPREL_HELP     "rprel                       Release core on ThreadArch (RedPine)."
#define JMSG_COMMAND_RPREL_ACT      "ThreadArch core has been released."
// RPSS
#define JMSG_COMMAND_RPSS_HELP      "rpss                        Single step on ThreadArch (RedPine)."
#define JMSG_COMMAND_RPSS_ACT       "ThreadArch core executed single step."
// RPRD
#define JMSG_COMMAND_RPRD_HELP      "rprd <adr>                  Read halfword from ThreadArch core."
#define JMSG_COMMAND_RPRD_HELP_2    "                            <adr> address in hex format (32-bit)."
#define JMSG_COMMAND_RPRD_ACT       "Value 0x%04x read from address 0x%08x."
// RPWR
#define JMSG_COMMAND_RPWR_HELP      "rpwr <adr> <val>            Write halfword to ThreadArch core."
#define JMSG_COMMAND_RPWR_HELP_2    "                            <adr> address in hex format (32-bit)."
#define JMSG_COMMAND_RPWR_HELP_3    "                            <val> value in hex format (16-bit)."
#define JMSG_COMMAND_RPWR_ACT       "Value 0x%04x written to address 0x%08x."
// RPPERF
#define JMSG_COMMAND_RPPERF_HELP    "rpperf                      Run memory performance test."
#define JMSG_COMMAND_RPPERF_WR      "Writting pattern into ThreadArch memory."
#define JMSG_COMMAND_RPPERF_RD      "Reading pattern from ThreadArch memory."
#define JMSG_COMMAND_RPPERF_VER     "Verifying pattern."
#define JMSG_COMMAND_RPPERF_PERF    "ThreadArch performance is %.2fkB/s for write and %.2fkB/s for read."

//***********************************************************************
//*** Help and messages for ARC test commands ***************************
//***********************************************************************
// ARCRES messages
#define JMSG_COMMAND_ARCRES_HELP       "arcres                      Reset ARC target."
#define JMSG_COMMAND_ARCRES_ACT        "Resetting ARC target."
// ARCPIN
#define JMSG_COMMAND_ARCPIN_HELP       "arcpin <val>                Control ARCangel mode pins."
#define JMSG_COMMAND_ARCPIN_HELP_2     "                            <val> combination of ARCangel mode pins"
#define JMSG_COMMAND_ARCPIN_HELP_3     "                                  0x01=SS0, 0x02=SS1 and 0x04=CNT."
#define JMSG_COMMAND_ARCPIN_ACT        "SS0=%d, SS1=%d, CNT=%d, OP=%d."
// ARCBLAST
#define JMSG_COMMAND_ARCBLAST_HELP     "arcblast <file>             Blast XBF file into ARCangel4 FPGA."
#define JMSG_COMMAND_ARCBLAST_HELP_2   "                            <file> filename of XBF file"
#define JMSG_COMMAND_ARCBLAST_FILE_ERR "Cannot open and read specified XBF file."
#define JMSG_COMMAND_ARCBLAST_FPGA_ERR "Could not successfully blast FPGA from XBF file."
#define JMSG_COMMAND_ARCBLAST_ACT      "Blasting XBF into ARCangel FPGA "
// ARCEXTCMD
#define JMSG_COMMAND_ARCEXTCMD_HELP    "arcextcmd <cmd> <param>     Send extended command into ARCangel4"
#define JMSG_COMMAND_ARCEXTCMD_HELP_2  "                            <cmd> command code in hex format (1 byte)"
#define JMSG_COMMAND_ARCEXTCMD_HELP_3  "                            <param> parameter in hex format (2 bytes)"
#define JMSG_COMMAND_ARCEXTCMD_ACT     "Response is 0x%04x."
// ARCRD
#define JMSG_COMMAND_ARCRD_HELP        "arcrd <type> <adr>          Read halfword from ARC core."
#define JMSG_COMMAND_ARCRD_HELP_2      "                            <type> access to memory(M), core(C) or "
#define JMSG_COMMAND_ARCRD_HELP_3      "                                   aux(A) register."
#define JMSG_COMMAND_ARCRD_HELP_4      "                            <adr> address in hex format (32-bit)."
#define JMSG_COMMAND_ARCRD_ACT         "Value 0x%08x read from address/offset 0x%08x."
// ARCWR
#define JMSG_COMMAND_ARCWR_HELP        "arcwr <type> <adr> <val>    Write halfword to ARC core."
#define JMSG_COMMAND_ARCWR_HELP_2      "                            <type> access to memory(M), core(C) or "
#define JMSG_COMMAND_ARCWR_HELP_3      "                                   aux(A) register."
#define JMSG_COMMAND_ARCWR_HELP_4      "                            <adr> address in hex format (32-bit)."
#define JMSG_COMMAND_ARCWR_HELP_5      "                            <val> value in hex format (32-bit)."
#define JMSG_COMMAND_ARCWR_ACT         "Value 0x%08x written to address/offset 0x%08x."

//***********************************************************************
//*** Help and messages for RISCV test commands ***************************
//***********************************************************************
//RISCVINIT
#define JMSG_COMMAND_RISCVINIT_HELP		    "rvinit                 Set RISC-V architecture type."
#define JMSG_COMMAND_RISCVINIT_HELP_2       "                       <architecture type> supported are:"
#define JMSG_COMMAND_RISCVINIT_HELP_3       "                       <8> <16> <32> <64> or <128>"                                                  
#define JMSG_COMMAND_RISCVINIT_ACT          "Idle count: 0x%08x\nRISC-V spec version: 0.11."
#define JMSG_COMMAND_RISCVINIT_ACT_2        "Idle count: 0x%08x\nRISC-V spec version: 0.13"                                                 
#define JMSG_COMMAND_RISCVINIT_ACT_3        "Idle count: 0x%08x\nRISC-V spec version: not specified" 

//RISCVHARTHALT
#define JMSG_COMMAND_RISCVHARTHALT_Help		"rvhalt <hart num>      Halt the selected hart."
#define JMSG_COMMAND_RISCVHARTHALT_Help_2	"                       <hart num> hart number to halt."
#define JMSG_COMMAND_RISCVHARTHALT_ACT	    "Selected hart 0x%08x is halted."

// RISCVRD
#define JMSG_COMMAND_RISCVRD_HELP		    "rvrd                   Read from RISC-V register/memory."
#define JMSG_COMMAND_RISCVRD_HELP_2		    "                       General purpose registers    : rvrd <type> <addr>"
#define JMSG_COMMAND_RISCVRD_HELP_3	        "                       Control and status registers : rvrd <type> <addr> <reg mech>"
#define JMSG_COMMAND_RISCVRD_HELP_4         "                       Floating point registers     : rvrd <type> <addr> <reg mech> <double precision>"
#define JMSG_COMMAND_RISCVRD_HELP_5         "                       Memory                       : rvrd <type> <addr> <mem mech> <number of bytes to read>"
#define JMSG_COMMAND_RISCVRD_HELP_6         "                       <Type>                       : Type of read operation"
#define JMSG_COMMAND_RISCVRD_HELP_7         "                                                        <R> : GPRs"
#define JMSG_COMMAND_RISCVRD_HELP_8         "                                                        <F> : FPRs"
#define JMSG_COMMAND_RISCVRD_HELP_9         "                                                        <C> : CSRs"
#define JMSG_COMMAND_RISCVRD_HELP_10        "                                                        <M> : Memory"
#define JMSG_COMMAND_RISCVRD_HELP_11        "                       <addr>                       : Address to read."
#define JMSG_COMMAND_RISCVRD_HELP_12        "                                                        GPRs addr range                    : <0x1000 - 0x101f>"
#define JMSG_COMMAND_RISCVRD_HELP_13        "                                                        Floating point register addr range : <0x1020 - 0x103f>"
#define JMSG_COMMAND_RISCVRD_HELP_14        "                                                        CSRs addr range                    : <0x0000 - 0x0fff>"
#define JMSG_COMMAND_RISCVRD_HELP_15        "                       <reg mech>                   : Debug mechanism to use in case of CSRs and FPRs"
#define JMSG_COMMAND_RISCVRD_HELP_16        "                                                        <0> : use program buffer"
#define JMSG_COMMAND_RISCVRD_HELP_17        "                                                        <1> : use abstract command only"
#define JMSG_COMMAND_RISCVRD_HELP_18        "                       <mem mech>                   : Debug mechanism to use in case of memory access"
#define JMSG_COMMAND_RISCVRD_HELP_19        "                                                        <0> : use system bus"
#define JMSG_COMMAND_RISCVRD_HELP_20        "                                                        <1> : use program buffer"
#define JMSG_COMMAND_RISCVRD_HELP_21        "                                                        <2> : use abstract memory access"
#define JMSG_COMMAND_RISCVRD_HELP_22        "                       <double precision>           : Double precision status (only for floating point registers)"
#define JMSG_COMMAND_RISCVRD_HELP_23        "                                                        <0> : double precision is not supported"
#define JMSG_COMMAND_RISCVRD_HELP_24        "                                                        <1> : double precision is supported"
#define JMSG_COMMAND_RISCVRD_HELP_25        "                       <number of bytes to read>    : Number of bytes to read from memory (supports up to 4-byte)"
#define JMSG_COMMAND_RISCVRD_ACT            "Value 0x%08x read from address/offset 0x%08x."
#define JMSG_COMMAND_RISCVRD_ACT_2          "Value bit ranges\n (63 - 32) 0x%08x\n (31 - 0) 0x%08x read from address/offset 0x%08x."
#define JMSG_COMMAND_RISCVRD_ACT_3          "Value bit ranges\n (127 - 96) 0x%08x\n (95 - 64) 0x%08x\n (63 - 32) 0x%08x\n (31 - 0) 0x%08x read from address/offset 0x%08x."

//RISCVWR
#define JMSG_COMMAND_RISCVWR_HELP		    "rvwr                   Write to RISC-V register/memory."
#define JMSG_COMMAND_RISCVWR_HELP_2		    "                       General purpose registers    : rvwr <type> <addr> <value>"
#define JMSG_COMMAND_RISCVWR_HELP_3         "                       Control and status registers : rvwr <type> <addr> <reg mech> <value>"
#define JMSG_COMMAND_RISCVWR_HELP_4		    "                       Floating point registers     : rvwr <type> <addr> <reg mech> <double precision> <value>"
#define JMSG_COMMAND_RISCVWR_HELP_5         "                       Memory                       : rvwr <type> <addr> <mem mech> <number of bytes to write> <value>"
#define JMSG_COMMAND_RISCVWR_HELP_6		    "                       <Type>                       : Type of write operation"
#define JMSG_COMMAND_RISCVWR_HELP_7         "                                                        <R> : GPRs"
#define JMSG_COMMAND_RISCVWR_HELP_8		    "                                                        <F> : FPRs"
#define JMSG_COMMAND_RISCVWR_HELP_9         "                                                        <C> : CSRs"
#define JMSG_COMMAND_RISCVWR_HELP_10        "                                                        <M> : Memory"
#define JMSG_COMMAND_RISCVWR_HELP_11        "                       <addr>                       : Address to write."
#define JMSG_COMMAND_RISCVWR_HELP_12        "                                                        GPRs addr range                    : <0x1000 - 0x101f>"
#define JMSG_COMMAND_RISCVWR_HELP_13        "                                                        Floating point register addr range : <0x1020 - 0x103f>"
#define JMSG_COMMAND_RISCVWR_HELP_14        "                                                        CSRs addr range                    : <0x0000 - 0x0fff>"
#define JMSG_COMMAND_RISCVWR_HELP_15        "                       <reg mech>                    : Debug mechanism to use in case of CSRs and FPRs"
#define JMSG_COMMAND_RISCVWR_HELP_16        "                                                        <0> : use program buffer"
#define JMSG_COMMAND_RISCVWR_HELP_17        "                                                        <1> : use abstract command only"
#define JMSG_COMMAND_RISCVWR_HELP_18        "                       <mem mech>                    : Debug mechanism to use in case of memory access"
#define JMSG_COMMAND_RISCVWR_HELP_19        "                                                        <0> : use system bus"
#define JMSG_COMMAND_RISCVWR_HELP_20        "                                                        <1> : use program buffer"
#define JMSG_COMMAND_RISCVWR_HELP_21        "                                                        <2> : use abstract memory access"
#define JMSG_COMMAND_RISCVWR_HELP_22        "                       <double precision>            : Double precision status (only for floating point registers)"
#define JMSG_COMMAND_RISCVWR_HELP_23        "                                                        <0> : double precision is not supported"
#define JMSG_COMMAND_RISCVWR_HELP_24        "                                                        <1> : double precision is supported"
#define JMSG_COMMAND_RISCVWR_HELP_25        "                       <number of bytes to write>    : Number of bytes to write to memory (supports up to 4-byte)"
#define JMSG_COMMAND_RISCVWR_HELP_26        "                       <value>                       : Data to be written"
#define JMSG_COMMAND_RISCVWR_HELP_27        "                                                        bit format <31-0> <63-32> <64-95> <96-127>"
#define JMSG_COMMAND_RISCVWR_ACT            "Value 0x%08x written to address/offset 0x%08x."
#define JMSG_COMMAND_RISCVWR_ACT_2          "Value bit ranges\n (63 - 32) 0x%08x\n (31 - 0) 0x%08x written to address/offset 0x%08x."
#define JMSG_COMMAND_RISCVWR_ACT_3          "Value bit ranges\n (127 - 96) 0x%08x\n (95 - 64) 0x%08x\n (63 - 32) 0x%08x\n (31 - 0) 0x%08x written to address/offset 0x%08x"

// error messages
#define JMSG_ERROR_INSUFFICIENT_MEM "Not enough memory to allocate buffer."      // error message (with prefix Error: )

#endif // _JCONMSG_H_
