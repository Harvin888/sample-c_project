/****************************************************************************
       Module: jconcmd.h
     Engineer: Vitezslav Hola
  Description: Header for commands in JTAG console for Opella-XD

Date           Initials    Description
18-Jan-2007    VH          initial
****************************************************************************/

#ifndef _JCONCMD_H_
#define _JCONCMD_H_

#include "protocol/common/drvopxd.h"

#define MAX_NESTED_CFILE            10             // max depth for calling cfile
#define MAX_CFILE_NAME              256            // max cfile name length


// Linux build change...
#ifndef __LINUX
#define strdup _strdup
#endif

typedef struct
{
    FILE *(pCFile[MAX_NESTED_CFILE]);
	uint32_t uiCurFileIndex;
    char pszCFileNames[MAX_NESTED_CFILE][MAX_CFILE_NAME];
} TyCmdFiles;

#ifdef RISCV_CMDS

#define RISCV_INVALID_ARCH                                      0

typedef struct
{
	unsigned long ulAbits;
	unsigned long ulArchType;
} TyRiscvParam;
#endif

void DoConsoleCommand(const char *pszCommandLine, TyCmdFiles *ptyCmdFiles, unsigned char *pbFinish);
void RunJTAGConsole(TyDevHandle tyDeviceHandle);

#endif // _JCONCMD_H_
