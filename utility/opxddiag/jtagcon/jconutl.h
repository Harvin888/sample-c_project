/****************************************************************************
       Module: jconutl.h
     Engineer: Vitezslav Hola
  Description: Header for utility functions in JTAG console for Opella-XD

Date           Initials    Description
18-Jan-2007    VH          initial
****************************************************************************/
#ifndef _JCONUTL_H_
#define _JCONUTL_H_

#include "protocol/common/drvopxd.h"

#define MAX_SCANCHAIN_LENGTH                 8160
#define ULONG_BITS                           (sizeof(uint32_t) * 8)
#define SCANCHAIN_ULONG_SIZE                 (MAX_SCANCHAIN_LENGTH / ULONG_BITS) // size of scanchain buffers in ulongs (32 bits/ulong)


#define MAX_CHAIN_LENGTH                     96
#define MAX_CHAIN_ULONG_SIZE                 (MAX_CHAIN_LENGTH / ULONG_BITS) // size of scanchain buffers in ulongs (32 bits/ulong)
#define MAX_TAPS_ON_SCANCHAIN                64

#define SHIR_DEFAULT_LENGTH                  5     // default number of bits for IR
#define SHDR_DEFAULT_LENGTH                  32    // default number of bits for DR

#define MAX_WAIT_TIME_MS                     9999  // maximum wait time 9999 ms

#define MAX_OUTPUT_LINE                      4096

#ifdef RISCV_CMDS
//gprs Register value
#define RISCV_GPRS_START_ADDR                                   0X1000
#define RISCV_GPRS_END_VALUE                                    0X101F
#define RISCV_RESERVED_START_VALUE                              0X1040
#define RISCV_FLOATING_POINT_REGISTER_START_Address             0X1020
#define RISCV_CSR_START_ADDR                                    0X0000
#define RISCV_CSR_LAST_ADDR                                     0X0FFF
#define RISCV_DPC_ADDR                                          0X7B1
#define RISCV_GDB_CSR_START_ADDRESS                             65
#define RISCV_CSR_DPC_FROM_GDB                                  32
#define RISCV_ABSTRCT_MEM_ACCESS                                4
#define RISCV_8_BIT_ARCH                                        8
#define RISCV_16_BIT_ARCH                                       16
#define RISCV_32_BIT_ARCH                                       32
#define RISCV_64_BIT_ARCH                                       64
#define RISCV_128_BIT_ARCH                                      128
#endif

typedef enum {
   INFO_MESSAGE      = 0,
   ERROR_MESSAGE     = 1,
   HELP_MESSAGE      = 2,
   DEBUG_MESSAGE     = 3,
   LOG_MESSAGE       = 4
} TyMessageType;

unsigned char TestScanChain(uint32_t *puiNumberOfTAPs); 
unsigned char CompareUlongs(uint32_t *pulUlongs1, uint32_t *pulUlongs2, uint32_t uiNumberOfBits);
unsigned char Parse_SHIR_DR_Params(const char *pszParamString, uint32_t *pulOutValue, uint32_t ulScanChainUlongs,  
                                   uint32_t *puiLengthBits, unsigned char bIRSelect);
unsigned char PrintUlongArray(char *pszString, uint32_t uiBufferLen, 
                              const uint32_t *pulData, uint32_t uiNumberOfUlongs);
void ShiftUlongArray(uint32_t *pulUlongArray, uint32_t uiNumberOfUlongs,
                     uint32_t uiNumberOfBits, unsigned char bShiftToRight);
void PrintMessage(TyMessageType tyMessageType, const char * pszToFormat, ...);
uint32_t StrToUlong(char * pszValue, unsigned char * pbValid);
uint32_t StrToUlong2(char * pszValue, unsigned char * pbValid, const char * pszStopchar);
void StrToUpr(char *pszString);
unsigned char Parse_ArmSHIRDR_Params(const char *pszParamString,
                                   uint32_t *pulOutIRValue, uint32_t ulScanChainUlongsIR, 
                                   uint32_t *pulOutDRValue, uint32_t ulScanChainUlongsDR, 
                                   uint32_t *puiLengthBitsIR,uint32_t *puiLengthBitsDR);
#ifdef RISCV_CMDS
char* ConvertDwErrToOpxdlayerErr(unsigned long UlError);
unsigned char Parse_RiscvWR_Params(char* pszParams, unsigned long ulArchType, unsigned long* pulAddress, unsigned long* pulMech, unsigned long* pulSizeOrDoublePre, uint32_t* pulData);
unsigned char Parse_RiscvRD_Params(char* pszParams, unsigned long ulArchType, unsigned long* pulAddress, unsigned long* pulMech, unsigned long* pulSizeOrDoublePre);
unsigned char Riscv_RD_WR_Print(unsigned long ulArchType, unsigned long pulAddress, uint32_t* puData, uint32_t uFlag);
#endif

#endif // _JCONUTL_H_

