/******************************************************************************
       Module: main.cpp
     Engineer: Vitezslav Hola
  Description: Main program for OpXDDiag (Opella-XD user & production diagnostics)
  Date           Initials    Description
  25-Sep-2006    VH          Initial
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "diagerr.h"
#include "diagglob.h"
#include "diaginfo.h"
#include "diagtest.h"
#include "diagopt.h"
#include "diaggen.h"
#include "jtagcon/jconcmd.h"
#include "protocol/common/drvopxd.h"

// local variables
TyDiagOptions tyDiagOptions;
static char ppszDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN];

// local functions
static void PrintTitleAndVersion(void);
static void PrintErrorMessage(int32_t iErrorNumber);
static int32_t CheckOpellaSerialNumber(char ppszDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN], unsigned short usConnectedDevices, const char *pszSerialNumber);

#ifndef __LINUX
 #include <windows.h>
#else
#include <unistd.h>
/****************************************************************************
Function: Sleep
Engineer: Nikolay Chokoev
Input: mseconds
Output: none
Description: 
Date           Initials    Description
18-Dec-2007    NCH         Initial
*****************************************************************************/
void Sleep(uint32_t  mseconds )
{
	// Windows Sleep uses miliseconds
	// linux usleep uses microsecond
	usleep( mseconds * 1000 );
}
#endif

/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: int32_t argc - number of arguments
               char *argv[] - program arguments
       Output: return value DIAG_xxx
  Description: main part of program
Date           Initials    Description
25-Sep-2006    VH          Initial
****************************************************************************/
int32_t main (int32_t argc, char *argv[])
{
   int32_t iRet;

   // show program title and version
   PrintTitleAndVersion();

   // analyze program parameters
   InitDiagOptions(&tyDiagOptions);
   if (AnalyseDiagOptions(argc,argv , &tyDiagOptions) != DIAG_NO_ERROR)
      {
      // invalid parameters
      PrintUsage();
      return(DIAG_INVALID_PARAMETERS);
      }

   // show help if required
   if (tyDiagOptions.bShowHelp)
      {
      PrintUsage();
      return(DIAG_NO_ERROR);
      }

   // show version
   if (tyDiagOptions.bShowVersion)
      {
      return(DIAG_NO_ERROR);
      }

   // generate production image if required
   if (tyDiagOptions.bGenerateBinaryImage)
      {
      printf("Generating production image ... ");
      iRet = GenerateProductionImage(&tyDiagOptions);
      if (iRet != DIAG_NO_ERROR)
         {
         printf("ERROR\n");
         PrintErrorMessage(iRet);
         return(iRet);
         }
      else
         {
         printf("OK\n");
         return(DIAG_NO_ERROR);
         }
      }

   // list information about all connected Opella
   if (tyDiagOptions.bShowList)
      {
      int32_t iCnt;
      unsigned short usDevices;

      printf("Detecting connected Opella-XD(s) ... ");
      DL_OPXD_GetConnectedDevices(ppszDeviceNumbers, &usDevices, OPELLAXD_USB_PID);
      switch(usDevices)
         {
         case 0 : 
            printf("none found\n");
            break;
         case 1 : 
            printf("1 found\n");
            break;
         default:
            printf("%d found\n", usDevices);
            break;
         }
      // show detail information for each device
      if (usDevices)
         {
         for (iCnt=0; iCnt<usDevices; iCnt++)
            {
            int32_t iRes;
            TyDevInfo tyDevInfo;
            printf("\n");

			   Sleep(1000);
            iRes = GetDeviceInfo(ppszDeviceNumbers[iCnt], &tyDevInfo);
            printf("Opella-XD serial number: %s\n", ppszDeviceNumbers[iCnt]);
            if (iRes == DIAG_NO_ERROR)
               {
               char pszLocTpaSerialNumber[INFO_SERIAL_NUMBER_LEN + 2];
               printf("           Manufactured: %s, %s\n", tyDevInfo.pszManufDate, tyDevInfo.pszBoardRevision);
               printf("               Firmware: %s, %s\n", tyDevInfo.pszFwVersion, tyDevInfo.pszFwDate);
               if (!strcmp(tyDevInfo.pszTpaSerialNumber, ""))
                  strcpy(pszLocTpaSerialNumber, "");
               else
                  sprintf(pszLocTpaSerialNumber, ", %s", tyDevInfo.pszTpaSerialNumber);

               switch (tyDevInfo.tyTpaType)
                  {
                  case TPA_NONE:
                     break;
                  case TPA_MIPS14:
                     printf("                    TPA: TPAOP-MIPS14, %s%s\n", tyDevInfo.pszTpaRevision, pszLocTpaSerialNumber);
                     break;
                  case TPA_ARM20:
                     printf("                    TPA: TPAOP-ARM20, %s%s\n", tyDevInfo.pszTpaRevision, pszLocTpaSerialNumber);
                     break;
                  case TPA_ARC20:
                     printf("                    TPA: TPAOP-ARC20, %s%s\n", tyDevInfo.pszTpaRevision, pszLocTpaSerialNumber);
                     break;
                  case TPA_DIAG:
                     printf("                    TPA: Production Test Board, %s%s\n", tyDevInfo.pszTpaRevision, pszLocTpaSerialNumber);
                     break;
                  case TPA_UNKNOWN:
                  case TPA_INVALID:
                  default:
                     printf("                    TPA: unknown\n");
                     break;
                  }
               }
            else
               printf("  %s\n", DIAG_INACCESSIBLE_DEVICE);
            }
         }
      return(DIAG_NO_ERROR);
      }

   // sign TPA
   if (tyDiagOptions.bWriteTpaSigniture)
      {
      TyTpaParameters tyTpaParams;
      unsigned short usNumberOfDevices;
      TyDevHandle tyHandle;

      iRet = DIAG_NO_ERROR;

      // verify parameters
      iRet = PrepareTpaSigniture(tyDiagOptions.pszTpaType, tyDiagOptions.pszTpaSerialNumber, tyDiagOptions.pszTpaRevision, &tyTpaParams);
      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      // determine which Opella-XD to use
      DL_OPXD_GetConnectedDevices(ppszDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
      switch(usNumberOfDevices)
         {
         case 0 : // no device connected
            iRet = DIAG_NO_DEVICE_CONNECTED;
            break;

         case 1 : // only 1 device, no sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               tyDiagOptions.pszSerialNumber = ppszDeviceNumbers[0];
            break;

         default : // several devices, sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               iRet = DIAG_MISSING_SERIAL_NUMBER;
            break;
         }

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      // configure seleted device and get handle
      // find device and unconfigure it
      switch (DL_OPXD_OpenDevice(tyDiagOptions.pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle))
         {
         case DRVOPXD_ERROR_NO_ERROR:
            break;
         case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
            iRet = DIAG_INVALID_SERIAL_NUMBER;
            break;
         default:
            iRet = DIAG_DRIVER_ERROR;
            break;
         }

      if ((iRet == DIAG_NO_ERROR) && !CheckOpellaSerialNumber(ppszDeviceNumbers, usNumberOfDevices, tyDiagOptions.pszSerialNumber))
         iRet = DIAG_INVALID_SERIAL_NUMBER;

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      if (DL_OPXD_UnconfigureDevice(tyHandle)!=DRVOPXD_ERROR_NO_ERROR)
         iRet = DIAG_DRIVER_ERROR;

      printf("Writing TPA signature ... ");
      if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_WriteTpaSigniture(tyHandle, &tyTpaParams) != DRVOPXD_ERROR_NO_ERROR))
         iRet = DIAG_DRIVER_ERROR;

      if (iRet != DIAG_NO_ERROR)
         {
         printf("ERROR\n");
         PrintErrorMessage(iRet);
         return(iRet);
         }
      else
         {
         printf("OK\n");
         return(DIAG_NO_ERROR);
         }
      }

   if (tyDiagOptions.bRunDiagnostic)
      {
      unsigned short usNumberOfDevices;

      iRet = DIAG_NO_ERROR;
      if ((tyDiagOptions.iDiagnosticSelection < 1) || (tyDiagOptions.iDiagnosticSelection > 10))
         {
         iRet = DIAG_INVALID_TEST_SELECTION;
         PrintErrorMessage(iRet);
         printf("\n");
         PrintUsage();
         return(iRet);
         }

      // determine which Opella-XD to use
      DL_OPXD_GetConnectedDevices(ppszDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
      switch(usNumberOfDevices)
         {
         case 0 : // no device connected
            iRet = DIAG_NO_DEVICE_CONNECTED;
            break;

         case 1 : // only 1 device, no sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               tyDiagOptions.pszSerialNumber = ppszDeviceNumbers[0];
            break;

         default : // several devices, sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               iRet = DIAG_MISSING_SERIAL_NUMBER;
            break;
         }

      if ((iRet == DIAG_NO_ERROR) && !CheckOpellaSerialNumber(ppszDeviceNumbers, usNumberOfDevices, tyDiagOptions.pszSerialNumber))
         iRet = DIAG_INVALID_SERIAL_NUMBER;

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }
      // run different diagnostics
      printf("Diagnostic test(s) for Opella %s\n", tyDiagOptions.pszSerialNumber);
      // check firmware version
      iRet = CheckForFirmware(tyDiagOptions.pszSerialNumber);
      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      // run CPU diagnostic (2) or all (1)
      if ((tyDiagOptions.iDiagnosticSelection == 2) || (tyDiagOptions.iDiagnosticSelection == 1))
         {
         iRet = RunOpellaCpuDiagnostic(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_CPU_ERROR;
            }
         }
      // run LED diagnostic (3) or all (1)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 3) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaLedDiagnostic(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_LED_ERROR;
            }
         }
      // run external RAM diagnostic (4) or all (1)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 4) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaExtramDiagnostic(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_SRAM_ERROR;
            }
         }
      // run voltage sense/adjust diagnostic (5) or all (1)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 5) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaVoltageDiagnostic(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_VOLTAGE_ERROR;
            }
         }
      // run FPGA configuration/verification diagnostic (6) or all (1)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 6) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaFpgaDiagnostic(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_FPGA_ERROR;
            }
         }
      // run clock generator diagnostic (7) or all (1)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 7) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaPllDiagnostic(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_CLOCK_ERROR;
            }
         }
      // run performance diagnostic (8)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 8) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaPerformanceMeasurement(tyDiagOptions.pszSerialNumber);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_PERFORMANCE_ERROR;
            }
         }
      // run TPA test (9)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 9) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaTpaDiagnostic(tyDiagOptions.pszSerialNumber, tyDiagOptions.bProductionBoardRequired);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_TPA_INTERFACE_ERROR;
            }
         }
      // run TPA external loopback (10)
      if ((iRet == DIAG_NO_ERROR) && 
          ((tyDiagOptions.iDiagnosticSelection == 10) || (tyDiagOptions.iDiagnosticSelection == 1)))
         {
         iRet = RunOpellaTpaLoopDiagnostic(tyDiagOptions.pszSerialNumber, tyDiagOptions.bProductionTpa, tyDiagOptions.bProductionTpaAdaptor);
         if (iRet != DIAG_NO_ERROR)
            {
            PrintErrorMessage(iRet);
            iRet = DIAG_TEST_TPA_LOOP_ERROR;
            }
         }
      return(iRet);
      }

   // run JTAG console
   if (tyDiagOptions.bRunJtagConsole)
      {
      unsigned short usNumberOfDevices;
      TyDevHandle tyHandle;

      iRet = DIAG_NO_ERROR;
      // determine which Opella-XD to use
      DL_OPXD_GetConnectedDevices(ppszDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
      switch(usNumberOfDevices)
         {
         case 0 : // no device connected
            iRet = DIAG_NO_DEVICE_CONNECTED;
            break;

         case 1 : // only 1 device, no sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               tyDiagOptions.pszSerialNumber = ppszDeviceNumbers[0];
            break;

         default : // several devices, sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               iRet = DIAG_MISSING_SERIAL_NUMBER;
            break;
         }

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      // check firmware version
      iRet = CheckForFirmware(tyDiagOptions.pszSerialNumber);
      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      // configure seleted device and get handle
      // find device and unconfigure it
      switch (DL_OPXD_OpenDevice(tyDiagOptions.pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle))
         {
         case DRVOPXD_ERROR_NO_ERROR:
            break;
         case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
            iRet = DIAG_INVALID_SERIAL_NUMBER;
            break;
         default:
            iRet = DIAG_DRIVER_ERROR;
            break;
         }

      if ((iRet == DIAG_NO_ERROR) && !CheckOpellaSerialNumber(ppszDeviceNumbers, usNumberOfDevices, tyDiagOptions.pszSerialNumber))
         iRet = DIAG_INVALID_SERIAL_NUMBER;

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      if (DL_OPXD_UnconfigureDevice(tyHandle)!=DRVOPXD_ERROR_NO_ERROR)
         iRet = DIAG_DRIVER_ERROR;

      if (iRet == DIAG_NO_ERROR)
         iRet = PrepareForJtagConsole(tyHandle);
         // run JTAG console

      if (iRet == DIAG_NO_ERROR)
         RunJTAGConsole(tyHandle);

      if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, FALSE)!=DRVOPXD_ERROR_NO_ERROR))
         iRet = DIAG_DRIVER_ERROR;

      if ((iRet == DIAG_NO_ERROR) && (DL_OPXD_UnconfigureDevice(tyHandle) != DRVOPXD_ERROR_NO_ERROR))
         iRet = DIAG_DRIVER_ERROR;

	  if(0 == strncmp(tyDiagOptions.pszTargetDevice,"riscv",5))
		(void)DL_OPXD_TerminateDW(tyHandle, RISCV);
	  else //TODO: extend when other architectures are implemented
		(void)DL_OPXD_TerminateDW(tyHandle, ARC);
      (void)DL_OPXD_CloseDevice(tyHandle);
      PrintErrorMessage(iRet);
      return(iRet);
      }

   if (tyDiagOptions.bUpdateFirmware)
      {
      unsigned short usNumberOfDevices;

      iRet = DIAG_NO_ERROR;
      // determine which Opella-XD to use
      DL_OPXD_GetConnectedDevices(ppszDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
      switch(usNumberOfDevices)
         {
         case 0 : // no device connected
            iRet = DIAG_NO_DEVICE_CONNECTED;
            break;

         case 1 : // only 1 device, no sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               tyDiagOptions.pszSerialNumber = ppszDeviceNumbers[0];
            break;

         default : // several devices, sn needed
            if (tyDiagOptions.pszSerialNumber == NULL)
               iRet = DIAG_MISSING_SERIAL_NUMBER;
            break;
         }

      if ((iRet == DIAG_NO_ERROR) && !CheckOpellaSerialNumber(ppszDeviceNumbers, usNumberOfDevices, tyDiagOptions.pszSerialNumber))
         iRet = DIAG_INVALID_SERIAL_NUMBER;

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         return(iRet);
         }

      Sleep(1000);

      // check firmware version and possible update
      // show detail information for each device
      iRet = UpdateFirmware(tyDiagOptions.pszSerialNumber);

      if (iRet != DIAG_NO_ERROR)
         {
         PrintErrorMessage(iRet);
         }
      return(iRet);
      }

   PrintUsage();
   return(DIAG_INVALID_PARAMETERS);
}


/****************************************************************************
     Function: PrintTitleAndVersion
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: print program title and version
Date           Initials    Description
25-Sep-2006    VH          Initial
****************************************************************************/
static void PrintTitleAndVersion(void)
{
   printf("%s\n", OPXDDIAG_PROG_TITLE);
   printf("%s\n", OPXDDIAG_PROG_VERSION);
   printf("\n");
}

/****************************************************************************
     Function: PrintErrorMessage
     Engineer: Vitezslav Hola
        Input: int32_t iErrorNumber - error code
       Output: none
  Description: print error description to screen
Date           Initials    Description
26-Sep-2006    VH          Initial
****************************************************************************/
static void PrintErrorMessage(int32_t iErrorNumber)
{
   switch(iErrorNumber)
      {
      case DIAG_NO_ERROR: 
         break;
      case DIAG_INVALID_PARAMETERS:
         printf("%s\n", DIAG_INVALID_PARAMETERS_MSG);
         break;
      case DIAG_DRIVER_ERROR:
         printf("%s\n", DIAG_DRIVER_ERROR_MSG);
         break;
      case DIAG_INVALID_SERIAL_NUMBER:
         printf("%s\n", DIAG_INVALID_SERIAL_NUMBER_MSG);
         break;
      case DIAG_INVALID_REVISION_NUMBER:
         printf("%s\n", DIAG_INVALID_REVISION_NUMBER_MSG);
         break;
      case DIAG_MISSING_SERIAL_NUMBER:
         printf("%s\n", DIAG_MISSING_SERIAL_NUMBER_MSG);
         break;
      case DIAG_NO_DEVICE_CONNECTED:
         printf("%s\n", DIAG_NO_DEVICE_CONNECTED_MSG);
         break;
      case DIAG_INVALID_TEST_SELECTION:
         printf("%s\n", DIAG_INVALID_TEST_SELECTION_MSG);
         break;

      case DIAG_UPDATE_REQUIRED:
         printf("%s\n", DIAG_UPDATE_REQUIRED_MSG);
         break;
      case DIAG_UPDATE_FAILED:
         printf("%s\n", DIAG_UPDATE_FAILED_MSG);
         break;

      case DIAG_MANUF_FILE_ERROR:
         printf("%s\n", DIAG_MANUF_FILE_ERROR_MSG);
         break;
      case DIAG_INVALID_LOADER:
         printf("%s\n", DIAG_INVALID_LOADER_MSG);
         break;
      case DIAG_INVALID_FIRMWARE:
         printf("%s\n", DIAG_INVALID_FIRMWARE_MSG);
         break;
      case DIAG_INVALID_DISKWARE:
         printf("%s\n", DIAG_INVALID_DISKWARE_MSG);
         break;
      case DIAG_INVALID_FPGAWARE:
         printf("%s\n", DIAG_INVALID_FPGAWARE_MSG);
         break;
      case DIAG_INVALID_TPA:
         printf("%s\n", DIAG_INVALID_TPA_MSG);
         break;

      case DIAG_TEST_OPELLA_ERROR:
         printf("%s\n", DIAG_TEST_OPELLA_ERROR_MSG);
         break;
      case DIAG_TEST_PC_ERROR:
         printf("%s\n", DIAG_TEST_PC_ERROR_MSG);
         break;
      case DIAG_TEST_TPA_ERROR:
         printf("%s\n", DIAG_TEST_TPA_ERROR_MSG);
         break;

      case DIAG_NO_TPA_FOR_JTAG_CONSOLE:
         printf("%s\n", DIAG_NO_TPA_FOR_JTAG_CONSOLE_MSG);
         break;
      case DIAG_UNSUPPORTED_TPA:
         printf("%s\n", DIAG_UNSUPPORTED_TPA_MSG);
         break;

      case DIAG_TPA_TEST_SCANCHAIN_ERROR:
         printf("%s\n", DIAG_TPA_TEST_SCANCHAIN_ERROR_MSG);
         break;
      case DIAG_TPA_TEST_ID_ERROR:
         printf("%s\n", DIAG_TPA_TEST_ID_ERROR_MSG);
         break;
      case DIAG_TPA_TEST_PIN:
         printf("%s\n", DIAG_TPA_TEST_PIN_MSG);
         break;
      case DIAG_INVALID_TARGET_DEVICE:
         printf("%s\n", DIAG_INVALID_TARGET_DEVICE_MSG);
         break;
      default:
         printf("%s\n", DIAG_UNKNOWN_ERROR_MSG);
         break;
      }
}


/****************************************************************************
     Function: CheckOpellaSerialNumber
     Engineer: Vitezslav Hola
        Input: char ppszNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN] - array of serial numbers of connected devices
               unsigned short usConnectedDevices - number of connected devices (> 0)
               const char *pszSerialNumber - serial number to compare
       Output: int32_t - 0 if serial number is not in the table, 1 otherwise
  Description: compares given serial number with table of serial numbers
Date           Initials    Description
31-Jan-2007    VH          Initial
****************************************************************************/
static int32_t CheckOpellaSerialNumber(char ppszNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN], unsigned short usConnectedDevices, const char *pszSerialNumber)
{
   unsigned short usDevice;

   assert(usConnectedDevices > 0);

   for (usDevice=0; usDevice < usConnectedDevices; usDevice++)
      {
      if (!strcmp(pszSerialNumber, ppszNumbers[usDevice]))
         return 1;         // serial number found
      }
   return 0;
}



