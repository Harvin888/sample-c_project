/******************************************************************************
       Module: diagopt.cpp
     Engineer: Vitezslav Hola
  Description: Analyzing options for OpXDDiag
  Date           Initials    Description
  26-Sep-2006    VH          Initial
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include "diagerr.h"
#include "diagopt.h"
#include "diagglob.h"

// program options
#define OPT_HELP                       "--help"             // program help
#define OPT_VERSION                    "--version"          // show version number
#define OPT_INSTANCE                   "--instance"         // specify instance for jtag console, firmware update and diagnostics
#define OPT_LIST                       "--list"
#define OPT_JTAGCON                    "--jtag-console"
#define OPT_FIRMWARE                   "--update-firmware"
#define OPT_DIAG                       "--diag"
#define OPT_DEVICE                     "--device"
// hidden switches
#define OPT_GENBIN                     "--genbin"
#define OPT_SIGNTPA                    "--sign-tpa"
#define OPT_PRODUCTION                 "--production"
#define OPT_TPA_PRODUCTION             "--tpa-production"
#define OPT_TPA_ADAPTOR                "--tpa-adaptor"



// "Usage" text
#define OPXDDIAG_USAGE                  OPXDDIAG_PROG_NAME" [options]"
#define OPXDDIAG_USAGE_OPT0             "Valid options are:"
#define OPXDDIAG_USAGE_OPT_HELP         "--help                         Display usage information."
#define OPXDDIAG_USAGE_OPT_VER          "--version                      Display program version."
#define OPXDDIAG_USAGE_OPT_LIST         "--list                         List all Opella-XD(s) connected."
// hidden option                         --genbin <serial-number> <revision>  
//                                                                      Generate binary image for OKI Flash Utility
// hidden option                         --sign-tpa <tpa-type> <serial-number> <revision>      
//                                                                      Write TPA signiture into connected TPA (part of manufacturing process)
//                                                                      <tpa-type> is one of following options:
//                                                                      MIPS14, ARM20, ARC20, DIAG
//                                                                      <serial-number> is a string with up to 15 characters
//                                                                      <revision> is string with format Ra-Mb-Ec where a, b and c are
//                                                                      PCB, mechanical and electrical revision numbers from 0 to 255
// hidden option                         --production                   Requires Opella-XD production test board during test
// hidden option                         --tpa-production               Run extensive TPA production diagnostic tests
// hidden option                         --tpa-adaptor                  Run extensive TPA adaptor board production diagnostic tests
// 
#define OPXDDIAG_USAGE_OPT_INST         "--instance <serial-number>     Select Opella-XD when multiple Opella-XD(s)"
#define OPXDDIAG_USAGE_OPT_INST2        "                               connected."
#define OPXDDIAG_USAGE_OPT_FW           "--update-firmware              Update Opella-XD firmware."
#define OPXDDIAG_USAGE_OPT_JCON         "--jtag-console                 Open JTAG low-level console mode."
#ifdef ARC_CMDS
#define OPXDDIAG_USAGE_OPT_JCON1        "--device                       Target device name can be specified."
#define OPXDDIAG_USAGE_OPT_JCON2        "                               The following devices are supported:"
#define OPXDDIAG_USAGE_OPT_JCON3        "                               arc (using JTAG/TPAOP-ARC20 R0)"
#define OPXDDIAG_USAGE_OPT_JCON4        "                               arcangel (using JTAG/TPAOP-ARC20 R0/ADOP-ARC15)"
#define OPXDDIAG_USAGE_OPT_JCON5        "                               arc-cjtag-tpa-r1 (using cJTAG/TPAOP-ARC20 R1)"
#define OPXDDIAG_USAGE_OPT_JCON6        "                               arc-jtag-tpa-r1 (using JTAG/TPAOP-ARC20 R1)"
#endif //ARC_CMDS
#ifdef RISCV_CMDS
#define OPXDDIAG_USAGE_OPT_JCON1        "--device                       Target device name can be specified."
#define OPXDDIAG_USAGE_OPT_JCON2        "                               The following devices are supported:"
#define OPXDDIAG_USAGE_OPT_JCON7        "                               riscv"
#endif //RISCV_CMDS


#define OPXDDIAG_USAGE_OPT_DIAG00       "--diag <diagnostic-test>       Run specific Opella-XD diagnostic test."
//#define OPXDDIAG_USAGE_OPT_DIAG01       "                           Show diagnotic menu if no test is not specified."
#define OPXDDIAG_USAGE_OPT_DIAG02       "                               <diagnostic-test> is:"
#define OPXDDIAG_USAGE_OPT_DIAG1        "                                1  Run all diagnostics"
#define OPXDDIAG_USAGE_OPT_DIAG2        "                                2  CPU test"
#define OPXDDIAG_USAGE_OPT_DIAG3        "                                3  LED test"
#define OPXDDIAG_USAGE_OPT_DIAG4        "                                4  SRAM test"
#define OPXDDIAG_USAGE_OPT_DIAG5        "                                5  Voltage sense/adjust test"
#define OPXDDIAG_USAGE_OPT_DIAG6        "                                6  FPGA test"
#define OPXDDIAG_USAGE_OPT_DIAG7        "                                7  Clock generator test"
#define OPXDDIAG_USAGE_OPT_DIAG8        "                                8  Performance test"
#define OPXDDIAG_USAGE_OPT_DIAG9        "                                9  TPA interface test"
#define OPXDDIAG_USAGE_OPT_DIAG10       "                                10 TPA loopback test"
//#define OPXDDIAG_USAGE_OPT_DIAG11       "                                11 Target loopback test"
//#define OPXDDIAG_USAGE_OPT_DIAG12       "                                12 Target interface test"


// local functions
void ConvertProgramOptionsToLowercase(int32_t argc, char *argv[]);

/****************************************************************************
     Function: PrintUsage
     Engineer: Vitezslav Hola
        Input: unsigned char bInhouseMode - inhouse mode
       Output: none
  Description: print help for program
Date           Initials    Description
25-Sep-2006    VH          Initial
****************************************************************************/
void PrintUsage(void)
{
   printf("%s\n", OPXDDIAG_USAGE);
   printf("\n");
   printf("%s\n", OPXDDIAG_USAGE_OPT0);

   printf("%s\n", OPXDDIAG_USAGE_OPT_HELP);
   printf("%s\n", OPXDDIAG_USAGE_OPT_VER);
   printf("%s\n", OPXDDIAG_USAGE_OPT_LIST);
   printf("%s\n", OPXDDIAG_USAGE_OPT_INST);
   printf("%s\n", OPXDDIAG_USAGE_OPT_INST2);
   printf("%s\n", OPXDDIAG_USAGE_OPT_FW);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON);
   #ifdef ARC_CMDS
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON1);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON2);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON3);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON4);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON5);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON6);
   #endif //ARC_CMDS
   #ifdef RISCV_CMDS
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON1);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON2);
   printf("%s\n", OPXDDIAG_USAGE_OPT_JCON7);
   #endif //RISCV_CMDS
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG00);
//   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG01);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG02);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG1);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG2);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG3);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG4);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG5);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG6);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG7);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG8);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG9);
   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG10);
//   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG11);
//   printf("%s\n", OPXDDIAG_USAGE_OPT_DIAG12);
}


/****************************************************************************
     Function: InitDiagOptions
     Engineer: Vitezslav Hola
        Input: TyDiagOptions *ptyDiagOptions - structure with program options 
       Output: none
  Description: initialize structure with program options (default settings)
Date           Initials    Description
06-Oct-2006    VH          Initial
****************************************************************************/
void InitDiagOptions(TyDiagOptions *ptyDiagOptions)
{
   assert (ptyDiagOptions != NULL);

   ptyDiagOptions->bShowHelp = false;
   ptyDiagOptions->bShowVersion = false;
   ptyDiagOptions->bGenerateBinaryImage = false;
   ptyDiagOptions->bWriteTpaSigniture = false;
   ptyDiagOptions->pszTpaSerialNumber = NULL;
   ptyDiagOptions->pszTpaRevision = NULL;
   ptyDiagOptions->pszTpaType = NULL;
   ptyDiagOptions->bRunDiagnostic = false;
   ptyDiagOptions->bRunJtagConsole = false;
   ptyDiagOptions->bShowList = false;
   ptyDiagOptions->pszSerialNumber = NULL;
   ptyDiagOptions->pszBoardRevision = NULL;
   ptyDiagOptions->iDiagnosticSelection = 0;
   ptyDiagOptions->bUpdateFirmware = false;
   ptyDiagOptions->bProductionBoardRequired = false;
   ptyDiagOptions->bProductionTpa = false;
   ptyDiagOptions->bProductionTpaAdaptor = false;
   ptyDiagOptions->pszTargetDevice = NULL;
}


/****************************************************************************
     Function: AnalyseDiagOptions
     Engineer: Vitezslav Hola
        Input: int32_t argc - number of program arguments
               char *argv[] - program arguments (strings separated by space)
               TyDiagOptions *ptyDiagOptions - structure for program settings
       Output: int32_t - error code DIAG_xxx
  Description: analyses program options and save result to structure
Date           Initials    Description
06-Oct-2006    VH          Initial
****************************************************************************/
int32_t AnalyseDiagOptions(int32_t argc, char *argv[], TyDiagOptions *ptyDiagOptions)
{
   int32_t iOption;

   assert(argc > 0);
   assert(argv != NULL);
   assert(ptyDiagOptions != NULL);

   // set default settings
   InitDiagOptions(ptyDiagOptions);
   // convert strings to lower case
   ConvertProgramOptionsToLowercase(argc, argv);

   if (argc == 1)
      {
      // show help
      ptyDiagOptions->bShowHelp = true;
      return DIAG_NO_ERROR;                        
      }

   // check for first option
   // version option
   if (!strcmp(argv[1], OPT_VERSION))
      {
      ptyDiagOptions->bShowVersion = true;
      return DIAG_NO_ERROR;                        
      }

   // help option
   if (!strcmp(argv[1], OPT_HELP))
      {
      ptyDiagOptions->bShowHelp = true;
      return DIAG_NO_ERROR;                        
      }

   // list option
   if (!strcmp(argv[1], OPT_LIST))
      {
      ptyDiagOptions->bShowList = true;
      return DIAG_NO_ERROR;                        
      }

   // generate binary image option (hidden option)
   if (!strcmp(argv[1], OPT_GENBIN))
      {
      if (argc < 4)
         return DIAG_INVALID_PARAMETERS;                 // invalid parameters
      if (CheckSerialNumberFormat(argv[2]))
         return(DIAG_INVALID_SERIAL_NUMBER);             // invalid format of serial number
      if (CheckRevisionFormat(argv[3]))
         return(DIAG_INVALID_REVISION_NUMBER);           // invalid format of revision

      ptyDiagOptions->bGenerateBinaryImage = true;
      ptyDiagOptions->pszSerialNumber = argv[2];
      ptyDiagOptions->pszBoardRevision = argv[3];
      return DIAG_NO_ERROR;                        
      }

   iOption = 1;
   while(iOption < argc)
      {
      if (!strcmp(argv[iOption], OPT_INSTANCE))             // get instance name
         {
         iOption++;
         if (iOption >= argc)
            return DIAG_INVALID_PARAMETERS;
         else
            ptyDiagOptions->pszSerialNumber = argv[iOption++];
         continue;
         }
      
      if (!strcmp(argv[iOption], OPT_DEVICE))             // get device name
         {
         iOption++;
         if (iOption >= argc)
            return DIAG_INVALID_PARAMETERS;
         else
            ptyDiagOptions->pszTargetDevice = argv[iOption++];
         
         if (iOption < argc)
            {
            if (!strcmp(argv[iOption], OPT_INSTANCE))
               continue;
            }
         else
            break;
         }
      if (!strcmp(argv[iOption], OPT_DIAG))
         {
         iOption++;
         ptyDiagOptions->bRunDiagnostic = true;
         if (iOption >= argc)
            return DIAG_INVALID_PARAMETERS;                 // expecting test number
         else
            {
            if (sscanf(argv[iOption],"%d", &ptyDiagOptions->iDiagnosticSelection) != 1)
               return DIAG_INVALID_PARAMETERS;
            iOption++;
            }
         continue;
         }

      if (!strcmp(argv[iOption], OPT_SIGNTPA))
         {
         iOption++;
         ptyDiagOptions->bWriteTpaSigniture = true;
         if ((iOption + 2) >= argc) 
            return DIAG_INVALID_PARAMETERS;                 // we need three more parameters (type and serial number)
         else
            {
            ptyDiagOptions->pszTpaType = argv[iOption];
            ptyDiagOptions->pszTpaSerialNumber = argv[iOption+1];
            ptyDiagOptions->pszTpaRevision = argv[iOption+2];
            iOption += 3;
            if ((iOption < argc) && (!strcmp(argv[iOption], OPT_INSTANCE)))
               continue;
            }
         break;
         }

      if (!strcmp(argv[iOption], OPT_JTAGCON))
         {
         iOption++;
         ptyDiagOptions->bRunJtagConsole = true;

         if (iOption < argc)
            {
            if (!strcmp(argv[iOption], OPT_INSTANCE) || !strcmp(argv[iOption], OPT_DEVICE))
               continue;
            }
         else
            break;
         }

      if (!strcmp(argv[iOption], OPT_FIRMWARE))
         {
         iOption++;
         ptyDiagOptions->bUpdateFirmware = true;
         continue;
         }

      if (!strcmp(argv[iOption], OPT_PRODUCTION))
         {
         iOption++;
         ptyDiagOptions->bProductionBoardRequired = true;
         continue;
         }

      if (!strcmp(argv[iOption], OPT_TPA_PRODUCTION))
         {
         iOption++;
         ptyDiagOptions->bProductionTpa = true;
         continue;
         }

      if (!strcmp(argv[iOption], OPT_TPA_ADAPTOR))
         {
         iOption++;
         ptyDiagOptions->bProductionTpaAdaptor = true;
         continue;
         }

      // unknown parameter
      return DIAG_INVALID_PARAMETERS;
      }

   return DIAG_NO_ERROR;                        
}


/****************************************************************************
     Function: CheckSerialNumberFormat
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - pointer to string with serial number
       Output: int32_t - 0 if format is ok
  Description: verify serial number format 
               only 'a'-'z', 'A'-'Z' and '0'-'9' characters are allowed
               maximum 15 character, at least 1
Date           Initials    Description
26-Sep-2006    VH          Initial
***************************************************************************/
int32_t CheckSerialNumberFormat(char *pszSerialNumber)
{
   char *pcPtr;

   assert(pszSerialNumber != NULL);

   // check length
   if ((strlen(pszSerialNumber) > 15) || (strlen(pszSerialNumber) < 1))
      return(1);

   // check each character, only [0-9],[a-z] and [A-Z] are allowed
   pcPtr = pszSerialNumber;
   while (*pcPtr)
      {
      char cChar = *pcPtr++;
      if (((cChar >= '0') && (cChar <= '9')) || 
          ((cChar >= 'a') && (cChar <= 'z')) || 
          ((cChar >= 'A') && (cChar <= 'Z'))
          )
         continue;
      else
         return(2);
      }

   return(0);
}


/****************************************************************************
     Function: CheckRevisionFormat
     Engineer: Vitezslav Hola
        Input: char *pszRevisionNumber - pointer to string with PCB revision
       Output: int32_t - 0 if format is ok
  Description: verify PCB revision format (Rxxx-Mxxx-Exxx) 
Date           Initials    Description
03-Aug-2006    VH          Initial
***************************************************************************/
int32_t CheckRevisionFormat(char *pszRevisionNumber)
{
   uint32_t uiRe, uiMe, uiEe;

   assert(pszRevisionNumber != NULL);

   // shortest string is "R0-M0-E0" (8 characters)
   // longest string is "R000-M000-E000" (14 characters)
   if ((strlen(pszRevisionNumber) > 14) || (strlen(pszRevisionNumber) < 8))
      return 1;

   if (sscanf(pszRevisionNumber, "r%u-m%u-e%u", &uiRe, &uiMe, &uiEe) != 3)
      return 2;

   // check values
   if ((uiRe > 255) || (uiMe > 255) || (uiEe > 255))
      return 3;

   return(0);
}


/****************************************************************************
     Function: GetCurrentDate
     Engineer: Vitezslav Hola
        Input: none
       Output: uint32_t - date in Opella-XD format
  Description: return current date in Opella-XD format
Date           Initials    Description
05-Mar-2007    VH          Initial
***************************************************************************/
uint32_t GetCurrentDate(void)
{
   time_t tmCalTime;
   struct tm *ptyLocalTime;
   uint32_t ulDate, ulYear, ulMonth, ulDay;
   uint32_t ulValue;

   // get date from actual time
   tmCalTime = time(NULL);
   ptyLocalTime = localtime(&tmCalTime);
   // convert time structure to BCD code
   ulValue = (uint32_t)ptyLocalTime->tm_mday;    // tm_mday [1,31]
   ulDay = (ulValue % 10);       ulValue /= 10;       ulDay += (ulValue * 0x10);

   ulValue = (uint32_t)ptyLocalTime->tm_mon + 1; // tm_mon [0,11]
   ulMonth = (ulValue % 10);     ulValue /= 10;       ulMonth += (ulValue * 0x10);

   ulValue = 1900 + (uint32_t)ptyLocalTime->tm_year;
   ulYear = (ulValue % 10);            ulValue /= 10;       
   ulYear += (ulValue % 10) * 0x10;    ulValue /= 10;
   ulYear += (ulValue % 10) * 0x100;   ulValue /= 10;
   ulYear += (ulValue % 10) * 0x1000;  ulValue /= 10;

   ulDate = ulYear | (ulMonth << 16) | (ulDay << 24);
   return (ulDate);
}


/****************************************************************************
     Function: ConvertProgramOptionsToLowercase
     Engineer: Vitezslav Hola
        Input: int32_t argc - number of options
               char *argv[] - parameters
       Output: none
  Description: convert all program input parameters to lower case
Date           Initials    Description
16-Apr-2007    VH          Initial
***************************************************************************/
void ConvertProgramOptionsToLowercase(int32_t argc, char *argv[])
{
   char *pcString;
   int32_t iCnt;

   // convert all parameters
   for (iCnt=0; iCnt<argc; iCnt++)
      {
      pcString = argv[iCnt];
      while (*pcString)
         {
         if ((*pcString >= 'A') && (*pcString <= 'Z'))
            *pcString -= ('A' - 'a');                             // subtract offset between lower and upper case
         pcString++;
         }
      }
}


