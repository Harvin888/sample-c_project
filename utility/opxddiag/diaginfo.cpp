/******************************************************************************
       Module: diaginfo.h
     Engineer: Vitezslav Hola
  Description: Implementation of diagnostic information
  Date           Initials    Description
  02-Oct-2006    VH          Initial
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "diaginfo.h"
#include "diagerr.h"
#include "protocol/common/drvopxd.h"

// defines
#define BUFFER_LEN                     10
#define OP4_FW_ID_FLAG                 0xC0DE1234

// local variables
static uint32_t pulBuffer[BUFFER_LEN];             // buffer for file headers

/****************************************************************************
     Function: GetFirmwareInfo
     Engineer: Vitezslav Hola
        Input: char *pszFirmwareFilename - filename with current firmware
               TyFirmwareInfo *ptyFirmwareInfo - pointer to structure for firmware info
       Output: int32_t - error codes DIAG_xxx
  Description: obtain information about current firmware version and date
Date           Initials    Description
02-Oct-2006    VH          Initial
****************************************************************************/
int32_t GetFirmwareInfo(char *pszFirmwareFilename, TyFirmwareInfo *ptyFirmwareInfo)
{ 
   FILE *pFwFile;

   uint32_t ulBytesRead, ulDate, ulVersion;

   assert(pszFirmwareFilename != NULL);
   assert(ptyFirmwareInfo != NULL);

   // open firmware file and read header
   pFwFile = fopen(pszFirmwareFilename, "rb");
   if (pFwFile == NULL)
      return(DIAG_INVALID_FIRMWARE);

   ulBytesRead = (uint32_t)fread((char *)pulBuffer, sizeof(uint32_t), BUFFER_LEN, pFwFile);
   if (ulBytesRead != BUFFER_LEN)
      {
      fclose(pFwFile);
      return(DIAG_INVALID_FIRMWARE);             // too short header
      }

   fclose(pFwFile);

   // check header
   if (pulBuffer[0] != OP4_FW_ID_FLAG)
      return(DIAG_INVALID_FIRMWARE);             // invalid id code
   // get version and date
   ulVersion = pulBuffer[5];
   ulDate = pulBuffer[6];

   // now convert values to string
   strcpy(ptyFirmwareInfo->pszVersion, "");
   strcpy(ptyFirmwareInfo->pszDate, "");

   ConvertVersionString(ulVersion, ptyFirmwareInfo->pszVersion);
   ConvertDateString(ulDate, ptyFirmwareInfo->pszDate);

   return(DIAG_NO_ERROR);
}


/****************************************************************************
     Function: GetDiskwareInfo
     Engineer: Vitezslav Hola
        Input: char *pszFirmwareFilename - filename with current diskware
               TyDiskwareInfo *ptyDiskwareInfo - pointer to structure for diskware info
       Output: int32_t - error codes DIAG_xxx
  Description: obtain information about diskware version, date and type
Date           Initials    Description
03-Oct-2006    VH          Initial
****************************************************************************/
int32_t GetDiskwareInfo(char *pszDiskwareFilename, TyDiskwareInfo *ptyDiskwareInfo)
{ 
   FILE *pDwFile;

   uint32_t ulBytesRead, ulDate, ulVersion, ulType, ulReqFwVersion;

   assert(pszDiskwareFilename != NULL);
   assert(ptyDiskwareInfo != NULL);

   // open diskware file and read header
   pDwFile = fopen(pszDiskwareFilename, "rb");
   if (pDwFile == NULL)
      return(DIAG_INVALID_DISKWARE);

   ulBytesRead = (uint32_t)fread((char *)pulBuffer, sizeof(uint32_t), BUFFER_LEN, pDwFile);
   if (ulBytesRead != BUFFER_LEN)
      {
      fclose(pDwFile);
      return(DIAG_INVALID_DISKWARE);             // too short header
      }

   fclose(pDwFile);

   // check reserved value for 0
   if (pulBuffer[7] != 0x00000000)
      return(DIAG_INVALID_DISKWARE);             // invalid id code

   // get values
   ulType = pulBuffer[1];
   ulVersion = pulBuffer[4];
   ulDate = pulBuffer[5];
   ulReqFwVersion = pulBuffer[6];

   // initialize structure for diskware info
   strcpy(ptyDiskwareInfo->pszVersion, "");
   strcpy(ptyDiskwareInfo->pszDate, "");
   strcpy(ptyDiskwareInfo->pszReqFwVersion, "");
   ptyDiskwareInfo->tyDwType = DW_UNKNOWN;

   ConvertVersionString(ulVersion, ptyDiskwareInfo->pszVersion);
   ConvertVersionString(ulReqFwVersion, ptyDiskwareInfo->pszReqFwVersion);
   ConvertDateString(ulDate, ptyDiskwareInfo->pszDate);

   switch(ulType & 0xff000000)
      {
      case 0x01000000:
         ptyDiskwareInfo->tyDwType = DW_MIPS;
         break;
      case 0x02000000:
         ptyDiskwareInfo->tyDwType = DW_ARM;
         break;
      case 0x03000000:
         ptyDiskwareInfo->tyDwType = DW_ARC;
         break;
      case 0xFF000000:
         ptyDiskwareInfo->tyDwType = DW_DIAG;
         break;
      default:
         // unknown diskware
         break;
      }

   return(DIAG_NO_ERROR);
}


/****************************************************************************
     Function: GetDeviceInfo
     Engineer: Vitezslav Hola
        Input: char *pszSerialNumber - device serial number
               TyDevInfo *ptyDevInfo - pointer to structure with device info
       Output: int32_t - error codes DIAG_xxx
  Description: obtain information about connected device
Date           Initials    Description
03-Oct-2006    VH          Initial
****************************************************************************/
int32_t GetDeviceInfo(char *pszSerialNumber, TyDevInfo *ptyDevInfo)
{
   TyDevHandle tyHandle;
   TyDeviceInfo tyDeviceInfo;
   TyError tyRes;

   assert(pszSerialNumber != NULL);
   assert(ptyDevInfo != NULL);

   strcpy(ptyDevInfo->pszFwDate, "");
   strcpy(ptyDevInfo->pszFwVersion, "");
   strcpy(ptyDevInfo->pszManufDate, "");
   strcpy(ptyDevInfo->pszBoardRevision, "");

   tyRes = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle);
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return DIAG_INVALID_SERIAL_NUMBER;
      default:
         return DIAG_DRIVER_ERROR;
      }

   //TODO: Diskware type now hard coded to ARC. Change it based on device selection
   if (DL_OPXD_GetDeviceInfo(tyHandle, &tyDeviceInfo, DW_ARC) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyHandle);
      return(DIAG_DRIVER_ERROR);
      }

   ConvertVersionString(tyDeviceInfo.ulFirmwareVersion, ptyDevInfo->pszFwVersion);
   ConvertDateString(tyDeviceInfo.ulFirmwareDate, ptyDevInfo->pszFwDate);
   ConvertDateString(tyDeviceInfo.ulManufacturingDate, ptyDevInfo->pszManufDate);
   ConvertPcbVersionString(tyDeviceInfo.ulBoardRevision, ptyDevInfo->pszBoardRevision);
   ptyDevInfo->tyTpaType = tyDeviceInfo.tyTpaConnected;
   ConvertPcbVersionString(tyDeviceInfo.ulTpaRevision, ptyDevInfo->pszTpaRevision);
   strncpy(ptyDevInfo->pszTpaSerialNumber, tyDeviceInfo.pszTpaSerialNumber, INFO_SERIAL_NUMBER_LEN);
   ptyDevInfo->pszTpaSerialNumber[INFO_SERIAL_NUMBER_LEN-1] = 0;
   (void)DL_OPXD_CloseDevice(tyHandle);
   return(DIAG_NO_ERROR);
}

/****************************************************************************
     Function: ConvertVersionString
     Engineer: Vitezslav Hola
        Input: uint32_t ulVersion - version
               char *pszVersionString - buffer for version string
       Output: none
  Description: convert version format to string
Date           Initials    Description
03-Oct-2006    VH          Initial
****************************************************************************/
void ConvertVersionString(uint32_t ulVersion, char *pszVersionString)
{
   uint32_t uiVal1, uiVal2, uiVal3;

   assert(pszVersionString != NULL);
   // decode firmware version
   uiVal1 = (uint32_t)((ulVersion & 0xff000000) >> 24);
   uiVal2 = (uint32_t)((ulVersion & 0x00ff0000) >> 16);
   uiVal3 = (uint32_t)((ulVersion & 0x0000ff00) >> 8);
   if (ulVersion & 0x000000ff)
      sprintf(pszVersionString, "v%x.%x.%x-%c", uiVal1, uiVal2, uiVal3, (char)(ulVersion & 0x000000ff));
   else
      sprintf(pszVersionString, "v%x.%x.%x", uiVal1, uiVal2, uiVal3);
}

/****************************************************************************
     Function: ConvertPcbVersionString
     Engineer: Vitezslav Hola
        Input: uint32_t ulVersion - version
               char *pszVersionString - buffer for version string
       Output: none
  Description: convert PCB version to string
Date           Initials    Description
16-Apr-2007    VH          Initial
****************************************************************************/
void ConvertPcbVersionString(uint32_t ulVersion, char *pszVersionString)
{
   uint32_t uiRVal, uiEVal, uiMVal;

   assert(pszVersionString != NULL);
   // decode firmware version
   if (ulVersion & 0x000000FF)
      {  // lowest byte is not 0, set all to 0s
      uiRVal = 0;
      uiEVal = 0;
      uiMVal = 0;
      }
   else
      {  // lowest byte is 0 as expected
      uiRVal = (uint32_t)((ulVersion & 0xFF000000) >> 24);
      uiEVal = (uint32_t)((ulVersion & 0x00FF0000) >> 16);
      uiMVal = (uint32_t)((ulVersion & 0x0000FF00) >> 8);
      }
   sprintf(pszVersionString, "R%d-M%d-E%d", uiRVal, uiMVal, uiEVal);
}

/****************************************************************************
     Function: ConvertDateString
     Engineer: Vitezslav Hola
        Input: uint32_t ulDate - date
               char *pszDateString - buffer for date string
       Output: none
  Description: convert date format to string
Date           Initials    Description
03-Oct-2006    VH          Initial
09-Nov-2007    VH          Fixed bug with Oct and Nov
****************************************************************************/
void ConvertDateString(uint32_t ulDate, char *pszDateString)
{
   uint32_t uiVal1, uiVal2, uiVal3;

   assert(pszDateString != NULL);
   uiVal1 = (uint32_t)((ulDate & 0xff000000) >> 24);
   uiVal2 = (uint32_t)((ulDate & 0x00ff0000) >> 16);
   uiVal3 = (uint32_t)(ulDate & 0x0000ffff);
   switch (uiVal2)
      {
      case 0x01:
         sprintf(pszDateString, "%x-Jan-%x", uiVal1, uiVal3);
         break;
      case 0x02:
         sprintf(pszDateString, "%x-Feb-%x", uiVal1, uiVal3);
         break;
      case 0x03:
         sprintf(pszDateString, "%x-Mar-%x", uiVal1, uiVal3);
         break;
      case 0x04:
         sprintf(pszDateString, "%x-Apr-%x", uiVal1, uiVal3);
         break;
      case 0x05:
         sprintf(pszDateString, "%x-May-%x", uiVal1, uiVal3);
         break;
      case 0x06:
         sprintf(pszDateString, "%x-Jun-%x", uiVal1, uiVal3);
         break;
      case 0x07:
         sprintf(pszDateString, "%x-Jul-%x", uiVal1, uiVal3);
         break;
      case 0x08:
         sprintf(pszDateString, "%x-Aug-%x", uiVal1, uiVal3);
         break;
      case 0x09:
         sprintf(pszDateString, "%x-Sep-%x", uiVal1, uiVal3);
         break;
      case 0x10:
         sprintf(pszDateString, "%x-Oct-%x", uiVal1, uiVal3);
         break;
      case 0x11:
         sprintf(pszDateString, "%x-Nov-%x", uiVal1, uiVal3);
         break;
      default:    // so it must be december
         sprintf(pszDateString, "%x-Dec-%x", uiVal1, uiVal3);
         break;
      }
}

