/******************************************************************************
       Module: diagerr.h
     Engineer: Vitezslav Hola
  Description: Header with error codes for OpXDDiag
  Date           Initials    Description
  26-Sep-2006    VH          Initial
******************************************************************************/
#ifndef _DIAGERR_H_
#define _DIAGERR_H_

// return codes
#define DIAG_NO_ERROR                     0

// diagnostic error codes
#define DIAG_TEST_CPU_ERROR               2
#define DIAG_TEST_LED_ERROR               3
#define DIAG_TEST_SRAM_ERROR              4
#define DIAG_TEST_VOLTAGE_ERROR           5
#define DIAG_TEST_FPGA_ERROR              6
#define DIAG_TEST_CLOCK_ERROR             7
#define DIAG_TEST_PERFORMANCE_ERROR       8
#define DIAG_TEST_INTERNAL_LOOP_ERROR     9
#define DIAG_TEST_TPA_INTERFACE_ERROR     10
#define DIAG_TEST_TPA_LOOP_ERROR          11

// general error codes
#define DIAG_INVALID_PARAMETERS           100
#define DIAG_DRIVER_ERROR                 101
#define DIAG_INVALID_SERIAL_NUMBER        102
#define DIAG_MISSING_SERIAL_NUMBER        103
#define DIAG_NO_DEVICE_CONNECTED          104
#define DIAG_INVALID_TEST_SELECTION       105
#define DIAG_MANUF_FILE_ERROR             106
#define DIAG_INVALID_LOADER               107
#define DIAG_INVALID_FIRMWARE             108
#define DIAG_UPDATE_REQUIRED              109
#define DIAG_UPDATE_FAILED                110
#define DIAG_INVALID_DISKWARE             120
#define DIAG_INVALID_FPGAWARE             121
#define DIAG_INVALID_TPA                  122
#define DIAG_TEST_OPELLA_ERROR            150
#define DIAG_TEST_PC_ERROR                151
#define DIAG_TEST_TPA_ERROR               152
#define DIAG_INVALID_TARGET_DEVICE        153
#define DIAG_INVALID_REVISION_NUMBER      160


#define DIAG_NO_TPA_FOR_JTAG_CONSOLE      200
#define DIAG_UNSUPPORTED_TPA              201

#define DIAG_TPA_TEST_SCANCHAIN_ERROR     300
#define DIAG_TPA_TEST_ID_ERROR            301
#define DIAG_TPA_TEST_PIN                 302

// error messages text
#define DIAG_INVALID_PARAMETERS_MSG       "Invalid program parameters"
#define DIAG_DRIVER_ERROR_MSG             "Cannot access Opella-XD driver (possibly in-use by another application?)"
#define DIAG_INVALID_SERIAL_NUMBER_MSG    "Cannot find Opella-XD with selected serial number"
#define DIAG_MISSING_SERIAL_NUMBER_MSG    "Please specify Opella-XD serial number"
#define DIAG_NO_DEVICE_CONNECTED_MSG      "No Opella-XD connected to the PC"
#define DIAG_INVALID_TEST_SELECTION_MSG   "Invalid diagnostic test selected"

#define DIAG_UPDATE_REQUIRED_MSG          "Please update firmware using --update-firmware option"
#define DIAG_UPDATE_FAILED_MSG            "Firmware upgrade failed. Please cycle power and retry."

#define DIAG_INACCESSIBLE_DEVICE          "Cannot obtain detailed information (possibly in-use by another application)"

#define DIAG_MANUF_FILE_ERROR_MSG         "Cannot create manufacturing file."
#define DIAG_INVALID_LOADER_MSG           "Cannot find loader file"
#define DIAG_INVALID_FIRMWARE_MSG         "Cannot find firmware file"
#define DIAG_INVALID_DISKWARE_MSG         "Cannot find diskware file"
#define DIAG_INVALID_FPGAWARE_MSG         "Cannot find FPGAware file"
#define DIAG_INVALID_TPA_MSG              "Invalid or unknown TPA connected to Opella-XD"

#define DIAG_INVALID_REVISION_NUMBER_MSG  "Invalid PCB revision format"

#define DIAG_TEST_OPELLA_ERROR_MSG        "Test failed\nSee diagnostic results for more details"
#define DIAG_TEST_PC_ERROR_MSG            "Test failed due to USB port failure\nSee diagnostic results for more details"
#define DIAG_TEST_TPA_ERROR_MSG           "Test failed\nCheck if TPA is not damaged"

#define DIAG_NO_TPA_FOR_JTAG_CONSOLE_MSG  "Error: JTAG console requires a TPA connected."
#define DIAG_UNSUPPORTED_TPA_MSG          "Missing or unsupported TPA connected to Opella-XD."

#define DIAG_TPA_TEST_SCANCHAIN_ERROR_MSG "Test failed due to scanchain error."
#define DIAG_TPA_TEST_ID_ERROR_MSG        "Test failed due to invalid TAP ID."
#define DIAG_TPA_TEST_PIN_MSG             "Test failed due to specific pin error\nSee diagnostic results for more details."
#define DIAG_INVALID_TARGET_DEVICE_MSG    "Invalid Target device (--device Option)"

#define DIAG_UNKNOWN_ERROR_MSG            "An unknown error occurred"

#endif // _DIAGERR_H_


