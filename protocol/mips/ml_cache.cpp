/****************************************************************************
       Module: ml_cache.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This section contains all function related to MIPS cache 
               operations.
Date           Initials    Description
08-Feb-2008    VH          Initial
23-Dec-2008    SPC         Returning errors more properly.
06-Aug-2009    SPC         Made cache flush routine run from probe
****************************************************************************/

#define MDI_LIB               // We are an MDI Library not a debugger

#include <stdio.h>
#include <string.h>
#include <assert.h>
#ifndef __LINUX
// Windows specific includes
#else
// Linux includes
#endif
#include "commdefs.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "mips.h"
#include "mdi.h"
#include "midlayer.h"
#include "gerror.h"
#include "ml_error.h"
#include "debug.h"
#include "../../utility/debug/debug.h"

// local constants
//#define INDEX_INVALIDATE_I                         0x0         
//#define INDEX_WRITEBACK_INV_D                      0x1         
//#define INDEX_INVALIDATE_SI                        0x2         
//#define INDEX_WRITEBACK_INV_SD                     0x3         
#define INDEX_LOAD_TAG_I                           0x4            // action depends on ErrCtl for SmartMIPS
#define INDEX_LOAD_TAG_D                           0x5            // action depends on ErrCtl for SmartMIPS
//#define INDEX_LOAD_TAG_SI                          0x6            // action depends on ErrCtl for SmartMIPS
//#define INDEX_LOAD_TAG_SD                          0x7            // action depends on ErrCtl for SmartMIPS
#define INDEX_STORE_TAG_I                          0x8            // action depends on ErrCtl for SmartMIPS
#define INDEX_STORE_TAG_D                          0x9            // action depends on ErrCtl for SmartMIPS
//#define INDEX_STORE_TAG_SI                         0xA            // action depends on ErrCtl for SmartMIPS
//#define INDEX_STORE_TAG_SD                         0xB            // action depends on ErrCtl for SmartMIPS
#define INDEX_STORE_DATA_I                         0xC            // SmartMIPS only, action depends on ErrCtl
#define INDEX_STORE_DATA_D                         0xD            // SmartMIPS only, action depends on ErrCtl
//#define INDEX_STORE_DATA_SI                        0xE            // SmartMIPS only, action depends on ErrCtl
//#define INDEX_STORE_DATA_SD                        0xF            // SmartMIPS only, action depends on ErrCtl
#define HIT_INVALIDATE_I                           0x10        
#define HIT_INVALIDATE_D                           0x11        
//#define HIT_INVALIDATE_SI                          0x12        
//#define HIT_INVALIDATE_SD                          0x13        
//#define FILL_I                                     0x14        
#define HIT_WRITEBACK_INV_D                        0x15        
//#define HIT_WRITEBACK_INV_SD                       0x17        
#define HIT_WRITEBACK_D                            0x19        
//#define HIT_WRITEBACK_SD                           0x1B        
//#define FETCH_LOCK_I                               0x1C        
//#define FETCH_LOCK_D                               0x1D        
#define BYTE_INDEX_BIT_SHIFT                       0x4
#define BYTE_INDEX_MASK                            0xF

/* Config1 Cache control definitions */
#define CFG1_ISMASK	0x01c00000	/* icache lines 64<<n */
#define CFG1_ISSHIFT	22
#define CFG1_ILMASK	0x00380000	/* icache line size 2<<n */
#define CFG1_ILSHIFT	19
#define CFG1_IAMASK	0x00070000	/* icache ways - 1 */
#define CFG1_IASHIFT	16
#define CFG1_DSMASK	0x0000e000	/* dcache lines 64<<n */
#define CFG1_DSSHIFT	13
#define CFG1_DLMASK	0x00001c00	/* dcache line size 2<<n */
#define CFG1_DLSHIFT	10
#define CFG1_DAMASK	0x00000380	/* dcache ways - 1 */
#define CFG1_DASHIFT	7

/* Config2 Cache control definitions */
#define CFG2_SSMASK	0x00000f00	/* scache sets per wway 64<<n */
#define CFG2_SSSHIFT	8
#define CFG2_SLMASK	0x000000f0	/* scache line size 2<<n */
#define CFG2_SLSHIFT	4
#define CFG2_SAMASK	0x0000000f	/* scache ways - 1 */
#define CFG2_SASHIFT	0

// local type definition
typedef struct _TyRangeStruct {
   unsigned long ulStart;
   unsigned long ulLength;
} TyRangeStruct;

// local function prototypes
static unsigned char CheckOverlappingRange(unsigned long ulStart1, unsigned long ulEnd1, unsigned long ulStart2, unsigned long ulEnd2, TyRangeStruct *ptyRange);
static unsigned char CheckKSEG0Cached(unsigned long ulConfig0);
static void MLC_SetupErrCtlValue(TyMLHandle *ptyHandle, unsigned char bWaySelect, unsigned char *pbRestoreRequired, unsigned long *pulOriginalValue);
static void MLC_RestoreErrCtlValue(TyMLHandle *ptyHandle, unsigned char bRestoreRequired, unsigned long ulOriginalValue);
static int MLC_ReadCacheRange(TyMLHandle *ptyHandle, unsigned long ulSrcResource, unsigned long ulStartCacheIndex, unsigned long ulEndCacheIndex,
                              TyCacheEntry *ptyData, unsigned long ulCount, unsigned char bScratchpadRam);

/****************************************************************************
     Function: MLC_InvalidateCacheOnReset
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: int - error code
  Description: check if we need to invalidate cache after reset and
               do it in that case
Date           Initials    Description
08-Feb-2008    VH          Initial
06-Aug-2009    SPC         Made cache flush routine run from probe
****************************************************************************/
int MLC_InvalidateCacheOnReset(TyMLHandle *ptyHandle)
{
   int iResult = ERR_NO_ERROR;
   assert(ptyHandle != NULL);
   // check if wee need to invalidate cache
   if (ptyHandle->bInvalidateCacheOnReset)
      {  // ok, we need to invalidate cache, just call function to do it
      iResult = MLC_FlushCache(ptyHandle, CACHE_TYPE_UNIFIED, CACHE_FLUSH_INVALIDATE);
      }
   return iResult;
}

/****************************************************************************
     Function: MLC_ReadCacheResourceArrayRange
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulSrcResource - source resource
               unsigned long ulStartEntry - start entry
               unsigned long ulEndEntry - end entry
               void *pvData - pointer to data
               unsigned char bScratchpadRam - scratch pad ram
       Output: int - error code
  Description: Read a range of cache entries.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_ReadCacheResourceArrayRange(TyMLHandle *ptyHandle, unsigned long ulSrcResource, unsigned long ulStartEntry, unsigned long ulEndEntry, 
                                    void *pvData, unsigned char bScratchpadRam)
{
   int iResult;
   unsigned long ulLength, ulLoopIncrement, ulIndexScalar, ulStartCacheIndex, ulEndCacheIndex;
   unsigned char bWaySelect;
   unsigned char *pucLocData;
   TyCacheEntry *ptyEntries = NULL;
   // check parameters
   assert(ptyHandle != NULL);
   assert(ptyHandle->ptyCacheEntries != NULL);
   if ((pvData == NULL) || (ulEndEntry < ulStartEntry))
      return ERR_NO_ERROR;
   ulLength = (ulEndEntry - ulStartEntry) + 1;                          // number of entries
   switch (ulSrcResource)   // TODO : Extend this to support Cache etc.
      {
      case MDIMIPPICACHET:
      case MDIMIPPDCACHET:    // reading from tag array (1 word with value + 1 dummy word)
         ulLoopIncrement = 2;       ulIndexScalar = 2;      bWaySelect = 0;
         break;
      case MDIMIPSICACHEWS:
      case MDIMIPSDCACHEWS:   // reading from tag array (1 word with value + 1 dummy word)
         ulLoopIncrement = 2;       ulIndexScalar = 2;      bWaySelect = 1;
         break;
      case MDIMIPPDCACHE:
      case MDIMIPPICACHE:     // reading from data array (4 words)
         ulLoopIncrement = 4;       ulIndexScalar = 4;      bWaySelect = 0;
         break;
      default:                // unsupported option
         return ERR_NO_ERROR;
      }
   ulStartCacheIndex = (ulStartEntry/ulIndexScalar) << BYTE_INDEX_BIT_SHIFT;
   ulEndCacheIndex   = ulStartCacheIndex + (((ulLength / ulIndexScalar) - 1) << BYTE_INDEX_BIT_SHIFT);
   ptyEntries = ptyHandle->ptyCacheEntries;
   // call function to read entries (number of entries is length in words divided by loop increment (words per each entry))
   iResult = MLC_ReadCacheRange(ptyHandle, ulSrcResource, ulStartCacheIndex, ulEndCacheIndex, ptyEntries, ulLength / ulLoopIncrement, bScratchpadRam);
   // we need to pick something from each entry
   pucLocData = (unsigned char *)pvData;
   ptyEntries = ptyHandle->ptyCacheEntries;
   while (ulLength)
      {
      if (ulLoopIncrement == 2)
         {  // we have 1 word value + 1 dummy value per each entry
         unsigned long pulLocValue[2]; 
         if (bWaySelect)
            pulLocValue[0] = ptyEntries->ulWaySelect;
         else
            pulLocValue[0] = ptyEntries->ulTagLo;
         pulLocValue[1] = 0;                                            // dummy word
         memcpy((void *)pucLocData, (void *)pulLocValue, 2 * sizeof(unsigned long));
         pucLocData += (2 * sizeof(unsigned long));
         }
      else
         {  // we have 4 data words
         memcpy((void *)pucLocData, (void *)&(ptyEntries->pulDataLo[0]), 4 * sizeof(unsigned long));
         pucLocData += (4 * sizeof(unsigned long));
         }
      ulLength -= ulLoopIncrement;
      ptyEntries++;
      }
   return iResult;
}

/****************************************************************************
     Function: MLC_FlushCache
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               TyCacheType tyCacheType - cache type
               TyCacheFlush tyFlushType - flush operation type
       Output: int - error code
  Description: flush cache content by running it from probe itself
Date           Initials    Description
06-Aug-2008    SPC         Initial
****************************************************************************/
int MLC_FlushCache(TyMLHandle *ptyHandle, 
                   unsigned int tyCacheType, 
                   TyCacheFlush tyFlushType)
{
   int iResult = ERR_NO_ERROR;
   unsigned long ulCP0ConfigRegister = 0;
   unsigned long ulCP0Config1Register = 0;
   unsigned long ulCP0Config2Register = 0;
   unsigned char bCacheInstructionHasBeenModified = 0;
   unsigned long ulBackupOfOriginalCacheInstruction=0x0,ulLocationOfCacheInstruction;
   unsigned long pulDataToProc[3];
   unsigned long pulDataFromProc[3];

   /* Currently we are only supporting the Index invalidate for Instruction cache and 
   	  Index Writeback invalidate for data and secondary caches */
   tyFlushType = tyFlushType; //Make Lint happy

   (void)MLR_ReadRegister(ptyHandle, ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, &ulCP0ConfigRegister);
   if (!CheckKSEG0Cached(ulCP0ConfigRegister))
      return ERR_NO_ERROR;

   //Read Config1 (CP0 Register 16, Select 1) register to get cache size
   (void)MLR_ReadCP0Register(ptyHandle, 16, &ulCP0Config1Register, 1);

   //Read Config2 (CP0 Register 16, Select 2) register to get L2 cache size
   (void)MLR_ReadCP0Register(ptyHandle, 16, &ulCP0Config2Register, 2);

   ulLocationOfCacheInstruction = ptyHandle->tyMipsDW2App.ulCacheFlushInstrAddress;
   ulBackupOfOriginalCacheInstruction = ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulLocationOfCacheInstruction];
   /* 0xBCE00000, cache	0x0,0(a3) */
   /* 0xBCE10000, cache	0x1,0(a3) */
   /* 0xBCE30000, cache	0x3,0(a3) */

   if (tyCacheType & CACHE_TYPE_INST)
	  {
	  unsigned long ulISmasked,ulISval,ulIS;
	  unsigned long ulILmasked,ulILval,ulIL;
	  unsigned long ulIAmasked,ulIAval,ulIA;
	  unsigned long ulICacheSize;
	  //Getting IS
	  ulISmasked = ulCP0Config1Register & CFG1_ISMASK;
	  ulISval = ulISmasked >> CFG1_ISSHIFT;
	  ulIS = 64 << ulISval;

	  //Getting IL
	  ulILmasked = ulCP0Config1Register & CFG1_ILMASK;
	  ulILval = ulILmasked >> CFG1_ILSHIFT;
	  ulIL = 2 << ulILval;

	  //Getting IA
	  ulIAmasked = ulCP0Config1Register & CFG1_IAMASK;
	  ulIAval = ulIAmasked >> CFG1_IASHIFT;
	  ulIA = ulIAval + 1;

	  ulICacheSize = ulIS*ulIL*ulIA;
	  pulDataToProc[0] = ulICacheSize;
	  pulDataToProc[1] = ulIL;

	  ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulLocationOfCacheInstruction] = 0xBCE00000; /* 0xBCE00000, cache	0x0,0(a3) */
	  bCacheInstructionHasBeenModified = 1;

	  iResult=ML_ExecuteDW2(ptyHandle,
							ptyHandle->tyMipsDW2App.pulCacheFlushApp,
							ptyHandle->tyMipsDW2App.ulCacheFlushInstructions*4,
							(void *)pulDataToProc, 
							2 * sizeof(unsigned long), 
						   (void *)pulDataFromProc, 
							2 * sizeof(unsigned long));
	  }
   if (tyCacheType & CACHE_TYPE_DATA)
	  {
	  unsigned long ulDSmasked,ulDSval,ulDS;
	  unsigned long ulDLmasked,ulDLval,ulDL;
	  unsigned long ulDAmasked,ulDAval,ulDA;
	  unsigned long ulDCacheSize;
	  //Getting DS
	  ulDSmasked = ulCP0Config1Register & CFG1_DSMASK;
	  ulDSval = ulDSmasked >> CFG1_DSSHIFT;
	  ulDS = 64 << ulDSval;

	  //Getting DL
	  ulDLmasked = ulCP0Config1Register & CFG1_DLMASK;
	  ulDLval = ulDLmasked >> CFG1_DLSHIFT;
	  ulDL = 2 << ulDLval;

	  //Getting DA
	  ulDAmasked = ulCP0Config1Register & CFG1_DAMASK;
	  ulDAval = ulDAmasked >> CFG1_DASHIFT;
	  ulDA = ulDAval + 1;

	  ulDCacheSize = ulDS*ulDL*ulDA;
	  pulDataToProc[0] = ulDCacheSize;
	  pulDataToProc[1] = ulDL;

	  ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulLocationOfCacheInstruction] = 0xBCE10000; /* 0xBCE10000, cache	0x1,0(a3) */
	  bCacheInstructionHasBeenModified = 1;

	  iResult=ML_ExecuteDW2(ptyHandle,
							ptyHandle->tyMipsDW2App.pulCacheFlushApp,
							ptyHandle->tyMipsDW2App.ulCacheFlushInstructions*4,
							(void *)pulDataToProc, 
							2 * sizeof(unsigned long), 
						   (void *)pulDataFromProc, 
							2 * sizeof(unsigned long));
	  }
   if (tyCacheType & CACHE_TYPE_SECONDARY)
      {
	  unsigned long ulSSmasked,ulSSval,ulSS;
	  unsigned long ulSLmasked,ulSLval,ulSL;
	  unsigned long ulSAmasked,ulSAval,ulSA;
	  unsigned long ulSCacheSize;

	  //Getting DS
	  ulSSmasked = ulCP0Config2Register & CFG2_SSMASK;
	  ulSSval = ulSSmasked >> CFG2_SSSHIFT;
	  ulSS = 64 << ulSSval;

	  //Getting DL
	  ulSLmasked = ulCP0Config2Register & CFG2_SLMASK;
	  ulSLval = ulSLmasked >> CFG2_SLSHIFT;
	  ulSL = 2 << ulSLval;

	  //Getting DA
	  ulSAmasked = ulCP0Config2Register & CFG2_SAMASK;
	  ulSAval = ulSAmasked >> CFG2_SASHIFT;
	  ulSA = ulSAval + 1;

	  ulSCacheSize = ulSS*ulSL*ulSA;
	  pulDataToProc[0] = ulSCacheSize;
	  pulDataToProc[1] = ulSL;

	  ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulLocationOfCacheInstruction] = 0xBCE30000; /* 0xBCE30000, cache	0x3,0(a3) */ 
	  bCacheInstructionHasBeenModified = 1;

	  iResult=ML_ExecuteDW2(ptyHandle,
							ptyHandle->tyMipsDW2App.pulCacheFlushApp,
							ptyHandle->tyMipsDW2App.ulCacheFlushInstructions*4,
							(void *)pulDataToProc, 
							2 * sizeof(unsigned long), 
						   (void *)pulDataFromProc, 
							2 * sizeof(unsigned long));
      }
   if (bCacheInstructionHasBeenModified)
      {
      ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulLocationOfCacheInstruction] = ulBackupOfOriginalCacheInstruction;
      }
   return iResult;
}
#if 0
/****************************************************************************
     Function: MLC_FlushCache
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               TyCacheType tyCacheType - cache type
               TyCacheFlush tyFlushType - flush operation type
       Output: int - error code
  Description: flush cache content
Date           Initials    Description
08-Feb-2008    VH          Initial
22-Jul-2008    NCH         Add content
06-Aug-2008    SPC         This function commented out for making it to run from probe
****************************************************************************/
int MLC_FlushCache(TyMLHandle *ptyHandle, 
                   TyCacheType tyCacheType, 
                   TyCacheFlush tyFlushType)
{

   TyError ErrRet = ERR_NO_ERROR;
   unsigned char pszReadBack[MAX_APPLICATION_SIZE];
   unsigned long ulIndex;
   unsigned long ulTagLoAndMask;
   unsigned long ulDummy;
   unsigned long ulNumberOfParams;
   unsigned long ulOffsetOfCacheInstruction=0;
   unsigned long ulLocationOfCacheInstruction=0;
   unsigned long ulNewCacheInstruction;
   unsigned long ulBackupOfOriginalCacheInstruction=0x0;
   int bCacheInstructionHasBeenModified = FALSE;

   
   DLOG2((LOGFILE,"\n\t\tMLC_FlushCache -> Called"));
   dbgprint("MLC_FlushCache \n");
   (void)CheckKSEG0Cached(0);
   // So You want to run target code.
   // Well you better be sure you know what you are doing.
   // A couple of important points
   // 1. If the memory you jump to is bad you will probably end up with a
   //    nasty DW2 lockup.
   // 2. You must run the srec which has the same endian mode as the processor is currently in.
   // 3. You should check if there is an available SREC for the current endian mode.
 

   if (!ptyHandle->bCacheFlushEnabled)
      return ERR_NO_ERROR;

   dbgprint("MLC_FlushCache 2\n");
   if (ptyHandle->bFlushOnlyDataCache)
      {
      if (tyCacheType == CACHE_TYPE_DATA && 
          tyFlushType == CACHE_FLUSH_WRITEBACK)
         {
         DLOG((LOGFILE,"\nEJTAG 2.6 Version of Flush cache, setting up TagLO And Mask for writeback only"));
         ulTagLoAndMask = 0xFFFFFFBF;
         ulNumberOfParams = 1;
         }
      else
         {
         DLOG((LOGFILE,"\nEJTAG 2.6 Version of Flush cache, setting up TagLO And Mask for writeback and Invalidate"));
         ulTagLoAndMask = 0xFFFFFF3F;
         ulNumberOfParams = 1;
         ptyHandle->bDataCacheAlreadyInvalidated = TRUE; // We only need to do this once
         }
      }
   else
      {
      DLOG((LOGFILE,"\nNot EJTAG 2.6 Version of Flush cache, no paramaters passed"));
      ulTagLoAndMask = 0xFFFFFFFF;
      ulNumberOfParams = 0;
      // However we do check for one thing...
      // We must ensure that the Cache is invalidated on Reset.
      if (tyCacheType == CACHE_TYPE_UNIFIED && tyFlushType == CACHE_FLUSH_INVALIDATE)
         {
         // Patch the cache flush routine to ensure that we Invalidate rather than WB_INV.
         DLOG((LOGFILE,"\n\t\t\tMLC_FlushCache -> Invalidating Unified Cache WITHOUT Write Back"));
         // Not yet implemented.. :-(

         ulOffsetOfCacheInstruction = ptyHandle->tyMipsDW2App.ulCacheFlushInstrAddress - DE_VECT;
         ulLocationOfCacheInstruction = ulOffsetOfCacheInstruction + ptyHandle->tyMipsDW2App.ulCacheFlushStartAddress;   
         if (ptyHandle->bBigEndian)
            {
            ulNewCacheInstruction = ML_SwapEndianWord(ptyHandle->ulInvalidateInst);
            ulBackupOfOriginalCacheInstruction = ML_SwapEndianWord((*(unsigned long *)&(ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction])));
            }
         else
            {
            ulNewCacheInstruction = ptyHandle->ulInvalidateInst;
            ulBackupOfOriginalCacheInstruction = (*(unsigned long *)&(ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction]));
            }

         if (ulBackupOfOriginalCacheInstruction == ptyHandle->ulInvalidateWBInst)
            {
            // Good overwrite cache instruction with CACHE_INSTRUCTION_WITH_INV
            ptyHandle->bCacheFlushEnabled  = FALSE; // Temp disable cache so write works
            ErrRet=MLM_WriteMemory(ptyHandle,
                                   ulLocationOfCacheInstruction,
                                   (unsigned char *)&ulNewCacheInstruction,
                                   4, 0);
            ptyHandle->bCacheFlushEnabled  = TRUE;
            if (ErrRet != ERR_NO_ERROR)
               {
               ASSERT_NOW();
               dbgprint("MLC_FlushCache e1\n");
               return ERR_UNABLE_TO_MOD_CACHE_INS_LITTLE;
               }
            // Also update the cache array so that we pass the verify test...
            ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction    ]  =  (unsigned char)(ulNewCacheInstruction       ) & 0xFF;
            ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 1]  =  (unsigned char)(ulNewCacheInstruction  >>  8) & 0xFF;
            ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 2]  =  (unsigned char)(ulNewCacheInstruction  >> 16) & 0xFF;
            ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 3]  =  (unsigned char)(ulNewCacheInstruction  >> 24) & 0xFF;
            // and don't forget to undo the change afterwards.
            bCacheInstructionHasBeenModified = TRUE;
            }
         else
            {
            ASSERT_NOW();
            dbgprint("MLC_FlushCache e2\n");
            return ERR_BAD_CACHE_INS_LITTLE;
            }
         }
      }


   // Before we run the code we must CIRCLE THE WAGGONS !!
   // Check if there is an SREC downloaded for the current Endian Mode.
#if 0000
   if (ptyHandle->bBigEndian)
      {
      if (!ptyHandle->bBigEndianSRECAvailable)
         {
         DLOG((LOGFILE,"\nMLC_FlushCache: Error: Currently in Big Endian and No Big Endian SREC available\n"));
         dbgprint("MLC_FlushCache e3\n");
         return ERR_NO_BIG_ENDIAN_SREC_AVAILABLE;
         }
      }
   else
      {
      if (!ptyHandle->bLittleEndianSRECAvaliable)
         {
         DLOG((LOGFILE,"\nMLC_FlushCache: Error: Currently in Little Endian and No Little Endian SREC available\n"));
         dbgprint("MLC_FlushCache e4\n");
         return ERR_NO_LITTLE_ENDIAN_SREC_AVAILABLE;
         }
      }
#endif
   // Next Verify before running from target memory
   if (ptyHandle->bCacheFlushVerify)
      {
      DLOG3((LOGFILE,"\n\t\tMLC_FlushCache -> Verify Application %d",FSHCACHEB));
      ptyHandle->bCacheFlushEnabled  = FALSE; // Temp disable cache so write works
      ErrRet=MLM_ReadMemory(ptyHandle,
                            ptyHandle->tyMipsDW2App.ulCacheFlushStartAddress,
                            (unsigned char *)pszReadBack,
                            ptyHandle->tyMipsDW2App.ulCacheFlushInstructions * sizeof(unsigned long),
                            0);
      ptyHandle->bCacheFlushEnabled  = TRUE; // Be sure to re-enable

      for (ulIndex=0; ulIndex<ptyHandle->tyMipsDW2App.ulCacheFlushInstructions; ulIndex++)
         {
         if (pszReadBack[ulIndex] != ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulIndex])
            {
            DLOG((LOGFILE,"\n\t\tMLC_FlushCache -> Verify Failed"));
            dbgprint("MLC_FlushCache e5\n");
            return ERR_INSTALLDW2_VERIFY_FAILED;
            }
         }
      DLOG3((LOGFILE,"\n\t\tMLC_FlushCache -> Verify Successful"));
      }


   ErrRet = ML_RunTargetCommand(ptyHandle,
                                ptyHandle->tyMipsDW2App.ulCacheFlushStartAddress,
                                &ulTagLoAndMask,ulNumberOfParams,&ulDummy,0);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

dbgprint("MLC_FlushCache 4\n");
   // Finally restore any changes to the cache flush routine if there were any
   if (bCacheInstructionHasBeenModified)
      {
      DLOG3((LOGFILE,"\n\t\tMLC_FlushCache -> Restoring Original Cache Instruction after Invalidation."));
      if (ptyHandle->bBigEndian)
         {
         // Also update the cache array so that we pass the verify test...
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction    ]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction  >> 24) & 0xFF;
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 1]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction  >> 16) & 0xFF;
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 2]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction  >>  8) & 0xFF;
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 3]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction       ) & 0xFF;
         // Finally write back the original Instruction
         ptyHandle->bCacheFlushEnabled  = FALSE; // Temp disable cache so write works
         ErrRet=MLM_WriteMemory(ptyHandle,
                                ulLocationOfCacheInstruction,
                                (unsigned char *)&ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction],
                                4,0);
         }
      else
         {
         // Also update the cache array so that we pass the verify test...
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction    ]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction       ) & 0xFF;
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 1]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction  >>  8) & 0xFF;
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 2]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction  >> 16) & 0xFF;
         ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction + 3]  =  (unsigned char)(ulBackupOfOriginalCacheInstruction  >> 24) & 0xFF;
         // Finally write back the original Instruction
         ptyHandle->bCacheFlushEnabled  = FALSE; // Temp disable cache so write works
         ErrRet=MLM_WriteMemory(ptyHandle,
                                ulLocationOfCacheInstruction,
                                (unsigned char *)&ptyHandle->tyMipsDW2App.pulCacheFlushApp[ulOffsetOfCacheInstruction],
                                4,0);
         }
      
      ptyHandle->bCacheFlushEnabled  = TRUE; // Be sure to re-enable
      if (ErrRet != ERR_NO_ERROR)
         {
         ASSERT_NOW();
         dbgprint("MLC_FlushCache e6\n");
         return ERR_CANT_RESTORE_CACHE_INS;
         }
      }
   dbgprint("MLC_FlushCache: %d \n",ErrRet);
   return ErrRet;

}
#endif

/****************************************************************************
     Function: MLC_PerformCacheAction
     Engineer: Vitezslav Hola
        Input: unsigned long ulAction - action to be performed (see 5 lowest bits encoded into CACHE instruction)
               unsigned long ulAddress - cache address
               unsigned long *pulTagLo -  pointer to TagLo value (can be NULL)
               unsigned long *pulDataLo - pointer to DataLo value (can be NULL)
       Output: int - error code
  Description: perform specified cache action using DW2 application
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_PerformCacheAction(TyMLHandle *ptyHandle, unsigned long ulAction, unsigned long ulAddress, unsigned long *pulTagLo, unsigned long *pulDataLo)
{
   int iResult = ERR_NO_ERROR;
   unsigned long ulCacheInst;
   unsigned long pulDataToProc[3];
   unsigned long pulDataFromProc[2];

   // setup parameters for DW2 application
   pulDataToProc[0] = ulAddress;                                  // 1st parameter is address in cache
   if (pulTagLo != NULL)                                          // 2nd parameter is taglo value
      pulDataToProc[1] = *pulTagLo;                      
   else
      pulDataToProc[1] = 0;
   if (pulDataLo != NULL)                                         // 3rd parameter is datalo value
      pulDataToProc[2] = *pulDataLo;                      
   else
      pulDataToProc[2] = 0;
   // modify DW2 application (cache instruction with selected action) on given offset
   ulCacheInst = ptyHandle->tyMipsDW2App.pulActCacheApp[ptyHandle->tyMipsDW2App.ulActCacheInstOffset];
   ulCacheInst &= 0xFFE0FFFF;                            // mask bits 16 to 20
   ulCacheInst |= ((ulAction & 0x0000001F) << 16);       // replace bits with selected action
   ptyHandle->tyMipsDW2App.pulActCacheApp[ptyHandle->tyMipsDW2App.ulActCacheInstOffset] = ulCacheInst;
   // call DW2 and process response
   iResult = ML_ExecuteDW2(ptyHandle, 
						   (void *)(ptyHandle->tyMipsDW2App.pulActCacheApp), 
						   ptyHandle->tyMipsDW2App.ulActCacheInstructions * sizeof(unsigned long),
                           (void *)pulDataToProc, 
						   3 * sizeof(unsigned long), 
						   (void *)pulDataFromProc, 
						   2 * sizeof(unsigned long));
   if (pulTagLo != NULL)
      {
      if (iResult == ERR_NO_ERROR)
         *pulTagLo = pulDataFromProc[0];
      else
         *pulTagLo = 0;
      }
   if (pulDataLo != NULL)
      {
      if (iResult == ERR_NO_ERROR)
         *pulDataLo = pulDataFromProc[1];
      else
         *pulDataLo = 0;
      }
   return iResult;
}

/****************************************************************************
     Function: MLC_AccessCacheTag
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned char bRead - read or write the entry
               unsigned long ulSrcResource - source resource
               unsigned long ulEntry - entry
               unsigned long ulWordSelect - selecting word
               unsigned long *pulTagLo - pointer to taglo value
               unsigned long *pulDataLo - pointer to datalo value
               unsigned long ulCount - number of entries
               unsigned long ulObjectSize - object size
       Output: int - error code
  Description: access tag for specified entry
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_AccessCacheTag(TyMLHandle *ptyHandle, unsigned char bRead, unsigned long ulSrcResource, unsigned long ulEntry,
                       unsigned long ulWordSelect, unsigned long *pulTagLo, unsigned long *pulDataLo, unsigned long ulCount, unsigned long ulObjectSize)
{
   unsigned char bWaySelect = 0;
   unsigned char bRestoreRequired = 0;
   unsigned long ulOrigErrCtlValue = 0;
   unsigned long ulIndex = 0;
   unsigned long ulDataLo, ulTagLo;
   unsigned long ulAction = INDEX_LOAD_TAG_D;
   int iResult = ERR_NO_ERROR;

   switch (ulSrcResource)
      {
      case MDIMIPPICACHE:     
         bWaySelect = 1;
         if (bRead)
            ulAction = INDEX_LOAD_TAG_I;
         else
            ulAction = INDEX_STORE_DATA_I;
         break;
      case MDIMIPPICACHET:
         if (bRead)
            ulAction = INDEX_LOAD_TAG_I;
         else
            ulAction = INDEX_STORE_TAG_I;
         break;
      case MDIMIPPDCACHE:
         bWaySelect = 1;
         if (bRead)
            ulAction = INDEX_LOAD_TAG_D;
         else
            ulAction = INDEX_STORE_DATA_D;
         break;
      case MDIMIPPDCACHET:
         if (bRead)
            ulAction = INDEX_LOAD_TAG_D;
         else
            ulAction = INDEX_STORE_TAG_D;
         break;
      default:
         // assert(0);
         ulAction = INDEX_LOAD_TAG_I;
         break;
      }
   // get number of bytes
   ulCount *= ulObjectSize;
   ulEntry <<= BYTE_INDEX_BIT_SHIFT;
   if (pulTagLo != NULL)
      ulTagLo = *pulTagLo;
   else
      ulTagLo = 0;
   if (pulDataLo != NULL)
      ulDataLo = *pulDataLo;
   else
      ulDataLo = 0;
   // change ErrClt register if necessary and go to main loop
   MLC_SetupErrCtlValue(ptyHandle, bWaySelect, &bRestoreRequired, &ulOrigErrCtlValue);
   while (ulCount)
      {
      unsigned long ulByteIndex = 0;
      // ensure effective address is in unmapped region otherwise TLB exception may occur
      ulIndex &= ~BYTE_INDEX_MASK;
      if (ulWordSelect)
         ulByteIndex = ((ulWordSelect * sizeof(unsigned long)) & BYTE_INDEX_MASK);
      ulIndex = (ulEntry | ulByteIndex) | 0xA0000000;
      iResult = MLC_PerformCacheAction(ptyHandle, ulAction, ulIndex, &ulTagLo, &ulDataLo);
      if (pulTagLo != NULL)
         *pulTagLo++ = ulTagLo;
      if (pulDataLo != NULL)
         *pulDataLo++ = ulDataLo;

      ulCount -= 4;
      ulWordSelect += 4;
      }
   // restore ErrCtl if necessary
   MLC_RestoreErrCtlValue(ptyHandle, bRestoreRequired, ulOrigErrCtlValue);
   return iResult;
}

/****************************************************************************
     Function: MLC_Hit_Invalidate_I
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - virtual address
       Output: int - error code
  Description: invalidate specified virtual address from instruction cache if present
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_Hit_Invalidate_I(TyMLHandle *ptyHandle, unsigned long ulAddress)
{
   return MLC_PerformCacheAction(ptyHandle, HIT_INVALIDATE_I, ulAddress, NULL, NULL);
}

/****************************************************************************
     Function: MLC_Hit_Invalidate_D
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - virtual address
       Output: int - error code
  Description: invalidate specified virtual address from data cache if present
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_Hit_Invalidate_D(TyMLHandle *ptyHandle, unsigned long ulAddress)
{
   return MLC_PerformCacheAction(ptyHandle, HIT_INVALIDATE_D, ulAddress, NULL, NULL);
}

/****************************************************************************
     Function: MLC_Hit_Writeback_Inv_D
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - virtual address
       Output: int - error code
  Description: if specified virtual address is present, valid and dirty in
               data cache, write it back and mark as invalid
  invalidate specified virtual address from data cache if present
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_Hit_Writeback_Inv_D(TyMLHandle *ptyHandle, unsigned long ulAddress)
{
   return MLC_PerformCacheAction(ptyHandle, HIT_WRITEBACK_INV_D, ulAddress, NULL, NULL);
}

/****************************************************************************
     Function: MLC_Hit_Writeback_D
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - virtual address
       Output: int - error code
  Description: if specified virtual address is present, valid and dirty in
               data cache and write it back.
  invalidate specified virtual address from data cache if present
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_Hit_Writeback_D(TyMLHandle *ptyHandle, unsigned long ulAddress)
{
   return MLC_PerformCacheAction(ptyHandle, HIT_WRITEBACK_D, ulAddress, NULL, NULL);
}




/****************************************************************************
     Function: MLC_AccessOverLapsInstScratchPad
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - start address
               unsigned long ulLength - length
       Output: unsigned char - 1 if access overlaps inst scratch pad, 0 otherwise
  Description: check if access on address with length overlaps inst scratchpad
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned char MLC_AccessOverLapsInstScratchPad(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned long ulLength)
{
   unsigned char bResult = 0;
   if(ptyHandle->bInstScratchPadRamPresent)
      {
      unsigned long ulEndAddress = ((ulStartAddress + ulLength) - 1);
      unsigned long ulScratchStart = KSEG0_START + ptyHandle->ulInstScratchPadRamStart;
      unsigned long ulScratchEnd = KSEG0_START + ptyHandle->ulInstScratchPadRamEnd;
      // check KSEG0
      if (CheckOverlappingRange(ulScratchStart, ulScratchEnd, ulStartAddress, ulEndAddress, NULL))
          bResult = 1;
      // check KSEG1
      ulScratchStart = KSEG1_START + ptyHandle->ulInstScratchPadRamStart;
      ulScratchEnd = KSEG1_START + ptyHandle->ulInstScratchPadRamEnd;
      if (CheckOverlappingRange(ulScratchStart, ulScratchEnd, ulStartAddress, ulEndAddress, NULL))
          bResult = 1;
      }
   return bResult;
}

/****************************************************************************
     Function: MLC_AccessOverLapsDataScratchPad
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - start address
               unsigned long ulLength - length
       Output: unsigned char - 1 if access overlapr data scratch pad, 0 otherwise
  Description: check if access on address with length overlaps data scratchpad
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned char MLC_AccessOverLapsDataScratchPad(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned long ulLength)
{
   if (ptyHandle->bDataScratchPadRamPresent)
      {
      unsigned long ulEndAddress = ((ulStartAddress + ulLength) - 1);
      unsigned long ulScratchStart = KSEG0_START + ptyHandle->ulDataScratchPadRamStart;
      unsigned long ulScratchEnd = KSEG0_START + ptyHandle->ulDataScratchPadRamEnd;
      if (CheckOverlappingRange(ulScratchStart, ulScratchEnd, ulStartAddress, ulEndAddress, NULL))
         return 1;
      }
   return 0;
}

/****************************************************************************
     Function: CheckOverlappingRange
     Engineer: Vitezslav Hola
        Input: unsigned long ulStart1 - start of 1st range
               unsigned long ulEnd1 - end of 1st range
               unsigned long ulStart2 - start of 2nd range
               unsigned long ulEnd2 - end of 2nd range
               TyRangeStruct *ptyRange - overlapping range info
       Output: unsigned char - 1 if ranges overlap, 0 otherwise
  Description: check if two ranges overlap and give info about that
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static unsigned char CheckOverlappingRange(unsigned long ulStart1, unsigned long ulEnd1, unsigned long ulStart2, unsigned long ulEnd2, TyRangeStruct *ptyRange)
{
   unsigned long ulLargestStart = 0;
   unsigned long ulSmallestEnd = 0;

   if (ulStart1 > ulStart2)
      ulLargestStart = ulStart1;
   else
      ulLargestStart = ulStart2;
   if (ulEnd1 < ulEnd2)
      ulSmallestEnd = ulEnd1;
   else
      ulSmallestEnd = ulEnd2;
   // check if overlap happens
   if (ulLargestStart <= ulSmallestEnd)
      {
      if (ptyRange != NULL)
         {
         ptyRange->ulStart = ulLargestStart;
         ptyRange->ulLength = ((ulSmallestEnd - ulLargestStart) + 1);
         }
      return 1;
      }
   return 0;
}

/****************************************************************************
     Function: CheckKSEG0Cached
     Engineer: Vitezslav Hola
        Input: unsigned long ulConfig0 - value of config 0 register
       Output: unsigned char - 1 if KSEG0 is cached, 0 otherwise
  Description: check if KSEG0 is cached based on config 0 register value
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static unsigned char CheckKSEG0Cached(unsigned long ulConfig0)
{
   switch (ulConfig0 & 0x00000007)
      {
      case 0x2:
      case 0x7:
         return 0;
      default:
         return 1;
      }
}

/****************************************************************************
     Function: MLC_SetupErrCtlValue
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned char bWaySelect - way selection for cache
               unsigned char *pbRestoreRequired - is restore required
               unsigned long *pulOriginalValue - store original value
       Output: none
  Description: check ErrCtl register and change way selection if required
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static void MLC_SetupErrCtlValue(TyMLHandle *ptyHandle, unsigned char bWaySelect, unsigned char *pbRestoreRequired, unsigned long *pulOriginalValue)
{
   unsigned char bChangeErrCtl = 0;
   unsigned long ulOriginalValue = 0;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pbRestoreRequired != NULL);
   assert(pulOriginalValue != NULL);
   // read ErrCtl CP0 register
   (void)MLR_ReadRegister(ptyHandle, REG_NUM_ERRCTRL, MDIMIPCP0, &ulOriginalValue); // Read the CP0 ErrCtl Register
   if ((ulOriginalValue & 0x20000000) && !bWaySelect)
      {
      (void)MLR_WriteRegister(ptyHandle, REG_NUM_ERRCTRL, MDIMIPCP0, ulOriginalValue & 0xDFFFFFFF);      // clear WST bit in original value
      bChangeErrCtl = 1;
      }
   else if (((ulOriginalValue & 0x20000000) == 0x0) && bWaySelect)
      {
      (void)MLR_WriteRegister(ptyHandle, REG_NUM_ERRCTRL, MDIMIPCP0, ulOriginalValue | 0x20000000);      // set WST bit in original value
      bChangeErrCtl = 1;
      }
   *pbRestoreRequired = bChangeErrCtl;
   *pulOriginalValue = ulOriginalValue;
}

/****************************************************************************
     Function: MLC_RestoreErrCtlValue
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned char bRestoreRequired - is restore required
               unsigned long ulOriginalValue - original value to restore
       Output: none
  Description: restore ErrCtl value if required with original value from MLC_SetupErrCtlValue
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static void MLC_RestoreErrCtlValue(TyMLHandle *ptyHandle, unsigned char bRestoreRequired, unsigned long ulOriginalValue)
{
   assert(ptyHandle != NULL);
   if (bRestoreRequired) 
      (void)MLR_WriteRegister(ptyHandle, REG_NUM_ERRCTRL, MDIMIPCP0, ulOriginalValue);
}

/****************************************************************************
     Function: MLC_ReadCacheRange
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulSrcResource - source resource
               unsigned long ulStartCacheIndex - start cache index
               unsigned long ulEndCacheIndex - end cache index
               TyCacheEntry *ptyData - pointer to data
               unsigned long ulCount - number of entries in array
               unsigned char bScratchpadRam - scratch pad ram
       Output: int - error code
  Description: Read a range of cache.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLC_ReadCacheRange(TyMLHandle *ptyHandle, unsigned long ulSrcResource, unsigned long ulStartCacheIndex, unsigned long ulEndCacheIndex,
                              TyCacheEntry *ptyData, unsigned long ulCount, unsigned char bScratchpadRam)
{
   int iResult;
   unsigned long pulDataToProc[3];
   unsigned long ulCacheAction = 0;
   // check parameters
   assert(ptyHandle != NULL);
   if ((ptyData == NULL) || (ulCount == 0))
      return ERR_NO_ERROR;
   if (ulCount > MAX_CACHE_ENTRIES)
      return ML_ERR_INSUFFICIENT_STRUCTURE_SIZE;
   // decide what cache action to do
   switch (ulSrcResource)
      {
      case MDIMIPPICACHE:
         ulCacheAction = INDEX_LOAD_TAG_I;
         break;
      case MDIMIPSICACHEWS:
         ulCacheAction = INDEX_LOAD_TAG_I;
         break;
      case MDIMIPPICACHET:
         ulCacheAction = INDEX_LOAD_TAG_I;
         break;
      case MDIMIPPDCACHE:
         ulCacheAction = INDEX_LOAD_TAG_D;
         break;
      case MDIMIPPDCACHET:
         ulCacheAction = INDEX_LOAD_TAG_D;
         break;
      case MDIMIPSDCACHEWS:
         ulCacheAction = INDEX_LOAD_TAG_D;
         break;
      default:
         ulCacheAction = INDEX_LOAD_TAG_I;
         break;
      }
   // now, we need to modiy DW2 application with proper instructions
   for (unsigned long ulLocIndex = 0; ulLocIndex < 4; ulLocIndex++)
      {
      unsigned long ulInstOffset = ptyHandle->tyMipsDW2App.pulReadCacheInstOffset[ulLocIndex];
      unsigned long ulCurrentInst = ptyHandle->tyMipsDW2App.pulReadCacheApp[ulInstOffset];
      ulCurrentInst &= 0xFFE0FFFF;                                      // clear bits 16 to 20
      ulCurrentInst |= ((ulCacheAction & 0x0000001F) << 16);            // encode cache action into instruction code (all in LE)
      ptyHandle->tyMipsDW2App.pulReadCacheApp[ulInstOffset] = ulCurrentInst;     // write modified instruction back
      }
   // prepare DW2 parameters and execute it
   pulDataToProc[0] = ulStartCacheIndex | 0xA0000000;                   // start index as effective address in unmapped region
   pulDataToProc[1] = ulEndCacheIndex | 0xA0000000;
   pulDataToProc[2] = (bScratchpadRam) ? 0x10000000 : 0x00000000;       // enable/disable SPR bit as required
   iResult = ML_ExecuteDW2(ptyHandle, 
						   (void *)(ptyHandle->tyMipsDW2App.pulReadCacheApp), 
						   ptyHandle->tyMipsDW2App.ulReadCacheInstructions * sizeof (unsigned long),
                           (void *)pulDataToProc, 
						   3 * sizeof(unsigned long), 
						   (void *)(ptyHandle->ptyCacheEntries), 
						   sizeof(TyCacheEntry) * ulCount);
   if (iResult != ERR_NO_ERROR)
      memset((void *)(ptyHandle->ptyCacheEntries), 0, sizeof(TyCacheEntry) * ulCount);
   return iResult;
}

/****************************************************************************
     Function: MLC_EnableKSeg0CachingForScratching
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned char bHandleCall - handling call
               unsigned char bEnable - enable or disable
               unsigned long *pulSavedValue - pointer to saved value
       Output: unsigned char - error code
  Description: Read a range of cache.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned char MLC_EnableKSeg0CachingForScratching(TyMLHandle *ptyHandle, unsigned char bHandleCall, unsigned char bEnable, unsigned long *pulSavedValue)
{
   unsigned char bResult = 0;
   unsigned long ulOriginalCP0Register = 0;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pulSavedValue != NULL);
   ulOriginalCP0Register = *pulSavedValue;
   if (bHandleCall)
      {
      unsigned long ulCP0Register = 0;
      (void)MLR_ReadRegister(ptyHandle, ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, &ulCP0Register);
      if (bEnable)
         ulOriginalCP0Register = ulCP0Register;                         // saving value
      // modify CP0 register
      ulCP0Register &= 0xFFFFFFF8;
      ulCP0Register |= 0x3;
      if (CheckKSEG0Cached(ulOriginalCP0Register))
         {
         if (bEnable)
            {
            ML_SetupConfig0Masks(ptyHandle, 1);                         // enable kSEG0 for cache
            (void)MLR_WriteRegister(ptyHandle, ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, ulCP0Register);
            ML_SetupConfig0Masks(ptyHandle, 0);                         // disable KSEG0 for cache
            }
         else
            {
            (void)MLR_WriteRegister(ptyHandle, ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, ulOriginalCP0Register);
            }
         }
      else
         bResult = 1;                                                   // KSEG0 is not cached which means scratch memory not used
      }
   *pulSavedValue = ulOriginalCP0Register;
   return bResult;
}

/****************************************************************************
     Function: MLC_ReadInstScratchPad
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - start address
               unsigned char *pucData - pointer to data
               unsigned long ulLength - length
               unsigned long ulObjectSize - object size
       Output: int - error code
  Description: Instruction scratchpad memory can only be read using cache instructions.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLC_ReadInstScratchPad(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize)
{
   int iResult;
   unsigned long ulCurrentAddress, ulStartOffset, ulEndOffset, ulOriginalLength;
   unsigned long pulDataLo[4];
   unsigned char *pucOriginalData = pucData;
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   // modify length
   if (ulObjectSize > 1)
      ulLength = ulLength * ulObjectSize;                               // length in bytes
   ulOriginalLength = ulLength;
   if (ptyHandle->bProcessorExecuting)
      return ERR_DIMEMREAD_NOT_DURING_EXE;                           // cannot do action while processor is running
   ulCurrentAddress = (ulStartAddress - ptyHandle->ulInstScratchPadRamStart) & 0x1FFFFFFF;
   // now read scratchpad ram
   if (ulCurrentAddress % 16)
      {  // there are some unalligned bytes
      unsigned long ulFirstBytes = 16 - (ulCurrentAddress % 16);
      if (ulFirstBytes > ulLength)
         ulFirstBytes = ulLength;
      // get start and end offset
      ulStartOffset = (ulCurrentAddress & 0xFFFFFFF0) / 4;
      ulEndOffset = ulStartOffset + 3;
      iResult = MLC_ReadCacheResourceArrayRange(ptyHandle, MDIMIPPICACHE, ulStartOffset, ulEndOffset, (void *)pulDataLo, 1);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      if (ptyHandle->bBigEndian)
         ML_SwapEndianess((void *)pulDataLo, 4, sizeof(unsigned long));
      // copy result
      memcpy((void *)pucData, (void *)pulDataLo, ulFirstBytes);
      ulLength -= ulFirstBytes;
      pucData += ulFirstBytes;
      ulCurrentAddress += ulFirstBytes;
      }
   // read by blocks
   while (ulLength / 16)
      {
      unsigned long ulCurrentEntries = ulLength / 16;
      if (ulCurrentEntries > MAX_CACHE_ENTRIES)
         ulCurrentEntries = MAX_CACHE_ENTRIES;
      ulStartOffset = (ulCurrentAddress & 0xFFFFFFF0) / 4;
      ulEndOffset = (ulStartOffset + 4*ulCurrentEntries) - 1;
      iResult = MLC_ReadCacheResourceArrayRange(ptyHandle, MDIMIPPICACHE, ulStartOffset, ulEndOffset, (void *)pucData, 1);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      if (ptyHandle->bBigEndian)
         ML_SwapEndianess((void *)pucData, ulCurrentEntries * 4, sizeof(unsigned long));
      ulLength -= (ulCurrentEntries * 16);
      ulCurrentAddress += (ulCurrentEntries * 16);
      pucData += (ulCurrentEntries * 16);
      }
   // read remaining
   if (ulLength % 16)
      {
      unsigned long ulRemainingBytes = ulLength % 16;
      ulStartOffset = (ulCurrentAddress & 0xFFFFFFF0) / 4;
      ulEndOffset = ulStartOffset + 3;
      iResult = MLC_ReadCacheResourceArrayRange(ptyHandle, MDIMIPPICACHE, ulStartOffset, ulEndOffset, (void *)pulDataLo, 1);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      if (ptyHandle->bBigEndian)
         ML_SwapEndianess((void *)pulDataLo, 4, sizeof(unsigned long));
      memcpy((void *)pucData, (void *)pulDataLo, ulRemainingBytes);
      }
   // we need to subsitute any SW breakpoints
   iResult = MLB_RemoveBPSFromReadBuff(ptyHandle, ulStartAddress, pucOriginalData, ulOriginalLength);
   return iResult;
}


