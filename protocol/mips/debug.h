/****************************************************************************
       Module: debug.h
     Engineer: Vitezslav Hola
  Description: Header with debug support for Opella-XD MIPS MDI.
Date           Initials    Description
*  LOG Levels:                                                           
*  DLOG      : Error Conditions + Basic Settings we should always know   
*  DLOG1     : HighLevel Function calls e.g. DiGetStatus                 
*  DLOG2     : Midlayer Function calls e.g. ML_GetStatus                 
*  DLOG3     : Specific details within a function                        
*  DLOG4     : Execution handler logging of executed op-codes            
*  DLOG5     : Temporary logging for Debug purposes. Will be removed !   
08-Feb-2008    VH          Initial
****************************************************************************/
#ifndef DEBUG_H_
#define DEBUG_H_

// Define this to enable Opella DLOGs even in release builds.
//#define ENABLE_EVEN_IN_RELEASE_BUILD
            

//#define ENABLE_DEBUG_LOG_OPELLA    

// Define this to encrypt the output of the Opella DLOGs
//#define ENCRYPT
            
// Comment the following section to control the level of debug output in Genia and Opella                                                        

#define DEBUG_LEVEL0
#define DEBUG_LEVEL1
#define DEBUG_LEVEL2
#define DEBUG_LEVEL3
#define DEBUG_LEVEL4
#define DEBUG_LEVEL5


// When DEBUG_RESET is defined Txrx_GDI call's reset over and over again so that you can see what went 
// wrong.
//#define DEBUG_RESET


// This enables the Diskware Diagnostic Registers...
#ifdef _DEBUG
   #define ENABLE_DW_DIAG
#endif

 

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////
//////// Please Change settings above this line.. Functions below should not be changed
////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////



// Define ASSERT
// Assert is now defined in 1 file


#ifdef ENABLE_EVEN_IN_RELEASE_BUILD
   #ifdef ENABLE_DEBUG_LOG_OPELLA     
	  #define ENABLE_DEBUG_LOG           // Level 0
   #endif
#else
   #ifdef _DEBUG                     // Debug Log only works in Debug MODE
	  #ifdef ENABLE_DEBUG_LOG_OPELLA     
		 #define ENABLE_DEBUG_LOG           // Level 0
	  #endif
   #endif
#endif

#define DPRINTF(VARARGS)

#ifdef ENABLE_DEBUG_LOG
    #ifdef _LINUX
           #define LOGFILE_NAME "/home/build/transfer/MipsLog.txt"  // Assume /home/build/transfer    
    #else
        #define LOGFILE_NAME "c:\\MIPSLog.txt"  // Everyone has a C:\ directory. Always use this file.
    #endif
#ifdef ENCRYPT
static char pszString[2000];
#define LOGFILE pszString
static FILE * LOG; // Local to Each Module
#else

#define LOGFILE      pfLogfile
    #ifdef _LINUX
        #include <stdio.h>
        static FILE *LOGFILE;
    #else
        static FILE * LOGFILE; // Local to Each Module
    #endif
#endif

#ifdef DEBUG_LEVEL0
   #ifdef ENCRYPT
      #define DLOG(VARARGS)       LOG     = fopen(LOGFILE_NAME,"a+b");              \
                                            sprintf VARARGS;                        \
                                            {                                       \
                                            unsigned int x;                         \
                                            for(x=0;x<200;x++)                      \
                                               {                                    \
                                               pszString[x]=pszString[x]^169;       \
                                               }                                    \
                                            pszString[199]=0x0;                     \
                                            fwrite(pszString,sizeof(char),200,LOG); \
                                            }                                       \
                                            fclose(LOG)                
   #else

        #ifdef _LINUX
        #define DLOG(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"a");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)   
        
        #else
        #define DLOG(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"at");  \
                                          fprintf VARARGS;           \
                                           fclose(LOGFILE)   
        #endif    
           
   #endif
#else 
   #define DLOG(VARARGS)
#endif


#ifdef DEBUG_LEVEL1
      #ifdef ENCRYPT
         #define DLOG1(VARARGS)      LOG     = fopen(LOGFILE_NAME,"a+b");              \
                                               sprintf VARARGS;                        \
                                               {                                       \
                                               unsigned int x;                         \
                                               for(x=0;x<200;x++)                      \
                                                  {                                    \
                                                  pszString[x]=pszString[x]^169;       \
                                                  }                                    \
                                               pszString[199]=0x0;                     \
                                               fwrite(pszString,sizeof(char),200,LOG); \
                                               }                                       \
                                               fclose(LOG)                
      #else
   
        #ifdef _LINUX
            #define DLOG1(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"a");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)   
        
        #else
            #define DLOG1(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"at");  \
                                          fprintf VARARGS;           \
                                           fclose(LOGFILE)   
        #endif    
                    
      #endif
#else 
      #define DLOG1(VARARGS)
#endif

   #ifdef DEBUG_LEVEL2
      #ifdef ENCRYPT
         #define DLOG2(VARARGS)      LOG     = fopen(LOGFILE_NAME,"a+b");              \
                                               sprintf VARARGS;                        \
                                               {                                       \
                                               unsigned int x;                         \
                                               for(x=0;x<200;x++)                      \
                                                  {                                    \
                                                  pszString[x]=pszString[x]^169;       \
                                                  }                                    \
                                               pszString[199]=0x0;                     \
                                               fwrite(pszString,sizeof(char),200,LOG); \
                                               }                                       \
                                               fclose(LOG)                
      #else
        #ifdef _LINUX
            #define DLOG2(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"a");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)   
        
        #else
            #define DLOG2(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"at");  \
                                          fprintf VARARGS;           \
                                           fclose(LOGFILE)   
        #endif    
                   
     #endif
   #else 
      #define DLOG2(VARARGS)
   #endif

   #ifdef DEBUG_LEVEL3
      #ifdef ENCRYPT
         #define DLOG3(VARARGS)      LOG     = fopen(LOGFILE_NAME,"a+b");              \
                                               sprintf VARARGS;                        \
                                               {                                       \
                                               unsigned int x;                         \
                                               for(x=0;x<200;x++)                      \
                                                  {                                    \
                                                  pszString[x]=pszString[x]^169;       \
                                                  }                                    \
                                               pszString[199]=0x0;                     \
                                               fwrite(pszString,sizeof(char),200,LOG); \
                                               }                                       \
                                               fclose(LOG)                
      #else

        #ifdef _LINUX
            #define DLOG3(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"a");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)   
        
        #else
            #define DLOG3(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"at");  \
                                          fprintf VARARGS;           \
                                           fclose(LOGFILE)   
        #endif   
                  
      #endif
   #else 
      #define DLOG3(VARARGS)
   #endif

   #ifdef DEBUG_LEVEL4
      #ifdef ENCRYPT
         #define DLOG4(VARARGS)      LOG     = fopen(LOGFILE_NAME,"a+b");              \
                                               sprintf VARARGS;                        \
                                               {                                       \
                                               unsigned int x;                         \
                                               for(x=0;x<200;x++)                      \
                                                  {                                    \
                                                  pszString[x]=pszString[x]^169;       \
                                                  }                                    \
                                               pszString[199]=0x0;                     \
                                               fwrite(pszString,sizeof(char),200,LOG); \
                                               }                                       \
                                               fclose(LOG)                
      #else
         #ifdef _LINUX
            #define DLOG4(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"a");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)   
        
        #else
           #define DLOG4(VARARGS)   LOGFILE = fopen(LOGFILE_NAME,"at");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)
        #endif
      #endif
   #else 
      #define DLOG4(VARARGS)
   #endif

   #ifdef DEBUG_LEVEL5
      #ifdef ENCRYPT
         #define DLOG5(VARARGS)      LOG     = fopen(LOGFILE_NAME,"a+b");              \
                                               sprintf VARARGS;                        \
                                               {                                       \
                                               unsigned int x;                         \
                                               for(x=0;x<200;x++)                      \
                                                  {                                    \
                                                  pszString[x]=pszString[x]^169;       \
                                                  }                                    \
                                               pszString[199]=0x0;                     \
                                               fwrite(pszString,sizeof(char),200,LOG); \
                                               }                                       \
                                               fclose(LOG)                
      #else
         #ifdef _LINUX
            #define DLOG5(VARARGS)       LOGFILE = fopen(LOGFILE_NAME,"a");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)   
        
        #else
            #define DLOG5(VARARGS)   LOGFILE = fopen(LOGFILE_NAME,"at");  \
                                            fprintf VARARGS;           \
                                            fclose(LOGFILE)
        #endif
      #endif
   #else 
      #define DLOG5(VARARGS)
   #endif

#else

   #define DLOG(VARARGS)  
   #define DLOG1(VARARGS)  
   #define DLOG2(VARARGS)  
   #define DLOG3(VARARGS)  
   #define DLOG4(VARARGS)  
   #define DLOG5(VARARGS)  

#endif


#endif   // DEBUG_H_
