/****************************************************************************
       Module: ml_brkpt.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This section contains all functions related to MIPS 
               breakpoint handling.
Date           Initials    Description
08-Feb-2008    VH          Initial
23-Dec-2008    SPC         Returning errors more properly.
06-Aug-2009    SPC         Made cache flush routine run from probe
03-Jan-2012    SV          Modified BRCM_TRACE_TRIGGER_REG value
****************************************************************************/

#define MDI_LIB               // We are an MDI Library not a debugger

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <assert.h>
#include "commdefs.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "mdi.h"
#include "mips.h"
#include "midlayer.h"
#include "gerror.h"
#include "ml_error.h"

#ifndef __LINUX
// Windows specific includes
#else
// Linux includes
#endif

/*****************************Extern Variables********************/
extern TyUserSettings tyCurrentUserSettings; 


// local structures
typedef struct
{
   unsigned long ulBreakpointIdentifier;
//   unsigned long ulAddressRegister;
//   unsigned long ulMaskRegister;
//   unsigned long ulASIDRegister;
//   unsigned long ulControlRegister;
//   unsigned long ulAddressOfBP;
//   unsigned long ulAddressMaskOfBP;
//   unsigned long ulASIDOfBP;
//   unsigned long ulControlValueOfBP;
//   unsigned long ulDataValueRegister;
//   unsigned long ulDataValueOfBP;
   int bBreakpointAvailable;
//   int bMIPS16Breakpoint;
} HWBP_RESOURCE;

#define MAX_SUPPORTED_INST_HWBPS        6
#define MAX_SUPPORTED_DATA_HWBPS        4

HWBP_RESOURCE HWInstBPArray[MAX_SUPPORTED_INST_HWBPS];
HWBP_RESOURCE HWDataBPArray[MAX_SUPPORTED_DATA_HWBPS];

// local macros
#define BP_UNIQUE_IDENTIFIER_FOR_WATCHPOINT           0x21212121
#define BP_UNIQUE_IDENTIFIER_FOR_COMPLEX_BP           0x77777777
#define BRCM_TRACE_TRIGGER_REG                        0x17

// local functions
static int MLB_UpdateBPForEndianness(TyMLHandle *ptyHandle, TyBreakpoint *ptyBp);
static int MLB_SetHWBP(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulASID, unsigned long ulBpSize);
static int MLB_ClearHWBP(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long *pulASID, unsigned long *pulBpSize, unsigned char *pbNoBpPresent);
static int MLB_ClearComplexBP(TyMLHandle *ptyHandle, unsigned long ulBreakpointID);
static int MLB_SetComplexInstHWBP(TyMLHandle *ptyHandle, TyBpFlags *ptyBreakpointFlags);
static int MLB_SetComplexDataHWBP(TyMLHandle *ptyHandle, TyBpFlags *ptyBreakpointFlags);
static int MLB_FreeAllComplexHWBpResources(TyMLHandle *ptyHandle);


/****************************************************************************
     Function: MLB_AddBPSToWriteBuff
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - start address (virtual)
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes
       Output: int - error code
  Description: Substitute any software breakpoints for memory values in buffer to be written to target.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_AddBPSToWriteBuff(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength)
{
   TyBreakpoint *ptyBp;
   unsigned long ulSDBBP = MIPS_INST_SDBBP;
   unsigned short usSDBBP16 = MIPS_INST_SDBBP16;
   unsigned char *pucSDBBP = (unsigned char *)&ulSDBBP;
   unsigned char *pucSDBBP16 = (unsigned char *)&usSDBBP16;
   unsigned long ulByteOffset;
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   ptyBp = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;                  // get pointer to list of breakpoints
   // get software breakpoint instructions
   if (ptyHandle->bBigEndian)
      {  // we need to swap opcodes for BE
      ulSDBBP = ML_SwapEndianWord(ulSDBBP);
      usSDBBP16 = ML_SwapEndianHalfword(usSDBBP16);
      }
   // go through all breakpoints
   while (ptyBp != NULL)
      {
      if (ptyBp->tyBPType != BP_HW)
         {  // it is not hardware breakpoint
         unsigned long ulBpAddress = ptyBp->ulBPVirtualAddress;
         unsigned char *pucPreviousData = (unsigned char *)(&(ptyBp->ulPreviousInstructionData));
         // check each byte of address
         // for each byte of word/halfword, we check if breakpoint address is inside range, store byte value into structure and replace with proper part
         // of software breakpoint instruction
         if ((ulBpAddress >= ulStartAddress) && (ulBpAddress < (ulStartAddress + ulLength)))
            {  // do action for lowest byte
            ulByteOffset = ulBpAddress - ulStartAddress;
            if (ptyBp->ulBPSize == 2)
               {
               pucPreviousData[0] = pucData[ulByteOffset];
               pucData[ulByteOffset] = pucSDBBP16[0];
               }
            else
               {
               pucPreviousData[0] = pucData[ulByteOffset];
               pucData[ulByteOffset] = pucSDBBP[0];
               }
            }
         if (((ulBpAddress + 0x00000001) >= ulStartAddress) && ((ulBpAddress + 0x00000001) < (ulStartAddress + ulLength)))
            {  // do action for second byte
            ulByteOffset = (ulBpAddress + 0x00000001) - ulStartAddress;
            if (ptyBp->ulBPSize == 2)
               {
               pucPreviousData[1] = pucData[ulByteOffset];
               pucData[ulByteOffset] = pucSDBBP16[1];
               }
            else
               {
               pucPreviousData[1] = pucData[ulByteOffset];
               pucData[ulByteOffset] = pucSDBBP[1];
               }
            }
         if (((ulBpAddress + 0x00000002) >= ulStartAddress) && ((ulBpAddress + 0x00000002) < (ulStartAddress + ulLength)))
            {  // do action for third byte
            ulByteOffset = (ulBpAddress + 0x00000002) - ulStartAddress;
            if (ptyBp->ulBPSize != 2)
               {  // assuming 32-bit breakpoint
               pucPreviousData[2] = pucData[ulByteOffset];
               pucData[ulByteOffset] = pucSDBBP[2];
               }
            }
         if (((ulBpAddress + 0x00000003) >= ulStartAddress) && ((ulBpAddress + 0x00000003) < (ulStartAddress + ulLength)))
            {  // do action for fourth byte
            ulByteOffset = (ulBpAddress + 0x00000003) - ulStartAddress;
            if (ptyBp->ulBPSize != 2)
               {  // assuming 32-bit breakpoint
               pucPreviousData[3] = pucData[ulByteOffset];           
               pucData[ulByteOffset] = pucSDBBP[3];
               }
            }
         }
      ptyBp = ptyBp->ptyNext;                                           // go to next breakpoint
      }
   // we are finished with all possible breakpoints
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_RemoveBPSFromReadBuff
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - start address (virtual)
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes
       Output: int - error code
  Description: Substitute any software breakpoints with previous memory values in buffer.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_RemoveBPSFromReadBuff(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength)
{
   TyBreakpoint *ptyBp;
   unsigned long ulByteOffset;
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   ptyBp = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;                  // get pointer to list of breakpoints
   // go through all potential breakpoints
   while (ptyBp != NULL)
      {
      if (ptyBp->tyBPType != BP_HW)
         {  // it is not hardware breakpoint
         int iResult;
         unsigned long ulBpAddress = ptyBp->ulBPVirtualAddress;
         unsigned char *pucPreviousData = (unsigned char *)(&(ptyBp->ulPreviousInstructionData));
         // check endianess of BP's is correct
         iResult = MLB_UpdateBPForEndianness(ptyHandle, ptyBp);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // for each byte from value, replace all bytes
         if ((ulBpAddress >= ulStartAddress) && (ulBpAddress < (ulStartAddress + ulLength)))
            {  // do action for 1st byte
            ulByteOffset = ulBpAddress - ulStartAddress;
            pucData[ulByteOffset] = pucPreviousData[0];                 // same action for 16-bit and 32-bit breakpoints
            }
         if (((ulBpAddress + 0x00000001) >= ulStartAddress) && ((ulBpAddress + 0x00000001) < (ulStartAddress + ulLength)))
            {  // do action for 2nd byte
            ulByteOffset = (ulBpAddress + 0x00000001) - ulStartAddress;
            pucData[ulByteOffset] = pucPreviousData[1];                 // same action for 16-bit and 32-bit breakpoints
            }
         if (((ulBpAddress + 0x00000002) >= ulStartAddress) && ((ulBpAddress + 0x00000002) < (ulStartAddress + ulLength)))
            {
            ulByteOffset = (ulBpAddress + 0x00000002) - ulStartAddress;
            if (ptyBp->ulBPSize != 2)
               pucData[ulByteOffset] = pucPreviousData[2];              // assuming 32-bit breakpoint only
            }
         if (((ulBpAddress + 0x00000003) >= ulStartAddress) && ((ulBpAddress + 0x00000003) < (ulStartAddress + ulLength)))
            {
            ulByteOffset = (ulBpAddress + 0x00000003) - ulStartAddress;
            if (ptyBp->ulBPSize != 2)
               pucData[ulByteOffset] = pucPreviousData[3];              // assuming 32-bit breakpoint only
            }
         }
      ptyBp = ptyBp->ptyNext;                                           // next breakpoint
      }
   // we are finished with all possible breakpoints
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_SetBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulBpVirtualAddress - virtual address for breakpoint
               unsigned long ulBpAccessAddress - address to access memory for breakpoint
               TyBPType tyBpType - breakpoint type (HW/SW)
               unsigned long ulBpSize - breakpoint size (2 is 16-bit, 4 is 32-bit)
       Output: int - error code
  Description: Set breakpoint to specified address. Supports both hardware and software
               breakpoints and 16/32 bit.
Date           Initials    Description
08-Feb-2008    VH          Initial
20-Nov-2008    SPC         Fixed the variable passing
08-Nov-2011    SPC         Inserting Hardware breakpoint if software breakpoint fails
****************************************************************************/
int MLB_SetBP(TyMLHandle *ptyHandle, unsigned long ulBpVirtualAddress, unsigned long ulBpAccessAddress, TyBPType tyBpType, unsigned long ulBpSize)
{
   // check parameters
   assert(ptyHandle != NULL);
   // enable memory writing in case it got cleared at any stage
   ML_EnableMemoryWriting(ptyHandle, 0, 0, 0);
   // check type of requested breakpoint
   if (tyBpType == BP_NO)
      {
      return ERR_SETBP_NOBP;
      }
   else if (tyBpType == BP_HW)
      {  // hardware breakpoint
      if (ptyHandle->bProcessorExecuting)
         return ERR_SETBP_NOT_DURING_EXE;                            // cannot set HW breakpoint while processor is running
      // leave job for another function (ASID is 0x0)
      return MLB_SetHWBP(ptyHandle, ulBpVirtualAddress, 0x0, ulBpSize);
      }
   else if (tyBpType == BP_SW)
      {  // software breakpoint
      int iResult = ERR_NO_ERROR;
      unsigned long ulSDBBP = MIPS_INST_SDBBP;
      unsigned short usSDBBP16 = MIPS_INST_SDBBP16;
      unsigned char bPrevDmaEnabledByUser, bPrevBPOnTheFlyEnabled;
      TyBreakpoint *ptyBp = NULL;
      // first try to allocate memory for next breakpoint
      ptyBp = (TyBreakpoint *)malloc(sizeof(TyBreakpoint));
      if (ptyBp == NULL)
         return ML_ERR_INSUFFICIENT_MEMORY;
      // swap bytes in breakpoint opcode for BE
      if (ptyHandle->bBigEndian)
         {
         ulSDBBP = ML_SwapEndianWord(ulSDBBP);
         usSDBBP16 = ML_SwapEndianHalfword(usSDBBP16);
         }
      // we got memory, we must unallocate it when this structure is not added to list with other breakpoints (to prevent memory leaks)
      // initialize structure
      ptyBp->bBigEndian = ptyHandle->bBigEndian;                        // target endianess
      ptyBp->ptyNext = NULL;                                            // next breakpoint (currently not added to list)
      ptyBp->ptyResource = NULL;                                        // no hardware resource
      ptyBp->tyBPType = BP_SW;                                          // software breakpoint
      ptyBp->ulBPVirtualAddress = ulBpVirtualAddress;                   // breakpoint virtual address
      ptyBp->ulBPAccessAddress = ulBpAccessAddress;                     // breakpoint access address
      ptyBp->ulPreviousInstructionData = 0;                             // initialize previous data, should be modified later
      // enable on the fly access to via DMA core
      bPrevDmaEnabledByUser = ptyHandle->bDmaEnabledByUser;
      bPrevBPOnTheFlyEnabled = ptyHandle->bBPOnTheFlyEnabled;
      if (ptyHandle->bProcessorExecuting && ptyHandle->bDmaSupported)
         {
         ptyHandle->bDmaEnabledByUser = 1;      ptyHandle->bBPOnTheFlyEnabled = 1;      
         }
      if (ptyHandle->bProcessorExecuting && (!ptyHandle->bDmaEnabledByUser || !ptyHandle->bBPOnTheFlyEnabled))
         iResult = ERR_SETBP_NOT_DURING_EXE;

      if (ulBpSize == 2)
         {  // using 16-bit software breakpoint
         unsigned short usPrevData = 0;
         unsigned short usReadBack = 0;
         ptyBp->ulBPSize = 2;                                           // 16-bit breakpoint
         // read previous value
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_ReadMemoryVirtual(ptyHandle, ulBpAccessAddress, (unsigned char *)&usPrevData, 2, 2);
         ptyBp->ulPreviousInstructionData = (unsigned long)usPrevData;
         // write breakpoint instruction to location in proper endian format
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryVirtual(ptyHandle, ulBpAccessAddress, (unsigned char *)&usSDBBP16, 2, 2);
         // and read it back for sure
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_ReadMemoryVirtual(ptyHandle, ulBpAccessAddress, (unsigned char *)&usReadBack, 2, 2);
         if ((iResult == ERR_NO_ERROR) && (usReadBack != usSDBBP16))
            iResult = ERR_SETBP_FAILED_SWBP;
         }
      else
         {  // using 32-bit software breakpoint
         unsigned long ulPrevData = 0;
         unsigned long ulReadBack = 0;
         ptyBp->ulBPSize = 4;                                           // 32-bit breakpoint
         // read previous value
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_ReadMemoryVirtual(ptyHandle, ulBpAccessAddress, (unsigned char *)&ulPrevData, 4, 4);
         ptyBp->ulPreviousInstructionData = ulPrevData;
         // write breakpoint instruction to location in proper endian format
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryVirtual(ptyHandle, ulBpAccessAddress, (unsigned char *)&ulSDBBP, 4, 4);
         // and read it back for sure
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_ReadMemoryVirtual(ptyHandle, ulBpAccessAddress, (unsigned char *)&ulReadBack, 4, 4);
         if ((iResult == ERR_NO_ERROR) && (ulReadBack != ulSDBBP))
            iResult = ERR_SETBP_FAILED_SWBP;
         }

      if ( iResult == ERR_SETBP_FAILED_SWBP )
         {
         // Software breakpoint insertion failed (May be it is because of cannot write to target memory).
         // We are trying to set hardware breakpoint if resource available. It is useful for flash based
         // programming debugging
         free(ptyBp);
         return MLB_SetHWBP(ptyHandle, ulBpVirtualAddress, 0x0, ulBpSize);
         }

      // restore on the fly access
      ptyHandle->bDmaEnabledByUser = bPrevDmaEnabledByUser;
      ptyHandle->bBPOnTheFlyEnabled = bPrevBPOnTheFlyEnabled;
      // we have done action with target, now we need to add breakpoint to list or free memory
      if (iResult == ERR_NO_ERROR)
         {  // add breakpoint structure to the list (put it to start of list)
         ptyBp->ptyNext = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;
         ptyHandle->tyMipsBreakpoints.ptyBreakpoint = ptyBp;
         }
      else
         {
         free(ptyBp);                                                   // error occured, so we need to free allocated memory
         }
      return iResult;
      }
   else
      {
      return ERR_SETBP_INVAL_TYPE;                                     // unsupported type of breakpoint
      }
}

/****************************************************************************
     Function: MLB_ClearBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - address to clear breakpoint
               TyBPType *ptyBpType - pointer to breakpoint type (cannot be NULL)
               unsigned long *pulBpSize - pointer to breakpoint size (cannot be NULL)
               unsigned char *pbNoBpPresent - is breakpoint present (cannot be NULL)
               unsigned long *pulBpAccessAddress - pointer to breakpoint access address (can be NULL)
       Output: int - error code
  Description: Clear breakpoint at specified address. Both hardware and software breakpoints
               are supported.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_ClearBP(TyMLHandle *ptyHandle, unsigned long ulAddress, TyBPType *ptyBpType, unsigned long *pulBpSize, unsigned char *pbNoBpPresent, unsigned long *pulBpAccessAddress)
{
   TyBreakpoint **pptyBp;
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(ptyBpType != NULL);
   assert(pulBpSize != NULL);
   assert(pbNoBpPresent != NULL);
   // enable memory writing
   ML_EnableMemoryWriting(ptyHandle, 0, 0, 0);
   // breakpoint address
   if (pulBpAccessAddress != NULL)
      *pulBpAccessAddress = ulAddress;
   // check if processor is executing
   if (ptyHandle->bProcessorExecuting && (!ptyHandle->bDmaEnabledByUser || !ptyHandle->bBPOnTheFlyEnabled))
      return ERR_CLEARBP_NOT_DURING_EXE;
   // check if breakpoint is complex
   if (((ulAddress & 0xFFFF0000) == 0x77770000) && ((ulAddress & 0x00000001) == 0x00000001))
      {  // odd address with 0x7777xxxx are complex breakpoints
      return MLB_ClearComplexBP(ptyHandle, ulAddress);
      }
   // initialize returned values
   *pbNoBpPresent = 1;     *pulBpSize = 4;   *ptyBpType = BP_SW;
   // look for breakpoints
   pptyBp = &(ptyHandle->tyMipsBreakpoints.ptyBreakpoint);
   if (*pptyBp == NULL)
      return ERR_NO_ERROR;                                           // no breakpoint found
   // go through all breakpoints
   while (*pptyBp != NULL)
      {
      if (((*pptyBp)->tyBPType == BP_SW) && ((*pptyBp)->ulBPVirtualAddress == ulAddress))
         {
         break;                                                         // found software breakpoint with given address
         }
      pptyBp = &((*pptyBp)->ptyNext);                                   // next breakpoint
      }
   if (*pptyBp == NULL)
      {  // could not find software breakpoint, so it must be hardware breakpoint
      unsigned long ulASID = 0;
      *pulBpSize = 4;      *ptyBpType = BP_HW;
      return MLB_ClearHWBP(ptyHandle, ulAddress, &ulASID, pulBpSize, pbNoBpPresent);
      }
   else
      {  // ok, we found software breakpoint
      TyBreakpoint *ptyBp = *pptyBp;
      // return access address
      if (pulBpAccessAddress != NULL)
         *pulBpAccessAddress = ptyBp->ulBPAccessAddress;
      *pulBpSize = ptyBp->ulBPSize;
      *ptyBpType = ptyBp->tyBPType;
      *pbNoBpPresent = 0;

       // restoring data value depends on breakpoint size
      ulAddress = ptyBp->ulBPAccessAddress;
      if (ptyBp->ulBPSize == 2)
         {  // this was 16-bit breakpoint
         unsigned short usPreviousData = (unsigned short)(ptyBp->ulPreviousInstructionData & 0x0000FFFF);
         unsigned short usReadBack = usPreviousData ^ 0xFFFF;           // initialized readback should be different than expected data
         // check if there is an issue with endianess
         if (ptyHandle->bBigEndian != ptyBp->bBigEndian)
            usPreviousData = ML_SwapEndianHalfword(usPreviousData);     // current endianess is different than when BP was written
         // write previous value and do readback
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryVirtual(ptyHandle, ulAddress, (unsigned char *)&usPreviousData, 2, 2);
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_ReadMemoryVirtual(ptyHandle, ulAddress, (unsigned char *)&usReadBack, 2, 2);
         if ((iResult == ERR_NO_ERROR) && (usReadBack != usPreviousData))
            iResult = ERR_CLRBP_FAILED_SWBP;
         }
      else
         {  // this was 32-bit breakpoint
         unsigned long ulPreviousData = ptyBp->ulPreviousInstructionData;
         unsigned long ulReadBack = ulPreviousData ^ 0xFFFFFFFF;        // initialized readback should be different than expected data
         // check if there is an issue with endianess
         if (ptyHandle->bBigEndian != ptyBp->bBigEndian)
            ulPreviousData = ML_SwapEndianWord(ulPreviousData);         // current endianess is different than when BP was written
         // write previous value and do readback
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryVirtual(ptyHandle, ulAddress, (unsigned char *)&ulPreviousData, 4, 4);
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_ReadMemoryVirtual(ptyHandle, ulAddress, (unsigned char *)&ulReadBack, 4, 4);
         if ((iResult == ERR_NO_ERROR) && (ulReadBack != ulPreviousData))
            iResult = ERR_CLRBP_FAILED_SWBP;
         }
      if (iResult != ERR_NO_ERROR)
         return iResult;                                                // something went wrong, so we going to keep breakpoint
      // remove structure from list of breakpoints and free allocated memory (we can use pptyBp)
      *pptyBp = (*pptyBp)->ptyNext;                                     // bypass current breakpoint in the list (keeping pointer in ptyBp)
      free(ptyBp);                                                      // free memory
      return ERR_NO_ERROR;                                           // we are finished
      }
}

/****************************************************************************
     Function: MLB_QueryBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - address
               TyBPType *ptyBpType - pointer to breakpoint type (cannot be NULL)
               unsigned long *pulBpSize - pointer to breakpoint size (cannot be NULL)
               unsigned char *pbNoBpPresent - is breakpoint present (cannot be NULL)
       Output: int - error code
  Description: Query if breakpoint exist at specified address.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_QueryBP(TyMLHandle *ptyHandle, unsigned long ulAddress, TyBPType *ptyBpType, unsigned long *pulBpSize, unsigned char *pbNoBpPresent)
{
   TyBreakpoint *ptyBp;
   assert(ptyHandle != NULL);
   assert(ptyBpType != NULL);
   assert(pulBpSize != NULL);
   assert(pbNoBpPresent != NULL);
   // check list of breakpoints
   ptyBp = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;
   while (ptyBp != NULL)
      {
      if (ptyBp->ulBPVirtualAddress == ulAddress)
         {  // we found breakpoint
         *pbNoBpPresent = 0;
         *pulBpSize = ptyBp->ulBPSize;
         *ptyBpType = ptyBp->tyBPType;
         return ERR_NO_ERROR;
         }
      ptyBp = ptyBp->ptyNext;                                           // next breakpoint in the list
      }
   *pbNoBpPresent = 1;
   *pulBpSize = 4;
   *ptyBpType = BP_SW;
   return ERR_NO_ERROR;
}


















/****************************************************************************
     Function: MLB_CheckWatchpointBPAddress
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - address
               unsigned long *pulBpIdentifier - pointer to breakpoint identifier
       Output: unsigned char - 1 if address matches one of breakpoints, 0 otherwise
  Description: Determine if address is same as breakpoint emulated with watchpoint
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned char MLB_CheckWatchpointBPAddress(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long *pulBpIdentifier)
{
   assert(ptyHandle != NULL);
   assert(pulBpIdentifier != NULL);
   // check watchpoints
   for (unsigned long ulIndex = 0; ulIndex < ptyHandle->tyMipsBreakpoints.ulWatchBPUsed; ulIndex++)
      {
      if (!(ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex].bBPAvailable) &&
          (ulAddress == ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex].ulAddressOfBP))
         {  // we found match
         *pulBpIdentifier = ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex].ulBreakpointIdentifier;
         return 1;
         }
      }
   // could not find match
   *pulBpIdentifier = 0;
   return 0;
}

/****************************************************************************
     Function: MLB_SetHWBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - breakpoint address
               unsigned long ulASID - ASID
               unsigned long ulBpSize - breakpoint size (2 is 16-bit, 4 is 32-bit)
       Output: int - error code
  Description: Set HW breakpoint to specified address.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLB_SetHWBP(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulASID, unsigned long ulBpSize)
{
   unsigned long ulIndex;
   int iResult = ERR_NO_ERROR;
   TyBreakpoint *ptyBp = NULL;
   // check parameters
   assert(ptyHandle != NULL);
   // check if we have enough breakpoints available
   if ((ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed >= ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported) && 
       (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed >= ptyHandle->tyMipsDSU.ulNumberOfWPAsBP))
      return ERR_NO_HW_BP_LEFT;                                      // no HW breakpoint left
   // allocate structure for breakpoint
   ptyBp = (TyBreakpoint *)malloc(sizeof(TyBreakpoint));                // make sure deallocating memory if returning error
   if (ptyBp == NULL)
      return ML_ERR_INSUFFICIENT_MEMORY;                                // not enough memory for BP structure
   // try to find resource for HW breakpoint
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
      if (ptyBpResource->bBPAvailable)
         {  // great, breakpoint is available
         unsigned long ulIBCValue = 0;
         unsigned long ulValue = 0;
         ulIBCValue = 0x00000001;                                       // BE = 1, ASIDuse = 0, TE = 0, others 0
         // write DSU address register
         ulValue = ulAddress;                                           // breakpoint address
         if (ptyHandle->bOddAddressForHWMIPS16BPs && (ulBpSize == 2))
            ulValue |= 0x00000001;
         iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulAddressDSUEjOfs, (unsigned char *)&ulValue, 4);
         // write DSU mask register
         ulValue = 0;                                                   // masking no bits
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulMaskDSUEjOfs, (unsigned char *)&ulValue, 4);
         // write ASID if supported
         if (ptyHandle->tyMipsDSU.bIBPASIDSupported && (iResult == ERR_NO_ERROR))
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulASIDDSUEjOfs, (unsigned char *)&ulASID, 4);
         // write DSU control register
         ulValue = ulIBCValue;                                          // ICB value
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulValue, 4);
         // check result
         if (iResult == ERR_NO_ERROR)
            {  // still no error, we can fill structures
            // HW resources
            ptyBpResource->bBPAvailable = 0;
            ptyBpResource->ulAddressOfBP = ulAddress;
            ptyBpResource->ulAddressMaskOfBP = 0;
            ptyBpResource->ulASIDOfBP = ulASID;
            ptyBpResource->ulControlValueOfBP = ulIBCValue;
            ptyBpResource->bMIPS16BP = (ulBpSize == 2) ? 1 : 0;
            ptyBpResource->ulAddressOfBP = ulAddress;
            ptyBpResource->bEmulatedWithWP = 0;
            ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed++;              // increase number of used breakpoints
            // breakpoint structure
            ptyBp->bBigEndian = ptyHandle->bBigEndian;                  // endianess does not really matter for HW breakpoints
            ptyBp->tyBPType = BP_HW;                                    // hardware breakpoint
            ptyBp->ptyResource = ptyBpResource;                         // address of resource
            ptyBp->ulBPAccessAddress = ulAddress;
            ptyBp->ulBPVirtualAddress = ulAddress;
            ptyBp->ulBPSize = ulBpSize;                                 // breakpoint size
            ptyBp->ulPreviousInstructionData = 0x0;                     // irrelevant for HW breakpoint
            // add allocated structure to the list
            ptyBp->ptyNext = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;
            ptyHandle->tyMipsBreakpoints.ptyBreakpoint = ptyBp;         // added to the list
            return ERR_NO_ERROR;
            }
         else
            {
            free(ptyBp);
            return iResult;
            }
         }
      }
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfWPAsBP; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex]);
      if (ptyBpResource->bBPAvailable)
         {
         unsigned long ulValue, ulIRInst;
         // bit I=1, discarting lowest bits from address for IWatchLo
         ulValue = (ulAddress & 0xFFFFFFF8) | 0x4;
         iResult = MLR_WriteRegister(ptyHandle, 0x32, MDIMIPCP0, ulValue);
         // setting IWatchHi register (ASID = 0, MASK = 0, G = 1)
         if (iResult == ERR_NO_ERROR)
            iResult = MLR_WriteRegister(ptyHandle, 0x33, MDIMIPCP0, 0x40000000);
         // enable breakpoint via EJWATCH register
         ulIRInst = MIPS_EJTAG_EJWATCH_INST;          ulValue = 0x01;         // enable WE bit in EJWATCH
         if (iResult == ERR_NO_ERROR)
            iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRInst, MIPS_EJTAG_EJWATCH_LENGTH, &ulValue, NULL);
         if (iResult == ERR_NO_ERROR)
            {  // add breakpoint info
            ptyBpResource->bBPAvailable = 0;
            ptyBpResource->ulAddressOfBP = ulAddress;
            ptyBpResource->ulAddressMaskOfBP = 0;
            ptyBpResource->ulASIDOfBP = 0;
            ptyBpResource->bMIPS16BP = (ulBpSize == 2) ? 1 : 0;
            ptyBpResource->ulBreakpointIdentifier = ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierForWatchpointBp;
            ptyBpResource->bEmulatedWithWP = 1;
            ptyHandle->tyMipsBreakpoints.ulWatchBPUsed++;
            // breakpoint structure
            ptyBp->bBigEndian = ptyHandle->bBigEndian;                  // endianess does not really matter for HW breakpoints
            ptyBp->tyBPType = BP_HW;                                    // hardware breakpoint
            ptyBp->ptyResource = ptyBpResource;                         // address of resource
            ptyBp->ulBPAccessAddress = ulAddress;
            ptyBp->ulBPVirtualAddress = ulAddress;
            ptyBp->ulBPSize = ulBpSize;                                 // breakpoint size
            ptyBp->ulPreviousInstructionData = 0x0;                     // irrelevant for HW breakpoint
            // add allocated structure to the list
            ptyBp->ptyNext = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;
            ptyHandle->tyMipsBreakpoints.ptyBreakpoint = ptyBp;         // added to the list
            return ERR_NO_ERROR;
            }
         else
            {
            free(ptyBp);
            return iResult;
            }
         }
      }
   // we could not find any available HW breakpoint
   free(ptyBp);
   return ERR_NO_HW_BP_LEFT;
}

/****************************************************************************
     Function: MLB_ClearHWBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - breakpoint address
               unsigned long *pulASID - pointer for ASID value
               unsigned long *pulBpSize - pointer for breakpoints size
               unsigned char *pbNoBpPresent - is breakpoint present
       Output: int - error code
  Description: Clear HW breakpoint to specified address.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLB_ClearHWBP(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long *pulASID, unsigned long *pulBpSize, unsigned char *pbNoBpPresent)
{
   TyBreakpoint **pptyBp;
   TyBreakpoint *ptyBp;
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pulBpSize != NULL);
   assert(pbNoBpPresent != NULL);
   *pbNoBpPresent = 1;
   *pulASID = 0;
   *pulBpSize = 4;
   // check if any hardware breakpoints are used
   if ((ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed == 0) && (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed == 0))
      {
      return ERR_NO_ERROR;                                           // no hardware breakpoint
      }
   // look for breakpoints
   pptyBp = &(ptyHandle->tyMipsBreakpoints.ptyBreakpoint);
   while (*pptyBp != NULL)
      {
      if (((*pptyBp)->ulBPVirtualAddress == ulAddress) && ((*pptyBp)->tyBPType == BP_HW))
         break;                                                         // found breakpoint
      pptyBp = &((*pptyBp)->ptyNext);                                   // next breakpoint
      }
   if (*pptyBp == NULL)
      return ERR_NO_ERROR;                                           // breakpoint not found
   // we have valid breakpoint so we need to just remove it
   *pulBpSize = (*pptyBp)->ulBPSize;
   *pbNoBpPresent = 0;
   ptyBp = *pptyBp;
   // check if breakpoint is in fact emulated using watchpoint (AMD Au1x00)
   if ((ptyBp->ptyResource)->bEmulatedWithWP && (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed > 0))
      {  // clear emulated HW breakpoint 
      unsigned long ulIRInst, ulValue;
      ulIRInst = MIPS_EJTAG_EJWATCH_INST;    ulValue = 0x0;             // disable WE bit
      iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRInst, MIPS_EJTAG_EJWATCH_LENGTH, &ulValue, NULL);
      ulValue = 0x0;
      if (iResult == ERR_NO_ERROR)
         iResult = MLR_WriteRegister(ptyHandle, 0x32, MDIMIPCP0, 0);    // clear IWatchLo
      if (iResult == ERR_NO_ERROR)
         iResult = MLR_WriteRegister(ptyHandle, 0x33, MDIMIPCP0, 0);    // clear IWatchHi
      // check result
      if (iResult == ERR_NO_ERROR)
         {
         (ptyBp->ptyResource)->bBPAvailable = 1;                        // breakpoint is now available
         ptyHandle->tyMipsBreakpoints.ulWatchBPUsed--;
         *pptyBp = ptyBp->ptyNext;                                      // remove structure from the list
         free(ptyBp);                                                   // free allocated memory
         return ERR_NO_ERROR;
         }
      }
   else if (ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed > 0)
      {  // it is real HW breakpoint, so just disable it
      unsigned long ulIBCValue = 0x0;                                   // disable breakpoint (BE = TE = ASIDuse = 0, others 0)
      iResult = MLM_WriteMemoryEjtag(ptyHandle, (ptyBp->ptyResource)->ulControlDSUEjOfs, (unsigned char *)&ulIBCValue, 4);
      if (iResult == ERR_NO_ERROR)
         {
         (ptyBp->ptyResource)->bBPAvailable = 1;                        // breakpoint is now available
         ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed--;
         *pptyBp = ptyBp->ptyNext;                                      // remove structure from the list
         free(ptyBp);                                                   // free allocated memory
         return ERR_NO_ERROR;
         }
      }
   return iResult;
}

/****************************************************************************
     Function: MLB_DetermineDSUConfiguration
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: int - error code
  Description: Function determines type of MIPS DSU (debug support unit).
               It includes number of hardware breakpoints, register addresses, etc.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_DetermineDSUConfiguration(TyMLHandle *ptyHandle)
{
   unsigned long ulIndex;
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   // depending on EJTAG version, decode information from IMPCODE register
   if (ptyHandle->ucEjtagVersion < EJTAG_VERSION_25)
      {  // EJTAG version 2.0 and older
      unsigned long ulImpcode = 0;
      // read IMPCODE register
      iResult = MLR_ReadRegister(ptyHandle, REG_MIPS_IMPCODE, ASHLING_INT_REG, &ulImpcode);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // check if instruction breakpoints are supported
      if (!(ulImpcode & BIT_5))
         {  // instruction breakpoints supported
         unsigned long ulIBSValue = 0;
         iResult = MLR_ReadRegister(ptyHandle, REG_MIPS_IBS, ASHLING_INT_REG, &ulIBSValue);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported = (ulIBSValue & 0x0F000000) >> 24;  // get BCN bitfield
         ptyHandle->tyMipsDSU.bIBPASIDSupported = (ulIBSValue & BIT_30) ? 1 : 0;          // is ASID supported
         }
      else
         {  // instruction breakpoints not supported
         ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported = 0;
         ptyHandle->tyMipsDSU.bIBPASIDSupported = 0;
         }
      // check if data breakpoints are supported
      if (!(ulImpcode & BIT_6))
         {  // instruction breakpoints supported
         unsigned long ulDBSValue = 0;
         iResult = MLR_ReadRegister(ptyHandle, REG_MIPS_DBS, ASHLING_INT_REG, &ulDBSValue);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported = (ulDBSValue & 0x0F000000) >> 24;     // get BCN bitfield
         ptyHandle->tyMipsDSU.bDBPASIDSupported = (ulDBSValue & BIT_30) ? 1 : 0;             // is ASID supported
         ptyHandle->tyMipsDSU.bLoadValueCompareSupported = (ulDBSValue & BIT_28) ? 1 : 0;    // load value compare supported
         ptyHandle->tyMipsDSU.bStoreValueCompareSupported = (ulDBSValue & BIT_29) ? 1 : 0;   // store value compare supported
         }
      else
         {  // instruction breakpoints not supported
         ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported = 0;
         ptyHandle->tyMipsDSU.bDBPASIDSupported = 0;
         ptyHandle->tyMipsDSU.bLoadValueCompareSupported = 0;
         ptyHandle->tyMipsDSU.bStoreValueCompareSupported = 0;
         }
      // check bus breakpoints
      if (!(ulImpcode & BIT_7))
         ptyHandle->tyMipsDSU.bProcBPSupported = 1;
      else
         ptyHandle->tyMipsDSU.bProcBPSupported = 0;
      }
   else
      {  // EJTAG version 2.5 and newer
      unsigned long ulDebugControlRegister = 0;
      // read DCR
      iResult = MLR_ReadRegister(ptyHandle, REG_MIPS_DCR, ASHLING_INT_REG, &ulDebugControlRegister);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // supporting instruction breakpoint ?
      if (ulDebugControlRegister & BIT_16)                              // checking InstBrk bit
         {  // instruction breakpoints supported, we need to read IBS register in DSU to find out how many of them
         unsigned long ulIBSValue = 0;
         iResult = MLR_ReadRegister(ptyHandle, REG_MIPS_IBS, ASHLING_INT_REG, &ulIBSValue);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported = (ulIBSValue & 0x0F000000) >> 24;  // get BCN bitfield
         ptyHandle->tyMipsDSU.bIBPASIDSupported = (ulIBSValue & BIT_30) ? 1 : 0;          // is ASID supported
         }
      else
         {  // instruction breakpoints not supported
         ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported = 0;
         ptyHandle->tyMipsDSU.bIBPASIDSupported = 0;
         }
      // supporting data breakpoint ?
      if (ulDebugControlRegister & BIT_17)                              // checking DataBrk bit
         {  // instruction breakpoints supported, we need to read IBS register in DSU to find out how many of them
         unsigned long ulDBSValue = 0;
         iResult = MLR_ReadRegister(ptyHandle, REG_MIPS_DBS, ASHLING_INT_REG, &ulDBSValue);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported = (ulDBSValue & 0x0F000000) >> 24;     // get BCN bitfield
         ptyHandle->tyMipsDSU.bDBPASIDSupported = (ulDBSValue & BIT_30) ? 1 : 0;             // is ASID supported
         ptyHandle->tyMipsDSU.bLoadValueCompareSupported = (ulDBSValue & BIT_28) ? 1 : 0;    // load value compare supported
         ptyHandle->tyMipsDSU.bStoreValueCompareSupported = (ulDBSValue & BIT_29) ? 1 : 0;   // store value compare supported
         }
      else
         {  // instruction breakpoints not supported
         ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported = 0;
         ptyHandle->tyMipsDSU.bDBPASIDSupported = 0;
         ptyHandle->tyMipsDSU.bLoadValueCompareSupported = 0;
         ptyHandle->tyMipsDSU.bStoreValueCompareSupported = 0;
         }
      ptyHandle->tyMipsDSU.bProcBPSupported = 0;
      }
   // verify if arrays are enough for supported breakpoints, if not restrict number of breakpoints used
   if (ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported > MAX_SUPPORTED_INST_HW_BP)
      ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported = MAX_SUPPORTED_INST_HW_BP;
   if (ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported > MAX_SUPPORTED_DATA_HW_BP)
      ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported = MAX_SUPPORTED_DATA_HW_BP;
   // check if we can emulate hardware breakpoints using watchpoints
   if (ptyHandle->bUseInstrWatchForBP)
      ptyHandle->tyMipsDSU.ulNumberOfWPAsBP = MAX_WATCHPOINTS_FOR_BP;
   else
      ptyHandle->tyMipsDSU.ulNumberOfWPAsBP = 0;
   // initialize address and data hardware breakpoint array
   if (ptyHandle->ucEjtagVersion >= EJTAG_VERSION_25)
      {  // EJTAG version 2.5 and newer
      for (ulIndex = 0; ulIndex < MAX_SUPPORTED_INST_HW_BP; ulIndex++)
         {
         TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
         ptyBpResource->ulAddressDSUEjOfs = DRSEG_OFFSET_EJ25_IBA(ulIndex);
         ptyBpResource->ulMaskDSUEjOfs = DRSEG_OFFSET_EJ25_IBM(ulIndex);
         ptyBpResource->ulASIDDSUEjOfs = DRSEG_OFFSET_EJ25_IBASID(ulIndex);
         ptyBpResource->ulControlDSUEjOfs = DRSEG_OFFSET_EJ25_IBC(ulIndex);
         ptyBpResource->ulDataValueDSUEjOfs = 0x0;
         }
      for (ulIndex = 0; ulIndex < MAX_SUPPORTED_DATA_HW_BP; ulIndex++)
         {
         TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
         ptyBpResource->ulAddressDSUEjOfs = DRSEG_OFFSET_EJ25_DBA(ulIndex);
         ptyBpResource->ulMaskDSUEjOfs = DRSEG_OFFSET_EJ25_DBM(ulIndex);
         ptyBpResource->ulASIDDSUEjOfs = DRSEG_OFFSET_EJ25_DBASID(ulIndex);
         ptyBpResource->ulControlDSUEjOfs = DRSEG_OFFSET_EJ25_DBC(ulIndex);
         ptyBpResource->ulDataValueDSUEjOfs = DRSEG_OFFSET_EJ25_DBV(ulIndex);
         }
      for (ulIndex = 0; ulIndex < MAX_WATCHPOINTS_FOR_BP; ulIndex++)
         {
         TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex]);
         ptyBpResource->ulAddressDSUEjOfs = 0x0;
         ptyBpResource->ulMaskDSUEjOfs = 0x0;
         ptyBpResource->ulASIDDSUEjOfs = 0x0;
         ptyBpResource->ulControlDSUEjOfs = 0x0;
         ptyBpResource->ulDataValueDSUEjOfs = 0x0;
         }
      }
   else
      {  // EJTAG version 2.0 and older
      for (ulIndex = 0; ulIndex < MAX_SUPPORTED_INST_HW_BP; ulIndex++)
         {
         TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
         ptyBpResource->ulAddressDSUEjOfs = DRSEG_OFFSET_EJ20_IBA(ulIndex);
         ptyBpResource->ulMaskDSUEjOfs = DRSEG_OFFSET_EJ20_IBM(ulIndex);
         ptyBpResource->ulASIDDSUEjOfs = 0x0;
         ptyBpResource->ulControlDSUEjOfs = DRSEG_OFFSET_EJ20_IBC(ulIndex);
         ptyBpResource->ulDataValueDSUEjOfs = 0x0;
         }
      for (ulIndex = 0; ulIndex < MAX_SUPPORTED_DATA_HW_BP; ulIndex++)
         {
         TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
         ptyBpResource->ulAddressDSUEjOfs = DRSEG_OFFSET_EJ20_DBA(ulIndex);
         ptyBpResource->ulMaskDSUEjOfs = DRSEG_OFFSET_EJ20_DBM(ulIndex);
         ptyBpResource->ulASIDDSUEjOfs = 0x0;
         ptyBpResource->ulControlDSUEjOfs = DRSEG_OFFSET_EJ20_DBC(ulIndex);
         ptyBpResource->ulDataValueDSUEjOfs = DRSEG_OFFSET_EJ20_DB(ulIndex);
         }
      for (ulIndex = 0; ulIndex < MAX_WATCHPOINTS_FOR_BP; ulIndex++)
         {
         TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex]);
         ptyBpResource->ulAddressDSUEjOfs = 0x0;
         ptyBpResource->ulMaskDSUEjOfs = 0x0;
         ptyBpResource->ulASIDDSUEjOfs = 0x0;
         ptyBpResource->ulControlDSUEjOfs = 0x0;
         ptyBpResource->ulDataValueDSUEjOfs = 0x0;
         }
      }
   // we need to mark all resources as unused when checking DSU for the first time
   if (!ptyHandle->tyMipsDSU.bDSUDetermined)
      {
      // mark HW instruction breakpoints as available
      for (ulIndex = 0; ulIndex < MAX_SUPPORTED_INST_HW_BP; ulIndex++)
         {
         ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex].bBPAvailable = 1;
         ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex].ulBreakpointIdentifier = 0x0;
         }
      ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed = 0;
      // mark HW data breakpoints as available
      for (ulIndex = 0; ulIndex < MAX_SUPPORTED_DATA_HW_BP; ulIndex++)
         {
         ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex].bBPAvailable = 1;
         ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex].ulBreakpointIdentifier = 0x0;
         }
      ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed = 0;
      // mark emulated HW breakpoints as available
      for (ulIndex = 0; ulIndex < MAX_WATCHPOINTS_FOR_BP; ulIndex++)
         {
         ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex].bBPAvailable = 1;
         ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex].ulBreakpointIdentifier = 0x0;
         }
      ptyHandle->tyMipsBreakpoints.ulWatchBPUsed = 0;
      // initialize unique identifiers
      ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp = BP_UNIQUE_IDENTIFIER_FOR_COMPLEX_BP;
      ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierForWatchpointBp = BP_UNIQUE_IDENTIFIER_FOR_WATCHPOINT;
      ptyHandle->tyMipsDSU.bDSUDetermined = 1;
      }
   return iResult;
}

/****************************************************************************
     Function: MLB_CheckHWBPUsed
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: unsigned char - 1 if any HW breakpoint used, 0 otherwise
  Description: Check if any HW breakpoints is currently being used
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned char MLB_CheckHWBPUsed(TyMLHandle *ptyHandle)
{
   assert(ptyHandle != NULL);
   if ((ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed == 0) && 
       (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed == 0) && 
       (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed == 0))
      return 0;
   else
      return 1;
}

/****************************************************************************
     Function: MLB_ClearComplexBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulBreakpointID - breakpoint identifier
       Output: int - error code
  Description: Clear complex HW breakpoint.
Date           Initials    Description
08-Feb-2008    VH          Initial
26-Nov-2011    SV          Modified to clear Zephyr Trace Register
****************************************************************************/
static int MLB_ClearComplexBP(TyMLHandle *ptyHandle, unsigned long ulBreakpointID)
{
   unsigned long ulIndex;
   unsigned long ulRegVal=0;
   // check parameters
   assert(ptyHandle != NULL);
   // check if we have any hardware breakpoints
   if ((ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed == 0) && (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed == 0))
      return ERR_NO_ERROR;                                           // nothing to do

   
   //To clear Zephyr Trace trigger register
   if (tyCurrentUserSettings.bZephyrTraceSupport == 1)
      (void)MLR_ReadCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,&ulRegVal, 5);

   // look for hardware instruction breakpoint
   for (ulIndex = 0; ulIndex < MAX_SUPPORTED_INST_HW_BP; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
      if (!(ptyBpResource->bBPAvailable) && (ptyBpResource->ulBreakpointIdentifier == ulBreakpointID))
         {  // found complex breakpoint
         unsigned long ulValue = 0;
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulValue, 4);
      
         if (tyCurrentUserSettings.bZephyrTraceSupport == 1)
            {
            ulValue = 0x0000000f;
            ulValue = ~(ulValue<<(ulIndex*4));
            ulValue &= ulRegVal; 
            (void)MLR_WriteCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,ulValue, 5);
            }
         ptyBpResource->bBPAvailable = 1;
         ptyBpResource->ulBreakpointIdentifier = 0x0;
         if (ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed > 0)
            ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed--;
         
         }
      }
   // look for hardware data breakpoint
   for (ulIndex = 0; ulIndex < MAX_SUPPORTED_DATA_HW_BP; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
      if (!(ptyBpResource->bBPAvailable) && (ptyBpResource->ulBreakpointIdentifier == ulBreakpointID))
         {  // found complex breakpoint
         unsigned long ulValue = 0;
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulValue, 4);
          
         if (tyCurrentUserSettings.bZephyrTraceSupport == 1)
            {
            ulValue = 0x000f0000;
            ulValue = ~(ulValue<<(ulIndex*4));
            ulValue = ~(ulValue);
            ulValue &= ulRegVal; 
            (void)MLR_WriteCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,ulValue, 5);
            }
         ptyBpResource->bBPAvailable = 1;
         ptyBpResource->ulBreakpointIdentifier = 0x0;
         if (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed > 0)
            ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed--;
         }
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_SetComplexHWBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               TyBpFlags *ptyBreakpointFlags - pointer to breakpoint flags
       Output: int - error code
  Description: Set complex hardware breakpoint.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_SetComplexHWBP(TyMLHandle *ptyHandle, TyBpFlags *ptyBreakpointFlags)
{
   // check parameters
   assert(ptyHandle != NULL);
   assert(ptyBreakpointFlags != NULL);
   // set instruction or data complex hardware breakpoint
   if (ptyBreakpointFlags->bDataBp)
      return MLB_SetComplexDataHWBP(ptyHandle, ptyBreakpointFlags);
   else
      return MLB_SetComplexInstHWBP(ptyHandle, ptyBreakpointFlags);
}

/****************************************************************************
     Function: MLB_SetComplexInstHWBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               TyBpFlags *ptyBreakpointFlags - pointer to breakpoint flags
       Output: int - error code
  Description: Set complex hardware instruction breakpoint.
Date           Initials    Description
08-Feb-2008    VH          Initial
11-Jun-2010    DK          Modifed to replace the advance break point
                           with latest one if alreay set
26-Nov-2011    SV          Modified to configure Zephyr Trace Register

****************************************************************************/
static int MLB_SetComplexInstHWBP(TyMLHandle *ptyHandle, TyBpFlags *ptyBreakpointFlags)
{
   unsigned long ulIndex;
   // check parameters
   assert(ptyHandle != NULL);
   if (ptyBreakpointFlags == NULL)
      return ERR_NO_ERROR;
  // are there any HW breakpoints available
   if (ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed >= ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported)
      return ERR_NO_HW_BP_LEFT;
   // look for spare hardware breakpoint
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
      if (ptyBpResource->bBPAvailable)
         {  // we found available breakpoint
         unsigned long ulIBCValue = 0;
         unsigned long ulAddress = ptyBreakpointFlags->ulBpAddress;
         unsigned long ulTriggerVal = 0x0000000b;
         unsigned long ulRegVal,ulValue;
         int iResult = ERR_NO_ERROR;
         // set IBC value as required
         ulIBCValue = 0;                                                // clear all bits
         if (ptyBreakpointFlags->bMatchASID)
            ulIBCValue |= BIT_23;                                       // set ASIDUse bit
         if (ptyBreakpointFlags->bGenBp)
            ulIBCValue |= BIT_0;                                        // set BE bit
         if (ptyBreakpointFlags->bGenTrigger)
            {                                                       // set TE bit
            ulIBCValue |= BIT_2;  

            
            if (tyCurrentUserSettings.bZephyrTraceSupport == 1)
               {
               ulTriggerVal = ulTriggerVal<<(ulIndex*4); 
               ulValue = 0x0000000f;                             //add trigger value to Trace Trigger Reg
               ulValue = ~(ulValue<<(ulIndex*4)); //shifting 0xf to the location to be cleared and negating it to 0x0
               if ((iResult = MLR_ReadCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,&ulRegVal,5)) == ERR_NO_ERROR)
                  {
                  
                  ulRegVal &= ulValue; 
                  ulRegVal = ulRegVal | ulTriggerVal;
                  if ((iResult = MLR_WriteCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,ulRegVal,5)) != ERR_NO_ERROR)
                     {
                     return iResult;
                     }
                  } 
               else
                  return iResult;

               }
            }
         // write breakpoint address
         if (ptyHandle->bOddAddressForHWMIPS16BPs && ptyBreakpointFlags->bMIPS16Breakpoint)
            ulAddress |= 0x00000001;
         iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulAddressDSUEjOfs, (unsigned char *)&ulAddress, 4);
         // write address mask
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulMaskDSUEjOfs, (unsigned char *)&(ptyBreakpointFlags->ulBpAddressMask), 4);
         // write ASID (if we can)
         if ((iResult == ERR_NO_ERROR) && ptyHandle->tyMipsDSU.bIBPASIDSupported)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulASIDDSUEjOfs, (unsigned char *)&(ptyBreakpointFlags->ulASIDToMatch), 4);
         // write control
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulIBCValue, 4);
         if (iResult != ERR_NO_ERROR)
            return iResult;                                             // could not set breakpoint
         // fill structure
         ptyBpResource->bBPAvailable = 0;
         ptyBpResource->ulAddressOfBP = ptyBreakpointFlags->ulBpAddress;
         ptyBpResource->ulAddressMaskOfBP = ptyBreakpointFlags->ulBpAddressMask;
         ptyBpResource->ulASIDOfBP = ptyBreakpointFlags->ulASIDToMatch;
         ptyBpResource->ulDataValueOfBP = 0;
         ptyBpResource->ulControlValueOfBP = ulIBCValue;
         ptyBpResource->bMIPS16BP = ptyBreakpointFlags->bMIPS16Breakpoint;
         // assing unique identifier and update it
         ptyBpResource->ulBreakpointIdentifier = ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp;
         ptyBreakpointFlags->ulBreakpointID = ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp;
         (ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp) += 2;            // keep it odd
         ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed++;                                // we have more breakpoints used
         return ERR_NO_ERROR;
         }
      }
   // some problem, we could not find available breakpoint
   return ERR_NO_HW_BP_LEFT;
}

/****************************************************************************
     Function: MLB_SetComplexDataHWBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               TyBpFlags *ptyBreakpointFlags - pointer to breakpoint flags
       Output: int - error code
  Description: Set complex hardware data breakpoint.
Date           Initials    Description
08-Feb-2008    VH          Initial
26-Nov-2011    SV          Modified to configure Zephyr Trace Register
****************************************************************************/
static int MLB_SetComplexDataHWBP(TyMLHandle *ptyHandle, TyBpFlags *ptyBreakpointFlags)
{
   unsigned long ulIndex;
   // check parameters
   assert(ptyHandle != NULL);
   if (ptyBreakpointFlags == NULL)
      return ERR_NO_ERROR;
   // are there any HW breakpoints available
   if (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed >= ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported)
      return ERR_NO_HW_BP_LEFT;
   // look for spare hardware breakpoint
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
      if (ptyBpResource->bBPAvailable)
         {  // we found available breakpoint
         unsigned long ulDBCValue = 0;
         int iResult = ERR_NO_ERROR;
         unsigned long ulTriggerVal = 0x000b0000;
         unsigned long ulRegVal,ulValue;
         // set IBC value as required
         ulDBCValue = 0;                                                // clear all bits
         if (ptyBreakpointFlags->bMatchASID)
            ulDBCValue |= BIT_23;                                       // set ASIDUse bit
         if (ptyBreakpointFlags->bGenBp)
            ulDBCValue |= BIT_0;                                        // set BE bit
         if (ptyBreakpointFlags->bGenTrigger)
            {                                                           // set TE bit
            ulDBCValue |= BIT_2;  
            
            //set Zephy Trace Trigger register
            if (tyCurrentUserSettings.bZephyrTraceSupport == 1)
               {
               ulTriggerVal = ulTriggerVal<<(ulIndex*4); 
               ulValue = 0x0000000f;                             //add trigger value to Trace Trigger Reg
               ulValue = ~(ulValue<<(ulIndex*4)); //shifting 0xf to the location to be cleared and negating it to 0x0
               if ((iResult = MLR_ReadCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,&ulRegVal,5)) == ERR_NO_ERROR)
                  {
                  
                  ulRegVal &= ulValue; 
                  ulRegVal = ulRegVal | ulTriggerVal;
                  if ((iResult = MLR_WriteCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,ulRegVal,5)) != ERR_NO_ERROR)
                     {
                     return iResult;
                     }
                  } 
               else
                  return iResult;
                  

               }
            }                                      // set TE bit
         if (!ptyBreakpointFlags->bFufillOnLoad)
            ulDBCValue |= BIT_12;                                       // set NoLB
         if (!ptyBreakpointFlags->bFufillOnStore)
            ulDBCValue |= BIT_13;                                       // set NoSB
         ulDBCValue |= (((unsigned long)(ptyBreakpointFlags->ucByteAccessIgnore)) << 14);       // set BAI bitfield (bits 14 to 21)
         ulDBCValue |= (((unsigned long)(ptyBreakpointFlags->ucByteLaneMask)) << 4);            // set BLM bitfield (bits 4 to 11)
         // write breakpoint address
         iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulAddressDSUEjOfs, (unsigned char *)&(ptyBreakpointFlags->ulBpAddress), 4);
         // write address mask
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulMaskDSUEjOfs, (unsigned char *)&(ptyBreakpointFlags->ulBpAddressMask), 4);
         // write ASID (if we can)
         if ((iResult == ERR_NO_ERROR) && ptyHandle->tyMipsDSU.bIBPASIDSupported)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulASIDDSUEjOfs, (unsigned char *)&(ptyBreakpointFlags->ulASIDToMatch), 4);
         // write data
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulDataValueDSUEjOfs, (unsigned char *)&(ptyBreakpointFlags->ulDataValue), 4);
         // write control
         if (iResult == ERR_NO_ERROR)
            iResult = MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulDBCValue, 4);
         if (iResult != ERR_NO_ERROR)
            return iResult;                                             // could not set breakpoint
         // fill structure
         ptyBpResource->bBPAvailable = 0;
         ptyBpResource->ulAddressOfBP = ptyBreakpointFlags->ulBpAddress;
         ptyBpResource->ulAddressMaskOfBP = ptyBreakpointFlags->ulBpAddressMask;
         ptyBpResource->ulASIDOfBP = ptyBreakpointFlags->ulASIDToMatch;
         ptyBpResource->ulDataValueOfBP = ptyBreakpointFlags->ulDataValue;
         ptyBpResource->ulControlValueOfBP = ulDBCValue;
         ptyBpResource->bMIPS16BP = 0;
         // assing unique identifier and update it
         ptyBpResource->ulBreakpointIdentifier = ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp;
         ptyBreakpointFlags->ulBreakpointID = ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp;
         (ptyHandle->tyMipsBreakpoints.ulUniqueIdentifierFoComplexBp) += 2;            // keep it odd
         ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed++;                                // we have more breakpoints used
         return ERR_NO_ERROR;
         }
      }
   // some problem, we could not find available breakpoint
   return ERR_NO_HW_BP_LEFT;
}

/****************************************************************************
     Function: MLB_ClearAllBP
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: int - error code
  Description: Clear all software and hardware breakpoints.
Date           Initials    Description
08-Feb-2008    VH          Initial
26-Nov-2011    SV          Modified to clear Zephyr Trace Register contents
****************************************************************************/
int MLB_ClearAllBP(TyMLHandle *ptyHandle)
{
   TyBreakpoint **pptyBp;
   // check parameters
   assert(ptyHandle != NULL);
   pptyBp = &(ptyHandle->tyMipsBreakpoints.ptyBreakpoint);
   // do for all possible breakpoints
   while (*pptyBp != NULL)
      {
      TyBreakpoint *ptyCurrentBp = *pptyBp;                             // current breakpoint
      if (ptyCurrentBp->tyBPType == BP_SW)
         {  // it is software breakpoint
         if (ptyCurrentBp->ulBPSize == 2)
            {  // 16-bit software breakpoint
            unsigned short usPreviousData = (unsigned short)(ptyCurrentBp->ulPreviousInstructionData & 0x0000FFFF);
            if (ptyHandle->bBigEndian != ptyCurrentBp->bBigEndian)
               usPreviousData = ML_SwapEndianHalfword(usPreviousData);
            (void)MLM_WriteMemoryVirtual(ptyHandle, ptyCurrentBp->ulBPAccessAddress, (unsigned char *)&usPreviousData, 2, 2);
            }
         else
            {  // 32-bit software breakpoint
            unsigned long ulPreviousData = ptyCurrentBp->ulPreviousInstructionData;
            if (ptyHandle->bBigEndian != ptyCurrentBp->bBigEndian)
               ulPreviousData = ML_SwapEndianWord(ulPreviousData);
            (void)MLM_WriteMemoryVirtual(ptyHandle, ptyCurrentBp->ulBPAccessAddress, (unsigned char *)&ulPreviousData, 4, 4);
            }
         }
      else
         {  // it is hardware breakpoint
         TyHWBPResource *ptyBpResource = ptyCurrentBp->ptyResource;
         if (ptyBpResource != NULL)
            {
            unsigned long ulValue = 0; 

            //clear Zephyr Trace Register
            if(tyCurrentUserSettings.bZephyrTraceSupport == 1)
              {
              (void)MLR_WriteCP0Register(ptyHandle,BRCM_TRACE_TRIGGER_REG,ulValue,5);
              }
            if (!ptyBpResource->bEmulatedWithWP)
               {  // it is real HW breakpoint using MIPS DSU
               unsigned long ulIBCValue = 0x0;                          // disable breakpoint (BE = TE = ASIDuse = 0, others 0)
               (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulIBCValue, 4);
               if (ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed > 0)
                  ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed--;       // decrement number of used hardware breakpoints
               }
            else
               {  // it is emulated breakpoint using watchpoint
               unsigned long ulIRInst = MIPS_EJTAG_EJWATCH_INST;
               // disable WE bit in EJWATCH register
               (void)ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRInst, MIPS_EJTAG_EJWATCH_LENGTH, &ulValue, NULL);
               (void)MLR_WriteRegister(ptyHandle, 0x32, MDIMIPCP0, 0);  // clear IWatchLo
               (void)MLR_WriteRegister(ptyHandle, 0x33, MDIMIPCP0, 0);  // clear IWatchHi
               if (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed > 0)
                  ptyHandle->tyMipsBreakpoints.ulWatchBPUsed--;         // decrement number of used watchpoints
               }
            ptyBpResource->bBPAvailable = 1;                            // breakpoint resource is available again
            }
         }
      *pptyBp = (*pptyBp)->ptyNext;                                     // remove current breakpoint from the list
      free(ptyCurrentBp);                                               // free allocated memory for current breakpoint
      }
   // no free all complex hardware resources
   (void)MLB_FreeAllComplexHWBpResources(ptyHandle);
   // always return success
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_FreeAllComplexHWBpResources
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: int - error code
  Description: Clear all complex hardware breakpoints.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLB_FreeAllComplexHWBpResources(TyMLHandle *ptyHandle)
{
   unsigned long ulIndex;
   assert(ptyHandle != NULL);
   // check all instruction breakpoint resources
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
      if (!ptyBpResource->bBPAvailable)
         {  // resource is used (not available)
         unsigned long ulIBCValue = 0;                                  // clear all bits in IBC register
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulIBCValue, 4);
         ptyBpResource->bBPAvailable = 1;
         if (ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed > 0)
            ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed--;
         }
      }
   // check all data breakpoint resources
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
      if (!ptyBpResource->bBPAvailable)
         {  // resource is used (not available)
         unsigned long ulDBCValue = 0;                                  // clear all bits in DBC register
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulDBCValue, 4);
         ptyBpResource->bBPAvailable = 1;
         if (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed > 0)
            ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed--;
         }
      }
   // make sure all HW breakpoints are available
   ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed = 0;
   ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed = 0;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_TempDisableAllHWBPs
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: int - error code
  Description: Disable all hardware breakpoints temporarily. 
               They can be restored using MLB_RestoreHWBPs.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_TempDisableAllHWBPs(TyMLHandle *ptyHandle)
{
   unsigned long ulIndex;
   // check parameters
   assert(ptyHandle != NULL);
   // check if any hardware breakpoints are currently used
   if ((ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed == 0) && (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed == 0) && 
       (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed == 0))
      return ERR_NO_ERROR;                                           // really no breakpoints are used so nothing to do
   // check instruction hardware breakpoints
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
      if (!ptyBpResource->bBPAvailable)
         {  // breakpoint in use, just write IBC register
         unsigned long ulIBCValue = 0;
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulIBCValue, 4);
         }
      }
   // check data hardware breakpoints
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
      if (!ptyBpResource->bBPAvailable)
         {  // breakpoint in use, just write DBC register
         unsigned long ulDBCValue = 0;
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpResource->ulControlDSUEjOfs, (unsigned char *)&ulDBCValue, 4);
         }
      }
   // finally check watchpoints
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfWPAsBP; ulIndex++)
      {
      TyHWBPResource *ptyBpResource = &(ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex]);
      if (!ptyBpResource->bBPAvailable)
         {  // disable watchpoint
         (void)MLR_WriteRegister(ptyHandle, 0x32, MDIMIPCP0, 0x0);      // clear IWatchLo
         }
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_RestoreHWBPs
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: int - error code
  Description: Restores hardware breakpoints disabled by MLB_TempDisableAllHWBPs. 
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLB_RestoreHWBPs(TyMLHandle *ptyHandle)
{
   unsigned long ulIndex;
   TyHWBPResource *ptyBpRes = NULL;
   // check parameters
   assert(ptyHandle != NULL);
   // check if any hardware breakpoints needs to be restored
   if ((ptyHandle->tyMipsBreakpoints.ulHWInstBPUsed == 0) && (ptyHandle->tyMipsBreakpoints.ulHWDataBPUsed == 0) && 
       (ptyHandle->tyMipsBreakpoints.ulWatchBPUsed == 0))
      return ERR_NO_ERROR;                                           // there is nothing to restore
   // check hardware instruction breakpoints
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported; ulIndex++)
      {
      ptyBpRes = &(ptyHandle->tyMipsBreakpoints.ptyHWInstBPResource[ulIndex]);
      if (!ptyBpRes->bBPAvailable)
         {  // this breakpoint need to be restored
         unsigned long ulAddressOfBp = ptyBpRes->ulAddressOfBP;
         if (ptyHandle->bOddAddressForHWMIPS16BPs && ptyBpRes->bMIPS16BP)
            ulAddressOfBp |= 0x00000001;
         // write address, mask, ASID and control for HW breakpoint
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulAddressDSUEjOfs, (unsigned char *)&ulAddressOfBp, 4);
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulMaskDSUEjOfs, (unsigned char *)&(ptyBpRes->ulAddressMaskOfBP), 4);
         if (ptyHandle->tyMipsDSU.bIBPASIDSupported)
            (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulASIDDSUEjOfs, (unsigned char *)&(ptyBpRes->ulASIDOfBP), 4);
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulControlDSUEjOfs, (unsigned char *)&(ptyBpRes->ulControlValueOfBP), 4);
         }
      }
   // check hardware data breakpoints
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported; ulIndex++)
      {
      ptyBpRes = &(ptyHandle->tyMipsBreakpoints.ptyHWDataBPResource[ulIndex]);
      if (!ptyBpRes->bBPAvailable)
         {  // this breakpoint need to be restored
         // write address, mask, ASID and control for HW breakpoint
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulAddressDSUEjOfs, (unsigned char *)&(ptyBpRes->ulAddressOfBP), 4);
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulMaskDSUEjOfs, (unsigned char *)&(ptyBpRes->ulAddressMaskOfBP), 4);
         if (ptyHandle->tyMipsDSU.bDBPASIDSupported)
            (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulASIDDSUEjOfs, (unsigned char *)&(ptyBpRes->ulASIDOfBP), 4);
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulDataValueDSUEjOfs, (unsigned char *)&(ptyBpRes->ulDataValueOfBP), 4);
         (void)MLM_WriteMemoryEjtag(ptyHandle, ptyBpRes->ulControlDSUEjOfs, (unsigned char *)&(ptyBpRes->ulControlValueOfBP), 4);
         }
      }
   // finaly, restore watchpoints
   for (ulIndex = 0; ulIndex < ptyHandle->tyMipsDSU.ulNumberOfWPAsBP; ulIndex++)
      {
      ptyBpRes = &(ptyHandle->tyMipsBreakpoints.ptyWatchpointHWInstBPResource[ulIndex]);
      if (!ptyBpRes->bBPAvailable)
         {  // this watchpoint needs to be restored
         unsigned long ulIRInst = MIPS_EJTAG_EJWATCH_INST;
         unsigned long ulValue = (ptyBpRes->ulAddressOfBP & 0xFFFFFFF8) | 0x00000004;     // discard lowest bits from address and set I bit
         (void)MLR_WriteRegister(ptyHandle, 0x32, MDIMIPCP0, ulValue);                    // write IWatchLo
         ulValue = 0x40000000;                                                            // set G bit for IWatchHi
         (void)MLR_WriteRegister(ptyHandle, 0x33, MDIMIPCP0, ulValue);                    // write IWatchHi
         ulValue = 0x1;                                                                   // set WE bit in EJWATCH
         (void)ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRInst, MIPS_EJTAG_EJWATCH_LENGTH, &ulValue, NULL);
         }
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_UpdateBPForEndianness
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               TyBreakpoint *ptyBp - pointer to breakpoint
       Output: int - error code
  Description: Change endianess of software breakpoints to match current target endianess.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLB_UpdateBPForEndianness(TyMLHandle *ptyHandle, TyBreakpoint *ptyBp)
{
   unsigned long ulSDBBP = MIPS_INST_SDBBP;
   unsigned short usSDBBP16 = MIPS_INST_SDBBP16;
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   // do we need to swap endianess
   if (ptyHandle->bEndianSwitchedForCacheFlush || (ptyBp == NULL) || (ptyBp->tyBPType != BP_SW) || (ptyBp->bBigEndian == ptyHandle->bBigEndian))
      return ERR_NO_ERROR;                                           // we do not have to worry about endianess
   // endianess is different and it means stored value needs to be swapped and instruction in the memory needs to be overwritten
   // first take care of stored value
   if (ptyBp->ulBPSize == 2)
      {  // 16-bit breakpoint
      unsigned short usPreviousData = (unsigned short)(ptyBp->ulPreviousInstructionData & 0x0000FFFF);
      usPreviousData = ML_SwapEndianHalfword(usPreviousData);
      ptyBp->ulPreviousInstructionData = (unsigned long)usPreviousData;
      }
   else
      {  // 32-bit breakpoint
      unsigned long ulPreviousData = ML_SwapEndianWord(ptyBp->ulPreviousInstructionData);
      ptyBp->ulPreviousInstructionData = ulPreviousData;
      }
   // update breakpoint endianess and make sure SDBBP instructions are in proper format
   ptyBp->bBigEndian = ptyHandle->bBigEndian;
   if (ptyBp->bBigEndian)
      {
      ulSDBBP = ML_SwapEndianWord(ulSDBBP);
      usSDBBP16 = ML_SwapEndianHalfword(usSDBBP16);
      }

   // now we need to write SDBBP instruction properly, we also reading value back if breakpoint was set properly
   if (ptyBp->ulBPSize == 2)
      {  // 16-bit breakpoint
      unsigned short usReadBack = 0;
      iResult = MLM_WriteMemoryVirtual(ptyHandle, ptyBp->ulBPAccessAddress, (unsigned char *)&usSDBBP16, 2, 2);
      if (iResult == ERR_NO_ERROR)
         iResult = MLM_ReadMemoryVirtual(ptyHandle, ptyBp->ulBPAccessAddress, (unsigned char *)&usReadBack, 2, 2);
      if ((iResult == ERR_NO_ERROR) && (usReadBack != usSDBBP16))
         iResult = ERR_SETBP_FAILED_SWBP;
      }
   else
      {  // 32-bit breakpoint
      unsigned long ulReadBack = 0;
      iResult = MLM_WriteMemoryVirtual(ptyHandle, ptyBp->ulBPAccessAddress, (unsigned char *)&ulSDBBP, 4, 4);
      if (iResult == ERR_NO_ERROR)
         iResult = MLM_ReadMemoryVirtual(ptyHandle, ptyBp->ulBPAccessAddress, (unsigned char *)&ulReadBack, 4, 4);
      if ((iResult == ERR_NO_ERROR) && (ulReadBack != ulSDBBP))
         iResult = ERR_SETBP_FAILED_SWBP;
      }
   return iResult;
}

/****************************************************************************
     Function: MLB_WriteBackSoftwareBreakpoints
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned char bWBOriginal- 1 to write back original values, 0 for breakpoints
       Output: int - error code
  Description: Write back or restore data for software breakpoints.
Date           Initials    Description
08-Feb-2008    VH          Initial
20-Nov-2008    SPC         Fixed the variable passing
****************************************************************************/
int MLB_WriteBackSoftwareBreakpoints(TyMLHandle *ptyHandle, unsigned char bWBOriginal)
{
   unsigned long ulSDBBP = MIPS_INST_SDBBP;
   unsigned short usSDBBP16 = MIPS_INST_SDBBP16;
   TyBreakpoint *ptyBp = NULL;
   // check parameters
   assert(ptyHandle != NULL);
   // make sure memory writing is enabled
   ML_EnableMemoryWriting(ptyHandle, 0, 0, 0);
   if (ptyHandle->bBigEndian)
      {
      ulSDBBP = ML_SwapEndianWord(ulSDBBP);
      usSDBBP16 = ML_SwapEndianHalfword(usSDBBP16);
      }
   // go through all software breakpoints
   ptyBp = ptyHandle->tyMipsBreakpoints.ptyBreakpoint;
   while (ptyBp != NULL)
      {
      if (ptyBp->tyBPType == BP_SW)
         {  // it is software breakpoint
         if (ptyBp->ulBPSize == 2)
            {  // 16-bit breakpoint
            unsigned short usDataToWrite;
            if (bWBOriginal)
               {
               usDataToWrite = (unsigned short)(ptyBp->ulPreviousInstructionData & 0x0000FFFF);
               if (ptyBp->bBigEndian != ptyHandle->bBigEndian)
                  usDataToWrite = ML_SwapEndianHalfword(usDataToWrite);
               }
            else
               usDataToWrite = usSDBBP16;
            (void)MLM_WriteMemoryVirtual(ptyHandle, ptyBp->ulBPAccessAddress, (unsigned char *)&usDataToWrite, 2, 2);
            }
         else
            {  // 32-bit breakpoint
            unsigned long ulDataToWrite;
            if (bWBOriginal)
               {
               ulDataToWrite = ptyBp->ulPreviousInstructionData;
               if (ptyBp->bBigEndian != ptyHandle->bBigEndian)
                  ulDataToWrite = ML_SwapEndianWord(ulDataToWrite);
               }
            else
               ulDataToWrite = ulSDBBP;
            (void)MLM_WriteMemoryVirtual(ptyHandle, ptyBp->ulBPAccessAddress, (unsigned char *)&ulDataToWrite, 4, 4);
            }
         }
      ptyBp = ptyBp->ptyNext;
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLB_GetBPIdForComplexBreak
     Engineer: Nikolay Chokoev
        Input: *ptyHandle  : handle
			   bInstBP     : TRUE for instruction BP else Data BP
               ulBrkStatus : bitmap extracted from IBS or DBS, a bit
                             identifies the HW resource that caused a break
       Output: the BPId
  Description: Determine the breakpoint ID (if any) for the complex
               hardware BP that caused a break, else 0
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
unsigned long MLB_GetBPIdForComplexBreak(TyMLHandle *ptyHandle,
										 int bInstBP, 
										 unsigned long ulBrkStatus)
{

   unsigned long 	ulIndex;
   unsigned long 	ulMaxBP;
   HWBP_RESOURCE  	*pArray;

   if (bInstBP)      // Instruction
      {
      ulMaxBP = ptyHandle->tyMipsDSU.ulNumberOfIBPsSupported;
      pArray = HWInstBPArray;
      }
   else              // Data
      {
      ulMaxBP = ptyHandle->tyMipsDSU.ulNumberOfDBPsSupported;
      pArray = HWDataBPArray;
      }

   for (ulIndex = 0; ulIndex < ulMaxBP; ulIndex++)
      {
      if ((ulBrkStatus & 0x01) &&
          (!pArray[ulIndex].bBreakpointAvailable))
         {
         return pArray[ulIndex].ulBreakpointIdentifier;
         }
      ulBrkStatus >>= 1;
      }

   return 0;
}

