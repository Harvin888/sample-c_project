/****************************************************************************
       Module: toplayer.h
     Engineer: Nikolay Chokoev
  Description: Header for Opella-XD MIPS Top Layer MDI.
Date           Initials    Description
04-Jul-2008    NCH          Initial
05-Jan-2009    SPC          Moved TyApplicationSpecs struct to midlayer.h
****************************************************************************/
#ifndef __TOPLAYER_H__
#define __TOPLAYER_H__

#include "stdio.h"

#define MAX_FILENAME_SIZE 0x300
#define  SCREENWIDTH 150

/* Valid values for MDIDDataT.FISA: */
#define MDIMIP_FISA_M1 "MIPSI"
#define MDIMIP_FISA_M2 "MIPSII"
#define MDIMIP_FISA_M3 "MIPSIII"
#define MDIMIP_FISA_M4 "MIPSIV"
#define MDIMIP_FISA_M5 "MIPSV"
#define MDIMIP_FISA_M32 "MIPS32"
#define MDIMIP_FISA_M64 "MIPS64"

#define MDI_NO_DEVICE_OPEN		0x22222222

typedef struct
{
   FILE  *pGlobalMDILogFile;        // Global File Pointer
   int   bStaticLogMDICalls;         // Global enable for the MDILog feature
   int   bStaticLogTimeStamp;        // Global Option to enable Log timestamps
   int   bLogMemoryRead;             // Global Option to log all memory read by the debugger
   int   bLogMemoryWritten;          // Global Option to log all memory Written by the debugger
   int   bAppendLogfile;             // Global Option to append to logfile. Otherwise file is wiped.
} TyMDILogSettings;

int TL_MDILogInitAndCheckSettings(TyMDILogSettings ** pptyMDILogSettings);
MDIInt32 TL_QuerySupportedDevices(MDIInt32 * pHowMany, MDIDDataT* pDeviceData);
int TL_DeviceConnect(void);
TyError TL_ClearAllBP (void);
int TL_DeviceDisconnect(void);
MDIInt32 TL_RegisterRead   (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count);
MDIInt32 TL_MDIRead        (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize);
MDIInt32 TL_MemoryRead     (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize);
MDIInt32 TL_RegisterWrite  (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count);
MDIInt32 TL_MemoryWrite    (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize);
MDIInt32 TL_MDIWrite       (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize);
TyError TL_Execute(void);
TyError TL_Step(MDIUint32 Steps, MDIUint32 Mode);
TyError TL_Stop(void);
TyError TL_GetStatus(TyStatus *ptyStatus);
TyError TL_Reset(MDIUint32 Mode);
TyError TL_SetBP(unsigned long ulBpVirtAddress,
                 unsigned long ulBpAccessAddress,
                 TyBPType bptBPType,
                 unsigned long ulBPSize);
TyError TL_SetComplexHWBP(TyBpFlags *ptyBreakpointFlags);
TyError TL_ClearBP(unsigned long 	ulAddress,
                   TyBPType			*pbptBPType,
                   unsigned long	*pulBPSize,
                   unsigned long	*pulBpAccessAddress);
//int TL_DeviceDisconnect(void);
TyError TL_GetDWFWVersions   (char *pszDiskware1Version,
                              char *pszFirmwareVersion,
                              char *pszFpgaware1Version,
                              char *pszFpgaware2Version,
                              char *pszFpgaware3Version,
                              char *pszFpgaware4Version);
TyError TL_DeviceConnect_Ex(int bSpecifiedProcessor);

TyError TL_CacheFlush(MDIUint32 Type, MDIUint32 Flag);
#endif   // __TOPLAYER_H__

