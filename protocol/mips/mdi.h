/****************************************************************************
       Module: mdi.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD MIPS MDI.
Date           Initials    Description
08-Feb-2008    VH          Initial
19-Dec-2008    RS          Added two register defenitions for Microchip
11-Aug-2009    SPC         Added OS MMU management
01-Nov-2011    ANGS        Added AshTraceElfPath() in extern "C"
****************************************************************************/
#ifndef MDI_H_
#define MDI_H_

#define MDI_CONFIG_FILE_NAME "AshMDI.cfg"
#define POWER_DOWN_MODE_STRING "Warning: Processor Currently In Power Down Mode"
#define MDIMIPVIRTUAL 	0x00001000 /* 0x10xx: 0x1000+ASID value */

// valid MDI resources
#define MDIMIPCPU                   1
#define MDIMIPPC                    2
#define MDIMIPHILO                  3
#define MDIMIPTLB                   4
#define MDIMIPPICACHET              5
#define MDIMIPPUCACHET              5
#define MDIMIPPDCACHET              6
#define MDIMIPSICACHET              7
#define MDIMIPSDCACHET              8
#define MDIMIP192ACC                9
#define MDIMIPCP0                   10
#define MDIMIPCP0C                  11
#define MDIMIPCP1                   12
#define MDIMIPCP1C                  13
#define MDIMIPCP2                   14
#define MDIMIPCP2C                  15
#define MDIMIPCP3                   16
#define MDIMIPCP3C                  17
#define MDIMIPFP                    18
#define MDIMIPDFP                   19
#define MDIMIPPICACHE               20
#define MDIMIPPUCACHE               20
#define MDIMIPPDCACHE               21
#define MDIMIPSICACHE               22
#define MDIMIPSUCACHE               22
#define MDIMIPSDCACHE               23
#define MDIMIPPHYSICAL              24
#define MDIMIPGVIRTUAL              25
#define MDIMIPEJTAG                 26
#define MDIMIPSRS                   27
// These are new resources for HiperSmart MIPS, have not been allocated by MIPS yet.
#define MDIMIPSICACHEWS             60
#define MDIMIPSDCACHEWS             61

// MDI interface definition and structures
typedef union
{
    unsigned long   ulData;
    struct
    {                                   // LSB First
        unsigned long  DEVRST         :1;   //lint !e46 Device Reset State
        unsigned long  FAEN           :1;   //lint !e46 Flash Access Enable
        unsigned long  FCBUSY         :1;   //lint !e46 Flash Controller Busy
        unsigned long  CFGRDY         :1;   //lint !e46 Configuration Ready
        unsigned long  ZeroRange1     :3;   //lint !e46 Unused
        unsigned long  CPS            :1;   //lint !e46 Code Protection Staus
        unsigned long  ZeroRange2     :24;   //lint !e46 Common. Unused
    } Bits;                             // Down to MSB
}TyMicrochipStatusRegister;

typedef struct 
{
    unsigned long ulBmxConValue;
    unsigned long ulBmxDkPbaValue;
    unsigned long ulBmxDuDbaValue;
    unsigned long ulBmxDuPbaValue;
    bool          bBmxRegistersModified;
} TyMicrochipBmxRegContext;

typedef unsigned int MDIUint32;
typedef int MDIInt32;

#ifdef WIN32
  typedef unsigned __int64 MDIUint64;
  typedef __int64 MDIInt64;
  #ifndef __stdcall
    #define __stdcall __stdcall
  #endif
#else
  #if __WORDSIZE == 64
    typedef unsigned long MDIUint64;
    typedef long MDIInt64;
  #else
    typedef unsigned long long MDIUint64;
    typedef long long MDIInt64;
  #endif
  #ifndef __stdcall
    #define __stdcall
  #endif
  #ifndef __declspec
    #define __declspec(e)
  #endif
#endif
typedef MDIUint32 MDIVersionT; 			// MDI version format

typedef struct MDIVersionRange_struct 	// MDI supported version range
{
   MDIVersionT oldest;
   MDIVersionT newest;
} MDIVersionRangeT;
/*
 * Define various revision fields
 */
#define MDIMajor           2
#define MDIMinor           32 /* 32 decimal */
#define MDIOldMajor        1
#define MDIOldMinor        0
#define MDICurrentRevision ((MDIMajor << 16 ) | MDIMinor)
#define MDIOldestRevision  ((MDIOldMajor << 16) | MDIOldMinor)

typedef MDIUint32 MDIHandleT;

#define MDINoHandle                 ((MDIHandleT) - 1)
typedef MDIUint32 MDITGIdT;

typedef struct MDITGData_struct
{
    MDITGIdT TGId;       /* MDI ID to reference this Target Group */
    char     TGName[81]; /* Descriptive string identifying this TG */
} MDITGDataT;
typedef MDIUint32 MDIDeviceIdT;

//*******************************************
// Defined for Ashling
typedef struct
{
   int 	iNumberOfMDIConnectionsEstablished;
   int  bFailedToConnectToTarget;
   MDIHandleT 	tyCurrentMDIHandle;
   MDIDeviceIdT tyCurrentDeviceID;
}TyLocMdiParams_t;
//*******************************************

typedef struct MDIDData_Struct
{
    MDIDeviceIdT Id;    /* MDI ID to reference this device */
    char DName[81];     /* Descriptive string identifying this device */
    char Family[15];    /* Device�s Family (CPU, DSP) */
    char FClass[15];    /* Device�s Class (MIPS, X86, PPC) */
    char FPart[15];     /* Generic Part Name */
    char FISA[15];      /* Instruction Set Architecture */
    char Vendor[15];    /* Vendor of Part */
    char VFamily[15];   /* Vendor Family name */
    char VPart[15];     /* Vendor Part Number */
    char VPartRev[15];  /* Vendor Part Revision Number */
    char VPartData[15]; /* Used for Part Specific Data */
    char Endian;        /* 0 Big Endian, 1 Little Endian */
} MDIDDataT;
/* Valid values for MDIDDataT.Family: */
#define MDIFamilyCPU "CPU"
#define MDIFamilyDSP "DSP"
/* Valid values for MDIDDataT.Endian: */
#define MDIEndianBig    0
#define MDIEndianLittle 1
/* MDI Resources */
typedef MDIUint32 MDIResourceT;
typedef MDIUint64 MDIOffsetT;

typedef struct MDIRange_struct
{
   MDIOffsetT Start;
   MDIOffsetT End;
} MDIRangeT;

typedef struct MDICRange_struct
{
   MDIOffsetT Offset;
   MDIResourceT Resource;
   MDIInt32 Count;
} MDICRangeT;

typedef struct MDIConfig_struct
{
    /* Provided By: Other Comments */
    char User[80];             /* Host: ID of caller of MDI */
    char      Implementer[80]; /* MDI ID of who implemented MDI */
    MDIUint32 MDICapability;   /* MDI: Flags for optional capabilities */
    /* Host: CB fn for MDI output */
    MDIInt32 (__stdcall *MDICBOutput) (MDIHandleT Device,
                                       MDIInt32 Type,
                                       char *Buffer,
                                       MDIInt32 Count );
    /* Host: CB fn for MDI input */
    MDIInt32 (__stdcall *MDICBInput) (MDIHandleT Device,
                                      MDIInt32 Type,
                                      MDIInt32 Mode,
                                      char **Buffer,
                                      MDIInt32 *Count);
    /* Host: CB fn for expression eval */
    MDIInt32 (__stdcall *MDICBEvaluate) (MDIHandleT Device,
                                         char *Buffer,
                                         MDIInt32 *ResultType,
                                         MDIResourceT *Resource,
                                         MDIOffsetT *Offset,
                                         MDIInt32 *Size,
                                         void **Value);
    /* Host: CB fn for sym/src lookup */
    MDIInt32 (__stdcall *MDICBLookup) (MDIHandleT Device,
                                       MDIInt32 Type,
                                       MDIResourceT Resource,
                                       MDIOffsetT Offset,
                                       char **Buffer );
    /* Host: CB fn for Event processing */
    MDIInt32 (__stdcall *MDICBPeriodic)(MDIHandleT Device);
    /* Host: CB fn for Synchronizing */
    MDIInt32 (__stdcall *MDICBSync)(MDIHandleT Device,
                                    MDIInt32 Type,
                                    MDIResourceT Resource);
} MDIConfigT;

/* MDIConfigT.MDICapability flag values, can be OR�ed together */
#define MDICAP_NoParser      1    /* No command parser */
#define MDICAP_NoDebugOutput 2    /* No Target I/O */
#define MDICAP_TraceOutput   4    /* Supports Trace Output */
#define MDICAP_TraceCtrl     8    /* Supports Trace Control */
#define MDICAP_TargetGroups  0x10 /* Supports Target Groups */
#define MDICAP_PDtrace       0x20 /* Supports PDtrace functions */
#define MDICAP_TraceFetchI   0x40 /* Supports Instruction Fetch during Trace */
#define MDICAP_TC            0x80 /* Supports Thread Contexts */
#define MDICAP_Teams         0x100 /* Supports Teams */
typedef struct MDIRunState_struct
{
    /**
     * MdiStatus values
     *
     *  Temporary states:
     *   NotRunning group: Halted, StepsDone, BPHit, UsrBPHit, Exception, TraceFull,...
     *
     *     These states will show up on the 1st call to RunState().
     *      After the 1st call, the new state will be MDIStatusNotRunning.
     *
     *  States used for transitions:
     *    These state inform the RunLoop() that a specific MdiDevice is requesting
     *    a cpu state (DevState) change.
     *
     *     MdiStatusHalting:
     *       the mdi device wants to stop. The cpu could still be running or already halted.
     *
     *     MdiStatusStartingRun:
     *       the mdi device would like to run as soon as possible. The cpu might still be halted by other vdev.
     *
     */
    MDIUint32 Status;
    union u_info
    {
        void *ptr;
        MDIUint32 value;
    } Info;
} MDIRunStateT;

/* Status values:  Info interpretation: */
#define MDIStatusNotRunning  1   /* none */
#define MDIStatusRunning     2   /* none */
#define MDIStatusHalted      3   /* none */
#define MDIStatusStepsDone   4   /* none */
#define MDIStatusExited      5   /* Info.value = exit value */
#define MDIStatusBPHit       6   /* Info.value = BpID */
#define MDIStatusUsrBPHit    7   /* none */
#define MDIStatusException   8   /* Info.value = which exception */
#define MDIStatusTraceFull   9   /* none */
/* 10 is skipped to maintained backward compatibility */
#define MDIStatusDisabled    11  /* Device is not in execution mode */
// additional Ashling specific values for status
#define MDIStatusInstBPHit          0x40
#define MDIStatusDataReadBPHit      0x41
#define MDIStatusDataWriteBPHit     0x42
#define MDIStatusMask        0xff /* Status values are in lowest byte */
/* These can be OR�ed in with MDIStatusRunning and MDIStatusNotRunning
 */
#define MDIStatusReset     0x100 /* currently held reset */
#define MDIStatusWasReset  0x200 /* reset asserted & released */
#define MDIStatusResetMask 0x300 /* reset state mask */
/* This can also be OR�ed in with MDIStatusHalted */
#define MDIStatusDescription 0x0400 /* Info.ptr = Descriptive string */

typedef struct MDICacheInfo_struct
{
    MDIInt32  Type;
    MDIUint32 LineSize;    /* Bytes of data in a cache line */
    MDIUint32 LinesPerSet; /* Number of lines in a set */
    MDIUint32 Sets;        /* Number of sets */
} MDICacheInfoT;

/* Values for MDICacheInfoT.Type (Cache types): */
#define MDICacheTypeNone            0
#define MDICacheTypeUnified         1
#define MDICacheTypeInstruction     2
#define MDICacheTypeData            3

typedef MDIUint32 MDIBpT;
#define MDIBPT_SWInstruction          1
#define MDIBPT_SWOneShot              2
#define MDIBPT_HWInstruction          3
#define MDIBPT_HWData                 4
#define MDIBPT_HWBus                  5
#define MDIBPT_HWInstructionPrimed    6
#define MDIBPT_HWInstructionQualified 7
#define MDIBPT_HWInstructionTuple     8
#define MDIBPT_HWDataPrimed           9
#define MDIBPT_HWDataQualified        10
#define MDIBPT_HWDataTuple            11
/* Hardware breakpoint types may have one or more of the following */
/* flag bits OR�ed in to specify additional qualifications. */
#define MDIBPT_HWFlg_AddrMask     0x10000
#define MDIBPT_HWFlg_AddrRange    0x20000
#define MDIBPT_HWFlg_DataValue    0x40000
#define MDIBPT_HWFlg_DataMask     0x80000
#define MDIBPT_HWFlg_DataRead     0x100000
#define MDIBPT_HWFlg_DataWrite    0x200000
#define MDIBPT_HWFlg_Trigger      0x400000
#define MDIBPT_HWFlg_TriggerOnly  0x800000
#define MDIBPT_HWFlg_TCMatch      0x1000000
#define MDIBPT_HWFlg_InvertMatch  0x2000000 // �not� match
#define MDIBPT_HWFlg_TypeQualMask 0xffff0000
#define MDIBPT_TypeMax      MDIBPT_HWDataTuple
#define MDIBPT_TypeMask     0xff

// following was added by Ashling to support setting of SW breakpoints in ares with inhibit attributes
#define MDIBPT_SWFlg_UseNonStandardAddress   0x01000000

/* Hardware breakpoint types 6 to 11 must specify the index of the instruction or
   data breakpoint in MDIBPT_IndexMask bits */
#define MDIBPT_IndexMask    0xff00
typedef MDIUint32 MDIBpIdT;
#define MDIAllBpID (~(MDIBpIdT)0)
typedef struct MDIBpData_struct
{
  MDIBpIdT Id;
  MDIBpT Type;
  MDIUint32    Enabled;    /* 0 if currently disabled, else 1 */
  MDIResourceT Resource;
  MDIRangeT    Range;      /* Range.End may be an end addr or mask */
  MDIUint64    Data;       /* valid only for data write breaks */
  MDIUint64    DataMask;   /* valid only for data write breaks */
  MDIUint32    PassCount;  /* Pass count reloaded when hit */
  MDIUint32    PassesToGo; /* Passes to go until next hit */
} MDIBpDataT;
#define MDIBPT_HWType_Exec       1
#define MDIBPT_HWType_Data       2
#define MDIBPT_HWType_Bus        4
#define MDIBPT_HWTYpe_AlignMask  0xf0
#define MDIBPT_HWType_AlignShift 4
#define MDIBPT_HWType_MaxSMask   0x3f00
#define MDIBPT_HWType_MaxSShift  8
#define MDIBPT_HWType_VirtAddr   0x4000
#define MDIBPT_HWType_ASID       0x8000
typedef struct MDIBpInfo_struct
{
    MDIInt32  Num;
    MDIUint32 Type;
} MDIBpInfoT;
/* MDI Trace data type */
typedef struct MDITrcFrame_Struct
{
   MDIUint32 Type;
   MDIResourceT Resource;
   MDIOffsetT Offset;
   MDIUint64 Value;
} MDITrcFrameT;

/*
 ** following was added by Ashling to support
 ** For MDISetBp(), MDISetSWBp(), and MDITraceRead(), setting the low
 ** order address bit to 1 means that the addressed instruction is a
 ** MIPS16 instruction.
 */
#define MDIMIP_Flg_MIPS16 1

#define MDITTypePC       1  /* Instruction address only */
#define MDITTypeInst     2  /* Instruction address and value */
#define MDITTypeRead     3  /* Data Load address only */
#define MDITTypeWrite    4  /* Data Store address only */
#define MDITTypeAccess   5  /* Data Access (Load/Store) address only */
#define MDITTypeRData_8  6  /* Data Load address and 8-bit value */
#define MDITTypeWData_8  7  /* Data Store address and 8-bit value */
#define MDITTypeRData_16 8  /* Data Load address and 16-bit value */
#define MDITTypeWData_16 9  /* Data Store address and 16-bit value */
#define MDITTypeRData_32 10 /* Data Load address and 32-bit value */
#define MDITTypeWData_32 11 /* Data Store address and 32-bit value */
#define MDITTypeRData_64 12 /* Data Load address and 64-bit value */
#define MDITTypeWData_64 13 /* Data Store address and 64-bit value */

/* Values for Flags parameter to MDITGOpen() and MDIOpen(): */
#define MDISharedAccess    0
#define MDIExclusiveAccess 1
/* Values for Flags parameter to MDITGClose() and MDIClose(): */
#define MDICurrentState 0
#define MDIResetState   1
/* Values for SyncType parameter to MDICBSync(): */
#define MDISyncBP    0
#define MDISyncState 1
#define MDISyncWrite 2
/* Values for Direction parameter to MDIMove(): */
#define MDIMoveForward  0
#define MDIMoveBackward 1
/* Values for Mode parameter to MDIFind(): */
#define MDIMatchForward     0
#define MDIMismatchForward  1
#define MDIMatchBackward    2
#define MDIMismatchBackward 3
/* Values for Mode parameter to MDIStep(): */
#define MDIStepInto    0
#define MDIStepForward 1
#define MDIStepOver    2
#define MDINoStep      ((MDIUint32)~0)
/* "Wait Forever" value for WaitTime parameter to MDIRunState(): */
#define MDIWaitForever              (-1)
/* Values for Mode parameter to MDIReset(): */
#define MDIFullReset       0
#define MDIDeviceReset     1
#define MDICPUReset        2
#define MDIPeripheralReset 3
/* Values for Flags parameter to MDIReset(): */
#define MDINonIntrusive    1

// Ashling specific values for Mode parameter to MDIReset()
#define MDIPrRstReset               100
#define MDIPerRstReset              101
#define MDIJtagRstReset             102
#define MDITRSTReset                103
#define MDIRSTReset                 104

/* Values for Flags parameter to MDICacheFlush(): */
#define MDICacheHit        0
#define MDICacheWriteBack  1
#define MDICacheInvalidate 2
#define MDICacheWBInval    (MDICacheWriteBack|MDICacheInvalidate)
/* 4 is skipped for backward compatibility */
#define MDICacheLock       5
#define MDICacheIndex      0x80
/* Values for Status parameter from MDITraceStatus(): */
#define MDITraceStatusNone    1
#define MDITraceStatusTracing 2
#define MDITraceStatusWaiting 3
#define MDITraceStatusFilling 4
#define MDITraceStatusStopped 5
/* Values for Type parameter to MDICBOutput() and MDICBInput(): */
#define MDIIOTypeMDIIn    1
#define MDIIOTypeMDIOut   2
#define MDIIOTypeMDIErr   3
#define MDIIOTypeTgtIn    4
#define MDIIOTypeTgtOut   5
#define MDIIOTypeTgtErr   6
#define MDIIOTypeMDNotify 7
/* Values for Mode parameter to MDICBInput(): */
#define MDIIOModeNormal 1
#define MDIIORawBlock   2
#define MDIIORawNoBlock 3
/* Values for Type parameter to MDICBEvaluate(): */
#define MDIEvalTypeResource 1
#define MDIEvalTypeChar     2
#define MDIEvalTypeInt      3
#define MDIEvalTypeUInt     4
#define MDIEvalTypeFloat    5
#define MDIEvalTypeNone     6
/* Values for Type parameter to MDICBLookup(): */
#define MDILookupNearest 1
#define MDILookupExact   2
#define MDILookupSource  3

/* MDI function return values: */
#define MDISuccess                    0  /* Success */
#define MDINotFound                   1  /* MDIFind() did not find a match */
#define MDIErrFailure                -1  /* Unable to perform operation */
#define MDIErrDevice                 -2  /* Invalid Device handle */
#define MDIErrSrcResource            -3  /* Invalid Resource type */
#define MDIErrDstResource            -4  /* Invalid Resource type */
#define MDIErrInvalidSrcOffset       -5  /* Offset is invalid for the specified
                                            resource */
#define MDIErrInvalidDstOffset       -6  /* Offset is invalid for the specified
                                            resource */
#define MDIErrSrcOffsetAlignment     -7  /* Offset is not correctly aligned for the
                                            specified ObjectSize*/
#define MDIErrDstOffsetAlignment     -8  /* Offset is not correctly aligned for the
                                            specified ObjectSize */
#define MDIErrSrcCount               -9  /* Count causes reference outside of
                                            the resource space */
#define MDIErrDstCount               -10 /* Count causes reference outside of
                                            the resource space */
#define MDIErrBPType                 -13 /* Invalid breakpoint type */
#define MDIErrRange                  -14 /* Specified range is outside of the
                                            scope for the resource */
#define MDIErrNoResource             -15 /* Hardware resources not available */
#define MDIErrBPId                   -16 /* Invalid Breakpoint ID */
#define MDIErrMore                   -17 /* More data is available than was
                                            requested */
#define MDIErrParam                  -18 /* A parameter is in error, see
                                            specific instructions */
#define MDIErrTGHandle               -19 /* Invalid Target Group Handle */
#define MDIErrMDIHandle              -20 /* Invalid MDI Environment Handle */
#define MDIErrVersion                -21 /* Version not supported */
#define MDIErrLoadLib                -22 /* MDIInit(): Error loading library */
#define MDIErrModule                 -23 /* MDIInit(): Unable to link required
                                            MDI functions from library */
#define MDIErrConfig                 -24 /* Required callback functions not
                                            present */
#define MDIErrDeviceId               -25 /* Invalid device ID */
#define MDIErrAbort                  -26 /* Command has been aborted */
#define MDIErrUnsupported            -27 /* Unsupported feature */
#define MDIErrLookupNone             -28 /* Address did not match a symbol or
                                            source line. */
#define MDIErrLookupError            -29 /* Invalid address for look up. */
#define MDIErrTracing                -30 /* Can�t clear trace buffer while
                                            capturing is in progress */
#define MDIErrInvalidFunction        -31 /* Function pointer is invalid */
#define MDIErrAlreadyConnected       -32 /* MDI Connection has already been made
                                            for this thread */
#define MDIErrTGId                   -33 /* Invalid Target Group ID */
#define MDIErrDuplicateBP            -34 /* A similar breakpoint has already been
                                            defined for this device, or for global
                                            breakpoints on any device */
#define MDIErrInvalidFrames          -35 /* Range of requested trace frames is
                                            invalid */
#define MDIErrWrongThread            -36 /* Call was not made by the connected
                                            thread */
#define MDIErrTargetRunning          -37 /* Trying to change execution mode of the
                                            thread when it is running */
#define MDIErrRecursive              -38 /* Illegal recursive call from from
                                            MDICDPeriodic */
#define MDIErrSrcObjectSize          -39 /* Invalid ObjectSize for resource */
#define MDIErrDstObjectSize          -40 /* Invalid ObjectSize for resource */
#define MDIErrTCId                   -41 /* TC is not valid for device */
#define MDIErrTooManyTeams           -42 /* Too many teams for MDILib */
#define MDIErrTeamId                 -43 /* Invalid team ID */
#define MDIErrDisabled               -44 /* Device is disabled */
#define MDIErrAlreadyMember          -45 /* Device is already a team member */
#define MDIErrNotMember              -46 /* Device is not a team member */
typedef MDIInt32 MDITCIdT;
typedef struct {
    MDIUint32 TCId;
    MDIUint32 Status;
} MDITCDataT;
#define MDITCStatusHalted         0
#define MDITCStatusFree           1
#define MDITCStatusRunning        2
#define MDITCStatusBlockedOnWait  3
#define MDITCStatusBlockedOnYield 4
#define MDITCStatusBlockedOnGS    5
#define MDITCStatusMask 0xff
#define MDITCStatusSuspended 0x100
typedef MDIInt32 MDITeamIdT;
typedef struct {
    /* MDIHandle is no longer used but it is remained here
       for backword compatibility for FS2 */
    MDIHandleT   MDIHandle;
    MDIHandleT   TGHandle;
    MDIHandleT   DevHandle;
    MDIUint32    Flags;
} MDITMDataT;
/* Cond parameter to MDISetBpPrimingCondition(): */
typedef MDIUint32 MDIPrimingConditionT;
/* Values for ConfigType parameter to MDICbtConfigQuery(): */
typedef MDIUint32 MDICbtConfigTypeT;
#define MDICBT_ConfigType_Primed    0
#define MDICBT_ConfigType_Qualified 1
#define MDICBT_ConfigType_Tuple     2
#define MDICBT_ConfigType_StopWatch 3
/* Values for BkptType parameter to MDICbtConfigQuery(): */
typedef MDIUint32 MDICbtBPTypeT;
#define MDICBT_BPType_StopWatchPair 0
#define MDICBT_BPType_Instruction   1
#define MDICBT_BPType_Data          2
/* Type and Type2 parameters to MDICbtConfigQuery(): */
typedef MDIUint32 MDICbtConfigItemTypeT;
/* Index parameter to MDICbtConfigQuery(): */
typedef MDIUint32 MDICbtIndexT;
/* Values for CbtConfig parameter to MDICbtConfigQuery(): */
typedef struct MDICbtConfig_struct
{
    MDICbtConfigItemTypeT Type;//type of configuration item
    MDICbtIndexT Index;//index for item
    MDICbtConfigItemTypeT Type2;//used for stopwatch pairs only
    MDICbtIndexT Index2;// used for stopwatch pairs only
} MDICbtConfigT;
/* Values for MDICbtConfigT.Type and .Type2 (Config types): */
#define MDICBT_Config_Bypass      0  //used for primed
#define MDICBT_Config_Instruction 1
#define MDICBT_Config_Data        2
/* Value parameter to MDIGetStopWatchValue(): */
typedef MDIUint32 MDIStopWatchValueT;
/* Values for Mode parameter to MDISetStopWatchMode(): */
typedef MDIUint32 MDIStopWatchModeT;
#define MDICBT_StopWatch_FreeRun 0  // in free run mode
#define MDICBT_StopWatch_Pair    1  // pair defined in PairIndex
/* PairIndex parameter to MDISetStopWatchMode(): */
typedef MDIUint32 MDIPairIndexT;
/* StartInstIndex parameter to MDISetStopWatchMode(): */
typedef MDIUint32 MDIStartIndexT;
/* StopInstIndex parameter to MDISetStopWatchMode(): */
typedef MDIUint32 MDIStopIndexT;
/* Function Prototypes */
#ifdef __cplusplus
extern "C" 
{
#endif

#if defined( MDI_LIB )
  /* MDILib, do extern function declarations */
#ifdef __LINUX
#define STDCALLEXP
#define yf(str) extern MDIInt32 str
#else //__LINUX
//#define yf(str) extern MDIInt32 __declspec(dllexport) str
#define yf(str) extern MDIInt32 str
#define STDCALLEXP __stdcall
#endif //__LINUX
#elif defined( MDILOAD_DEFINE )
#define STDCALLEXP
  /* debugger, do function pointer definitions */
#ifdef __LINUX
#define yf(str) MDIInt32 (*str)
#else //__LINUX
#define yf(str) MDIInt32 (__stdcall *str)
#endif //__LINUX
#else
#define STDCALLEXP
  /* debugger, do extern function pointer declarations */
#ifdef __LINUX
#define yf(str) MDIInt32 (*str)
#else //__LINUX
#define yf(str) MDIInt32 (__stdcall *str)
#endif
#endif //__LINUX
    /* 0 */
yf(MDIVersion)(MDIVersionRangeT *);
yf(MDIConnect)(MDIVersionT, MDIHandleT *, MDIConfigT *);
yf(MDIDisconnect)(MDIHandleT, MDIUint32);
yf(MDITGQuery)(MDIHandleT, MDIInt32 *HowMany, MDITGDataT *);
yf(MDITGOpen)(MDIHandleT, MDITGIdT, MDIUint32, MDIHandleT *);
    /* 5 */
yf(MDITGClose)(MDIHandleT, MDIUint32);
yf(MDITGExecute)(MDIHandleT);
yf(MDITGStop)(MDIHandleT);
yf(MDIDQuery)(MDIHandleT, MDIInt32 *HowMany, MDIDDataT *);
yf(MDIOpen)(MDIHandleT, MDIDeviceIdT, MDIUint32, MDIHandleT *);
    /* 10 */
yf(MDIClose)(MDIHandleT, MDIUint32);
yf(MDIRead)(MDIHandleT, MDIResourceT SrcR, MDIOffsetT SrcO, void * Buffer, MDIUint32 ObjectSize, MDIUint32 Count);
yf(MDIWrite)(MDIHandleT, MDIResourceT DstR, MDIOffsetT DstO, void * Buffer, MDIUint32 ObjectSize, MDIUint32 Count);
yf(MDIReadList)(MDIHandleT, MDIUint32 ObjectSize, MDICRangeT *SrcList, MDIUint32 ListCount, void * Buffer );
yf(MDIWriteList)(MDIHandleT, MDIUint32 ObjectSize, MDICRangeT *DstList, MDIUint32 ListCount, void * Buffer );
    /* 15 */
yf(MDIMove)   (MDIHandleT, MDIResourceT, MDIOffsetT,  MDIResourceT,
               MDIOffsetT, MDIUint32,    MDIUint32,   MDIUint32);
yf(MDIFill)   (MDIHandleT, MDIResourceT, MDIRangeT,   void*, MDIUint32, MDIUint32);
yf(MDIFind)   (MDIHandleT, MDIResourceT, MDIRangeT,   void*, void*,
               MDIUint32,  MDIUint32,    MDIOffsetT*, MDIUint32);
yf(MDIExecute)(MDIHandleT);
yf(MDIStep)(MDIHandleT, MDIUint32 Steps, MDIUint32 Mode);
    /* 20 */
yf(MDIStop)(MDIHandleT);
yf(MDIReset)(MDIHandleT, MDIUint32 Flag);
yf(MDICacheQuery)(MDIHandleT, MDICacheInfoT *CacheInfo);
yf(MDICacheFlush)(MDIHandleT, MDIUint32 Type, MDIUint32 Flag);
yf(MDIRunState)(MDIHandleT, MDIInt32 WaitTime, MDIRunStateT *runstate);
    /* 25 */
yf(MDISetBp)(MDIHandleT, MDIBpDataT *);
yf(MDISetSWBp)(MDIHandleT , MDIResourceT , MDIOffsetT , MDIBpIdT *);
yf(MDIClearBp)(MDIHandleT, MDIBpIdT);
yf(MDIEnableBp)(MDIHandleT, MDIBpIdT);
yf(MDIDisableBp)(MDIHandleT, MDIBpIdT);
    /* 30 */
yf(MDIBpQuery)(MDIHandleT, MDIInt32 *HowMany, MDIBpDataT *);
yf(MDIDoCommand)(MDIHandleT, char *Buffer);
yf(MDIAbort)( MDIHandleT );
yf(MDITraceEnable)(MDIHandleT);
yf(MDITraceDisable)(MDIHandleT);
    /* 35 */
yf(MDITraceClear)(MDIHandleT);
yf(MDITraceStatus)(MDIHandleT, MDIUint32 *);
yf(MDITraceCount)(MDIHandleT, MDIUint32 *);
yf(MDITraceRead)(MDIHandleT, MDIUint32, MDIUint32, MDIUint32, MDITrcFrameT *);
yf(MDISetTC)      (MDIHandleT, MDITCIdT);
    /* 40 */
yf(MDIGetTC)       (MDIHandleT, MDITCIdT*);
yf(MDITCQuery)     (MDIHandleT, MDIInt32*,  MDITCDataT*);
yf(MDISetRunMode)(MDIHandleT, MDITCIdT,   MDIUint32,   MDIUint32);
yf(MDITeamCreate)  (MDIHandleT, MDITeamIdT*);
yf(MDIQueryTeams)  (MDIHandleT, MDIInt32*, MDITeamIdT*);
    /* 45 */
yf(MDIHwBpQuery)(MDIHandleT, MDIInt32*, MDIBpInfoT*);
yf(MDICacheOp)   (MDIHandleT, MDIResourceT, MDIInt32, MDIResourceT, MDIOffsetT, MDIUint32 );
yf(MDICacheSync) (MDIHandleT, MDIResourceT, MDIOffsetT, MDIUint32 );
yf(MDICacheInfo) (MDIHandleT, MDIResourceT, MDICacheInfoT*);
yf(MDITeamClear)   (MDIHandleT, MDITeamIdT);
    /* 50 */
yf(MDITeamDestroy) (MDIHandleT, MDITeamIdT);
yf(MDITMAttach) (MDIHandleT, MDITeamIdT, MDITMDataT*);
yf(MDITMDetach) (MDIHandleT, MDITeamIdT, MDITMDataT*);
yf(MDIQueryTM)  (MDIHandleT, MDITeamIdT, MDIInt32*, MDITMDataT*);
yf(MDITeamExecute) (MDIHandleT, MDITeamIdT);
    /* 55 */
yf(MDISetBpPrimingCondition) (MDIHandleT, MDIBpIdT, MDIPrimingConditionT);
yf(MDIGetBpPrimingCondition) (MDIHandleT, MDIBpIdT, MDIPrimingConditionT*);
yf(MDICbtConfigQuery) (MDIHandleT, MDICbtConfigTypeT, MDICbtBPTypeT,
   MDICbtIndexT, MDIInt32*, MDICbtConfigT*);
yf(MDIGetStopWatchValue) (MDIHandleT, MDIStopWatchValueT*);
yf(MDIClearStopWatch) (MDIHandleT);
yf(MDISetStopWatchMode) (MDIHandleT, MDIStopWatchModeT, MDIPairIndexT,
   MDIStartIndexT, MDIStopIndexT);
yf(MDIGetStopWatchMode) (MDIHandleT, MDIStopWatchModeT*, MDIPairIndexT*,
   MDIStartIndexT*, MDIStopIndexT*);

yf(MLN_SetupMicrochipFlash)(TyMicrochipStatusRegister *ptyMicrochipStatusRegister, int bClearCodeProtection);
yf(MLN_PartitionRamForMicrochipFlashSetup)(unsigned long ulDataKernelSpace);
yf(MLN_SaveMicrochipBmxRegisters)(TyMicrochipBmxRegContext *tyMicrochipBmxRegContext);
yf(MLN_RestoreMicrochipBmxRegisters)(TyMicrochipBmxRegContext *tyMicrochipBmxRegContext);
yf(MLN_MchpRecoveryByChipErase)(void);
yf(MLN_EraseChipMicrochipFlash)(TyMicrochipStatusRegister *ptyMicrochipStatusRegister);

// Type for OS
typedef enum {
	Linux26
	} TyOS;
yf(AshInitOSMMU)(TyOS os, unsigned long *ulPgDirAddrs);

yf(AshInitOSAppMMU)(unsigned long ulAppCon, unsigned long ulPgDirAddrs);

yf(AshFetchTrace)(unsigned int ulBegPtr,unsigned int ulEndPtr);

yf(AshConfigureTrace)(unsigned int ulTC,unsigned int ulTC2,unsigned int ulTC3);

yf(AshTraceElfPath)(char* pszElfPath);

#undef yf

#ifdef __cplusplus
}
#endif

#endif   // MDI_H_

