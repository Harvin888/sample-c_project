 /******************************************************************************
       Module: MmuLinux26.h
     Engineer: Suresh P. C.
  Description: Header file for Linux 32 bit kernel Debug support.

  Date           Initials    Description
  16-Jul-2009    SPC         Initial
******************************************************************************/

#ifndef _MMULINUX26_H_
#define _MMULINUX26_H_

#include "protocol/common/drvopxd.h"
#include "protocol/common/osdbg.h"
#include "midlayer.h"

#define CONFIG_PAGE_SIZE_4KB

#ifdef CONFIG_PAGE_SIZE_4KB
#define PAGE_SHIFT	12
#endif

#ifdef CONFIG_PAGE_SIZE_8KB
#define PAGE_SHIFT	13
#endif

#ifdef CONFIG_PAGE_SIZE_16KB
#define PAGE_SHIFT	14
#endif

#ifdef CONFIG_PAGE_SIZE_64KB
#define PAGE_SHIFT	16
#endif

#define PAGE_SIZE	(1UL << PAGE_SHIFT)

#define PGD_T_LOG2 2 //logic is (__builtin_ffs(sizeof(pgd_t)) - 1)
#define PTE_T_LOG2 2 //logic is (__builtin_ffs(sizeof(pte_t)) - 1) 
                     //sizeof(pte_t) is 4. __builtin_ffs = (1's Index position from LSB) + 1

//#define __PGD_ORDER	(32 - 3 * PAGE_SHIFT + PGD_T_LOG2 + PTE_T_LOG2)
//#define PGD_ORDER	(__PGD_ORDER >= 0 ? __PGD_ORDER : 0)

//#define PTE_ORDER	0


#define PTRS_PER_PGD	((PAGE_SIZE /*<< PGD_ORDER*/) / 4 /*sizeof(pgd_t)*/ )
#define PTRS_PER_PTE	((PAGE_SIZE /*<< PTE_ORDER*/) / 4 /*sizeof(pte_t)*/ )

#define PGDIR_SHIFT	(2 * PAGE_SHIFT /*+ PTE_ORDER*/ - PTE_T_LOG2)
#define PGDIR_SIZE	(1UL << PGDIR_SHIFT)

#define pgd_index(address)	(((address) >> PGDIR_SHIFT) & (PTRS_PER_PGD-1))  //0x3FF
/* Find an entry in the third-level page table.. */
#define __pte_offset(address)						\
  	(((address) >> PAGE_SHIFT) & (PTRS_PER_PTE - 1))

//Function declarations
int InitLinux26MMU(TyMLHandle *ptyMLHandle, unsigned long ulPgDirAddrs);
int IsLinux26MMUMappingNeeded(unsigned long ulAddrs);
int SetLinux26MMUMapping(TyMLHandle *ptyMLHandle, unsigned long ulAddrs, int iWriteAccess);
void UnSetLinux26MMUMapping(TyMLHandle *ptyMLHandle);
int InitLinux26AppMMU(unsigned long ulAppCon, unsigned long ulPgDirAddrs);

#endif //_MMULINUX26_H_
