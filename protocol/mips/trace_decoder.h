#ifndef __TRACE_DECODER_H_
#define __TRACE_DECODER_H_

typedef int TrcInt32;

#define MAX_INSTR_STR_SIZE      256
#define MAX_LENGTH              100
#define MAX_DEFERRED_LOAD       8
#define MAX_CYCLE_GRP_TOKEN     4
#define END_CYCLE_MASK          0x08
#define IDLE_CYCLE_TOKEN        0x3
#define CACHE_MISS_TOKEN        0xB
#define NIBBLE_IN_WORD          8
#define END_TOKEN               0x0
#define TOKEN_1                 0x0
#define ERR_END_OF_SECTION      -1
#define ERR_DECODER             1

#define MODE_T0                     0x00000700
#define MODE_T1                     0x07000000

#define DS_T0                       0x2000
#define DS_T1                       0x20000000

#define MODE_T0_SHIFT                 8
#define MODE_T1_SHIFT                24

#define DS_T0_SHIFT                  13
#define DS_T1_SHIFT                  29
#define MODE_MAX_VALUE                4

/*The filter values are decided based on PF-XD requirement*/
#define FILTER_TO_MASK              0x01
#define FILTER_T1_MASK              0x02
#define FILTER_IDLE_CYCLE_MASK      0x04
#define FILTER_CACHE_MISS_MASK      0x08
#define FILTER_BRANCH_MASK          0x10
#define FILTER_EVENT_MASK           0x20
#define FILTER_LOAD_MASK            0x40
#define FILTER_STORE_MASK           0x80


/*These strings will be shown in Execution Detail column of Trace View*/
#define JPIPE_ARITHMETIC_INSTR                  "J-pipe arithmetic/no-op instr"
#define JPIPE_BRANCH_INSTR_NOT_TAKEN            "J-pipe branch instr-not taken"
#define JPIPE_BRANCH_INSTR_NOT_TAKEN_LIKELY     "J-pipe branch instr-not taken likely"
#define JPIPE_BRANCH_INSTR_J_OR_BRANCH          "J-pipe branch instr-J or taken branch"
#define JPIPE_BRANCH_INSTR_JAL_BAL_BRANCH       "J-pipe branch instr-AL,B..AL,taken"
#define JPIPE_BRANCH_INSTR_JR_JALR              "J-pipe branch instr-JR,JALR"

#define KPIPE_ARITHMETIC_INSTR                  "K-pipe arithmetic/no-op instr"
#define KPIPE_CONTROL_INSTR                     "K-Pipe control instr"
#define KPIPE_LOAD_INSTR_CACHE_HIT              "K-Pipe load  instr,cache hit"
#define KPIPE_STORE_INSR_CACHE_HIT              "K-Pipe store instr,cache hit"
#define KPIPE_STORE_INSR_CACHE_MISS             "K-Pipe store instr,cache miss"
#define KPIPE_STORE_INSR_UNCACHED               "K-Pipe store instr,uncached"

#define KPIPE_LOAD_INSTR_DATA_DEFERRED          "K-Pipe load instr,data deferred"
#define KPIPE_DATA_DEFERRED_LOAD_CACHE_MISS     "K-Pipe data for deferred load,cache miss"
#define KPIPE_DATA_DEFERRED_LOAD_UNCACHED       "K-Pipe data for deferred load,uncached"

#define IDLE_CYCLES                             "Idle cycle(s)"
#define END_OF_CONTROL_WORD                     "End of control word"
#define END_OF_SECTION                          "End of section"

#define TRACING_RE_ENABLED                      "Tracing re-enabled"

#define RESUME                                  "Resume"
#define EVENT                                   "Exception,Pipeline flush or Interrupt occured"
#define CACHE_MISS                              "I/D cache miss"

/*Check if the nibble is 4-bit token*/
#define FOUR_BIT_NIBBLE_CODE(x) !(x>>2)

/*Join to nibbles to form a byte*/
#define JOIN_NIBBLES(x,y) ((x<<4) | y)

/*
NOTE: A token is End of Cycle if
        1. Bit[3] is 1, but not an IDLE cycle token. i.e.,Bit[7:5] is not 0x3
        2. the token is miss token.i.e., Bit[7:4] is 0xB
*/

/*Check for END TOKEN*/
#define END_OF_CYCLE(x) (((x & END_CYCLE_MASK) && (x>>5 !=IDLE_CYCLE_TOKEN) ) || (CACHE_MISS_TOKEN == x>>4))

/*Check for IDLE CYCLE TOKEN*/
#define IDLE_CYCLE(x) ((x>>5) == IDLE_CYCLE_TOKEN)

/*This structure stores information for each raw for Trace View*/
typedef struct TyTraceLineInfo
    {
    unsigned int ulPC;
    unsigned int ulNextPC;
    unsigned int iThreadID;
    unsigned int ulOpcode;
    unsigned char ucFilterCode;
    char ucInstruction[MAX_INSTR_STR_SIZE];
    char ucInstructionType[MAX_INSTR_STR_SIZE];
    unsigned int ulLoadStoreAddr;
    unsigned int ulLoadStoreData;

    TyTraceLineInfo *ptyNext;

    }TyTraceLineInfo;

/*TyDeferredLoadQueue keeps an array of deferred load tokens*/
typedef struct
    {
    int iEntry;
    TyTraceLineInfo *tyTraceLineInfo;
    }TyDeferredLoadQueue;

/*TyCycleGrpOfTokens keeps an array of byte tokens which belong to the same cycle*/
typedef struct
    {
    unsigned int ulByteToken;
    TyTraceLineInfo *ptyTraceLineInfo;
    }TyCycleGrpOfTokens;

/*Bit definition of 8-bit trace token*/
typedef union 
    {
    unsigned char ucToken;

    struct 
        {
        unsigned char Bit2_0 :3; //LSB
        unsigned char Bit_3  :1;
        unsigned char Bit_4  :1;
        unsigned char Bit7_5 :3; //MSB
        }Bits;
    }TyTraceToken;

/*Nibble definition of Command word*/
typedef union
    {
    unsigned int ulCommandWord;
    struct 
        {
        /*7.6.5...0 order used as the target memory is Big Endian*/
        unsigned int ulNibble7 : 4;//LSB first
        unsigned int ulNibble6 : 4;
        unsigned int ulNibble5 : 4;
        unsigned int ulNibble4 : 4;
        unsigned int ulNibble3 : 4;
        unsigned int ulNibble2 : 4;
        unsigned int ulNibble1 : 4;
        unsigned int ulNibble0 : 4; //MSB last
        }Nibble;
    }TyCommandWord;

#define NIBBLE_7 tyCommandWord.Nibble.ulNibble7
#define NIBBLE_6 tyCommandWord.Nibble.ulNibble6
#define NIBBLE_5 tyCommandWord.Nibble.ulNibble5
#define NIBBLE_4 tyCommandWord.Nibble.ulNibble4
#define NIBBLE_3 tyCommandWord.Nibble.ulNibble3
#define NIBBLE_2 tyCommandWord.Nibble.ulNibble2
#define NIBBLE_1 tyCommandWord.Nibble.ulNibble1
#define NIBBLE_0 tyCommandWord.Nibble.ulNibble0

/*Function declerations*/
void InitializeTracePtr         (TyTraceLineInfo* ptyTempPtr);
void InitializeDecoder          (void);
int  ProcessCommandWord         (void);
int  PrepareCycleGrpOfTokens    (unsigned int ulByteToken        ,TyTraceLineInfo* ptyTraceLineInfoCurrent);
int  processCycleGrpOfTokens    (void);
int  ProcessByteCode            (unsigned int ulByteToken        ,TyTraceLineInfo *tyTraceLineInfo);
int  ProcessNibbleCode          (unsigned int ulNibbleToken      ,TyTraceLineInfo *tyTraceLineInfo,int iNibbleCnt);
int  ProcessJPipeInstr          (TyTraceToken &tyTraceToken      ,TyTraceLineInfo *tyTraceLineInfo);
int  ProcessIdleCycle           (TyTraceToken &tyTraceToken      ,TyTraceLineInfo *tyTraceLineInfo);
int  ProcessKPipeInstr          (TyTraceToken &tyTraceToken      ,TyTraceLineInfo *tyTraceLineInfo);
int  ProcessResumeEventsCacheMiss     (TyTraceToken &tyTraceToken,TyTraceLineInfo *tyTraceLineInfo);
int  ProcessDeferredLoad        (TyTraceToken &tyTraceToken      ,TyTraceLineInfo *tyTraceLineInfo);
int  ProcessDeferredData        (TyTraceToken &tyTraceToken      ,TyTraceLineInfo* tyTraceLineInfo);
bool IsBranchAddressNotRecorded (void);
bool IsDelaySlotInstProcessed   (void);
bool IsLoadInstrToken           (const TyTraceToken &tyTraceToken);
bool IsStoreInstrToken          (const TyTraceToken &tyTraceToken);
bool IsDeferredDataToken        (const TyTraceToken &tyTraceToken);
bool IsLoadInstrCacheHit        (const TyTraceToken &tyTraceToken);
void CheckForDelaySlotInst      (const TyTraceToken &tyTraceToken);
void UpdatePCWithBranchAddr     (void);

#endif

