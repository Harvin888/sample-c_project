 /******************************************************************************
       Module: ml_tlb.h
     Engineer: Suresh P. C.
  Description: Header file for TLB management

  Date           Initials    Description
  16-Jul-2009    SPC         Initial
******************************************************************************/

#ifndef __ML_TLB_H__
#define __ML_TLB_H__

/* EntryLo Register format */
typedef union __TyEntryLo
{
   unsigned int Data;
   struct
   {
      unsigned int G    :1;
      unsigned int V    :1;
      unsigned int D    :1;
      unsigned int C    :3;
      unsigned int PFN  :20;
      unsigned int Zero :4;
      unsigned int RI   :2;
   }Bits;
} TyEntryLo;

/* EntryHi Register format */
typedef union __TyEntryHi
{
   unsigned int Data;
   struct
   {
      unsigned int ASID    :8;
      unsigned int Zero    :3;
      unsigned int VPN2X   :2;
      unsigned int VPN2    :19;
   }Bits;
} TyEntryHi;

/* PageMask Register format */
typedef union __TyPageMask
{
   unsigned int Data;
   struct
   {
      unsigned int LowerZero  :11;
      unsigned int Mask       :18; 
      unsigned int UpperZero  :3;
   }Bits;
} TyPageMask;

/* TLB Entry */
typedef struct __TyTLBEntry
{
   TyEntryLo   EntryLo0;
   TyEntryLo   EntryLo1;
   TyEntryHi   EntryHi;
   TyPageMask  PageMask;
}TyTLBEntry;

/* Function Prototypes */
int SearchTLB(unsigned long ulVPN);

#endif // __ML_TLB_H__
