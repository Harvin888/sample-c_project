/****************************************************************************
       Module: midlayer.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This is main section of midlayer. Some functions are placed in
               other files.
               ml_reg.cpp - MIPS register access
               ml_cache.cpp - MIPS cache operations
               ml_tlb.cpp - MIPS TLB and MMU access
               ml_mem.cpp - MIPS memory access
               ml_brkpt.cpp - MIPS breakpoint handling
               ml_nvram.cpp - MIPS non-volatile RAM access
Date           Initials    Description
08-Feb-2008    VH          Initial
20-Nov-2008    SPC         Device Enable password support added
23-Dec-2008    SPC         Returning errors more properly.
05-Jan-2009    SPC         Moved TL_LoadDW2Applications to ML_LoadDW2Applications.
06-Aug-2009    SPC         Made cache flush routine run from probe
****************************************************************************/

#ifndef __LINUX
// Windows specific includes
   #include <stdio.h>
   #include "windows.h"
   #define snprintf _snprintf 
#else
// Linux includes
   #include <stdio.h>
   #include <string.h>
   #include <malloc.h>
   #include <unistd.h>

#endif
#define MDI_LIB               // We are an MDI Library not a debugger
#include <assert.h>
#include "firmware/export/api_def.h"
#include "firmware/export/api_cmd.h"
#include "debug.h"
#include "ml_error.h"
#include "mdi.h"
#include "mips.h"
#include "commdefs.h"
#include "gerror.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "midlayer.h"
#include "ml_disa.h"
#include "loadsrec.h"

#include "../../utility/debug/debug.h"

#define MAX_PASSWD_LEN 100
//todo: remove...
#define MAX_DW2_LOOPS_SLOW_JTAG     4000 // For QuickTurn support
#define MAX_DW2_LOOPS_NORMAL_JTAG   100  // No DW2 Application should wait this long
// 
#define MAX_WAIT_FOR_SINGLE_STEP 5  // If we haven't returned after 5  checks something is seriously wrong

// test scanchain
#define MIPS_TSTSC_BITS 	512               // number of bits for testing scanchain (MUST be div by 32)
#define MIPS_TSTSC_WORDS 	(MIPS_TSTSC_BITS / 32) 

#define SET_PSS_TO_0_OR_MASK         0x00000000 //clear bit 6-9 for pss=0
#define SET_PSS_TO_1_OR_MASK         0x00000040 //set bit 6 for pss=1
#define SET_PSS_TO_2_OR_MASK         0x00000080 //set bit 7 for pss=2
#define SET_PSS_TO_3_OR_MASK         0x000000C0 //set bit 6,7 for pss=3
//#define SET_PSS_TO_4_OR_MASK         0x00000100 //set bit 8 for pss=4

#define SET_PSS_TO_0_AND_MASK        0xFFFFFFFF //set bit 6-9 for pss=0
#define SET_PSS_TO_1_AND_MASK        0xFFFFFC7F //keep bit 6 set and clear bit 7,8,9 for pss=1
#define SET_PSS_TO_2_AND_MASK        0xFFFFFCBF //keep bit 7 set and clear bit 6,8,9 for pss=2
#define SET_PSS_TO_3_AND_MASK        0xFFFFFFCF //keep bits 6, 7 set and clear bits 8,9 for pss=2
//#define SET_PSS_TO_4_AND_MASK        0xFFFFFD3F //keep bit 8 set and clear bit 6,7,9 for pss=4

//hard code the number of memory reads before reset to 8.
// 1 read before reset worked for PNX-8535 but initial 
// expts on 8542-IKOS showed it was a max of up to 6. Make
// this xml configurable later if this is shown to work.
#define NUMBER_OF_MEMORY_READS_BEFORE_RESET     0x8 
// local variables
char pszGlobalDllPath[DLLPATH_LENGTH];    // contains DLL path

// Used by DW2 Header Apps
unsigned long pulCommonDataFromProc[MAX_MIPS_DW2_DATA_SIZE];
unsigned long pulCommonDataToProc[MAX_MIPS_DW2_DATA_SIZE];
extern TyApplicationSpecs tyAppSpecArray[LAST_APP];
//DW2_Application * tyDW2_Array;
//TyTargetConfig TargetConfig;

// function prototypes
typedef void (*TyOpenOpellaXDCallback)(unsigned char); //todo: callback
//static 
TyError ML_WriteOpellaXDTargetConfig(TyMLHandle *ptyHandle,
                                     TyUserSettings *ptyUserSettings);
static void ShiftArrayOfWordsRight(unsigned long *pulArray, 
                                   unsigned long ulWords);
static TyError ML_SaveContext(TyMLHandle *ptyHandle,
                              int bUseTargetMemoryIfNeeded);
static TyError ML_RestoreShadowRegisterSet(TyMLHandle    *ptyHandle);
void ML_EnableCoreSelect(TyMLHandle *ptyHandle);
void ML_DetermineMulticoreInfo(TyMLHandle *ptyHandle,
                               TyUserSettings *tyUserSettings);
unsigned long * LtoP(unsigned long value);


//todo: move to global utility file to use from who need it
/****************************************************************************
     Function: StringToLwr
     Engineer: Nikolay Chokoev
        Input: char *pszString - string to convert
       Output: none
  Description: Convert string to lower case.
Date           Initials    Description
07-Dec-2007    NCH         Initial
*****************************************************************************/
void StringToLwr(char *pszString)
{
   while (*pszString)
      {
      if ((*pszString >= 'A') && (*pszString <= 'Z'))
         *pszString += 'a'-'A';
      pszString++;
      }
}//todo up

/****************************************************************************
     Function: CompareArrayOfWords
     Engineer: Nikolay Chokoev
        Input: unsigned long *pulArray1 - pointer to 1st array of words
               unsigned long *pulArray2 - pointer to 2nd array of words
               unsigned long ulNumberOfBits - number of bits to compare (least significant)
       Output: int - 0 if arrays are same, otherwise value different than 0
  Description: compare two array of words taking 
               only certain number of bits for comparison
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
static int CompareArrayOfWords(unsigned long *pulArray1, 
                               unsigned long *pulArray2, 
                               unsigned long ulNumberOfBits)
{
   if ((pulArray1 == NULL) || (pulArray2 == NULL) || (ulNumberOfBits == 0))
      return 0;

   // compare whole words
   while (ulNumberOfBits>=32)
      {
      if (*pulArray1 != *pulArray2)
         return 1;
      pulArray1++;
      pulArray2++;
      ulNumberOfBits -= 32;
      }
   // compare parts of last word with mask
   if (ulNumberOfBits)
      {
      unsigned long ulMask = 0xFFFFFFFF >> (32 - ulNumberOfBits);
      if ((*pulArray1 & ulMask) != (*pulArray2 & ulMask))
         return 1;
      }
   // otherwise both arrays are same
   return 0;
}

/****************************************************************************
     Function: MLGetLibPath
     Engineer: Nikolay Chokoev
        Input: pszLibPath : pointer to where the string path will be copied
               iMaxSize   : number of bytes to be copied
       Output: Error code
  Description: Get the path string. Use in toplayer to get the path.
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
int MLGetLibPath(char *pszLibPath, int iMaxSize)
{
   if (iMaxSize > DLLPATH_LENGTH)
      iMaxSize = DLLPATH_LENGTH;
   strncpy(pszLibPath,pszGlobalDllPath, (unsigned int)iMaxSize);
   return 0;
}

/****************************************************************************
     Function: ML_GetBasicProcessorSettings
     Engineer: Nikolay Chokoev
        Input: tyUserSettings : User Settings
       Output: Error code
  Description: Get the configuration from the processor.
Date           Initials    Description
07-Jul-2008    NCH          Initial
06-Aug-2009    SPC          Removed Cache flush routing mapping
****************************************************************************/
TyError ML_GetBasicProcessorSettings(TyMLHandle *ptyHandle,
                                     const TyUserSettings *tyUserSettings)
{
   TyError tyErrRet;

   IDCODE_REG     tyIDCODEValue;
   IMPCODE_REG    tyIMPCODEValue;
   TyTCBCONTROLA tyTCBCONTROLA;
   TyTCBCONTROLB tyTCBCONTROLB;
   TyTCBCONFIG   tyTCBCONFIG;
   TyApplicationType   tySaveContextApp;                // Application to be used to Save Context
   TyApplicationType   tyRestoreContextApp;            // Application to be used to Restore Context

   DLOG2((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> Called"));

   if (ptyHandle->bSlowJTAGEnabled)
      ptyHandle->uiMaxDW2Loops = MAX_DW2_LOOPS_SLOW_JTAG;
   else
      ptyHandle->uiMaxDW2Loops = MAX_DW2_LOOPS_NORMAL_JTAG;

   (void)MLR_ReadRegister(ptyHandle,
                          REG_MIPS_IDCODE,
                          ASHLING_INT_REG,
                          &tyIDCODEValue.ulData);
   (void)MLR_ReadRegister(ptyHandle,
                          REG_MIPS_IMPCODE,
                          ASHLING_INT_REG,
                          &tyIMPCODEValue.ulData);

   DLOG((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> IDCODE  = %08X",tyIDCODEValue.ulData));
   DLOG((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> IMPCODE = %08X",tyIMPCODEValue.ulData));

   switch (tyIMPCODEValue.Bits.EJTAGVer)
      {
      case 0:
         // EJTAG Versions 1 and 2
         ptyHandle->ucEjtagVersion = EJTAG_VERSION_20;
         DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> EJTAG Version : 2.0.X or 1.5.X"));
         break;
      case 1:
         // EJTAG Version 2.5
         DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> EJTAG Version : 2.5.X"));
         ptyHandle->ucEjtagVersion = EJTAG_VERSION_25;
         break;
      case 2:
         // EJTAG Version 2.6
         DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> EJTAG Version : 2.6.X"));
         ptyHandle->ucEjtagVersion = EJTAG_VERSION_26;
         break;
      case 3:
         // EJTAG Version 3.1, we will treat as version 2.6 as a temp fix.
         DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> EJTAG Version : 3.1.X"));
         ptyHandle->ucEjtagVersion = EJTAG_VERSION_26;
         break;
      case 4:
         // EJTAG Version 4.1, we will treat as version 2.6 as a temp fix.
         DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> EJTAG Version : 3.1.X"));
         ptyHandle->ucEjtagVersion = EJTAG_VERSION_26;
         break;
      case 5:
         // EJTAG Version 5, we will treat as version 2.6 as a temp fix.
         DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> EJTAG Version : 3.1.X"));
         ptyHandle->ucEjtagVersion = EJTAG_VERSION_26;
         break;
      default:
         // Invalid Case
         ASSERT_NOW();
         DLOG3((LOGFILE,"\n\t\tInvalid EJTAG Version %d",tyIMPCODEValue.Bits.EJTAGVer));
         return ERR_GETBASESET_INVALID_EJTAG_VER;
      }

//   if (tyIMPCODEValue.Bits.MipsType == 0)
//      {
//      // 32 Bit MIPS processor
//      TargetConfig.b32BitProcessor = TRUE;
//      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> 32 Bit MIPS processor Detected"));
//      }
//   else
//      {
//      // 64 Bit MIPS Processor
//      TargetConfig.b32BitProcessor = FALSE;
//      DLOG((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> 64 Bit MIPS processor Detected"));
//      }

   // If a different core is specified for DMA access then assume that DMA is supported
   if (tyUserSettings->ucMultiCoreCoreNum != tyUserSettings->ucMultiCoreDMACoreNum)
      tyIMPCODEValue.Bits.NoDMA = 0x0;

   if (tyIMPCODEValue.Bits.NoDMA)
      {
      // DMA Mode Not Supported
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> DMA Not Supported"));
      ptyHandle->bDmaSupported = FALSE;
      }
   else
      {
      // DMA Mode Supported
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> DMA Supported"));
      ptyHandle->bDmaSupported = TRUE;
      ptyHandle->ucFixedMapSystem     = 1;
      }

   ptyHandle->bBigEndian                     = FALSE;
   ptyHandle->bEndianSwitchedForCacheFlush   = FALSE;
   ptyHandle->bDmaEnabledByUser              = (unsigned char)tyUserSettings->bDMATurnedOn; 
   ptyHandle->bBPOnTheFlyEnabled             = (unsigned char)tyUserSettings->bDMATurnedOn;  
   ptyHandle->bMemOnTheFlyEnabled            = (unsigned char)tyUserSettings->bDMATurnedOn;  
   ptyHandle->bOddAddressForHWMIPS16BPs      = (tyUserSettings->bOddAddressFor16BitModeBps != 0x0);
   ptyHandle->ulBackupOfStatusRegister       = 0x0;
   ptyHandle->ulDataScratchPadRamStart       = tyUserSettings->ulDataScratchPadRamStart;
   ptyHandle->ulDataScratchPadRamEnd         = tyUserSettings->ulDataScratchPadRamEnd;  
   ptyHandle->ulInstScratchPadRamStart       = tyUserSettings->ulInstScratchPadRamStart;
   ptyHandle->ulInstScratchPadRamEnd         = tyUserSettings->ulInstScratchPadRamEnd;  
   ptyHandle->bDataScratchPadRamPresent      = (ptyHandle->ulDataScratchPadRamStart != ptyHandle->ulDataScratchPadRamEnd); 
   ptyHandle->bInstScratchPadRamPresent      = (ptyHandle->ulInstScratchPadRamStart != ptyHandle->ulInstScratchPadRamEnd); 
   ptyHandle->bEndianModeModifiable          = (unsigned char)tyUserSettings->bEndianModeModifiable;
   ptyHandle->bDataCacheAlreadyInvalidated   = FALSE;
   ptyHandle->bUseTargetMemoryWhileWriting   = FALSE;
   ptyHandle->bUseTargetMemoryWhileReading   = FALSE;

   // FINALLY THE STUFF THAT IS SPECIFIC TO EJTAG VERSIONS
   if (ptyHandle->ucEjtagVersion == EJTAG_VERSION_20)
      {

      if (tyIMPCODEValue.Bits.SDBBPCode)
         {
         // This indicates that the "New" i.e. EJTAG 1.5 and subsequent 
         // encodings of SDBBP is used. We do nothing in this case as we have 
         // assumed that this is the minimum EJTAG version we support.
         }
      else
         {
         // EJTAG Version 1.3 Encoding of SDBBP used.
         // We do not support this and it should never happen.
         ASSERT_NOW();
         DLOG((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> Unsupported SDBBP encoding used"));
         }

      if (tyIMPCODEValue.Bits.PhysAW == 1)
         {
         //todo: Measure it!
         ptyHandle->ulMipsCoreEjtagAddressLength=32;

         }
      else
         {
         // Otherwise it is fixed at 32 bits in length.
         ptyHandle->ulMipsCoreEjtagAddressLength = 32;
         }

      ptyHandle->bSSInMIPS16Supported = (tyUserSettings->bSSIn16BitModeSupported != 0x0);
      // The following are fixed settings for EJTAG 1.53
      ptyHandle->bLevelWhichIndicatesProcessorClockHalted = 0;  // EJTAG 1.5.3
      ptyHandle->bDszInsteadOfPsz               = TRUE;        // EJTAG 1.5.3
      ptyHandle->ulExpectedControlDatWrite      = 0x002C8109;
      ptyHandle->ulExpectedControlInsDatFetch   = 0x00248109;
      ptyHandle->ulCP0RegNumberforDebug         = REG_MIPS_C0_R16 - REG_START_OF_MIPS_CP ;  // EJTAG 1.5.3
      ptyHandle->ulCP0RegNumberforDEPC          = REG_MIPS_C0_R17 - REG_START_OF_MIPS_CP;   // EJTAG 1.5.3
      ptyHandle->ulCP0RegNumberforEPC           = REG_MIPS_C0_R14 - REG_START_OF_MIPS_CP;
      ptyHandle->ulCP0RegNumberforConfig        = REG_MIPS_C0_R22 - REG_START_OF_MIPS_CP;   // EJTAG 1.5.3
      ptyHandle->ulCP0BitOffsetInConfigforEndianMode = 16;  // END bit
      ptyHandle->bEJTAGBootSupported            = FALSE;
      // We now clear the MP bit in Save Context (see RCX_APP.AS)
      ptyHandle->bNeedToClearMPBit              = FALSE;
      ptyHandle->bProbeTrapFlagExists           = FALSE;       // EJTAG 1.5.3
      ptyHandle->bBasicMappingSystemInUse       = TRUE;
      ptyHandle->bAutoHaltCountersSupported     = FALSE;       // EJTAG 1.5.3
      ptyHandle->ulConfigORMask                 = 0x0C000018;  // Set Bits 26,27(Disable Cache) and Bits 3,4 (Stop Timers)
      ptyHandle->ulConfigANDMask                = 0xFFFFFFFF;  // Clear No Bits  NOTE: THESE DEFAULT VALUES ARE OVERRIDDEN BY
      ptyHandle->bFlushOnlyDataCache            = FALSE;       // Old Style
      ptyHandle->bBackupTLBAndCacheRegisters    = FALSE;       // No Cache window in these old processors
      ptyHandle->ulDefaultValueForDCR           = 0x0000001B;  // TM = 1 (Not Real time), MRst = 1 (OFF), MP=0 (Off),MNmi=1 (off), MInt = 1 (off) rest 0.
      ptyHandle->bEnableDSUOnBreakFromEmulation = TRUE;        // Enable Debug Support Unit in SaveContext
      ptyHandle->tyTraceSettings.tyTraceType    = EJTAG2_TRACE; // Trace type if supported
      ptyHandle->ucDefaultValueOfClkEnBit       = 1 ;          // By Default Enable Trace Clock on EJTAG 2.0
      ptyHandle->ulDefaultControlProbeWillService = 0x00048001;  //CLKEN = 1;PrAcc=1;ProbTrap=1;ProbEn=1 // WARNING BEWARE EJTAG 2.0.0 and TRAP 
      ptyHandle->ulDefaultControlCompleteAccess   = 0x00008001;  //CLKEN =1 ;PrAcc=0;ProbTrap=1;ProbEn=1 
      tySaveContextApp    = SCX00SCR;             // PR1910 Save Context Srecord
      tyRestoreContextApp = RCX00RRR;             // PR1910 Restore Context Srecord
      if (tyUserSettings->EJTAGStyle == (unsigned long)EJTAG20StyleExt)
         ptyHandle->tyTraceSettings.bExtendedEJTAG20Trace = TRUE;
      else
         ptyHandle->tyTraceSettings.bExtendedEJTAG20Trace = FALSE;      

      if (ptyHandle->bSlowJTAGEnabled)
         ptyHandle->bUseOfTargetMemoryReadWriteAllowed = FALSE;  // Don't use with slow JTAG rates
      else
         ptyHandle->bUseOfTargetMemoryReadWriteAllowed = TRUE;
      }
   else
      {
      ptyHandle->bSSInMIPS16Supported           = FALSE; // MIPS16 isn't even supported in EJTAG 2.5
      ptyHandle->ulMipsCoreEjtagAddressLength   =32;     //patch in ML_DetermineLengthOfAddressRegister();
      // The following are fixed settings for EJTAG 2.5.0
      ptyHandle->bLevelWhichIndicatesProcessorClockHalted = 1; // EJTAG 2.5.0
      ptyHandle->bDszInsteadOfPsz               = FALSE;       // EJTAG 2.5.0
      ptyHandle->ulExpectedControlDatWrite      = 0x400CC008;
      ptyHandle->ulExpectedControlInsDatFetch   = 0x4004C008;
      ptyHandle->ulCP0RegNumberforDebug         = REG_MIPS_C0_R23 - REG_START_OF_MIPS_CP; // EJTAG 2.5.0
      ptyHandle->ulCP0RegNumberforDEPC          = REG_MIPS_C0_R24 - REG_START_OF_MIPS_CP; // EJTAG 2.5.0
      ptyHandle->ulCP0RegNumberforEPC           = REG_MIPS_C0_R14 - REG_START_OF_MIPS_CP;
      ptyHandle->ulCP0RegNumberforConfig        = REG_MIPS_C0_R16 - REG_START_OF_MIPS_CP; // EJTAG 2.5.0
      ptyHandle->ulCP0BitOffsetInConfigforEndianMode = 15;  // BE bit
      ptyHandle->bEJTAGBootSupported            = tyUserSettings->bEJTAGBootSupported;
      ptyHandle->bNeedToClearMPBit              = FALSE;
      ptyHandle->bProbeTrapFlagExists           = TRUE;  // EJTAG 2.5.0
      ptyHandle->bBasicMappingSystemInUse       = FALSE;
      ptyHandle->bAutoHaltCountersSupported     = TRUE;  // EJTAG 2.5.0 
      ptyHandle->ulConfigORMask                 = 0x00000002;  // Set Bit 1 (Disable Cache)
      ptyHandle->ulConfigANDMask                = 0xFFFFFFFA;  // Clear Bits 0,2 ( Disable Cache)NOTE: THESE DEFAULT VALUES ARE OVERRIDDEN BY
      ptyHandle->bFlushOnlyDataCache            = FALSE;       // Assume Old Style for now, SmartMIPS handled later

      if (tyUserSettings->bMicroChipM4KFamily)
         {
         tySaveContextApp = SCX02RCR;          // MIPS32 Save Context Srecord for MCHP device
         tyRestoreContextApp = RCX02RRR;       // MIPS32 Restore Context Srecord for MCHP device
         ptyHandle->bBackupTLBAndCacheRegisters = FALSE; 
         }
      else
         {
         if (tyUserSettings->bZephyrTraceSupport) 
         {
            tySaveContextApp = SCXBCMAP;     // MIPS32 Save Context Srecord
            tyRestoreContextApp = RCXBCMAP;  // MIPS32 Restore Context Srecord
         }
         else
         {
            tySaveContextApp = SCX01RCR;     // MIPS32 Save Context Srecord
            tyRestoreContextApp = RCX01RRR;  // MIPS32 Restore Context Srecord
         }
         // Cache window exists backup additional registers.
         ptyHandle->bBackupTLBAndCacheRegisters = TRUE;         
         }    

      ptyHandle->ulDefaultValueForDCR = 0x0000001B;          // ProbEn = 1, SRstE = 1, NMIE = 1, IntE = 1 , rest 0
      ptyHandle->bEnableDSUOnBreakFromEmulation = FALSE;     // No  Debug Support Unit bit in Config.
      ptyHandle->tyTraceSettings.tyTraceType = NO_TRACE;     // This version of EJTAG had no trace support
      ptyHandle->ucDefaultValueOfClkEnBit = 0 ;              // There is no CLKEn bit in EJTAG 2.5
      ptyHandle->ulDefaultControlProbeWillService = 0x0004C000;  //PrAcc=1;ProbTrap=1;ProbEn=1 // WARNING BEWARE EJTAG 2.0.0 and TRAP 
      ptyHandle->ulDefaultControlCompleteAccess   = 0x0000C000;  //PrAcc=0;ProbTrap=1;ProbEn=1 
      if (ptyHandle->bSlowJTAGEnabled)
         ptyHandle->bUseOfTargetMemoryReadWriteAllowed = FALSE;  // Don't use with slow JTAG rates
      else
         ptyHandle->bUseOfTargetMemoryReadWriteAllowed = TRUE;
      }    

   if (ptyHandle->ucEjtagVersion == EJTAG_VERSION_26)
      {
      if (tyUserSettings->bOnlyFlushDataCache)
         {
         // On SmartMIPS processors we support the concept of a Cache window
         // On such processors we must enable this option
         ptyHandle->bFlushOnlyDataCache = TRUE;    // Needed so we keep Cache window in sync
         }
      else
         ptyHandle->bFlushOnlyDataCache = FALSE;   // Not needed

      ptyHandle->bSSInMIPS16Supported = tyIMPCODEValue.Bits.Mips16;   // Single Step in MIPS16 Mode is supported on this processor

      // Trace Settings
      (void)MLR_ReadRegister(ptyHandle,
                             REG_MIPS_TCBCONTROLA,
                             ASHLING_INT_REG,
                             &tyTCBCONTROLA.ulData);
      (void)MLR_ReadRegister(ptyHandle,
                             REG_MIPS_TCBCONTROLB,
                             ASHLING_INT_REG,
                             &tyTCBCONTROLB.ulData);
      (void)MLR_ReadRegister(ptyHandle,
                             REG_MIPS_TCBCONFIG,
                             ASHLING_INT_REG,
                             &tyTCBCONFIG.ulData);

      ptyHandle->tyTraceSettings.bOnChipTraceSupported   = (unsigned char)tyTCBCONFIG.Bits.OnT;
      ptyHandle->tyTraceSettings.bOffChipTraceSupported  = (unsigned char)tyTCBCONFIG.Bits.OfT;
      ptyHandle->tyTraceSettings.tySupportedTraceModes   = (TySupportedTraceModes)tyTCBCONTROLA.Bits.VModes;
      ptyHandle->tyTraceSettings.ulMaxClockRatio         = tyTCBCONFIG.Bits.CRMax;
      ptyHandle->tyTraceSettings.ulMinClockRatio         = tyTCBCONFIG.Bits.CRMin;
      ptyHandle->tyTraceSettings.ulOffChipClockRatio     = tyTCBCONTROLB.Bits.CCR;
      ptyHandle->tyTraceSettings.ulOnChipTraceMemorySize = ((unsigned long)1 << ((unsigned long)tyTCBCONFIG.Bits.SZ + (unsigned long)8));
      ptyHandle->tyTraceSettings.ulTCBRevision           = tyTCBCONFIG.Bits.REV;

      switch (tyTCBCONFIG.Bits.PW)
         {
         case 0:
            ptyHandle->tyTraceSettings.ulTraceBusWidth = 4;
            break;
         case 1:
            ptyHandle->tyTraceSettings.ulTraceBusWidth = 8;
            break;
         case 2:
            ptyHandle->tyTraceSettings.ulTraceBusWidth = 16;
            break;
         default:
            ASSERT_NOW();
            break;
         }

      ptyHandle->tyTraceSettings.tyTraceType = TCB_TRACE;     // This version of EJTAG has a TCB

      // Now lets log some of that
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> TCB Revision = %d", ptyHandle->tyTraceSettings.ulTCBRevision));
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> Bus Width = %d", ptyHandle->tyTraceSettings.ulTraceBusWidth));
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> OnChipTraceSupported = %X", ptyHandle->tyTraceSettings.bOnChipTraceSupported));
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> OffChipTraceSupported = %X", ptyHandle->tyTraceSettings.bOffChipTraceSupported));
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> Supported Modes = %X", ptyHandle->tyTraceSettings.tySupportedTraceModes));
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> Max Clock Ratio  %d ", ptyHandle->tyTraceSettings.ulMaxClockRatio)); 
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> Min Clock Ratio  %d ", ptyHandle->tyTraceSettings.ulMinClockRatio)); 
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> Current Clock Ratio %d", ptyHandle->tyTraceSettings.ulOffChipClockRatio)); 
      DLOG((LOGFILE,"\nML_GetBasicProcessorSettings -> On Chip Trace Mem Size = %d", ptyHandle->tyTraceSettings.ulOnChipTraceMemorySize));

      }

#ifdef USE_DIAGNOSTIC_REGISTERS
   {
      int iDiag;
      for (iDiag=0; iDiag<MAX_DIAGNOSTIC_REGISTERS; iDiag++)
         {
         ptyHandle->pulDiagnosticRegisters[iDiag] = 0xFFFFFFFF;
         }
//   ptyHandle->pulDiagnosticRegisters[0x00] = TargetConfig.tyDeviceType;
      //ptyHandle->pulDiagnosticRegisters[0x01] = ptyHandle->bCacheFlushEnabled;
      //ptyHandle->pulDiagnosticRegisters[0x02] = ptyHandle->bCacheFlushVerify;
      ptyHandle->pulDiagnosticRegisters[0x03] = 0;
   }
#endif


   if (ptyHandle->bBigEndian)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> Big Endian Mode Selected"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> Little Endian Mode Selected"));
      }

   if (ptyHandle->bDmaEnabledByUser)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> DMA Turned On by User"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> DMA Turned Off by User"));
      }

   if (ptyHandle->bDisableIntSS)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> DisableIntSS On"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> DisableIntSS Off"));
      DPRINTF(("\nInterupts Available during SingleStep"));
      }

   if (ptyHandle->bSingleStepUsingSWBP)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> SingleStepUsingSWBP On"));
      DPRINTF(("\nSingleStep Using Software BP"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> SingleStepUsingSWBP Off"));
      DPRINTF(("\nSingleStep Using Hardware or Software BP"));
      }

   if (ptyHandle->bMemOnTheFlyEnabled)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> RD/WR Mem On fly enabled"));
      DPRINTF(("\nMem Read Write On the Fly Enabled"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> RD/WR Mem On fly disabled"));
      DPRINTF(("\nMem Read Write On the Fly Disabled"));
      }

   if (ptyHandle->bBPOnTheFlyEnabled)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> Set BP On fly enabled"));
      DPRINTF(("\nBreakpoints On the Fly Enabled"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> Set BP On fly disabled"));
      DPRINTF(("\nBreakpoints On the Fly Disabled"));
      }

   if (ptyHandle->bUserHaltCountersIDM)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> UserHaltCountersIDM On"));
      DPRINTF(("\nHalt Counters In Debug Mode"));
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetBasicProcessorSettings -> UserHaltCountersIDM Off"));
      DPRINTF(("\nDo Not Halt Counters In Debug Mode"));
      }

   //Load Save context application. Moved from TL_DeviceConnect
   tyErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                   tySaveContextApp,
                                   &ptyHandle->tyMipsDW2App.pulSaveContextApp,
                                   &ptyHandle->tyMipsDW2App.ulSaveContextInstructions,
                                   NULL,NULL,(TySymbolIdentifier)0);
   if (tyErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications Failed"));
      return tyErrRet;
      }

   //Load Restore context application. Moved from TL_DeviceConnect
   tyErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                   tyRestoreContextApp,
                                   &ptyHandle->tyMipsDW2App.pulRestoreContextApp,
                                   &ptyHandle->tyMipsDW2App.ulRestoreContextInstructions,
                                   NULL,NULL,(TySymbolIdentifier)0);
   if (tyErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications Failed"));
      return tyErrRet;
      }

   tyErrRet = ML_WriteOpellaXDTargetConfig(ptyHandle, (TyUserSettings*)tyUserSettings);
   if (tyErrRet != ERR_NO_ERROR)
      return tyErrRet;


   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_BasicSettingsOnError
     Engineer: Nikolay Chokoev
        Input: tyUserSettings : User Settings
       Output: Error code
  Description: As ML_BasicSettings() but at error condition
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_BasicSettingsOnError(TyMLHandle *ptyHandle,
                                const TyUserSettings *tyUserSettings)
{


   DLOG2((LOGFILE,"\n\t\tML_BasicSettingsOnError -> Called"));

   // Pretend the processor isn't executing so that people can try to do stuff from the cmd line
   ptyHandle->bProcessorExecuting = FALSE;
   // Most errors are with EJTAG2.0 Assume this
   ptyHandle->ucEjtagVersion = EJTAG_VERSION_20;
   // DMA mode
   ptyHandle->bDmaSupported         = TRUE;
   ptyHandle->ucFixedMapSystem      = 1;
   // TO_DO: Not sure if we can read CP0 config reg yet, for now set 
   // BigEndian=FALSE, this will be updated during the first save context
   ptyHandle->bBigEndian            = FALSE;
   ptyHandle->bDmaEnabledByUser     = FALSE; 
   ptyHandle->bOddAddressForHWMIPS16BPs   = FALSE;
   ptyHandle->ulBackupOfStatusRegister    = 0x0;      // Give this a reset value
   ptyHandle->bDataScratchPadRamPresent=  FALSE;
   ptyHandle->bInstScratchPadRamPresent=  FALSE;
   // Also Reset this flag to ensure proper invalidation occurs.
   ptyHandle->bDataCacheAlreadyInvalidated = FALSE;  // Flag for use by MLM_WriteMemory
   ptyHandle->ulMipsCoreEjtagAddressLength = 32;
   ptyHandle->bLevelWhichIndicatesProcessorClockHalted = 0;  // EJTAG 1.5.3
   ptyHandle->bDszInsteadOfPsz    = TRUE;                    // EJTAG 1.5.3
   ptyHandle->ucNoOfNOPSInERETBDS     = 1;            // MOJO or ADOC
   ptyHandle->ulExpectedControlDatWrite    = 0x002C8109;
   ptyHandle->ulExpectedControlInsDatFetch = 0x00248109;
   ptyHandle->ulCP0RegNumberforDebug = REG_MIPS_C0_R16 - REG_START_OF_MIPS_CP ; // EJTAG 1.5.3
   ptyHandle->ulCP0RegNumberforDEPC  = REG_MIPS_C0_R17 - REG_START_OF_MIPS_CP; // EJTAG 1.5.3
   ptyHandle->ulCP0RegNumberforEPC   = REG_MIPS_C0_R14 - REG_START_OF_MIPS_CP;
   ptyHandle->ulCP0RegNumberforConfig= REG_MIPS_C0_R22 - REG_START_OF_MIPS_CP; // EJTAG 1.5.3
   ptyHandle->ulCP0BitOffsetInConfigforEndianMode = 16;      // END bit

   ptyHandle->bEJTAGBootSupported  = FALSE;
   // We now clear the MP bit in Save Context (see RCX_APP.AS)
   ptyHandle->bNeedToClearMPBit    = FALSE;
   ptyHandle->bProbeTrapFlagExists = FALSE;                        // EJTAG 1.5.3
   ptyHandle->bBasicMappingSystemInUse = TRUE;
   ptyHandle->bAutoHaltCountersSupported = FALSE;         // EJTAG 1.5.3
   ptyHandle->ulConfigORMask  = 0x0C000018;               // Set Bits 26,27(Disable Cache) and Bits 3,4 (Stop Timers)
   ptyHandle->ulConfigANDMask = 0xFFFFFFFF;               // Clear No Bits  NOTE: THESE DEFAULT VALUES ARE OVERRIDDEN BY
   ptyHandle->bFlushOnlyDataCache       = FALSE;          // Old Style
   ptyHandle->bBackupTLBAndCacheRegisters = FALSE;        // No Cache window in these old processors
   ptyHandle->ulDefaultValueForDCR = 0x0000001B;          // TM = 1 (Not Real time), MRst = 1 (OFF), MP=0 (Off),MNmi=1 (off), MInt = 1 (off) rest 0.
   ptyHandle->bEnableDSUOnBreakFromEmulation = TRUE;      // Enable Debug Support Unit in SaveContext
   ptyHandle->tyTraceSettings.tyTraceType = EJTAG2_TRACE; // Trace type if supported
   ptyHandle->ucDefaultValueOfClkEnBit = 1;               // By Default Enable Trace Clock on EJTAG 2.0
   ptyHandle->ulDefaultControlProbeWillService = 0x0004C001;  //CLKEN = 1;PrAcc=1;ProbTrap=1;ProbEn=1 // WARNING BEWARE EJTAG 2.0.0 and TRAP 
   ptyHandle->ulDefaultControlCompleteAccess   = 0x0000C001;  //CLKEN =1 ;PrAcc=0;ProbTrap=1;ProbEn=1 

   if (tyUserSettings->EJTAGStyle == (unsigned long)EJTAG20StyleExt)
      ptyHandle->tyTraceSettings.bExtendedEJTAG20Trace = TRUE;
   else
      ptyHandle->tyTraceSettings.bExtendedEJTAG20Trace = FALSE;

   return ERR_NO_ERROR;
} //lint -e1746

/****************************************************************************
     Function: ML_ConfigureMulticoreSupport
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle to device.
               bInitial  : '1' configures them scanchain for no-multicore
                           '0' configures it as per actual multicore info 
       Output: Error code
  Description: Configures OpellaXD Multicore support
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
void ML_ConfigureMulticoreSupport(TyMLHandle *ptyHandle, unsigned char bInitial)
{
   // Opella-XD multicore setup
   TyCoreConfig ptyCoreConfig[MAX_CORES_ON_SCANCHAIN];
   unsigned char ucIndex;

   if ((!bInitial) && (ptyHandle->ulNumberOfCoresOnScanchain > 1))
      {
      for (ucIndex=0; ucIndex < MAX_CORES_ON_SCANCHAIN; ucIndex++)
         {
         unsigned long ulBypassCode = 0xFFFFFFFF;
         if (ucIndex < ptyHandle->ulNumberOfCoresOnScanchain)
            {     // copy data from DW structure
            ptyCoreConfig[ucIndex].usDefaultIRLength = 
            (unsigned short)ptyHandle->tyMultiCoreDeviceInfo[ucIndex].ulIRLength;
            ptyCoreConfig[ucIndex].usDefaultDRLength = 32;        // does not matter
            ptyCoreConfig[ucIndex].pulBypassIRPattern = 
            &ptyHandle->tyMultiCoreDeviceInfo[ucIndex].ulBypassCode;
            }
         else
            {     // empty rest of items
            ptyCoreConfig[ucIndex].usDefaultIRLength = 5;         // does not matter, should not be used at all
            ptyCoreConfig[ucIndex].usDefaultDRLength = 32;        // does not matter
            ptyCoreConfig[ucIndex].pulBypassIRPattern = &ulBypassCode;
            }
         }
      (void)DL_OPXD_JtagConfigMulticore(ptyHandle->iDLHandle, 
                                        ptyHandle->ulNumberOfCoresOnScanchain, 
                                        ptyCoreConfig);
      }
   else
      {
      (void)DL_OPXD_JtagConfigMulticore(ptyHandle->iDLHandle, 0, NULL);
      }
}

/****************************************************************************
     Function: ML_WriteOpellaXDTargetConfig
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle to the devices
               ptyUserSettings : user settings (from the cfg file)
       Output: Error code
  Description: Configures OpellaXD diskware
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
//static 
TyError ML_WriteOpellaXDTargetConfig(TyMLHandle *ptyHandle, 
                                     TyUserSettings *ptyUserSettings)
{
   TyDwMipsCoreConfig tyDwMipsCoreConfig;
   TyDwScanConfig tyDwScanConfig;
   TyError ErrRet = ERR_NO_ERROR;
   // now fill mips core config (keep alphabetical order in TyDwMipsCoreConfig structure for easy maintaining)
   tyDwMipsCoreConfig.bBigEndian                               = ptyHandle->bBigEndian;
   tyDwMipsCoreConfig.bDMASupported                            = ptyHandle->bDmaSupported;
   tyDwMipsCoreConfig.bDMATurnedOnByUser                       = ptyHandle->bDmaEnabledByUser;
   tyDwMipsCoreConfig.bDoubleReadOnDMA                         = ptyUserSettings->bDoubleReadOnDMA;
   tyDwMipsCoreConfig.bDszInsteadOfPsz                         = ptyHandle->bDszInsteadOfPsz;
   tyDwMipsCoreConfig.ulEjtagStyle                       = ptyUserSettings->EJTAGStyle;
   tyDwMipsCoreConfig.bLevelWhichIndicatesProcessorClockHalted = ptyHandle->bLevelWhichIndicatesProcessorClockHalted;
   tyDwMipsCoreConfig.bProbeTrapFlagExists                     = ptyHandle->bProbeTrapFlagExists;
   tyDwMipsCoreConfig.bSelectAddrRegAfterEveryAccess           = ptyUserSettings->bSelectAddrRegAfterEveryAccess;
   tyDwMipsCoreConfig.bSyncInstNotSupported                    = ptyUserSettings->bSyncInstNotSupported;
   tyDwMipsCoreConfig.bUseFastDMAFunc                          = ptyUserSettings->bUseFastDMAFunc;
   tyDwMipsCoreConfig.bUseRestrictedDmaAddressRange            = ptyUserSettings->bUseRestrictedDmaAddressRange;
   tyDwMipsCoreConfig.ucDefaultValueOfClkEnBit                 = ptyHandle->ucDefaultValueOfClkEnBit;
   tyDwMipsCoreConfig.ucMapSystem                              = (unsigned char)ptyHandle->ucFixedMapSystem;
   tyDwMipsCoreConfig.ucNoOfNOPSInERETBDS                      = ptyHandle->ucNoOfNOPSInERETBDS;
   tyDwMipsCoreConfig.ucNumberOfNOPSInJUMPBDS                  = ptyUserSettings->ucNumberOfNOPSInJUMPBDS;
   tyDwMipsCoreConfig.ulDefaultControlCompleteAccess           = ptyHandle->ulDefaultControlCompleteAccess;
   tyDwMipsCoreConfig.ulDefaultControlProbeWillService         = ptyHandle->ulDefaultControlProbeWillService;
   tyDwMipsCoreConfig.ulDRLength                               = 32;                   // not configurable in PathFinder
   tyDwMipsCoreConfig.ulEndOfValidDmaAddressRange              = ptyHandle->ulEndOfDmaAddressRange;
   tyDwMipsCoreConfig.ulExpectedControlDatWrite                = ptyHandle->ulExpectedControlDatWrite;
   tyDwMipsCoreConfig.ulExpectedControlInsDatFetch             = ptyHandle->ulExpectedControlInsDatFetch;
   tyDwMipsCoreConfig.ulIRLength                               = 5;                    // not configurable in PathFinder
   tyDwMipsCoreConfig.ulLengthOfAddressRegister                = (unsigned long)ptyUserSettings->ucLengthOfAddressRegister;
   tyDwMipsCoreConfig.ulMaxDW2Loops                            = (unsigned long)ptyHandle->uiMaxDW2Loops;
   tyDwMipsCoreConfig.ulStartOfValidDmaAddressRange            = ptyHandle->ulStartOfDmaAddressRange;
   tyDwMipsCoreConfig.usDMACoreNumber                          = (unsigned short)ptyHandle->ulDmaCore;
   tyDwMipsCoreConfig.ulCacheDebugReadTransferRestriction      = ptyUserSettings->ulLimitCacheRdBpS;
   tyDwMipsCoreConfig.ulCacheDebugWriteTransferRestriction     = ptyUserSettings->ulLimitCacheWrBpS;
   tyDwMipsCoreConfig.ulDmaReadTransferRestriction          = ptyUserSettings->ulLimitDmaRdBpS;
   tyDwMipsCoreConfig.ulDmaWriteTransferRestriction            = ptyUserSettings->ulLimitDmaWrBpS;
   tyDwMipsCoreConfig.ulMaximumFrequency                 = 100000000;            // set 100 MHz as maximum for the target

   // fill information about whole scanchain 
   if (ptyHandle->ulNumberOfCoresOnScanchain > 1)
      {
      tyDwScanConfig.usNumberOfCores                              = (unsigned short)ptyHandle->ulNumberOfCoresOnScanchain;
      tyDwScanConfig.ulEnablingIRLength                           = ptyHandle->ulCoreSelectIRLength;          // length of IR when enabling other cores
      tyDwScanConfig.pulEnablingIRValue[0]                        = ptyHandle->ulCoreSelectCode;              // value for IR when enabling other cores
      }
   else
      {
      tyDwScanConfig.usNumberOfCores                              = 1;
      tyDwScanConfig.ulEnablingIRLength                           = 0;
      tyDwScanConfig.pulEnablingIRValue[0]                        = 0x0;
      }

   // write configuration into Opella-XD MIPS diskware
   (void)DL_OPXD_Mips_WriteDiskwareStruct(ptyHandle->iDLHandle, 0, (int)DWS_SCAN, (void *)&tyDwScanConfig, sizeof(TyDwScanConfig));
   (void)DL_OPXD_Mips_WriteDiskwareStruct(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, (int)DWS_MIPS_CORE, (void *)&tyDwMipsCoreConfig, sizeof(TyDwMipsCoreConfig));
   // if DMA and DEBUG cores are different, we use same config for both of them (PFMIPS has only single config structure even if DMA and DEBUG cores are different)
   if (ptyHandle->ulMipsCore != ptyHandle->ulDmaCore)
      (void)DL_OPXD_Mips_WriteDiskwareStruct(ptyHandle->iDLHandle, ptyHandle->ulDmaCore, (int)DWS_MIPS_CORE, (void *)&tyDwMipsCoreConfig, sizeof(TyDwMipsCoreConfig));

   return ErrRet;
}

/****************************************************************************
     Function: ML_ExecuteDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               void *pvApp - pointer to DW2 app instructions
               unsigned int uiAppBytes - size of DW2 app in bytes
               void *pvDataToProc - pointer to DW2 app data to MIPS
               unsigned long ulDataToProcWords - size of data to MIPS via DW2 app in bytes
               void *pvDataFromProc - pointer to DW2 app data from MIPS
               unsigned int uiDataFromProcBytes - size of data from MIPS via DW2 app in bytes
       Output: int - error code (see ml_error.h)
  Description: This is key function in midlayer which call diskware to execute specified DW2
               application (MIPS code) with some data given to application for MIPS and reading
               some data by application from MIPS.
               Application will be executed by diskware using EJTAG in DMSEG memory.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
int ML_ExecuteDW2(TyMLHandle  *ptyHandle, 
                  void         *pvApp, 
                  unsigned int    uiAppBytes, 
                  void         *pvDataToProc, 
                  unsigned int    uiDataToProcBytes, 
                  void        *pvDataFromProc, 
                  unsigned int    uiDataFromProcBytes)
{
   TyError tyResult;
   TyDW2AppParams tyLocDW2Params;
   // just fill parameters and call DL function
   tyLocDW2Params.pvAppInst = pvApp;                  
   tyLocDW2Params.uiInstSize = uiAppBytes;
   tyLocDW2Params.ppvAppDataToProc[0] = pvDataToProc;        
   tyLocDW2Params.puiDataToProcSize[0] = uiDataToProcBytes;
   tyLocDW2Params.ppvAppDataToProc[1] = NULL;                 
   tyLocDW2Params.puiDataToProcSize[1] = 0;
   tyLocDW2Params.ppvAppDataFromProc[0] = pvDataFromProc;    
   tyLocDW2Params.puiDataFromProcSize[0] = uiDataFromProcBytes;
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;               
   tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;    // diskware must know if processor is BE or LE when handling byte/halfword access to EJTAG DATA
   tyLocDW2Params.bSwapDataValues = 0;                   // do not swap words/halfwords values in data to/from processor, just keep them in current format
   // call DL function
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   dbgprint("DL_OPXD_Mips_ExecuteDW2App: e%d \n", tyResult);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}
#endif
/****************************************************************************
     Function: ML_ExecuteDW2
     Engineer: Nikolay Chokoev
        Input: TyMLHandle *ptyHandle - handle to connection
               *pulApplication - pointer to DW2 app instructions
               ucNumberOfInstructions - number of instructions to be executed
               *pulDataToProc - pointer to DW2 app data to MIPS
               *pulDataFromProc - pointer to DW2 app data from MIPS
       Output: int - error code (see ml_error.h)
  Description: This is key function in midlayer which call diskware to execute specified DW2
               application (MIPS code) with some data given to application for MIPS and reading
               some data by application from MIPS.
               Application will be executed by diskware using EJTAG in DMSEG memory.
Date           Initials    Description
17-Jul-2008    NCH         Initial
****************************************************************************/
int ML_ExecuteDW2(TyMLHandle  *ptyHandle, 
                  void         *pvApp, 
                  unsigned int    uiAppBytes, 
                  void         *pvDataToProc, 
                  unsigned int    uiDataToProcBytes, 
                  void        *pvDataFromProc, 
                  unsigned int    uiDataFromProcBytes)
{

   TyError ErrRet = ERR_NO_ERROR;
   assert(pvApp != NULL);

   if (uiDataToProcBytes > sizeof(pulCommonDataToProc))
      return ERR_UNKNOWN_ERROR;

   DLOG3((LOGFILE,"\n\t\tML_ExecuteDW2 -> Called"));
   DLOG3((LOGFILE,"\n\t\tML_ExecuteDW2 -> uiDataToProcBytes  : %d", uiDataToProcBytes));
   DLOG3((LOGFILE,"\n\t\tML_ExecuteDW2 -> uiDataFromProcBytes: %d", uiDataFromProcBytes));

   memcpy(pulCommonDataToProc,pvDataToProc,uiDataToProcBytes);
   ErrRet = DL_OPXD_Mips_ExecuteDW2(ptyHandle->iDLHandle, 
                                    ptyHandle->ulMipsCore,
                                    (unsigned long*)pvApp, 
                                    pulCommonDataFromProc, 
                                    pulCommonDataToProc, 
                                    (unsigned short)uiAppBytes/sizeof(unsigned long), 
                                    ptyHandle->bBigEndian);

   if ((pvDataFromProc != NULL) &&
       (uiDataFromProcBytes > 0))
      memcpy(pvDataFromProc,pulCommonDataFromProc,uiDataFromProcBytes);

   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      {
      dbgprint("ML_ExecuteDW2 : e%d\n\n",ErrRet);
      return ML_ConvertDLError(ErrRet);
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_MDIRead
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulSrcResource - source resource
               unsigned long ulSrcOffset - source offset
               unsigned char *pucBuff - pointer to buffer
               unsigned long ulCount - number of items
               unsigned long ulObjectSize - item size
       Output: int - error code (see ml_error.h)
  Description: Read MDI resource.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_MDIRead(TyMLHandle *ptyHandle, 
               unsigned long ulSrcResource, 
               unsigned long ulSrcOffset, 
               unsigned char *pucBuff, 
               unsigned long ulCount, 
               unsigned long ulObjectSize)
{
   unsigned long ulLocWord = 0;
   unsigned long ulByteSize;
   // check parameters
   assert(ptyHandle != NULL);
   if (ulObjectSize <= 1)
      ulByteSize = ulCount;
   else
      ulByteSize = ulObjectSize * ulCount;
   // determine which resource to use
   switch (ulSrcResource)
      {
      case MDIMIPGVIRTUAL:    // access to MIPS virtual memory space
         return MLM_ReadMemory(ptyHandle, ulSrcOffset, pucBuff, ulByteSize, ulObjectSize);
      case MDIMIPPHYSICAL:    // access to MIPS physical memory space
         return MLM_ReadMemoryPhysical(ptyHandle, ulSrcOffset, pucBuff, ulByteSize);
      case MDIMIPEJTAG:       // access to MIPS EJTAG memory space (DRSEG)
         return MLM_ReadMemoryEjtag(ptyHandle, ulSrcOffset, pucBuff, ulByteSize);
      case MDIMIPTLB:
         return MLT_ReadTLB(ptyHandle, ulSrcOffset, (void *)pucBuff, ulCount, ulObjectSize);
      case MDIMIPPDCACHE:
      case MDIMIPPICACHE:
         {
            unsigned long ulCacheStartIndex, ulCacheEndIndex;
            if (ulCount == 0)
               return ERR_NO_ERROR;
            ulCacheStartIndex = ulSrcOffset / sizeof(unsigned long);       // offset is in bytes, we need offset in unsigned long
            ulCacheEndIndex = (ulCacheStartIndex + ((ulCount * ulObjectSize) / sizeof(unsigned long))) - 1;
            return MLC_ReadCacheResourceArrayRange(ptyHandle, ulSrcResource, ulCacheStartIndex, ulCacheEndIndex, (void *)pucBuff, 0);
         }
      case MDIMIPPICACHET:
      case MDIMIPPDCACHET:
      case MDIMIPSICACHEWS:
      case MDIMIPSDCACHEWS:
         {
            unsigned long ulCacheStartIndex, ulCacheEndIndex;
            if (ulCount == 0)
               return ERR_NO_ERROR;
            ulCacheStartIndex = ulSrcOffset;                               // ofset is in entries
            ulCacheEndIndex = (ulCacheStartIndex + ((ulCount * ulObjectSize) / sizeof(unsigned long))) - 1;
            return MLC_ReadCacheResourceArrayRange(ptyHandle, ulSrcResource, ulCacheStartIndex, ulCacheEndIndex, (void *)pucBuff, 0); 
         }
      case ASHLING_INT_REG:      // accessing internal register style
         {
            int iResult = MLR_ReadRegister(ptyHandle, ulSrcOffset, ASHLING_INT_REG, &ulLocWord);
            memcpy((void *)pucBuff, (void *)&(ulLocWord), sizeof(unsigned long));
            return iResult;
         }
      case MDIMIP192ACC:         // accessing HiperSmart ACX register
         {
            int iResult = MLR_ReadRegister(ptyHandle, ulSrcOffset, ulSrcResource, &ulLocWord);
            memcpy((void *)pucBuff, (void *)&(ulLocWord), sizeof(unsigned long));
            return iResult;
         }
      default:
         {
            memset((void *)pucBuff, 0, ulByteSize);
            return ERR_NO_ERROR;
         }
      }
} 

/****************************************************************************
     Function: ML_MDIWrite
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulSrcResource - source resource
               unsigned long ulSrcOffset - source offset
               unsigned char *pucBuff - pointer to buffer
               unsigned long ulCount - number of items
               unsigned long ulObjectSize - item size
       Output: int - error code (see ml_error.h)
  Description: Write MDI resource.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_MDIWrite(TyMLHandle *ptyHandle, 
                unsigned long ulSrcResource, 
                unsigned long ulSrcOffset, 
                unsigned char *pucBuff, 
                unsigned long ulCount, 
                unsigned long ulObjectSize)
{
   unsigned long ulLocWord = 0;
   unsigned long ulByteSize;
   // check parameters
   assert(ptyHandle != NULL);
   if (ulObjectSize <= 1)
      ulByteSize = ulCount;
   else
      ulByteSize = ulCount * ulObjectSize;
   // determine which resource to use
   switch (ulSrcResource)
      {
      case MDIMIPGVIRTUAL:    // accessing MIPS virtual memory space
         return MLM_WriteMemory(ptyHandle, ulSrcOffset, pucBuff, ulByteSize, ulObjectSize);
      case MDIMIPPHYSICAL:    // accessing MIPS physical memory space
         return MLM_WriteMemoryPhysical(ptyHandle, ulSrcOffset, pucBuff, ulByteSize);
      case MDIMIPEJTAG:       // accessing MIPS EJTAG memory space (DRSEG)
         return MLM_WriteMemoryEjtag(ptyHandle, ulSrcOffset, pucBuff, ulByteSize);
      case MDIMIPTLB:
         return MLT_WriteTLB(ptyHandle, ulSrcOffset, (void *)pucBuff, ulCount, ulObjectSize);
      case MDIMIPPDCACHE:                                 
      case MDIMIPPICACHE:
         {
            unsigned long ulWordSelect = (ulSrcOffset / sizeof(unsigned long)) % 4;
            unsigned long ulCacheIndex = (ulSrcOffset / sizeof(unsigned long)) / 4;
            return MLC_AccessCacheTag(ptyHandle, 0, ulSrcResource, ulCacheIndex, ulWordSelect, NULL, (unsigned long *)pucBuff, ulCount, ulObjectSize);
         }
      case MDIMIPPICACHET:
      case MDIMIPPDCACHET:
      case MDIMIPSICACHEWS:
      case MDIMIPSDCACHEWS:
         {
            unsigned long ulCacheIndex = ulSrcOffset / 2;
            unsigned long ulWordSelect = 0;
            return MLC_AccessCacheTag(ptyHandle, 0, ulSrcResource, ulCacheIndex, ulWordSelect, (unsigned long *)pucBuff, NULL, ulCount, ulObjectSize); 
         }
      case ASHLING_INT_REG:      // accessing internal register
         memcpy((void *)&(ulLocWord), (void *)pucBuff, sizeof(unsigned long));
         return MLR_WriteRegister(ptyHandle, ulSrcOffset, ASHLING_INT_REG, ulLocWord);
      case MDIMIP192ACC:         // accessing HiperSmart ACX register
         memcpy((void *)&(ulLocWord), (void *)pucBuff, sizeof(unsigned long));
         return MLR_WriteRegister(ptyHandle, ulSrcOffset, ulSrcResource, ulLocWord);
      default:
         return ERR_NO_ERROR;
      }
} 



/****************************************************************************
     Function: ML_ScanIR
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulIRLength - IR register length
               unsigned long *pulIRDataIn - value to IR register
               unsigned long *pulIRDataOut - value from IR register
       Output: int - error code (see ml_error.h)
  Description: Select specific IR register. All communication with current MIPS core.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_ScanIR(TyMLHandle *ptyHandle, 
              unsigned long ulIRLength, 
              unsigned long *pulIRDataIn, 
              unsigned long *pulIRDataOut)
{
   TyError tyResult;
   // check parameters
   assert(ptyHandle != NULL);
   // do not check other parameters as they are going to be handled in DL_OPXD_xxx function
   tyResult = DL_OPXD_JtagScanIR(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, ulIRLength, pulIRDataIn, pulIRDataOut);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}

/****************************************************************************
     Function: ML_ScanDR
     Engineer: Nikolay Chokoev
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulDRLength - DR register length
               unsigned long *pulDRDataIn - value to DR register
               unsigned long *pulDRDataOut - value from DR register
       Output: int - error code (see ml_error.h)
  Description: Select specific DR register. All communication with current MIPS core.
Date           Initials    Description
08-Feb-2008    NCH         Initial
****************************************************************************/
int ML_ScanDR(TyMLHandle *ptyHandle, 
              unsigned long ulDRLength, 
              unsigned long *pulDRDataIn, 
              unsigned long *pulDRDataOut)
{
   TyError tyResult;
   // check parameters
   assert(ptyHandle != NULL);
   // do not check other parameters as they are going to be handled in DL_OPXD_xxx function
   tyResult = DL_OPXD_JtagScanDR(ptyHandle->iDLHandle, 
                                 ptyHandle->ulMipsCore, 
                                 ulDRLength, 
                                 pulDRDataIn, 
                                 pulDRDataOut);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}

/****************************************************************************
     Function: ML_ScanIRandDR
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulIRLength - IR register length
               unsigned long *pulIRDataIn - value to IR register
               unsigned long ulDRLength - DR register length
               unsigned long *pulDRDataIn - value to DR register
               unsigned long *pulDRDataOut - value from DR register
       Output: int - error code (see ml_error.h)
  Description: Select specific IR register and shift data in and from DR register
               with given number of bits. All communication with current MIPS core.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_ScanIRandDR(TyMLHandle *ptyHandle, 
                   unsigned long ulIRLength, 
                   unsigned long *pulIRDataIn, 
                   unsigned long ulDRLength, 
                   unsigned long *pulDRDataIn, 
                   unsigned long *pulDRDataOut)
{
   TyError tyResult;
   // check parameters
   assert(ptyHandle != NULL);
   // do not check other parameters as they are going to be handled in DL_OPXD_xxx function
   tyResult = DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, ulIRLength, pulIRDataIn,
                                      ulDRLength, pulDRDataIn, pulDRDataOut); 
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}

/****************************************************************************
     Function: ML_ConvertDLError
     Engineer: Vitezslav Hola
        Input: int iError - error code from DL (driver layer)
       Output: int - error code (see ml_error.h)
  Description: Convert error code obtained from driver layer to midlayer error code.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_ConvertDLError(int iError)
{
   int iResult;
   // it is important to check no error case separately first to be fast as possible
   switch (iError)
      {
      // no error
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_NO_ERROR;
         break;
         // general error messages
      case DRVOPXD_ERROR_NO_USB_DEVICE_CONNECTED:           // no Opella-XD connected
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         iResult = ERR_NO_USB_DEVICE_CONNECTED;
         break;
      case DRVOPXD_ERROR_DEVICE_ALREADY_IN_USE:
         iResult = ERR_FAILED_SEND_USB_PACKET;
         break;
      case DRVOPXD_ERROR_USB_DRIVER:
         iResult = ERR_OPELLA_FAILURE;
         break;
      case DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE:
         iResult = ERR_FAILED_TO_PROGRAM_FPGA;
         break;
      case DRVOPXD_ERROR_INVALID_FIRMWARE:
      case DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME:
         iResult = ERR_FAILED_TO_FIND_DISKWARE_FILE;
         break;
      case DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME:
         iResult = ERR_FAILED_TO_FIND_FPGA_FILE;
         break;
      case DRVOPXD_ERROR_TGT_RESET_OCCURRED:
      case DRVOPXD_ERROR_EXECUTE_DW2:
         iResult = ERR_EXEDW2_RESET_OCCURRED;
         break;
      default:                                              // unknown error code
         iResult = ERR_OPELLA_FAILURE;
         break;
      }
   return iResult;
}

/****************************************************************************
     Function: ML_SwapEndianWord
     Engineer: Vitezslav Hola
        Input: unsigned long ulWord - word value
       Output: unsigned long - word value with swapped bytes
  Description: Swap bytes in words to convert between LE and BE.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned long ML_SwapEndianWord(unsigned long ulWord)
{
   unsigned long ulLocValue = (ulWord >> 24) | (ulWord << 24);
   ulLocValue |= ((ulWord >> 8) & 0x0000FF00);
   ulLocValue |= ((ulWord << 8) & 0x00FF0000);
   return ulLocValue;
}

/****************************************************************************
     Function: ML_SwapEndianHalfword
     Engineer: Vitezslav Hola
        Input: unsigned short usHalfword - halfword value
       Output: unsigned short - halfword value with swapped bytes
  Description: Swap bytes in halfword to convert between LE and BE.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned short ML_SwapEndianHalfword(unsigned short usHalfword)
{
   return((usHalfword >> 8) | ((usHalfword << 8) & 0xFF00));
}

/****************************************************************************
     Function: ML_SwapEndianess
     Engineer: Vitezslav Hola
        Input: void *pvData - pointer to block of data
               unsigned long ulCount - number of values
               unsigned long ulObjectSize - object size
       Output: none
  Description: Swap endianess in whole block of data depending on object size.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
void ML_SwapEndianess(void *pvData, 
                      unsigned long ulCount, 
                      unsigned long ulObjectSize)
{
   if ((pvData == NULL) || (ulCount == 0))
      return;
   if (ulObjectSize == 4)
      {  // swap bytes in words
      unsigned long ulOrigValue, ulSwappedValue;
      unsigned long *pulLocPtr = (unsigned long *)pvData;
      for (unsigned long ulIndex = 0; ulIndex < ulCount; ulIndex++)
         {
         ulOrigValue = *pulLocPtr;
         ulSwappedValue = (ulOrigValue >> 24) | (ulOrigValue << 24);
         ulSwappedValue |= ((ulOrigValue >> 8) & 0x0000FF00);
         *pulLocPtr++ = ulSwappedValue | ((ulOrigValue << 8) & 0x00FF0000);
         }
      }
   else if (ulObjectSize == 2)
      {  // swap bytes in halfwords
      unsigned short usOrigValue, usSwappedValue;
      unsigned short *pusLocPtr = (unsigned short *)pvData;
      for (unsigned long ulIndex = 0; ulIndex < ulCount; ulIndex++)
         {
         usOrigValue = *pusLocPtr;
         usSwappedValue = (usOrigValue >> 8);
         usSwappedValue |= ((usOrigValue & 0x00FF) << 8);
         *pusLocPtr++ = usSwappedValue;
         }
      }
   // no other supported object size
}

/****************************************************************************
     Function: ML_Delay
     Engineer: Vitezslav Hola
        Input: unsigned int uiMiliseconds - number of miliseconds to sleep
       Output: none
  Description: Delay in miliseconds for Windows and Linux.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
void ML_Delay(unsigned int uiMiliseconds)
{
#ifndef __LINUX
   // Windows implementation, just using Win32 API
   Sleep(uiMiliseconds);
#else
   // Linux implementation, using usleep (time in microseconds)
   usleep(uiMiliseconds * 1000);
#endif
}

/****************************************************************************
     Function: ML_SetBootType
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned char bNormalBoot - 1 to use NORMALBOOT or 0 for EJTAGBOOT
       Output: int - error code
  Description: Select boot type behavior using EJTAG NORMALBOOT or EJTABOOT instructions.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_SetBootType(TyMLHandle *ptyHandle, 
                   unsigned char bNormalBoot)
{
   assert(ptyHandle != NULL);
   ptyHandle->bNormalBootSelected = bNormalBoot;
   if (ptyHandle->bEJTAGBootSupported && ptyHandle->bUseEjtagBoot)
      {
      unsigned long ulIRValue;
      if (bNormalBoot)
         ulIRValue = MIPS_EJTAG_NORMALBOOT_INST;
      else
         ulIRValue = MIPS_EJTAG_EJTAGBOOT_INST;
      return ML_ScanIR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, NULL);
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_NotImplementedYet
     Engineer: Vitezslav Hola
        Input: none
       Output: int - error code
  Description: dummy function as reference to function not implemented yet
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int ML_NotImplementedYet(TyMLHandle *ptyHandle)
{
   NOREF(ptyHandle);
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetupConfig0Masks
     Engineer: Nikolay Chokoev
        Input: ptyHandle : Handle to the device
               bEnableKSEG0CacheForNow : 
       Output: none
  Description: Determine the masks for CP0 Register Config Select 0
Date           Initials    Description
04-Jul-2008    NCH          Initial
todo: Init bAutoHaltCountersSupported, bUserHaltCountersIDM
****************************************************************************/
void ML_SetupConfig0Masks(TyMLHandle *ptyHandle, 
                          int bEnableKSEG0CacheForNow)
{
   unsigned long ulConfigORMask; 
   unsigned long ulConfigANDMask;

   assert(ptyHandle != NULL);
   ulConfigORMask  = 0x0;        // By Default Don't Set any bits
   ulConfigANDMask = 0xFFFFFFFF; // By Default Don't Clear any Bits

   if ((EJTAG_VERSION_20 == ptyHandle->ucEjtagVersion) && 
       ptyHandle->bUserHaltCountersIDM)
      {
      // MIPS PRX9XX Processor and We have chosen to Halt Coutners
      ulConfigORMask = ulConfigORMask | 0x18;   // Set bits 3,4 in Config to Stop Timers
      }
   else
      {
      // Either Counters or Halted Automatically or the user has chosen not to Halt them
      ulConfigORMask  = ulConfigORMask  | 0x0;  // Don't Set Any Bits
      }

   if (!bEnableKSEG0CacheForNow)
      {
      if (EJTAG_VERSION_20 == ptyHandle->ucEjtagVersion)
         {
         ulConfigORMask = ulConfigORMask | 0x0C000000; // Set Bits 26,27
         }
      else
         {
         ulConfigORMask = ulConfigORMask | 0x00000002;     // Set Bits 1
         ulConfigANDMask = ulConfigANDMask & 0xFFFFFFFA;   // Clear Bits 0,2
         }
      }

   // Use to Mask subsequent writes by the user to the Config Register
   ptyHandle->ulConfigORMask  = ulConfigORMask;  
   // Used to Mask subsequent writes by they user to the Config Register
   ptyHandle->ulConfigANDMask = ulConfigANDMask; 
}

/****************************************************************************
     Function: ML_EnableMemoryWriting
     Engineer: Nikolay Chokoev
        Input: ptyHandle      : Device handle
               bStartExecute  :
               bFinishExecute :
               bReset         :
       Output: none
  Description: Some configurations require a write to a "special" memory region
               to enable writing...do it if required
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
void ML_EnableMemoryWriting(TyMLHandle    *ptyHandle,
                            int      bStartExecute,
                            int      bFinishExecute,
                            int      bReset)  
{
   int iErrRet = ERR_NO_ERROR;
   if (ptyHandle->bEnableWriteRequired && ptyHandle->Pi.bPipeLineLearned)
      {
      // Read/Modify/Write the desired value
      unsigned long  ulData = 0x0 ;

      if ((iErrRet=MLM_ReadMemory(ptyHandle,
                                  ptyHandle->ulEnableWriteAddress,
                                  (unsigned char *)&ulData,
                                  sizeof(ulData),
                                  0))== ERR_NO_ERROR)
         {
         if (bFinishExecute || bReset)
            {
            // Remember this value because we will restore it when we
            // go back into emulation
            ptyHandle->ulAppWriteData = ulData;
            }

         if (bStartExecute)
            {
            // Restore the application value
            ulData = ptyHandle->ulAppWriteData;
            }
         else
            {
            // Modify it for enabling writing
            ulData &= ptyHandle->ulEnableWriteMask; 
            ulData |= ptyHandle->ulEnableWriteValue;
            }
         iErrRet = MLM_WriteWordsDW2(ptyHandle,
                                     ptyHandle->ulEnableWriteAddress, 
                                     1,
                                     (void*)&ulData,
                                     TRUE);
         if (iErrRet!= ERR_NO_ERROR)
            {
            ASSERT_NOW();     
            }
         }
      }
}

#if 0
/****************************************************************************
     Function: ML_InstallDW2Application
     Engineer: Nikolay Chokoev
        Input: *ptyHandle  :
            ulAddress :
       Output: Error code
  Description: Copy the Cache Flush application to Target memory
Date           Initials    Description
07-Jul-2008    NCH          Initial
06-Aug-2009    SPC          Commented for making cache flush routine run from probe
****************************************************************************/
static int ML_InstallDW2Application(TyMLHandle *ptyHandle,
                                    unsigned long *pulApp,
                                    unsigned long ulAppStartAddress,
                                    unsigned long ulAppSizeBytes,
                                    int bBackupMemory, 
                                    int bVerify)
{

   int iErrRet = ERR_NO_ERROR;
   unsigned long pszReadBack[MAX_APPLICATION_SIZE];
   unsigned long ulIndex;
   int bBackupOfCacheFlushEnabled;

   DLOG2((LOGFILE,"\n\t\tML_InstallDW2Application -> Called (Copies app to target mem)"));

   if (bBackupMemory)
      {
      DLOG((LOGFILE,"\n\t\tML_InstallDW2Application ->  Backup Memory Not Implemented"));
      return ERR_INSTALLDW2_NOBACKUP; //todo error in ML
      }

   // Next Check if the application is too big to fit in the allocated space

   if (ulAppSizeBytes > LENGTH_OF_MEM_RESERVED_FOR_CACHE_ROUTINE )
      {
      DLOG((LOGFILE,"\n\t\tML_InstallDW2Application ->  Insufficient Target Memory for Cache Flush Routine %08X bytes required",ulAppSizeBytes));
      return ERR_INSTALLDW2_INSUFFICIENTMEM; //todo error in ML
      }

   if (pulApp != NULL)
      {
      // OK so we are going to download a diskware 2 application
      // We need to be very careful of the endian issues here
      // The Rules:
      // 1. If you are going to use this code in a particular endian mode
      //    then  you must write it to memory in that mode. You cannot guess
      //    the byte order because you don't know if mem is Byte,H-Word or Word.
      // 2. We only need to write it in one endian mode (the current one) because
      //    if the endian mode changes later, then we know it can be changed and 
      //    as such we simply change it back to the previous one to run the code.
      // 3. We have made the difficult but admirable decision of never corrupting 
      //    the ptyHandle->bBigEndian flag. This would make the task of doing the 
      //    write in big endian mode very easy but it would also require years in
      //    purgatory

      // Step 1: Check if we need to reverse the SREC, and do it if necessary

      // Important that we don't try to use target memory when installing the target memory app
      // Don't forget to backup the status of the ReadWriteFast options first
      bBackupOfCacheFlushEnabled = ptyHandle->bCacheFlushEnabled;
      ptyHandle->bCacheFlushEnabled  = FALSE;

      // Step 2: Now we simply download the Srecord
      iErrRet=MLM_WriteMemory(ptyHandle,
                              ulAppStartAddress,
                              (unsigned char*)pulApp,
                              ulAppSizeBytes, 0);

      // Don't forget to restore the backup
      ptyHandle->bCacheFlushEnabled  = (unsigned char)bBackupOfCacheFlushEnabled;
      if (iErrRet != ERR_NO_ERROR)
         return iErrRet;

      DLOG3((LOGFILE,"\n\t\tML_InstallDW2Application -> Installed Application"));
      }
   else
      {
      DLOG((LOGFILE,"\n\t\tML_InstallDW2Application -> Attempt to Install Invalid Application"));
      return ERR_INSTALLDW2_INVALID_APP;  //todo error in ML
      }

   if (bVerify)
      {
      DLOG3((LOGFILE,"\n\t\tML_InstallDW2Application -> Verify Application"));
      iErrRet=MLM_ReadMemory(ptyHandle,
                             ulAppStartAddress,
                             (unsigned char *)pszReadBack,
                             ulAppSizeBytes, 0);

      for (ulIndex=0; ulIndex < (ulAppSizeBytes/sizeof(unsigned long)); ulIndex++)
         {
         if (pszReadBack[ulIndex] != pulApp[ulIndex])
            {
            DLOG((LOGFILE,"\n\t\tML_InstallDW2Application -> Verify Failed"));
            return ERR_INSTALLDW2_VERIFY_FAILED; //todo error in ML
            }
         }

      DLOG3((LOGFILE,"\n\t\tML_InstallDW2Application -> Verify Successful"));
      }

   return iErrRet;

}
#endif
/****************************************************************************
     Function: ML_SetTargetEndian
     Engineer: Nikolay Chokoev
        Input: *ptyHandle  :
            bSetToBigEndian :
       Output: Error code
  Description: Copy the Cache Flush application to Target memory
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
int ML_SetTargetEndian(TyMLHandle *ptyHandle,
                       int bSetToBigEndian)
{
   int iErrRet;
   unsigned long ulCP0ConfigValue;

   DLOG((LOGFILE,"\nML_SetTargetEndian -> Called : BigEndian = %X",bSetToBigEndian));

   if (ptyHandle->bBigEndian == bSetToBigEndian)
      {
      // We already have the correct endian setting. do nothing
      return ERR_NO_ERROR;
      }

   // Now we must change endian
   // First Read the register containing the Endian Flag

   iErrRet = MLR_ReadRegister(ptyHandle, ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, &ulCP0ConfigValue);
   if (iErrRet != ERR_NO_ERROR)
      return iErrRet;

   // Now Set or Clear the END or BE bit as requested

   if (bSetToBigEndian)
      ulCP0ConfigValue = ulCP0ConfigValue | (1UL << ptyHandle->ulCP0BitOffsetInConfigforEndianMode);
   else
      ulCP0ConfigValue = ulCP0ConfigValue & ~(1UL << ptyHandle->ulCP0BitOffsetInConfigforEndianMode);

   // Then write the value back

   iErrRet = MLR_WriteRegister(ptyHandle, ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, ulCP0ConfigValue);
   if (iErrRet != ERR_NO_ERROR)
      return iErrRet;

   // Finally Check if the Change worked

   if (ptyHandle->bBigEndian != bSetToBigEndian)
      {
      // Our attempted change had no effect. Better Luck next time.
      return ERR_SETEND_CHANGE_FAILED;
      }

   return ERR_NO_ERROR;

}

#if 0
/****************************************************************************
     Function: ML_InstallCacheFlushApp
     Engineer: Nikolay Chokoev
        Input: *ptyHandle  :
            ulAddress :
       Output: Error code
  Description: Copy the Cache Flush application to Target memory
Date           Initials    Description
07-Jul-2008    NCH          Initial
06-Aug-2009    SPC          Commented for making cache flush routine run from probe
****************************************************************************/
int ML_InstallCacheFlushApp(TyMLHandle *ptyHandle,
                            unsigned long ulAddress)
{
   int iErrRet = ERR_NO_ERROR;
   int bBackupOnlyFlushDataCache;

   // if not supported, then nothing to do..
   if (!ptyHandle->bCacheFlushSupported)
      return ERR_NO_ERROR;

   if (ulAddress != 0)
      {
      ptyHandle->tyMipsDW2App.ulCacheFlushStartAddress = ulAddress;
      }


   // If the cache flush routine start address is not yet setup then we will
   // disable cache flush. Pathfinder sends us an unaligned address initially
   // when the user has enabled cache debug but the address is not yet known,
   // typically the address becomes known when a .cso file is loaded.

   if (ptyHandle->tyMipsDW2App.ulCacheFlushStartAddress & 0x03)
      {
      ptyHandle->bCacheFlushEnabled = FALSE;
#ifdef USE_DIAGNOSTIC_REGISTERS
      ptyHandle->pulDiagnosticRegisters[0x05] = ptyHandle->bCacheFlushEnabled;
#endif
      return ERR_NO_ERROR;
      }

   ptyHandle->bCacheFlushEnabled = TRUE;

   DLOG3((LOGFILE,"\n\t\tML_InstallCacheFlushApp -> Installing FSHCACHE"));

   // Special Hack to ensure we don't jump to cache flush before cache flush enabled.
   // TODO: Make this a more permanent solution

   // Because write memory flushes cache if bOnlyFlushDataCache is enabled, 
   // we don't want this happening
   // while we are attempting to download the cache flush routine.
   // So 1. Backup flag   2. Set to False   3. Restore flag

   bBackupOnlyFlushDataCache = ptyHandle->bFlushOnlyDataCache;
   ptyHandle->bFlushOnlyDataCache = FALSE;

   // Next We install the cache flush routines
   // We must install each one in the specific endian mode
   //if (ptyHandle->bEndianModeModifiable)
   //{
   // todo: Not Implemented yet !!!
   //return ML_ERR_UNKNOWN_CACHE_ERROR;//todo error
   //}
   //else
   //{
   //DLOG((LOGFILE,"\nML_InstallCacheFlushApp: EndianMode is not modifiable"));
   DLOG((LOGFILE,"\nML_InstallCacheFlushApp: Loading Cache Flush Routine"));
   iErrRet=ML_InstallDW2Application(ptyHandle,
                                    ptyHandle->tyMipsDW2App.pulCacheFlushApp,
                                    ptyHandle->tyMipsDW2App.ulCacheFlushStartAddress,
                                    ptyHandle->tyMipsDW2App.ulCacheFlushInstructions*sizeof(unsigned long),
                                    FALSE,TRUE); // Don't backup main memory. Verify  Little Endian Version
   if (iErrRet != ERR_NO_ERROR)
      return iErrRet;
   //}

   // Restore flag before testing return code
   ptyHandle->bFlushOnlyDataCache = (unsigned char)bBackupOnlyFlushDataCache;
#if 0000
   if (iErrRet != ERR_NO_ERROR)
      return iErrRet;

   // Cache Flush Routine Successfully installed.
   if (ptyHandle->bUseOfTargetMemoryReadWriteAllowed)
      {
      // This feature is only enabled when Cache flush app is installed.
      ptyHandle->bUseTargetMemoryWhileWriting = ptyHandle->bWriteUsingTargetMemEnabled;
      ptyHandle->bUseTargetMemoryWhileReading = ptyHandle->bReadUsingTargetMemEnabled;
      }

   return 0;//todo ERR_NO_ERROR;
#endif
   return iErrRet;
}
#endif
/****************************************************************************
     Function: DL_CheckOpellaXDInstance
     Engineer: Nikolay Chokoev
        Input:   :
       Output: Error code
  Description: Establish a connection to the debugger
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
void ML_CheckOpellaXDInstance(TyMLHandle *ptyHandle)
{
   unsigned short usNumberOfDevices;
   char ppszLocDeviceNumbers[MAX_DEVICES_SUPPORTED][16];

   // if string is not empty, then exit (everything is fine)
   if (strcmp(ptyHandle->pszSerialNumber, ""))
      return;

   DL_OPXD_GetConnectedDevices(ppszLocDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
   if (usNumberOfDevices)
      {
      // so use first listed device
      strcpy(ptyHandle->pszSerialNumber, ppszLocDeviceNumbers[0]);
      }
}

/****************************************************************************
     Function: ML_OpenOpellaXD
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle to the device
               tyCallback: callback function
       Output: Error code
  Description: Establish a connection to the debugger
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
int ML_OpenOpellaXD(TyMLHandle *ptyHandle,
                    TyOpenOpellaXDCallback tyCallback)
{
   char pszLocDiskwareName[_MAX_PATH];
   char pszLocFpgawareName[_MAX_PATH];
   int iResult = 0; //todo error code

   // get full path to Opella-XD diskware and FPGAware for MIPS
   strcpy(pszLocDiskwareName, pszGlobalDllPath);
#ifdef __LINUX
   strcat(pszLocDiskwareName, "/");
#else
   strcat(pszLocDiskwareName, "\\");
#endif
   strcat(pszLocDiskwareName, OPXD_DISKWARE_MIPS_FILENAME);
   // create full fpgaware filename
   strcpy(pszLocFpgawareName, pszGlobalDllPath);
#ifdef __LINUX
   strcat(pszLocFpgawareName, "/");
#else
   strcat(pszLocFpgawareName, "\\");
#endif
   strcat(pszLocFpgawareName, OPXD_FPGAWARE_MIPS_FILENAME);

   if (tyCallback != NULL)
      tyCallback(0);
   switch (DL_OPXD_OpenDevice(ptyHandle->pszSerialNumber, 
                              OPELLAXD_USB_PID, false, &(ptyHandle->iDLHandle)))
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         ptyHandle->iDLHandle = MAX_DEVICES_SUPPORTED;
         return ERR_NO_USB_DEVICE_CONNECTED;
      default:
         ptyHandle->iDLHandle = MAX_DEVICES_SUPPORTED;
         return ERR_OPELLA_FAILURE;
      }
   (void)DL_OPXD_UnconfigureDevice(ptyHandle->iDLHandle);
   if (DL_OPXD_ConfigureDevice(ptyHandle->iDLHandle, DW_MIPS, FPGA_MIPS, pszLocDiskwareName, pszLocFpgawareName) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_OPELLA_FAILURE;

   return iResult;
}

/****************************************************************************
     Function: ML_UpdateOpellaXDFirmware
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handled
               tyCallback: callback
       Output: Error code
  Description: Update the opella-Xd firmware if it is needed
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
int ML_UpdateOpellaXDFirmware(TyMLHandle *ptyHandle,
                              TyOpenOpellaXDCallback tyCallback)
{
   int iFwDiff;
   char pszLocFirmwareName[_MAX_PATH];
   unsigned char bReconnect = 0;
   TyError tyResult = ERR_NO_ERROR;

   // get full path to Opella-XD firmware
   strcpy(pszLocFirmwareName, pszGlobalDllPath);
#ifdef __LINUX
   strcat(pszLocFirmwareName, "/");
#else
   strcat(pszLocFirmwareName, "\\");
#endif
   strcat(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);

   if (tyCallback != NULL)
      tyCallback(0);
   switch (DL_OPXD_OpenDevice(ptyHandle->pszSerialNumber, 
                              OPELLAXD_USB_PID, false, &(ptyHandle->iDLHandle)))
      {
      case DRVOPXD_ERROR_NO_ERROR:
         break;
      case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
         return ERR_NO_USB_DEVICE_CONNECTED;
      case DRVOPXD_ERROR_DEVICE_ALREADY_IN_USE:
         return ERR_OPXD_ALREADY_IN_USE;
      default:
         return ERR_OPELLA_FAILURE;
      }
   if (tyCallback != NULL)
      tyCallback(5);
   (void)DL_OPXD_UnconfigureDevice(ptyHandle->iDLHandle);
   if (tyCallback != NULL)
      tyCallback(10);
   // try to upgrade firmware
   if (DL_OPXD_UpgradeDevice(ptyHandle->iDLHandle, UPG_NEWER_VERSION, pszLocFirmwareName, &bReconnect, &iFwDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(ptyHandle->iDLHandle);
      if (tyCallback != NULL)
         tyCallback(100);
      return ERR_OPELLA_FAILURE;
      }

   if (bReconnect)
      {
      unsigned char ucRestartCnt;

      (void)DL_OPXD_CloseDevice(ptyHandle->iDLHandle);
      for (ucRestartCnt=0; ucRestartCnt<8; ucRestartCnt++)
         {
         if (tyCallback != NULL)
            tyCallback((unsigned char)(15 + ucRestartCnt*10));
         ML_Delay(1000);
         }
      if ((DL_OPXD_OpenDevice(ptyHandle->pszSerialNumber, 
                              OPELLAXD_USB_PID, false, &(ptyHandle->iDLHandle)) != DRVOPXD_ERROR_NO_ERROR) ||
          (DL_OPXD_UpgradeDevice(ptyHandle->iDLHandle, UPG_CHECK_ONLY, pszLocFirmwareName, &bReconnect, &iFwDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR) || 
          (iFwDiff != 0))
         {
         tyResult = ERR_OPELLA_FAILURE;
         }
      }
   else
      {
      if (tyCallback != NULL)
         tyCallback(95);
      }
   // close handle
   (void)DL_OPXD_CloseDevice(ptyHandle->iDLHandle);
   if (tyCallback != NULL)
      tyCallback(100);

   return tyResult;
}

/****************************************************************************
     Function: ML_InitConnection
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handle
       Output: Error code
  Description: Establish a connection to the debugger
Date           Initials    Description
07-Jul-2008    NCH          Initial
****************************************************************************/
int ML_InitConnection(TyMLHandle *ptyHandle)
{
   TyDeviceInfo tyLocDeviceInfo;

   int ErrRet = ERR_NO_ERROR;

   DLOG((LOGFILE,"\n\t\tML_InitConnection -> Called"));

   // first verify Opella-XD has latest firmware
   ML_CheckOpellaXDInstance(ptyHandle);

// #if GDBSERVERDLL
//    printf("Checking Opella-XD firmware .....");
// #else
//    // Create the progress bar...
//    pOpenUSBProgress = new CProgressControl;
//    (void)pOpenUSBProgress->ShowWindow(SW_SHOW);
//    ((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 100);                  // initialize progress bar
//    pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Updating Opella-XD firmware .....");
// #endif
//
//    OpenUsbDeviceMoveProgress(0);
//    ErrRet = ML_UpdateOpellaXDFirmware(ptyHandle,
// 									  OpenUsbDeviceMoveProgress);
   ErrRet = ML_UpdateOpellaXDFirmware(ptyHandle, NULL);

// #if GDBSERVERDLL
//    printf("\n");
// #else
//    //Close progress bar
//    (void)pOpenUSBProgress->ShowWindow(SW_HIDE);
//    delete pOpenUSBProgress;
// #endif

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   // now configure Opella-XD
// #if GDBSERVERDLL
//    printf("Configuring Opella-XD .....");
// #else
//    // Create the progress bar...
//    pOpenUSBProgress = new CProgressControl;
//    (void)pOpenUSBProgress->ShowWindow(SW_SHOW);
//    ((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 100);                  // initialize progress bar
//    pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Configuring Opella-XD .....");
// #endif
//    OpenUsbDeviceMoveProgress(0);
//    ErrRet = ML_OpenOpellaXD(ptyHandle,
// 							OpenUsbDeviceMoveProgress

   ErrRet = ML_OpenOpellaXD(ptyHandle, NULL);

//    OpenUsbDeviceMoveProgress(100);
//
// #if GDBSERVERDLL
//    printf("\n");
// #else
//    //Close progress bar
//    (void)pOpenUSBProgress->ShowWindow(SW_HIDE);
//    delete pOpenUSBProgress;
// #endif

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   // Get Device Details
   ErrRet = DL_OPXD_GetDeviceInfo(ptyHandle->iDLHandle, &tyLocDeviceInfo);

   if (ErrRet != ERR_NO_ERROR)
      return ML_ConvertDLError(ErrRet);;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetDllPath
     Engineer: Nikolay Chokoev
        Input: const char *pszDllPath - DLL full path
       Output: void
  Description: extract path to DLL folder and store into global variable
               (to be used with ML_CreateFullFilename and others)
Date           Initials    Description
08-Jul-2008    NCH          Initial
****************************************************************************/
void ML_SetDllPath(const char *pszDllPath)
{
   char *pszPtr;

   pszGlobalDllPath[0] = 0;
   // copy string to buffer and convert to lower case
   strncpy(pszGlobalDllPath, pszDllPath, DLLPATH_LENGTH - 1);
   pszGlobalDllPath[DLLPATH_LENGTH - 1] = 0;

#ifdef __LINUX
   // try to find .so extension
   pszPtr = strstr(pszGlobalDllPath,".so");
   if (pszPtr != NULL)
      {     // found .so so go backwards to nearest '/' (or to start of string)
      *pszPtr=0;
      while ((*pszPtr != '/') && (pszPtr > pszGlobalDllPath))
         pszPtr--;
      *pszPtr = 0;   // cut string (remove dll name+extension and \ character
      }
#else
   StringToLwr(pszGlobalDllPath);
   // try to find .dll extension
   pszPtr = strstr(pszGlobalDllPath,".dll");
   if (pszPtr != NULL)
      {     // found .dll so go backwards to nearest '\\' (or to start of string)
      *pszPtr=0;
      while ((*pszPtr != '\\') && (pszPtr > pszGlobalDllPath))
         pszPtr--;
      *pszPtr = 0;   // cut string (remove dll name+extension and \ character
      }
#endif
   else
      pszGlobalDllPath[0] = 0;   // wrong format, so use empty string
}

/****************************************************************************
     Function: ML_DetermineScanChainInfo
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle to device
               tyUserSettings : user settings
       Output: none
  Description: Determine information about the current scan chain and specific
               information about a two devices on the chain.
Date           Initials    Description
10-Jul-2008    NCH          Initial
20-Nov-2008    SPC          Fixed Multicore information updation.
****************************************************************************/
void ML_DetermineScanChainInfo(TyMLHandle       *ptyHandle,
                               const TyUserSettings *tyUserSettings)
{

   DLOG2((LOGFILE,"\n\t\tDL_DetermineScanChainInfo -> Called"));
   ptyHandle->ulCoreSelectIRLength = tyUserSettings->ulCoreSelectIRLength;
   ptyHandle->ulCoreSelectCode = tyUserSettings->ulCoreSelectCode;
   if (tyUserSettings->ucMultiCoreCoreNum)
      {
      ptyHandle->ulMipsCore = tyUserSettings->ucMultiCoreCoreNum -1;
      }
   else
      {
      ptyHandle->ulMipsCore = tyUserSettings->ucMultiCoreCoreNum;
      }
   if (tyUserSettings->ucMultiCoreDMACoreNum)
      {
      ptyHandle->ulDmaCore  = tyUserSettings->ucMultiCoreDMACoreNum -1;
      }
   else
      {
      ptyHandle->ulDmaCore  = tyUserSettings->ucMultiCoreDMACoreNum;
      }

   // Number of cores
   ptyHandle->ulNumberOfCoresOnScanchain = (unsigned char)tyUserSettings->ucMultiCoreNumCores;
   // Debug Core IR Length
   ptyHandle->ulMipsCoreEjtagIRLength = (unsigned char)tyUserSettings->tyMultiCoreDeviceInfo[ptyHandle->ulMipsCore].ulIRLength;
   // DMA Core IR Length
   ptyHandle->ulDmaCoreEjtagIRLength  = (unsigned char)tyUserSettings->tyMultiCoreDeviceInfo[ptyHandle->ulDmaCore].ulIRLength;

   DLOG2((LOGFILE,"\n"));
   DLOG2((LOGFILE,"\nMulticore Information"));                                                   
   DLOG2((LOGFILE,"\n**********************************************************************"));
   DLOG2((LOGFILE,"\nptyHandle->ulMipsCore                        %d",ptyHandle->ulMipsCore));                                                   
   DLOG2((LOGFILE,"\nptyHandle->ulDmaCore                         %d",ptyHandle->ulDmaCore));                                                   
   DLOG2((LOGFILE,"\nptyHandle->ulMipsCoreEjtagIRLength           %d",ptyHandle->ulMipsCoreEjtagIRLength));                                                   
   DLOG2((LOGFILE,"\nptyHandle->ulDmaCoreEjtagIRLength            %d",ptyHandle->ulDmaCoreEjtagIRLength));                                                   
   DLOG2((LOGFILE,"\n"));
   DLOG2((LOGFILE,"\n\t\tDL_DetermineScanChainInfo -> Complete"));

   return;
}

/****************************************************************************
     Function: ML_TestScanchain
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : target parameters/settings
              unsigned char bVerifyNumberOfCores - verify number of cores with MC settings
       Output: Error code
  Description: Initialise Debugger
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_TestScanchain(TyMLHandle *ptyHandle, 
                         unsigned char bVerifyNumberOfCores)
{
   unsigned long ulIndex;
   unsigned char bPatternMatch = 0;
   TyError iResult = ERR_NO_ERROR;
   int ErrRet;
   // check parameters
   if (ptyHandle == NULL)
      return ERR_UNKNOWN_ERROR;
   // testing scanchain is based on putting all cores into bypass mode and shifting different patterns via DR
   // all output patterns should correspond input patterns shifted by number of cores on scanchain (apply also if there is no core)
   //
   // 1st step - reset TAPs using TMS sequence
   if ((ErrRet = DL_OPXD_JtagResetTAP(ptyHandle->iDLHandle, 0)) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ML_ConvertDLError(ErrRet);
   // 2nd step - disable MC
   (void)DL_OPXD_JtagConfigMulticore(ptyHandle->iDLHandle, 0, NULL);
   // 3rd step - prepare patterns
   if (iResult == ERR_NO_ERROR)
      {
      TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
      unsigned long pulDRPatternOut1[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternOut2[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternOut3[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternOut4[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternIn1[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternIn2[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternIn3[MIPS_TSTSC_WORDS];
      unsigned long pulDRPatternIn4[MIPS_TSTSC_WORDS];
      // prepare test patterns
      for (ulIndex=0; ulIndex < MIPS_TSTSC_WORDS; ulIndex++)
         {
         pulDRPatternOut1[ulIndex] = 0xDB6DB6DB;   pulDRPatternOut2[ulIndex] = 0x24924924;
         pulDRPatternIn1[ulIndex] = 0x0;           pulDRPatternIn2[ulIndex] = 0x0;
         pulDRPatternOut3[ulIndex] = 0xAAAAAAAA;   pulDRPatternOut4[ulIndex] = 0x55555555;
         pulDRPatternIn3[ulIndex] = 0x0;           pulDRPatternIn4[ulIndex] = 0x0;
         }
      // do 4 scans (place all cores into bypass, shift 1st pattern into DR, shift 2nd pattern into DR and go back to bypass
      tyRes = DL_OPXD_JtagScanIR(ptyHandle->iDLHandle, 0, MIPS_TSTSC_BITS, 
                                 NULL, NULL);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_JtagScanDR(ptyHandle->iDLHandle, 0, MIPS_TSTSC_BITS, 
                                    pulDRPatternOut1, pulDRPatternIn1);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_JtagScanDR(ptyHandle->iDLHandle, 0, MIPS_TSTSC_BITS, 
                                    pulDRPatternOut2, pulDRPatternIn2);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_JtagScanDR(ptyHandle->iDLHandle, 0, MIPS_TSTSC_BITS, 
                                    pulDRPatternOut3, pulDRPatternIn3);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_JtagScanDR(ptyHandle->iDLHandle, 0, MIPS_TSTSC_BITS, 
                                    pulDRPatternOut4, pulDRPatternIn4);
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_JtagScanIR(ptyHandle->iDLHandle, 0, MIPS_TSTSC_BITS, 
                                    NULL, NULL);
      switch (tyRes)
         {
         case DRVOPXD_ERROR_NO_ERROR:        // no error, just go ahead
            break;
         default:                            // any error in driver
            iResult = ERR_UNKNOWN_ERROR;
            break;
         }
      if (iResult == ERR_NO_ERROR)
         {  // verify results
         unsigned long ulNumberOfBits = MIPS_TSTSC_BITS;
         // check if pattern matches (test up to supported number of cores)
         for (ulIndex=0; ulIndex < (MAX_DEVICE_IN_CHAIN+1); ulIndex++)
            {
            if (!CompareArrayOfWords(pulDRPatternOut1, pulDRPatternIn1, ulNumberOfBits) &&
                !CompareArrayOfWords(pulDRPatternOut2, pulDRPatternIn2, ulNumberOfBits) &&
                !CompareArrayOfWords(pulDRPatternOut3, pulDRPatternIn3, ulNumberOfBits) &&
                !CompareArrayOfWords(pulDRPatternOut4, pulDRPatternIn4, ulNumberOfBits)
               )
               {  // found pattern match
               bPatternMatch = 1;
               if (bVerifyNumberOfCores && (ulIndex > ptyHandle->ulNumberOfCoresOnScanchain))
                  bPatternMatch = 0;
               break;
               }
            ShiftArrayOfWordsRight(pulDRPatternIn1, MIPS_TSTSC_WORDS);
            ShiftArrayOfWordsRight(pulDRPatternIn2, MIPS_TSTSC_WORDS);
            ShiftArrayOfWordsRight(pulDRPatternIn3, MIPS_TSTSC_WORDS);
            ShiftArrayOfWordsRight(pulDRPatternIn4, MIPS_TSTSC_WORDS);
            ulNumberOfBits--;
            }
         }
      }
   // 5th step - restore MC settings
   if (ptyHandle->ulNumberOfCoresOnScanchain > 1)
      {  // more than 1 cores, we need to configure probe
      TyCoreConfig ptyScanchainConfig[MAX_DEVICE_IN_CHAIN];
      // create structure with core information
      for (ulIndex=0; ulIndex < ptyHandle->ulNumberOfCoresOnScanchain; ulIndex++)
         {
         ptyScanchainConfig[ulIndex].usDefaultIRLength = 
         (unsigned short)ptyHandle->tyMultiCoreDeviceInfo[ulIndex].ulIRLength;
         ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;   // does not matter
         ptyScanchainConfig[ulIndex].pulBypassIRPattern = 
         &ptyHandle->tyMultiCoreDeviceInfo[ulIndex].ulBypassCode;
         }
      (void)DL_OPXD_JtagConfigMulticore(ptyHandle->iDLHandle, 
                                        ptyHandle->ulNumberOfCoresOnScanchain, 
                                        ptyScanchainConfig);
      }
   else
      (void)DL_OPXD_JtagConfigMulticore(ptyHandle->iDLHandle, 0, NULL);
   // 6th step - reset TAPs using TMS sequence (always)
   (void)DL_OPXD_JtagResetTAP(ptyHandle->iDLHandle, 0);

   if ((iResult == ERR_NO_ERROR) && !bPatternMatch)
      iResult = ERR_UNKNOWN_ERROR; //todo error test failed

   return iResult;
}

/****************************************************************************
     Function: ML_CheckForTarget
     Engineer: Nikolay Chokoev
        Input: ptyHandle : Device handle
       Output: Error code
  Description: Initialise Debugger
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_CheckForTarget (TyMLHandle *ptyHandle)
{
   DLOG2((LOGFILE,"\n\t\tML_CheckForTarget -> Called"));

   if (ML_TestScanchain(ptyHandle, 1) != ERR_NO_ERROR)
      {
      return ERR_MLCHECKTARGT_NO_TARGET;
      }

   DLOG3((LOGFILE,"\n\t\tML_CheckForTarget -> Target Detected"));

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_InitialiseDebugger
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handle
               tyUserSettings : User configured settings
       Output: Error code
  Description: Initialise Debugger
Date           Initials    Description
10-Jul-2008    NCH          Initial
20-Nov-2008    SPC          Device Enable password support added
19-Dec-2008    RS           Microchip support added
****************************************************************************/
TyError ML_InitialiseDebugger (TyMLHandle *ptyHandle,
                               TyUserSettings tyUserSettings)

{
   TyError ErrRet;
   DLOG2((LOGFILE,"\n\t\tML_InitialiseDebugger -> Called"));

   // Opella-XD target voltage and speed settings
   double dTargetVoltage = 3.3;                    // 3.3V as default setting
   if (ptyHandle->usTargetSupply == (unsigned short)Match_Target)
      {  // set target voltage tracking in the probe
      ErrRet = DL_OPXD_SetVtpaVoltage(ptyHandle->iDLHandle, dTargetVoltage, 1, 1);    // track target voltage, 3.3V as default value
      ErrRet = ML_ConvertDLError(ErrRet);
      }
   else
      {
      ErrRet = DL_OPXD_SetVtpaVoltage(ptyHandle->iDLHandle, dTargetVoltage, 0, 1);    // use fixed 3.3V target voltage
      ErrRet = ML_ConvertDLError(ErrRet);
      }
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;
   ML_Delay(600);                                                                // wait some time to stabilize voltage

   // Slow JTAG is enabled for PR1910 QuickTurn only
   ptyHandle->bSlowJTAGEnabled = FALSE;
   if (tyUserSettings.bUseSlowJTAG)
      {
      DLOG2((LOGFILE,"\n\t\t Quickturn Device Selected. Using Slow JTAG MODE"));
      ptyHandle->bSlowJTAGEnabled = TRUE;
      }
   else
      {
      DLOG2((LOGFILE,"\n\t\t Normal Device Selected. Using Normal JTAG MODE"));
      ptyHandle->bSlowJTAGEnabled = FALSE;
      }

   if (ptyHandle->bSlowJTAGEnabled)
      ptyHandle->uiMaxDW2Loops = MAX_DW2_LOOPS_SLOW_JTAG;
   else
      ptyHandle->uiMaxDW2Loops = MAX_DW2_LOOPS_NORMAL_JTAG;

   ErrRet = ML_WriteOpellaXDTargetConfig(ptyHandle, &tyUserSettings);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   if (DL_OPXD_Mips_InitialiseDebugger(ptyHandle->iDLHandle, ptyHandle->dJtagFreq) != DRVOPXD_ERROR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger : Failed to See Opella"));
      return ERR_FAILED_TO_SEE_OPELLA_USB;
      }

   //for the limerick device, user may not have access to ETAP due to bad programming of configuration bits,
   //can recover by erasing flash via MTAP, exiting out of MdiOpen and getting Pathfinder to prompt user to 
   //close Pathfinder, power off board and reconnect Opella and repower on baord.
   if (tyUserSettings.bMchpRecoveryByChipErase)
      {
      ErrRet = MLN_MchpRecoveryByChipErase();
      if (ErrRet != ERR_NO_ERROR)
         {
         DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger : MLN_MchpRecoveryByChipErase Failed to recover by Chip erase"));
         }
      else
         {
         DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger : MLN_MchpRecoveryByChipErase performed Chip Erase"));
         }
      return ErrRet;  //we exit out of the recovery mode at this stage once flash is erased to allow user to repower system
      }

   if (tyUserSettings.bCoreSelectBeforeReset)
      {
      DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger: DL_EnableCoreSelect-> Called"));
      ML_EnableCoreSelect(ptyHandle);
      }

   // Now reset the TAP Controller
   //   (void)DL_OPXD_JtagResetTAP(ptyHandle->iDLHandle, FALSE);

   // Note: We must wait a bit after reset to ensure that device has finished it's reset.
   if (ptyHandle->bSlowJTAGEnabled)
      ML_Delay(50);

   //todo: multicore #if MAX_SUPPORTED_BIT_LENGTH > 128
   //todo: multicore     if (TargetConfig.ucEJTAGSecSequence == YODA_SECURITY_SEQUENCE)
   //todo: multicore         DL_ProgramYodaTCB(); // Yoda needs the TCB programmed after a TAP reset
   //todo: multicore #endif


   // Enable core with password support eg: PNX 85500
   // TODO: We have to verify the correct location of this operation :SPC
   /*if ( 0 != (strcmp(tyUserSettings.szDeviceEnablePasswdFile,"")) ) {
       (void)ML_EnableTargetPasswd(ptyHandle,tyUserSettings.szDeviceEnablePasswdFile);
       }*/
   if ( 0 != (strcmp(tyUserSettings.szPasswordSequence,"")) )
      {
      (void)ML_EnableTargetPasswdSetting(ptyHandle,&tyUserSettings);
      }

   // On these types of processors we can't check for the target as 
   // EJTAG isn't enabled yet.
   ErrRet = ML_CheckForTarget(ptyHandle);

   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger : Failed to See Target"));
      return ErrRet;
      }

   // Next We determine the Target Configuration type
   ErrRet = ML_GetBasicProcessorSettings(ptyHandle, &tyUserSettings);

   return ErrRet;
}

/****************************************************************************
     Function: ML_InitialiseDebugger_Ex
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handle
               TyUserSettings tyUserSettings : user settings
             bool bSpecifiedProcessor      : FALSE for diag. tools
       Output: Error code
  Description: extended version of ML_InitialiseDebugger supporting JTAG
               diagnostic tools
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_InitialiseDebugger_Ex (TyMLHandle *ptyHandle,
                                  TyUserSettings tyUserSettings, 
                                  int bSpecifiedProcessor)

{
   TyError ErrRet = ERR_NO_ERROR;

   DLOG2((LOGFILE,"\n\t\tML_InitialiseDebugger_Ex -> Called"));

   // just call ML_InitialiseDebugger when processor is specified
   if (bSpecifiedProcessor)
      return ML_InitialiseDebugger(ptyHandle,tyUserSettings);

   // Opella-XD target voltage and speed settings
   double dTargetVoltage = 3.3;                    // 3.3V as default setting

   ErrRet = DL_OPXD_SetVtpaVoltage(ptyHandle->iDLHandle, dTargetVoltage, 0, 1);          // use fixed 3.3V target voltage
   ErrRet = ML_ConvertDLError(ErrRet);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;


   ptyHandle->ulMipsCoreEjtagDataLength = 0x20; 

//   TargetConfig.DWInfo     = tyUserSettings.DWInfo;

   DLOG2((LOGFILE,"\n\t\t Normal Device Selected. Using Normal JTAG MODE"));
   ptyHandle->bSlowJTAGEnabled = FALSE;
   ptyHandle->uiMaxDW2Loops = MAX_DW2_LOOPS_NORMAL_JTAG;

   ErrRet = ML_WriteOpellaXDTargetConfig(ptyHandle,&tyUserSettings);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;


   if (DL_OPXD_Mips_Initialise_Debugger_Ex(ptyHandle->iDLHandle,
                                           bSpecifiedProcessor,
                                           ptyHandle->dJtagFreq) != DRVOPXD_ERROR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger_Ex : Failed to See Opella"));
      return ERR_FAILED_TO_SEE_OPELLA_USB;
      }

   //for the limerick device we call EnableCoreSelectReset to enable the EJTAG TAP. This must be done
   //before processor reset (pin 11)
   if (tyUserSettings.bCoreSelectBeforeReset)
      {
      DLOG((LOGFILE,"\n\t\tML_InitialiseDebugger_Ex: DL_EnableCoreSelect -> Called"));
      ML_EnableCoreSelect(ptyHandle);
      }

   return ErrRet;
}

/****************************************************************************
     Function: ML_GetStatus
     Engineer: Nikolay Chokoev
        Input: ulMode : Reset Mode
               tyUserSettings : User configured settings
       Output: Error code
  Description: Reset as per ulMode
               For ulMode==ModeFullReset this is:
               Initialise Debugger and check for target
               Reset Processor
               Halt Processor in debug mode
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_GetStatus(TyMLHandle *ptyHandle,
                     TyStatus * tyStatus,
                     int bUseTargetMemoryIfNeeded)
{
   int bContextSaved = FALSE;
   TyError ErrRet = ERR_NO_ERROR;

   CONTROL_REG tyLocControl;
   CONTROL_REG tyControlOutput;
   static DEBUG_REG   drLocDebug; // Must remember the cause of break


   DLOG2((LOGFILE,"\n\t\tML_GetStatus -> Called"));

   DLOG3((LOGFILE,"\n\t\tML_GetStatus -> ptyHandle->tyTraceSettings.tyTraceType = %d",ptyHandle->tyTraceSettings.tyTraceType));

   tyStatus->bNoClockPresent        = FALSE; // Important to initialise this variable
   tyStatus->bReducedPowerMode      = FALSE; // Important to initialise this variable
   tyStatus->bExternalResetDetected = FALSE;

   tyControlOutput.ulData = 0x0;   // Set to 0
   tyControlOutput.Bits.PrAcc = 1; // Then set PrAcc bit to one to ensure access is serviced before finishing

   if (ptyHandle->bProbeTrapFlagExists)
      tyControlOutput.Bits.ProbTrap = 1; // Using EJTAG Memory, Inverse meaning on other versions
   tyControlOutput.Bits.ProbEn   = 1; // Probe will service Processor Accesses

   tyControlOutput.Bits.ClkEn = ptyHandle->ucDefaultValueOfClkEnBit;     // Enable DCLK during Trace

   // Read back the control Register
   (void)ML_ScanIRandDR(ptyHandle, 
                        IR_LENGTH, 
                        LtoP(CONTROL), 
                        32, 
                        &tyControlOutput.ulData, 
                        &tyLocControl.ulData);

   DLOG3((LOGFILE,"\nControl = %08X",tyLocControl.ulData));

   if (tyLocControl.Bits.Rocc == 1)
      {
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> Reset Has Occurred since last control read"));
      if (!ptyHandle->bNormalBootSelected)
         ASSERT_NOW();   // An external reset has happened but we haven't enabled Go from Insertion
      // Reset has occurred since last Control Read
      // Doing Nothing about this for now.
      // TODO: Decide what to do in this event
      tyStatus->bExternalResetDetected = TRUE;
      }

   tyStatus->bNoClockPresent = FALSE;
// GO_FROM_INSERTION_SUPPORT
   if (tyLocControl.Bits.Rocc == 1)
      {
      if (tyLocControl.Bits.EJTAGBrk && !tyLocControl.Bits.DM)
         {
         // A reset has occured since the last control read and a Debug Interupt exception is pending
         // but we are not yet in debug mode. Strange!
         // Could it be that the clock has disappeared??????
         tyStatus->bNoClockPresent = TRUE;
         if (tyLocControl.Bits.DM == 1)
            {
            ASSERT_NOW();  // This cannot happen
            }
         else
            {
            tyStatus->bInDebugMode = FALSE;
            }
         }
      }

// Support for Powerdown mode
   if (tyLocControl.Bits.Doze == 1)
      {
      // We are in Reduced power mode.
      tyStatus->bReducedPowerMode = TRUE;
      if (tyLocControl.Bits.DM == 1)
         tyStatus->bInDebugMode= TRUE;
      else
         tyStatus->bInDebugMode= FALSE;
      return ERR_NO_ERROR;
      }

   if (tyLocControl.Bits.DM == 1)
      {
      DLOG((LOGFILE,"\nCurrently In Debug Mode"));
      // Currently in debug mode
      tyStatus->bInDebugMode= TRUE;
      // Since we have broken from execution
      // we must save the status of processor registers
      // We must be very careful that no other operation happens on the processor until Status has been saved
      // Also we must ensure that we don't save the context a second time without having gone into 
      // emulation again


      // If this is the first time we checked status since going into emulation
      // Then we need to save the context.
      if (ptyHandle->bProcessorExecuting)
         {
         // Get current processor status
         ErrRet = ML_SaveContext(ptyHandle, bUseTargetMemoryIfNeeded);
         if (ErrRet != ERR_NO_ERROR)
            return ErrRet;
         bContextSaved = TRUE;
         // After Break We need to discover why we broke
         ErrRet = MLR_ReadRegister(ptyHandle,
                                   ptyHandle->ulCP0RegNumberforDebug,
                                   MDIMIPCP0,
                                   &drLocDebug.ulData);
         if (ErrRet != ERR_NO_ERROR)
            return ErrRet;
         (void)ML_SetBootType(ptyHandle, FALSE);
         // Enable memory writing if necessary
         ML_EnableMemoryWriting(ptyHandle, FALSE, TRUE, FALSE);
         }
      else
         {
         // Otherwise we are still in Debug mode and nothing has changed.
         DLOG3((LOGFILE,"\n\t\tML_GetStatus -> Still In Debug Mode"));
         }      
      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> Processor Still Execution"));

      // We Are not in Debug Mode.
      if (ptyHandle->bProcessorExecuting)
         {
         // We have told the processor to start code execution
         // thus we are not surprised to find that we are executing
         tyStatus->bInDebugMode= FALSE;
         }
      else
         {
         tyStatus->bInDebugMode= FALSE;
         if (!tyStatus->bNoClockPresent)
            {
            // We haven't told the processor to execute code.
            // Therefore it is very strange to find that we are now executing.
            // The only likely explaination is that a target reset occurred.
            // We don't like people doing this to us while we are in debug mode 
            // so we will return a fatal error and force a target reset.
            DLOG((LOGFILE,"\n\t\tML_GetStatus -> We Never Told target to start Execution: TARGET_RESET_OCCURRED"));
            return ERR_MLGETSTAT_TARGET_RESET_OCCURRED;
            }
         }
      }

   // Now We must determine the Cause of Break

   tyStatus->ulBPId = 0;

   if (drLocDebug.Bits.DSS == 1)
      {
      // Cause of Break due to Single Step
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: SINGLE_STEP"));
      tyStatus->cobCause = COB_SINGLE_STEP;
      return ErrRet;
      }

   if (drLocDebug.Bits.DINT == 1)
      {
      // Cause of Break due to Debug interrupt
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: USER_HALT"));
      tyStatus->cobCause = COB_USER_HALT;
      return ErrRet;
      }


   if (drLocDebug.Bits.DBp == 1)
      {
      // Cause of Break due to Debug Instruction
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: BP_OPCODE"));
      tyStatus->cobCause = COB_BP_OPCODE;
      return ErrRet;
      }


   if (drLocDebug.Bits.DIB == 1)
      {
      // Cause of Break due to Instruction Hardware Breakpoint
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: BP_HARDWARE"));
      tyStatus->cobCause = COB_BP_HARDWARE;
      tyStatus->ulBPId = MLB_GetBPIdForComplexBreak(ptyHandle,
                                                    TRUE, 
                                                    ptyHandle->ulInstBrkStatus);
      return ErrRet;
      }


   if (drLocDebug.Bits.DDBL == 1)
      {
      // Cause of Break due to Data Load Hardware Breakpoint
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: BP_DATA_RD"));
      tyStatus->cobCause = COB_BP_DATA_RD;
      tyStatus->ulBPId = MLB_GetBPIdForComplexBreak(ptyHandle,
                                                    FALSE, 
                                                    ptyHandle->ulDataBrkStatus);
      return ErrRet;
      }

   if (drLocDebug.Bits.DDBS == 1)
      {
      // Cause of Break due to Data Store Hardware Breakpoint
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: BP_DATA_WR"));
      tyStatus->cobCause = COB_BP_DATA_WR;
      tyStatus->ulBPId = MLB_GetBPIdForComplexBreak(ptyHandle,
                                                    FALSE, 
                                                    ptyHandle->ulDataBrkStatus);
      return ErrRet;
      }

   if (ptyHandle->bUseInstrWatchForBP && (drLocDebug.Bits.DExcCode == 0x17))
      {
      unsigned long ulPCAddress, ulBPIdentifier;
      // Cause of Break due to Debug Watchpoint Exception
      // setting BP_WATCHPOINT_MISS only when context has been just saved (processor was running)
      DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: BP_WATCHPOINT_MATCH or BP_WATCHPOINT_MISS"));
      (void)MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulPCAddress);
      if (!MLB_CheckWatchpointBPAddress(ptyHandle,ulPCAddress, &ulBPIdentifier) && bContextSaved)
         {
         // we hit other address in watchpoint block, need to re-execute
         tyStatus->cobCause = COB_BP_WATCHPOINT_MISS;
         }
      else
         {
         tyStatus->cobCause = COB_BP_WATCHPOINT_MATCH;
         tyStatus->ulBPId = ulBPIdentifier;
         }
      return ErrRet;
      }

   DLOG3((LOGFILE,"\n\t\tML_GetStatus -> COB: UNKNOWN"));

   tyStatus->cobCause = COB_UNKNOWN;
   return ErrRet;
}

/****************************************************************************
     Function: ML_DetermineTypeOfOperation
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: Type of Operation
  Description: Reads the Control Register and determines what the processor would
               like to do next.
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
TyTypeOfOperation ML_DetermineTypeOfOperation (TyMLHandle *ptyHandle)
{

   CONTROL_REG tyLocControl;
   CONTROL_REG tyControlOutput;

   tyControlOutput.ulData = 0x0;   // Set to 0
   tyControlOutput.Bits.PrAcc = 1; // Then set PrAcc bit to one to ensure access is serviced before finishing
   tyControlOutput.Bits.ProbEn   = 1; // Probe will service Processor Accesses

   tyControlOutput.Bits.ClkEn = ptyHandle->ucDefaultValueOfClkEnBit;  // Always Enable Trace CLOCK

   if (ptyHandle->bProbeTrapFlagExists)
      tyControlOutput.Bits.ProbTrap = 1; // Using EJTAG Memory, Inverse meaning on other versions
   // First Read back the control
   (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle,            //tyDevHandle
                                 ptyHandle->ulMipsCore,          //ulCore
                                 ptyHandle->ulMipsCoreEjtagIRLength,//ulIRLength =>IR_LENGTH
                                 LtoP(CONTROL),                  //*pulDataInForIR
                                 32,                       //ulDRLength
                                 &tyControlOutput.ulData,        //*pulDataInForDR
                                 &tyLocControl.ulData);          //*pulDataOutFromDR
#ifdef DEBUG_DETERMINE_TYPE_OF_OPERATION
   DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
#endif

   // First Check for Anomolies
   if (tyLocControl.Bits.Rocc == 1)
      {
      // Reset Occurred
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Reset Occurred"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      return(RESETOCCURRED);
      }

   if (tyLocControl.Bits.DM == 0)
      {
      // Not In Debug Mode
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Not in Debug Mode"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      return(NOTINDEBUGMODE);
      }

   if (tyLocControl.Bits.Doze == 1)
      {
      // Low Power Mode Detected
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> LowPowerDetected"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      return(LOWPOWERDETECTED);
      }

   if (tyLocControl.Bits.Halt == ptyHandle->bLevelWhichIndicatesProcessorClockHalted )
      {
      // System Internal Clock has Stopped
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Internal Clock Stopped"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      return(HALTDETECTED);
      }

   if (tyLocControl.Bits.PerRst == 1)
      {
      // Peripheral Reset Occurred
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Peripheral Reset Occurred"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      return(PERIPHERALRESETOCCURRED);
      }


   if (tyLocControl.Bits.PrAcc == 0)
      {
      // No Processor Access is Pending
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> No Access Pending"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      return(NOACCESSPENDING);
      }

   if (tyLocControl.Bits.ProbEn == 0)
      {
      // Probe does not service Processor Accesses
      // MISSED A CYCLE THIS SHOULD NOT HAPPEN
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Probe Not Enabled"));
      DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
      ASSERT_NOW();
      return(PROBENOTENABLED); // DN Uncommented 9-aug-10
      }

   if (ptyHandle->bProbeTrapFlagExists) // EJTAG 1.53 Does not have a probTrap Flag
      {
      if (tyLocControl.Bits.ProbTrap == 0)
         {
         // Trap From Normal Memory
         DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Trap from normal memory"));
         DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Control = %08X",tyLocControl.ulData));
         ASSERT_NOW();
         return(TRAPFROMNORMALMEMORY); // DN Uncommented 9-aug-10
         }
      }


   // Now we check what type of Processor Access is Needed.
   if (tyLocControl.Bits.PRnW == 0)
      {
      // Read or Fetch
      if (ptyHandle->bDszInsteadOfPsz)
         {
         // EJTAG 1.53 used Data Transfer Size Instead of PSZ
         if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x0))
            // Byte Access
            {
            if (ptyHandle->bAssumeWordAccess)
               return READWORD;
            else
               return READBYTE;
            }
         else
            if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x0))
               // HalfWord Access
               return(READHALFWORD);
            else
               if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x1))
                  // Word Access
                  return(READWORD);
               else
                  if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x1))
                     // Triple Access
                     return(READTRIPLE);
                  else
                     {
                     DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Invalid Case"));
                     ASSERT_NOW();
                     return(INVALIDSITUATION);
                     }

         }
      else
         {
         switch (tyLocControl.Bits.Psz)
            {
            case 0:
               // Byte Access
               return(READBYTE);
            case 1:
               // HalfWord Access
               return(READHALFWORD);
            case 2:
               // Word Access
               return(READWORD);
            case 3:
               // Triple Access
               return(READTRIPLE);
            default:
               DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Invalid Case"));
               ASSERT_NOW();
               return(INVALIDSITUATION);
            }
         }

      }
   else
      {
      // Write Pending


      if (ptyHandle->bDszInsteadOfPsz)
         {
         // EJTAG 1.53 used Data Transfer Size Instead of PSZ
         if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x0))
            {
            // Byte Access
            if (ptyHandle->bAssumeWordAccess)
               return WRITEWORD;
            else
               return WRITEBYTE;
            }
         else

            if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x0))
               // HalfWord Access
               return(WRITEHALFWORD);
            else
               if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x1))
                  // Word Access
                  return(WRITEWORD);
               else
                  if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x1))
                     // Triple Access
                     return(WRITETRIPLE);
                  else
                     {
                     DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Invalid Case"));
                     ASSERT_NOW();
                     return(INVALIDSITUATION);
                     }
         }
      else
         {
         switch (tyLocControl.Bits.Psz)
            {
            case 0:
               // Byte Access
               if (ptyHandle->bAssumeWordAccess)
                  return WRITEWORD;
               else
                  return WRITEBYTE;
            case 1:
               // HalfWord Access
               return(WRITEHALFWORD);
            case 2:
               // Word Access
               return(WRITEWORD);
            case 3:
               // Triple Access
               return(WRITETRIPLE);
            default:
               // This should never Happen
               DLOG3((LOGFILE,"\n\t\t\tML_DetermineTypeOfOperation -> Invalid Case"));
               ASSERT_NOW();
               return(INVALIDSITUATION);
            }
         }
      }
}

/****************************************************************************
     Function: ML_ProgramLecoTCB
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: Special LECO Support
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
void ML_ProgramLecoTCB(void)
{
// Note: This function is compiled into both Opella.DLL and Opella USB diskware
//       We only want the Opella.DLL version of it to run.
#if 0000 //todo is it only for OpellaUSB???
   unsigned long  ulData            = 0x00;
   unsigned char  bEnableBackup     = MltiInfo.bMultiCoreSystem;
   unsigned long  ulIRRegister      = 0x11;    // (TCB PROGRAM) 
   unsigned char  ProgramSequenceOut[18];
   unsigned char  ProgramSequenceIn [18];

   (void)DL_PulseTRST();

   memset(ProgramSequenceOut,0x0,sizeof(ProgramSequenceOut));
   memset(ProgramSequenceIn,0x0,sizeof(ProgramSequenceIn));

   //magic numbers supplied by philips
   ProgramSequenceOut[7] = 0x40;

   MltiInfo.bMultiCoreSystem = FALSE;
   DL_ScanIR(&ulIRRegister,&ulData,IR_LENGTH,TRUE); 

   DL_ScanDR_NoMC((unsigned long *)ProgramSequenceOut,(unsigned long *)ProgramSequenceIn,142,TRUE); 

   memset(ProgramSequenceOut,0x00,sizeof(ProgramSequenceOut));

   //magic numbers supplied by philips
   DL_ScanDR_NoMC((unsigned long *)ProgramSequenceOut,(unsigned long *)ProgramSequenceIn,142,TRUE); 

   MltiInfo.bMultiCoreSystem = bEnableBackup;
#endif
}
/****************************************************************************
     Function: ML_EnterDebugMode
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: Error code
  Description: Reset Processor and Place in DEBUG Mode
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_EnterDebugMode (TyMLHandle *ptyHandle)

{ 
   TyTypeOfOperation tyTypeOfOperation;

   CONTROL_REG tyLocControl;
   CONTROL_REG tyControlOutput;

   tyLocControl.ulData = 0x0;
   tyControlOutput.ulData = 0x0;

   DLOG2((LOGFILE,"\n\t\tML_EnterDebugMode -> Called"));

   tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);

   if ((tyTypeOfOperation == NOTINDEBUGMODE) || (tyTypeOfOperation == RESETOCCURRED ))
      {
      DLOG3((LOGFILE,"\n\t\tML_EnterDebugMode -> Asserting EJTAGBrk now"));

      tyControlOutput.ulData = 0x0;      // Initialise to 0
      tyControlOutput.Bits.EJTAGBrk = 1; // Send Debug Interrupt Request
      if (ptyHandle->bProbeTrapFlagExists)
         tyControlOutput.Bits.ProbTrap = 1; // Using EJTAG Memory, Inverse meaning on other versions
      tyControlOutput.Bits.ProbEn   = 1; // Probe will service Processor Accesses

      tyControlOutput.Bits.ClkEn = ptyHandle->ucDefaultValueOfClkEnBit;     // Enable DCLK during Trace

      // First Read back the control
      (void)ML_ScanIRandDR(ptyHandle, 
                           IR_LENGTH, 
                           LtoP(CONTROL), 
                           32, 
                           &tyControlOutput.ulData, 
                           &tyLocControl.ulData);

      if (ptyHandle->bAssertHaltTwice)
         {
         ML_Delay(20);
         // Also if the board is in a reset start, another halt request is neccessary
         (void)ML_ScanIRandDR(ptyHandle, 
                              IR_LENGTH, 
                              LtoP(CONTROL), 
                              32, 
                              &tyControlOutput.ulData, 
                              &tyLocControl.ulData);
         }

      }
   else
      {
      DLOG3((LOGFILE,"\n\t\tML_EnterDebugMode -> No need to break: Already in Debug mode"));
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_EnableCoreSelect
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle to device
               tyUserSettings : User configured settings
       Output: void
  Description: Special Yoda Support
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
void ML_EnableCoreSelect(TyMLHandle *ptyHandle)
{
   DLOG2((LOGFILE,"\n\t\tDL_EnableCoreSelect -> Called"));

   // We may also have to enable that device
   if (ptyHandle->ulCoreSelectIRLength != 0)
      {
      // One device in the core until we enable the second. Yoda.
      unsigned long ulDataIgnore;
      unsigned long ulDataIn = ptyHandle->ulCoreSelectCode;
      DLOG2((LOGFILE,"\n\t\tDL_EnableCoreSelect -> Activated"));

      (void)ML_ScanIR(ptyHandle, 
                      ptyHandle->ulCoreSelectIRLength,
                      &ulDataIn, 
                      &ulDataIgnore);

      }
}

/****************************************************************************
     Function: ML_ResetSoft
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle to device
               tyUserSettings : User configured settings
       Output: Error
  Description: Reset Processor and/or Peripherals or neither and Halt
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_ResetSoft(TyMLHandle *ptyHandle,
                     int bProcessorReset, 
                     int bPeripheralReset, 
                     int bHardBetweenSoftReset)
{
   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulDataRecv;
   CONTROL_REG crLocalControl;

   DLOG2((LOGFILE,"\n\t\tML_ResetSoft -> Called"));
   (void)ML_ScanIR(ptyHandle, 
                   IR_LENGTH, //todo handle
                   LtoP(CONTROL), 
                   &ulDataRecv);

   if (bProcessorReset || bPeripheralReset)
      {
      crLocalControl.ulData=0x0;        // Init to 0
      crLocalControl.Bits.ClkEn = ptyHandle->ucDefaultValueOfClkEnBit;     // Enable DCLK during Trace
      if (ptyHandle->bProbeTrapFlagExists)
         crLocalControl.Bits.ProbTrap = 1; // Using EJTAG Memory, Inverse meaning on other versions
      crLocalControl.Bits.ProbEn =1;    // Enable Probe
      crLocalControl.Bits.PrAcc  =1 ;   // Very important not to nuke the first processor access
      if (bProcessorReset)
         crLocalControl.Bits.PrRst =1;     // Reset Processor Core
      if (bPeripheralReset)
         crLocalControl.Bits.PerRst=1;     // Reset Peripherals (inc EJTAG + DSU on Philips processors)

//      DL_ScanDR(&crLocalControl.ulData,&ulDataRecv,32,TRUE);  // Reset
      (void)ML_ScanDR(ptyHandle, 
                      32, //todo handle
                      &crLocalControl.ulData, 
                      &ulDataRecv);

      if (ptyHandle->bSlowJTAGEnabled) // Wait longer for slow EJTAG
         ML_Delay(500); // Wait a little for the processor to reset.
      else
         ML_Delay(50); // Wait a little for the processor to reset.
      }

   if (bHardBetweenSoftReset)
      {
      // Next The Hard Reset.
      DLOG3((LOGFILE,"\n\t\tML_ResetSoft -> Performing Hard Reset for 200ms while in SoftReset"));
      (void)DL_OPXD_Mips_ResetProc(ptyHandle->iDLHandle,0,1,NULL);  // Clear the Reset pin and hold in Reset
      ML_Delay(300);    // Delay 300 ms while in reset
      (void)DL_OPXD_Mips_ResetProc(ptyHandle->iDLHandle,0,0,NULL);  // Set the Reset pin to bring out of reset
      // This delay was previously removed for normal JTAG but it didn't work without it
      // It has also been increased to 500ms as Genia didn't work with the Malta board
      // when the delay was 300. Do not change this delay without testing on ALL 
      // platforms
      ML_Delay(500);      
      }

   // Finally we remove the reset leaving the jtag brk set.
   crLocalControl.ulData=0x0;        // Init to 0
   crLocalControl.Bits.ClkEn = ptyHandle->ucDefaultValueOfClkEnBit; 
   crLocalControl.Bits.EJTAGBrk =1;  // Execute Debug Exception handler 
   if (ptyHandle->bProbeTrapFlagExists)
      crLocalControl.Bits.ProbTrap = 1; // Using EJTAG Memory, Inverse meaning on other versions
   crLocalControl.Bits.ProbEn =1;    // Enable Probe
   crLocalControl.Bits.PrRst =0;     // Come out of Reset Processor Core
   crLocalControl.Bits.PerRst =0;    // Come out of Reset Peripherals (inc EJTAG + DSU on Philips processors)
   crLocalControl.Bits.PrAcc =1 ;    // Very important not to nuke the first processor access

//   DL_ScanDR(&crLocalControl.ulData,&ulDataRecv,32,TRUE);  // Clear the Reset Bits and Halt processor.
   (void)ML_ScanDR(ptyHandle, 
                   32, //todo handle
                   &crLocalControl.ulData, 
                   &ulDataRecv);
   if (ptyHandle->bSlowJTAGEnabled) // Wait longer for slow EJTAG
      ML_Delay(500);

   return ErrRet;
}

/****************************************************************************
     Function: ML_ResetProc
     Engineer: Nikolay Chokoev
        Input: ulMode : Reset Mode
               tyUserSettings : User configured settings
       Output: Error code
  Description: Reset Processor and Place in DEBUG Mode
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_ResetProc(TyMLHandle *ptyHandle,
                     unsigned long ulMode, 
                     TyUserSettings tyUserSettings)
{

   TyError ErrRet=ERR_NO_ERROR;
   TyTypeOfOperation tyTypeOfOperation;
   TyStatus tyStatus;
   DEBUG_REG tyLocalDebug;
   CONFIG_REG tyLocalConfig;
   unsigned char pucReadBuffer[0x200];
   int i;

   DLOG2((LOGFILE,"\n\t\tML_ResetProc(ulMode=%d) -> Called", ulMode));

   if ((ulMode == MODEFULLRESET) || (ulMode == MODERSTRESET))
      {
      if (ptyHandle->bEJTAGBootSupported)
         (void)ML_SetBootType(ptyHandle,FALSE);
      // Enable memory writing if necessary
      ML_EnableMemoryWriting(ptyHandle,FALSE,FALSE,TRUE);

      if (ptyHandle->bPerformHardReset || (ulMode == MODERSTRESET))
         {
         DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Performing Hard Reset for 200ms"));
         (void)DL_OPXD_Mips_ResetProc(ptyHandle->iDLHandle,0,1,NULL);// Clear the Reset pin and hold in Reset
         ML_Delay(200);    // Delay 200 ms while in reset
         (void)DL_OPXD_Mips_ResetProc(ptyHandle->iDLHandle,0,0,NULL);// Set the Reset pin to bring out of reset
         ML_Delay(ptyHandle->ulResetDelayMS); 
         //todo: why is 4096 in the .cfg file?
         if (tyUserSettings.ucEJTAGSecSequence == LECO_SECURITY_SEQUENCE)
            {
            DLOG2((LOGFILE,"\n\t\tApplying LECO+ Reset Sequence"));
            // Disable Opella-USB multi-core support	//todo
            ML_ConfigureMulticoreSupport(ptyHandle,TRUE); //todo
            ML_ProgramLecoTCB(); 
            // Enable Opella-USB multi-core support		//todo
            ML_ConfigureMulticoreSupport(ptyHandle,FALSE);//todo
            }
         }
      }

   if (ptyHandle->bUseEjtagBoot)
      {
      // Unfortunately if we needed bEJTAGBootDuringReset then we probably got the settings wrong..
      // Read them again just to be sure.
      ErrRet = ML_GetBasicProcessorSettings(ptyHandle,&tyUserSettings);
      }

   if (ptyHandle->ucDMAWriteAfterReset)
      (void)MLM_WriteMemoryPhysical(ptyHandle,
                                    ptyHandle->ulDMAResetWriteAddr,
                                    (unsigned char *)&ptyHandle->ulDMAResetWriteValue,
                                    sizeof(ptyHandle->ulDMAResetWriteValue));

   // This needs to be specific to Yoda as it means nothing to other systems
   if (!tyUserSettings.bCoreSelectBeforeReset)   //for the yoda device we write to IR register after a pin11 reset
      {
      DLOG((LOGFILE,"\n\t\tML_ResetProc : DL_EnableCoreSelect after reset"));
      ML_EnableCoreSelect(ptyHandle);
      }

   if (ptyHandle->ucMemReadAfterReset)
      {
      for (i=0; i < NUMBER_OF_MEMORY_READS_BEFORE_RESET; i++)
         {
         (void)MLM_ReadMemory(ptyHandle,0x0,pucReadBuffer,20,4);
         }
      }

   switch (ulMode)
      {
      default:
      case MODEFULLRESET:
         // Make sure that the reset happened
         tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);

         if (tyTypeOfOperation != RESETOCCURRED)
            {
            DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Still not in DebugMode after Hard Reset"));
            // Perhaps it is EJTAG 1.53 and we are not yet in debug mode.
            // Step 2 Halt processor
            (void)ML_ResetSoft(ptyHandle,
                               ptyHandle->bPerformSoftProcReset,
                               ptyHandle->bPerformSoftPerReset,
                               ptyHandle->bHardBetweenSoftReset);
            }
         break;
      case MODEDEVICERESET:         // per MDI definition: both processor and peripheral reset
         (void)ML_ResetSoft(ptyHandle,TRUE, TRUE,FALSE);
         break;

      case MODECPURESET:
      case MODEPRRSTRESET:
         (void)ML_ResetSoft(ptyHandle,TRUE, FALSE ,FALSE); // reset processor only and halt
         break;

      case MODEPERIPHERALRESET:
      case MODEPERRSTRESET:
         (void)ML_ResetSoft(ptyHandle,FALSE, TRUE ,FALSE); // reset peripherals only and halt
         break;

      case MODEJTAGRSTRESET:
         (void)MLR_ReadRegister(ptyHandle,
                                ptyHandle->ulCP0RegNumberforDebug,
                                MDIMIPCP0,
                                &tyLocalDebug.ulData);
         tyLocalDebug.Bits.JtagRst = 1;                           
         (void)MLR_WriteRegister(ptyHandle,
                                 ptyHandle->ulCP0RegNumberforDebug,
                                 MDIMIPCP0,
                                 tyLocalDebug.ulData);
         if (ptyHandle->bSlowJTAGEnabled) // Wait longer for slow EJTAG
            ML_Delay(500);
         else
            ML_Delay(50);
         tyLocalDebug.Bits.JtagRst = 0;                           
         (void)MLR_WriteRegister(ptyHandle,
                                 ptyHandle->ulCP0RegNumberforDebug,
                                 MDIMIPCP0,
                                 tyLocalDebug.ulData);

         (void)ML_ResetSoft(ptyHandle, FALSE, FALSE,FALSE);            // halt processor
         // This does not always get processor back!
         break;

      case MODETRSTRESET:
         (void)DL_OPXD_Mips_ResetProc(ptyHandle->iDLHandle,0,1,NULL);  // Clear the Reset pin and hold in Reset
         if (ptyHandle->bSlowJTAGEnabled)
            ML_Delay(5);
         (void)DL_OPXD_Mips_ResetProc(ptyHandle->iDLHandle,0,0,NULL);  // Set the Reset pin to bring out of reset

         (void)ML_ResetSoft(ptyHandle, FALSE, FALSE,FALSE);            // halt processor
         // This does not always get processor back!
         break;

      case MODERSTRESET:
         // should already have done a hard reset, now try to halt the processor
         (void)ML_ResetSoft(ptyHandle, FALSE, FALSE,FALSE);
         break;
      case MODENORESET:
         //We are not resetting the target. Just try to halt the processor.
         (void)ML_ResetSoft(ptyHandle, FALSE, FALSE,FALSE);
         break;
      }

   if (ptyHandle->ulDelayAfterResetMS)
      ML_Delay(ptyHandle->ulDelayAfterResetMS);

   tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
   if (tyTypeOfOperation != READWORD)
      {
      DLOG((LOGFILE,"\n\t\tML_ResetProc -> Error: Still not in Debug Mode, Giving Second Chance!!"));

      // I think he deserves one more chance.
      // Already we reset and on upon removing reset tried to exert the EJTAG Brk.
      // Some processors may not like this. e.g. MOJO.
      // Now that the processor has had a little time to think and a chance 
      // to copy its Flash to RAM. (If thats what its into). We again try to 
      // halt it.
      ErrRet = ML_EnterDebugMode(ptyHandle);
      if (ErrRet != ERR_NO_ERROR)
         {
         dbgprint("ML_EnterDebugMode: %d \n", ErrRet);
         }
      tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
      if ((tyTypeOfOperation != READWORD) && (tyTypeOfOperation != READBYTE) )
         {
         // Yer Luck has run out mate...
         ASSERT_NOW();     
         DLOG((LOGFILE,"\n\t\tML_ResetProc -> Error: Still not in Debug Mode, No More Chances!!"));
         return ERR_RESPROC_FAILED_TO_ENTER_DEBUG_MODE;  // Still not reset, something is wrong.
         }
      }

   // We should also clear the Memory Protection bit for EJTAG 1.53 and EJTAG 2.00
   // We would like to NO LONGER NEED TO CLEAR THE MP BIT.
   // This should be done by SaveContext after each halt from emulation.
   // to support debug through hard reset.

   if (ptyHandle->bNeedToClearMPBit)
      {
      // Now we add Special 3930 / 3940 / 1910 Support
      DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Now About to Clear Memory Protection bits on PR1900 and PR1910"));

      //todo add DW2 to hdl
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulClearMPBitApp,
                           (unsigned int)ptyHandle->tyMipsDW2App.ulClearMPBitInstructions * sizeof(unsigned long),
                           pulCommonDataFromProc,
                           MAX_DW2_DATA, //todo: do we need it?
                           pulCommonDataToProc,
                           MAX_DW2_DATA); //todo: do we need it?

      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }

   // Check if processor really halted
   ptyHandle->bProcessorExecuting=TRUE;
   // ML_GetStatus will clear it

   ErrRet = ML_GetStatus(ptyHandle,&tyStatus,FALSE);    // Getting Status saves the context also
   // Don't Use Target Memory because its not set up yet
   if (ErrRet != ERR_NO_ERROR)
      {
      return ErrRet;
      }

   // We should now be successfully  in Debug Mode.
   // Now we can attempt to setup the DSU

   DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Now Determine the DSU Configuration"));
   ErrRet=MLB_DetermineDSUConfiguration(ptyHandle);

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   // The Reset Corrupts the DSU so we must
   // Re-initialise all Hardware Breakpoints which were
   // previously set
   DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Now Restore any Hardware breakpoints if they exist"));
   ErrRet=MLB_RestoreHWBPs(ptyHandle);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   // Next we may need to enable MIPS16 mode for some processors.
   // This ensures that Trace works correctly for them when they enable MIPS16.
   // Note: We may need to make this optional later as it is kinda intrusive :-(
   if (tyUserSettings.bEnableMIPS16ModeForTrace)
      {
      DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Enable MIPS16 mode: i.e. the ability to use MIPS16 code"));
      (void)MLR_ReadRegister(ptyHandle,
                             ptyHandle->ulCP0RegNumberforConfig,
                             MDIMIPCP0,
                             &tyLocalConfig.ulData);
      tyLocalConfig.Bits.MIPS16 = 1;
      (void)MLR_WriteRegister(ptyHandle,
                              ptyHandle->ulCP0RegNumberforConfig,
                              MDIMIPCP0,
                              tyLocalConfig.ulData);
      }

   if (ptyHandle->bAutoHaltCountersSupported && ptyHandle->bUserHaltCountersIDM)
      {
      // Automatically halt counters when in Debug Mode
      DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Automatically Halt Counters While in Debug mode"));
      (void)MLR_ReadRegister(ptyHandle,
                             ptyHandle->ulCP0RegNumberforDebug,
                             MDIMIPCP0,
                             &tyLocalDebug.ulData);
      tyLocalDebug.Bits.CountDM = 0;                           
      (void)MLR_WriteRegister(ptyHandle,
                              ptyHandle->ulCP0RegNumberforDebug,
                              MDIMIPCP0,
                              tyLocalDebug.ulData);
      }
   else
      {
      // Either User has chosen not to Halt counters or we need to do it in the Debug Exception Handler.
      DLOG3((LOGFILE,"\n\t\tML_ResetProc -> Don't Automatically Halt Counters While in Debug mode"));
      }


   return ErrRet;
}

/****************************************************************************
     Function: ML_StopExecution
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: Error code
  Description: Send Debug Interrupt signal to Processor
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_StopExecution(TyMLHandle *ptyHandle)
{
   TyError  ErrRet;
   TyStatus tyStatus;

   DLOG((LOGFILE,"\n\t\tML_StopExecution -> Called"));

   if (ptyHandle->bProcessorExecuting)
      {
      ErrRet=ML_EnterDebugMode(ptyHandle);
      if (ErrRet!=ERR_NO_ERROR)
         return ErrRet;
      }
   else
      {
      DLOG3((LOGFILE,"\t Asked to Stop when not executing. Ignore"));
      return ERR_NO_ERROR; // Lets Assume we can get awy with this for now.
      }

   // Then check if we really stopped and Save Context
   ErrRet=ML_GetStatus(ptyHandle,&tyStatus,TRUE);  // Should be safe enough to use target memory if necessary.

   // We must inform the Trace system that we have stopped as otherwise it won't 
   // ask the processor for real.


   // show that target is being halted on probe
   if (ErrRet == ERR_NO_ERROR)
      {
      ErrRet = DL_OPXD_IndicateTargetStatus(ptyHandle->iDLHandle, 0, 0);
      ErrRet = ML_ConvertDLError(ErrRet);
      }

   if (ErrRet!=ERR_NO_ERROR)
      return ErrRet;

   if (tyStatus.bInDebugMode == TRUE)
      {
      // First check if we are also stuck in Reduced Power Mode
      if (tyStatus.bReducedPowerMode)
         return ERR_PROC_HELD_IN_RPMODE;

      // We have broken, alls well that ends well
      return ERR_NO_ERROR;
      }
   else
      {
      // First check if we are stuck in Reduced Power Mode
      if (tyStatus.bReducedPowerMode)
         return ERR_PROC_HELD_IN_RPMODE;
      if (tyStatus.bExternalResetDetected)
         return ERR_PROC_HELD_IN_RESET;

      // Still Running.. Something is Wrong
      return ERR_PROC_HELD_IN_RESET;
      }
}

/****************************************************************************
     Function: ML_DetermineMulticoreInfo
     Engineer: Nikolay Chokoev
        Input: 
               tyUserSettings : User configured settings
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
void ML_DetermineMulticoreInfo(TyMLHandle *ptyHandle,
                               TyUserSettings *tyUserSettings)
{
   unsigned char ucIndex;
   for (ucIndex=0; ucIndex < ptyHandle->ulNumberOfCoresOnScanchain; ucIndex++)
      {
      ptyHandle->tyMultiCoreDeviceInfo[ucIndex].ulIRLength = 
      tyUserSettings->tyMultiCoreDeviceInfo[ucIndex].ulIRLength;
      ptyHandle->tyMultiCoreDeviceInfo[ucIndex].ulBypassCode = 
      tyUserSettings->tyMultiCoreDeviceInfo[ucIndex].ulBypassCode;
      }
}

/****************************************************************************
     Function: ML_Reset
     Engineer: Nikolay Chokoev
        Input: ulMode : Reset Mode
               tyUserSettings : User configured settings
       Output: Error code
  Description: Reset as per ulMode
Date           Initials    Description
10-Jul-2008    NCH          Initial
19-Dec-2008    RS           Microchip support added
****************************************************************************/
TyError ML_Reset (TyMLHandle *ptyHandle,
                  unsigned long ulMode, 
                  TyUserSettings tyUserSettings)
{
   TyError ErrRet;
   TyStatus tyStatus;

   DLOG2((LOGFILE,"\n\t\tML_Reset : Called"));

   //initialise other devices
   ML_DetermineScanChainInfo(ptyHandle, &tyUserSettings);
   ML_DetermineMulticoreInfo(ptyHandle, &tyUserSettings);

   ML_ConfigureMulticoreSupport(ptyHandle,TRUE);//todo

   if (ulMode == MODEFULLRESET || ulMode == MODENORESET)
      {
      DLOG((LOGFILE,"\n\t\tML_Reset : Mode is ModeFullReset"));
      ErrRet=ML_InitialiseDebugger(ptyHandle, tyUserSettings);  // Lets try talking to Hardware !!  

      if (ErrRet != ERR_NO_ERROR)
         {
         (void)ML_BasicSettingsOnError(ptyHandle, &tyUserSettings); // On Error we need some reasonable Defaults to allow for debug
         return ErrRet;
         }
      }

   if (ptyHandle->ucASHDMADebuggerClient)
      return ERR_NO_ERROR;

   if (tyUserSettings.bMchpRecoveryByChipErase)
      return ERR_NO_ERROR;  //We are in recovery mode so no point in resetting the device as recovery requires repowering the board

   ErrRet=ML_ResetProc(ptyHandle, ulMode,tyUserSettings);            // Reset Processor Into Debug Mode

   // Configure the ROM size for the P9SC146 and P9SC290 devices
   if (ptyHandle->ucMemWriteBeforeReset)
      {
      ErrRet = MLM_WriteWordsDW2(ptyHandle,
                                 ptyHandle->ulMemResetWriteAddr, 
                                 1,
                                 (void*)&ptyHandle->ulMemResetWriteValue,
                                 TRUE);
      if (ErrRet!= ERR_NO_ERROR)
         ASSERT_NOW();
      // Needs a reset to commit the value
      ErrRet=ML_ResetProc(ptyHandle, ulMode,tyUserSettings);            // Reset Processor Into Debug Mode
      }


   if (ErrRet != ERR_NO_ERROR)
      {
      (void)ML_BasicSettingsOnError(ptyHandle, &tyUserSettings); // On Error we need some reasonable Defaults to allow for debug
      return ErrRet;
      }


   ErrRet=ML_StopExecution(ptyHandle);        // This should halt the processor

   if (ErrRet != ERR_NO_ERROR)
      {
      (void)ML_BasicSettingsOnError(ptyHandle, &tyUserSettings); // On Error we need some reasonable Defaults to allow for debug
      return ErrRet;
      }

   // Check if processor really halted
   ErrRet = ML_GetStatus(ptyHandle, &tyStatus, FALSE);    // Getting Status saves the context also
   // We can't use target memory yet
   if (ErrRet != ERR_NO_ERROR)
      {
      (void)ML_BasicSettingsOnError(ptyHandle, &tyUserSettings); // On Error we need some reasonable Defaults to allow for debug
      return ErrRet;
      }

   if (tyStatus.bInDebugMode == TRUE)
      {
      // Sucessfully Entered Debug mode
      // Assume that status saved correctly
      MLM_LearnPipeLine(ptyHandle);
      }
   else
      {
      (void)ML_BasicSettingsOnError(ptyHandle, &tyUserSettings); // On Error we need some reasonable Defaults to allow for debug
      return ErrRet;
      }


   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: ML_Reset_Ex
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handle
               ulMode : Reset Mode
               tyUserSettings : User configured settings
       Output: Error code
  Description: Extended version of ML_Reset supporting JTAG diagnostic tools
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError ML_Reset_Ex (TyMLHandle *ptyHandle,
                     unsigned long ulMode, 
                     TyUserSettings tyUserSettings, 
                     int bSpecifiedProcessor)
{
   TyError ErrRet;

   DLOG((LOGFILE,"\n\t\tML_Reset_Ex : Called"));

   // if processor is specified, use ML_Reset
   if (bSpecifiedProcessor)
      return ML_Reset(ptyHandle, ulMode, tyUserSettings);

   ML_ConfigureMulticoreSupport(ptyHandle, TRUE);
   ptyHandle->bUseEjtagBoot = FALSE;

   // we need to initialise debugger, but not processor (target)
   ErrRet = ML_InitialiseDebugger_Ex(ptyHandle,tyUserSettings, bSpecifiedProcessor);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   return ERR_NO_ERROR;
}


//Local Functions

/****************************************************************************
     Function: ShiftArrayOfWordsRight
     Engineer: Nikolay Chokoev
        Input: unsigned long *pulArray - pointer to array of words
               unsigned long ulWords - number of words in array
       Output: none
  Description: shift array of words by 1 bit right
            only certain number of bits for comparison
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
static void ShiftArrayOfWordsRight(unsigned long *pulArray, 
                                   unsigned long ulWords)
{
   unsigned long ulIndex;
   unsigned long ulTransfer = 0;

   if ((!ulWords) || (pulArray == NULL))
      return;

   for (ulIndex = ulWords; ulIndex > 0; ulIndex--)
      {
      unsigned long ulTemp = (pulArray[ulIndex-1] & 0x1);   // get LSB
      pulArray[ulIndex-1] >>= 1;
      pulArray[ulIndex-1] |= ulTransfer;
      ulTransfer = ulTemp << 31;    // carry bit for next word
      }
}

/****************************************************************************
     Function: ML_SaveShadowRegisterSet
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
static TyError ML_SetUpShadowMasks(TyMLHandle   *ptyHandle,
                                   unsigned long *pulSrsCtlORMask, 
                                   unsigned long *pulSrsCtlANDMask)
{
   TyError ErrRet = ERR_NO_ERROR;  
   unsigned long ulNumberOfShadowSets;
   SRSCTL_REG tyLocSrsCtl;
   unsigned long ulRegNumber = 12;
   unsigned long ulSel = 2;


   DLOG2((LOGFILE,"\n\t\tML_SetUpShadowMasks -> Called"));

   ulNumberOfShadowSets = ptyHandle->ulNumberOfShadowSetsPresent;  

   ASSERT(pulSrsCtlORMask != NULL);
   ASSERT(pulSrsCtlANDMask != NULL);
   ASSERT(ulNumberOfShadowSets > 0);
   ASSERT(ulNumberOfShadowSets <= MAX_NUM_SHADOW_REGISTER_SETS);

   //based on number of shadow masks present, set masks based on CSS
   //CURRENTLY IMPLEMENT UP TO HSS=3 AS PER Rev 2.0 of M4K spec
   if (ulNumberOfShadowSets == 1)
      {
      ErrRet = MLR_ReadCP0Register(ptyHandle, ulRegNumber, 
                                   &tyLocSrsCtl.ulData, ulSel);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;

      //ASSERT(tyLocSrsCtl.Bits.CSS > 0);
      ASSERT(tyLocSrsCtl.Bits.CSS <= 1);

      DLOG4((LOGFILE,"\n\t\tML_SetUpShadowMasks -> for Number of Shadow Sets = %d and SrsCtl.css = %d",
             ulNumberOfShadowSets, tyLocSrsCtl.Bits.CSS));

      if (tyLocSrsCtl.Bits.CSS == 0)
         {
         pulSrsCtlORMask[0]  = SET_PSS_TO_1_OR_MASK; //init at start when moved
         pulSrsCtlANDMask[0] = SET_PSS_TO_1_AND_MASK;
         }
      else if (tyLocSrsCtl.Bits.CSS == 1)
         {
         pulSrsCtlORMask[0]  = SET_PSS_TO_0_OR_MASK; //init at start when moved
         pulSrsCtlANDMask[0] = SET_PSS_TO_0_AND_MASK;
         }
      }
   else if (ulNumberOfShadowSets == 3)
      {
      ErrRet = MLR_ReadCP0Register(ptyHandle,ulRegNumber,
                                   &tyLocSrsCtl.ulData,ulSel);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;

      //ASSERT(tyLocSrsCtl.Bits.CSS > 0);
      ASSERT(tyLocSrsCtl.Bits.CSS <= 3);       

      DLOG4((LOGFILE,"\n\t\tML_SetUpShadowMasks -> for Number of Shadow Sets = %d and SrsCtl.css = %d",
             ulNumberOfShadowSets, tyLocSrsCtl.Bits.CSS));

      if (tyLocSrsCtl.Bits.CSS == 0)
         {
         pulSrsCtlORMask[0]  = SET_PSS_TO_1_OR_MASK; 
         pulSrsCtlANDMask[0] = SET_PSS_TO_1_AND_MASK;
         pulSrsCtlORMask[1]  = SET_PSS_TO_2_OR_MASK; 
         pulSrsCtlANDMask[1] = SET_PSS_TO_2_AND_MASK;
         pulSrsCtlORMask[2]  = SET_PSS_TO_3_OR_MASK; 
         pulSrsCtlANDMask[2] = SET_PSS_TO_3_AND_MASK; 
         }
      else if (tyLocSrsCtl.Bits.CSS == 1)
         {
         pulSrsCtlORMask[0]  = SET_PSS_TO_0_OR_MASK; 
         pulSrsCtlANDMask[0] = SET_PSS_TO_0_AND_MASK;
         pulSrsCtlORMask[1]  = SET_PSS_TO_2_OR_MASK; 
         pulSrsCtlANDMask[1] = SET_PSS_TO_2_AND_MASK;
         pulSrsCtlORMask[2]  = SET_PSS_TO_3_OR_MASK; 
         pulSrsCtlANDMask[2] = SET_PSS_TO_3_AND_MASK; 
         }
      else if (tyLocSrsCtl.Bits.CSS == 2)
         {
         pulSrsCtlORMask[0]  = SET_PSS_TO_0_OR_MASK; 
         pulSrsCtlANDMask[0] = SET_PSS_TO_0_AND_MASK;
         pulSrsCtlORMask[1]  = SET_PSS_TO_1_OR_MASK; 
         pulSrsCtlANDMask[1] = SET_PSS_TO_1_AND_MASK;
         pulSrsCtlORMask[2]  = SET_PSS_TO_3_OR_MASK; 
         pulSrsCtlANDMask[2] = SET_PSS_TO_3_AND_MASK; 
         }
      else if (tyLocSrsCtl.Bits.CSS == 2)
         {
         pulSrsCtlORMask[0]  = SET_PSS_TO_0_OR_MASK; 
         pulSrsCtlANDMask[0] = SET_PSS_TO_0_AND_MASK;
         pulSrsCtlORMask[1]  = SET_PSS_TO_1_OR_MASK; 
         pulSrsCtlANDMask[1] = SET_PSS_TO_1_AND_MASK;
         pulSrsCtlORMask[2]  = SET_PSS_TO_2_OR_MASK; 
         pulSrsCtlANDMask[2] = SET_PSS_TO_2_AND_MASK; 
         }
      }

   return ERR_NO_ERROR;

}

/****************************************************************************
     Function: ML_SaveShadowRegisterSet
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handle
       Output: Error code
  Description:
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
static TyError ML_SaveShadowRegisterSet(TyMLHandle *ptyHandle)
{
   TyError ErrRet = ERR_NO_ERROR;  
   unsigned long ulSrsCtlORMask[MAX_NUM_SHADOW_REGISTER_SETS]  = {0,0,0};   
   unsigned long ulSrsCtlANDMask[MAX_NUM_SHADOW_REGISTER_SETS] = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};
   unsigned long ulNumberOfShadowSets;           
   unsigned int i,j;

   DLOG2((LOGFILE,"\n\t\tML_SaveShadowRegisterSet -> Called"));

   ulNumberOfShadowSets = ptyHandle->ulNumberOfShadowSetsPresent;           

   //set up array of shadow masks
   ErrRet = ML_SetUpShadowMasks(ptyHandle, ulSrsCtlORMask, ulSrsCtlANDMask);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   //check that number of shadow registers is set correctly at a value of 0,1,
   ASSERT(ulNumberOfShadowSets > 0);
   ASSERT(ulNumberOfShadowSets <= MAX_NUM_SHADOW_REGISTER_SETS);
   ASSERT(ptyHandle->pulShadowRegisterSet != NULL);
   ASSERT(tyApp < LAST_APP);

   // Save the specified number of shadow register sets
   for (i=0; i < ulNumberOfShadowSets; i++)
      {

      DLOG4((LOGFILE,"\n\t\tML_SaveShadowRegisterSet -> ulSRSORMask = 0x%08X",ulSrsCtlORMask[i] ));
      DLOG4((LOGFILE,"\n\t\tML_SaveShadowRegisterSet -> ulSRSANDMask = 0x%08X",ulSrsCtlANDMask[i] ));       

      pulCommonDataToProc[0x0]      = ulSrsCtlORMask[i];  // Pass OR Mask to the DW2
      pulCommonDataToProc[0x1]      = ulSrsCtlANDMask[i]; // Pass AND Mask to the DW2

      //todo DW2 data size
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulSaveShadowRegisterSetApp,
                           ptyHandle->tyMipsDW2App.ulSaveShadowRegisterSetInstructions * sizeof(unsigned long),
                           pulCommonDataToProc,
                           MAX_DW2_DATA, //todo: how much do we need?
                           pulCommonDataFromProc,
                           MAX_DW2_DATA); //todo: how much do we need?


      for (j=0; j < NUM_SHADOW_REGISTERS; j++)    //pass in array of shadow registers to restored for this set
         ptyHandle->pulShadowRegisterSet[i][j] = pulCommonDataFromProc[j];

      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }  

   return ErrRet;
}

/****************************************************************************
     Function: ML_SaveContext
     Engineer: Nikolay Chokoev
        Input: ptyHandle : device handle
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
06-Aug-2009    SPC          Backing up the TLB while the target halted.
*****************************************************************************/
static TyError ML_SaveContext(TyMLHandle *ptyHandle,
                              int bUseTargetMemoryIfNeeded)
{

   TyError ErrRet = ERR_NO_ERROR;

   IBS_REG     ibsInstructionBreakStatus;
   DBS_REG     dbsDataBreakStatus;
   unsigned long ulPCValueAtBreak;
   unsigned long ulCP0ConfigValue=0;
   CONFIG_REG tyLocConfig;

   NOREF(bUseTargetMemoryIfNeeded);

   DLOG2((LOGFILE,"\n\t\tML_SaveContext -> Called"));

   // Save Context      
   DLOG3((LOGFILE,"\n\t\tML_SaveContext -> Returning to Debug Mode: Save Context"));

   ML_SetupConfig0Masks(ptyHandle,TRUE);

   DLOG4((LOGFILE,"\n\t\tML_SaveContext -> ptyHandle->ulConfigORMask = 0x%08X", ptyHandle->ulConfigORMask));
   DLOG4((LOGFILE,"\n\t\tML_SaveContext -> ptyHandle->ulConfigANDMask = 0x%08X",ptyHandle->ulConfigANDMask));

   pulCommonDataToProc[0x0]      = ptyHandle->ulConfigORMask;  // Pass OR Mask to the DW2
   pulCommonDataToProc[0x1]      = ptyHandle->ulConfigANDMask; // Pass AND Mask to the DW2

   //todo DW2 data size
   ErrRet=ML_ExecuteDW2(ptyHandle,
                        (void *)(ptyHandle->tyMipsDW2App.pulSaveContextApp),
                        ptyHandle->tyMipsDW2App.ulSaveContextInstructions * sizeof(unsigned long),
                        (void *)(pulCommonDataToProc),
                        3*sizeof(unsigned long),//MAX_DW2_DATA, //todo: how much do we need?
                        (void *)(ptyHandle->pulProcessorContext),
                        MAX_DW2_DATA); //todo: how much do we need?
   ptyHandle->bProcessorExecuting = FALSE; // Don't save context again

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;
   if (ptyHandle->ulNumberOfShadowSetsPresent > 0)
      {
      ErrRet = ML_SaveShadowRegisterSet(ptyHandle);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }

   // Next We read the PC Value.
   // If this is odd then we are in MIPS16 mode
   // In this case make it even again and remember the fact that we
   // were in MIPS16 Mode so we can set it again before we return
   (void)MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulPCValueAtBreak); // Get original value
   if ((ulPCValueAtBreak & 0x1) == 0x1)
      {
      // PC Value is Odd => either we are in MIPS16 mode or something is very wrong
      DLOG3((LOGFILE,"\n\t\tML_SaveContext -> PC Value is Odd : BrokeInMIPS16Mode"));
      (void)MLR_WriteRegister(ptyHandle,0,MDIMIPPC,ulPCValueAtBreak-1); // Give Pathfinder an even value
      ptyHandle->bBrokeInMIPS16Mode = TRUE;
      }
   else
      {
      ptyHandle->bBrokeInMIPS16Mode = FALSE;
      }

   (void)MLR_ReadRegister(ptyHandle,ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0, &ulCP0ConfigValue);
   ptyHandle->bBigEndian = (((ulCP0ConfigValue >> ptyHandle->ulCP0BitOffsetInConfigforEndianMode) & 1) == 1);
   DLOG((LOGFILE,"\nML_SaveContext : Current Endian BigEndian = %X",ptyHandle->bBigEndian));

   if (ptyHandle->bBasicMappingSystemInUse)
      {
      DLOG3((LOGFILE,"\n\t\tML_SaveContext -> Now determine the MAP bit"));
      (void)MLR_ReadRegister(ptyHandle,ptyHandle->ulCP0RegNumberforConfig, MDIMIPCP0,&ulCP0ConfigValue); // Read the CP0 Config Register

      if (((ulCP0ConfigValue >> 20) & 1) == 1)
         {
         //MAP Bit = 2  Address Translation Scheme 2
         DLOG3((LOGFILE,"\n\t\tML_SaveContext -> Address Translation Scheme 2"));
         ptyHandle->ucFixedMapSystem = 2;
         }
      else
         {
         //MAP Bit = 1  Address Translation Scheme 1
         DLOG3((LOGFILE,"\n\t\tML_SaveContext -> Address Translation Scheme 1"));
         ptyHandle->ucFixedMapSystem = 1;
         }
      }
   else
      ptyHandle->ucFixedMapSystem = 1; // Just assume a default, it doesn't really matter for now

   // Now we must write a safe default value to the Debug Control Register
   // Note: In EJTAG 2.0 versions we will have cleared this during context save
   // (void)MLR_WriteRegister(ptyHandle,REG_MIPS_DCR,ASHLING_INT_REG,ptyHandle->ulDefaultValueForDCR); 

   // We Must also Ensure the DSU is enabled in case a user accidentally disables it in his code
   // First Read back real value of Config
   // Then set the DSU bit
   // Finally write back the new value

   if (ptyHandle->bEnableDSUOnBreakFromEmulation)
      {
      DLOG3((LOGFILE,"\n\t\tML_SaveContext -> Enabling DSU bit in Config Register"));
      (void)MLR_ReadRegister(ptyHandle,ptyHandle->ulCP0RegNumberforConfig,MDIMIPCP0,&tyLocConfig.ulData);
      // Now Set the DSU bit
      tyLocConfig.Bits.DSU = 1; // Enable DSU
      // and write it back
      (void)MLR_WriteRegister(ptyHandle,ptyHandle->ulCP0RegNumberforConfig,MDIMIPCP0,tyLocConfig.ulData);
      }

   // We should also check which Hardware Breakpoint occurred and clear the 
   // Break Status bit in IBS register. Otherwise we will break again as soon 
   // as we enter emulation.
   (void)MLR_ReadRegister(ptyHandle,REG_MIPS_IBS,ASHLING_INT_REG,&ibsInstructionBreakStatus.ulData);
   ptyHandle->ulInstBrkStatus = ibsInstructionBreakStatus.Bits.BrkStatus;

   DLOG3((LOGFILE,"\n\t\tML_SaveContext -> IBS Value = %08X",ibsInstructionBreakStatus.ulData));

   if(ibsInstructionBreakStatus.Bits.BrkStatus != 0) {
      ibsInstructionBreakStatus.Bits.BrkStatus = 0x0; // Clear the Status  bits
      (void)MLR_WriteRegister(ptyHandle,REG_MIPS_IBS,ASHLING_INT_REG,ibsInstructionBreakStatus.ulData);
      }

   // We should also check if a Data Breakpoint occurred and clear the 
   // Break Status bit in DBS register. Otherwise we will break again as soon 
   // as we enter emulation.
   (void)MLR_ReadRegister(ptyHandle,REG_MIPS_DBS,ASHLING_INT_REG,&dbsDataBreakStatus.ulData);
   ptyHandle->ulDataBrkStatus = dbsDataBreakStatus.Bits.BrkStatus;

   DLOG3((LOGFILE,"\n\t\tML_SaveContext -> DBS Value = %08X",dbsDataBreakStatus.ulData));

   if(dbsDataBreakStatus.Bits.BrkStatus != 0){
      dbsDataBreakStatus.Bits.BrkStatus = 0x0; // Clear the Status  bits

      (void)MLR_WriteRegister(ptyHandle,REG_MIPS_DBS,ASHLING_INT_REG,dbsDataBreakStatus.ulData);
      }

   // Finally we should Flush the cache if Cache Debugging is enabled
   // SPC: Commented out because now we are using hit invalidate while reading and writing memory
   // 
   /* if (bUseTargetMemoryIfNeeded)
      {
      dbgprint("Flush the cache (1) \n");
      if (ptyHandle->bFlushOnlyDataCache)
         {
         DLOG4((LOGFILE,"\nML_SaveContext ->  Calling WriteBack Data Cache"));
         ErrRet=MLC_FlushCache(ptyHandle,
                               CACHE_TYPE_DATA,
                               CACHE_FLUSH_WRITEBACK);
         if (ErrRet != ERR_NO_ERROR)
            return ErrRet;
         }
      else
         {
         DLOG4((LOGFILE,"\nML_SaveContext ->  Calling WriteBackAndInvalidate Both Caches"));
         ErrRet=MLC_FlushCache(ptyHandle,
                               CACHE_TYPE_UNIFIED,//BothIAndDCache,
                               CACHE_FLUSH_WRITEBACK_AND_INVALIDATE);
         if (ErrRet != ERR_NO_ERROR)
            return ErrRet;
         }
      }*/

   ptyHandle->bDataCacheAlreadyInvalidated = FALSE;  // Flag for use by MLM_WriteMemory

   //Creating backup for TLB. This will be useful for MMU walkup
   CreateBackupOfTLB(ptyHandle);

   DLOG4((LOGFILE,"\nML_SaveContext -> DataCacheAlreadyInvalidated = FALSE"));

   return ErrRet;
}

/****************************************************************************
     Function: ML_ExecuteInstruction
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
            pulExpectedAddress: Address which is expected from the processor
               ulInstruction: Opcode to feed to the processor in case of Opcode Execution
               ulDataIn     : Data value to feed to the processor in case of read operation
               pulDataOut   : Storage for word written in case of processor write operation
               Operation    : InstructionFetch: We expect processor to fetch an opcode
                              DataFetch       : We expect processor to read a word
                              WriteFromProcessor: We expect the processor to perform a word write
       Output: Type of Operation
  Description: This fuction is the cornerstone of the entire operation.
               Most of the higher level functions use this to communicate with the Emulating processor
               Further explainations of this Function to follow
            only certain number of bits for comparison
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyTypeOfOperation ML_ExecuteInstruction (TyMLHandle   *ptyHandle,
                                         unsigned long    *pulExpectedAddress,
                                         unsigned long    ulInstruction,
                                         unsigned long    ulDataIn,
                                         unsigned long    *pulDataOut,
                                         TyExpectedOperation Operation)
{

   unsigned long ulControlOutput  =0xDEADDEAD;
   unsigned long ulControlInput   =0xDEADDEAD;
   unsigned long ulExpectedControl=0xDEADDEAD;
   unsigned long ulExpectedAddress=0xDEADDEAD;
   unsigned long ulDataToWrite;
   unsigned long ulDataReadBack;
   unsigned long ulDataIgnore;
   unsigned long ulAddress[2];

   DLOG2((LOGFILE,"\n\t\tML_ExecuteInstruction -> Called"));

   if (Operation == INSTRUCTIONFETCH_IC)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlInsDatFetch;
      ulDataToWrite = ulInstruction;
      ulExpectedAddress = *pulExpectedAddress;
      ulControlInput = ulExpectedControl;
      }

   if (Operation == DATAFETCH_IC)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlInsDatFetch;
      ulExpectedAddress = *pulExpectedAddress;
      ulDataToWrite = ulDataIn;
      ulControlInput = ulExpectedControl;
      }


   if (Operation == WRITEFROMPROCESSOR_IC)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlDatWrite;
      ulExpectedAddress = MAGIC_FIFO_LOCATION;
      ulControlInput = ulExpectedControl;
      }

   if (Operation == INSTRUCTIONFETCH)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlInsDatFetch;
      ulDataToWrite = ulInstruction;
      ulExpectedAddress = *pulExpectedAddress;
      }

   if (Operation == DATAFETCH)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlInsDatFetch;
      ulExpectedAddress = *pulExpectedAddress;
      ulDataToWrite = ulDataIn;
      }


   if (Operation == WRITEFROMPROCESSOR)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlDatWrite;
      ulExpectedAddress = MAGIC_FIFO_LOCATION;
      }

   if (Operation == WRITEFROMPROCESSOR_UA)
      {
      ulExpectedControl = ptyHandle->ulExpectedControlDatWrite;
      ulExpectedAddress = *pulExpectedAddress;
      }


   if (ulExpectedAddress == 0xDEADDEAD)
      {
      // Invalid Operation Chosen
      ASSERT_NOW();
      return PROBENOTENABLED;
      }


   if (ulControlInput == ulExpectedControl)
      {
      // Sometimes we simply don't bother validating Control
      }
   else
      {
      // Step 1 Read Control
      ulControlOutput = ptyHandle->ulDefaultControlProbeWillService;
      (void)ML_ScanIRandDR(ptyHandle,        //TyMLHandle *ptyHandle, 
                           IR_LENGTH,        //ulIRLength, 
                           LtoP(CONTROL),    //*pulIRDataIn, 
                           32,               //ulDRLength, 
                           &ulControlOutput, // *pulDRDataIn, 
                           &ulControlInput); //*pulDRDataOut
      }



   if ((ulControlInput & EJTAG_BRK_MASK)== ulExpectedControl)
      {

      // Read Memory
      (void)ML_ScanIRandDR(ptyHandle,      //TyMLHandle *ptyHandle, 
                           IR_LENGTH,     //ulIRLength, 
                           LtoP(ADDRESS), //*pulIRDataIn, 
                           ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength, 
                           LtoP(0),       // *pulDRDataIn, 
                           (unsigned long*)ulAddress);   //*pulDRDataOut
      DLOG3((LOGFILE,"\n\t\tML_ExecuteInstruction -> Address = %08X",ulAddress[0]));


      if (ulAddress[0] == ulExpectedAddress)
         {
         // Fetching Start Address Now
         (void)ML_ScanIRandDR(ptyHandle,     //TyMLHandle *ptyHandle, 
                              IR_LENGTH,     //ulIRLength, 
                              LtoP(DATA),    //*pulIRDataIn, 
                              DR_LENGTH,     //ulDRLength, 
                              &ulDataToWrite,// *pulDRDataIn, 
                              &ulDataReadBack);//*pulDRDataOut
         ulControlOutput = ptyHandle->ulDefaultControlCompleteAccess;

         // Complete the Instruction
         (void)ML_ScanIRandDR(ptyHandle,     //TyMLHandle *ptyHandle, 
                              IR_LENGTH,     //ulIRLength, 
                              LtoP(CONTROL), //*pulIRDataIn, 
                              32,            //ulDRLength, 
                              &ulControlOutput,// *pulDRDataIn, 
                              &ulDataIgnore);//*pulDRDataOut

         }
      else
         {
         DPRINTF(("\nBARF 1"));
         DLOG2((LOGFILE,"\n\t\tML_ExecuteInstruction -> Invalid Expected: %08X",ulExpectedAddress));
         dbgprint("ML_ExecuteInstruction -> Invalid Expected: %08X",ulExpectedAddress);
         if (ulAddress[0] == DE_VECT)
            {
            // Exception occurred in DEBUG MODE
            DPRINTF(("\nDebug Exception"));
            DLOG2((LOGFILE,"\n\t\tML_ExecuteInstruction -> Cause = Exception"));
            ASSERT_NOW();
            return(DEBUGEXCEPTIONOCCURRED);
            }
         else
            {
            DPRINTF(("\nBARF 3"));
            DLOG2((LOGFILE,"\n\t\tML_ExecuteInstruction -> Cause = Unknown"));
            // BAD ADDRESS Deal with it
            ASSERT_NOW();
            return(DEBUGEXCEPTIONOCCURRED);
            }
         }
      }
   else
      {
      DPRINTF(("\nBARF 4"));
      DLOG2((LOGFILE,"\n\t\tML_ExecuteInstruction -> Invalid Control %08X -> Expected %08X",ulControlInput,ulExpectedControl));
      // BAD CONTROL Deal with it
      ASSERT_NOW();
      return(PROBENOTENABLED);
      }                                     

   if (Operation == WRITEFROMPROCESSOR || Operation == WRITEFROMPROCESSOR_IC)
      {
      *pulDataOut = ulDataReadBack;
      }

   if (Operation == INSTRUCTIONFETCH || Operation == INSTRUCTIONFETCH_IC)
      {
      *pulExpectedAddress += 4;
      }

   return(READWORD);
}

/****************************************************************************
     Function: ML_DeviceDisconnect
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: Error code
  Description: Disconect device
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_DeviceDisconnect(TyMLHandle    *ptyHandle)
{
   (void)DL_OPXD_SetVtpaVoltage(ptyHandle->iDLHandle, (double)0, 1, 0);    // disconnect TPA buffers
   if (ptyHandle->iDLHandle < MAX_DEVICES_SUPPORTED)
      {
      (void)DL_OPXD_UnconfigureDevice(ptyHandle->iDLHandle);
      (void)DL_OPXD_CloseDevice(ptyHandle->iDLHandle);
      ptyHandle->iDLHandle = MAX_DEVICES_SUPPORTED;
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_StartExecution
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_StartExecution(TyMLHandle *ptyHandle)
{

   unsigned long ulCurrentPCValue;
   TyError ErrRet = ERR_NO_ERROR;
   unsigned long ulDCRReadBack;

   DLOG2((LOGFILE,"\n\t\tML_StartExecution -> Called"));

   // Just before we leave debug mode we must be sure to enable trace.

   // Read DCR before you say you are executing.
   (void)MLR_ReadRegister(ptyHandle, REG_MIPS_DCR,ASHLING_INT_REG,&ulDCRReadBack); 
   DLOG((LOGFILE,"\nMLTR_StartExecution DCR = %08X\n",ulDCRReadBack));

   // Turn off any debug access
   ML_EnableMemoryWriting(ptyHandle, TRUE,FALSE,FALSE);

   ptyHandle->bProcessorExecuting=TRUE;  

   if (ptyHandle->ulNumberOfShadowSetsPresent > 0)
      {
      ErrRet = ML_RestoreShadowRegisterSet(ptyHandle);

      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }

   // First Deal with MIPS16 mode
   // If we were in MIPS16 Mode when we stopped execution, assume we are still in it
   // In that case we need to restore the PC to its ODD value
   if (ptyHandle->bBrokeInMIPS16Mode)
      {
      DLOG3((LOGFILE,"\n\t\tML_StartExecution -> We broke In MIPS16 mode: PC = PC + 1"));
      (void)MLR_ReadRegister(ptyHandle, 0,MDIMIPPC,&ulCurrentPCValue);
      (void)MLR_WriteRegister(ptyHandle, 0,MDIMIPPC,ulCurrentPCValue+1);
      }

   // show that target is being started on probe
//   if (ErrRet == ERR_NO_ERROR)
//      {
   ErrRet = DL_OPXD_IndicateTargetStatus(ptyHandle->iDLHandle, 0, 1);
   ErrRet = ML_ConvertDLError(ErrRet);
//      }

   if (ErrRet == ERR_NO_ERROR)
      {
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulRestoreContextApp,
                           ptyHandle->tyMipsDW2App.ulRestoreContextInstructions * sizeof(unsigned long),
                           ptyHandle->pulProcessorContext,
                           MAX_DW2_DATA, //todo: how much do we need?
                           pulCommonDataFromProc,
                           MAX_DW2_DATA); //todo: how much do we need?
      }

   return ErrRet;
}

/****************************************************************************
     Function: ML_SWSingleStep
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_SWSingleStep(TyMLHandle  *ptyHandle, int bStepOver)
{
   unsigned long  ulLoop;
   unsigned long  ulOriginalPCValue;
   TyStatus       tyStatus;
   TyDisaResult   DisaResult;
   TyBPType       bptBPType1,    bptBPType2;
   unsigned long  ulBPSize1,     ulBPSize2;
   unsigned char  bNoBPPresent1, bNoBPPresent2;
   TyError        ErrRet=ERR_NO_ERROR;
   unsigned char  bSetBPAtNext = FALSE;
   unsigned char  bSetBPAtDest = FALSE;
   unsigned long  ulMaxWaitForStepReturn;

   memset(&tyStatus,0x0,sizeof(tyStatus));
   if (bStepOver)
      {
      ulMaxWaitForStepReturn = 0x50000;   // Wait a very long time because we could be stepping over slow code
      }
   else
      {
      // Only wait long enough to ensure one instruction has been executed.
      ulMaxWaitForStepReturn = MAX_WAIT_FOR_SINGLE_STEP;
      }

   DLOG2((LOGFILE,"\n\t\tML_SWSingleStep -> Called"));

   (void)MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulOriginalPCValue);

   // determine the branch destination (if any) and next address associated 
   // with this instruction
   if (ptyHandle->bBrokeInMIPS16Mode)
      {
      (void)ML_Disa16(ptyHandle, ulOriginalPCValue, &DisaResult);
      }
   else
      {
      (void)ML_Disa32(ptyHandle, ulOriginalPCValue, &DisaResult);
      }


   // check if there is already a BP at the next instruction address
   (void)MLB_QueryBP(ptyHandle,DisaResult.ulNextAddr,&bptBPType1,&ulBPSize1,&bNoBPPresent1);
   if (bNoBPPresent1)
      {
      // Hmm, if this region is read ihibitted then the setting of the software breakpoint will not work
      if ((ErrRet = MLB_SetBP(ptyHandle, DisaResult.ulNextAddr,DisaResult.ulNextAddr,BP_SW,DisaResult.ulNextInstSize)) != ERR_NO_ERROR)
         return ErrRet;
      bSetBPAtNext = TRUE;
      }
   else
      {
      // TO_DO: Already have BP at next instruction address
      // 1. Should we check if ulBPSize1 == DisaResult.ulNextInstSize
      // 2. Should we check if its a HW_BP, would it matter to the user if
      //    the step worked due to a previously set HW_BP
      // For now we assume that the existing BP is good enough for us.
      }


   if (bStepOver && DisaResult.bCallInst)
      {
      // Stepping Over Call, Do Not Set BP at Call Destination
      }
   else if (DisaResult.ulDestAddr != DisaResult.ulNextAddr)
      {
      // check if there is already a BP at the branch or call destination
      (void)MLB_QueryBP(ptyHandle, DisaResult.ulDestAddr,&bptBPType2,&ulBPSize2,&bNoBPPresent2);
      if (bNoBPPresent2)
         {
         // Set a Software BP at the destination address
         if ((ErrRet = MLB_SetBP(ptyHandle, DisaResult.ulDestAddr,DisaResult.ulDestAddr,BP_SW,DisaResult.ulDestInstSize)) != ERR_NO_ERROR)
            return ErrRet;
         bSetBPAtDest = TRUE;
         }
      else
         {
         // TO_DO: Already have BP at destination address
         // 1. Should we check if ulBPSize2 == DisaResult.ulDestInstSize
         // 2. Should we check if its a HW_BP, would it matter to the user if
         //    the step worked due to a previously set HW_BP
         // For now we assume that the existing BP is good enough for us.
         }
      }

   // We have set all our BP's now start execution and wait for break
   (void)ML_StartExecution(ptyHandle);

   for (ulLoop=0; ulLoop< ulMaxWaitForStepReturn; ulLoop++)
      {
      ErrRet=ML_GetStatus(ptyHandle, &tyStatus,TRUE); // OK to use target memory if necessary
      if (ErrRet!=ERR_NO_ERROR)
         {
         // Serious Problem
         ASSERT_NOW();
         return ErrRet;
         }

      if (tyStatus.bInDebugMode)
         {
         // Back in debug mode
         // Execution has been halted
         break;
         }
      }


   if (tyStatus.bInDebugMode == FALSE)
      {
      // Never Returning from SW Single Step
      // TO_DO: decide what to do here and if the max waits should be increased
      //        could this happen if the processor is busy servicing interrupts
      ASSERT_NOW();
      if (bStepOver)
         {
         DLOG((LOGFILE,"\n\t\tML_SWSingleStep -> Failed to Return from Software SingleStep Over after %d checks",ulLoop));
         return ERR_SSTEPOVER_TIMEOUT;
         }
      else
         {
         DLOG((LOGFILE,"\n\t\tML_SWSingleStep -> Failed to Return from Software SingleStep FWD after %d checks",ulLoop));
         return ERR_SSTEPFWD_TIMEOUT;
         }
      }

   // Processor Has halted

   // if we set a BP at the next instruction address, then clear it
   if (bSetBPAtNext)
      {
      (void)MLB_ClearBP(ptyHandle, DisaResult.ulNextAddr,&bptBPType2,&ulBPSize2,&bNoBPPresent2,NULL);
      }

   // if we set a BP at the branch destination, then clear it
   if (bSetBPAtDest)
      {
      (void)MLB_ClearBP(ptyHandle, DisaResult.ulDestAddr,&bptBPType1,&ulBPSize1,&bNoBPPresent1,NULL);
      }


   return ErrRet;
}

/****************************************************************************
     Function: ML_SingleStep
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_SingleStep(TyMLHandle *ptyHandle)
{

   unsigned long  ulDebugValue;

   unsigned long  ulCheckedControl;
   unsigned long  ulOriginalPCValue;
   unsigned long  ulCurrentPCValue;
   MIPS_STATUS_REG   srStatusRegister;
   MIPS_STATUS_REG   srStatusRegisterAfterEmulation;

   unsigned int x;

   int bStillExecuting;
   TyStatus tyStatus;
   TyError ErrRet=ERR_NO_ERROR;

   DLOG2((LOGFILE,"\n\t\tML_SingleStep -> Called"));


   if (ptyHandle->bSingleStepUsingSWBP)
      {
      return ML_SWSingleStep(ptyHandle, FALSE);     
      }


   (void)MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulOriginalPCValue); // Get original value

   if (ptyHandle->bBrokeInMIPS16Mode && !ptyHandle->bSSInMIPS16Supported)
      {
      // If we broke In MIPS16 Mode  and the processor is a PR1900
      // Single Step doesn't work under MIPS16
      DLOG3((LOGFILE,"\n\t\tML_SingleStep -> Attempt to SingleStep in MIPS16 Mode ( Not Supported)"));
      ASSERT_NOW();   // Test 
      return(ERR_SSTEP_CANT_SS_IN_MIPS16);
      }

   // Now we implement the feature "Disable Interrupts during Single Step"
   if (ptyHandle->bDisableIntSS)
      {
      DLOG3((LOGFILE,"\n\t\tML_SingleStep -> First Disable Interrupts"));

      (void)MLR_ReadRegister(ptyHandle,REG_NUM_STATUS, MDIMIPCP0,&srStatusRegister.ulData); // Get original value of Status Register
      ptyHandle->ulBackupOfStatusRegister = srStatusRegister.ulData; // Backup this value

      srStatusRegister.Bits.IM      = 0;   // Disable Hardware Interrupts during Single Step
      srStatusRegister.Bits.SWM     = 0;   // Disable Software Interrupts during Single Step
      srStatusRegister.Bits.IEc     = 0;   // Disable all Interrupts during Single Step

      (void)MLR_WriteRegister(ptyHandle, REG_NUM_STATUS, MDIMIPCP0,srStatusRegister.ulData); // Write it back with Interrupts Disabled

      }

   // Turn on Single Step Mode
   // Set Bit 8 of CP0 Debug Register
   (void)MLR_ReadRegister(ptyHandle,ptyHandle->ulCP0RegNumberforDebug,MDIMIPCP0,&ulDebugValue); // Get original value

   ulDebugValue = ulDebugValue | (0x1 << 8);         // Set Bit 8

   (void)MLR_WriteRegister(ptyHandle,ptyHandle->ulCP0RegNumberforDebug,MDIMIPCP0,ulDebugValue); // Write back the value


   for (x=0;x<=1;x++)
      {
      // Sometimes Single Step doesn't work.
      // This is a strange bug and there is certainly a better fix than this.
      // TODO: Improve this patch.

      (void)ML_StartExecution(ptyHandle);
      bStillExecuting = TRUE;       // This is very important as otherwise we don't save context

      ulCheckedControl = 0;


      while (bStillExecuting)
         {
         ErrRet=ML_GetStatus(ptyHandle, &tyStatus,TRUE); // OK to use target memory if necessary
         if (ErrRet!=ERR_NO_ERROR)
            {
            // Serious Problem
            ASSERT_NOW();
            return ErrRet;
            }

         if (tyStatus.bReducedPowerMode)
            {
            // We are waiting in Reduced Power Mode
            // We will never return from the single step unless we force it.
            ErrRet=ML_EnterDebugMode(ptyHandle);
            if (ErrRet!=ERR_NO_ERROR)
               return ErrRet;

            ErrRet=ML_GetStatus(ptyHandle,&tyStatus,TRUE); // OK to use target memory if necessary
            if (ErrRet!=ERR_NO_ERROR)
               {
               // Serious Problem
               ASSERT_NOW();
               return ErrRet;
               }

            if (!tyStatus.bInDebugMode ||
                tyStatus.bReducedPowerMode)
               {
               // Still not in Debug mode...
               // must be register controller PWR MGMT
               return ERR_PROC_HELD_IN_RPMODE;
               }
            }

         if (tyStatus.bInDebugMode)
            {
            // Back in debug mode
            // Execution has been halted
            bStillExecuting=FALSE;
            }

         ulCheckedControl++;

         if (ulCheckedControl>=MAX_WAIT_FOR_SINGLE_STEP)
            {
            // Never Returning from Single Step
            DLOG((LOGFILE,"\n\t\tML_SingleStep -> Failed to Return from SingleStep after %d checks",ulCheckedControl));
            ASSERT_NOW();
            return(ERR_SSTEP_DIDNT_RETURN); 
            }

         }

      (void)MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulCurrentPCValue); // What is it now 

      if (ulCurrentPCValue != ulOriginalPCValue)
         {
         // We must have done a good step.
         break;
         // NOTE: We only do this once as we may be on a JMP $ Statement.
         }
      else
         {
         DLOG((LOGFILE,"\n\t\tML_SingleStep -> Need to do an additional SingleStep. First one didn't work"));
         }
      }

   // Processor Has halted
   // Don't save context as get status will do that
   // Finally Clear the Single Step Flag
   // Clear Bit 8 of CP0 Debug Register
   (void)MLR_ReadRegister(ptyHandle,ptyHandle->ulCP0RegNumberforDebug,MDIMIPCP0,&ulDebugValue); // Get original value

   ulDebugValue = ulDebugValue & (0xFFFFFFFF - (0x1 << 8));         // Clear Bit 8

   (void)MLR_WriteRegister(ptyHandle,ptyHandle->ulCP0RegNumberforDebug,MDIMIPCP0,ulDebugValue); // Write back the value

   // Instead restore the value of CP0_R12
   if (ptyHandle->bDisableIntSS)
      {
      DLOG3((LOGFILE,"\n\t\tML_SingleStep -> Restore Interrupts to R12"));

      srStatusRegister.ulData = ptyHandle->ulBackupOfStatusRegister;
      (void)MLR_ReadRegister(ptyHandle,REG_NUM_STATUS, MDIMIPCP0,&srStatusRegisterAfterEmulation.ulData);
      // Now Restore the Mask bits from before the copy.
      srStatusRegisterAfterEmulation.Bits.IM  = srStatusRegister.Bits.IM;
      srStatusRegisterAfterEmulation.Bits.SWM = srStatusRegister.Bits.SWM;
      srStatusRegisterAfterEmulation.Bits.IEc = srStatusRegister.Bits.IEc;
      // Now Write back the Correct Version
      (void)MLR_WriteRegister(ptyHandle,REG_NUM_STATUS, MDIMIPCP0,srStatusRegisterAfterEmulation.ulData);
      // Now Don't do this the next time unless we have stepped again
      }

   return ErrRet;

}

/****************************************************************************
     Function: ML_Step
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
               ulMode : mode
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_Step(TyMLHandle    *ptyHandle,
                unsigned long ulMode)
{
   TyError ErrRet = ERR_NO_ERROR;
   unsigned long ulLocationOfPC;
   unsigned long ulLocationOfPCBPAccessAddr;

   unsigned long ulLocationOfBranchDelaySlot;
   unsigned long ulLocationOfBranchDelaySlotBPAccessAddr;

   TyBPType       bptBPType1;
   unsigned long  ulBPSize1;
   unsigned char  bNoBPPresent1;
   TyBPType       bptBPType2;
   unsigned long  ulBPSize2;
   unsigned char  bNoBPPresent2;


   if (ptyHandle->bProcessorExecuting)
      return ERR_DISSTEP_NOT_DURING_EXE;

   // Get Current PC Value
   ErrRet=MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulLocationOfPC);

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;
   ulLocationOfPCBPAccessAddr = ulLocationOfPC;


   /*Always remove 2 Breakpoints when stepping to cover the case where there is a Branch Delay Slot*/
   ulLocationOfBranchDelaySlot               = ulLocationOfPC + 4;
   ulLocationOfBranchDelaySlotBPAccessAddr   = ulLocationOfBranchDelaySlot;


   (void)MLB_ClearBP(ptyHandle,
                     ulLocationOfPC,
                     &bptBPType1,
                     &ulBPSize1,
                     &bNoBPPresent1,
                     &ulLocationOfPCBPAccessAddr);

   if (ptyHandle->bBrokeInMIPS16Mode)
      {
      // If we are in MIPS16 then we don't need to clear the BP in the BDS
      bNoBPPresent2  = TRUE;
      bptBPType2     = BP_NO;
      ulBPSize2      = 4;
      }
   else
      {
      // We must be in MIPS32. Therefore we must clear the BP in the BDS
      (void)MLB_ClearBP(ptyHandle,
                        ulLocationOfBranchDelaySlot,
                        &bptBPType2,
                        &ulBPSize2,
                        &bNoBPPresent2,
                        &ulLocationOfBranchDelaySlotBPAccessAddr);
      }

   // Check if we have any Hardware Breakpoints used and if so
   // we disable them. We restore them after the single step using 
   // MLB_RestoreHardwareBreakpoints
   if (MLB_CheckHWBPUsed(ptyHandle))
      {
      ErrRet=MLB_TempDisableAllHWBPs(ptyHandle);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }

   switch (ulMode)
      {
      default:
      case MODESTEPINTO:
         ErrRet = ML_SingleStep(ptyHandle);
         break;
      case MODESTEPFORWARD:
         ErrRet = ML_SWSingleStep(ptyHandle,FALSE);
         break;
      case MODESTEPOVER:
         ErrRet = ML_SWSingleStep(ptyHandle,TRUE);
         break;
      }

   // Restore Breakpoints if they were present
   // First Restore Complex Breakpoints
   if (MLB_CheckHWBPUsed(ptyHandle))
      {
      ErrRet=MLB_RestoreHWBPs(ptyHandle);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }


   if (!bNoBPPresent1)
      (void)MLB_SetBP(ptyHandle,
                      ulLocationOfPC,
                      ulLocationOfPCBPAccessAddr,
                      bptBPType1,ulBPSize1);
   if (!bNoBPPresent2)
      (void)MLB_SetBP(ptyHandle,
                      ulLocationOfBranchDelaySlot,
                      ulLocationOfBranchDelaySlotBPAccessAddr,
                      bptBPType2,ulBPSize2);


   if (ErrRet!=ERR_NO_ERROR)
      return ErrRet;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_Execute
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_Execute(TyMLHandle    *ptyHandle)
{
   TyError ErrRet = ERR_NO_ERROR;
   unsigned long ulLocationOfPC;
   unsigned long ulLocationOfPCBPAccessAddr;

   unsigned long ulLocationOfBranchDelaySlot;
   unsigned long ulLocationOfBranchDelaySlotBPAccessAddr;
   

   TyBPType       bptBPType1;
   unsigned long  ulBPSize1;
   TyBPType       bptBPType2;
   unsigned long  ulBPSize2;
   //TyStatus       tyStatus;
   unsigned char  bNoBPPresent2=1;
   unsigned char  bNoBPPresent1=1;
   unsigned char  bHardwareBreakpointsPresent;
   
   bptBPType2 = BP_NO;  // Keep Lint Happy
   ulBPSize2  = 4 ;     // Keep Lint Happy

   //ErrRet = ML_GetStatus(ptyHandle,&tyStatus,FALSE);    // Getting Status saves the context also

   if (ptyHandle->bProcessorExecuting)
      return ERR_TXRX_CONEXE_NOT_DURING_EXE;

   // Read Current PC Value
   ErrRet=MLR_ReadRegister(ptyHandle,0,MDIMIPPC,&ulLocationOfPC);

   if (ErrRet != ERR_NO_ERROR)
      {
      return ErrRet;
      }
   ulLocationOfPCBPAccessAddr = ulLocationOfPC;

   //Always remove 2 Breakpoints when stepping to cover the case where there is a Branch Delay Slot
   ulLocationOfBranchDelaySlot               = ulLocationOfPC + 4;
   ulLocationOfBranchDelaySlotBPAccessAddr   = ulLocationOfBranchDelaySlot;

   ErrRet=MLB_ClearBP(ptyHandle,ulLocationOfPC,&bptBPType1,&ulBPSize1,&bNoBPPresent1,&ulLocationOfPCBPAccessAddr);

   if (ptyHandle->bBrokeInMIPS16Mode)
      {
      // If we are in MIPS16 then we don't need to clear the BP in the BDS
      bNoBPPresent2 = TRUE;
      }
   else
      {
      // We must be in MIPS32. Therefore we must clear the BP in the BDS
      ErrRet = MLB_ClearBP(ptyHandle,ulLocationOfBranchDelaySlot,&bptBPType2,&ulBPSize2,&bNoBPPresent2,&ulLocationOfBranchDelaySlotBPAccessAddr);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }
	
   // Check if we have any Hardware Breakpoints used and if so
   // we disable them. We restore them after the single step using 
   // MLB_RestoreHardwareBreakpoints
   bHardwareBreakpointsPresent = MLB_CheckHWBPUsed(ptyHandle);
   if (bHardwareBreakpointsPresent)
      {
      ErrRet=MLB_TempDisableAllHWBPs(ptyHandle);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }
	
   // Single Step in if either location had a breakpoint set
   if (!bNoBPPresent1 || !bNoBPPresent2 || bHardwareBreakpointsPresent )
   //if (bHardwareBreakpointsPresent )
      {
      unsigned char   bNormalBootSelectedBackup = ptyHandle->bNormalBootSelected; // Boot to debug mode or normal mode?
      ErrRet = ML_SingleStep(ptyHandle);
      // Now retsore boottype
      (void)ML_SetBootType(ptyHandle, bNormalBootSelectedBackup);
      }

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;


   // The Restore Breakpoints
   // First Restore Complex Breakpoints
   if (bHardwareBreakpointsPresent)
      {
      ErrRet=MLB_RestoreHWBPs(ptyHandle);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }
	
   if (!bNoBPPresent1)
      ErrRet=MLB_SetBP(ptyHandle,ulLocationOfPC,ulLocationOfPCBPAccessAddr,bptBPType1,ulBPSize1);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   if (!bNoBPPresent2)
      ErrRet = MLB_SetBP(ptyHandle,ulLocationOfBranchDelaySlot,ulLocationOfBranchDelaySlotBPAccessAddr,bptBPType2,ulBPSize2);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;


   ErrRet=ML_StartExecution(ptyHandle);

   if (ErrRet!=ERR_NO_ERROR)
      {
      return ErrRet; 
      }

   return ERR_NO_ERROR;

}

/****************************************************************************
     Function: ML_RestoreShadowRegisterSet
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
static TyError ML_RestoreShadowRegisterSet(TyMLHandle    *ptyHandle)
{
   TyError ErrRet = ERR_NO_ERROR;  
   unsigned long ulSrsCtlORMask[MAX_NUM_SHADOW_REGISTER_SETS]  = {0,0,0};   
   unsigned long ulSrsCtlANDMask[MAX_NUM_SHADOW_REGISTER_SETS] = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};
   unsigned long ulNumberOfShadowSets;           
   unsigned int i,j;

   DLOG2((LOGFILE,"\n\t\tML_RestoreShadowRegisterSet -> Called"));

   //set up array of shadow masks
   ErrRet = ML_SetUpShadowMasks(ptyHandle, ulSrsCtlORMask, ulSrsCtlANDMask);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   ulNumberOfShadowSets = ptyHandle->ulNumberOfShadowSetsPresent;

   //check that number of shadow registers is set correctly at a value of 1 or 3 etc 
   ASSERT(ulNumberOfShadowSets > 0);
   ASSERT(ulNumberOfShadowSets <= MAX_NUM_SHADOW_REGISTER_SETS);
   ASSERT(ptyHandle->pulShadowRegisterSet != NULL);
   ASSERT(tyApp < LAST_APP); 

   // Restore the specified number of shadow register sets
   for (i=0; i < ulNumberOfShadowSets; i++)
      {
      DLOG4((LOGFILE,"\n\t\tML_RestoreShadowRegisterSet -> ulSRSORMask = 0x%08X",ulSrsCtlORMask[i] ));
      DLOG4((LOGFILE,"\n\t\tML_RestoreShadowRegisterSet -> ulSRSANDMask = 0x%08X",ulSrsCtlANDMask[i] ));       

      pulCommonDataToProc[0x0]      = ulSrsCtlORMask[i];  // Pass OR Mask to the DW2
      pulCommonDataToProc[0x1]      = ulSrsCtlANDMask[i]; // Pass AND Mask to the DW2

      for (j=0; j < NUM_SHADOW_REGISTERS; j++)    //pass in array of shadow registers to restored for this set
         pulCommonDataToProc[j+2] = ptyHandle->pulShadowRegisterSet[i][j];

      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulRestoreShadowRegisterSetApp,
                           ptyHandle->tyMipsDW2App.ulRestoreShadowRegisterSetInstructions * sizeof(unsigned long),
                           pulCommonDataToProc,
                           MAX_DW2_DATA, //todo: how much do we need?
                           pulCommonDataFromProc,
                           MAX_DW2_DATA); //todo: how much do we need?


      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }

   return ErrRet;
}

//todo remove !!!!!
unsigned long * LtoP(unsigned long value)
{
   static unsigned long x;

   x=value;

   return(&x);

}

/****************************************************************************
     Function: ML_ConvertVersionDate
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: none
  Description: Converts version data from DW to string
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
void ML_ConvertVersionDate(unsigned long ulVersion, 
                           unsigned long ulDate, 
                           char *pszBuffer, 
                           unsigned int uiSize)
{
   if ((pszBuffer == NULL) || (uiSize == 0))
      return;
   pszBuffer[0] = 0;             // empty string

   // converting version into string
   unsigned long ulVer1 = (ulVersion & 0xFF000000) >> 24;
   unsigned long ulVer2 = (ulVersion & 0x00FF0000) >> 16;
   unsigned long ulVer3 = (ulVersion & 0x0000FF00) >> 8;
   unsigned long ulVer4 = (ulVersion & 0x000000FF);
   bool bIsMajorRelease = true;

   // is it major release
   if (((ulVer4 >= 'A') && (ulVer4 <= 'Z')) || ((ulVer4 >= 'a') && (ulVer4 <= 'z')))
      {
      bIsMajorRelease = false;
      }

   // converting date into string (i.e. 02-Nov-2007)
   char pszLocMonth[16];
   unsigned long ulDay, ulMonth, ulYear;
   ulDay   = (ulDate & 0xFF000000) >> 24;
   ulMonth  = (ulDate & 0x00FF0000) >> 16;
   ulYear   = (ulDate & 0x0000FFFF);
   switch (ulMonth)
      {
      case 0x01:  strcpy(pszLocMonth, "Jan");   break;
      case 0x02:  strcpy(pszLocMonth, "Feb");   break;
      case 0x03:  strcpy(pszLocMonth, "Mar");   break;
      case 0x04:  strcpy(pszLocMonth, "Apr");   break;
      case 0x05:  strcpy(pszLocMonth, "May");   break;
      case 0x06:  strcpy(pszLocMonth, "Jun");   break;
      case 0x07:  strcpy(pszLocMonth, "Jul");   break;
      case 0x08:  strcpy(pszLocMonth, "Aug");   break;
      case 0x09:  strcpy(pszLocMonth, "Sep");   break;
      case 0x10:  strcpy(pszLocMonth, "Oct");   break;
      case 0x11:  strcpy(pszLocMonth, "Nov");   break;
      case 0x12:
      default:    strcpy(pszLocMonth, "Dec");   break;
      }

   if ( bIsMajorRelease )
      {
      (void)snprintf(pszBuffer, uiSize, "%02x-%s-%04x Version %x.%x.%x", 
                     (char)ulDay, pszLocMonth, (unsigned int)ulYear, 
                     (unsigned int)ulVer1, (unsigned int)ulVer2, (unsigned int)ulVer3);
      }
   else
      {
      (void)snprintf(pszBuffer, uiSize, "%02x-%s-%04x Version %x.%x.%x-%c", 
                     (char)ulDay, pszLocMonth, (unsigned int)ulYear, 
                     (unsigned int)ulVer1, (unsigned int)ulVer2, (unsigned int)ulVer3, (unsigned char)ulVer4);
      }

   // ensure string is always terminated correctly
   pszBuffer[uiSize - 1] = 0;                         
}

/****************************************************************************
     Function: ML_RunTargetCommand
     Engineer: Nikolay Chokoev
        Input: ptyHandle : handle
       Output: none
  Description: Run command on target
Date           Initials    Description
10-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_RunTargetCommand(TyMLHandle    *ptyHandle,
                            unsigned long ulAddressOfTargetApplication,
                            unsigned long *pulParamsPassed,
                            unsigned long ulNumberPassed,
                            unsigned long *pulParamsReturned,
                            unsigned long ulNumberReturned)
{

   TyError ErrRet = ERR_NO_ERROR;
   unsigned long ulIndex;

   DLOG2((LOGFILE,"\n\t\tML_RunTargetCommand -> Called"));
   DLOG3((LOGFILE,"\n\t\tML_RunTargetCommand -> About to Execute Target code at address 0x%08X",ulAddressOfTargetApplication));
   DLOG3((LOGFILE,"\n\t\tML_RunTargetCommand -> Say a quick prayer that we make it back in one piece !!"));
   dbgprint("ML_RunTargetCommand \n");
   pulCommonDataToProc[0x0] = ulAddressOfTargetApplication;
   pulCommonDataToProc[0x1] = ptyHandle->tyMipsDW2App.ulRunTargetInstrAddress;

   // Fill out the paramater array
   for (ulIndex = 0;ulIndex<ulNumberPassed;ulIndex++)
      pulCommonDataToProc[0x2 + ulIndex] = pulParamsPassed[ulIndex];

   ErrRet=ML_ExecuteDW2(ptyHandle,
                        ptyHandle->tyMipsDW2App.pulRunTargetCommandApp,
                        (ptyHandle->tyMipsDW2App.ulRunTargetCommandInstructions*sizeof(unsigned long)),
                        pulCommonDataToProc, (ulNumberPassed+2)*sizeof(unsigned long),
                        pulCommonDataFromProc, ulNumberReturned*sizeof(unsigned long));

   // Fill out the return array
   for (ulIndex = 0;ulIndex<ulNumberReturned;ulIndex++)
      pulCommonDataFromProc[0x0 + ulIndex] = pulParamsReturned[ulIndex];
   dbgprint("ML_RunTargetCommand: %d \n",ErrRet);
   return ErrRet;
}

/****************************************************************************
     Function: ParseIRDRString
     Engineer: Suresh P.C
        Input: pszInStr: Input string
       Output: pulOutValue: Value converted to array
  Description: Converts given string to unsinged long arrray to use in ScanIR
Date           Initials    Description
07-Oct-2008    SPC          Initial
*****************************************************************************/
int ParseIRDRString(const char *pszInStr, unsigned long *pulOutValue, unsigned long ulOutLen)
{
   char *pszLocValue;
   unsigned int uiStringPosition, uiUlongIndex = 0, uiUlongPosition = 0;
   unsigned char ucTempValue;

   uiStringPosition = strlen(pszInStr)-1;
   pszLocValue = (char *)(&(pszInStr[uiStringPosition]));
   // set unsigned long array to zero
   memset((void *)pulOutValue, 0x00, sizeof(unsigned long) * ulOutLen);
   while (uiStringPosition > 0)
      {
      switch (*pszLocValue)
         {
         case '0':
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':
            ucTempValue = (unsigned char)((*pszLocValue) - '0');
            break;

         case 'A' : 
         case 'B' : 
         case 'C' : 
         case 'D' : 
         case 'E' : 
         case 'F' : 
            ucTempValue = (unsigned char)(10 + ((*pszLocValue) - 'A'));
            break;
         case 'a' : 
         case 'b' : 
         case 'c' : 
         case 'd' : 
         case 'e' : 
         case 'f' : 
            ucTempValue = (unsigned char)(10 + ((*pszLocValue) - 'a'));
            break;

         case 'X' :     // terminating char, must be at position 1
         case 'x' :     // terminating char, must be at position 1
            if (uiStringPosition != 1)
               return FALSE;
            else
               return TRUE;               // everything done, valid parameters
         default:
            if ( (*pszLocValue == '\n') ||  (*pszLocValue == '\r') )
               {
               pszLocValue--;
               uiStringPosition--;
               continue;
               }
            else
               {
               return FALSE;
               }
         }

      // check ucTempValue and position in buffer (ignore other chars)
      if ((ucTempValue != 0) && (uiUlongIndex < ulOutLen))
         {
         // add next 4 bits
         pulOutValue[uiUlongIndex] |= (((unsigned long)(ucTempValue)) << (uiUlongPosition*4));
         }

      // move inside the buffer
      if ((++uiUlongPosition) > 7)
         {
         uiUlongIndex++;
         uiUlongPosition = 0;
         }

      pszLocValue--;
      uiStringPosition--;
      }
   return FALSE;
}

/****************************************************************************
     Function: ML_EnableTargetPasswd
     Engineer: Suresh P.C
        Input: ptyHandle : handle szPasswdFileName: File name for password
       Output: none
  Description: Enable Target with password. PNX 85500 support
Date           Initials    Description
07-Oct-2008    SPC          Initial
04-Jan-2008    SPC          Changed Password feature for to new requirement
*****************************************************************************/
/*TyError ML_EnableTargetPasswd(TyMLHandle 	*ptyHandle,
                       char szPasswdFileName[])
{
#define PSWD_MODE_LEN 728
#define MAX_IRDR_LEN 128
   const char PASSWD_MODE_IR_CMD[] = "0x08";
    const char PASSWD_MODE_IR_CMD1[] = "0x15";
    const char PASSWD_MODE_IR_CMD2[] = "0xaffff";
    const char PASSWD_MODE_DR_CMD1[] = "0xaabbccdd";
   const short PASSWD_MODE_IR_LEN   = 8;
   const char PASSWD_MODE_IR_LEN_85500 = 6;
    unsigned long PASSWD_MODE_IR_LEN_85500_1 = 21;
    unsigned long PASSWD_MODE_DR_LEN_85500_1 = 32;
    char PASSWD_MODE_DR_CMD[PSWD_MODE_LEN] = {0};
   char PASSWD_MODE_DR_CMD_85500[] = "0x00";
   const short PASSWD_MODE_DR_LEN   = 2899;
   const char PASSWD_MODE_DR_LEN_85500 = 4;

   const char PASSWD_ENTER_IR_CMD[] = "0x15";
   const short PASSWD_ENTER_IR_LEN   = 8;
   const short PASSWD_DR_LEN   = 73;
   const short PASSWD_DR_LEN_85500 = 77;

   FILE *fp;
   char szPasswdString[MAX_PASSWD_LEN];
   unsigned long pulScanIn[MAX_IRDR_LEN]={0};
   unsigned long pulScanOut[MAX_IRDR_LEN]={0};

   DLOG2((LOGFILE,"\n\t\tML_EnableTargetPasswd -> Called"));

   // Checking the file for password
   fp = fopen(szPasswdFileName,"r");
   if (fp == NULL) {
      return ERR_UNKNOWN_ERROR;
   }
    if(!fgets(szPasswdString,MAX_PASSWD_LEN,fp)){
        fclose(fp);
      return ERR_UNKNOWN_ERROR;
    }
    fclose(fp);


   if (!strcmp(ptyHandle->pszProcName,"NXP PNX8543 - 4KEc (14pin)"))
   {

      //Setting 27th location to 1
      for (int i=0; i<PSWD_MODE_LEN-1; i++)
      {
         PASSWD_MODE_DR_CMD[i] = '0';
      }
      PASSWD_MODE_DR_CMD[1] = 'x';
      PASSWD_MODE_DR_CMD[26] = '1';

      //	  Password sequence for 8543.
      //    shir 0x08 8
      //    shdr 0x0000000000000000000000001(0...0)700 Zero's 2899
      //    # Wait for entry into mode
      //    wait 100
      //    # Load Password from password file.
      //    shir 0x15 8
      //    shdr <password> 73
      //    # Wait for check of password
      //    wait 2000

      (void)ParseIRDRString(PASSWD_MODE_IR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_MODE_IR_LEN,pulScanIn,pulScanOut);
      (void)ParseIRDRString(PASSWD_MODE_DR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_MODE_DR_LEN,pulScanIn,pulScanOut);
      ML_Delay(200);

      (void)ParseIRDRString(PASSWD_ENTER_IR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_ENTER_IR_LEN,pulScanIn,pulScanOut);
      (void)ParseIRDRString(szPasswdString,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_DR_LEN, pulScanIn, pulScanOut);
      ML_Delay(2000);
   }
   else if(!(strcmp(ptyHandle->pszProcName,"NXP PNX85500 - MIPS2 (4Kec)")
         &&strcmp(ptyHandle->pszProcName,"NXP PNX85500 - MIPS1 (24K)")))
   {
      //  Password sequence for 85500
      //  shir 0x08 6
      //	shdr 0x0 4			
      //	shdr <password in here> 77			
      //	wait 2000
      //	shdr 0x0 4

      (void)ParseIRDRString(PASSWD_MODE_IR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_MODE_IR_LEN_85500,pulScanIn,NULL);
      (void)ParseIRDRString(PASSWD_MODE_DR_CMD_85500,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_MODE_DR_LEN_85500,pulScanIn,NULL);
      ML_Delay(200);

      (void)ParseIRDRString(szPasswdString,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_DR_LEN_85500, pulScanIn, pulScanOut);
      ML_Delay(2000);
      (void)ParseIRDRString(PASSWD_MODE_DR_CMD_85500,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_MODE_DR_LEN_85500,pulScanIn,NULL);


   }

   return ERR_NO_ERROR;
}*/

/****************************************************************************
     Function: ML_EnableTargetPasswdSetting
     Engineer: Suresh P.C
        Input: ptyHandle : handle szPasswdFileName: File name for password
       Output: none
  Description: Enable Target with password. PNX 85500 support
Date           Initials    Description
07-Oct-2008    SPC          Initial
04-Jan-2008    SPC          Changed Password feature for to new requirement
04-Dec-2009    DK           Changed Password feature for 85500M1 Processor
*****************************************************************************/
TyError ML_EnableTargetPasswdSetting(TyMLHandle    *ptyHandle,
                                     TyUserSettings *ptyUserSettings)
{
#define PSWD_MODE_LEN 728
#define MAX_IRDR_LEN 128
   const char PASSWD_MODE_IR_CMD[] = "0x08";
   const short PASSWD_MODE_IR_LEN   = 8;
   char PASSWD_MODE_DR_CMD[PSWD_MODE_LEN] = {0};
   const short PASSWD_MODE_DR_LEN   = 2899;
   const char PASSWD_ENTER_IR_CMD[] = "0x15";
   const short PASSWD_ENTER_IR_LEN   = 8;
   const short PASSWD_DR_LEN   = 73;
   FILE *fp;
   char szPasswdString1[MAX_PASSWD_LEN];
   char szPasswdString2[MAX_PASSWD_LEN];
   char szPasswdString3[MAX_PASSWD_LEN];
   char szPasswdFileName[MAX_PASSWD_LEN];
   unsigned long pulScanIn[MAX_IRDR_LEN]={0};
   unsigned long pulScanOut[MAX_IRDR_LEN]={0};

   DLOG2((LOGFILE,"\n\t\tML_EnableTargetPasswdSetting -> Called"));

   if (!strcmp(ptyUserSettings->szPasswordSequence,"8543"))
      {
      strcpy(szPasswdFileName , ptyUserSettings->szPasswordFile1);
      // Checking the file for password
      fp = fopen(szPasswdFileName,"r");
      if (fp == NULL)
         return ERR_UNKNOWN_ERROR;
      if (!fgets(szPasswdString1,MAX_PASSWD_LEN,fp))
         {
         fclose(fp);
         return ERR_UNKNOWN_ERROR;
         }
      fclose(fp);

      //Setting 27th location to 1
      for (int i=0; i<PSWD_MODE_LEN-1; i++)
         {
         PASSWD_MODE_DR_CMD[i] = '0';
         }
      PASSWD_MODE_DR_CMD[1] = 'x';
      PASSWD_MODE_DR_CMD[26] = '1';

      //	  Password sequence for 8543.
      //    shir 0x08 8
      //    shdr 0x0000000000000000000000001(0...0)700 Zero's 2899
      //    # Wait for entry into mode
      //    wait 100
      //    # Load Password from password file.
      //    shir 0x15 8
      //    shdr <password> 73
      //    # Wait for check of password
      //    wait 2000

      (void)ParseIRDRString(PASSWD_MODE_IR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_MODE_IR_LEN,pulScanIn,pulScanOut);
      (void)ParseIRDRString(PASSWD_MODE_DR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_MODE_DR_LEN,pulScanIn,pulScanOut);
      ML_Delay(200);

      (void)ParseIRDRString(PASSWD_ENTER_IR_CMD,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_ENTER_IR_LEN,pulScanIn,pulScanOut);
      (void)ParseIRDRString(szPasswdString1,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_DR_LEN, pulScanIn, pulScanOut);
      ML_Delay(2000);
      }
   else if (!(strcmp(ptyUserSettings->szPasswordSequence,"85500M1")))
      {
      //
      //Setting PassWord1
      //
      strcpy(szPasswdFileName , ptyUserSettings->szPasswordFile1);
      fp = fopen(szPasswdFileName,"r");
      if (fp == NULL)
         {
         return ERR_UNKNOWN_ERROR;
         }
      if (!fgets(szPasswdString1,MAX_PASSWD_LEN,fp))
         {
         fclose(fp);
         return ERR_UNKNOWN_ERROR;
         }
      fclose(fp);
      ML_PasswordSetting( ptyHandle , szPasswdString1 );
      //
      //Setting PassWord2
      //
      strcpy(szPasswdFileName , ptyUserSettings->szPasswordFile2);
      fp = fopen(szPasswdFileName,"r");
      if (fp == NULL)
         {
         return ERR_UNKNOWN_ERROR;
         }
      if (!fgets(szPasswdString2,MAX_PASSWD_LEN,fp))
         {
         fclose(fp);
         return ERR_UNKNOWN_ERROR;
         }
      fclose(fp);
      ML_PasswordSetting( ptyHandle , szPasswdString2 );
      //#
      //Setting PassWord3
      //
      strcpy(szPasswdFileName , ptyUserSettings->szPasswordFile3);
      fp = fopen(szPasswdFileName,"r");
      if (fp == NULL)
         {
         return ERR_UNKNOWN_ERROR;
         }
      if (!fgets(szPasswdString3,MAX_PASSWD_LEN,fp))
         {
         fclose(fp);
         return ERR_UNKNOWN_ERROR;
         }
      fclose(fp);
      ML_PasswordSetting( ptyHandle , szPasswdString3 );
     }

   return ERR_NO_ERROR;
}
/****************************************************************************
     Function: ML_PasswordSetting
     Engineer: Dileep Kumar K
        Input: ptyHandle : handle szPasswdFileName: File name for password
       Output: none
  Description: Enable password. PNX 85500 support
Date           Initials    Description
04-Dec-2009    DK          Initial
*****************************************************************************/
void  ML_PasswordSetting( TyMLHandle    *ptyHandle ,char *PasswordString ) 
{ 
   const char PASSWD_MODE_IR_CMD[] = "0x08";
   //const char PASSWD_MODE_IR_CMD1[] = "0x15";
   //const char PASSWD_MODE_IR_CMD2[] = "0xaffff";
   //const char PASSWD_MODE_DR_CMD1[] = "0xaabbccdd";
   const char PASSWD_MODE_IR_LEN_85500 = 6;
   //unsigned long PASSWD_MODE_IR_LEN_85500_1 = 21;
   //unsigned long PASSWD_MODE_DR_LEN_85500_1 = 32;
   char PASSWD_MODE_DR_CMD_85500[] = "0x00";
   const char PASSWD_MODE_DR_LEN_85500 = 4;
   // const char PASSWD_ENTER_IR_CMD[] = "0x15";
   const short PASSWD_DR_LEN_85500 = 77;
   unsigned long pulScanIn[MAX_IRDR_LEN]={0};
   unsigned long pulScanOut[MAX_IRDR_LEN]={0};
   // Password sequence for 85500
   // shir 0x08 6
   // shdr 0x0 4
   // shdr 0x1880e953d0e0b3482873 77
   // wait 2000
   // shir 0x15 6
   // shir 0xaffff 21
   // shdr 0xaabbccdd 32
   // wait 1000
   (void)ParseIRDRString(PASSWD_MODE_IR_CMD,pulScanIn, MAX_IRDR_LEN);
   (void)ML_ScanIR(ptyHandle, PASSWD_MODE_IR_LEN_85500,pulScanIn,NULL);
   (void)ParseIRDRString(PASSWD_MODE_DR_CMD_85500,pulScanIn, MAX_IRDR_LEN);
   (void)ML_ScanDR(ptyHandle, PASSWD_MODE_DR_LEN_85500,pulScanIn,NULL);
   
   (void)ParseIRDRString(PasswordString,pulScanIn, MAX_IRDR_LEN);
   (void)ML_ScanDR(ptyHandle, PASSWD_DR_LEN_85500, pulScanIn, pulScanOut);
   ML_Delay(2000);
   /*(void)ParseIRDRString(PASSWD_MODE_IR_CMD1,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_MODE_IR_LEN_85500,pulScanIn,NULL);

      (void)ParseIRDRString(PASSWD_MODE_IR_CMD2,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanIR(ptyHandle, PASSWD_MODE_IR_LEN_85500_1,pulScanIn,NULL);

      (void)ParseIRDRString(PASSWD_MODE_DR_CMD1,pulScanIn, MAX_IRDR_LEN);
      (void)ML_ScanDR(ptyHandle, PASSWD_MODE_DR_LEN_85500_1,pulScanIn,NULL);
      ML_Delay(1000);*/
}
   
   /****************************************************************************
        Function: ML_LoadDW2Applications
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Attempt target connection, reset processor and halt in
               debug mode.
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
TyError ML_LoadDW2Applications ( TyApplicationSpecs   *ptyAppSpecArray,
                                 TyApplicationType    atApp,
                                 unsigned long  **pulApp,
                                 unsigned long  *pulAppSize,
                                 unsigned long  *pulAppStartAddress,
                                 unsigned long  *pulInstrOffset,
                                 TySymbolIdentifier tySimbol)
{
   TyError ErrRet = ERR_NO_ERROR;
   CLoadSrecFile SrecLoad;
   NOREF(atApp);
   ErrRet=SrecLoad.ProcessSRecFile( ptyAppSpecArray[atApp].szSrecFilename,
                                    0x0,
                                    0x0,
                                    0xFFFFFFFF,
                                    ptyAppSpecArray[atApp].ulNewLocation,
                                    pulApp,
                                    pulAppSize,
                                    pulAppStartAddress,
                                    pulInstrOffset,
                                    tySimbol);

   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   return ErrRet;
}


