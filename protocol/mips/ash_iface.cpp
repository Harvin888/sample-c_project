/****************************************************************************
       Module: ash_iface.cpp
     Engineer: Nikolay Chokoev
  Description: Implementation of Opella-XD MIPS MDI top layer interface.
Date           Initials    Description
04-Jul-2008    NCH         Initial
20-Nov-2008    SPC         Updated Error message generation
****************************************************************************/
#define MDI_LIB               // We are an MDI Library not a debugger

#include <string.h>
#include <stdio.h>
#define DEFINE_ERROR_STRUCT
#include "protocol/common/drvopxd.h"
#include "commdefs.h"
#include "gerror.h"
#include "midlayer.h"
#include "mdi.h"
#include "ash_iface.h"
#include "toplayer.h"
#include "debug.h"
#include "../../utility/debug/debug.h"
static char szCurrentErrorMessage[0x500];

extern TyLocMdiParams_t tyLocMdiPrms; //local for MDI
extern TyMLHandle *ptyMLHandle;

static unsigned long AdditionalTests(void);

/****************************************************************************
     Function: AshGetDWFWVersions
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT unsigned long AshGetDWFWVersions   (char *pszDiskware1Version,
                                                              char *pszFirmwareVersion,
                                                              char *pszFpgaware1Version,
                                                              char *pszFpgaware2Version,
                                                              char *pszFpgaware3Version,
                                                              char *pszFpgaware4Version)
{
   TyError ErrRet;

   ErrRet= TL_GetDWFWVersions   (pszDiskware1Version,
                                 pszFirmwareVersion,
                                 pszFpgaware1Version,
                                 pszFpgaware2Version,
                                 pszFpgaware3Version,
                                 pszFpgaware4Version);

//todo:log   TL_MDILog("\nAshGetDWFWVersions: Called");

   if (ErrRet != ERR_NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return (unsigned long)MDIErrFailure;
      }

   return MDISuccess;
}

/****************************************************************************
     Function: AshErrorGetMessage
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT unsigned long AshErrorGetMessage(char ** ppszErrorMsg)
{
   *ppszErrorMsg = (char *)szCurrentErrorMessage;
   
//todo:log   TL_MDILog("\nAshErrorGetMessage: %s",szCurrentErrorMessage);

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: AshScanIR
     Engineer: Nikolay Chokoev
        Input: ulIRLength - length of IR register, valid range is <1;8160>
             : pulDataOut : pointer to data to be shifted into scanchain, 
                              if NULL, all 1s are used
             : pulDataIn : pointer to data to be shifted from scanchain, 
                              if NULL, output data are ignored
       Output: Error code
  Description: shifts specific data into IR register of selected core 
               on scanchain (other cores in bypass mode) and get output data
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT void AshScanIR(unsigned long *pulDataOut, 
                                         unsigned long *pulDataIn, 
                                         unsigned long ulTargetIRLength)
{

   // just scan IR and save output, turn off multicore support for this operation
   ML_ConfigureMulticoreSupport(ptyMLHandle,TRUE);
   (void)DL_OPXD_JtagScanIR(ptyMLHandle->iDLHandle,
                            0, 
                            ulTargetIRLength, 
                            pulDataOut, 
                            pulDataIn);
   ML_ConfigureMulticoreSupport(ptyMLHandle,FALSE);
}

/****************************************************************************
     Function: AshScanDR
     Engineer: Nikolay Chokoev
        Input: ulIRLength - length of IR register, valid range is <1;8160>
             : pulDataOut : pointer to data to be shifted into scanchain, 
                              if NULL, all 1s are used
             : pulDataIn : pointer to data to be shifted from scanchain, 
                              if NULL, output data are ignored
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT void AshScanDR(unsigned long *pulDataOut, 
                                         unsigned long *pulDataIn, 
                                         unsigned long ulTargetDRLength)
{
   // just scan DR and save output, turn off multicore support
   ML_ConfigureMulticoreSupport(ptyMLHandle,TRUE);
   (void)DL_OPXD_JtagScanDR(ptyMLHandle->iDLHandle,
                            0, 
                            ulTargetDRLength, 
                            pulDataOut, 
                            pulDataIn);
   ML_ConfigureMulticoreSupport(ptyMLHandle,FALSE);
}

/****************************************************************************
     Function: AshSetJtagFrequency
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT unsigned long AshSetJtagFrequency(double udNewJtagRatio)
{  
   //in case of Opella-XD we passes the frequency in MHz
   return (unsigned long)DL_OPXD_JtagSetFrequency(ptyMLHandle->iDLHandle, 
                                                  udNewJtagRatio, NULL, NULL);
}

/****************************************************************************
     Function: AshMDIOpenEx
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT MDIInt32 AshMDIOpenEx (MDIHandleT  MDIHandle,
                                                 MDIDeviceIdT DeviceID,
                                                 MDIUint32 Flags,
                                                 MDIHandleT* pDeviceHandle,
                                                 int bInitialAccess)
{
   TyError  ErrRet = ERR_NO_ERROR;
   dbgprint("dbg:AshMDIOpenEx\n");
   // if not supporting diag tools, just call MDIOpen
   if(bInitialAccess)
      return MDIOpen(MDIHandle, DeviceID, Flags, pDeviceHandle);

   if (MDIHandle != tyLocMdiPrms.tyCurrentMDIHandle)
      {
//todo:log      TL_MDILog("\nMDIOpen: ");
      ErrRet = MDIErrTGHandle;
      goto MDIOpenError;
      }

   if (Flags == MDISharedAccess)
      {
      ErrRet = MDIErrParam; // Shared Access not supported 
      goto MDIOpenError;
      }

   if (!(Flags & MDIExclusiveAccess))
      {
      ErrRet = MDIErrParam; // If not Exclusive then what ?? 
      goto MDIOpenError;
      }

   // TODO: Error Check for maximum devices.

   tyLocMdiPrms.tyCurrentDeviceID = DeviceID; // Set local copy of Device ID
   // We don't do anything in particular with it for now, but it might be useful later.

   *pDeviceHandle = tyLocMdiPrms.tyCurrentDeviceID;

   ErrRet = TL_DeviceConnect_Ex(bInitialAccess);

   if (ErrRet != ERR_NO_ERROR)
      {
      tyLocMdiPrms.bFailedToConnectToTarget=TRUE;
      SetupErrorMessage(ErrRet);   
      ErrRet = MDIErrFailure;
      goto MDIOpenError;
      }

   ErrRet = MDISuccess;

MDIOpenError:
//todo:log   TL_MDILog("\nMDIOpen: MDIHandle: %08X  DeviceID: %08X Flags: %08X DeviceHandle:  %08X",MDIHandle,DeviceID,Flags,*pDeviceHandle);
//todo:log   TL_MDILogError(ErrRet);
   return ErrRet;
}

/****************************************************************************
     Function: AshScanDRLong
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT void AshScanDRLong(unsigned long *pulDataOut, 
                                             unsigned long *pulDataIn, 
                                             unsigned long ulTargetDRLength)
{
   NOREF(pulDataOut);
   NOREF(pulDataIn);
   NOREF(ulTargetDRLength);
   dbgprint("dbg:AshScanDRLong\n");
   // not implemented for Opella-XD
}

/****************************************************************************
     Function: AshListOpellaXDs
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
extern "C" ASH_DLL_EXPORT void AshListOpellaXDs(char ppszDeviceNumbers[][16], 
                                                unsigned short *pusNumberOfDevices, 
                                                unsigned short usMaxNumberOfDevices)
{
   unsigned short usNumberOfDevices, usCnt;
   char ppszLocDeviceNumbers[MAX_DEVICES_SUPPORTED][16];

   DL_OPXD_GetConnectedDevices(ppszLocDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
   
   if (usMaxNumberOfDevices > usNumberOfDevices)
	   usMaxNumberOfDevices = usNumberOfDevices;
   
   // copy detected serial numbers
   for (usCnt=0; usCnt < usMaxNumberOfDevices; usCnt++)
   {
		memcpy((void *)ppszDeviceNumbers[usCnt], (void *)ppszLocDeviceNumbers[usCnt], 16);		
   }

   if (pusNumberOfDevices != NULL)
   {
		if (usMaxNumberOfDevices < usNumberOfDevices)
			*pusNumberOfDevices = usMaxNumberOfDevices;
		else
			*pusNumberOfDevices = usNumberOfDevices;
   }
}



/****************************************************************************
     Function: SetupErrorMessage
     Engineer: Nikolay Chokoev
        Input: iErrorNumber : Error number.
       Output: None
  Description: Set Cause of Error which DiGetErrorMessage will report
Date           Initials    Description
04-Jul-2008    NCH         Initial
20-Nov-2008    SPC         Error message generation updated to print error
                           numbers.
****************************************************************************/
void SetupErrorMessage(int iErrorNumber)
{

//todo: define remove
#if 0// GDBSERVERDLL
   if (iErrorNumber >= ERR_LAST_ERROR)
      iErrorNumber = ERR_UNKNOWN_ERROR;
   strcpy(szCurrentErrorMessage,tyError[iErrorNumber].pszErrorString);

#else
   char szLocal[100];
   
   if (iErrorNumber >= ERR_LAST_ERROR)
   {
       // Don't crash if an invalid error number occurs
       DLOG((LOGFILE,"\nSetupErrorMessage : Invalid Error %d",iErrorNumber));
       iErrorNumber = ERR_UNKNOWN_ERROR;
   }
   
   if (iErrorNumber < ERR_LAST_ERROR)
   {
       sprintf(szCurrentErrorMessage,"Error %d: %s",iErrorNumber, tyError[iErrorNumber].pszErrorString);
   }
   else
   {
       strcpy(szCurrentErrorMessage,"Internal Error. Please Contact mips@ashling.com");
       sprintf(szLocal,"\nError Number : %d",iErrorNumber);
       strcat(szCurrentErrorMessage,szLocal);
       return;
   }
#endif
}

/****************************************************************************
     Function: AshConsole
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
void AshConsole (unsigned long ulNumberOfLoops, int  bAdditionalTests)
{
   TyError ErrRet;
   static FILE * pErrorFile;
   unsigned long ulNumberOfReset=0;
   unsigned long ulNumberOfErrors=0;
   unsigned int uiIndex;
   unsigned long ulAdditionalErrors =0;
   
   pErrorFile = fopen("C:\\miperr.txt","at");  \

   printf("\nAshling Opella Stress Test Workbench  Version 2.0.0");
   printf("\n\nThis test generates 2 files C:\\MIPSERR.TXT and C:\\mipslog.txt");
   printf("\nPlease note: This may take a long time to complete\n\n");

   for (uiIndex=0;uiIndex<ulNumberOfLoops;uiIndex++)
      {
      ErrRet=TL_DeviceConnect();
      if (ErrRet!=0)
         {
         ulNumberOfErrors++;
         }
      else
         {
         if (bAdditionalTests)
            {
            // Do More Tests Here
            ulAdditionalErrors += AdditionalTests();
            }
         }
      ulNumberOfReset++;
      printf("\nErrCode = %lu\tTotal Resets = %lu\tRst Errs = %lu\tPostRst Errors %lu", (unsigned long)ErrRet,ulNumberOfReset,ulNumberOfErrors,ulAdditionalErrors);
      fprintf(pErrorFile,"\nErrCode = %lu\tTotal Resets = %lu\tRst Errs = %lu\tPostRst Errors %lu", (unsigned long)ErrRet,ulNumberOfReset,ulNumberOfErrors,ulAdditionalErrors);
      }
   fclose(pErrorFile);       

   printf("\n\nTest Complete\n");
   if (ulNumberOfErrors != 0 || ulAdditionalErrors != 0)
      {
      printf("\n********* This System has FAILED the test! ******************");
      printf("\nTotal Resets              = %lu",ulNumberOfReset);
      printf("\nTotal Reset Failures      = %lu",ulNumberOfErrors);
      printf("\nTotal Post Reset Failures = %lu\n\n",ulAdditionalErrors);
      }
   else
      {
      printf("\n********* This System has PASSED the test! ******************");
      printf("\nTotal Resets              = %lu\n\n",ulNumberOfReset);
      }

   return;
}

static unsigned long AdditionalTests (void)
{
   unsigned long ulIndex;
   unsigned long ErrRet;
   unsigned long ulReadValue;
   unsigned long ulWriteValue;
   unsigned long ulNumberOfErrors = 0;

   for (ulIndex=0;ulIndex < 20; ulIndex++)
      {
      // Write to EPC, Read back from EPC
      // EPC is a full access 32 bit register and is CP0 Reg 14 in
      // both EJTAG sytle 2.5 and 2.0 

      ulWriteValue = 0x13476297 * ulIndex;
      ulReadValue  = 0x0;

      ErrRet=(unsigned long)TL_RegisterWrite(MDIMIPCP0,
                                             (MDIOffsetT)14,
                                             &ulWriteValue,
                                             1);
      
      if (ErrRet != 0)
         {
         ulNumberOfErrors++;
         }

      ErrRet=(unsigned long)TL_RegisterRead(MDIMIPCP0,
                                            (MDIOffsetT)14,
                                            &ulReadValue,
                                            1);
      
      if (ErrRet != 0)
         {
         ulNumberOfErrors++;
         }

      if (ulReadValue != ulWriteValue)
         {
         ulNumberOfErrors++;
         }
      }

   return ulNumberOfErrors;
}

#if 0000
/****************************************************************************
     Function: AshWriteEtherDetails
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long   AshWriteEtherDetails(  int iPortNumber, 
                                       TyBaudRate tyBaudRate, 
                                       char *pszIPAddress, 
                                       char *pszSubnetAddress, 
                                       char *pszGatewayAddress)
{

   TyError ErrRet;

   ErrRet= TL_WriteEtherDetails(iPortNumber,        
                                tyBaudRate,  
                                pszIPAddress,     
                                pszSubnetAddress, 
                                pszGatewayAddress);
   
   if (ErrRet != ERR_NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return (unsigned long)MDIErrFailure;
      }

   return MDISuccess;
}

/****************************************************************************
     Function: AshReadEtherDetails
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long AshReadEtherDetails(int iPortNumber, 
                                  TyBaudRate tyBaudRate, 
                                  char *pszIPAddress, 
                                  char *pszSubnetAddress, 
                                  char *pszGatewayAddress, 
                                  char *pszEthernetAddress)
{
   TyError ErrRet;

   ErrRet = TL_ReadEtherDetails(iPortNumber,         
                                tyBaudRate,   
                                pszIPAddress,      
                                pszSubnetAddress,  
                                pszGatewayAddress, 
                                pszEthernetAddress);

   if (ErrRet != ERR_NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return (unsigned long)MDIErrFailure;
      }

   return MDISuccess;
}

/****************************************************************************
     Function: AshGetSetClearAuxIO
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long AshGetSetClearAuxIO(unsigned char  ucAuxIOType, 
                                  BOOL           *pbAuxIOState)
{
   TyError ErrRet;

   ErrRet = TL_GetSetClearAuxIO(ucAuxIOType,pbAuxIOState);

   if (ErrRet != ERR_NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return (unsigned long)MDIErrFailure;
      }

   return MDISuccess;
}

/****************************************************************************
     Function: AshGetTraceFrames
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long AshGetTraceFrames(unsigned long ulNumberOfFrames,
                                unsigned long ulStartFrame,
                                TyTrFrame    *ptyTraceFrame)
{
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: AshGetTraceFramesCount
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long AshGetTraceFramesCount(unsigned long *pulNumberOfFrames)
{
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: AshArmDisarmTrace
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long AshArmDisarmTrace(BOOL bArm)
{
   return ERR_NO_ERROR;
}   

/****************************************************************************
     Function: AshSetupTriggers
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long  AshSetupTriggers(TyMIPSTraceTriggersSetup tyTraceTriggersSetup)
{
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: AshGetTraceRAMSize
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
14-Jul-2008    NCH          Initial
*****************************************************************************/
unsigned long AshGetTraceRAMSize(unsigned long * pulNumberOfTraceRacks)
{
   return ERR_NO_ERROR;
}  

#endif



