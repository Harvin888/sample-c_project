/****************************************************************************
       Module: ml_tlb.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This section contains all functions related to MIPS TLB and
               MMU.
Date           Initials    Description
08-Feb-2008    VH          Initial
19-Dec-2008    RS          Fixed Read/Write TLB issue.
06-Aug-2009    SPC         TLB functions added for OS MMU 
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#ifndef __LINUX
// Windows specific includes
#else
// Linux includes
#endif
#include "firmware/export/api_def.h"
#include "commdefs.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "midlayer.h"
#include "gerror.h"
#include "ml_tlb.h"

#define TLB_ENTRY_SIZE              16      // size of TLB entry
#define Config1RegNo                16
#define Config1RegSel               1
#define MMUSizeBitMask              0x7E000000
#define MMUSizeBitShift             25

static unsigned short usTlbCount = 0;
static TyTLBEntry *pEntries ;

static void GetTLBCount( TyMLHandle *ptyHandle, unsigned short *usCount );

/****************************************************************************
     Function: MLT_ReadTLB
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddres - address for TLB
               void *pvTLBEntry - pointer to store TLB entries
               unsigned long ulCount - number of entries
               unsigned long ulObjectSize - size for each TLB entry
       Output: int - error code
  Description: read MIPS processor TLB entries
Date           Initials    Description
08-Feb-2008    VH          Initial
19-Dec-2008    RS          Fixed TLB read issue
****************************************************************************/
int MLT_ReadTLB(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvTLBEntry, unsigned long ulCount, unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   unsigned long pulDW2DataToProc[2];
   unsigned long *pulDW2DataFromProc;
   // check parameters
   assert(ptyHandle != NULL);
   if ( (pvTLBEntry == NULL) || (ulCount == 0) || (ulObjectSize == 0) )
      return ERR_NO_ERROR;
   //We are allocating one word more
   pulDW2DataFromProc = (unsigned long *)malloc((ulCount+4)*ulObjectSize);
   // calculate start and end index (end index is last index + 1) as required by ReadTLB DW2 app
   pulDW2DataToProc[0] = (ulAddress * 4) / TLB_ENTRY_SIZE;                                // 1st parameter is start index
   pulDW2DataToProc[1] = ((ulAddress * 4) + (ulCount * ulObjectSize)) / TLB_ENTRY_SIZE;   // 2nd parameter is end index
   // run DW2 app and get result (TLB entries does not need any conversion, directly taking them)
   iResult = ML_ExecuteDW2(ptyHandle, 
                           (void *)(ptyHandle->tyMipsDW2App.pulReadTLBApp), 
                           ptyHandle->tyMipsDW2App.ulReadTLBInstructions * sizeof(unsigned long),
                           (void *)pulDW2DataToProc, 
                           2*sizeof(unsigned long), 
                           (void *)pulDW2DataFromProc, 
                           (ulCount+4) * ulObjectSize);
   if ( iResult != ERR_NO_ERROR )
      memset(pvTLBEntry, 0, ulCount * ulObjectSize);                                      // return 0 when error occured
   else
      if ( pulDW2DataFromProc )
         memcpy((void *)pvTLBEntry,(void *)(pulDW2DataFromProc+0x4),(size_t)(ulCount * ulObjectSize));

   free(pulDW2DataFromProc);
   return iResult;
}

/****************************************************************************
     Function: MLT_WriteTLB
     Engineer: vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddres - address for TLB
               void *pvTLBEntry - pointer to TLB entries
               unsigned long ulCount - number of entries
               unsigned long ulObjectSize - size for each TLB entry
       Output: int - error code
  Description: write MIPS processor TLB Entries
Date           Initials    Description
08-Feb-2008    VH          Initial
19-Dec-2008    RS          Fixed TLB Write issue
****************************************************************************/
int MLT_WriteTLB(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvTLBEntry, unsigned long ulCount, unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   unsigned long ulIndex;
   unsigned long pulDW2DataToProc[5];
   unsigned char *pucTLBEntry = (unsigned char *)pvTLBEntry;
   // check parameters
   assert(ptyHandle != NULL);
   if ( (pucTLBEntry == NULL) || (ulCount == 0) || (ulObjectSize == 0) )
      return ERR_NO_ERROR;
   // write TLB entries one by one in loop
   for ( ulIndex=0; ulIndex < ulCount; ulIndex+=ulObjectSize )
      {
      pulDW2DataToProc[0] = ((ulAddress * 4) + (ulIndex * ulObjectSize)) / TLB_ENTRY_SIZE;   // 1st parameter is index
      memcpy(pulDW2DataToProc+1,pucTLBEntry, TLB_ENTRY_SIZE);   // other parameters are TLB entry values
      iResult = ML_ExecuteDW2(ptyHandle, 
                              (void *)(ptyHandle->tyMipsDW2App.pulWriteTLBApp), 
                              ptyHandle->tyMipsDW2App.ulWriteTLBInstructions * sizeof(unsigned long),
                              (void *)pulDW2DataToProc, 
                              5*sizeof(unsigned long), 
                              NULL, 
                              0);
      if ( iResult != ERR_NO_ERROR )
         break;
      pucTLBEntry += TLB_ENTRY_SIZE;   // increase pointer in TLB entries
      }
   return iResult;
}

/****************************************************************************
     Function: GetTLBCount
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: unsigned short *usCount - TLB Count
  Description: Retuns the the TLB size available in a MIPS target
Date           Initials    Description
11-Aug-2009    SPC          Initial
****************************************************************************/
static void GetTLBCount( TyMLHandle *ptyHandle, unsigned short *usCount )
{
   unsigned long ulConfig1Val;
   unsigned int uiMMUSizeMasked;

   (void) MLR_ReadCP0Register(ptyHandle,Config1RegNo,&ulConfig1Val,Config1RegSel);
   uiMMUSizeMasked = ulConfig1Val & MMUSizeBitMask;
   *usCount = (uiMMUSizeMasked >> MMUSizeBitShift)+1;
}

/****************************************************************************
     Function: CreateBackupOfTLB
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: none
  Description: Backup the target TLB entries
Date           Initials    Description
11-Aug-2009    SPC          Initial
****************************************************************************/
void CreateBackupOfTLB(TyMLHandle *ptyHandle)
{
   unsigned long ultlbSize;
   if ( !usTlbCount )
      {
      GetTLBCount(ptyHandle, &usTlbCount);
      pEntries = (TyTLBEntry*)malloc( sizeof(TyTLBEntry) * usTlbCount );
      if ( NULL == pEntries )
         return;
      }
   memset( pEntries, 0, sizeof(TyTLBEntry) * usTlbCount );
   ultlbSize = usTlbCount * 4;
   (void)MLT_ReadTLB(ptyHandle, 0x0, pEntries, 4, ultlbSize );
}

/****************************************************************************
     Function: SearchTLB
     Engineer: Suresh P.C
        Input: unsigned long ulVPN - VPN for searching
       Output: -1 for error and TLB index otherwise
  Description: Backup the target TLB entries
Date           Initials    Description
11-Aug-2009    SPC          Initial
****************************************************************************/
int SearchTLB(unsigned long ulVPN)
{
   int iIndex;
   if ( !pEntries )
      return -1;
   for ( iIndex = 0; iIndex< usTlbCount; ++iIndex )
      {
      if ( pEntries[iIndex].EntryHi.Bits.VPN2 == ulVPN )
         {
         return iIndex;
         }
      }
   return -1;
}

