/****************************************************************************
       Module: midlayer.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD MIPS MDI midlayer.
Date           Initials    Description
08-Feb-2008    VH          Initial
06-Aug-2009    SPC         Made cache flush routine run from probe
03-Nov-2011    ANGS        Modified TyUserSettings for Zephyr trace support
26-Nov-2011    SV          Included MLR_WriteCP0Register() function declaration
****************************************************************************/
#ifndef MIDLAYER_H_
#define MIDLAYER_H_

#ifndef MAX_PATH
#define	MAX_PATH	(260)
#endif

#ifdef __LINUX
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#endif

// midlayer constants
#ifdef DEBUG
#define USE_DIAGNOSTIC_REGISTERS             1                          // add diagnostic registers to DLL
#endif
#define MAX_DIAGNOSTIC_REGISTERS             16
#define ML_MAX_MEMORY_BLOCK_SIZE             32768                      // maximum memory block size is 32 kB
#define MAX_CACHE_ENTRIES                    96                         // number of cache entries
#define MAX_MIPS_SW_BREAKPOINTS              2500                       // maximum number of breakpoints supported
#define MAX_SUPPORTED_INST_HW_BP             6                          // maximum number of HW instruction breakpoints
#define MAX_SUPPORTED_DATA_HW_BP             4                          // maximum number of HW data breakpoints
#define MAX_WATCHPOINTS_FOR_BP               1                          // maximum watchpoints to use emulating HW breakpoint
#define OPXD_DISKWARE_MIPS_FILENAME 		"opxddmip.bin"
#define OPXD_FPGAWARE_MIPS_FILENAME 		"opxdfmip.bin"
#define OPXD_FIRMWARE_FILENAME 				"opxdfw.bin"

#define MAX_DESCRIPTION_STRING      		100
#define MAX_STRING_SIZE                      0x100
#define DLLPATH_LENGTH 						(_MAX_PATH)

#define IR_LENGTH              5    // Default JTAG Instruction Register is 5 bits in length  ( May Vary)
#define DR_LENGTH              32   // JTAG Data Register is 32 bits in length 

#define MAX_DEVICE_IN_CHAIN 128 //MAX_CORES_ON_SCANCHAIN in "firmware/export/api_cmd.h"

#define MAX_TXCMD_WR_MEM_LEN              0x7FF8
#define MAX_TXCMD_RD_MEM_LEN              0x7FF8
#define MAX_TXCMD_RD_TRC_LEN              0x200

#define MAX_NUM_SHADOW_REGISTER_SETS 0x3 
#define NUM_SHADOW_REGISTERS         0x20 

#define IDCODE      0x00000001UL
#define IMPCODE     0x00000003UL
#define ADDRESS     0x00000008UL
#define DATA        0x00000009UL
#define CONTROL     0x0000000AUL
#define ALL         0x0000000BUL
#define EJTAGBOOT   0x0000000CUL
#define NORMALBOOT  0x0000000DUL
#define PCTRACE     0x00000010UL
#define TCBCONTROLA 0x00000010UL   // EJTAG 2.60 and up only
#define TCBCONTROLB 0x00000011UL   // EJTAG 2.60 and up only
#define TCBDATA     0x00000012UL   // EJTAG 2.60 and up only
#define EJWATCH     0x0000001CUL   // AMD AU1X00 only
#define BYPASS_REG  0x000000FFUL // Support multiple JTAG devices
#define BIT_0_BIT_1_ADDRESS_MASK 0xFFFFFFFC

// Security sequence identifiers           
#define NO_SEQUENCE             0x0
#define YODA_SECURITY_SEQUENCE  0x1
#define LECO_SECURITY_SEQUENCE  0x2

/* Values for Mode parameter to ML_Reset(): */
#define MODEFULLRESET          0
#define MODEDEVICERESET        1
#define MODECPURESET           2
#define MODEPERIPHERALRESET    3
#define MODEPRRSTRESET         4
#define MODEPERRSTRESET        5
#define MODEJTAGRSTRESET       6
#define MODETRSTRESET          7
#define MODERSTRESET           8
#define MODENORESET            9

/* Values for Mode parameter to ML_Step(): */
#define MODESTEPINTO           0
#define MODESTEPFORWARD        1
#define MODESTEPOVER           2

// MAGIG LOCATIONS IN DSEG
#define LOCATION_OF_START_ADDRESS 0xFF210200
#define MAGIC_FIFO_LOCATION       0xFF210204

#define DMSEG                    0xFF200000
#define DRSEG                    0xFF300000
#define DE_VECT    				 (DMSEG + 0x200)          // Debug Exception Vector
#define DE_VECT_FOR_DATA_SCAN    (DMSEG + 0x200)          // Debug Exception Vector
#define EJTAG_BRK_MASK           0xFFFFEFFF

#define MAX_DW2_DATA          	0x200

// defines for MIPS Instruction OPCODES
#define lui_t1_0xff21            0x3C09FF21
#define sw_t0_516_t1             0xAD280204
#define lw_t0_512_t1             0x8D280200
#define lw_t2_0_t0               0x8D0A0000
#define lw_t3_0_t0               0x8D0B0000
#define sw_t2_516_t1             0xAD2A0204
#define sw_t3_516_t1             0xAD2B0204
#define nop                      0x00000000
#define jr_t0                    0x01000008
#define lui_at_ff22              0x3c01ff22
#define lw_t0_200_at             0x8C280200
#define lw_ra_204_at             0x8C3F0204
#define j_minus_20               0x1000FFF7
#define j_minus_28               0x1000FFF5
#define lui_t1_FF20              0x3C09FF20
#define ori_t1_0200              0x35290200
#define jr_t1                    0x01200008
#define sync                     0x0000000F 
#define lw_t2_516_t1             0x8D2A0204
#define lw_t3_516_t1             0x8D2B0204
#define sw_t2_0_t0               0xAD0A0000
#define sw_t3_0_t0               0xAD0B0000
#define sb_t2_0_t0               0xA10A0000
#define DERET_INSTRUCTION        0x4200001F
#define J_DEBUG_EXECTION_VECTOR  0x0BC80080
#define SDBBP_OPCODE             0x7000003F  // Opcode for 32 bit Software Breakbpoint
#define SDBBP_OPCODE16           0xE801      // Opcode for 16 bit Software Breakbpoint

// Cause of break
typedef enum
{
   COB_LOSS_OF_CLOCK               	= 0x00,
   COB_TARGET_PROC_RST             	= 0x01,
   COB_TARGET_PROC_POWER_DOWN      	= 0x02,
   COB_TRACE_BUF_FULL              	= 0x03,
   COB_STOP_TRIGGER                	= 0x04,
   COB_TIMER_EXP                   	= 0x05,
   COB_BP_OPCODE                   	= 0x06,
   COB_BP_DATA_RD                  	= 0x07,
   COB_BP_DATA_WR                  	= 0x08,
   COB_EXT_HALT                    	= 0x09,
   COB_USER_HALT                   	= 0x0A,
   COB_SINGLE_STEP                 	= 0x0B,
   COB_TARGET_PROC_IDLE            	= 0x0C,
   COB_BP_PAGE                     	= 0x0D,
   COB_UNKNOWN                 		= 0x0E,
   COB_TARGET_PROC_ACK             	= 0x0F,
   COB_N_FRAMES_AFTER_STOP_TRIGGER 	= 0x10,
   COB_ILLEGAL_OPCODE              	= 0x11,
   COB_EXEC_OUTSIDE_WIN_SP         	= 0x12,
   COB_EXEC_OUTSIDE_IMPLEM_SP      	= 0x13,
   COB_EXEC_DURING_FAME_CALC       	= 0x14,
   COB_EXEC_DURING_EE_PROG         	= 0x15,
   COB_DDCHECK_MISMATCH            	= 0x16,
   COB_STEP_OVER_CALL              	= 0x17,
   COB_BP_HARDWARE                 	= 0x18,
   COB_WATCHPOINT              		= 0x19,
   COB_USER_INTERRUPT          		= 0x1A,
   COB_BRANCH_THROUGH0         		= 0x1B,
   COB_SOFTWARE_INTERRUPT      		= 0x1C,
   COB_PREFETCH_ABORT          		= 0x1D,
   COB_DATA_ABORT              		= 0x1E,
   COB_ADDRESS_EXCEPTION       		= 0x1F,
   COB_IRQ                     		= 0X20,
   COB_FIQ                     		= 0X21,   
   COB_GEN_TGT_PROC_RST            	= 0X22,
   COB_ANGEL_EXCEPTION_HALT        	= 0x23,
   COB_EXCEPTION               		= 0x24,
   COB_DEBUG_EXCEPTION         		= 0x25,
   COB_CALVA_XDATA             		= 0x26, 
   COB_SFR_COMPARATOR          		= 0x27, 
   COB_EXT_RESET               		= 0x28, 
   COB_EXT_POWER               		= 0x29,
   COB_BP_WATCHPOINT_MATCH         	= 0x30,
   COB_BP_WATCHPOINT_MISS          	= 0x31
} TyCauseOfBreak;

typedef struct
{
   unsigned char 	bNoClockPresent;
   unsigned char 	bInDebugMode;
   unsigned char 	bReducedPowerMode;
   unsigned char 	bExternalResetDetected;
   TyCauseOfBreak   cobCause;
   unsigned long  	ulBPId;
}TyStatus;

typedef enum
{
   TRACE_PC                      = 0x0 ,
   TRACE_PC_LDST_DATA            = 0x1,
   TRACE_PC_LDST_ADDR_DATA       = 0x2
}TySupportedTraceModes;

// 
// type definition
// CP0 Status Register
typedef union
{
   unsigned long   ulData;
   struct
   {                                   // LSB First
   unsigned long  IEc            :1;   //lint !e46 Common. Interrupt Enable (Current)
   unsigned long  KUc            :1;   //lint !e46 Common. Kernel/User Mode (Current)
   unsigned long  IEp            :1;   //lint !e46 Common. Interrupt Enable (Previous)
   unsigned long  KUp            :1;   //lint !e46 Common. Kernel/User Mode (Previous)
   unsigned long  IEo            :1;   //lint !e46 Common. Interrupt Enable (Old)
   unsigned long  KUo            :1;   //lint !e46 Common. Kernel/User Mode (Old)
   unsigned long  ZeroRange1     :2;   //lint !e46 Common. Unused
   unsigned long  SWM           :2;   //lint !e46 Common. Software Interrupts Mask (2)
   unsigned long  IM             :6;   //lint !e46 Common. Hardware Interrupts Mask (6)
   unsigned long  IsC            :1;   //lint !e46 Common. Isolate Cache
   unsigned long  ZeroRange2     :1;   //lint !e46 Common. Unused
   unsigned long  SR             :1;   //lint !e46 Common. Soft Reset
   unsigned long  CM             :1;   //lint !e46 Common. Cache Miss 
   unsigned long  NMI            :1;   //lint !e46 Common. NMI Detected
   unsigned long  ZeroRange3     :1;   //lint !e46 Common. Unused
   unsigned long  BEV            :1;   //lint !e46 Common. Boot Exception Vector 0x8000_0000 or 0xBFC0_0180
   unsigned long  ZeroRange4     :2;   //lint !e46 Common. Unused
   unsigned long  RE             :1;   //lint !e46 Common. Reverse Endian 
   unsigned long  ZeroRange5     :2;   //lint !e46 Common. Unused
   unsigned long  CUo            :1;   //lint !e46 Common. Co-Processor Usable 
   unsigned long  ZeroRange6     :3;   //lint !e46 Common. Unused
   } Bits;                             // Down to MSB
}MIPS_STATUS_REG;

// Note The IDCODE Register has the same bitfields for EJTAG V1.5.3 and V2.5.0
typedef union
{
   unsigned long   ulData;
   struct
   {                                  // LSB First           
   unsigned long  SetToOne      :1;                   //lint !e46
   unsigned long  ManufID       :11;                  //lint !e46
   unsigned long  PartNo        :16;                  //lint !e46
   unsigned long  Version       :4;                   //lint !e46
   }Bits;                             // Down to MSB                        
}IDCODE_REG;

// CP0 Config Register
typedef union
{
   unsigned long   ulData;           // WARNING: THESE ARE NOT COMMON!!! EJTAG 2.0.0 ONLY
   struct
   {                                   // LSB First
   unsigned long  CPU_S          :1;   //lint !e46 EJTAG 2.0 . CPU in Sleep Mode
   unsigned long  CPU_C          :1;   //lint !e46 EJTAG 2.0 . CPU in Coma Mode
   unsigned long  ZeroRange1     :1;   //lint !e46 EJTAG 2.0 . Unused
   unsigned long  ST1           :1;   //lint !e46  EJTAG 2.0 . Stop Timer 1
   unsigned long  ST2           :1;   //lint !e46  EJTAG 2.0 . Stop Timer 2
   unsigned long  DEMSZ          :4;   //lint !e46 EJTAG 2.0 . Size of Deeply Embedded Memory
   unsigned long  ZeroRange2     :5;   //lint !e46 EJTAG 2.0 . Unused
   unsigned long  BROM           :1;   //lint !e46 EJTAG 2.0 . Select between PI and DVP bus if CMEM
   unsigned long  MIPS16         :1;   //lint !e46 EJTAG 2.0 . MIPS16 allowed
   unsigned long  END            :1;   //lint !e46 EJTAG 2.0 . 0 = little Endian, 1 = Big
   unsigned long  DSU            :1;   //lint !e46 EJTAG 2.0 . Debug Support Unit Enabled
   unsigned long  ZeroRange3     :2;   //lint !e46 EJTAG 2.0 . Unused
   unsigned long  MAP            :1;   //lint !e46 EJTAG 2.0 . Static Memory translation selection bit
   unsigned long  CSZ            :3;   //lint !e46 EJTAG 2.0 . Cache Size e.g. 4= 4KB 5= 8KB
   unsigned long  ZeroRange4     :2;   //lint !e46 EJTAG 2.0 . Unused
   unsigned long  ICD            :1;   //lint !e46 EJTAG 2.0 . Instruction Cache Disable
   unsigned long  DCD            :1;   //lint !e46 EJTAG 2.0 . Data Cache Disable
   unsigned long  CL             :1;   //lint !e46 EJTAG 2.0 . Cache Lock
   unsigned long  ZeroRange5     :3;   //lint !e46 EJTAG 2.0 . Unused
   } Bits;                             // Down to MSB
}CONFIG_REG;

// CP0 SRSCtl Register
typedef union
{
   unsigned long   ulData;           // WARNING: THESE ARE ASSUMED INDEPENDENT OF EJTAG VERSION
   struct
   {                                   // LSB First
   unsigned long  CSS            :4;    
   unsigned long  ZeroRange1     :2;
   unsigned long  PSS            :4;    
   unsigned long  ZeroRange2     :2;
   unsigned long  ESS            :4;    
   unsigned long  ZeroRange3     :2;
   unsigned long  EICSS          :4;    
   unsigned long  ZeroRange4     :4;
   unsigned long  HSS            :4;    
   unsigned long  ZeroRange5     :2;
   } Bits;                             // Down to MSB
}SRSCTL_REG;

// Version Which Merges EJTAG 2.5.1 and 1.5.3
// Need to define this so that it is identical for all sorts
// of compilers. To do this we have to use the base type of char
// and splt any definitions that span a byte bounday
typedef union
{
   unsigned long   ulData;
   struct
      {                   
      // Byte 0
      unsigned char  ClkEn        :1;     //lint !e46 EJTAG1.53 Only DCLK Enable
      unsigned char  TOF          :1;     //lint !e46 EJTAG1.53 Only DMA Test Output Flag
      unsigned char  TIF          :1;     //lint !e46 EJTAG1.53 Only DMA Test Input Flag
      unsigned char  DM           :1;     //lint !e46 Common (Debug Mode)
      unsigned char  Dinc         :1;     //lint !e46 EJTAG1.53 Only DMA Address AutoIncrement
      unsigned char  Dlock        :1;     //lint !e46 EJTAG1.53 Only DMA Lock (atomic Transaction)
      unsigned char  ZeroRange1   :1;     //lint !e46 Common Reserved Area
      unsigned char  Dsz_0        :1;     //lint !e46 EJTAG1.53 Only Data Transfer Size (V2.5 uses Psz)

      // Byte 1
      unsigned char  Dsz_1        :1;     //lint !e46 EJTAG1.53 Only Data Transfer Size (V2.5 uses Psz)
      unsigned char  Drwn         :1;     //lint !e46 EJTAG1.53 Only DMA Read, Not Write
      unsigned char  Derr         :1;     //lint !e46 EJTAG1.53 Only DMA Error
      unsigned char  Dstrt        :1;     //lint !e46 EJTAG1.53 Only DMA Start/Busy
      unsigned char  EJTAGBrk     :1;     //lint !e46 Common : Send EJTAG Break (Causes Debug Exception)
      unsigned char  Dabort       :1;     //lint !e46 EJTAG1.53 Extension. TBD if it works.
      unsigned char  ProbTrap     :1;     //lint !e46 EJTAG2.5 Only (Trap from Probe Memory)
      unsigned char  ProbEn       :1;     //lint !e46 Common EJTAG Probe Enable

      // Byte 2
      unsigned char  PrRst        :1;     //lint !e46 Common Processor Reset
      unsigned char  DmaAcc       :1;     //lint !e46 EJTAG1.53 Only DMA Access
      unsigned char  PrAcc        :1;     //lint !e46 Common Processor Access (Processor tells probe)
      unsigned char  PRnW         :1;     //lint !e46 Common Processor Access Read, not Write
      unsigned char  PerRst       :1;     //lint !e46 Common Peripheral Reset
      unsigned char  Halt         :1;     //lint !e46 EJTAG2.5, Run in V1.53 (opposite Meaning)
      unsigned char  Doze         :1;     //lint !e46 Common Processor in Doze State
      unsigned char  Sync         :1;     //ling !e46 EJTAG1.53 Sync Start of Trace with last access

      // Byte 3
      unsigned char  PCLen        :2;     //lint !e46 EJTAG1.53 Target PC Output Length
      unsigned char  ZeroRange3   :3;     //lint !e46 Common Reserved Area
      unsigned char  Psz          :2;     //lint !e46 EJTAG2.5 Only, Data Transfer Size (Dsz in 1.53) 
      unsigned char  Rocc         :1;     //lint !e46 EJTAG2.5 Only, Reset Occurred
      } Bits;                           // Down to MSB
}CONTROL_REG;

// Version Which Merges EJTAG 2.5.1 and 1.5.3
typedef union
{
   unsigned long   ulData;
   struct
   {                                  // LSB First
   unsigned long  MipsType      :1;                   //lint !e46
   unsigned long  ZeroRange1    :4;   //lint !e46 Obsolete EJTAG Field
   unsigned long  NoInstBrk     :1;   //lint !e46 EJTAG 1.53 only. If DSU has Ins break feature
   unsigned long  NoDataBrk     :1;   //lint !e46 EJTAG 1.53 only. If DSU has Data break feature
   unsigned long  NoProcBrk     :1;   //lint !e46 EJTAG 1.53 only. If DSU has Proc Bus break feature
   unsigned long  PCSTW         :3;   //lint !e46 EJTAG 1.53 only. Width of PCST
   unsigned long  TPCW          :3;   //lint !e46 EJTAG 1.53 only. Width of TPC
   unsigned long  NoDMA         :1;   //lint !e46 Common. Is DMA mode supported
   unsigned long  NoPCTrace     :1;   //lint !e46 EJTAG 1.53 only. Is PCTrace Supported
   unsigned long  Mips16        :1;   //lint !e46 Common. Is MIPS16 supported.
   unsigned long  ICacheC       :1;   //lint !e46 EJTAG 1.53 only. Is ICache coherent with DMA
   unsigned long  DCacheC       :1;   //lint !e46 EJTAG 1.53 only. Is DCache coherent with DMA
   unsigned long  PhysAW        :1;   //lint !e46 EJTAG 1.53 only. Physical Address Width
   unsigned long  ZeroRange2    :1;   //lint !e46 Common. Reserved  Bit
   unsigned long  ASIDSize      :2;   //lint !e46 EJTAG 2.50 only. Size of ASID Field
   unsigned long  SDBBPCode     :1;   //lint !e46 EJTAG 1.53 only. SW BP uses Special2 Opcode
   unsigned long  DINTSup       :1;   //lint !e46 EJTAG 2.50 only. Support for DINT Line from Probe
   unsigned long  ZeroRange3    :3;   //lint !e46 Common. Reserved Area
   unsigned long  RXK           :1;   //lint !e46 EJTAG 2.50 only. R3K or R4K privileged environment
   unsigned long  EJTAGVer      :3;   //lint !e46 Common. EJTAG Version.
   } Bits;                           // Down to MSB
}IMPCODE_REG;

typedef union
{
   unsigned long   ulData;
   struct
   {                                 // LSB First
   unsigned long  DSS           :1;  //lint !e46 Debug Single Step Occurred
   unsigned long  DBp           :1;  //lint !e46 Debug Breakpoint Execption
   unsigned long  DDBL          :1;  //lint !e46 Debug Load Breakpoint Exception Occurred
   unsigned long  DDBS          :1;  //lint !e46 Debug Store Breakpoint Exception Occurred
   unsigned long  DIB           :1;  //lint !e46 Debug Instruction Breakpoint Exception Occurred
   unsigned long  DINT          :1;  //lint !e46 Debug Interrupt Exception Occurred (User Halt)
   unsigned long  ZeroRange1    :1;  //lint !e46 Unused
   unsigned long  JtagRst       :1;  //lint !e46 EJTAG 1.53 Only: JTAG Reset: RST EJTAG + DSU
   unsigned long  SSt           :1;  //lint !e46 Single Step Feature Enabled
   unsigned long  NoSSt         :1;  //lint !e46 EJTAG 2.51 Only: Single Step Feature Available
   unsigned long  DExcCode      :5;  //lint !e46 Common ?? : Debug Exception code: Values differ between versions
   unsigned long  EJTAGver      :3;  //lint !e46 Common: EJTAG Ver : 0 - Version 1+2 1 - Version 2.5
   unsigned long  DDBLImpr      :1;  //lint !e46 EJTAG 2.51 Only: Data Break Load Imprecise
   unsigned long  DDBSImpr      :1;  //lint !e46 EJTAG 2.51 Only: Data Break Store Imprecise
   unsigned long  IEXI          :1;  //lint !e46 EJTAG 2.51 Only:Imprecise Error eXception Inhibit
   unsigned long  DBusEP        :1;  //lint !e46 EJTAG 2.51 Only:Data Access Bus Error Exception Pending
   unsigned long  CacheEP       :1;  //lint !e46 EJTAG 2.51 Only:Cache Error is Pending 
   unsigned long  MCheckP       :1;  //lint !e46 EJTAG 2.51 Only:Machine Check Error is Pending
   unsigned long  IBusEP        :1;  //lint !e46 EJTAG 2.51 Only:Bus Error Exception is Pending
   unsigned long  CountDM       :1;  //lint !e46 EJTAG 2.51 Only:Count Register Running in Debug Mode
   unsigned long  Halt          :1;  //lint !e46 EJTAG 2.51 Only:Internal System Bus clock Stopped
   unsigned long  Doze          :1;  //lint !e46 EJTAG 2.51 Only: Processor in Low Power mode when debExp occurred
   unsigned long  LSNM          :1;  //lint !e46 EJTAG 2.51 Only: Loads in DSEG Address range go to system Memory
   unsigned long  NoDCR         :1;  //lint !e46 EJTAG 2.51 Only: No Dseg Present
   unsigned long  DM            :1;  //lint !e46 Common: Processor Operating in Debug Mode
   unsigned long  DBD           :1;  //lint !e46 Common: In Branch Delay slot
   } Bits;                                            
}DEBUG_REG;

typedef union
{
   unsigned long   ulData;
   struct
      {                                   // LSB First
      unsigned long  On           :1;     //lint !e46 Global Trace Enable
      unsigned long  Mode         :3;     //lint !e46 Trace MODE to be used
      unsigned long  G            :1;     //lint !e46 Trace Enabled for all processes
      unsigned long  ASID         :8;     //lint !e46 ASID of App to be traced
      unsigned long  U            :1;     //lint !e46 Enable Trace in User Mode
      unsigned long  K            :1;     //lint !e46 Enable Trace in Kernel Mode
      unsigned long  S            :1;     //lint !e46 Enable Trace in Supervisor Mode
      unsigned long  E            :1;     //lint !e46 Enable Trace when either EXL or ELR == 1
      unsigned long  D            :1;     //lint !e46 Enable Trace in Debug Mode
      unsigned long  IO           :1;     //lint !e46 Inhibit Overflow ( non-Realtime trace)
      unsigned long  TB           :1;     //lint !e46 Trace all Branches (not just unpredictable ones)
      unsigned long  SyP          :3;     //lint !e46 Synchronisation Period
      unsigned long  ADW          :1;     //lint !e46 PD0_AD bus width 0 = 16 bits, 1 = 32 bits
      unsigned long  VModes       :2;     //lint !e46 Trace modes supported by Processor
      unsigned long  Zero         :1;     //lint !e46 Unused
      unsigned long  Reserved     :5;     //lint !e46 Reserved
      } Bits;                             // Down to MSB
}TyTCBCONTROLA;

typedef union
{
   unsigned long   ulData;
   struct
      {                                   // LSB First
      unsigned long  EN           :1;     //lint !e46 Enable Trace
      unsigned long  OfC          :1;     //lint !e46 Use Off Chip Trace Memory
      unsigned long  CA           :1;     //lint !e46 CycleAccurate Trace ( Include Stall Info)
      unsigned long  Zero1        :4;     //lint !e46 Zero 
      unsigned long  Cal          :1;     //lint !e46 Calibrate Off Chip trace interface
      unsigned long  CCR          :3;     //lint !e46 Off Chip Clock Ratio  (CR means something to VC++)
      unsigned long  Zero2        :1;     //lint !e46 Zero
      unsigned long  TM           :2;     //lint !e46 Trace Mode
      unsigned long  BF           :1;     //lint !e46 Buffer Full
      unsigned long  TR           :1;     //lint !e46 Trace Memory Reset
      unsigned long  RM           :1;     //lint !e46 Read On-chip Trace Memory
      unsigned long  Zero3        :3;     //lint !e46 Zero
      unsigned long  WR           :1;     //lint !e46 Write Registers
      unsigned long  REG          :5;     //lint !e46 Address of register to be accessed through TCBDATA
      unsigned long  Zero4        :2;     //lint !e46 Zero
      unsigned long  Impl         :3;     //lint !e46 Reserved
      unsigned long  WE           :1;     //lint !e46 Write Enable (Must be set for other bits to take effect
      } Bits;                             // Down to MSB
}TyTCBCONTROLB;

typedef union     // Note This Register is accessed through TCBDATA 
{
   unsigned long   ulData;
   struct
      {                                   // LSB First
      unsigned long  REV           :4;     //lint !e46 Revision of TCB
      unsigned long  OfT           :1;     //lint !e46 Off-chip Trace interface is present
      unsigned long  OnT           :1;     //lint !e46 On-chip Trace Interface is Present
      unsigned long  PiN           :3;     //lint !e46 Pipe Number
      unsigned long  PW            :2;     //lint !e46 Pipe Width
      unsigned long  CRMin         :3;     //lint !e46 Off-chip Minimun Clock Ratio
      unsigned long  CRMax         :3;     //lint !e46 Off-chip Maximum Clock Ratio
      unsigned long  SZ            :4;     //lint !e46 On-chip Trace Memory Size (256 bytes -> 8 MB)
      unsigned long  TRIG          :4;     //lint !e46 Number of Triggers Implemented
      unsigned long  Impl          :6;     //lint !e46 Reserved
      unsigned long  CF1           :1;     //lint !e46 Inidicates if TCBCONFIG1 exists
      } Bits;                              // Down to MSB
}TyTCBCONFIG;     // Note This Register is accessed through TCBDATA

typedef union     // Note This Register is accessed through TCBDATA 
{
   unsigned long   ulData;
   struct
      {                                   // LSB First
      unsigned long  TR           :1;     //lint !e46 Trigger Happened
      unsigned long  F0           :1;     //lint !e46 Fire Once
      unsigned long  Type         :2;     //lint !e46 Trigger Type (Start,End,About,Info)
      unsigned long  PDTri        :1;     //lint !e46 Use TC_ProbeTrigIn signal
      unsigned long  CHTri        :1;     //lint !e46 Use TC_ChipTrigIn signal
      unsigned long  DM           :1;     //lint !e46 Trigger on Rising edge of Debug Mode
      unsigned long  Impl1        :7;     //lint !e46 Reserved
      unsigned long  PDTro        :1;     //lint !e46 Generate TC_ProbeTrigOut signal
      unsigned long  CHTro        :1;     //lint !e46 Generate TC_ChipTrigOut signal
      unsigned long  CTATrg       :1;     //lint !e46 Update TCBCONTROLA as per TCBDCH
      unsigned long  Impl2        :6;     //lint !e46 Reserved
      unsigned long  Trace        :1;     //lint !e46 Generate a TF6 trace info
      unsigned long  TCBinfo      :8;     //lint !e46 Info to be used in TF6 fram
      } Bits;                              // Down to MSB
}TyTCBTRIGX;     // Note This Register is accessed through TCBDATA

// Instruction Breakpoint Status
typedef union
{
   unsigned long   ulData;
   struct
   {                                   // LSB First
   unsigned long  BrkStatus      :15;  //lint !e46 Common. Break Status.Which of BP 0-14 Occurred
   unsigned long  ZeroRange3     :9;   //lint !e46 Common. Unused
   unsigned long  BCN            :4;   //lint !e46 Common. Number of Breakpoints Implemented 1-15
   unsigned long  ZeroRange2     :2;   //lint !e46 Common. Unused
   unsigned long  ASIDsup        :1;   //lint !e46 Common. ASID Supported?
   unsigned long  ZeroRange1     :1;   //lint !e46 Common. Unused
   } Bits;                             // Down to MSB
}IBS_REG;

// Data Breakpoint Status
typedef union
{
   unsigned long   ulData;
   struct
   {                                   // LSB First
   unsigned long  BrkStatus      :15;  //lint !e46 Common. Break Status.Which of BP 0-14 Occurred
   unsigned long  ZeroRange2     :9;   //lint !e46 Common. Unused
   unsigned long  BCN            :4;   //lint !e46 Common. Number of Breakpoints Implemented 1-15
   unsigned long  NoLVMatch      :1;   //lint !e46 Common. No Load Value Compare Supported
   unsigned long  NoSVMatch      :1;   //lint !e46 Common. No Store Value Compare Supported
   unsigned long  ASIDsup        :1;   //lint !e46 Common. ASID Supported?
   unsigned long  ZeroRange1     :1;   //lint !e46 Common. Unused
   } Bits;                             // Down to MSB
}DBS_REG;

typedef enum 
{ 
   CACHE_FLUSH_WRITEBACK, 
   CACHE_FLUSH_INVALIDATE, 
   CACHE_FLUSH_WRITEBACK_AND_INVALIDATE 
} TyCacheFlush;

#define  CACHE_TYPE_INST      0x1
#define  CACHE_TYPE_DATA      0x2
#define  CACHE_TYPE_SECONDARY 0x4
#define  CACHE_TYPE_UNIFIED   0x7

typedef enum 
{ 
   BP_START             = 0x00,
//todo temp   BP_OPCODE            = 0x00,
//todo temp   BP_DATA_RD           = 0x01,
//todo temp   BP_DATA_WR           = 0x02,
//todo temp   BP_PAGE              = 0x03,
   BP_XDATA             = 0x04,
   BP_ON_CHIP_RAM       = 0x05,
   BP_RAM_BR_RANGE      = 0x06,
   BP_ALL               = 0x07,
   BP_SW                = 0x08,
   BP_COCO_PSEN         = 0x09,
   BP_COCO_OPCODE       = 0x0A,
   BP_HW                = 0x0B,
   BP_NO                = 0x0C,
   BP_DATA              = 0x0D,
   BP_TEMP_OPCODE       = 0x0E,
   BP_TEMP_HW           = 0x0F,
   BP_OPCODE_16BIT      = 0x10,
   BP_OPCODE_32BIT      = 0x11,
   BP_WATCH_BYTEREAD    = 0x12,
   BP_WATCH_HALFREAD    = 0x13,
   BP_WATCH_WORDREAD    = 0x14,
   BP_WATCH_BYTEWRITE   = 0x15,
   BP_WATCH_HALFWRITE   = 0x17,
   BP_WATCH_WORDWRITE   = 0x18,
   BP_ARM_DATA_ADDR_WP  = 0x18,
   BP_ARM_DATA_WP       = 0x19,
   BP_WP                = 0x1A,
   BP_END               = 0x1B
} TyBPType;

typedef struct
{
   unsigned long  ulNextAddr;
   unsigned long  ulNextInstSize;
   unsigned long  ulDestAddr;
   unsigned long  ulDestInstSize;
   int            bCallInst;
} TyDisaResult;
// midlayer structures

typedef struct 
{
   unsigned long			  ulIRLength;			   		// Device IR Length	
   unsigned long			  ulBypassCode;		   			// Device Bypass Code	
}  TyMultiCoreDeviceInfo;

typedef struct    dsu
{
   unsigned long ulNumberOfIBPsSupported;    	// Number of Instruction Breakpoints Supported
   unsigned long ulNumberOfDBPsSupported;    	// Number of Data Breakpoints Supported
   unsigned char bIBPASIDSupported; 			// Does the IB System support ASID compares
   unsigned char bDBPASIDSupported;  			// Does the DB System support ASID Compares
   unsigned char bStoreValueCompareSupported; 	// Does the DB system support store value compares
   unsigned char bLoadValueCompareSupported; 	// Does the DB system support load value compares
} DSU_CFG; 

typedef enum
{
   NO_TRACE                      = 0x0,            // Trace is not supported e.g. EJTAG 2.5
   TCB_TRACE                     = 0x1,            // EJTAG 2.6 style Trace
   EJTAG2_TRACE                  = 0x2             // EJTAG 2.0 style Trace
}TyTraceType;

typedef struct
{
   TyTraceType   		tyTraceType;
   TySupportedTraceModes   tySupportedTraceModes;
   unsigned long        ulTraceBusWidth;
   unsigned long        ulOffChipClockRatio;
   unsigned long        ulOnChipTraceMemorySize;
   unsigned long        ulMaxClockRatio;
   unsigned long        ulMinClockRatio;
   unsigned char 		bOnChipTraceSupported;
   unsigned char 		bOffChipTraceSupported;
   unsigned long        ulTCBRevision;
   unsigned char 		bExtendedEJTAG20Trace;
}TyTraceSettings;


typedef enum
{
   EJTAG26_STYLE_TRACE,
   EJTAG20_STYLE_TRACE,             // Normal
   EJTAG20E_STYLE_TRACE             // Extended
} TyTraceStyle;

typedef struct
{
   int   bDMATurnedOn;
   int   bRstTgtBforeConn;
   int   bEndianModeModifiable;
   int   bReadUsingTargetMemEnabled;
   int   bWriteUsingTargetMemEnabled;
   int   bMchpRecoveryByChipErase;
   char  szOpellaXDNumber[16];
   unsigned long ulDataScratchPadRamStart;    
   unsigned long ulDataScratchPadRamEnd;     
   unsigned long ulInstScratchPadRamStart;   
   unsigned long ulInstScratchPadRamEnd; 
  //
  // 
  // 
   unsigned char bSSIn16BitModeSupported;
   unsigned long EJTAGStyle;                // Careful - this MUST be the size of an enumerated types
   unsigned char bUseSlowJTAG;              // Use slow jtag support
   unsigned char bEJTAGBootSupported;       // EJTAG BootMode Supported
   unsigned long ulNumInstHWBps;            // Number of instruction hardware breakpoints
   unsigned long ulNumDataHWBps;            // Number of data hardware breakpoints
   unsigned char bHWBpASIDSupported;        // Do the hardware breakpoints support ASIDs
   unsigned char bOddAddressFor16BitModeBps;// Use odd addresses for MIPS16 breakpoints
   unsigned char bEnableMIPS16ModeForTrace; // Some enable MIPS16 mode in case it gets enabled during exec
   unsigned char bZephyrTraceSupport;       // ZephyrTrace Supported                                  // 
   unsigned char bDoubleReadOnDMA;          // Enable DMA mode fix (very specific fix)
   unsigned char bOnlyFlushDataCache;       // Only flush the data cache
   unsigned char bFixedCoreNums;            // Use family file entries
   unsigned char ucMultiCoreCoreNum;        // Core number of device to use (if multiple devices!)
   unsigned char ucMultiCoreDMACoreNum;     // Use this core for DMA accesses (PR4450 support)
   unsigned char ucNumberOfNOPSInJUMPBDS;   // How many NOPS for the JUMP BDS (1 or 3 on PR4450)
   unsigned char ucMultiCoreNumCores;       // Number of cores in the chain; 0- no multicore
   unsigned char ucLengthOfAddressRegister; // AMD AU1500 uses a non-standard Address register length
   unsigned char bSelectAddrRegAfterEveryAccess;  // AMD AU1500 requires the ALL/ADRRESS register to be selected after each access
   unsigned char bSyncInstNotSupported;     // Sync instruction is not supported
   unsigned long ulCoreSelectCode;          // If not BYPASS, then this should be scanned into the instruction register after a reset to select the core we want
   unsigned long ulCoreSelectIRLength;      // This will also be used as the bypass code for the first device in the chain
   unsigned char ucEJTAGSecSequence;        // Special sequence after a TAP reset
   unsigned long ulInvalidateInst;          // Invalidate Instruction
   unsigned long ulInvalidateWBInst;        // Write Back Invalidate Instruction
   unsigned char bUseFastDMAFunc ;          // We have two dma functions.
   unsigned char bCoreSelectBeforeReset;    // XML configurable to allow core select before reset for "Limerick" microchip device
   unsigned char bMicroChipM4KFamily;       // XML configurable to say if device belongs to Microchip M4K family
   unsigned char bUseRestrictedDmaAddressRange; // allow DMA access only within a restricted DMA range ( added for 8335 support)
   unsigned long ulLimitDmaRdBpS;            // transfer restriction for DMA reads (bytes/sec)
   unsigned long ulLimitDmaWrBpS;            // transfer restriction for DMA writes (bytes/sec)
   unsigned long ulLimitCacheRdBpS;          // transfer restriction for reads using cache debug routine (bytes/sec)
   unsigned long ulLimitCacheWrBpS;          // transfer restriction for writes using cache debug routine (bytes/sec)
   char  szPasswordSequence[MAX_PATH];
   char  szDeviceEnablePasswdFile[MAX_PATH];
   char  szPasswordFile1[MAX_PATH];
   char  szPasswordFile2[MAX_PATH];
   char  szPasswordFile3[MAX_PATH];
   TyMultiCoreDeviceInfo      tyMultiCoreDeviceInfo[MAX_DEVICE_IN_CHAIN]; 
} TyUserSettings;

// breakpoint flags
typedef struct _TyBpFlags {
   unsigned long ulBreakpointID;
   unsigned long ulBpAddress;
   unsigned long ulBpAddressMask;
   unsigned long ulDataValue;
   unsigned long ulASIDToMatch;
   unsigned char ucByteAccessIgnore ;
   unsigned char ucByteLaneMask;
   unsigned char bDataBp;
   unsigned char bMatchASID;
   unsigned char bGenTrigger;
   unsigned char bGenBp;
   unsigned char bFufillOnStore;
   unsigned char bFufillOnLoad;
   unsigned char bMIPS16Breakpoint;
} TyBpFlags;

// structure for cache entries
typedef struct _TyCacheEntry {
   unsigned long ulTagLo;
   unsigned long pulDataLo[4];
   unsigned long ulWaySelect;
} TyCacheEntry;

// structure for HW breakpoint resource
typedef struct _TyHWBPResource
{
   unsigned long ulBreakpointIdentifier;                                // breakpoint identifier
   unsigned long ulAddressDSUEjOfs;                                     // address register offset in DRSEG (DSU)
   unsigned long ulMaskDSUEjOfs;                                        // mask register offset in DRSEG (DSU)
   unsigned long ulASIDDSUEjOfs;                                        // ASID register offset in DRSEG (DSU)
   unsigned long ulControlDSUEjOfs;                                     // control register offset in DRSEG (DSU)
   unsigned long ulDataValueDSUEjOfs;                                   // data value register offset in DRSEG (DSU)
   unsigned long ulAddressOfBP;
   unsigned long ulAddressMaskOfBP;
   unsigned long ulASIDOfBP;
   unsigned long ulControlValueOfBP;
   unsigned long ulDataValueOfBP;
   unsigned char bBPAvailable;                                          // is BP available
   unsigned char bMIPS16BP;                                             // 16-bits or 32-bit breakpoint
   unsigned char bEmulatedWithWP;                                       // breakpoint is emulated with watchpoint
} TyHWBPResource;

// structure for single breakpoint
typedef struct _TyBreakpoint {
   struct _TyBreakpoint *ptyNext;                                       // pointer to next item (NULL for last one)
   unsigned long ulPreviousInstructionData;
   unsigned long ulBPVirtualAddress;
   unsigned long ulBPAccessAddress;
   unsigned long ulBPSize;
   TyBPType tyBPType;
   TyHWBPResource *ptyResource;
   unsigned char bBigEndian;
} TyBreakpoint;

// structure for MIPS breakpoints
typedef struct _TyMipsBreakpoints {
   TyBreakpoint *ptyBreakpoint;                                         // linked array of breakpoints (dynamic)
   TyHWBPResource ptyHWInstBPResource[MAX_SUPPORTED_INST_HW_BP];
   TyHWBPResource ptyHWDataBPResource[MAX_SUPPORTED_DATA_HW_BP];
   TyHWBPResource ptyWatchpointHWInstBPResource[MAX_WATCHPOINTS_FOR_BP];
   unsigned long ulHWInstBPUsed;                                        // number of HW instruction breakpoints used
   unsigned long ulHWDataBPUsed;                                        // number of HW data breakpoints used
   unsigned long ulWatchBPUsed;                                         // number of watchpoints used as HW instruction breakpoint
   unsigned long ulUniqueIdentifierForWatchpointBp;
   unsigned long ulUniqueIdentifierFoComplexBp;
} TyMipsBreakpoints;

// structure with MIPS DSU (debug support unit) information
typedef struct _TyMipsDSU {
   unsigned long ulNumberOfIBPsSupported;                               // number of HW instruction breakpoints supported
   unsigned long ulNumberOfDBPsSupported;                               // number of HW data breakpoints supported
   unsigned long ulNumberOfWPAsBP;                                      // number of watchpoints to emulate breakpoints
   unsigned char bIBPASIDSupported;                                     // DSU supports instruction ASID register
   unsigned char bDBPASIDSupported;                                     // DSU supports data ASID register
   unsigned char bLoadValueCompareSupported;                            // DSU supports load value compare for data breakpoints
   unsigned char bStoreValueCompareSupported;                           // DSU supports store value compare for data breakpoints
   unsigned char bProcBPSupported;                                      // DSU supports processor bus breakpoints
   unsigned char bDSUDetermined;                                        // DSU already determined
} TyMipsDSU;

// structure with all loaded DW2 applications
typedef struct _TyMipsDW2Applications {
   // DW2 application to read words
   unsigned long *pulReadWordsApp;
   unsigned long ulReadWordsInstructions;
   // DW2 application to write words
   unsigned long *pulWriteWordsApp;
   unsigned long ulWriteWordsInstructions;
   // DW2 application to read halfwords
   unsigned long *pulReadHalfwordsApp;
   unsigned long ulReadHalfwordsInstructions;
   // DW2 application to write halfwords
   unsigned long *pulWriteHalfwordsApp;
   unsigned long ulWriteHalfwordsInstructions;
   // DW2 application to read bytes
   unsigned long *pulReadBytesApp;
   unsigned long ulReadBytesInstructions;
   // DW2 application to write bytes
   unsigned long *pulWriteBytesApp;
   unsigned long ulWriteBytesInstructions;
   // DW2 application to read TLB
   unsigned long *pulReadTLBApp;
   unsigned long ulReadTLBInstructions;
   // DW2 application to TLB bytes
   unsigned long *pulWriteTLBApp;
   unsigned long ulWriteTLBInstructions;
   // DW2 application to do cache action
   unsigned long *pulActCacheApp;
   unsigned long ulActCacheInstructions;
   unsigned long ulActCacheInstOffset;
   // DW2 application to read cache
   unsigned long *pulReadCacheApp;
   unsigned long ulReadCacheInstructions;
   unsigned long pulReadCacheInstOffset[4];
   // DW2 application to read CP0 register
   unsigned long *pulReadCP0App;
   unsigned long ulReadCP0Instructions;
   unsigned long ulReadCP0InstOffset;
   // DW2 application to write CP0 register
   unsigned long *pulWriteCP0App;
   unsigned long ulWriteCP0Instructions;
   unsigned long ulWriteCP0InstOffset;
   // DW2 application to read ACX register
   unsigned long *pulReadACXApp;
   unsigned long ulReadACXInstructions;
   // DW2 application to write ACX register
   unsigned long *pulWriteACXApp;
   unsigned long ulWriteACXInstructions;
   // cache debug routine
   unsigned long *pulCacheFlushApp;
   unsigned long ulCacheFlushStartAddress;
   unsigned long ulCacheFlushInstrAddress;
   unsigned long ulCacheFlushInstructions;
   // DW2 application to Save Context
   unsigned long *pulSaveContextApp;
   unsigned long ulSaveContextInstructions;
   // DW2 application to Restore Context
   unsigned long *pulRestoreContextApp;
   unsigned long ulRestoreContextInstructions;
   // DW2 application to Save Shadow Register Set
   unsigned long *pulSaveShadowRegisterSetApp;
   unsigned long ulSaveShadowRegisterSetInstructions;
   // DW2 application to Restore Shadow Register Set
   unsigned long *pulRestoreShadowRegisterSetApp;
   unsigned long ulRestoreShadowRegisterSetInstructions;
   // DW2 application to Run target command
   unsigned long *pulRunTargetCommandApp;
   unsigned long ulRunTargetInstrAddress;
   unsigned long ulRunTargetCommandInstructions;
   // DW2 application to Clear MP bit command
   unsigned long *pulClearMPBitApp;
   unsigned long ulClearMPBitInstructions;
} TyMipsDW2Applications;

typedef struct 
{
   unsigned char bPipeLineLearned;
   unsigned char ucGlobalCycForMemRead1;
   unsigned char ucGlobalCycForMemRead2;
   unsigned char ucGlobalCycForMemStore1;
   unsigned char ucGlobalCycForMemStore2;
   unsigned char ucGlobalCycBetwStores;
   unsigned char ucGlobalCycBetwReads;
   unsigned char ucGlobalCyclesBeforeJump;
}  TyPipelineInfo;

// structure with information about current core and connection
typedef struct _TyMLHandle {
   // 
   int  iDLHandle;                              // handle to connection DL (driver layer)
   char pszSerialNumber[MAX_SERIAL_NUMBER_LEN]; // Serial number to connect to
   char pszProcName[MAX_STRING_LENGTH_PROC_NAME];
   unsigned long ulNumberOfCoresOnScanchain;	//Number of cores on the scanchain
   // MIPS processor configuration
   unsigned long ulMipsCore;                    // MIPS core number on scanchain for DL functions
   unsigned long ulDmaCore;                     // MIPS DMA core number of scanchain for DL functions
   unsigned char bBigEndian;                    // MIPS core is big endian or little endian
   unsigned char bEndianModeModifiable;         // Mode Modifiable
   unsigned char bBrokeInMIPS16Mode;            // broke in MIPS16 mode
   unsigned char bProcessorExecuting;           // processor is executing
   unsigned char ucEjtagVersion;                // EJTAG version (as EjtagVer bits in debug register)
   unsigned char bEJTAGBootSupported;           // is EJTAGBoot instruction supported
   unsigned char bEndianSwitchedForCacheFlush;  // endianess is currently switched for cache flush
   // MIPS core parameters
   unsigned long ulMipsCoreEjtagIRLength;       // length of IR for MIPS core
   unsigned long ulMipsCoreEjtagAddressLength;  // length of EJTAG address register for MIPS core
   unsigned long ulMipsCoreEjtagDataLength;     // length of EJTAG data register for MIPS core
   unsigned long ulMipsCoreEjtagIdcodeInst;     // IDCODE instruction for MIPS core
   // DMA core parameters
   unsigned long ulDmaCoreEjtagIRLength;        // length of IR for DMA core
   unsigned long ulDmaCoreEjtagAddressLength;   // length of EJTAG address register for DMA core
   unsigned long ulDmaCoreEjtagDataLength;      // length of EJTAG data register for DMA core
   unsigned char bDoubleReadOnDma;              // need double read for DMA core
   unsigned char bDmaSupported;                 // does target supports DMA core
   unsigned char bDmaEnabledByUser;             // DMA access enabled by user
   unsigned char bUseDmaRestrictedRange;        // using restricted DMA address range
   unsigned long ulStartOfDmaAddressRange;      // start address of DMA address range (physical address)
   unsigned long ulEndOfDmaAddressRange;        // end address of DMA address range (physical address)
   unsigned char bMemOnTheFlyEnabled;           // memory access on the fly
   unsigned char bBPOnTheFlyEnabled;            // breakpoint access on the fly
   // MIPS memory mapping
   unsigned char bUseMMUMapping;                // using MMU mapping
   unsigned char ucFixedMapSystem;              // which fixed mapping system to use
   // other MIPS processor options
   unsigned char bSelectIRAfterEachAccess;      // we need to select IR after each access
   // MIPS reset and boot options
   unsigned char bNormalBootSelected;           // is normal boot instruction selected
   unsigned char bUseEjtagBoot;                 // using EJTAG boot is allowed
   unsigned char bEJTAGBootDuringReset;
   // MIPS fast download support TODO:
   unsigned char bUseTargetMemoryWhileWriting;  // use cache flush routine for fast writes
   unsigned char bUseTargetMemoryWhileReading;  // use cache flush routine for fast reads
   unsigned char bCacheFlushFastTreshold;       // amount of bytes to use as treshold for fast reads/writes
   // MIPS cache flush support
   unsigned char bInvalidateCacheOnReset;       // invalidate cache on reset
   unsigned char bFlushOnlyDataCache;           // flush only data cache
   unsigned char bDataCacheAlreadyInvalidated;  // data cache already invalidated
   unsigned long ulInvalidateInst;
   unsigned long ulInvalidateWBInst;
   // MIPS Scratch Pad options
   unsigned char bInstScratchPadRamPresent;     // inst scratch pad RAM is present
   unsigned long ulInstScratchPadRamStart;      // inst scratch pad RAM start address
   unsigned long ulInstScratchPadRamEnd;        // inst scratch pad RAM end address
   unsigned char bDataScratchPadRamPresent;     // data scratch pad RAM is present
   unsigned long ulDataScratchPadRamStart;      // data scratch pad RAM start address
   unsigned long ulDataScratchPadRamEnd;        // data scratch pad RAM end address
   // MIPS CP0 
   unsigned long ulCP0RegNumberforConfig;       // number of CP0 register used as config (may differ)
   unsigned long ulCP0RegNumberforDebug;        // number of CP0 register used as debug (may differ)
   unsigned long ulCP0RegNumberforEPC;
   unsigned long ulCP0RegNumberforDEPC;
   unsigned long ulCP0BitOffsetInConfigforEndianMode;
   unsigned long ulConfigORMask;                // OR mask for CP0 config register
   unsigned long ulConfigANDMask;               // AND mask for CP0 config register
   unsigned char bBackupTLBAndCacheRegisters;   // are TLB and cache registers stored in context
   // MIPS core processor context
   unsigned long *pulProcessorContext;          // pointer to array with MIPS processor context (must be allocated separately)
   unsigned long *pulShadowRegisterSet[MAX_NUM_SHADOW_REGISTER_SETS];         // shadow register sets for MIPS processor
   // Misc 
   unsigned long ulAppWriteData;
   unsigned char bOddAddressForHWMIPS16BPs;     // odd address for HW breakpoint MIPS16
   unsigned long ulExpectedControlDatWrite;
   unsigned long ulExpectedControlInsDatFetch;
   unsigned long ulBackupOfStatusRegister;
   unsigned long ulDefaultValueForDCR;          // Value to be written to DCR on each break from emulation
   unsigned long ulInstBrkStatus;
   unsigned long ulDataBrkStatus;
   unsigned long ulDefaultControlProbeWillService;
   unsigned long ulDefaultControlCompleteAccess;
   unsigned char bProbeTrapFlagExists;    // ProbeTrap in Control Register Doesn't exist for EJTAG2_0 and downwards
   unsigned int  uiMaxDW2Loops;
   unsigned char bSSInMIPS16Supported;    //Single step in MIPS16 mode
   //User settings
   int	    bUserHaltCountersIDM;
   double   dJtagFreq;
   unsigned char bUseInstrWatchForBP;     // using instruction watchpoint as HW breakpoint
   unsigned char bPerformHardReset;       // Reset Proc uses RST Line
   unsigned char bSingleStepUsingSWBP;
   unsigned char bPerformSoftProcReset;   // Reset Proc using PrRst in EJTAG_CONTROL
   unsigned char bPerformSoftPerReset;    // Reset Proc using PerRst in EJTAG_CONTROL
   unsigned char bHardBetweenSoftReset;   // Perform a hard reset while doing a softreset.
   unsigned long ulResetDelayMS;          // Time to get the target stable after a target reset.
   unsigned long ulDelayAfterResetMS;     // Number of milliseconds to delay after a reset
   unsigned char ucMemWriteBeforeReset;   // Write a value before reset to configure the memory size
   unsigned long ulMemResetWriteAddr;     // Address of Reset Write
   unsigned long ulMemResetWriteValue;    // Value of Reset Write
   unsigned char ucDMAWriteAfterReset;    // Write a value using DMA straight after a reset (Viper2 reset release for example)
   unsigned long ulDMAResetWriteAddr;     // Address of DMA Reset Write
   unsigned long ulDMAResetWriteValue;    // Value of DMA Reset Write
   unsigned char ucMemReadAfterReset;     // Read 4 words after a reset (needed for PNX8535)
   unsigned short usTargetSupply;         // Should we supply fixed 3v3 or match the target
   unsigned char ucASHDMADebuggerClient;  // Basic connection for ASH_DMA client
   unsigned char ucNoOfNOPSInERETBDS;
   unsigned char bDisableIntSS;
   unsigned char bSlowJTAGEnabled; //todo: use delay after reset instead...
   unsigned char bAssumeWordAccess;
   unsigned char bAssertHaltTwice;
   unsigned int  bEnableWriteRequired;    // Write to SFR required before write is performed
   unsigned long ulEnableWriteAddress;    // Address to write to before write is performed
   unsigned long ulEnableWriteValue;      // Value to write to SFR before write is peformed
   unsigned long ulEnableWriteMask;       // Mask to use before write is peformed
   unsigned long ulNumberOfShadowSetsPresent;
   //TO BE SORTED AND CLEANED. Most of them can be removed/replaced with existing 
   unsigned char ucDefaultValueOfClkEnBit;
   unsigned char bNeedToClearMPBit;      // Need to Clear the Memory Protection bit to get write access to DMSEG
   unsigned char bAutoHaltCountersSupported; // EJTAG feature to halt counters works otherwise Patch DEH to halt counters in debug mode
   unsigned char bLevelWhichIndicatesProcessorClockHalted;
   unsigned char bDszInsteadOfPsz;              // 1 for EJTAG 1.53 , 2 for EJTAG 2.0
   unsigned char bBasicMappingSystemInUse; // Basic Memory Mapping system in use
   unsigned char bUseOfTargetMemoryReadWriteAllowed;////No - related to bSlowJTAGEnabled
   unsigned char bEnableDSUOnBreakFromEmulation;
#if 0000
   int   bBigEndianSRECAvailable; //todo
   int   bLittleEndianSRECAvaliable;// todo
#endif
   // MIPS core DW2 applications
   TyMipsDW2Applications tyMipsDW2App;    // MIPS DW2 applications structure
   // MIPS cache entries structure
   TyCacheEntry 	 ptyCacheEntries[MAX_CACHE_ENTRIES];    // pointer to array with cache entries (MAX_CACHE_ENTRIES items)
   // MIPS breakpoints
   TyMipsBreakpoints tyMipsBreakpoints;   // MIPS breakpoint structure
   TyMipsDSU 		 tyMipsDSU;           // MIPS DSU (debug support settings)
   TyTraceSettings   tyTraceSettings;
   //Pipelinr information
   TyPipelineInfo Pi;
   //Multicore information
   unsigned long ulCoreSelectCode;
   unsigned long ulCoreSelectIRLength;
   TyMultiCoreDeviceInfo tyMultiCoreDeviceInfo[MAX_DEVICE_IN_CHAIN];
   // diagnostics registers
#ifdef USE_DIAGNOSTIC_REGISTERS
   unsigned long pulDiagnosticRegisters[MAX_DIAGNOSTIC_REGISTERS];
#endif
} TyMLHandle;

// DW2
#define MAX_APPLICATION_SIZE  1024 // 1KB
#define LENGTH_OF_MEM_RESERVED_FOR_CACHE_ROUTINE 0x200      // 0.5 KB
#define BIG_ENDIAN_CACHE_OFFSET     (LENGTH_OF_MEM_RESERVED_FOR_CACHE_ROUTINE / 2) 
typedef enum
{
   MAGIC_TGT_RETURN           = 0x0,               // RunTgtCmd Symbol
   MAGIC_WRITEMEM             = 0x1,               // Address in Target memory where the writemem command is stored
   MAGIC_READMEM              = 0x2,               // Address in Target memory where the readmem command is stored
   MAGIC_CACHE_INS_LOCATION   = 0x3,               // Address in Target memory where the cache instruction is stored
   MAGIC_LAST_ENTRY           = 0x4
} TySymbolIdentifier;

typedef enum
{
   USR_APP        = 0x0,
   SCX00SCR       = 0x1,        // Save Context (EJTAG 2.0) Stop timers, cache debug enabled.
   SCX01RCR       = 0x2,        // Save Context (EJTAG 2.5) Cache debug enabled.
   RCX00RRR       = 0x3,        // Restore Context (EJTAG 2.0 Type)
   RCX01RRR       = 0x4,        // Restore Context (EJTAG 2.5 Type)
   RUNTGCMD       = 0x5,        // Run Target Command ( Jump to target memory )
   FSHCACHEL      = 0x6,        // Flush Cache Code. (Different SREC depending on CFG file, Little Endian Version)
   FSHCACHEB      = 0x7,        // Flush Cache Code. (Different SREC depending on CFG file, Big Endian Version)
   SCX02RCR       = 0x8,        // Save Context (EJTAG 3.1 for Microchip M4K Limerick) Cache debug enabled.
   RCX02RRR       = 0x9,        // Restore Context (EJTAG 3.1 for Microchip M4K Limerick)    
   SAV00SRS       = 0xA,        // Save Shadow Register Set 
   RES00SRS       = 0xB,        // Restore Shadow Register Set                                
   SCXBCMAP       = 0xC,        // Save Context for BRCM targets
   RCXBCMAP       = 0xD,        // Restore Context for BRCM targets
   LAST_APP       = 0xE         // Always keep this as the highest number
} TyApplicationType;

typedef struct
{
   TyApplicationType atApp;
   char szSrecFilename[MAX_PATH];
   unsigned long ulNewLocation;
   int bRelocate;
} TyApplicationSpecs;

typedef struct
{
   unsigned long        ulSymbolAddress;     // Left as Struct in case more members are needed
} TySymbol;                                  // Used to store Symbol Identifier Versus Address

typedef struct 
{
   unsigned long  ulStartAddress;
   unsigned long  ulLength;
   unsigned char  pucData[MAX_APPLICATION_SIZE];
   unsigned char  pucDataBackup[MAX_APPLICATION_SIZE];
   TySymbol       tySymbolArray[MAGIC_LAST_ENTRY];
   int            bActive;
   int            bBigEndian;
} DW2_Application;

#define NUM_BITS_IN_ULONG                             32
#define MAX_SUPPORTED_BIT_LENGTH 128
//#define MAX_SUPPORTED_BIT_LENGTH 448
#define NUM_BITS_IN_BYTE         8

#define NUM_BYTES_IN_BIT_ARRAY    (MAX_SUPPORTED_BIT_LENGTH/NUM_BITS_IN_BYTE)
#define NUM_ULONGS_IN_BIT_ARRAY   (MAX_SUPPORTED_BIT_LENGTH/NUM_BITS_IN_ULONG)
typedef struct
{
   // For convenient access as bytes or ulongs, use a union
   union {
         unsigned char ucBitArray[NUM_BYTES_IN_BIT_ARRAY];
         unsigned long ulBitArray[NUM_ULONGS_IN_BIT_ARRAY];
         } Array;
} TyBitArray;

typedef enum TyExpectedOperation
{
   INSTRUCTIONFETCH,
   DATAFETCH,
   WRITEFROMPROCESSOR,
   INSTRUCTIONFETCH_IC,    // Same as InstructionFetch   but Ignore Control Register
   DATAFETCH_IC,           // Same as DataFetch          but Ignore Control Register
   WRITEFROMPROCESSOR_IC,  // Same as WriteFromProcessor but Ignore Control Register
   WRITEFROMPROCESSOR_UA   // Same as WriteFromProcessor but uses the address passed.
}TyExpectedOperation;

typedef enum 
{
   RESETOCCURRED,
   NOTINDEBUGMODE,
   LOWPOWERDETECTED,
   HALTDETECTED,
   PERIPHERALRESETOCCURRED,
   NOACCESSPENDING,
   PROBENOTENABLED,
   TRAPFROMNORMALMEMORY,
   READBYTE,
   READHALFWORD,
   READWORD,
   READTRIPLE,
   WRITEBYTE,
   WRITEHALFWORD,
   WRITEWORD,
   WRITETRIPLE,
   INVALIDSITUATION,
   STOPEXECUTION,
   EXECNOPINBDSTHENSTOP,
   DEBUGEXCEPTIONOCCURRED
}TyTypeOfOperation;

// midlayer function prototypes
int MLGetLibPath(char *pszLibPath, int iSize);
int ML_ExecuteDW2(TyMLHandle *ptyHandle, void *pvApp, unsigned int uiAppBytes, void *pvDataToProc, unsigned int uiDataToProcBytes, 
                  void *pvDataFromProc, unsigned int uiDataFromProcBytes);
int ML_MDIRead(TyMLHandle *ptyHandle, unsigned long ulSrcResource, unsigned long ulSrcOffset, unsigned char *pucBuff, unsigned long ulCount, unsigned long ulObjectSize);
int ML_MDIWrite(TyMLHandle *ptyHandle, unsigned long ulSrcResource, unsigned long ulSrcOffset, unsigned char *pucBuff, unsigned long ulCount, unsigned long ulObjectSize);


void ML_SetupConfig0Masks(TyMLHandle *ptyHandle, int bEnableKSEG0CacheForNow);
int ML_ScanIR(TyMLHandle *ptyHandle, unsigned long ulIRLength, unsigned long *pulIRDataIn, unsigned long *pulIRDataOut);
int ML_ScanDR(TyMLHandle *ptyHandle,unsigned long ulDRLength,unsigned long *pulDRDataIn,unsigned long *pulDRDataOut);
int ML_ScanIRandDR(TyMLHandle *ptyHandle, unsigned long ulIRLength, unsigned long *pulIRDataIn,
                   unsigned long ulDRLength, unsigned long *pulDRDataIn, unsigned long *pulDRDataOut);
int ML_InstallCacheFlushApp(TyMLHandle *ptyHandle, unsigned long ulData);
void ML_EnableMemoryWriting(TyMLHandle *ptyHandle, int bStartExecute, int bFinishExecute, int bReset);
int ML_SetBootType(TyMLHandle *ptyHandle, unsigned char bNormalBoot);
void ML_Delay(unsigned int uiMiliseconds);
int ML_SetTargetEndian(TyMLHandle *ptyHandle, unsigned char bSetToBigEndian);


int ML_ConvertDLError(int iError);
int ML_NotImplementedYet(TyMLHandle *ptyHandle);
// midlayer miscellaneous functions
unsigned long ML_SwapEndianWord(unsigned long ulWord);
unsigned short ML_SwapEndianHalfword(unsigned short usHalfword);
void ML_SwapEndianess(void *pvData, unsigned long ulCount, unsigned long ulObjectSize);
void ML_SetDllPath(const char *pszDllPath);
//int ML_DetermineSettings(TyMLHandle *ptyHandle);
int ML_InitConnection(TyMLHandle *ptyHandle);
TyError ML_Reset (TyMLHandle *ptyHandle,
				  unsigned long ulMode, 
				  TyUserSettings tyUserSettings);
TyTypeOfOperation ML_ExecuteInstruction (TyMLHandle 	*ptyHandle,
										 unsigned long 	*pulExpectedAddress,
										 unsigned long 	ulInstruction,
										 unsigned long 	ulDataIn,
										 unsigned long 	*pulDataOut,
										 TyExpectedOperation Operation);
TyError ML_StopExecution(TyMLHandle *ptyHandle);
TyError ML_GetStatus(TyMLHandle *ptyHandle,
					 TyStatus * tyStatus,
					 int bUseTargetMemoryIfNeeded);


TyTypeOfOperation ML_DetermineTypeOfOperation (TyMLHandle *ptyHandle);
TyError ML_DeviceDisconnect(TyMLHandle 	*ptyHandle);
TyError ML_Step(TyMLHandle 	*ptyHandle,
                unsigned long ulMode);
TyError ML_Execute(TyMLHandle 	*ptyHandle);
void ML_ConvertVersionDate(unsigned long ulVersion, 
                           unsigned long ulDate, 
                           char *pszBuffer, 
                           unsigned int uiSize);
TyError ML_Reset_Ex (TyMLHandle *ptyHandle,
                     unsigned long ulMode, 
                     TyUserSettings tyUserSettings, 
                     int bSpecifiedProcessor);
void ML_ConfigureMulticoreSupport(TyMLHandle *ptyHandle,unsigned char bInitial);
TyError ML_RunTargetCommand(TyMLHandle 	*ptyHandle,
                            unsigned long ulAddressOfTargetApplication,
                            unsigned long *pulParamsPassed,
                            unsigned long ulNumberPassed,
                            unsigned long *pulParamsReturned,
                            unsigned long ulNumberReturned);
/*TyError ML_EnableTargetPasswd(TyMLHandle 	*ptyHandle,
							  char szPasswdFileName[]);*/
TyError ML_EnableTargetPasswdSetting(TyMLHandle 	*ptyHandle,
							  TyUserSettings *ptyUserSettings);
void  ML_PasswordSetting( TyMLHandle    *ptyHandle ,char *PasswordString ) ;
TyError ML_LoadDW2Applications ( TyApplicationSpecs 	*ptyAppSpecArray,
                                 TyApplicationType 	atApp,
                                 unsigned long  **pulApp,
                                 unsigned long  *pulAppSize,
                                 unsigned long  *pulAppStartAddress,
                                 unsigned long  *pulInstrOffset,
                                 TySymbolIdentifier tySimbol);

// midlayer functions related to MIPS register access
int MLR_ReadRegister(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long ulRegisterType, unsigned long *pulData);
int MLR_WriteRegister(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long ulRegisterType, unsigned long ulData);


// midlayer functions related to MIPS memory access
int MLM_ReadMemory(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
int MLM_WriteMemory(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);

// MLM functions (dealing with checking range, MMU, breakpoints, etc.)
int MLM_ReadMemoryVirtual(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
int MLM_WriteMemoryVirtual(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
int MLM_ReadMemoryPhysical(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength);
int MLM_WriteMemoryPhysical(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength);
// MLM functions (dealing with address/size allignment, etc.)
int MLM_ReadMemoryEjtag(TyMLHandle *ptyHandle, unsigned long ulStartOffset, unsigned char *pucData, unsigned long ulLength);
int MLM_WriteMemoryEjtag(TyMLHandle *ptyHandle, unsigned long ulStartOffset, unsigned char *pucData, unsigned long ulLength);
int MLM_ReadMemoryDma(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength);
int MLM_WriteMemoryDma(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength);
int MLM_ReadMemoryDW2(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
int MLM_WriteMemoryDW2(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
// lowest MLM functions calling diskware (data format specific)
int MLM_ReadWordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian);
int MLM_ReadHalfwordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian);
int MLM_ReadBytesDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData);
int MLM_WriteWordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian);
int MLM_WriteHalfwordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian);
int MLM_WriteBytesDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData);
int MLM_ReadWordsFast(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData);
int MLM_WriteWordsFast(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData);
int MLM_ReadWordDma(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvData);
TyError MLM_ReadWord(TyMLHandle *ptyHandle,unsigned long ulStartAddress,unsigned long *pulDestinationPointer);
int MLM_WriteWordDma(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvData);
int MLM_ReadMultipleWordsDma(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData);
int MLM_WriteMultipleWordsDma(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData);
int MLM_WriteWords(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, unsigned long *pulData);
int MLM_WriteWord(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulData);
int MLM_WriteMemoryStd(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
int MLM_ReadWords(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, unsigned long *pulData);
int MLM_ReadMemoryStd(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);

// miscellaneous memory functions
void MLM_LearnPipeLine(TyMLHandle *ptyHandle);
unsigned short MLM_GetHalfWord(TyMLHandle *ptyHandle,
                               unsigned long ulStartAddress);
unsigned long MLM_GetWord(TyMLHandle *ptyHandle,
                          unsigned long ulStartAddress);
// midlayer functions related to MIPS breakpoint handling
int MLB_WriteBackSoftwareBreakpoints(TyMLHandle *ptyHandle, unsigned char bWBOriginal);
int MLB_AddBPSToWriteBuff(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength); 
int MLB_RemoveBPSFromReadBuff(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength);
int MLB_DetermineDSUConfiguration(TyMLHandle *ptyHandle);
int MLB_SetBP(TyMLHandle *ptyHandle, unsigned long ulBpVirtualAddress, unsigned long ulBpAccessAddress, TyBPType tyBpType, unsigned long ulBpSize);
int MLB_ClearBP(TyMLHandle *ptyHandle, unsigned long ulAddress, TyBPType *ptyBpType, unsigned long *pulBpSize, unsigned char *pbNoBpPresent, unsigned long *pulBpAccessAddress);
int MLB_QueryBP(TyMLHandle *ptyHandle, unsigned long ulAddress, TyBPType *ptyBpType, unsigned long *pulBpSize, unsigned char *pbNoBpPresent);
int MLB_SetComplexHWBP(TyMLHandle *ptyHandle, TyBpFlags *ptyBreakpointFlags);
int MLB_ClearAllBP(TyMLHandle *ptyHandle);
int MLB_TempDisableAllHWBPs(TyMLHandle *ptyHandle);
int MLB_RestoreHWBPs(TyMLHandle *ptyHandle);
unsigned char MLB_CheckWatchpointBPAddress(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long *pulBpIdentifier);
unsigned char MLB_CheckHWBPUsed(TyMLHandle *ptyHandle);
unsigned long MLB_GetBPIdForComplexBreak(TyMLHandle *ptyHandle,
										 int bInstBP, 
										 unsigned long ulBrkStatus);


// midlayer functions related to MIPS cache
int MLC_InvalidateCacheOnReset(TyMLHandle *ptyHandle);
int MLC_ReadInstScratchPad(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize);
int MLC_FlushCache(TyMLHandle *ptyHandle, unsigned int tyCacheType, TyCacheFlush tyFlushType);
int MLC_ReadCacheResourceArrayRange(TyMLHandle *ptyHandle, unsigned long ulSrcResource, unsigned long ulStartEntry, unsigned long ulEndEntry, 
                                    void *pvData, unsigned char bScratchpadRam);
int MLC_PerformCacheAction(TyMLHandle *ptyHandle, unsigned long ulAction, unsigned long ulAddress, unsigned long *pulTagLo, unsigned long *pulDataLo);
int MLC_AccessCacheTag(TyMLHandle *ptyHandle, unsigned char bRead, unsigned long ulSrcResource, unsigned long ulEntry,
                       unsigned long ulWordSelect, unsigned long *pulTagLo, unsigned long *pulDataLo, unsigned long ulCount, unsigned long ulObjectSize);
int MLC_Hit_Invalidate_I(TyMLHandle *ptyHandle, unsigned long ulAddress);
int MLC_Hit_Invalidate_D(TyMLHandle *ptyHandle, unsigned long ulAddress);
int MLC_Hit_Writeback_Inv_D(TyMLHandle *ptyHandle, unsigned long ulAddress);
int MLC_Hit_Writeback_D(TyMLHandle *ptyHandle, unsigned long ulAddress);
unsigned char MLC_EnableKSeg0CachingForScratching(TyMLHandle *ptyHandle, unsigned char bHandleCall, unsigned char bEnable, unsigned long *pulSavedValue);

unsigned char MLC_AccessOverLapsInstScratchPad(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned long ulLength);
unsigned char MLC_AccessOverLapsDataScratchPad(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned long ulLength);

int MLR_ReadCP0Register(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long *pulData, unsigned long ulSel);
int MLR_WriteCP0Register(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long ulData, unsigned long ulSel);
// midllayer functions related to MIPS TLB and MMU
int MLT_ReadTLB(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvTLBEntry, unsigned long ulCount, unsigned long ulObjectSize);
int MLT_WriteTLB(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvTLBEntry, unsigned long ulCount, unsigned long ulObjectSize);
void CreateBackupOfTLB(TyMLHandle *ptyHandle);

#endif   // MIDLAYER_H_
