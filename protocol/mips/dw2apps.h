/****************************************************************************
       Module: dw2apps.h
     Engineer: Vitezslav Hola
  Description: Header with definition of implicit MIPS DW2 applications.
               These applications are hardcoded and can be run 
               by MIPS diskware.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#ifndef DW2APPS_H_
#define DW2APPS_H_

// Following code contains implicit MIPS DW2 applications. These applications are basic
// access to MIPS registers/memory and are written in general way.
// All applications are intented to start at 0xFF200200 address. Maximum size is 64kB.
// Data from DW2 application to diskware/debugger are from 0xFF210200 (up to 64kB).
// Data from diskware/debugger to DW2 application are from 0xFF220200 (up to 64kB).
//
// When adding/modifying DW2 application in this header, make sure that:
// 1) sufficient number of NOPs added after return to debug vector (0xFF200200), recommended is 5 nops.
// 2) number of instructions should be determined by sizeof() operator.
// 

// WriteWords application - write words into MIPS memory, getting address at 0xff220200, number of words at 0xff220204 and 
//                          words to write from 0xff220208 (>0, up to 8192 words => up to 32 kB memory), endianess does not matter here
unsigned long pulMipsDW2AppCodeWriteWords[] = {
   0x3c08ff22,                      // 0xff200200     lui $t0,0xff22
   0x8d090200,                      // 0xff200204     lw $t1,0x200($t0)
   0x8d0a0204,                      // 0xff200208     lw $t2,0x204($t0)
   0x3c08ff22,                      // 0xff20020c     lui $t0,0xff22 (in load delay slot)
   0x35080208,                      // 0xff200210     ori $t0,$t0,0x0208
   0x00000000,                      // 0xff200214     nop
   0x8d010000,                      // 0xff200218     lw $at,0($t0)             (LOOP)
   0x254affff,                      // 0xff20021c     addiu $t2,$t2,-1
   0x25080004,                      // 0xff200220     addiu $t0,$t0,4
   0xad210000,                      // 0xff200224     sw $at,0($t1)
   0x25290004,                      // 0xff200228     addiu $t1,$t1,4
   0x1540fffa,                      // 0xff20022c     bne $t2,0,LOOP
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x0000000f,                      // 0xff200238     sync (may be replaced by nop in midlayer if sync is not supported)
   0x0bc80080,                      // 0xff20023c     j 0xff200200
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000                       // 0xff200250     nop
};
unsigned long ulMipsDW2AppInstructionsWriteWords = sizeof(pulMipsDW2AppCodeWriteWords) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetWriteWords = 0x0e;            // this variable MUST contain inst offset for sync instruction in code

// WriteHalfwords application - write halfwords into MIPS memory, getting address at 0xff220200, number of words at 0xff220204 and 
//                              halfwords to write from 0xff220208 (>0, up to 16384 halfwords => up to 32 kB memory), endianess does not matter here
unsigned long pulMipsDW2AppCodeWriteHalfwords[] = {
   0x3c08ff22,                      // 0xff200200     lui $t0,0xff22
   0x8d090200,                      // 0xff200204     lw $t1,0x200($t0)
   0x8d0a0204,                      // 0xff200208     lw $t2,0x204($t0)
   0x3c08ff22,                      // 0xff20020c     lui $t0,0xff22 (in load delay slot)
   0x35080208,                      // 0xff200210     ori $t0,$t0,0x0208
   0x00000000,                      // 0xff200214     nop
   0x85010000,                      // 0xff200218     lh $at,0($t0)             (LOOP)
   0x254affff,                      // 0xff20021c     addiu $t2,$t2,-1
   0x25080002,                      // 0xff200220     addiu $t0,$t0,2
   0xa5210000,                      // 0xff200224     sh $at,0($t1)
   0x25290002,                      // 0xff200228     addiu $t1,$t1,2
   0x1540fffa,                      // 0xff20022c     bne $t2,0,LOOP
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x0000000f,                      // 0xff200238     sync (may be replaced by nop in midlayer if sync is not supported)
   0x0bc80080,                      // 0xff20023c     j 0xff200200
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000                       // 0xff200250     nop
};
unsigned long ulMipsDW2AppInstructionsWriteHalfwords = sizeof(pulMipsDW2AppCodeWriteHalfwords) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetWriteHalfwords = 0x0e;        // this variable MUST contain inst offset for sync instruction in code

// WriteBytes application - write bytes into MIPS memory, getting address at 0xff220200, number of words at 0xff220204 and 
//                          bytes to write from 0xff220208 (>0, up to 32768 words => up to 32 kB memory), endianess does not matter here
unsigned long pulMipsDW2AppCodeWriteBytes[] = {
   0x3c08ff22,                      // 0xff200200     lui $t0,0xff22
   0x8d090200,                      // 0xff200204     lw $t1,0x200($t0)
   0x8d0a0204,                      // 0xff200208     lw $t2,0x204($t0)
   0x3c08ff22,                      // 0xff20020c     lui $t0,0xff22 (in load delay slot)
   0x35080208,                      // 0xff200210     ori $t0,$t0,0x0208
   0x00000000,                      // 0xff200214     nop
   0x81010000,                      // 0xff200218     lb $at,0($t0)             (LOOP)
   0x254affff,                      // 0xff20021c     addiu $t2,$t2,-1
   0x25080001,                      // 0xff200220     addiu $t0,$t0,1
   0xa1210000,                      // 0xff200224     sb $at,0($t1)
   0x25290001,                      // 0xff200228     addiu $t1,$t1,1
   0x1540fffa,                      // 0xff20022c     bne $t2,0,LOOP
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x0000000f,                      // 0xff200238     sync (may be replaced by nop in midlayer if sync is not supported)
   0x0bc80080,                      // 0xff20023c     j 0xff200200
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000                       // 0xff200250     nop
};
unsigned long ulMipsDW2AppInstructionsWriteBytes = sizeof(pulMipsDW2AppCodeWriteBytes) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetWriteBytes = 0x0e;            // this variable MUST contain inst offset for sync instruction in code

// ReadWords application - read words from MIPS memory, getting address at 0xff220200, number of words at 0xff220204 and 
//                         words to store from 0xff210200 (>0, up to 8192 words => up to 32 kB memory), endianess does not matter here
unsigned long pulMipsDW2AppCodeReadWords[] = {
   0x3c08ff22,                      // 0xff200200     lui $t0,0xff22
   0x8d090200,                      // 0xff200204     lw $t1,0x200($t0)
   0x8d0a0204,                      // 0xff200208     lw $t2,0x204($t0)
   0x3c08ff21,                      // 0xff20020c     lui $t0,0xff21
   0x35080200,                      // 0xff200210     ori $t0,$t0,0x0200
   0x00000000,                      // 0xff200214     nop
   0x8d210000,                      // 0xff200218     lw $at,0($t1)
   0x254affff,                      // 0xff20021c     addiu $t2,$t2,-1
   0x25290004,                      // 0xff200220     addiu $t1,$t1,4
   0xad010000,                      // 0xff200224     sw $at,0($t0)
   0x25080004,                      // 0xff200228     addiu $t0,$t0,4
   0x1540fffa,                      // 0xff20022c     bne $t2,0,LOOP
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x0000000f,                      // 0xff200238     sync (may be replaced by nop in midlayer if sync is not supported)
   0x0bc80080,                      // 0xff20023c     j 0xff200200
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000                       // 0xff200250     nop
};
unsigned long ulMipsDW2AppInstructionsReadWords = sizeof(pulMipsDW2AppCodeReadWords) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetReadWords = 0x0e;             // this variable MUST contain inst offset for sync instruction in code

// ReadHalfwords application - read halfwords from MIPS memory, getting address at 0xff220200, number of halfwords at 0xff220204 and 
//                             halfwords to store from 0xff210200 (>0, up to 16384 halfwords => up to 32 kB memory), endianess does not matter here
unsigned long pulMipsDW2AppCodeReadHalfwords[] = {
   0x3c08ff22,                      // 0xff200200     lui $t0,0xff22
   0x8d090200,                      // 0xff200204     lw $t1,0x200($t0)
   0x8d0a0204,                      // 0xff200208     lw $t2,0x204($t0)
   0x3c08ff21,                      // 0xff20020c     lui $t0,0xff21
   0x35080200,                      // 0xff200210     ori $t0,$t0,0x0200
   0x00000000,                      // 0xff200214     nop
   0x85210000,                      // 0xff200218     lh $at,0($t1)
   0x254affff,                      // 0xff20021c     addiu $t2,$t2,-1
   0x25290002,                      // 0xff200220     addiu $t1,$t1,2
   0xa5010000,                      // 0xff200224     sh $at,0($t0)
   0x25080002,                      // 0xff200228     addiu $t0,$t0,2
   0x1540fffa,                      // 0xff20022c     bne $t2,0,LOOP
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x0000000f,                      // 0xff200238     sync (may be replaced by nop in midlayer if sync is not supported)
   0x0bc80080,                      // 0xff20023c     j 0xff200200
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000                       // 0xff200250     nop
};
unsigned long ulMipsDW2AppInstructionsReadHalfwords = sizeof(pulMipsDW2AppCodeReadHalfwords) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetReadHalfwords = 0x0e;         // this variable MUST contain inst offset for sync instruction in code

// ReadBytes application - read bytes from MIPS memory, getting address at 0xff220200, number of bytes at 0xff220204 and 
//                         bytes to store from 0xff210200 (>0, up to 32768 bytes => up to 32 kB memory), endianess does not matter here
unsigned long pulMipsDW2AppCodeReadBytes[] = {
   0x3c08ff22,                      // 0xff200200     lui $t0,0xff22
   0x8d090200,                      // 0xff200204     lw $t1,0x200($t0)
   0x8d0a0204,                      // 0xff200208     lw $t2,0x204($t0)
   0x3c08ff21,                      // 0xff20020c     lui $t0,0xff21
   0x35080200,                      // 0xff200210     ori $t0,$t0,0x0200
   0x00000000,                      // 0xff200214     nop
   0x81210000,                      // 0xff200218     lb $at,0($t1)
   0x254affff,                      // 0xff20021c     addiu $t2,$t2,-1
   0x25290001,                      // 0xff200220     addiu $t1,$t1,1
   0xa1010000,                      // 0xff200224     sb $at,0($t0)
   0x25080001,                      // 0xff200228     addiu $t0,$t0,1
   0x1540fffa,                      // 0xff20022c     bne $t2,0,LOOP
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x0000000f,                      // 0xff200238     sync (may be replaced by nop in midlayer if sync is not supported)
   0x0bc80080,                      // 0xff20023c     j 0xff200200
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000                       // 0xff200250     nop
};
unsigned long ulMipsDW2AppInstructionsReadBytes = sizeof(pulMipsDW2AppCodeReadBytes) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetReadBytes = 0x0e;             // this variable MUST contain inst offset for sync instruction in code

// WriteCP0 application - write CP0 register, value to write at 0xff220200
//                        instruction mtc0 $t0,$1 must be modified before using with required CP0 register number (instead of $1)
unsigned long pulMipsDW2AppCodeWriteCP0[] = {
   0x3c01ff22,                      // 0xff200200     lui $at,0xff22
   0x8c280200,                      // 0xff200204     lw $t0,0x200($at)
   0x00000000,                      // 0xff200208     nop
   0x00000000,                      // 0xff20020c     nop
   0x40880800,                      // 0xff200210     mtc0 $t0,$1 (to be modified by midlayer with required CP0 number) (offset 4)
   0x00000000,                      // 0xff200214     nop
   0x0bc80080,                      // 0xff200218     j 0xff200200
   0x00000000,                      // 0xff20021c     nop
   0x00000000,                      // 0xff200220     nop
   0x00000000,                      // 0xff200224     nop
   0x00000000,                      // 0xff200228     nop
   0x00000000                       // 0xff20022c     nop
};
unsigned long ulMipsDW2AppInstructionsWriteCP0 = sizeof(pulMipsDW2AppCodeWriteCP0) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetWriteCP0 = 0x04;              // this variable mus contain inst offset for mtc0 $t0,$1 instruction

// ReadCP0 application - read CP0 register, value to store at 0xff210200
//                       instruction mfc0 $t0,$1 must be modified before using with required CP0 register number (instead of $1)
unsigned long pulMipsDW2AppCodeReadCP0[] = {
   0x3c01ff21,                      // 0xff200200     lui $at,0xff21
   0x40080800,                      // 0xff200204     mfc0 $t0,$1 (to be modified by midlayer with required CP0 number) (offset 1)
   0x00000000,                      // 0xff200208     nop
   0x00000000,                      // 0xff20020c     nop
   0xac280200,                      // 0xff200210     sw $t0,0x200($at)
   0x00000000,                      // 0xff200214     nop
   0x00000000,                      // 0xff200218     nop
   0x0bc80080,                      // 0xff20021c     j 0xff200200
   0x00000000,                      // 0xff200220     nop
   0x00000000,                      // 0xff200224     nop
   0x00000000,                      // 0xff200228     nop
   0x00000000,                      // 0xff20022c     nop
   0x00000000                       // 0xff200230     nop
};
unsigned long ulMipsDW2AppInstructionsReadCP0 = sizeof(pulMipsDW2AppCodeReadCP0) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetReadCP0 = 0x01;               // this variable mus contain inst offset for mfc0 $t0,$1 instruction

// WriteTLB application - write TLB entry for MIPS, input values are index (0xff220200), entryLo0 (0xff220204), entryLo1 (0xff220208),
//                        entryHi (0xff22020c) and pagemask (0xff220210)
unsigned long pulMipsDW2AppCodeWriteTLB[] = {
   0x3c10ff22,                      // 0xff200200     lui $s0,0xff22
   0x8e050200,                      // 0xff200204     lw $a1,0x200($s0)
   0x3c11ff21,                      // 0xff200208     lui $s1,0xff21
   0x00000000,                      // 0xff20020c     nop
   0x40850000,                      // 0xff200210     mtc0 $a1,$0
   0x00000000,                      // 0xff200214     nop
   0x00000000,                      // 0xff200218     nop
   0x8e080204,                      // 0xff20021c     lw $t0,0x204($s0)
   0x8e090208,                      // 0xff200220     lw $t1,0x208($s0)
   0x8e0a020c,                      // 0xff200224     lw $t2,0x20c($s0)
   0x8e0b0210,                      // 0xff200228     lw $t3,0x210($s0)
   0x40881000,                      // 0xff20022c     mtc0 $t0,$2
   0x40891800,                      // 0xff200230     mtc0 $t1,$3
   0x408a5000,                      // 0xff200234     mtc0 $t2,$10
   0x408b2800,                      // 0xff200238     mtc0 $t3,$5
   0x00000000,                      // 0xff20023c     nop
   0x42000002,                      // 0xff200240     tlbwi
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x0bc80080,                      // 0xff200250     j 0xff200200
   0x00000000,                      // 0xff200254     nop
   0x00000000,                      // 0xff200258     nop
   0x00000000,                      // 0xff20025c     nop
   0x00000000,                      // 0xff200260     nop
   0x00000000                       // 0xff200264     nop
};
unsigned long ulMipsDW2AppInstructionsWriteTLB = sizeof(pulMipsDW2AppCodeWriteTLB) / sizeof(unsigned long);

// ReadTLB application - read TLB entries for MIPS, start index (0xff220200), stop index (0xff220204) must be (number of indexes + start index)
//                       for each index read, entryLo0 (0xff210200 + current_index * 0x10), entryLo1 (0xff210204 + current_index * 0x10)
//                       entryHi (0xff210208 + current_index * 0x10) and pagemask (0xff21020c + current_index * 0x10)
unsigned long pulMipsDW2AppCodeReadTLB[] = {
   0x3c10ff22,                      // 0xff200200     lui $s0,0xff22
   0x8e050200,                      // 0xff200204     lw $a1,0x200($s0)
   0x8e060204,                      // 0xff200208     lw $a2,0x204($s0)
   0x3c11ff21,                      // 0xff20020c     lui $s1,0xff21
   0x36310200,                      // 0xff200210     ori $s1,$s1,0x0200
   0x40850000,                      // 0xff200214     mtc0 $a1,$0       (LOOP)
   0x00000000,                      // 0xff200218     nop
   0x42000001,                      // 0xff20021c     tlbr
   0x24a50001,                      // 0xff200220     addiu $a1,$a1,1
   0x40081000,                      // 0xff200224     mfc0 $t0,$2
   0x40091800,                      // 0xff200228     mfc0 $t1,$3
   0x400a5000,                      // 0xff20022c     mfc0 $t2,$10
   0x400b2800,                      // 0xff200230     mfc0 $t3,$5
   0xae280000,                      // 0xff200234     sw $t0,0x0($s1)
   0xae290004,                      // 0xff200238     sw $t1,0x4($s1)
   0xae2a0008,                      // 0xff20023c     sw $t2,0x8($s1)
   0xae2b000c,                      // 0xff200240     sw $t3,0xc($s1)
   0x26310010,                      // 0xff200244     addiu $s1,$s1,0x10
   0x14a6fff2,                      // 0xff200248     bne $a1,$a2,LOOP
   0x00000000,                      // 0xff20024c     nop
   0x00000000,                      // 0xff200250     nop
   0x0bc80080,                      // 0xff200254     j 0xff200200
   0x00000000,                      // 0xff200258     nop
   0x00000000,                      // 0xff20025c     nop
   0x00000000,                      // 0xff200260     nop
   0x00000000,                      // 0xff200264     nop
   0x00000000                       // 0xff200268     nop
};
unsigned long ulMipsDW2AppInstructionsReadTLB = sizeof(pulMipsDW2AppCodeReadTLB) / sizeof(unsigned long);

// WriteACX application - write ACX (8-bit register) for SmartMIPS, value at 0xff220200
unsigned long pulMipsDW2AppCodeWriteACX[] = {
   0x00004010,                      // 0xff200200     mfhi $t0
   0x00004812,                      // 0xff200204     mflo $t1
   0x3c01ff22,                      // 0xff200208     lui $at,0xff22
   0x8c2b0200,                      // 0xff20020c     lw $t3,0x200($at)
   0x00000000,                      // 0xff200210     nop
   0x01600011,                      // 0xff200214     mthi $t3
   0x00000000,                      // 0xff200218     nop
   0x02000053,                      // 0xff20021c     mtlhx $s0
   0x00000000,                      // 0xff200220     nop
   0x01000011,                      // 0xff200224     mthi $t0
   0x01200013,                      // 0xff200228     mtlo $t1
   0x00000000,                      // 0xff20022c     nop
   0x0bc80080,                      // 0xff200230     j 0xff200200
   0x00000000,                      // 0xff200234     nop
   0x00000000,                      // 0xff200238     nop
   0x00000000,                      // 0xff20023c     nop
   0x00000000,                      // 0xff200240     nop
   0x00000000                       // 0xff200244     nop
};
unsigned long ulMipsDW2AppInstructionsWriteACX = sizeof(pulMipsDW2AppCodeWriteACX) / sizeof(unsigned long);

// ReadACX application - read ACX (8-bit register) for SmartMIPS, value to store at 0xff210200
unsigned long pulMipsDW2AppCodeReadACX[] = {
   0x00008052,                      // 0xff200200     mflhxu $s0
   0x3c01ff21,                      // 0xff200204     lui $at,0xff21
   0x00004010,                      // 0xff200208     mfhi $t0
   0x00000000,                      // 0xff20020c     nop
   0x02000053,                      // 0xff200210     mtlhx $s0
   0xac280200,                      // 0xff200214     sw $t0,0x200($at)
   0x00000000,                      // 0xff200218     nop
   0x0bc80080,                      // 0xff20021c     j 0xff200200
   0x00000000,                      // 0xff200220     nop
   0x00000000,                      // 0xff200224     nop
   0x00000000,                      // 0xff200228     nop
   0x00000000,                      // 0xff20022c     nop
   0x00000000                       // 0xff200230     nop
};
unsigned long ulMipsDW2AppInstructionsReadACX = sizeof(pulMipsDW2AppCodeReadACX) / sizeof(unsigned long);

// WriteCache application - write cache entry for MIPS, cache param at 0xff220200, taglo at 0xff220204
unsigned long pulMipsDW2AppCodeWriteCache[] = {
   0x3c10ff22,                      // 0xff200200     lui $s0,0xff22
   0x8e050200,                      // 0xff200204     lw $a1,0x200($s0)
   0x8e080204,                      // 0xff200208     lw $t0,0x204($s0)
   0x3c11ff21,                      // 0xff20020c     lui $s1,0xff21
   0x00000000,                      // 0xff200210     nop
   0x4088e000,                      // 0xff200214     mtc0 $t0,$28,0 (taglo)
   0xbca80000,                      // 0xff200218     cache 0x8,0($a1)
   0x00000000,                      // 0xff20021c     nop
   0x00000000,                      // 0xff200220     nop
   0x0bc80080,                      // 0xff200224     j 0xff200200
   0x00000000,                      // 0xff200228     nop
   0x00000000,                      // 0xff20022c     nop
   0x00000000,                      // 0xff200230     nop
   0x00000000,                      // 0xff200234     nop
   0x00000000                       // 0xff200238     nop
};
unsigned long ulMipsDW2AppInstructionsWriteCache = sizeof(pulMipsDW2AppCodeWriteCache) / sizeof(unsigned long);

// ReadCache application - read cache entries for MIPS, start index at 0xff220200, end index at 0xff220204, ErrCtrl modifier at 0xff220208
//                         for each index 6 words (from 0xff210200 + word*24): taglo, datalo, datalo (word1), datalo(word2), datalo (word3), taglo (waysel)
unsigned long pulMipsDW2AppCodeReadCache[] = {
   0x3c10ff22,                      // 0xff200200     lui $s0,0xff22
   0x8e050200,                      // 0xff200204     lw $a1,0x200($s0)
   0x8e060204,                      // 0xff200208     lw $a2,0x204($s0)
   0x8e070208,                      // 0xff20020c     lw $a3,0x208($s0)
   0x3c11ff21,                      // 0xff200210     lui $s1,0xff21
   0x400dd000,                      // 0xff200214     mfc0 $t5,$26,0
   0x3c0fcfff,                      // 0xff200218     lui $t7,0xcfff
   0x01ed7824,                      // 0xff20021c     and $t7,$t7,$t5
   0x01e77825,                      // 0xff200220     or $t7,$t7,$a3
   0x3c0e2000,                      // 0xff200224     lui $t6,0x2000
   0x01cf7025,                      // 0xff200228     or $t6,$t6,$t7
   0x408fd000,                      // 0xff20022c     mtc0 $t7,$26,0
   0x00000000,                      // 0xff200230     nop
   0xbca40000,                      // 0xff200234     cache 4,0x0($a1) (will be modifed by midlayer) (LOOP) (offset 0x0d)
   0x408ed000,                      // 0xff200238     mtc0 $t6,$26,0
   0x4008e000,                      // 0xff20023c     mfc0 $t0,$28,0
   0x4009e001,                      // 0xff200240     mfc0 $t1,$28,1
   0xbca40004,                      // 0xff200244     cache 4,0x4($a1) (will be modified by midlayer) (offset 0x11)
   0xae280200,                      // 0xff200248     sw $t0,0x200($s1)
   0x400ae001,                      // 0xff20024c     mfc0 $t2,$28,1
   0xbca40008,                      // 0xff200250     cache 4,0x8($a1) (will be modified by midlayer) (offset 0x14)
   0xae290204,                      // 0xff200254     sw $t1,0x204($s1)
   0x400be001,                      // 0xff200258     mfc0 $t3,$28,1
   0xbca4000c,                      // 0xff20025c     cache 4,0xC($a1) (will be modifed by midlayer) (offset 0x17)
   0xae2a0208,                      // 0xff200260     sw $t2,0x208($s1)
   0x400ce001,                      // 0xff200264     mfc0 $t4,$28,1
   0xae2b020c,                      // 0xff200268     sw $t3,0x20C($s1)
   0x4008e000,                      // 0xff20026c     mfc0 $t0,$28
   0xae2c0210,                      // 0xff200270     sw $t4,0x210($s1)
   0xae280214,                      // 0xff200274     sw $t0,0x214($s1)
   0x408fd000,                      // 0xff200278     mtc0 $t7,$26,0
   0x26310018,                      // 0xff20027c     addiu $s1,$s1,24
   0x24a50010,                      // 0xff200280     addiu $a1,$a1,0x10
   0x14a6ffeb,                      // 0xff200284     bne $a1,$a2,LOOP
   0x00000000,                      // 0xff200288     nop
   0x00000000,                      // 0xff20028c     nop
   0x408dd000,                      // 0xff200290     mtc0 $t5,$26,0
   0x00000000,                      // 0xff200294     nop
   0x0bc80080,                      // 0xff200298     j 0xff200200
   0x00000000,                      // 0xff20029c     nop
   0x00000000,                      // 0xff2002a0     nop
   0x00000000,                      // 0xff2002a4     nop
   0x00000000,                      // 0xff2002a8     nop
   0x00000000                       // 0xff2002ac     nop
};
unsigned long ulMipsDW2AppInstructionsReadCache = sizeof(pulMipsDW2AppCodeReadCache) / sizeof(unsigned long);
unsigned long pulMipsDW2AppInstOffsetsReadCache[4] = {0x0D, 0x11, 0x14, 0x17};      // this array MUST contain inst offset for cache instruction in code

// ActCache application - do cache action for MIPS, address at 0xff220200, taglo value at 0xff220204 and datalo value at 0xff220208
//                        value read from taglo at 0xff210200, value read from datalo at 0xff210204
unsigned long pulMipsDW2AppCodeActCache[] = {
   0x3c10ff22,                      // 0xff200200     lui $s0,0xff22
   0x8e050200,                      // 0xff200204     lw $a1,0x200($s0)
   0x8e080204,                      // 0xff200208     lw $t0,0x204($s0)
   0x8e090208,                      // 0xff20020c     lw $t1,0x208($s0)
   0x3c11ff21,                      // 0xff200210     lui $s1,0xff21
   0x4088e000,                      // 0xff200214     mtc0 $t0,$28,0
   0x00000000,                      // 0xff200218     nop
   0x4089e001,                      // 0xff20021c     mtc0 $t1,$28,1
   0x00000000,                      // 0xff200220     nop
   0xbca80000,                      // 0xff200224     cache 0x8,0($a1) (will be modified in midlayer) (offset 0x09)
   0x00000000,                      // 0xff200228     nop
   0x4008e000,                      // 0xff20022c     mfc0 $t0,$28,0
   0x4009e001,                      // 0xff200230     mfc0 $t1,$28,1
   0xae280200,                      // 0xff200234     sw $t0,0x200($s1)
   0xae290204,                      // 0xff200238     sw $t1,0x204($s1)
   0x00000000,                      // 0xff20023c     nop
   0x0bc80080,                      // 0xff200240     j 0xff200200
   0x00000000,                      // 0xff200244     nop
   0x00000000,                      // 0xff200248     nop
   0x00000000,                      // 0xff20024c     nop
   0x00000000,                      // 0xff200250     nop
   0x00000000                       // 0xff200254     nop
};
unsigned long ulMipsDW2AppInstructionsActCache = sizeof(pulMipsDW2AppCodeActCache) / sizeof(unsigned long);
unsigned long ulMipsDW2AppInstOffsetActCache = 0x09;           // this variable MUST contain inst offset for cache instruction in code

// ClearMPBit application - clear memory protection bit for DSEG on PR19xx MIPS processors and enable DSU
unsigned long pulMipsDW2AppCodeClearMPBit[] = {
   0x00000000,                      // 0xff200200     nop
   0x401ab000,                      // 0xff200204     mfc0 $k0,$22
   0x3c1b0002,                      // 0xff200208     lui $k1,0x0002
   0x035bd025,                      // 0xff20020c     or $k0,$k0,$k1
   0x409ab000,                      // 0xff200210     mtc0 $k0,$22
   0x00000000,                      // 0xff200214     nop
   0x3c02ff30,                      // 0xff200218     lui $v0,0xff30
   0x8c430000,                      // 0xff20021c     lw $v1,0($v0)
   0x3c08ffff,                      // 0xff200220     lui $t0,0xffff
   0x2508fffb,                      // 0xff200224     addiu $t0,0xfffb
   0x00681824,                      // 0xff200228     and $v1,$v1,$t0
   0xac430000,                      // 0xff20022c     sw $v1,0($v0)
   0x00000000,                      // 0xff200230     nop
   0x0bc80080,                      // 0xff200234     j 0xff200200
   0x00000000,                      // 0xff200238     nop
   0x00000000,                      // 0xff20023c     nop
   0x00000000,                      // 0xff200240     nop
   0x00000000,                      // 0xff200244     nop
   0x00000000                       // 0xff200248     nop
};
unsigned long ulMipsDW2AppInstructionsClearMPBit = sizeof(pulMipsDW2AppCodeClearMPBit) / sizeof(unsigned long);

#endif   // DW2APPS_H_
