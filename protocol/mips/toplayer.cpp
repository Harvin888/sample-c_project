/****************************************************************************
       Module: toplayer.cpp
     Engineer: Nikolay Chokoev
  Description: Implementation of Opella-XD MIPS MDI top layer interface.
Date           Initials    Description
04-Jul-2008    NCH         Initial
23-Dec-2008    SPC         Error Handling
01-Jan-2009    SPC         Fixed Cache debugging of Big Endian targets.
06-Aug-2009    SPC         Made cache flush routine run from probe
****************************************************************************/

#define MDI_LIB               // We are an MDI Library not a debugger

#ifdef __LINUX
#include <stdlib.h>
#include <string.h>
#else
#include "windows.h"
// lint -save -e*
#include "winreg.h"
//lint -restore
#endif
#include "protocol/common/drvopxd.h"
#include "commdefs.h"
#include "gerror.h"
#include "midlayer.h"
#include "mdi.h"
#include "toplayer.h"
#include "debug.h"
#include "ash_iface.h"
#include "mdi/dw2/ReadHalf.h"
#include "mdi/dw2/WritHalf.h"
#include "mdi/dw2/ReadByte.h"
#include "mdi/dw2/WritByte.h"
#include "mdi/dw2/RdTLBEnt.h"
#include "mdi/dw2/WrTLBEnt.h"
#include "mdi/dw2/ReadACX.h"
#include "mdi/dw2/WriteACX.h"
#include "mdi/dw2/Readmem.h"
#include "mdi/dw2/Writewrd.h"
#include "mdi/dw2/CacheAct.h"
#include "mdi/dw2/ReadCp0.h"
#include "mdi/dw2/WriteCp0.h"
#include "mdi/dw2/RdCacEnt.h"
#include "mdi/dw2/ClrMPBit.h"
#include "mdi/dw2/CacheFlush.h"
#include "../../utility/debug/debug.h"

#define SIZE_OF_DEVICE_FIELDS 15
#define MAX_ALLOWED_RANGES 100
#define START_OF_VALID_RANGE 0x00000000
#define END_OF_VALID_RANGE   0xFFFFFFFF

struct MemRange 
   {
   unsigned long ulStartAddress;
   unsigned long ulEndAddress;
   }; 

TyMDILogSettings tyMDILogSettings;
TyUserSettings tyCurrentUserSettings;          // Setup Once by TL_DetermineSettings
TyApplicationSpecs tyAppSpecArray[LAST_APP];
TyMLHandle *ptyMLHandle;

static MemRange ValidRanges[MAX_ALLOWED_RANGES];
static unsigned long ulTotalRanges=0x0;
static int iLastErrRet;
extern int   bASHDMAClient; 
extern char pszGlobalDllPath[DLLPATH_LENGTH];

#define BETWEEN(test_val, low, high)   (((test_val) >= (low)) && ((test_val) <= (high)))
#define DMSEG_START 0xFF200000
#define DMSEG_END   0xFF2FFFFF


//todo: move to global utility file to use from who need it
/****************************************************************************
     Function: StrToUlong
     Engineer: Nikolay Chokoev
        Input: char *pszValue - string value
               int *pbValid - storage for flag if value is valid
       Output: unsigned long - value represented by the string
  Description: Converts string to long supports decimal and hex 
               when the string value begins with 0x.
Date           Initials    Description
09-Jul-2008    NCH         Initial
*****************************************************************************/
unsigned long StrToUlong(char *pszValue, int *pbValid)
{
   unsigned long ulValue;
   char *pszStop = NULL;
//todo   assert(pszValue != NULL);
   // dont allow octal, so skip leading '0's unless its 0x...
   while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] != 'X') && (pszValue[1] != '\0'))
      pszValue++;
   // strtoul supports decimal nnnn and hex 0xnnnn...
   ulValue = strtoul(pszValue, &pszStop, 0);
   // only valid if all characters were processed ok...
   if ((pszStop) && (pszStop[0] == '\0'))
      {
      if (pbValid)
         *pbValid = 1;
      return ulValue;
      }
   else
      {
      if (pbValid)
         *pbValid = 0;
      return 0;
      }
}

/****************************************************************************
     Function: IfTrueString
     Engineer: Nikolay Chokoev
        Input: char *pszString - string to convert
       Output: none
  Description: 
Date           Initials    Description
07-Dec-2007    NCH         Initial
*****************************************************************************/
int IfTrueString (char * pszString)
{
   unsigned int iTemp = 0;
   bool bResult;

   if (sscanf(pszString, "%d", &iTemp) == 1)
      {
      bResult = (iTemp != 0);
      }
   else
      {
      ASSERT_NOW(); // No Paramater found in String
      bResult = FALSE;
      }

   return bResult;
}
//todo up


/****************************************************************************
     Function: TL_LoadArrayFromConfigFile
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static TyError TL_LoadArrayFromConfigFile(char * szFilename)
{

   FILE * pfConfigFile;

#define MAX_LINE_LENGTH 200

   char szLineFromFile[MAX_LINE_LENGTH];
   unsigned long ulStartOfRange,ulEndOfRange;
   unsigned long ulScanResult;

   pfConfigFile = fopen(szFilename,"rt");
   if (!pfConfigFile)
      {
      // Fatal Error: Can't open config File
      return ERR_LOADARRAY_CFG_NOT_FOUND; 
      }

   while (!feof(pfConfigFile))
      {
      (void)fgets(szLineFromFile,MAX_LINE_LENGTH,pfConfigFile);

      ulScanResult=(unsigned long)sscanf(szLineFromFile,"%lx %lx",&ulStartOfRange,&ulEndOfRange);
      if (ulScanResult == 2 && (ulEndOfRange > ulStartOfRange))
         {
         // Good line
         ValidRanges[ulTotalRanges].ulStartAddress = ulStartOfRange;
         ValidRanges[ulTotalRanges].ulEndAddress   = ulEndOfRange;
         ulTotalRanges++;
         }
      else
         {
         // Line was a comment or invalid, do nothing
         }
      }

   fclose (pfConfigFile);


   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: TL_SortRangeArray
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static TyError TL_SortRangeArray(void)
{
   unsigned int i,j;
   MemRange TempNode;

   for (i=0;i<ulTotalRanges;i++)
      {
      for (j=i;j<ulTotalRanges;j++)
         {
         if (ValidRanges[j].ulStartAddress < ValidRanges[i].ulStartAddress)
            {
            // Swap the nodes
            TempNode = ValidRanges[i];
            ValidRanges[i]=ValidRanges[j];
            ValidRanges[j]=TempNode;
            }
         }
      }

   return ERR_NO_ERROR;

}

/****************************************************************************
     Function: TL_DeleteNode
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static TyError TL_DeleteNode(unsigned int uiNodeToDelete)
{
   unsigned int i;

   for (i=uiNodeToDelete;i<ulTotalRanges;i++)
      ValidRanges[i]=ValidRanges[i+1];

   ulTotalRanges--;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_JoinRanges
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static TyError TL_JoinRanges(void)
{
   unsigned int i;

   for (i=0;i<(ulTotalRanges-1);i++)
      {
      if (ValidRanges[i].ulEndAddress >= ValidRanges[i+1].ulStartAddress)
         {
         // This range is contiguous
         ValidRanges[i].ulEndAddress = ValidRanges[i+1].ulEndAddress;
         (void)TL_DeleteNode(i+1);
         // Now that we have deleted a node we need to step back one and test again
         i--;
         }

      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_LogRanges
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static TyError TL_LogRanges(void)
{
   unsigned int i;

   DLOG2((LOGFILE,"\nThe following Memory Ranges will be accessible by Pathfinder"));
   for (i=0;i<ulTotalRanges;i++)
      {
      DLOG2((LOGFILE,"\nRange %d:\t 0x%08X -> 0x%08X",i,ValidRanges[i].ulStartAddress,ValidRanges[i].ulEndAddress));
      }
   DLOG2((LOGFILE,"\nEnd of Ranges"));

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_InitialiseMemoryProtectionSystem
     Engineer: Nikolay Chokoev
        Input: 
       Output: Error code
  Description: 
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_InitialiseMemoryProtectionSystem(int bEnable)
{

#define RANGES_FILE "ranges"
#define RANGES_EXT  ".cfg"

   TyError ErrRet;
   char szInFileName[DLLPATH_LENGTH];
   char szRangesFileName[_MAX_PATH];

   strncpy(szInFileName,pszGlobalDllPath,DLLPATH_LENGTH);
#ifdef __LINUX
   sprintf(szRangesFileName,"%s/%s%s",szInFileName,RANGES_FILE,RANGES_EXT);
#else
   sprintf(szRangesFileName,"%s\\%s%s",szInFileName,RANGES_FILE,RANGES_EXT);
#endif


   if (!bEnable)
      {
      ulTotalRanges=1;

      ValidRanges[0].ulStartAddress=START_OF_VALID_RANGE;
      ValidRanges[0].ulEndAddress  =END_OF_VALID_RANGE;
      return ERR_NO_ERROR;
      }
   else
      ulTotalRanges=0;

   ErrRet=TL_LoadArrayFromConfigFile(szRangesFileName);

   if (ErrRet!=ERR_NO_ERROR)
      return ErrRet;

   ErrRet=TL_SortRangeArray();
   if (ErrRet!=ERR_NO_ERROR)
      return ErrRet;

   ErrRet=TL_JoinRanges();
   if (ErrRet!=ERR_NO_ERROR)
      return ErrRet;

   ErrRet=TL_LogRanges();
   if (ErrRet!=ERR_NO_ERROR)
      return ErrRet;


   return ERR_NO_ERROR;

}

#ifdef __LINUX
//(void)GetPrivateProfileString("General","DMATurnedOn","",szDMATurnedOn,MAX_STRING_SIZE,szConfigFile);
int GetPrivateProfileString(const char *pszSectName, 
                            const char *pszoptionName,
                            const char *pszDummy, 
                            char       *pszOption,
                            int        iMaxSize, 
                            const char *pszEnvVarName)
{
   FILE *pIniFile;
   char pszCurrentSection[32];
   char pszIniLine[_MAX_PATH];
   NOREF(pszDummy);
   NOREF(pszSectName); //not used for now

   if ((pszEnvVarName == NULL) || (pszoptionName == NULL) || (pszOption == NULL) )
      return 0;
   // check if environment variable exists and possibly use it as INI filename, try to open INI file
   pIniFile = fopen(pszEnvVarName, "rt");
   if (pIniFile == NULL)
      return 0;

   strcpy(pszCurrentSection, "");
   // read INI file line by line
   while (fgets(pszIniLine, _MAX_PATH, pIniFile) != NULL)
      {  // got line so try to parse it, first we need to remove any '\r' and '\n' characters (just strap them)
      char *pszLabelEnd = NULL;
      pszLabelEnd = strchr(pszIniLine, '\n');
      if (pszLabelEnd != NULL)
         *pszLabelEnd = 0;
      pszLabelEnd = strchr(pszIniLine, '\r');
      if (pszLabelEnd != NULL)
         *pszLabelEnd = 0;
      if ((pszIniLine[0] == '[') && (strchr(pszIniLine, ']') != NULL))  
         {  // got section label
         pszLabelEnd = strchr(pszIniLine, ']');
         if (pszLabelEnd != NULL)
            *pszLabelEnd = 0;                                           // cut end of label
         strncpy(pszCurrentSection, &(pszIniLine[1]), 32);                // copy label name into local 
         pszCurrentSection[31] = 0;
         }
      else if (strchr(pszIniLine, '=') != NULL)
         {  // got variable assignment
         pszLabelEnd = strchr(pszIniLine, '=');
         if (pszLabelEnd != NULL)
            {
            char *pszValue;
            pszValue = pszLabelEnd + 1;                                    // value after '=' character to the end of line
            *pszLabelEnd = 0;                                              // cut '=' from string
            //skip pszCurrentSection  as there is only one section
            if(strcmp(pszIniLine,pszoptionName) == 0)
               sprintf(pszOption,"%s",pszValue);
            }
         }
      }
   fclose(pIniFile);
   return 0;
}
#endif

#ifndef _LINUX
void _RegCloseKey(HKEY hAppKey)
{
    if (hAppKey != NULL)
       {
       (void)RegCloseKey(hAppKey);
       }
}

/****************************************************************************
     Function: TL_WriteRegistryLongKey
     Engineer: Nikolay Chokoev
        Input: iErrorNumber : Error number.
       Output: Error code
  Description: Writes a long value to the registry, Creates Key if necessary
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static int TL_WriteRegistryLongKey(const char * pszKeyName,
								   const char * pszValueName,
								   unsigned long ulValue)
{
   
   char szModule[0x200];
   char szTempBuffer[0x200];
   char szRegistryKey[0x200];
   char * pcCharPtr;
   HKEY     hAppKey = NULL; 
   long lRetCode;
//   unsigned long dwResult;
   int iErrRet;
   DWORD    dwDisposition  = 0x0;

   if ((pszKeyName == NULL) || (pszValueName== NULL))
      {
      iErrRet = 1; // TODO: Fix Error Code  
      _RegCloseKey(hAppKey);
      }

   // Get the module name and path
   (void)GetModuleFileName(NULL,szModule,(DWORD)sizeof(szModule));
  
   // Strip off the path
   pcCharPtr = strrchr(szModule,'\\');
   if(pcCharPtr != NULL)
	  strcpy(szTempBuffer, pcCharPtr);
   else
	  {
	  return 1; //todo error code
	  }

   // Remove the extension
   pcCharPtr = strrchr(szTempBuffer,'.');
   if (pcCharPtr)
      *pcCharPtr = '\0';
   
   // Format the key
   sprintf(szRegistryKey,"SOFTWARE\\Ashling MicroSystems Ltd.%s\\%s",szTempBuffer,pszKeyName);
   
   // Try and open the key (create if necessary)
   lRetCode = RegCreateKeyEx ( HKEY_CURRENT_USER, 
                               szRegistryKey, 
                               0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, 
                               NULL, &hAppKey, 
                               &dwDisposition); 
    
   if (lRetCode != ERROR_SUCCESS)
      {
      iErrRet = 2; // TODO: Fix Error Code  
      _RegCloseKey(hAppKey);
      }
   
   // Now set the Value
   lRetCode = RegSetValueEx(hAppKey,pszValueName,0,REG_DWORD,(unsigned char*)&ulValue,4);
   
   if (lRetCode != ERROR_SUCCESS)
      {
      iErrRet = 3; // TODO: Fix Error Code  
      _RegCloseKey(hAppKey);
      }
   
   iErrRet = ERR_NO_ERROR;
   _RegCloseKey(hAppKey);
   return iErrRet;
}
#endif

#ifndef _LINUX
/****************************************************************************
     Function: TL_ReadRegistryLongKey
     Engineer: Nikolay Chokoev
        Input: iErrorNumber : Error number.
       Output: Error code
  Description: Reads a long value from the registry
Date           Initials    Description
04-Jul-2008    NCH         Initial
20-Nov-2008    SPC         Fixed crash while register key closing
****************************************************************************/
static int TL_ReadRegistryLongKey(const char * pszKeyName,
								  const char * pszValueName,
								  unsigned long *pulValue,
								  int *pbSuccess)
{
   
   char szModule[0x200];
   char szTempBuffer[0x200];
   char szRegistryKey[0x200];
   char * pcCharPtr;
   HKEY     hAppKey = NULL; 
   long lRetCode;
//   unsigned long dwResult;
   unsigned long ulDataValueSize = 4;
   int iErrRet;
   unsigned long ulTypeInfo=0;

   // Assume it worked unless told otherwise
   *pbSuccess = TRUE;

   if ((pszKeyName == NULL) || 
	   (pszValueName == NULL) ||
	   (pulValue == NULL))
      {
      iErrRet = 1; // TODO: Fix Error Code  
      *pbSuccess=FALSE;
      return iErrRet;
      }

   // Get the module name and path
   (void)GetModuleFileName(NULL,szModule,(DWORD)sizeof(szModule));
   
   // Strip off the path
   pcCharPtr = strrchr(szModule,'\\');
   if(pcCharPtr != NULL)
	  strcpy(szTempBuffer, pcCharPtr);
   else
	  {
	  *pbSuccess = 1;
	  return 1; //todo error code
	  }
	  

   // Remove the extension
   pcCharPtr = strrchr(szTempBuffer,'.');
   if (pcCharPtr)
      *pcCharPtr = '\0';
   
   // Format the key
   sprintf(szRegistryKey,"SOFTWARE\\Ashling MicroSystems Ltd.%s\\%s",szTempBuffer,pszKeyName);
   
   // Try and open the key 
   lRetCode = RegOpenKeyEx ( HKEY_CURRENT_USER, 
                             szRegistryKey, 
                             0, 
                             KEY_READ, 
                             &hAppKey
                             );
    
   if (lRetCode != ERROR_SUCCESS)
      {
      iErrRet = 2; // TODO: Fix Error Code  
      *pbSuccess=FALSE;
      goto CleanUpOnErrorReadRegKey;
      }
   
   // Now Read The Value
   lRetCode = RegQueryValueEx(hAppKey,pszValueName,NULL,&ulTypeInfo,(unsigned char*)pulValue,&ulDataValueSize);
   
   if (lRetCode != ERROR_SUCCESS)
      {
      iErrRet = 3; // TODO: Fix Error Code  
      *pbSuccess=FALSE;
      goto CleanUpOnErrorReadRegKey;
      }
   
   iErrRet = 0;//todo error code

   //Finally Close the key
CleanUpOnErrorReadRegKey:
   //Finally Close the key
   _RegCloseKey(hAppKey);
   return iErrRet;
}
#endif

/****************************************************************************
     Function: TL_MDILogOpen
     Engineer: Nikolay Chokoev
        Input: bAppendToLogFile
       Output: None
  Description: Initialise MDI Logging and open log file
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static int TL_MDILogOpen(int bAppendToLogFile)
{
#if 0000 //todo
   char szPfwModuleName [MAX_PATH];
   char szMDILogFileName[MAX_PATH];
   char szDrive         [MAX_PATH];
   char szDir           [MAX_PATH];

   if (!tyMDILogSettings.bStaticLogMDICalls)
      return ERR_NO_ERROR; // For now just ignore

   // First Get the name of the log file
   (void)GetModuleFileName(szPfwModuleName, sizeof(szPfwModuleName));
   _splitpath(szPfwModuleName, szDrive, szDir, NULL, NULL);
   _makepath(szMDILogFileName, szDrive, szDir,"AshMDILog", "TXT");

   // Next Open the Log File
   if (bAppendToLogFile)
      tyMDILogSettings.pGlobalMDILogFile = fopen(szMDILogFileName,"a");
   else
      tyMDILogSettings.pGlobalMDILogFile = fopen(szMDILogFileName,"w+");

   if (tyMDILogSettings.pGlobalMDILogFile == NULL)
      {
      tyMDILogSettings.bStaticLogMDICalls = FALSE;
      return ERR_NO_ERROR;
      }
#endif
   bAppendToLogFile = bAppendToLogFile;
   tyMDILogSettings.pGlobalMDILogFile = NULL;
   tyMDILogSettings.bStaticLogMDICalls = FALSE;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_GetSetRegistryLongKey
     Engineer: Nikolay Chokoev
        Input: iErrorNumber : Error number.
       Output: None
  Description: Reads a long registry key. If the key does not exist it sets
               the key to a defined default state.
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
static int TL_GetSetRegistryLongKey(const char * pszKeyName,
				    const char * pszValueName,
				    unsigned long * pulValueReadBack,
				    unsigned long ulDefaultValue )
{
#ifdef _LINUX
   NOREF(pszKeyName);
   NOREF(pszValueName);
   *pulValueReadBack = ulDefaultValue;
#else

   int bSuccess = FALSE;
   int iErrRet;

   iErrRet=TL_ReadRegistryLongKey(pszKeyName,pszValueName,pulValueReadBack,&bSuccess);
   if ((iErrRet != 0) || !bSuccess)//todo error code
      {
      // Could not find key attempt to create the Key
      iErrRet=TL_WriteRegistryLongKey(pszKeyName,pszValueName,ulDefaultValue);
      if (iErrRet != 0)//todo error code
         {
         ASSERT_NOW();
         *pulValueReadBack = 0;
         return ERR_NO_ERROR;
         }

      // Next Attempt to read it back
      iErrRet=TL_ReadRegistryLongKey(pszKeyName,pszValueName,pulValueReadBack,&bSuccess);
      if ((iErrRet != 0) || !bSuccess) //todo error code
         {
         ASSERT_NOW();
         *pulValueReadBack = 0;
         return 0;//todo error code
         }
      }
#endif

   return 0;//todo error code
}

/****************************************************************************
     Function: TL_DetermineSettings
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: This function determines all User and Device Settings
Date           Initials    Description
04-Jul-2008    NCH         Initial
20-Nov-2008    SPC         Device Enable password support added
06-Aug-2009    SPC         Made cache flush routine run from probe.
19-Oct-2011    ANGS        Modified for Zephyr trace support
****************************************************************************/
int TL_DetermineSettings()
{
    if (ptyMLHandle == NULL)
    {
        ASSERT_NOW();
        return iLastErrRet;
    }

   NOREF(ptyMLHandle); //todo
   // General Variables
   char           szConfigFile   [DLLPATH_LENGTH];
//   TyTraceSupport tyTraceType;
   char           szTemp         [MAX_STRING_SIZE];
   unsigned long  ulLoop;
   int bValid;

   // Common Settings
   char szDMATurnedOn                  [MAX_STRING_SIZE];
   char szRstTgtBforeConn              [MAX_STRING_SIZE];
   char szDisableIntSS                 [MAX_STRING_SIZE];
   char szSingleStepUsingSWBP          [MAX_STRING_SIZE];
   char szUserHaltCountersIDM          [MAX_STRING_SIZE];
   char szMchpRecoveryByChipErase      [MAX_STRING_SIZE];
   char szCacheFlushVerify             [MAX_STRING_SIZE];
   char szCacheFlushFilename           [MAX_STRING_SIZE];
   char szCacheFlushLocation           [MAX_STRING_SIZE];
   char szDataScratchPadRamStart       [MAX_STRING_SIZE];
   char szDataScratchPadRamEnd         [MAX_STRING_SIZE];
   char szInstScratchPadRamStart       [MAX_STRING_SIZE];
   char szInstScratchPadRamEnd         [MAX_STRING_SIZE];
   char szJtagFreqInKhz                [MAX_STRING_SIZE];
   char szEJTAGStyle                   [MAX_STRING_SIZE];
   char szusTargetSupply               [MAX_STRING_SIZE];
   char szbUseSlowJTAG                 [MAX_STRING_SIZE];
   char szbEJTAGDMAOnly                [MAX_STRING_SIZE];
   char szbNoMMUTranslation            [MAX_STRING_SIZE];
//   char szbFreeRunningJTAGClk          [MAX_STRING_SIZE];
   char szbUseEJTAGBootDuringReset     [MAX_STRING_SIZE];
   char szbEJTAGBootSupported          [MAX_STRING_SIZE];
   char szulNumInstHWBps               [MAX_STRING_SIZE];
   char szulNumDataHWBps               [MAX_STRING_SIZE];
   char szbHWBpASIDSupported           [MAX_STRING_SIZE];
   char szbUseInstrWatchForBP          [MAX_STRING_SIZE];
   char szbOddAddressFor16BitModeBps   [MAX_STRING_SIZE];
   char szbSSIn16BitModeSupported      [MAX_STRING_SIZE];
   char szbEnableMIPS16ModeForTrace    [MAX_STRING_SIZE];
   char szbZephyrTraceSupport          [MAX_STRING_SIZE];
   char szulNumberOfNOPSInERETBDS      [MAX_STRING_SIZE];
   char szulNumberOfNOPSInJUMPBDS      [MAX_STRING_SIZE];
   char szucMultiCoreCoreNum           [MAX_STRING_SIZE];
   char szucMultiCoreDMACoreNum        [MAX_STRING_SIZE];
   char szbPerformHardReset            [MAX_STRING_SIZE];
   char szbPerformSoftProcessorReset   [MAX_STRING_SIZE];
   char szbPerformSoftPeripheralReset  [MAX_STRING_SIZE];
   char szbPerformHardBetweenSoftReset [MAX_STRING_SIZE];
   char szulInvalidateInst             [MAX_STRING_SIZE];
   char szulInvalidateWBInst           [MAX_STRING_SIZE];
   char szbOnlyFlushDataCache          [MAX_STRING_SIZE];
   char szbDoubleReadOnDMA             [MAX_STRING_SIZE];
   char szbAssumeWordAccess            [MAX_STRING_SIZE];
   char szbWriteEnableRequired         [MAX_STRING_SIZE];
   char szulEnableWriteAddress         [MAX_STRING_SIZE];
   char szulEnableWriteValue           [MAX_STRING_SIZE];
   char szulEnableWriteMask            [MAX_STRING_SIZE];
   char szucMultiCoreNumCores          [MAX_STRING_SIZE];
   char szucIDCODEInstCode             [MAX_STRING_SIZE];
   char szucLengthOfAddressRegister    [MAX_STRING_SIZE];
   char szbSelectAddrRegAfterEveryAccess[MAX_STRING_SIZE];
   char szbSyncInstNotSupported        [MAX_STRING_SIZE];
   char szbAssertHaltTwice             [MAX_STRING_SIZE];
   char szbUseFastDMAFunc              [MAX_STRING_SIZE];
   char szulCoreSelectCode             [MAX_STRING_SIZE];
   char szulCoreSelectIRLength         [MAX_STRING_SIZE];
   char szbCoreSelectBeforeReset       [MAX_STRING_SIZE];
   char szulIRLength                   [MAX_DEVICE_IN_CHAIN][MAX_STRING_SIZE];
   char szulBypassCode                 [MAX_DEVICE_IN_CHAIN][MAX_STRING_SIZE];
   char szulDelayAfterResetMS          [MAX_STRING_SIZE];
   char szulResetDelayMS               [MAX_STRING_SIZE];
   char szucMemWriteBeforeReset        [MAX_STRING_SIZE];
   char szulMemResetWriteAddr          [MAX_STRING_SIZE];
   char szulMemResetWriteValue         [MAX_STRING_SIZE];
   char szucDMAWriteAfterReset         [MAX_STRING_SIZE];
   char szulDMAResetWriteAddr          [MAX_STRING_SIZE];
   char szulDMAResetWriteValue         [MAX_STRING_SIZE];
   char szucMemReadAfterReset          [MAX_STRING_SIZE];
   char szucEJTAGSeq                   [MAX_STRING_SIZE];
//   char szFpgaware                     [MAX_STRING_SIZE];
   char szbUseRestrictedDmaAddressRange	[MAX_STRING_SIZE];
   char szulStartOfValidDmaAddressRange [MAX_STRING_SIZE];
   char szulEndOfValidDmaAddressRange   [MAX_STRING_SIZE];
   char szbMicroChipM4KFamily           [MAX_STRING_SIZE];   
   char szulNumberOfShadowSetsPresent   [MAX_STRING_SIZE];
   char szulLimitDmaRdBpS              [MAX_STRING_SIZE];
   char szulLimitDmaWrBpS              [MAX_STRING_SIZE];
   char szulLimitCacheRdBpS            [MAX_STRING_SIZE];
   char szulLimitCacheWrBpS            [MAX_STRING_SIZE];
   char szTraceType              [MAX_STRING_SIZE];
   char szEndianModeModifiable   [MAX_STRING_SIZE];
   char szOpellaXDNumber         [MAX_STRING_SIZE];
   char szProcName				 [MAX_STRING_SIZE];

   unsigned long ulReadUsingTargetMemoryEnabled=0;
   unsigned long ulWriteUsingTargetMemoryEnabled=0;

   // TODO: ADD Sensible Default Values Here.
   
//   GetConfigFileName(szConfigFile);
#ifdef __LINUX
   sprintf(szConfigFile,"%s/%s",pszGlobalDllPath,MDI_CONFIG_FILE_NAME);
#else
   sprintf(szConfigFile,"%s\\%s",pszGlobalDllPath,MDI_CONFIG_FILE_NAME);
#endif

   (void)GetPrivateProfileString("General","DMATurnedOn"              ,"",szDMATurnedOn              ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","RstTgtBforeConn"         ,"1",szRstTgtBforeConn          ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","DisableIntSS"             ,"",szDisableIntSS             ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","SingleStepUsingSWBP"      ,"",szSingleStepUsingSWBP      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","UserHaltCountersIDM"      ,"",szUserHaltCountersIDM      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","MchpRecoveryByChipErase"  ,"",szMchpRecoveryByChipErase  ,MAX_STRING_SIZE,szConfigFile); 
   (void)GetPrivateProfileString("General","CacheFlushVerify"         ,"",szCacheFlushVerify         ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","CacheFlushFilename"       ,"",szCacheFlushFilename       ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","CacheFlushLocation"       ,"",szCacheFlushLocation       ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","DSPRStart"                ,"",szDataScratchPadRamStart   ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","DSPREnd"                  ,"",szDataScratchPadRamEnd     ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ISPRStart"                ,"",szInstScratchPadRamStart   ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ISPREnd"                  ,"",szInstScratchPadRamEnd     ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","JtagFreqInKHz"            ,"",szJtagFreqInKhz            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","OpellaXDInstance"         ,"",szOpellaXDNumber           ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ProcessorName"            ,"",szProcName                 ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","EJTAGStyle"                    ,"",szEJTAGStyle                 ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","usTargetSupply"                ,"",szusTargetSupply             ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bUseSlowJTAG"                  ,"",szbUseSlowJTAG               ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bEJTAGDMAOnly"                ,"",szbEJTAGDMAOnly            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bNoMMUTranslation"                ,"",szbNoMMUTranslation          ,MAX_STRING_SIZE,szConfigFile);
//   (void)GetPrivateProfileString("General","bFreeRunningJTAGClk"           ,"",szbFreeRunningJTAGClk        ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bUseEJTAGBootDuringReset"      ,"",szbUseEJTAGBootDuringReset   ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bEJTAGBootSupported"           ,"",szbEJTAGBootSupported        ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulNumInstHWBps"                ,"",szulNumInstHWBps             ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulNumDataHWBps"                ,"",szulNumDataHWBps             ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bHWBpASIDSupported"            ,"",szbHWBpASIDSupported         ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bUseInstrWatchForBP"           ,"",szbUseInstrWatchForBP        ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bOddAddressFor16BitModeBps"    ,"",szbOddAddressFor16BitModeBps ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bSSIn16BitModeSupported"       ,"",szbSSIn16BitModeSupported    ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bEnableMIPS16ModeForTrace"     ,"",szbEnableMIPS16ModeForTrace  ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bZephyrTraceSupport"           ,"",szbZephyrTraceSupport        ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulNumberOfNOPSInERETBDS"       ,"",szulNumberOfNOPSInERETBDS    ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulNumberOfNOPSInJUMPBDS"       ,"",szulNumberOfNOPSInJUMPBDS    ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucMultiCoreCoreNum"            ,"",szucMultiCoreCoreNum         ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucMultiCoreDMACoreNum"         ,"",szucMultiCoreDMACoreNum      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bPerformHardReset"             ,"",szbPerformHardReset          ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bPerformSoftProcessorReset"    ,"",szbPerformSoftProcessorReset ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bPerformSoftPeripheralReset"   ,"",szbPerformSoftPeripheralReset,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bPerformHardBetweenSoftReset"  ,"",szbPerformHardBetweenSoftReset,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulInvalidateInst"              ,"",szulInvalidateInst            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulInvalidateWBInst"            ,"",szulInvalidateWBInst          ,MAX_STRING_SIZE,szConfigFile);

   (void)GetPrivateProfileString("General","bOnlyFlushDataCache"           ,"",szbOnlyFlushDataCache        ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bDoubleReadOnDMA"              ,"",szbDoubleReadOnDMA           ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bAssumeWordAccess"             ,"",szbAssumeWordAccess           ,MAX_STRING_SIZE,szConfigFile);

   (void)GetPrivateProfileString("General","bWriteEnableRequired"          ,"",szbWriteEnableRequired       ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulEnableWriteAddress"          ,"",szulEnableWriteAddress       ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulEnableWriteValue"            ,"",szulEnableWriteValue         ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulEnableWriteMask"             ,"",szulEnableWriteMask          ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucMultiCoreNumCores"           ,"",szucMultiCoreNumCores        ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucIDCODEInstCode"              ,"",szucIDCODEInstCode           ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucLengthOfAddressRegister"     ,"",szucLengthOfAddressRegister  ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bSelectAddrRegAfterEveryAccess","",szbSelectAddrRegAfterEveryAccess,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bSyncInstNotSupported"            ,"",szbSyncInstNotSupported      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bAssertHaltTwice"              ,"",szbAssertHaltTwice           ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bUseFastDMAFunc"               ,"",szbUseFastDMAFunc            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulCoreSelectCode"              ,"",szulCoreSelectCode           ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulCoreSelectIRLength"          ,"",szulCoreSelectIRLength       ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bCoreSelectBeforeReset"        ,"",szbCoreSelectBeforeReset     ,MAX_STRING_SIZE,szConfigFile);   
   (void)GetPrivateProfileString("General","bUseRestrictedDmaAddressRange" ,"",szbUseRestrictedDmaAddressRange  ,MAX_STRING_SIZE,szConfigFile);   
   (void)GetPrivateProfileString("General","ulStartOfValidDmaAddressRange" ,"",szulStartOfValidDmaAddressRange  ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulEndOfValidDmaAddressRange"   ,"",szulEndOfValidDmaAddressRange    ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","bMicroChipM4KFamily"           ,"",szbMicroChipM4KFamily            ,MAX_STRING_SIZE,szConfigFile);   
   (void)GetPrivateProfileString("General","ulNumberOfShadowSetsPresent"   ,"",szulNumberOfShadowSetsPresent    ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulLimitDmaRdBpS"               ,"",szulLimitDmaRdBpS            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulLimitDmaWrBpS"               ,"",szulLimitDmaWrBpS            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulLimitCacheRdBpS"             ,"",szulLimitCacheRdBpS          ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulLimitCacheWrBpS"             ,"",szulLimitCacheWrBpS          ,MAX_STRING_SIZE,szConfigFile);

   for(ulLoop = 0; ulLoop < MAX_DEVICE_IN_CHAIN; ulLoop++)
      {
       sprintf(szTemp,"ulIRLength[%ld]",ulLoop);
       (void)GetPrivateProfileString("General",szTemp                      ,"",szulIRLength[ulLoop]            ,MAX_STRING_SIZE,szConfigFile);
       sprintf(szTemp,"ulBypassCode[%ld]",ulLoop);
       (void)GetPrivateProfileString("General",szTemp                      ,"",szulBypassCode[ulLoop]          ,MAX_STRING_SIZE,szConfigFile);
      }

   (void)GetPrivateProfileString("General","ulDelayAfterResetMS"           ,"",szulDelayAfterResetMS      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulResetDelayMS"                ,"",szulResetDelayMS           ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucMemWriteBeforeReset"         ,"",szucMemWriteBeforeReset    ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulMemResetWriteAddr"           ,"",szulMemResetWriteAddr      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulMemResetWriteValue"          ,"",szulMemResetWriteValue     ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucDMAWriteAfterReset"          ,"",szucDMAWriteAfterReset     ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulDMAResetWriteAddr"           ,"",szulDMAResetWriteAddr      ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ulDMAResetWriteValue"          ,"",szulDMAResetWriteValue     ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucMemReadAfterReset"           ,"",szucMemReadAfterReset   ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","ucEJTAGSeq"                    ,"",szucEJTAGSeq               ,MAX_STRING_SIZE,szConfigFile);
//   (void)GetPrivateProfileString("General","szFpgaware"                    ,"",szFpgaware                 ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","TraceType"             ,"",szTraceType            ,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","EndianModeModifiable"  ,"",szEndianModeModifiable ,MAX_STRING_SIZE,szConfigFile);

   //Directly getting the szDeviceEnablePasswdFile from mdiconfig file
   (void)GetPrivateProfileString("General","szDeviceEnablePasswdFile", "",
								 tyCurrentUserSettings.szDeviceEnablePasswdFile,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","szPasswordSequence", "",
								 tyCurrentUserSettings.szPasswordSequence,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","szPasswordFile1", "",
								 tyCurrentUserSettings.szPasswordFile1,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","szPasswordFile2", "",
								 tyCurrentUserSettings.szPasswordFile2,MAX_STRING_SIZE,szConfigFile);
   (void)GetPrivateProfileString("General","szPasswordFile3", "",
                                 tyCurrentUserSettings.szPasswordFile3,MAX_STRING_SIZE,szConfigFile);

   tyCurrentUserSettings.bDMATurnedOn     =  (unsigned char)IfTrueString(szDMATurnedOn);
   tyCurrentUserSettings.bRstTgtBforeConn =  (unsigned char)IfTrueString(szRstTgtBforeConn);
   ptyMLHandle->bDisableIntSS             =  (unsigned char)IfTrueString(szDisableIntSS);
   ptyMLHandle->bSingleStepUsingSWBP      =  (unsigned char)IfTrueString(szSingleStepUsingSWBP);
   ptyMLHandle->bUserHaltCountersIDM      =  (unsigned char)IfTrueString(szUserHaltCountersIDM);
   ptyMLHandle->ulDataScratchPadRamStart  =  StrToUlong(szDataScratchPadRamStart,&bValid);
   ptyMLHandle->ulDataScratchPadRamEnd    =  StrToUlong(szDataScratchPadRamEnd,&bValid);
   ptyMLHandle->ulInstScratchPadRamStart  =  StrToUlong(szInstScratchPadRamStart,&bValid);
   ptyMLHandle->ulInstScratchPadRamEnd    =  StrToUlong(szInstScratchPadRamEnd,&bValid);
   tyCurrentUserSettings.bEndianModeModifiable  =  (unsigned char)IfTrueString(szEndianModeModifiable);
   ptyMLHandle->bUseEjtagBoot             =  (unsigned char)IfTrueString(szbUseEJTAGBootDuringReset);
   ptyMLHandle->bUseInstrWatchForBP       =  (unsigned char)IfTrueString(szbUseInstrWatchForBP);
   ptyMLHandle->ucNoOfNOPSInERETBDS       =  (unsigned char)StrToUlong  (szulNumberOfNOPSInERETBDS,&bValid);
   ptyMLHandle->bPerformHardReset         =  (unsigned char)IfTrueString(szbPerformHardReset);
   ptyMLHandle->bPerformSoftProcReset     =  (unsigned char)IfTrueString(szbPerformSoftProcessorReset);
   ptyMLHandle->bPerformSoftPerReset      =  (unsigned char)IfTrueString(szbPerformSoftPeripheralReset);
   ptyMLHandle->bHardBetweenSoftReset     =  (unsigned char)IfTrueString(szbPerformHardBetweenSoftReset);
   ptyMLHandle->ulInvalidateInst          =  StrToUlong  (szulInvalidateInst,&bValid);
   ptyMLHandle->ulInvalidateWBInst        =  StrToUlong  (szulInvalidateWBInst,&bValid);
   ptyMLHandle->bAssumeWordAccess         =  (unsigned char)IfTrueString(szbAssumeWordAccess);
   ptyMLHandle->bEnableWriteRequired      =  (unsigned char)IfTrueString(szbWriteEnableRequired);
   ptyMLHandle->ulEnableWriteAddress      =  StrToUlong  (szulEnableWriteAddress,&bValid);
   ptyMLHandle->ulEnableWriteValue        =  StrToUlong  (szulEnableWriteValue,&bValid);
   ptyMLHandle->ulEnableWriteMask         =  StrToUlong  (szulEnableWriteMask,&bValid);
   ptyMLHandle->ulMipsCoreEjtagIdcodeInst =  (unsigned char)StrToUlong  (szucIDCODEInstCode,&bValid);
   ptyMLHandle->ulMipsCoreEjtagDataLength =  (unsigned char)StrToUlong  (szucLengthOfAddressRegister,&bValid);
   ptyMLHandle->bAssertHaltTwice          =  (unsigned char)StrToUlong  (szbAssertHaltTwice,&bValid);
   ptyMLHandle->ulStartOfDmaAddressRange  =  StrToUlong  (szulStartOfValidDmaAddressRange,&bValid);
   ptyMLHandle->ulEndOfDmaAddressRange    =  StrToUlong  (szulEndOfValidDmaAddressRange,&bValid);
   ptyMLHandle->ulNumberOfShadowSetsPresent =  StrToUlong  (szulNumberOfShadowSetsPresent,&bValid);
   //Reset config
   ptyMLHandle->ulDelayAfterResetMS       =  StrToUlong  (szulDelayAfterResetMS,&bValid);
   ptyMLHandle->ulResetDelayMS            =  StrToUlong  (szulResetDelayMS,&bValid);
   ptyMLHandle->ucMemWriteBeforeReset     =  (unsigned char)StrToUlong  (szucMemWriteBeforeReset,&bValid);
   ptyMLHandle->ulMemResetWriteAddr       =  StrToUlong  (szulMemResetWriteAddr,&bValid);
   ptyMLHandle->ulMemResetWriteValue      =  StrToUlong  (szulMemResetWriteValue,&bValid);
   ptyMLHandle->ucDMAWriteAfterReset      =  (unsigned char)StrToUlong  (szucDMAWriteAfterReset,&bValid);
   ptyMLHandle->ulDMAResetWriteAddr       =  StrToUlong  (szulDMAResetWriteAddr,&bValid);
   ptyMLHandle->ulDMAResetWriteValue      =  StrToUlong  (szulDMAResetWriteValue,&bValid);
   ptyMLHandle->ucMemReadAfterReset       =  (unsigned char)StrToUlong  (szucMemReadAfterReset,&bValid);
   ptyMLHandle->usTargetSupply            =  (unsigned short)StrToUlong  (szusTargetSupply,&bValid);
   ptyMLHandle->dJtagFreq                 =  (double)StrToUlong(szJtagFreqInKhz,&bValid) / 1000.0;
   ptyMLHandle->ucASHDMADebuggerClient    =  bASHDMAClient ||  (unsigned char)IfTrueString(szbEJTAGDMAOnly); // Determined from MDIConnect
   ptyMLHandle->bUseMMUMapping            = (unsigned char)IfTrueString(szbNoMMUTranslation);
   strncpy(ptyMLHandle->pszSerialNumber, szOpellaXDNumber, 15);
   ptyMLHandle->pszSerialNumber[15] = 0;
   strcpy(ptyMLHandle->pszProcName, szProcName);
   
   //todo: Multicore
   tyCurrentUserSettings.ucMultiCoreCoreNum     =  (unsigned char)StrToUlong  (szucMultiCoreCoreNum,&bValid);
   tyCurrentUserSettings.ucMultiCoreDMACoreNum  =  (unsigned char)StrToUlong  (szucMultiCoreDMACoreNum,&bValid);
   tyCurrentUserSettings.ucMultiCoreNumCores    =  (unsigned char)StrToUlong  (szucMultiCoreNumCores,&bValid);
   tyCurrentUserSettings.ucEJTAGSecSequence     =  (unsigned char)StrToUlong  (szucEJTAGSeq,&bValid);
   tyCurrentUserSettings.ulCoreSelectCode       =  StrToUlong  (szulCoreSelectCode,&bValid);
   tyCurrentUserSettings.ulCoreSelectIRLength   =  StrToUlong  (szulCoreSelectIRLength,&bValid);
   tyCurrentUserSettings.bCoreSelectBeforeReset =  (unsigned char)IfTrueString (szbCoreSelectBeforeReset          );
   
   for(ulLoop = 0; ulLoop < MAX_DEVICE_IN_CHAIN; ulLoop++)
      {
      tyCurrentUserSettings.tyMultiCoreDeviceInfo[ulLoop].ulIRLength   =  StrToUlong  (szulIRLength[ulLoop],&bValid);
      tyCurrentUserSettings.tyMultiCoreDeviceInfo[ulLoop].ulBypassCode =  StrToUlong  (szulBypassCode[ulLoop],&bValid);
      }
   //todo:   
   tyCurrentUserSettings.bMchpRecoveryByChipErase  =  (unsigned char)IfTrueString(szMchpRecoveryByChipErase);
   tyCurrentUserSettings.bEnableMIPS16ModeForTrace =  (unsigned char)IfTrueString(szbEnableMIPS16ModeForTrace);
   tyCurrentUserSettings.bZephyrTraceSupport       =  (unsigned char)IfTrueString(szbZephyrTraceSupport);
   //Handle init values
   tyCurrentUserSettings.bEJTAGBootSupported       =  (unsigned char)IfTrueString(szbEJTAGBootSupported);
   tyCurrentUserSettings.bUseSlowJTAG              =  (unsigned char)IfTrueString(szbUseSlowJTAG);
   tyCurrentUserSettings.EJTAGStyle                =  StrToUlong  (szEJTAGStyle,&bValid);
   tyCurrentUserSettings.bOnlyFlushDataCache       =  (unsigned char)IfTrueString(szbOnlyFlushDataCache         );
   tyCurrentUserSettings.bSSIn16BitModeSupported      =  (unsigned char)IfTrueString(szbSSIn16BitModeSupported);
   //DW
   tyCurrentUserSettings.bUseRestrictedDmaAddressRange   =  (unsigned char)IfTrueString      (szbUseRestrictedDmaAddressRange          );
   tyCurrentUserSettings.bSyncInstNotSupported           =  (unsigned char)StrToUlong  (szbSyncInstNotSupported,&bValid);
   tyCurrentUserSettings.bSelectAddrRegAfterEveryAccess  =  (unsigned char)StrToUlong  (szbSelectAddrRegAfterEveryAccess,&bValid);
   tyCurrentUserSettings.bDoubleReadOnDMA          =  (unsigned char)IfTrueString(szbDoubleReadOnDMA            );
   tyCurrentUserSettings.ucNumberOfNOPSInJUMPBDS   =  (unsigned char)StrToUlong  (szulNumberOfNOPSInJUMPBDS,&bValid);
   tyCurrentUserSettings.ucLengthOfAddressRegister = (unsigned char)StrToUlong  (szucLengthOfAddressRegister,&bValid);
   tyCurrentUserSettings.bUseFastDMAFunc           = (unsigned char)StrToUlong  (szbUseFastDMAFunc,&bValid);
   tyCurrentUserSettings.ulLimitDmaRdBpS           = StrToUlong (szulLimitDmaRdBpS,&bValid);
   tyCurrentUserSettings.ulLimitDmaWrBpS           = StrToUlong (szulLimitDmaWrBpS,&bValid);
   tyCurrentUserSettings.ulLimitCacheRdBpS         = StrToUlong (szulLimitCacheRdBpS,&bValid);
   tyCurrentUserSettings.ulLimitCacheWrBpS         = StrToUlong (szulLimitCacheWrBpS,&bValid);
   //Not Used: Check!
   tyCurrentUserSettings.ulNumInstHWBps            = StrToUlong  (szulNumInstHWBps,&bValid);
   tyCurrentUserSettings.ulNumDataHWBps            = StrToUlong  (szulNumDataHWBps,&bValid);
   tyCurrentUserSettings.bHWBpASIDSupported        = (unsigned char)IfTrueString(szbHWBpASIDSupported);
   tyCurrentUserSettings.bOddAddressFor16BitModeBps = (unsigned char)IfTrueString(szbOddAddressFor16BitModeBps);
   tyCurrentUserSettings.bMicroChipM4KFamily       = (unsigned char)IfTrueString      (szbMicroChipM4KFamily          );

   //strncpy(tyCurrentUserSettings.szOpellaXDNumber, szOpellaXDNumber, 15);
   //tyCurrentUserSettings.szOpellaXDNumber[15] = 0;
//   tyCurrentUserSettings.bEJTAGDMAOnly               =  (unsigned char)IfTrueString(szbEJTAGDMAOnly);
//   tyCurrentUserSettings.bFreeRunningJTAGClk         =  (unsigned char)IfTrueString(szbFreeRunningJTAGClk);

   // The following allows us to temporarily disable this feature using the registry if 
   // we need to debug something.
   (void)TL_GetSetRegistryLongKey("General","UseTgtMemForWrite",&ulWriteUsingTargetMemoryEnabled,1);
   (void)TL_GetSetRegistryLongKey("General","UseTgtMemForRead" ,&ulReadUsingTargetMemoryEnabled,1);
//   ptyMLHandle->bReadUsingTargetMemEnabled   = (ulReadUsingTargetMemoryEnabled  != 0); 
//   ptyMLHandle->bWriteUsingTargetMemEnabled  = (ulWriteUsingTargetMemoryEnabled != 0);

   tyAppSpecArray[FSHCACHEL].bRelocate       = TRUE;
   tyAppSpecArray[FSHCACHEL].atApp           = FSHCACHEL;

   tyAppSpecArray[FSHCACHEB].bRelocate       = TRUE;
   tyAppSpecArray[FSHCACHEB].atApp           = FSHCACHEB;

   // Now the Special SRECORDS
   tyAppSpecArray[SCX00SCR].bRelocate         = FALSE;
   tyAppSpecArray[SCX00SCR].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[SCX00SCR].atApp             = SCX00SCR;
   strcpy(tyAppSpecArray[SCX00SCR].szSrecFilename,"scx00scr.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[SCX01RCR].bRelocate         = FALSE;
   tyAppSpecArray[SCX01RCR].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[SCX01RCR].atApp             = SCX01RCR;
   strcpy(tyAppSpecArray[SCX01RCR].szSrecFilename,"scx01rcr.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[SCX02RCR].bRelocate         = FALSE;
   tyAppSpecArray[SCX02RCR].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[SCX02RCR].atApp             = SCX02RCR;
   strcpy(tyAppSpecArray[SCX02RCR].szSrecFilename,"scx02rcr.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[RCX00RRR].bRelocate         = FALSE;
   tyAppSpecArray[RCX00RRR].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[RCX00RRR].atApp             = RCX00RRR;
   strcpy(tyAppSpecArray[RCX00RRR].szSrecFilename,"rcx00rrr.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[RCX01RRR].bRelocate         = FALSE;
   tyAppSpecArray[RCX01RRR].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[RCX01RRR].atApp             = RCX01RRR;
   strcpy(tyAppSpecArray[RCX01RRR].szSrecFilename,"rcx01rrr.sre");

     // Now the Special SRECORDS
   tyAppSpecArray[RCX02RRR].bRelocate         = FALSE;
   tyAppSpecArray[RCX02RRR].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[RCX02RRR].atApp             = RCX02RRR;
   strcpy(tyAppSpecArray[RCX02RRR].szSrecFilename,"rcx02rrr.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[SAV00SRS].bRelocate         = FALSE;
   tyAppSpecArray[SAV00SRS].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[SAV00SRS].atApp             = SAV00SRS;
   strcpy(tyAppSpecArray[SAV00SRS].szSrecFilename,"sav00srs.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[RES00SRS].bRelocate         = FALSE;
   tyAppSpecArray[RES00SRS].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[RES00SRS].atApp             = RES00SRS;
   strcpy(tyAppSpecArray[RES00SRS].szSrecFilename,"res00srs.sre");

   // Now the Special SRECORDS
   tyAppSpecArray[RUNTGCMD].bRelocate         = FALSE;
   tyAppSpecArray[RUNTGCMD].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[RUNTGCMD].atApp             = RUNTGCMD;
   strcpy(tyAppSpecArray[RUNTGCMD].szSrecFilename,"runtgcmd.sre");

   // Now loading BRCM Srecords
   tyAppSpecArray[SCXBCMAP].bRelocate         = FALSE;
   tyAppSpecArray[SCXBCMAP].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[SCXBCMAP].atApp             = SCXBCMAP;
   strcpy(tyAppSpecArray[SCXBCMAP].szSrecFilename,"SCXBCMAP.sre");

   tyAppSpecArray[RCXBCMAP].bRelocate         = FALSE;
   tyAppSpecArray[RCXBCMAP].ulNewLocation     = 0xFF200200;
   tyAppSpecArray[RCXBCMAP].atApp             = RCXBCMAP;
   strcpy(tyAppSpecArray[RCXBCMAP].szSrecFilename,"RCXBCMAP.sre");
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_MDILogInitAndCheckSettings
     Engineer: Nikolay Chokoev
        Input: iErrorNumber : Error number.
       Output: None
  Description: Check the registry to see what options are enabled
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
int TL_MDILogInitAndCheckSettings(TyMDILogSettings ** pptyMDILogSettings)
{
   unsigned long ulLogMDICalls=0;
   unsigned long ulLogMDICallsTimeStamp=0;
   unsigned long ulLogMDICallsAppendLogFile=0;
   unsigned long ulLogMemoryRead=0;
   unsigned long ulLogMemoryWritten=0;
   int 			bOriginalLogEnabledSetting = FALSE;
   static int 	bFirstTimeInHere = TRUE;
   
   if (bFirstTimeInHere)
      {
      tyMDILogSettings.bStaticLogMDICalls = FALSE; // Assume Logfile wasn't already open
      bFirstTimeInHere = FALSE;
      }

   (void)TL_GetSetRegistryLongKey("Debug","LogMDICallsAppendLogfile",&ulLogMDICallsAppendLogFile,0); // Default to Disabled
   tyMDILogSettings.bAppendLogfile = (ulLogMDICallsAppendLogFile !=0 );

   (void)TL_GetSetRegistryLongKey("Debug","LogMDICallsTimeStamp",&ulLogMDICallsTimeStamp,0); // Default to Disabled
   tyMDILogSettings.bStaticLogTimeStamp = (ulLogMDICallsTimeStamp !=0 );

   (void)TL_GetSetRegistryLongKey("Debug","LogMemoryRead",&ulLogMemoryRead,0); // Default to Disabled
   tyMDILogSettings.bLogMemoryRead = (ulLogMemoryRead !=0 );

   (void)TL_GetSetRegistryLongKey("Debug","LogMemoryWritten",&ulLogMemoryWritten,0); // Default to Disabled
   tyMDILogSettings.bLogMemoryWritten = (ulLogMemoryWritten !=0 );

   bOriginalLogEnabledSetting = tyMDILogSettings.bStaticLogMDICalls;

   (void)TL_GetSetRegistryLongKey("Debug","LogMDICalls",&ulLogMDICalls,0); // Default to Disabled
   tyMDILogSettings.bStaticLogMDICalls = (ulLogMDICalls !=0 );

   if (tyMDILogSettings.bStaticLogMDICalls != bOriginalLogEnabledSetting)
      {
      // Change detected
      if (tyMDILogSettings.bStaticLogMDICalls)
         {
         // Logging Enabled. Open File for output.
         (void)TL_MDILogOpen(tyMDILogSettings.bAppendLogfile);
         }
      else
         {
         // Logging Disabled. Close file
         if (tyMDILogSettings.pGlobalMDILogFile != NULL)
            {
            // Attempt to close the file
            fclose(tyMDILogSettings.pGlobalMDILogFile);
            tyMDILogSettings.pGlobalMDILogFile = NULL;
            }
         }
      }

   if (pptyMDILogSettings != NULL)
      {
      *pptyMDILogSettings = &tyMDILogSettings;
      }

   return 0; //todo error code
}

/****************************************************************************
     Function: TL_QuerySupportedDevices
     Engineer: Nikolay Chokoev
        Input: *pHowMany : Number of Devices
			   *pDeviceData : Data on the Devices
       Output: Error
  Description: Query to either:                   
               1. Ask how many devices we supports        
               2. Ask for data on each of those devices
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_QuerySupportedDevices(MDIInt32 	*pHowMany,
								  MDIDDataT	*pDeviceData)
{
   
   unsigned int uiIndex;

   if (*pHowMany == 0)
      {
      // They just want to know how many devices we support
      *pHowMany = 1;  // For NOW    TODO: Read Family Config file to get this value
      return ERR_NO_ERROR;
      }

   // Otherwise we must report information on each device

   for (uiIndex=0; uiIndex < (unsigned int)*pHowMany; uiIndex++)
      {
      // Most important paramater is DeviceID
      pDeviceData->Id   = MDI_NO_DEVICE_OPEN + uiIndex; // Simply use the order of the family config file
      // For each device fill out its details.
      // TODO: Read this from Family File
      strcpy(pDeviceData->DName,"MIPS32 4Kc");
      
      // All the following fields must have tailing zeros after the string
      memset(pDeviceData->Family,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->FClass,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->FPart,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->FISA,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->Vendor,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->VFamily,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->FPart,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->VPartRev,0,SIZE_OF_DEVICE_FIELDS);
      memset(pDeviceData->VPartData,0,SIZE_OF_DEVICE_FIELDS);

      strcpy(pDeviceData->Family,"CPU");
      strcpy(pDeviceData->FClass,"MIPS");
      strcpy(pDeviceData->FPart,"MIPS324Kc");
      strcpy(pDeviceData->FISA,MDIMIP_FISA_M32);
      pDeviceData->Endian = 1; //Setting endian as little

      // The other paramaters are device specific and we set them to null
      // TODO: Perhaps we need to be nicer here

      pDeviceData++; // Move to next Device
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_DeviceConnect
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Attempt target connection, reset processor and halt in
               debug mode.
Date           Initials    Description
04-Jul-2008    NCH         Initial
03-Sep-2008    SPC         Memory allocation fix
01-Jan-2009    SPC         Fixed Cache debugging of Big Endian targets, Save &
                           Restore context application loading moved to midlayer.
****************************************************************************/
int TL_DeviceConnect(void)
{
   int iErrRet;
   unsigned long ulTemp;
   if(ptyMLHandle == NULL) 
      {
       ptyMLHandle = (TyMLHandle *)malloc(sizeof(TyMLHandle));
       if (ptyMLHandle == NULL)
           {// we can do anything but return with error
           iLastErrRet = 1;
           return 1; //todo error
           }
      }
   (void)memset(ptyMLHandle, 0, sizeof(TyMLHandle));

   iErrRet=TL_DetermineSettings();
   if (iErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tTL_OpellaConnect -> DetermineSettings Failed"));
	  free(ptyMLHandle);
	  ptyMLHandle = NULL;
      iLastErrRet = iErrRet;
      return iErrRet;
      }

   iErrRet=ML_InitConnection(ptyMLHandle);

   if (iErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> InitPort Failed"));
	  free(ptyMLHandle);
	  ptyMLHandle = NULL;
      iLastErrRet = iErrRet;
      return iErrRet;
      }

   ptyMLHandle->pulProcessorContext = (unsigned long*)malloc(MAX_DW2_DATA*4);
   if (ptyMLHandle->pulProcessorContext == NULL)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> Malloc Failed"));
      iLastErrRet = iErrRet;
      return iErrRet;
      }
   (void)memset(ptyMLHandle->pulProcessorContext, 0, MAX_DW2_DATA*4);

   for(ulTemp=0; ulTemp < MAX_NUM_SHADOW_REGISTER_SETS; ulTemp++)
      {
         ptyMLHandle->pulShadowRegisterSet[ulTemp] = 
            (unsigned long*)calloc(NUM_SHADOW_REGISTERS, sizeof(unsigned long));
      if (ptyMLHandle->pulShadowRegisterSet[ulTemp] == NULL)
         {
         DLOG((LOGFILE,"\n\tDiGdiOpen -> Malloc Failed"));
         return iErrRet;
         }
      }
   iErrRet = TL_InitialiseMemoryProtectionSystem(TRUE);

   if (iErrRet != ERR_NO_ERROR)
      {
      (void) TL_InitialiseMemoryProtectionSystem(FALSE); // If it didn't work, disable it. (ignore error)
      DLOG((LOGFILE,"\n\tDiGdiOpen -> Memory Protection Failed"));
      iLastErrRet = iErrRet;
      return iErrRet;
      }

   /* Save Shadow Registers */
   if(tyCurrentUserSettings.bMicroChipM4KFamily)
      {
      iErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                     SAV00SRS,
                                     &ptyMLHandle->tyMipsDW2App.pulSaveShadowRegisterSetApp,
                                     &ptyMLHandle->tyMipsDW2App.ulSaveShadowRegisterSetInstructions,
                                     NULL,NULL,(TySymbolIdentifier)0);
      if (iErrRet != ERR_NO_ERROR)
         {
         DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications SAV00SRS Failed"));
         return iErrRet;
         }
      }
   else
      {
      }

   /* Restore Shadow Registers */
   if(tyCurrentUserSettings.bMicroChipM4KFamily)
      {
      iErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                     RES00SRS,
                                     &ptyMLHandle->tyMipsDW2App.pulRestoreShadowRegisterSetApp,
                                     &ptyMLHandle->tyMipsDW2App.ulRestoreShadowRegisterSetInstructions,
                                     NULL,NULL,(TySymbolIdentifier)0);
      if (iErrRet != ERR_NO_ERROR)
         {
         DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications RES00SRS Failed"));
         return iErrRet;
         }
      }
   else
      {
      }

   /* Run target command */
   iErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                  RUNTGCMD,
                                  &ptyMLHandle->tyMipsDW2App.pulRunTargetCommandApp,
                                  &ptyMLHandle->tyMipsDW2App.ulRunTargetCommandInstructions,
                                  NULL,
                                  &ptyMLHandle->tyMipsDW2App.ulRunTargetInstrAddress,
                                  MAGIC_TGT_RETURN);

   // DW2 application to read words
   ptyMLHandle->tyMipsDW2App.pulReadWordsApp = pulReadmem;
   ptyMLHandle->tyMipsDW2App.ulReadWordsInstructions = ulReadmemNoOfInstructions;
   // DW2 application to write words
   ptyMLHandle->tyMipsDW2App.pulWriteWordsApp = pulWritewrd;
   ptyMLHandle->tyMipsDW2App.ulWriteWordsInstructions = ulWritewrdNoOfInstructions;
   // DW2 application to read halfwords
   ptyMLHandle->tyMipsDW2App.pulReadHalfwordsApp = pulReadHalf;
   ptyMLHandle->tyMipsDW2App.ulReadHalfwordsInstructions = ulReadHalfNoOfInstructions;
   // DW2 application to write halfwords
   ptyMLHandle->tyMipsDW2App.pulWriteHalfwordsApp = pulWritHalf;
   ptyMLHandle->tyMipsDW2App.ulWriteHalfwordsInstructions = ulWritHalfNoOfInstructions;
   // DW2 application to read bytes
   ptyMLHandle->tyMipsDW2App.pulReadBytesApp = pulReadByte;
   ptyMLHandle->tyMipsDW2App.ulReadBytesInstructions = ulReadByteNoOfInstructions;
   // DW2 application to write bytes
   ptyMLHandle->tyMipsDW2App.pulWriteBytesApp = pulWritByte;
   ptyMLHandle->tyMipsDW2App.ulWriteBytesInstructions = ulWritByteNoOfInstructions;
   // DW2 application to read TLB
   ptyMLHandle->tyMipsDW2App.pulReadTLBApp = pulRdTLBEnt;
   ptyMLHandle->tyMipsDW2App.ulReadTLBInstructions = ulRdTLBEntNoOfInstructions;
   // DW2 application to TLB bytes
   ptyMLHandle->tyMipsDW2App.pulWriteTLBApp = pulWrTLBEnt;
   ptyMLHandle->tyMipsDW2App.ulWriteTLBInstructions = ulWrTLBEntNoOfInstructions;
   // DW2 application to do cache action
   ptyMLHandle->tyMipsDW2App.pulActCacheApp = pulCacheAct;
   ptyMLHandle->tyMipsDW2App.ulActCacheInstructions = ulCacheActNoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulActCacheInstOffset = 0x0A;
   // DW2 application to read cache
   ptyMLHandle->tyMipsDW2App.pulReadCacheApp = pulRdCacEnt;
   ptyMLHandle->tyMipsDW2App.ulReadCacheInstructions = ulRdCacEntNoOfInstructions;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[0] = 0xD;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[1] = 0x11;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[2] = 0x14;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[3] = 0x17;
   // DW2 application to read CP0 register
   ptyMLHandle->tyMipsDW2App.pulReadCP0App = pulReadCp0;
   ptyMLHandle->tyMipsDW2App.ulReadCP0Instructions = ulReadCp0NoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulReadCP0InstOffset = 0x01;
   // DW2 application to write CP0 register
   ptyMLHandle->tyMipsDW2App.pulWriteCP0App = pulWriteCp0;
   ptyMLHandle->tyMipsDW2App.ulWriteCP0Instructions = ulWriteCp0NoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulWriteCP0InstOffset = 0x04;
   // DW2 application to read ACX register
   ptyMLHandle->tyMipsDW2App.pulReadACXApp = pulReadACX;
   ptyMLHandle->tyMipsDW2App.ulReadACXInstructions = ulReadACXNoOfInstructions;
   // DW2 application to write ACX register
   ptyMLHandle->tyMipsDW2App.pulWriteACXApp = pulWriteACX;
   ptyMLHandle->tyMipsDW2App.ulWriteACXInstructions = ulWriteACXNoOfInstructions;
   // DW2 application to Clear MP bit command
   ptyMLHandle->tyMipsDW2App.pulClearMPBitApp = pulClrMPBit;
   ptyMLHandle->tyMipsDW2App.ulClearMPBitInstructions = ulClrMPBitNoOfInstructions;
   // DW2 application to Cache Flush
   ptyMLHandle->tyMipsDW2App.pulCacheFlushApp = pulCacheFlush;
   ptyMLHandle->tyMipsDW2App.ulCacheFlushInstructions = ulCacheFlushNoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulCacheFlushInstrAddress = ulCachFlushInstrLoc;

   if (iErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications RUNTGCMD Failed"));
      return iErrRet;
      }

   if (tyCurrentUserSettings.bRstTgtBforeConn)
       {
       iErrRet=ML_Reset(ptyMLHandle, MODEFULLRESET, tyCurrentUserSettings);  // Lets try talking to Hardware !!  
       }
   else
       {
       iErrRet=ML_Reset(ptyMLHandle, MODENORESET, tyCurrentUserSettings);  // Lets try talking to Hardware !!  
       }

   if (iErrRet != ERR_NO_ERROR)
      {
      ASSERT_NOW();
      DLOG((LOGFILE,"\n\tDiGdiOpen -> Initialise Debugger Failed"));
	  //(void)TL_DeviceDisconnect();
      iLastErrRet = iErrRet;
      return iErrRet;
      }

   iErrRet = TL_ClearAllBP();
   if((ptyMLHandle->ucASHDMADebuggerClient))
      {
      iErrRet = ERR_NO_ERROR;
      // No error...we do not halt the processor
      }

   if (iErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tTL_DeviceConnect -> Failed to Clear all Breakpoints"));
      iLastErrRet = iErrRet;
      return iErrRet;
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_DeviceDisconnect
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Disconnect target
Date           Initials    Description
11-Jul-2008    NCH         Initial
03-Sep-2008    SPC         Freeing up allocated memory for ptyMLHandle
****************************************************************************/
int TL_DeviceDisconnect(void)
{
//todo multicore
   int iErr;
   unsigned long ulTemp;

   if (ptyMLHandle == NULL)
   {
       return 0;
   }

   iErr = ML_DeviceDisconnect(ptyMLHandle);

   if (ptyMLHandle->pulProcessorContext != NULL)
      {
      free(ptyMLHandle->pulProcessorContext);
      ptyMLHandle->pulProcessorContext = NULL;
      }
   for(ulTemp=0; ulTemp < MAX_NUM_SHADOW_REGISTER_SETS; ulTemp++)
      {
         if(ptyMLHandle->pulShadowRegisterSet[ulTemp] != NULL)
            {
            free(ptyMLHandle->pulShadowRegisterSet[ulTemp]);
            ptyMLHandle->pulShadowRegisterSet[ulTemp] = NULL;
            }
      }
   if(ptyMLHandle->tyMipsDW2App.pulSaveContextApp != NULL)
      {
      free(ptyMLHandle->tyMipsDW2App.pulSaveContextApp);
      ptyMLHandle->tyMipsDW2App.ulSaveContextInstructions = 0;
      }
   if(ptyMLHandle->tyMipsDW2App.pulRestoreContextApp != NULL)
      {
      free(ptyMLHandle->tyMipsDW2App.pulRestoreContextApp);
      ptyMLHandle->tyMipsDW2App.ulRestoreContextInstructions = 0;
      }
   if(ptyMLHandle->tyMipsDW2App.pulSaveShadowRegisterSetApp != NULL)
      {
      free(ptyMLHandle->tyMipsDW2App.pulSaveShadowRegisterSetApp);
      ptyMLHandle->tyMipsDW2App.ulSaveShadowRegisterSetInstructions = 0;
      }
   if(ptyMLHandle->tyMipsDW2App.pulRestoreShadowRegisterSetApp != NULL)
      {
      free(ptyMLHandle->tyMipsDW2App.pulRestoreShadowRegisterSetApp);
      ptyMLHandle->tyMipsDW2App.ulRestoreShadowRegisterSetInstructions = 0;
      }
   if(ptyMLHandle->tyMipsDW2App.pulRunTargetCommandApp != NULL)
      {
      free(ptyMLHandle->tyMipsDW2App.pulRunTargetCommandApp);
      ptyMLHandle->tyMipsDW2App.ulRunTargetCommandInstructions = 0;
      ptyMLHandle->tyMipsDW2App.ulRunTargetInstrAddress = 0;
      }
   free(ptyMLHandle);
   ptyMLHandle = NULL;
   return (iErr);
}

/****************************************************************************
     Function: TL_RegisterRead
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Read register
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_RegisterRead   (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count)
{
   unsigned long ulIndex;
   TyError ErrRet;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

//todo multicore
   for (ulIndex=(unsigned long)SrcOffset; ulIndex < (SrcOffset + Count); ulIndex++)
      {
      ErrRet = MLR_ReadRegister(ptyMLHandle, 
                                (unsigned long)ulIndex,
                                SrcResource,
                                (unsigned long *)pBuff);
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;

      pBuff = (((unsigned long *)pBuff) + 1); // Increment by 4 bytes
      }


   return ERR_NO_ERROR;
} 

/****************************************************************************
     Function: TL_MemoryRead
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Read memory
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_MemoryRead     (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize)
{
   
   TyError ErrRet;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   switch (ulObjectSize)
      {
      case 0:
         break;
      default:
         return ERR_TLMEMRD_INVALID_SIZE;
      }

   if (Count > MAX_TXCMD_RD_MEM_LEN)
      {
      ASSERT_NOW();
      return ERR_TLMEMRD_TOO_LARGE;          // TODO: We should not be limited here
      }

   if ((BETWEEN(SrcOffset,DMSEG_START,DMSEG_END)) && (BETWEEN(SrcOffset + Count,DMSEG_START,DMSEG_END)))
      {
      // Entire Range withing DMSEG
      return ERR_NO_ERROR;
      }

   if (BETWEEN(SrcOffset,DMSEG_START,DMSEG_END))
      {
      // Only Start Address within DMSEG => End Address is outside of DMSEG
      // Move Start address to end of DMSEG and modify buffer accordingly
      Count = Count - ((DMSEG_END + 1) - (unsigned int)SrcOffset); // Count reduced by difference
      SrcOffset = DMSEG_END + 1;                     // Start Address moved to End of DMSEG
      pBuff = (void*)  (((unsigned char *)pBuff) + ((DMSEG_END + 1) - SrcOffset)); // Buffer moved
      }

   if (BETWEEN(SrcOffset + Count,DMSEG_START,DMSEG_END))
      {
      // Only End Address within DMSEG => Start Address below DMSEG
      // Simply reduce the count
      Count = Count - ( (unsigned int)(SrcOffset + Count) - DMSEG_START); 
      }

   switch (SrcResource)
      {
      case MDIMIPGVIRTUAL:
         // Global Virtual Memory we can deal with this
         ErrRet = MLM_ReadMemory(ptyMLHandle,(unsigned long)SrcOffset,(unsigned char *)pBuff,Count,ulObjectSize);
         break;
      case MDIMIPVIRTUAL:
      case MDIMIPPHYSICAL:
      default:
         return ERR_TLMEMRD_INVALID_RES;
      }
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   return MDISuccess;
}

/****************************************************************************
     Function: TL_MDIRead
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: MDIRead
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_MDIRead        (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize)
{
   
   TyError ErrRet;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   ErrRet=ML_MDIRead(ptyMLHandle,
                     SrcResource,
                     (unsigned long)SrcOffset,
                     (unsigned char*)pBuff,
                     Count,ulObjectSize);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   return ERR_NO_ERROR;
} 

/****************************************************************************
     Function: TL_RegisterWrite
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Read memory
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_RegisterWrite  (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count)
{
   unsigned int uiIndex;
   TyError ErrRet;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   for (uiIndex=(unsigned int)SrcOffset; uiIndex < (SrcOffset + Count); uiIndex++)
      {
      ErrRet = MLR_WriteRegister(ptyMLHandle,
                                 (unsigned long)SrcOffset,
                                 SrcResource,
                                 *((unsigned long * )pBuff));
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;

      pBuff = (((unsigned long *)pBuff) + 1); // Increment by 4 bytes
      }

   return ERR_NO_ERROR;
} 

/****************************************************************************
     Function: TL_MemoryWrite
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Write memory
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_MemoryWrite    (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize)
{
   
   TyError ErrRet;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   switch (ulObjectSize)
      {
      case 0:
         break;
      default:
         return ERR_TLMEMWR_INVALID_SIZE;
      }

   if (Count > MAX_TXCMD_WR_MEM_LEN)
      {
      ASSERT_NOW();
      return ERR_TLMEMWR_TOO_LARGE;          // TODO: We should not be limited here
      }

   switch (SrcResource)
      {
      case MDIMIPGVIRTUAL:
         // Global Virtual Memory we can deal with this
         ErrRet = MLM_WriteMemory(ptyMLHandle,
                                  (unsigned long)SrcOffset,
                                  (unsigned char *)pBuff,
                                  Count,ulObjectSize);
         break;
      case MDIMIPVIRTUAL:
      case MDIMIPPHYSICAL:
      default:
         return ERR_TLMEMWR_INVALID_RES;
      }
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   return MDISuccess;
} 

/****************************************************************************
     Function: TL_MDIWrite
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Write memory
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
MDIInt32 TL_MDIWrite       (MDIResourceT SrcResource,
                            MDIOffsetT   SrcOffset,
                            void *       pBuff,
                            MDIUint32    Count,
                            MDIUint32    ulObjectSize)
{
   
   TyError ErrRet;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   ErrRet=ML_MDIWrite(ptyMLHandle,
                      (unsigned long)SrcResource,
                      (unsigned long)SrcOffset,
                      (unsigned char*)pBuff,
                      Count,ulObjectSize);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   return ERR_NO_ERROR;
} 

/****************************************************************************
     Function: TL_ClearAllBP
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: Write memory
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_ClearAllBP(void)
{
   TyError iErr;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }
   iErr = MLB_ClearAllBP(ptyMLHandle);
   return (iErr);
}

/****************************************************************************
     Function: TL_Stop
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_Stop(void)
{
   TyError iErr;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   iErr = ML_StopExecution(ptyMLHandle);
   return (iErr);
}

/****************************************************************************
     Function: TL_Stop
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_Reset(MDIUint32 Mode)
{
   
   TyError ErrRet;
   unsigned long  ulMode;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   switch (Mode)
      {
      default:
      case MDIFullReset:
         ulMode = MODEFULLRESET;
         break;
      case MDIDeviceReset:
         ulMode = MODEDEVICERESET;
         break;
      case MDICPUReset:
         ulMode = MODECPURESET;
         break;
      case MDIPeripheralReset:
         ulMode = MODEPERIPHERALRESET;
         break;
      case MDIPrRstReset:
         ulMode = MODEPRRSTRESET;
         break;
      case MDIPerRstReset:
         ulMode = MODEPERRSTRESET;
         break;
      case MDIJtagRstReset:
         ulMode = MODEJTAGRSTRESET;
         break;
      case MDITRSTReset:
         ulMode = MODETRSTRESET;
         break;
      case MDIRSTReset:
         ulMode = MODERSTRESET;
         break;
      }

   ErrRet = ML_Reset(ptyMLHandle, ulMode, tyCurrentUserSettings);

   return ErrRet;

}

/****************************************************************************
     Function: TL_GetStatus
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_GetStatus(TyStatus * ptyStatus) 
{
   TyError iErr;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   iErr = ML_GetStatus(ptyMLHandle, ptyStatus,TRUE); // Should be safe enough to use target memory if needed
   return (iErr);
}

/****************************************************************************
     Function: TL_SetBP
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_SetBP(unsigned long ulBpVirtAddress,
                 unsigned long ulBpAccessAddress,
                 TyBPType bptBPType,
                 unsigned long ulBPSize)
{
   TyError iErr;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   iErr = MLB_SetBP(ptyMLHandle,ulBpVirtAddress,ulBpAccessAddress,bptBPType,ulBPSize);
   return (iErr);
}

/****************************************************************************
     Function: TL_SetComplexHWBP
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_SetComplexHWBP(TyBpFlags *ptyBreakpointFlags)
{
   TyError iErr;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   iErr = MLB_SetComplexHWBP(ptyMLHandle, ptyBreakpointFlags);
   return (iErr);
}

/****************************************************************************
     Function: TL_ClearBP
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_ClearBP(unsigned long ulAddress,
                   TyBPType      *pbptBPType,
                   unsigned long *pulBPSize,
                   unsigned long *pulBpAccessAddress)
{
   unsigned char bNoBPPresent; // Unused
   TyError iErr;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   iErr =  MLB_ClearBP(ptyMLHandle,
                       ulAddress,
                       pbptBPType,
                       pulBPSize,
                       &bNoBPPresent,
                       pulBpAccessAddress);
   return (iErr);
}

/****************************************************************************
     Function: TL_Step
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_Step(MDIUint32 Steps, MDIUint32 Mode)
{
   
   TyError ErrRet;
   unsigned int uiIndex;
   unsigned long        ulMode;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   switch (Mode)
      {
      case MDIStepInto:
         ulMode = MODESTEPINTO;
         break;
      case MDIStepForward:
         ulMode = MODESTEPFORWARD;
         break;
      case MDIStepOver:
         ulMode = MODESTEPOVER;
         break;
      default:
         return ERR_TLSTEP_NOT_SUPPORTED;
      }


   for (uiIndex =0; uiIndex < Steps; uiIndex++)
      {
      ErrRet=ML_Step(ptyMLHandle,ulMode); 
      if (ErrRet != ERR_NO_ERROR)
         return ErrRet;
      }
   
   return ERR_NO_ERROR;

}

/****************************************************************************
     Function: TL_Execute
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error
  Description: 
Date           Initials    Description
11-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_Execute(void)
{
    if (ptyMLHandle == NULL)
    {
        ASSERT_NOW();
        return iLastErrRet;
    }

   return ML_Execute(ptyMLHandle);
}


/****************************************************************************
     Function: TL_GetDWFWVersions    
     Engineer: Nikolay Chokoev
        Input: pszDiskware1Version: Diskware1 Version String
               pszFirmwareVersion : Firmware Version String
               pszFpgaware1Version: Fpgaware1 Version String
               pszFpgaware2Version: Fpgaware2 Version String
               pszFpgaware3Version: Fpgaware3 Version String
               pszFpgaware4Version: Fpgaware4 Version String
       Output: Error value returned. See GDI spec for possible values.
  Description: Gets the diskware and fpgawares version numbers.
Date           Initials    Description
14-Jul-2008    NCH         Initial
****************************************************************************/
TyError TL_GetDWFWVersions   (char *pszDiskware1Version,
                              char *pszFirmwareVersion,
                              char *pszFpgaware1Version,
                              char *pszFpgaware2Version,
                              char *pszFpgaware3Version,
                              char *pszFpgaware4Version)
{
//todo   TyError ErrRet;

   TyDeviceInfo tyDevInfo;
   if (ptyMLHandle == NULL)
   {
       ASSERT_NOW();
       return iLastErrRet;
   }

   NOREF(pszFpgaware2Version);
   NOREF(pszFpgaware3Version);
   NOREF(pszFpgaware4Version);
   (void)DL_OPXD_GetDeviceInfo(ptyMLHandle->iDLHandle,&tyDevInfo);

   // OpellaXD version numbers
   //sprintf(pszDiskware1Version,"v%s, %s", OPELLAXD_VERSION_NUMBER, OPELLAXD_VERSION_DATE);
   ML_ConvertVersionDate(tyDevInfo.ulDiskwareVersion, 
                         tyDevInfo.ulDiskwareDate, 
                         pszDiskware1Version, 
                         MAX_STRING_SIZE);
   ML_ConvertVersionDate(tyDevInfo.ulFirmwareVersion, 
                         tyDevInfo.ulFirmwareDate, 
                         pszFirmwareVersion, 
                         MAX_STRING_SIZE);
   ML_ConvertVersionDate(tyDevInfo.ulFpgawareVersion, 
                         tyDevInfo.ulFpgawareDate, 
                         pszFpgaware1Version, 
                         SCREENWIDTH);

   return ERR_NO_ERROR; 

}

/****************************************************************************
     Function: TL_DeviceConnect_Ex    
     Engineer: Nikolay Chokoev
        Input: bSpecifiedProcessor : FALSE for diagnostic tools
  Description: Extended version of TL_DeviceConnect supporting JTAG diagnostic
               tools
Date           Initials    Description
14-Jul-2008    NCH         Initial
02-Sep-2008    SPC         Memory allocating ptyMLHandle to fix crash
20-Nov-2008    SPC         Freeing memories.
****************************************************************************/
TyError TL_DeviceConnect_Ex(int bSpecifiedProcessor)
{
   TyError ErrRet = ERR_NO_ERROR;
   unsigned long ulTemp;
   
   if(ptyMLHandle == NULL) 
      {
      ptyMLHandle = (TyMLHandle *)malloc(sizeof(TyMLHandle));
      if (ptyMLHandle == NULL)
         {// we can do anything but return with error
         return 1; //todo error
         }
      }
   (void)memset(ptyMLHandle, 0, sizeof(TyMLHandle));

   ErrRet=TL_DetermineSettings();
   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tTL_OpellaConnect -> DetermineSettings Failed"));
      return ErrRet;
      }

   ErrRet=ML_InitConnection(ptyMLHandle);

   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> InitPort Failed"));
      return ErrRet;
      }

   ptyMLHandle->pulProcessorContext = (unsigned long*)malloc(MAX_DW2_DATA*4);
   if (ptyMLHandle->pulProcessorContext == NULL)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> Malloc Failed"));
      return ErrRet;
      }
   (void)memset(ptyMLHandle->pulProcessorContext, 0, MAX_DW2_DATA*4);

   for(ulTemp=0; ulTemp < MAX_NUM_SHADOW_REGISTER_SETS; ulTemp++)
      {
         ptyMLHandle->pulShadowRegisterSet[ulTemp] = 
            (unsigned long*)calloc(NUM_SHADOW_REGISTERS, sizeof(unsigned long));
      if (ptyMLHandle->pulShadowRegisterSet[ulTemp] == NULL)
         {
         DLOG((LOGFILE,"\n\tDiGdiOpen -> Malloc Failed"));
         return ErrRet;
         }
      }

   ErrRet = TL_InitialiseMemoryProtectionSystem(TRUE);

   if (ErrRet != ERR_NO_ERROR)
      {
      (void) TL_InitialiseMemoryProtectionSystem(FALSE); // If it didn't work, disable it. (ignore error)
      DLOG((LOGFILE,"\n\tDiGdiOpen -> Memory Protection Failed"));
      return ErrRet;
      }
                

   // Special Savecontext
   ErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                 SCX00SCR,
                                 &ptyMLHandle->tyMipsDW2App.pulSaveContextApp,
                                 &ptyMLHandle->tyMipsDW2App.ulSaveContextInstructions,
                                 NULL,NULL,(TySymbolIdentifier)0);

   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications SCX00SCR Failed"));
      return ErrRet;
      }

   ErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                 SCX01RCR,
                                 &ptyMLHandle->tyMipsDW2App.pulSaveContextApp,
                                 &ptyMLHandle->tyMipsDW2App.ulSaveContextInstructions,
                                 NULL,NULL,(TySymbolIdentifier)0);
   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications SCX01RCR Failed"));
      return ErrRet;
      }

   /* Restore Context */
   ErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                 RCX00RRR,
                                 &ptyMLHandle->tyMipsDW2App.pulRestoreContextApp,
                                 &ptyMLHandle->tyMipsDW2App.ulRestoreContextInstructions,
                                 NULL,NULL,(TySymbolIdentifier)0);
   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications RCX00RRR Failed"));
      return ErrRet;
      }
   
   ErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                 RCX01RRR,
                                 &ptyMLHandle->tyMipsDW2App.pulRestoreContextApp,
                                 &ptyMLHandle->tyMipsDW2App.ulRestoreContextInstructions,
                                 NULL,NULL,(TySymbolIdentifier)0);
   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications RCX01RRR Failed"));
      return ErrRet;
      }

   ErrRet=ML_LoadDW2Applications(tyAppSpecArray,
                                 RUNTGCMD,
                                 &ptyMLHandle->tyMipsDW2App.pulRunTargetCommandApp,
                                 &ptyMLHandle->tyMipsDW2App.ulRunTargetCommandInstructions,
                                 NULL,NULL,(TySymbolIdentifier)0);
   if (ErrRet != ERR_NO_ERROR)
      {
      DLOG((LOGFILE,"\n\tDiGdiOpen -> LoadDW2Applications RUNTGCMD Failed"));
      return ErrRet;
      }

   // DW2 application to read words
   ptyMLHandle->tyMipsDW2App.pulReadWordsApp = pulReadmem;
   ptyMLHandle->tyMipsDW2App.ulReadWordsInstructions = ulReadmemNoOfInstructions;
   // DW2 application to write words
   ptyMLHandle->tyMipsDW2App.pulWriteWordsApp = pulWritewrd;
   ptyMLHandle->tyMipsDW2App.ulWriteWordsInstructions = ulWritewrdNoOfInstructions;
   // DW2 application to read halfwords
   ptyMLHandle->tyMipsDW2App.pulReadHalfwordsApp = pulReadHalf;
   ptyMLHandle->tyMipsDW2App.ulReadHalfwordsInstructions = ulReadHalfNoOfInstructions;
   // DW2 application to write halfwords
   ptyMLHandle->tyMipsDW2App.pulWriteHalfwordsApp = pulWritHalf;
   ptyMLHandle->tyMipsDW2App.ulWriteHalfwordsInstructions = ulWritHalfNoOfInstructions;
   // DW2 application to read bytes
   ptyMLHandle->tyMipsDW2App.pulReadBytesApp = pulReadByte;
   ptyMLHandle->tyMipsDW2App.ulReadBytesInstructions = ulReadByteNoOfInstructions;
   // DW2 application to write bytes
   ptyMLHandle->tyMipsDW2App.pulWriteBytesApp = pulWritByte;
   ptyMLHandle->tyMipsDW2App.ulWriteBytesInstructions = ulWritByteNoOfInstructions;
   // DW2 application to read TLB
   ptyMLHandle->tyMipsDW2App.pulReadTLBApp = pulRdTLBEnt;
   ptyMLHandle->tyMipsDW2App.ulReadTLBInstructions = ulRdTLBEntNoOfInstructions;
   // DW2 application to TLB bytes
   ptyMLHandle->tyMipsDW2App.pulWriteTLBApp = pulWrTLBEnt;
   ptyMLHandle->tyMipsDW2App.ulWriteTLBInstructions = ulWrTLBEntNoOfInstructions;
   // DW2 application to do cache action
   ptyMLHandle->tyMipsDW2App.pulActCacheApp = pulCacheAct;
   ptyMLHandle->tyMipsDW2App.ulActCacheInstructions = ulCacheActNoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulActCacheInstOffset = 0x0A;
   // DW2 application to read cache
   ptyMLHandle->tyMipsDW2App.pulReadCacheApp = pulRdCacEnt;
   ptyMLHandle->tyMipsDW2App.ulReadCacheInstructions = ulRdCacEntNoOfInstructions;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[0] = 0xD;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[1] = 0x11;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[2] = 0x14;
   ptyMLHandle->tyMipsDW2App.pulReadCacheInstOffset[3] = 0x17;
   // DW2 application to read CP0 register
   ptyMLHandle->tyMipsDW2App.pulReadCP0App = pulReadCp0;
   ptyMLHandle->tyMipsDW2App.ulReadCP0Instructions = ulReadCp0NoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulReadCP0InstOffset = 0x01;
   // DW2 application to write CP0 register
   ptyMLHandle->tyMipsDW2App.pulWriteCP0App = pulWriteCp0;
   ptyMLHandle->tyMipsDW2App.ulWriteCP0Instructions = ulWriteCp0NoOfInstructions;
   ptyMLHandle->tyMipsDW2App.ulWriteCP0InstOffset = 0x04;
   // DW2 application to read ACX register
   ptyMLHandle->tyMipsDW2App.pulReadACXApp = pulReadACX;
   ptyMLHandle->tyMipsDW2App.ulReadACXInstructions = ulReadACXNoOfInstructions;
   // DW2 application to write ACX register
   ptyMLHandle->tyMipsDW2App.pulWriteACXApp = pulWriteACX;
   ptyMLHandle->tyMipsDW2App.ulWriteACXInstructions = ulWriteACXNoOfInstructions;
   // DW2 application to Clear MP bit command
   ptyMLHandle->tyMipsDW2App.pulClearMPBitApp = pulClrMPBit;
   ptyMLHandle->tyMipsDW2App.ulClearMPBitInstructions = ulClrMPBitNoOfInstructions;

   ErrRet=ML_Reset_Ex(ptyMLHandle,
                      MODEFULLRESET, 
                      tyCurrentUserSettings, 
                      bSpecifiedProcessor);

   if (ErrRet != ERR_NO_ERROR)
      {
      ASSERT_NOW();
      DLOG((LOGFILE,"\n\tDiGdiOpen -> Initialise Debugger Failed"));
      return ErrRet;
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TL_CacheFlush    
     Engineer: Suresh P.C
        Input: Type : Type of cache to be flushed
	   		   Flag : Type of invalidation to be done
  Description: Flush the cache
Date           Initials    Description
30-Aug-2011    SPC         Initial
****************************************************************************/
TyError TL_CacheFlush(MDIUint32 Type, MDIUint32 Flag)
{
   TyError ErrRet;
   unsigned int cacheType;
   if (ptyMLHandle == NULL)
      {
      ASSERT_NOW();
      return iLastErrRet;
      }
   switch (Type)
	  {
	  case MDICacheTypeInstruction:
		 cacheType = CACHE_TYPE_INST;
		 break;
	  case MDICacheTypeData:
		 cacheType = CACHE_TYPE_DATA;
		 break;
	  default:
		 //cacheType = CACHE_TYPE_UNIFIED;
         //For testing purpose, now only invalidate Secondary cache
         cacheType = CACHE_TYPE_SECONDARY;
		 break;
	  }
   //The flags can be MDICacheHit/MDICacheWriteBack/MDICacheInvalidate
   //But we are only doing Writeback invalidate

   //Currently MLC_FlushCache handles only CACHE_FLUSH_INVALIDATE
   Flag = Flag; //Make lint happy

   //Currently MLC_FlushCache handles only CACHE_TYPE_UNIFIED
   ErrRet = MLC_FlushCache (ptyMLHandle, cacheType, CACHE_FLUSH_INVALIDATE);
   return ErrRet;
}

