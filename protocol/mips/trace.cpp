/***************************trace.cpp*****************************/
/**  \file

\brief This file is the interface for GDBServer for all 
        Zephyr trace related activities.
*/
/*-----------------------------------------------------------------------------
* Version    Author          Date                  Description
*------------------------------------------------------------------------------
*   1.0      Rejeesh         17-Aug-2011         Initial Version Created

*----------------------------------------------------------------------------*/

/*****************************Standard Headers********************************/
#ifdef __LINUX
#include <stdlib.h>
#include <string.h>
#else
#include "windows.h"
#endif
#include <assert.h>
#include <stdio.h>
/***********************Application Specific Headers**************************/
#define MDI_LIB               // We are an MDI Library not a debugger
#define MAX_DISKWARE_DATA 0x200
#include "trace.h"
#include "gerror.h"
#include "mdi.h"
#include "protocol/common/drvopxd.h"
#include "midlayer.h"
#include "protocol/common/drvopxdmips.h"
#include "commdefs.h"
#include "mips.h"
#include "mdi/dw2/ReadSecCach.h"
#include "ash_iface.h"
#include "trace_disassembler.h"
/*****************************Extern Variables********************************/
extern TyMLHandle *ptyMLHandle;

/*****************************Global Variables********************************/
TyTraceControlRegs tyTraceControlRegs;
/******************************************************************************
*   Function: AshFetchTrace
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	17-Aug-2011     RSB         Initial
******************************************************************************/
/**
   \brief This API will be used by GDBServer
          for fetching Zephyr trace from the target.

   \details Functionalities implemented are:
               - Fetch raw trace from the target.
               - Raw trace is stored in rawtrace.tmp
               - Invokes decoder to decode the raw trace

   \param[in] ulBegPtr               - Address of Begin Pointer
   \param[in] ulEndPtr               - Address of End Pointer               

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
/*
NOTE : Differnce between Begptr and end ptr should be a multiple of 8 bytes;
This is how the routine is written;otherwise go change the routine
*/
extern "C" int AshFetchTrace(unsigned int ulBegPtr,unsigned int ulEndPtr)
{
    unsigned int ulDataToProc[2];
    int iResult = ERR_NO_ERROR;

    unsigned int ulTotalSize        = 0;
    unsigned int ulRemainingBytes   = 0;
    unsigned int ulnewEndPtr        = 0;

    ulTotalSize = ulEndPtr - ulBegPtr;
    unsigned int ulTotalSizeWords = ulTotalSize/4;
    unsigned int *pulData       = (unsigned int *)malloc(ulTotalSize);

    /*Null checking*/
    if (NULL == pulData)
        {
            SetupErrorMessage(ERR_ZEPHYR_TRACE_FETCH);
            iResult = ZEPHYR_TRACE_ERROR;                
            return iResult;
        }

    unsigned int *pulTempData   = pulData;

    //TODO: CHECK if we support more than 512 bytes at a time;if so change the limit here
    if (ulTotalSize > MAX_DISKWARE_DATA)
        {
        ulRemainingBytes = ulTotalSize;
        while (ulRemainingBytes > MAX_DISKWARE_DATA )
            {
            ulnewEndPtr = ulBegPtr + MAX_DISKWARE_DATA;
            //Prepare input for dw2 app
            ulDataToProc[0] = ulBegPtr;         /*Start address*/
            ulDataToProc[1] = ulnewEndPtr;      /*End address*/

            iResult = ML_ExecuteDW2(ptyMLHandle, 
                                    (void *)(pulReadSecCach), 
                                    ulReadSecCachNoOfInstructions * sizeof (unsigned int),
                                    (void *)ulDataToProc, 
                                    2 * sizeof(unsigned int), 
                                    (void *)(pulTempData), 
                                    sizeof(unsigned int) * (MAX_DISKWARE_DATA/4));
            if (ERR_NO_ERROR != iResult)
                {
                SetupErrorMessage(ERR_ZEPHYR_TRACE_FETCH);
                iResult = ZEPHYR_TRACE_ERROR;                
                free(pulData);
                return iResult;
                }

            ulBegPtr         += MAX_DISKWARE_DATA;
            ulRemainingBytes -= MAX_DISKWARE_DATA; 
            if(pulTempData != NULL)
                {
                pulTempData       = pulTempData + MAX_DISKWARE_DATA/sizeof(unsigned int);
                }
            };

        //now, <512 bytes remaining,so execute it here
        if(ulRemainingBytes > 0)
            {
            ulnewEndPtr = ulBegPtr+ ulRemainingBytes;
            //Execute the dw2 app
            ulDataToProc[0] = ulBegPtr;         // start index address
            ulDataToProc[1] = ulnewEndPtr;      //we will read 512 bytes

            iResult = ML_ExecuteDW2(ptyMLHandle, 
                                    (void *)(pulReadSecCach), 
                                    ulReadSecCachNoOfInstructions * sizeof (unsigned int),
                                    (void *)ulDataToProc, 
                                    2 * sizeof(unsigned int), 
                                    (void *)(pulTempData), 
                                    sizeof(unsigned int) * (ulRemainingBytes/4));

            if (ERR_NO_ERROR != iResult)
                {
                SetupErrorMessage(ERR_ZEPHYR_TRACE_FETCH);
                iResult = ZEPHYR_TRACE_ERROR;                
                free(pulData);
                return iResult;
                }
            }
        } 
    else
        {
        // prepare DW2 parameters and execute it
        ulDataToProc[0] = ulBegPtr;         // start index address
        ulDataToProc[1] = ulEndPtr;         //we will read 512 bytes
        iResult = ML_ExecuteDW2(ptyMLHandle, 
                                (void *)(pulReadSecCach), 
                                ulReadSecCachNoOfInstructions * sizeof (unsigned int),
                                (void *)ulDataToProc, 
                                2 * sizeof(unsigned int), 
                                (void *)(pulData), 
                                sizeof(unsigned int) * ulTotalSizeWords);

        if (ERR_NO_ERROR != iResult)
            {
            SetupErrorMessage(ERR_ZEPHYR_TRACE_FETCH);
            iResult = ZEPHYR_TRACE_ERROR;                
            free(pulData);
            return iResult;
            }

        }
    /*Save the raw trace data to rawtrace.tmp. This is required for PF-XD to save raw trace to FILENAME.CSV.RAW */
    FILE *pTraceFile = fopen("rawtrace.tmp","w");
    if((pTraceFile != NULL) && (pulData != NULL))
           {
               for(unsigned int i = 0;i<ulTotalSizeWords;i++)
               {
                   fprintf(pTraceFile,"%x\n",*(pulData+i));
               }
               fflush(pTraceFile);
               fclose(pTraceFile);
           }

    /*Call decoder to decode raw trace*/
    iResult = decoder_main(pulData,ulTotalSizeWords);
    if (ERR_NO_ERROR != iResult)
        {
        SetupErrorMessage(iResult);     
        free(pulData);
        return ZEPHYR_TRACE_ERROR;
        }
    /*Free the pointer*/
    free(pulData);

    return ERR_NO_ERROR;
}
/******************************************************************************
*   Function: AshConfigureTrace
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	24-Aug-2011     RSB         Initial
******************************************************************************/
/**
   \brief This API will be used by GDBServer for configuring Zephyr trace.

   \details Functionalities implemented are:
               - Update the TraceControl registers in the target
               - Update the TraceControl structure with the new values

   \param[in] ulTC  - TraceControl Register value
   \param[in] ulTC2 - TraceControl2 Register value 
   \param[in] ulTC3 - TraceControl3 Register value

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
extern "C" int AshConfigureTrace(unsigned int ulTC,
                                 unsigned int ulTC2,
                                 unsigned int ulTC3
                                 )
{
    int iErrRet = ERR_NO_ERROR;

     /*Write TraceControl2(CP0 23,Sel 2)*/
     tyTraceControlRegs.tyTraceControl2.ulTraceControl2 = ulTC2;
     iErrRet = MLR_WriteRegister(ptyMLHandle,REG_NUM_ZEPHYR_TRACECONTROL2, MDIMIPCP0, ulTC2);
     if (ERR_NO_ERROR != iErrRet)
        {
        SetupErrorMessage(ERR_ZEPHYR_TRACE_CONFIG);             
        return ZEPHYR_TRACE_ERROR;
        }

     /*Write TraceControl3(CP0 23,Sel 3)*/
     tyTraceControlRegs.tyTraceControl3.ulTraceControl3 = ulTC3;
     iErrRet = MLR_WriteRegister(ptyMLHandle,REG_NUM_ZEPHYR_TRACECONTROL3, MDIMIPCP0, ulTC3);
     if (ERR_NO_ERROR != iErrRet)
        {
        SetupErrorMessage(ERR_ZEPHYR_TRACE_CONFIG);
        return ZEPHYR_TRACE_ERROR;
        }

     /*Note:This is done here to ensure that above steps are not traced as this step will enable trace*/
     /*Write TraceControl(CP0 23,Sel 1)*/
     tyTraceControlRegs.tyTraceControl.ulTraceControl = ulTC;
     iErrRet = MLR_WriteRegister(ptyMLHandle,REG_NUM_ZEPHYR_TRACECONTROL, MDIMIPCP0, ulTC);
     if (ERR_NO_ERROR != iErrRet)
        {
        SetupErrorMessage(ERR_ZEPHYR_TRACE_CONFIG);
        return ZEPHYR_TRACE_ERROR;
        }

     return ERR_NO_ERROR;
}
/*******************************************************************************
     Function: AshTraceElfPath
     Engineer: Amala Nair G S
        Input: char* pszElfPath - full path of .elf file
       Output: int - error code
  Description: This API will be used by GDBServer to pass the full path of .elf
Date           Initials    Description
01-Nov-2011    ANGS        Initial
********************************************************************************/
extern "C" int AshTraceElfPath(char* pszElfPath)
{
    int iErrRet = ERR_NO_ERROR; 
    /*Deinit disassembler before initializing again*/
    (void)DeInitDisa();

    /*Init Disassembler*/
    if(ERR_NO_ERROR != InitDisa(pszElfPath))
        {
        SetupErrorMessage(ERR_ZEPHYR_INIT_TRACE_DISASSEMBLER);
        return ZEPHYR_TRACE_ERROR;
        }
    return iErrRet;
}

