/****************************************************************************
       Module: loadsrec.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of loading SREC file.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#ifndef __LINUX
// Windows specific includes
#include <windows.h>
#include <winerror.h>
#else
// Linux includes

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MDI_LIB               // We are an MDI Library not a debugger
#include "protocol/common/drvopxd.h"
#include "commdefs.h"
#include "mdi.h"
#include "debug.h"
#include "gerror.h"
#include "midlayer.h"
#include "toplayer.h"
#include "loadsrec.h"
#include "firmware/export/api_cmd.h"
#include "../../utility/debug/debug.h"

// ------------------------------------------------
#include "message.h" //this only for compatibility-
// ------------------------------------------------

#define MAKE_UBYTE(LOW_NIBBLE,HIGH_NIBBLE)  (((unsigned char)LOW_NIBBLE) | (((unsigned char)HIGH_NIBBLE)<<4))
#define MAKE_UINT(LOW_UBYTE,HIGH_UBYTE)  (((unsigned int)LOW_UBYTE) | (((unsigned int)HIGH_UBYTE)<<8))
#define MAKE_ULONG(LOW_WORD, HIGH_WORD) ((unsigned long)(((unsigned int)(LOW_WORD)) | (((unsigned long)((unsigned int)(HIGH_WORD))) << 16)))

#ifndef __LINUX
//#include "/pathfndr/protocol/gdi/mips/opelmips/opelmips.h"
//extern CGDITESTApp theApp;
#endif

// Each time a new symbol is added this table must be updated so that the UpdateSymbolValue function 
// can determine the Symbol Identifier from its name.
// You must also update the TySymbolType structure in types.h

TySymbolLookup tySymbolLookupTable[]=
{
   {MAGIC_TGT_RETURN          ,  "MAGIC_TGT_RETURN"},
   {MAGIC_WRITEMEM            ,  "MAGIC_WRITEMEM"},
   {MAGIC_READMEM             ,  "MAGIC_READMEM"},
   {MAGIC_CACHE_INS_LOCATION  ,  "MAGIC_CACHE_INS_LOCATION"},
   {MAGIC_LAST_ENTRY          ,  "MAGIC_AFTER_CACHE"}
};

// Constructor
CLoadSrecFile::CLoadSrecFile()
{
   strcpy(szModuleName, "");
   strcpy(szInFileName, "");
   pInFile      = NULL;
   bRelocate    = FALSE; 
   slRelocation = 0;

   ptyApplicationArray[0].bRecordValid=0;
   ptyApplicationArray[0].ulEndAddress=0;
   ptyApplicationArray[0].ulStartAddress=0;

}

/****************************************************************************
     Function: Initialise
     Engineer: Nikolay Chokoev
        Input: int argc, char *argv[]
       Output: TyError
  Description: Determine and open input file
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::Initialise(int argc, char *argv)
{
   char szLocInFileName [_MAX_PATH];
   char szLocSrecFileName[_MAX_PATH];

#ifdef __LINUX
   argc = argc;
   strcpy (szLocInFileName,  argv);
   strcpy (szLocSrecFileName,  argv);

#else

   NOREF(argc);

   (void)MLGetLibPath(szLocInFileName,_MAX_PATH);

   sprintf(szLocSrecFileName,"%s\\%s",szLocInFileName,argv);

#endif

   DLOG((LOGFILE,"\nCLoadSrecFile::Initialise -> Srec Filename : %s",szLocSrecFileName));

   // Open the input Srec file for reading
   if ((pInFile = fopen (szLocSrecFileName,"rb")) == NULL)
      {
      DLOG((LOGFILE,"\n\tProcessSrecFile Error Cannot open %s",szLocInFileName));
      return ERR_LOADSREC_CANNOT_OPEN_FILE;
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ProcessSRecFile
     Engineer: Nikolay Chokoev
        Input: none        (as pInFile and pOutFile are global)
       Output: TyError
  Description: Read and process all records in the input SRec file, writing
               appropriate records to the output OMF file.
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::ProcessSRecFile(char           *pdInFile, 
									   unsigned long  Rel_Offset, 
									   unsigned long  CodeMemStart, 
									   unsigned long  CodeMemEnd,
									   unsigned long  ulNewLocation,
                                       unsigned long  **pulApp,
                                       unsigned long  *pulAppSize,
                                       unsigned long  *pulAppStartAddress,
                                       unsigned long  *pulInstrOffset,
                                       TySymbolIdentifier tySimbol)
{
   TyError        	ErrRet                           = ERR_NO_ERROR;
   unsigned long 	ulBaseAddress                    = 0;
//   unsigned long 	ulRecordNumber                   = 0;
   unsigned char 	ubRecType, ubRecLen, ubCount     = 0; // was unsigned char
   unsigned char 	ubHigh1, ubLow1, ubHigh2, ubLow2 = 0;
   unsigned short 	uHigh, uLow                      = 0;
   bool           	bFinished                        = FALSE;
   char           	szBuffer[MAX_CHARS_IN_SREC_RECORD+1];
   unsigned char 	DataBuffer[MAX_DATA_BYTES_IN_DATA_RECORD];
   char           	szSymbolName[MAX_STRING_SIZE];


   NOREF(CodeMemStart);
   NOREF(CodeMemEnd);

   ErrRet = Initialise(0,pdInFile);
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;
   // cir_tab.mem_type = code;

   ErrRet = InitialiseWriteBuffer();
   if (ErrRet != ERR_NO_ERROR)
      return ErrRet;

   if(pInFile == NULL)
	  {
//	  printf("Error: Reading input file <%s>\n",szInFileName);
	  return F_CANNOT_FIND_READ_FILE;
	  }
       
   // for all records in the input SRec file
   while (!bFinished && !ErrRet)
      {
      // read next S-Record
      if (fgets(szBuffer, sizeof(szBuffer), pInFile) == NULL)
         {
         // must be either End-Of-File or Error
         if (feof(pInFile))
            {
            bFinished = TRUE;
            break;
            }
         else
            {
//            printf("Error: Reading input file <%s>\n",szInFileName);
            return F_CANNOT_FIND_READ_FILE;
            }
         }

      if (szBuffer[0] != 'S')
         {
//         printf("Error: Invalid S-Record found at line %ld.\n", ulRecordNumber + 1);
         if (pInFile != NULL)
            (void)fclose(pInFile);
         return ERR_LOADSREC_INVALID_SREC_FILE;
         }
      ubRecType = AsciiHexToUbyte(szBuffer[1]); // Get the record type
      switch (ubRecType)
         {
         /* S0 NO DATA
            S1 DATA
            S2 DATA
            S3 DATA
            S4 Symbol
            S5 NO DATA
            S7 NO DATA
            S8 NO DATA
            S9 NO DATA */
         
         case S_Record_TYPE_S0: // Header Record so ignore   
            // NO DATA
            break;

         case S_Record_TYPE_S1: // 2 byte address and contains data
            ubRecLen      = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[3]), AsciiHexToUbyte(szBuffer[2])); 
            ubRecLen      -=3;  // -3 as it includes 1 for address and 1 for checksum

            ubHigh1       = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[5]), AsciiHexToUbyte(szBuffer[4]));
            ubLow1        = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[7]), AsciiHexToUbyte(szBuffer[6]));
            ulBaseAddress = MAKE_UINT (ubLow1, ubHigh1); // Complete Address

            // extract the data bytes converting to binary
            for (ubCount=0; ubCount<ubRecLen; ubCount++)
               {
               DataBuffer[ubCount] = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[9+(ubCount*2)]), AsciiHexToUbyte(szBuffer[8+(ubCount*2)]));
               }

            ErrRet = BufferedWrite((Rel_Offset+ulBaseAddress),
                                   (unsigned long)ubRecLen,
                                   DataBuffer);

            if (ErrRet!= ERR_NO_ERROR)
               {
               ASSERT_NOW();
               return(ErrRet);
               }
            break;

         case S_Record_TYPE_S2: // 3 BYTE ADDRESS + DATA
            ubRecLen = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[3]), AsciiHexToUbyte(szBuffer[2])); 
            ubRecLen -=4;  // -4 as it includes 3 for address and 1 for checksum

            ubHigh1  = 0;
            ubLow1   = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[5]), AsciiHexToUbyte(szBuffer[4]));
            uHigh    = (unsigned char)MAKE_UINT(ubLow1, ubHigh1);   // Address P1

            ubHigh2  = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[7]), AsciiHexToUbyte(szBuffer[6]));
            ubLow2   = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[9]), AsciiHexToUbyte(szBuffer[8]));
            uLow     = MAKE_UINT(ubLow2, ubHigh2);   // Address P2

            ulBaseAddress = MAKE_ULONG(uLow, uHigh); // Complete Address

            // extract the data bytes converting to binary
            for (ubCount = 0; ubCount < ubRecLen; ubCount++)
               {
               DataBuffer[ubCount] = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[11+(ubCount*2)]), AsciiHexToUbyte(szBuffer[10+(ubCount*2)]));
               }
            ErrRet = BufferedWrite((Rel_Offset+ulBaseAddress),
                                   (unsigned long)ubRecLen,
                                   DataBuffer);
            if (ErrRet!= ERR_NO_ERROR)
               {
               ASSERT_NOW();
               return(ErrRet);
               }
            break;

         case S_Record_TYPE_S3: // 4 byte Address + Data
            ubRecLen = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[3]), AsciiHexToUbyte(szBuffer[2])); 
            ubRecLen -=5; // -5 as it includes 4 for address and 1 for checksum

            ubHigh1  = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[5]), AsciiHexToUbyte(szBuffer[4]));
            ubLow1   = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[7]), AsciiHexToUbyte(szBuffer[6]));
            uHigh    = MAKE_UINT(ubLow1, ubHigh1);   // Address p1

            ubHigh2  = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[9]), AsciiHexToUbyte(szBuffer[8]));
            ubLow2   = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[11]), AsciiHexToUbyte(szBuffer[10]));
            uLow     = MAKE_UINT(ubLow2, ubHigh2);   // Address p2

            ulBaseAddress = MAKE_ULONG(uLow, uHigh); // Complete address

            // extract the data bytes converting to binary
            for (ubCount = 0; ubCount < (ubRecLen); ubCount++)
               {
               DataBuffer[ubCount] = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[13+(ubCount*2)]), AsciiHexToUbyte(szBuffer[12+(ubCount*2)]));
               }

            ErrRet = BufferedWrite((Rel_Offset+ulBaseAddress),
                                   (unsigned long)ubRecLen,
                                   DataBuffer);

            if (ErrRet!= ERR_NO_ERROR)
               {
               ASSERT_NOW();
               return(ErrRet);
               }
            break;

         case S_Record_TYPE_S4:
            // Symbol Record
            ubRecLen = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[3]), AsciiHexToUbyte(szBuffer[2])); 
            ubRecLen -=5; // -5 as it includes 4 for address and 1 for checksum

            ubHigh1  = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[5]), AsciiHexToUbyte(szBuffer[4]));
            ubLow1   = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[7]), AsciiHexToUbyte(szBuffer[6]));
            uHigh    = MAKE_UINT(ubLow1, ubHigh1);   // Address p1

            ubHigh2  = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[9]), AsciiHexToUbyte(szBuffer[8]));
            ubLow2   = (unsigned char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[11]), AsciiHexToUbyte(szBuffer[10]));
            uLow     = MAKE_UINT(ubLow2, ubHigh2);   // Address p2

            ulBaseAddress = MAKE_ULONG(uLow, uHigh); // Complete address

            // extract the symbol name
            for (ubCount = 0; ubCount < (ubRecLen); ubCount++)
               {
               szSymbolName[ubCount] = (char)MAKE_UBYTE(AsciiHexToUbyte(szBuffer[13+(ubCount*2)]), AsciiHexToUbyte(szBuffer[12+(ubCount*2)]));
               }

            szSymbolName[ubCount] = 0x0; // Terminate.

            // Symbol now in szSymbolName,ulBaseAddress
            ErrRet = UpdateDW2SymbolRecord (szSymbolName,ulBaseAddress);

            break;

         case S_Record_TYPE_S5:
            // NO DATA
            break;

         case S_Record_TYPE_S7:
            // NO DATA & not interested in execution start address

            break;

         case S_Record_TYPE_S8:
            // NO DATA & not interested in execution start address
            break;

         case S_Record_TYPE_S9:
            // NO DATA & not interested in execution start address
            break;

         default:
//            printf("Warning: Unknown S-Record '%02X' found at line %ld.\n", ubRecType, ulRecordNumber + 1);
            break;
         }
//      ulRecordNumber++;
      }   // for all records ...


   ErrRet = ProcessWriteBuffer(ulNewLocation,
                               pulApp,
                               pulAppSize,
                               pulAppStartAddress); //lint !e530

   if (ErrRet != ERR_NO_ERROR)
      {
      return ErrRet;
      }

   ErrRet = ProcessSymbolBuffer(tySimbol, pulInstrOffset);
   
   if (ErrRet != ERR_NO_ERROR)
      {
      return ErrRet;
      }

   if (pInFile != NULL)
      (void)fclose(pInFile);

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: AsciiHexToUbyte
     Engineer: Nikolay Chokoev
        Input: cHexVal: ASCII character to covert
       Output: cHexVal converted
  Description: Converts hexadecimal ASCII to binary
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
unsigned char CLoadSrecFile::AsciiHexToUbyte(char cHexVal)
{
   if ((cHexVal >= '0') && (cHexVal <= '9'))
      return(unsigned char)(cHexVal - '0');
   else if ((cHexVal >= 'A') && (cHexVal <= 'F'))
      return(unsigned char)(cHexVal - 'A' + 10);
   else
      return(unsigned char)(cHexVal - 'a' + 10);
}


/****************************************************************************
     Function: BufferedWrite
     Engineer: Nikolay Chokoev
        Input: ulStartAddress,ulLength,pucData,TyApplicationType atApp
       Output: Error Code
  Description: Buffers write of DW2 applications
Version       Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::BufferedWrite(unsigned long ulStartAddress,
                                     unsigned long ulLength,
                                     unsigned char *pucData)
{

   unsigned long ulIndex;

   if (!ptyApplicationArray[0].bRecordValid)
      {
      ptyApplicationArray[0].ulStartAddress = ulStartAddress;
      ptyApplicationArray[0].ulEndAddress   = ulStartAddress - 1; // as if contiguous
      ptyApplicationArray[0].bRecordValid    = TRUE;
      }

   if (ulStartAddress < ptyApplicationArray[0].ulStartAddress)
      {
      // This shouldn't happen
      DLOG((LOGFILE,"\nBufferedWrite -> Attempt to write to a lower address"));
      return ERR_BUFWRITE_ATTEMPTED_LOW_WRITE;
      }

   if ((ulStartAddress + ulLength) > (ptyApplicationArray[0].ulStartAddress + MAX_APPLICATION_SIZE))
      {
      // Buffer too big
      DLOG((LOGFILE,"\nBufferedWrite -> Buffer too big"));
      return ERR_BUFWRITE_BUF_TOO_BIG;
      }

   if (ulStartAddress != ptyApplicationArray[0].ulEndAddress+1)
      {
      // This shouldn't happen
      DLOG((LOGFILE,"\nBufferedWrite -> Attempt to write non-contiguous block"));
      return ERR_BUFWRITE_ATTEMPTED_NONCONTG_WRITE;
      }


   for (ulIndex  = (ulStartAddress - ptyApplicationArray[0].ulStartAddress); 
       ulIndex <  ((ulStartAddress+ulLength)-ptyApplicationArray[0].ulStartAddress);
       ulIndex++)
      {
      ptyApplicationArray[0].pucDataRecord[ulIndex] = *pucData;
      pucData++;
      }

   if ((ulStartAddress + ulLength) > ptyApplicationArray[0].ulEndAddress)
      ptyApplicationArray[0].ulEndAddress = ulStartAddress + ulLength - 1;


   return ERR_NO_ERROR;

}

/****************************************************************************
     Function: UpdateDW2SymbolRecord
     Engineer: Nikolay Chokoev
        Input: pszSymbolName,ulSymbolAddress,TyApplicationType atApp
       Output: Error Code
  Description: Updates ptyApplicationArray with Symbolic Information
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::UpdateDW2SymbolRecord(char * pszSymbolName,
                                             unsigned long ulSymbolAddress)
{

   TySymbolIdentifier tySymbolIdentifier;
   unsigned int uiIndex;

   memset(&tySymbolIdentifier,0x0,sizeof(tySymbolIdentifier));
   // First Search for Identifier for this String

   for (uiIndex=0; uiIndex < (unsigned int)MAGIC_LAST_ENTRY; uiIndex++)
      {
      if (strcmp(tySymbolLookupTable[uiIndex].szSymbolName,pszSymbolName)==0)
         {
         tySymbolIdentifier = (TySymbolIdentifier) uiIndex;
         break;
         }
      }

   if (uiIndex == (unsigned int)MAGIC_LAST_ENTRY)
      {
      // No Match was found for this Symbol
      DLOG((LOGFILE,"\nUpdateDW2SymbolRecord -> Symbol %s not found",pszSymbolName));
      return ERR_LSREC_SYM_NOT_FOUND;
      }

   if (ptyApplicationArray[0].bRecordValid)
      {
      ptyApplicationArray[0].tySymbolArray[tySymbolIdentifier].ulSymbolAddress    = ulSymbolAddress;
      DLOG((LOGFILE,"\nUpdateDW2SymbolRecord -> Success: APP: %d",0));
      DLOG((LOGFILE,"\nUpdateDW2SymbolRecord -> Success Sym:%s Ident:%d Addr:%08lX",pszSymbolName,tySymbolIdentifier,ulSymbolAddress));
      }
   else
      {
      // Attempt to Assign symbolic information to Invalid Record
      DLOG((LOGFILE,"\nUpdateDW2SymbolRecord -> Attempt to Assign symbolic information to Invalid Record"));
      return ERR_LSREC_INVALID_REC;
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: InitialiseWriteBuffer
     Engineer: Nikolay chokoev
        Input: TyApplicationType atApp
       Output: Error Code
  Description: Initialised the WriteBuffer
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::InitialiseWriteBuffer(void)
{
   unsigned long ulIndex;

   ptyApplicationArray[0].ulStartAddress=0x0;
   ptyApplicationArray[0].ulEndAddress  =0x0;
   ptyApplicationArray[0].bRecordValid  = FALSE;;

   // Finally nuke the record

   for (ulIndex=0;ulIndex<MAX_APPLICATION_SIZE;ulIndex++)
      ptyApplicationArray[0].pucDataRecord[ulIndex]=0x0;

   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: ProcessWriteBuffer
     Engineer: Nikolay chokoev   
        Input: TyApplicationType atApp
       Output: Error Code
  Description: Process the WriteBuffer (Send it to the Emulator)
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::ProcessWriteBuffer(unsigned long  ulNewLocation,
                                          unsigned long  **pulApp,
                                          unsigned long  *pulAppSize,
                                          unsigned long  *pulAppStartAddress)
{
   unsigned long ulIndex;
   unsigned long ulTempAddress;
   unsigned long ulTempData;
   unsigned long ulTempLength;
   // First Lets log the Buffer

   ulTempAddress = ulNewLocation; // Over-ride the start Address of the SREC
   ulTempLength  = ptyApplicationArray[0].ulEndAddress-ptyApplicationArray[0].ulStartAddress +1;
   DLOG((LOGFILE,"\n Write Buffer => Over-ride Start Address NEW: 0x%08X OLD: 0x%08X",ulTempAddress,ptyApplicationArray[0].ulStartAddress));
   DLOG((LOGFILE,"\n Write Buffer => Logging Write Buffer"));

   for (ulIndex = 0; ulIndex < ulTempLength; ulIndex+=4)
      {
      ulTempAddress = ulNewLocation + ulIndex;
      ulTempData = *(unsigned long *)((&ptyApplicationArray[0].pucDataRecord[ulIndex]));
      DLOG((LOGFILE,"\n Write Buffer => %08X\t:\t %08X",ulTempAddress,ulTempData));
	  NOREF(ulTempData);
      }

   DLOG((LOGFILE,"\n Write Buffer => Logging Write Buffer Complete"));

   ulTempAddress = ulNewLocation; // Over-ride the start Address of the SREC

   DLOG((LOGFILE,"\n Write Buffer => Now Pass it to Midlayer 0x%08X Length 0x%08X",ulTempAddress,ulTempLength));
//todo: allocate memory for app and assign it to the pointer here
//   ErrRet = TL_LoadDW2Application(ulTempAddress,
//                                  ptyApplicationArray[0].pucDataRecord,
//                                  ulTempLength,atApp);

   /* Allocate space for the application instructions */
//   *pulApp = (unsigned long *)malloc(ulTempLength);
   *pulApp = (unsigned long *)malloc(MAX_MIPS_DW2_APP_SIZE);   //todo: use the line below
                                                               //when the new optimised
                                                               // driver function is ready
                                                               // DL_OPXD_Mips_ExecuteDW2App()
   

   if (*pulApp == NULL)
      {
      DLOG((LOGFILE,"\n Write Buffer : Error Allocating Memory"));
      return ERR_LOADDW2_NOMEM;
      }

   /* Fill new memory pool with application data */
   memcpy(*pulApp, ptyApplicationArray[0].pucDataRecord, ulTempLength);

   /* Set application size (in instructions) */
   *pulAppSize = ulTempLength / sizeof(unsigned long);

   /* Set new application address if the pointer is valid
      Else the address assumed is 0xFF200200 */
   if(pulAppStartAddress != NULL)
      *pulAppStartAddress = ulTempAddress;

   return ERR_NO_ERROR;       //lint !e429
}


/****************************************************************************
     Function: ProcessSymbolBuffer
     Engineer: Nikolay Chokoev
        Input: TyApplicationType atApp
       Output: Error Code
  Description: Sends all the symbolic Information for an Application
               to the diskware or Midlayer
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
TyError CLoadSrecFile::ProcessSymbolBuffer(TySymbolIdentifier tySymbol,
                                           unsigned long  *pulInstrOffset)
{

   if(NULL == pulInstrOffset)
      return ERR_NO_ERROR; //No valid pointer. Nothing to do. 

   // First Log the symbols
   DLOG((LOGFILE,"\n ProcessSymbolBuffer => Logging Symbols Buffer"));

   if(tySymbol >= MAGIC_LAST_ENTRY)
      return 1; //todo error

   *pulInstrOffset = ptyApplicationArray[0].tySymbolArray[(unsigned int)tySymbol].ulSymbolAddress;
   DLOG((LOGFILE,
         "\n ProcessSymbolBuffer => Symbol Identifier%d\tAddress:0x%08X",
         (unsigned int)tySymbol,*pulInstrOffset));

   return ERR_NO_ERROR;
}







