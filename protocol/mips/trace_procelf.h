/****************************************************************************
		 Module:mdi
	  Engineer: Ranjith T.C.
  Description:Structures and variables nedded for ELF processing
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup
****************************************************************************/
#ifndef __TRACE_PROCELF_H_
#define __TRACE_PROCELF_H_
#include "../common/drvopxd.h"
//////////////////////////////////////////////////////////////
// 32 bit ELF Data Types as per Ref.1. Table 8 reproduced below
// -----------------------------------------------------------
// Name            Size   Alignment   Purpose
// -----------------------------------------------------------
// Elf32_Addr       4         4       Unsigned program address
// Elf32_Half       2         2       Unsigned medium integer
// Elf32_Off        4         4       Unsigned file offset
// Elf32_Sword      4         4       Signed large integer
// Elf32_Word       4         4       Unsigned large integer
// unsigned char    1         1       Unsigned small integer
//////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// Definitions and Structure that defines ELF Header as per Ref.1. //
/////////////////////////////////////////////////////////////////////
#define ELFCLASSNONE 0
#define ELFCLASS32   1
#define ELFCLASS64   2


#define ELFDATANONE  0
#define ELFDATA2LSB  1
#define ELFDATA2MSB  2

#define EI_MAG0      0
#define EI_MAG1      1
#define EI_MAG2      2
#define EI_MAG3      3
#define EI_CLASS     4
#define EI_DATA      5
#define EI_VERSION   6
#define EI_PAD       7
#define EI_NIDENT    16

#define ET_NONE      0           // No file type
#define ET_REL       1           // Relocatable file
#define ET_EXEC      2           // Executable file
#define ET_DYN       3           // Shared object file
#define ET_CORE      4           // Core file
#define ET_LOPROC    0xff00      // Processor-specific
#define ET_HIPROC    0xffff      // Processor-specific

/////////////////////////////////////////////////////////////////////////////
// Definitions and Structure that defines ELF Program Header as per Ref.1. //
/////////////////////////////////////////////////////////////////////////////
#define PT_NULL      0
#define PT_LOAD      1
#define PT_DYNAMIC   2
#define PT_INTERP    3
#define PT_NOTE      4
#define PT_SHLIB     5
#define PT_PHDR      6
#define PT_LOPROC    0x70000000
#define PT_HIPROC    0x7fffffff


typedef  unsigned int   Elf32_Addr;
typedef  unsigned short  Elf32_Half;
typedef  unsigned int   Elf32_Off;
typedef  int            Elf32_Sword;
typedef  unsigned int   Elf32_Word;



typedef struct
	{
	unsigned char  e_ident[EI_NIDENT];
	Elf32_Half     e_type;
	Elf32_Half     e_machine;
	Elf32_Word     e_version;
	Elf32_Addr     e_entry;
	Elf32_Off      e_phoff;
	Elf32_Off      e_shoff;
	Elf32_Word     e_flags;
	Elf32_Half     e_ehsize;
	Elf32_Half     e_phentsize;
	Elf32_Half     e_phnum;
	Elf32_Half     e_shentsize;
	Elf32_Half     e_shnum;
	Elf32_Half     e_shstrndx;
	} TyElf32_Ehdr;

typedef struct
	{
	Elf32_Word     p_type;
	Elf32_Off      p_offset;
	Elf32_Addr     p_vaddr;
	Elf32_Addr     p_paddr;
	Elf32_Word     p_filesz;
	Elf32_Word     p_memsz;
	Elf32_Word     p_flags;
	Elf32_Word     p_align;
	} TyElf32_Phdr;
typedef struct 
	{
	TyElf32_Ehdr tyElfHeader;								// ELF Header
	FILE* pInFile;											// Output File pointer (CSO file)
	int bBigEndian;											// TRUE if input file is in big-endian format
	unsigned int uEntryPoint;								// program entry point
	unsigned int uLowestTextAddress;						// lowest .text address referenced in A.OUT file
	unsigned int uTextOffset;				    			// file offset to .text section in A.OUT file
	unsigned int uTextLength;								// length of .text section in A.OUT file
	unsigned int uLowestDataAddress;			    		// lowest .data address referenced in A.OUT file
	unsigned int uDataOffset;								// file offset to .data section in A.OUT file
	unsigned int uDataLength;								// length of .data section in A.OUT file

	} TyElfProcessData;

typedef struct _tyElfAddressList
	{
	unsigned int ulStartAddress;
	unsigned int ulEndAddress;
	unsigned int ulMappedAddress;
	struct _tyElfAddressList *tyNextNode;
	}TyElfAddressList;

int ProcessELFFile(char *pcFileName, TyElfAddressList **ptyElfAddressList);
int ReadBytes(FILE *pFile, unsigned int ulFilePosition, unsigned char bSetFilePosition, void *pBuffer, unsigned int ulNumBytes);
int AddToList(TyElfAddressList **ptyElfAddressList,
				  unsigned int ulStartAddress,
				  unsigned int ulEndAddress,
				  unsigned int ulMappedAddress);
TyError DeInitELF(void);

#endif   // __TRACE_PROCELF_H_
