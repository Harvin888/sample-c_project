/***************************************************************************
       Module: message.h
     Engineer: Hugh O'Keeffe                            
  Description: PF message contants
Version        Date           Initials    Description
1.0.0          10-MAY-1994    HO'K        merged PF11 and PF51 versions
3.6.4-PF11     17-MAY-1994    HO'K        Merged COSBANK-MH14/04/94 updates
1.0.1-PFW11    12-JUL-1994    HO'K        Merged merrors.h/emulerrs.h/terrors.h
                                          trinterr.h/tgerr.h/parerr.h/txrx_ie.h
3.7.1-PF11     05-JUL-1994    GN          COCOBANK-GN10/06/94
                                          Banked coco error added.
                                          Coco memface errors added.
1.1.0-B        22-OCT-1994    TM/HO'K     PFW-COCO/Moved all internal errors in
                                          to this file.
1.1.0          28-OCT-1994    HO'K        Completely re-organised file to avoid
                                          duplicate errors numbers (hopefully).
                                          All internal error numbers should
                                          now be in this file.
3.7.3-PF11     27-JAN-1995    HO'K        SEABANK-GN-20-11-94 
1.2.0          22-MAY-1995    TM          PFW-SEA
0.9.0-PFW66    16-JUN-1995    HO'K        Added new errors, ini file settings.
0.9.0-PFW08    30-JUN-1995    PJS         Added new errors, config, mode
1.2.0-PFW11    20-JUL-1995    M.O.H       Added new errors, prom programming
0.9.1-SFAMF    07-SEP-1995    M.O.H       Added new errors, sfamf.
0.9.0-PFWXA    11-DEC-1995    HO'K/PJS    initial
0.9.0-PFWU51   03-FEB-1996    KA          Bank error messages
0.9.0-PFW77    08-FEB-1996    PJS         initial
1.0.1-PFWXA    27-MAR-1996    HO'K/PJS    initial
2.0.0-SFCM     27-JUN-1996    PJS         initial
1.1.4-PFWXA    09-SEP-1996    HO'K        added some messages
0.9.2-SFAMF    23-AUG-1996    M.O.H       Added new errors for SFAMF32/TYPE 2
0.0.1-SFIEEE   27-SEP-1996    PJS         initial
0.0.2-SFIEEE   18-OCT-1996    PJS         added messages
1.1.2-PFWU51   19-NOV-1996    HO'K        RTOS support
0.0.3-SFIEEE32 28-NOV-1996    PJS         added messages
1.1.4-PFWXA    17-JAN-1997    PJS         new errors for symbol relocation
1.1.4-PFWXA    25-FEB-1997    PJS         new errors for FPGA download
1.0.6-PFW77    07-Mar-1997    MOH         See BLK_MODE_20-JAN-1997_MOH.
1.1.5-PFWU51   15-MAY-1997    MOH         Added new error messages.
1.1.6-PFWU51   15-MAY-1997    MOH         Added new error messages.
1.1.7-PFWU51   17-JUN-1997    MOH         Remove 'Not Supported' error message.
1.1.4-PFWXA    07-JUL-1997    PJS         new errors for XA mapping
1.2.9-PFW11    10-SEP-1997    PJS         Call Stack Support
1.1.9-PFWU51   15-OCT-1997    KA          Enhanced variable window
1.1.5-PFWXA    03-NOV-1997    MOH         New errors for STARS
1.0.9-PFW77    17-NOV-1997    MOH         New errors for ULTRA77-STARS
0.9.0-PFW12    28-NOV-1997    PJS         initial
1.0.9-PFW77    17-NOV-1997    MOH         New errors for ULTRA77-STARS
1.0.0-KDI      21-JUL-1998    PJS         initial errors for KDI
1.1.0-PFWU66   09-AUG-1999    PJS         new errors for OKI enhancements
1.1.1-PFNTU66  01-NOV-1999    SC          Added new def for log file, 17011
1.1.1-PFNTU66  15-NOV-1999    SC          Added  STR_UNUSED_REG
1.4.3-PFNT51   01-DEC-1999    SC          Added internal error def  IE_TXRXCMD_16 
****************************************************************************/

#ifdef PFW_MFC
// These aren't messages, just constants
// Copied from the non PFW_MFC section
// Move to a different header file at later stage (when more emerge)
#define  CL_SYNTAX                                    1
#define  MID_BOX                                      2
#include "\Pathfndr\winmfc\resource\resource.h"
#else

/*
NOTE: 
If you are adding to this file then OBSERVE and use the existing layout.

   -   Search for 'unused' to find vacant numbers.
   -   Update 'unused' comments when you 'use' a number

Follow the following guidelines for internal error numbers:

#define  IE_XXXXXXXXNN                               ?????

XXXXXXXX: up to eight letters specifying module name
      NN: up to two digits, uniquely identifying error within module
   ?????: the actual error number as displayed to the user (< 31000).

In PFW, if a string is not found for an error number it is assumed to be an
internal error.

For error strings use:
#define  ERR_XXXXXXXX                                 ?????

For strings use:
#define  STR_XXXXXXXX                                 ?????

*/


/* From [00000] to [00099] */

#define  CL_LINE_MSG                                  0
#define  DWSUCCESS                                    0
#define  CL_SYNTAX                                    1
/*
E_START is not a message, its used to size message arrays for PF-DOS
(see msg.h)
*/
#define  E_START                                      1
#define  ILL_CHAR_E                                   1
#define  EOL_IN_STR_E                                 2
#define  MID_BOX                                      2
#define  CL_BOX                                       3
#define  STR_TOO_LONG_E                               3
#define  CL_COVER                                     4
#define  NUM_TOO_BIG_E                                4
#define  CL_BOX_SYNTAX                                5
#define  INV_CONST_CHAR_E                             5  
#define  ERR_STREAM                                   6
#define  INVALID_OMF_TYPE_E                           7

// MARTIN_SFAMF32
#define  TYPE1_INVALID_FOR_SFAMF32                    8
#define  TYPE2_INVALID_FOR_SFAMF                      9

// BLK_MODE_20-JAN-1997_MOH
#define  ERR_DW_CANNOT_ALLOC                          10

#define  COMMON_E                                     11
#define  ASH_SYN_ASM_E                                12
#define  ASH_PR_ASM_E                                 13
#define  ASH_AC_ASM_E                                 14
#define  ASH_SIM_ASM_E                                15
#define  ASH_COM_ASM_E                                16
#define  ASH_TAG_TX_E                                 17
#define  PR_ERR_OFFSET                                18
#define  INCORRECT_LOCID                              19
#define  MEM_MALLOC_C                                 20
#define  DWNOMEMORY                                   MEM_MALLOC_C
#define  DWDATAERR                                    MEM_MALLOC_C
#define  DWFILERR                                     MEM_MALLOC_C
#define  DWINUSE                                      MEM_MALLOC_C
#define  DWINVPAR                                     MEM_MALLOC_C
#define  DWINVWIND                                    MEM_MALLOC_C
#define  DWITEMLIST                                   MEM_MALLOC_C
#define  DWMAXWINDOWS                                 MEM_MALLOC_C
#define  DWNOCURRENT                                  MEM_MALLOC_C
#define  DWNOEGAINS                                   MEM_MALLOC_C
#define  DWNOROOMFORFRAME                             MEM_MALLOC_C
#define  DWNOTHRCRT                                   MEM_MALLOC_C
#define  DWNOTSETUP                                   MEM_MALLOC_C
#define  DWNOTVISIBLE                                 MEM_MALLOC_C
#define  DWNOWINDOW                                   MEM_MALLOC_C
#define  DWOFFSCREEN                                  MEM_MALLOC_C
#define  DWUSERABORT                                  MEM_MALLOC_C
#define  IE_DWIN_DWIN_CREATE_1                        MEM_MALLOC_C
#define  SW_PHYS_WIN_TOO_SMALL                        MEM_MALLOC_C
#define  MEM_CALLOC_C                                 21
#define  NO_MEM_ALLOC_C                               22
#define  CANNOT_FREE_NULL_PTR_C                       23
#define  INCORRECT_BYTECOUNT_C                        24
#define  FREE_DYN_LIST_C                              25
#define  CANNOT_REALLOC_NULL_PTR_C                    26
#define  MEM_REALLOC_C                                27
#define  NULL_PTR_PASSED_E                            28
#define  MEM_OVERWRITE                                29
#define  INIT_MEM_LIST_FAIL                           30
#define  NOT_REALLOCATING_BAD_BLOCK                   31
#define  EMRGNCY_DUMP_TO_FILE                         32
#define  DEBUG_NOT_ON                                 33
#define  ERR_OPEN_FILE_C                              34
#define  RX_BAD_BYTE_COUNT                            35
#define  NO_MOD_LIST_C                                36
#define  ABS_FMT_NOT_OK_C                             37
#define  SINGLE_BP_DISPLAY                            38
#define  STR_PC_VAL                                   39
#define  KEY_NOT_FUNCTION_KEY                         40
#define  KEY_FUNCTION_NOT_EXECUTED                    41
#define  KEY_SYSTEM_CURRENTLY_ACTIVE                  42
#define  INVALID_KEY_STRING_E                         43
#define  SLASH_IN_KEY_STRING_EXPECTED_E               44
#define  ERR_CLOSE_FILE_C                             45
#define  STR_BP_OPCODE                                46   
#define  STR_BP_DATA_WR                               47
#define  STR_BP_DATA_RD                               48
#define  STR_BP_ALL                                   49
#define  STR_DISP_BP                                  50
#define  NO_EDIT_WINDOW_C                             51
#define  INVALID_SPS_ARG_C                            52
#define  STR_SET_BP                                   53
#define  STR_CLR_BP                                   54
#define  STR_WR_PC                                    55
#define  STR_HEX                                      56
#define  STR_ASC                                      57
#define  STR_DEC                                      58
#define  STR_OCT                                      59
#define  F_TELL_FILE                                  60
#define  F_CANNOT_FIND_READ_FILE                      61
#define  F_WRITE_PROT_FILE                            62
#define  F_CANNOT_OPEN_FILE                           63
#define  F_ERROR_READING_FILE                         64
#define  F_NULL_FILE                                  65
#define  F_ERROR_WRITING_FILE                         66
#define  F_CLOSE_FILE                                 67
#define  F_CLOSE_ALL_FILES                            68
#define  F_FILE_ERROR                                 69
#define  F_FLUSH_ERROR                                70
#define  F_SEEK_ERROR                                 71
#define  F_INVALID_NUMBER_TO_GET                      72
#define  F_GET_STRING                                 73
#define  FILE_STATUS_INVALID                          74
#define  ERR_IN_CHANGING_DIR                          75
#define  DIRECTORY_FETCH_E                            76
#define  F_TEMPFILE_WRITE_ERROR                       77
#define  F_TEMPFILE_READ_ERROR                        78
#define  CANNOT_CREATE_FILE                           79
#define  F_E2BIG                                      80
#define  F_EACCES                                     81
#define  F_EBADF                                      82
#define  F_EDEADLOCK                                  83
#define  F_EDOM                                       84
#define  F_EEXIST                                     85
#define  F_EINVAL                                     86
#define  F_EMFILE                                     87
#define  F_ENOENT                                     88
#define  F_ENOEXEC                                    89
#define  F_ENOMEM                                     90
#define  F_ENOSPC                                     91
#define  F_ERANGE                                     92
#define  F_EXDEV                                      93
#define  STR_BIN                                      94
#define  NO_TEMP_BP_SET                               95
#define  STR_IP_BASE                                  96
#define  STR_OP_BASE                                  97
#define  STR_CIP_BASE                                 98
#define  ERROR_SAVING_FILE                            99

/* From [00100] to [00199] */

#define  MAX_CMSG_NUM                                 100
#define  NUM_EV_E                                     101
#define  RNGE_EV_E                                    102
#define  EXPR_EV_E                                    103
#define  REG_BANK_NO_E                                104
#define  PORT_NO_E                                    105
#define  TX_SEQUENCE_E                                106
#define  ADRS_SIZ_E                                   107
#define  CMD_INVALID_IN_DDC_MODE_E                    108
#define  TOO_MANY_GR_PARAMETERS_E                     109
#define  INV_PROMPT_STR_E                             110
#define  INV_FN_KEY_STR_E                             111
#define  NO_CLOCK_B_E                                 112
#define  NO_ASCII_INPUT_BASE                          113
#define  PORT_1_OR_2_E                                114
#define  PORT_NOT_E                                   115
#define  TOO_MUCH_STACK_E                             116
#define  NOT_BYTE_ER                                  117
#define  FILENAME_EXPT_ER                             118
#define  INVALID_PROM_TYP                             119
#define  INVALID_SP_VALUE                             120
#define  NO_HELP_SCREEN_E                             121
#define  P80515_COM_ONLY_E                            122
#define  TX_SEQ_TOO_BIG                               123
#define  TOO_MANY_SUB_PARAMETERS_E                    124
#define  RPAR_COMMA_EXPECTED_E                        125
#define  ERR_LOOPBACK_CREATE                          126
#define  P8044_COM_ONLY_E                             127
#define  NO_SUCH_OPCODE                               128
#define  PC_NOT_AT_OPCODE                             129
#define  COM_ERR_IN_STEP                              130
#define  BPS_ARE_DISABLED                             131
#define  MODE_INVALID_FOR_ROMLESS                     132
#define  NO_SOURCE_AVAILABLE_E                        133
#define  NOT_A_SOURCE_LINE_ERR                        134
#define  NUMBER_TOO_LARGE                             135
#define  EXTRA_CHARS_IN_STRING                        136
#define  INVALID_CHAR_IN_STRING                       137
#define  NO_NUMBER_ENTERED                            138
#define  STEP_SIZE_ZERO_NOT_ALLOWED_E                 139
#define  PATH_TOO_LONG                                140
#define  CODE_BP_MUST_BE_ENAB                         141
#define  PATHFINDER_FEATURE_ONLY                      142
#define  COMMAND_ILLEGAL_IN_ILA                       143
#define  BOX_VALID_FOR_ROMLESS_ONLY                   144
#define  NO_MEM_FOR_SAVE_E                            145
#define  EXPR_EXPTED                                  146
#define  TOO_MUCH_CODE_FOR_HLL_STEP                   147
#define  PC_SCOPE_NEQ_TO_AUTO_SYM_SCOPE               148
#define  MONITOR_OFFSET_TOO_BIG_E                     149
#define  BANKWINDOW_1_OR_2_E                          150
#define  INVALID_BANKMAP_E                            151
#define  INVALID_BANKNUMB_E                           152
#define  BANKMAP_NOT_INIT_E                           153
#define  BANKPORT_NOT_INIT_E                          154
#define  STR_COP_BASE                                 155
#define  STR_CLK                                      156
#define  STR_CLK_FREQ                                 157
#define  STR_CLK_WARN                                 158
#define  STR_MAP_TARG                                 159
/* DW_ERRS_STRT is not a message */
#define  DW_ERRS_STRT                                 160
#define  COMM_DW_REP                                  160
#define  STR_MAP_EMUL                                 161
#define  STR_MAP_MEM                                  162
#define  STR_CODE_MEM                                 163
#define  STR_DATA_MEM                                 164
#define  STR_MEM_WR                                   165
#define  COM_3_4_OLDCOMS                              166
#define  STR_MAP_FROM_TO                              167
#define  BANKED_STR_MAP_FROM_TO                       168

#define  STR_PORT                                     169
#define  STR_MAP_ONCHIP                               170
#define  STR_RANGE_WARN_MSG                           171
#define  STR_TIME_DATE                                172
#define  STR_EXPR                                     173
#define  STR_WINUPDATE                                174
#define  STR_ON                                       175
#define  STR_OFF                                      176
#define  STR_AUTOMOD                                  177
#define  STR_LOG                                      178
#define  STR_LOG_FILE_OPEN                            179
#define  STR_LOG_FILE_CLOSED                          180
#define  WINUPDATE_ONLY_GRFILE                        181
#define  STR_WRITECHECK                               182
#define  STR_PFVER                                    183
#define  STR_HIGHLEVEL                                184
#define  STR_DWVER1                                   185
#define  STR_FWVER                                    186
#define  STR_VERTITLE                                 187
#define  STR_MAP_WARN_MSG                             188
#define  STR_DWVER2                                   189
#define  STR_COB                                      190
#define  STR_EMUL_ENTRY_TIME                          191
#define  STR_EXE_TIME                                 192
#define  STR_NOT_PRESENT                              193
#define  STR_VCC                                      194
#define  DW_ERRS_END                                  195
#define  STR_PRESENT                                  196
#define  SEA_NO_COLON_E                               197   
#define  SEA_BAD_BIN_DATA_E                           198   
#define  SEA_NO_OPTION_E                              199   


/* From [00200] to [00299] */

#define  SEA_OPTION_NOT_SETUP_E                       200   
#define  SEA_ADDRESS_ALREADY_USED_E                   201   
#define  SEA_FIFO_OVERFLOW_E                          202   
#define  TRIG_DOES_NOT_MATCH_SETUP                    203
#define  NO_END_ADDR_OR_ST_TRIG_IN_BLK                204
#define  TR_QUAL_SAYS_HEX_ONLY_E                      205
#define  CANNOT_LOAD_OLD_TDF                          206
#define  STR_NEXT_INST                                207
#define  ERR_OPENING_TDF_FILE_FOR_WRITE               208
#define  ERR_BAD_DIR_IN_CASE                          209
#define  ERR_BAD_WIN_IN_DTRHA                         210
#define  ERR_BAD_DIR_IN_DTRHA                         211
#define  ERR_BAD_DIR_IN_DHEXLINE                      212
#define  ERR_BAD_DIR_IN_DACTLINE                      213
#define  ERR_BAD_WIN_IN_TRHADISKDISP                  214
#define  ERR_WTRDISK_NUM                              215
#define  ERR_BAD_DIR_DTRHACUR                         216
#define  ERR_BAD_WIN_DTRHACUR                         217
#define  ERR_BAD_WIN_HACURDISP                        218
#define  ERR_BAD_LEN_HACURDISP                        219
#define  ERR_BAD_DIR_HACURDISP                        220
#define  ERR_BAD_LEN_HASRNDISP                        221
#define  ERR_BAD_WIN_HADISP                           222
#define  ERR_BAD_DIR_HADISP                           223
#define  ERR_BAD_WIN_FINHASRANGE                      224
#define  ERR_BAD_WIN_HALINEIND                        225
#define  ERR_BAD_WIN_INITTRHALPS                      226
#define  ERR_BAD_WIN_WHEREISHALINE                    227
#define  ERR_BAD_LEN_HAABSDISP                        228
#define  ERR_BAD_DIR_HAABSDISP                        229
#define  ERR_BAD_WINPOS_HAABSDISP                     230
#define  ERR_BAD_WIN_PHAUP                            231
#define  ERR_BAD_WIN_PHADN                            232
#define  ERR_BAD_WIN_HASRNDISP                        233
#define  ERR_BAD_WIN_HAABSDISP                        234
#define  ERR_BAD_WIN_HAPPINLDN                        235
#define  ERR_BAD_WIN_HAPPINLUP                        236
#define  ERR_BAD_DIR_CDISP                            237
#define  ERR_BAD_LEN_CSRNDISP                         238
#define  ERR_BAD_WINPOS_CABSDISP                      239
#define  ERR_CLOSING_TDF_FILE                         240
#define  ERR_RX_RET_E                                 241
#define  ERR_CLOSING_TDF_WRITE                        242
#define  ERR_INVALID_TDF1                             243
#define  ERR_READING_TDF2                             244
#define  ERR_INVALID_TDF2                             245
#define  ERR_WRITING_TDF2                             246
#define  ERR_OPENING_CMP_FILE                         247
#define  ERR_OPENING_TCF_FILE                         248
#define  ERR_BAD_TDF_VER                              249
#define  ERR_OPENING_TDF_READ                         250
#define  ERR_DIFF_NO_TR_FRAMES                        251
#define  ERR_TRIG_POS_DIFF                            252
#define  ERR_TR_QUALS_DIFF                            253
#define  ERR_A_TRIG_FR_DIFF                           254
#define  ERR_TR_STOP_DIFF                             255
#define  EMPTY_M                                      256
#define  ERR_TR_MODE_DIFF                             257
#define  ERR_TR_LINES_DIFF                            258
#define  ERR_TR_SYNC_DIFF                             259
#define  ERR_TR_PROC_DIFF                             260
#define  ERR_DIFF_NO_TR_BLOCKS                        261
#define  ERR_DIFF_TR_BLK_SIZE                         262
#define  STR_COB_UNKNOWN                              263
#define  ERR_OPENING_TRDISK_FILE                      264
#define  ERR_CLOSING_TRDISK_FILE                      265
#define  STR_REGISTERS                                266
#define  ERR_TRJMPADDR_NOT_FOUND                      267
#define  ERR_INVALID_FRAME                            268
#define  TRPROC_NOT_SUPPORTED                         269
#define  ATTEMPT_READ_PASSED_BUFFER                   270
#define  TD_SCREEN_EMPTY_E                            271
#define  WERR_HW_DIFFERENT                            272
#define  ERR_NO_TRIG_TRJMPTRIG                        273
#define  INVALID_FILE_FORMAT_E                        274
#define  NO_SOURCE                                    275
#define  START_OF_TRACE                               276
#define  END_OF_TRACE                                 277
#define  BLOCK_DOES_NOT_EXIST                         278
#define  STR_LOSS_OF_CLOCK                            279
#define  STR_TARGET_PROC_RST                          280
#define  STR_TARGET_PROC_EXE_HALT                     281
#define  STR_TRACE_BUF_FULL                           282
#define  STR_STOP_TRIGGER                             283
#define  STR_TIMER_EXP                                284
#define  STR_OPCODE_BP                                285
#define  STR_DATA_RD_BP                               286
#define  STR_DATA_WR_BP                               287
#define  STR_EXT_HALT                                 288
#define  STR_USER_HALT                                289
#define  STR_SINGLE_STEP                              290
#define  ERR_INVALID_BASEPTR                          291
#define  ERR_EVENT_COUNT_TO_LARGE                     292
#define  ERR_MAP_ONCHIP_RAM                           293
#define  ERR_MAP_ONCHIP_ROM                           294
#define  INVALID_BASEPTR                              295
#define  ERR_BAD_WIN_UPDTRSCNTK                       296
#define  ERR_BAD_WIN_TRJMPTRIG                        297
#define  ERR_BAD_WIN_TRJMPADDR                        298
#define  ERR_USER_ESC                                 299

/* From [00300] to [00399] */

#define  C_PROM_WR_E                                  300
#define  C_WR_E                                       301
#define  C_ADRS_E                                     302
#define  C_INV_BP_E_D_E                               303
#define  C_EPNR_E                                     304
#define  C_INT_GONE_E                                 305
#define  C_TX_REP_CMD_E                               306
#define  C_INV_CMD_E                                  307
#define  C_TIM_OVF_E                                  308
#define  C_INV_TIMER_E                                309
#define  C_INCOMP_FIRMWARE_E                          310
#define  STR_DWVER3                                   311
#define  C_INV_MAP_E                                  312
#define  C_INV_TR_MDE_E                               313
#define  C_INV_PROM_CDE_E                             314
#define  C_INV_CHK_E                                  315
#define  C_INV_R_W_CMD_E                              316
#define  C_INV_DEST_E                                 317
#define  C_BM_R_W_E                                   318
#define  C_BM_LEN_E                                   319
#define  C_INV_SRCE_E                                 320
#define  C_INV_PRM_SRCE_E                             321
#define  INVALID_DATA_READ_EVENT                      322
#define  C_S_C_BP_CMD_E                               323
#define  C_INV_SG_CBP_E                               324
#define  C_INV_R_BP_CMD_E                             325
#define  C_T_CLK_ABS_E                                326
#define  C_EPNR2                                      327
#define  C_SIU_W_E                                    328
#define  C_INV_SG_ST_E                                329
#define  C_TST_FAIL_E                                 330
#define  C_FEAT_NOT_SUP_E                             331
#define  C_DWARE_DOESNT_SUP_E                         332
#define  CANNOT_LOG_TRACE_ERR                         333
#define  C_INV_BAUD_E                                 334
/* MOH_STARS_97_77 */
#define  NO_TRACE_ENABLE_FILE                         335
#define  C_INSUFF_CTRL_MEM_E                          336
#define  C_60K_RW_E                                   337
#define  IE_TXRX_01                                   338
#define  IE_TXRX_02                                   339
#define  GIVEN_SLOT1_UNAVIL                           340
#define  GIVEN_SLOT2_UNAVIL                           341
#define  NO_COM_PORT_AVAIL                            342
#define  ERROR_LOAD_DISKWARE                          343
#define  ERR_SETTING_BP                               344
#define  ERR_SETTING_OPCODE_BP                        345   
#define  ERR_SETTING_DATA_RD_BP                       346   
#define  ERR_SETTING_DATA_WR_BP                       347   
#define  ERR_CANNOT_RELOAD_DW1                        348
#define  FILE_NAME_EXP                                349
#define  IO_ERR_START                                 350
#define  HARD_DATA_ERROR                              351
#define  INV_DATA_SPEC                                352
#define  OP_ERROR                                     353
#define  INT_ERR                                      354
#define  DEV_OFF_LINE                                 355
#define  FILE_OFF_LINE                                356
#define  BAD_FILE_NAME                                357
#define  NO_ROOM                                      358
#define  UNK_DEV                                      359
#define  NO_FILE                                      360
#define  FILE_PROT                                    361
#define  FILE_USE                                     362
#define  FILE_N_OPEN                                  363
#define  DATA_FOR                                     364
#define  LINE_LONG                                    365
#define  INV_COM_SPEC                                 366
#define  NO_MEM                                       367
#define  COMM_ENV_TOO_LONG_E                          368
#define  ERR_INVALID_BAUDRATE                         369
#define  ERR_TRANSFER_MODE                            370
#define  ERR_EMULATION_MODE                           371
#define  ERR_REG_NOT_SUPPORTED                        372
#define  STR_SERVICE_MODE                             373
#define  RANGE_ERROR_E                                374
#define  NO_LIR_BPTS_IN_CTF_E                         375
#define  STR_NO_BP_FND                                376
#define  STR_ER_BP                                    377
#define  STR_ENABLED                                  378
#define  STR_DISABLED                                 379
#define  RD_OUTSIDE_CODE_MEM_E                        380
#define  RD_OUTSIDE_XDATA_MEM_E                       381
#define  RD_OUTSIDE_RAM_MEM_E                         382
#define  RD_OUTSIDE_BIT_MEM_E                         383
#define  RD_OUTSIDE_SFR_MEM_E                         384
#define  WR_OUTSIDE_CODE_MEM_E                        385
#define  WR_OUTSIDE_XDATA_MEM_E                       386
#define  WR_OUTSIDE_RAM_MEM_E                         387
#define  WR_OUTSIDE_BIT_MEM_E                         388
#define  WR_OUTSIDE_SFR_MEM_E                         389
#define  ONLY_STATIC_ALLOWED_E                        390
#define  MAX_CMO_ALLOWED_E                            391
#define  NOT_SAME_CMO_PAGE_E                          392
#define  NO_CMO_IN_CTF_E                              393
#define  C_NO_RXED_E                                  394
#define  C_NO_TX_E                                    395
#define  EMUL_RESET_E                                 396
#define  C_TARGET_BRD_FAULT                           397
#define  STR_BP_FROM_TO                               398
#define  STR_BP_AT                                    399
/* From [00400] to [00499] */

#define  SY_INIT_ERR_E                                400
#define  MD_INIT_ERR_E                                401
#define  SY_STORE_ERR_E                               402
#define  MD_STORE_ERR_E                               403
#define  SY_NO_SYM_ERR_E                              404
#define  MD_NO_MOD_ERR_E                              405
#define  SYM_NAME_E                                   406
#define  PATH_FROM_ROOT_E                             407
#define  REC_FMT_INVALID_E                            408
#define  DEF_RES_SYM_E                                409
#define  MOD_NOT_EXIST_E                              410
#define  NO_MEM_FOR_SYMBOLS_E                         411
#define  NO_MEM_FOR_MODULES_E                         412
#define  SYM_NOT_DEFINED_E                            413
#define  NO_PLM_STMT_E                                414
#define  NOT_A_DISA_ROW_E                             415
#define  SYM_DATABASE_CORRUPT_E                       416
#define  NO_CODE_FOR_STMT_E                           417
#define  PRE_DEFINED_CHANGE_E                         418
#define  CANT_DEL_RES_SYM_E                           419
#define  NOT_ENOUGH_SPACE_FOR_SYMBOLS_E               420
#define  SYM_FILE_OPEN_ERROR                          421
#define  NO_MORE_SPACE_TO_OVERFLOW_E                  422
#define  SYMBOLS_RANGE_OVERLAP_E                      423
#define  NO_PREDEFINED_SYMBOLS_LOADED                 424
#define  SYM_FILE_WRITE_PROTECTED_E                   425
#define  ERR_OPEN_COMM_FAIL                           426
#define  ERR_COMM_IN_USE                              427
#define  IE_PROMMENU_00                               428 
#define  IE_PROMMENU_01                               429 
#define  IE_PROMMENU_02                               430 
#define  IE_PROMMENU_03                               431 
#define  IE_PROMMENU_04                               432 
#define  IE_PROMMENU_05                               433 
#define  IE_PROMMENU_06                               434 
#define  IE_PROMMENU_07                               435 
#define  IE_PROMSECU_00                               436 
#define  IE_PROMMAIN_00                               437 
#define  IE_PROMMENU_08                               438 
#define  IE_PROMUTIL_00                               439 
#define  IE_PROMUTIL_01                               440 
#define  IE_PROMUTIL_02                               441 
#define  STR_XDATA_MEM                                442
#define  STR_RAM_MEM                                  443
#define  STR_SFR_MEM                                  444
#define  STR_FRAMES_PER_BLOCK                         445
#define  STR_BANK_CODE_MEM                            446
#define  STR_BANK_CODEBP_MEM                          447
#define  STR_RELOAD_USER_PROG                         448
#define  ERR_CANNOT_SEARCH_FILE                       449

#define  GROUP_FILE_ONLY_E                            450
#define  GROUP_FILE_OPEN_E                            451
#define  GROUP_FILE_FGETS_E                           452
#define  GROUP_FILE_FTELL_E                           453
#define  GROUP_FILE_FSEEK_E                           454
#define  GROUP_FILE_MAX_NEST                          455
#define  MATCHING_DECLARE_E                           456
#define  MATCHING_IIF_E                               457
#define  MATCHING_EELSE_E                             458
#define  MATCHING_WHILE_E                             459
#define  MISSING_EENDIF_E                             460
#define  MISSING_WEND_E                               461
#define  NESTING_DECLARE_E                            462
#define  TOO_MANY_WHILE_BLKS_E                        463
#define  TOO_MANY_IF_BLKS_E                           464
#define  SWAP_FILE_FOPEN_E                            465
#define  SWAP_FILE_FPUTC_E                            466
#define  SWAP_FILE_FGETC_E                            467
#define  SWAP_FILE_FCLOSE_E                           468
#define  SWAP_FILE_REMOVE_E                           469
#define  SWAP_FILE_FSEEK_E                            470
#define  MAX_MACRO_GLOBALS_REACHED                    471
#define  MAX_MACRO_PARAMS_REACHED                     472
#define  MAX_MACRO_LOCAL_REACHED                      473
#define  VARIABLE_DEFINED_ALREADY                     474
#define  DECLARE_BLOCK_ONLY_E                         475
#define  UNEXPECTED_MACRO_TOKEN_E                     476
#define  SIMPLE_TYPE_EXPECTED                         477
#define  MACRO_DEST_STRING_EXPECTED_E                 478
#define  RESULT_STRING_TOO_BIG_E                      479
#define  NUMERIC_CONSTANT_EXPECTED_E                  480
#define  RESULT_BINARY_TOO_BIG_E                      481
#define  UNKNOWN_TYPE_FOR_NUMBER                      482
#define  MACRO_VARIABLE_EXPECTED_E                    483
#define  LOCATE_ROW_NUMBER_TOO_BIG_E                  484
#define  LOCATE_COL_NUMBER_TOO_BIG_E                  485
#define  MACRO_OUT_FILE_ALREADY_OPEN_E                486
#define  MACRO_OUT_FILE_FOPEN_E                       487
#define  MACRO_OUT_FILE_NOT_OPEN_E                    488
#define  MACRO_OUT_FILE_FCLOSE_E                      489
#define  MACRO_OUT_FILE_WRITE_E                       490
#define  STR_DSP_MAP_WARN_MSG                         491
#define  STR_BLOCK_2                                  492
#define  STR_BLOCK_32                                 493
#define  STR_BLOCK_512                                494
#define  STR_CONTINUOUS                               495
#define  ERR_LOADING_DW                               496
#define  STR_RD_BP_INFO                               497
#define  STR_INTERNAL_ACC                             498
#define  STR_EXTERNAL_ACC                             499

/* From [00500] to [00599] */

#define  NO_COMMA_E                                   500
#define  A_OR_B_EXPT                                  501
#define  X_OR_Y_EXPT                                  502
#define  EXPR_X_Y_HASH_EXPT                           503
#define  A_B_EXPR_HASH_X_Y_EXPT                       504
#define  EXPR_X_Y_EXPT                                505
#define  EXPR_EXPT                                    506
#define  DEST_OUT_OF_RNG                              507
#define  VAL_NOT_IN_BYTE                              508
#define  NO_R0__R1_E                                  509
#define  NO_R0__R7_E                                  510
#define  NO_A_E                                       511
#define  NO_C_E                                       512
#define  NO_AB_E                                      513
#define  NO_PLUS_E                                    514
#define  NO_DPTR_E                                    515
#define  NO_DPTR_PC_E                                 516
#define  NO_A_AT_E                                    517
#define  NO_R0_R1_DPTR_E                              518
#define  NO_EXP_E                                     519
#define  NON_BIT_ADDR_E                               520
#define  BAD_BIT_OFFSET_E                             521
#define  NOT_BYTE_E                                   522
#define  NON_IN_BLOCK_E                               523
#define  NON_REL_E                                    524
#define  NON_CODE_SEG_E                               525
#define  NON_RAM_SFR_SEG_E                            526
#define  NON_BIT_SEG_E                                527
#define  WRONG_SYNTAX_E                               528
#define  SYNTAX_E                                     529
#define  NO_HASH_R0__R7_AT_EXP_E                      530
#define  NO_DIV_EXP_E                                 531
#define  NO_A_HASH_E                                  532
#define  NO_A_AT_R0__R7_E                             533
#define  NO_HASH_EXP_E                                534
#define  NO_AT_A_C_EXP_DPTR_R0__R7_E                  535
#define  NO_HASH_A_EXP_E                              536
#define  NO_AT_A_C_EXP_R0__R7_HASH_E                  537
#define  NO_C_EXP_E                                   538
#define  NO_AT_R0__R7_EXP_E                           539
#define  NO_A_EXP_E                                   540
#define  DATA_AFTER_INSTR_E                           541
#define  STR_A_EVENT                                  542
#define  STR_B_EVENT                                  543
#define  STR_C_EVENT                                  544
#define  STR_D_EVENT                                  545
#define  STR_NOT_A_EVENT                              546
#define  STR_NOT_B_EVENT                              547
#define  STR_NOT_C_EVENT                              548
#define  STR_NOT_D_EVENT                              549
#define  STR_ALWAYS_START_TRG                         550
#define  NO_A_C_EXP_E                                 551
#define  NO_A_R0__R7_AT_EXP_E                         552
#define  NO_R0__R7_EXP_E                              553
#define  NO_AT_A_DPTR_R0__R7_EXP_E                    554
#define  NO_AT_EXP_E                                  555
#define  STR_STOP_TRG_DISABLED                        556
#define  NO_HASH_E                                    557
#define  NO_AT_E                                      558
#define  STR_THEN                                     559
#define  STR_OR                                       560
#define  STR_AND                                      561
#define  STR_TRIG_START_STOP                          562
#define  STR_PSEN                                     563
#define  STR_RD_CYCLE                                 564
#define  STR_WR_CYCLE                                 565
#define  STR_RD_OR_WR_CYCLE                           566
#define  STR_STOP_TRACE_CNT                           567
#define  STR_ON_EMUL_HALT                             568
#define  STR_ON_STOP_TRIG                             569
#define  STR_WHEN_TRACE_BUF_FULL                      570
#define  NO_LOG_FILE_OPEN_E                           571
#define  ERR_DW_NOT_LOADED                            572
#define  STR_CODE_EXE                                 573
#define  NO_COMMA_EXP_E                               574   
#define  NO_X_EXP_E                                   575   
#define  NO_X_SP_EXP_E                                576   
#define  NO_X_HASH_EXP_E                              577   
#define  NO_BIT_0_7_E                                 578   
#define  NO_HASH_COMMA_EXP_E                          579   
#define  NO_X_E                                       580   
#define  NO_X_SP_E                                    581   
#define  ERR_CPU_NOT_SPEC                             582
#define  ERR_MAX_BIT_ADDRESS                          583
#define  ERR_INVALID_FOR_BIT_SYM                      584
#define  STR_IDENT_SIGS                               585
#define  STR_E_EVENT                                  586
#define  STR_F_EVENT                                  587
#define  STR_NOT_E_EVENT                              588
#define  STR_NOT_F_EVENT                              589
#define  STR_ONLY                                     590
#define  STR_BLOCK_4                                  591
#define  STR_BLOCK_8                                  592
#define  STR_BLOCK_16                                 593
#define  STR_BLOCK_64                                 594
#define  STR_BLOCK_128                                595
#define  STR_XDATA_TRACED                             596
#define  STR_PROC_MODE                                597
#define  STR_TARGET_PROC_POWER_DOWN                   598
#define  STR_TARGET_PROC_IDLE                         599

/* From [00600] to [00699] */

#define  NO_PORT_E                                    600
#define  CMD_BUF_SIZE_E                               601
#define  TOO_MANY_WINS_E                              602
#define  CANNOT_LOAD_SPS_E                            603
#define  FILE_LOAD_BOX_E                              604
#define  NO_COLS_MENU_IN_MONO_E                       605
#define  NOT_IN_WIN_MODE_E                            606
#define  WINS_ALREADY_ENABLED_E                       607
#define  WINS_ALREADY_DISABLED_E                      608
#define  CANNOT_RESTORE_SCREEN_E                      609
#define  DEF_EXT_TOO_LONG_E                           610
#define  TOP_WIN_NOT_CODE_E                           611
#define  NO_LIST_FILE_TO_DISPLAY_E                    612
#define  CODE_WIN_UPDATE_E                            613
#define  LINE_NOT_EXECUTABLE_E                        614
#define  WINS_INIT_E                                  615
#define  INVALID_LOAD_FILE_FORMAT                     616
#define  NO_EOF_MARKER                                617
#define  BAD_VERSION_OF_CONFIG_FILE                   618
#define  CANNOT_OPEN_WIN_IN_DDC_E                     619
#define  INVALID_DWIN_EXPR                            620
#define  NOT_A_DATA_DISPLAY_WIN_E                     621
#define  NO_MEM_TO_OPEN_WIN_E                         622
#define  ENV_DECIMAL_NUM_TOO_BIG_E                    623
#define  ENV_DECIMAL_NUM_BAD_CHARS                    624
#define  SYM_ENV_PATH_NAME_INVALID_E                  625
#define  WRONG_HASH                                   626
#define  ERR_INVALID_MASK                             627
#define  STR_NO_EXIT_FOUND                            628
#define  STR_ENTRY_EQUAL_EXIT                         629
#define  INVALID_SYMBOLS_FILE                         630
#define  UNEXPECTED_END_OF_SYMBOLS_FILE               631
#define  F_CANNOT_FIND_SYMBOLS_FILE                   632
#define  SYM_FILE_CHANGED_DURING_DOS                  633
#define  INVALID_SYMBUF_VERSION                       634
#define  INVALID_SYMBUF_CHECKSUM                      635
#define  SYMBOLS_BUFF_IS_CORRUPT                      636
#define  FAMILY_CFG_FILE_CORRUPT                      637
#define  FAMILY_CFG_NOT_FOUND                         638
#define  INVALID_CONFIGURATION_FILE                   639
#define  CANNOT_LOAD_HC11_E                           640
#define  PROBE_CFGFILE_MISMATCH                       641
#define  CFG_FILE_CORRUPT                             642
#define  ERR_INVALID_EVENT_SPEC                       643


// PROMS-MARTIN_H-19-JUL-1995
#define  STR_PROM_SIZE                                644
#define  TITLE_PROM_SIZE                              645
#define  STR_EEPROM_SIZE                              646
#define  TITLE_EEPROM_SIZE                            647
#define  TITLE_PAGE_NUMBER                            648
#define  STR_PAGE_NUMBER                              649
#define  TITLE_PROM_PAGE                              650
#define  STR_PROM_PAGE                                651
#define  TITLE_CHECK_RESULT                           652
#define  STR_CHECK_RESULT                             653
#define  TITLE_BLANK_RESULT                           654
#define  STR_BLANK_RESULT                             655
#define  STR_YES_NO                                   656
#define  STR_YES_NO_YN                                657
#define  STR_PROG_ABORT                               658
#define  STR_PROG_BLANK                               659
#define  STR_PROG_NON_BLANK                           660
#define  TITLE_PROG_RESULT                            661
#define  STR_PROG_BAD                                 662
#define  STR_PROG_OK                                  663
#define  TITLE_VERIFY_RESULT                          664
#define  STR_VERIFY_BAD                               665
#define  STR_VERIFY_OK                                666
#define  TITLE_READ_RESULT                            667
#define  STR_READ_OK                                  668
#define  STR_READ_PROGRESS                            669
#define  TITLE_PAGE_RESULT                            670
#define  STR_PAGE_BAD                                 671
#define  STR_NON_SEC_PROM                             672
#define  TITLE_SECURE_RESULT                          673
#define  STR_SECURE_OK                                674
#define  STR_SECURE_BAD                               675
#define  STR_PROMTYPE_BAD                             676
#define  STR_GET_YES_NO                               677
#define  STR_GET_YES_NO_YN                            678
#define  STR_PAGE_PROM1                               679
#define  STR_PAGE_PROM2                               680
#define  STR_PAGE_PROM3                               681
#define  STR_PAGE_REMIND                              682
#define  STR_CHECK_PROGRESS                           683
#define  STR_CHECK_PAGED_PROGRESS                     684
#define  STR_TESTING_PROGRESS                         685
#define  STR_PROG_PROGRESS                            686
#define  STR_PROG_TIMING                              687
#define  STR_VERIFY_PROGRESS                          688
#define  TITLE_FILE_ERROR                             689
#define  STR_FILE_ERROR_1                             690
#define  STR_FILE_ERROR_2                             691
#define  STR_FILE_ERROR_3                             692
#define  STR_FILE_ERROR_4                             693
#define  STR_FILE_ERROR_5                             694
#define  STR_FILE_ERROR_6                             695
#define  TITLE_MEMORY_ERROR                           696
#define  STR_MEMORY_ERROR_1                           697
#define  TITLE_OPERATION_ERROR                        698
#define  STR_OPERATION_ERROR_1                        699
                                                        
                                                        
/* From [00700] to [00799] */                           

#define  MONVAR_WINFULL_E                             700
#define  EXPRESSION_NOT_MONITORED_E                   701
#define  RIGHT_EXPR_TOO_BIG_E                         702
#define  INV_OP_TYPES_FOR_OP                          703
#define  INV_TYPE_FOR_SYM                             704
#define  MISSING_RPAREN_IN_EXPR                       705
#define  ARRAY_EXPECTED                               706
#define  ARRAY_BOUNDRY_EXCEEDED                       707
#define  EXPECTING_STRUCTURE                          708
#define  ELEMENT_DOES_NOT_EXIST                       709
#define  INV_OP_FOR_OPERANDS                          710
#define  NOT_A_VALID_EXPR_TOKEN                       711
#define  EXPR_SYNTAX_E                                712
#define  SUB_TYPE_MUST_BE_WORD_OR_BYTE                713
#define  INV_SEG_TYPE                                 714
#define  BIT_OR_SFR_PRE_DOT_E                         715
#define  NON_BIT_ADDRESSABLE_BYTE_E                   716
#define  BIT_OFFSET_INVALID_E                         717
#define  DIVIDE_BY_ZERO_E                             718
#define  NOT_ASSIGNMENT_COMPATABLE_E                  719
#define  BAD_LEFT_OF_ASSIGNMENT_E                     720
#define  ASCII_STRING_TOO_LARGE_E                     721
#define  ASCII_STR_LEN_0_NOT_ALLOWED_E                722
#define  CANNOT_DEREFERENCE_NON_PTR_E                 723
#define  INVALID_SEG_FOR_POINTER_OBJECT               724
#define  BAD_CONTEXT_FOR_MACRO_STRING                 725
#define  MUST_BE_BYTE_VALUES                          726
#define  END_RNGE_LESS_THAN_STRT                      727
#define  NUMERIC_BANKNO_ONLY_E                        728
#define  BANKNO_TOO_BIG_E                             729
#define  TO_BANKNO_NOT_SAME_E                         730
#define  ARR_TOO_SMALL                                731
#define  ERR_DATA_VALUE_UNIQUE                        732
#define  ERR_BANKS_NOT_CONTIGUOUS                     734
#define  ERR_TOO_MANY_DONT_CARES                      735
#define  STR_EMULATOR_MEMS                            736
#define  ERR_MAX_CONTROL_EXCEEDED                     737
#define  STR_COCO_MEM                                 738
#define  STR_BANK_COCO_MEM                            739
#define  BANKS_MUST_MATCH                             740

#define  STR_OPERATION_ERROR_3                        741
#define  STR_OPERATION_ERROR_4                        742
#define  STR_OPERATION_ERROR_5                        743
#define  STR_OPERATION_ERROR_6                        744
#define  TITLE_INSERT_PROM                            745
#define  STR_INSERT_PROM_1                            746
#define  STR_INSERT_PROM_2                            747
#define  STR_INSERT_PROM_3                            748
#define  STR_INSERT_PROM_4                            749
#define  STR_INSERT_PROM_5                            750
#define  STR_INSERT_PROM_6                            751
#define  STR_INSERT_PROM_7                            752
#define  STR_CONTINUE_YN                              753
#define  TITLE_IDENTIFY_PROM                          754
#define  STR_IDENT_RESULT_OK                          755
#define  STR_IDENT_RESULT_BAD                         756
#define  STR_IDENT_ERROR_1                            757
#define  STR_IDENT_ERROR_2                            758
#define  STR_OPERATION_ERROR_2                        759
#define  TITLE_EMUL_START_ADDR                        760
#define  TITLE_PROM_START_ADDR                        761
#define  TITLE_PROM_LENGTH                            762
#define  TITLE_PROMMENU_DLG                           763
#define  TITLE_SECURITY_DLG                           764
#define  TITLE_DEVICE_SELECT                          765
#define  STR_NO_DEVICE_SELECTED                       766
#define  STR_PAGED_PROM                               767
#define  STR_NO_PAGE_SELECTED                         768
#define  STR_IDENT_S                                  769
#define  STR_CMD_ERROR                                770
#define  STR_SELECTED_PROM                            771
#define  STR_INVALID_PROM                             772
#define  STR_NO_PROM_PROG                             773
#define  STR_PROGRAM_ABORT                            774
#define  STR_SELECTED_PAGE_NUM                        775
#define  STR_NO_PROM_SELECTED                         776
#define  STR_PAGED_PROM_REMIND                        777
#define  STR_FILE_ERROR_7                             778
#define  STR_FILE_ERROR_8                             779
#define  STR_PROM_PROG_1                              780
#define  STR_PROM_PROG_2                              781
#define  STR_PROM_PROG_3                              782
#define  STR_PROM_PROG_4                              783
#define  STR_UNSUITABLE_PROG                          784
#define  ERR_ACCESS_OUTSIDE_XDATA                     785
#define  ERR_ACCESS_OUTSIDE_CODE                      786
#define  ERR_ACCESS_OUTSIDE_SFR                       787
#define  ERR_ACCESS_OUTSIDE_RAM                       788
#define  STR_EMULATOR_INITIALISATION                  789
#define  ERR_NOT_VALID_BANK_FILE                      790
#define  STR_BANK_CFG_DLG_TITLE                       791
#define  ERR_ACCESS_OUTSIDE_MEMORY                    792
#define  ERR_NO_EVENT_RAM                             793
#define  ERR_RANGE_EXCEEDS_MAX_VAL                    794
#define  STR_MEM                                      795
#define  STR_BP_MEM                                   796
#define  STR_TRACE_SIZE                               797
#define  STR_ON_STOP_TRIG_WTRCBUFF                    798
#define  STR_HALT_DDC_DISABLED                        799

/* From [00800] to [00899] */
#define  NO_B_E                                       800
#define  NO_W_E                                       801
#define  NO_D_W_E                                     802
#define  NO_W_B_E                                     803
#define  NO_D_W_B_E                                   804
#define  NO_LSQB_E                                    805
#define  NO_RSQB_E                                    806
#define  NO_LSQB_EXP_E                                807
#define  NO_RSQB_EXP_E                                808
#define  NO_PLUS_MINUS_RSQB_E                         809
#define  NO_R0L__R3H_E                                810
#define  NO_R4L__R7H_E                                811
#define  NO_R0L__R7H_E                                812
#define  NO_R0L__R7H_HASH_E                           813
#define  NO_R0__RMAX_E                                814
#define  NO_R8__RMAX_E                                815
#define  NO_R0__RMAX_HASH_E                           816
#define  NO_R0__R7_LSQB_A_E                           817
#define  NO_REG_E                                     818
#define  NO_REG_A_E                                   819
#define  NO_REG_HASH_E                                820
#define  NO_REG_LSQB_E                                821
#define  NO_REG_EXP_E                                 822
#define  NO_REG_HASH_EXP_E                            823
#define  NO_REG_HASH_LSQB_E                           824
#define  NO_REG_HASH_LSQB_EXP_E                       825
#define  NO_REG_HASH_LSQB_EXP_C_E                     826
#define  NO_REG_HASH_LSQB_EXP_USP_E                   827
#define  NO_REG_LSQB_EXP_E                            828
#define  NO_REG_LSQB_EXP_C_E                          829
#define  NO_REG_LSQB_EXP_USP_C_E                      830
#define  NO_DIRECT_E                                  831
#define  NO_BIT_E                                     832
#define  NO_0__15_E                                   833
#define  NO_0__31_E                                   834
#define  NO_EVEN_ADDR_E                               835
#define  REGISTER_DUPLICATED_E                        836
#define  NOT_WORD_E                                   837
#define  NO_M8__7_E                                   838
#define  NO_M32768__32767_E                           839
#define  NO_EVEN_BYTE_REG_E                           840
#define  NO_DOUBLE_REG_E                              841      
#define  NO_EVEN_BYTE_OR_WORD_REG_E                   842      
#define  NO_EVEN_BYTE_OR_DOUBLE_REG_E                 843      
#define  INVALID_PROCMODE_SEL_E                       844      
#define  INVALID_MAP_SEL_E                            845      
#define  FAMILY_CFG_NO_VALID_PROC                     846      
#define  BIT_OFFSET_0__7_INVALID_E                    847      
#define  BIT_OFFSET_0__15_INVALID_E                   848      
#define  STR_PROC_MODE_0                              849
#define  STR_PROC_MODE_1                              850
#define  STR_PROC_VOLT_0                              851
#define  STR_PROC_VOLT_1                              852
#define  STR_PROC_VOLT_2                              853
#define  STR_PROC_VOLT_TRK                            854
#define  STR_PROC_MODE_TRK                            855
#define  STR_PROC_BUS_8                               856
#define  STR_PROC_BUS_16                              857
#define  STR_PROC_BUS_TRK                             858
#define  STR_CAUSE_OF_BRK                             859
#define  ERR_SAVE_64K_MAX                             860
#define  CLOSING_SYMBOLS                              861
#define  CANNOT_CMO_SFR                               862
#define  STR_PROC_MODE_2                              863  
#define  STR_MAP_RES_2                                864  
#define  STR_MAP_RES_32                               865  
#define  STR_MAP_RES_512                              866  
#define  STR_PROC_BUS_12                              867
#define  STR_PROC_BUS_20                              868
#define  STR_PROC_BUS_24                              869
#define  STR_COB_BP_PAGE                              870
#define  STR_TARGET_PROC_ACK                          871
#define  ERR_CANNOT_MAP                               872
#define  ERR_TOO_MANY_TYPES                           873
#define  DOS_ENV_VAR_NOT_DEFINED                      874
#define  STR_RELOAD_USER_PROG_IN_DDC                  875
#define  STR_WR_TWO_PART_PC                           876
#define  STR_TWO_PART_PC_VAL                          877
#define  INVALID_REGISTER                             878

/* BLK_MODE_20-JAN-1997_MOH */
#define  STR_STOP_TRACE_ACCEL                         879
#define  STR_NEXT_BLOCK_ACCEL                         880
#define  STR_STOP_TRACE                               881
#define  STR_NEXT_BLOCK                               882
#define  BLK_MODE_NOT_ALLOWED                         883
#define  STR_PROBE_VOLT                               884

#define  UNABLE_TO_READ_VARIABLE_FILE                 885
#define  VARIABLE_FILTER                              886
#define  LOAD_VARIABLE                                887
#define  SAVE_VARIABLE                                888
#define  CMD_INVALID_FOR_PROC                         889
#define  CMD_INVALID_IN_MIFARE_CODE                   890
#define  ERR_LOAD_MIFARE_CODE                         891
#define  IE_A_MONVAR_00                               892
#define  STR_CODE_FETCH                               893
#define  STR_BUS_MODE_A                               894
#define  STR_BUS_MODE_B                               895
#define  ERR_STARS_HARDWARE_EVENT_ERROR               896
#define  CANT_LOAD_GDIXA_DLL                          897
#define  ERR_MEM_POS                                  898
#define  STR_ON_CHIP_RAM_BP                           899

/* From [00900] to [00999] */
#define  NO_A_B_E                                     900  
#define  NO_COLON_E                                   901  
#define  NO_EXP_DP_DT_A_B_E                           902  
#define  NO_EXP_DP_DT_E                               903  
#define  NO_EXP_DP_DT_HASH_E                          904  
#define  NO_EXP_DP_DT_LG_LPAREN_E                     905  
#define  NO_EXP_DP_DT_LG_LPAREN_HASH_E                906  
#define  NO_EXP_DP_E                                  907  
#define  NO_EXP_LG_E                                  908  
#define  NO_EXP_LG_LPAREN_E                           909  
#define  NO_EXP_LPAREN_E                              910  
#define  NO_LPAREN_E                                  911  
#define  NO_RPAREN_E                                  912  
#define  NO_Y_E                                       913  
#define  NO_X_S_E                                     914  
#define  NO_X_Y_E                                     915  
#define  NO_X_Y_S_E                                   916  
#define  NO_ABS_ADDR_E                                917  
#define  NO_DIRECT_ABS_E                              918  
#define  NO_8_16_E                                    919  
#define  FLAG_DUPLICATED_E                            920  
#define  NO_REGISTER_E                                921  
#define  NO_FLAG_E                                    922  
#define  NO_ABS_CODE_ADDR_E                           923   
#define  ERR_NO_FUNC_FOR_ADDR                         924   
#define  ERR_NO_BANKED_HW                             925   
#define  ERR_INSUFFICENT_BANKED_RAM                   926
#define  STR_RTOS_NAME                                927
#define  STR_RTOS_WIN_TITLE                           928
#define  ERR_SWBP_IN_EMUL                             929
#define  ERR_SWBP_LEN_ONE                             930

#define  IE_TREE_1                                    931
#define  IE_TREE_2                                    932
#define  IE_TREE_3                                    933
#define  IE_TREE_4                                    934
#define  IE_TREE_5                                    935
#define  IE_TREE_6                                    936
#define  IE_TREE_7                                    937
#define  IE_TREE_8                                    938
#define  IE_TREE_9                                    939
#define  IE_TREE_10                                   940
#define  IE_TREE_11                                   941
#define  IE_TREE_12                                   942
#define  IE_TREE_13                                   943
#define  IE_TREE_14                                   944
#define  IE_TREE_15                                   945
#define  IE_TREE_16                                   946
#define  IE_TREE_17                                   947
#define  IE_A_MAP_00                                  948
#define  STR_AUTOSTEP                                 949

/* From [00950] to [00950] */
#define  ERR_CANNOT_MAP_ONCHIP_MEM                    950
#define  STR_BP_XDATA                                 951
#define  STR_MODE_PROC_RESET                          952
#define  STR_MODE_OF_EXECUTION                        953
#define  STR_WAIT                                     954
#define   ASSEMBLER_PARSER_ERROR                       955
#define  ERR_WRITE_MIFARE_CONFIG_BYTE                 956                       
#define  ERR_SENDING_PROCTYPE                         957
#define  ERR_SPAWNING_PROCESS                         958
#define  STR_PREDEFSYM_MAKING                         959
#define  NO_A_B_CCR_E                                 960 
#define  NO_X_Y_SP_E                                  961 
#define  NO_X_Y_SP_PC_E                               962 
#define  NO_A_B_D_X_Y_SP_E                            963 
#define  NO_A_B_CCR_TMP2_D_X_Y_SP_E                   964 
#define  NO_A_B_CCR_TMP3_D_X_Y_SP_E                   965 
#define  NO_TMP2_D_X_Y_SP_E                           966 
#define  NO_EXP_D_E                                   967 
#define  NO_EXP_A_B_D_E                               968 
#define  NO_EXP_HASH_A_B_D_E                          969 
#define  NO_EXP_LSQB_A_B_D_E                          970 
#define  NO_EXP_LSQB_HASH_A_B_D_E                     971 
#define  NO_PLUS_MINUS_X_Y_SP_PC_E                    972 
#define  NO_HASH_PLUS_MINUS_X_Y_SP_PC_E               973 
#define  NO_1__8_E                                    974 
#define  NO_M16__15_E                                 975 
#define  NO_M256__255_E                               976 
#define  NO_COMMA_PLUS_MINUS_E                        977 
#define  NO_EXP_X_Y_SP_E                              978 
#define  SOURCE_LINE_TOO_LONG                         979
#define  IE_TXRXCMD_00                                980
#define  IE_TXRXCMD_01                                981
#define  IE_TXRXCMD_02                                982
#define  IE_TXRXCMD_03                                983
#define  IE_TXRXCMD_04                                984
#define  IE_TXRXCMD_05                                985
#define  IE_TXRXCMD_06                                986
#define  IE_TXRXCMD_07                                987
#define  IE_TXRXCMD_08                                988
#define  IE_TXRXCMD_09                                989
#define  IE_TXRXCMD_10                                990
#define  IE_TXRXCMD_11                                991
#define  IE_TXRXCMD_12                                992
#define  IE_TXRXCMD_13                                993
#define  IE_TXRXCMD_14                                994
#define  IE_TXRXCMD_15                                995
#define  IE_TXRXCMD_16                                996
#define  IE_TXRXCMD_17                                997

/* From [01000] to [01099] */

#define  NO_EVENTS_SELECTED_E                         1000
#define  NOT_READY_TO_QUIT_E                          1001
#define  TOO_MANY_EVENTS_E                            1002
#define  ESC_EXIT_FROM_PAIR_M                         1003
#define  NO_MEM_FOR_SEA_E                             1004
#define  NO_MORE_MEM_FOR_SEA_E                        1005
#define  DIV_BY_ZERO_E                                1006
#define  GREATER_THAN_100_E                           1007
#define  SEA_NOT_SETUP_E                              1008
#define  SEA_ACTIVATED_M                              1009
#define  SEA_DEACTIVATED_M                            1010
#define  SEA_ALL_EVENTS_CLEARED_M                     1011
#define  NO_SEA_HARDWARE_E                            1012
#define  SEA_BUSY_E                                   1013
#define  BAD_VER_OF_SEA_SETUP_FILE                    1014
#define  BAD_DATA_IN_SEA_SETUP_FILE                   1015
#define  SEA_DATA_RESET_W                             1016
#define  SEA_DATA_RESET_OK_M                          1017
#define  SEA_NO_FNS_IN_HILEVEL_MOD_E                  1018
#define  SEA_NO_FNS_IN_MOD_E                          1019
#define  SEA_NO_MODULES_E                             1020
#define  SEA_NO_FNS_TO_DISPLAY                        1021
#define  SEA_HILEVEL_MOD_NO_FNS                       1022
#define  SEA_FILE_ERROR_E                             1023
#define  ERROR_IN_SEA_TMP_STORE_E                     1024
#define  LOADING_EVENTS_M                             1025
#define  SEA_RUNNING_ON_ENTRY_M                       1026
#define  NO_SEA_FN_SEL_WHILE_EM_M                     1027
#define  FT_BUSY_E                                    1028
#define  CANNOT_VIEW_PA_IN_FT                         1029
#define  CANNOT_VIEW_FT_IN_PA                         1030
#define  SEACONTROL_OUT_OF_SYNC_E                     1031
#define  FT_BUF_PROBLEM_E                             1032
#define  SEA_NO_SUCH_FRAME_E                          1033
#define  FT_NO_FRAMES_IN_BUFFER                       1034
#define  BAD_BANKED_ENTRY_EXIT_PAIR                   1035  
#define  SEA_BANKED_BOARD_E                           1036  
#define  STR_FRAMES_AFTER_STOP_TRG                    1037
#define  STR_EMULATOR_CONFIGURATION                   1038
#define  STR_MEM_NOT_POSTIONED                        1039
#define  STR_MEM_POS                                  1040
#define  INVALID_ASSEMBLY_LABEL                       1041
#define  INVALID_ASSEMBLY_MNEM                        1042
#define  FILE_LONGER_THAN_SUPPORTED_SIZE              1043
#define  ERR_CONFIG_EMU_CHIP                          1044
#define  CANNOT_CONFIG_EMU_CHIP_IN_DDC                1045
#define  NO_DATA_WITH_CODE_EXEC                       1046
#define  NO_16_BIT_DATA_ON_ODD_ADDR                   1047
#define  STEPCAM_INIT_FAILED                          1048
#define  STEPCAM_NO_CURRENT_DOCUMENT                  1049
#define  ADAF_LOG_FILE_E                              1050
#define  FT_NO_PREV_TRIG_FOUND_E                      1051
#define  FT_NO_NEXT_TRIG_FOUND_E                      1052
#define  ERR_TR_CSWIN                                 1053
#define  ERR_TR_WTWIN                                 1054
#define  FT_NO_FTRACE_DATA_M                          1055

/* MOH_STARS_97 */
#define  ERR_INVALID_CMD_STARS_ENABLED                1056
#define  ERR_STARS_FIFO_OVERFLOW                      1057
#define  ERR_INVALID_STARS_MODE                       1058
#define  ERR_RISC_COMMS_FAILURE                       1059
#define  STR_CLEARING_STARS_EVENTS                    1050
#define  EVENTS_LIST_FULL                             1061
#define  NO_CODE_LOADED                               1062
#define  OUT_OF_MEMORY                                1063
#define  NO_EVENTS_SELECTED                           1064
#define  ENTRY_EXIT_ADDR_SAME                         1065
#define  EXIT_ADDRESSES_SAME                          1066
#define  EEP_SELECTED_ALREADY                         1067
#define  INVALID_ENTRY_ADDR                           1068
#define  INVALID_EXIT1_ADDR                           1069
#define  INVALID_EXIT2_ADDR                           1070
#define  STR_SFWVER                                   1071
#define  ERR_INVALID_STARS_TYPE                       1072
#define  ERR_INVALID_SRAM_CINFIG                      1073
#define  ERR_STARS_TIMER_OVERFLOW                     1074
#define  ERR_DRAM_SIZE                                1075
#define  ERR_BAD_SRAM                                 1076
#define  ERR_BAD_COMM_SRAM                            1077
#define  ERR_STARS_ABSENT                             1078
#define  SAME_ADDRESS_USED                            1079
#define  STR_SWVER                                    1080
#define  CANT_LOAD_STARS_DLL                          1081
#define  STR_CODESPACE                                1082

/* MOH_STARS_97_UPDATE */
#define  STR_DLLVER                                   1083
#define  ENTRY_NOT_FOUND                              1084
#define  EXIT_NOT_FOUND                               1085
#define  MFC_FILE_EXCEPTION                           1086
#define  INVALID_FUNCS_IN_SSF                         1087
#define  INVALID_SSF_FORMAT                           1088
#define  STARS_CLEARING_EVENTS                        1089
#define  STARS_WRITING_EVENTS                         1090
#define  STARS_READING_TRACE                          1091
#define  STARS_SEARCHING_TRACE                        1092

/* MOH_STARS_97_25 */
#define  NO_VALID_SYMBOLS_FOUND                       1093
#define  STARS_LOGGING                                1094
#define  CANT_LOAD_STEPCAM_DLL                        1096
#define  OUTSIDE_SCHAR_LIMIT                          1097
#define  OUTSIDE_UCHAR_LIMIT                          1098
#define  OUTSIDE_SINT_LIMIT                           1099

/* From [01100] to [01199] */
#define  OUTSIDE_UINT_LIMIT                           1100
#define  OUTSIDE_ULONG_LIMIT                          1101
#define  OUTSIDE_SLONG_LIMIT                          1102
#define  ERR_TR_TRIGPUMWIN                            1103
#define  ERR_INVALID_BANK_WR                          1104
#define  TA_SCREEN_EMPTY_E                            1105
#define  TH_SCREEN_EMPTY_E                            1106
#define  ERR_TR_TCWIN                                 1107
#define  ERR_TR_TCCLWIN                               1108
#define  STR_SYM_NOT_FOUND                            1109
#define  ERR_COMPARING_TRFRAME                        1110
#define  ERR_TR_TCWIN_VISON                           1111
#define  ERR_TR_TCWIN_CURON                           1112
#define  ERR_TR_TCWIN_ASCROFF                         1113
#define  ERR_TR_TCWIN_CURWOFF                         1114
#define  ERR_TR_TCWIN_ZOOM                            1115
#define  ERR_TR_TCWIN_CURR                            1116
#define  ERR_TR_TCWIN_VCLEAR                          1117
#define  ERR_TYPE_EXISTS_MEM                          1118
#define  FUNCTION_TOO_SHORT_FOR_ANAL                  1119

#define  INI_KDI_SECTION                              1120
#define  INI_KDI_KSM_NAME                             1121
#define  INI_LOAD_KSM                                 1122
#define  KSM_FILTER                                   1123
#define  ERR_ADD_MENU_ITEMS_KSM_DLL                   1124
#define  ERR_ALREADY_LOADED_KDI_DLL                   1125
#define  ERR_LOADING_KDI_DLL                          1126
#define  ERR_UNLOADING_KDI_DLL                        1127
#define  ERR_LINKING_KDI_DLL                          1128
#define  ERR_INITIALISING_KDI_DLL                     1129
#define  ERR_ALREADY_LOADED_KSM_DLL                   1130
#define  ERR_LOADING_KSM_DLL                          1131
#define  ERR_UNLOADING_KSM_DLL                        1132
#define  ERR_LINKING_KSM_DLL                          1133
#define  ERR_KDIOPEN_KSM_DLL                          1134
#define  ERR_KDICLOSE_KSM_DLL                         1135
#define  ERR_SET_CALLBACK_KSM_DLL                     1136
#define  ERR_SET_CPUNAME_KSM_DLL                      1137
#define  ERR_INIT_REGISTER_MAP_KSM_DLL                1138
#define  ERR_INIT_MEMORY_MAP_KSM_DLL                  1139
#define  IE_KDIXV_00                                  1140
#define  IE_KDIXV_01                                  1141
#define  IE_KDIXV_02                                  1142
#define  IE_KDIXV_03                                  1143
#define  IE_KDIXV_04                                  1144
#define  IE_KDIXV_05                                  1145
#define  IE_KDIXV_06                                  1146
#define  IE_KDIXV_07                                  1147
#define  IE_KDIXV_08                                  1148
#define  IE_KDIFACE_00                                1149
#define  IE_KDIFACE_01                                1150
#define  IE_KDIFACE_02                                1151
#define  IE_KDIFACE_03                                1152
#define  IE_KDIFACE_04                                1153
#define   ASM16_VAL_TOO_LARGE                  1154   
#define   ASM16_IMPLICIT_ZERO                  1155
#define   ASM16_VAL_NOT_ALLOWED                   1156
#define   ASM16_MACRO_NOT_ALLOWED                 1157
#define   ASM16_INCORRECT_OPERAND                 1158
#define  ASM16_INVALID_OPERAND_FOR_INST               1159
#define   ASM16_UNRECOGNISED_OPERAND                 1160
#define   ASM16_INVALID_DISP_SIZE_SPECIFIER          1161
#define  ASM16_NEG_DISPL_NOT_ALLOWED              1162
#define  ASM16_NEG_ABSOL_NOT_ALLOWED              1163   
#define  ASM16_OPERAND_NOT_VALID_SIZE             1164
#define  ASM16_DISPLACEMENT_INVALID               1165
#define  ASM16_SPECIFIED_SIZE_TOO_SMALL           1166
#define  ASM51_16BIT_ADDR_EXPECTED                1167
#define  ASM51_11BIT_ADDR_EXPECTED                1168
#define  ASM51_REL_ADDR_EXPECTED                  1169
#define  ASM51_RN_RI_DIR_A_EXPECTED               1170
#define  ASM51_RN_RI_DIR_A_DPTR_EXPECTED          1171
#define  ASM51_RN_RI_DIR_IMMED_EXPECTED           1172
#define  ASM51_DIRECT_ADDR_EXPECTED               1173
#define  ASM51_A_IMMED_EXPECTED                   1174
#define  ASM51_DIR_A_EXPECTED                     1175
#define  ASM51_RN_RI_DIR_EXPECTED                 1176
#define  ASM51_RI_EXPECTED                        1177
#define  ASM51_RN_RI_A_EXPECTED                   1178
#define  ASM51_RN_DIR_EXPECTED                    1179
#define  ASM51_DIR_IMMED_EXPECTED                 1180
#define  ASM51_IMMED_EXPECTED                     1181
#define  ASM51_A_C_DIR_EXPECTED                   1182
#define  ASM51_BIT_EXPECTED                       1183
#define  ASM51_A_C_BIT_EXPECTED                   1184
#define  ASM51_C_BIT_EXPECTED                     1185
#define  ASM51_RI_A_DPTR_EXPECTED                 1186
#define  ASM51_RI_DPTR_EXPECTED                   1187
#define  ASM51_A_DPTR_EXPECTED                    1188
#define  ASM51_RN_RI_DIR_A_C_BIT_DPTR_EXPECTED    1189
#define  ASM51_A_DIR_IMMED_EXPECTED               1190
#define  ASM51_RN_RI_A_DIR_IMMED_EXPECTED         1191
#define  ASM51_C_EXPECTED                         1192
#define  ASM51_IMMED16_EXPECTED                   1193
#define  ASM51_DPTR_EXPECTED                      1194
#define  ASM51_INDIRA_ADDR_EXPECTED               1195
#define  ASM51_PLUSDPTR_EXPECTED                  1196
#define  ERR_HSPEED_SCCODE_MAP_ERR                1197
#define  ERR_HS_OC_ENABLED_MAP_TARGET_ONLY        1198
#define  CAN_ONLY_MONITOR_STRING_E                1199
    
/* From [01200] to [01299] */
#define  FIRST_REC_INVALID_E                          1200
#define  OBJ_FMT_INVALID_E                            1201
#define  NOT_ABSOLUTE_REC_E                           1202
#define  OMF_CHECKSUM_E                               1203
#define  INVALID_TEXT_E                               1204
#define  OMF_FORMAT_NOT_8051_E                        1205
#define  UNKNOWN_LANGUAGE_ERROR                       1207
#define  INVALID_LST_REF_E                            1208
#define  SYM_ERR_CREATE_RTN_E                         1209
#define  SYM_ERR_CREATE_SYM_E                         1210
#define  FREE_MEMORY_ERR                              1211
#define  INVALID_ASDOF_ARRAY_SIZE_E                   1212
#define  SYM_ERR_CREATE_STMT_E                        1213
#define  INVALID_ADR_ORG_LOW_E                        1214
#define  UNKNOWN_OMF_SYM_BLOCK_TYP_E                  1215
#define  INVALID_MEMSPACE_TYPE_E                      1216
#define  NOT_ABSOLUTE_SEG_REC_E                       1217
#define  SYM_ERR_CREATE_MEMB_E                        1218
#define  INVALID_MEMB_ADR_ORG_E                       1219
#define  UNEXPECTED_END_OF_OBJ_FILE_E                 1220
#define  CANT_FIND_ENTRY_POINT_E                      1221
#define  ILLEGAL_ASM_RANGE_ERR                        1222
#define  CANT_STORE_MODULE_SYMBOL_E                   1223 
#define  INSUFFIC_MEMORY_FOR_LOAD                     1224
#define  CANT_STORE_STMT_SYMBOL_E                     1225
#define  CANT_STORE_ROUTINE_SYMBOL_E                  1226
#define  CANT_STORE_SYMBOL_E                          1227
#define  SYM_ERR_CREATE_SYST_E                        1228
#define  BAD_OR_NO_PREDEF_SYM_FILE_E                  1229
#define  NOT_VALID_PLM51_ASDOF                        1230
#define  NOT_VALID_IARC51_ASDOF                       1231
#define  INCORRECT_ASDOF_VERSION_E                    1232
#define  TYPE_DEFN_NOT_FOUND                          1233
#define  TYPE_ALREADY_DEFINED_E                       1234
#define  ARRAY_ELEMENT_TYPE_NOT_FOUND                 1235
#define  UNKNOWN_TYPE_DEFINITION_E                    1236
#define  MEMBER_TYPE_NOT_DEFINED_E                    1237
#define  MEMBER_ALREADY_DEFINED_E                     1238
#define  INV_IARC_LIBFNC_FILE_E                       1239 
#define  TYPE_NOT_FOUND_E                             1240
#define  NO_MEM_FOR_C_LIB_TABLE_E                     1241
#define  UNKNOWN_MODEL_E                              1242
#define  NOT_VALID_KEIL51_ASDOF                       1243
#define  NOT_VALID_TASKING51_ASDOF                    1244
#define  BAD_BANK_SWITCH_INFO_REC_E                   1245
#define  ASDOF_LENGTH_ERR_E                           1246
#define  ASDOF_UNBALANCED_SYMBOLS_BANK_E              1247
#define  ASDOF_BANK_RANGE_E                           1248
#define  ASDOF_BANK_HEADER_REC_E                      1249
#define  INVALID_BANKCHANGE_REQUEST_E                 1250
#define  STR_START_TRIG_COUNT                         1251
#define  ERR_NO_DSR_ACCESS                            1252
#define  ERR_NO_CSR_ACCESS                            1253
#define  ERR_WR_DATA_LEN                              1254
#define  IE_ASM8051_00                                1255
#define  IE_ASM8051_01                                1256
#define  IE_ASM8051_02                                1257
#define  IE_ASM8051_03                                1258
#define  IE_ASM8051_04                                1259
#define  IE_ASM8051_05                                1260
#define  IE_ASM8051_06                                1261
#define  IE_ASM8051_07                                1262
#define  IE_ASM8051_08                                1263
#define  IE_ASM8051_09                                1264
#define  IE_ASM8051_10                                1265
#define  IE_ASM8051_11                                1266
#define  IE_ASM8051_12                                1267
#define  IE_ASM8051_13                                1268
#define  IE_ASM8051_14                                1269
#define  IE_ASM8051_15                                1270
#define  IE_ASM8051_16                                1271
#define  IE_ASM8051_17                                1272
#define  IE_ASM8051_18                                1273
#define  IE_ASM8051_19                                1274
#define  IE_ASM8051_20                                1275
#define  IE_ASM8051_21                                1276
#define  IE_ASM8051_22                                1277
#define  IE_ASM8051_23                                1278
#define  IE_ASM8051_24                                1279
#define  IE_ASM8051_25                                1280
#define  IE_ASM8051_26                                1281
#define  IE_ASM8051_27                                1282
                                                          

/* From [01300] to [01399] */

#define  INVALID_CODECOV_ENABLE_FILE                  1301
#define  INVALID_FUNCTRC_ENABLE_FILE                  1302
#define  INVALID_CTMS_ENABLE_FILE                     1303
#define  ERR_DW1_CHECKSUM                             1304
#define  ERR_DW2_CHECKSUM                             1305
#define  ERR_INVALID_REQ                              1306
#define  ERR_INVALID_CMD                              1307
#define  ERR_DW1_DW2                                  1308
#define  ERR_UNKNOWN_DW_ERROR                         1309
#define  NOT_TRIG_FILE_PAS_E                          1310
#define  TRACE_IS_ON                                  1311
#define  TRACE_IS_OFF                                 1312
#define  TRIGGER_NOT_ACTIVE                           1313
#define  TRIGGER_ACTIVE_FROM                          1314
#define  TRIGGER_ACTIVE_BUT_UNKNOWN                   1315
#define  DONT_ALLOW_RWBPTS_WITH_TRACE                 1316
#define  SPECIFIED_ADDRESS_TOO_BIG                    1317
#define  PROGRAM_NOT_RUNNING                          1318
#define  INSTR_NOT_PRESENT_E                          1319
#define  NO_TRACED_DATA_E                             1320
#define  ERR_DW3_CHECKSUM                             1321
#define  ERR_FPGA_NO_FILE_SIZE                        1322  
#define  ERR_FPGA_FILE_TOO_BIG                        1323  
#define  ERR_FPGA_FILE_EMPTY                          1324  
#define  ERR_NOT_AN_FPGA_FILE                         1325  
#define  ERR_FPGA_FILE_CORRUPT                        1326  
#define  IE_FPGA_MEM_ALLOC                            1327  
#define  IE_FPGA_MEM_DEALLOC                          1328  
#define  LOAD_FPGAWARE                                1329  
#define  IE_WINCOMM_00                                1330  
#define  STR_FPGAVER                                  1331  
#define  STR_FAMILY_BRD_REV                           1332
#define  ERR_BLOCK_MAP_UNSUPPORTED                    1333
#define  STR_BP_BR_RANGE                              1334
#define  STR_BP_ON_CHIP_RAM                           1335
#define  ILLEGAL_RANGE_OVERLAP                        1336
#define  WARN_RANGE_CONSEQ_FOR_SMARTXA                1337
#define  STR_ILLEGAL_INST                             1338
#define  STR_EXE_OUTSIDE_IMPLEM_SP                    1339
#define  STR_EE_EXEC_DURING_FAME                      1340
#define  STR_EE_EXEC_DURING_PROG                      1341
#define  STR_EXE_OUTSIDE_WIN_SP                       1342
#define  INVALID_PSN_FILE                             1343
#define  ERR_INVALID_MAP_EXPANDED                     1344
#define  STR_SEAWAREVER                               1345
#define  STR_PRU_BRD_REV                              1346
#define  STR_CTRL_BRD_REV                             1347
#define  STR_ULM_BRD_REV                              1348
#define  STR_ULB_BRD_REV                              1349
#define  SFR_COCO_INVALID                             1350
#define  ERR_FORCED_EMULATOR_RESET                    1351
#define  ERR_CANT_MAP_WE5000_EEPROM                   1352
#define  ERR_INCORRECT_PROG_BYTES_VERIFICATION        1353
#define  STR_UNUSED_REG                               1354
#define  ERR_CANT_CHANGE_WE5000_MAP_DEFAULT           1355
#define  ONLY_XTERNAL_XDATA_ALLOWED_E                 1356
#define  STR_BANK_XDATA_MEM                           1357
#define  STR_BANK_SAA_BPOINTS                         1358
#define  STR_BANK_SAA_CODE                            1359
#define  STR_BANK_CODE_BPOINTS                        1360
#define  STR_BANK_1MSAA_CODE                          1361
#define  OFFSET_SPEC_FOR_BITFIELD_ONLY                1362
#define  SIZE_SPEC_FOR_REG_OR_BITFIELD_ONLY           1363
#define  UNABLE_TO_PREDEF_SYMBOL                      1364
#define  SIZE_SPECIFIER_REQUIRED                      1365
#define  OFFSET_SPECIFIER_REQUIRED                    1366
#define  RD_SPEC_FOR_REG_OR_BITFIELD_ONLY             1367
#define  MEM_SPEC_FOR_REG_OR_BITFIELD_ONLY            1368
#define  ADDR_SPEC_FOR_REG_OR_BITFIELD_ONLY           1369
#define  UNABLE_TO_INTERPRET_TRACE_CODE               1370
#define  STR_HARDWARE_BP                              1371
#define  STR_COB_WATCHPOINT                           1372  
#define  STR_COB_USER_INTERRUPT                       1373
#define  STR_COB_BRANCH_THROUGH0                      1374
#define  STR_COB_SOFTWARE_INTERRUPT                   1375
#define  STR_COB_PREFETCH_ABORT                       1376
#define  STR_COB_DATA_ABORT                           1377
#define  STR_COB_ADDRESS_EXCEPTION                    1378
#define  STR_COB_IRQ                                  1379
#define  STR_COB_FIQ                                  1380
#define  ERR_ZERO_LENGTH_IS_INVALID                   1381

/* From [01382] to [01499] */

#define  INVALID_SWAP_MEMORY_BLOCK                    1400
#define  CANT_CREATE_SWAPFILE                         1401
#define  INVALID_SWAPFILE_NAME                        1402
#define  INSUFFICIENT_DISKSPACE_TO_SWAP               1403
#define  ERROR_RESETTING_SWAPFILE                     1404
#define  INSUFFICIENT_SPACE_FOR_IDEAS                 1405
#define  STR_BACKGROUND                               1406
#define  ERROR_PARAM_MISSING                          1407
#define  STR_GENERAL_TGT_RST                          1408
#define  STR_EEPROM_MEM                               1409
#define  INVALID_BANKING_MODEL                        1410
#define  ERR_BP_OUTSIDE_PAGE                          1411
#define  ERR_RANGE_OC_BP_IN_SC                        1412
#define  NO_RD_WR_BP_FOR_FAMILY                       1413
#define  IE_PROCDWRF15                                1414
#define  ERR_BAD_STAB                                 1415
/* [01417] to [01449] unused */
#define  UNSUCCESSFUL_MEMORY_DOWNSIZE                 1450
#define  SWAP_DISK_WRITE_ERROR                        1451
#define  UNSUCCESSFUL_EXEC                            1452
#define  INVALID_BANK_PORT                            1453
#define  INVALID_BANKMAP_GRANULARITY                  1454
#define  BANK_MAP_DISPLAY                             1455
#define  BANK_MODE_OFF                                1456
#define  BANK_EXCEEDS_MAX_BANKS                       1457
#define  INVALID_BANKWINDOW                           1458

/* From [01500] to [01599] */

#define  CREATE_COCO_BACKUP_FILE_FAILED               1501
#define  ERROR_SAVING_COCO_HEADER_REC                 1502
#define  INVALID_COCO_FILE                            1503
#define  WRONG_FINDER_VERSION                         1504
#define  WRONG_COCO_VERSION                           1505
#define  STR_TARGET                                   1506
#define  CORRUPT_COCO_FILE                            1507
#define  NO_MEM_FOR_COCO_NODE                         1508
#define  CANT_SIZE_MODLWIN                            1509
#define  INVALID_RANGE_FOR_BP                         1510
#define  MAX_NO_BREAK_RANGES_SET                      1511
#define  RAM_BPS_ENABLED                              1512
#define  NO_BREAK_RANGE_AT_THIS_ADDR                  1513
#define  NO_BREAK_RANGES_SET                          1514
#define  MAX_NO_RAM_BPS_SET                           1515
#define  NO_RAM_BP_AT_THIS_ADDR                       1516
#define  NO_RAM_BPS_SET                               1517
#define  NO_ED_TRIG_WITH_RAM_BPS                      1518
#define  NO_GO_FOR_TM_WITH_RAM_BP                     1519
#define  NO_GOTO_WITH_BREAK_UPD                       1520
#define  COCO_DISABLED                                1521
#define  BANKED_COCO_FAILURE                          1522
#define  COCO_INVALID_BANK_RANGES                     1523
#define  ER_BP_COCO_ENABLED                           1524
#define  STR_EVENTS_ABCD                              1525
#define  STR_EVENTS_AB                                1526
#define  STR_VOLTAGE                                  1527
#define  STR_MODE                                     1528
#define  STR_PROC                                     1529
#define  STR_BUS_WIDTH                                1530
#define  STR_NO_EXIT_FOUND_IN_FUNC                    1531
#define  CANT_MAP_BANKED_MEMORY                       1532
/* [01533] to [01598] unused */

#define  IE_PARSER_00                                 1550
#define  IE_PARSER_01                                 1551
#define  IE_PARSER_02                                 1552
#define  IE_PARSER_03                                 1553
#define  IE_PARSER_04                                 1554
#define  IE_PARSER_05                                 1555
#define  IE_PARSER_06                                 1556
#define  IE_PARSER_07                                 1557
/*
MAX_EMSG_NUM is not a message, its used to size message arrays for PF-DOS
(see msg.h)
*/
#define  MAX_EMSG_NUM                                 1599

/* From [01600] to [01699] */
#define  IE_ACTION_00                                 1600
#define  IE_ACTION_01                                 1601
#define  IE_ACTION_02                                 1602
#define  IE_ACTION_03                                 1603
#define  IE_ACTION_04                                 1604
#define  IE_ACTION_05                                 1605
#define  IE_ACTION_06                                 1606
#define  IE_ACTION_07                                 1607
#define  IE_ACTION_08                                 1608
#define  IE_ACTION_09                                 1609
#define  IE_ACTION_10                                 1610
#define  IE_ACTION_11                                 1611
#define  IE_ACTION_12                                 1612
#define  IE_ACTION_13                                 1613
#define  IE_ACTION_14                                 1614
#define  IE_ACTION_15                                 1615
#define  IE_ACTION_16                                 1616
#define  IE_ACTION_17                                 1617
#define  IE_ACTION_18                                 1618
#define  IE_ACTION_19                                 1619
#define  IE_ACTION_20                                 1620
#define  IE_ACTION_21                                 1621
#define  IE_ACTION_22                                 1622
#define  IE_ACTION_23                                 1623
#define  IE_ACTION_24                                 1624
#define  IE_ACTION_25                                 1625
#define  IE_ACTION_26                                 1626
#define  IE_ACTION_27                                 1627
#define  IE_ACTION_28                                 1628
#define  IE_ACTION_29                                 1629
#define  IE_ACTION_30                                 1630
#define  IE_ACTION_31                                 1631
#define  IE_ACTION_32                                 1632
#define  IE_ACTION_33                                 1633
#define  IE_ACTION_34                                 1634
#define  IE_ACTION_35                                 1635
#define  IE_ACTION_36                                 1636
#define  IE_ACTION_37                                 1637
#define  IE_ACTION_38                                 1638
#define  IE_ACTION_39                                 1639
#define  IE_ACTION_40                                 1640
#define  IE_ACTION_41                                 1641
#define  IE_ACTION_42                                 1642
#define  IE_ACTION_43                                 1643
#define  IE_ACTION_44                                 1644
#define  IE_ACTION_45                                 1645
#define  IE_ACTION_46                                 1646
#define  IE_ACTION_47                                 1647
#define  IE_ACTION_48                                 1648
#define  IE_ACTION_49                                 1649
#define  IE_ACTION_50                                 1650
#define  IE_ACTION_51                                 1651
#define  IE_ACTION_52                                 1652
#define  IE_ACTION_53                                 1653
#define  IE_ACTION_54                                 1654

/* From [01700] to [01799] */
#define  INCOMPATIBLE_COMMAND_PARAM                   1700
#define  COMMAND_PARAM_TOO_LARGE                      1701
#define  STR_COMMANDECHOING                           1702
#define  STR_DISPREGECHOING                           1703
#define  STR_ECHO                                     1704
#define  STR_ERRORECHOING                             1705
#define  STR_TYPECHECK                                1706
#define  STR_SCOPECHECK                               1707
#define  USE_ASHLING_KEY_SEQ                          1708
#define  INVALID_PATH_NAME                            1709
#define  STEP_S_INVALID                               1710
#define  EOL_EXPECTED                                 1711
#define  INVALID_TIMER_UNIT                           1712
#define  CANT_GO_FRM_RST_FOR_TIME                     1713
#define  STR_FLOAT_EXPR                               1714
#define  STR_CONSTANT                                 1715
#define  CANNOT_ASSIGN_DOSENV_PARAM                   1716
#define  EXPECTED_CORRECT_MACRO_NAME                  1717
#define  TOO_LONG                                     1718
#define  NO_ELEMENT                                   1719
#define  FIRST_PARAM_MACRO_NO_EXIST                   1720
#define  CANNOT_USE_MACRO_NON_STRING                  1721
#define  SRC_STR_INVALID                              1722
#define  DEST_STR_INVALID                             1723
#define  INPUT_VALUE_TOO_BIG_FOR_STR                  1724
#define  STR_STR_CONSTANT                             1725
#define  NEED_MACRO_VAR_TYPE                          1726
#define  COMMAND_NEEDS_OUTPUT_STRING                  1727
#define  OUTPUT_STRING_TOO_LONG                       1728
#define  NEED_START_ADDRESS                           1729
#define  START_ADDRESS_ON_64K_BOUNDARY                1730
#define  STR_DDCHECK                                  1731
#define  STR_PDBREAK                                  1732
#define  INI_ENABLE_DDCHECK                           1733
#define  INI_ENABLE_PDBREAK                           1734
#define  STR_COB_DDCHECK_MISMATCH                     1735
#define  NEED_PRINT_START_ADDRESS                     1736
#define  INI_BPDLG_START_ADDR                         1737
#define  INI_BPDLG_END_ADDR                           1738
/* From [01800] to [01849] */
#define  IE_GRPFILE_00                                1800
#define  IE_GRPFILE_01                                1801
#define  IE_GRPFILE_02                                1802
#define  IE_GRPFILE_03                                1803
#define  IE_GRPFILE_04                                1804
#define  IE_GRPFILE_05                                1805
#define  IE_GRPFILE_06                                1806
#define  IE_GRPFILE_07                                1807
#define  IE_GRPFILE_08                                1808
#define  IE_GRPFILE_09                                1809
#define  IE_GRPFILE_10                                1810

/* From [01850] to [01899] */
#define  CANNOT_ASSIGN_DOSENV                         1850
#define  CANNOT_ASSIGN_PARAM                          1851
#define  CANNOT_EVALUATE_MACRO_STRING                 1852
#define  CANNOT_MONITOR_MACRO_AS_STR                  1853
#define  CANNOT_ASSIGN_MACRO_STRING                   1854
#define  PARAM_STR_TOO_LONG                           1855
#define  INVALID_PARAM                                1856
#define  WEND_WITHOUT_WHILE                           1857
#define  IF_WITHOUT_ELSE_ENDIF                        1858
#define  ELSE_WITHOUT_ENDIF                           1859
#define  ENDIF_WITHOUT_IF_ELSE                        1860
#define  CANNOT_ASSIGN_MACRO_NON_STRING               1861

/* From [01900] to [01924] */
#define  IE_EVALUATE_00                               1900
#define  IE_EVALUATE_01                               1901
#define  IE_EVALUATE_02                               1902
#define  IE_EVALUATE_03                               1903
#define  IE_EVALUATE_04                               1904
#define  IE_EVALUATE_05                               1905

/* From [01925] to [01949] */
#define  IE_UTIL_00                                   1925
#define  IE_UTIL_01                                   1926

/* From [01950] to [2000] */
#define  MISSING_LPAREN_IN_EXPR                       1950
#define  CANNOT_EVALUATE_MACRO_ADDR                   1951
#define  CANNOT_EVALUATE_REG_ADDR                     1952

/* From [02001] to [02099] */
#define  AS_ACC                                       2001
#define  HL_ACC                                       2002
#define  RG_ACC                                       2003
#define  VA_ACC                                       2004
#define  MD1_ACC                                      2005
#define  MD2_ACC                                      2006
#define  MC_ACC                                       2007
#define  ST_ACC                                       2008
#define  ES_ACC                                       2009
#define  RM_ACC                                       2010
#define  BT_ACC                                       2011
#define  SF_ACC                                       2012
#define  TP_ACC                                       2013
#define  TC_ACC                                       2014
#define  ML_ACC                                       2015
#define  CD_ACC                                       2016
#define  UST_ACC                                      2017
#define  RT_ACC                                       2018
#define  CS_ACC                                       2019
#define  EE_ACC                                       2020
#define  CC_ACC                                       2021
/* [02022] to [02029] unused */

#define  PFW_TITLE                                    2030
/* [02031] unused */
#define  IN_DDC_MSG                                   2032
#define  LOAD_FILTER                                  2033
#define  SAVE_FILTER                                  2034
/* [02035] to [02036] unused */
#define  START_ADDRESS                                2037
#define  END_ADDRESS                                  2038
#define  EXIT_MSG                                     2039
#define  LOADS_MSG                                    2040
#define  LOADH_MSG                                    2041
#define  LOAD_MSG                                     2042
#define  SAVES_MSG                                    2043
#define  SAVEH_MSG                                    2044
#define  SAVE_MSG                                     2045
#define  ERASE_BKPT_MSG                               2046
#define  XDATA1_STRT_ADDRESS                          2047
#define  DISA_STRT_ADDRESS                            2048
#define  SRCE_STRT_ADDRESS                            2049
#define  PC_VALUE                                     2050
#define  MONITOR_VALUE                                2051
#define  VAR_WIN_NOT_OPEN                             2052
#define  NO_CFG_FILE                                  2053
#define  RUN_GRP_FILTER                               2054
#define  DEVICE_RESET                                 2055
#define  EMULATOR_RESET                               2056
#define  DISPLAY_FILTER                               2057
#define  LOAD_DISPLAY                                 2058
#define  SAVE_DISPLAY                                 2059
#define  INCORRECT_CFG_TITLE                          2060
#define  INCORRECT_CFG_MSG                            2061
#define  NEW_VARIABLE_VALUE                           2062
#define  NEW_REGISTER_VALUE                           2063
#define  XDATA2_STRT_ADDRESS                          2064
#define  CODE_STRT_ADDRESS                            2065
#define  IE_MEMORY_00                                 2066
#define  OFFSET_VAL                                   2067
/* [02068] to [02069] unused */

#define IE_VARTREE_00                                 2070
#define IE_VARTREE_01                                 2071
#define IE_VARTREE_02                                 2072
#define IE_VARTREE_03                                 2073
#define IE_VARTREE_04                                 2074

#define CANNOT_PRINT_ARRAY_COMP_TYPE                  2090
#define CANNOT_CONVERT_STRUC_TO_STR                   2091
/* From [02100] to [02199] */

#define  HELP_MF_SYSMENU                              2100
#define  HELP_SC_RESTORE                              2101
#define  HELP_SC_MOVE                                 2102
#define  HELP_SC_SIZE                                 2103
#define  HELP_SC_MINIMIZE                             2104
#define  HELP_SC_MAXIMIZE                             2105
#define  HELP_SC_CLOSE                                2106
#define  HELP_SC_TASKLIST                             2107
#define  HELP_SC_NEXTWINDOW                           2108
#define  HELP_SC_PREVWINDOW                           2109
/* [02110] to [02149] unused */
#define  TVCC_PRESENT_RES                             2150
#define  TVCC_ABSENT_RES                              2151
#define  NOCLOCK                                      2152
#define  PMODE_NORMAL                                 2153
#define  PMODE_POWERDOWN                              2154
#define  PMODE_IDLE                                   2155
#define  PMODE_COMMAND                                2156
#define  PROCESSOR_RUNNING                            2157
#define  PROCESSOR_STOPPED                            2158
#define  STR_STEP_OVER_CALL                           2159
#define  OUT_OF_SCOPE_STRING_WITH_EQUALS              2160
#define  OUT_OF_RANGE_STRING_WITH_EQUALS              2161
#define  DIV_BY_ZERO_RESULT                           2162
#define  RD_MEMORY_MAP                                2163
#define  SET_MEMORY_MAP                               2164
#define  RD_CLOCK_SOURCE                              2165
#define  SET_CLOCK_SOURCE                             2166
#define  RD_PROC_MODE                                 2167
#define  SET_PROC_MODE                                2168
#define  LOAD_DISKWARE                                2169
#define  NO_SRC_TO_DISPLAY                            2170
#define  SRC_WIN_TITLE                                2171
#define  NOT_ENOUGH_MEM_FOR_SYMS                      2172
#define  USER_INTERVENTION                            2173
#define  ITEM_DISABLED                                2174
#define  SPG_NOT_FOUND                                2175
#define  INITIALISING_TRACE                           2176
#define  TR_UPDATE                                    2177
#define  TR_FRAME                                     2178
#define  TR_BLOCK                                     2179
#define  TR_ADDRESS                                   2180
#define  TR_TRIGGER                                   2181
#define  TR_SOURCE                                    2182
#define  TR_NORMAL                                    2183
#define  TR_EXPANDED                                  2184
/* reserve [02186] to [02189] */
#define  JUMP_TO_FRAME                                2190
#define  JUMP_TO_BLOCK                                2191
#define  JUMP_TO_ADDRESS                              2192
#define  ADDRESS_SEARCH_MSG                           2193
#define  UPDATE_TO_VIEW_TRACE                         2194
#define  BUF_FILE_NOT_UPD                             2195
#define  OPTION_NOT_IMPLEMENTED                       2196
#define  OUT_OF_SCOPE                                 2197
#define  TRIGGER_SEARCH_MSG                           2198

/* From [02200] to [02299] */

#define  RG_ROW3                                      2203
#define  RG_ROW4                                      2204
#define  NO_RTOS_TO_DISPLAY                           2205
#define  MISSING_RTOS_SYMBOL                          2206
/* [02207] to [02220] unused */
#define  ESROW0                                       2221
#define  ES_RUN_TITLE                                 2222
#define  ES_STOP_TITLE                                2223
#define  SHORT_ESNAME                                 2224
#define  ES_ROW1                                      2225
#define  ES_ROW2                                      2226
#define  ESROW0_WITH_VCC                              2227
#define  ESROW0_WITHOUT_VCC                           2228
#define  ERR_DP_RAM_ADDR                              2229
#define  RMROW_0                                      2230
#define  BTROW_0                                      2231
#define  SFROW_0                                      2232
/* [02233] to [02244] unused */
#define  ERR_DP_VAR_OVERLAP                           2245
#define  STR_BP_PAGE                                  2246  
#define  ERR_INVALID_PAGE_RANGE                       2247  
#define  STR_RESET_STACK_ADDR                         2248

/* MOH_STARS_97 */
#define  INI_STARS_SECTION                            2249
#define  INI_BAR_COLOR_TIME                           2250
#define  INI_BAR_COLOR_MAX                            2251
#define  INI_BAR_COLOR_MIN                            2252
#define  INI_BAR_COLOR_COUNT                          2253
#define  INI_BAR_COLOR_AVER                           2254
#define  INI_BCK_COLOR_TIME                           2255
#define  INI_BCK_COLOR_MINMAX                         2256
#define  INI_BCK_COLOR_COUNT                          2257
#define  INI_BCK_COLOR_AVER                           2258
#define  INI_TXT_COLOR_TIME                           2259
#define  INI_TXT_COLOR_MINMAX                         2260
#define  INI_TXT_COLOR_COUNT                          2261
#define  INI_TXT_COLOR_AVER                           2262

/* [02249] to [02300] unused */

/* From [02300] to [02399] */

#define  INI_FILE_SECTION                             2301
#define  INI_CONTROL_BAR                              2302
#define  INI_STATUS_BAR_HELP                          2303
#define  INI_EDITOR                                   2304
#define  INI_NOTEPAD                                  2305
#define  INI_SVAS_PROG                                2306
#define  INI_LD_PROG                                  2307
#define  INI_SVAS_PROG_FILTER                         2308
#define  INI_LD_PROG_FILTER                           2309
#define  INI_RUN_GROUP                                2310
#define  INI_RUN_GROUP_FILTER                         2311
#define  INI_A_MEMFORSYMS                             2312
#define  INI_A_STMTS                                  2313
#define  INI_A_SYMS                                   2314
#define  INI_STATUS_BAR                               2315
#define  INI_ENABLE_3D                                2316
#define  INI_LD_TRACE                                 2317
#define  INI_SVAS_TRACE                               2318
#define  INI_LOG_TRACE                                2319
#define  INI_LOAD_TRIGGER                             2320
#define  INI_SVAS_TRIGGER                             2321
#define  INI_TRACK_AS_WIN                             2322
#define  INI_TRACK_HL_WIN                             2323
#define  INI_TRACK_TP_WIN                             2324
#define  INI_CODE_BROWSE                              2325
#define  INI_COMPORT                                  2326
#define  INI_FONT_SECTION                             2327
#define  INI_FONT_HEIGHT                              2328
#define  INI_FONT_WIDTH                               2329
#define  INI_FONT_CHARSET                             2330
#define  INI_FONT_PITCH                               2331
#define  INI_FONT_FACE                                2332
#define  INI_LD_BANKCONFIG                            2333
#define  INI_FONT_WEIGHT                              2334
#define  INI_LD_DISPLAY                               2335
#define  INI_SVAS_DISPLAY                             2336
#define  INI_SVAS_BANKCONFIG                          2337
#define  INI_CONTROL_BAR_HELP                         2338
#define  INI_COCO_MODE                                2339
#define  INI_COCO_LOG_TYPE                            2340
#define  INI_COCO_LOG_FORMAT                          2341
#define  INI_STARTTRIG_COL                            2342
#define  INI_STOPTRIG_COL                             2343
#define  INI_CLEAR_BPS_ON_FILE_LOAD                   2344
#define  INI_CALL_STACK_PARMS                         2345
#define  INI_LD_VARIABLES                             2346
#define  INI_SVAS_VARIABLES                           2347
#define  P85W8532_CONFIG_SECTION                      2348
#define  P8WE5000_CONFIG_SECTION                      2349
#define  ACTIVATING_TRIG                              2350
#define  TRIGGER_ADDRESS                              2351
#define  TRACE_FILTER                                 2352
#define  LOADING_TRACE                                2353
#define  SAVING_TRACE                                 2354
#define  LOGGING_TRACE                                2355
#define  LOAD_TRACE                                   2356
#define  SAVE_TRACE                                   2357
#define  LOG_FILTER                                   2358
#define  START_POSITION                               2359
#define  END_POSITION                                 2360
#define  EMULATING                                    2361
#define  STR_XDATA_READ                               2362
#define  STR_XDATA_WRITE                              2363
#define  STR_CODE_ACCESS                              2364
#define  STR_OPCODE_FETCH                             2365
#define  STR_ANY_ACCESS                               2366
#define  STR_NEVER                                    2367
#define  SYMBOL                                       2368
#define  BINARY                                       2369
#define  DONTCARE                                     2370
#define  TABLE                                        2371
#define  FUNCTION                                     2372
#define  STR_A_ONLY                                   2373
#define  STR_B_ONLY                                   2374
#define  STR_A_AND_B                                  2375
#define  STR_A_AND_NOT_B                              2376
#define  STR_A_OR_B                                   2377
#define  STR_A_OR_NOT_B                               2378
#define  STR_A_THEN_B                                 2379
#define  STR_A_THEN_NOT_B                             2380
#define  INVALID_SYM_IN_TRIG_FILE                     2381
#define  TRIG_START_STOP                              2382
#define  COUNT_EV_BEFORE_TRG                          2383
#define  THEN                                         2384
#define  ALSO                                         2385
#define  START_TRG_ONLY                               2386
#define  EVERY_START_TRG                              2387
#define  NOT_ENABLED                                  2388
#define  HALT_EMULATION                               2389
#define  NOT_HALT_EMULATION                           2390
#define  HALT_TRACE_ACQ                               2391
#define  NOT_HALT_TRACE_ACQ                           2392
#define  ERROR_IN_LOAD_CONFIG_FILE                    2393
#define  LOAD_CONFIG                                  2394
#define  SAVE_CONFIG                                  2395
#define  PROC_CFG_DLG_TITLE                           2396
#define  TB_BLOCK_1                                   2397
#define  TB_BLOCK_8                                   2398
#define  TB_BLOCK_128                                 2399

/* From [02400] to [02499] */

#define  TB_CONTINUOUS                                2400
#define  START_TRACE_CNT                              2401
#define  STOP_TRACE_CNT_STRG                          2402
#define  STOP_TRACE_CNT                               2403
#define  START_TRACE_BLK                              2404
#define  STOP_TRACE_BLK                               2405
#define  BLOCK_SIZE_EXCEEDED                          2406
#define  TRACE_START_STOP                             2407
#define  LINESTRACED                                  2408
#define  TGL17_51ROM                                  2409
#define  TGL17_51ROMLESS                              2410
#define  TGL17_552ROM_0                               2411
#define  TGL17_552ROMLESS                             2412
#define  TGL17_515ROM_0                               2413
#define  TGL17_652ROM_0                               2414
#define  TGL17_P02OPT                                 2415
#define  TGL17_515ROMLESS_0                           2416
#define  TGL17_5371                                   2417
#define  TGL17_EXT                                    2418
#define  TRIGGER_FILTER                               2419
#define  SAVE_TRIGGER                                 2420
#define  LOAD_TRIG_MSG                                2421
#define  SAVE_TRIG_MSG                                2422
#define  AND_EXT1                                     2423
#define  AND_EXT2                                     2424
#define  UNDEF_SYMBOL_OR_ADDR                         2425
#define  ILLEGAL_ADDRESS                              2426
#define  ILLEGAL_OFFSET                               2427
#define  UNDEFINED_SYMBOL                             2428
#define  DATA_TOO_LARGE                            2429
#define  INVALID_CHAR                                 2430
#define  INVALID_LEN                                  2431
#define  EVA_ADDR                                     2432
#define  EVB_ADDR                                     2433
#define  EVA_DATA                                     2434
#define  EVB_DATA                                     2435
#define  DO_NOT_CARE                                  2436
#ifndef _H2INC         /* PFWXA-PJS-13-MAY-1995 */
#define  LOW                                          2437
#define  HIGH                                         2438
#endif                 /* PFWXA-PJS-13-MAY-1995 */
#define  START_EXC_STOP                               2439
#define  RANGE_START_ADDRESS                          2440
#define  RANGE_END_ADDRESS                            2441
#define  NO_FUNCTIONS                                 2442
#define  NO_MODULES                                   2443
#define  MA_LIR                                       2444
#define  MA_READ                                      2445
#define  MA_WRITE                                     2446
#define  MA_ALL                                       2447
#define  MA_NEVER                                     2448
#define  LINES_TRACED_PORT_OPT                        2449
#define  NO_BANKS                                     2450
#define  NEVER_HAPPEN                                 2451
#define  TARGET_CLOCK_WARN                            2452
#define  BANK_NUM                                     2453
#define  FILE_NAME                                    2454
#define  INVALID_CMDLINE_ARG                          2455
#define  STR_CLOCK_A                                  2456
#define  STR_CLOCK_B                                  2457
#define  STR_CLOCK_C                                  2458
#define  STR_CLOCK_T                                  2459
#define  STR_CLOCK                                    2460
#define  LOAD_BANKCONFIG                              2461
#define  SAVE_BANKCONFIG                              2462
#define  BANKCONFIG_FILTER                            2463
#define  STR_CLOCK_P                                  2464
#define  STR_PROBE                                    2465
#define  STR_ADDR_RANGE_TABLE                         2466
#define  CONFIG_FILTER                                2467
#define  EXCEED_MAX_REG_VALUE                         2468
#define  VALUE_CANNOT_BE_ZERO                         2469 /* BLK_MODE_20-JAN-1997_MOH */
// MOH-03-06-97
#define  CANT_MAP_FROM_ONCHIP_CODE                    2470
#define  CANT_MAP_FROM_ONCHIP_XDATA                   2471
#define  SYMBOL_WORD                                  2472
#define  BINARY_WORD                                  2473
#define  DONTCARE_WORD                                2474
#define  SYMBOL_BYTE                                  2475
#define  BINARY_BYTE                                  2476
#define  DONTCARE_BYTE                                2477
#define  BYTE_DATA_TOO_LARGE                          2478
#define  EXCLUDE_P3P7                                 2479
#define  EXCLUDE_EXT                                  2480
/* [02485] to [02599] unused */
#define  INI_P85W8532_FFC7                            2481
#define  INI_P85W8532_FFC8                            2482
#define  INI_P85W8532_FFC9                            2483
#define  INI_P8WE5000_BANKTYPE                        2484
#define  INI_P8WE5000_FIXED_LBS                       2485
#define  INI_P8WE5000_FIXED_HBS                       2486
#define  INI_P8WE5000_AUTO_ENABLE_BANK                2487

/* From [02500] to [02599] */


/* From [02600] to [02699] */

#define  W_START                                      2600
#define  T_CLOCK_ABSENT_W                             2601
#define  BKPS_SOMEWHERE_IN_LINE_W                     2602
#define  BP_ON_SBUF_REGISTER                          2603
#define  COCO_DISABLED_MSG                            2604
#define  COCO_ENABLED_MSG                             2605
#define  DISABLE_COCO_MESSAGE                         2606
#define  ENABLE_COCO_ON_CTF_MESSAGE                   2607
#define  ENAB_RM_BPS_MSG                              2608
#define  MUST_DISABLE_RAM_BP                          2609
#define  RESET_COCO_MESSAGE                           2610
#define  RESET_COCO_WARNING                           2611
#define  WAIT_FOR_COCO_DISABLE                        2612
#define  WAIT_FOR_COCO_ENABLE                         2613
#define  WAIT_FOR_COCO_LOG                            2614
#define  WAIT_FOR_COCO_RESET                          2615
#define  WAIT_FOR_COCO_SAVE                           2616
#define  WAIT_FOR_ONCHIP_SETUP                        2617
#define  WRONG_SW_VERSION                             2618
#define  ENABLE_COCO_ON_CTG_MESSAGE                   2619
#define  CANNOT_DISP_43_LINES_E                       2620
#define  WAIT_FOR_COCO_DATA                           2621  
#define  DATA_BEING_LOGGED                            2622
#define  NO_FAST_BREAK_RANGES                         2623
#define  IDEAS_MISSING                                2624
/*
MAX_WMSG_NUM is not a message, its used to size message arrays for PF-DOS
(see msg.h)
*/
#define  MAX_WMSG_NUM                                 2625


#define  INVALID_RANGE                                2626
#define  DIFFERENT_BANKS                              2627
#define  ILLEGAL_BANK                                 2628
#define  COCO_DEFAULT_ERR_MSG                         2629
#define  COCO_ENABLED_MESSAGE                         2630
#define  COCO_DISABLED_MESSAGE                        2631
#define  WAIT_FOR_COCO_LOAD                           2632
#define  JUMP_TO_MODULE                               2633
#define  SPECIFY_ADDRESS_RANGE                        2634
#define  LOG_COCO_FILTER                              2635
#define  INI_COCO_SECTION                             2636
#define  INI_MARK_CHAR                                2637
#define  INI_PART_MARK_CHAR                           2638
#define  INI_UNTESTED_MARK_CHAR                       2639
#define  INI_UNTESTED_PART_MARK_CHAR                  2640
#define  INI_SEVEN_BIT_ASCII                          2641
#define  INI_SAVE_COCO_BINARY                         2642
#define  INI_LOAD_COCO_BINARY                         2643
#define  INI_LOG_COCO_REPORT                          2644
#define  INI_ON                                       2645
#define  INI_OFF                                      2646
#define  INI_SEA_SECTION                              2647
#define  NO_COCO_DATA                                 2648
#define  RESET_BEFORE_LOAD_COCO_MESSAGE               2649
#define  INI_AUTO_COCO                                2650
#define  INI_AUTO_COCO_SAVE                           2651
#define  NO_FNS_LOG_ANYWAY                            2652
#define  NOT_IN_MODULE_SCOPE                          2653
#define  INI_COCO_MARK_UNTESTED                       2654
#define  INI_COCO_MARK_COMMENTS                       2655
#define  INI_COCO_INCLUSIVE_RANGES                    2656
#define  SAVE_COCO_PROMPT                             2657
#define  INI_COCO_USE_DEFAULT_MARK                    2658
#define  WAIT_FOR_BANKED_COCO_ENABLE                  2659
#define  NO_EXCL_CODE_IN_MODL                         2660
#define  NO_RANGES_COVERED_IN_MODL                    2661
#define  EXCL_RANGE_NOT_COVERED                       2662
#define  NO_RANGES_COVERED_IN_FUNC                    2663
#define  DISABLE_SAVE_COCO_MSG                        2664
#define  RESET_SAVE_COCO_MSG                          2665
#define  NO_RANGES_COVERED                            2666
#define  BIN_COCO_FILTER                              2667
#define  CODE_COVERAGE                                2668
#define  WRONG_COVERAGE_MODE                          2669
#define  CANNOT_LOAD_COCO_FILE                        2670
#define  WRONG_COCO_FILE_TYPE                         2671
/* From [02672] to [02700] */
#define  INI_COUNT_BAR                                2701    
#define  INI_EXEC_TIME_BAR                            2702    
#define  INI_LOG_SEA_DATA                             2703    
#define  INI_MAX_BAR                                  2704    
#define  INI_MIN_BAR                                  2705    
#define  INI_METER_BKGND                              2729    
#define  INI_SEA_CONTROL_PANEL                        2706    
#define  INI_SEA_FT_OUTPUT                            2707    
#define  INI_SEA_PA_OUTPUT                            2708    
/* [02709]  unused */
#define  INI_SEA_SETUP                                2710    
#define  INI_SEA_WINDOWS_TIMER                        2711    
#define  INI_SEA_WINDOWS_TIMER_ENABLE                 2712    
#define  INVALID_FT_FRAME                             2713    
#define  LOAD_SEA_SETUP                               2714    
#define  MAX_NUM_FNS_REACHED                          2715    
#define  SAVE_SEA_DATA                                2716    
#define  SAVE_SEA_SETUP                               2717    
#define  SEA_DATA_FILTER                              2718    
#define  SEA_FT_FILTER                                2719    
#define  SEA_SETUP_FILTER                             2720    
#define  WAIT_FOR_SEA_LOG                             2721    
#define  EVENT_NOT_FOUND                              2722    
#define  INITIALISING_SEA                             2723    
#define  INI_SEA_ADDRPAIRS                            2724    
#define  LOADING_SEA_EVENTS                           2725    
#define  NO_MORE_SEA_BINS                             2726    
#define  NO_SEA_FT_OPTION                             2727    
#define  RE_USE_ADDR_MSG                              2728    
#define  SEA_SETUP_FILE_CORRUPT                       2730    
#define  SEA_TITLE                                    2731    
#define  JUMP_TO_FRAME_STR                            2732  
#define  JUMP_TO_TIME_STR                             2733  
#define  INVALID_EXPR                                 2734  
#define  SMALL_TIME                                   2735  
#define  BIG_TIME                                     2736  
#define  ENTRY_SEARCH_ANYWAY                          2737  
#define  EXIT_SEARCH_ANYWAY                           2738  
#define  NO_EVENT_AT_ADDRESS                          2739  
#define  START_TRIG_ADDR_USED                         2740  
#define  STOP_TRIG_ADDR_USED                          2741  
#define  START_USED_AS_STOP                           2742  
#define  STOP_USED_AS_START                           2743  
#define  INVALID_WORD_DATA_OFFCHIP_ADDR               2744  
#define  INVALID_WORD_DATA_ODD_ADDR                   2745
#define  F_BUF_FILE_READONLY                          2746
                                                        
/* [02747] to [02799] unused */

/* From [02800] to [02899] */


/* From [02900] to [02999] */


/* From [03000] to [03099] */


/* From [03100] to [03199] */


/* From [03200] to [03299] */


/* From [03300] to [03399] */

#define  INI_CODE_TRACKING                            3337
#define  INI_EMUL_POLL_INTERVAL                       3338
#define  INI_LD_CONFIG                                3339
#define  INI_SVAS_CONFIG                              3340
#define  INI_HILIGHT_COL                              3341
#define  INI_FIXEDTEXT_COL                            3342
#define  INI_TAB_TO_SPACE                             3343
#define  INI_HL_WIN_TAB_TO_SPACE                      3344
#define  INI_USE_TAB_STOPS                            3345
#define  INI_HL_WIN_TAB_STOPS                         3346

/* [03347] to [03349] unused */

/* From [03350] to [03399] */

#define  INI_MAP_SECTION                              3350     
#define  INI_CODE_MAP_RES                             3351     
#define  INI_XDATA_MAP_RES                            3352     
#define  INI_LD_MAP                                   3353     
#define  INI_SVAS_MAP                                 3354     
#define  INI_BROWSE_MAP                               3355     
#define  LOAD_MAP                                     3356     
#define  SAVE_MAP                                     3357     
#define  BROWSE_MAP                                   3358     
#define  MAP_FILTER                                   3359     
#define  STR_MAP_RES_WARN_MSG                         3360     
#define  STR_DUP_MAP_WARN_MSG                         3361     
#define  STR_TITLE_MEMMAP_DLG                         3362     
#define  STR_TITLE_GRP_MAP_MEMORY                     3363     
#define  STR_TITLE_GRP_MAP_EMUL_MEMORY                3364     
#define  STR_TITLE_TEXT_MAP_CURR_MAPPING              3365     
#define  STR_TITLE_TEXT_MAP_RESOLUTION                3366     
#define  ERR_MAP_FILE_CORRUPT                         3367     
#define  ERR_MAP_FILE_INVALID                         3368     
#define  ERR_MAP_FILE_MEM_XDATA                       3369     
#define  ERR_MAP_FILE_MEM_CODE                        3370     
#define  ERR_MAP_FILE_BAD_EMULMEM                     3371     
#define  IE_LDSVMAP00                                 3372     
#define  IE_MAPUTIL00                                 3373     
#define  IE_MAPUTIL01                                 3374     
#define  IE_MAPUTIL02                                 3375     
#define  IE_MAPUTIL03                                 3376     
#define  IE_MAPUTIL04                                 3377     
#define  IE_MAPUTIL05                                 3378     
#define  IE_MAPUTIL06                                 3379     
#define  IE_MAPUTIL07                                 3380     
#define  IE_MAPUTIL08                                 3381     
#define  ERR_IC_SIMM_OVERLAP                          3382     
#define  ERR_MEMPOS_DISABLED                          3383     
#define  ERR_INVALID_MEMPOS_IC_START                  3384     
#define  ERR_INVALID_MEMPOS_SIMM_START                3385     
#define  STR_IC_MEM                                   3386     
#define  STR_SIMM_MEM                                 3387     
#define  INI_SEARCH_SECTION                           3388
#define  INI_SEARCH_SOURCE_POSITION                   3389
#define  INI_SEARCH_SOURCE_STRING                     3390
#define  INI_SEARCH_SOURCE_CASE_SENSITIVE             3391
#define  INI_SEARCH_SOURCE_KEEP_DIALOG_OPEN           3392
#define  INI_SEARCH_SOURCE_FORWARD                    3393
#define  INI_SEARCH_TRACEADDR_POSITION                3394
#define  INI_SEARCH_TRACEADDR_ADDRESS                 3395
#define  INI_SEARCH_TRACEADDR_KEEP_DIALOG_OPEN        3396
#define  INI_SEARCH_TRACEADDR_FORWARD                 3397
#define  INI_SEARCH_TRIGGER_POSITION                  3398
#define  INI_SEARCH_TRIGGER_TYPE                      3399

/* From [03400] to [03499] */

#define  INI_SEARCH_TRIGGER_KEEP_DIALOG_OPEN          3400
#define  INI_SEARCH_TRIGGER_FORWARD                   3401

/* [03402] to [03499] unused */


/* From [03500] to [03599] */


/* From [03600] to [03699] */


/* From [03700] to [03799] */


/* From [03800] to [03899] */


/* From [03900] to [03999] */


/* From [04000] to [04099] */

/*
S_START is not a message, its used to size message arrays for PF-DOS
(see msg.h)
*/
#define  S_START                                      4000
#define  LST_OLDER_THAN_SRC_M                         4001
#define  EXIT_IN_LINE_ASS_M                           4002
#define  ESC_TO_EXIT_M                                4003
#define  MOVE_WIN_M                                   4004
#define  MOVE_WIN_HELP_M                              4005
#define  HOW_TO_EXIT_M                                4006
#define  SIZE_WIN_M                                   4007
#define  SIZE_WIN_HELP_M                              4008
#define  NO_WINS_ON_SCREEN_M                          4009
#define  WDE_MODE_M                                   4010
#define  WDE_HELP1_M                                  4011
#define  WDE_HELP2_M                                  4012
#define  WIN_COLOUR_SELECT_M                          4013
#define  NO_CHANGE_TO_SCREEN_M                        4014
#define  FILE_LOAD_M                                  4015
#define  PU_TOGGLE_ITEM_M                             4016
#define  PU_SEL_ITEM_M                                4017
#define  PGDN_MORE_M                                  4018
#define  PGUP_MORE_M                                  4019
#define  PGUP_PGDN_MORE_M                             4020
#define  WDE_UPD_ON_M                                 4021
#define  WDE_UPD_OFF_M                                4022
#define  NO_BREAKPOINT_FOUND                          4023
#define  NO_HLDB                                      4024
#define  READING_MAP_WAIT                             4025
#define  Y_OR_N_TO_QUIT                               4026
#define  MAP_CONF_WAIT_MESSAGE                        4027
#define  WAIT_FOR_HALT_MSG                            4028
#define  ASM_DEFINED_M                                4029
#define  PLM_DEFINED_M                                4030
#define  LIB_DEFINED_M                                4031
#define  C_DEFINED_M                                  4032
/*
MAX_SMSG_NUM is not a message, its used to size message arrays for PF-DOS
(see msg.h)
*/
#define  MAX_SMSG_NUM                                 4032
#define IE_VARTREE_E_1                                4033
#define IE_VARTREE_E_2                                4034
#define IE_VARTREE_E_3                                4035
#define VARIABLE_NAME_TOO_LONG                        4036
#define IE_VARTREE_E_4                                4037
#define IE_VARTREE_E_5                                4038
#define IE_VARTREE_E_6                                4039
#define IE_VARTREE_E_7                                4040
#define IE_VARTREE_E_8                                4041
#define ELEMENT_NOT_STRUCT_MEMBER                     4042
#define POSSIBLE_SOURCE_IN_HEADER_FILE                4043
/* [04044] to [04999] unused */

/* From [04100] to [04199] */


/* From [04200] to [04299] */


/* From [04300] to [04399] */


/* From [04400] to [04499] */


/* From [04500] to [04599] */


/* From [04600] to [04699] */


/* From [04700] to [04799] */


/* From [04800] to [04899] */


/* From [04900] to [04999] */


/* From [05000] to [05099] */

#define  HL_NOT_A_STMT_LINE_E                         5000
/* [05001] unused */
#define  CANNOT_DISP_BPS_IN_CODE_E                    5002
/* [05003] to [05999] unused */

/* From [05100] to [05199] */


/* From [05200] to [05299] */


/* From [05300] to [05399] */


/* From [05400] to [05499] */


/* From [05500] to [05599] */


/* From [05600] to [05699] */


/* From [05700] to [05799] */


/* From [05800] to [05899] */


/* From [05900] to [05999] */


/* From [06000] to [06099] */


/* From [06100] to [06199] */


/* From [06200] to [06299] */


/* From [06300] to [06399] */


/* From [06400] to [06499] */


/* From [06500] to [06599] */


/* From [06600] to [06699] */


/* From [06700] to [06799] */


/* From [06800] to [06899] */


/* From [06900] to [06999] */


/* From [07000] to [07099] */


/* From [07100] to [07199] */


/* From [07200] to [07299] */


/* From [07300] to [07399] */


/* From [07400] to [07499] */


/* From [07500] to [07599] */


/* From [07600] to [07699] */


/* From [07700] to [07799] */


/* From [07800] to [07899] */


/* From [07900] to [07999] */


/* From [08000] to [08099] */


/* From [08100] to [08199] */


/* From [08200] to [08299] */


/* From [08300] to [08399] */


/* From [08400] to [08499] */


/* From [08500] to [08599] */


/* From [08600] to [08699] */


/* From [08700] to [08799] */


/* From [08800] to [08899] */


/* From [08900] to [08999] */


/* From [09000] to [09099] */


/* From [09100] to [09199] */


/* From [09200] to [09299] */


/* From [09300] to [09399] */


/* From [09400] to [09499] */


/* From [09500] to [09599] */


/* From [09600] to [09699] */


/* From [09700] to [09799] */


/* From [09800] to [09899] */


/* From [09900] to [09999] */


/* From [10000] to [10099] */

#define  IE_DISPMSG_BOX_WRITE_1                       10001
#define  IE_DISPMSG_BOX_WRITE_2                       10002
#define  IE_DISPMSG_BOX_WRITE_3                       10003
#define  IE_DISPMSG_BOX_WRITE_4                       10004
#define  IE_DISPMSG_BOX_WRITE_5                       10005
#define  IE_DISPMSG_BOX_WRITE_6                       10006
#define  IE_DISPMSG_BOX_WRITE_7                       10007
#define  IE_DISPMSG_BOX_WRITE_8                       10008
#define  IE_DISPMSG_DISP_COMP_MSG_1                   10009
#define  IE_DISPMSG_FMESSAGE_1                        10010
#define  IE_DISPMSG_DISP_FMSG_1                       10011
#define  IE_RTFILL00                                  10012   
#define  IE_RTFILL01                                  10013   
#define  IE_RTFILL02                                  10014   
/* [10015] to [10100] unused */

/* From [10100] to [10199] */

#define  IE_CURSFNS_LCURPUSH_1                        10101
#define  IE_CURSFNS_LCURPOP_1                         10102
/* [10103] unused */
#define  IE_DWIN_DWIN_ENTRY_1                         10104
#define  IE_DWIN_DWIN_ENTRY_2                         10105
#define  IE_DWIN_DWIN_ENTRY_3                         10106
#define  IE_DWIN_DWIN_DELETE_1                        10107
#define  IE_DWIN_DWIN_DELETE_2                        10108
#define  IE_DWIN_DWIN_DELETE_3                        10109
#define  IE_GETNTRY_USR_STRING_ENTRY_1                10110
#define  IE_GETNTRY_USR_STRING_ENTRY_2                10111
#define  IE_GETNTRY_USR_STRING_ENTRY_3                10112
#define  IE_GETNTRY_USR_STRING_ENTRY_4                10113
#define  IE_GETNTRY_DISP_STR_1                        10114
#define  IE_GETNTRY_DISP_STR_2                        10115
#define  IE_VBORDER_VFRAME_1                          10116
#define  IE_VBORDER_VFRAME_2                          10117
#define  IE_VBORDER_VTITLE_1                          10118
#define  IE_VCHANGE_VZOOM_1                           10119
#define  IE_VCHANGE_VZOOM_2                           10120
#define  IE_VKILL_VDELETE_1                           10121
#define  IE_VKILL_VDELETE_2                           10122
#define  IE_VKILL_VDELETE_3                           10123
#define  IE_VKILL_VDELETE_4                           10124
#define  IE_VKILL_VDELETE_5                           10125
#define  IE_VKILL_VDELETE_6                           10126
#define  IE_VKILL_VDELETE_7                           10127
#define  IE_VMAKE_VCREAT_1                            10128
#define  IE_VMAKE_VCREAT_2                            10129
#define  IE_VMAKE_VCREAT_3                            10130
#define  IE_VMAKE_VCREAT_4                            10131
#define  IE_VMAKE_VCREAT_5                            10132
#define  IE_VMAKE_VCREAT_6                            10133
#define  IE_VMAKE_VCREAT_7                            10134
#define  IE_VUPDPSP__VFRWMEM_1                        10135
#define  IE_WINFUNC_WINFRAME_1                        10136
#define  IE_WINFUNC_PCURS_LINE_1                      10137
#define  IE_WINFUNC_DEL_PCURS_1                       10138
#define  IE_WINFUNC_DRAW_PCURS_1                      10139
#define  IE_WINFUNC_NEW_LINE_1                        10140
#define  IE_WINFUNC_ADD_BASE_TO_TITLE_1               10141
#define  IE_WINFUNC_MAKEPICT_1                        10142
#define  IE_CMDLINE_CL_WRITECH_1                      10143
#define  IE_CMDLINE_CL_WRITEBACKSPACE_1               10144
#define  IE_CMDLINE_WINDOWS_MODE_1                    10145
#define  IE_CMDLINE_WINDOWS_MODE_2                    10146
#define  IE_CMDLINE_WINDOWS_MODE_3                    10147
#define  IE_DWIN_DWIN_MESSAGE_1                       10148
#define  IE_DWIN_DWIN_FRAME_ENTRY_1                   10149
#define  IE_DWIN_DWIN_TEXT_1                          10150
#define  IE_DWIN_DWIN_TOGGLE_ENTRY_1                  10151
#define  IE_DWIN_BOX_FILE_NAME_1                      10152
#define  IE_DWIN_WRCHR_NODBOX                         10153
#define  IE_DWIN_DWIN_ENTRY_4                         10154
/* [10155] to [10400] unused */

/* From [10200] to [10299] */


/* From [10300] to [10399] */


/* From [10400] to [10499] */

#define  IE_REGWIN_UPD_REG_VALS                       10401
#define  IE_BTWIN_FILL_BT_WIN_1                       10402
#define  IE_MDWIN_FILL_MD_WIN_1                       10403
#define  IE_MDWIN_FILL_MD_WIN_2                       10404
#define  IE_MDWIN_MD_SCROLL_1                         10405
#define  IE_MDWIN_MD_SCROLL_2                         10406
#define  IE_MDWIN_MD_SCROLL_3                         10407
#define  IE_RMWIN_FILL_RM_WIN_1                       10408
#define  IE_SFRWIN_FILL_SFR_WIN_1                     10409
#define  IE_SFRWIN_FILL_SFR_WIN_2                     10410
#define  IE_SFRWIN_FILL_SFR_WIN_3                     10411
#define  IE_SFRWIN_FILL_SFR_WIN_4                     10412
#define  IE_SFRWIN_FILL_SFR_WIN_5                     10413
#define  IE_SFRWIN_FILL_SFR_WIN_6                     10414
#define  IE_SFRWIN_FILL_SFR_WIN_7                     10415
#define  IE_SFRWIN_FILL_SFR_WIN_8                     10416
#define  IE_SFRWIN_FILL_SFR_WIN_9                     10417
#define  IE_SFRWIN_FILL_SFR_WIN_10                    10418
#define  IE_CONFWIN_SET_MAP_STRING                    10419
#define  IE_CONFWIN_FILL_CONFWIN                      10420
#define  IE_STWIN_ST_SCROLL                           10421
#define  IE_INITWIN_INIT_NON_RES_1                    10422
#define  IE_INITWIN_INIT_NON_RES_2                    10423
#define  IE_RMWIN_RM_SCROLL_1                         10424
/* [10425] to [10492] unused */
#define  IE_INITWIN_INIT_SDT_6                        10493
#define  IE_INITWIN_INIT_SDT_5                        10494
#define  IE_INITWIN_INIT_SDT_4                        10495
#define  IE_SCRNPROC_SAV_SCRN_DES_7                   10496
#define  IE_SCRNPROC_SAV_SCRN_DES_6                   10497
#define  IE_SCRNPROC_SAV_SCRN_DES_5                   10498
#define  IE_CHGWDISP_WIN_MENU_1                       10499

/* From [10500] to [10599] */

#define  IE_CHGWDISP_WIN_MENU_2                       10500
#define  IE_CHGWDISP_WIN_MENU_3                       10501
#define  IE_CHGWDISP_WINSIZ_1                         10502
#define  IE_CHGWDISP_STUP_WIN_1                       10503
#define  IE_CHGWDISP_STUP_WIN_2                       10504
#define  IE_CHGWDISP_STUP_WIN_3                       10505
#define  IE_CHGWDISP_STUP_WIN_4                       10506
#define  IE_CHGWDISP_STUP_WIN_5                       10507
#define  IE_CHGWDISP_STUP_WIN_6                       10508
#define  IE_CHGWDISP_STUP_WIN_7                       10509
#define  IE_CHGWDISP_STUP_WIN_8                       10510
#define  IE_CHGWDISP_STUP_WIN_9                       10511
#define  IE_CHGWDISP_STUP_WIN_10                      10512
#define  IE_CHGWDISP_STUP_WIN_11                      10513
#define  IE_CHGWDISP_WINMOV_1                         10514
#define  IE_ENVFNS_INV_SPS_ARG_1                      10515
#define  IE_ENVFNS_FREE_INVOC_ARGS_1                  10516
#define  IE_FBOX_PD_FLOAD_1                           10517
#define  IE_FBOX_PD_FLOAD_2                           10518
#define  IE_FBOX_PD_FLOAD_3                           10519
#define  IE_FBOX_PD_FSAVE_1                           10520
#define  IE_FBOX_PD_FSAVE_2                           10521
#define  IE_FBOX_PD_FSAVE_3                           10522
#define  IE_FBOX_PD_FSAVE_4                           10523
#define  IE_FBOX_PD_FSAVE_5                           10524
#define  IE_FBOX_PD_FSAVE_6                           10525
#define  IE_FBOX_PD_FSAVE_7                           10526
#define  IE_FBOX_PD_FSAVE_8                           10527
#define  IE_FBOX_PD_FMODULE_1                         10528
#define  IE_FBOX_PD_FMODULE_2                         10529
#define  IE_FBOX_PD_FADDR_1                           10530
#define  IE_FBOX_PD_FADDR_2                           10531
#define  IE_FBOX_PD_FSTRING_1                         10532
#define  IE_FBOX_PD_FSTRING_2                         10533
#define  IE_INITWIN_WCPYSHELL_1                       10534
#define  IE_INITWIN_W_FIXED_1                         10535
#define  IE_INITWIN_WDSINIT_1                         10536
#define  IE_INITWIN_WDSINIT_2                         10537
#define  IE_INITWIN_WDSINIT_3                         10538
#define  IE_INITWIN_WDSINIT_4                         10539
#define  IE_INITWIN_WDSINIT_5                         10540
#define  IE_INITWIN_WDSINIT_6                         10541
#define  IE_INITWIN_WDSINIT_7                         10542
#define  IE_INITWIN_W_INIT_1                          10543
#define  IE_INITWIN_INIT_SDT_1                        10544
#define  IE_INITWIN_INIT_SDT_2                        10545
#define  IE_INITWIN_INIT_SDT_3                        10546
#define  IE_SCRNPROC_WINS_IN_SDT_1                    10547
#define  IE_SCRNPROC_WINS_IN_SDT_2                    10548
#define  IE_SCRNPROC_WINS_IN_SDT_3                    10549
#define  IE_SCRNPROC_WINS_IN_SDT_4                    10550
#define  IE_SCRNPROC_WINS_IN_SDT_5                    10551
#define  IE_SCRNPROC_WINS_IN_SDT_6                    10552
#define  IE_SCRNPROC_SETUP_SCRN_DES_1                 10553
#define  IE_SCRNPROC_SETUP_SCRN_DES_2                 10554
#define  IE_SCRNPROC_SETUP_SCRN_DES_3                 10555
#define  IE_SCRNPROC_SETUP_SCRN_DES_4                 10556
#define  IE_SCRNPROC_SETUP_SCRN_DES_5                 10557
#define  IE_SCRNPROC_SETUP_SCRN_DES_6                 10558
#define  IE_SCRNPROC_SETUP_SCRN_DES_7                 10559
#define  IE_SCRNPROC_SETUP_SCRN_DES_8                 10560
#define  IE_SCRNPROC_SAV_SCRN_DES_1                   10561
#define  IE_SCRNPROC_SAV_SCRN_DES_2                   10562
#define  IE_SCRNPROC_SAV_SCRN_DES_3                   10563
#define  IE_SCRNPROC_SAV_SCRN_DES_4                   10564
#define  IE_SCRNPROC_FREE_SDT_1                       10565
#define  IE_SCRNPROC_FREE_SDT_2                       10566
#define  IE_SCRNPROC_FREE_SDT_3                       10567
#define  IE_SCRNPROC_FREE_SDT_4                       10568
#define  IE_LDWWPROC_LD_DISKWARE_1                    10569
#define  IE_LDWWPROC_LD_DISKWARE_2                    10570
#define  IE_LDWWPROC_LD_DISKWARE_3                    10571
#define  IE_LDWWPROC_LD_DISKWARE_4                    10572
#define  IE_LDWWPROC_LD_DISKWARE_5                    10573
#define  IE_ST_SCRN_STARTUP_SCRN_DISP_1               10574
#define  IE_ST_SCRN_STARTUP_SCRN_DISP_2               10575
#define  IE_ST_SCRN_STARTUP_SCRN_DISP_3               10576
#define  IE_ST_SCRN_STARTUP_SCRN_DISP_4               10577
#define  IE_WINS_ST_WINS_INIT_1                       10578
#define  IE_WINS_ST_WINS_INIT_2                       10579
#define  IE_WINS_ST_WINS_INIT_3                       10580
#define  IE_WINS_ST_WINS_INIT_4                       10581
#define  IE_WINS_ST_WINS_STARTUP_1                    10582
#define  IE_PUMENUS_PUSETUP_1                         10583
#define  IE_PUMENUS_PUSETUP_2                         10584
#define  IE_PUMENUS_PUMENU_1                          10585
#define  IE_WINVIS_TURN_WIN_ON_OFF_1                  10586
#define  IE_WINVIS_REM_WIN_FROM_SCRN_1                10587
#define  IE_WINVIS_REM_WIN_FROM_SCRN_2                10588
#define  IE_WINVIS_ADD_WIN_TO_SCRN_1                  10589
#define  IE_WINVIS_ADD_WIN_TO_SCRN_2                  10590
#define  IE_SCRNPROC_IS_WIN_ON_SCRN_1                 10591
#define  IE_SETVMODE_SETDISP_1                        10592
#define  IE_SETVMODE_SETDISP_2                        10593
#define  IE_SETVMODE_SETDISP_3                        10594
#define  IE_SELWATTR_CHG_WINDOW_ATTR_1                10595
#define  IE_MEMBOX_PD_XDATA_STADDR_1                  10596
#define  IE_MEMBOX_PD_XDATA_STADDR_2                  10597
#define  IE_MEMBOX_PD_XDATA_STADDR_3                  10598
#define  IE_MEMBOX_PD_DISA_STADDR_1                   10599

/* From [10600] to [10699] */

#define  IE_MEMBOX_PD_DISA_STADDR_2                   10600
#define  IE_MEMBOX_PD_DISA_STADDR_3                   10601
#define  IE_SCROLLW_SCR_COL_1                         10602
#define  IE_SCROLLW_SCR_ROW_1                         10603
#define  IE_SCROLLW_SCR_ROW_2                         10604
#define  IE_SCROLLW_NEW_VIS_ROWS_1                    10605
#define  IE_SCROLLW_A_SCROLL_1                        10606
#define  IE_SCROLLW_GET_LINE_FROM_B_1                 10607
#define  IE_SCROLLW_GET_LINE_FROM_B_2                 10608
#define  IE_SCROLLW_GET_LINE_FROM_B_3                 10609
#define  IE_FIXED_RC_DRAW_FIXED_DATA_1                10610
#define  IE_SYWIN_FILL_SY_WIN_1                       10611
#define  IE_SYWIN_SY_SCROLL_1                         10612
#define  IE_GETNTRY_VALIDATE_EXPR_1                   10613
#define  IE_GETNTRY_EVAL_EXPR_1                       10614
#define  IE_SELWATTR_UPD_TR_COLOURS_1                 10615
#define  IE_SCCONF_SAVE_SCREEN_CONF_1                 10616
#define  IE_SCCONF_LOAD_SCREEN_CONF_1                 10617
#define  IE_SCCONF_LOAD_SCREEN_CONF_2                 10618
#define  IE_SCCONF_LOAD_SCREEN_CONF_3                 10619
/* [10620] to [11035] unused */

/* From [10700] to [10799] */


/* From [10800] to [10899] */


/* From [10900] to [10999] */


/* From [11000] to [11099] */

#define  IE_DDVMAKE_WCPYSHELL_1                       11036
#define  IE_DDVMAKE_W_FIXED_1                         11037
#define  IE_DDVMAKE_WDSINIT_1                         11038
#define  IE_DDVMAKE_WDSINIT_2                         11039
#define  IE_DDVMAKE_WDSINIT_3                         11040
#define  IE_DDVMAKE_WDSINIT_4                         11041
#define  IE_DDVMAKE_WDSINIT_5                         11042
#define  IE_DDVMAKE_WDSINIT_6                         11043
#define  IE_DDVMAKE_WDSINIT_7                         11044
#define  IE_DDVMAKE_FREE_DD_WIN_1                     11045
#define  IE_DDVMAKE_FREE_DD_WIN_2                     11046
#define  IE_DDVMAKE_FREE_DD_WIN_3                     11047
#define  IE_DDVMAKE_ALLOC_DD_WIN_1                    11048
#define  IE_DDVMAKE_ALLOC_DD_WIN_2                    11049
#define  IE_DDVMAKE_ALLOC_DD_WIN_3                    11050
/* [11051] to [11700] unused */

/* From [11100] to [11199] */


/* From [11200] to [11299] */


/* From [11300] to [11399] */


/* From [11400] to [11499] */


/* From [11500] to [11599] */


/* From [11600] to [11699] */


/* From [11700] to [11799] */

#define  FN_INIT_AS_STRUCT                            11701
#define  FN_BUILD_AS_STRUCT                           11702
#define  FN_SCROLL_AS_STRUCT                          11703
#define  IE_CODWINS_TOGGLE_BP_1                       11704
#define  IE_CODWINS_GO_TO_CURSOR_1                    11705
#define  IE_CODWINS_PC_AT_CURSOR_1                    11706
/* [11707] to [11713] unused */
#define  IE_ASSCRN_OP_CODE_ADDR_1                     11714
#define  IE_ASSCRN_UPD_AS_COLOURS_1                   11715
#define  IE_ASSCRN_CURR_INST_ATTR_1                   11716
#define  IE_ASSCRN_AS_BKP_ATTR_1                      11717
#define  IE_ASSCRN_AS_SYM_ATTR_1                      11718
/* [11719] unused */
#define  IE_ASSCRN_CURR_INST_ON_BKP_1                 11720
#define  IE_ASSPUT_SCROLL_AS_STRUCT_1                 11721
#define  IE_ASSPUT_BPS_IN_AS_STRUCT_1                 11722
#define  IE_ASSPUT_BPS_IN_AS_ROW_1                    11723
#define  IE_ASSGET_DRAW_BPS_IN_WIN_1                  11724
#define  IE_ASSGET_DEL_BPS_IN_LINE_1                  11725
#define  IE_ASSGET_DEL_BPS_IN_WIN_1                   11726
#define  IE_ASSGET_DR_CURR_INST_BAR_1                 11727
#define  IE_ASSGET_DEL_CURR_INST_BAR_1                11728
#define  IE_ASWIN_DISP_AS_LINE_CL_1                   11729
#define  IE_ASWIN_FILL_AS_WIN_1                       11730
#define  IE_ASWIN_UPDATE_AS_WIN_1                     11731
#define  IE_ASWIN_AS_SCROLL_1                         11732
#define  IE_ASWIN_AS_SCROLL_2                         11733
#define  IE_HLSCRN_WRITE_HLL_LINES_1                  11734
#define  IE_HLSCRN_UPD_HL_COLOURS_1                   11735
#define  IE_HLSCRN_CURR_INST_ON_BKP_1                 11736
#define  IE_HLSCRN_HL_BKP_ATTR_1                      11737
#define  IE_HLSGET_DRAW_BPS_IN_WIN_1                  11738
#define  IE_HLSGET_DEL_BPS_IN_LINE_1                  11739
#define  IE_HLSGET_DEL_BPS_IN_WIN_1                   11740
#define  IE_HLSGET_DR_CURR_INST_BAR_1                 11741
#define  IE_HLSGET_DEL_CURR_INST_BAR_1                11742
#define  IE_HLSPUT_SCROLL_HL_STRUCT_1                 11743
#define  IE_HLWIN_GET_HL_WIN_NAME_1                   11744
#define  IE_ASSGET_DRAW_BPS_IN_LINE_1                 11745
#define  IE_HLWIN_HL_SCROLL_1                         11746
#define  IE_HLSPUT_LINE_ENTRY_1                       11747
#define  IE_HLSPUT_LINE_ENTRY_2                       11748
#define  IE_HLSPUT_LINE_ENTRY_3                       11749
#define  IE_HLSPUT_LINE_ENTRY_4                       11750
#define  IE_HLSCRN_STMT_ON_HL_LINE_1                  11751
#define  IE_HLSCRN_HL_CURR_INST_ATTR_1                11752
#define  IE_HLSGET_DRAW_BPS_IN_LINE_1                 11753
#define  IE_HLSCRN_WRITE_HLL_LINES_CS1                11754
#define  IE_HLSCRN_WRITE_HLL_LINES_CS2                11755
/* [11756] to [12000] unused */

/* From [11800] to [11899] */


/* From [11900] to [11999] */


/* From [12000] to [12099] */

#define  IE_GLOBAL_APPEND_MAL                         12001
#define  IE_GLOBAL_APPEND_FRE                         12002
#define  IE_GLOBAL_C_APPEND_MAL                       12003
#define  IE_GLOBAL_C_APPEND_FRE                       12004
/* [12005] to [14000] unused */

/* From [12100] to [12199] */


/* From [12200] to [12299] */


/* From [12300] to [12399] */


/* From [12400] to [12499] */


/* From [12500] to [12599] */


/* From [12600] to [12699] */


/* From [12700] to [12799] */


/* From [12800] to [12899] */


/* From [12900] to [12999] */


/* From [13000] to [13099] */


/* From [13100] to [13199] */


/* From [13200] to [13299] */


/* From [13300] to [13399] */


/* From [13400] to [13499] */


/* From [13500] to [13599] */


/* From [13600] to [13699] */


/* From [13700] to [13799] */


/* From [13800] to [13899] */


/* From [13900] to [13999] */


/* From [14000] to [14099] */

#define  IE_TXRX_S_BP_FMT_INIT                        14001
#define  IE_TXRX_C_BP_FMT_INIT                        14002
#define  IE_WR_MEM_FMT_INIT                           14003
#define  IE_TXRX_RD_MEM_FMT_INIT                      14004
#define  IE_TXRX_D_BP_FMT_INIT                        14005
#define  IE_TXRX_BP_FMT_INIT                          14006
#define  IE_TXRX_RESET_FMT_INIT                       14007
#define  IE_TXRX_MAP_FMT_INIT                         14008
#define  IE_TXRX_MAP_DISP_FMT_INIT                    14009
#define  IE_TXRX_DISABLE_BP_INIT                      14010
#define  IE_COMM_INITSYSTEM                           14011
#define  IE_INVALID_SLOTNUMBER                        14012
#define  IE_INVALID_BAUD_RATE                         14013
#define  IE_TXRX_16REG_FMT_INIT                       14014
#define  IE_TXRX_8REG_FMT_INIT                        14015
#define  IE_WRITERNG00                                14016
#define  IE_TRCEUTIL00                                14017
#define  IE_TRCEUTIL01                                14018
#define  IE_TRCEUTIL02                                14019
#define  IE_TRCEUTIL03                                14020
/* [14021] to [16000] unused */


/* From [14100] to [14199] */


/* From [14200] to [14299] */


/* From [14300] to [14399] */


/* From [14400] to [14499] */


/* From [14500] to [14599] */


/* From [14600] to [14699] */


/* From [14700] to [14799] */


/* From [14800] to [14899] */


/* From [14900] to [14999] */


/* From [15000] to [15099] */


/* From [15100] to [15199] */


/* From [15200] to [15299] */


/* From [15300] to [15399] */


/* From [15400] to [15499] */


/* From [15500] to [15599] */


/* From [15600] to [15699] */


/* From [15700] to [15799] */


/* From [15800] to [15899] */


/* From [15900] to [15999] */


/* From [16000] to [16099] */

#define  IE_TR_L4_TINFINIT_GETSYNCBITS_M              16001
#define  IE_TR_L4_TINFINIT_GETSYNCBITS_F              16002
#define  IE_TR_L4_TINFOACC_ADDDEF_F_BDEF              16003
#define  IE_TR_L4_TINFOACC_ADDDEF_C_BDEF              16004
#define  IE_TR_L4_TINFOACC_ADDDEF_R_BDEF              16005
#define  IE_TR_L4_IOINFO_GETIOBY_UNMUX                16006
/* [16007] to [16009] unused */
#define  IE_TR_L3_TSRCE3_MERGELINE_M                  16010
/* [16011] to [16019] unused */
#define  IE_TR_L3_TDISA3_BNORDIS_F_LAB1               16020
#define  IE_TR_L3_TDISA3_BNORDIS_F_INST               16021
#define  IE_TR_L3_TDISA3_BNORDIS_F_IDAT               16022
#define  IE_TR_L3_TDISA3_BNORDIS_F_LAB2               16023
/* [16024] to [16029] unused */
#define  IE_TR_L3_TDISA3_BEXPDIS_F_LAB1               16030
#define  IE_TR_L3_TDISA3_BEXPDIS_F_INST               16031
#define  IE_TR_L3_TDISA3_BEXPDIS_F_IDAT               16032
#define  IE_TR_L3_TDISA3_BEXPDIS_F_LAB2               16033
/* [16034] to [16039] unused */
#define  IE_TR_L3_LNHND_FREETRLN_F_LARR               16040
#define  IE_TR_L3_LNHND_FREETRLN_F_LPTR               16041
#define  IE_TR_L3_LNHND_FREETRLN_F_LCOL               16042
#define  IE_TR_L3_LNHND_FREETRLN_F_LINF               16043
/* [16044] to [16049] unused */
#define  IE_TR_L3_LNHND_ALLALINE_M_LINE               16050
#define  IE_TR_L3_LNHND_ALLALINE_R_LINE               16051
#define  IE_TR_L3_LNHND_ALLALINE_C_STR                16052
#define  IE_TR_L3_LNHND_ALLALINE_M_LINF               16053
#define  IE_TR_L3_LNHND_ALLALINE_R_LINF               16054
#define  IE_TR_L3_LNHND_ALLALINE_M_COL                16055
#define  IE_TR_L3_LNHND_ALLALINE_R_COL                16056
/* [16057] to [16059] unused */
#define  IE_TR_L3_LNHND_LADDSTR_C_FSTR                16060
#define  IE_TR_L3_LNHND_LADDSTR_R_LINE                16061
#define  IE_TR_L3_LNHND_LADDSTR_R_FSTR                16062
#define  IE_TR_L3_LNHND_LADDSTR_R_LIN2                16063
#define  IE_TR_L3_LNHND_LADDSTR_F_FSTR                16064
#define  IE_TR_TI_SCNINT_ADSTRLN_C_FSTR               16065
#define  IE_TR_TI_SCNINT_ADSTRLN_R_LIN2               16066
#define  IE_TR_TI_SCNINT_ADSTRLN_F_FSTR               16067
#define  IE_TR_TI_SCNINT_COPYTLINETY_C_1              16068
#define  IE_TR_TI_SCNINT_COPYTLINETY_C_2              16069
#define  IE_TR_L0_T0ACTION_CODE_DISP_1                16070
#define  IE_TR_L0_T0ACTION_HEXACT_DISP_1              16071
/* [16072] to [16099] unused */

/* From [16100] to [16199] */

#define  IE_TR_L5_TMEMMAN_MAKBLK_M_TRBUF              16100
#define  IE_TR_L5_TMEMMAN_CLRTRBUF_F_BUF              16101
#define  IE_TSCN_SETTRCOL_NOWIN                       16102
/* [16103] to [17000] unused */

/* From [16200] to [16299] */


/* From [16300] to [16399] */


/* From [16400] to [16499] */


/* From [16500] to [16599] */


/* From [16600] to [16699] */


/* From [16700] to [16799] */


/* From [16800] to [16899] */


/* From [16900] to [16999] */


/* From [17000] to [17099] */

#define  IE_SEAINIT_ALLOC_SEADATA_1                   17001
#define  IE_PADISP_CHECKSEADATAMEM_1                  17002
#define  IE_SEARDBIN_READSEABRANGE_1                  17003
#define  IE_SEARDBIN_READSEABRANGE_2                  17004
#define  IE_SEACODE_ADDTOEXITLIST_1                   17005
#define  IE_SEACODE_ADDTOEXITLIST_2                   17006
#define  IE_SEACODEA_FREEEXITS_1                      17007
#define  IE_FTDBOXES_EVENTSEARCH_1                    17008
#define  IE_FTDBOXES_JMPTOFRAME_1                     17009
#define  IE_FTDISP_GETLASTFRINBUF_1                   17010
#define  STR_LOG_FILE_IN_USE                          17011
/* [17012] to [18000] unused */

/* From [17100] to [17199] */


/* From [17200] to [17299] */


/* From [17300] to [17399] */


/* From [17400] to [17499] */


/* From [17500] to [17599] */


/* From [17600] to [17699] */


/* From [17700] to [17799] */


/* From [17800] to [17899] */


/* From [17900] to [17999] */


/* From [18000] to [18099] */

#define  IE_TRIG_TGWIN_SDT                            18001
#define  IE_TRIG_TRGPUM_ILLEGPAR                      18002
#define  IE_TRIG_TRGPUM_CNMAKPOPWIN                   18003
#define  IE_TRIG_TRGPUM_TMPTRG                        18004
#define  IE_TRIG_TRGPUM_FRETRG                        18005
/* [18006] to [20000] unused */

/* From [18100] to [18199] */


/* From [18200] to [18299] */


/* From [18300] to [18399] */


/* From [18400] to [18499] */


/* From [18500] to [18599] */


/* From [18600] to [18699] */


/* From [18700] to [18799] */


/* From [18800] to [18899] */


/* From [18900] to [18999] */


/* From [19000] to [19099] */


/* From [19100] to [19199] */


/* From [19200] to [19299] */


/* From [19300] to [19399] */


/* From [19400] to [19499] */


/* From [19500] to [19599] */


/* From [19600] to [19699] */


/* From [19700] to [19799] */


/* From [19800] to [19899] */


/* From [19900] to [19999] */


/* From [20000] to [20099] */

#define  IE_EF_ASYSTEM_VERSTR_BADMTH                  20001
#define  IE_EF_ASYSTEM_FILLLEV_BADCASE                20002
#define  IE_EF_ASYSTEM_WRNAMINT_BADCASE               20003
#define  IE_EF_ASYSTEM_DOALINE_BADCASE                20004
#define  IE_EF_ASYSTEM_DRLOOPTXT_COM                  20005
#define  IE_A_SYSTEM_DOS_COM_FRE                      20006
#define  IE_EF_SYSTEM_DIV_BY_ZERO                     20007
#define  IE_EF_ASYSTEM_FILLINTDATA_BDCSE              20008
#define  IE_EF_ASYSTEM_WRNAMINT_BADCSE01              20009
#define  IE_EF_ASYSTEM_WRNAMINT_BADCSE02              20010
#define  IE_EF_ASYSTEM_WRNAMINT_BADCSE03              20011
#define  IE_EF_ASYSTEM_WRNAMINT_BADCSE04              20012
#define  IE_EF_ASYSTEM_WRNAMINT_BADCSE05              20013
#define  IE_EF_ASYSTEM_READINTREG_BDCASE              20014
#define  IE_EF_ASYSTEM_NEWINTABLE_BDCASE              20015
#define  IE_PFWEND00                                  20016
/* [20017] to [20020] unused */
#define  IE_EF_ASTEP_STEPFRM_BADCASE                  20021
#define  IE_EF_ASTEP_STOVCALLS_BADCASE                20022
#define  IE_EF_ASTEP_HLL_STEP                         20023
#define  IE_EF_ASTEP_HLL_USER_BP                      20024
/* [20025] to [20100] unused */

/* From [20100] to [20199] */

#define  IE_A_MAPBP_DISP_BP_DEF                       20101
#define  IE_A_MAPBP_MAP_DISP                          20102
#define  IE_A_MAPBP_READ_MAP                          20103
#define  IE_A_MAPBP_ERASE_BP                          20104
#define  IE_P_BAUD_GET                                20105
/* [20106] to [20150] unused */
#define  IE_A_MEMORY_READ_REG                         20151
#define  IE_A_MEMORY_WRITE_BOX                        20152
#define  IE_A_MEMORY_RD_LOCM_DEF1                     20153
#define  IE_A_MEMORY_RD_LOCM_DEF2                     20154
#define  IE_A_MEMORY_REGBANK_DISP1                    20155
#define  IE_A_MEMORY_REGBANK_DISP2                    20156
#define  IE_A_MEMORY_DATA_TO_STR_BADCASE              20157
#define  IE_A_MEMORY_SET_REG_BLK                      20158
#define  IE_A_MEMORY_READRXREG                        20159
#define  IE_A_MEMORY_TXRXWRITEREG                     20160
/* [20161] to [20200] unused */

/* From [20200] to [20299] */

#define  IE_A_GEN_ERROR                               20201
/* [20202] to [20250] unused */
#define  IE_EMULFNS_GO_ILLEG_TIME                     20251
#define  IE_EMULFNS_GO_MILLI_SEC_STR                  20252
#define  IE_EMULFNS_GO_ILLEG_DAYSTR                   20253
#define  IE_EMULFNS_GO_ILLEG_PAR                      20254
#define  IE_EMULFNS_GO_ILLEG_HOURSTR                  20255
#define  IE_GOUTILS_G_STEP_CL_UP                      20256
#define  IE_GOUTILS_COMPARERUNTIME_1                  20257
/* [20258] to [20300] unused */

/* From [20300] to [20399] */

#define  IE_A_GROUP_KEYDEF_STR1                       20301
#define  IE_A_GROUP_KEYDEF_STR2                       20302
#define  IE_A_GROUP_KEYDEF_FRE                        20303
#define  IE_A_GROUP_KEYDEF_STR3                       20304
#define  IE_A_GROUP_KEYDEF_STR4                       20305
/* [20306] to [20350] unused */
#define  IE_A_CONFIG_SET_BASE                         20351
#define  IE_A_CONFIG_SELPOART                         20352
#define  IE_A_CONFIG_SHOW_CLOCK                       20353
#define  IE_A_CONFIG_MODE_SELECT                      20354
#define  IE_A_CONFIG_DISP_BAUD                        20355
#define  IE_EMULFNS_A_SYM                             20356
#define  IE_BANKCFG00                                 20357
#define  IE_BANKCFG01                                 20358
/* [20359] to [20375] unused */
#define  IE_A_SYM_SYM_STR_DISP_FREEMEM                20376
#define  IE_A_SYM_SYM_STR_PS_FREEMEM                  20377
#define  IE_A_SYM_MOD_CHNG_FREEMEM                    20378
#define  IE_A_SYM_SYM_DEF_FREEMEM                     20379
#define  IE_A_SYM_REM_SYM_FREEMEM                     20380
#define  IE_A_SYM_VAL_SYM_FREEMEM                     20381
#define  IE_A_SYM_SYM_DEF_BADCASE                     20382
/* [20383] to [20399] unused */

/* From [20400] to [20499] */
#define  IE_A_REG00                                   20400    
#define  PC_SCOPE_NEQ_TO_REG_SYM_SCOPE                20401    
/* [20402] to [20499] unused */


/* From [20500] to [20599] */


/* From [20600] to [20699] */


/* From [20700] to [20799] */


/* From [20800] to [20899] */


/* From [20900] to [20999] */


/* From [21000] to [21099] */

#define  IE_P_GROUP_INIT_GR                           21001
#define  IE_P_GROUP_HANDLE_GR                         21002
#define  IE_P_SAVE_SAVET_PR                           21003
#define  IE_P_KEY_CMD_STR                             21004
#define  IE_P_READ_SET_R                              21005
#define  IE_P_EXPR_SET_LOCN                           21006
#define  IE_P_DOS_SET_DOS_CAL                         21007
#define  IE_P_DOS_SET_DOS_REL                         21008
#define  IE_P_MODE_IES                                21009
#define  IE_P_DEL_CAL                                 21010
#define  IE_P_DEL_REL                                 21011
#define  IE_P_CLOCK_DEF                               21012
/* [21013] unused */
#define  IE_P_PROM_CHK_SEC                            21014
#define  IE_P_PROM_CHECK_EMS                          21015
#define  IE_P_SYM_NAME_PTR                            21016
#define  IE_P_SYM_PATH_PTR                            21017
#define  IE_P_SUB_REL                                 21018
#define  IE_P_SUB_CAL                                 21019
#define  IE_P_SUB_REL2                                21020
#define  IE_P_SUB_CAL2                                21021
#define  IE_P_ENABLE_DEF                              21022
#define  IE_P_GO_DEF                                  21023
#define  IE_P_MAP_DEF                                 21024
#define  IE_P_GEN_DEF                                 21025
#define  IE_P_GEN_DEF2                                21026
#define  IE_P_GEN_DEF3                                21027
#define  IE_P_GEN_DEF4                                21028
#define  IE_P_LOAD_DEF1                               21029
#define  IE_P_BPTS_BPCODE                             21030
#define  IE_P_BPTS_BMTYPE                             21031
#define  IE_P_BPTS_CCTYPE                             21032
#define  IE_P_WRITE_INIT_W_BUF                        21033
#define  IE_P_WRITE_ADD_BYTE_MAL                      21034
#define  IE_P_WRITE_ADD_BYTE_REL                      21035
#define  IE_P_WRITE_SET_W_MEM                         21036
#define  IE_P_MODE_BANKMODE                           21037
#define  IE_ES_PREG_GETREGNO_BADCASE                  21038
#define  IE_ES_PCOPY_MEMDEST_BADCASE                  21039
#define  IE_P_BASE_TYPE_BASE                          21040
#define  IE_P_TRACE_DEF                               21041
#define  IE_P_STEP_DEFAULT                            21042
#define  IE_P_KEY_APPEND_EOL                          21043
#define  IE_P_SAVE_GET_SAVE_FMT                       21044
#define  IE_P_STEP_DEFAULT2                           21045
#define  IE_P_WRITEC                                  21046
#define  IE_A_SETBANKTRACE_FREE                       21047
#define  IE_P_MODE_BSTEF                              21048
#define  IE_P_WRITE00                                 21049
#define  IE_P_GEN_00                                  21050
/* [21051] to [21052] unused */
#define  IE_EF_SAVEFILE_NO_ENTRY_POINT                21053
#define  IE_EF_SAVEFILE_NO_LANGUAGE                   21054
#define  IE_A_SAVE_SAVE_FILE                          21055
/* [21056] to [21070] unused */
#define  IE_A_CODECO__CL_WRITE_FROM_SYM               21071
#define  IE_A_CODECO__CL_WRITE_TO_SYM                 21072
#define  IE_A_CODECO__ADD_TO_STR_BADCASE              21073
#define  IE_A_CODECO__CL_WRITE_TO_BLANK               21074
/* [21075] to [21101] unused */

/* From [21100] to [21199] */

#define  IE_BUILDVAR_INIT_STRUC_BADRANGE              21102
#define  IE_BUILDVAR_SIMPLE_BADCASE1                  21103
#define  IE_BUILDVAR_SIMPLE_BADCASE2                  21104
#define  IE_BUILDVAR_RECURSE_BADCASE                  21105
#define  IE_BUILDVAR_RECURSE_TOO_DEEP                 21106
#define  IE_BUILDVAR_OPENVBOXBUFF_OPENED              21107
#define  IE_BUILDVAR_READVBOXBUFF_BADBUF              21108
#define  IE_BUILDVAR_GETSIZE_BADCSE1                  21109
#define  IE_BUILDVAR_GETSIZE_BADCSE2                  21110
#define  IE_BUILDVAR_GETSIZE_NOTSIMPLE                21111
#define  IE_BUILDVAR_DISPLAY_PTR_BADCSE               21112
#define  IE_CEXPR_GETTHESYMVALUE_BADCASE              21113
#define  IE_BUILDVAR_BAD_PTR_SIZE                     21114   
#define  IE_BUILDVAR_BAD_PTR_TYPE                     21115
#define  IE_BUILDVAR_INVALID_FINDER                   21116
#define  IE_CEXPR_GENERIC_PTR_NOT_SUPP                21117
/* [21118] to [21130] unused */
#define  IE_MONVAR_CPWIN_NOVARWIN                     21131
/* [21132] to [21140] unused */
#define  MACRO_VAR_SIZE_EXCEEDED                      21141
#define  IE_MPFNS_UPDATE_2                            21142
#define  IE_MPFNS_ALLOCATE_1                          21143
#define  IE_MPFNS_SEARCH_1                            21144
#define  IE_MPFNS_READ_1                              21145
#define  IE_MPFNS_READ_2                              21146
#define  IE_MPFNS_STORE_ACTUAL_PARAMS_1               21147
#define  IE_MPFNS_GETSTRINGCMDTYPE_1                  21148
#define  IE_MPFNS_STRINGCOMMANDS_1                    21149
#define  IE_MPFNS_STRINGCOMMANDS_2                    21150
#define  IE_MPFNS_BINARYCOMMANDS_1                    21151
#define  IE_MPFNS_FIXUPBYREFPARAM_1                   21152
#define  IE_MPFNS_FIXUPBYREFPARAM_2                   21153
#define  IE_MPFNS_MEMCMPCOMMAND_1                     21154
#define  IE_MPFNS_RUNCMPCOMMAND_1                     21155
#define  IE_MPFNS_RUNCMPCOMMAND_2                     21156
#define  IE_MPFNS_SET_MACRO_IOTYPE_1                  21157
#define  IE_MPFNS_PRINTCOMMAND_1                      21158
/* [21159] to [21160] unused */
#define  IE_FINDER_VER_NOT_SUPP_E                     21161
#define  IE_LOADUTIL_RIARCLIB_1                       21162
#define  IE_LOADUTIL_RIARCLIB_2                       21163
#define  IE_LOADUTIL_FREEIAR_1                        21164
#define  IE_LOADUTIL_FREEIAR_2                        21165
#define  IE_LOADFILE_LOR_1_L                          21166
#define  IE_LOADFILE_PDTR_1_L                         21167
#define  IE_LOADFILE_PDTR_2_L                         21168
#define  IE_LOADFILE_PMHR_1                           21169
#define  IE_LOADFILE_LD_DISKWARE_2                    21170
#define  IE_LOADFILE_LD_DISKWARE_1                    21171
#define  IE_LOADFILE_LD_DISKWARE_3                    21172    
#define  IE_LOADFILE_PROC_ASDOF_DATA                  21173
#define  IE_LOADFILE_PROC_ASDOF_DATA_2                21174
/* [21175] to [21190] unused */
#define  IE_AEQUATE_CASTRIGHT_BADCASE_00              21191
#define  IE_AEQUATE_CASTRIGHT_BADCASE_01              21192
#define  IE_AEQUATE_CASTRIGHT_BADCASE_02              21193
#define  IE_AEQUATE_CASTRIGHT_BADCASE_03              21194
#define  IE_AEQUATE_CASTFLOAT_BADCASE_00              21195
/* [21196] to [21199] unused */

/* From [21200] to [21299] */

#define  IE_HLLSTEP_ANALYSE_LIB_BADCASE               21200
#define  IE_HLLSTEP00                                 21201
/* [21200] to [21299] unused */

/* From [21300] to [21399] */

#define  IE_A_ONCHIP_SET_BREAK_RANGES                 21300
#define  IE_A_ONCHIP_CLR_BREAK_RANGES_2               21301
#define  IE_A_ONCHIP_DISP_BREAK_RANGES                21302
#define  IE_A_ONCHIP_CLR_BREAK_RANGES                 21303
#define  IE_P_ONCHIP_I_OR_N_CHK                       21304
#define  IE_A_ONCHIP_SET_ONCHIP                       21305
#define  IE_A_ONCHIP_CLR_ONCHIP_2                     21306
#define  IE_A_ONCHIP_CLR_ONCHIP                       21307
#define  IE_ONCHIP_SET_TRIG                           21308
#define  IE_P_ONCHIP_CHK_ONCHIP_BP                    21309
#define  IE_ONCHIP_SET_BREAK_RANGES                   21310
/* [21311] to [22000] unused */

/* From [21400] to [21499] */
#define  IE_PROCCFG_GETPROCINFO_1                     21400    
#define  IE_PROCCFG_GETPROCINFO_2                     21401    
#define  IE_PROCCFG_GETPROCINFO_3                     21402    
#define  IE_PROCCFG_GETPROCINFO_4                     21403    
#define  IE_P_MODE_BSEF                               21404    
#define  IE_P_MODE_CHECKVALIDMODE                     21405    
#define  IE_P_MODE_PFMODE_E                           21406    
#define  IE_P_MODE_TXMODE_E                           21407    
#define  IE_P_STACK_01                                21408    
/* [21409] to [21500] unused */


/* From [21500] to [21599] */


/* From [21600] to [21699] */


/* From [21700] to [21799] */


/* From [21800] to [21899] */


/* From [21900] to [21999] */


/* From [22000] to [22099] */

#define  IE_GRFILES_POPFILEENTRIES_1_L                22001
#define  IE_GRFILES_POPFILEENTRIES_2_L                22002
#define  IE_GRFILES_INITGRFILES_1_L                   22003
#define  IE_GRFILES_INITGRFILES_2_L                   22004
#define  IE_GRFILES_INITGRFILES_3_L                   22005
#define  IE_GRFILES_GROUP_1_L                         22006
#define  IE_GRFILES_GROUP_2_L                         22007
#define  IE_GRFILES_GROUP_3_L                         22008
#define  IE_GRFILES_GROUP_4_L                         22009
#define  IE_GRFILES_GROUP_5_L                         22010
#define  IE_GRFILES_GROUP_6_L                         22011
#define  IE_GRFILES_PUSHFILEENTRIES_1_L               22012
#define  IE_GRFILES_PUSHFILEENTRIES_2_L               22013
#define  IE_GRFILES_PUSHFILEENTRIES_3_L               22014
#define  IE_GRFILES_PUSHFILEENTRIES_4_L               22015
#define  IE_GRFILES_POPFILEENTRIES_POP                22016
#define  IE_GRFILES_GROUP_NULL                        22017
/* [22018] to [22100] unused */

/* From [22100] to [22199] */

#define  IE_INITTOK_INIT_TOK_1_L                      22101
#define  IE_INITTOK_INSERT_1_L                        22102
/* [22103] unused */
#define  IE_INITTOK_INSERT_UNKNOWN_TABLE              22104
#define  IE_INITTOK_INIT_TOK_2_L                      22105
#define  IE_INITIKEY_INIT_FN_KEY_1                    22106
#define  MEMORY_ERROR_PROCASM00                       22107
/* [22108] to [22200] unused */

/* From [22200] to [22299] */

#define  IE_INLINE_REPLACE_PARAMETER_1                22201
#define  IE_INLINE_REPLACE_PARAMETER_2                22202
/* [22203] to [22400] unused */

/* From [22300] to [22399] */


/* From [22400] to [22499] */

#define  IE_KEYBUF_ADDKEY_1_L                         22401
#define  IE_KEYBUF_ADDKEY_2_L                         22402
#define  IE_KEYBUF_ADDFIRSTKEY_1_L                    22403
#define  IE_KEYBUF_ADDFIRSTKEY_2_L                    22404
#define  IE_KEYBUF_KEY_DELETE_1                       22405
#define  IE_KEYBUF_KEY_DELETE_2                       22406
#define  IE_KEYBUF_ADDKEYS_1_L                        22407
#define  IE_KEYBUF_ADDKEYS_2_L                        22408
#define  IE_KEYBUF_KEY_DELETE_1_L                     22409
#define  IE_KEYBUF_KEY_DELETE_2_L                     22410
#define  IE_KEYBUF_FREEKEYS_1_L                       22411
#define  IE_KEYBUF_TAKEKEY_1_L                        22412
#define  IE_KEYBUF_TAKEKEY_2_L                        22413
/* [22414] to [22600] unused */

/* From [22500] to [22599] */


/* From [22600] to [22699] */

#define  IE_NKEY_DO_ENTER_1_L                         22601
#define  IE_NKEY_DO_ENTER_2_L                         22602
/* [22603] to [22700] unused */

/* From [22700] to [22799] */

#define  IE_PRE_SETUP_SUB_CHARS_1_L                   22701
#define  IE_PRE_SETUP_SUB_CHARS_2_L                   22702
#define  IE_PRE_GET_SUB_PARAMETERS_1_L                22703
#define  IE_PRE_GET_SUB_PARAMETERS_2_L                22704
#define  IE_PRE_GET_SUB_PARAMETERS_3_L                22705
/* [22706] to [22900] unused */

/* From [22800] to [22899] */


/* From [22900] to [22999] */

#define  IE_STRINGS_STR_POSITION_1_L                  22901
#define  IE_STRINGS_STR_POSITION_2_L                  22902
#define  IE_STRINGS_STR_POSITION_3_L                  22903
#define  IE_STRINGS_STR_POSITION_4_L                  22904
#define  IE_STRINGS_STR_POSITION_5_L                  22905
#define  IE_STRINGS_STR_POSITION_6_L                  22906
#define  IE_STRINGS_STR_POSITION_NULL_1               22907
#define  IE_STRINGS_STR_POSITION_NULL_2               22908
#define  IE_STRINGS_STR_DELETE_RANGE_1                22909
#define  IE_STRINGS_STR_DELETE_RANGE_2                22910
#define  IE_STRINGS_STR_DELETE_NULL                   22911
#define  IE_STRINGS_STR_INSERT_NULL_1                 22912
#define  IE_STRINGS_STR_INSERT_NULL_2                 22913
#define  IE_STRINGS_STR_INSERT_RANGE                  22914
/* [22915] to [23000] unused */

/* From [23000] to [23099] */

#define  IE_SUBST_GET_SUBST_ROOM_1_L                  23001
#define  IE_SUBST_GET_SUBST_ROOM_2_L                  23002
#define  IE_SUBST_ADD_REPLACEMENT_1_L                 23003
#define  IE_SUBST_ADD_REPLACEMENT_2_L                 23004
#define  IE_SUBST_ADD_REPLACEMENT_1                   23005
#define  IE_SUBST_ADD_REPLACEMENT_2                   23006
#define  IE_SUBST_EXPANDED_LINE_1_L                   23007
#define  IE_SUBST_EXPANDED_LINE_2_L                   23008
#define  IE_SUBST_EXPANDED_LINE_3_L                   23009
/* [23010] to [23100] unused */

/* From [23100] to [23199] */

#define  IE_TOKENS_PROC_CONT_CHARS_1_L                23101
#define  IE_TOKENS_PROC_CONT_CHARS_2_L                23102
#define  IE_TOKENS_PROC_CONT_CHARS_3_L                23103
#define  IE_TOKENS_PROC_CONT_CHARS_4_L                23104
#define  IE_TOKENS_PROCESS_ILLEGAL_1_L                23105
#define  IE_TOKENS_PROCESS_SYMBOL_1_L                 23106
#define  IE_TOKENS_PROCESS_SYMBOL_2_L                 23107
#define  IE_TOKENS_PROCESS_SYMBOL_3_L                 23108
#define  IE_TOKENS_PROCESS_SYMBOL_4_L                 23109
#define  IE_TOKENS_PROCESS_SYMBOL_5_L                 23110
#define  IE_TOKENS_PROCESS_SYMBOL_6_L                 23111
#define  IE_TOKENS_PROCESS_DELIMITER_1                23112
#define  IE_TOKENS_GET_NEXT_CHAR_1_L                  23113
#define  IE_TOKENS_DEALLOCATE_TOKEN_1_L               23114
#define  IE_TOKENS_DEALLOCATE_TOKEN_2_L               23115
#define  IE_TOKENS_DEALLOCATE_TOKEN_3_L               23116
#define  IE_TOKENS_DEALLOCATE_TOKEN_4_L               23117
#define  IE_TOKENS_CHECK_VALID_NUMBER_1               23118
#define  IE_TOKENS_TRUNCATE_LEXEME_1_L                23119
#define  IE_TOKENS_SRCH_FOR_KEYWORD_1_L               23120
#define  IE_TOKENS_SRCH_FOR_KEYWORD_2_L               23121
#define  IE_TOKENS_CPYSTR_TO_CMDLINE_FAT              23122
/* [23123] to [23300] unused */

/* From [23200] to [23299] */


/* From [23300] to [23399] */

#define  IE_TOKTAB_GET_MATCH_1                        23301
#define  IE_TOKTAB_GET_MATCH_1_L                      23302
/* [23303] to [24000] unused */

/* From [23400] to [23499] */


/* From [23500] to [23599] */


/* From [23600] to [23699] */


/* From [23700] to [23799] */


/* From [23800] to [23899] */


/* From [23900] to [23999] */


/* From [24000] to [24099] */

#define  IE_COLOUR_ADDCOLOURENTRY_1_L                 24001
#define  IE_COLOUR_ADDCOLOURENTRY_2_L                 24002
#define  IE_COLOUR_DISPOSECOLOUR_1_L                  24003
#define  IE_COLOUR_REMOVECOLOURROW_1_L                24004
#define  IE_COLOUR_REMOVECOLOURROW_2_L                24005
/* [24006] to [24100] unused */

/* From [24100] to [24199] */

#define  IE_DISA_DISASSEMBLE_LINE_1                   24101
#define  IE_DISA_DISASSEMBLE_LINE_2                   24102
#define  IE_DISA_DISASSEMBLE_LINE_1_L                 24103
#define  IE_DISA_PART_DISA_1_L                        24104
#define  IE_DISA_PART_DISA_2_L                        24105
#define  IE_DISA_TOFIELDSIZESUFFIX_1                  24106
#define  IE_DISA_HEXPR_1_L                            24107
#define  IE_DISA_HEXPR_2_L                            24108
#define  IE_DISA00                                    24109
#define  IE_DISA01                                    24110
/* [24111] to [24200] unused */

/* From [24200] to [24299] */

#define  IE_MEM_FACE_ASTRCALLOC_1                     24201
#define  IE_MEM_FACE_ASTRREALLOC_1                    24202
/* [24203] to [24300] unused */

/* From [24300] to [24399] */

#define  BUFFER_STACK_BY_START_AC                     24301
#define  BUFFER_STACK_BY_START_FREE                   24302
#define  IE_BUFFER_ASM_INFO_REL                       24303
#define  IE_BUFFER_ASM_INFO_CAL                       24304
#define  IE_BUFFER_DISPOSE_STRINGS                    24305
#define  IE_BUFFER_DATA_INFO_REL                      24306
#define  IE_BUFFER_DATA_INFO_CAL                      24307
#define  IE_BUFFER_DATA_INFO_DEF1                     24308
#define  IE_BUFFER_SIZE_OF_READ                       24309
#define  IE_BUFFER_BYTE_INFO_REL1                     24310
#define  IE_BUFFER_BYTE_INFO_CAL1                     24311
#define  IE_BUFFER_BYTE_INFO_CAL2                     24312
#define  IE_BUFFER_BYTE_INFO_REL2                     24313
#define  IE_BUFFER_BYTE_INFO_FREE                     24314
#define  IE_BUFFER_BYTE_INFO_REL3                     24315
#define  IE_BUFFER_NEXT_READ_BYTE_CAL                 24316
#define  IE_BUFFER_NEXT_READ_BYTE_REL                 24317
#define  IE_BUFFER_FORMAT_STACK_INF_REL               24318
#define  IE_BUFFER_FORMAT_STACK_INFO_CAL              24319
#define  IE_BUFFER_FORMAT_STACK_INF_REL2              24320
#define  IE_BUFFER_FORMAT_STACK_DATA_MAL              24321
#define  IE_BUFFER_STACK_INFO_MAL                     24322
#define  IE_BUFFER_STACK_INFO_FRE                     24323
#define  IE_BUFFER_PLM_INFO_REL                       24324
#define  IE_BUFFER_PLM_INFO_CAL                       24325
#define  IE_BUFFER_PLM_SCROLL_UP_REL                  24326
#define  IE_BUFFER_PLM_SCROLL_UP_CAL                  24327
#define  IE_BUFFER_PLM_SCROLL_DN_REL                  24328
#define  IE_BUFFER_PLM_SCROLL_DN_CAL                  24329
#define  IE_BUFFER_HL_INFO_REL                        24330
#define  IE_BUFFER_HL_INFO_CAL                        24331
#define  IE_BUFFER_ASM_INFO_ASTRCAL                   24332
#define  IE_BUFFER_HLL_INFO_1_L                       24333
#define  IE_BUFFER_HLL_INFO_2_L                       24334
#define  IE_BUFFER_HLL_INFO_3_L                       24335
#define  IE_BUFFER_HLL_SCROLL_UP_1_L                  24336
#define  IE_BUFFER_HLL_SCROLL_UP_2_L                  24337
#define  IE_BUFFER_HLL_SCROLL_UP_3_L                  24338
#define  IE_BUFFER_HLL_SCROLL_DOWN_1_L                24339
#define  IE_BUFFER_HLL_SCROLL_DOWN_2_L                24340
#define  IE_BUFFER_HLL_SCROLL_DOWN_3_L                24341
#define  IE_BUFFER_NUM_BYTES                          24342
#define  IE_BUFFER_FGETPREV_NO_LANG                   24343
#define  IE_BUFFER_PFGPAL_1                           24344
#define  IE_BUFFER_INIT_BUF_1                         24345
#define  IE_BUFFER00                                  24346
#define  IE_BUFFER01                                  24347
#define  IE_BUFFER02                                  24348
#define  IE_BUFFER03                                  24349
#define  IE_BUFFER_DATA_INFO_DEF2                     24350
/* [24351] to [24400] unused */

/* From [24400] to [24499] */

#define  IE_FORDATA_UNIT_TYPE                         24401
#define  IE_FORMAT_DATA_CALLOC                        24402
#define  IE_FORDATA_MEM_TYPE                          24403
#define  IE_FORDATA_BASE_TYPE                         24404
#define  IE_FORMAT_DATA_STRING_TOO_LONG               24405
/* [24406] to [24450] unused */
#define  IE_UT_BUF_DISP_HL_BUF_FREE1                  24451
#define  IE_UT_BUF_DISP_HL_BUF_FREE2                  24452
#define  IE_UT_BUF_DISP_HL_BUF_FREE3                  24453
#define  IE_UT_BUF_DISP_HL_BUF_FREE4                  24454
#define  IE_UT_BUF_DISP_PLM_BUF_FREE1                 24455
#define  IE_UT_BUF_DISP_PLM_BUF_FREE2                 24456
#define  IE_UT_BUF_DISP_PLM_BUF_FREE3                 24457
#define  IE_UT_BUF_DISP_PLM_BUF_FREE4                 24458
/* [24459] to [24500] unused */

/* From [24500] to [24599] */

#define  IE_DATABUF_ADD_TO_BUF_MAL                    24501
#define  IE_DATABUF_ADD_TO_BUF_REAL                   24502
#define  IE_DATABUF_ADD_TO_BUF_AL                     24503
#define  IE_DATABUF_DEL_FRM_BUF_RAL1                  24504
#define  IE_DATABUF_DEL_FRM_BUF_FRE                   24505
#define  IE_DATABUF_DEL_FRM_BUF_RAL2                  24506
#define  IE_DATABUF_DEL_FRM_BUF_RAL3                  24507
#define  IE_DATABUF_PRN_DATA_BUF_DEF                  24508
#define  IE_DATABUF_PRN_EDIT_BUF_DEF                  24509
#define  IE_DATABUF_READ_EMU_DEF1                     24510
#define  IE_DATABUF_READ_EMU_DEF2                     24511
#define  IE_DATABUF_WRITE_EMU_DEF1                    24512
#define  IE_DATABUF_WRITE_EMU_DEF2                    24513
#define  IE_DATABUF_INFOINBUF1                        24514
#define  IE_RESET_BUFFER_LIST                         24515
/* [24516] to [24550] unused */
#define  IE_HL_BUF_RD_IN_HL_BUF_MAL                   24551
#define  IE_HL_BUF_RD_IN_HL_BUF_CAL                   24552
#define  IE_HL_BUF_AD_HL_COD_DATA_MAL                 24553
#define  IE_HL_BUF_AD_HL_COD_DAT_REL                  24554
#define  IE_HL_BUF_AD_HL_COD_DAT_CAL                  24555
/* [24556] to [24600] unused */

/* From [24600] to [24699] */

#define  IE_INLNASM_ASM_LINE_REL                      24601
#define  IE_INLNASM_PROCESS                           24602
#define  IE_INLNASM_ACTION                            24603
#define  IE_INLNASM_01                                24604 
#define  IE_INLNASM_02                                24605 
#define  IE_INLNASM_03                                24606 
#define  IE_INLNASM_04                                24607 
#define  IE_INLNASM_05                                24608 
#define  IE_INLNASM_06                                24609 
#define  IE_INLNASM_07                                24610 
#define  IE_INLNASM_08                                24611 
#define  IE_INLNASM_09                                24612 
#define  IE_INLNASM_10                                24613 
#define  IE_INLNASM_11                                24614 
#define  IE_INLNASM_12                                24615
#define  FATAL_ASSEMBLER_ERROR                        24616
#define  IE_ASM377_00                                 24617
#define  IE_ASM377_01                                 24618
#define  IE_ASM377_02                                 24619
#define  IE_ASM377_03                                 24620
#define  IE_SRCHSOUR_00                               24621
#define  IE_SRCHTRAD_00                               24622
#define  IE_SRCHTRIG_00                               24623
/* [24624] to [24650] unused */                             

#define  IE_EMULINIT_CAL                              24651
/* [24652] to [24700] unused */

/* From [24700] to [24799] */

#define  IE_FILE_F_OPEN_TOO_LONG                      24701
#define  FILE_ALREADY_OPEN_E                          24702
#define  NO_FILE_OPEN_ERR                             24703
/* [24704] to [24750] unused */
#define  IE_SRCEBUF_ADDLNFILEBUFFER_1_L               24751
#define  IE_SRCEBUF_ADDLNFILEBUFFER_2_L               24752
#define  IE_SRCEBUF_ADDLNFILEBUFFER_3_L               24753
#define  IE_SRCEBUF_ADDLNFILEBUFFER_4_L               24754
#define  IE_SRCEBUF_FBGETS_NO_SEEK_DONE               24755
#define  IE_SRCEBUF_FGETALINE_NO_LANG                 24756
#define  IE_SRCEBUF_FBOPEN_1                          24757
/* [24758] to [26000] unused */

/* From [24800] to [24899] */


/* From [24900] to [24999] */


/* From [25000] to [25099] */


/* From [25100] to [25199] */


/* From [25200] to [25299] */


/* From [25300] to [25399] */


/* From [25400] to [25499] */


/* From [25500] to [25599] */


/* From [25600] to [25699] */


/* From [25700] to [25799] */


/* From [25800] to [25899] */


/* From [25900] to [25999] */


/* From [26000] to [26099] */

#define  IE_INIT_PARSER                               26001
/* [26002] to [26999] unused */

/* From [26100] to [26199] */


/* From [26200] to [26299] */


/* From [26300] to [26399] */


/* From [26400] to [26499] */


/* From [26500] to [26599] */


/* From [26600] to [26699] */


/* From [26700] to [26799] */


/* From [26800] to [26899] */


/* From [26900] to [26999] */


/* From [27000] to [27099] */

#define  IE_WINUTILS00                                27000
#define  IE_WINUTILS01                                27001
#define  IE_WINUTILS02                                27002
#define  IE_WINUTILS03                                27003
#define  IE_WINUTILS04                                27004
#define  IE_WINUTILS05                                27005
#define  IE_WINUTILS06                                27006
#define  IE_WINUTILS07                                27007
#define  IE_WINUTILS08                                27008
#define  IE_WINUTILS09                                27009
#define  IE_WINUTILS10                                27010
#define  IE_WINUTILS11                                27011
#define  IE_XVTCTRL00                                 27012                                  
#define  IE_XVTCTRL01                                 27013                                  
#define  IE_INLNMEM_00                                27014
#define  IE_SPECLADR_00                               27015
#define  IE_INIT_00                                   27016
#define  IE_INIT_01                                   27017
#define  IE_INLNASM_00                                27018
#define  IE_XVTCTRL02                                 27019                                  
/* [27020] to [27049] unused */
#define  IE_WINLIST00                                 27050
#define  IE_WINLIST01                                 27051
#define  IE_WINLIST02                                 27052
#define  IE_WINLIST03                                 27053
#define  IE_WINLIST04                                 27054
/* [27055] to [27149] unused */

/* From [27100] to [27199] */

#define  IE_POSNFILE00                                27150
#define  IE_POSNFILE01                                27151
#define  IE_POSNFILE02                                27152
#define  IE_POSNFILE03                                27153
#define  IE_POSNFILE04                                27154
#define  IE_POSNFILE05                                27155
#define  IE_POSNFILE06                                27156
#define  IE_POSNFILE07                                27157
#define  IE_POSNFILE08                                27158
#define  IE_POSNFILE09                                27159
#define  IE_POSNFILE10                                27160
#define  IE_POSNFILE11                                27161
#define  IE_POSNFILE12                                27162
#define  IE_POSNFILE13                                27163
#define  IE_POSNFILE14                                27164
#define  IE_POSNFILE15                                27165
/* [27166] to [27199] unused */

/* From [27200] to [27299] */

#define  IE_DISPLAY00                                 27200
#define  IE_DISPLAY01                                 27201
#define  IE_DISPLAY02                                 27202
/* [27203] to [27249] unused */
#define  IE_ABOUTDLG00                                27250
/* [27251] to [27299] unused */

/* From [27300] to [27399] */

#define  IE_WININIT00                                 27300
#define  IE_WININIT01                                 27301
#define  IE_WININIT02                                 27302
#define  IE_WININIT03                                 27303
#define  IE_WININIT04                                 27304
#define  IE_WININIT05                                 27305
#define  IE_WININIT06                                 27306
#define  IE_WININIT07                                 27307
#define  IE_WININIT08                                 27308
/* [27309] to [27349] unused */
#define  IE_CMDWIN00                                  27350
#define  IE_CMDWIN01                                  27351
#define  IE_CMDWIN02                                  27352
#define  IE_CMDWIN03                                  27353
#define  IE_CMDWIN04                                  27354
#define  IE_CMDWIN05                                  27355
#define  IE_CMDWIN06                                  27356
#define  IE_CMDWIN07                                  27357
#define  IE_CMDWIN08                                  27358
#define  IE_CMDWIN09                                  27359
#define  IE_CMDWIN10                                  27360
#define  IE_CMDWIN11                                  27361
#define  IE_CMDWIN12                                  27362
#define  IE_CMDWIN13                                  27363
#define  IE_CMDWIN14                                  27364
#define  IE_CMDWIN15                                  27365
/* [27366] to [27399] unused */

/* From [27400] to [27499] */

#define  IE_TXEDIT00                                  27400
#define  IE_TXEDIT01                                  27401
#define  IE_TXEDIT02                                  27402
/* [27403] to [27449] unused */
#define  IE_MSWIN00                                   27450
#define  IE_MSWIN01                                   27451
#define  IE_MSWIN02                                   27452
#define  IE_MSWIN03                                   27453
#define  IE_MSWIN04                                   27454
#define  IE_MSWIN05                                   27455
#define  IE_MSWIN06                                   27456
#define  IE_MSWIN07                                   27457
#define  IE_MSWIN08                                   27458
#define  IE_MSWIN09                                   27459
#define  IE_MSWIN10                                   27460
#define  IE_MSWIN11                                   27461
#define  IE_MSWIN12                                   27462
#define  IE_MSWIN13                                   27463
#define  IE_MSWIN14                                   27464
#define  IE_MSWIN15                                   27465
#define  IE_MSWIN16                                   27466
#define  IE_MSWIN17                                   27467
#define  IE_MSWIN18                                   27468
#define  IE_MSWIN19                                   27469
#define  IE_MSWIN20                                   27470
#define  IE_MSWIN21                                   27471
/* [27472] to [27499] unused */

/* From [27500] to [27599] */

#define  IE_CMDLINE00                                 27500
#define  IE_CMDLINE01                                 27501
#define  IE_CMDLINE02                                 27502
#define  IE_CMDLINE03                                 27503
#define  IE_CMDLINE04                                 27504
#define  IE_CMDLINE05                                 27505
#define  IE_CMDLINE06                                 27506
#define  IE_CMDLINE07                                 27507
/* [27508] to [27549] unused */
#define  IE_WINWRITE00                                27550
#define  IE_WINWRITE01                                27551
#define  IE_WINWRITE02                                27552
/* [27553] to [27559] unused */
#define  IE_HLFILL00                                  27560
#define  IE_HLFILL01                                  27561
#define  IE_HLFILL02                                  27562
#define  IE_HLFILL03                                  27563
#define  IE_HLFILL04                                  27564
#define  IE_HLFILL05                                  27565
#define  IE_HLFILL06                                  27566
#define  IE_HLFILL07                                  27567
#define  IE_HLFILL08                                  27568
#define  IE_HLFILL09                                  27569
/* [27570] to [27579] unused */
#define  IE_CSFILL00                                  27580
#define  IE_CSFILL01                                  27581
#define  IE_CSFILL02                                  27582
#define  IE_CSFILL03                                  27583
#define  IE_CSFILL04                                  27584
#define  IE_CSFILL05                                  27585
/* [27586] to [27599] unused */

/* From [27600] to [27699] */


/* From [27700] to [27799] */

#define  IE_WINEVENT00                                27700
#define  IE_WINEVENT01                                27701
#define  IE_WINEVENT02                                27702
#define  IE_WINEVENT03                                27703
#define  IE_WINEVENT04                                27704
#define  IE_WINEVENT05                                27705
#define  IE_WINEVENT06                                27706
#define  IE_WINEVENT07                                27707
#define  IE_WINEVENT08                                27708
#define  IE_WINEVENT09                                27709
/* [27710] to [27749] unused */                            
#define  IE_PFWINIT00                                 27750
#define  IE_PFWINIT01                                 27751
#define  IE_PFWINIT02                                 27752
/* [27753] to [27799] unused */

/* From [27800] to [27899] */

#define  IE_DISPMSG00                                 27800
#define  IE_DISPMSG01                                 27801
/* [27802] to [27849] unused */
#define  IE_ESFILL00                                  27850
/* [27851] to [27899] unused */

/* From [27900] to [27999] */

#define  IE_WINSCROL00                                27900
#define  IE_WINSCROL01                                27901
/* [27902] to [27949] unused */
#define  IE_BRKPOINT00                                27950
#define  IE_BRKPOINT01                                27951
#define  IE_BRKPOINT02                                27952
#define  IE_BRKPOINT03                                27953
#define  IE_BPDLG00                                   27954
/* [27955] to [28009] unused */

/* From [28000] to [28099] */

#define  IE_FILE00                                    28010
/* [28011] to [28019] unused */
#define  IE_RUN00                                     28020
/* [28021] to [28029] unused */
#define  IE_GETADDR00                                 28030
#define  IE_GETADDR01                                 28031
#define  IE_GETADDR02                                 28032
#define  IE_GETADDR03                                 28033
#define  IE_GETVALUE00                                28034
#define  IE_GETVALUE01                                28035
/* [28036] to [28049] unused */
#define  IE_MEMORY00                                  28050
/* [28051] to [28099] unused */

/* From [28100] to [28199] */

#define  IE_MONVAR00                                  28100
/* [28101] to [28109] unused */
#define  IE_UNMONVAR00                                28110
/* [28111] to [28119] unused */
#define  IE_MEMMAP00                                  28120
#define  IE_MEMMAP01                                  28121    
#define  IE_MEMMAP02                                  28122    
#define  IE_MEMMAP03                                  28123    
/* [28124] to [28129] unused */
#define  IE_CLOCK00                                   28130
#define  IE_CLOCK01                                   28131
/* [28132] to [28139] unused */
#define  IE_PROCMODE00                                28140
#define  IE_PROCMODE01                                28141
/* [28142] to [28149] unused */
#define  IE_BASES00                                   28150
/* [28151] to [28159] unused */
#define  IE_EMULCFG00                                 28160
/* [28161] to [28169] unused */
#define  IE_CFGWARN00                                 28170
/* [28171] to [28179] unused */
#define  IE_ABOUTDLG                                  28180
/* [28181] to [28189] unused */
#define  IE_CBARSBAR00                                28190
#define  IE_CBARSBAR01                                28191
/* [28192] to [28199] unused */
#define  IE_UNIVCFG_00                                28192
#define  IE_UNIVCFG_01                                28193
#define  IE_UNIVCFG_02                                28194
#define  IE_UNIVCFG_03                                28195
#define  IE_UNIVCFG_04                                28196
#define  IE_UNIVCFG_05                                28197
#define  IE_UNIVCFG_06                                28198
#define  IE_UNIVCFG_07                                28199

/* From [28200] to [28299] */

#define  IE_PROFILE00                                 28200
#define  IE_PROFILE01                                 28201
#define  IE_PROFILE02                                 28202
#define  IE_PROFILE03                                 28203
#define  IE_PROFILE04                                 28204
/* [28205] to [28209] unused */
#define  IE_WINHELP00                                 28210
#define  IE_WINHELP01                                 28211
#define  IE_WINHELP02                                 28212
#define  IE_WINHELP03                                 28213
/* [28214] to [28219] unused */
#define  IE_ASFILL00                                  28220
/* [28221] to [28229] unused */
#define  IE_RUNGROUP00                                28230
/* [28231] to [28239] unused */
#define  IE_COMMDLG00                                 28240
/* [28241] to [28259] unused */
#define  IE_EXEKEY00                                  28260
#define  IE_EXEKEY01                                  28261
/* [28262] to [28269] unused */
#define  IE_PFWFOCUS00                                28270
#define  IE_PFWFOCUS01                                28271
/* [28272] to [28279] unused */
#define  IE_CODEWINS00                                28280
#define  IE_CODEWINS01                                28281
#define  IE_CODEWINS02                                28282
/* [28283] to [28289] unused */
#define  IE_TRACETRK00                                28290
#define  IE_HALTDDC00                                 28291
/* [28292] to [28294] unused */
#define  IE_BUTTNBAR00                                28295
#define  IE_BUTTNBAR01                                28296
#define  IE_BUTTNBAR02                                28297
#define  IE_BUTTNBAR03                                28298
#define  IE_BUTTNBAR04                                28299

/* From [28300] to [28399] */

#define  IE_TPFILL00                                  28300
#define  IE_TPFILL01                                  28301
/* [28302] to [28309] unused */
#define  IE_XVTMENU00                                 28310
#define  IE_XVTMENU01                                 28311
#define  IE_XVTMENU02                                 28312
#define  IE_XVTMENU03                                 28313
#define  IE_XVTMENU04                                 28314
#define  IE_XVTMENU05                                 28315
#define  IE_XVTMENU06                                 28316
/* [28317] to [28319] unused */
#define  IE_TCFILL00                                  28320
#define  IE_TCFILL01                                  28321
/* [28322] unused */
#define  IE_TCFILL02                                  28323
#define  IE_TCFILL03                                  28324
#define  IE_TCFILL04                                  28325
/* [28326] to [28329] unused */
#define  IE_TRACEWIN00                                28330
/* [28331] to [28339] unused */
#define  IE_VIEW00                                    28340
#define  IE_VIEW01                                    28341
#define  IE_VIEW02                                    28342
/* [28343] to [28349] unused */
#define  IE_QUICKTRG00                                28350
/* [28351] to [28359] unused */
#define  IE_TURNTRON00                                28360
#define  IE_EDADDTBL00                                28361
#define  IE_EDADDTBL01                                28362
#define  IE_EDADDTBL02                                28363
#define  IE_EDADDTBL03                                28364
#define  IE_EDADDTBL04                                28365
#define  IE_EDADDTBL05                                28366
#define  IE_EDADDTBL06                                28367
#define  IE_EDADDTBL07                                28368
/* [28369] unused */
#define  IE_EDITTRIG00                                28370
/* [28371] to [28379] unused */
#define  IE_EVNTSPEC00                                28380
#define  IE_EVNTSPEC01                                28381
#define  IE_EVNTSPEC02                                28382
#define  IE_EVNTSPEC03                                28383
#define  IE_EVNTSPEC04                                28384
#define  IE_EVNTSPEC05                                28385
#define  IE_EVNTSPEC06                                28386
#define  IE_EVNTSPEC07                                28387
#define  IE_EVNTSPEC08                                28388
#define  IE_EVNTSPEC09                                28389
#define  IE_EVNTSPEC10                                28390
#define  IE_EVNTSPEC11                                28391
#define  IE_EVNTSPEC12                                28392
#define  IE_EVNTSPEC13                                28393
#define  IE_EVNTSPEC14                                28394
#define  IE_EVNTSPEC15                                28395
#define  IE_EVNTSPEC16                                28396
#define  IE_EVNTSPEC17                                28397
#define  IE_EVNTSPEC18                                28398
/* [28399] to [28399] unused */

/* From [28400] to [28499] */

#define  IE_STRSTTRG00                                28400
/* [28401] to [28409] unused */
#define  IE_CNTEVTRG00                                28410
/* [28411] to [28419] unused */
#define  IE_ONSTPTRG00                                28420
/* [28421] to [28429] unused */
#define  IE_TRACEMOD00                                28430
/* [28431] to [28439] unused */
#define  IE_STRSTTRA00                                28440
/* [28441] to [28449] unused */
#define  IE_WTRCBUFF00                                28450
/* [28451] to [28459] unused */
#define  IE_LINESTRC00                                28460
/* [28461] to [28469] unused */
#define  IE_EDDATTBL00                                28470
/* [28471] to [28474] unused */
#define  IE_COCODLGS00                                28475
/* [28476] to [28477] unused */
#define  IE_CODEBROW21                                28478
#define  IE_CODEBROW20                                28479
#define  IE_CODEBROW00                                28480
#define  IE_CODEBROW01                                28481
#define  IE_CODEBROW02                                28482
#define  IE_CODEBROW03                                28483
#define  IE_CODEBROW04                                28484
#define  IE_CODEBROW05                                28485
#define  IE_CODEBROW06                                28486
#define  IE_CODEBROW07                                28487
#define  IE_CODEBROW08                                28488
#define  IE_CODEBROW09                                28489
#define  IE_CODEBROW10                                28490
#define  IE_CODEBROW11                                28491
#define  IE_CODEBROW12                                28492
#define  IE_CODEBROW13                                28493
#define  IE_CODEBROW14                                28494
#define  IE_CODEBROW15                                28495
#define  IE_CODEBROW16                                28496
#define  IE_CODEBROW17                                28497
#define  IE_CODEBROW18                                28498
#define  IE_CODEBROW19                                28499

/* From [28500] to [28599] */

#define  IE_FONT00                                    28500
#define  IE_FONT01                                    28501
#define  IE_FONT02                                    28502
#define  IE_FONT03                                    28503
#define  IE_FONT04                                    28504
#define  IE_FONT05                                    28505
#define  IE_FONT06                                    28506
#define  IE_FONT07                                    28507
#define  IE_FONT08                                    28508
#define  IE_FONT09                                    28509
/* [28510] to [28599] unused */

/* From [28600] to [28629] */

#define  IE_COCOSTAT00                                28600
#define  IE_COCOSTAT01                                28601
#define  IE_COCOSTAT02                                28602
#define  IE_COCOSTAT03                                28603
#define  IE_COCOSTAT04                                28604
#define  IE_COCOSTAT05                                28605
#define  IE_COCOSTAT06                                28606
#define  IE_COCOSTAT07                                28607
#define  IE_COCOSTAT08                                28608
#define  IE_COCOSTAT09                                28609
#define  IE_COCOSTAT10                                28610
#define  IE_COCOSTAT11                                28611
#define  IE_COCOSTAT12                                28612
#define  IE_COCOSTAT13                                28613
#define  IE_COCOSTAT14                                28614
#define  IE_COCOSTAT15                                28615
/* [28616] to [28629] unused */

/* From [28630] to [28649] */
#define  IE_REPGEN1                                   28616
#define  IE_REPGEN2                                   28617
/* [28632] to [28649] unused */
/* From [28650] to [28674] */
#define  IE_COCOINFO1                                 28650
/* [28651] to [28674] unused */

/* From [28675] to [28689] */
#define  IE_COCOLINE1                                 28675
/* [28676] to [28689] unused */

/* From [28690] to [28699] */
#define  IE_COCOSAVE1                                 28690
/* [28691] to [28699] unused */

/* From [28700] to [28799] */
#define IE_SEACTRL00        28700 
#define IE_SEACTRL01        28701
#define IE_SEACTRL02        28702
#define IE_SEACTRL03        28703
#define IE_SEACTRL04        28704
#define IE_METER00          28705
#define IE_METER01          28706
#define IE_METER02          28707
#define IE_METER03          28708
#define IE_METER04          28709
#define IE_METER05          28710
#define IE_METER06          28711
#define IE_METER07          28712
#define IE_METER08          28713
#define IE_METER09          28714
#define IE_METER10          28715
#define IE_METER11          28716
#define IE_METER12          28717
#define IE_METER13          28718
#define IE_METER14          28719
#define IE_SEAFTRACE00      28720
#define IE_SEAFTRACE01      28721
#define IE_SEAFTRACE02      28722
#define IE_SEAFTRACE03      28723
#define IE_SEAFTRACE04      28724
#define IE_SEAFTRACE05      28725
#define IE_SEAFTRACE06      28726
#define IE_SEAPERFANALYS00  28727
#define IE_SEAPERFANALYS01  28728
#define IE_SEAPERFANALYS02  28729
#define IE_SEAPERFANALYS03  28730
#define IE_DLGTEMPL00       28731
#define IE_DLGTEMPL01       28732
#define IE_PCPERF00         28733
#define IE_PCPERF01         28734
#define IE_PCPERF02         28735
#define IE_FNSELECT00       28736
#define IE_FNSELECT01       28737
#define IE_FNSELECT02       28738
#define IE_FNSELECT03       28739
#define IE_FNSELECT04       28740
#define IE_FNSELECT05       28741
#define IE_FNSELECT06       28742
#define IE_FNSELECT07       28743
#define IE_FNSELECT08       28744
#define IE_FNSELECT09       28745
#define IE_FNSELECT10       28746
#define IE_FNSELECT11       28847
#define IE_FNSELECT12       28848
#define IE_FNSELECT13       28849
#define IE_FNSELECT14       28850
#define IE_FNSELECT15       28851
#define IE_FNSELECT16       28852
#define IE_FNSELECT17       28853
#define IE_FNSELECT18       28854
#define IE_ADDRPAIR00       28855
#define IE_ADDRPAIR01       28856
#define IE_ADDRPAIR02       28857
#define IE_ADDRPAIR03       28858
#define IE_ADDRPAIR04       28859
#define IE_ADDRPAIR05       28860
#define IE_ADDRPAIR06       28861
#define IE_ADDRPAIR07       28862
#define IE_ADDRPAIR08       28863
#define IE_ADDRPAIR09       28864
#define IE_SEAUTIL00        28865
#define IE_SEAUTIL01        28866
#define IE_SEAUTIL02        28867
#define IE_SEASAVE00        28868
#define IE_SEASAVE01        28869
#define IE_SEASAVE02        28870
#define IE_SEASAVE03        28871
#define NO_FILE_SELECTED    28872  
/* [28873] to [28799] unused */



/* From [28800] to [28899] */


/* From [28900] to [28999] */


/* From [29000] to [29099] */

#define  IE_DATARANG00                                29000
#define  IE_DATARANG01                                29001
#define  IE_DATARANG02                                29002
#define  IE_SYMINIT00                                 29003
#define  IE_SYMINIT01                                 29004
#define  IE_SYMINIT02                                 29005
#define  IE_SYMINIT03                                 29006
#define  IE_SYMINIT04                                 29007
#define  IE_LOADFILE00                                29008
#define  IE_LOADUTIL00                                29009
#define  IE_LOADUTIL01                                29010
#define  IE_LOADUTIL02                                29011
#define  IE_LOADUTIL03                                29012
#define  IE_SAVEFILE00                                29013
#define  IE_NO_EXE_IN_FILE                            29014
#define  IE_COMM00                                    29015
#define  IE_TCODEINF00                                29016
#define  IE_SPGAPI00                                  29017
#define  IE_SPGAPI01                                  29018
#define  INVALID_HILEVEL_ENABLE_FILE                  29019
#define  IE_PROCCFG00                                 29020
#define  IE_PROCCFG01                                 29021
#define  IE_PROCCFG02                                 29022
#define  IE_PROCCFG03                                 29023
#define  IE_PROCCFG04                                 29024
#define  IE_PROCCFG05                                 29025
#define  IE_PROCCFG06                                 29026
#define  IE_PROCCFG07                                 29027
#define  IE_PROCCFG08                                 29028
#define  IE_PROCCFG09                                 29029
#define  IE_PROCCFG10                                 29030
#define  IE_TRACEQUA00                                29031
#define  IE_TRACEQUA01                                29032
#define  IE_PROCCFG11                                 29033
#define  IE_PROCCFG12                                 29034
#define  IE_BPUTIL00                                  29035
#define  IE_SYMINIT05                                 29036
#define  IE_SYMINIT06                                 29037
#define  IE_ASDOFUTL00                                29038   
#define  IE_ASDOFUTL01                                29039   
#define  IE_A_TYP_RW00                                29040
#define  IE_OVERFLOW00                                29041
#define  IE_ASDOFUTL02                                29042
#define  IE_STPTRACE00                                29043
#define  IE_SEACMD00                                  29044
#define  IE_SEACMD01                                  29045
#define  IE_FINDEV00                                  29046
#define  IE_FT00                                      29047
#define  IE_FT01                                      29048
#define  IE_FT02                                      29049
#define  IE_FT03                                      29050
#define  IE_FT04                                      29051
#define  IE_FT05                                      29052
#define  IE_FTTRIG00                                  29053
#define  IE_JUMPTO00                                  29054
#define  IE_FTTRIG01                                  29055   //TM
#define  IE_FTTRIG02                                  29056   //TM
#define  IE_PROCCFG13                                 29057
#define  INVALID_STEPCAM_ENABLE_FILE                  29058
/* [29059] to [29999] unused */                            

/* From [29100] to [29199] */
#define  IE_TXRXCMD_SIMULATE_EMULATOR_00               29100   
#define  IE_TXRXCMD_SIMULATE_EMULATOR_01               29101   
#define  IE_TXRXCMD_SIMULATE_EMULATOR_02               29102   
#define  IE_TXRXCMD_SIMULATE_EMULATOR_03               29103   
#define  IE_TXRXCMD_SIMULATE_EMULATOR_04               29104   
#define  IE_TXRXCMD_SIMULATE_EMULATOR_05               29105   
#define  IE_TXRXCMD_SIMULATE_EMULATOR_06               29106   
#define  IE_TXRXCMD_GET_MAP_GRAN                       29107
#define  IE_TXRXCMD00                                  29108   
#define  IE_TXRXCMD01                                  29109   
#define  IE_TXRXCMD02                                  29110   
/* [29111] to [29199] unused */ 


/* From [29200] to [29299] */
#define  IE_XTPFILL_00                                29200
#define  IE_XTPFILL_01                                29201
#define  IE_XTPFILL_02                                29202
#define  IE_XTCFILL_00                                29203
#define  IE_XTCFILL_01                                29204
#define  IE_XTCFILL_02                                29205
#define  IE_XTCFILL_03                                29206
/* [29207] to [29299] unused */ 


/* From [29300] to [29399] */
#define  IE_SFHITECH00                                29300
#define  NO_SYMBOL_TABLE_IN_MAP_FILE                  29301
#define  NO_MEM_FOR_GLOB_SYMBOL           29302
#define  INSUFFICIENT_MEM_FOR_FUNCTION                29303
#define  CAN_NOT_ASSIGN_CODE_RANGES                   29304
#define  ERR_OPEN_SYM_FILE_E                          29305
#define  NO_LOCALS_SECTION_IN_SYM_FILE                29306
#define  UNEXPECTED_END_OF_SYM_FILE                   29307
#define  ERROR_READING_SYM_FILE                       29308
#define  ERROR_READING_SYM_FILE_01                    29309
#define  ERROR_READING_LINE_NUM_TABLE                 29310
#define  MEM_LINESYMB_01                              29311
#define  ERROR_CONVERTING_LINE_ADDRESS                29312
/* [29313] unused */
#define  ERR_OPEN_SOURCE_FILE_E                       29314
#define  ERR_CLOSE_SOURCE_FILE                        29315
#define  ERROR_IN_MODULE_HANDLING                     29316
#define  IE_LINESYMB_01                               29317
#define  FREE_LINE_NUM_STRUCT_E                       29318
#define  NO_MODULES_IN_SYM_FILE                       29319
#define  CANT_CREATE_SDB_FILE_NAME                    29320
#define  ERR_READING_SDB_FILE00                       29320
#define  ERR_READING_SDB_FILE01                       29321
#define  UNKNOWN_DECLARATION_TYPE                     29322
#define  IE_PROCSDB_01                                29323
#define  SFHITECH_TO_DO_ENUM_DECLARATION              29324
#define  ERR_DECLARATION_NOT_TERMINATED               29325
#define  UNKNOWN_ADDR_FOR_SYMBOL                      29326
#define  ERR_NO_FUNCTION_LIST                         29327
#define  ERR_NO_SYMBOL_LIST                           29328
#define  ERR_FUNCTION_NOT_FOUND                       29329
#define  ERR_SYMBOL_NOT_FOUND                         29330
#define  IE_GETTYPE_01                                29330
#define  IE_GETTYPE_02                                29331
#define  CANT_FIND_USER_TYPE                          29332
#define  NO_MEM_FOR_TYPE_DEF                          29333
#define  NOT_A_VAILD_TYPE_STRING                      29334
#define  NON_DEFINING_DECLARATION_ERROR               29335
#define  UNKNOWN_UNSIGNED_TYPE                        29336
#define  UNKNOWN_SIGNED_TYPE                          29337
#define  UNKNOWN_FUNCTION_TYPE                        29338
#define  IE_PROCSDB_02                                29339
#define  MISSING_STRUCT_UNION_TYPE_DEF                29340
#define  UNKNOWN_ELEMENT_SIZE                         29340
#define  NO_MEM_FOR_STRUCT_MEMBER                     29341
#define  ERR_WRONG_STRUCT_DECLARATION                 29342
#define  NO_MEM_FOR_STRUCT_UNION                      29343
#define  CANT_READ_BIT_FIELD_SPEC                     29344
#define  IE_PROCMAP_00                                29345
#define  IE_PROCMAP_01                                29346
#define  IE_PROCMAP_02                                29347
#define  IE_PROCMAP_03                                29348
#define  CANNOT_OPEN_HEX_FILE                         29349
#define  IE_PROCHEX_00                                29350
#define  UNKNOWN_INTEL_REC_TYPE                       29350
#define  UNKNOWN_REGISTER_NUMBER                      29351
#define  CANT_READ_LOC_VAR_MEM_SIZE                   29352
#define  CANT_FIND_MEM_MODEL                          29353
#define  NO_MEM_FOR_SOURCE_LIST                       29354
#define  CANT_FIND_COMPILER_TYPE                      29355
#define  MISSING_LINE_NUM_INFO                        29356
#define  NO_CLASS_CODE_IN_MAP_FILE                    29357       
#define  ERROR_READING_SYM_FILE_02                    29358       
#define  ERROR_READING_FAKE_SYMBOLS                   29359
#define  ERROR_READING_ASSEMBLER_TABLE                29360
#define  NO_MEM_FOR_ASSEMBLER_RANGE                   29360
#define  ID_PROCMAP_01                                29361
#define  ID_PROCMAP_02                                29362
#define  ID_PROCSDB_01                                29363
#define  ID_GETTYPE_01                                29364
#define  ID_GETTYPE_02                                29365
#define  ID_GETTYPE_03                                29366
#define  ID_GETTYPE_04                                29367
#define  ID_GETTYPE_05                                29368
#define  ID_PROCSDB_02                                29369
#define  ID_PROCSDB_03                                29370
#define  ID_PROCSDB_04                                29371
#define  ID_PROCSL_01                                 29372
#define  ID_PROCASM_01                                29373
/* From [29378] to [29399] */
                                                         
/* From [29400] to [29499] */
#define  PROCESSOR_NOT_SUPPORTED                      29400  
#define  WRONG_PROCESSOR_SPECIFIED                    29401  
#define  USE_COSMIC_VERSION_3                         29402  
#define  USE_COSMIC_VERSION_4                         29403  
#define  IE_PROCCOS08                                 29404  
#define  IE_PROCCOS09                                 29405  
#define  NO_MEM_FOR_FORREF_LIST                       29406
/* [29407] to [29409] unused */ 

/* From [29410] to [29499] */
#define  ASSIGN_VAR_LETTER_EXPECTED                   29410  
#define  ASSIGN_VAR_NUMBER_EXPECTED                   29411  
#define  ASSIGN_W0__W7_EXPECTED                       29412  
#define  AT_ATTRIBUTE_EXPECTED                        29413  
#define  AT_NAME_EXPECTED                             29414  
#define  AT_SYMBOL_TYPE_INDEX_EXPECTED                29415  
#define  AT_VAR_LETTER_EXPECTED                       29416  
#define  AT_VAR_NUMBER_EXPECTED                       29417  
#define  BB10_TOOL_TYPE_EXPECTED                      29418  
#define  BB10_ZERO_NAME_EXPECTED                      29419  
#define  BB11_SECTION_SIZE_EXPECTED                   29420  
#define  BB_BLOCK_SIZE_EXPECTED                       29421  
#define  BB_BLOCK_TYPE_EXPECTED                       29422  
#define  BB_BLOCK_TYPE_UNKNOWN                        29423  
#define  BB_NAME_EXPECTED                             29424  
#define  DATA_RELOC_CMDS_NOT_SUPPORTED                29425  
#define  FUNCTION_END_ADDR_EXPECTED                   29426  
#define  IE_PROCIEEE00                                29427  
#define  IE_PROCIEEE01                                29428  
#define  IE_PROCIEEE02                                29429  
#define  IE_PROCIEEE03                                29430  
#define  IE_PROCIEEE04                                29431  
#define  IE_PROCIEEE05                                29432  
#define  IE_PROCIEEE06                                29433  
#define  IE_PROCIEEE07                                29434  
#define  IE_UTILIEEE01                                29435  
#define  IE_UTILIEEE02                                29436  
#define  IE_UTILIEEE03                                29437  
#define  IE_UTILIEEE04                                29438  
#define  IE_SFIEEE00                                  29439  
#define  INSUFFICENT_SAVE_VAR_MEMORY                  29440  
#define  INVALID_BB4_RECORD                           29441  
#define  INVALID_BB6_RECORD                           29442  
#define  INVALID_BB_BLOCK_IN_LEVEL_BB10               29443  
#define  INVALID_BB_BLOCK_IN_LEVEL_BB3                29444  
#define  INVALID_BB_BLOCK_IN_LEVEL_BB4                29445  
#define  INVALID_BB_BLOCK_IN_LEVEL_BB5                29446  
#define  INVALID_BB_BLOCK_IN_LEVEL_BB6                29447  
#define  INVALID_BB_BLOCK_IN_LEVEL_DEBUG              29448  
#define  LD_RECORD_DATA_TOO_LONG                      29449  
#define  LD_RECORD_WITHOUT_PC_ADDRESS                 29450  
#define  NN_INDEX_EXPECTED                            29451  
#define  NN_NAME_EXPECTED                             29452  
#define  NO_DEBUG_INFO_AVAILABLE                      29453  
#define  TY_SYMBOL_TYPE_EXPECTED                      29454  
#define  TY_TYPE_INDEX_EXPECTED                       29455  
#define  TY_VAR_LETTER_EXPECTED                       29456  
#define  TY_VAR_NUMBER_EXPECTED                       29457  
#define  UNEXPECTED_COMMAND_IN_BB1                    29458  
#define  UNEXPECTED_COMMAND_IN_BB10                   29459  
#define  UNEXPECTED_COMMAND_IN_BB11                   29460  
#define  UNEXPECTED_COMMAND_IN_BB2                    29461  
#define  UNEXPECTED_COMMAND_IN_BB3                    29462  
#define  UNEXPECTED_COMMAND_IN_BB4                    29463  
#define  UNEXPECTED_COMMAND_IN_BB5                    29464  
#define  UNEXPECTED_COMMAND_IN_BB6                    29465  
#define  IEEE_EXPR_EXPECTED                           29466  
#define  IEEE_STRING_EXPECTED                         29467  
#define  IEEE_NUMBER_EXPECTED                         29468  
#define  IEEE_MB_EXPECTED                             29469  
#define  IEEE_AD_EXPECTED                             29470  
#define  IEEE_BB__EXPECTED                            29471  
#define  IEEE_UNEXPECTED_TOKEN                        29472  
#define  IEEE_UNDEF_VAR_IN_EXPR                       29473  
#define  IEEE_INVALID_EXPR                            29474  
#define  IEEE_FUNCTION_NOT_SUPPORTED                  29475  
#define  IEEE_EXPR_TOO_COMPLEX                        29476  
#define  IE_TOKIEEE00                                 29477  
#define  INSUFFICENT_SAVE_ENTRY_MEMORY                29478
#define  TY_STRUCT_SIZE_EXPECTED                      29479  
#define  TY_MEMBER_NAME_EXPECTED                      29480
#define  TY_MEMBER_TYPE_INDEX_EXPECTED                29481
#define  TY_MEMBER_OFFSET_EXPECTED                    29482
#define  TY_MEMBER_SIZE_EXPECTED                      29483
#define  IE_PROCIEEE08                                29484
#define  IE_PROCIEEE09                                29485
#define  IE_PROCIEEE10                                29486
#define  IE_PROCIEEE11                                29487 
#define  OPTIMISED_AWAY_WITH_EQUALS                   29488 
#define  OPTIMISED_AWAY_PERMANENT                     29489 
#define  OPTIMISED_AWAY_TEMPORARY                     29490 

/* [29491] to [29499] unused */ 

/* From [29500] to [29599] */
#define  IE_PROCGHS00                                 29500
#define  IE_PROCGHS01                                 29501
#define  IE_PROCGHS02                                 29502
#define  IE_PROCGHS03                                 29503
#define  IE_PROCGHS04                                 29504
#define  IE_PROCGHS05                                 29505
#define  IE_PROCGHS06                                 29506
#define  IE_PROCGHS07                                 29507
#define  IE_PROCGHS08                                 29508
#define  IE_PROCGHS09                                 29509
#define  IE_PROCGHS10                                 29510
#define  IE_PROCGHS11                                 29511
#define  IE_PROCGHS12                                 29512
#define  IE_PROCGHS13                                 29513
#define  INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY      29514
#define  INSUFFICENT_DATA_BUFFER_MEMORY               29515
#define  INSUFFICENT_FILE_DESCRIPTOR_TABLE_MEMORY     29516
#define  ERR_NOT_AN_ELF_FILE                          29517
#define  IE_SFGHS00                                   29518
#define  IE_READGHS01                                 29519
#define  IE_READGHS02                                 29520
#define  IE_READGHS03                                 29521
#define  IE_READGHS04                                 29522
#define  IE_READGHS05                                 29523
#define  IE_UTILGHS03                                 29524
#define  IE_UTILGHS04                                 29525

#define  IE_UTILGHS05                                 29526
#define  IE_UTILGHS06                                 29527
#define  IE_UTILGHS07                                 29528
#define  IE_PROCGHS14                                 29529
#define  IE_READGHS06                                 29530

#define  IE_SFDWARF00                                 29531 
#define  IE_PROCDWRF00                                29532
#define  IE_PROCDWRF01                                29533
#define  IE_PROCDWRF02                                29534
#define  IE_PROCDWRF03                                29535
#define  IE_PROCDWRF04                                29536
#define  IE_PROCDWRF05                                29537
#define  IE_PROCDWRF06                                29538
#define  IE_PROCDWRF07                                29539
#define  IE_PROCDWRF08                                29540
#define  IE_PROCDWRF09                                29541
#define  IE_PROCDWRF10                                29542
#define  IE_PROCDWRF11                                29543
#define  IE_PROCDWRF12                                29544
#define  IE_PROCDWRF13                                29545
#define  IE_PROCDWRF14                                29546
#define  IE_READDWRF00                                29547
#define  IE_READDWRF01                                29548
#define  IE_READDWRF02                                29549
#define  IE_READDWRF03                                29550
#define  IE_READDWRF04                                29551
#define  IE_READDWRF05                                29552
#define  IE_READDWRF06                                29553
#define  IE_READDWRF07                                29554
#define  IE_READDWRF08                                29555
#define  IE_READDWRF09                                29556
#define  IE_READDWRF10                                29557
#define  IE_READDWRF11                                29558
#define  IE_READDWRF12                                29559
#define  IE_READDWRF13                                29560
#define  IE_READDWRF14                                29561
#define  IE_READDWRF15                                29562
#define  IE_READDWRF16                                29563
#define  IE_READDWRF17                                29564
#define  IE_READDWRF18                                29565
#define  IE_READDWRF19                                29566
#define  IE_READDWRF20                                29567
#define  IE_READDWRF21                                29568
#define  IE_READDWRF22                                29569
#define  IE_READDWRF23                                29570
#define  IE_UTILDWRF03                                29571
#define  IE_UTILDWRF04                                29572
#define  IE_UTILDWRF05                                29573
#define  IE_UTILDWRF06                                29574
#define  IE_UTILDWRF07                                29575
#define  IE_SOURCE00                                  29576
#define  IE_SOURCE01                                  29577
#define  IE_SOURCE02                                  29578
#define  IE_SOURCE03                                  29579
#define  INSUFFICENT_SECTION_HEADER_TABLE_MEMORY      29580
#define  ERR_WRONG_DWARF_VERSION                      29581
#define  ERR_CANNOT_FIND_ABBREV                       29582
#define  ERR_EXPECTING_ARRAY_DIMENSION                29583
#define  ERR_BAD_ABBREV_OFFSET_IN_COMP_UNIT_HEADER    29584 /*PJS-03-JUL-2000*/
#define  IE_UTILDWRF08                                29585
#define  ERR_SPECIFIED_LOGFILE_IN_USE                 29586
#define  ERR_MUST_USE_ETPU_SWITCH                     29587
#define  IE_UTILDWRF09                                29588

/* From [29600] to [29699] */


/* From [29700] to [29799] */


/* From [29800] to [29899] */


/* From [29900] to [29999] */


/*
XVT uses from 30000 upwards for its strings. See XVTV_RES_STRING_BASE
definition in xvt headers. Hence, only define internal error numbers
> 30000. disp_msg() in dispmsg.c will not attempt to load a message
string if its ID > XVTV_RES_STRING_BASE i.e. it will assume its an
internal error number.
*/

/* From [30000] to [30030] are used by Pfw.rc */

/* From [30031] to [30099] */

#define  NEED_BANKS_FOR_COSMIC_BANKED                 30031
#define  IE_SYMBOLS_FPSTMT_1                          30032
#define  IE_SYMBOLS_FNSTMT_1                          30033
#define  ERR_CLOSE_ASDOF_FILE_E                       30034
#define  IE_SYMBOLS_GETSTATEMENT_1                    30035
#define  IE_SYMBOLS_LOSE_1                            30036
#define  IE_SYMBOLS_GNMA_1                            30037
#define  IE_SYMBOLS_GNMA_2                            30038
#define  IE_SYMBOLS_LIM_1                             30039
#define  COSMIC_LONGINT_NON_SUPPORT                   30040
#define  COSMIC_NON_MULTISEG_FILE                     30041
#define  TOO_MANY_SECTIONS_OR_CORRUPT                 30042
#define  UNKNOWN_MAGIC_NUMBER                         30043
#define  INSUFFIC_MEM_BANK_ARRAY                      30044
#define  INVALID_DEBUG_BANK_SECT_NUM                  30045
#define  UNKNOWN_COSMIC_DEBUG_TYPE                    30046
#define  INSUFFIC_DUPL_LINE_MEM_E                     30047
#define  INVALID_DLINE_RECORD                         30048
#define  CANT_OPEN_FILE_E                             30049
#define  INVALID_DENTRY_RECORD                        30050
#define  INVALID_LST_FILENAME                         30051
#define  INVALID_SEG_E                                30052
#define  INVALID_DATA_RANGE_INDEX                     30053
#define  IE_SYMUTIL2_DEFSYMBOL_1                      30054
#define  IE_SYMUTILS_GLAR_1                           30055
#define  IE_SYMUTILS_GSFN_1                           30056
#define  IE_INV_DEF_TYPE                              30057
#define  INVALID_DSTART_RECORD                        30058
#define  UNEXPECTED_END_OF_FILE                       30059
#define  INSUFFIC_BANK_REC_MEM                        30060
#define  ERROR_READING_COMMAND_LINE                   30061
#define  INVOCATION_LINE_TOO_LONG                     30062
#define  SAME_INPUT_AND_OUTPUT_FILES                  30063
#define  NO_BUF_EXTENSION                             30064
#define  HELP_DISPLAY_PSEUDO_ERROR                    30065
#define  FIRST_CONTROL_NOT_INFILENAME                 30066
#define  EXPECTED_ARGUMENT_BEFORE                     30067
#define  UNKNOWN_CONTROL                              30068
#define  DUPLICATE_CONTROL                            30069
#define  MAX_2_BANK_CONTROLS                          30070
#define  INVALID_MEM_SIZE                             30071
#define  INVALID_BANK_CONTROL                         30072
#define  INCOMPATIBLE_CONTROLS                        30073
#define  MISSING_INCLUDE_OMIT                         30074
#define  MISSING_ARGUMENT_FOR_CONTROL                 30075
#define  INSUFFIC_MEM_FOR_CONTROL                     30076
#define  CANT_NOSTATEMENTS_WITH_ASDOF                 30077
#define  CANT_OPEN_CONTROL_FILE                       30078
#define  CANT_OPEN_C_FILE                             30079
#define  ERROR_CHECKING_LST_FILE                      30080
#define  ERR_CLOSE_LST_FILE_E                         30081
#define  ERR_CLOSE_C_FILE                             30082
#define  ERR_CLOSE_OMF_FILE_E                         30083
#define  CANT_CONVERT_ASDOF_INPUT                     30084
#define  FIRST_REC_INVALID                            30085
#define  WRONG_ASDOF_REVISION                         30086
#define  NOT_ABSOLUTE_REC_ERR                         30087
#define  OBJECT_RECORD_LENGTH_ERR                     30088
#define  TOO_MANY_NESTED_PROCS_F                      30089
#define  TOO_MANY_PROC_ENDS_F                         30090
#define  INVALID_SYMBOL_TYPE                          30091
#define  LINE_INDEX_NOT_FOUND                         30092
#define  INP_OMF_CHECKSUM_ERROR                       30093
#define  INVALID_TEXT_ERR                             30094
#define  PROCEDURE_NOT_FOUND                          30095
#define  OUTPUT_OBJ_WRITE_ERR                         30096
#define  FILE_READ_ERROR                              30097
#define  INSUFFIENT_TEMP_STRUCT_MEM_E                 30098
#define  INSUFFIENT_SYM_MEM                           30099
                                                          
/* From [30100] to [30199] */

#define  IE_OVERFLOW_BUFFERKIND_1                     30100
#define  IE_OVERFLOW_GNMBPOAK_1                       30101
#define  IE_OVERFLOW_ABTML_1                          30102
#define  IE_OVERFLOW_NDB_1                            30103
#define  IE_OVERFLOW_GNAAP_1                          30104
#define  IE_OVERFLOW_GNAAP_2                          30105
#define  IE_OVERFLOW_DIDB_1                           30106
#define  IE_OVERFLOW_UBCOI_1                          30107
#define  IE_OVERFLOW_UPSP_1                           30108
#define  IE_OVERFLOW_CIS_1                            30109
#define  IE_OVERFLOW_PAP_1                            30110
#define  IE_OVERFLOW_PAP_2                            30111
#define  IE_OVERFLOW_CII_1                            30112
#define  IE_OVERFLOW_CII_2                            30113
#define  IE_OVERFLOW_CII_3                            30114
#define  IE_OVERFLOW_CII_4                            30115
#define  IE_OVERFLOW_CII_5                            30116
#define  IE_OVERFLOW_CII_6                            30117
#define  IE_OVERFLOW_CII_7                            30118
#define  IE_OVERFLOW_ABTPFL_1                         30119
#define  IE_OVERFLOW_TBFPFL_1                         30120
#define  IE_OVERFLOW_GNPFB_1                          30121
#define  IE_OVERFLOW_APOD_1                           30122
#define  IE_OVERFLOW_GLPB_1                           30123
#define  IE_OVERFLOW_SUBM_1_L                         30124
#define  IE_OVERFLOW_TFNFIML_1                        30125
#define  IE_OVERFLOW_TFNFIML_2                        30126
#define  IE_OVERFLOW_ATNFIML_1                        30127
#define  IE_OVERFLOW_ATNFIML_2                        30128
#define  IE_OVERFLOW_GANFB_1                          30129
#define  IE_OVERFLOW_WDB_1                            30130
#define  IE_OVERFLOW_WBDB_1                           30131
#define  ARRAY_HAS_TOO_MANY_DIMENSIONS                30132
#define  CONVERTER_NOT_SUPPORTED                      30133
#define  CONFIG_FILE_NOT_FOUND                        30134
#define  BAD_CONFIG_FILE                              30135
#define  INVALID_DEND_RECORD                          30136
#define  EXPECTING_A_NUMBER                           30137
#define  IE_PROCCOS00                                 30138
#define  IE_PROCCOS01                                 30139
#define  IE_PROCCOS02                                 30140
#define  IE_PROCCOS03                                 30141
#define  IE_PROCCOS04                                 30142
#define  IE_PROCCOS05                                 30143
#define  IE_PROCCOS06                                 30144
#define  IE_PROCCOS07                                 30145
#define  IE_SFCM00                                    30146
#define  IE_MODLIST00                                 30147   
#define  IE_MODLIST01                                 30148
#define  IE_MODLIST02                                 30149
#define  IE_PRIORITY_GBP_1                            30150
#define  IE_PRIORITY_AAA_1                            30151
#define  IE_PRIORITY_TFPLL_1                          30152
#define  IE_PRIORITY_ATPLL_1                          30153
#define  IE_PRIORITY_MUIPLL_1                         30154
#define  IE_PRIORITY_GPIPLL_1                         30155
#define  IE_PRIORITY_GPNFMI_1                         30156
#define  IE_PRIORITY_LIPLLE_1                         30157
#define  IE_PRIORITY_LIPLLE_2                         30158
#define  IE_PRIORITY_IPH_1_L                          30159
#define  IE_PRIORITY_GMUMI_1                          30160
#define  ERR_OPEN_LIBINFO_FILE                        30161
#define  LIBINFO_FILE_READ_ERROR                      30162
#define  INVALID_LIBINFO_FILE                         30163
#define  INSUFFIC_LIBINFO_MEM                         30164
#define  ERR_CLOSE_LIBINFO_FILE                       30165
#define  KEIL_RECORDS_IN_PLM_INPUT                    30166
#define  ASDOF_FILE_WRITE_ERR                         30167
#define  CONFLICTING_SOURCE_SPECIFIERS                30168
#define  ERR_OPEN_MAP_FILE_E                          30169
#define  ERR_OPEN_SOURCELIST_FILE_E                   30170
#define  ERROR_READING_MAP_FILE                       30171
#define  ERROR_READING_SOURCELIST_FILE                30172
#define  MISSING_SOURCELIST_MODNAME                   30173
#define  MISSING_SOURCELIST_LANGUAGE                  30174
#define  INVALID_SOURCELIST_LANGUAGE                  30175
#define  UNEXPECTED_SOURCELIST_WORD                   30176
#define  NOT_VALID_PLM_LIST_E                         30177
#define  ERR_CLOSE_MAP_FILE_E                         30178
#define  NO_VALID_LIST_IN_MAP                         30179
#define  NO_VALID_PLM_LIST_IN_MAP                     30180
#define  NO_LIST_INFO_IN_MAP                          30181
#define  NO_VALID_LIST_IN_SOURCES                     30182
#define  NO_VALID_PLM_LIST_IN_SOURCES                 30183
#define  NO_LIST_INFO_IN_SOURCES                      30184
#define  INSUFFICIENT_LST_SYMBOL_MEM                  30185
#define  INSUFFIC_RESERVE_MEM_E                       30186
#define  MEMBER_OF_STRUCTURE_NOT_FOUND                30187
#define  SYMBOL_NOT_FOUND                             30188
#define  ERROR_IN_PLM_LIST_LINES                      30189
#define  NO_SYMBOLS_IN_LIST                           30190
#define  ERROR_IN_PLM_LIST_SYMBS                      30191
#define  UNKNOWN_LIST_SYMBOL_TYPE                     30192
#define  INSUFFICIENT_KEIL_TYPEDEF_MEM                30193
#define  PERMANENT_STRUCTURE_NOT_FOUND                30194
#define  UNKNOWN_KEIL_TYPEDEF                         30195
#define  UNKNOWN_KEIL_TYPEINDEX                       30196
#define  UNKNOWN_KEIL_ARRAYINDEX                      30197
#define  KEIL_STRUCT_TYPEDEF_ERR                      30198
#define  INVALID_KEIL_TYPEINDEX                       30199

/* From [30200] to [30299] */

#define  IE_KERNEL_PACKEDFP_1                         30200
#define  IE_KERNEL_PACKEDFP_2                         30201
#define  IE_KERNEL_POINTERFP_1                        30202
#define  SYM_KERNEL_POINTERFP_2                       30203
#define  SYM_KERNEL_POINTERFP_3                       30204
#define  IE_KERNEL_FI_1                               30205
#define  IE_KERNEL_FI_2                               30206
#define  IE_KERNEL_FI_3                               30207
#define  IE_KERNEL_FI_4                               30208
#define  IE_KERNEL_FI_5                               30209
#define  IE_KERNEL_FI_6                               30210
#define  IE_KERNEL_FI_7                               30211
#define  SYM_KERNEL_FI_8                              30212
#define  IE_KERNEL_FI_9                               30213
#define  IE_KERNEL_FI_10                              30214
#define  IE_KERNEL_FI_11                              30215
#define  IE_KERNEL_NAP_1                              30216
#define  IE_KERNEL_NAP_2                              30217
#define  IE_KERNEL_NAP_3                              30218
#define  IE_KERNEL_NAP_4                              30219
#define  IE_KERNEL_NAP_5                              30220
#define  IE_KERNEL_NAP_6                              30221
#define  IE_KERNEL_NAP_7                              30222
#define  IE_KERNEL_ISDB_1_L                           30223
#define  IE_KERNEL_ISDB_2_L                           30224
#define  IE_KERNEL_SUELM_1                            30225
#define  IE_KERNEL_SUELM_2                            30226
#define  IE_KERNEL_SUELM_1_L                          30227
#define  IE_KERNEL_SUNELM_1                           30228
#define  IE_KERNEL_SUNELM_1_L                         30229
#define  IE_KERNEL_SUTPAUP_1                          30230
#define  IE_KERNEL_SUTPAUP_2                          30231
#define  IE_KERNEL_SUP_1                              30232
#define  IE_KERNEL_SUP_2                              30233
#define  SYMBOLS_INTER_1                              30234
#define  SYMBOLS_INTER_2                              30235
#define  LINE_MEM_ALLOC_SYSTEM_ERROR                  30236
#define  ERR_CLOSE_OBJ_FILE_E                         30237
#define  UNKNOWN_HIERARCHY_TYPE_MODIFIER              30238
#define  NO_MEMORY_FOR_TYPEDEF                        30239
#define  MISSING_STRUCT_TYPEDEF                       30240
#define  TYPEDEF_U_FLOAT_NOT_SUPPORTED                30241
#define  TYPEDEF_U_DOUBLE_NOT_SUPPORTED               30242
#define  NO_MEM_FOR_CODE_RANGE                        30243
#define  SEGMENT_NOT_FOUND_ERROR                      30244
#define  FUNCTION_DEF_NOT_FOUND                       30245
#define  NO_MEM_FOR_SEGMENT_RANGE                     30246
#define  ERROR_UPDATING_CODE_RANGES                   30247
#define  IE_MAKASDOF14                                30248
#define  NO_MEM_FOR_ARRAY_TYPE                        30249
#define  NO_MEM_FOR_POINTER_TYPE                      30250
#define  UNKNOWN_DEF_TYPE                             30251
#define  NO_MEM_FOR_STRUCT                            30252
#define  NO_MEM_FOR_STRUCT_MEMB                       30253
#define  IE_ADDRESS_ARKFSI_1                          30254
#define  IE_ADDRESS_ARKFSI_2                          30255
#define  IE_READADR_GMCO_1                            30256
#define  IE_READADR_GMAO_1                            30257
#define  UNKNOWN_OKI_TYPE_SPEC                        30258
#define  IE_KEILTYPS00                                30259
#define  IE_KEILTYPS01                                30260
#define  IE_KEILTYPS02                                30261
#define  IE_MAKASDOF00                                30262
#define  IE_MAKASDOF01                                30263
#define  IE_MAKASDOF02                                30264
#define  IE_MAKASDOF03                                30265
#define  IE_MAKASDOF04                                30266
#define  IE_MAKASDOF05                                30267
#define  IE_MAKASDOF06                                30268
#define  IE_MAKASDOF07                                30269
#define  IE_MAKASDOF08                                30270
#define  IE_MAKASDOF09                                30271
#define  IE_MAKASDOF10                                30272
#define  IE_MAKASDOF11                                30273
#define  IE_MAKASDOF12                                30274
#define  IE_MAKASDOF13                                30275
#define  IE_SFAMF00                                   30276
#define  IE_LST_ANAL00                                30277
#define  IE_LST_ANAL01                                30278
#define  IE_LST_ANAL02                                30279
#define  IE_MODLIST03                                 30280
#define  IE_IARSTEND00                                30281
#define  SEGMENT_DEF_REC_MISSING                      30282
#define  IE_KERNEL_NAP_8                              30283
#define  IE_KERNEL_FI_12                              30284
#define  IE_OVERFLOW_CII_8                            30285
#define  IE_SYMBOLS_GET_NEAREST_RELOC_1               30286
#define  IE_SYMBOLS_GET_RELOC_1                       30287
#define  IE_SYMBOLS_CREATE_RELOC_1                    30288
#define  IE_SYMBOLS_STORE_RELOC_1                     30289
#define  IE_SYMBOLS_CREATE_STORE_RELOC_1              30290
#define  IE_SYMBOLS_CREATE_STORE_RELOC_2              30291
#define  IE_SYMBOLS_ADD_CFI_RECORD_1                  30292
/* [30293] to [30299] unused */

/* From [30300] to [30399] */

#define  IE_SYMBOLSC_SFA_1                            30300
#define  IE_SYMBOLSC_RFA_1                            30301
#define  IE_SYMBOLSC_RFA_2                            30302
#define  IE_TIMESTMP00                                30303
/* [30304] to [30349] unused */
#define  IE_HASH_ASE_1                                30350
#define  IE_HASH_ASYE_1                               30351
#define  IE_LOADBUF_LSBCI_1                           30352

/* [30353] to [30383] */
#define  IE_SYMBOLS_SYMEXISTS_1                       30353
#define  IE_SYMBOLS_DELETESYMBOL_1                    30354
#define  IE_SYMBOLS_CREATESYMBOL_1                    30355
#define  INSUFFIC_LST_NAME_MEM_E                      30356
#define  IE_SYMBOLS_CREATEROUTINE_1                   30357
#define  IE_SYMBOLS_STORESYMBOL_1                     30358
#define  IE_SYMBOLS_CREATESTATEMENT_1                 30359
#define  IE_SYMBOLS_STORESTATEMENT_1                  30360
#define  INSUFFIC_BUFFER_MEM_E                        30361
#define  INSUFFIC_LINE_MEM_E                          30362
#define  IE_SYMBOLS_FFBTE_1                           30363
#define  ERR_OPEN_OMF_FILE_E                          30364
#define  IE_SYMBOLS_FPBTE_1                           30365
#define  IE_SYMBOLS_FNBTE_1                           30366
#define  IE_SYMBOLS_FLBTE_1                           30367
#define  IE_SYMBOLS_RE_1                              30368
#define  IE_SYMBOLS_GS_1                              30369
#define  IE_SYMBOLS_BT_1                              30370
#define  ERR_OPEN_ASDOF_FILE_E                        30371
#define  IE_SYMBOLS_STOREROUTINE_1                    30372
#define  IE_SYMBOLS_GETROUTINE_1                      30373
#define  IE_SYMBOLS_FPR_1                             30374
#define  IE_SYMBOLS_FFCR_1                            30375
#define  IE_SYMBOLS_FNSR_1                            30376
#define  IE_SYMBOLS_FPSR_1                            30377
#define  IE_SYMBOLS_FLCR_1                            30378
#define  IE_SYMBOLS_FFS_1                             30379
#define  INSUFFIC_SYMF_MEMORY_RESERVE                 30380
#define  IE_SYMBOLS_FNS_1                             30381
#define  IE_SYMBOLS_FPS_1                             30382
#define  IE_SYMBOLS_FFSTMT_1                          30383

/* From [30384] to [30399] unused */

/* From [30400] to [30499] */

#define  IE_RANGE_NRE_1_L                             30400
#define  IE_RANGE_RRNFT_1                             30401
#define  IE_RANGE_RRTFN_1                             30402
#define  IE_RANGE_IRRS_1                              30403
#define  IE_RANGE_LIRE_1                              30404
#define  IE_RANGE_CR_1                                30405
/* [30406] to [30449] unused */
#define  IE_SUBRANGE_GTORSRS_1                        30450
#define  IE_SUBRANGE_SUTORSRS_1                       30451
#define  IE_SUBRANGE_PFRSRS_1                         30452
#define  IE_SUBRANGE_PORSRS_1                         30453
#define  IE_SUBRANGE_IR_1                             30454
#define  IE_SUBRANGE_IR_2                             30455
#define  IE_SUBRANGE_DR_1                             30456
#define  IE_SUBRANGE_DR_2                             30457
#define  IE_SUBRANGE_DR_3                             30458
#define  IE_SUBRANGE_DR_4                             30459
/* [30460] to [30499] unused */

/* From [30500] to [30599] */

#define  IE_GETRANGE_GR_1                             30500
/* [30501] to [30549] unused */
#define  IE_ADDRSORT_BAD_MEMBUFFERPTR                 30550
/* [30551] to [30649] unused */

/* From [30600] to [30699] */

#define  IE_ADDRESS_AAE_1                             30650
#define  IE_ADDRESS_DAE_1                             30651
#define  IE_ADDRESS_DAE_2                             30652
#define  IE_ADDRESS_BAE_1                             30653
#define  IE_COCOINFO_ADD_NEW_NODE                     30654
#define  IE_COCOINFO_REMOVE_NODE                      30655
#define  IE_REPGEN_WRITE_MODULE_DET                   30656
#define  IE_COCOBOX_COCO_MODULE_BOX                   30657
#define  IE_COCOBOX_BANK_ROOTS_ALLOC                  30658
#define  IE_COCOBOX_BANK_ROOTS_DEALLOC                30659
#define  IE_GLOBCOCO_ALLOC                            30660
#define  IE_GLOBCOCO_DEALLOC                          30661
#define  IE_BANK00                                    30662
#define  IE_STORECOMPLEXTYPE                          30663
#define  IE_KSMFACE_00                                30664
/* [30665] to [30999] unused */

/* From [30700] to [30799] */
#define ERROR_READING_REC                             30700


/* From [30800] to [30899] */


/* From [30900] to [30999] */


/* From [31000] to [31099] */
#endif
