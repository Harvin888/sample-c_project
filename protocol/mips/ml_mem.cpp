/****************************************************************************
       Module: ml_mem.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This section contains all functions related to MIPS memory 
               access using both normal and DMA.
Date           Initials    Description
08-Feb-2008    VH          Initial
23-Dec-2008    SPC         Returning errors more properly.
06-Aug-2009    SPC         Made cache flush routine run from probe and 
                           Added OS MMU management
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "firmware/export/api_def.h"
#include "commdefs.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "mips.h"
#include "midlayer.h"
#include "ml_error.h"
#include "gerror.h"
#include "debug.h"
#include "../../utility/debug/debug.h"
#include "protocol/common/osdbg.h"
#ifndef __LINUX
// Windows specific includes
#else
// Linux includes
#endif

// local macros

// local functions
static int MLM_VirtualToPhysical(TyMLHandle *ptyHandle, unsigned long ulVirtualAddress, unsigned long ulLength, unsigned long *pulPhysicalAddress, unsigned long *pulValidLength);
static unsigned char MLM_CheckValidDmaAddressRange(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned long ulLength, unsigned char bVirtualAddress);

/****************************************************************************
     Function: MLM_ReadMemory
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes of memory
               unsigned long ulObjectSize - object size
       Output: int - error code
  Description: Read block of memory given by virtual address. 
               This function takes care of SW breakpoints and caches.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_ReadMemory(TyMLHandle *ptyHandle, 
				   unsigned long ulStartAddress, 
				   unsigned char *pucData, 
				   unsigned long ulLength, 
				   unsigned long ulObjectSize)
{
   unsigned char bSavedDmaEnabledByUser, bSavedMemOnTheFlyEnabled, bSPROverlap;
   unsigned long ulLocSavedValue = 0;
   int iResult = ERR_NO_ERROR;
   // check parameters, other parameters will be checked inside called functions
   assert(ptyHandle != NULL);
   bSavedDmaEnabledByUser = ptyHandle->bDmaEnabledByUser;
   bSavedMemOnTheFlyEnabled = ptyHandle->bMemOnTheFlyEnabled;
   // when processor is executing, we may try to use DMA core (if supported)
   if (ptyHandle->bProcessorExecuting && ptyHandle->bDmaSupported)
      {
      ptyHandle->bDmaEnabledByUser = 1;
      ptyHandle->bMemOnTheFlyEnabled = 1;
      }
   // check instruction scratch pad RAM
   if (MLC_AccessOverLapsInstScratchPad(ptyHandle, ulStartAddress, ulLength))
      {
      iResult = MLC_ReadInstScratchPad(ptyHandle, ulStartAddress, pucData, ulLength, ulObjectSize);
      ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
      ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
      return iResult;
      }
   bSPROverlap = MLC_AccessOverLapsDataScratchPad(ptyHandle, ulStartAddress, ulLength);
   if(MLC_EnableKSeg0CachingForScratching(ptyHandle, bSPROverlap, 1, &ulLocSavedValue))
      {  // trying to read scratch pad ram memory but caching is not enabled, so let fill this memory as 0xDEAD
      unsigned long ulByteLength;
      if (ulObjectSize <= 1)
         ulByteLength = ulLength;
      else
         ulByteLength = ulLength * ulObjectSize;
      if (pucData != NULL)
         {
         for (unsigned long ulIndex=0; ulIndex < ulByteLength; ulIndex++)
            {
            if (ulIndex % 2)
               *pucData++ = 0xDE;
            else
               *pucData++ = 0xAD;
            }
         }
      ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
      ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
      return iResult;
      }
   // call function to read memory
   iResult = MLM_ReadMemoryVirtual(ptyHandle, ulStartAddress, pucData, ulLength, ulObjectSize);
   // memory may contain software breakpoints which have been previously set
   (void)MLC_EnableKSeg0CachingForScratching(ptyHandle, bSPROverlap, 0, &ulLocSavedValue);
   if (iResult == ERR_NO_ERROR)
      iResult = MLB_RemoveBPSFromReadBuff(ptyHandle, ulStartAddress, pucData, ulLength);
   //TODO: check and add MLM_AnalyseErrorAndDealWithIt
   // restored settings regarding DMA core
   ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
   ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
   return iResult;
}

/****************************************************************************
     Function: MLM_WriteMemory
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes of memory
               unsigned long ulObjectSize - object size
       Output: int - error code
  Description: Write block of memory given by virtual address. 
               This function takes care of SW breakpoints and caches.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_WriteMemory(TyMLHandle *ptyHandle, 
					unsigned long ulStartAddress, 
					unsigned char *pucData, 
					unsigned long ulLength, 
					unsigned long ulObjectSize)
{
   unsigned char bSavedDmaEnabledByUser, bSavedMemOnTheFlyEnabled, bSPROverlap;
   unsigned long ulLocSavedValue = 0;
   int iResult = ERR_NO_ERROR;
   // check parameters, other parameters will be checked inside called functions
   assert(ptyHandle != NULL);
   bSavedDmaEnabledByUser = ptyHandle->bDmaEnabledByUser;
   bSavedMemOnTheFlyEnabled = ptyHandle->bMemOnTheFlyEnabled;
   // check if write access collides with cache debug routine
   //if ((ptyHandle->bCacheFlushEnabled) && MLM_ThisOverWritesCacheRoutine(ptyHandle, ulStartAddress, ulLength))
   //   return ERR_MEMWRITE_BREAKS_CACHEROUTINE;
   if (ptyHandle->bProcessorExecuting && ptyHandle->bDmaSupported)
      {
      ptyHandle->bDmaEnabledByUser = 1;
      ptyHandle->bMemOnTheFlyEnabled = 1;
      }
   if (ptyHandle->bProcessorExecuting && (!ptyHandle->bDmaEnabledByUser || !ptyHandle->bMemOnTheFlyEnabled))
      {
      ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
      ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
      return ERR_DIMEMWRITE_NOT_DURING_EXE;
      }
   // do we need to flush cache
   // SPC: Commented out because now we are using hit invalidate while reading and writing memory
   //      It is implemented in Write and Read virtural memory functions
   /*
   if (ptyHandle->bFlushOnlyDataCache)
      {
      if (ulLength)
         {
         if (!ptyHandle->bDataCacheAlreadyInvalidated)
            {
            iResult = MLC_FlushCache(ptyHandle, CACHE_TYPE_DATA, CACHE_FLUSH_WRITEBACK_AND_INVALIDATE);
            if (iResult != ERR_NO_ERROR)
               {
               ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
               ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
               return iResult;
               }
            ptyHandle->bDataCacheAlreadyInvalidated = 1;
            }
         }
      else
         {  // try to invalidate cache on start address aligned to word !!!
         iResult = MLC_Hit_Invalidate_D(ptyHandle, ulStartAddress & 0xFFFFFFFC);
         if (iResult != ERR_NO_ERROR)
            {
            ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
            ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
            return iResult;
            }
         }
      }
   */
   // check scratch par ram
   bSPROverlap = MLC_AccessOverLapsDataScratchPad(ptyHandle, ulStartAddress, ulLength);
   (void)MLC_EnableKSeg0CachingForScratching(ptyHandle, bSPROverlap, 1, &ulLocSavedValue);
   // check for SW breakpoints in current region
   iResult = MLB_AddBPSToWriteBuff(ptyHandle, ulStartAddress, pucData, ulLength);
   // call write function
   if (iResult == ERR_NO_ERROR)
      iResult = MLM_WriteMemoryVirtual(ptyHandle, ulStartAddress, pucData, ulLength, ulObjectSize);
   (void)MLC_EnableKSeg0CachingForScratching(ptyHandle, bSPROverlap, 0, &ulLocSavedValue);
   // restore settings and finish
   ptyHandle->bDmaEnabledByUser = bSavedDmaEnabledByUser;
   ptyHandle->bMemOnTheFlyEnabled = bSavedMemOnTheFlyEnabled;
   return iResult;
}

/****************************************************************************
     Function: MLM_ReadMemoryVirtual
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes of memory
               unsigned long ulObjectSize - object size
       Output: int - error code
  Description: Read block of memory given by virtual address. SW breakpoints are ignored in this case
               and no cache handling as well. Address and size needs to be aligned depending on object size.
Date           Initials    Description
08-Feb-2008    VH          Initial
27-Nov-2008    SPC         Changed DW2 word read call to Diskware calls 
                           to improve performance. 
****************************************************************************/
int MLM_ReadMemoryVirtual(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   if (ulLength > ML_MAX_MEMORY_BLOCK_SIZE)
      return ERR_DIMEMREAD_TOO_LARGE;
   // check to use DMA core or MIPS core
   if (ptyHandle->bDmaSupported && 
       (ptyHandle->bDmaEnabledByUser || ptyHandle->bMemOnTheFlyEnabled))
      {  // we may use DMA core
      unsigned long ulPhysicalAddress = ulStartAddress;
      unsigned long ulValidLength = ulLength;
      // convert virtual address to physical
      iResult = MLM_VirtualToPhysical(ptyHandle, ulStartAddress, ulLength, &ulPhysicalAddress, &ulValidLength);
      if (iResult != ERR_NO_ERROR)
         {
         memset((void *)pucData, 0, ulLength);
         return iResult;
         }
      // check range
      if ((ulValidLength < ulLength) || !MLM_CheckValidDmaAddressRange(ptyHandle, ulPhysicalAddress, ulValidLength, 0))
         return ERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE;
      // call function for access using DMA core
      return MLM_ReadMemoryDma(ptyHandle, ulPhysicalAddress, pucData, ulLength);
      }
   else
      {  // we should use MIPS core
      if (ptyHandle->bProcessorExecuting)
         return ERR_DIMEMREAD_NOT_DURING_EXE;

      //Check for OS MMU mappings.
      if (IsOSMMUMappingNeeded(ulStartAddress))
         {
         (void)SetOSMMUMapping(ptyHandle, ulStartAddress, 0);
         }

      // check alignment for words and halfwords
      switch (ulObjectSize)
         {
         case 1:
         case 0:
            // no alignment checking
            break;
         case 4:
            if ((ulStartAddress % 4) || (ulLength % 4))
               iResult = ERR_WRITEWPSAFE_UNALIGNED_START;
            break;
         case 2:
            if ((ulStartAddress % 2) || (ulLength % 2))
               iResult = ERR_WRITEWPSAFE_UNALIGNED_START;
            break;
         default:
            iResult = ERR_WRITEWPSAFE_UNALIGNED_START;
            break;
         }
      if (iResult != ERR_NO_ERROR)
         {
         memset((void *)pucData, 0, ulLength);
         return iResult;
         }
      // ok, we can call function for access memory
      iResult = MLM_ReadMemoryStd(ptyHandle, ulStartAddress, pucData, ulLength, ulObjectSize);
      UnsetOSMMUMapping(ptyHandle);
      return iResult;
      }
}

/****************************************************************************
     Function: MLM_WriteMemoryVirtual
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes of memory
               unsigned long ulObjectSize - object size
       Output: int - error code
  Description: Write block of memory given by virtual address. SW breakpoints are ignored in this case
               and no cache handling as well. Address and size needs to be aligned depending on object size.
Date           Initials    Description
08-Feb-2008    VH          Initial
27-Nov-2008    SPC         Changed DW2 word read call to Diskware calls 
                           to improve performance. 
****************************************************************************/
int MLM_WriteMemoryVirtual(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   if (ulLength > ML_MAX_MEMORY_BLOCK_SIZE)
      return ERR_DIMEMWRITE_TOO_LARGE;

   // check to use DMA core or MIPS core
   if (ptyHandle->bDmaSupported && (ptyHandle->bDmaEnabledByUser || ptyHandle->bMemOnTheFlyEnabled))
      {  // we may use DMA core
      unsigned long ulPhysicalAddress = ulStartAddress;
      unsigned long ulValidLength = ulLength;
      // convert virtual address to physical
      iResult = MLM_VirtualToPhysical(ptyHandle, ulStartAddress, ulLength, &ulPhysicalAddress, &ulValidLength);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // check range
      if ((ulValidLength < ulLength) || !MLM_CheckValidDmaAddressRange(ptyHandle, ulPhysicalAddress, ulValidLength, 0))
         return ERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE;
      // call function for access using DMA core
      return MLM_WriteMemoryDma(ptyHandle, ulPhysicalAddress, pucData, ulLength);
      }
   else
      {  // we should use MIPS core
      if (ptyHandle->bProcessorExecuting)
         return ERR_DIMEMWRITE_NOT_DURING_EXE;

      //Check for OS MMU mappings.
      if (IsOSMMUMappingNeeded(ulStartAddress))
         {
         (void)SetOSMMUMapping(ptyHandle, ulStartAddress, 1);
         }

      // check alignment for words and halfwords
      switch (ulObjectSize)
         {
         case 1:
         case 0:
            // no alignment checking
            break;
         case 4:
            if ((ulStartAddress % 4) || (ulLength % 4))
               iResult = ERR_WRITEWPSAFE_UNALIGNED_START;
            break;
         case 2:
            if ((ulStartAddress % 2) || (ulLength % 2))
               iResult = ERR_WRITEWPSAFE_UNALIGNED_START;
            break;
         default:
            iResult = ERR_WRITEWPSAFE_UNALIGNED_START;
            break;
         }
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // ok, we can call function for access memory
      iResult = MLM_WriteMemoryStd(ptyHandle, ulStartAddress, pucData, ulLength, ulObjectSize);
      UnsetOSMMUMapping(ptyHandle);
      return iResult;
      }
}

/****************************************************************************
     Function: MLM_ReadMemoryPhysical
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - physical address in memory
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes of memory
       Output: int - error code
  Description: Read block of memory given by physical address. SW breakpoints are ignored in this case
               and no cache handling as well. Address and size does not need to be aligned to any size.
Date           Initials    Description
08-Feb-2008    VH          Initial
30-Dec-2008    SPC         Added support to write/read data > 8KB through DMA
****************************************************************************/
int MLM_ReadMemoryPhysical(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength)
{
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   //if (ulLength > ML_MAX_MEMORY_BLOCK_SIZE)
      //return ERR_DIMEMREAD_TOO_LARGE;
   // check if target supports DMA access
   if (!ptyHandle->bDmaSupported)
      return ERR_DMA_UNSUPPORTED;
   // check if are withing restricted range (if used)
   if (!MLM_CheckValidDmaAddressRange(ptyHandle, ulStartAddress, ulLength, 0))
      return ERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE;
   // check if processor is executing
   if (ptyHandle->bProcessorExecuting && (!ptyHandle->bMemOnTheFlyEnabled))
      return ERR_DIMEMREAD_NOT_DURING_EXE;
   // call function to do physical access using DMA core
   return MLM_ReadMemoryDma(ptyHandle, ulStartAddress, pucData, ulLength);
}

/****************************************************************************
     Function: MLM_WriteMemoryPhysical
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - physical address in memory
               unsigned char *pucData - pointer to data
               unsigned long ulLength - number of bytes of memory
       Output: int - error code
  Description: Write block of memory given by physical address. SW breakpoints are ignored in this case
               and no cache handling as well. Address and size does not need to be aligned to any size.
Date           Initials    Description
08-Feb-2008    VH          Initial
30-Dec-2008    SPC         Added support to write/read data > 8KB through DMA
****************************************************************************/
int MLM_WriteMemoryPhysical(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength)
{
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   //if (ulLength > ML_MAX_MEMORY_BLOCK_SIZE)
      //return ERR_DIMEMWRITE_TOO_LARGE;
   // check if target supports DMA access
   if (!ptyHandle->bDmaSupported)
      return ERR_DMA_UNSUPPORTED;
   // check if are withing restricted range (if used)
   if (!MLM_CheckValidDmaAddressRange(ptyHandle, ulStartAddress, ulLength, 0))
      return ERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE;
   // check if processor is executing
   if (ptyHandle->bProcessorExecuting && (!ptyHandle->bMemOnTheFlyEnabled))
      return ERR_DIMEMWRITE_NOT_DURING_EXE;
   // call function to do physical access using DMA core
   return MLM_WriteMemoryDma(ptyHandle, ulStartAddress, pucData, ulLength);
}

/****************************************************************************
     Function: MLM_ReadMemoryDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data (cannot be NULL)
               unsigned long ulLength - number of bytes in memory (at least 1)
               unsigned long ulObjectSize - size of object to access (0 - does not matter, 1 - byte, 2 - halfword, 4 - word)
       Output: int - error code
  Description: Read block of memory using MIPS DW2 application (instructions in DMSEG).
               Access type is given by object size type, address MUST be aligned regarding selected object type).  
Date           Initials    Description
08-Feb-2008    VH          Initial
25-Nov-2008    SPC         Fixed memory address calculation error
****************************************************************************/
int MLM_ReadMemoryDW2(TyMLHandle *ptyHandle, 
                      unsigned long ulStartAddress, 
                      unsigned char *pucData, 
                      unsigned long ulLength, 
                      unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pucData != NULL);
   assert(ulLength > 0);
   assert(ulLength < ML_MAX_MEMORY_BLOCK_SIZE);
   // address and length alignment should be checked in MLM function calling this one
   // if object size is specified, do exactly what caller ask for (no optimization, etc.)
   if (ulObjectSize == 0)
      {  // caller does not care what type of access to use, preferring fastest way
      // no assumption about length and alignment
      if (ulStartAddress % 4)
         {  // there are some unaligned bytes
         unsigned long ulFirstBytes = 4 - (ulStartAddress % 4);
         if (ulLength < ulFirstBytes)
            ulFirstBytes = ulLength;
         // deal with first bytes
         iResult = MLM_ReadBytesDW2(ptyHandle, ulStartAddress, ulFirstBytes, (void *)pucData);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address 
         ulStartAddress += (4 - (ulStartAddress % 4));         // align address to next word
         ulLength -= ulFirstBytes;
         pucData += ulFirstBytes;
         }
      if (ulLength / 4)
         {  // do optimized word access
         if (ptyHandle->bUseTargetMemoryWhileReading && 
             (ulLength >= ptyHandle->bCacheFlushFastTreshold))
            iResult = MLM_ReadWordsFast(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData);
         else
            iResult = MLM_ReadWordsDW2(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData, 1);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address, length and pointer to data
         ulStartAddress += ((ulLength / 4) * sizeof(unsigned long));
         pucData += ((ulLength / 4) * sizeof(unsigned long));
         ulLength -= ((ulLength / 4) * sizeof(unsigned long));
         }
      if (ulLength % 4)
         {  // any remaining bytes
         iResult = MLM_ReadBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
         ulLength = 0;
         }
      }
   else if (ulObjectSize == 4)
      {  // requesting word access, address and length MUST be aligned
      iResult = MLM_ReadWordsDW2(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData, 1);
      }
   else if (ulObjectSize == 2)
      {  // requesting halfword access, address and length MUST be aligned
      iResult = MLM_ReadHalfwordsDW2(ptyHandle, ulStartAddress, ulLength / 2, (void *)pucData, 1);
      }
   else if (ulObjectSize == 1)
      {  // requesting byte access
      iResult = MLM_ReadBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
      }
   else
      {  // not supported size
      memset((void *)pucData, 0, ulLength);
      return ERR_WRITEWPSAFE_UNALIGNED_START;
      }
   return iResult;
}

/****************************************************************************
     Function: MLM_WriteMemoryDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data (cannot be NULL)
               unsigned long ulLength - number of bytes in memory (at least 1)
               unsigned long ulObjectSize - size of object to access (0 - does not matter, 1 - byte, 2 - halfword, 4 - word)
       Output: int - error code
  Description: Write block of memory using MIPS DW2 application (instructions in DMSEG).
               Access type is given by object size type, address MUST be aligned regarding selected object type).  
Date           Initials    Description
08-Feb-2008    VH          Initial
25-Nov-2008    SPC         Fixed memory address calculation error
****************************************************************************/
int MLM_WriteMemoryDW2(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength, unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pucData != NULL);
   assert(ulLength > 0);
   assert(ulLength < ML_MAX_MEMORY_BLOCK_SIZE);
   // address and length alignment should be checked in MLM function calling this one
   // if object size is specified, do exactly what caller ask for (no optimization, etc.)
   if (ulObjectSize == 0)
      {  // caller does not care what type of access to use, preferring fastest way
      // no assumption about length and alignment
      if (ulStartAddress % 4)
         {  // there are some unaligned bytes
         unsigned long ulFirstBytes = 4 - (ulStartAddress % 4);
         if (ulLength < ulFirstBytes)
            ulFirstBytes = ulLength;
         // deal with first bytes
         iResult = MLM_WriteBytesDW2(ptyHandle, ulStartAddress, ulFirstBytes, (void *)pucData);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address 
         ulStartAddress += (4 - (ulStartAddress % 4));         // align address to next word
         ulLength -= ulFirstBytes;
         pucData += ulFirstBytes;
         }
      if (ulLength / 4)
         {  // do optimized word access
         if (ptyHandle->bUseTargetMemoryWhileWriting && (ulLength >= ptyHandle->bCacheFlushFastTreshold))
            iResult = MLM_WriteWordsFast(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData);
         else
            iResult = MLM_WriteWordsDW2(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData, 1);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address, length and pointer to data
         ulStartAddress += ((ulLength / 4) * sizeof(unsigned long));
         pucData += ((ulLength / 4) * sizeof(unsigned long));
         ulLength -= ((ulLength / 4) * sizeof(unsigned long));
         }
      if (ulLength % 4)
         {  // any remaining bytes
         iResult = MLM_WriteBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
         ulLength = 0;
         }
      }
   else if (ulObjectSize == 4)
      {  // requesting word access, address and length MUST be aligned
      iResult = MLM_WriteWordsDW2(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData, 1);
      }
   else if (ulObjectSize == 2)
      {  // requesting halfword access, address and length MUST be aligned
      iResult = MLM_WriteHalfwordsDW2(ptyHandle, ulStartAddress, ulLength / 2, (void *)pucData, 1);
      }
   else if (ulObjectSize == 1)
      {  // requesting byte access
      iResult = MLM_WriteBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
      }
   else
      {  // not supported size
      return ERR_WRITEWPSAFE_UNALIGNED_START;
      }
   return iResult;
}

/****************************************************************************
     Function: MLM_ReadMemoryEjtag
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartOffset - offset within DRSEG, must be aligned to words
               unsigned char *pucData - pointer to data
               unsigned long ulSize - number of bytes of memory to read, must be multiplied by 4
       Output: int - error code
  Description: Read block of EJTAG memory.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_ReadMemoryEjtag(TyMLHandle *ptyHandle, unsigned long ulStartOffset, unsigned char *pucData, unsigned long ulLength)
{
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   if (ulLength % 4)                                              
      return ERR_WRITEWPSAFE_UNALIGNED_START;
   if (ulLength > ML_MAX_MEMORY_BLOCK_SIZE)
      return ERR_DIMEMREAD_TOO_LARGE;
   // address alignment and total size will be checked in called function
   // call function to read DRSEG with full address, length in words and do not normalize endian (as DRSEG is considered as LE)
   return MLM_ReadWordsDW2(ptyHandle, MIPS_DRSEG + ulStartOffset, ulLength / 4, (unsigned long *)pucData, 0);
}

/****************************************************************************
     Function: MLM_WriteMemoryEjtag
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartOffset - offset within DRSEG, must be aligned to words
               unsigned char *pucData - pointer to data
               unsigned long ulSize - number of bytes of memory to write, must be multiplied by 4
       Output: int - error code
  Description: Write block of EJTAG memory.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_WriteMemoryEjtag(TyMLHandle *ptyHandle, unsigned long ulStartOffset, unsigned char *pucData, unsigned long ulLength)
{
   // check parameters
   assert(ptyHandle != NULL);
   if ((pucData == NULL) || (ulLength == 0))
      return ERR_NO_ERROR;
   if (ulLength % 4)                                              
      return ERR_WRITEWPSAFE_UNALIGNED_START;
   if (ulLength > ML_MAX_MEMORY_BLOCK_SIZE)
      return ERR_DIMEMWRITE_TOO_LARGE;
   // address alignment and total size will be checked in called function
   // call function to read DRSEG with full address, length in words and do not normalize endian (as DRSEG is considered as LE)
   return MLM_WriteWordsDW2(ptyHandle, MIPS_DRSEG + ulStartOffset, ulLength / 4, (unsigned long *)pucData, 0);
}

/****************************************************************************
     Function: MLM_ReadMemoryDma
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - physical address in memory
               unsigned char *pucData - pointer to data (cannot be NULL)
               unsigned long ulLength - number of bytes in memory (should be at least 1)
       Output: int - error code
  Description: Read block of memory using DMA core. This function deals with endianess, data format size
               and alignment. Note total length (number of bytes) is not checked here as it is expected
               to be checked in layer above.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_ReadMemoryDma(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pucData != NULL);
   assert(ulLength > 0);
   // we have only word access via MIPS DMA core however address here may not be aligned and also length and array is in bytes
   // first check if we need to deal with unaligned starting address, we also assume
   if (ulStartAddress % 4)
      {  // start address not aligned to word
      unsigned long ulFirstBytes = 4 - (ulStartAddress % 4);
      unsigned long ulLocWord = 0;
      unsigned char *pucLocWordBytes = (unsigned char *)(&ulLocWord);
      // read first unaligned word from aligned address
      iResult = MLM_ReadWordDma(ptyHandle, ulStartAddress - (ulStartAddress % 4), (void *)(&ulLocWord));
      if (iResult != ERR_NO_ERROR)
         return iResult;
      if (ulLength < ulFirstBytes)
         ulFirstBytes = ulLength;                                       // how many bytes to handle from first word
      // depending on endianess, deal with bytes from this word
      if (ptyHandle->bBigEndian)
         {  // target is BE
         unsigned long ulByteIndex = 3 - (ulStartAddress % 4);
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulFirstBytes; ulIndex++)
            *pucData++ = pucLocWordBytes[ulByteIndex - ulIndex];
         }
      else
         {  // target is LE
         unsigned long ulByteIndex = ulStartAddress % 4;
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulFirstBytes; ulIndex++)
            *pucData++ = pucLocWordBytes[ulByteIndex + ulIndex];
         }
      // update length and address (pointer to buffer has been already updated)
      ulLength -= ulFirstBytes;                                         // decreasing length by first bytes
      ulStartAddress += (4 - (ulStartAddress % 4));                     // now address should be aligned on next word
      }
   // now we can read whole block of words
   if (ulLength / 4)
      {  // we can read whole words
      iResult = MLM_ReadMultipleWordsDma(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // update pointer, address and length with aligned number of words
      pucData += ((ulLength / 4) * 4);
      ulStartAddress += ((ulLength / 4) * 4);
      ulLength = (ulLength % 4);                                        // just remaining bytes
      }
   // finally, there could be remaining part of word to read
   if (ulLength)
      {
      unsigned long ulLocWord = 0;
      unsigned char *pucLocWordBytes = (unsigned char *)(&ulLocWord);
      // read first unaligned word from aligned address
      iResult = MLM_ReadWordDma(ptyHandle, ulStartAddress, (void *)(&ulLocWord));
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // depending on endianess, deal with bytes from this word
      if (ptyHandle->bBigEndian)
         {  // target is BE
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulLength; ulIndex++)
            *pucData++ = pucLocWordBytes[3 - ulIndex];
         }
      else
         {  // target is LE
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulLength; ulIndex++)
            *pucData++ = pucLocWordBytes[ulIndex];
         }
      ulLength = 0;                                                     // finished with whole block access
      }
   return iResult;
}

/****************************************************************************
     Function: MLM_WriteMemoryDma
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - physical address in memory
               unsigned char *pucData - pointer to data (cannot be NULL)
               unsigned long ulLength - number of bytes in memory (at least 1 bytes)
       Output: int - error code
  Description: Write block of memory using DMA core. This function deals with endianess, data format size
               and alignment. Note total length (number of bytes) is not checked here as it is expected
               to be checked in layer above.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_WriteMemoryDma(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned char *pucData, unsigned long ulLength)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pucData != NULL);
   assert(ulLength > 0);
   // we have only word access via MIPS DMA core however address here may not be aligned and also length and array is in bytes
   // first check if we need to deal with unaligned starting address, we also assume
   if (ulStartAddress % 4)
      {  // start address not aligned to word
      unsigned long ulFirstBytes = 4 - (ulStartAddress % 4);
      unsigned long ulLocWord = 0;
      unsigned char *pucLocWordBytes = (unsigned char *)(&ulLocWord);
      if (ulLength < ulFirstBytes)
         ulFirstBytes = ulLength;                                       // how many bytes to handle from first word
      // read first unaligned word from aligned address
      iResult = MLM_ReadWordDma(ptyHandle, ulStartAddress - (ulStartAddress % 4), (void *)(&ulLocWord));
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // modify certain bytes in depending on endianess
      if (ptyHandle->bBigEndian)
         {  // target is BE
         unsigned long ulByteIndex = 3 - (ulStartAddress % 4);
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulFirstBytes; ulIndex++)
            pucLocWordBytes[ulByteIndex - ulIndex] = *pucData++;
         }
      else
         {  // target is LE
         unsigned long ulByteIndex = ulStartAddress % 4;
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulFirstBytes; ulIndex++)
            pucLocWordBytes[ulByteIndex + ulIndex] = *pucData++;
         }
      // write first word
      iResult = MLM_WriteWordDma(ptyHandle, ulStartAddress - (ulStartAddress % 4), (void *)(&ulLocWord));
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // update length and address (pointer to buffer has been already updated)
      ulLength -= ulFirstBytes;                                         // decreasing length by first bytes
      ulStartAddress += (4 - (ulStartAddress % 4));                     // now address should be aligned on next word
      }
   // now we can write whole block of words
   if (ulLength / 4)
      {  // we can read whole words
      iResult = MLM_WriteMultipleWordsDma(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // update pointer, address and length with aligned number of words
      pucData += ((ulLength / 4) * 4);
      ulStartAddress += ((ulLength / 4) * 4);
      ulLength = ulLength % 4;                                          // length is just remaining bytes
      }
   // finally, there could be remaining part of word to read
   if (ulLength)
      {
      unsigned long ulLocWord = 0;
      unsigned char *pucLocWordBytes = (unsigned char *)(&ulLocWord);
      // read first unaligned word from aligned address
      iResult = MLM_ReadWordDma(ptyHandle, ulStartAddress, (void *)(&ulLocWord));
      if (iResult != ERR_NO_ERROR)
         return iResult;
      // depending on endianess, deal with bytes from this word
      if (ptyHandle->bBigEndian)
         {  // target is BE
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulLength; ulIndex++)
            pucLocWordBytes[3 - ulIndex] = *pucData++;
         }
      else
         {  // target is LE
         // copy given number of bytes to buffer (and updating pointer)
         for (unsigned long ulIndex=0; ulIndex < ulLength; ulIndex++)
            pucLocWordBytes[ulIndex] = *pucData++;
         }
      // write last word
      iResult = MLM_WriteWordDma(ptyHandle, ulStartAddress, (void *)(&ulLocWord));
      if (iResult != ERR_NO_ERROR)
         return iResult;
      ulLength = 0;                                                     // finished with whole block access
      }
   return iResult;
}

/****************************************************************************
     Function: MLM_ReadWordDma
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Read a word using MIPS DMA core.
Date           Initials    Description
08-Feb-2008    VH          Initial
02-Dec-2008    SPC         Cleanup
****************************************************************************/
int MLM_ReadWordDma(TyMLHandle *ptyHandle, unsigned long ulAddress, void *pvData)
{
    TyError tyResult;
    // check parameters
    assert(ptyHandle != NULL);
    assert((ulAddress % 4) == 0);
    assert(pvData != NULL);
    tyResult = DL_OPXD_Mips_ReadWordDma(ptyHandle->iDLHandle, 
                                        ptyHandle->ulDmaCore, 
                                        ulAddress, 
                                        (unsigned long*)pvData, 
                                        ptyHandle->bBigEndian); 
    if (tyResult != DRVOPXD_ERROR_NO_ERROR)
        {
        return ML_ConvertDLError(tyResult);
        }
    return ERR_NO_ERROR;
}
/****************************************************************************
     Function: MLM_ReadMultipleWordsDma
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Read block of words using MIPS DMA core.
Date           Initials    Description
02-Dec-2008    SPC         Initial
30-Dec-2008    SPC         Added support to write/read data > 8KB through DMA
****************************************************************************/
int MLM_ReadMultipleWordsDma(TyMLHandle *ptyHandle, unsigned long ulAddress, 
                             unsigned long ulCount, void *pvData)
{
   TyError tyResult;
   unsigned long ulDataReadRem,ulDataReadLen;
   unsigned long *ulDataBuf;
   // check parameters
   assert(ptyHandle != NULL);
   assert((ulAddress % 4) == 0);
   if ((ulCount == 0) || (pvData == NULL))
      return ERR_NO_ERROR;

   ulDataReadRem = ulCount;
   ulDataBuf = (unsigned long *)pvData;
   if (ulCount > ML_MAX_MEMORY_BLOCK_SIZE/4)
      ulDataReadLen = ML_MAX_MEMORY_BLOCK_SIZE/4;
   else
      ulDataReadLen = ulCount;

   while (ulDataReadRem)
      {
      tyResult = DL_OPXD_Mips_ReadMultipleWordsDma(ptyHandle->iDLHandle, 
                                                   ptyHandle->ulDmaCore, 
                                                   ulAddress, 
                                                   ulDataBuf, 
                                                   ulDataReadLen,
                                                   ptyHandle->bBigEndian); 
      if (tyResult != DRVOPXD_ERROR_NO_ERROR)
         return ML_ConvertDLError(tyResult);
      ulDataReadRem -= ulDataReadLen;
      ulAddress += ulDataReadLen*4;
      ulDataBuf += ulDataReadLen;
      if (ulDataReadRem > ML_MAX_MEMORY_BLOCK_SIZE/4)
         ulDataReadLen = ML_MAX_MEMORY_BLOCK_SIZE/4;
      else
         ulDataReadLen = ulDataReadRem;
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLM_WriteWordDma
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               void *pvData - pointer to data
       Output: int - error code
  Description: Write a word using MIPS DMA core.
Date           Initials    Description
08-Feb-2008    VH          Initial
02-Dec-2008    SPC         Cleanup
****************************************************************************/
int MLM_WriteWordDma(TyMLHandle *ptyHandle, 
                      unsigned long ulAddress, 
                      void *pvData)
{
    TyError tyResult;
    // check parameters
    assert(ptyHandle != NULL);
    assert((ulAddress % 4) == 0);
    assert(pvData != NULL);
    tyResult = DL_OPXD_Mips_WriteWordDma(ptyHandle->iDLHandle, 
                                         ptyHandle->ulDmaCore, 
                                         ulAddress, 
                                         (unsigned long*)pvData, 
                                         ptyHandle->bBigEndian); 

    if (tyResult != DRVOPXD_ERROR_NO_ERROR)
        return ML_ConvertDLError(tyResult);
    return ERR_NO_ERROR;  
}

/****************************************************************************
     Function: MLM_WriteMultipleWordsDma
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Write block of words using MIPS DMA core.
Date           Initials    Description
02-Dec-2008    SPC         Initial
30-Dec-2008    SPC         Added support to write/read data > 8KB through DMA
****************************************************************************/
int MLM_WriteMultipleWordsDma(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData)
{
   TyError tyResult;
   unsigned long ulDataWriteRem,ulDataWriteLen;
   unsigned long *ulDataBuf;
   // check parameters
   assert(ptyHandle != NULL);
   assert((ulAddress % 4) == 0);
   if ((ulCount == 0) || (pvData == NULL))
      return ERR_NO_ERROR;

   ulDataWriteRem = ulCount;
   ulDataBuf = (unsigned long *)pvData;
   if (ulCount > ML_MAX_MEMORY_BLOCK_SIZE/4)
      ulDataWriteLen = ML_MAX_MEMORY_BLOCK_SIZE/4;
   else
      ulDataWriteLen = ulCount;

   while (ulDataWriteRem)
      {
      tyResult = DL_OPXD_Mips_WriteMultipleWordsDma(ptyHandle->iDLHandle, 
                                                    ptyHandle->ulDmaCore, 
                                                    ulAddress, 
                                                    ulDataBuf, 
                                                    ulDataWriteLen,
                                                    ptyHandle->bBigEndian); 
      if (tyResult != DRVOPXD_ERROR_NO_ERROR)
         return ML_ConvertDLError(tyResult);
      ulDataWriteRem -= ulDataWriteLen;
      ulAddress += ulDataWriteLen*4;
      ulDataBuf += ulDataWriteLen;
      if (ulDataWriteRem > ML_MAX_MEMORY_BLOCK_SIZE/4)
         ulDataWriteLen = ML_MAX_MEMORY_BLOCK_SIZE/4;
      else
         ulDataWriteLen = ulDataWriteRem;
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLM_ReadWordsFast
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Read block of words using FAST transfer through MIPS core.
               This function requires cache flush routine loaded in memory.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_ReadWordsFast(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData)
{
   // XXX - not implemented yet
   ptyHandle = ptyHandle;  ulAddress = ulAddress;  ulCount = ulCount;   pvData = pvData;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLM_WriteWordsFast
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Write block of words using FAST transfer through MIPS core.
               This function requires cache flush routine loaded in memory.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLM_WriteWordsFast(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData)
{
   // XXX - not implemented yet
   ptyHandle = ptyHandle;  ulAddress = ulAddress;  ulCount = ulCount;   pvData = pvData;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLM_ReadWordsDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
               unsigned char bNormaliseEndian - 1 if care about endianess, 0 otherwise
       Output: int - error code
  Description: Read block of words using DW2 application.
               If bNormaliseEndian is 1, result values will be store in same order as MIPS core (BE or LE).
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
int MLM_ReadWordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian)
{
   TyError tyResult;
   unsigned long pulDataToProc[2];
   TyDW2AppParams tyLocDW2Params;
   // check parameters and address alignment
   assert(ptyHandle != NULL);
   assert(pvData != NULL);
   assert(ulCount <= (ML_MAX_MEMORY_BLOCK_SIZE / 4));
   if (ulAddress & 0x3)
      return ERR_WRITEWPSAFE_UNALIGNED_START;
   if (ulCount == 0)                                                                                     // make sure DW2 is not called with 0 count
      return ERR_NO_ERROR;
   // we need to fill parameters for DW2 application
   tyLocDW2Params.pvAppInst = (void *)ptyHandle->tyMipsDW2App.pulReadWordsApp;
   tyLocDW2Params.uiInstSize = (unsigned int)(ptyHandle->tyMipsDW2App.ulReadWordsInstructions * sizeof(unsigned long)); // size in bytes
   // Values in can be either BE or LE and are transferred in this format to diskware. As diskware uses EJTAG DATA which is invariant of endianess order,
   // we would need to convert whole block from BE to LE. As diskware can do it much efficiently, we going to use only flag to tell diskware what to do.
   if (bNormaliseEndian && ptyHandle->bBigEndian)
      {  // target is BE and we want to normalize it, all data to/from processor will be swapped before feeding into EJTAG DATA
      tyLocDW2Params.bSwapDataValues = 1;
      pulDataToProc[0] = ML_SwapEndianWord(ulAddress);               // 1st parameter is address (all data to/from proc will be swapped)
      pulDataToProc[1] = ML_SwapEndianWord(ulCount);                 // 2nd parameter is number of words (all data to/from proc will be swapped)
      }
   else
      {  // no swapping of bytes, treat data to/from processor as it is
      tyLocDW2Params.bSwapDataValues = 0;
      pulDataToProc[0] = ulAddress;                                  // 1st parameter is address
      pulDataToProc[1] = ulCount;                                    // 2nd parameter is number of words
      }
   tyLocDW2Params.ppvAppDataToProc[0] = (void *)pulDataToProc;       // assign pointer 
   tyLocDW2Params.puiDataToProcSize[0] = 2 * sizeof(unsigned long);  // using 2 words (8 bytes)
   tyLocDW2Params.ppvAppDataToProc[1] = NULL;                        // no more data to proc
   tyLocDW2Params.puiDataToProcSize[1] = 0;
   tyLocDW2Params.ppvAppDataFromProc[0] = pvData;                    // data from processor are directly read words by DW2 in required format
   tyLocDW2Params.puiDataFromProcSize[0] = (unsigned int)(ulCount * sizeof(unsigned long));  // size in bytes
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;                      // no more output data
   tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;    // endianess flag is used when DSEG is accessed by bytes/halfwords
   // call DW2 in diskware and check result
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      {  // error occurred, so rather return all values as 0 to be consistent
      memset(pvData, 0, ulCount * sizeof(unsigned long));
      return ML_ConvertDLError(tyResult);
      }
}
#endif
extern unsigned long pulCommonDataFromProc[MAX_DW2_DATA];
extern unsigned long pulCommonDataToProc[MAX_DW2_DATA];
int MLM_ReadWordsDW2(TyMLHandle *ptyHandle, 
                     unsigned long ulAddress, 
                     unsigned long ulCount, 
                     void *pvData, 
                     unsigned char bNormaliseEndian)
{

   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulCnt;
   unsigned long ulLocAddress;
   unsigned long ulLocData;
   unsigned long *pulLocpData;

   DLOG2((LOGFILE,"\n\t\tMLM_ReadWordsDW2 -> Called"));

   
   if ((ulAddress & 0xFFFFFFFC) != ulAddress)
      {
      ASSERT_NOW();    // Read Must be Word aligned
      DLOG2((LOGFILE,"\n\tMLM_ReadWordsDW2  : Alignment ERROR !!"));
      return 99; // TO_DO: Fix This error Code
      }
   pulLocpData = (unsigned long*)pvData;
   ulLocAddress = ulAddress;
   for(ulCnt=0; ulCnt < ulCount; ulCnt++)
      {
      pulCommonDataToProc[0]=ulLocAddress;
      DLOG2((LOGFILE,"\nMLM_ReadWordsDW2  Address=%08X ",ulAddress));
      pulCommonDataFromProc[0] = 0xDEADDEAD; // Initialize to dead value
   
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulReadWordsApp,
                           ptyHandle->tyMipsDW2App.ulReadWordsInstructions*4,
                           pulCommonDataToProc,
                           4,
                           pulCommonDataFromProc,
                           4);
   
      ulLocData = pulCommonDataFromProc[0];
   
      if (bNormaliseEndian && ptyHandle->bBigEndian)
         *(unsigned long*)(pulLocpData + (ulCnt)) = ML_SwapEndianWord(ulLocData);
      else
         *(unsigned long*)(pulLocpData + (ulCnt)) = ulLocData;

      DLOG2((LOGFILE,"\tData=%08X",*(unsigned long*)(pulLocpData + (ulCnt*4))));
      ulLocAddress+=4;
      }
   return ErrRet;
}
/****************************************************************************
     Function: MLM_ReadHalfwordsDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to halfword)
               unsigned long ulCount - number of halfwords
               void *pvData - pointer to data
               unsigned char bNormaliseEndian - 1 if care about endianess, 0 otherwise
       Output: int - error code
  Description: Read block of halfwords using DW2 application. See comments on MLM_ReadWordsDW2 for bNormaliseEndian.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000  // OPTIMIZED_DW
int MLM_ReadHalfwordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian)
{
   TyError tyResult;
   unsigned long pulDataToProc[2];
   TyDW2AppParams tyLocDW2Params;
   // check parameters and address alignment
   assert(ptyHandle != NULL);
   assert(pvData != NULL);
   assert(ulCount <= (ML_MAX_MEMORY_BLOCK_SIZE / 2));
   if (ulAddress & 0x1)
      return ERR_WRITEWPSAFE_UNALIGNED_START;
   if (ulCount == 0)                                                                                     // make sure DW2 is not called with 0 count
      return ERR_NO_ERROR;
   // we need to fill parameters for DW2 application
   tyLocDW2Params.pvAppInst = (void *)ptyHandle->tyMipsDW2App.pulReadHalfwordsApp;
   tyLocDW2Params.uiInstSize = (unsigned int)(ptyHandle->tyMipsDW2App.ulReadHalfwordsInstructions * sizeof(unsigned long)); // size in bytes
   // see comments for ReadWordsDW2 to explanation about endianess
   if (bNormaliseEndian && ptyHandle->bBigEndian)
      {  // target is BE and we want to normalize it, all data to/from processor will be swapped before feeding into EJTAG DATA
      tyLocDW2Params.bSwapDataValues = 1;
      pulDataToProc[0] = ML_SwapEndianWord(ulAddress);               // 1st parameter is address (all data to/from proc will be swapped)
      pulDataToProc[1] = ML_SwapEndianWord(ulCount);                 // 2nd parameter is number of halfwords (all data to/from proc will be swapped)
      }
   else
      {  // no swapping of bytes, treat data to/from processor as it is
      tyLocDW2Params.bSwapDataValues = 0;
      pulDataToProc[0] = ulAddress;                                  // 1st parameter is address
      pulDataToProc[1] = ulCount;                                    // 2nd parameter is number of halfwords
      }
   tyLocDW2Params.ppvAppDataToProc[0] = (void *)pulDataToProc;       // assign pointer 
   tyLocDW2Params.puiDataToProcSize[0] = 2 * sizeof(unsigned long);  // using 2 words (8 bytes)
   tyLocDW2Params.ppvAppDataToProc[1] = NULL;                        // no more data to proc
   tyLocDW2Params.puiDataToProcSize[1] = 0;
   tyLocDW2Params.ppvAppDataFromProc[0] = pvData;                    // data from processor are directly read halfwords by DW2 in required format
   tyLocDW2Params.puiDataFromProcSize[0] = (unsigned int)(ulCount * sizeof(unsigned short)); // size in bytes
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;                      // no more output data
   tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;    // endianess flag is used when DSEG is accessed by bytes/halfwords
   // call DW2 in diskware and check result
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      {  // error occurred, so rather return all values as 0 to be consistent
      memset(pvData, 0, ulCount * sizeof(unsigned short));
      return ML_ConvertDLError(tyResult);
      }
}
#endif
int MLM_ReadHalfwordsDW2(TyMLHandle *ptyHandle, 
                         unsigned long ulAddress, 
                         unsigned long ulCount, 
                         void *pvData, 
                         unsigned char bNormaliseEndian)
{
   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulCnt;
   unsigned long ulLocAddress;
   unsigned short usLocData;
   unsigned short *pusLocpData;

   DLOG2((LOGFILE,"\n\t\tMLM_ReadHalfwordsDW2 -> Called"));

   
   if ((ulAddress & 0xFFFFFFFE) != ulAddress)
      {
      ASSERT_NOW();    // Read Must be Word aligned
      DLOG2((LOGFILE,"\n\tMLM_ReadHalfwordsDW2  : Alignment ERROR !!"));
      return 98; // TO_DO: Fix This error Code
      }
   pusLocpData = (unsigned short*)pvData;
   ulLocAddress = ulAddress;
   for(ulCnt=0; ulCnt < ulCount; ulCnt++)
      {
      pulCommonDataToProc[0]=ulLocAddress;
      DLOG2((LOGFILE,"\tMLM_ReadHalfwordsDW2  Address=%08X ",ulAddress));
      pulCommonDataFromProc[0] = 0xDEADDEAD; // Initialize to dead value
   
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulReadHalfwordsApp,
                           ptyHandle->tyMipsDW2App.ulReadHalfwordsInstructions*4,
                           pulCommonDataToProc,
                           2,
                           pulCommonDataFromProc,
                           2);
   
      usLocData = (unsigned short)pulCommonDataFromProc[0];
   
      if (bNormaliseEndian && ptyHandle->bBigEndian)
         *(unsigned short*)(pusLocpData + (ulCnt)) = ML_SwapEndianHalfword(usLocData);
      else
         *(unsigned short*)(pusLocpData + (ulCnt)) = usLocData;

      DLOG2((LOGFILE,"\tData=%04X",*(unsigned short*)(pusLocpData + (ulCnt*2))));
      ulLocAddress+=2;
      }
   return ErrRet;
}
/****************************************************************************
     Function: MLM_ReadBytesDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to halfword)
               unsigned long ulCount - number of halfwords
               void *pvData - pointer to data
       Output: int - error code
  Description: Read block of bytes using DW2 application. Endianess does not matter for bytes.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
int MLM_ReadBytesDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData)
{
   TyError tyResult;
   unsigned long pulDataToProc[2];
   TyDW2AppParams tyLocDW2Params;
   // check parameters, no alignment issues
   assert(ptyHandle != NULL);
   assert(pvData != NULL);
   assert(ulCount <= ML_MAX_MEMORY_BLOCK_SIZE);
   if (ulCount == 0)                                                                                     // make sure DW2 is not called with 0 count
      return ERR_NO_ERROR;
   // we need to fill parameters for DW2 application
   tyLocDW2Params.pvAppInst = (void *)ptyHandle->tyMipsDW2App.pulReadBytesApp;
   tyLocDW2Params.uiInstSize = (unsigned int)(ptyHandle->tyMipsDW2App.ulReadBytesInstructions * sizeof(unsigned long)); // size in bytes
   // see comments for ReadWordsDW2 to explanation about endianess
   tyLocDW2Params.bSwapDataValues = 0;                               // no swapping
   pulDataToProc[0] = ulAddress;                                     // 1st parameter is address
   pulDataToProc[1] = ulCount;                                       // 2nd parameter is number of bytes
   tyLocDW2Params.ppvAppDataToProc[0] = (void *)pulDataToProc;       // assign pointer 
   tyLocDW2Params.puiDataToProcSize[0] = 2 * sizeof(unsigned long);  // using 2 words (8 bytes)
   tyLocDW2Params.ppvAppDataToProc[1] = NULL;                        // no more data to proc
   tyLocDW2Params.puiDataToProcSize[1] = 0;
   tyLocDW2Params.ppvAppDataFromProc[0] = pvData;                    // data from processor are directly read bytes by DW2
   tyLocDW2Params.puiDataFromProcSize[0] = (unsigned int)ulCount;    // size in bytes
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;                      // no more output data
   tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;    // endianess flag is used when DSEG is accessed by bytes/halfwords
   // call DW2 in diskware and check result
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      {  // error occurred, so rather return all values as 0 to be consistent
      memset(pvData, 0, ulCount);
      return ML_ConvertDLError(tyResult);
      }
}
#endif
int MLM_ReadBytesDW2(TyMLHandle *ptyHandle, 
                     unsigned long ulAddress, 
                     unsigned long ulCount, 
                     void *pvData)
{
   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulCnt;
   unsigned long ulLocAddress;
   unsigned char ucLocData;
   unsigned char *pucLocpData;

   DLOG2((LOGFILE,"\n\t\tMLM_ReadBytesDW2 -> Called"));

   pucLocpData = (unsigned char*)pvData;
   ulLocAddress = ulAddress;
   for(ulCnt=0; ulCnt < ulCount; ulCnt++)
      {
      pulCommonDataToProc[0]=ulLocAddress;
      DLOG2((LOGFILE,"\tMLM_ReadBytesDW2  Address=%08X ",ulAddress));
      pulCommonDataFromProc[0] = 0xDEADDEAD; // Initialize to dead value
   
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulReadBytesApp,
                           ptyHandle->tyMipsDW2App.ulReadBytesInstructions*4,
                           pulCommonDataToProc,
                           1,
                           pulCommonDataFromProc,
                           1);
   
      ucLocData = (unsigned char)pulCommonDataFromProc[0];
   
      *(unsigned char*)(pucLocpData + ulCnt) = ucLocData;

      DLOG2((LOGFILE,"\tData=%02X",*(unsigned char*)(pucLocpData + ulCnt)));
      ulLocAddress++;
      }
   return ErrRet;
}

/****************************************************************************
     Function: MLM_WriteWordsDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
               unsigned char bNormaliseEndian - 1 if care about endianess, 0 otherwise
       Output: int - error code
  Description: Write block of words using DW2 application. See comments on MLM_ReadWordsDW2 for bNormaliseEndian.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
int MLM_WriteWordsDW2(TyMLHandle *ptyHandle, 
					  unsigned long ulAddress, 
					  unsigned long ulCount, 
					  void *pvData, 
					  unsigned char bNormaliseEndian)
{
   TyError tyResult;
   unsigned long pulDataToProc[2];
   TyDW2AppParams tyLocDW2Params;
   // check parameters and address alignment
   assert(ptyHandle != NULL);
   assert(pvData != NULL);
   assert(ulCount <= (ML_MAX_MEMORY_BLOCK_SIZE / 4));
   if (ulAddress & 0x3)
      return ERR_WRITEWPSAFE_UNALIGNED_START;
   if (ulCount == 0)                                                                                     // make sure DW2 is not called with 0 count
      return ERR_NO_ERROR;
   // we need to fill parameters for DW2 application
   tyLocDW2Params.pvAppInst = (void *)ptyHandle->tyMipsDW2App.pulWriteWordsApp;
   tyLocDW2Params.uiInstSize = (unsigned int)(ptyHandle->tyMipsDW2App.ulWriteWordsInstructions * sizeof(unsigned long)); // size in bytes
   if (bNormaliseEndian && ptyHandle->bBigEndian)
      {  // target is BE and we want to normalize it, all data to/from processor will be swapped before feeding into EJTAG DATA
      tyLocDW2Params.bSwapDataValues = 1;
      pulDataToProc[0] = ML_SwapEndianWord(ulAddress);               // 1st parameter is address (all data to/from proc will be swapped)
      pulDataToProc[1] = ML_SwapEndianWord(ulCount);                 // 2nd parameter is number of words (all data to/from proc will be swapped)
      }
   else
      {  // no swapping of bytes, treat data to/from processor as it is
      tyLocDW2Params.bSwapDataValues = 0;
      pulDataToProc[0] = ulAddress;                                  // 1st parameter is address
      pulDataToProc[1] = ulCount;                                    // 2nd parameter is number of words
      }
   tyLocDW2Params.ppvAppDataToProc[0] = (void *)pulDataToProc;       // assign pointer 
   tyLocDW2Params.puiDataToProcSize[0] = 2 * sizeof(unsigned long);  // using 2 words (8 bytes)
   tyLocDW2Params.ppvAppDataToProc[1] = pvData;                      // 2nd block for data itself in proper format
   tyLocDW2Params.puiDataToProcSize[1] = (unsigned int)(ulCount * sizeof(unsigned long));    // size in bytes
   tyLocDW2Params.ppvAppDataFromProc[0] = NULL;       tyLocDW2Params.puiDataFromProcSize[0] = 0;
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;       tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;                // endianess flag is used when DSEG is accessed by bytes/halfwords
   // call DW2 in diskware and check result
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}
#endif
int MLM_WriteWordsDW2(TyMLHandle *ptyHandle, 
					  unsigned long ulAddress, 
					  unsigned long ulCount, 
					  void *pvData, 
					  unsigned char bNormaliseEndian)
{
   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulCnt;
   unsigned long *pulLocpData;
   unsigned long ulLocAddress;
   unsigned long ulLocValue;

   DLOG2((LOGFILE,"\n\t\tMLM_WriteWordsDW2 -> Called"));

   if ((ulAddress & 0xFFFFFFFC) != ulAddress)
      {
      ASSERT_NOW();    // Read Must be Word aligned
      DLOG2((LOGFILE,"\n\tMLM_WriteWordsDW2  : Alignment ERROR !!"));
      return 99; // TO_DO: Fix This error Code
      }

   pulLocpData = (unsigned long*)pvData;
   ulLocAddress = ulAddress;
   for(ulCnt=0; ulCnt < ulCount; ulCnt++)
      {
      if (bNormaliseEndian && ptyHandle->bBigEndian)
         ulLocValue = ML_SwapEndianWord(*pulLocpData++);
      else
         ulLocValue = *pulLocpData++;
   
      pulCommonDataToProc[0]=ulLocAddress;
      pulCommonDataToProc[1]=ulLocValue;

      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulWriteWordsApp,
                           ptyHandle->tyMipsDW2App.ulWriteWordsInstructions*4,
                           pulCommonDataToProc,
                           4,
                           pulCommonDataFromProc,
                           4);
      ulLocAddress+=4;
      }
   return ErrRet;
}

/****************************************************************************
     Function: MLM_WriteHalfwordsDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of halfwords
               void *pvData - pointer to data
               unsigned char bNormaliseEndian - 1 if care about endianess, 0 otherwise
       Output: int - error code
  Description: Write block of halfwords using DW2 application. See comments on MLM_ReadWordsDW2 for bNormaliseEndian.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
int MLM_WriteHalfwordsDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData, unsigned char bNormaliseEndian)
{
   TyError tyResult;
   unsigned long pulDataToProc[2];
   TyDW2AppParams tyLocDW2Params;
   // check parameters and address alignment
   assert(ptyHandle != NULL);
   assert(pvData != NULL);
   assert(ulCount <= (ML_MAX_MEMORY_BLOCK_SIZE / 2));
   if (ulAddress & 0x1)
      return ERR_WRITEWPSAFE_UNALIGNED_START;
   if (ulCount == 0)                                                                                     // make sure DW2 is not called with 0 count
      return ERR_NO_ERROR;
   // we need to fill parameters for DW2 application
   tyLocDW2Params.pvAppInst = (void *)ptyHandle->tyMipsDW2App.pulWriteHalfwordsApp;
   tyLocDW2Params.uiInstSize = (unsigned int)(ptyHandle->tyMipsDW2App.ulWriteHalfwordsInstructions * sizeof(unsigned long)); // size in bytes
   if (bNormaliseEndian && ptyHandle->bBigEndian)
      {  // target is BE and we want to normalize it, all data to/from processor will be swapped before feeding into EJTAG DATA
      tyLocDW2Params.bSwapDataValues = 1;
      pulDataToProc[0] = ML_SwapEndianWord(ulAddress);               // 1st parameter is address (all data to/from proc will be swapped)
      pulDataToProc[1] = ML_SwapEndianWord(ulCount);                 // 2nd parameter is number of words (all data to/from proc will be swapped)
      }
   else
      {  // no swapping of bytes, treat data to/from processor as it is
      tyLocDW2Params.bSwapDataValues = 0;
      pulDataToProc[0] = ulAddress;                                  // 1st parameter is address
      pulDataToProc[1] = ulCount;                                    // 2nd parameter is number of halfwords
      }
   tyLocDW2Params.ppvAppDataToProc[0] = (void *)pulDataToProc;       // assign pointer 
   tyLocDW2Params.puiDataToProcSize[0] = 2 * sizeof(unsigned long);  // using 2 words (8 bytes)
   tyLocDW2Params.ppvAppDataToProc[1] = pvData;                      // 2nd block for data itself in proper format
   tyLocDW2Params.puiDataToProcSize[1] = (unsigned int)(ulCount * sizeof(unsigned short));    // size in bytes
   tyLocDW2Params.ppvAppDataFromProc[0] = NULL;       tyLocDW2Params.puiDataFromProcSize[0] = 0;
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;       tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;                // endianess flag is used when DSEG is accessed by bytes/halfwords
   // call DW2 in diskware and check result
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}
#endif
int MLM_WriteHalfwordsDW2(TyMLHandle *ptyHandle, 
                          unsigned long ulAddress, 
                          unsigned long ulCount, 
                          void *pvData, 
                          unsigned char bNormaliseEndian)
{
   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulCnt;
   unsigned short *pusLocpData;
   unsigned long ulLocAddress;
   unsigned short usLocValue;

   DLOG2((LOGFILE,"\n\t\tMLM_WriteHalfwordsDW2 -> Called"));

   if ((ulAddress & 0xFFFFFFFE) != ulAddress)
      {
      ASSERT_NOW();    // Read Must be Word aligned
      DLOG2((LOGFILE,"\n\tMLM_WriteHalfwordsDW2  : Alignment ERROR !!"));
      return 99; // TO_DO: Fix This error Code
      }

   pusLocpData = (unsigned short*)pvData;
   ulLocAddress = ulAddress;
   for(ulCnt=0; ulCnt < ulCount; ulCnt++)
      {
      if (bNormaliseEndian && ptyHandle->bBigEndian)
         usLocValue = ML_SwapEndianHalfword(*pusLocpData++);
      else
         usLocValue = *pusLocpData++;
   
      pulCommonDataToProc[0]=ulLocAddress;
      pulCommonDataToProc[1]=usLocValue;
   
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulWriteHalfwordsApp,
                           ptyHandle->tyMipsDW2App.ulWriteHalfwordsInstructions*4,
                           pulCommonDataToProc,
                           2,
                           pulCommonDataFromProc,
                           2);
      ulLocAddress+=2;
      }
   return ErrRet;
}

/****************************************************************************
     Function: MLM_WriteBytesDW2
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of halfwords
               void *pvData - pointer to data
       Output: int - error code
  Description: Write block of bytes using DW2 application. Endianess does not matter for bytes.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
int MLM_WriteBytesDW2(TyMLHandle *ptyHandle, unsigned long ulAddress, unsigned long ulCount, void *pvData)
{
   TyError tyResult;
   unsigned long pulDataToProc[2];
   TyDW2AppParams tyLocDW2Params;
   // check parameters and
   assert(ptyHandle != NULL);
   assert(pvData != NULL);
   assert(ulCount <= ML_MAX_MEMORY_BLOCK_SIZE);
   if (ulCount == 0)                                                                                     // make sure DW2 is not called with 0 count
      return ERR_NO_ERROR;
   // we need to fill parameters for DW2 application
   tyLocDW2Params.pvAppInst = (void *)ptyHandle->tyMipsDW2App.pulWriteBytesApp;
   tyLocDW2Params.uiInstSize = (unsigned int)(ptyHandle->tyMipsDW2App.ulWriteBytesInstructions * sizeof(unsigned long)); // size in bytes
   tyLocDW2Params.bSwapDataValues = 0;
   pulDataToProc[0] = ulAddress;                                     // 1st parameter is address
   pulDataToProc[1] = ulCount;                                       // 2nd parameter is number of bytes
   tyLocDW2Params.ppvAppDataToProc[0] = (void *)pulDataToProc;       // assign pointer 
   tyLocDW2Params.puiDataToProcSize[0] = 2 * sizeof(unsigned long);  // using 2 words (8 bytes)
   tyLocDW2Params.ppvAppDataToProc[1] = pvData;                      // 2nd block for data itself in proper format
   tyLocDW2Params.puiDataToProcSize[1] = (unsigned int)ulCount;      // size in bytes
   tyLocDW2Params.ppvAppDataFromProc[0] = NULL;       tyLocDW2Params.puiDataFromProcSize[0] = 0;
   tyLocDW2Params.ppvAppDataFromProc[1] = NULL;       tyLocDW2Params.puiDataFromProcSize[1] = 0;
   tyLocDW2Params.bBigEndian = ptyHandle->bBigEndian;                // endianess flag is used when DSEG is accessed by bytes/halfwords
   // call DW2 in diskware and check result
   tyResult = DL_OPXD_Mips_ExecuteDW2App(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, &tyLocDW2Params);
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      return ML_ConvertDLError(tyResult);
}
#endif
int MLM_WriteBytesDW2(TyMLHandle *ptyHandle, 
                      unsigned long ulAddress, 
                      unsigned long ulCount, 
                      void *pvData)
{
   TyError ErrRet=ERR_NO_ERROR;
   unsigned long ulCnt;
   unsigned char *pucLocpData;
   unsigned long ulLocAddress;
   unsigned char ucLocValue;

   DLOG2((LOGFILE,"\n\t\tMLM_WriteBytesDW2 -> Called"));

   pucLocpData = (unsigned char*)pvData;
   ulLocAddress = ulAddress;
   for(ulCnt=0; ulCnt < ulCount; ulCnt++)
      {
      ucLocValue = *pucLocpData++;
   
      pulCommonDataToProc[0]=ulLocAddress;
      pulCommonDataToProc[1]=ucLocValue;
   
      ErrRet=ML_ExecuteDW2(ptyHandle,
                           ptyHandle->tyMipsDW2App.pulWriteBytesApp,
                           ptyHandle->tyMipsDW2App.ulWriteBytesInstructions*4,
                           pulCommonDataToProc,
                           1,
                           pulCommonDataFromProc,
                           1);
      ulLocAddress++;
      }
   return ErrRet;
}

/****************************************************************************
     Function: MLM_CheckValidDmaAddressRange
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address
               unsigned long ulLength - number of bytes
               unsigned char bVirtualAddress - using virtual or physical address
       Output: unsigned char - 1 if access is within valid DMA range, 0 otherwise
  Description: Check if requested DMA access is inside valid range for DMA core.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static unsigned char MLM_CheckValidDmaAddressRange(TyMLHandle *ptyHandle, unsigned long ulStartAddress, unsigned long ulLength, unsigned char bVirtualAddress)
{
   unsigned long ulStartAddressPhysical = ulStartAddress;
   assert(ptyHandle != NULL);
   // check if restricted range is used
   if (!ptyHandle->bUseDmaRestrictedRange)
      return 1;                                                         // no reason to test the range
   if (bVirtualAddress)
      {  // convert virtual address to physical address
      (void)MLM_VirtualToPhysical(ptyHandle, ulStartAddress, ulLength, &ulStartAddressPhysical, &ulLength);
      }
   // check address ranger
   if (ulStartAddressPhysical < ptyHandle->ulStartOfDmaAddressRange)
      return 0;
   if (ulLength && (((ulStartAddressPhysical + ulLength) - 1) > ptyHandle->ulEndOfDmaAddressRange))
      return 0;
   // address range is valid
   return 1;
}

/****************************************************************************
     Function: MLM_VirtualToPhysical
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulVirtualAddress - virtual address in memory
               unsigned long ulLength - number of bytes to access
               unsigned long *pulPhysicalAddress - pointer to store physical address
               unsigned long *pulValidLength - pointer to number of valid bytes to access
       Output: int - error code
  Description: Convert virtual address into physical using fixed mapping system.
               MMU translation is not supported at the moment.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLM_VirtualToPhysical(TyMLHandle *ptyHandle, unsigned long ulVirtualAddress, unsigned long ulLength, 
                                 unsigned long *pulPhysicalAddress, unsigned long *pulValidLength)
{
   unsigned long ulPhysicalAddress = ulVirtualAddress;
   unsigned long ulValidLength = ulLength;
   // check parameters
   assert(ptyHandle != NULL);
   // using MMU mapping?
   if (!ptyHandle->bUseMMUMapping)
      {  // using fixed mapping system
      unsigned long ulBytesInPage = 0;
      if (ptyHandle->ucFixedMapSystem == 1)
         {  // fixed map scheme 1
         // virtual address                  physical address
         // 0x00000000 - 0x7FFFFFFF    ->    0x40000000 - 0xBFFFFFFF
         // 0x80000000 - 0x9FFFFFFF    ->    0x00000000 - 0x1FFFFFFF
         // 0xA0000000 - 0xBFFFFFFF    ->    0x00000000 - 0x1FFFFFFF
         // 0xC0000000 - 0xFFFFFFFF    ->    0xC0000000 - 0xFFFFFFFF
         if (ulVirtualAddress < 0x80000000)
            {
            ulPhysicalAddress = ulVirtualAddress + 0x40000000;
            ulBytesInPage = 0x80000000 - ulVirtualAddress;
            }
         else if (ulVirtualAddress < 0xA0000000)
            {
            ulPhysicalAddress = ulVirtualAddress - 0x80000000;
            ulBytesInPage = 0xA0000000 - ulVirtualAddress;
            }
         else if (ulVirtualAddress < 0xC0000000)
            {
            ulPhysicalAddress = ulVirtualAddress - 0xA0000000;
            ulBytesInPage = 0xC0000000 - ulVirtualAddress;
            }
         else
            {
            ulPhysicalAddress = ulVirtualAddress;
            ulBytesInPage = (0xFFFFFFFF - ulVirtualAddress) + 1;
            }
         }
      else
         {  // fixed map scheme 2
         // virtual address                  physical address
         // 0x00000000 - 0x1FFFFFFF    ->    0x00000000 - 0x1FFFFFFF
         // 0x20000000 - 0x7FFFFFFF    ->    0x20000000 - 0x7FFFFFFF
         // 0x80000000 - 0x9FFFFFFF    ->    0x00000000 - 0x1FFFFFFF
         // 0xA0000000 - 0xBFFFFFFF    ->    0x00000000 - 0x1FFFFFFF
         // 0xC0000000 - 0xFFFFFFFF    ->    0xC0000000 - 0xFFFFFFFF
         if (ulVirtualAddress < 0x20000000)
            {
            ulPhysicalAddress = ulVirtualAddress;
            ulBytesInPage = 0x20000000 - ulVirtualAddress;
            }
         else if (ulVirtualAddress < 0x80000000)
            {
            ulPhysicalAddress = ulVirtualAddress;
            ulBytesInPage = 0x80000000 - ulVirtualAddress;
            }
         else if (ulVirtualAddress < 0xA0000000)
            {
            ulPhysicalAddress = ulVirtualAddress - 0x80000000;
            ulBytesInPage = 0xA0000000 - ulVirtualAddress;
            }
         else if (ulVirtualAddress < 0xC0000000)
            {
            ulPhysicalAddress = ulVirtualAddress - 0xA0000000;
            ulBytesInPage = 0xC0000000 - ulVirtualAddress;
            }
         else
            {
            ulPhysicalAddress = ulVirtualAddress;
            ulBytesInPage = (0xFFFFFFFF - ulVirtualAddress) + 1;
            }
         }
      // check if number of bytes in page is enough for access length
      if (ulLength > ulBytesInPage)
         ulValidLength = ulBytesInPage;
      else
         ulValidLength = ulLength;
      }
   // MMU mapping has not been implemented yet, so physical == virtual address
   // assign address and valid length and finish
   if (pulPhysicalAddress != NULL)
      *pulPhysicalAddress = ulPhysicalAddress;
   if (pulValidLength != NULL)
      *pulValidLength = ulValidLength;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLM_LearnPipeLine
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: Error code
  Description: Learn about the pipeline so that our optimized memory routines
               will work without input from us
               Do a store followed by a load and then a jump back to the reset 
               vector. Record when events happen.
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
extern unsigned long * LtoP(unsigned long value); //TODO remove
void MLM_LearnPipeLine(TyMLHandle *ptyHandle)
{
   TyBitArray           tyAddress;   // Instead of initializing each time, do it once
   TyBitArray           tyReqAddress;
   TyBitArray           tyDestAddress;
   unsigned long        ulDataReadBack;
   unsigned long        ulLoop;
   unsigned long        ulNumAcc;
   int                 	bDataAlreadyAccepted = FALSE;
   int                 	bDataAlreadyWritten  = FALSE;
   TyTypeOfOperation    tyTypeOfOperation;


   memset(&tyAddress,0x00,sizeof(tyAddress));
   memset(&tyReqAddress,0x00,sizeof(tyReqAddress));
   memset(&tyDestAddress,0x00,sizeof(tyDestAddress));
   
   tyAddress.Array.ulBitArray[0] = DE_VECT;

   ptyHandle->Pi.ucGlobalCycForMemRead1           = 0x0; 
   ptyHandle->Pi.ucGlobalCycForMemRead2           = 0x0; 
   ptyHandle->Pi.ucGlobalCycForMemStore1          = 0x0;
   ptyHandle->Pi.ucGlobalCycForMemStore2          = 0x0;
   ptyHandle->Pi.ucGlobalCyclesBeforeJump         = 0x0;  


   // Feed in a few nops just to flush out the pipeline - setup t1
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress, lui_t1_0xff21,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress, nop		    ,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress, nop		    ,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress, nop		    ,0,&ulDataReadBack,INSTRUCTIONFETCH);
   // Now we want to know when data will be presented to us when we do two
   // stores one after another. Feed in the stores and wait for the 
   // requests to satisfy the stores
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,sw_t2_516_t1,0,&ulDataReadBack,INSTRUCTIONFETCH_IC);


   // The AMD AU1500 doesn't require s delay in the pipeline for a store operation so
   // check if the processor is ready to give us dat
   tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
   if ((tyTypeOfOperation == READWORD) || (tyTypeOfOperation == WRITEWORD))
      {
	  //TODO use ML_ ???
      (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle, 	//tyDevHandle
									ptyHandle->ulMipsCore, 	//ulCore
									IR_LENGTH,				//ulIRLength 
									LtoP(ADDRESS), 			//*pulDataInForIR
									ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
									LtoP(0),//*pulDataInForDR
									(unsigned long*)&tyReqAddress);  //*pulDataOutFromDR
      DLOG((LOGFILE,"\n\t\tAddress -> %08X",tyReqAddress.Array.ulBitArray[0]));
      if (tyReqAddress.Array.ulBitArray[0] == MAGIC_FIFO_LOCATION)
         {
         // Excellent - he wants to write. Accept it
         (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyReqAddress,0x0,0,&ulDataReadBack,WRITEFROMPROCESSOR);
         bDataAlreadyAccepted = TRUE;
         }
      }
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,sw_t3_516_t1,0,&ulDataReadBack,INSTRUCTIONFETCH_IC);

   // The AMD AU1500 doesn't require s delay in the pipeline for a store operation so
   // check if the processor is ready to give us data
   if(bDataAlreadyAccepted)
      {
      tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
      if ((tyTypeOfOperation == READWORD) || (tyTypeOfOperation == WRITEWORD))
         {
		 (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle, 	//tyDevHandle
									   ptyHandle->ulMipsCore, 	//ulCore
									   IR_LENGTH,				//ulIRLength 
									   LtoP(ADDRESS), 			//*pulDataInForIR
									   ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
									   LtoP(0),//*pulDataInForDR
									   (unsigned long*)&tyReqAddress);  //*pulDataOutFromDR
         DLOG((LOGFILE,"\n\t\tAddress -> %08X",tyReqAddress.Array.ulBitArray[0]));
         if (tyReqAddress.Array.ulBitArray[0] == MAGIC_FIFO_LOCATION)
            {
            // Excellent - he wants to write. Accept it
            (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyReqAddress,0x0,0,&ulDataReadBack,WRITEFROMPROCESSOR);
            bDataAlreadyAccepted = TRUE;
            }
         }
      }

   if (!bDataAlreadyAccepted)
	  {   
	  ptyHandle->Pi.ucGlobalCycForMemStore1 ++;  // The first cycle is the second store
	  ulNumAcc = 0x2;

	  // Wait up to 10 cycles for the request
      for (ulLoop=0;ulLoop < 10; ulLoop = 0x0)
         {
		 tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
		 if ((tyTypeOfOperation != READWORD) && (tyTypeOfOperation != WRITEWORD))
			break;
		 (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle, 	//tyDevHandle
									   ptyHandle->ulMipsCore, 	//ulCore
									   IR_LENGTH,				//ulIRLength 
									   LtoP(ADDRESS), 			//*pulDataInForIR
									   ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
									   LtoP(0),//*pulDataInForDR
									   (unsigned long*)&tyReqAddress);  //*pulDataOutFromDR   
         if (tyReqAddress.Array.ulBitArray[0] == tyAddress.Array.ulBitArray[0])
            {
			if(ulNumAcc == 2)
			   ptyHandle->Pi.ucGlobalCycForMemStore1 ++;
   
			ptyHandle->Pi.ucGlobalCycForMemStore2 ++;
   
            // Next sequential instruction - feed a nop
			(void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
			}
         else
            if (tyReqAddress.Array.ulBitArray[0] == MAGIC_FIFO_LOCATION)
			   {
			   // Excellent - he wants to write. Accept it
			   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyReqAddress,0x0,0,&ulDataReadBack,WRITEFROMPROCESSOR);
			   ulNumAcc--;
			   ptyHandle->Pi.ucGlobalCycForMemStore2 ++;
			   if (!ulNumAcc)
				  break;
			   }
         }
      }

   // A few nops to clear the snots out
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);


   // Load into t0 - we will keep feeding nops until we get the request to feed from
   // the address we have just requested
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,lw_t0_512_t1,0,&ulDataReadBack,INSTRUCTIONFETCH);
  
   // The AMD AU1500 doesn't require s delay in the pipeline for a load operation so
   // check if the processor is ready to retrieve data
   tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
   if (tyTypeOfOperation == READWORD)
      {
	  (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle, 	//tyDevHandle
									ptyHandle->ulMipsCore, 	//ulCore
									IR_LENGTH,				//ulIRLength 
									LtoP(ADDRESS), 			//*pulDataInForIR
									ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
									LtoP(0),//*pulDataInForDR
									(unsigned long*)&tyReqAddress);  //*pulDataOutFromDR      
         if (tyReqAddress.Array.ulBitArray[0] == LOCATION_OF_START_ADDRESS)
            {
            // Excellent - he wants to read. Feed him the reset vector
            (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyReqAddress,0,DE_VECT_FOR_DATA_SCAN,&ulDataReadBack,DATAFETCH);
            bDataAlreadyWritten = TRUE;
            }
      }
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,lw_t0_512_t1,0,&ulDataReadBack,INSTRUCTIONFETCH);
   
   // The AMD AU1500 doesn't require s delay in the pipeline for a load operation so
   // check if the processor is ready to retrieve data
   if (bDataAlreadyWritten)
      {
      tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
      if (tyTypeOfOperation == READWORD)
         {
		 (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle, 	//tyDevHandle
									   ptyHandle->ulMipsCore, 	//ulCore
									   IR_LENGTH,				//ulIRLength 
									   LtoP(ADDRESS), 			//*pulDataInForIR
									   ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
									   LtoP(0),//*pulDataInForDR
									   (unsigned long*)&tyReqAddress);  //*pulDataOutFromDR           if (tyReqAddress.Array.ulBitArray[0] == LOCATION_OF_START_ADDRESS)
            {
            // Excellent - he wants to read. Feed him the reset vector
            (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyReqAddress,0,DE_VECT_FOR_DATA_SCAN,&ulDataReadBack,DATAFETCH);
            bDataAlreadyWritten = TRUE;
            }
         }
      }

   if (!bDataAlreadyWritten)
      {
      ptyHandle->Pi.ucGlobalCycForMemRead1++;
      ulNumAcc = 0x2;
   
      // Wait up to 10 cycles for the request
      for (ulLoop=0;ulLoop < 10; ulLoop = 0x0)
         {
         tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
         if (tyTypeOfOperation != READWORD)
            break;
         (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle,  //tyDevHandle
                                       ptyHandle->ulMipsCore,  //ulCore
                                       IR_LENGTH,           //ulIRLength 
                                       LtoP(ADDRESS),          //*pulDataInForIR
                                       ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
                                       LtoP(0),//*pulDataInForDR
                                       (unsigned long*)&tyReqAddress);  //*pulDataOutFromDR     
         if (tyReqAddress.Array.ulBitArray[0] == tyAddress.Array.ulBitArray[0])
            {
            // Next sequential instruction - feed a nop
            if (ulNumAcc == 2)
               ptyHandle->Pi.ucGlobalCycForMemRead1 ++;
   
            ptyHandle->Pi.ucGlobalCycForMemRead2++;
            (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
            }
         else
            if (tyReqAddress.Array.ulBitArray[0] == LOCATION_OF_START_ADDRESS)
               {
               // Excellent - he wants to read. Feed him the reset vector
               (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyReqAddress,0,DE_VECT_FOR_DATA_SCAN,&ulDataReadBack,DATAFETCH);
               ulNumAcc--;
               ptyHandle->Pi.ucGlobalCycForMemRead2 ++;
               if (!ulNumAcc)
                  break;
               }
         }
      }

   // A few nops to clear the snots out
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);


   // Now perform the jump and wait until we get there
   tyDestAddress.Array.ulBitArray[0] = DE_VECT;
   (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,jr_t0,0,&ulDataReadBack,INSTRUCTIONFETCH);
   
   // Wait up to 10 cycles for the request
   for (ulLoop=0;ulLoop < 10; ulLoop = 0x0)
      {
      tyTypeOfOperation = ML_DetermineTypeOfOperation(ptyHandle);
      if (tyTypeOfOperation != READWORD)
         break;
      (void)DL_OPXD_JtagScanIRandDR(ptyHandle->iDLHandle,    //tyDevHandle
                                    ptyHandle->ulMipsCore,  //ulCore
                                    IR_LENGTH,           //ulIRLength 
                                    LtoP(ADDRESS),          //*pulDataInForIR
                                    ptyHandle->ulMipsCoreEjtagDataLength,//ulDRLength
                                    LtoP(0),//*pulDataInForDR
                                    (unsigned long*)&tyReqAddress);  //*pulDataOutFromDR  
      if (tyReqAddress.Array.ulBitArray[0] == tyAddress.Array.ulBitArray[0])
         {
         // Next sequential instruction - feed a nop
         ptyHandle->Pi.ucGlobalCyclesBeforeJump++;
         (void)ML_ExecuteInstruction(ptyHandle,(unsigned long*)&tyAddress,nop,0,&ulDataReadBack,INSTRUCTIONFETCH);
         }
      else
         if (tyReqAddress.Array.ulBitArray[0] == tyDestAddress.Array.ulBitArray[0])
            {
            // Thats it we're done
            break;
            }
         else
            {
            // Something unexpected like an exception happened.
            ASSERT_NOW();
            break;
            }
      }

   ptyHandle->Pi.ucGlobalCycBetwStores = ptyHandle->Pi.ucGlobalCycForMemStore2 - ptyHandle->Pi.ucGlobalCycForMemStore1;
   ptyHandle->Pi.ucGlobalCycBetwReads  = ptyHandle->Pi.ucGlobalCycForMemRead2 - ptyHandle->Pi.ucGlobalCycForMemRead1;

   ptyHandle->Pi.ucGlobalCycBetwReads = ptyHandle->Pi.ucGlobalCycBetwReads;

   ptyHandle->Pi.bPipeLineLearned = 0x1; 

   TyDwMipsPipelineConfig tyPipelineConfig;
   // fill Opella-XD pipeline config for MIPS core (quite simple)
   tyPipelineConfig.bPipeLineLearned            = ptyHandle->Pi.bPipeLineLearned;
   tyPipelineConfig.ucPipelineCycBetwReads      = ptyHandle->Pi.ucGlobalCycBetwReads;
   tyPipelineConfig.ucPipelineCycBetwStores     = ptyHandle->Pi.ucGlobalCycBetwStores;
   tyPipelineConfig.ucPipelineCycForMemRead1    = ptyHandle->Pi.ucGlobalCycForMemRead1;
   tyPipelineConfig.ucPipelineCycForMemRead2    = ptyHandle->Pi.ucGlobalCycForMemRead2;
   tyPipelineConfig.ucPipelineCycForMemStore1   = ptyHandle->Pi.ucGlobalCycForMemStore1;
   tyPipelineConfig.ucPipelineCycForMemStore2   = ptyHandle->Pi.ucGlobalCycForMemStore2;
   tyPipelineConfig.ucPipelineCyclesBeforeJump  = ptyHandle->Pi.ucGlobalCyclesBeforeJump;
   // we need to decide which core are we configuring (we assume pipeline config is relevant only for DEBUG core (MIPS core) not DMA core
   (void)DL_OPXD_Mips_WriteDiskwareStruct(ptyHandle->iDLHandle, ptyHandle->ulMipsCore , (int)DWS_MIPS_PIPELINE, (void *)&tyPipelineConfig, sizeof(TyDwMipsPipelineConfig));

}

/****************************************************************************
     Function: MLM_ReadWord
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: 
  Description: 
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
unsigned char MLM_WithinRestrictedDmaAddressRange(TyMLHandle *ptyHandle,
                                                  unsigned long ulStartAddress, 
                                                  unsigned long ulLength)
{
    unsigned long ulStartPhysicalAddress;
    unsigned long ulEndPhysicalAddress;
    unsigned long ulBytesToUse;   
    unsigned long ulStartOfDmaAddressRange;
    unsigned long ulEndOfDmaAddressRange;

    DLOG2((LOGFILE,"\n\t\tMLM_WithinRestrictedDmaAddressRange-> Called"));
    DLOG3((LOGFILE,"\n\t\tMLM_WithinRestrictedDmaAddressRange -> : Start %08lX \t Length %08lX Bytes",ulStartAddress,ulLength*4)); 

    ulStartOfDmaAddressRange  = ptyHandle->ulStartOfDmaAddressRange;
    ulEndOfDmaAddressRange    = ptyHandle->ulEndOfDmaAddressRange;    

    (void) MLM_VirtualToPhysical(ptyHandle,
                                 ulStartAddress,
                                 ulLength*4,
                                 &ulStartPhysicalAddress,
                                 &ulBytesToUse);

    ulEndPhysicalAddress = ulStartPhysicalAddress + ulBytesToUse;

    //check if ulPhyiscalAddress and ulLength*4 is within DMA address range     
    if(ulStartPhysicalAddress < ulStartOfDmaAddressRange) 
        {
        DLOG3((LOGFILE,"\n\t\tMLM_WithinRestrictedDmaAddressRange -> CPU access as Start %08lX is less than StartOfDmaRange %08lX",
               ulStartPhysicalAddress,ulStartOfDmaAddressRange)); 
        return FALSE;
        }
    else
       {
       if(ulEndPhysicalAddress > ulEndOfDmaAddressRange+1) 
            {
            DLOG3((LOGFILE,"\n\t\tMLM_WithinRestrictedDmaAddressRange -> CPU access as End %08lX is greater than EndOfDmaRange %08lX",
                   ulEndPhysicalAddress,ulStartOfDmaAddressRange)); 
            return FALSE;
            }
       else
           {
           DLOG3((LOGFILE,"\n\t\tMLM_WithinRestrictedDmaAddressRange -> DMA access as Start %08lX and  End %08lX are withinDMA range\n",
                  ulStartPhysicalAddress,ulEndPhysicalAddress));
           return TRUE;
           }
       }   
}

/****************************************************************************
     Function: MLM_ReadWord
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: 
  Description: 
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
TyError MLM_ReadWord ( TyMLHandle *ptyHandle,
                       unsigned long ulStartAddress, 
                       unsigned long *pulDestinationPointer)
{      
  TyError ErrRet;
  unsigned long ulPhysicalAddress;
  unsigned long ulBytesToUse;
  unsigned char bWithinRestrictedDmaAddressRange;

  DLOG2((LOGFILE,"\n\t\tMLM_ReadWord -> Called"));

  (void)DL_OPXD_SyncCache(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, 
                          ulStartAddress & 0xFFFFFFFC );

  if(ptyHandle->bUseDmaRestrictedRange)
      bWithinRestrictedDmaAddressRange = 
         MLM_WithinRestrictedDmaAddressRange(ptyHandle, ulStartAddress, 1);
  else
      bWithinRestrictedDmaAddressRange = TRUE;
 
  if (ptyHandle->bDmaSupported && 
      ptyHandle->bDmaEnabledByUser && 
      bWithinRestrictedDmaAddressRange) // && 0x0) // TEMP DISABLE
     {
     // Address Translation HERE
     (void) MLM_VirtualToPhysical(ptyHandle,
                                  ulStartAddress,
                                  4,
                                  &ulPhysicalAddress,
                                  &ulBytesToUse);
     ErrRet = MLM_ReadWordDma(ptyHandle,ulPhysicalAddress,pulDestinationPointer);
     }
  else
     {
        ErrRet = DL_OPXD_Mips_ReadWord(ptyHandle->iDLHandle, 
                                       ptyHandle->ulMipsCore, 
                                       ulStartAddress, 
                                       pulDestinationPointer, 
                                       ptyHandle->bBigEndian, 
                                       ptyHandle->bDmaEnabledByUser);
        return ML_ConvertDLError(ErrRet);
     }
  return ErrRet;
}

/****************************************************************************
     Function: MLM_LearnPipeLine
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: 
  Description: 
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
unsigned short MLM_GetHalfWord(TyMLHandle *ptyHandle,
                               unsigned long ulStartAddress)
{

   unsigned long  ulData=0;
   unsigned short usData=0;
   unsigned long ulWordAlignedAddress;

   DLOG2((LOGFILE,"\n\t\tMLM_GetHalfWord -> Called"));

   if ((ulStartAddress & 1) != 0)
      {
      // ulStartAddress Not Half Word aligned
      // Break now
      ASSERT_NOW();
      return 0xDEAD;
      }

   ulWordAlignedAddress = ulStartAddress & 0xFFFFFFFC;

   (void)MLM_ReadWord(ptyHandle,ulWordAlignedAddress, &ulData);


   if ((ulStartAddress & 3) == 2)
      {
      // On HalfWord Boundary
      usData = (unsigned short)(ulData >> 16) & 0xFFFF;
      }
   else
      {
      // On Word Boundary
      usData = (unsigned short)(ulData & 0xFFFF);
      }

   return usData;

}

/****************************************************************************
     Function: MLM_GetWord
     Engineer: Nikolay Chokoev
        Input: *ptyHandle : handle
       Output: 
  Description: 
Date           Initials    Description
11-Jul-2008    NCH          Initial
****************************************************************************/
unsigned long MLM_GetWord(TyMLHandle *ptyHandle,
                          unsigned long ulStartAddress)
{

   unsigned long ulValue=0;

   DLOG2((LOGFILE,"\n\t\tMLM_GetWord -> Called"));

   if ((ulStartAddress & 0xFFFFFFFC) != ulStartAddress)
      {
      // ulStartAddress Not Word aligned
      // Break now
      ASSERT_NOW();
      return 0xDEADDEAD;
      }

   (void)MLM_ReadWord(ptyHandle, ulStartAddress, &ulValue);

   return ulValue;

}

/****************************************************************************
     Function: MLM_WriteMemoryStd
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data (cannot be NULL)
               unsigned long ulLength - number of bytes in memory(at least 1)
               unsigned long ulObjectSize - size of object to access 
			        (0 - does not matter, 1 - byte, 2 - halfword, 4 - word)
       Output: int - error code
  Description: Write block of memory using DiskWare calls
               Access type is given by object size type, 
			   address MUST be aligned regarding selected object type).  
Date           Initials    Description
28-Nov-2008    SPC          Initial
****************************************************************************/
int MLM_WriteMemoryStd(TyMLHandle *ptyHandle, unsigned long ulStartAddress, 
					   unsigned char *pucData, unsigned long ulLength, 
					   unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pucData != NULL);
   assert(ulLength > 0);
   assert(ulLength < ML_MAX_MEMORY_BLOCK_SIZE);
   // address and length alignment should be checked in MLM function calling this one
   // if object size is specified, do exactly what caller ask for (no optimization, etc.)
   if (ulObjectSize == 0)
      {  // caller does not care what type of access to use, preferring fastest way
      // no assumption about length and alignment
      if (ulStartAddress % 4)
         {  // there are some unaligned bytes
         unsigned long ulFirstBytes = 4 - (ulStartAddress % 4);
         if (ulLength < ulFirstBytes)
            ulFirstBytes = ulLength;
         // deal with first bytes 
		 // TODO: currently we are doing it with DW2 App. we have to implement it for this
         iResult = MLM_WriteBytesDW2(ptyHandle, ulStartAddress, ulFirstBytes, (void *)pucData);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address 
         ulStartAddress += (4 - (ulStartAddress % 4));         // align address to next word
         ulLength -= ulFirstBytes;
         pucData += ulFirstBytes;
         }
      if (ulLength / 4)
         {  // do optimized word access
         if (ptyHandle->bUseTargetMemoryWhileWriting && (ulLength >= ptyHandle->bCacheFlushFastTreshold))
            iResult = MLM_WriteWordsFast(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData);
         else
            iResult = MLM_WriteWords(ptyHandle, ulStartAddress, ulLength, (unsigned long*)pucData);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address, length and pointer to data
         ulStartAddress += ((ulLength / 4) * sizeof(unsigned long));
         pucData += ((ulLength / 4) * sizeof(unsigned long));
         ulLength -= ((ulLength / 4) * sizeof(unsigned long));
         }
      if (ulLength % 4)
         {  // any remaining bytes
         iResult = MLM_WriteBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
         ulLength = 0;
         }
      }
   else if (ulObjectSize == 4)
      {  // requesting word access, address and length MUST be aligned
      //iResult = MLM_WriteWordsDW2(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData, 1);
      if( (ulLength/4) == 1 )
         {
         iResult = MLM_WriteWord(ptyHandle, ulStartAddress,*(unsigned long*)pucData);
         }
      else
         {
         iResult = MLM_WriteWords(ptyHandle, ulStartAddress, ulLength/4, (unsigned long*)pucData);
         }
      }
   else if (ulObjectSize == 2)
      {  // requesting halfword access, address and length MUST be aligned
      iResult = MLM_WriteHalfwordsDW2(ptyHandle, ulStartAddress, ulLength / 2, (void *)pucData, 1);
      }
   else if (ulObjectSize == 1)
      {  // requesting byte access
      iResult = MLM_WriteBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
      }
   else
      {  // not supported size
      return ERR_WRITEWRDSTD_UNALIGNED_START;
      }
   return iResult;
}

/****************************************************************************
     Function: MLM_WriteWords
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Write block of words using Diskware calls.
Date           Initials    Description
28-Nov-2008    SPC          Initial
****************************************************************************/
int MLM_WriteWords(TyMLHandle *ptyHandle, 
					  unsigned long ulAddress, 
					  unsigned long ulCount, 
					  unsigned long *pulData)
{
    int iResult = ERR_NO_ERROR;
    unsigned long ulBytesWrittenSoFar = 0;
    unsigned long ulNumberOfWordPairsToWrite = 0;

    if ((ulAddress & 0xFFFFFFFC) != ulAddress)
        {
        ASSERT_NOW();    // Read Must be Word aligned
        DLOG2((LOGFILE,"\n\MLM_WriteWords  : Alignment ERROR !!"));
        return ERR_WRITEWRDSTD_UNALIGNED_START;
        }

    (void)DL_OPXD_SyncCache(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, 
                            ulAddress & 0xFFFFFFFC );

    // First we write as batches of word pairs
    ulNumberOfWordPairsToWrite = (ulCount - ulBytesWrittenSoFar) / 8;
    if (ulNumberOfWordPairsToWrite != 0)
        {
        iResult = DL_OPXD_Mips_WriteWordPairsFast( ptyHandle->iDLHandle, 
                                                   ptyHandle->ulMipsCore, ulAddress, 
                                                   pulData, 
                                                   ulNumberOfWordPairsToWrite, 
                                                   ptyHandle->bBigEndian);
        if  ( iResult != ERR_NO_ERROR)
            {
            return ML_ConvertDLError(iResult);
            }
        }
    ulBytesWrittenSoFar += ulNumberOfWordPairsToWrite * 8; 
    ulAddress += ulBytesWrittenSoFar;
    pulData += ulBytesWrittenSoFar / 4;
    if ( (ulCount - ulBytesWrittenSoFar) >= 4 )
        {
        // We must write another Word before we do the final bytes
        iResult = DL_OPXD_Mips_WriteWord(ptyHandle->iDLHandle, ptyHandle->ulMipsCore,
                                         ulAddress, pulData, ptyHandle->bBigEndian, 
                                         ptyHandle->bDmaEnabledByUser);
        if  ( iResult != ERR_NO_ERROR)
            {
            return ML_ConvertDLError(iResult);
            }

        ulAddress +=4;
        ulBytesWrittenSoFar +=4;
        }
    return iResult;
}

/****************************************************************************
     Function: MLM_WriteWord
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulData - Data to write
       Output: int - error code
  Description: Write block of words using Diskware calls.
Date           Initials    Description
16-Dec-2008    SPC          Initial
****************************************************************************/
int MLM_WriteWord(TyMLHandle *ptyHandle, 
					  unsigned long ulAddress, 
					  unsigned long ulData)
{
    int iResult = ERR_NO_ERROR;

    if ((ulAddress & 0xFFFFFFFC) != ulAddress)
        {
        ASSERT_NOW();    // Read Must be Word aligned
        DLOG2((LOGFILE,"\n\MLM_WriteWords  : Alignment ERROR !!"));
        return ERR_WRITEWRDSTD_UNALIGNED_START;
        }

    (void)DL_OPXD_SyncCache(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, 
                            ulAddress & 0xFFFFFFFC );

    iResult = DL_OPXD_Mips_WriteWord(ptyHandle->iDLHandle, ptyHandle->ulMipsCore,
                                     ulAddress, &ulData, ptyHandle->bBigEndian, 
                                     ptyHandle->bDmaEnabledByUser);

    if  ( iResult != ERR_NO_ERROR)
        {
        return ML_ConvertDLError(iResult);
        }

    return iResult;
}

/****************************************************************************
     Function: MLM_ReadMemoryStd
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulStartAddress - virtual address in memory
               unsigned char *pucData - pointer to data (cannot be NULL)
               unsigned long ulLength - number of bytes in memory(at least 1)
               unsigned long ulObjectSize - size of object to access 
               (0 - does not matter, 1 - byte, 2 - halfword, 4 - word)
       Output: int - error code
  Description: Read block of memory using iskWare calls
               Access type is given by object size type, 
               address MUST be aligned regarding selected object type).  
Date           Initials    Description
01-Dec-2008    SPC         Initial
****************************************************************************/
int MLM_ReadMemoryStd(TyMLHandle *ptyHandle, unsigned long ulStartAddress, 
                      unsigned char *pucData, unsigned long ulLength, 
                      unsigned long ulObjectSize)
{
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   assert(pucData != NULL);
   assert(ulLength > 0);
   assert(ulLength < ML_MAX_MEMORY_BLOCK_SIZE);
   // address and length alignment should be checked in MLM function calling this one
   // if object size is specified, do exactly what caller ask for (no optimization, etc.)
   if (ulObjectSize == 0)
      {  // caller does not care what type of access to use, preferring fastest way
      // no assumption about length and alignment
      if (ulStartAddress % 4)
         {  // there are some unaligned bytes
         unsigned long ulFirstBytes = 4 - (ulStartAddress % 4);
         if (ulLength < ulFirstBytes)
            ulFirstBytes = ulLength;
         // deal with first bytes
         iResult = MLM_ReadBytesDW2(ptyHandle, ulStartAddress, ulFirstBytes, (void *)pucData);
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address 
         ulStartAddress += (4 - (ulStartAddress % 4));         // align address to next word
         ulLength -= ulFirstBytes;
         pucData += ulFirstBytes;
         }
      if (ulLength / 4)
         {  // do optimized word access
         if (ptyHandle->bUseTargetMemoryWhileReading && 
             (ulLength >= ptyHandle->bCacheFlushFastTreshold))
            {
            iResult = MLM_ReadWordsFast(ptyHandle, ulStartAddress, ulLength / 4, (void *)pucData);
            }
         else
            {
            if( (ulLength / 4) == 1 )
               {
               iResult = MLM_ReadWord(ptyHandle, ulStartAddress,(unsigned long *)pucData);
               }
            else
               {
               iResult = MLM_ReadWords(ptyHandle, ulStartAddress, ulLength / 4, (unsigned long*)pucData);
               }
            }
         if (iResult != ERR_NO_ERROR)
            return iResult;
         // update address, length and pointer to data
         ulStartAddress += ((ulLength / 4) * sizeof(unsigned long));
         pucData += ((ulLength / 4) * sizeof(unsigned long));
         ulLength -= ((ulLength / 4) * sizeof(unsigned long));
         }
      if (ulLength % 4)
         {  // any remaining bytes
         iResult = MLM_ReadBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
         ulLength = 0;
         }
      }
   else if (ulObjectSize == 4)
      {  // requesting word access, address and length MUST be aligned
      if( (ulLength/4) == 1 )
         {
         iResult = MLM_ReadWord(ptyHandle, ulStartAddress,(unsigned long *)pucData);
         }
      else
         {
         iResult = MLM_ReadWords(ptyHandle, ulStartAddress, ulLength / 4, (unsigned long*)pucData);
         }
      }
   else if (ulObjectSize == 2)
      {  // requesting halfword access, address and length MUST be aligned
      iResult = MLM_ReadHalfwordsDW2(ptyHandle, ulStartAddress, ulLength / 2, (void *)pucData, 1);
      }
   else if (ulObjectSize == 1)
      {  // requesting byte access
      iResult = MLM_ReadBytesDW2(ptyHandle, ulStartAddress, ulLength, (void *)pucData);
      }
   else
      {  // not supported size
      memset((void *)pucData, 0, ulLength);
      return ERR_READWRDSTD_UNALIGNED_START;
      }
   return iResult;
}

/****************************************************************************
     Function: MLM_ReadWords
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddress - start address (must be aligned to word)
               unsigned long ulCount - number of words
               void *pvData - pointer to data
       Output: int - error code
  Description: Write block of words using Diskware calls.
Date           Initials    Description
28-Nov-2008    SPC          Initial
****************************************************************************/
int MLM_ReadWords(TyMLHandle *ptyHandle, 
					  unsigned long ulAddress, 
					  unsigned long ulCount, 
					  unsigned long *pulData)
{
    int iResult = ERR_NO_ERROR;

    if ((ulAddress & 0xFFFFFFFC) != ulAddress)
        {
        ASSERT_NOW();    // Read Must be Word aligned
        DLOG2((LOGFILE,"\n\MLM_ReadWords  : Alignment ERROR !!"));
        return ERR_READWRDSTD_UNALIGNED_START;
        }

    (void)DL_OPXD_SyncCache(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, 
                            ulAddress & 0xFFFFFFFC );

    iResult = DL_OPXD_Mips_ReadMultipleWordsStd(ptyHandle->iDLHandle, ptyHandle->ulMipsCore, ulAddress, pulData, ulCount, ptyHandle->bBigEndian);
    if  ( iResult != ERR_NO_ERROR)
        {
        return ML_ConvertDLError(iResult);
        }

    return iResult;
}


