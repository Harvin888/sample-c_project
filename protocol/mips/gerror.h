/****************************************************************************
       Module: gerror.h
     Engineer: Nikolay Chokoev
  Description: Error definitions.
Date           Initials    Description
04-Jul-2008    NCH          Initial
23-Dec-2008    SPC          Updated error messages, Made errors common to 
                            GDBServer and PathFinder.
****************************************************************************/

#define MAX_ERROR_STRING 1000


/****************************************************************************
  Description: Rules of Use
 
   To Add an Error:
   ****************

   1. Create Error Definition of form ERR_NAME giving it the number of 
      ERR_LAST_ERROR (Note: All errors are decimal) Then Increment the 
     value of ERR_LAST_ERROR
   2. Give the Error a String description to be displayed in pathfinder
      FORM STRERR_NAME
   3. If there isn't a General error description of the the form: 
      GENERR_SOMETHING which describes the error then you should create one
   4. Finally update the tyError table to include your ERR_NAME,STR_ERR_NAME
      set the flag indicating if a target reset is needed 0 = NO 1 = YES
   5. NOTE: Please ensure that the order of the tyError Array matches the
      numbering of the Error Definitions.
      

****************************************************************************/



/****************************************************************************
  Description: ERROR Definitions
****************************************************************************/


#define ERR_NO_ERROR                                0
#define ERR_UNKNOWN_ERROR                           1
#define ERR_FAILED_TO_SEE_OPELLA_PPT                2
#define ERR_SETBP_NOBP                              3
#define ERR_SETBP_NOT_DURING_EXE                    4
#define ERR_SETBP_NO_HWBP                           5
#define ERR_SETBP_FAILED_SWBP                       6
#define ERR_SETBP_INVAL_TYPE                        7
#define ERR_CLEARBP_NOT_DURING_EXE                  8
#define ERR_FAILED_TO_INIT_PORT                     9
#define ERR_INITDEB_SDREQ_FAILED                    10
#define ERR_RESPROC_FAILED_TO_ENTER_DEBUG_MODE      11
#define ERR_STARTEXE_CORRUPT                        12
#define ERR_GETSTAT_CORRUPT                         13
#define ERR_SSTEP_CANT_SS_IN_MIPS16                 14
#define ERR_SSTEP_DIDNT_RETURN                      15
#define ERR_READREG_INVALID                         16
#define ERR_WRITEREG_INVALID                        17
#define ERR_READMEM_TOO_LARGE                       18
#define ERR_READMEM_ASSERT1                         19
#define ERR_READMEM_ASSERT2                         20
#define ERR_WRITEMEM_TOO_LARGE                      21
#define ERR_WRITEWPSAFE_UNALIGNED_START             22
#define ERR_WRITEWPFAST_UNALIGNED_START             23
#define ERR_WRITEWRDSTD_UNALIGNED_START             24
#define ERR_WRITEMWRDDMA_UNALIGNED_START            25
#define ERR_WRITEMWRDFDMA_UNALIGNED_START           26
#define ERR_READMWRDDMA_UNALIGNED_START             27
#define ERR_WRITEWRDDMA_UNALIGNED_START             28
#define ERR_READWRDDMA_UNALIGNED_START              29
#define ERR_READMWRDSTD_UNALIGNED_START             30
#define ERR_READWRDSTD_UNALIGNED_START              31
#define ERR_READWRDSTD_READFAILED1                  32
#define ERR_READWRDSTD_READFAILED2                  33
#define ERR_EXEINS_PROBE_NOT_ENABLED                34
#define ERR_WRITECP0_CORRUPT                        35
#define ERR_WRITECP0_INVALID                        36
#define ERR_READCP0_CORRUPT                         37
#define ERR_READCP0_INVALID                         38
#define ERR_DIOPEN_NO_TARGET                        39
#define ERR_DIMEMWRITE_NOT_DURING_EXE               40
#define ERR_DIMEMWRITE_TOO_LARGE                    41
#define ERR_DIMEMREAD_NOT_DURING_EXE                42
#define ERR_DIMEMREAD_TOO_LARGE                     43
#define ERR_DIREGWRITE_NOT_DURING_EXE               44
#define ERR_DIREGREAD_NOT_DURING_EXE                45
#define ERR_DIRESET_NO_TARGET                       46
#define ERR_DISSTEP_NOT_DURING_EXE                  47
#define ERR_DIEXECCONT_NOT_DURING_EXE               48
#define ERR_DIEXECSTOP_FAILED                       49
#define ERR_OPTION_NOT_IMPLEMENTED                  50
#define ERR_DIBPCLEARALL_NOT_DURING_EXE             51
#define ERR_GETBASESET_INVALID_EJTAG_VER            52
#define ERR_EXEDW2_RESET_OCCURRED                   53
#define ERR_EXEDW2_INVALID_ADDRESS                  54
#define ERR_MLGETSTAT_TARGET_RESET_OCCURRED         55
#define ERR_EXEDW2_NOT_IN_DEBUG_MODE                56
#define ERR_MLCHECKTARGT_NO_TARGET                  57
#define ERR_DISETUPERR_RESET_FAILED                 58
#define ERR_TXRX_SS_NOT_DURING_EXE                  59
#define ERR_TXRX_CONEXE_NOT_DURING_EXE              60
#define ERR_TXRX_HALT_FAILED                        61
#define ERR_NO_HW_BP_LEFT                           62
#define ERR_EXEDW2_LOCKUP                           63
#define ERR_EXEDW2_LOW_PWR                          64
#define ERR_EXEDW2_HALT_DETECTED                    65
#define ERR_EXEDW2_NO_ACCESS                        66
#define ERR_EXEDW2_PROBE_DISABLED                   67
#define ERR_EXEDW2_NORMAL_TRAP                      68
#define ERR_EXEDW2_BYTE_READ                        69
#define ERR_EXEDW2_HW_READ                          70
#define ERR_EXEDW2_TRIPPLE_READ                     71
#define ERR_EXEDW2_BYTE_WRITE                       72
#define ERR_EXEDW2_HW_WRITE                         73
#define ERR_LOADARRAY_CFG_NOT_FOUND                 74
#define ERR_LOADSREC_CANNOT_OPEN_FILE               75
#define ERR_INSTALLDW2_VERIFY_FAILED                76
#define ERR_INSTALLDW2_INVALID_APP                  77
#define ERR_INSTALLDW2_NOBACKUP                     78
#define ERR_INITMP_BADMODULE                        79
#define ERR_GDIOPEN_INVALID_PATHFINDER              80
#define ERR_LOADSREC_INVALID_SREC_FILE              81
#define ERR_TXRX_FAILED                             82
#define ERR_DW1_FILE_FAILED                         83
#define ERR_FPGA1_FILE_FAILED                       84
#define ERR_FPGA2_FILE_FAILED                       85
#define ERR_FPGA3_FILE_FAILED                       86
#define ERR_FPGA4_FILE_FAILED                       87
#define ERR_PORT_NOT_AVAILABLE                      88
#define ERR_CONNECT_FAILED                          89
#define ERR_DISCONNECT_FAILED                       90
#define ERR_TXRX_ETHER_FAILED                       91
#define ERR_ADDRESS_IN_USE                          92
#define ERR_USB_CONNECT_FAILED                      93
#define ERR_TXRX_USB_FAILED                         94
#define ERR_SETEND_CHANGE_FAILED                    95
#define ERR_CNVSREC_START_NOT_ALIGNED               96
#define ERR_CNVSREC_LEN_NOT_ALIGNED                 97
#define ERR_MDICON_INVALID_REVISION                 98
#define ERR_MDICON_TOO_MANY_CONNECTIONS             99
#define ERR_TLMEMRD_TOO_LARGE                      100
#define ERR_TLMEMRD_INVALID_SIZE                   101
#define ERR_TLMEMRD_INVALID_RES                    102
#define ERR_TLMEMWR_TOO_LARGE                      103
#define ERR_TLMEMWR_INVALID_SIZE                   104
#define ERR_TLMEMWR_INVALID_RES                    105
#define ERR_TLSTEP_NOT_SUPPORTED                   106
#define ERR_BUFWRITE_ATTEMPTED_LOW_WRITE           107
#define ERR_BUFWRITE_BUF_TOO_BIG                   108
#define ERR_BUFWRITE_ATTEMPTED_NONCONTG_WRITE      109
#define ERR_LSREC_SYM_NOT_FOUND                    110
#define ERR_LSREC_INVALID_REC                      111
#define ERR_GETDW2VER_UNSUPPORTED                  112
#define ERR_READETH_UNSUPPORTED                    113
#define ERR_RDMULWRD_UNALIGNED                     114
#define ERR_SETCBP_UNSUPPORTED                     115
#define ERR_EXEDW2_OUTOFBOUNDS                     116 
#define ERR_WRETH_UNSUPPORTED                      117
#define ERR_GINITCON_INVALID_PROTOCOL              118
#define ERR_GGETSTAT_OPTION_UNSUPPORTED            119
#define ERR_GREADREG_UNSUPPORTED                   120
#define ERR_GWRREG_UNSUPPORTED                     121
#define ERR_GMDIRD_UNSUPPORTED                     122
#define ERR_GMDIREAD_TOO_BIG                       123
#define ERR_GMDIWR_UNSUPPORTED                     124
#define ERR_GMDIWR_TOO_BIG                         125
#define ERR_WRMEM_UNSUPPORTED                      126
#define ERR_GTXRX_UNSUPPORTED                      127
#define ERR_GDISCON_UNSUPPORTED                    128
#define ERR_PROC_HELD_IN_RESET                     129
#define ERR_DMA_UNSUPPORTED                        130
#define ERR_RDMEM_EJTAG_UNALIGNED_START            131
#define ERR_RDMEM_EJTAG_INVALID_LENGTH             132
#define ERR_WRMEM_EJTAG_UNALIGNED_START            133
#define ERR_WRMEM_EJTAG_INVALID_LENGTH             134
#define ERR_FAILED_TO_LOAD_FPGA_EJTAG20            135
#define ERR_FAILED_TO_LOAD_FPGA_EJTAG26            136
#define ERR_LOAD_FPGA_FAILURE                      137
#define ERR_INVALID_COMMAND                        138
#define ERR_SSTEPOVER_TIMEOUT                      139
#define ERR_SSTEPFWD_TIMEOUT                       140
#define ERR_ADOC_DETECTED                          141
#define ERR_MOJO_DETECTED                          142
#define ERR_PROC_HELD_IN_RPMODE                    143
#define ERR_FAILED_TO_LOAD_FPGA_EJTAG20E           144
#define ERR_TIMEOUT_FEEDINGDATA                    145
#define ERR_BACK_AT_DEVECTOR                       146
#define ERR_UNABLE_TO_RETTODEVECTOR                147
#define ERR_TIMEOUT_RECEIVINGDATA                  148
#define ERR_ANALERR_RESET_OCCURRED                 149
#define ERR_ANALERR_NOT_IN_DEBUG_MODE              150
#define ERR_ANALERR_LOW_PWR                        151
#define ERR_ANALERR_HALT_DETECTED                  152
#define ERR_ANALERR_PROBE_DISABLED                 153
#define ERR_ANALERR_NORMAL_TRAP                    154
#define ERR_ANALERR_BYTE_READ                      155
#define ERR_ANALERR_HW_READ                        156
#define ERR_ANALERR_TRIPPLE_READ                   157
#define ERR_ANALERR_BYTE_WRITE                     158
#define ERR_ANALERR_HW_WRITE                       159
#define ERR_ANALERR_INVALIDCONTROL                 160
#define ERR_MEMWRITE_BREAKS_CACHEROUTINE           161
#define ERR_INSTALLDW2_INSUFFICIENTMEM             162
#define ERR_READWDFAST_UNALIGNED_START             163
#define ERR_UNABLE_TO_MOD_CACHE_INS_BIG            164
#define ERR_BAD_CACHE_INS_BIG                      165
#define ERR_UNABLE_TO_MOD_CACHE_INS_LITTLE         166
#define ERR_BAD_CACHE_INS_LITTLE                   167
#define ERR_NO_BIG_ENDIAN_SREC_AVAILABLE           168
#define ERR_NO_LITTLE_ENDIAN_SREC_AVAILABLE        169
#define ERR_CANT_RESTORE_CACHE_INS                 170
#define ERR_LOADDW2_NOMEM                          171
#define ERR_ALLOCBPS_NO_MEM                        172
#define ERR_SETBP_INSUFFICIENT_SWBPS               173
#define ERR_WMEMCRITICAL_BAD_ADDR_BIG              174
#define ERR_WMEMCRITICAL_BAD_ADDR_LITTLE           175
#define ERR_RMEMCRITICAL_BAD_ADDR_BIG              176
#define ERR_RMEMCRITICAL_BAD_ADDR_LITTLE           177
#define ERR_ANALERR_PERRESET                       178 
#define ERR_ANALERR_NO_ACCESS_PENDING              179
#define ERR_ANALERR_TW_WRITE                       180
#define ERR_ANALERR_INVALID                        181
#define ERR_ANALERR_STOP                           182
#define ERR_WMRF_NO_LITTLE_ENDIAN_SREC_AVAILABLE   183
#define ERR_WMRF_NO_BIG_ENDIAN_SREC_AVAILABLE      184
#define ERR_INCORRECT_USB_DRIVER                   185
#define ERR_FAILED_TO_PROGRAM_FPGA                 186
#define ERR_NO_USB_DEVICE_CONNECTED                187
#define ERR_FAILED_SEND_USB_PACKET                 188
#define ERR_FAILED_RECEIVE_USB_PACKET              189
#define ERR_FAILED_TO_WRITE_USB_MEMORY             190
#define ERR_OPELLA_FAILURE                         191
#define ERR_FAILED_TO_FIND_FPGA_FILE               192
#define ERR_FAILED_TO_FIND_DISKWARE_FILE           193
#define ERR_INCORRECT_DISKWARE_FILE                194
#define ERR_ASHPORT_FAILED_TO_START                195
#define ERR_FAILED_TO_CONFIG_PP_PORT               196
#define ERR_TARGET_RESET                           197
#define ERR_FAILED_TO_SEE_OPELLA_USB               198
#define ERR_CLRBP_FAILED_SWBP                      199
#define ERR_FAILED_TO_LOAD_FPGA_GENERIC            200
#define ERR_INSUFFICIENT_PC_MEMORY_FOR_SWAP        201
#define ERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE        202
#define ERR_OPXD_ALREADY_IN_USE                    203
#define ERR_ZEPHYR_TRACE_FETCH                     204
#define ERR_ZEPHYR_INIT_TRACE_DISASSEMBLER         205
#define ERR_ZEPHYR_TRACE_DECODER                   206
#define ERR_ZEPHYR_TRACE_CONFIG                    207
#define ERR_ZEPHYR_TRACE_DISASSEMBLER              208
#define ERR_LAST_ERROR                             209

/****************************************************************************
  Description: ERROR String Tables
****************************************************************************/
//Error messages are common for PathFinder and GDB Server because no seperate MDI for each.

#define GENSTR_NOT_ALLOWED_IN_DDC \
"Command not allowed while processor is executing."
      
#define GENSTR_FAILED_TO_SEE_OPELLA_PPT \
"PC unable to communicate with Opella-XD. \
Please ensure your Opella-XD is powered, connected to your PC�s Parallel Port and \
PathFinder and your Parallel Port are configured correctly."

#define GENSTR_FAILED_TO_SEE_TARGET \
"Unable to communicate with your target's EJTAG debug port. \
Debugger software can communicate with the Opella-XD, however, \
it cannot initialise your target's EJTAG debug port. \
Please ensure Opella-XD is connected properly to the PC and \
your target system and that your debugger software is configured correctly."

#define GENSTR_FAILED_TO_ENTER_DEBUG_MODE \
"Debugger software unable to put the target into debug mode. \
Please check your debugger software is configured correctly and \
try with a lower EJTAG frequency."

#define GENSTR_TARGET_COMMUNICATIONS_FAILURE \
"PathFinder is unable to communicate with your target�s EJTAG debug port \
(communication was initially established but has now failed). \
Please ensure your debugger is connected to your target and your target is powered up."

#define GENSTR_UNABLE_TO_CONFIGURE_PPORT \
"Cannot configure Parallel Port.\
 Ensure Correct Parallel Port Settings and if Using NT,\
 ensure Ashport Service is running"

#define GENSTR_FAILED_TO_RESET_AND_ENABLE_DEBUG_MODE \
"Failed to Reset Processor and Enable Debug Mode.\
 Check Target Configuration and Ensure Target is powered"

#define GENSTR_UNABLE_TO_SINGLE_STEP_IN_MIPS16 \
"An attempt was made to Single Step in MIPS16 Mode.\
 \nNote: You cannot start execution if there\
 is a breakpoint at the current PC"

#define GENSTR_UNKNOWN_ERROR \
"Unknown MIPS Error"

#define GENSTR_INTERNAL_ERROR \
"Internal Error: Please report this error to MIPS@ASHLING.COM"


#define GENSTR_FAILED_TO_SETBP \
"Failed to Set Software Breakpoint !\
 Perhaps this is a ROM Address. If so please use Hardware Breakpoints"

#define GENSTR_INVALID_EJTAGVER \
"Target core has an unknown/invalid EJTAG version. Verify your emulator is \
connected properly to the target and that the EJTAG frequency is not set too high."

#define GENSTR_TGTRESET_DETECTED \
"A possible Target Reset has been detected. \
 Please reset your MIPS target using the reset button in PathFinder to ensure \
 reliable target communications"

#define GENSTR_HALT_FAILED \
"Fatal Error Occurred while attempting to halt\
 processor"

#define GENSTR_TO_FEW_HWBP \
"Unable to Set Hardware Breakpoint. Insufficient Hardware\
 Breakpoint Resources."

#define GENSTR_SSTEP_DIDNT_RET \
"SingleStep Operation Failed. \
 Timeout waiting for processor to halt execution."

#define GENSTR_LOADARRAY_CFG_NOT_FOUND \
"Error loading Memory Protection Range Configuration File. \
 Please check if file <Ranges.cfg> exists"

#define GENSTR_LOADSREC_CANNOT_OPEN_FILE \
"Error Loading Cache Invalidation File. \
  Please contact your software vendor"

#define GENSTR_INSTALLDW2_VERIFY_FAILED \
"Error : Verify failed while loading Cache Invalidation File. \
  Please ensure that there is valid target memory at the location specified in the configuration dialog \
  and that your application is not corrupting this memory"

#define GENSTR_GDIOPEN_INVALID_PATHFINDER \
" PathFinder Version Conflict. This version of PathFinder is not supported by Vitra.dll"

#define GENSTR_TXRX_FAILED       \
"MDI Error: The PC is unable to receive from the emulator.\nCheck that the cable is connected and that the emulator is on."

#define GENSTR_DW1_FILE_FAILED   \
"MDI Error: Cannot find or open file 'VITRA.DW1'"

#define GENSTR_FPGA1_FILE_FAILED \
"MDI Error: Cannot find or open file 'VITRA1.ABF'"

#define GENSTR_FPGA2_FILE_FAILED \
"MDI Error: Cannot find or open file 'VITRA2.ABF'"

#define GENSTR_FPGA3_FILE_FAILED \
"MDI Error: Cannot find or open file 'VITRA3.ABF'"

#define GENSTR_FPGA4_FILE_FAILED \
"MDI Error: Cannot find or open file 'VITRA4.ABF'"

#define GENSTR_PORT_NOT_AVAILABLE \
"MDI Error: Specified serial port is not available"

#define GENSTR_CONNECT_FAILED    \
"MDI Error: Failed to connect to Vitra. Ethernet socket error detected."

#define GENSTR_DISCONNECT_FAILED \
"MDI Error: Ethernet socket error detected on closing"

#define GENSTR_TXRX_ETHER_FAILED \
"MDI Error: The PC is unable to receive from the emulator.\nCheck that the cable is connected and that the emulator is on."

#define GENSTR_ADDRESS_IN_USE \
"MDI Error: IP Address specified is already in use"
                             
#define GENSTR_USB_CONNECT_FAILED \
"Failed to connect to Vitra/Genia. USB error detected"

#define GENSTR_TXRX_USB_FAILED \
"MDI Error: The PC is unable to receive from the emulator.\nCheck that the USB cable is connected and that the emulator is on."

#define GENSTR__SETEND_CHANGE_FAILED \
"Change in Endian Mode Detected. Failed to restore previous Endian Mode. Unable to Use Cache Flush Routine."

#define GENSTR_PROC_HELD_IN_RESET \
"Cannot halt processor while it is being held in reset. \nPlease remove the reset condition and try again."

#define GENSTR_DMA_UNSUPPORTED \
"DMA mode not supported. DMA mode required to Read and Write using a Physical address."

#define GENSTR_TGTCOMM_SILCON_PROB \
"A Fatal Target communications failure has occurred. \
 Please ensure that your target is still powered and connected. \
 Also if using N1 ADOC silicon please upgrade to an N2 device to fix \
 known problems with the EJTAG module. Other possible causes include corruption of Cache Invalidation routine \
 (this can be debugged using the verify option in the configuration dialog), \
 and processor bus lockup due to infinite timeout on PI-BUS. "
 
#define GENSTR_FAILED_TO_LOAD_FPGA_EJTAG20 \
"Error Loading Trace FPGA: VTMIPE20.ABF"
                               
#define GENSTR_FAILED_TO_LOAD_FPGA_EJTAG20E \
"Error Loading Trace FPGA: VTMIPE2E.ABF"
                                        
#define GENSTR_FAILED_TO_LOAD_FPGA_EJTAG26 \
"Error Loading Trace FPGA: VTMIPE26.ABF"

#define GENSTR_FAILED_TO_LOAD_FPGA_GENERIC \
"Error Loading Trace FPGA"
                               
#define GENSTR_LOAD_FPGA_FAILURE \
"Internal Error: Failed to Load Vitra FPGA. Possible cause: Insufficient Genia Memory. Please report to mips@ashling.com"

#define GENSTR_INVALID_COMMAND \
"Internal Error: Unimplemented Diskware command. Please report to mips@ashling.com"

#define GENSTR_SSTEPOVER_TIMEOUT \
"Step Over Operation Failed. \
 Timeout waiting for processor to halt execution."

#define GENSTR_SSTEPFWD_TIMEOUT \
"Step Forward Operation Failed. \
 Timeout waiting for processor to halt execution."

#define GENSTR_ADOC_DETECTED \
"Error ADOC Device Detected, Please Configure for Philips ADOC."

#define GENSTR_MOJO_DETECTED \
"Error MOJO Device Detected, Please Configure for Philips MOJO."

#define GENSTR_PROC_HELD_IN_RPMODE \
"Error cannot the halt processor as it is being held in reduced power mode. Please issue a reset to remove the reduced power condition."

#define GENSTR_MEMWRITE_BREAKS_CACHEROUTINE  \
" An attempted Memory Write would have overwritten the Cache Invalidation Routine and was aborted, please check your settings."

#define GENSTR_INCORRECT_USB_DRIVER \
" Incorrect USB Driver. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_FAILED_TO_PROGRAM_FPGA \
" Failed to program FPGA. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_NO_USB_DEVICE_CONNECTED \
"Cannot communicate with Opella-XD. Please ensure Opella-XD is connected properly to the PC and your target system and that your debugger software is configured correctly."
#define GENSTR_FAILED_SEND_USB_PACKET \
" USB communications error. Failed to Transmit USB packet. Please ensure that your Opella-XD is connected and powered."
#define GENSTR_FAILED_RECEIVE_USB_PACKET \
" USB communications error. Failed to Reveive USB packet. Please ensure that your Opella-XD is connected and powered."
#define GENSTR_FAILED_TO_WRITE_USB_MEMORY \
" Failed to write USB memory. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_OPELLA_FAILURE \
" Opella-XD Failure. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_FAILED_TO_FIND_FPGA_FILE \
" Failed to find FPGA file. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_FAILED_TO_FIND_DISKWARE_FILE \
" Failed to find diskware file. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_INCORRECT_DISKWARE_FILE \
" Incorrect Diskware File diskware file. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_ASHPORT_FAILED_TO_START \
" AshPort Failed to Start. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_FAILED_TO_CONFIG_PP_PORT \
" Failed to configure Parallel Port. Please contact Ashling Support using the e-mail address mips@ashling.com"
#define GENSTR_TARGET_RESET \
" Target Reset. Please contact Ashling Support using the e-mail address mips@ashling.com"

#define GENSTR_FAILED_TO_SEE_OPELLA_USB \
"PC unable to communicate with Opella-XD. \
Please ensure your Opella-XD is powered, connected to your PC's USB port and \
PathFinder is configured correctly."

#define GENSTR_CLRBP_FAILED_SWBP \
"Failed to clear software breakpoint"

#define GENSTR_OUTSIDE_VALID_DMA_ADDRESS_RANGE \
"Not performing DMA memory access as address is outside valid DMA addreess range."

#define GENSTR_OPXD_ALREADY_IN_USE \
"The specified Opella-XD is already in use."

/********************************************
ZEPHYR TRACE RELATED ERROR MESSAGES
********************************************/

#define GENSTR_ZEPHYR_TRACE_FETCH \
"Error occurred while fetching raw trace."

#define GENSTR_ZEPHYR_TRACE_INIT_DISASSEMBLER \
"Error occurred while initializing disassembler."

#define GENSTR_ZEPHYR_TRACE_DECODER \
"Error occurred while decoding raw trace."

#define GENSTR_ZEPHYR_TRACE_CONFIG \
"Error occurred while configuring target for trace."

#define GENSTR_ZEPHYR_TRACE_DISASSEMBLER \
"Error occurred while disassembling raw trace."
                                                               
                                                  
/****************************************************************************
  Description: Each Error must have a defined string from the general Error
  descriptions
****************************************************************************/

#define STRERR_NO_ERROR                                  ""
#define STRERR_UNKNOWN_ERROR                             GENSTR_UNKNOWN_ERROR   
#define STRERR_FAILED_TO_SEE_OPELLA_PPT                  GENSTR_FAILED_TO_SEE_OPELLA_PPT 
#define STRERR_SETBP_NOBP                                GENSTR_FAILED_TO_SETBP 
#define STRERR_SETBP_NOT_DURING_EXE                      GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_SETBP_NO_HWBP                             GENSTR_INTERNAL_ERROR 
#define STRERR_SETBP_FAILED_SWBP                         GENSTR_FAILED_TO_SETBP 
#define STRERR_SETBP_INVAL_TYPE                          GENSTR_INTERNAL_ERROR 
#define STRERR_CLEARBP_NOT_DURING_EXE                    GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_FAILED_TO_INIT_PORT                       GENSTR_FAILED_TO_SEE_OPELLA_PPT 
#define STRERR_INITDEB_SDREQ_FAILED                      GENSTR_FAILED_TO_SEE_TARGET 
#define STRERR_RESPROC_FAILED_TO_ENTER_DEBUG_MODE        GENSTR_FAILED_TO_ENTER_DEBUG_MODE 
#define STRERR_STARTEXE_CORRUPT                          GENSTR_INTERNAL_ERROR 
#define STRERR_GETSTAT_CORRUPT                           GENSTR_INTERNAL_ERROR 
#define STRERR_SSTEP_CANT_SS_IN_MIPS16                   GENSTR_UNABLE_TO_SINGLE_STEP_IN_MIPS16 
#define STRERR_SSTEP_DIDNT_RETURN                        GENSTR_SSTEP_DIDNT_RET
#define STRERR_READREG_INVALID                           GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEREG_INVALID                          GENSTR_INTERNAL_ERROR 
#define STRERR_READMEM_TOO_LARGE                         GENSTR_INTERNAL_ERROR 
#define STRERR_READMEM_ASSERT1                           GENSTR_INTERNAL_ERROR 
#define STRERR_READMEM_ASSERT2                           GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEMEM_TOO_LARGE                        GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEWPSAFE_UNALIGNED_START               GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEWPFAST_UNALIGNED_START               GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEWRDSTD_UNALIGNED_START               GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEMWRDDMA_UNALIGNED_START              GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEMWRDFDMA_UNALIGNED_START             GENSTR_INTERNAL_ERROR 
#define STRERR_READMWRDDMA_UNALIGNED_START               GENSTR_INTERNAL_ERROR 
#define STRERR_WRITEWRDDMA_UNALIGNED_START               GENSTR_INTERNAL_ERROR 
#define STRERR_READWRDDMA_UNALIGNED_START                GENSTR_INTERNAL_ERROR 
#define STRERR_READMWRDSTD_UNALIGNED_START               GENSTR_INTERNAL_ERROR 
#define STRERR_READWRDSTD_UNALIGNED_START                GENSTR_INTERNAL_ERROR 
#define STRERR_READWRDSTD_READFAILED1                    GENSTR_TARGET_COMMUNICATIONS_FAILURE 
#define STRERR_READWRDSTD_READFAILED2                    GENSTR_TARGET_COMMUNICATIONS_FAILURE 
#define STRERR_EXEINS_PROBE_NOT_ENABLED                  GENSTR_TARGET_COMMUNICATIONS_FAILURE 
#define STRERR_WRITECP0_CORRUPT                          GENSTR_INTERNAL_ERROR 
#define STRERR_WRITECP0_INVALID                          GENSTR_INTERNAL_ERROR 
#define STRERR_READCP0_CORRUPT                           GENSTR_INTERNAL_ERROR 
#define STRERR_READCP0_INVALID                           GENSTR_INTERNAL_ERROR 
#define STRERR_DIOPEN_NO_TARGET                          GENSTR_TARGET_COMMUNICATIONS_FAILURE 
#define STRERR_DIMEMWRITE_NOT_DURING_EXE                 GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_DIMEMWRITE_TOO_LARGE                      GENSTR_INTERNAL_ERROR 
#define STRERR_DIMEMREAD_NOT_DURING_EXE                  GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_DIMEMREAD_TOO_LARGE                       GENSTR_INTERNAL_ERROR 
#define STRERR_DIREGWRITE_NOT_DURING_EXE                 GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_DIREGREAD_NOT_DURING_EXE                  GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_DIRESET_NO_TARGET                         GENSTR_TARGET_COMMUNICATIONS_FAILURE 
#define STRERR_DISSTEP_NOT_DURING_EXE                    GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_DIEXECCONT_NOT_DURING_EXE                 GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_DIEXECSTOP_FAILED                         GENSTR_TARGET_COMMUNICATIONS_FAILURE 
#define STRERR_OPTION_NOT_IMPLEMENTED                    GENSTR_INTERNAL_ERROR 
#define STRERR_DIBPCLEARALL_NOT_DURING_EXE               GENSTR_NOT_ALLOWED_IN_DDC 
#define STRERR_GETBASESET_INVALID_EJTAG_VER              GENSTR_INVALID_EJTAGVER 
#define STRERR_EXEDW2_RESET_OCCURRED                     GENSTR_TGTRESET_DETECTED 
#define STRERR_EXEDW2_INVALID_ADDRESS                    GENSTR_INTERNAL_ERROR  
#define STRERR_MLGETSTAT_TARGET_RESET_OCCURRED           GENSTR_TGTRESET_DETECTED 
#define STRERR_EXEDW2_NOT_IN_DEBUG_MODE                  GENSTR_TARGET_COMMUNICATIONS_FAILURE  
#define STRERR_MLCHECKTARGT_NO_TARGET                    GENSTR_FAILED_TO_SEE_TARGET  
#define STRERR_DISETUPERR_RESET_FAILED                   GENSTR_INTERNAL_ERROR  
#define STRERR_TXRX_SS_NOT_DURING_EXE                    GENSTR_NOT_ALLOWED_IN_DDC
#define STRERR_TXRX_CONEXE_NOT_DURING_EXE                GENSTR_NOT_ALLOWED_IN_DDC
#define STRERR_TXRX_HALT_FAILED                          GENSTR_HALT_FAILED
#define STRERR_NO_HW_BP_LEFT                             GENSTR_TO_FEW_HWBP
#define STRERR_EXEDW2_LOCKUP                             GENSTR_TGTCOMM_SILCON_PROB 
#define STRERR_EXEDW2_LOW_PWR                            GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_HALT_DETECTED                      GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_NO_ACCESS                          GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_PROBE_DISABLED                     GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_NORMAL_TRAP                        GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_BYTE_READ                          GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_HW_READ                            GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_TRIPPLE_READ                       GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_BYTE_WRITE                         GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_HW_WRITE                           GENSTR_INTERNAL_ERROR
#define STRERR_LOADARRAY_CFG_NOT_FOUND                   GENSTR_LOADARRAY_CFG_NOT_FOUND
#define STRERR_LOADSREC_CANNOT_OPEN_FILE                 GENSTR_LOADSREC_CANNOT_OPEN_FILE
#define STRERR_INSTALLDW2_VERIFY_FAILED                  GENSTR_INSTALLDW2_VERIFY_FAILED
#define STRERR_INSTALLDW2_INVALID_APP                    GENSTR_INTERNAL_ERROR
#define STRERR_INSTALLDW2_NOBACKUP                       GENSTR_INTERNAL_ERROR
#define STRERR_INITMP_BADMODULE                          GENSTR_INTERNAL_ERROR
#define STRERR_GDIOPEN_INVALID_PATHFINDER                GENSTR_GDIOPEN_INVALID_PATHFINDER
#define STRERR_ERR_LOADSREC_INVALID_SREC_FILE            GENSTR_LOADSREC_CANNOT_OPEN_FILE
#define STRERR_TXRX_FAILED                               GENSTR_TXRX_FAILED       
#define STRERR_DW1_FILE_FAILED                           GENSTR_DW1_FILE_FAILED   
#define STRERR_FPGA1_FILE_FAILED                         GENSTR_FPGA1_FILE_FAILED 
#define STRERR_FPGA2_FILE_FAILED                         GENSTR_FPGA2_FILE_FAILED 
#define STRERR_FPGA3_FILE_FAILED                         GENSTR_FPGA3_FILE_FAILED 
#define STRERR_FPGA4_FILE_FAILED                         GENSTR_FPGA4_FILE_FAILED 
#define STRERR_PORT_NOT_AVAILABLE                        GENSTR_PORT_NOT_AVAILABLE
#define STRERR_CONNECT_FAILED                            GENSTR_CONNECT_FAILED    
#define STRERR_DISCONNECT_FAILED                         GENSTR_DISCONNECT_FAILED 
#define STRERR_TXRX_ETHER_FAILED                         GENSTR_TXRX_ETHER_FAILED 
#define STRERR_ADDRESS_IN_USE                            GENSTR_ADDRESS_IN_USE    
#define STRERR_USB_CONNECT_FAILED                        GENSTR_USB_CONNECT_FAILED 
#define STRERR_TXRX_USB_FAILED                           GENSTR_TXRX_USB_FAILED    
#define STRERR_SETEND_CHANGE_FAILED                      GENSTR_INTERNAL_ERROR
#define STRERR_CNVSREC_START_NOT_ALIGNED                 GENSTR_INTERNAL_ERROR 
#define STRERR_CNVSREC_LEN_NOT_ALIGNED                   GENSTR_INTERNAL_ERROR 
#define STRERR_MDICON_INVALID_REVISION                   GENSTR_INTERNAL_ERROR
#define STRERR_MDICON_TOO_MANY_CONNECTIONS               GENSTR_INTERNAL_ERROR
#define STRERR_TLMEMRD_TOO_LARGE                         GENSTR_INTERNAL_ERROR    
#define STRERR_TLMEMRD_INVALID_SIZE                      GENSTR_INTERNAL_ERROR 
#define STRERR_TLMEMRD_INVALID_RES                       GENSTR_INTERNAL_ERROR 
#define STRERR_TLMEMWR_TOO_LARGE                         GENSTR_INTERNAL_ERROR
#define STRERR_TLMEMWR_INVALID_SIZE                      GENSTR_INTERNAL_ERROR
#define STRERR_TLMEMWR_INVALID_RES                       GENSTR_INTERNAL_ERROR
#define STRERR_TLSTEP_NOT_SUPPORTED                      GENSTR_INTERNAL_ERROR
#define STRERR_BUFWRITE_ATTEMPTED_LOW_WRITE              GENSTR_INTERNAL_ERROR
#define STRERR_BUFWRITE_BUF_TOO_BIG                      GENSTR_INTERNAL_ERROR
#define STRERR_BUFWRITE_ATTEMPTED_NONCONTG_WRITE         GENSTR_INTERNAL_ERROR
#define STRERR_LSREC_SYM_NOT_FOUND                       GENSTR_INTERNAL_ERROR
#define STRERR_LSREC_INVALID_REC                         GENSTR_INTERNAL_ERROR
#define STRERR_GETDW2VER_UNSUPPORTED                     GENSTR_INTERNAL_ERROR
#define STRERR_READETH_UNSUPPORTED                       GENSTR_INTERNAL_ERROR
#define STRERR_RDMULWRD_UNALIGNED                        GENSTR_INTERNAL_ERROR
#define STRERR_SETCBP_UNSUPPORTED                        GENSTR_INTERNAL_ERROR
#define STRERR_EXEDW2_OUTOFBOUNDS                        GENSTR_TGTCOMM_SILCON_PROB
#define STRERR_WRETH_UNSUPPORTED                         GENSTR_INTERNAL_ERROR
#define STRERR_GINITCON_INVALID_PROTOCOL                 GENSTR_INTERNAL_ERROR
#define STRERR_GGETSTAT_OPTION_UNSUPPORTED               GENSTR_INTERNAL_ERROR
#define STRERR_GREADREG_UNSUPPORTED                      GENSTR_INTERNAL_ERROR
#define STRERR_GWRREG_UNSUPPORTED                        GENSTR_INTERNAL_ERROR
#define STRERR_GMDIRD_UNSUPPORTED                        GENSTR_INTERNAL_ERROR
#define STRERR_GMDIREAD_TOO_BIG                          GENSTR_INTERNAL_ERROR
#define STRERR_GMDIWR_UNSUPPORTED                        GENSTR_INTERNAL_ERROR
#define STRERR_GMDIWR_TOO_BIG                            GENSTR_INTERNAL_ERROR
#define STRERR_WRMEM_UNSUPPORTED                         GENSTR_INTERNAL_ERROR
#define STRERR_GTXRX_UNSUPPORTED                         GENSTR_INTERNAL_ERROR
#define STRERR_GDISCON_UNSUPPORTED                       GENSTR_INTERNAL_ERROR
#define STRERR_PROC_HELD_IN_RESET                        GENSTR_PROC_HELD_IN_RESET
#define STRERR_DMA_UNSUPPORTED                           GENSTR_DMA_UNSUPPORTED
#define STRERR_RDMEM_EJTAG_UNALIGNED_START               GENSTR_INTERNAL_ERROR
#define STRERR_RDMEM_EJTAG_INVALID_LENGTH                GENSTR_INTERNAL_ERROR
#define STRERR_WRMEM_EJTAG_UNALIGNED_START               GENSTR_INTERNAL_ERROR
#define STRERR_WRMEM_EJTAG_INVALID_LENGTH                GENSTR_INTERNAL_ERROR
#define STRERR_FAILED_TO_LOAD_FPGA_EJTAG20               GENSTR_FAILED_TO_LOAD_FPGA_EJTAG20
#define STRERR_FAILED_TO_LOAD_FPGA_EJTAG26               GENSTR_FAILED_TO_LOAD_FPGA_EJTAG26
#define STRERR_LOAD_FPGA_FAILURE                         GENSTR_LOAD_FPGA_FAILURE
#define STRERR_INVALID_COMMAND                           GENSTR_INVALID_COMMAND  
#define STRERR_SSTEPOVER_TIMEOUT                         GENSTR_SSTEPOVER_TIMEOUT  
#define STRERR_SSTEPFWD_TIMEOUT                          GENSTR_SSTEPFWD_TIMEOUT   
#define STRERR_ADOC_DETECTED                             GENSTR_ADOC_DETECTED
#define STRERR_MOJO_DETECTED                             GENSTR_MOJO_DETECTED
#define STRERR_PROC_HELD_IN_RPMODE                       GENSTR_PROC_HELD_IN_RPMODE
#define STRERR_FAILED_TO_LOAD_FPGA_EJTAG20E              GENSTR_FAILED_TO_LOAD_FPGA_EJTAG20E
#define STRERR_TIMEOUT_FEEDINGDATA                       GENSTR_INTERNAL_ERROR
#define STRERR_BACK_AT_DEVECTOR                          GENSTR_INTERNAL_ERROR
#define STRERR_UNABLE_TO_RETTODEVECTOR                   GENSTR_INTERNAL_ERROR
#define STRERR_TIMEOUT_RECEIVINGDATA                     GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_RESET_OCCURRED                    GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_NOT_IN_DEBUG_MODE                 GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_LOW_PWR                           GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_HALT_DETECTED                     GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_PROBE_DISABLED                    GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_NORMAL_TRAP                       GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_BYTE_READ                         GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_HW_READ                           GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_TRIPPLE_READ                      GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_BYTE_WRITE                        GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_HW_WRITE                          GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_INVALIDCONTROL                    GENSTR_INTERNAL_ERROR
#define STRERR_MEMWRITE_BREAKS_CACHEROUTINE              GENSTR_MEMWRITE_BREAKS_CACHEROUTINE
#define STRERR_INSTALLDW2_INSUFFICIENTMEM                GENSTR_INTERNAL_ERROR
#define STRERR_READWDFAST_UNALIGNED_START                GENSTR_INTERNAL_ERROR
#define STRERR_UNABLE_TO_MOD_CACHE_INS_BIG               GENSTR_INTERNAL_ERROR
#define STRERR_BAD_CACHE_INS_BIG                         GENSTR_INTERNAL_ERROR
#define STRERR_UNABLE_TO_MOD_CACHE_INS_LITTLE            GENSTR_INTERNAL_ERROR
#define STRERR_BAD_CACHE_INS_LITTLE                      GENSTR_INTERNAL_ERROR
#define STRERR_NO_BIG_ENDIAN_SREC_AVAILABLE              GENSTR_INTERNAL_ERROR
#define STRERR_NO_LITTLE_ENDIAN_SREC_AVAILABLE           GENSTR_INTERNAL_ERROR
#define STRERR_CANT_RESTORE_CACHE_INS                    GENSTR_INTERNAL_ERROR
#define STRERR_LOADDW2_NOMEM                             GENSTR_INTERNAL_ERROR
#define STRERR_ALLOCBPS_NO_MEM                           GENSTR_INTERNAL_ERROR
#define STRERR_SETBP_INSUFFICIENT_SWBPS                  GENSTR_INTERNAL_ERROR
#define STRERR_WMEMCRITICAL_BAD_ADDR_BIG                 GENSTR_INTERNAL_ERROR
#define STRERR_WMEMCRITICAL_BAD_ADDR_LITTLE              GENSTR_INTERNAL_ERROR
#define STRERR_RMEMCRITICAL_BAD_ADDR_BIG                 GENSTR_INTERNAL_ERROR
#define STRERR_RMEMCRITICAL_BAD_ADDR_LITTLE              GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_PERRESET                          GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_NO_ACCESS_PENDING                 GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_TW_WRITE                          GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_INVALID                           GENSTR_INTERNAL_ERROR
#define STRERR_ANALERR_STOP                              GENSTR_INTERNAL_ERROR
#define STRERR_WMRF_NO_LITTLE_ENDIAN_SREC_AVAILABLE      GENSTR_INTERNAL_ERROR
#define STRERR_WMRF_NO_BIG_ENDIAN_SREC_AVAILABLE         GENSTR_INTERNAL_ERROR
#define STRERR_INCORRECT_USB_DRIVER                      GENSTR_INCORRECT_USB_DRIVER        
#define STRERR_FAILED_TO_PROGRAM_FPGA                    GENSTR_FAILED_TO_PROGRAM_FPGA      
#define STRERR_NO_USB_DEVICE_CONNECTED                   GENSTR_NO_USB_DEVICE_CONNECTED     
#define STRERR_FAILED_SEND_USB_PACKET                    GENSTR_FAILED_SEND_USB_PACKET      
#define STRERR_FAILED_RECEIVE_USB_PACKET                 GENSTR_FAILED_RECEIVE_USB_PACKET   
#define STRERR_FAILED_TO_WRITE_USB_MEMORY                GENSTR_FAILED_TO_WRITE_USB_MEMORY  
#define STRERR_OPELLA_FAILURE                            GENSTR_OPELLA_FAILURE              
#define STRERR_FAILED_TO_FIND_FPGA_FILE                  GENSTR_FAILED_TO_FIND_FPGA_FILE    
#define STRERR_FAILED_TO_FIND_DISKWARE_FILE              GENSTR_FAILED_TO_FIND_DISKWARE_FILE
#define STRERR_INCORRECT_DISKWARE_FILE                   GENSTR_INCORRECT_DISKWARE_FILE     
#define STRERR_ASHPORT_FAILED_TO_START                   GENSTR_ASHPORT_FAILED_TO_START     
#define STRERR_FAILED_TO_CONFIG_PP_PORT                  GENSTR_FAILED_TO_CONFIG_PP_PORT    
#define STRERR_TARGET_RESET                              GENSTR_TARGET_RESET                
#define STRERR_FAILED_TO_SEE_OPELLA_USB                  GENSTR_FAILED_TO_SEE_OPELLA_USB 
#define STRERR_CLRBP_FAILED_SWBP                         GENSTR_CLRBP_FAILED_SWBP
#define STRERR_FAILED_TO_LOAD_FPGA_GENERIC               GENSTR_FAILED_TO_LOAD_FPGA_GENERIC
#define STRERR_INSUFFICIENT_PC_MEMORY_FOR_SWAP           GENSTR_INTERNAL_ERROR
#define STRERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE           GENSTR_OUTSIDE_VALID_DMA_ADDRESS_RANGE
#define STRERR_OPXD_ALREADY_IN_USE                       GENSTR_OPXD_ALREADY_IN_USE
#define STRERR_ZEPHYR_TRACE_FETCH                        GENSTR_ZEPHYR_TRACE_FETCH
#define STRERR_ZEPHYR_TRACE_INIT_DISASSEMBLER            GENSTR_ZEPHYR_TRACE_INIT_DISASSEMBLER
#define STRERR_ZEPHYR_TRACE_DECODER                      GENSTR_ZEPHYR_TRACE_DECODER
#define STRERR_ZEPHYR_TRACE_CONFIG                       GENSTR_ZEPHYR_TRACE_CONFIG
#define STRERR_ZEPHYR_TRACE_DISASSEMBLER                 GENSTR_ZEPHYR_TRACE_DISASSEMBLER   
#define STRERR_LAST_ERROR                                ""  



/****************************************************************************
  Description: Typedefinition for Error Structure
****************************************************************************/

#ifdef  DEFINE_ERROR_STRUCT // Declared in Opelmips.cpp (opella) and gdilayer.cpp (vitra/genia)

typedef struct error 
{  
   char pszErrorString[MAX_ERROR_STRING];
   bool          bFatalJTAGError;
} TyErrorType;

#endif

/****************************************************************************
  Description: Now we create a table of Errors including string description
               and a flag to determine if a target reset is needed.
****************************************************************************/

// This Array is only defined once
#ifdef  DEFINE_ERROR_STRUCT

  // Create Array of Error Structures

  TyErrorType tyError[]=
  { 
     {STRERR_NO_ERROR                              ,0},
     {STRERR_UNKNOWN_ERROR                         ,0},
     {STRERR_FAILED_TO_SEE_OPELLA_PPT              ,1},
     {STRERR_SETBP_NOBP                            ,0},
     {STRERR_SETBP_NOT_DURING_EXE                  ,0},
     {STRERR_SETBP_NO_HWBP                         ,0},
     {STRERR_SETBP_FAILED_SWBP                     ,0},
     {STRERR_SETBP_INVAL_TYPE                      ,0},
     {STRERR_CLEARBP_NOT_DURING_EXE                ,0},
     {STRERR_FAILED_TO_INIT_PORT                   ,0},
     {STRERR_INITDEB_SDREQ_FAILED                  ,0},
     {STRERR_RESPROC_FAILED_TO_ENTER_DEBUG_MODE    ,1},
     {STRERR_STARTEXE_CORRUPT                      ,0},
     {STRERR_GETSTAT_CORRUPT                       ,0},
     {STRERR_SSTEP_CANT_SS_IN_MIPS16               ,0},
     {STRERR_SSTEP_DIDNT_RETURN                    ,0},
     {STRERR_READREG_INVALID                       ,0},
     {STRERR_WRITEREG_INVALID                      ,0},
     {STRERR_READMEM_TOO_LARGE                     ,0},
     {STRERR_READMEM_ASSERT1                       ,0},
     {STRERR_READMEM_ASSERT2                       ,0},
     {STRERR_WRITEMEM_TOO_LARGE                    ,0},
     {STRERR_WRITEWPSAFE_UNALIGNED_START           ,0},
     {STRERR_WRITEWPFAST_UNALIGNED_START           ,0},
     {STRERR_WRITEWRDSTD_UNALIGNED_START           ,0},
     {STRERR_WRITEMWRDDMA_UNALIGNED_START          ,0},
     {STRERR_WRITEMWRDFDMA_UNALIGNED_START         ,0},
     {STRERR_READMWRDDMA_UNALIGNED_START           ,0},
     {STRERR_WRITEWRDDMA_UNALIGNED_START           ,0},
     {STRERR_READWRDDMA_UNALIGNED_START            ,0},
     {STRERR_READMWRDSTD_UNALIGNED_START           ,0},
     {STRERR_READWRDSTD_UNALIGNED_START            ,0},
     {STRERR_READWRDSTD_READFAILED1                ,0},
     {STRERR_READWRDSTD_READFAILED2                ,0},
     {STRERR_EXEINS_PROBE_NOT_ENABLED              ,1},
     {STRERR_WRITECP0_CORRUPT                      ,0},
     {STRERR_WRITECP0_INVALID                      ,0},
     {STRERR_READCP0_CORRUPT                       ,0},
     {STRERR_READCP0_INVALID                       ,0},
     {STRERR_DIOPEN_NO_TARGET                      ,0},
     {STRERR_DIMEMWRITE_NOT_DURING_EXE             ,0},
     {STRERR_DIMEMWRITE_TOO_LARGE                  ,0},
     {STRERR_DIMEMREAD_NOT_DURING_EXE              ,0},
     {STRERR_DIMEMREAD_TOO_LARGE                   ,0},
     {STRERR_DIREGWRITE_NOT_DURING_EXE             ,0},
     {STRERR_DIREGREAD_NOT_DURING_EXE              ,0},
     {STRERR_DIRESET_NO_TARGET                     ,0},
     {STRERR_DISSTEP_NOT_DURING_EXE                ,0},
     {STRERR_DIEXECCONT_NOT_DURING_EXE             ,0},
     {STRERR_DIEXECSTOP_FAILED                     ,0},
     {STRERR_OPTION_NOT_IMPLEMENTED                ,0},
     {STRERR_DIBPCLEARALL_NOT_DURING_EXE           ,0},
     {STRERR_GETBASESET_INVALID_EJTAG_VER          ,0},
     {STRERR_EXEDW2_RESET_OCCURRED                 ,1},
     {STRERR_EXEDW2_INVALID_ADDRESS                ,1},
     {STRERR_MLGETSTAT_TARGET_RESET_OCCURRED       ,0},
     {STRERR_EXEDW2_NOT_IN_DEBUG_MODE              ,1},
     {STRERR_MLCHECKTARGT_NO_TARGET                ,1},
     {STRERR_DISETUPERR_RESET_FAILED               ,0}, // NEVER SET bResetRequired for this Error
     {STRERR_TXRX_SS_NOT_DURING_EXE                ,0},
     {STRERR_TXRX_CONEXE_NOT_DURING_EXE            ,0},
     {STRERR_TXRX_HALT_FAILED                      ,0},
     {STRERR_NO_HW_BP_LEFT                         ,0},
     {STRERR_EXEDW2_LOCKUP                         ,1},
     {STRERR_EXEDW2_LOW_PWR                        ,1},
     {STRERR_EXEDW2_HALT_DETECTED                  ,1},
     {STRERR_EXEDW2_NO_ACCESS                      ,1},
     {STRERR_EXEDW2_PROBE_DISABLED                 ,1},
     {STRERR_EXEDW2_NORMAL_TRAP                    ,1},
     {STRERR_EXEDW2_BYTE_READ                      ,1},
     {STRERR_EXEDW2_HW_READ                        ,1},
     {STRERR_EXEDW2_TRIPPLE_READ                   ,1},
     {STRERR_EXEDW2_BYTE_WRITE                     ,1},
     {STRERR_EXEDW2_HW_WRITE                       ,1},
     {STRERR_LOADARRAY_CFG_NOT_FOUND               ,0},
     {STRERR_LOADSREC_CANNOT_OPEN_FILE             ,0},
     {STRERR_INSTALLDW2_VERIFY_FAILED              ,0},
     {STRERR_INSTALLDW2_INVALID_APP                ,0},
     {STRERR_INSTALLDW2_NOBACKUP                   ,0},
     {STRERR_INITMP_BADMODULE                      ,0},
     {STRERR_GDIOPEN_INVALID_PATHFINDER            ,0},
     {STRERR_LOADSREC_CANNOT_OPEN_FILE             ,0},
     {STRERR_TXRX_FAILED                           ,0},   
     {STRERR_DW1_FILE_FAILED                       ,0}, 
     {STRERR_FPGA1_FILE_FAILED                     ,0}, 
     {STRERR_FPGA2_FILE_FAILED                     ,0}, 
     {STRERR_FPGA3_FILE_FAILED                     ,0}, 
     {STRERR_FPGA4_FILE_FAILED                     ,0}, 
     {STRERR_PORT_NOT_AVAILABLE                    ,0}, 
     {STRERR_CONNECT_FAILED                        ,0}, 
     {STRERR_DISCONNECT_FAILED                     ,0}, 
     {STRERR_TXRX_ETHER_FAILED                     ,0}, 
     {STRERR_ADDRESS_IN_USE                        ,0}, 
     {STRERR_USB_CONNECT_FAILED                    ,0},
     {STRERR_TXRX_USB_FAILED                       ,0},
     {STRERR_SETEND_CHANGE_FAILED                  ,0},
     {STRERR_CNVSREC_START_NOT_ALIGNED             ,0},
     {STRERR_CNVSREC_LEN_NOT_ALIGNED               ,0},
     {STRERR_MDICON_INVALID_REVISION               ,0},
     {STRERR_MDICON_TOO_MANY_CONNECTIONS           ,0},
     {STRERR_TLMEMRD_TOO_LARGE                     ,0},
     {STRERR_TLMEMRD_INVALID_SIZE                  ,0},
     {STRERR_TLMEMRD_INVALID_RES                   ,0},
     {STRERR_TLMEMWR_TOO_LARGE                     ,0},
     {STRERR_TLMEMWR_INVALID_SIZE                  ,0},
     {STRERR_TLMEMWR_INVALID_RES                   ,0},
     {STRERR_TLSTEP_NOT_SUPPORTED                  ,0},
     {STRERR_BUFWRITE_ATTEMPTED_LOW_WRITE          ,0},
     {STRERR_BUFWRITE_BUF_TOO_BIG                  ,0},
     {STRERR_BUFWRITE_ATTEMPTED_NONCONTG_WRITE     ,0},
     {STRERR_LSREC_SYM_NOT_FOUND                   ,0},
     {STRERR_LSREC_INVALID_REC                     ,0},
     {STRERR_GETDW2VER_UNSUPPORTED                 ,0},
     {STRERR_READETH_UNSUPPORTED                   ,0},
     {STRERR_RDMULWRD_UNALIGNED                    ,0},
     {STRERR_SETCBP_UNSUPPORTED                    ,0},
     {STRERR_EXEDW2_OUTOFBOUNDS                    ,1},
     {STRERR_WRETH_UNSUPPORTED                     ,0},
     {STRERR_GINITCON_INVALID_PROTOCOL             ,0},
     {STRERR_GGETSTAT_OPTION_UNSUPPORTED           ,0},
     {STRERR_GREADREG_UNSUPPORTED                  ,0},
     {STRERR_GWRREG_UNSUPPORTED                    ,0},
     {STRERR_GMDIRD_UNSUPPORTED                    ,0},
     {STRERR_GMDIREAD_TOO_BIG                      ,0},
     {STRERR_GMDIWR_UNSUPPORTED                    ,0},
     {STRERR_GMDIWR_TOO_BIG                        ,0},
     {STRERR_WRMEM_UNSUPPORTED                     ,0},
     {STRERR_GTXRX_UNSUPPORTED                     ,0},
     {STRERR_GDISCON_UNSUPPORTED                   ,0},
     {STRERR_PROC_HELD_IN_RESET                    ,0},
     {STRERR_DMA_UNSUPPORTED                       ,0},
     {STRERR_RDMEM_EJTAG_UNALIGNED_START           ,0},
     {STRERR_RDMEM_EJTAG_INVALID_LENGTH            ,0},
     {STRERR_WRMEM_EJTAG_UNALIGNED_START           ,0},
     {STRERR_WRMEM_EJTAG_INVALID_LENGTH            ,0},
     {STRERR_FAILED_TO_LOAD_FPGA_EJTAG20           ,0},
     {STRERR_FAILED_TO_LOAD_FPGA_EJTAG26           ,0},
     {STRERR_LOAD_FPGA_FAILURE                     ,0},
     {STRERR_INVALID_COMMAND                       ,0},
     {STRERR_SSTEPOVER_TIMEOUT                     ,0}, 
     {STRERR_SSTEPFWD_TIMEOUT                      ,0}, 
     {STRERR_ADOC_DETECTED                         ,0}, 
     {STRERR_MOJO_DETECTED                         ,0}, 
     {STRERR_PROC_HELD_IN_RPMODE                   ,0},
     {STRERR_FAILED_TO_LOAD_FPGA_EJTAG20E          ,0},
     {STRERR_TIMEOUT_FEEDINGDATA                   ,1},
     {STRERR_BACK_AT_DEVECTOR                      ,1},
     {STRERR_UNABLE_TO_RETTODEVECTOR               ,1},
     {STRERR_TIMEOUT_RECEIVINGDATA                 ,1},
     {STRERR_ANALERR_RESET_OCCURRED                ,1},
     {STRERR_ANALERR_NOT_IN_DEBUG_MODE             ,1},
     {STRERR_ANALERR_LOW_PWR                       ,1},
     {STRERR_ANALERR_HALT_DETECTED                 ,1},
     {STRERR_ANALERR_PROBE_DISABLED                ,1},
     {STRERR_ANALERR_NORMAL_TRAP                   ,1},
     {STRERR_ANALERR_BYTE_READ                     ,1},
     {STRERR_ANALERR_HW_READ                       ,1},
     {STRERR_ANALERR_TRIPPLE_READ                  ,1},
     {STRERR_ANALERR_BYTE_WRITE                    ,1},
     {STRERR_ANALERR_HW_WRITE                      ,1},
     {STRERR_ANALERR_INVALIDCONTROL                ,1},
     {STRERR_MEMWRITE_BREAKS_CACHEROUTINE          ,0},
     {STRERR_INSTALLDW2_INSUFFICIENTMEM            ,0},
     {STRERR_READWDFAST_UNALIGNED_START            ,0},
     {STRERR_UNABLE_TO_MOD_CACHE_INS_BIG           ,0},
     {STRERR_BAD_CACHE_INS_BIG                     ,0},
     {STRERR_UNABLE_TO_MOD_CACHE_INS_LITTLE        ,0},
     {STRERR_BAD_CACHE_INS_LITTLE                  ,0},
     {STRERR_NO_BIG_ENDIAN_SREC_AVAILABLE          ,0},
     {STRERR_NO_LITTLE_ENDIAN_SREC_AVAILABLE       ,0},
     {STRERR_CANT_RESTORE_CACHE_INS                ,0},
     {STRERR_LOADDW2_NOMEM                         ,0},
     {STRERR_ALLOCBPS_NO_MEM                       ,0},
     {STRERR_SETBP_INSUFFICIENT_SWBPS              ,0},
     {STRERR_WMEMCRITICAL_BAD_ADDR_BIG             ,0},
     {STRERR_WMEMCRITICAL_BAD_ADDR_LITTLE          ,0},
     {STRERR_RMEMCRITICAL_BAD_ADDR_BIG             ,0},
     {STRERR_RMEMCRITICAL_BAD_ADDR_LITTLE          ,0},
     {STRERR_ANALERR_PERRESET                      ,0},   
     {STRERR_ANALERR_NO_ACCESS_PENDING             ,0},
     {STRERR_ANALERR_TW_WRITE                      ,0},
     {STRERR_ANALERR_INVALID                       ,0},
     {STRERR_ANALERR_STOP                          ,0},
     {STRERR_WMRF_NO_LITTLE_ENDIAN_SREC_AVAILABLE  ,0},
     {STRERR_WMRF_NO_BIG_ENDIAN_SREC_AVAILABLE     ,0},
     {STRERR_INCORRECT_USB_DRIVER                  ,0},
     {STRERR_FAILED_TO_PROGRAM_FPGA                ,0},
     {STRERR_NO_USB_DEVICE_CONNECTED               ,0},
     {STRERR_FAILED_SEND_USB_PACKET                ,0},
     {STRERR_FAILED_RECEIVE_USB_PACKET             ,0},
     {STRERR_FAILED_TO_WRITE_USB_MEMORY            ,0},
     {STRERR_OPELLA_FAILURE                        ,0},
     {STRERR_FAILED_TO_FIND_FPGA_FILE              ,0},
     {STRERR_FAILED_TO_FIND_DISKWARE_FILE          ,0},
     {STRERR_INCORRECT_DISKWARE_FILE               ,0},
     {STRERR_ASHPORT_FAILED_TO_START               ,0},
     {STRERR_FAILED_TO_CONFIG_PP_PORT              ,0},
     {STRERR_TARGET_RESET                          ,0},
     {STRERR_FAILED_TO_SEE_OPELLA_USB              ,1},
     {STRERR_CLRBP_FAILED_SWBP                     ,0},
     {STRERR_FAILED_TO_LOAD_FPGA_GENERIC           ,0},
     {STRERR_INSUFFICIENT_PC_MEMORY_FOR_SWAP       ,0},
     {STRERR_OUTSIDE_VALID_DMA_ADDRESS_RANGE       ,0},
     {STRERR_OPXD_ALREADY_IN_USE                   ,0},
     {STRERR_ZEPHYR_TRACE_FETCH                    ,0},
     {STRERR_ZEPHYR_TRACE_INIT_DISASSEMBLER        ,0},
     {STRERR_ZEPHYR_TRACE_DECODER                  ,0},
     {STRERR_ZEPHYR_TRACE_CONFIG                   ,0},
     {STRERR_ZEPHYR_TRACE_DISASSEMBLER             ,0},
     {STRERR_LAST_ERROR                            ,1}
  };
   
#endif
      
      
      
      
      
      
      

