/******************************************************************************
Module		: ash_dma_test.cpp
Engineer		: Rejeesh S Babu
Description		: Example program to demonstrate ash_dma usage
Date           	Initials	    Description
01-Jun-2009   	   RS          	      Initial
******************************************************************************/

// Common includes
#include "windows.h"
#include "stdio.h"
#include <assert.h>
#include "../ash_dma/ash_dma.h"


// local variables
TyDMAParameters tyDMAParameters;
unsigned int 	uiDataToWrite[OPXDDMA_MAX_LEN];
unsigned int 	uiDataToRead[OPXDDMA_MAX_LEN];

VPDMA_OPEN             wdmaOpenConnection         = NULL;
VPDMA_CLOSE            wdmaCloseConnection        = NULL;
VPDMA_READLONG         wdmaReadTargetMemoryLong   = NULL;
VPDMA_WRITELONG        wdmaWriteTargetMemoryLong  = NULL;

// local functions
static void			 PrintTitleAndVersion			  (void);
static void			 PrintUsage						  (void);
static void			 ConvertProgramOptionsToLowercase (int argc		 ,char *argv[]);
static int			 AnalyseAshDmaOptions			  (int argc      ,char *argv[], TyDMAParameters *ptyDMAOptions);
static unsigned long StrToUlong						  (char *pszValue,int *pbValid);
void 				 InitDMAOptions					  (void);


/***************************************************************************
Function	: main
Engineer	: Rejeesh S Babu
Input		: int argc - number of arguments
			  char *argv[] - program arguments
Output		: return value 
Description	: main part of program
Date              	Initials    	Description
01-Jun-2009   	   RS          	    Initial
****************************************************************************/
int main(int argc, char* argv[])
{
   HINSTANCE hASHDMADll;
   unsigned int i,j=0;

   // show program title and version
   PrintTitleAndVersion();

   //Initialize DMA parameters
   InitDMAOptions();

   // analyze program parameters
   if (AnalyseAshDmaOptions(argc,argv , &tyDMAParameters) != DMA_NO_ERROR)
      {
      // invalid parameters
      PrintUsage();
      return(DMA_INVALID_PARAMETERS);
      }

   //Load library
   hASHDMADll = LoadLibrary("ash_dma.dll");
   if (hASHDMADll == NULL)
      printf("Unable to load ash_dma library\n");
   else
      {
      printf("Library loaded.\n");
      wdmaOpenConnection         = (VPDMA_OPEN)GetProcAddress(hASHDMADll,VPDMA_OPEN_FUNC);
      wdmaCloseConnection        = (VPDMA_CLOSE)GetProcAddress(hASHDMADll,VPDMA_CLOSE_FUNC);
      wdmaReadTargetMemoryLong   = (VPDMA_READLONG)GetProcAddress(hASHDMADll,VPDMA_READLONG_FUNC);
      wdmaWriteTargetMemoryLong  = (VPDMA_WRITELONG)GetProcAddress(hASHDMADll,VPDMA_WRITELONG_FUNC);

      if (wdmaOpenConnection && wdmaCloseConnection && wdmaReadTargetMemoryLong && wdmaWriteTargetMemoryLong)
         {
         if (wdmaOpenConnection()== 0x0)
            {
			    printf("Connected to target.\n");
				printf("\nWriting to memory using DMA...\n");

				for( i = 0,j=0; i < tyDMAParameters.ulLength; i++,j+=4 )
					printf("Writing %08X to address %08X \n",uiDataToWrite[i],tyDMAParameters.ulAddr+j);
	
				if( DMA_FAILURE == wdmaWriteTargetMemoryLong(tyDMAParameters.ulAddr, uiDataToWrite, tyDMAParameters.ulLength) )
					{
						printf("Error writing memory\n");
					}

				printf("\nReading back memory using DMA...\n");
	
				if( DMA_FAILURE == wdmaReadTargetMemoryLong(tyDMAParameters.ulAddr, uiDataToRead, tyDMAParameters.ulLength))
				{
					printf("Error reading memory\n");
				}
                    
				for(i = 0,j=0; i < tyDMAParameters.ulLength; i++,j+=4 )
					printf("Address: %08X Data %08X \n",tyDMAParameters.ulAddr+j,uiDataToRead[i]);

				for(i=0;i<tyDMAParameters.ulLength;i++)
					if (uiDataToRead[i] != uiDataToWrite[i]) 
						{
						printf("(DMA memory test failed!\n");
						return 0;
						}
				printf("\nDMA memory test passed\n");
	
				if( DMA_FAILURE == wdmaCloseConnection())
					{
						printf("Error: Closing connection\n");
					}
	
            }
         else
            printf("Unable to connect\n");
         }
      else
         printf("Unable to resolve function\n");
      }

   if (hASHDMADll)
      (void)FreeLibrary(hASHDMADll);

   return 0;
}
/****************************************************************************
     Function: PrintTitleAndVersion
     Engineer: Rejeesh S Babu
        Input: none
       Output: none
  Description: print program title and version
Date           Initials    Description
01-Jun-2009    RS          Initial
****************************************************************************/
static void PrintTitleAndVersion(void)
{
   printf("%s\n", OPXDDMA_PROG_TITLE);
   printf("%s\n", OPXDDMA_PROG_VERSION);
   printf("\n");
}
/****************************************************************************
Function	: PrintUsage
Engineer	: Rejeesh S Babu
Input		: none
Output		: none
Description : print help for program
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static void PrintUsage(void)
{
	printf("%s\n", OPXDDMA_USAGE0);
	printf("\n");
	printf("%s\n", OPXDDMA_USAGE1);
	printf("%s\n", OPXDDMA_USAGE_OPT0);	
	printf("%s\n", OPXDDMA_USAGE_OPT_DMA0);
	printf("%s\n", OPXDDMA_USAGE_OPT_DMA1);
}
/****************************************************************************
Function	: AnalyseDMAOptions
Engineer	: Rejeesh S Babu
Input		: int argc - number of program arguments
char *argv[]: program arguments (strings separated by space)
			  TyDMAParameters *ptyDMAOptions - structure for program settings
Output		: int - error code DMA_xxx
Description: analyzes program options and save result to structure.
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static int AnalyseAshDmaOptions(int argc, char *argv[], TyDMAParameters *ptyDMAOptions)
{
	int iOption=1;
	int  bValid = 0;

	assert(argc > 0);
	assert(argv != NULL);
	assert(ptyDMAOptions != NULL);
	
	if(argc == 1)
	{
		InitDMAOptions();
		return DMA_NO_ERROR;
	}
	else if(argc != 3)
		return DMA_INVALID_PARAMETERS;
	// convert strings to lower case
	ConvertProgramOptionsToLowercase(argc, argv);
	
	ptyDMAOptions->ulAddr  =StrToUlong(argv[iOption++],&bValid);
	if(!bValid)
		return DMA_INVALID_PARAMETERS;	
	ptyDMAOptions->ulLength =StrToUlong(argv[iOption++],&bValid);
	if(!bValid)
		return DMA_INVALID_PARAMETERS;	
	else 
		return DMA_NO_ERROR;
}
/****************************************************************************
Function	: ConvertProgramOptionsToLowercase
Engineer	: Rejeesh S Babu
Input		: int argc - number of options
			  char *argv[] - parameters
Output		: none
Description : convert all program input parameters to lower case.
Date           Initials    Description
16-Jan-2009       RS          Initial
***************************************************************************/
static void ConvertProgramOptionsToLowercase(int argc, char *argv[])
{
	char *pcString;
	int iCnt;
	
	// convert all parameters
	for (iCnt=0; iCnt<argc; iCnt++)
	{
		pcString = argv[iCnt];
		while (*pcString)
		{
			if ((*pcString >= 'A') && (*pcString <= 'Z'))
				*pcString -= ('A' - 'a');                             // subtract offset between lower and upper case
			pcString++;
		}
	}
}
/****************************************************************************
Function	: StrToUlong
Engineer	: Rejeesh S Babu
Input		: char *pszValue    : string value
			  int *pbValid      : storage for flag if value is valid
Output		: unsigned long     : value represented by the string
Description	: Converts string to long supports decimal and hex 
			  when the string value begins with 0x.
Date           Initials    Description
16-Jan-2009       RS          Initial
*****************************************************************************/
static unsigned long StrToUlong(char *pszValue, int *pbValid)
{
	unsigned long ulValue;
	//__int64 ullValue;
	char *pszStop = NULL;
	assert(pszValue != NULL);
	// dont allow octal, so skip leading '0's unless its 0x...
	while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] != 'X') && (pszValue[1] != '\0'))
		pszValue++;
	// strtoul supports decimal nnnn and hex 0xnnnn...
	ulValue = strtoul(pszValue, &pszStop, 0);
		// only valid if all characters were processed ok...
	if ((pszStop) && (pszStop[0] == '\0'))
	{
		if (pbValid)
			*pbValid = 1;
		return ulValue;
	}
	else
	{
		if (pbValid)
			*pbValid = 0;
		return 0;
	}
}
/****************************************************************************
Function	: InitDMAOptions
Engineer	: Rejeesh S Babu
Input		: 
Output	:
Description : Initialize DMA parameters.
Date           Initials    Description
16-Jan-2009       RS          Initial
***************************************************************************/
void InitDMAOptions()
{
	
	for(int i=0;i<OPXDDMA_MAX_LEN;i++)
	   uiDataToWrite[i]=0xAABBCCDD;
	tyDMAParameters.ulAddr  =0x00000000;
	tyDMAParameters.ulLength=0x0000000A;

}
