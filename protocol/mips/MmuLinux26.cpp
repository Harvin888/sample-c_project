/******************************************************************************
       Module: MmuLinux26.cpp
     Engineer: Suresh P. C.
  Description: Implementation file for Linux 32 bit kernel Debug support.

  Date           Initials    Description
  16-Jul-2009    SPC         Initial
  04-Nov-2009    SPC         Added Application specific MMU support & Big Endian Support.
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "protocol/common/drvopxd.h"
#include "midlayer.h"
#include "ml_tlb.h"
#include "MmuLinux26.h"
#include "mips.h"

static TyTLBEntry tlbEntryBackup;
static int tlbEntryBkpAvailable = 0; 
static unsigned long ulGlobalPgd = 0;
static unsigned long ulAppPgd = 0, ulAppContext = 0;

#define APP_SEC_END_ADRR 0x80000000
#define ASID_MASK 0x000000FF

/****************************************************************************
     Function: UnSetLinux26MMUMapping
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
       Output: none
  Description: Restore the backup of TLB that made while making MMU mapping
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
void UnSetLinux26MMUMapping(TyMLHandle *ptyMLHandle)
{
   if ( tlbEntryBkpAvailable )
      {
      (void)MLT_WriteTLB(ptyMLHandle,0x0,&tlbEntryBackup,4,4);
      tlbEntryBkpAvailable = 0;
      }
}

/****************************************************************************
     Function: SetLinux26MMUMapping
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulAddrs - Memory address that need MMU translation
       Output: 0 : Success ; Error code: otherwise
  Description: Creates MMU mapping and update the TLB
Date           Initials    Description
17-Jul-2009    SPC         Initial
04-Nov-2009    SPC         Added Application specific MMU support & Big Endian Support.
****************************************************************************/
int SetLinux26MMUMapping(TyMLHandle *ptyMLHandle, unsigned long ulAddrs, int iWriteAccess)
{
   unsigned long ulPgdIndex, ulPgdAddr;
   unsigned short usPteIndex;
   unsigned long ulPteBaseAddr,ulPteAddr,ulPfn1,ulPfn2;
   TyTLBEntry tlbEntry;
   int tlbIndex = -1;
   unsigned long ulPgd;

   memset(&tlbEntry, 0, sizeof(tlbEntry));

   tlbEntry.EntryHi.Bits.VPN2 = ulAddrs >> (PAGE_SHIFT + 1);

   //First we checking for a TLB entry exist or not
   if ( 0 < (tlbIndex = SearchTLB(tlbEntry.EntryHi.Bits.VPN2)) )
      {
      int itlbEntryModified = 0;
      //TLB entry exists for the address. we have to make its cache property to 
      //write-through. Otherwise it wont update the memory.
      (void)MLT_ReadTLB(ptyMLHandle, (unsigned long)tlbIndex*4, &tlbEntry, 4, 4);

      if ( iWriteAccess )
         {
         if ( ( tlbEntry.EntryLo1.Bits.PFN != 0 ) && (tlbEntry.EntryLo1.Bits.D != 1 ) )
            {
            tlbEntry.EntryLo1.Bits.D = 1; //Making dirty bit to 1
            itlbEntryModified = 1;
            }
         if ( ( tlbEntry.EntryLo0.Bits.PFN != 0 ) && (tlbEntry.EntryLo0.Bits.D != 1 ) )
            {
            tlbEntry.EntryLo0.Bits.D = 1; //Making dirty bit to 1
            itlbEntryModified = 1;
            }
         if ( tlbEntry.EntryLo1.Bits.C == 3 ) //Cacheable, noncoherent, write-back, write allocate
            {
            tlbEntry.EntryLo1.Bits.C = 1; //Cacheable, noncoherent, write-through, write allocate
            itlbEntryModified = 1;
            }
         if ( tlbEntry.EntryLo0.Bits.C == 3 ) //Cacheable, noncoherent, write-back, write allocate
            {
            tlbEntry.EntryLo0.Bits.C = 1; //Cacheable, noncoherent, write-through, write allocate
            itlbEntryModified = 1;
            }
         }

      if ( itlbEntryModified )
         {
         (void)MLT_WriteTLB(ptyMLHandle, (unsigned long)tlbIndex*4,&tlbEntry,4,4);
         }
      return 0;
      }

   // Not found a TLB entry. So we have to create one.

   if (ulAddrs < APP_SEC_END_ADRR)
      {
      ulPgd = ulAppPgd;
      tlbEntry.EntryHi.Bits.ASID = ulAppContext  & ASID_MASK;
      }
   else
      {
      ulPgd = ulGlobalPgd;
      }

   if (ulPgd == 0)
	   return 0;

   // finding pgd index
   ulPgdIndex = pgd_index(ulAddrs);
   ulPgdAddr =(ulPgd + (ulPgdIndex*4));

   //Getting pte address
   (void) MLM_ReadWord(ptyMLHandle, ulPgdAddr, &ulPteBaseAddr);

   // If target is big endian, swap the bytes
   if ( ptyMLHandle->bBigEndian )
	   ulPteBaseAddr = ML_SwapEndianWord(ulPteBaseAddr);

   if ( ulPteBaseAddr == 0 )
      return 0;

   //finding pte index
   usPteIndex = (unsigned short)__pte_offset(ulAddrs) & 0xFFFE;
   ulPteAddr = (ulPteBaseAddr + (usPteIndex*4)) ; 

   //Getting pte address
   (void)MLM_ReadWord(ptyMLHandle,ulPteAddr,&ulPfn1);
   // If target is big endian, swap the bytes
   if ( ptyMLHandle->bBigEndian )
	   ulPfn1 = ML_SwapEndianWord(ulPfn1);

   usPteIndex++;
   ulPteAddr = (ulPteBaseAddr + (usPteIndex*4)) ; 

   //Getting pte address
   (void)MLM_ReadWord(ptyMLHandle,ulPteAddr,&ulPfn2);
   // If target is big endian, swap the bytes
   if ( ptyMLHandle->bBigEndian )
	   ulPfn2 = ML_SwapEndianWord(ulPfn2);

   if ( ( ulPfn1 == 0 ) && ( ulPfn2 == 0 ) )
      return 0;

   //Backing up the first entry of TLB
   (void)MLT_ReadTLB(ptyMLHandle, 0x0, &tlbEntryBackup, 4, 4);
   tlbEntryBkpAvailable = 1;

   //Create new TLB entry
   tlbEntry.PageMask.Bits.Mask = 0;
   tlbEntry.EntryLo0.Data = ulPfn1 >> 6; //Got from __update_tlb fn in Linux
   tlbEntry.EntryLo1.Data = ulPfn2 >> 6; //Got from __update_tlb fn in Linux

   if ( iWriteAccess )
      {
      if ( ( tlbEntry.EntryLo1.Bits.PFN != 0 ) && (tlbEntry.EntryLo1.Bits.D != 1 ) )
         {
         tlbEntry.EntryLo1.Bits.D = 1; //Making dirty bit to 1
         }
      if ( ( tlbEntry.EntryLo0.Bits.PFN != 0 ) && (tlbEntry.EntryLo0.Bits.D != 1 ) )
         {
         tlbEntry.EntryLo0.Bits.D = 1; //Making dirty bit to 1
         }
      }
   if ( tlbEntry.EntryLo1.Bits.C == 3 ) //Cacheable, noncoherent, write-back, write allocate
      {
      tlbEntry.EntryLo1.Bits.C = 1; //Cacheable, noncoherent, write-through, write allocate
      }
   if ( tlbEntry.EntryLo0.Bits.C == 3 ) //Cacheable, noncoherent, write-back, write allocate
      {
      tlbEntry.EntryLo0.Bits.C = 1; //Cacheable, noncoherent, write-through, write allocate
      }

   (void)MLT_WriteTLB(ptyMLHandle, 0x0, &tlbEntry, 4, 4);

   return 0;
}

/****************************************************************************
     Function: IsLinux26MMUMappingNeeded
     Engineer: Suresh P.C
        Input: unsigned long ulAddrs - Memory address that need MMU translation
       Output: 1: if needs a Mapping 0: not need a MMU mapping
  Description: Check if there is a MMU mapping needed.
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
int IsLinux26MMUMappingNeeded(unsigned long ulAddrs)
{
   unsigned long msb_bits = 0;

   /*
   MIPS Memory mapping's are
      useg  0x0000.0000 0x7FFF.FFFF  Mapped   Cached
      kseg0 0x8000.0000 0x9FFF.FFFF  Unmapped Cached
      kseg1 0xA000.0000 0xBFFF.FFFF  Unmapped Uncached
      kseg2 0xC000.0000 0xDFFF.FFFF  Mapped   Cached
      kseg3 0xE000.0000 0xFFFF.FFFF  Mapped   Cached
   */
   //We are checking the first 2 bits to binary 10
   //If there is a result, we are return that mapping needed.

   msb_bits = ulAddrs >> 30;
   return(msb_bits ^ 2)?1:0;
}

/****************************************************************************
     Function: InitLinux26MMU
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulPgDirAddrs - Address of swapper_pg_dir
       Output: 0 : Success ; Error code: otherwise
  Description: Initializes the Linux 2.6 MMU mapping
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
int InitLinux26MMU(TyMLHandle *ptyMLHandle, unsigned long ulPgDirAddrs)
{
   ptyMLHandle = ptyMLHandle;
   ulGlobalPgd = ulPgDirAddrs;
   return 0;
}

/****************************************************************************
     Function: InitLinux26AppMMU
     Engineer: Suresh P.C
        Input: unsigned long ulAppCon - Application context
               unsigned long ulPgDirAddrs - Address to App PGD
       Output: 0 : Success ; Error code: otherwise
  Description: Initializes the Linux 2.6 Application MMU mapping
Date           Initials    Description
20-Oct-2009    SPC         Initial
****************************************************************************/
int InitLinux26AppMMU(unsigned long ulAppCon, unsigned long ulPgDirAddrs)
{
   ulAppPgd = ulPgDirAddrs;
   ulAppContext = ulAppCon;
   return 0;
}


