/****************************************************************************
       Module: commdefs.h
     Engineer: Nikolay Chokoev
  Description: Ashling specific common defines used by mdi and gdb server
Date           Initials    Description
03-Jul-2008    NCH         Initial & cleanup
****************************************************************************/

#ifndef __COMMDEFS_H__
#define __COMMDEFS_H__

#ifndef NOREF
#define NOREF(var)  (var=var)
#endif

#define ASSERT(exp) ((void)0)
#define ASSERT_NOW() ((void)0)

#define MIPS_IR_IDCODE 0x1
#define MIPS_IR_BYPASS 0x1f

typedef enum
{
   EJTAG26Style         = 0,
   EJTAG20StyleStd      = 1,    // 4 Bit EJTAG20 Trace
   EJTAG20StyleExt      = 2     // 8 Bit EJTAG20 Trace
} TyEJTAGStyle;

typedef enum
{
   Match_Target,
   Fixed_3v3
} TyTargetSupply;

typedef enum
{
   TraceSupportNotAvailable         = 0,
   EJTAG26StyleTraceSupport         = 1,
   EJTAG20StyleTraceSupportStd      = 2,    // 4 Bit EJTAG20 Trace
   EJTAG20StyleTraceSupportExt      = 3     // 8 Bit EJTAG20 Trace
} TyTraceSupport;

typedef enum
{
   UnSupportedTLB,
   StandardR3000,
   StandardR4000,
   SmartMIPSR4000
} TyTLBType;

typedef enum
{
   Disa_For_PCP_Proc,
   Disa_For_TriCore_Proc_RiderA,
   Disa_For_TriCore_Proc_RiderB,

   // MIPS Disassembly types (Instruction Sets)
   Disa_MIPS_UnSpecified      = 0x00000000,
   Disa_MIPS_I                = 0x00000001,
   Disa_MIPS_II               = 0x00000002,
   Disa_MIPS_III              = 0x00000004,
   Disa_MIPS_IV               = 0x00000008,
   Disa_MIPS_32               = 0x00000010,
   Disa_MIPS_64               = 0x00000020,
   Disa_MIPS_4KSc             = 0x00000040,
   Disa_MIPS_BRCM			  = 0x00000080,
   Disa_MIPS_All              = 0xFFFFFFFF,

   // ARM Disassembly types (Instruction Sets)
   Disa_ARM_Unknown           = 0x00000000,
   Disa_ARM_Arm               = 0x00000001,
   Disa_ARM_Thumb             = 0x00000002,


   // PowerPC Disassembly types (Instruction Sets)
   Disa_PPC_Core               = 0x00000000,
   Disa_PPC_eTPU               = 0x00000001,

   // AVR32 Disassembly types (Instruction Sets)
   Disa_AVR32_Morgan           = 0x00000000,
   Disa_AVR32_Bravo            = 0x00000001,

   // 8051 and Theseus types (Instruction sets)
   Disa_8051_Standard          = 0x00000000,
   Disa_8051_Theseus_Linear    = 0x00000001  // Theseus linear mode extended instruction set


} ProcDisaType; 

// Bank Switching
typedef enum
{
   XDATA_SWITCH         = 0x00,
   PORT_SWITCH          = 0x01,
   SFR_SWITCH_EXTEN     = 0x02,
   SFR_SWITCH_BANKSW    = 0x03,
   SFR_SWITCH_ROMBK     = 0x04,
   SFR_SWITCH_LBS       = 0x05,
   SFR_SWITCH_HBS       = 0x06,
   SFR_SWITCH_LBS_HBS   = 0x07,
   SFR_SWITCH_TAMAEMU   = 0x08 // Tama-EMU banking support
}TXRXBankSwitchType;

// Ultra51 Prescalar settings
typedef enum
{
   NO_PRESCALAR = 0x00,
   PRESCALAR    = 0x01
} TXRXPreScalar;

typedef enum
{
   P3_FOR_XDATA = 0x00,
   P3_FOR_IO    = 0x01,
   P3_AUTO      = 0x02
} TXRXUseP3;

// WatchDog
typedef enum
{
   NO_WDOG           = 0x00,
   WDOG_NOT_USED     = 0x01,
   ENABLE_WDOG       = 0x02,
   WDOG_HWDI_SUPPORT = 0x03
} TXRXWatchDog;

typedef enum
{
   UndefinedEEWrRt,// 
   P8xx5000EEWrRt,
   SmartMXEEWrRt,
   MF2D80EEWrRt,
   MF2D8184EEWrRt
}  TyEEWrRoutine;

typedef enum
{
   StandardCodeRdRt,// 
   MF2D80CodeRdRt,
   MF2D8184CodeRdRt
}  TyCodeRdRoutine;

typedef enum
{
   UndefinedProbe,
   PRU_XA,         // PRU_XA probe
   PRU_S3
}TyProbeType;

typedef enum
{
   UndefinedGroup,// All other processors that don't have a specifiec group
   SmartMX,       // SmartMX group of processors
   SmartXA2,      // SmartXA2 group of processors
   HiPerSmart,    // HiPerSmart group of processors
   StdMIPSDevice, // Standard group of MIPS processors
   P8RF5000,      // P8RF5000 group of processors
   P8WE5000,      // P8WE5000 group of processors
   Mifare,        // Mifare group of processors
   Painter1,      // Painter1 group of processors
   Painter2,      // Painter2 group of processors
   _8xC51MX_Grp,  // Standard MX group of processors
   Calva,         // Calva group of processors
   PNX,           // PNX group of processors
   Tamaemu,       // Tamaemu group of processors
   PCD509X,       // PC509X group of processors
   PCD509XY,      // PC509XY group of processors
   _8xC190,       // _8xC190 group of processors
   _8xC55x,       // _8xC55x group of processors
   _8xC66x,       // _8xC66x group of processors
   SmartXA1 ,     // SmartXA2 group of processors
   NoDebugCore,   // NXP Group of processors without direct debug visibility
   AVR32A,        // AVR32A group of processors
   AVR32B,        // AVR32B group of processors
   NoMatchGroup   // A procesor can never have this group...
}  TyProcFamGrp;

#endif //__COMMDEFS_H__
