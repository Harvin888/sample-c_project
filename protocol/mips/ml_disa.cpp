
/****************************************************************************
       Module: ml_disa.cpp
     Engineer: Nikolay Chokoev
  Description: Implementation of Opella-XD MIPS MDI midlayer disassembly
Date           Initials    Description
08-Feb-2008    NCH         Initial
****************************************************************************/

#define MDI_LIB               // We are an MDI Library not a debugger

#include <stdio.h>
#include <assert.h>

#include "firmware/export/api_def.h"
#include "firmware/export/api_cmd.h"

#include "debug.h"
#include "ml_error.h"
#include "mdi.h"
#include "mips.h"
#include "commdefs.h"
#include "gerror.h"

#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "midlayer.h"
#include "ml_disa.h"
#ifndef __LINUX
// Windows specific includes
#include "windows.h"
#else
// Linux includes
#endif

#define BITS0_4(VAL)   (unsigned long)(VAL         & 0x1F ) 
#define BITS0_7(VAL)   (unsigned long)(VAL         & 0xFF ) 
#define BITS0_10(VAL)  (unsigned long)(VAL         & 0x7FF) 
#define BITS5_9(VAL)   (unsigned long)((VAL >>  5) & 0x1F ) 
#define BITS5_10(VAL)  (unsigned long)((VAL >>  5) & 0x3F ) 
#define BITS8_10(VAL)  (unsigned long)((VAL >>  8) & 0x07 )  

// Array to determine MIPS32 register number given a MIPS16 register number

static unsigned long Mips16ToMips32Reg[8] = { 16, 17, 2, 3, 4, 5, 6, 7 };



// Table of Mips16 branch instructions
// -----------------------------------

static TyDisa16Entry Disa16Table[]=
   //ulMask    ulOpcode   InstType
   //------    --------   --------
   {{0xFFFF,   0xE8A0,    JRC_RA_INST16     },      // jrc    $ra
    {0xFFFF,   0xE820,    JR_RA_INST16      },      // jr     $ra
    {0xFF00,   0x6100,    B_REL8_INST16     },      // btnez  i8/i16
    {0xFF00,   0x6000,    B_REL8_INST16     },      // bteqz  i8/i16
    {0xFC00,   0x1C00,    JALX_INST16       },      // jalx   i26
    {0xFC00,   0x1800,    JAL_INST16        },      // jal    i26
    {0xF8FF,   0xE8C0,    JALRC_RARX_INST16 },      // jalrc  $ra,$rx
    {0xF8FF,   0xE880,    JRC_RX_INST16     },      // jrc    $rx
    {0xF8FF,   0xE840,    JALR_RARX_INST16  },      // jalr   $ra,$rx
    {0xF8FF,   0xE800,    JR_RX_INST16      },      // jr     $rx
    {0xF800,   0x2800,    B_REL8_INST16     },      // bnez   i8/i16
    {0xF800,   0x2000,    B_REL8_INST16     },      // beqz   i8/i16
    {0xF800,   0x1000,    B_REL11_INST16    }};     // b      i11/i16

#define NUM_DISA16_TABLE_ENTRIES ((int)(sizeof(Disa16Table)/sizeof(TyDisa16Entry)))



// Table of Mips32 branch instructions
// -----------------------------------

static TyDisa32Entry Disa32Table[]=
   //ulMask      ulOpcode     InstType
   //------      --------     --------
  {{0xFC1FF83F, 0x00000008,  JR_INST32     },       // "jr"        
   {0xFC1F003F, 0x00000009,  JALR_INST32   },       // "jalr"      
   {0xFC000000, 0x08000000,  J_INST32      },       // "j"         
   {0xFC000000, 0x0C000000,  JAL_INST32    },       // "jal"       
   {0xFC000000, 0x74000000,  JALX_INST32   },       // "jalx"
   {0xFFFFFFFF, 0x42000018,  ERET_INST32   },       // "eret"
   {0xFFFFFFFF, 0x4200001F,  DERET_INST32  },       // "deret"
   {0xFC1F0000, 0x04000000,  B_REL16_INST32},       // "bltz"      
   {0xFC1F0000, 0x04010000,  B_REL16_INST32},       // "bgez"      
   {0xFC1F0000, 0x04020000,  B_REL16_INST32},       // "bltzl"     
   {0xFC1F0000, 0x04030000,  B_REL16_INST32},       // "bgezl"     
   {0xFC1F0000, 0x04100000,  B_REL16_INST32},       // "bltzal"    
   {0xFC1F0000, 0x04110000,  B_REL16_INST32},       // "bgezal"    
   {0xFC1F0000, 0x04120000,  B_REL16_INST32},       // "bltzall"   
   {0xFC1F0000, 0x04130000,  B_REL16_INST32},       // "bgezall"   
   {0xFC000000, 0x10000000,  B_REL16_INST32},       // "beq"       
   {0xFC000000, 0x14000000,  B_REL16_INST32},       // "bne"       
   {0xFC1F0000, 0x18000000,  B_REL16_INST32},       // "blez"      
   {0xFC1F0000, 0x1C000000,  B_REL16_INST32},       // "bgtz"      
   {0xFFFF0000, 0x41000000,  B_REL16_INST32},       // "bc0f"      
   {0xFFFF0000, 0x41010000,  B_REL16_INST32},       // "bc0t"      
   {0xFFFF0000, 0x41020000,  B_REL16_INST32},       // "bc0fl"     
   {0xFFFF0000, 0x41030000,  B_REL16_INST32},       // "bc0tl"     
   {0xFFFF0000, 0x45000000,  B_REL16_INST32},       // "bc1f"      
   {0xFFE30000, 0x45000000,  B_REL16_INST32},       // "bc1f"      
   {0xFFFF0000, 0x45010000,  B_REL16_INST32},       // "bc1t"      
   {0xFFE30000, 0x45010000,  B_REL16_INST32},       // "bc1t"      
   {0xFFFF0000, 0x45020000,  B_REL16_INST32},       // "bc1fl"     
   {0xFFE30000, 0x45020000,  B_REL16_INST32},       // "bc1fl"     
   {0xFFFF0000, 0x45030000,  B_REL16_INST32},       // "bc1tl"     
   {0xFFE30000, 0x45030000,  B_REL16_INST32},       // "bc1tl"     
   {0xFFFF0000, 0x49000000,  B_REL16_INST32},       // "bc2f"      
   {0xFFE30000, 0x49000000,  B_REL16_INST32},       // "bc2f"      
   {0xFFFF0000, 0x49010000,  B_REL16_INST32},       // "bc2t"      
   {0xFFE30000, 0x49010000,  B_REL16_INST32},       // "bc2t"      
   {0xFFFF0000, 0x49020000,  B_REL16_INST32},       // "bc2fl"     
   {0xFFE30000, 0x49020000,  B_REL16_INST32},       // "bc2fl"     
   {0xFFFF0000, 0x49030000,  B_REL16_INST32},       // "bc2tl"     
   {0xFFE30000, 0x49030000,  B_REL16_INST32},       // "bc2tl"     
   {0xFC000000, 0x50000000,  B_REL16_INST32},       // "beql"      
   {0xFC000000, 0x54000000,  B_REL16_INST32},       // "bnel"      
   {0xFC1F0000, 0x58000000,  B_REL16_INST32},       // "blezl"     
   {0xFC1F0000, 0x5C000000,  B_REL16_INST32}};      // "bgtzl"

#define NUM_DISA32_TABLE_ENTRIES ((int)(sizeof(Disa32Table)/sizeof(TyDisa32Entry)))


/****************************************************************************
     Function: ML_Disa16
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
13-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_Disa16(TyMLHandle 	 *ptyHandle, 
                  unsigned long  ulPCAddr, 
                  TyDisaResult   *pDisaResult)
{
   TyError        ErrRet   = ERR_NO_ERROR;
   unsigned long          ulImmediate;
   unsigned long          ulDestReg;
   unsigned short         uExtend = 0;
   unsigned short         uInstruction;
   long           i;
   TyInst16Types  InstType = OTHER_INST16;
   
   pDisaResult->bCallInst = FALSE;

   // if not extended and not jal/jalx, next instruction is 2 bytes on
   pDisaResult->ulNextAddr = ulPCAddr + 2;
   // next instruction still in 16-bit code
   pDisaResult->ulNextInstSize = 2;

   // if not branch/call, have no destination, indicate by copying next address
   pDisaResult->ulDestAddr     = pDisaResult->ulNextAddr;
   pDisaResult->ulDestInstSize = pDisaResult->ulNextInstSize;

   // read the instruction halfword
   if(ptyHandle->bBigEndian)
      uInstruction = ML_SwapEndianHalfword(MLM_GetHalfWord(ptyHandle,ulPCAddr));
   else
      uInstruction = MLM_GetHalfWord(ptyHandle,ulPCAddr);

   // check for extended instructions
   if ((uInstruction & 0xF800) == 0xF000)
      {
      // save the extended part and read the real instruction halfword
      uExtend = uInstruction;
      if(ptyHandle->bBigEndian)
         uInstruction = ML_SwapEndianHalfword(MLM_GetHalfWord(ptyHandle,ulPCAddr+2));
      else
         uInstruction = MLM_GetHalfWord(ptyHandle,ulPCAddr+2);

      // allow for 2 extra bytes in extended instructions
      pDisaResult->ulNextAddr = ulPCAddr + 4;
      pDisaResult->ulDestAddr = pDisaResult->ulNextAddr;
      }


   // search for this instruction in our branch table
   for (i=0; i<NUM_DISA16_TABLE_ENTRIES; i++)
      {
      if ((uInstruction & Disa16Table[i].uMask) == Disa16Table[i].uOpcode)
         {
         InstType = Disa16Table[i].InstType;
         break;
         }
      }


   // if a branch, determine the destination 
   switch (InstType)
      {
      case JALR_RARX_INST16:
      case JALRC_RARX_INST16:
      case JR_RA_INST16:
      case JRC_RA_INST16:
      case JR_RX_INST16:
      case JRC_RX_INST16:
         switch (InstType)
            {
            case JALR_RARX_INST16:
            case JALRC_RARX_INST16:
               pDisaResult->bCallInst = TRUE;
               break;
            default:
               break;
            }
         switch (InstType)
            {
            case JALR_RARX_INST16:
            case JR_RA_INST16:
            case JR_RX_INST16:
               // allow for branch delay slot (2 bytes)
               pDisaResult->ulNextAddr += 2;
               break;
            default:
               break;
            }
         switch (InstType)
            {
            case JR_RA_INST16:
            case JRC_RA_INST16:
               // Mips32 General register 31 contains the destination
               ulDestReg = 31;
               break;
            default:
               // Mips16 register at bits 8-10 contains the destination
               ulDestReg = Mips16ToMips32Reg[BITS8_10(uInstruction)];
               break;
            }

         // read the register contents (destination address)
         (void)MLR_ReadRegister(ptyHandle,ulDestReg, MDIMIPCPU, &pDisaResult->ulDestAddr);
         if (pDisaResult->ulDestAddr & 1)
            {
            // destination is in 16-bit code
            pDisaResult->ulDestAddr--;
            pDisaResult->ulDestInstSize = 2;
            }
         else
            {
            pDisaResult->ulDestInstSize = 4;
            }
         break;


      case JALX_INST16:
         // JALX switching modes 16-bit to 32-bit
         pDisaResult->ulDestInstSize = 4;
         // fall through
      case JAL_INST16:
         // JALX and JAL are Call Instructions
         pDisaResult->bCallInst = TRUE;

         // JALX and JAL have 26-bit immediate
         // save the high part and read the low instruction halfword
         uExtend = uInstruction;
         if(ptyHandle->bBigEndian)
            uInstruction = ML_SwapEndianHalfword(MLM_GetHalfWord(ptyHandle,ulPCAddr+2));
         else
            uInstruction = MLM_GetHalfWord(ptyHandle,ulPCAddr+2);

         ulImmediate = (unsigned long)uInstruction;        // bits 15:0  of imm
         ulImmediate |= (BITS5_9(uExtend) << 16);  // bits 20:16 of imm
         ulImmediate |= (BITS0_4(uExtend) << 21);  // bits 25:21 of imm
         pDisaResult->ulDestAddr = ulImmediate << 2;
         pDisaResult->ulDestAddr |= ((ulPCAddr + 4) & 0xF0000000);   //add top pc nibble

         // allow for branch delay slot(2) + extra bytes in jal/jalx(2)
         pDisaResult->ulNextAddr += 4;
         break;


      case B_REL8_INST16:
      case B_REL11_INST16:
         if (uExtend == 0)
            {
            if (InstType == B_REL8_INST16)
               {
               ulImmediate = BITS0_7(uInstruction) << 1;
               if (ulImmediate & 0x0100)
                  {
                  // negative
                  ulImmediate =  (0x0 - ulImmediate)  & 0x1FF;
                  pDisaResult->ulDestAddr = ulPCAddr + 2 - ulImmediate;
                  }
               else
                  {
                  // positive
                  pDisaResult->ulDestAddr = ulPCAddr + 2 + ulImmediate;
                  }
               }
            else     // B_Rel11_Inst16
               {
               ulImmediate = BITS0_10(uInstruction) << 1;
               if (ulImmediate & 0x0800)
                  {
                  // negative
                  ulImmediate =  (0x0 - ulImmediate)  & 0xFFF;
                  pDisaResult->ulDestAddr = ulPCAddr + 2 - ulImmediate;
                  }
               else
                  {
                  // positive
                  pDisaResult->ulDestAddr = ulPCAddr + 2 + ulImmediate;
                  }
               }
            }
         else
            {
            // extended version is same for both (16-bit immediate)
            ulImmediate  =  BITS0_4 (uInstruction);
            ulImmediate |= (BITS5_10(uExtend) <<5);
            ulImmediate |= (BITS0_4(uExtend) <<11);
            ulImmediate <<= 2;
            if (ulImmediate & 0x010000)
               {
               // negative
               ulImmediate =  (0x0 - ulImmediate)  & 0x1FFFF;
               pDisaResult->ulDestAddr = ulPCAddr + 4 - ulImmediate;
               }
            else
               {
               // positive
               pDisaResult->ulDestAddr = ulPCAddr + 4 + ulImmediate;
               }
            }
         break;

      case OTHER_INST16:
      default:
         break;
      }

   return ErrRet;
}


/****************************************************************************
     Function: ML_Disa32
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
13-Jul-2008    NCH          Initial
*****************************************************************************/
TyError ML_Disa32(TyMLHandle 	 *ptyHandle,
                  unsigned long  ulPCAddr, 
                  TyDisaResult   *pDisaResult)
{
   TyError        ErrRet   = ERR_NO_ERROR;
   unsigned long          ulDestReg;
   long           lDestAddr;
   unsigned long          ulInstruction;
   long           i;
   TyInst32Types  InstType = OTHER_INST32;
   
   pDisaResult->bCallInst = FALSE;

   // if not branch/call, next instruction is 4 bytes on
   pDisaResult->ulNextAddr     = ulPCAddr + 4;
   // next instruction still in 32-bit code
   pDisaResult->ulNextInstSize = 4;

   // if not branch/call, have no destination, indicate by copying next address
   pDisaResult->ulDestAddr     = pDisaResult->ulNextAddr;
   pDisaResult->ulDestInstSize = pDisaResult->ulNextInstSize;

   // read the instruction word
   if(ptyHandle->bBigEndian)
      ulInstruction = ML_SwapEndianWord(MLM_GetWord(ptyHandle,ulPCAddr));
   else
      ulInstruction = MLM_GetWord(ptyHandle,ulPCAddr);

   // search for this instruction in our branch table
   for (i=0; i<NUM_DISA32_TABLE_ENTRIES; i++)
      {
      if ((ulInstruction & Disa32Table[i].ulMask) == Disa32Table[i].ulOpcode)
         {
         InstType = Disa32Table[i].InstType;
         break;
         }
      }


   // if a branch, determine the destination 
   switch (InstType)
      {
      case JALR_INST32:
         // JALR is a Call Instruction
         pDisaResult->bCallInst = TRUE;
         // fall through
      case JR_INST32:
         // General register at bits 25-21 contains the destination
         ulDestReg = (ulInstruction >> 21) & 0x1F;
         // read the register contents (destination address)
         (void)MLR_ReadRegister(ptyHandle,ulDestReg, MDIMIPCPU, &pDisaResult->ulDestAddr);
         if (pDisaResult->ulDestAddr & 1)
            {
            // destination is in 16-bit code
            pDisaResult->ulDestAddr--;
            pDisaResult->ulDestInstSize = 2;
            }
         else
            {
            // destination is in 32-bit code
            pDisaResult->ulDestInstSize = 4;
            }
         pDisaResult->ulNextAddr += 4;        // allow for branch delay slot
         break;


      case JALX_INST32:
         // JALX switching modes 32-bit to 16-bit
         pDisaResult->ulDestInstSize = 2;
         // fall through
      case JAL_INST32:
         // JALX and JAL are Call Instructions
         pDisaResult->bCallInst = TRUE;
         // fall through
      case J_INST32:
         // JALX, JAL and J have 26-bit offset
         pDisaResult->ulDestAddr = (ulInstruction & 0x03FFFFFF) << 2;
         pDisaResult->ulDestAddr |= ((ulPCAddr + 4) & 0xF0000000);   //add top pc nibble
         pDisaResult->ulNextAddr += 4;        // allow for branch delay slot
         break;


      case B_REL16_INST32:
         lDestAddr = (long)ulInstruction;
         lDestAddr <<= 16; //lint !e703
         lDestAddr >>= 14; //lint !e704 !e703
         lDestAddr += (ulPCAddr + 4);  //lint !e704 !e703 !e713 !e737 // signed addition
         pDisaResult->ulDestAddr = (unsigned long)lDestAddr;
         pDisaResult->ulNextAddr += 4;        // allow for branch delay slot
         break;

      case ERET_INST32:
      case DERET_INST32:
         if (InstType == ERET_INST32)
            {
            // read the EPC register contents (execption return address)
            (void)MLR_ReadRegister(ptyHandle,
                                   ptyHandle->ulCP0RegNumberforEPC, //todo: ask?
                                   MDIMIPCP0, 
                                   &pDisaResult->ulDestAddr);
            // TO_DO: consider if/when we need to read ErrorEPC instead
            }
         else         // DERET_Inst32
            {
            // read the DEPC register contents (debug execption return address)
            (void)MLR_ReadRegister(ptyHandle,
                                   ptyHandle->ulCP0RegNumberforDEPC, //todo: ask?
                                   MDIMIPCP0, 
                                   &pDisaResult->ulDestAddr);
            }

         if (pDisaResult->ulDestAddr & 1)
            {
            // destination is in 16-bit code
            pDisaResult->ulDestAddr--;
            pDisaResult->ulDestInstSize = 2;
            }
         else
            {
            // destination is in 32-bit code
            pDisaResult->ulDestInstSize = 4;
            }
         pDisaResult->ulNextAddr += 4;        // allow for branch delay slot
         break;

      case OTHER_INST32:
      default:
         break;
      }

   return ErrRet;
}
