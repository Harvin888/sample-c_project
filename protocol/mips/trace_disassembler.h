#ifndef DISA_MIPS_H
#define DISA_MIPS_H
#include "../common/drvopxd.h"
#include "trace_procelf.h"
#include "trace.h"
#include "trace_decoder.h"

#define EOS                         '\0'
#define MAX_OPERANDS                4
#define MAX_INSTR_BYTES             4
#define NORMAL_EXCEPTION_VECTOR     0x80000180
#define BOOTSTRAP_EXCEPTION_VECTOR  0xbfc00380
#define PRIMARY_OPCODE_MAX          64

#ifndef NO_ERROR
#define NO_ERROR			0
#endif
typedef enum
	{
	ERR_F_NULL_FILE=1,
	ERR_NOT_AN_ELF_FILE,
	ERR_UNEXPECTED_END_OF_FILE,
	ERR_NOT_ABSOLUTE_REC_E,
	ERR_F_SEEK,
	ERR_IE_PROCDWRF00,
	ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY,
	ERR_INSUFFICENT_DATA_BUFFER_MEMORY,
    ELF_ADDR_LIST_NOT_FOUND,
    INVALID_MEM_LOCATION,
    MALLOC_FAILURE

	}TyDisaErrors;
typedef struct tyExtraDisaInfo
   {
  
   bool  bHasJumpDelaySlot;   // If MIPS16 instruction has a branch delay slot.
   bool  bBranchLikelyInst;   // MIPS - In the unlikely event that a branch likely
                              // event does not branch, the instruction in the 
                              // BDS is nullified. Important for trace disassembly
   bool  bMemoryReadInst;     // Not used yet
   bool  bMemoryWriteInst;    // Not used yet
   unsigned char unMemAccessSize;     // Not used yet
   
   }TyExtraDisaInfo;

typedef enum
{
   // Extra information about the instruction
   Disa_MIPS_NoExtraInfo      = 0x00000000,
   Disa_MIPS_HasJumpDelaySlot = 0x00000001,
   Disa_MIPS_BranchLikelyInst = 0x00000002,
   Disa_MIPS_MemoryLoadAccess = 0x00000004,
   Disa_MIPS_MemoryStoreAccess= 0x00000008,
   Disa_MIPS_ByteAccess       = 0x00000010,
   Disa_MIPS_HWordAccess      = 0x00000020,
   Disa_MIPS_WordAccess       = 0x00000040
} TyDisaExtraInfoBitmap;
/*  Global Enumerated Types Section        */
typedef enum { FJUMP_INST ,           // Fixed Jump      e.g. J 0x1000, Branch -50
               VJUMP_INST ,          // Variable Jump  e.g. J R0
               FCALL_INST ,          // Fixed Call     e.g. JAL 0x1000, Call 0x900
               VCALL_INST ,          // Variable Call  e.g. JAL R0, Call R2
               RET_INST   ,          // Return         e.g. J RA, Ret
               CONST_INST ,          // Constant Data Region e.g. ".const 0xA0000100"
               OTHER_INST            // Anything which isn't one of the above
             } TyInstType;

typedef union
   {
   unsigned int uFullWord;
   struct
      {
      unsigned short n0: 6;
      unsigned short n1: 5;
      unsigned short n2: 5;
      unsigned short n3: 5;
      unsigned short n4: 5;
      unsigned short n5: 6;
      };
   struct
      {
      unsigned char  ub0;
      unsigned char  ub1;
      unsigned char  ub2;
      unsigned char  ub3;
      };
   struct
      {
      unsigned short uLoHalf;
      unsigned short uHiHalf;
      };
   } TyInstruction;

typedef struct
   {
   const char  *pszName;
   void  (*pfOperand1)(TyInstruction *);
   void  (*pfOperand2)(TyInstruction *);
   void  (*pfOperand3)(TyInstruction *);
   void  (*pfOperand4)(TyInstruction *);
   } TyDisaAsmOp;

typedef struct tyDisaInfo
   {
   unsigned int            ulOpcodeMask;
   TyInstruction     Opcode;
   TyDisaAsmOp       DisaAsmOp;
   TyInstType        InstructionType;
   unsigned int            ulValidInstructionSets;
   unsigned int            ulExtraInfo;
   } TyDisaInfo;

int     Disassemble (TyTraceLineInfo* ptyTraceLineInfo,bool bNextAddr);
TyError InitDisa    (char* pcFileName);
TyError DeInitDisa  (void);

#endif 
