/****************************************************************************
       Module: trace_disassembler.cpp
     Engineer: Ranjith T.C.
  Description: disassembler for the MIPS instruction set
    Date           Initials    Description
21-Oct-2011         RTC         Initial
****************************************************************************/
/*****************************Standard Headers********************************/                  

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/***********************Application Specific Headers**************************/
#include "trace_disassembler.h"
#include "protocol/common/drvopxd.h"
#include "protocol/mips/commdefs.h"
#include "midlayer.h"
#include "trace_decoder.h"

/*****************************Extern Variables********************/
extern TyUserSettings tyCurrentUserSettings; 
extern  TyElfProcessData tyELFProcData;
//extern char szTraceElfPath[256];
/***********************Global variables**************************/
static char szOpcode[MAX_INSTR_STR_SIZE];
static char OperandArray[MAX_OPERANDS][MAX_INSTR_STR_SIZE];
static char *  pszCurrentOperand;
static unsigned int  ulGlobalCurrentPC;
static unsigned int  ulGloablDestPC;
static TyElfAddressList *ptyElfAddressList = NULL;

static const char* RegName[]={
   "$zero","$at","$v0","$v1","$a0","$a1","$a2","$a3","$t0","$t1",
   "$t2","$t3","$t4","$t5","$t6","$t7","$s0","$s1","$s2","$s3",
   "$s4","$s5","$s6","$s7","$t8","$t9","$k0","$k1","$gp","$sp",
   "$fp", "$ra"
};

static const char* FloatRegName[]={
   "$f0",  "$f1",  "$f2",  "$f3",  "$f4",  "$f5",  "$f6",  "$f7",
   "$f8",  "$f9",  "$f10", "$f11", "$f12", "$f13", "$f14", "$f15",
   "$f16", "$f17", "$f18", "$f19", "$f20", "$f21", "$f22", "$f23",
   "$f24", "$f25", "$f26", "$f27", "$f28", "$f29", "$f30", "$f31"
};

static const char* CoProcRegName[]={
   "$cp0",  "$cp1",  "$cp2",  "$cp3",  "$cp4",  "$cp5",  "$cp6",  "$cp7",
   "$cp8",  "$cp9",  "$cp10", "$cp11", "$cp12", "$cp13", "$cp14", "$cp15",
   "$cp16", "$cp17", "$cp18", "$cp19", "$cp20", "$cp21", "$cp22", "$cp23",
   "$cp24", "$cp25", "$cp26", "$cp27", "$cp28", "$cp29", "$cp30", "$cp31"
};
static const char* BRCMHWRegister[]={
   "CPUNum","SYNCI_Step","CC","CCRes","","","","","","","","","","","","",
   "","","","","","","","","PerfCnt0","PerfCnt1","PerfCnt2","PerfCnt3","",
   "UserLocal","",""
};
static const char * CacheOperations[] = {

   "Index_Invalidate_I",
   "Index_Writeback_Inv_D",
   "Index_Invalidate_T",
   "Index_Writeback_Inv_SD",
   "Index_Load_Tag_I",
   "Index_Load_Tag_D",
   "Index_Load_Tag_T",
   "Index_Load_Tag_SD",
   "Index_Store_Tag_I",
   "Index_Store_Tag_D",
   "Index_Store_Tag_T",
   "Index_Store_Tag_SD",
   " "," "," "," ",
   "Hit_Invalidate_I",
   "Hit_Invalidate_D",
   "Hit_Invalidate_T",
   "Hit_Invalidate_SD",
   "Hit_Writeback_Invalidate_I",
   "Hit_Writeback_Invalidate_D",
   "Hit_Writeback_Invalidate_T",
   "Hit_Writeback_Invalidate_SD",
   "Hit_Writeback_I",
   "Hit_Writeback_D",
   "Hit_Writeback_T",
   "Hit_Writeback_SD",
   "Fetch_Lock_I",
   "Fetch_Lock_D",
   "Fetch_Lock_T",
   "Fetch_Lock_SD"

};
static const char* BRCMDspCtrl[]={"OVF",
   "DSSL"
};

/************************************************************/

static void _o16_b21(TyInstruction * Inst)
{
   signed int    lValue;

   lValue = (signed int)((signed short)Inst->uLoHalf);
   if (lValue < 0)
   {
      // Negative Value
      sprintf(pszCurrentOperand, "-0x%X(%s)", -lValue, RegName[Inst->n4]);
   } else
   {
      // Positive Value
      sprintf(pszCurrentOperand, "0x%X(%s)", lValue, RegName[Inst->n4]);
   }

}

static void _ix16_b21(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s(%s)", RegName[Inst->n3], RegName[Inst->n4]);
}

static void _pc16(TyInstruction * Inst)
{
   char     szSymbolName[MAX_STRING_SIZE];
   // bool     bSymbolFound = FALSE;
   signed int     lv;
   //shift operation is performed on the signed value inorder to find the 18-bit offset
   lv = (signed int)Inst->uFullWord;   
   lv <<= 16;                               //lint !e701
   lv >>= 14;                               //lint !e702
   lv += ulGlobalCurrentPC + 4;             //lint !e737 !e713
   sprintf(szSymbolName,"0x%08X", lv);
   strncpy(pszCurrentOperand, szSymbolName, MAX_INSTR_STR_SIZE);
   ulGloablDestPC = lv;                     //lint !e732
}

static void _pc26(TyInstruction * Inst)
{
   char     szSymbolName[MAX_STRING_SIZE];
   // bool     bSymbolFound = FALSE;
   signed int     lv;
    //shift operation is performed on the signed value inorder to find the 18-bit offset
   lv = (signed int)Inst->uFullWord & 0x03FFFFFF;
   lv <<= 2;                                    //lint !e701
   lv |= ((ulGlobalCurrentPC + 4) & 0xF0000000);//lint !e737 !e713 //add top pc nibble to   
   sprintf(szSymbolName,"0x%08X", lv);   
   strncpy(pszCurrentOperand, szSymbolName, MAX_INSTR_STR_SIZE);
   ulGloablDestPC = lv;                         //lint !e732
}

static void _cp11(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", CoProcRegName[Inst->n2]);
}

static void _cp16(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", CoProcRegName[Inst->n3]);
}

static void _cp21(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", CoProcRegName[Inst->n4]);
}

static void _fp6(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", FloatRegName[Inst->n1]);
}

static void _fp11(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", FloatRegName[Inst->n2]);
}

static void  _fp16(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", FloatRegName[Inst->n3]);
}

static void _fp21(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", FloatRegName[Inst->n4]);
}

static void _cc8(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "$fcc%d", (Inst->n1 >> 2));
}   

static void _cc18(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "$fcc%d", (Inst->n3 >> 2));
}   
static void _r11(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", RegName[Inst->n2] );
}

static void _r16(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", RegName[Inst->n3]);
}

static void _jr21(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", RegName[Inst->n4] );
}

static void _r21(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s", RegName[Inst->n4] );
}

static void _i0_15(TyInstruction * Inst)
{
   signed int    lValue;

   lValue = (signed int)((signed short)Inst->uLoHalf);

   if (lValue < 0)
   {
      // Negative Value
      sprintf(pszCurrentOperand, "-0x%X", -lValue); 
   } else
   {
      // Positive Value
      sprintf(pszCurrentOperand, "0x%X", lValue);
   }
}


static void _ui0_15( TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", Inst->uLoHalf);
}

static void _ui6_10(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", Inst->n1);
}

static void _ui11_15(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", Inst->n2);
}

static void _ui11_15_extsize(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", Inst->n2+1);
}
static void _ui11_15_inssize(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", (Inst->n2-Inst->n1)+1);
}



static void _ui16_20(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", Inst->n3);
} 

static void _ui16_20_cache_op(TyInstruction * Inst)
{
   //if (Inst->n3 < sizeof(CacheOperations)/sizeof(char *))
   sprintf(pszCurrentOperand, CacheOperations[Inst->n3], Inst->n3);
   //else
   //  sprintf(pszCurrentOperand, "0x%X", Inst->n3);
}


static void _ui16_25(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", (Inst->uHiHalf & 0x03FF));
}

static void _ui0_2(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "0x%X", (Inst->n0 & 0x07));
}

static void _shift6(TyInstruction * Inst)
{

   sprintf(pszCurrentOperand,"0x%X",Inst->n1);
}
static void _HiLo16(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "HiLo[%X]",Inst->n3>>3);

}

static void _BRCM_DspCtrl21(TyInstruction *Inst)
{
   sprintf(pszCurrentOperand, "%s",BRCMDspCtrl[Inst->n4]);

}
static void _BRCM_DspCtrl11(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s",BRCMDspCtrl[Inst->n2]);

}
static void _BrcmHWR(TyInstruction * Inst)
{
   sprintf(pszCurrentOperand, "%s",BRCMHWRegister[Inst->n2]);

}

/************************************************************************************/
/*lint -e708*/ /*lint -e655*/  /*lint -e641*/  
static TyDisaInfo tbl_0[]= {
   { 0xFFFFFFFF,{0x00000000},{"nop",NULL,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{0x00000040},{"ssnop",NULL,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{0x00000000},{"sll",_r11,_r16,_ui6_10,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0307FF,{0x00000001},{"movf",_r11,_r21,_cc18,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},    
   { 0xFC0307FF,{0x00010001},{"movt",_r11,_r21,_cc18,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},    
   { 0xFFE0003F,{0x00000002},{"srl",_r11, _r16, _ui6_10,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{0x00200002},{"rotr",_r11,_r16,_ui6_10,NULL},OTHER_INST,Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},  //SmartMips
   { 0xFFE0003F,{0x00000003},{"sra",_r11,_r16,_ui6_10,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{0x00000004},{"sllv",_r11,_r16,_r21,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F,{0x00000005},{"selsl",_r11,_r21,_r16,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo},    
   { 0xFC0007FF,{0x00000006},{"srlv",_r11,_r16,_r21,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x00000046},{"rotrv",_r11,_r16,_r21,NULL},OTHER_INST,Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},  //SmartMips
   { 0xFC0007FF,{0x00000007},{"srav",_r11,_r16,_r21,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1FF83F,{0x00000008},{"jr",_jr21,NULL,NULL,NULL},RET_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F003F,{0x00000009},{"jalr",_r11,_jr21,NULL,NULL}, VCALL_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x0000000A},{"movz",_r11,_r21,_r16,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x0000000B},{"movn",_r11,_r21,_r16,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x0000000C},{"syscall",NULL,NULL,NULL,NULL},FJUMP_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x0000000D},{"break",NULL,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFF83F,{0x0000000F},{"sync",NULL,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF07FF,{0x00000010},{"mfhi",_r11,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFFF003F,{0x00000010},{"mfhi",_r11,_ui6_10,NULL,NULL},OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC1FFFFF,{0x00000011},{"mthi",_r21,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC1FFF3F,{0x00000011},{"mthi",_r21,_ui6_10,NULL,NULL},OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFFF07FF,{0x00000012},{"mflo",_r11,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFFFF003F,{0x00000012},{"mflo",_r11,_ui6_10,NULL,NULL},OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFFF07FF,{0x00000052},{"mflhxu",_r11,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},  // SmartMips
   { 0xFC1FFFFF,{0x00000013},{"mtlo",_r21 ,NULL,NULL,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1FFF3F,{0x00000013},{"mtlo",_r21,_ui6_10,NULL,NULL},OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC1FFFFF,{0x00000053},{"mtlhx",_r21,NULL,NULL,NULL},OTHER_INST,Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},  // SmartMips
   { 0xFC0007FF,{0x00000014},{"dsllv",  _r11, _r16, _r21,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0xFC0007FF,{0x00000016},{"dsrlv",  _r11, _r16, _r21,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0xFC0007FF,{0x00000017},{"dsrav",  _r11, _r16, _r21 ,NULL},OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0xFC00FFFF,{0x00000018},{"mult",         _r21, _r16,NULL,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00FFFF,{0x00000019},{"multu",        _r21, _r16,NULL,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00FFFF,{0x00000459},{"multp",        _r21, _r16 ,NULL ,NULL}, OTHER_INST, Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},  // SmartMips
   { 0xFC00FFFF,{0x0000001A},{"div",          _r21, _r16 ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00FFFF,{0x0000001B},{"divu",         _r21, _r16 ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00FFFF,{0x0000001C},{"dmult",        _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0xFC00FFFF,{0x0000001D},{"dmultu",       _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0xFC00FFFF,{0x0000001E},{"ddiv",         _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},    
   { 0xFC00FFFF,{0x0000001F},{"ddivu",        _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},    
   { 0xFC0007FF,{0x00000020},{"add",    _r11, _r21, _r16 ,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x00000021},{"addu",   _r11, _r21, _r16 ,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{0x00000022},{"sub",    _r11, _r21, _r16 ,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x00000023},{"subu",   _r11, _r21, _r16 ,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x00000024},{"and",    _r11, _r21, _r16 ,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x00000025},{"or",     _r11, _r21, _r16 ,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},  
   { 0xFC0007FF,{0x00000026},{"xor",    _r11, _r21, _r16 ,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x00000027},{"nor",    _r11, _r21, _r16 ,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x00000028},{"madd16",       _r21, _r16 ,NULL ,NULL}, OTHER_INST, (unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo},    
   { 0xFC00003F,{0x00000029},{"dmadd16",      _r21, _r16 ,NULL ,NULL}, OTHER_INST, (unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo},    
   { 0xFC0007FF,{0x0000002A},{"slt",    _r11, _r21, _r16 ,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x0000002B},{"sltu",   _r11, _r21, _r16 ,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{0x0000002C},{"dadd",   _r11, _r21, _r16 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFC0007FF,{0x0000002D},{"daddu",  _r11, _r21, _r16 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFC0007FF,{0x0000002E},{"dsub",   _r11, _r21, _r16 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFC0007FF,{0x0000002F},{"dsubu",  _r11, _r21, _r16 ,NULL},OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFC00003F,{0x00000030},{"tge",    _r21, _r16  ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x00000031},{"tgeu",   _r21, _r16  ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x00000032},{"tlt",    _r21, _r16  ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x00000033},{"tltu",   _r21, _r16 ,NULL,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x00000034},{"teq",    _r21, _r16 ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{0x00000036},{"tne",    _r21, _r16 ,NULL,NULL},OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFE0003F,{0x00000038},{"dsll",   _r11, _r16, _ui6_10 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFFE0003F,{0x0000003A},{"dsrl",   _r11, _r16, _ui6_10 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFFE0003F,{0x0000003B},{"dsra",   _r11, _r16, _ui6_10 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFFE0003F,{0x0000003C},{"dsll32", _r11, _r16, _ui6_10 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFFE0003F,{0x0000003E},{"dsrl32", _r11, _r16, _ui6_10 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0xFFE0003F,{0x0000003F},{"dsra32", _r11, _r16, _ui6_10 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_1[]=
{{ 0xFC1F0000, {0x04000000}, {"bltz",   _r21,       _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x04010000}, {"bgez",   _r21,       _pc16 ,NULL,NULL},  FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x04020000}, {"bltzl",  _r21,       _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0xFC1F0000, {0x04030000}, {"bgezl",  _r21,       _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0xFC1F0000,{ 0x04080000}, {"tgei",   _r21,       _i0_15,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000, {0x04090000}, {"tgeiu",  _r21,       _i0_15,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x040A0000}, {"tlti",   _r21,       _i0_15 ,NULL,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x040B0000}, {"tltiu",  _r21,       _i0_15 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x040C0000}, {"teqi",   _r21,       _i0_15 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x040E0000}, {"tnei",   _r21,       _i0_15 ,NULL,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x04100000}, {"bltzal", _r21,       _pc16  ,NULL,NULL},  FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x04110000}, {"bgezal", _r21,       _pc16 ,NULL,NULL},  FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC1F0000,{ 0x04120000}, {"bltzall",_r21,       _pc16 ,NULL,NULL},  FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0xFC1F0000,{ 0x04130000}, {"bgezall",_r21,       _pc16 ,NULL,NULL},  FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst},
   { 0xFC1F0000,{ 0x041F0000}, {"synci"  ,_o16_b21  ,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_BranchLikelyInst},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_2[]=
{{0xFC000000, {0x08000000}, {"j",                 _pc26, NULL, NULL, NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_3[]=
{{ 0xFC000000,{ 0x0C000000}, {"jal",               _pc26 ,NULL,NULL,NULL}, FCALL_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_4[]=
{{ 0xFC000000,{ 0x10000000}, {"beq",    _r21, _r16, _pc16,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_5[]=
{{ 0xFC000000,{ 0x14000000}, {"bne",    _r21, _r16, _pc16,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_6[]=
{{ 0xFC1F0000, {0x18000000}, {"blez",    _r21,      _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_7[]=
{{ 0xFC1F0000,{ 0x1C000000}, {"bgtz",    _r21,      _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_8[]=
{{ 0xFC000000,{ 0x20000000}, {"addi",    _r16, _r21, _i0_15 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_9[]=
{{ 0xFC000000, {0x24000000}, {"addiu",   _r16, _r21, _i0_15 ,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_10[]=
{{ 0xFC000000, {0x28000000}, {"slti",    _r16, _r21, _i0_15,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_11[]=
{{ 0xFC000000, {0x2C000000}, {"sltiu",   _r16, _r21, _i0_15 ,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_12[]=
{{ 0xFC000000,{ 0x30000000}, {"andi",    _r16, _r21, _ui0_15,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_13[]=
{{ 0xFC000000, {0x34000000}, {"ori",     _r16, _r21, _ui0_15,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_14[]=
{{ 0xFC000000, {0x38000000}, {"xori",    _r16, _r21, _ui0_15,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_15[]=
{{ 0xFFE00000, {0x3C000000}, {"lui",     _r16,       _ui0_15,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_16[]=
{   { 0xFFE007FF,{0x40000000}, {"mfc0",   _r16, _cp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007F8,{ 0x40000000}, {"mfc0",   _r16, _cp11, _ui0_2 ,NULL},OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x40200000}, {"dmfc0",  _r16, _cp11  ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007F8,{ 0x40200000}, {"dmfc0",  _r16, _cp11, _ui0_2 ,NULL},OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE00000,{ 0x40400000}, {"cfc0",   _r16, _cp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF, {0x40800000}, {"mtc0",   _r16, _cp11  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007F8,{ 0x40800000}, {"mtc0",   _r16, _cp11, _ui0_2,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x40A00000}, {"dmtc0",  _r16, _cp11 ,NULL,NULL}, OTHER_INST,Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007F8,{ 0x40A00000}, {"dmtc0",  _r16, _cp11, _ui0_2 ,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE00000,{ 0x40C00000}, {"ctc0",   _r16, _cp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x42000001}, {"tlbr" ,NULL,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x42000002}, {"tlbwi"  ,NULL,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x42000006}, {"tlbwr"  ,NULL,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x42000008}, {"tlbp"  ,NULL,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x42000010}, {"rfe" ,NULL,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_II,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x42000018}, {"eret"  ,NULL,NULL,NULL,NULL},RET_INST,  Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFFFFFF,{ 0x4200001F}, {"deret" ,NULL,NULL,NULL,NULL}, RET_INST,   Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x42000020}, {"wait"  ,NULL,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x42000021}, {"standby",NULL,NULL,NULL,NULL}, OTHER_INST,(unsigned int) Disa_MIPS_All,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x42000022}, {"suspend" ,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int) Disa_MIPS_All,Disa_MIPS_NoExtraInfo},
   { 0xFFFF0000,{ 0x41000000}, {"bc0f",               _pc16 ,NULL,NULL,NULL}, FJUMP_INST,Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x41010000}, {"bc0t",               _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x41020000}, {"bc0fl",              _pc16 ,NULL,NULL,NULL},FJUMP_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x41030000}, {"bc0tl",              _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo},
   { 0xFFE0FFFF,{ 0x41606000}, {"di" ,   _r16 ,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0FFFF,{ 0x41606020}, {"ei" ,   _r16  ,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE003FF,{ 0x41400000}, {"rdpgpr" ,   _r11,   _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE003FF,{ 0x41C00000}, {"wrpgpr" ,   _r11,   _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_17[]={
   { 0xFFE007FF,{ 0x44000000}, {"mfc1",      _r16, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x44200000}, {"dmfc1",     _r16, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x44400000}, {"cfc1",      _r16, _fp11  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x44800000}, {"mtc1",      _r16, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF, {0x44A00000}, {"dmtc1",     _r16, _fp11,NULL,NULL},OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x44C00000}, {"ctc1",      _r16, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x45000000}, {"bc1f",            _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE30000,{ 0x45000000}, {"bc1f",      _cc18,_pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000, {0x45010000}, {"bc1t",            _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE30000,{ 0x45010000}, {"bc1t",      _cc18,_pc16,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x45020000}, {"bc1fl",           _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE30000,{ 0x45020000}, {"bc1fl",     _cc18,_pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x45030000}, {"bc1tl",           _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE30000,{ 0x45030000}, {"bc1tl",     _cc18,_pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000000}, {"add.s",     _fp6, _fp11, _fp16 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000001}, {"sub.s",     _fp6, _fp11, _fp16 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000002}, {"mul.s",     _fp6, _fp11, _fp16 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46000003}, {"div.s",     _fp6, _fp11, _fp16 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46000004}, {"sqrt.s",    _fp6, _fp11,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000005}, {"abs.s",     _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000006}, {"mov.s",     _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000007}, {"neg.s",     _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{0x46000008}, {"round.l.s", _fp6, _fp11 ,NULL,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000009}, {"trunc.l.s", _fp6, _fp11,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600000A}, {"ceil.l.s",  _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600000B}, {"floor.l.s", _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600000C}, {"round.w.s", _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600000D}, {"trunc.w.s", _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600000E}, {"ceil.w.s",  _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600000F}, {"floor.w.s", _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE3003F,{ 0x46000011}, {"movf.s",    _fp6, _fp11, _cc18,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE3003F,{ 0x46010011}, {"movt.s",    _fp6, _fp11, _cc18,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000012}, {"movz.s",    _fp6, _fp11, _r16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000013}, {"movn.s",    _fp6, _fp11, _r16 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000015}, {"recip.s",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000016}, {"rsqrt.s",   _fp6, _fp11,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000021}, {"cvt.d.s",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46000024}, {"cvt.w.s",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000025}, {"cvt.l.s",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFE0003F,{ 0x46000030}, {"c.f.s",     _cc8, _fp11, _fp16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000031}, {"c.un.s",    _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000032}, {"c.eq.s",    _cc8, _fp11, _fp16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000033}, {"c.ueq.s",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000034}, {"c.olt.s",   _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46000035}, {"c.ult.s",   _cc8, _fp11, _fp16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000036}, {"c.ole.s",   _cc8, _fp11, _fp16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000037}, {"c.ule.s",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFE0003F,{ 0x46000038}, {"c.sf.s",    _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46000039}, {"c.ngle.s",  _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600003A}, {"c.seq.s",   _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600003B}, {"c.ngl.s",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600003C}, {"c.lt.s",    _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600003D}, {"c.nge.s",   _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x4600003E}, {"c.le.s",    _cc8, _fp11, _fp16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4600003F}, {"c.ngt.s",   _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200000}, {"add.d",     _fp6, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200001}, {"sub.d",     _fp6, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200002}, {"mul.d",     _fp6, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46200003}, {"div.d",     _fp6, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200004}, {"sqrt.d",    _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46200005}, {"abs.d",     _fp6, _fp11  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200006}, {"mov.d",     _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200007}, {"neg.d",     _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200008}, {"round.l.d", _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200009}, {"trunc.l.d", _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620000A}, {"ceil.l.d",  _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620000B}, {"floor.l.d", _fp6, _fp11,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x4620000C}, {"round.w.d", _fp6, _fp11,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620000D}, {"trunc.w.d", _fp6, _fp11,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620000E}, {"ceil.w.d",  _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620000F}, {"floor.w.d", _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE3003F,{ 0x46200011}, {"movf.d",    _fp6, _fp11, _cc18,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE3003F,{ 0x46210011}, {"movt.d",    _fp6, _fp11, _cc18,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFE0003F,{ 0x46200012}, {"movz.d",    _fp6, _fp11, _r16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200013}, {"movn.d",    _fp6, _fp11, _r16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200015}, {"recip.d",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46200016}, {"rsqrt.d",   _fp6, _fp11,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46200020}, {"cvt.s.d",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200024}, {"cvt.w.d",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200025}, {"cvt.l.d",   _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFE0003F,{ 0x46200030}, {"c.f.d",     _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200031}, {"c.un.d",    _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x46200032}, {"c.eq.d",    _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200033}, {"c.ueq.d",   _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200034}, {"c.olt.d",   _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200035}, {"c.ult.d",   _cc8, _fp11, _fp16,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200036}, {"c.ole.d",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46200037}, {"c.ule.d",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFFE0003F,{ 0x46200038}, {"c.sf.d",    _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620003A}, {"c.seq.d",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620003B}, {"c.ngl.d",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x4620003C}, {"c.lt.d",    _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x4620003D}, {"c.nge.d",   _cc8, _fp11, _fp16 ,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x4620003E}, {"c.le.d",    _cc8, _fp11, _fp16,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F, {0x4620003F}, {"c.ngt.d",   _cc8, _fp11, _fp16 ,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46800020}, {"cvt.s.w",   _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46800021}, {"cvt.d.w",   _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46A00020}, {"cvt.s.l",   _fp6, _fp11 ,NULL,NULL},  OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE0003F,{ 0x46A00021}, {"cvt.d.l",   _fp6, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE003FF,{ 0x44E00000}, {"mthc1",     _r16, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_BranchLikelyInst},
   { 0xFFE003FF,{ 0x44600000}, {"mfhc1",     _r16, _fp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_BranchLikelyInst},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_18[]={
   { 0xFFE007FF, {0x48000000}, {"mfc2",     _r16, _cp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007F8,{ 0x48000000}, {"mfc2",     _r16, _cp11, _ui0_2,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF, {0x48400000}, {"cfc2",     _r16, _cp11,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x48800000}, {"mtc2",     _r16, _cp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007F8,{ 0x48800000}, {"mtc2",     _r16, _cp11, _ui0_2,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF, {0x48C00000}, {"ctc2",     _r16, _cp11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x49000000}, {"bc2f",            _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE30000, {0x49000000}, {"bc2f",     _cc18, _pc16,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000, {0x49010000}, {"bc2t",            _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE30000,{ 0x49010000}, {"bc2t",     _cc18, _pc16,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFFFF0000,{ 0x49020000}, {"bc2fl",           _pc16 ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0xFFE30000,{ 0x49020000}, {"bc2fl",    _cc18, _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0xFFFF0000, {0x49030000}, {"bc2tl",           _pc16   ,NULL,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst},
   { 0xFFE30000, {0x49030000}, {"bc2tl",    _cc18, _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0xFFE00000,{ 0x48E00000}, {"mthc2",     _r16,_ui0_15  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_BranchLikelyInst},
   { 0xFFE00000,{ 0x48600000}, {"mfhc2",     _r16,_ui0_15  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_BranchLikelyInst},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_19[]={
   { 0xFC00F83F,{ 0x4C000000}, {"lwxc1",     _fp6, _ix16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00F83F,{ 0x4C000001}, {"ldxc1",     _fp6, _ix16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{ 0x4C000008}, {"swxc1",     _fp11,_ix16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF, {0x4C000009}, {"sdxc1",     _fp11,_ix16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC0007FF,{ 0x4C00000F}, {"prefx",     _ui11_15, _ix16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000020}, {"madd.s",    _fp6, _fp21, _fp11, _fp16}, OTHER_INST,Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000021}, {"madd.d",    _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000028}, {"msub.s",    _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000029}, {"msub.d",    _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000030}, {"nmadd.s",   _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F, {0x4C000031}, {"nmadd.d",   _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000038}, {"nmsub.s",   _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0xFC00003F,{ 0x4C000039}, {"nmsub.d",   _fp6, _fp21, _fp11, _fp16}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_20[]={
   { 0xFC000000,{ 0x50000000}, {"beql",      _r21, _r16, _pc16,NULL}, FJUMP_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_21[]={
   { 0xFC000000, {0x54000000}, {"bnel",      _r21, _r16, _pc16 ,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_22[]=
{{ 0xFC1F0000,{ 0x58000000}, {"blezl",     _r21,       _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_23[]=
{{ 0xFC1F0000,{ 0x5C000000}, {"bgtzl",     _r21,       _pc16 ,NULL,NULL}, FJUMP_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_BranchLikelyInst}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_24[]=
{   { 0xFC000000, {0x60000000}, {"daddi",     _r16, _r21, _i0_15,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_25[]=
{   { 0xFC000000, {0x64000000}, {"daddiu",    _r16, _r21, _i0_15,NULL},OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_26[]=
{   { 0xFC000000, {0x68000000}, {"ldl",       _r16, _o16_b21 ,NULL,NULL},OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_27[]=
{   { 0xFC000000, {0x6C000000}, {"ldr",       _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_28[]=
{   { 0xFC00FFFF, {0x70000000}, {"madd",            _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFC00FF3F, {0x70000000}, {"madd",      _r21, _r16,_ui6_10,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00FFFF, {0x70000001}, {"maddu",           _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFC00FF3F, {0x70000001}, {"maddu",     _r21, _r16,_ui6_10,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00FFFF, {0x70000441}, {"maddp",           _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},         // SmartMips
   { 0xFC00FFFF,{0x70000481}, {"pperm",           _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},         // SmartMips
   { 0xFC0007FF, {0x70000002}, {"mul",       _r11, _r21, _r16,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00FFFF, {0x70000004}, {"msub",            _r21, _r16,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFC00FF3F,{ 0x70000004}, {"msub",      _r21, _r16,_ui6_10 ,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00FFFF,{ 0x70000005}, {"msubu",           _r21, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFC00FF3F, {0x70000005}, {"msubu",     _r21, _r16,_ui6_10,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x70000088}, {"lwxs",      _r11, _ix16_b21 ,NULL,NULL},OTHER_INST, Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},         // SmartMips
   { 0xFC0007FF,{ 0x70000020}, {"clz",       _r11, _r21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x70000021}, {"clo",       _r11, _r21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x70000024}, {"dclz",      _r11, _r21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x70000025}, {"dclo",      _r11, _r21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x70000030}, {"min",       _r11, _r21,_r16  ,NULL}, OTHER_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo},  // PR4450 extended instruction
   { 0xFC0007FF,{ 0x70000031}, {"max",       _r11, _r21,_r16,NULL}, OTHER_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo},  // PR4450 extended instruction
   { 0xFC0007FF,{ 0x70000034}, {"abs_sub",   _r11, _r21,_r16 ,NULL}, OTHER_INST, Disa_MIPS_32,Disa_MIPS_NoExtraInfo},  // PR4450 extended instruction
   { 0xFC00003F, {0x7000003F}, {"sdbbp",     _ui16_25 ,NULL,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00000F,{ 0x7000000A}, {"mfdcr",_r11, _BRCM_DspCtrl21 ,NULL,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00000F,{ 0x7000000B}, {"mtdcr",_r16, _BRCM_DspCtrl11 ,NULL,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F, {0x70000018}, {"adds",_cp11,_cp21,_cp16 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0000FF, {0x7000001C}, {"subs",_cp11,_cp21,_cp16 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F,{ 0x70000008}, {"abss",_cp11,_cp16 ,NULL ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F, {0x70000017}, {"sathilo",_ui6_10  ,NULL ,NULL,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F, {0x70000019}, {"slas",_cp11,_cp16,_shift6,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F,{ 0x7000001d}, {"slavs",_cp11,_cp16,_cp21 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000004C}, {"maddx.ll",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000008C}, {"maddx.hh",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700000CC}, {"maddx.d",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x7000014C}, {"maddx.lh",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000018C}, {"maddx.hl",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x700001CC}, {"maddx.dx",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x7000044C}, {"maddx.sll",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000048C}, {"maddx.shh",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700004CC}, {"maddx.sd",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x7000054C}, {"maddx.slh",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000058C}, {"maddx.shl",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700005CC}, {"maddx.sdx",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000064C}, {"maddx.qll",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000068C}, {"maddx.qhh",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700006CC}, {"maddx.qd",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000074C}, {"maddx.qlh",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000078C}, {"maddx.qhl",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700007CC}, {"maddx.qdx",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000005C}, {"msubx.ll",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000009C}, {"msubx.hh",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x700000DC}, {"msubx.d",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x7000015C}, {"msubx.lh",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000019C}, {"msubx.hl",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700001DC}, {"msubx.dx",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x7000045C}, {"msubx.sll",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000049C}, {"msubx.shh",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700004DC}, {"msubx.sd",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000055C}, {"msubx.slh",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000059C}, {"msubx.shl",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700005DC}, {"msubx.sdx",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000065C}, {"msubx.qll",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000069C}, {"msubx.qhh",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700006DC}, {"msubx.qd",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF, {0x7000075C}, {"msubx.qlh",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x7000079C}, {"msubx.qhl",_cp21,_cp16,_ui11_15,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC0007FF,{ 0x700007DC}, {"msubx.qdx",_cp21,_cp16,_ui11_15 ,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_29[]=
{{ 0xFC000000,{ 0x74000000}, {"jalx",              _pc26,NULL,NULL,NULL}, FCALL_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_30[]=
{   { 0xFC000000,{ 0x78000000}, {"undef" ,NULL,NULL,NULL,NULL}, CONST_INST, (unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_31[]=
{   { 0xFC00003F, {0x7C000000}, {"ext",   _r16, _r21,_ui6_10,_ui11_15_extsize}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFC00003F,{ 0x7C000004}, {"ins",   _r16, _r21,_ui6_10,_ui11_15_inssize}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x7C000420}, {"seb",   _r11, _r16 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x7C000620}, {"seh",   _r11, _r16  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF,{ 0x7C0000A0}, {"wsbh",  _r11, _r16  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo}, 
   { 0xFFE007FF, {0x7C00003b}, {"rdhwr", _r16, _BrcmHWR  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0xFC000000, {0x7C000000}, {"undef" ,NULL,NULL,NULL,NULL},  CONST_INST, (unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_32[]=
{{ 0xFC000000,{ 0x80000000}, {"lb",   _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_33[]=
{{ 0xFC000000, {0x84000000}, {"lh",   _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_34[]=
{{ 0xFC000000,{ 0x88000000}, {"lwl",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_35[]=
{{ 0xFC000000,{ 0x8C000000}, {"lw",   _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_36[]=
{{ 0xFC000000, {0x90000000}, {"lbu",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_37[]=
{{ 0xFC000000,{ 0x94000000}, {"lhu",  _r16, _o16_b21,NULL,NULL}, OTHER_INST,Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_38[]=
{{ 0xFC000000, {0x98000000}, {"lwr",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_39[]=
{{ 0xFC000000,{ 0x9C000000}, {"lwu",  _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_40[]=
{{ 0xFC000000, {0xA0000000}, {"sb",   _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_41[]=
{{ 0xFC000000, {0xA4000000}, {"sh",   _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_42[]=
{{ 0xFC000000, {0xA8000000}, {"swl",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_43[]=
{{ 0xFC000000, {0xAC000000}, {"sw",   _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_44[]=
{{ 0xFC000000,{ 0xB0000000}, {"sdl",  _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_45[]=
{{ 0xFC000000,{ 0xB4000000}, {"sdr",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_46[]=
{{ 0xFC000000,{ 0xB8000000}, {"swr",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_47[]=
{{ 0xFC000000,{ 0xBC000000}, {"cache",_ui16_20_cache_op, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_48[]=
{{ 0xFC000000, {0xC0000000}, {"ll",   _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_49[]=
{{ 0xFC000000, {0xC4000000}, {"lwc1", _fp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_50[]=
{{ 0xFC000000, {0xC8000000}, {"lwc2", _cp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_51[]=
{{ 0xFC000000, {0xCC000000}, {"pref", _ui16_20, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_52[]=
{{ 0xFC000000, {0xD0000000}, {"lld",  _r16, _o16_b21,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_53[]=
{{ 0xFC000000, {0xD4000000}, {"ldc1", _fp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},    
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_54[]=
{{ 0xFC000000, {0xD8000000}, {"ldc2", _cp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},    
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_55[]=
{{ 0xFC000000, {0xDC000000}, {"ld",   _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_56[]=
{{ 0xFC000000, {0xE0000000}, {"sc",   _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo}, 
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_57[]=
{{ 0xFC000000, {0xE4000000}, {"swc1",  _fp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_58[]=
{{ 0xFC000000, {0xE8000000}, {"swc2", _cp16, _o16_b21  ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_4KSc,Disa_MIPS_NoExtraInfo},
   { 0xFC000000, {0xE8000000}, {"swc2",_HiLo16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_59[]=
{  { 0xFFE007FF,{ 0xEC00003b}, {"brdhwr", _r16, _BrcmHWR  ,NULL,NULL}, OTHER_INST,Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},
   { 0xFC000000, {0xEC000000}, {"undef" ,NULL,NULL,NULL,NULL}, CONST_INST, (unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo},
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_60[]=
{{ 0xFC000000, {0xF0000000}, {"scd",  _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},    
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_61[]=
{{ 0xFC000000, {0xF4000000}, {"sdc1",  _fp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_62[]=
{{ 0xFC000000, {0xF8000000}, {"sdc2", _cp16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_32|Disa_MIPS_BRCM,Disa_MIPS_NoExtraInfo},    
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

static TyDisaInfo tbl_63[]=
{{ 0xFC000000, {0xFC000000}, {"sd",   _r16, _o16_b21 ,NULL,NULL}, OTHER_INST, Disa_MIPS_64,Disa_MIPS_NoExtraInfo},   
   { 0x00000000,{0x00000000},{NULL,NULL,NULL,NULL,NULL},OTHER_INST,(unsigned int)Disa_MIPS_All,Disa_MIPS_NoExtraInfo}
};

/************************************************************************************/

static TyDisaInfo *ppDisaInfoTable[]={
   tbl_0,  tbl_1,  tbl_2,  tbl_3,  tbl_4,  tbl_5,  tbl_6,  tbl_7,
   tbl_8,  tbl_9,  tbl_10, tbl_11, tbl_12, tbl_13, tbl_14, tbl_15,
   tbl_16, tbl_17, tbl_18, tbl_19, tbl_20, tbl_21, tbl_22, tbl_23,
   tbl_24, tbl_25, tbl_26, tbl_27, tbl_28, tbl_29, tbl_30, tbl_31,
   tbl_32, tbl_33, tbl_34, tbl_35, tbl_36, tbl_37, tbl_38, tbl_39, 
   tbl_40, tbl_41, tbl_42, tbl_43, tbl_44, tbl_45, tbl_46, tbl_47,
   tbl_48, tbl_49, tbl_50, tbl_51, tbl_52, tbl_53, tbl_54, tbl_55,
   tbl_56, tbl_57, tbl_58, tbl_59, tbl_60, tbl_61, tbl_62, tbl_63,
   NULL};
/*******************************************************************
     Function: Disassemble
     Engineer: Ranjith T C 
        Input: pDisaInst *: pointer to storage for found instruction
              
       Output: TyError: NO_ERROR or error number
  Description: Disassembles and returns instruction at specified location.
               Sets all members of pDisaInst.
 Date           Initials    Description
 21-Oct-2011      RTC         Initial
********************************************************************/
int Disassemble(TyTraceLineInfo* ptyTraceLineInfo   ,bool bNextAddr)
{
   int            i;
   const char          *pszSep;  // Seperator
   unsigned int ulMemoryLocation=0;
   unsigned int         ulRequiredInstructionSet=false;
   TyDisaInfo*    pDisaInfo=NULL;
   TyDisaAsmOp    DisassembleOp;
   TyInstruction  Instruction;    
   TyElfAddressList *ptyTempList = ptyElfAddressList;
   Instruction.uFullWord=0;     

   /*TODO: Check if swapping is required*/
   //ulGlobalCurrentPC = ML_SwapEndianWord(ptyTraceLineInfo->ulPC);
   ulGlobalCurrentPC = ptyTraceLineInfo->ulPC;
   szOpcode[0] = EOS;
   if (tyCurrentUserSettings.bZephyrTraceSupport==TRUE)
   {
      ulRequiredInstructionSet = (unsigned int)Disa_MIPS_BRCM;   
   }
   else
   {
      ulRequiredInstructionSet = (unsigned int)((unsigned int)Disa_MIPS_All -(unsigned int) Disa_MIPS_4KSc);
   }
   for (i=0; i<MAX_OPERANDS; ++i)
   {
      OperandArray[i][0] = EOS;
   }
   pszCurrentOperand = OperandArray[0];    
   ulMemoryLocation = 0;
   if (NULL == ptyTempList)
   {
      //return error No Elf Address list found
      return ELF_ADDR_LIST_NOT_FOUND;
   } 
   else
   {
      do
      {
         if (ulGlobalCurrentPC >= ptyTempList->ulStartAddress 
             && ulGlobalCurrentPC <= ptyTempList->ulEndAddress)
         {
            ulMemoryLocation = ptyTempList->ulMappedAddress + 
                               (ulGlobalCurrentPC - 
                                ptyTempList->ulStartAddress);
            Instruction.uFullWord = *(unsigned int *)ulMemoryLocation;
            break;
         }
         ptyTempList = ptyTempList->tyNextNode;
      } while (NULL != ptyTempList);
      if (!ulMemoryLocation)
      {
        //return error on invalid memory location
        strcpy((char *)ptyTraceLineInfo->ucInstruction,"outside scope of current program");
        ptyTraceLineInfo->ulOpcode=0x0;
        ptyTraceLineInfo->ulNextPC  =0x0;
        return NO_ERROR;
       // return INVALID_MEM_LOCATION;
      }
   }
   if (tyELFProcData.bBigEndian)
   {
      Instruction.uFullWord = ML_SwapEndianWord( Instruction.uFullWord );
   }

   // get the table corresponding to the primary opcode
   pDisaInfo = ppDisaInfoTable[Instruction.n5];

   // for each entry in this table
   while (pDisaInfo->ulOpcodeMask != 0)
   {
      // check for a matching instruction subset and opcode
      if (((pDisaInfo->ulValidInstructionSets & ulRequiredInstructionSet) !=0) &&
          (pDisaInfo->Opcode.uFullWord == (Instruction.uFullWord & pDisaInfo->ulOpcodeMask)))
      {
         // have found a matching opcode          
         DisassembleOp   = pDisaInfo->DisaAsmOp;
         strcpy(szOpcode, DisassembleOp.pszName);          
         //Operand1
         if (DisassembleOp.pfOperand1)
         {
            DisassembleOp.pfOperand1(&Instruction);
            pszCurrentOperand = OperandArray[1];

            //Operand2
            if (DisassembleOp.pfOperand2)
            {
               DisassembleOp.pfOperand2(&Instruction);
               pszCurrentOperand = OperandArray[2];

               //Operand3
               if (DisassembleOp.pfOperand3)
               {
                  DisassembleOp.pfOperand3(&Instruction);
                  pszCurrentOperand = OperandArray[3];

                  //Operand4
                  if (DisassembleOp.pfOperand4)
                  {
                     DisassembleOp.pfOperand4(&Instruction);
                  }
               }
            }
         }
         break;
      }
      pDisaInfo++;
   }
   if ((szOpcode[0] == EOS) || (strcmp(szOpcode, "undef") == 0))
   {
      sprintf(szOpcode, ".long");
      sprintf(OperandArray[0], "0x%08X", Instruction.uFullWord);
      OperandArray[1][0] = EOS;
   }
   strcpy((char *)ptyTraceLineInfo->ucInstruction, szOpcode);
   pszSep = " ";

   for (i=0; i<MAX_OPERANDS; ++i)
   {
      if (OperandArray[i][0] == EOS)
         break;
      strcat((char *)ptyTraceLineInfo->ucInstruction, pszSep);
      strcat((char *)ptyTraceLineInfo->ucInstruction, OperandArray[i]);
      pszSep = ",";
   }
   ptyTraceLineInfo->ulOpcode=Instruction.uFullWord;  
   if ((unsigned int)bNextAddr==TRUE)
      ptyTraceLineInfo->ulNextPC  =  ulGloablDestPC;

   return NO_ERROR;   
}

/*******************************************************************
     Function: InitDisa
     Engineer: Ranjith T C 
        Input: Nil
       Output: TyError: NO_ERROR or error number
  Description: Initializes the disassembler.
               Sets all members of ptyElfAddressList.
Date           Initials    Description
21-Oct-2011      RTC         Initial
********************************************************************/

TyError InitDisa(char* pcFileName)
{
   TyError ErrRet = NO_ERROR;

   ErrRet = ProcessELFFile(pcFileName, &ptyElfAddressList);

   return ErrRet;
}

/*******************************************************************
     Function: DeInitDisa
     Engineer: Ranjith T C 
        Input: Nil
              
       Output: TyError: NO_ERROR or error number
  Description: Clean up the disassembler.               
Date           Initials    Description
21-Oct-2011      RTC         Initial
********************************************************************/
TyError DeInitDisa(void)
{
   TyError ErrRet = NO_ERROR;
   TyElfAddressList *ptyTempList;
   (void)DeInitELF();
   while (ptyElfAddressList!=NULL)
      {
      ptyTempList=ptyElfAddressList;
      ptyElfAddressList=ptyElfAddressList->tyNextNode;
      free(ptyTempList);

      }
   ptyElfAddressList=NULL;
   return ErrRet;
}
