#ifndef ASH_DMA_h__
#define ASH_DMA_h__

/*=========================================================
Description of ASH_DMA APIs used in this test application.
===========================================================
1. 	API name 		: wdmaOpenConnection
	Function 		: Configure the target using AshMdi.cfg file
	Usage 	 		: int wdmaOpenConnection()
	Return Value 	: 0 : Success
					 -1 : Fail

2. API name 		: wdmaCloseConnection
   Function 		: Closes connection to the target
   Usage			: int wdmaCloseConnection()
   Return Value 	: 0 : Success
					 -1 : Fail

3. API Name			:wdmaWriteTargetMemoryLong
   Function 		: Write to target physical memory
   Usage 	 		:int wdmaWriteTargetMemoryLong(unsigned int uiAddress,
											       unsigned int  * puiData,
											       unsigned int  uiLen)
   parameters		: uiAddress : Physical address to load data.
				 	  puiData   :Pointer to data to load.
					  uiLen		:Block length to write data.
   Return Value 	: 0 : Success
					 -1 : Fail

4. API Name			:wdmaReadTargetMemoryLong
   Function 		:Read from target physical memory
   Usage 	 		:int wdmaReadTargetMemoryLong(unsigned int uiAddress,
											       unsigned int  * puiData,
											       unsigned int  uiLen)
   parameters		: uiAddress : Physical address to read data.
				 	  puiData   :Pointer to store the read data.
					  uiLen		:Block length to read data.
   Return Value 	: 0 : Success
					 -1 : Fail

******************************************************************************/

#include    <windows.h>
#define DMA_NO_ERROR			        0
#define DMA_INVALID_PARAMETERS          1
#define DMA_ERROR					    2

#define DMA_SUCCESS 0
#define DMA_FAILURE -1

#define VPDMA_OPEN_FUNC       "wdmaOpenConnection"
#define VPDMA_CLOSE_FUNC      "wdmaCloseConnection"
#define VPDMA_READLONG_FUNC   "wdmaReadTargetMemoryLong"
#define VPDMA_WRITELONG_FUNC  "wdmaWriteTargetMemoryLong"

// Program title and version number
#define OPXDDMA_PROG_NAME              "ASH_DMA_TEST"
#define OPXDDMA_PROG_TITLE             "Ashling DMA Diagnostic Utility ("OPXDDMA_PROG_NAME")."
#define OPXDDMA_PROG_VERSION           "v1.0.0, 02-Jun-2009, (c)Ashling Microsystems Ltd 2009."
#define OPXDDMA_MAX_LEN					0x1000

// "Usage" text
#define OPXDDMA_USAGE0				   "Usage:"
#define OPXDDMA_USAGE1                 OPXDDMA_PROG_NAME" 0xAddr 0xLen"
#define OPXDDMA_USAGE_OPT0             "where:"
#define OPXDDMA_USAGE_OPT_DMA0         "0xAddr:		Start Address	(Default:0x00000000)" 
#define OPXDDMA_USAGE_OPT_DMA1         "0xLen :		Block length	(Default:0x0000000A)" 

//Structure for DMA options
typedef struct _DMAParameters
{
	unsigned int  ulAddr;
	unsigned int  ulLength;
}TyDMAParameters;

typedef int  (CALLBACK * VPDMA_OPEN       ) ( void ) ;
typedef int  (CALLBACK * VPDMA_CLOSE      ) ( void ) ;
typedef int  (CALLBACK * VPDMA_READLONG   ) ( unsigned int , unsigned int  * , unsigned int ) ;
typedef int  (CALLBACK * VPDMA_WRITELONG  ) ( unsigned int , unsigned int  * , unsigned int ) ;

#endif // ASH_DMA_h__

