/******************************************************************************
 Module		: ash_dma.cpp
Engineer		: Daniel Madden,
Description		: Defines the entry point for the DLL application.
Date           	Initials    	Description
08-Nov-2007  	  DM          	   Initial
******************************************************************************/
//includes
#include <stdio.h>
#include <assert.h>
#include "ASH_DMA.h"
#include "../mdi.h"

//local defines
//#define  MDILOAD_DEFINE
#define     MDIClose_Export_Name             "_MDIClose@8"       
#define     MDIConnect_Export_Name           "_MDIConnect@12"    
#define     MDIDisconnect_Export_Name        "_MDIDisconnect@8"  
#define     MDIOpen_Export_Name              "_MDIOpen@16"       
#define     MDIRead_Export_Name              "_MDIRead@28"       
#define     MDIWrite_Export_Name             "_MDIWrite@28"        

//static global vars
static HINSTANCE              hMipsInstMdiDll;
static MDIHandleT             tyCurrentMDIHandle;
static MDIHandleT             tyCurrentDevHndl;


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    hModule = hModule;
    lpReserved = lpReserved;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    default:
        break;
    }
    return TRUE;
}




typedef int (__stdcall *TyMDIRead)          (MDIHandleT, MDIResourceT SrcR, MDIOffsetT SrcO,void * Buffer, MDIUint32 ObjectSize, MDIUint32 Count);          
typedef int (__stdcall *TyMDIWrite)         (MDIHandleT, MDIResourceT DstR, MDIOffsetT DstO,void * Buffer, MDIUint32 ObjectSize, MDIUint32 Count);         
typedef int (__stdcall *TyMDIOpen)          (MDIHandleT, MDIDeviceIdT, MDIUint32, MDIHandleT *);        
typedef int (__stdcall *TyMDIClose)         (MDIHandleT, MDIUint32);                                   
typedef int (__stdcall *TyMDIConnect)       (MDIVersionT, MDIHandleT *, MDIConfigT *);               
typedef int (__stdcall *TyMDIDisconnect)    (MDIHandleT, MDIUint32);                              


/****************************************************************************
     Function: FileExists
     Engineer: Daniel Madden
        Input: pszFile - file to be checked for existence
       Output: bool
  Description: Check to see if a file exists in the specified directory
Date        Initials    Description
27-Jul-2006 DM			Initial
*****************************************************************************/
bool FileExists(char* pszFile)
{
    bool     bRetVal  = false;
    FILE *   hFile    = NULL;
    
    assert(pszFile != NULL);
    
    hFile = fopen(pszFile, "r" );
    
    if (hFile != NULL)
    {
        bRetVal = true;
        fclose(hFile);
    }
    return bRetVal;
}

/****************************************************************************
     Function: RetrievePFMIPSBaseDirectory
     Engineer: Patrick Noonan
        Input: char *pszKeyName
               char *pszKeyValue
       Output: None
  Description: Read strings in registry
Version     Date           Initials    Description
?.?.?-ALL   08-FEB-2001    PN          initial
****************************************************************************/
void RetrievePFMIPSBaseDirectory(char *pszKeyName, char *pszKeyValue)
{
    HKEY     hRegKey; 
    LONG     lResult = 0x0;
    DWORD    dwType;
    DWORD    dwCount;
    DWORD    dwDisposition  = 0x0;
    char     szRegistryKey[MAX_PATH]; 
    LONG     lRetCode; 
    
    if ((pszKeyName == NULL) || (pszKeyValue == NULL))
    {
        // Failed to open key for some reason
        // Just return
        return;
    }
    
    // Format the key
    strcpy(szRegistryKey,"SOFTWARE\\Ashling MicroSystems Ltd.\\ASH_DMA");
    
    // Try and open the key (create if necessary)
    lRetCode = RegCreateKeyEx ( HKEY_CURRENT_USER, 
        szRegistryKey, 
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, 
        NULL, &hRegKey, 
        &dwDisposition); 
    
    if (lRetCode != ERROR_SUCCESS)
    {
        return;
    }
    
    strcpy(pszKeyValue,"");
    lResult = RegQueryValueEx(hRegKey,pszKeyName, NULL, &dwType,
        NULL, &dwCount);
    
    if ((lResult == ERROR_SUCCESS) && dwType == REG_SZ)
    {
        if (dwCount < MAX_PATH)
            lResult = RegQueryValueEx(hRegKey,pszKeyName, NULL, &dwType,
            (unsigned char *)pszKeyValue, &dwCount);
    }
    (void)RegCloseKey(hRegKey);
    return;
}

/****************************************************************************
     Function: MDIDLLGetProcAddress
     Engineer: Patrick Noonan
        Input: HMODULE  hModule
               LPCSTR   lpMangledProcName
       Output: FARPROC 
  Description: Similiar to GetProcAddress but tries the unmangled name if
               the mangled name is not found. The MDI specification implies 
               that unmangled names should be used but certain DLLs use mangled.
               We support both
Version     Date           Initials    Description
1.0.7       12-Jun-2002    DN          Initial
****************************************************************************/
FARPROC MDIDLLGetProcAddress(HMODULE hModule,LPCSTR lpMangledProcName)
{
    FARPROC  lpRetFn = NULL;
    lpRetFn = GetProcAddress(hModule,lpMangledProcName);
    
    if (lpRetFn == NULL)
    {
        // Mangled name was not present, try the unmangled name
        char  szUnMangledName[MAX_PATH];
        char  *pcCharPtr = NULL;
        
        // remove the preceding underscore if present
        if (lpMangledProcName[0x0] == '_')
            strcpy(szUnMangledName,lpMangledProcName+0x1);
        else
            strcpy(szUnMangledName,lpMangledProcName);
        // Remove the trailing mangling as well
        pcCharPtr= strchr(szUnMangledName,'@');
        if (pcCharPtr)
            *pcCharPtr = '\0';
        // Try again with the new address!!
        lpRetFn = GetProcAddress(hModule,szUnMangledName);
    }
    
    return lpRetFn;
}

extern "C"
{

/****************************************************************************
     Function: wdmaOpenConnection
     Engineer: Patrick Noonan
        Input: None
       Output: int
  Description: Open a connection
   // We want to make a connection. 
   // 3. Load the appropriate DLL
   // 4. Call MDI Open on that interface
   // 5. Resolve the MDIRead and MDIWrite functions
Date           Initials    Description
17-Nov-2004    PN          Initial
27-Jul-2006	   DM		   Updated so it could work with GDBServer and TASTE
21-Aug-2006	   DM		   Updated to use AshMdi.cfg instead of Mips.cfg
18-Apr-2007	   DM		   Added Opella XD target
*****************************************************************************/
int __stdcall wdmaOpenConnection(void)
{
    int   iRetVal        = DMA_FAILURE;
    char  szCurrentDir  [MAX_PATH];
    char  szMIPSDir     [MAX_PATH];
    char  szCfgFile     [MAX_PATH];
    char  szBuffer      [MAX_PATH];
    char  szTemp        [MAX_PATH];
    DWORD dwVal;
    
    
    (void) GetCurrentDirectory(sizeof(szCurrentDir),szCurrentDir);
    
    
    // 1. Determine the location of the configuration file from the registry
    szMIPSDir[0] = '\0';
    RetrievePFMIPSBaseDirectory("PFMIPS_DIR",szMIPSDir);
    if (strlen(szMIPSDir)== 0x0) // neither GDBServer or PFMIPS installed so break
    {
        iRetVal        = DMA_FAILURE; // Config file not found
        goto ReturnPoint;                  
    }
    
    //2. Determine if AshMDI.CFG exists.
    (void)SetCurrentDirectory(szMIPSDir);  
    sprintf(szCfgFile,"%s\\AshMDI.CFG",szMIPSDir);
    
    // 3. If AshMdi.CFG exists, determine if Opella or Vitra and load appropiate DLL.
    if(FileExists(szCfgFile))
    {      
        dwVal = GetPrivateProfileString("General","TargetType","-1",szBuffer,(DWORD)(sizeof(szBuffer)),szCfgFile);
        if (dwVal)
        {
            int iTargetType;
            
            sscanf(szBuffer,"%d",&iTargetType);
            switch (iTargetType)
            {
            case -1:
                iRetVal        = DMA_FAILURE; // Config file not found
                goto ReturnPoint;
            case 0x0:
            case 0xA:
                sprintf(szTemp,"%s\\OPELMIPS.DLL",szMIPSDir);
                if(FileExists(szTemp))			
                    break;
                else
                {
                    iRetVal        = DMA_FAILURE; // DLL file not found
                    goto ReturnPoint;
                }
            case 0x1:
            case 0x2:
            case 0xB:
            case 0xC:
                sprintf(szTemp,"%s\\VITRAMIP.DLL",szMIPSDir);
                if(FileExists(szTemp))			
                    break;
                else
                {
                    iRetVal        = DMA_FAILURE; // DLL file not found
                    goto ReturnPoint;
                }
            case 0x10: 
                sprintf(szTemp,"%s\\OpxdMips.dll",szMIPSDir);
                if(FileExists(szTemp))			
                    break;
                else
                {
                    iRetVal        = DMA_FAILURE; // DLL file not found
                    goto ReturnPoint;
                }
            default:
                iRetVal        = DMA_FAILURE; // Config file not found
                goto ReturnPoint;
            }
        }
        //failure to read AshMDI.cfg
        else
        {
            iRetVal = DMA_FAILURE; // 
            goto ReturnPoint;         
        }
    }
    //4. If AshMdi.CFG does not exist then bad installation of GDBServer or PFMips
    else
    {
        iRetVal = DMA_FAILURE; // 
        goto ReturnPoint;         
    }
    
    //5. Load the DLL just found
    hMipsInstMdiDll = LoadLibrary(szTemp);
    
    if (hMipsInstMdiDll != NULL)
    {
        MDIConnect              = (TyMDIConnect)              MDIDLLGetProcAddress(hMipsInstMdiDll,MDIConnect_Export_Name);
        MDIDisconnect           = (TyMDIDisconnect)           MDIDLLGetProcAddress(hMipsInstMdiDll,MDIDisconnect_Export_Name);
        MDIWrite                = (TyMDIWrite)                MDIDLLGetProcAddress(hMipsInstMdiDll,MDIWrite_Export_Name);
        MDIRead                 = (TyMDIRead)                 MDIDLLGetProcAddress(hMipsInstMdiDll,MDIRead_Export_Name);
        MDIOpen                 = (TyMDIOpen)                 MDIDLLGetProcAddress(hMipsInstMdiDll,MDIOpen_Export_Name);
        MDIClose                = (TyMDIClose)                MDIDLLGetProcAddress(hMipsInstMdiDll,MDIClose_Export_Name);
        
        // First check that all MDI compulsary functions are present
        if (MDIConnect               &&
            MDIDisconnect            &&
            MDIOpen                  &&
            MDIClose                 &&
            MDIRead                  &&
            MDIWrite                  )
        {
            MDIVersionT     tyVersion;
            MDIDeviceIdT    tyMDIDeviceID;
            MDIConfigT      tyMDIConfig;
            MDIUint32       tyFlags;
            
            tyVersion = MDICURRENTREVISION;
            memset(&tyMDIConfig,0,sizeof(tyMDIConfig));
            strcpy(tyMDIConfig.User,"ASH_DMA");
            iRetVal = MDIConnect(tyVersion,&tyCurrentMDIHandle,&tyMDIConfig);
            
            if (iRetVal == MDISuccess)
            {
                tyMDIDeviceID  = 0x0;
                tyFlags        = MDIExclusiveAccess; 
                iRetVal        = MDIOpen(tyCurrentMDIHandle,tyMDIDeviceID,tyFlags,&tyCurrentDevHndl);
            }
        }
    }
    
ReturnPoint:
    if (iRetVal != DMA_SUCCESS)
    {
        iRetVal = DMA_FAILURE;
        if (hMipsInstMdiDll != NULL)  
            (void)FreeLibrary(hMipsInstMdiDll);
    }
    (void)SetCurrentDirectory(szCurrentDir);  
    
    
    return iRetVal;
}
/****************************************************************************
     Function: wdmaCloseConnection
     Engineer: Patrick Noonan
        Input: None
       Output: int
  Description: Close a connection
Date           Initials    Description
17-Nov-2004    PN          Initial
*****************************************************************************/
int __stdcall wdmaCloseConnection(void)
{
    int iErrRet;
    
    if (!MDIClose || !MDIDisconnect || !hMipsInstMdiDll)
        return DMA_FAILURE;
    
    iErrRet = MDIClose(tyCurrentDevHndl,MDICurrentState);
    
    if (!iErrRet)
        iErrRet = MDIDisconnect(tyCurrentMDIHandle,MDICurrentState);
    
    
    if (hMipsInstMdiDll != NULL)  
        (void)FreeLibrary(hMipsInstMdiDll);
    
    MDIConnect          = NULL;
    MDIDisconnect       = NULL;
    MDIOpen             = NULL;
    MDIClose            = NULL;
    MDIRead             = NULL;
    MDIWrite            = NULL;
    hMipsInstMdiDll     = NULL;
    
    if (iErrRet != DMA_SUCCESS)
        iErrRet = DMA_FAILURE;
    
    return iErrRet;
}

/****************************************************************************
     Function: wdmaReadTargetMemoryLong
     Engineer: Patrick Noonan
        Input: unsigned int uiAddress, 
               unsigned int  * puiData,
               unsigned int  uiLen)    
       Output: int
  Description: Read via DMA
Date           Initials    Description
17-Nov-2004    PN          Initial
*****************************************************************************/
int __stdcall  wdmaReadTargetMemoryLong(unsigned int uiAddress,
                                        unsigned int  * puiData,
                                        unsigned int  uiLen)
{
    int iRetVal = DMA_FAILURE;
    if(MDIRead)
    {
        if(MDIRead(tyCurrentDevHndl,MDIMIPPHYSICAL,(MDIOffsetT)uiAddress,(void *)puiData,sizeof(int),uiLen) == 0x0)
            iRetVal = DMA_SUCCESS;
    }
    return iRetVal;
}
/****************************************************************************
     Function: wdmaWriteTargetMemoryLong
     Engineer: Patrick Noonan
        Input: unsigned int uiAddress, 
               unsigned int  * puiData,
               unsigned int  uiLen)    
       Output: int
  Description: Write via DMA
Date           Initials    Description
17-Nov-2004    PN          Initial
*****************************************************************************/
int __stdcall wdmaWriteTargetMemoryLong(unsigned int uiAddress,
                                        unsigned int  * puiData,
                                        unsigned int  uiLen)
{
    int iRetVal = DMA_FAILURE;
    if(MDIWrite)
    {
        if(MDIWrite(tyCurrentDevHndl,MDIMIPPHYSICAL,(MDIOffsetT)uiAddress,(void *)puiData,sizeof(int),uiLen) == 0x0)
            iRetVal = DMA_SUCCESS;
    }
    return iRetVal;
}
}
