/****************************************************************************
       Module: ml_nvram.cpp
     Engineer: Rejeesh S Babu
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This section contains all functions related to MIPS 
               non-volatile RAM (Flash) access.
               Currently, it supports only Microchip devices.
Date           Initials    Description
19-Dec-2008      RS          Initial  
16-Jan-2009     SPC          Made __stdcall to Linux compatible
****************************************************************************/
#define MDI_LIB               // We are an MDI Library not a debugger

#include <string.h>
#include <stdio.h>
#include "protocol/common/drvopxd.h"
#include "commdefs.h"
#include "gerror.h"
#include "midlayer.h"
#include "mdi.h"
#include "ash_iface.h"
#include "toplayer.h"
#include "debug.h"

#define NO_ERROR                       0x0 

//program kernel space = 0x800 - MCHP_*K_KERNEAL_DATA_SPACE

#define MCHP_2K_KERNEL_DATA_SPACE       0x800
#define MCHP_4K_KERNEL_DATA_SPACE       0x1000
#define MCHP_6K_KERNEL_DATA_SPACE       0x1800
#define MCHP_8K_KERNEL_DATA_SPACE       0x2000
#define MCHP_10K_KERNEL_DATA_SPACE      0x2800
#define MCHP_12K_KERNEL_DATA_SPACE      0x3000
#define MCHP_14K_KERNEL_DATA_SPACE      0x3800
#define MCHP_16K_KERNEL_DATA_SPACE      0x4000
#define MCHP_18K_KERNEL_DATA_SPACE      0x4800
#define MCHP_20K_KERNEL_DATA_SPACE      0x5000
#define MCHP_22K_KERNEL_DATA_SPACE      0x5800
#define MCHP_24K_KERNEL_DATA_SPACE      0x6000
#define MCHP_26K_KERNEL_DATA_SPACE      0x6800
#define MCHP_28K_KERNEL_DATA_SPACE      0x7000
#define MCHP_30K_KERNEL_DATA_SPACE      0x7800
#define MCHP_32K_KERNEL_DATA_SPACE      0x8000

#define FLASH_STATUS_TIMEOUT               1
#define FLASH_ERASE_TIMEOUT                2
#define FAILED_PARTITION_RAM               3
#define FAILED_RESTORE_BMX_REGISGTERS      5

#define MCHP_STATUS_TIME        10  //10ms to check if flash controller is ready
#define MCHP_CHIPERASE_TIME     400 //400ms to check if the chip has been erased
extern TyMLHandle *ptyMLHandle;

//Microchip TAP commands
//IR  (instruction) commands for Microchip TAP controller
#define MTAP_IR_LENGTH      0x5


#define MTAP_SW_MTAP        0x04 // switch to MCHP TAP ctrl 
#define MTAP_SW_ETAP        0x05 // switch to EJTAG TAP ctrl 
#define MTAP_COMMAND        0x07 //TDI and TDO connected to MCHP Command Shift register 

//DR  (instruction) commands for Microchip TAP controller
#define MTAP_DR_LENGTH      0x8

#define MCHP_STATUS         0x00 // read Status Register 
#define MCHP_ERASE          0xFC  //inform flash controller to do flash erase


#define MCHP_BMXCON_ADDRESS             0xbf882000
#define MCHP_BMXCON_REQUIRED_VALUE      0x001f0040

#define MCHP_BMXDKPBA_ADDRESS           0xbf882010

#define MCHP_BMXDUDBA_ADDRESS           0xbf882020
#define MCHP_BMXDUDBA_REQUIRED_VALUE    0x8000

#define MCHP_BMXDUPBA_ADDRESS           0xbf882030
#define MCHP_BMXDUPBA_REQUIRED_VALUE    0x8000

/****************************************************************************
     Function: MLN_SetupMicrochipFlash
     Engineer: Rejeesh S Babu
        Input: none
       Output: Error Status
               tyMircohipStatus, set status of MicrochipStatus register
               to enable flash programming of Microchip Flash
  Description: Sets up the flash controller via the Microchip TAP for flash
               programming
               Now, it runs in DLL, but will be ported to diskware if performance 
               deems it necessary
Version     Date           Initials    Description
1.0.0		19-Dec-2008	   RS		   Initial  
****************************************************************************/
extern "C" MDIInt32 MLN_SetupMicrochipFlash(TyMicrochipStatusRegister *ptyMicrochipStatusRegister,
                                int bClearCodeProtection)
{
     unsigned long ulDataIn;
     unsigned long ulDataOut;
     unsigned int uiNumLoops=0;
    
     DLOG2((LOGFILE,"\nMLN_SetupMicrochipFlash: Called "));
    
     ASSERT(ptyMicrochipStatusRegister != NULL);
    
     ulDataIn = MTAP_SW_MTAP;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //switch to MTAP 
   
     ulDataIn = MTAP_COMMAND;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //ensures TDO is connected to TDI for MCHP
    
     //keep reading Status register for 50ms  until flash device is ready
     ulDataIn = MCHP_STATUS;
     for(uiNumLoops=0; uiNumLoops <= 5; uiNumLoops++)
         {
         (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
         ptyMicrochipStatusRegister->ulData = ulDataOut; //read the Microchip status register
         ML_Delay(MCHP_STATUS_TIME); 
         //check if flash device is ready
         if((ptyMicrochipStatusRegister->Bits.CFGRDY == 1) && (ptyMicrochipStatusRegister->Bits.FCBUSY == 0))
             break;
         }
     if(uiNumLoops==5)
          {
          DLOG3((LOGFILE,"\n\t\tMLN_SetupMicrochipFlash: -> Failed to activate flash controller",ptyMicrochipStatusRegister->ulData));
          return FLASH_STATUS_TIMEOUT; //flash device status not available after 200ms
          }
     else
          {
          DLOG3((LOGFILE,"\n\t\tMLN_SetupMicrochipFlash: -> Activated flash controller",ptyMicrochipStatusRegister->ulData));
          }
     if(bClearCodeProtection)
         {
         //check code protection status and erase chip if device is code protected
         if(ptyMicrochipStatusRegister->Bits.CPS == 0)
         {
             DLOG3((LOGFILE,"\n\t\tMLN_SetupMicrochipFlash: -> CPS set so we must do a chip erase of flash"));
             ulDataIn = MCHP_ERASE;  //issue erase command
             (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
             ptyMicrochipStatusRegister->ulData = ulDataOut; //read the Microchip status register
             //keep reading Status register up to 2s  until flash device is ready          
             ulDataIn = MCHP_STATUS;
    
             for(uiNumLoops=0; uiNumLoops <= 5; uiNumLoops++)
                 {
                 (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
                 ptyMicrochipStatusRegister->ulData = ulDataOut; //read the Microchip status register
                 ML_Delay(MCHP_CHIPERASE_TIME); 
                 //check if flash device is ready
                 if((ptyMicrochipStatusRegister->Bits.CFGRDY == 1) && (ptyMicrochipStatusRegister->Bits.FCBUSY == 0))
                     break;
                 }
             if(uiNumLoops==5)
                 {
                 DLOG3((LOGFILE,"\n\t\tMLN_SetupMicrochipFlash: -> Failed to do a chip erase of flash",ptyMicrochipStatusRegister->ulData));
                 return FLASH_ERASE_TIMEOUT; //flash device status not available after 200ms        
                 }
             else
                 {
                 DLOG3((LOGFILE,"\n\t\tMLN_SetupMicrochipFlash: -> Performed a chip erase of flash",ptyMicrochipStatusRegister->ulData));
                 }
          }             
         }
    
     ulDataIn = MTAP_SW_ETAP;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //switch to ETAP
    
     return NO_ERROR;   
}
/****************************************************************************
     Function: MLN_PartitionRamForMicrochipFlashSetup
     Engineer: Rejeesh S Babu
        Input: ulDataKernelSpace = set amount of kernel ram space for data
       Output: Error Status
  Description: Partitions the amount of data RAM in kernel space
			   for program memory and data memory based on ulDataKernal 
               Now, it runs in DLL, but will be ported to diskware if performance 
               deems it necessary
Version     Date           Initials    Description
1.0.0		19-Dec-2008	   RS		   Initial  
****************************************************************************/
extern "C" MDIInt32 MLN_PartitionRamForMicrochipFlashSetup(unsigned long ulDataKernelSpace)
{
    unsigned long ulDataValueInRegister=0;

    DLOG2((LOGFILE,"\n:MLN_PartitionRamForMicrochipFlashSetup Called "));

    //assert ulDataKernelSpace is not 0 and not greater than 30k
    ASSERT(ulDataKernelSpace >  0);
    ASSERT(ulDataKernelSpace <= MCHP_30K_KERNEL_DATA_SPACE);

    //write required value to BMXCON
    (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXCON_ADDRESS,MCHP_BMXCON_REQUIRED_VALUE);

    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXCON_ADDRESS,&ulDataValueInRegister);
	
    if(ulDataValueInRegister != MCHP_BMXCON_REQUIRED_VALUE)
        {
        DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXCON. Value read is 0x%x\n",
               MCHP_BMXCON_REQUIRED_VALUE, ulDataValueInRegister));
        return FAILED_PARTITION_RAM; 
        }
		

    //write and verify required value to BMXDKPBA 
    //find range specified by user and set as required value by BMXDKPBA
    if(ulDataKernelSpace <= MCHP_2K_KERNEL_DATA_SPACE)
        {
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_2K_KERNEL_DATA_SPACE);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);
  
        if(ulDataValueInRegister != MCHP_2K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_2K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }            
        }
    else if(ulDataKernelSpace <= MCHP_4K_KERNEL_DATA_SPACE)
        {
           (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_4K_KERNEL_DATA_SPACE);
    
           (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);
    
           if(ulDataValueInRegister != MCHP_4K_KERNEL_DATA_SPACE)
                {
                DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                       MCHP_4K_KERNEL_DATA_SPACE,ulDataValueInRegister));
                return FAILED_PARTITION_RAM; 
                }   
        }
    else if(ulDataKernelSpace <= MCHP_6K_KERNEL_DATA_SPACE)
        {
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_6K_KERNEL_DATA_SPACE);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

        if(ulDataValueInRegister != MCHP_6K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_6K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
        }
    else if(ulDataKernelSpace <= MCHP_8K_KERNEL_DATA_SPACE)
        {

         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_8K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_8K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_8K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
        }
    else if(ulDataKernelSpace <= MCHP_10K_KERNEL_DATA_SPACE)
        {
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_10K_KERNEL_DATA_SPACE);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

        if(ulDataValueInRegister != MCHP_10K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_10K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
        }
    else if(ulDataKernelSpace <= MCHP_12K_KERNEL_DATA_SPACE)
        {
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_12K_KERNEL_DATA_SPACE);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

        if(ulDataValueInRegister != MCHP_12K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_12K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
        }
    else if(ulDataKernelSpace <= MCHP_14K_KERNEL_DATA_SPACE)
        {

        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_14K_KERNEL_DATA_SPACE);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

        if(ulDataValueInRegister != MCHP_14K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_14K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
        }
    else if(ulDataKernelSpace <= MCHP_16K_KERNEL_DATA_SPACE)
        {
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_16K_KERNEL_DATA_SPACE);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);


        if(ulDataValueInRegister != MCHP_16K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_16K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
        }
    else if(ulDataKernelSpace <= MCHP_18K_KERNEL_DATA_SPACE)
         {

         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_18K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_18K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_18K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace <= MCHP_20K_KERNEL_DATA_SPACE)
         {

         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_20K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);
         if(ulDataValueInRegister != MCHP_20K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_20K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace <= MCHP_22K_KERNEL_DATA_SPACE)
         {
         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_22K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_22K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_22K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace <= MCHP_24K_KERNEL_DATA_SPACE)
         {
         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_24K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_24K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_24K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace <= MCHP_26K_KERNEL_DATA_SPACE)
         {

         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_26K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_26K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_26K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace <= MCHP_28K_KERNEL_DATA_SPACE)
         {
         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_28K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_28K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_28K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace <= MCHP_30K_KERNEL_DATA_SPACE)
         {

         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_30K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_30K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_30K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    else if(ulDataKernelSpace < MCHP_32K_KERNEL_DATA_SPACE)
         {


         (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,MCHP_30K_KERNEL_DATA_SPACE);

         (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

         if(ulDataValueInRegister != MCHP_30K_KERNEL_DATA_SPACE)
            {
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   MCHP_30K_KERNEL_DATA_SPACE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
            }   
         }
    //write and verify required value to BMXDUDBA
    (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDUDBA_ADDRESS,MCHP_BMXDUDBA_REQUIRED_VALUE);

    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDUDBA_ADDRESS,&ulDataValueInRegister);
	
    if(ulDataValueInRegister != MCHP_BMXDUDBA_REQUIRED_VALUE)
        {
        DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDUDBA. Value read is 0x%x\n",
                 MCHP_BMXDUDBA_REQUIRED_VALUE,ulDataValueInRegister));
        return FAILED_PARTITION_RAM; 
        } 

    //write and verify required value to BMXDUPBA
    (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDUPBA_ADDRESS,MCHP_BMXDUPBA_REQUIRED_VALUE);

    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDUPBA_ADDRESS,&ulDataValueInRegister);

	if(ulDataValueInRegister != MCHP_BMXDUPBA_REQUIRED_VALUE)
		{
            DLOG3((LOGFILE,"\n\t\tMLN_PartitionRamForMicrochipFlashSetup: -> Failed to write 0x%x to BMXDUDBA. Value read is 0x%x\n",
                 MCHP_BMXDUPBA_REQUIRED_VALUE,ulDataValueInRegister));
            return FAILED_PARTITION_RAM; 
        }   

    DLOG3((LOGFILE,"\n:MLN_PartitionRamForMicrochipFlashSetup Correctly set BMX registers "));
    return NO_ERROR;
}
/****************************************************************************
     Function: MLN_SaveMicrochipBmxRegisters
     Engineer: Rejeesh S Babu
        Input: tyMicrochipBmxRegContext - BMX registers to be saved
       Output: Error Status
  Description: Saves Micrcochip BMX registers that are
               temporarily modified by Pathfinder debugger (for example during flash 
               programming). Saved BMX registers restored afterwards using 
               MLN_RestoreMicrochipBmxRegisters().
               Now, it runs in DLL, but will be ported to diskware if performance 
               deems it necessary
Version     Date           Initials    Description
1.0.0		19-Dec-2008	   RS		   Initial  
****************************************************************************/
extern "C" MDIInt32 MLN_SaveMicrochipBmxRegisters(TyMicrochipBmxRegContext *tyMicrochipBmxRegContext)
{
    DLOG2((LOGFILE,"\n:MLN_SaveMicrochipBmxRegisters Called "));

    ASSERT(tyMicrochipBmxRegContext);

    //read and save BMXCON register value
    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXCON_ADDRESS,&(tyMicrochipBmxRegContext->ulBmxConValue));

    DLOG3((LOGFILE,"\n:MLN_SaveMicrochipBmxRegisters BMXCON saved as 0x%x\n",tyMicrochipBmxRegContext->ulBmxConValue));

    //read and save BMXDKPBA register value
    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&(tyMicrochipBmxRegContext->ulBmxDkPbaValue));
    DLOG3((LOGFILE,"\n:MLN_SaveMicrochipBmxRegisters BMXDKPBA saved as 0x%x\n",tyMicrochipBmxRegContext->ulBmxDkPbaValue));

    //read and save BMXDUPBA register value
    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDUPBA_ADDRESS,&(tyMicrochipBmxRegContext->ulBmxDuPbaValue));
    DLOG3((LOGFILE,"\n:MLN_SaveMicrochipBmxRegisters BMXDUDBA saved as 0x%x\n",tyMicrochipBmxRegContext->ulBmxDuPbaValue));

    //read and save BMXDUDBA register value
    (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDUDBA_ADDRESS,&(tyMicrochipBmxRegContext->ulBmxDuDbaValue));
    DLOG3((LOGFILE,"\n:MLN_SaveMicrochipBmxRegisters BMXDUDBA saved as 0x%x\n",tyMicrochipBmxRegContext->ulBmxDuDbaValue));

    tyMicrochipBmxRegContext->bBmxRegistersModified = TRUE;

    DLOG3((LOGFILE,"\n:MLN_SaveMicrochipBmxRegisters Correctly saved BMX registers "));
    return NO_ERROR;
}
/****************************************************************************
     Function: MLN_RestoreMicrochipBmxRegisters
     Engineer: Rejeesh S Babu
        Input: tyMicrochipBmxRegContext - BMX registers to be restored
       Output: Error Status
  Description: Restores original Micrcochip BMX registers that were
               temporarily modified by Pathfinder debugger (for example during flash 
               programming). Restores BMX registers to values saved by 
               MLN_SaveMicrochipBmxRegisters().
               Now, it runs in DLL, but will be ported to diskware if performance 
               deems it necessary
Version     Date           Initials    Description
1.0.0		19-Dec-2008	   RS		   Initial  
****************************************************************************/
extern "C" MDIInt32 MLN_RestoreMicrochipBmxRegisters(TyMicrochipBmxRegContext *tyMicrochipBmxRegContext)
{
    unsigned long ulDataValueInRegister;

    DLOG2((LOGFILE,"\n:MLN_RestoreMicrochipBmxRegisters Called "));
	
	ASSERT(tyMicrochipBmxRegContext);

    if(tyMicrochipBmxRegContext->bBmxRegistersModified)
        {
        //restore BMXCON register value
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXCON_ADDRESS,tyMicrochipBmxRegContext->ulBmxConValue);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXCON_ADDRESS,&ulDataValueInRegister);

    	if(ulDataValueInRegister != tyMicrochipBmxRegContext->ulBmxConValue)
            {
            DLOG3((LOGFILE,"\n\tMLN_RestoreMicrochipBmxRegisters: -> Failed to write 0x%x to BMXCON. Value read is 0x%x\n",
                   tyMicrochipBmxRegContext->ulBmxConValue,ulDataValueInRegister));
            return FAILED_RESTORE_BMX_REGISGTERS; 
            }
    
        //restore BMXDKPBA register value
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,tyMicrochipBmxRegContext->ulBmxDkPbaValue);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDKPBA_ADDRESS,&ulDataValueInRegister);

    	if(ulDataValueInRegister != tyMicrochipBmxRegContext->ulBmxDkPbaValue)
            {
            DLOG3((LOGFILE,"\n\tMLN_RestoreMicrochipBmxRegisters: -> Failed to write 0x%x to BMXDKPBA. Value read is 0x%x\n",
                   tyMicrochipBmxRegContext->ulBmxDkPbaValue,ulDataValueInRegister));
            return FAILED_RESTORE_BMX_REGISGTERS; 
            }
    
        //restore BMXDUDBA register value

        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDUDBA_ADDRESS,tyMicrochipBmxRegContext->ulBmxDuDbaValue);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDUDBA_ADDRESS,&ulDataValueInRegister);

    	if(ulDataValueInRegister != tyMicrochipBmxRegContext->ulBmxDuDbaValue)
            {
            DLOG3((LOGFILE,"\n\tMLN_RestoreMicrochipBmxRegisters: -> Failed to write 0x%x to BMXDUDBA. Value read is 0x%x\n",
                   tyMicrochipBmxRegContext->ulBmxDuDbaValue,ulDataValueInRegister));
            return FAILED_RESTORE_BMX_REGISGTERS; 
            }
    
        //restore BMXDUPBA register value
        (void)MLM_WriteWord(ptyMLHandle,(unsigned long )MCHP_BMXDUPBA_ADDRESS,tyMicrochipBmxRegContext->ulBmxDuPbaValue);

        (void)MLM_ReadWord ( ptyMLHandle,(unsigned long )MCHP_BMXDUPBA_ADDRESS,&ulDataValueInRegister);

        if(ulDataValueInRegister != tyMicrochipBmxRegContext->ulBmxDuPbaValue)
            {
            DLOG3((LOGFILE,"\n\tMLN_RestoreMicrochipBmxRegisters: -> Failed to write 0x%x to BMXDUPBA. Value read is 0x%x\n",
                   tyMicrochipBmxRegContext->ulBmxDuPbaValue,ulDataValueInRegister));
            return FAILED_RESTORE_BMX_REGISGTERS; 
            }    
    
        DLOG3((LOGFILE,"\n:MLN_RestoreMicrochipBmxRegisters Correctly restored BMX registers "));
        return NO_ERROR;
        }
    else
        {
        DLOG3((LOGFILE,"\n:MLN_RestoreMicrochipBmxRegisters not reqquired to restore BMX registers "));
        return NO_ERROR;
        }    
}
/****************************************************************************
     Function: MLN_MchpRecoveryByChipErase
     Engineer: Rejeesh S Babu
        Input: none
       Output: Error Status
               
  Description: Recovers debugger access to ETAP by chip erase of flash via MTAP                            
Version     Date           Initials    Description
1.0.0		19-Dec-2008	   RS		   Initial  
****************************************************************************/
extern "C" MDIInt32 MLN_MchpRecoveryByChipErase(void)
{
     unsigned long ulDataIn;
     unsigned long ulDataOut;
     unsigned int  uiNumLoops;
     TyMicrochipStatusRegister tyMicrochipStatusRegister;
    
     DLOG2((LOGFILE,"\nMLN_MchpRecoveryByChipErase: Called "));

     ulDataIn = MTAP_SW_MTAP;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //switch to MTAP 
    
     ulDataIn = MTAP_COMMAND;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //put MTAP in command mode
    
     ulDataIn = MCHP_ERASE;  //issue erase command
     (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
     tyMicrochipStatusRegister.ulData = ulDataOut; //read the Microchip status register
    
     //keep reading Status register for 2s  until flash device is ready 
     ulDataIn = MCHP_STATUS;
     for(uiNumLoops=0; uiNumLoops <= 5; uiNumLoops++)
         {
         (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
         tyMicrochipStatusRegister.ulData = ulDataOut; //read the Microchip status register
         ML_Delay(MCHP_CHIPERASE_TIME); 
         //check if flash device is ready
         if((tyMicrochipStatusRegister.Bits.CFGRDY == 1) && (tyMicrochipStatusRegister.Bits.FCBUSY == 0))
             break;
         }

     if(uiNumLoops==5)
         {
         DLOG3((LOGFILE,"\n\t\nMLN_MchpRecoveryByChipErase: -> Failed to do a chip erase of flash. Microchip Status Register is %d",tyMicrochipStatusRegister.ulData));
         return FLASH_ERASE_TIMEOUT; //flash device status not available after 200ms   
         }
     else
         {
         DLOG3((LOGFILE,"\n\t\tMLN_MchpRecoveryByChipErase: -> Performed a chip erase of flash. Microchip Status Register is %d ",tyMicrochipStatusRegister.ulData));
         }
      
     return NO_ERROR;
}
/****************************************************************************
     Function: MLN_EraseChipMicrochipFlash
     Engineer: Rejeesh S Babu
        Input: none
       Output: Error Status
               tyMircohipStatus, set status of MicrochipStatus register
               to enable flash programming of Microchip Flash
  Description: Erases the flash microchip for flash programming via MTAP command             
Version     Date           Initials    Description
1.0.0		19-Dec-2008	   RS		   Initial  
****************************************************************************/
extern "C" MDIInt32 MLN_EraseChipMicrochipFlash(TyMicrochipStatusRegister *ptyMicrochipStatusRegister)
{
     unsigned long ulDataIn;
     unsigned long ulDataOut;
     unsigned int  uiNumLoops;
    
     DLOG2((LOGFILE,"\nMLN_EraseChipMicrochipFlash: Called "));
    
     ASSERT(ptyMicrochipStatusRegister != NULL);
    
     ulDataIn = MTAP_SW_MTAP;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //switch to MTAP 
    
     ulDataIn = MTAP_COMMAND;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //put MTAP in command mode
    
     ulDataIn = MCHP_ERASE;  //issue erase command
     (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
     ptyMicrochipStatusRegister->ulData = ulDataOut; //read the Microchip status register
    
     //keep reading Status register for 2s  until flash device is ready 
     ulDataIn = MCHP_STATUS;
     for(uiNumLoops=0; uiNumLoops <= 5; uiNumLoops++)
         {
         (void)ML_ScanDR(ptyMLHandle,MTAP_DR_LENGTH,&ulDataIn,&ulDataOut);
         ptyMicrochipStatusRegister->ulData = ulDataOut; //read the Microchip status register
         ML_Delay(MCHP_CHIPERASE_TIME); 
         //check if flash device is ready
         if((ptyMicrochipStatusRegister->Bits.CFGRDY == 1) && (ptyMicrochipStatusRegister->Bits.FCBUSY == 0))
             break;
         }

     if(uiNumLoops==5)
         {
         DLOG3((LOGFILE,"\n\t\tMLN_EraseChipMicrochipFlash: -> Failed to do a chip erase of flash",ptyMicrochipStatusRegister->ulData));
         return FLASH_ERASE_TIMEOUT; //flash device status not available after 200ms   
         }
     else
         {
         DLOG3((LOGFILE,"\n\t\tMLN_EraseChipMicrochipFlash: -> Performed a chip erase of flash",ptyMicrochipStatusRegister->ulData));
         }
    ulDataIn = MTAP_SW_ETAP;
     (void)ML_ScanIR(ptyMLHandle,MTAP_IR_LENGTH,&ulDataIn,&ulDataOut); //switch to ETAP
    return NO_ERROR;
}

