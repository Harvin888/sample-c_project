

/**  \file

*    \brief  ELF file processing module
*/
/*lint -e641*/
/*lint -e774*/
/****************************************************************************
*       Module: trace_procelf.cpp
*    Engineer: Ranjith T.C.
* Description: Processing ELF
Date           Initials    Description**
*08-Feb-2008*  RTC         Initial &** cleanup**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "trace_procelf.h"
#include "trace_disassembler.h"
#include "midlayer.h"
static int VerifyFileFormat(FILE *pFile);
static int ProcessELFHeader(void);
static int ProcessELFProgramHeaderTable(TyElfAddressList **ptyElfAddressList);
static int FileSeek(FILE *pFile, unsigned int ulOffset, signed short siOrigin);
// Exportedvariable
TyElfProcessData tyELFProcData;
static unsigned char *pDataBuffer = NULL;



// local variables


/**************************************************************************** 
    Function: ProcessELFFile
    Engineer: Ranjith T.C.
      Input: pcFileName- name of the elf
               ptyElfAddressList-pointer to the program section list 
      Output: int - error code
  Description: Process ELF file
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup
*****************************************************************************/ 
int ProcessELFFile(char *pcFileName, TyElfAddressList **ptyElfAddressList)
{
   int iError = NO_ERROR;  
   tyELFProcData.pInFile=fopen(pcFileName,"rb");
   if (tyELFProcData.pInFile == NULL)
      return ERR_F_NULL_FILE;
   // Check file format
   iError = VerifyFileFormat(tyELFProcData.pInFile);
   // Process ELF header
   if (!iError)
      iError = ProcessELFHeader();
   // Read program header table
   if (!iError)
      iError = ProcessELFProgramHeaderTable(ptyElfAddressList);
   // Close file and return error code
   fclose(tyELFProcData.pInFile);

   return iError;
}

/****************************************************************************
    Function: VerifyFileFormat
    Engineer: Ranjith T.C.
      Input: FILE *pFile - pointer to file
      Output: int - error code
  Description: Veryfie if given file is realy ELF
Date           Initials    Description
21-Oct-2011    RTC         Initial & cleanup
****************************************************************************/
static int VerifyFileFormat(FILE *pFile)
{
   int iError = 0;
   unsigned char pucData[4];
   //assert(pFile != NULL);
   // read first 4 bytes of the file
   iError = ReadBytes(pFile, 0, 1, (void *)pucData, 4);
   if (iError)
      return iError;
   // check ELF header
   if ((pucData[EI_MAG0] == 0x7f) && (pucData[EI_MAG1] == 'E') && (pucData[EI_MAG2] == 'L') && (pucData[EI_MAG3] == 'F'))
      return 0;
   return ERR_NOT_AN_ELF_FILE;
}

/****************************************************************************
    Function: ProcessELFHeader
    Engineer: Ranjith T.C.
      Input: none
      Output: int - error code
  Description: Process the ELF file's header.
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup
****************************************************************************/
static int ProcessELFHeader(void)
{
   int iError = 0;

   // read the ELF header (at the beginning of the ELF file)
   iError = ReadBytes(tyELFProcData.pInFile, 0, 1, (void *)&(tyELFProcData.tyElfHeader), sizeof(tyELFProcData.tyElfHeader));
   if (iError)
      return iError;
   if (tyELFProcData.tyElfHeader.e_ident[EI_DATA] == ELFDATA2MSB)
   {
      tyELFProcData.bBigEndian = TRUE;
   }
   if (tyELFProcData.bBigEndian)
   {
      tyELFProcData.tyElfHeader.e_type = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_type);
      tyELFProcData.tyElfHeader.e_machine = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_machine);
      tyELFProcData.tyElfHeader.e_version = ML_SwapEndianWord(tyELFProcData.tyElfHeader.e_version);
      tyELFProcData.tyElfHeader.e_entry = ML_SwapEndianWord(tyELFProcData.tyElfHeader.e_entry);
      tyELFProcData.tyElfHeader.e_phoff = ML_SwapEndianWord(tyELFProcData.tyElfHeader.e_phoff);
      tyELFProcData.tyElfHeader.e_shoff = ML_SwapEndianWord(tyELFProcData.tyElfHeader.e_shoff);
      tyELFProcData.tyElfHeader.e_flags = ML_SwapEndianWord(tyELFProcData.tyElfHeader.e_flags);
      tyELFProcData.tyElfHeader.e_ehsize = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_ehsize);
      tyELFProcData.tyElfHeader.e_phentsize = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_phentsize);
      tyELFProcData.tyElfHeader.e_phnum = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_phnum);
      tyELFProcData.tyElfHeader.e_shentsize = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_shentsize);
      tyELFProcData.tyElfHeader.e_shnum = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_shnum);
      tyELFProcData.tyElfHeader.e_shstrndx = ML_SwapEndianHalfword(tyELFProcData.tyElfHeader.e_shstrndx);
   }
   // accept absolute (executable) files only
   if (tyELFProcData.tyElfHeader.e_type != ET_EXEC)
      return ERR_NOT_ABSOLUTE_REC_E;
   return iError;
} 

/****************************************************************************
    Function: ProcessELFProgramHeaderTable
    Engineer: Ranjith T.C.
      Input: ptyElfAddressList- pointer to the program section list
      Output: int - error code
  Description: Process the ELF file's Data via the Program Header Table.
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup

****************************************************************************/
static int ProcessELFProgramHeaderTable(TyElfAddressList **ptyElfAddressList)
{
   int iError = 0;
   unsigned int ulProgramHeaderTableSize, ulEntry;
   TyElf32_Phdr *pProgramHeaderTable = NULL;
   

   //Message here
   if ((tyELFProcData.tyElfHeader.e_phoff == 0) || (tyELFProcData.tyElfHeader.e_phnum == 0))
   {  // no program header table
     
      return 0;
   }
   if (tyELFProcData.tyElfHeader.e_phentsize < sizeof(TyElf32_Phdr))
   {  // should not happen 
      iError = ERR_IE_PROCDWRF00;
      return iError;
      //goto Exit;
   }
   // Program Header Table Size is the number of entries * entry size
   ulProgramHeaderTableSize = (unsigned int)(tyELFProcData.tyElfHeader.e_phnum * tyELFProcData.tyElfHeader.e_phentsize);
   pProgramHeaderTable = (TyElf32_Phdr *)malloc(ulProgramHeaderTableSize);
   if (pProgramHeaderTable == NULL)
   {
      iError = ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY;
      return iError;
      //goto Exit;
   }
   // read the complete Program Header Table
   iError = ReadBytes(tyELFProcData.pInFile, tyELFProcData.tyElfHeader.e_phoff, 1, pProgramHeaderTable, ulProgramHeaderTableSize);
   if (iError)
   {
      if (pProgramHeaderTable != NULL)
      {
         free(pProgramHeaderTable);
      }
      return iError;
      //goto Exit;
   }
   if (tyELFProcData.bBigEndian)
   {  // invert values for each entry in the Program Header Table
      for (ulEntry=0; ulEntry < tyELFProcData.tyElfHeader.e_phnum; ulEntry++)
      {
         pProgramHeaderTable[ulEntry].p_type=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_type);
         pProgramHeaderTable[ulEntry].p_offset=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_offset);
         pProgramHeaderTable[ulEntry].p_vaddr=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_vaddr);
         pProgramHeaderTable[ulEntry].p_paddr=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_paddr);
         pProgramHeaderTable[ulEntry].p_filesz=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_filesz);
         pProgramHeaderTable[ulEntry].p_memsz=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_memsz);
         pProgramHeaderTable[ulEntry].p_flags=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_flags);
         pProgramHeaderTable[ulEntry].p_align=ML_SwapEndianWord(pProgramHeaderTable[ulEntry].p_align);
      }
   }
   // process each entry in the Program Header Table
   for (ulEntry=0; ulEntry<tyELFProcData.tyElfHeader.e_phnum; ulEntry++)
   {
      
      if ((pProgramHeaderTable[ulEntry].p_type == PT_LOAD) && (pProgramHeaderTable[ulEntry].p_filesz != 0) && (pProgramHeaderTable[ulEntry].p_memsz != 0))
      {
         if (pProgramHeaderTable[ulEntry].p_filesz > pProgramHeaderTable[ulEntry].p_memsz)
         {           
            continue;
         } 
         else
         {
            unsigned int ulIncreaseOffset = 0;
            if (pProgramHeaderTable[ulEntry].p_offset == 0)
            {  // this Segment includes the ELF file header which is always at 0, skip over the ELF file header
               ulIncreaseOffset = sizeof(tyELFProcData.tyElfHeader);
            }
            if ((pProgramHeaderTable[ulEntry].p_offset + ulIncreaseOffset) == tyELFProcData.tyElfHeader.e_phoff)
            {  // this Segment includes the program header table, skip over the program header table
               ulIncreaseOffset += ulProgramHeaderTableSize;
            }
            if (ulIncreaseOffset != 0)
            {
               if ((pProgramHeaderTable[ulEntry].p_filesz <= ulIncreaseOffset) || (pProgramHeaderTable[ulEntry].p_memsz  <= ulIncreaseOffset))
                  continue;                                   // nothing left after skipping headers, ignore the segment
               pProgramHeaderTable[ulEntry].p_offset += ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_vaddr  += ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_paddr  += ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_filesz -= ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_memsz  -= ulIncreaseOffset;
            }

            pDataBuffer = (unsigned char *)malloc(pProgramHeaderTable[ulEntry].p_filesz);
            if (pDataBuffer == NULL)
            {
               iError = ERR_INSUFFICENT_DATA_BUFFER_MEMORY;
               if (pProgramHeaderTable != NULL)
               {
                  free(pProgramHeaderTable);
               }
               return iError;
               //goto Exit;
            }
         }

         // read all data bytes
         iError = ReadBytes(tyELFProcData.pInFile, pProgramHeaderTable[ulEntry].p_offset, 1, pDataBuffer, pProgramHeaderTable[ulEntry].p_filesz);
         if (iError != 0)
         {
            if (pProgramHeaderTable != NULL)
            {
               free(pProgramHeaderTable);
            }
            return iError;
            //goto Exit;
         }
         iError = AddToList(ptyElfAddressList,
                            (unsigned int)pProgramHeaderTable[ulEntry].p_paddr,
                            (unsigned int)pProgramHeaderTable[ulEntry].p_paddr + pProgramHeaderTable[ulEntry].p_memsz,
                            (unsigned int)pDataBuffer);
         if (iError)
         {
            if (pProgramHeaderTable != NULL)
            {
               free(pProgramHeaderTable);
            }
            return iError;
            //goto Exit;
         }
      }
   }

   if (pProgramHeaderTable != NULL)
      free(pProgramHeaderTable);
   pProgramHeaderTable = NULL ;
   return iError;
}

/******************************************************************************
*   Function	:	AddToList
*   Engineer	:	Ranjith T.C.
*   Input       :   ptyElfAddressList- Pointer to the program section list
                    ulStartAddress- Start address of the section
                    ulEndAddress- End address of the section
                    ulMappedAddress- Mapped address of the elf in the host 
*   Output      :   int - error code
*   Description	:   Add new node to the list
*	Date           Initials    Description
*	21-Oct-2011     RTC        Initial
******************************************************************************/
int AddToList(TyElfAddressList **ptyElfAddressList,
              unsigned int ulStartAddress,
              unsigned int ulEndAddress,
              unsigned int ulMappedAddress)
{
   int ErrRet = 0;
   TyElfAddressList *ptyNode = NULL;

   if (NULL == *ptyElfAddressList)
   {
      ptyNode = (TyElfAddressList*)malloc(sizeof(TyElfAddressList));
      if (ptyNode)
      {
         ptyNode->ulStartAddress = ulStartAddress;
         ptyNode->ulEndAddress = ulEndAddress;
         ptyNode->ulMappedAddress = ulMappedAddress;
         ptyNode->tyNextNode = NULL;
         *ptyElfAddressList = ptyNode;
      } else
      {
         return(int)MALLOC_FAILURE;
      }
   } else
   {
      ptyNode = *ptyElfAddressList;
      while (NULL != ptyNode->tyNextNode)
      {
         ptyNode = ptyNode->tyNextNode; 
      } 
      ptyNode->tyNextNode = (TyElfAddressList*)malloc(sizeof(TyElfAddressList));
      if (ptyNode->tyNextNode)
      {
         ptyNode = ptyNode->tyNextNode;
         ptyNode->ulStartAddress = ulStartAddress;
         ptyNode->ulEndAddress = ulEndAddress;
         ptyNode->ulMappedAddress = ulMappedAddress;
         ptyNode->tyNextNode = NULL;
      } else
      {
         return(int)MALLOC_FAILURE;
      }
   }
   return ErrRet;
}
/****************************************************************************
    Function: ReadBytes
    Engineer: Ranjith T.C.
      Input: FILE *pFile - file from which bytes are read
            unsigned int ulFilePosition - position to read from if bSetFilePosition==1
            unsigned char bSetFilePosition - if 1 read form uFilePosition else use the current seek position
            void *pBuffer - pointer to storage for the bytes
            unsigned int ulNumBytes - count of bytes to read
      Output: int - error code
  Description: Reads uNumBytes bytes from pFile at either uFilePosition or at the current seek position.
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup
****************************************************************************/
int ReadBytes(FILE *pFile, unsigned int ulFilePosition, unsigned char bSetFilePosition, void *pBuffer, unsigned int ulNumBytes)
{
   int iError;
   signed short sTemp;
   unsigned int ulLoop;
   unsigned char *pubBuffer = (unsigned char *)pBuffer;

   if (bSetFilePosition)
   {  // position file pointer
      iError = FileSeek(pFile, ulFilePosition, SEEK_SET);
      if (iError != 0)
         return iError;
   }
   for (ulLoop=0; ulLoop < ulNumBytes; ulLoop++)
   {
      sTemp = (signed short)fgetc(pFile);
      if (sTemp == EOF)
         return ERR_UNEXPECTED_END_OF_FILE;
      pubBuffer[ulLoop] = (unsigned char)sTemp;
   }
   return 0;
}

/****************************************************************************
    Function: FileSeek
    Engineer: Ranjith T.C.
      Input: FILE *pFile - file for operation
            unsigned int ulOffset - offset from origin
            signed short siOrigin - origin (0 start of file, 1 current position, 2 EOF)
      Output: int - error code
  Description: Replace for fseek with special return code if operation is not successful.
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup
****************************************************************************/
static int FileSeek(FILE *pFile, unsigned int ulOffset, signed short siOrigin)
{
   if (pFile == NULL)
      return ERR_F_NULL_FILE;
   else
   {
      if (fseek(pFile, (int)ulOffset, siOrigin) != 0)
         return ERR_F_SEEK;
   }
   return 0;
}
/****************************************************************************
    Function: DeInitELF
    Engineer: Ranjith T.C.
      Input: Nil           
      Output: Nil
  Description: CleanUpMemory
Date           Initials    Description
21-Oct-2011    RTC          Initial & cleanup
****************************************************************************/
TyError DeInitELF(void)
{
    TyError ErrRet = NO_ERROR;

    if(NULL!=pDataBuffer)
        {
        free(pDataBuffer);
        pDataBuffer=NULL;
        }

    
    return ErrRet;

}
