/****************************************************************************
       Module: midlayer.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD MIPS MDI midlayer.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#ifndef __ML_DISA_H__
#define __ML_DISA_H__


typedef enum
{
   OTHER_INST16,
   JAL_INST16,
   JALX_INST16,
   JALR_RARX_INST16,
   JALRC_RARX_INST16,
   JR_RA_INST16,
   JR_RX_INST16,
   JRC_RA_INST16,
   JRC_RX_INST16,
   B_REL8_INST16,
   B_REL11_INST16
} TyInst16Types;

typedef struct
{
   unsigned short    uMask;
   unsigned short    uOpcode;
   TyInst16Types     InstType;
} TyDisa16Entry;



typedef enum
{
   OTHER_INST32,
   JR_INST32,
   JALR_INST32,
   J_INST32,
   JAL_INST32,
   JALX_INST32,
   B_REL16_INST32,
   ERET_INST32,
   DERET_INST32
} TyInst32Types;

typedef struct
{
   unsigned long  ulMask;
   unsigned long  ulOpcode;
   TyInst32Types     InstType;
} TyDisa32Entry;


TyError ML_Disa16(TyMLHandle 	*ptyHandle, 
                  unsigned long ulPCAddr, 
                  TyDisaResult * pDisaResult);
TyError ML_Disa32(TyMLHandle 	*ptyHandle,
                  unsigned long ulPCAddr, 
                  TyDisaResult * pDisaResult);

#endif   //__ML_DISA_H__
