/***************************trace_decoder.cpp*****************************/
/**  \file

\brief Implements the core functionalities involved in decoding the 
Zephy trace from the target.
*/
/*-----------------------------------------------------------------------------
* Version    Author          Date                  Description
*------------------------------------------------------------------------------
*   1.0      Rejeesh         10-Aug-2011         Initial Version Created

*----------------------------------------------------------------------------*/
/*****************************Standard Headers********************************/
#ifdef __LINUX
    #include <stdlib.h>
    #include <string.h>
#else
    #include "windows.h"
#endif
#include <assert.h>
#include <stdio.h>
/***********************Application Specific Headers**************************/
#define MDI_LIB               // We are an MDI Library not a debugger
#include "trace.h"
#include "trace_decoder.h"
#include "gerror.h"
#include "mdi.h"
#include "protocol/common/drvopxd.h"
#include "midlayer.h"
#include "protocol/common/drvopxdmips.h"
#include "commdefs.h"
#include "mips.h"
#include "trace_disassembler.h"
/*****************************Extern Variables********************/
extern TyTraceControlRegs tyTraceControlRegs;

/***********************Global variables**************************/
/*gulPC[0] represents T0 PC and gulPC[1] represents T1 PC*/
static unsigned int  gulPC[2];

/*Pointer to the next data word*/
static unsigned int* gpulDataWordPtr; 

/*gbSplitToken =1 if there is a split token*/
static bool gbSplitToken = 0;

/*1st nibble of split token is stored in this variable*/
static unsigned int  gulSplitToken = 0;

/*gbBranchPending[0/1] = 1 if next address of a Branch instruction is to be identified for T0/T1*/
static bool gbBranchPending[2] = {0};

/*gbProcessDelaySlot[0/1] = 1 if the delay slot instr. is to be processed for T0/T1*/
static bool gbProcessDelaySlot[2] = {0};

/*gbBranchAddrNotRec[0/1] = 1 if branch address in not recorded in raw trace for T0/T1*/
static bool gbBranchAddrNotRec[2] = {0};

/*Branch address of T0/T1 obtained from disassembler is stored in this variable*/
static unsigned int gulBranchPC[2] = {0};

/*Cycle group of tokens Count*/
static int  giCycleGrpCnt = 0;

/*Deferred data*/
static unsigned int giDeferredData = 0;

/*Stores the trace information of all lines*/
static TyTraceLineInfo *ptyTraceInfo        = NULL;

/*This pointer points to the current trace line info pointer*/
static TyTraceLineInfo *ptyTraceLineInfoCurrent = NULL;

/*Stores trace information of deferred loads*/
static TyDeferredLoadQueue tyDeferredLoadQueue[MAX_DEFERRED_LOAD];

/*Stores trace tokens which belong to same cycle group of tokens*/
static TyCycleGrpOfTokens  tyCycleGrpOfTokens[MAX_CYCLE_GRP_TOKEN];

/*This contains a single command word*/
static TyCommandWord tyCommandWord;

/******************************************************************************
*   Function: decoder_main
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	24-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This is the core function in which decoding of nth section is carried out

   \details Functionalities implemented are:
               - Decodes the trace buffer.
               - Disassembles the trace data
               - Temporary file is generated with Trace informations

   \param[in] pulData - Pointer to the structure which contains raw trace data
   \param[in] ulSize  - Total size of raw trace data in words.

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int decoder_main(unsigned int* pulData,unsigned int ulSize)
{
    assert(pulData != NULL);

    /*Open the file trace.tmp for writing the reconstructed trace data*/
    FILE *pTraceFile = fopen("trace.tmp","w");

    int iErrRet = 0;

    /*These are the different mode information received from raw trace*/
    const char *pcMode[]= {"User mode","Supervisor mode","Kernel mode",
                           "Exception mode","Error exception mode","Debug mode"};

    unsigned int *pulCommandWordPtr    = NULL;

    /*Calculate end of section*/
    unsigned int *pulEndPtr = pulData + (ulSize);

    /*Calculate the present processor modes*/
    unsigned int ulModeT0 = (*(pulData) & MODE_T0)>> MODE_T0_SHIFT;
    unsigned int ulModeT1 = (*(pulData) & MODE_T1)>> MODE_T1_SHIFT;

    /*Check if any delay slot instr. is pending*/
    unsigned  int iDS0 = (pulData[0] & DS_T0) >> DS_T0_SHIFT;
    unsigned  int iDS1 = (pulData[0] & DS_T1) >> DS_T1_SHIFT;

    /*Print the present processor modes to the file*/
    if(NULL !=pTraceFile && (ulModeT0 < MODE_MAX_VALUE) && (ulModeT1 < MODE_MAX_VALUE))
        {
            fprintf(pTraceFile,"T0 Processor Mode:%s T1 Procesor mode:%s\n",pcMode[ulModeT0],pcMode[ulModeT1]);
        }

    /*Initialize all decoder related structures and variables*/
    InitializeDecoder();

    /*Update gbBranchPending[0] based on iDS0  */
    if(1 == iDS0)
        {
        gbBranchPending[0] = 1;
        }
    /*Update gbBranchPending[1] based on iDS1  */
    if(1 == iDS1)
        {
        gbBranchPending[1] = 1;
        }

    /*Update PC[0/1]*/
    gulPC[0]     = *(pulData+1);
    gulPC[1]     = *(pulData+2);

    /*Update command word ptr*/
    pulCommandWordPtr   = pulData+3;

    /*Process word by word*/
    while (pulCommandWordPtr < pulEndPtr)
        {
        /*Pick the commad word to be processed*/
        tyCommandWord.ulCommandWord = *pulCommandWordPtr;

        /*gpulDataWordPtr should be the next word to command word*/
        gpulDataWordPtr = pulCommandWordPtr + 1;

        /*Process the command word*/
        iErrRet = ProcessCommandWord();

        if (NO_ERROR != iErrRet)
            {
            /*Return proper error message*/
            switch(iErrRet)
                {
                case 1 : 
                    /*Only decoder returns "1" for error*/
                    iErrRet = ERR_ZEPHYR_TRACE_DECODER;
                    break;
                case -1 :
                    /*End of Section,this is not an error*/
                    iErrRet = NO_ERROR;
                    break;
                default:
                    /*All other error messages are returned by Disassembler*/
                    iErrRet = ERR_ZEPHYR_TRACE_DISASSEMBLER;
                    break;
                }
            break;
            }
        /*Next command word should be gpulDataWordPtr*/
        pulCommandWordPtr = gpulDataWordPtr;
        }
    /*Check if any cycle group is pending
    NOTE:Not sure if these tokens need to be processed as no END token is identified.
         Here,we are processing anyway*/
    if(giCycleGrpCnt >0)
        {
           if(NO_ERROR != processCycleGrpOfTokens())
               {
               iErrRet =  ERR_ZEPHYR_TRACE_DECODER;
               }
        }

    /*Log the trace data to trace.tmp file and free the ptyTraceInfo pointer*/
    if ((pTraceFile != NULL) && (ptyTraceInfo != NULL))
        {
        while (ptyTraceInfo != NULL)
            {
            TyTraceLineInfo* ptyTraceInfoTemp = ptyTraceInfo;
            fprintf(pTraceFile,"%x;%s;%d;0x%08x;0x%08x;%s;0x%08x;0x%08x\n",
                    ptyTraceInfo->ucFilterCode,
                    ptyTraceInfo->ucInstructionType,
                    ptyTraceInfo->iThreadID,
                    ptyTraceInfo->ulPC,
                    ptyTraceInfo->ulOpcode,
                    ptyTraceInfo->ucInstruction,
                    ptyTraceInfo->ulLoadStoreAddr,
                    ptyTraceInfo->ulLoadStoreData);
            fflush(pTraceFile);
            ptyTraceInfo = ptyTraceInfo->ptyNext;            
            free(ptyTraceInfoTemp);
            };
        fflush(pTraceFile);
        fclose(pTraceFile);
        }
    return iErrRet;
}
/******************************************************************************
*   Function: InitializeDecoder
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	10-Nov-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function initializes the decoder

   \details Functionalities implemented are:
               - Initialize all global variables and structures used by the decoder
*/
void InitializeDecoder()
{
    int i = 0;

    gbSplitToken            = 0;
    gbBranchPending[0]      = 0;
    gbBranchPending[1]      = 0;

    giCycleGrpCnt       = 0;

    /*TODO: Kimming says deferred load data can be spread accross 2 section
            Hence this initialization might not be needed.*/
    for (i = 0;i<MAX_DEFERRED_LOAD;i++)
        {
        /*This is initialized as 0xF instead of 0x0 since an entry can be 0x0*/
        tyDeferredLoadQueue[i].iEntry           = 0xF;
        tyDeferredLoadQueue[i].tyTraceLineInfo  = NULL;
        }

    for (i = 0;i<MAX_CYCLE_GRP_TOKEN;i++)
        {
        tyCycleGrpOfTokens[i].ptyTraceLineInfo   = NULL;
        tyCycleGrpOfTokens[i].ulByteToken        = 0;
        }
}
/******************************************************************************
*   Function: InitializeTracePtr
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	10-Nov-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function initializes the trace pointer

   \details Functionalities implemented are:
               - Initializes the trace pointer with default values.
*/
void InitializeTracePtr(TyTraceLineInfo* ptyTempPtr)
{
    ptyTempPtr->iThreadID       = 0;
    ptyTempPtr->ptyNext         = NULL;
    ptyTempPtr->ucFilterCode    = 0;
    strcpy(ptyTempPtr->ucInstruction,"----");
    strcpy(ptyTempPtr->ucInstructionType,"UNDEFINED");
    ptyTempPtr->ulLoadStoreAddr = 0x0;
    ptyTempPtr->ulLoadStoreData = 0x0;
    ptyTempPtr->ulNextPC        = 0x0;
    ptyTempPtr->ulOpcode        = 0x0;
    ptyTempPtr->ulPC            = 0x0;

}
/******************************************************************************
*   Function: ProcessCommandWord
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	26-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes a single command word

   \details Functionalities implemented are:
               - Process single command word

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessCommandWord()
{
    int iNibbleCnt;
    int iErrRet = 0;
    TyTraceLineInfo *ptyTempPtr                  = NULL;

    /*This pointer temporarily stores the current trace line info pointer*/
    TyTraceLineInfo *ptyTraceLineInfoCurrentTemp = NULL;

    unsigned int ulByteToken                     = 0;

    for (iNibbleCnt=0;iNibbleCnt<NIBBLE_IN_WORD;)
        {

        /*Allocate memory for the tyTraceLineInfo structure*/
        ptyTempPtr = (TyTraceLineInfo*)malloc(sizeof(TyTraceLineInfo));
        InitializeTracePtr(ptyTempPtr);

        if (ptyTraceInfo == NULL)
            {
            ptyTraceInfo                = ptyTempPtr;
            ptyTraceLineInfoCurrent     = ptyTempPtr;
            }
        else
            {
            if (ptyTraceLineInfoCurrent != NULL)
                {
                ptyTraceLineInfoCurrentTemp      = ptyTraceLineInfoCurrent;
                ptyTraceLineInfoCurrent->ptyNext = ptyTempPtr;
                ptyTraceLineInfoCurrent          = ptyTraceLineInfoCurrent->ptyNext;
                }
            }

        /*Take nibble by nibble and process*/
        switch (iNibbleCnt)
            {
            /*MSB Nibble*/
            case 0:
                if (gbSplitToken)
                    {
                    gbSplitToken = 0;
                    ulByteToken = JOIN_NIBBLES(gulSplitToken,NIBBLE_0);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt++;
                    }
                else
                    if (FOUR_BIT_NIBBLE_CODE(NIBBLE_0))
                    {
                        //Process Four Bit Nibble code                        
                        iErrRet = ProcessNibbleCode(NIBBLE_0,ptyTraceLineInfoCurrent,0);
                        iNibbleCnt++;
                    }
                /*8-bit byte code*/
                    else
                    {
                        ulByteToken = JOIN_NIBBLES(NIBBLE_0,NIBBLE_1);
    
                        /*Add this byte to cycle group of tokens*/
                        iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);
    
                        iNibbleCnt += 2;
                    }
                break;
            case 1:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_1))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_1,ptyTraceLineInfoCurrent,1);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    ulByteToken = JOIN_NIBBLES(NIBBLE_1,NIBBLE_2);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt += 2;
                    }
                break;

            case 2:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_2))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_2,ptyTraceLineInfoCurrent,2);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    ulByteToken = JOIN_NIBBLES(NIBBLE_2,NIBBLE_3);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt += 2;
                    }
                break;

            case 3:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_3))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_3,ptyTraceLineInfoCurrent,3);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    ulByteToken = JOIN_NIBBLES(NIBBLE_3,NIBBLE_4);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt += 2;
                    }
                break;

            case 4:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_4))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_4,ptyTraceLineInfoCurrent,4);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    ulByteToken = JOIN_NIBBLES(NIBBLE_4,NIBBLE_5);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt += 2;
                    }
                break;

            case 5:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_5))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_5,ptyTraceLineInfoCurrent,5);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    ulByteToken = JOIN_NIBBLES(NIBBLE_5,NIBBLE_6);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt += 2;
                    }
                break;

            case 6:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_6))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_6,ptyTraceLineInfoCurrent,6);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    ulByteToken = JOIN_NIBBLES(NIBBLE_6,NIBBLE_7);

                    /*Add this byte to cycle group of tokens*/
                    iErrRet = PrepareCycleGrpOfTokens(ulByteToken,ptyTraceLineInfoCurrent);

                    iNibbleCnt += 2;
                    }
                break;

            case 7:
                if (FOUR_BIT_NIBBLE_CODE(NIBBLE_7))
                    {
                    //Process Four Bit Nibble code                        
                    iErrRet = ProcessNibbleCode(NIBBLE_7,ptyTraceLineInfoCurrent,7);
                    iNibbleCnt++;
                    }
                /*8-bit byte code*/
                else
                    {
                    gulSplitToken = NIBBLE_7;
                    gbSplitToken = 1;
                    iNibbleCnt++;
                    if(ptyTraceLineInfoCurrentTemp != NULL)
                        {
                            ptyTraceLineInfoCurrentTemp->ptyNext = NULL;
                        }
                    free(ptyTraceLineInfoCurrent);                                        
                    ptyTraceLineInfoCurrent = ptyTraceLineInfoCurrentTemp;
                    }
                break;
            default:
                break;
            }
        if (NO_ERROR != iErrRet )
            {
            break;
            }
        }

    return iErrRet;
}
/******************************************************************************
*   Function: PrepareCycleGrpOfTokens
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	09-Nov-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function prepares cycle group of tokens

   \details Functionalities implemented are:
               - Add the byte token entry to TyCycleGrpOfTokens
               - Process cycle group of tokens if the byte token is an end token

   \param[in] ulByteToken               - Byte token
   \param[out] ptyTraceLineInfoCurrent  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int PrepareCycleGrpOfTokens(unsigned int ulByteToken,TyTraceLineInfo* ptyTraceLineInfo)
{
    int iErrRet = 0;

    /*If byte token is an idle cycle token,then process it and exit here itself
      Idle cycle is not part of cycle group of tokens*/
    if(IDLE_CYCLE(ulByteToken) )
        {  
            TyTraceToken tyTraceToken;
            tyTraceToken.ucToken = (unsigned char)ulByteToken;
            iErrRet = ProcessIdleCycle(tyTraceToken,ptyTraceLineInfo);
            return iErrRet;
        }

    /*This is not an idle cycle,so add this entry to cycle group of tokens*/
    if(giCycleGrpCnt < MAX_CYCLE_GRP_TOKEN)
        {
        tyCycleGrpOfTokens[giCycleGrpCnt].ptyTraceLineInfo  = ptyTraceLineInfo;
        tyCycleGrpOfTokens[giCycleGrpCnt].ulByteToken       = ulByteToken;
        giCycleGrpCnt++;
        }
    else
        return ERR_DECODER;

    /*If End of Cycle group,process the tokens*/
    if (END_OF_CYCLE(ulByteToken))
        {
        iErrRet = processCycleGrpOfTokens();
        if(NO_ERROR != iErrRet )
            {
            giCycleGrpCnt = 0;
            return iErrRet;
            }
        /*Reset giCycleGrpCnt*/
        giCycleGrpCnt = 0;
        }
    else
        {
        /*Do nothing*/
        }
    return iErrRet;
}
/******************************************************************************
*   Function: processCycleGrpOfTokens
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	30-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes Cycle group of tokens

   \details Functionalities implemented are:
               - If  Data/Address words are present,then they are processed first
               - Then Command words are processed.
 
   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int processCycleGrpOfTokens()
{
    int iErrRet = 0;
    bool bLA    = 0;
    bool bLD    = 0;
    bool bSA    = 0;
    bool bSD    = 0;
    int i       = 0;

    int iThreadID = 0;
    TyTraceToken tyTraceToken;

    /*NOTE: As per the spec,data/address words,when present,always appear in the following order: 
        1. Operand address of a load/store instruction.(K pipe)
        2. Operand data of a load/store instruction.(K pipe)
        3. Target Address of a branch instruction.(J Pipe)

    This order will not change even if the order of instr. in cycle group change.
    So here we are processing Address/data words(if present) before processing tokens.

    ASSUMPTION: Cycle group of token will not have both Load and store instruction,only one*/

    /*Process Operand address (if any) of a load/store instruction.(K pipe)*/
    for(i = 0; i<giCycleGrpCnt; i++)
        {
            tyTraceToken.ucToken    = (unsigned char)tyCycleGrpOfTokens[i].ulByteToken;

            /*Check if it is a Load Instr.(Cached or uncached)*/
            if(IsLoadInstrToken(tyTraceToken))
                {                
                iThreadID = tyTraceToken.Bits.Bit_4;

                /*Check if LA is recorded*/
                bLA = (iThreadID)? tyTraceControlRegs.tyTraceControl3.Bits.T1_LA:
                                   tyTraceControlRegs.tyTraceControl3.Bits.T0_LA;
                if(bLA)
                    {
                    /*If LA is recorded, present dataword contains the Load Address*/
                    tyCycleGrpOfTokens[i].ptyTraceLineInfo->ulLoadStoreAddr = *gpulDataWordPtr++;                                                
                    }
                }
            /*Check if it is a Store Instr.*/
            if(IsStoreInstrToken(tyTraceToken))
                {
                iThreadID = tyTraceToken.Bits.Bit_4;

                /*Check if SA is recorded*/
                bSA = (iThreadID)? tyTraceControlRegs.tyTraceControl3.Bits.T1_SA:
                                   tyTraceControlRegs.tyTraceControl3.Bits.T0_SA;
                if(bSA)
                    {
                    /*If SA is recorded, present dataword contains the Store Address*/
                    tyCycleGrpOfTokens[i].ptyTraceLineInfo->ulLoadStoreAddr = *gpulDataWordPtr++;                                                
                    }
                } 
        }

    /*Process Operand data (if any) of a load/store instruction.(K pipe)*/
    for(i = 0; i<giCycleGrpCnt; i++)
        {
            tyTraceToken.ucToken    = (unsigned char)tyCycleGrpOfTokens[i].ulByteToken;

            /*Check if it is Load instr with cache hit*/
            if(IsLoadInstrCacheHit(tyTraceToken))
                {
                iThreadID = tyTraceToken.Bits.Bit_4;

                /*Check if Load Data is recorded*/
                bLD = (iThreadID)? tyTraceControlRegs.tyTraceControl3.Bits.T1_LD:
                                   tyTraceControlRegs.tyTraceControl3.Bits.T0_LD;
                if(bLD)
                    {
                    /*If LD is recorded, present dataword contains the Load Data*/
                    tyCycleGrpOfTokens[i].ptyTraceLineInfo->ulLoadStoreData = *gpulDataWordPtr++;                                              
                    }
                }
            /*Check if it is data for deferred load*/
            if(IsDeferredDataToken(tyTraceToken))
                {
                iThreadID = tyTraceToken.Bits.Bit_4;

                /*Check if Load Data is recorded*/
                bLD = (iThreadID)? tyTraceControlRegs.tyTraceControl3.Bits.T1_LD:
                                   tyTraceControlRegs.tyTraceControl3.Bits.T0_LD;
                if(bLD)
                    {
                    /*If LD is recorded, present dataword contains the deferred Data*/
                    giDeferredData = *gpulDataWordPtr++;                   
                    }
                }
            /*Check if it is data for store instruction*/
            if(IsStoreInstrToken(tyTraceToken))
                {
                iThreadID = tyTraceToken.Bits.Bit_4;

                /*Check if Load Data is recorded*/
                bSD = (iThreadID)? tyTraceControlRegs.tyTraceControl3.Bits.T1_SD:
                                   tyTraceControlRegs.tyTraceControl3.Bits.T0_SD;
                if(bSD)
                    {
                    /*If SD is recorded, present dataword contains the Store Data*/
                    tyCycleGrpOfTokens[i].ptyTraceLineInfo->ulLoadStoreData = *gpulDataWordPtr++;                                                
                    }
                } 
        }

    /*Now process the tokens,BA is updated in ProcessByteCode()*/
    for (i=0;i<giCycleGrpCnt;i++)
        {
        iErrRet = ProcessByteCode(tyCycleGrpOfTokens[i].ulByteToken,tyCycleGrpOfTokens[i].ptyTraceLineInfo);
        if (iErrRet != NO_ERROR)
            {
            return iErrRet;
            }
        }
    return iErrRet;
}
/******************************************************************************
*   Function: ProcessByteCode
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	26-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes an 8-bit byte code

   \details Functionalities implemented are:
               - Processes 8-bit byte code

   \param[in] ulByteToken       - Byte token
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessByteCode(unsigned int ulByteToken,TyTraceLineInfo* tyTraceLineInfo)
{

    TyTraceToken tyTraceToken;
    tyTraceToken.ucToken = (unsigned char)ulByteToken;
    int iErrRet= 0;

    /*Check if any Branch instruction was executed,if so check for delay-slot instr.*/
    /*NOTE: BA follows the delay slot inst,not the branch token*/
    if(gbBranchPending[0] ||  gbBranchPending[1])
        {
            /*Branch instruction of T0/T1 was executed,so check for respective delay slot instr.*/
            CheckForDelaySlotInst(tyTraceToken); 
        }

    switch (tyTraceToken.Bits.Bit7_5)
        {
        case 2  :
            iErrRet = ProcessJPipeInstr(tyTraceToken,tyTraceLineInfo);
            break;
        case 3  :
            iErrRet = ProcessIdleCycle(tyTraceToken,tyTraceLineInfo);
            break;
        case 4  :
            iErrRet = ProcessKPipeInstr(tyTraceToken,tyTraceLineInfo);
            break;
        case 5  :
            iErrRet = ProcessResumeEventsCacheMiss(tyTraceToken,tyTraceLineInfo);
            break;
        case 6  :
            iErrRet = ProcessDeferredLoad(tyTraceToken,tyTraceLineInfo);
            break;
        case 7  :
            iErrRet = ProcessDeferredData(tyTraceToken,tyTraceLineInfo);
            break;
            /*These cases should be processed as 4-bit nibble code*/
        case 0  :
        case 1  :
        default :
            iErrRet = 1;
            break;
        }
    /*If presently executed instr. is a delay-slot instr,
      then update the Branch PC of the corresponding thread*/
    if(gbProcessDelaySlot[0] || gbProcessDelaySlot[1])
        {
        UpdatePCWithBranchAddr();
        }

    return iErrRet;

}
/******************************************************************************
*   Function: ProcessNibbleCode
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	26-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes an 4-bit nibble code

   \details Functionalities implemented are:
               - Processes 4-bit nibble code

   \param[in] ulNibbleToken     - Nibble token
   \param[in] iNibbleCnt        - Position of nibble in Command word
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessNibbleCode(unsigned int ulNibbleToken,TyTraceLineInfo* tyTraceLineInfo,int iNibbleCnt)
{
    int iErrRet = 0;

    /*End token*/
    if (END_TOKEN == ulNibbleToken)
        {
        /*Check if this end token is the 1st token of the command word*/
        if (TOKEN_1 == iNibbleCnt)
            {
            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",END_OF_SECTION);

            iErrRet = ERR_END_OF_SECTION;
            }
        else
        /*Do nothing,just process the next NIBBLE*/
            {
            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",END_OF_CONTROL_WORD);   

            /*Kimming's mail says END_OF_CONTROL_WORD at any nibble position can be treated as EOS*/
            iErrRet = ERR_END_OF_SECTION;
            }
        /*Before exiting from the decoder, Process any pending cycle group of tokens*/
        if(giCycleGrpCnt > 0)
            {
               if(NO_ERROR != processCycleGrpOfTokens())
                   {
                   return ERR_DECODER;
                   }
            }            
        }
    /*Idle Cycle count(1-3 cycles)*/
    else
        {
        /*Update Filter with Idle cycle Information*/
        tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_IDLE_CYCLE_MASK;

        /*Update no: of idle cycle in Line info structure*/
        sprintf(tyTraceLineInfo->ucInstructionType,"%d %s",(int)ulNibbleToken,IDLE_CYCLES);
        }

    return iErrRet;
}
/******************************************************************************
*   Function: ProcessIdleCycle
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	27-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes 8-bit IdleCycle information

   \details Functionalities implemented are:
               - Processes 8-bit IdleCycle information

   \param[in] tyTraceToken      - Byte token
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessIdleCycle(TyTraceToken &tyTraceToken,TyTraceLineInfo* tyTraceLineInfo)
{
    int iIdleCnt = (tyTraceToken.Bits.Bit_3 << 3) | tyTraceToken.Bits.Bit2_0;

    /*Update Filter with Idle cycle Information*/
    tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_IDLE_CYCLE_MASK;

    /*Update no: of idle cycle in Line info structure*/
    sprintf(tyTraceLineInfo->ucInstructionType,"%d %s",(int)iIdleCnt,IDLE_CYCLES);

    return 0;
}
/******************************************************************************
*   Function: ProcessJPipeInstr
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	27-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes J-Pipe instruction byte-code

   \details Functionalities implemented are:
               - Processes J-Pipe instruction byte-code

   \param[in] tyTraceToken      - Byte token
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessJPipeInstr(TyTraceToken &tyTraceToken,TyTraceLineInfo* tyTraceLineInfo)
{
    int iErrRet = 0;
    bool bTCR   = 0;
    bool bATB   = 0;
    int iBits   = tyTraceToken.Bits.Bit2_0;

    /*Update ThreadID,currentPC and filter*/
    tyTraceLineInfo->iThreadID    = (unsigned char)tyTraceToken.Bits.Bit_4;
    tyTraceLineInfo->ulPC         = (tyTraceLineInfo->iThreadID)?gulPC[1]:gulPC[0];
    tyTraceLineInfo->ucFilterCode = (tyTraceLineInfo->iThreadID)?tyTraceLineInfo->ucFilterCode | FILTER_T1_MASK:
                                    tyTraceLineInfo->ucFilterCode | FILTER_TO_MASK;
    switch (iBits)
        {
        /*Arithmetic instruction*/        
        case 0 : 
            /*Disassemble instruction,next address not required*/
            iErrRet = Disassemble(tyTraceLineInfo,0);

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",JPIPE_ARITHMETIC_INSTR);
            break;

            /*Branch instruction- not taken,Normal Branch*/
        case 1 : 
            /*Disassemble instruction,next address not required*/
            iErrRet = Disassemble(tyTraceLineInfo,false);

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",JPIPE_BRANCH_INSTR_NOT_TAKEN);     
            break;

            /*Branch instruction- not taken likely*/
        case 2 : 
            /*Disassemble instruction,next address not required*/
            iErrRet = Disassemble(tyTraceLineInfo,false);

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=8;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",JPIPE_BRANCH_INSTR_NOT_TAKEN_LIKELY); 
            break;

            /*Branch instruction- J or taken branch*/
        case 3 : 
            /*Check if Branch Target Address is recorded*/
            bATB = (tyTraceLineInfo->iThreadID)?tyTraceControlRegs.tyTraceControl3.Bits.T1_ATB:
                   tyTraceControlRegs.tyTraceControl3.Bits.T0_ATB;

            /*Disassemble instruction,next address not required if ATB is recorded*/
            iErrRet = Disassemble(tyTraceLineInfo,!bATB);

            /*Update Filter*/
            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_BRANCH_MASK;

            /*Update Pending branch adress flag*/
            gbBranchPending[tyTraceLineInfo->iThreadID] = true;

            /*If ATB not recorded*/
            if (!bATB)
                {
                   gulBranchPC[tyTraceLineInfo->iThreadID]          = tyTraceLineInfo->ulNextPC;
                   gbBranchAddrNotRec[tyTraceLineInfo->iThreadID]   = true;                
                }

            /*This is the location for delay slot instr.*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;
            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",JPIPE_BRANCH_INSTR_J_OR_BRANCH); 
            break;

            /*Branch instruction- JAL,B..AL,taken*/
        case 4 : 
            /*Check if Branch Target Address is recorded*/
            bTCR = (tyTraceLineInfo->iThreadID)?tyTraceControlRegs.tyTraceControl3.Bits.T1_TCR:
                   tyTraceControlRegs.tyTraceControl3.Bits.T0_TCR;

            /*Disassemble instruction,next address not required if bTCR is recorded*/
            iErrRet = Disassemble(tyTraceLineInfo,!bTCR);

            /*Update Filter*/
            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_BRANCH_MASK;

            /*Update Pending branch adress flag*/
            gbBranchPending[tyTraceLineInfo->iThreadID] = true;

            /*Taken Branch address is not recorded*/
            if (!bTCR)
                {
                   gulBranchPC[tyTraceLineInfo->iThreadID]          = tyTraceLineInfo->ulNextPC;
                   gbBranchAddrNotRec[tyTraceLineInfo->iThreadID]   = true;
                }

            /*This is the location for delay slot instr.*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;
            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",JPIPE_BRANCH_INSTR_JAL_BAL_BRANCH); 
            break;

        case 5 : 
            /*Reserved for Future*/
            break;

            /*Branch instruction- JR,JALR*/
        case 6 : 
            /*Disassemble instruction,next address not required */
            iErrRet = Disassemble(tyTraceLineInfo,false);

            /*Update Filter*/
            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_BRANCH_MASK;

            /*Update Pending branch adress flag*/
            gbBranchPending[tyTraceLineInfo->iThreadID] = true;

            /*This is the location for delay slot instr.*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",JPIPE_BRANCH_INSTR_JR_JALR); 
            break;
            /*Reserved for Future*/
        case 7 :
            break;
            /*Error case*/
        default:
            iErrRet = 1;
            break;
        }
    return iErrRet;
}
/******************************************************************************
*   Function: ProcessJPipeInstr
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	27-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes K-Pipe instruction byte-code

   \details Functionalities implemented are:
               - Processes K-Pipe instruction byte-code

   \param[in] tyTraceToken      - Byte token
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessKPipeInstr(TyTraceToken &tyTraceToken,TyTraceLineInfo* tyTraceLineInfo)
{
    int iErrRet = 0;

    /*Update ThreadID,currentPC and filter*/
    tyTraceLineInfo->iThreadID    = (unsigned char)tyTraceToken.Bits.Bit_4;
    tyTraceLineInfo->ulPC         = (tyTraceLineInfo->iThreadID)?gulPC[1]:gulPC[0];
    tyTraceLineInfo->ucFilterCode = (tyTraceLineInfo->iThreadID)?tyTraceLineInfo->ucFilterCode | FILTER_T1_MASK:
                                    tyTraceLineInfo->ucFilterCode | FILTER_TO_MASK;

    switch (tyTraceToken.Bits.Bit2_0)
        {
        /*Arithmetic instruction*/
        case 0 : 
            iErrRet = Disassemble(tyTraceLineInfo,0);

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_ARITHMETIC_INSTR); 

            break;

            /*Control instruction*/
        case 1 : 

            iErrRet = Disassemble(tyTraceLineInfo,0);

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_CONTROL_INSTR); 

            break;

            /*Load instruction,cache hit*/
        case 2 : 

            /*Disassemble instruction,next address not required */
            iErrRet = Disassemble(tyTraceLineInfo,0);

            /*Update Filter*/
            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_LOAD_MASK;

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_LOAD_INSTR_CACHE_HIT); 

            break;

            /*Reserved for Future*/
        case 3 :             
            break;

            /*Store instruction,cache hit*/
            /*Store instruction,cache miss*/
            /*Store instruction,uncached*/
        case 4 :
        case 5 :  
        case 6 :

            /*Disassemble instruction,next address not required */
            iErrRet = Disassemble(tyTraceLineInfo,0);

            /*Update Filter*/
            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_STORE_MASK;

            /*Update next PC*/
            gulPC[tyTraceLineInfo->iThreadID]+=4;

            /*Cache hit,update instr. type*/
            if (4 == tyTraceToken.Bits.Bit2_0)
                {
                sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_STORE_INSR_CACHE_HIT); 
                }
            /*Cache miss,update filter and instr. type*/
            if (5 == tyTraceToken.Bits.Bit2_0)
                {
                tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_CACHE_MISS_MASK;
                sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_STORE_INSR_CACHE_MISS); 
                }
            /*Uncached,update instr. type*/
            if (6 == tyTraceToken.Bits.Bit2_0)
                {
                sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_STORE_INSR_UNCACHED); 
                }
            break;
            /*Reserved for Future*/
        case 7 :
            break;

            /*Error case*/
        default:
            iErrRet = 1;
            break;
        }
    return iErrRet;
}
/******************************************************************************
*   Function: ProcessDeferredLoad
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	27-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes deferred load instruction byte-code

   \details Functionalities implemented are:
               - Processes deferred load instruction byte-code

   \param[in] tyTraceToken      - Byte token
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessDeferredLoad(TyTraceToken &tyTraceToken,TyTraceLineInfo* tyTraceLineInfo)
{
    int iErrRet = 0;

    /*Update ThreadID,currentPC and filter*/
    tyTraceLineInfo->iThreadID    = (unsigned char)tyTraceToken.Bits.Bit_4;
    tyTraceLineInfo->ulPC         = (tyTraceLineInfo->iThreadID)?gulPC[1]:gulPC[0];
    tyTraceLineInfo->ucFilterCode = (tyTraceLineInfo->iThreadID)?tyTraceLineInfo->ucFilterCode | FILTER_T1_MASK:
                                    tyTraceLineInfo->ucFilterCode | FILTER_TO_MASK;

    /*Disassemble instruction,next address not required */
    iErrRet = Disassemble(tyTraceLineInfo,0);

    /*Update Filter*/
    tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_LOAD_MASK;

    /*Update next PC*/
    gulPC[tyTraceLineInfo->iThreadID]+=4;

    /*Update instruction type*/
    sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_LOAD_INSTR_DATA_DEFERRED); 

    /*Add the entry to the deferred load queue*/

    tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].iEntry            = tyTraceToken.Bits.Bit2_0;
    tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].tyTraceLineInfo   = tyTraceLineInfo;

    return iErrRet;
}
/******************************************************************************
*   Function: ProcessDeferredData
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	27-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes Deferred data

   \details Functionalities implemented are:
               - Processes Deferred data(cached and uncached)

   \param[in] tyTraceToken      - Byte token

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessDeferredData(TyTraceToken &tyTraceToken,TyTraceLineInfo* tyTraceLineInfo)
{
    bool bEntryMatch = 0;
    int iErrRet = 0;
    int iEntry = tyTraceToken.Bits.Bit2_0;

    if(tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].iEntry == iEntry)
        {
            bEntryMatch = 1;
            /*Reset the array value*/
            tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].iEntry = 0xF;
        }

    /*Match found*/
    if (bEntryMatch)
        {
        /*Cache miss*/
        if (0==tyTraceToken.Bits.Bit_4)
            {
            /*Update Filter*/
            if(tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].tyTraceLineInfo != NULL)
                {
                tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].tyTraceLineInfo->ucFilterCode = tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].tyTraceLineInfo->ucFilterCode | FILTER_CACHE_MISS_MASK;
                }
            else
                {
                return ERR_DECODER;
                }
            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_DATA_DEFERRED_LOAD_CACHE_MISS);
            }
        /*Uncached*/
        else
            {
            /*Update instruction type*/
            sprintf(tyTraceLineInfo->ucInstructionType,"%s",KPIPE_DATA_DEFERRED_LOAD_UNCACHED);
            }

        tyDeferredLoadQueue[tyTraceToken.Bits.Bit2_0].tyTraceLineInfo->ulLoadStoreData = giDeferredData;

        }
    else
        {
        /*This data should be the deferred data of a load which was not traced.
          Do nothing,just ignore*/
        }    
    return iErrRet;
}
/******************************************************************************
*   Function: ProcessResumeEvents
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	27-Oct-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function processes resume,Event and Cache miss tokens

   \details Functionalities implemented are:
               - Processes resume, Event and cache tokens

   \param[in]  tyTraceToken     - Byte token
   \param[out] tyTraceLineInfo  - Pointer to the structure element to be updated with Trace info

   \retval RET_SUCCESS - On Success and appropriate return value if otherwise.
*/
int ProcessResumeEventsCacheMiss(TyTraceToken &tyTraceToken,TyTraceLineInfo* tyTraceLineInfo)
{
    int iErrRet = 0;

        /*Resume/Event token*/
        if(0 == tyTraceToken.Bits.Bit_4)
            {
                /*Resume token*/
                if(0 == tyTraceToken.Bits.Bit2_0)
                    {
                    /*Update instruction type*/
                    tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_EVENT_MASK;
                    sprintf(tyTraceLineInfo->ucInstructionType,"%s",TRACING_RE_ENABLED);
                    gulPC[0] = *gpulDataWordPtr++;
                    gulPC[1] = *gpulDataWordPtr++;
                    }
                else
                    {
                    /*Update instruction type*/
                    sprintf(tyTraceLineInfo->ucInstructionType,"%s",EVENT);
                    /*Update Filter*/
                    tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_EVENT_MASK;
                    /*If Event on Thread 0,update PC[T0]*/
                    if(1 == (tyTraceToken.Bits.Bit2_0 & 0x01))
                        {
                            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_TO_MASK;
                            tyTraceLineInfo->ulPC = gulPC[0];
                            gulPC[0] = *gpulDataWordPtr++;
                        }
                    /*If Event on Thread 1,update PC[T1]*/
                    if(2 == (tyTraceToken.Bits.Bit2_0 & 0x02))
                        {
                            tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_T1_MASK;
                            tyTraceLineInfo->ulPC = gulPC[1];
                            gulPC[1] = *gpulDataWordPtr++;
                        }  

                    }   
            }
        else
            {
            /*Cache miss token*/
            if(1 == tyTraceToken.Bits.Bit_4)
                {
                /*Update instruction type*/
                sprintf(tyTraceLineInfo->ucInstructionType,"%s",CACHE_MISS);
                /*Update Filter*/
                /*NOTE:This may not be a useful filter as PC is not captured for these misses*/
                tyTraceLineInfo->ucFilterCode = tyTraceLineInfo->ucFilterCode | FILTER_CACHE_MISS_MASK;
                /*TODO: Check if anything else is required*/             
                }
            /*Error-Unknown token*/
            else
                {
                iErrRet = 1;
                }
            }

    return iErrRet;
}
/******************************************************************************
*   Function: IsBranchAddressNotRecorded
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	15-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function returns whether the branch address is recorded or not in the raw trace

   \details Functionalities implemented are:
               - returns whether the branch address is recorded or not in the raw trace
               - Reset the flag

   \retval bool  - 1 if branch address is NOT recorded,0 otherwise

*/
/*bool IsBranchAddressNotRecorded()
{
    if(gbBranchAddrNotRec)
    {
        gbBranchAddrNotRec = false;
        return true;
    }
    return false;
}*/
/******************************************************************************
*   Function: IsDelaySlotInstProcessed
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	15-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function returns whether the Delay Slot Inst. is processed or not

   \details Functionalities implemented are:
               - returns whether the Delay Slot Inst. is processed or not
               - Reset the flag

   \retval bool  - 1 if Delay Slot Inst. is processed,0 otherwise

*/
/*bool IsDelaySlotInstProcessed()
{
    if(gbProcessDelaySlot)
        {
            gbProcessDelaySlot = false;
            return true;
        }
    return false;
}*/
/******************************************************************************
*   Function: IsLoadInstrToken
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	30-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function returns whether the input token is a load instr. or not

   \details Functionalities implemented are:
               - returns whether the input token is a load instr. or not

   \param[in]  tyTraceToken     - Byte token

   \retval bool  - 1 if this is a load instr. token,0 otherwise

*/
bool IsLoadInstrToken(const TyTraceToken &tyTraceToken)
{
    /*Token is a load instr. token if:
    1. Bit[7:5] is 0x6(Load instr. data deferred) OR
    2. Bit[7:5] is 0x4 AND Bit[2:0] is 0x2(Load instr,cache hit)*/
    if( (tyTraceToken.Bits.Bit7_5 == 0x6) || 
        (tyTraceToken.Bits.Bit7_5 == 0x4&& tyTraceToken.Bits.Bit2_0 == 0x2))
        {
        return true;
        }
    else{
        return false;
        }

}
/******************************************************************************
*   Function: IsStoreInstrToken
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	30-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function returns whether the input token is a store instr. or not

   \details Functionalities implemented are:
               - returns whether the input token is a store instr. or not

   \param[in]  tyTraceToken     - Byte token

   \retval bool  - 1 if this is a store instr. token,0 otherwise

*/
bool IsStoreInstrToken(const TyTraceToken &tyTraceToken)
{
    /*Token is a store instr. token if:
    1. Bit[7:5] is 0x4 AND
    2. Bit[2:0] is 0x4 or 0x5 or 0x6*/
    if( (tyTraceToken.Bits.Bit7_5 == 0x4) && 
        (tyTraceToken.Bits.Bit2_0 == 0x4 || tyTraceToken.Bits.Bit2_0 == 0x5 || tyTraceToken.Bits.Bit2_0 == 0x6))
        {
        return true;
        }
    else
        {
        return false;
        }
}
/******************************************************************************
*   Function: IsDeferredDataToken
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	30-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function returns whether the input token is a deferred data token

   \details Functionalities implemented are:
               - returns whether the input token is a deferred data token or not

   \param[in]  tyTraceToken     - Byte token

   \retval bool  - 1 if this is a deferred data token,0 otherwise

*/
bool IsDeferredDataToken(const TyTraceToken &tyTraceToken)
{
    /*Token is a store instr. token if:
    1. Bit[7:5] is 0x7*/

    if(tyTraceToken.Bits.Bit7_5 == 0x7)
        {
        return true;
        }
    else
        {
        return false;
        }
}
/******************************************************************************
*   Function: IsLoadInstrCacheHit
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	30-Dec-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function returns whether the input token is a Load Instr with cache hit

   \details Functionalities implemented are:
               - returns whether the input token is a Load Instr with cache hit

   \param[in]  tyTraceToken     - Byte token

   \retval bool  - 1 if this is a Load Instr with cache hit token,0 otherwise

*/
bool IsLoadInstrCacheHit(const TyTraceToken &tyTraceToken)
{
    /*Token is a Load Instr with cache hit token if:
    1. Bit[7:5] is 0x4 AND
    2. Bit[7:5] is 0x2*/

    if(tyTraceToken.Bits.Bit7_5 == 0x4 && tyTraceToken.Bits.Bit2_0 == 0x2)
        {
        return true;
        }
    else
        {
        return false;
        }
}
/******************************************************************************
*   Function: CheckForDelaySlotInst
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	11-Jan-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function checks whether the present token is a delay slot instruction.

   \details Functionalities implemented are:
               - checks whether the present token is a delay slot instruction.
               - Set and reset the branch flags appropriately

   \param[in]  tyTraceToken     - Byte token
*/
void CheckForDelaySlotInst(const TyTraceToken &tyTraceToken)
{
    /*If delay slot is pending for Thread 0*/
    if(gbBranchPending[0])
        {
        if (0 == tyTraceToken.Bits.Bit_4)
            {
            /*Not yet guaranteed that this is an instr. token of Thread 0*/
            /*For following tokens,though Bit_4 is ZERO they are not inst. codes
              1. Idle cycle(0110 nn)
              2. Resume    (1010E000)
              3. Event     (1010EPT1T0)
              4. Data for deferred Ld-Cached(1110Entry)*/
                if(tyTraceToken.Bits.Bit7_5 == 0x3 ||
                   tyTraceToken.Bits.Bit7_5 == 0x5 ||
                   tyTraceToken.Bits.Bit7_5 == 0x7 )
                    {
                        /*Do nothing.Present token is not delay slot instr.*/
                        /*TODO  :Check whether gbBranchPending[0]needs to be reset for 
                                 Resume(1010E000)and Event(1010EPT1T0)*/
                    }
                else
                    {
                    /*This token is delay slot inst.,hence reset the flags*/
                    gbBranchPending[0]    = false;
                    /*gbProcessDelaySlot =1 means, we have identified the delay-slot instr.
                      and will be processed now.
                      PC[T0] shal be updated with BA after delay slot inst is decoded*/
                    gbProcessDelaySlot[0] = true;
                    }
            }
        else
            {
            /*Do nothing.Present token is not T0 delay slot instr.*/
            }

        }
    /*If delay slot is pending for Thread 1*/
    if(gbBranchPending[1])
        {
        if (1 == tyTraceToken.Bits.Bit_4)
            {
            /*Not yet guaranteed that this is an instr. token of Thread 1*/
            /*For following tokens,though Bit_4 is ONE they are not inst. codes
              1. Idle cycle(0111 nn)
              2. Cache miss(1011 T1T0T1T0)
              3. data for deferred Ld-Uncached(1111Entry)*/
                if(tyTraceToken.Bits.Bit7_5 == 0x3 ||
                   tyTraceToken.Bits.Bit7_5 == 0x5 ||
                   tyTraceToken.Bits.Bit7_5 == 0x7 )
                    {
                        /*Do nothing.Present token is not delay slot instr.*/

                        /*TODO  :Check whether gbBranchPending[0]needs to be reset for 
                                 Resume(1010E000)and Event(1010EPT1T0)*/
                    }
                else
                    {
                    /*This token is delay slot inst.,hence reset the flags*/
                    gbBranchPending[1]    = false;
                    /*gbProcessDelaySlot =1 means, we have identified the delay-slot instr.
                      and will be processed now.
                      PC[T1] shal be updated with BA after delay slot inst is decoded*/
                    gbProcessDelaySlot[1] = true;
                    }
            }
        else
            {
                /*Do nothing.Present token is not T1 delay slot instr.*/
            }
        }
}
/******************************************************************************
*   Function: UpdatePCWithBranchAddr
*   Engineer: Rejeesh S Babu
*	Date           Initials    Description
*	11-Jan-2011     RSB         Initial
******************************************************************************/
/**
   \brief This function updates PC[0/1] with the branch address.
          This function is called only when a delay-slot instr. is executed

   \details Functionalities implemented are:
               - update PC[0/1] with the branch address.
               - Set and reset the branch flags appropriately.
*/
void UpdatePCWithBranchAddr(void)
{
    /*Update Branch Address for T0*/
    if(gbProcessDelaySlot[0])
        {
        /*If branch address is not recorded in raw trace*/
        if(gbBranchAddrNotRec[0])
            {
                gulPC[0] = gulBranchPC[0];
                /*Reset the flag*/
                gbBranchAddrNotRec[0] = false;
            }
        else
            {
                gulPC[0] = *gpulDataWordPtr++;
            }
        /*Reset the flag*/
        gbProcessDelaySlot[0] = false;
        }

    /*Update Branch Address for T1*/
    if(gbProcessDelaySlot[1])
        {
        /*If branch address is not recorded in raw trace*/
        if(gbBranchAddrNotRec[1])
            {
                gulPC[1] = gulBranchPC[1];
                /*Reset the flag*/
                gbBranchAddrNotRec[1] = false;
            }
        else
            {
                gulPC[1] = *gpulDataWordPtr++;
            }
        /*Reset the flag*/
        gbProcessDelaySlot[1] = false;
        }
}
