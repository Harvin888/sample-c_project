/************************** trace.h ****************************************/
/**  \file     \brief Header file for Zephyr Trace configuration register
*//*-----------------------------------------------------------------------------
*Version	Author	Date		Description
*------------------------------------------------------------------------------
* 0.1       RSB     25-Aug-2011 Initial
*----------------------------------------------------------------------------*/

#ifndef __TRACE_H_
#define __TRACE_H_
// NULL pointer
#ifndef NULL
    #define NULL 0
#endif

#define ZEPHYR_TRACE_ERROR                24/*It is a GDBServer requirement that this number has to be 24*/

int decoder_main(unsigned int* pulData,unsigned int ulTotalSize);

/*Bit definition of TraceControl Register*/
typedef union 
    {
    unsigned int ulTraceControl;
    struct
        {
        unsigned int ASID0   : 8;//LSB First
        unsigned int ASID1   : 8;
        unsigned int THREAD  : 4;
        unsigned int MEMSIZE : 4;
        unsigned int NOTUSED : 4;
        unsigned int LM      : 1;
        unsigned int CA      : 1;
        unsigned int ACT     : 1;
        unsigned int ON      : 1;//MSB Last
        }Bits;
    }TyTraceControl;

/*Bit definition of TraceControl2 Register*/
typedef union 
    {
    unsigned int ulTraceControl2;
    struct
        {
        unsigned int NOTUSED1: 8;//LSB First
        unsigned int TRIGMASK: 8;
        unsigned int TRIGTYPE: 3;
        unsigned int FULL    : 1;
        unsigned int NOTUSED2: 7;
        unsigned int WE      : 1;
        unsigned int REG     : 4;//MSB Last
        }Bits;
    }TyTraceControl2;

/*Bit definition of TraceControl3 Register*/
typedef union 
    {
    unsigned int ulTraceControl3;
    struct
        {
        unsigned int T0_LA       : 1;//LSB First
        unsigned int T0_SA       : 1;
        unsigned int T0_LD       : 1;
        unsigned int T0_SD       : 1;
        unsigned int T0_NOTUSED1 : 2;
        unsigned int T0_TCR      : 1;
        unsigned int T0_ATB      : 1;
        unsigned int T0_U        : 1;
        unsigned int T0_S        : 1;
        unsigned int T0_K        : 1;
        unsigned int T0_Ex       : 1;
        unsigned int T0_Er       : 1;
        unsigned int T0_D        : 1;
        unsigned int T0_CT       : 1;
        unsigned int T0_NOTUSED2 : 1;
        unsigned int T1_LA       : 1;
        unsigned int T1_SA       : 1;
        unsigned int T1_LD       : 1;
        unsigned int T1_SD       : 1;
        unsigned int T1_NOTUSED1 : 2;
        unsigned int T1_TCR      : 1;
        unsigned int T1_ATB      : 1;
        unsigned int T1_U        : 1;
        unsigned int T1_S        : 1;
        unsigned int T1_K        : 1;
        unsigned int T1_Ex       : 1;
        unsigned int T1_Er       : 1;
        unsigned int T1_D        : 1;
        unsigned int T1_CT       : 1;
        unsigned int T1_NOTUSED2 : 1;//MSB Last
        }Bits;
    }TyTraceControl3;

/*Strutcure containing all Trace control registers*/
typedef struct 
    {
    TyTraceControl  tyTraceControl  ;
    TyTraceControl2 tyTraceControl2 ;
    TyTraceControl3 tyTraceControl3 ;
    }TyTraceControlRegs;

#endif
