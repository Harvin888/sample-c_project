/****************************************************************************
       Module: ml_reg.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI midlayer.
               This section contains all functions related to MIPS 
               register access.
Date           Initials    Description
08-Feb-2008    VH          Initial
23-Dec-2008    SPC         Returning errors more properly.
06-Aug-2009    SPC         Made cache flush routine run from probe
26-NOV-2011    SV          Modified MLR_WriteCP0Register()'s linkage from static to global 
****************************************************************************/

#define MDI_LIB               // We are an MDI Library not a debugger

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "firmware/export/api_def.h"
#include "commdefs.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "mdi.h"
#include "mips.h"
#include "midlayer.h"
#include "gerror.h"
#include "ml_error.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdmips.h"
#include "../../utility/debug/debug.h"

#ifndef __LINUX
// Windows specific includes
#else
// Linux includes
#endif

// local constants

// local type definition

// local function prototypes
//static int MLR_WriteCP0Register(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long ulData, unsigned long ulSel);
static int MLR_ReadACXRegister(TyMLHandle *ptyHandle, unsigned long *pulData);
static int MLR_WriteACXRegister(TyMLHandle *ptyHandle, unsigned long ulData);

//int MLR_ReadCP0Register(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long *pulData, unsigned long ulSel);

/****************************************************************************
     Function: MLR_ReadRegister
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulRegNumber - register number
               unsigned long ulRegisterType - register type
               unsigned long *pulData - pointer to value
       Output: int - error code
  Description: read single MIPS register (any type)
Date           Initials    Description
08-Feb-2008    VH          Initial
23-Jan-2008    SPC         Commented out the reading of EJTAGDATA and BYPASS
26-NOv-2011	   SV		   Modified MLR_WriteCP0Register()'s linkage from static to global 
****************************************************************************/
int MLR_ReadRegister(TyMLHandle *ptyHandle, 
					 unsigned long ulRegNumber, 
					 unsigned long ulRegisterType, 
					 unsigned long *pulData)
{
   int iResult;
   unsigned long ulIRValue, ulDRValue, ulTCBControlB;
   // read register
   assert(ptyHandle != NULL);
   if (pulData == NULL)
      return ERR_NO_ERROR;
   *pulData = 0;
   // check if dealing with special register type
   if (ulRegisterType == ASHLING_INT_REG)
      {  // special register type
#ifdef USE_DIAGNOSTIC_REGISTERS
      if ((ulRegNumber >= REG_MIPS_DW_DIAG0) && (ulRegNumber <= REG_MIPS_DW_DIAGF))
         {
         if ((ulRegNumber - REG_MIPS_DW_DIAG0) < MAX_DIAGNOSTIC_REGISTERS)
            *pulData = ptyHandle->pulDiagnosticRegisters[ulRegNumber - REG_MIPS_DW_DIAG0];
         else
            *pulData = 0x0;
         return ERR_NO_ERROR;
         }
#endif
      if (ulRegNumber == REG_MIPS_IDCODE)
         {  // reading EJTAG IDCODE
         ulIRValue = ptyHandle->ulMipsCoreEjtagIdcodeInst;
         iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_IDCODE_LENGTH, NULL, pulData);
         return iResult;
         }
      if (ulRegNumber == REG_MIPS_IMPCODE)
         {  // reading EJTAG IMPCODE
         ulIRValue = MIPS_EJTAG_IMPCODE_INST;
         iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_IMPCODE_LENGTH, NULL, pulData);
         return iResult;
         }
      if (ulRegNumber == REG_MIPS_CONTROL)
         {
         ulIRValue = MIPS_EJTAG_CONTROL_INST;
         ulDRValue = BIT_15 | BIT_18;                 // ProbEn and PrAcc as 1
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_CONTROL_LENGTH, &ulDRValue, pulData);
         }
      if (ulRegNumber == REG_MIPS_EJTAGDATA)
         {  // this does not make really sense but let do read if user wants it
         //ulIRValue = MIPS_EJTAG_DATA_INST;
         //return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, ptyHandle->ulMipsCoreEjtagDataLength, NULL, pulData);
         *pulData = 0;
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_EJTAGADDR)
         {
         ulIRValue = MIPS_EJTAG_ADDRESS_INST;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, ptyHandle->ulMipsCoreEjtagAddressLength, NULL, pulData);
         }
      if (ulRegNumber == REG_MIPS_BYPASS)
         {  // this does not make sense but let user do it
         //ulIRValue = MIPS_EJTAG_BYPASS_INST;
         //return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_BYPASS_LENGTH, NULL, pulData);
         *pulData = 0;
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_MIPS16E_ENABLED)
         {
         *pulData = (unsigned long)ptyHandle->bBrokeInMIPS16Mode;
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_WRITE_ENABLE_REG)
         {
         *pulData = ptyHandle->ulAppWriteData;
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_DCR)
         {
         if (ptyHandle->bProcessorExecuting)
            return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
         return MLM_ReadWordsDW2(ptyHandle, MIPS_DCR_ADDRESS, 1, pulData, 0);       // endian independent
         }
      if (ulRegNumber == REG_MIPS_TCBCONTROLA)
         {         
         ulIRValue = MIPS_EJTAG_TCBCONTROLA_INST;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLA_LENGTH, NULL, pulData);
         }
      if (ulRegNumber == REG_MIPS_TCBCONTROLB)
         {         
         ulIRValue = MIPS_EJTAG_TCBCONTROLB_INST;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLB_LENGTH, NULL, pulData);
         }
      if (ulRegNumber == REG_MIPS_TCBDATA)
         {         
         ulIRValue = MIPS_EJTAG_TCBDATA_INST;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBDATA_LENGTH, NULL, pulData);
         }
      if (ptyHandle->ucEjtagVersion >= EJTAG_VERSION_25)
         {  // DSU compatible with EJTAG 2.5 and newer
         if (ulRegNumber == REG_MIPS_IBS)
            {
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            return MLM_ReadMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ25_IBS, (unsigned char *)pulData, 4);  // get IBS from DSU
            }
         if (ulRegNumber == REG_MIPS_DBS)
            {
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            return MLM_ReadMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ25_DBS, (unsigned char *)pulData, 4);  // get DBS from DSU
            }
         }
      else
         {  // DSU compatible with EJTAG 2.0 and older
         if (ulRegNumber == REG_MIPS_IBS)
            {
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            return MLM_ReadMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ20_IBS, (unsigned char *)pulData, 4);  // get IBS from DSU
            }
         if (ulRegNumber == REG_MIPS_DBS)
            {
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            return MLM_ReadMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ20_DBS, (unsigned char *)pulData, 4);  // get DBS from DSU
            }
         }
      // now we assume it is one of special EJTAG registers
      ulIRValue = MIPS_EJTAG_TCBCONTROLB_INST;     ulDRValue = 0;       ulTCBControlB = 0;
      iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLB_LENGTH, &ulDRValue, &ulTCBControlB);
      if (iResult != ERR_NO_ERROR)
         {
         *pulData = 0;     return iResult;
         }
      ulTCBControlB &= ~BIT_20;           // WR=0 (do not write register)
      ulTCBControlB &= 0xFC1FFFFF;        // clear REG bitfield
      switch (ulRegNumber)
         {
         case REG_MIPS_TCBCONFIG:
            ulTCBControlB |= (0 << 21);
            break;
         case REG_MIPS_TCBTW:
            ulTCBControlB |= (4 << 21);
            break;
         case REG_MIPS_TCBRDP:
            ulTCBControlB |= (5 << 21);
            break;
         case REG_MIPS_TCBWRP:
            ulTCBControlB |= (6 << 21);
            break;
         case REG_MIPS_TCBSTP:
            ulTCBControlB |= (7 << 21);
            break;
         case REG_MIPS_TCBTRIG0:
            ulTCBControlB |= (16 << 21);
            break;
         case REG_MIPS_TCBTRIG1:
            ulTCBControlB |= (17 << 21);
            break;
         case REG_MIPS_TCBTRIG2:
            ulTCBControlB |= (18 << 21);
            break;
         case REG_MIPS_TCBTRIG3:
            ulTCBControlB |= (19 << 21);
            break;
         case REG_MIPS_TCBTRIG4:
            ulTCBControlB |= (20 << 21);
            break;
         case REG_MIPS_TCBTRIG5:
            ulTCBControlB |= (21 << 21);
            break;
         case REG_MIPS_TCBTRIG6:
            ulTCBControlB |= (22 << 21);
            break;
         case REG_MIPS_TCBTRIG7:
            ulTCBControlB |= (23 << 21);
            break;
         default:
            iResult = ERR_READREG_INVALID;
            break;
         }
      //  read special case of EJTAG registers
      ulTCBControlB |= BIT_31;      // WE = 1
      ulIRValue = MIPS_EJTAG_TCBCONTROLB_INST;
      iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLB_LENGTH, &ulTCBControlB, NULL);
      if (iResult != ERR_NO_ERROR)
         {
         *pulData = 0;     return iResult;
         }
      ulIRValue = MIPS_EJTAG_TCBDATA_INST;
      return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBDATA_LENGTH, NULL, pulData);
      }
   else
      {  // MIPS register
      switch (ulRegisterType)
         {
         case MDIMIPCPU:      // general MIPS registers
            {
            if (ulRegNumber == REG_MIPS_ZERO)
               {  // zero register is always 0
               *pulData = 0;        
               return ERR_NO_ERROR;
               }
            if ((ulRegNumber >= 1) && (ulRegNumber < 32))
               {  // general purpose MIPS register, we can get value from saved context
               *pulData = ptyHandle->pulProcessorContext[ulRegNumber - 1];    // context starts with register $1, register $0 (zero) is not in context
               return ERR_NO_ERROR;
               }
            return ERR_READREG_INVALID; 
            }
         case MDIMIPPC:
            {  // PC is stored also in context
            *pulData = ptyHandle->pulProcessorContext[31];                    // PC as at offset 31
            return ERR_NO_ERROR;
            }
         case MDIMIPHILO:
            {     // special CP0 registers
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            if (ulRegNumber < 2)
               {  // special case for HI and LO multiply registers
               if (ulRegNumber == 0)
                   return MLR_ReadCP0Register(ptyHandle, REG_MIPS_HI, pulData, 0x100); // 0x100 is used inside to signal special CP0
               else
                   return MLR_ReadCP0Register(ptyHandle, REG_MIPS_LO, pulData, 0x100); // 0x100 is used inside to signal special CP0
               }
            return ERR_READREG_INVALID; 
            }
         case MDIMIPCP0:
            {     // MIPS CP0 registers - some of them are part of context
            // recalculate select and register number (each bank has 32 registers = select), register number from 0 to 32
            unsigned long ulSel = ulRegNumber / 32;
            ulRegNumber = ulRegNumber % 32;
            if ((ulRegNumber == ptyHandle->ulCP0RegNumberforDebug) && (ulSel == 0))
               {  // take debug register from context as offset 32
               *pulData = ptyHandle->pulProcessorContext[32];
               return ERR_NO_ERROR;
               }
            if ((ulRegNumber == ptyHandle->ulCP0RegNumberforConfig) && (ulSel == 0))
               {  // take config register from context as offset 33
               *pulData = ptyHandle->pulProcessorContext[33];
               return ERR_NO_ERROR;
               }
            if (ptyHandle->bBackupTLBAndCacheRegisters)
               {  // handle some register that are also cached in context
               if (ulSel == 0)
                  {  
                  switch (ulRegNumber)
                     {
                     case 2:     // EntryLo0 at offset 34
                        *pulData = ptyHandle->pulProcessorContext[34];
                        return ERR_NO_ERROR;
                     case 3:     // EntryLo1 at offset 35
                        *pulData = ptyHandle->pulProcessorContext[35];
                        return ERR_NO_ERROR;
                     case 10:    // EntryHi at offset 36
                        *pulData = ptyHandle->pulProcessorContext[36];
                        return ERR_NO_ERROR;
                     case 5:     // PageMask at offset 37
                        *pulData = ptyHandle->pulProcessorContext[37];
                        return ERR_NO_ERROR;
                     case 0:     // Index at offset 38
                        *pulData = ptyHandle->pulProcessorContext[38];
                        return ERR_NO_ERROR;
                     case 28:    // TagLo at offset 39
                        *pulData = ptyHandle->pulProcessorContext[39];
                        return ERR_NO_ERROR;
                     case 29: // I_TagHi at offset 43 (BRCM)
                        *pulData = ptyHandle->pulProcessorContext[43];
                        return ERR_NO_ERROR;
                     default:    // register is not cached, so continue
                        break;
                     }
                  }
               if (ulSel == 1)
                  {
                  if (ulRegNumber == 28)
                     {  // DataLo at offset 40
                     *pulData = ptyHandle->pulProcessorContext[40];   
                     return ERR_NO_ERROR;
                     }
                  if (ulRegNumber == 29)
                     {  // I_DataHi at offset 44 (BRCM)
                     *pulData = ptyHandle->pulProcessorContext[44];   
                     return ERR_NO_ERROR;
                     }
                  if (ulRegNumber == 23)
                     {  // TraceControl at offset 47
                     *pulData = ptyHandle->pulProcessorContext[47];   
                     return ERR_NO_ERROR;
                     }
                  }
               if (ulSel == 2)
                  {
                  if (ulRegNumber == 28)
                     {  // D_TagLo at offset 41 (BRCM)
                     *pulData = ptyHandle->pulProcessorContext[41];   
                     return ERR_NO_ERROR;
                     }
                  if (ulRegNumber == 29)
                     {  // D_TagHi at offset 45 (BRCM)
                     *pulData = ptyHandle->pulProcessorContext[45];   
                     return ERR_NO_ERROR;
                     }
                  }
               if (ulSel == 3)
                  {
                  if (ulRegNumber == 28)
                     {  // DataLo at offset 42 (BRCM)
                     *pulData = ptyHandle->pulProcessorContext[42];   
                     return ERR_NO_ERROR;
                     }
                  if (ulRegNumber == 29)
                     {  // D_DataHi at offset 46 (BRCM)
                     *pulData = ptyHandle->pulProcessorContext[46];   
                     return ERR_NO_ERROR;
                     }
                  }
               }
            // we could not find CP0 register cached, so we must read it now
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGREAD_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            return MLR_ReadCP0Register(ptyHandle, ulRegNumber, pulData, ulSel);        // we have to read CP0 from MIPS
            }
         case MDIMIP192ACC:   
            {     // reading ACX register on SmartMIPS, need to use special function
            return MLR_ReadACXRegister(ptyHandle, pulData);
            }
         case MDIMIPSRS:
            {     // reading shadow register for Microchip
            if ((ulRegNumber == REG_MIPS_SRS1_ZERO) || (ulRegNumber == REG_MIPS_SRS2_ZERO) || (ulRegNumber == REG_MIPS_SRS3_ZERO))
               {
               *pulData = 0;                                                           // zero registers
               return ERR_NO_ERROR;    
               }
            if ((ulRegNumber > REG_MIPS_SRS1_ZERO) && (ulRegNumber <= REG_MIPS_SRS1_RA))
               {
               *pulData = (ptyHandle->pulShadowRegisterSet[0])[(ulRegNumber - REG_MIPS_SRS1_ZERO)-1];
               return ERR_NO_ERROR;    
               }
            if ((ulRegNumber > REG_MIPS_SRS2_ZERO) && (ulRegNumber <= REG_MIPS_SRS2_RA))
               {
               *pulData = (ptyHandle->pulShadowRegisterSet[0])[(ulRegNumber - REG_MIPS_SRS2_ZERO)-1];
               return ERR_NO_ERROR;    
               }
            if ((ulRegNumber > REG_MIPS_SRS3_ZERO) && (ulRegNumber <= REG_MIPS_SRS3_RA))
               {
               *pulData = (ptyHandle->pulShadowRegisterSet[0])[(ulRegNumber - REG_MIPS_SRS3_ZERO)-1];
               return ERR_NO_ERROR;    
               }
            return ERR_READREG_INVALID; 
            }
         default:
            // not supported yet
            return ERR_READREG_INVALID; 
         }
      }
   // should not get here
}

/****************************************************************************
     Function: MLR_WriteRegister
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulRegNumber - register number
               unsigned long ulRegisterType - register type
               unsigned long ulData - value to write
       Output: int - error code
  Description: write single MIPS register (any type)
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLR_WriteRegister(TyMLHandle *ptyHandle, 
					  unsigned long ulRegNumber, 
					  unsigned long ulRegisterType, 
					  unsigned long ulData)
{
   unsigned long ulIRValue, ulDRValue;
   int iResult;
   // check parameters
   assert(ptyHandle != NULL);
   // check register type
   if (ulRegisterType == ASHLING_INT_REG)
      {
#ifdef USE_DIAGNOSTIC_REGISTERS
      if ((ulRegNumber >= REG_MIPS_DW_DIAG0) && (ulRegNumber<=REG_MIPS_DW_DIAGF))
         {
         if ((ulRegNumber - REG_MIPS_DW_DIAG0) < MAX_DIAGNOSTIC_REGISTERS)
            ptyHandle->pulDiagnosticRegisters[ulRegNumber - REG_MIPS_DW_DIAG0] = ulData;
         return ERR_NO_ERROR;
         }
#endif
      if (ulRegNumber == REG_MIPS_CACHE_FLUSH_RTN_ADDR)
         {
         //ptyHandle->bCacheFlushEnabled = TRUE;
         //return ML_InstallCacheFlushApp(ptyHandle, ulData);
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_INVALIDATE_CACHE_NOW)
         {  // we should invalidate cache now
         return  MLC_InvalidateCacheOnReset(ptyHandle);
         }
      if (ulRegNumber == REG_MIPS_SW_BP_WRITEBACK)
         {  // write back or restore data displaced by SW breakpoints
         return MLB_WriteBackSoftwareBreakpoints(ptyHandle, (unsigned char)ulData);
         }
      if (ulRegNumber == REG_MIPS_WRITE_ENABLE_REFRESH_REG)
         {  // enable memory writing in case it has got cleared at any stage (i.e. external reset during debug mode)
         ML_EnableMemoryWriting(ptyHandle, 0, 0, 0);
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_EXECUTION_CTRL)
         {
         if (ptyHandle->bEJTAGBootSupported)
            {
            if (ulData)
               return ML_SetBootType(ptyHandle, 1);
            else
               return ML_SetBootType(ptyHandle, 0);
            }
         return ERR_WRITEREG_INVALID;                    // should not have called this register if not supported
         }
      if (ulRegNumber == REG_MIPS_IDCODE)
         {  // trying to write IDCODE (it is read-only), whatever
         ulIRValue = ptyHandle->ulMipsCoreEjtagIdcodeInst;
         ulDRValue = ulData;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_IDCODE_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber==REG_MIPS_IMPCODE)
         {  // trying to write IMPCODE (it is read-only), whatever
         ulIRValue = MIPS_EJTAG_IMPCODE_INST;
         ulDRValue = ulData;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_IMPCODE_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber == REG_MIPS_TCBCONTROLA)
         {
         ulIRValue = MIPS_EJTAG_TCBCONTROLA_INST;
         ulDRValue = ulData;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLA_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber == REG_MIPS_TCBCONTROLB)
         {
         ulIRValue = MIPS_EJTAG_TCBCONTROLB_INST;
         ulDRValue = ulData | BIT_31;                             // WE must be 1 to do write properly                          
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLB_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber == REG_MIPS_TCBDATA)
         {
         ulIRValue = MIPS_EJTAG_TCBDATA_INST;
         ulDRValue = ulData;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBDATA_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber == REG_MIPS_CONTROL)
         {
         ulIRValue = MIPS_EJTAG_CONTROL_INST;
         ulDRValue = ulData;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_CONTROL_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber == REG_MIPS_EJTAGDATA)
         {
         unsigned long pulLongData[2];
         ulIRValue = MIPS_EJTAG_DATA_INST;
         pulLongData[0] = ulData;      pulLongData[1] = 0;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, ptyHandle->ulMipsCoreEjtagDataLength, pulLongData, NULL);
         }
      if (ulRegNumber == REG_MIPS_EJTAGADDR)
         {
         unsigned long pulLongData[2];
         ulIRValue = MIPS_EJTAG_ADDRESS_INST;
         pulLongData[0] = ulData;      pulLongData[1] = 0;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, ptyHandle->ulMipsCoreEjtagAddressLength, pulLongData, NULL);
         }
      if (ulRegNumber == REG_MIPS_BYPASS)
         {
         ulIRValue = MIPS_EJTAG_BYPASS_INST;
         ulDRValue = ulData;
         return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_BYPASS_LENGTH, &ulDRValue, NULL);
         }
      if (ulRegNumber == REG_MIPS_MIPS16E_ENABLED)
         {
         ptyHandle->bBrokeInMIPS16Mode = (ulData > 0)? 1 : 0;     // 0 - MIPS32, 1 - MIPS16
         return ERR_NO_ERROR;
         }
      if (ulRegNumber==REG_MIPS_ENABLE_CODECOVERAGE)
         {
         if (ulData)                                              // #define SFR_EMU_CCM 0xA200E06C
            return ML_NotImplementedYet(ptyHandle);               // MLM_SetWord(SFR_EMU_CCM, 0x4);
         else
            return ML_NotImplementedYet(ptyHandle);               // MLM_SetWord(SFR_EMU_CCM, 0x0);
         }
      if (ulRegNumber==REG_MIPS_CLEAR_CODECOVERAGE)
         {
         return ML_NotImplementedYet(ptyHandle);                  // MLM_SetWord(SFR_EMU_CCM, 0x1);
         }
      if (ulRegNumber == REG_MIPS_WRITE_ENABLE_REG)
         {
         ptyHandle->ulAppWriteData = ulData;
         return ERR_NO_ERROR;
         }
      if (ulRegNumber == REG_MIPS_DCR)
         {
         return MLM_WriteWordsDW2(ptyHandle, MIPS_DCR_ADDRESS, 1, &ulData, 0);
         }
      if (ptyHandle->ucEjtagVersion >= EJTAG_VERSION_25)
         {  // EJTAG version 2.5 and newer
         switch (ulRegNumber)
            {
            case REG_MIPS_IBS:
               return MLM_WriteMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ25_IBS, (unsigned char *)&ulData, 4);
            case REG_MIPS_DBS:
               return MLM_WriteMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ25_DBS, (unsigned char *)&ulData, 4);
            default:
                break;
            }
         }
      else
         {  // EJTAG version 2.0 and older
         switch (ulRegNumber)
            {
            case REG_MIPS_IBS:
               return MLM_WriteMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ20_IBS, (unsigned char *)&ulData, 4);
            case REG_MIPS_DBS:
               return MLM_WriteMemoryEjtag(ptyHandle, DRSEG_OFFSET_EJ20_DBS, (unsigned char *)&ulData, 4);
            default:
                break;
            }
         }
      // ok, we assume it is one of special EJTAG registers
      ulIRValue = MIPS_EJTAG_TCBCONTROLB_INST;
      ulDRValue = 0;
      iResult = ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLB_LENGTH, NULL, &ulDRValue);
      if (iResult != ERR_NO_ERROR)
         return iResult;
      ulDRValue |= BIT_20;          // do write to register
      ulDRValue &= 0xFC1FFFFF;      // clear REG bitfield
      switch (ulRegNumber)
         {
         case REG_MIPS_TCBCONFIG:
            ulDRValue |= (0 << 21);
            break;
         case REG_MIPS_TCBTW:
            ulDRValue |= (4 << 21);
            break;
         case REG_MIPS_TCBRDP:
            ulDRValue |= (5 << 21);
            break;
         case REG_MIPS_TCBWRP:
            ulDRValue |= (6 << 21);
            break;
         case REG_MIPS_TCBSTP:
            ulDRValue |= (7 << 21);
            break;
         case REG_MIPS_TCBTRIG0:
            ulDRValue |= (16 << 21);
            break;
         case REG_MIPS_TCBTRIG1:
            ulDRValue |= (17 << 21);
            break;
         case REG_MIPS_TCBTRIG2:
            ulDRValue |= (18 << 21);
            break;
         case REG_MIPS_TCBTRIG3:
            ulDRValue |= (19 << 21);
            break;
         case REG_MIPS_TCBTRIG4:
            ulDRValue |= (20 << 21);
            break;
         case REG_MIPS_TCBTRIG5:
            ulDRValue |= (21 << 21);
            break;
         case REG_MIPS_TCBTRIG6:
            ulDRValue |= (22 << 21);
            break;
         case REG_MIPS_TCBTRIG7:
            ulDRValue |= (23 << 21);
            break;
         default:
            return ERR_WRITEREG_INVALID;
         }
      ulIRValue = MIPS_EJTAG_TCBCONTROLB_INST;     ulDRValue |= BIT_31;
      (void) ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBCONTROLB_LENGTH, &ulDRValue, NULL);
      ulIRValue = MIPS_EJTAG_TCBDATA_INST;         ulDRValue = ulData;        
      return ML_ScanIRandDR(ptyHandle, ptyHandle->ulMipsCoreEjtagIRLength, &ulIRValue, MIPS_EJTAG_TCBDATA_LENGTH, &ulDRValue, NULL);
      }
   else
      {
      switch (ulRegisterType)
         {
         case MDIMIPCPU:
            {
            if (ulRegNumber == REG_MIPS_ZERO)
               return ERR_NO_ERROR;                                                 // does not make sense to write zero register, no effect
            if ((ulRegNumber >= 1)&& (ulRegNumber < 32))
               {  // general purpose MIPS register, we will rewrite value in context, registers are indexed from $1, $0 is not in context
               ptyHandle->pulProcessorContext[ulRegNumber - 1] = ulData;
               return ERR_NO_ERROR;
               }
            return ERR_WRITEREG_INVALID;                                      // invalid number
            }
         case MDIMIPPC:
            {  // PC is also in context at offset 31
            ptyHandle->pulProcessorContext[31] = ulData; 
            return ERR_NO_ERROR;
            }
         case MDIMIPHILO:
            {
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGWRITE_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            if (ulRegNumber < 2)
               {  // special case for Hi and Lo multiply registers
               if (ulRegNumber == 0)
                  return MLR_WriteCP0Register(ptyHandle, REG_MIPS_HI, ulData, 0x100);  // 0x100 is special number
               else
                  return MLR_WriteCP0Register(ptyHandle, REG_MIPS_LO, ulData, 0x100);  // 0x100 is special number
               }
            return ERR_WRITEREG_INVALID;                                      // invalid number
            }
         case MDIMIPCP0:
            {  // MIPS CP0 register
            unsigned long ulSel = ulRegNumber / 32;                                    // each bank contains 32 registers
            ulRegNumber = ulRegNumber % 32;                                            // register within bank
            if (ptyHandle->bProcessorExecuting)
               return ERR_DIREGWRITE_NOT_DURING_EXE;                              // cannot do operation since not in debug mode
            if ((ulRegNumber == ptyHandle->ulCP0RegNumberforDebug) && (ulSel == 0))
               {  // debug register is stored in context at offset 32
               ptyHandle->pulProcessorContext[32] = ulData;                            // write context value
               iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);  // write to CP0
               if (iResult != ERR_NO_ERROR)
                  return iResult;
               // read back register from CP0 to have correct reserved bits   
               return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[32]), ulSel); 
               }
            if ((ulRegNumber == ptyHandle->ulCP0RegNumberforConfig) && (ulSel == 0))
               {  // config register is stored in context at offset 33
               ptyHandle->pulProcessorContext[33] = ulData;
               iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);  // write to CP0
               if (iResult != ERR_NO_ERROR)
                  return iResult;
               // read back register from CP0 to have correct reserved bits   
               iResult = MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[33]), ulSel); 
               if (iResult != ERR_NO_ERROR)
                  return iResult;
               if ((ptyHandle->pulProcessorContext[33] >> (ptyHandle->ulCP0BitOffsetInConfigforEndianMode)) & 0x00000001)
                  ptyHandle->bBigEndian = 1;                                           // target is BE
               else
                  ptyHandle->bBigEndian = 0;                                           // target is LE
               ulData = ulData | ptyHandle->ulConfigORMask;                            // set bits we need to set
               ulData = ulData & ptyHandle->ulConfigORMask;                            // clear bits we need to clear
               return MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);     // do final write
               }
            if (ptyHandle->bBackupTLBAndCacheRegisters)
               {  // handle registers which are also cached in context
               if (ulSel == 0)
                  {
                  switch (ulRegNumber)
                     {
                     case 2:  // EntryLo0 at offset 34, write value and do readback
                        {
                        ptyHandle->pulProcessorContext[34] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[34]), ulSel);
                        }
                     case 3:  // EntryLo1 at offset 35, write value and do readback
                        {
                        ptyHandle->pulProcessorContext[35] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[35]), ulSel);
                        }
                     case 10: // EntryHi at offset 36
                        {
                        ptyHandle->pulProcessorContext[36] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[36]), ulSel);
                        }
                     case 5:  // PageMask at offset 37
                        {
                        ptyHandle->pulProcessorContext[37] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[37]), ulSel);
                        }
                     case 0:  // Index at offset 38
                        {
                        ptyHandle->pulProcessorContext[38] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[38]), ulSel);
                        }
                     case 28: // TagLo
                        {
                        ptyHandle->pulProcessorContext[39] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[39]), ulSel); 
                        }
                     case 29: // I_TagHi at offset 43 (BRCM)
                        {
                        ptyHandle->pulProcessorContext[43] = ulData;
                        iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                        if (iResult != ERR_NO_ERROR)
                           return iResult;
                        return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[43]), ulSel); 
                        }
                     default: // no cache register, just continue
                        break;
                     }
                  }
               if (ulSel == 1)
                  {
                  if (ulRegNumber == 28)
                     {  // DataLo at offset 40
                     ptyHandle->pulProcessorContext[40] = ulData;
                     iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                     if (iResult != ERR_NO_ERROR)
                        return iResult;
                     return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[40]), ulSel); 
                     }
                  if (ulRegNumber == 29)
                     {  // I_DataHi at offset 44 (BRCM)
                     ptyHandle->pulProcessorContext[44] = ulData;
                     iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                     if (iResult != ERR_NO_ERROR)
                        return iResult;
                     return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[44]), ulSel); 
                     }
                  if (ulRegNumber == 23)
                     {  // TraceControl at offset 47
                     ptyHandle->pulProcessorContext[47] = ulData;
                     // We are not writing TraceControl register to the target right now.  
                     // We will write it when restoring the context.
                     return ERR_NO_ERROR;
                     }
                  }
               if (ulSel == 2)
                  {
                  if (ulRegNumber == 28)
                     {  // D_TagLo at offset 41 (BRCM)
                     ptyHandle->pulProcessorContext[41] = ulData;
                     iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                     if (iResult != ERR_NO_ERROR)
                        return iResult;
                     return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[41]), ulSel); 
                     }
                  if (ulRegNumber == 29)
                     {  // D_TagHi at offset 45 (BRCM)
                     ptyHandle->pulProcessorContext[45] = ulData;
                     iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                     if (iResult != ERR_NO_ERROR)
                        return iResult;
                     return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[45]), ulSel); 
                     }
                  }
               if (ulSel == 3)
                  {
                  if (ulRegNumber == 28)
                     {  // DataLo at offset 42 (BRCM)
                     ptyHandle->pulProcessorContext[42] = ulData;
                     iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                     if (iResult != ERR_NO_ERROR)
                        return iResult;
                     return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[42]), ulSel); 
                     }
                  if (ulRegNumber == 29)
                     {  // D_DataHi at offset 46 (BRCM)
                     ptyHandle->pulProcessorContext[46] = ulData;
                     iResult = MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
                     if (iResult != ERR_NO_ERROR)
                        return iResult;
                     return MLR_ReadCP0Register(ptyHandle, ulRegNumber, &(ptyHandle->pulProcessorContext[46]), ulSel); 
                     }
                  }
               }
            if ((ptyHandle->ucEjtagVersion >= EJTAG_VERSION_25) && (ulRegNumber == (REG_MIPS_C0_R12 - REG_START_OF_MIPS_CP)) && (ulSel == 0))
               {  // we do not allow user to change status register, bit RP (reduced power) as it is serious issue in debug mode
               // user should do this action in his own code running in normal mode
               ulData &= ~BIT_27;
               }
            if (ulRegNumber < 32)
               {  // CP0 MIPS register but not in context
               return MLR_WriteCP0Register(ptyHandle, ulRegNumber, ulData, ulSel);
               }
            return ERR_WRITEREG_INVALID;                             // invalid CP0 register
            }
         case MDIMIP192ACC :
            {
            return MLR_WriteACXRegister(ptyHandle, ulData);
            }
         case MDIMIPSRS:
            {  // MIPS shadow registers
            if ((ulRegNumber == REG_MIPS_SRS1_ZERO) || (ulRegNumber == REG_MIPS_SRS2_ZERO) || (ulRegNumber == REG_MIPS_SRS3_ZERO))
               return ERR_NO_ERROR;                            // write into zero register is ignored
            if ((ulRegNumber > REG_MIPS_SRS1_ZERO) && (ulRegNumber <= REG_MIPS_SRS1_RA))
               {
               (ptyHandle->pulShadowRegisterSet[0])[(ulRegNumber - REG_MIPS_SRS1_ZERO)-1] = ulData;
               return ERR_NO_ERROR;    
               }
            if ((ulRegNumber > REG_MIPS_SRS2_ZERO) && (ulRegNumber <= REG_MIPS_SRS2_RA))
               {
               (ptyHandle->pulShadowRegisterSet[1])[(ulRegNumber - REG_MIPS_SRS2_ZERO)-1] = ulData;
               return ERR_NO_ERROR;    
               }
            if ((ulRegNumber > REG_MIPS_SRS3_ZERO) && (ulRegNumber <= REG_MIPS_SRS3_RA))
               {
               (ptyHandle->pulShadowRegisterSet[2])[(ulRegNumber - REG_MIPS_SRS3_ZERO)-1] = ulData;
               return ERR_NO_ERROR;    
               }
            return ERR_WRITEREG_INVALID;
            }
         default:
            // not supported yet
            return ERR_WRITEREG_INVALID;
         }
      }
   // should not get here
}

/****************************************************************************
     Function: MLR_ReadCP0Register
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulRegNumber - register number
               unsigned long *pulData - pointer to data
               unsigned long ulSel - register select
       Output: int - error code
  Description: read MIPS CP0 register using DW2 application
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLR_ReadCP0Register(TyMLHandle *ptyHandle, unsigned long ulRegNumber, unsigned long *pulData, unsigned long ulSel)
{
   unsigned long pulDataFromProc[1];
   unsigned long ulReadCP0Instruction = MIPS_INST_MFC0_T0_1;
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   // we need to replace instruction in DW2 application with proper one with register number and select
   if (ulSel >= 0x100)
      {  // very special case for high and low multiply registers
      if (ulRegNumber == REG_MIPS_LO)
         ulReadCP0Instruction = MIPS_INST_MFLO_T0;
      else if (ulRegNumber == REG_MIPS_HI)
         ulReadCP0Instruction = MIPS_INST_MFHI_T0;
      // other case should not happen so we can leave original instruction which is harmless
      }
   else
      {
      if (ulRegNumber <= 31)
         {
         ulReadCP0Instruction &= 0xFFFF07FF;            // clear bits 11 to 15
         ulReadCP0Instruction |= (ulRegNumber << 11);   // replace bits 11 to 15 with register number
         ulReadCP0Instruction |= (ulSel & 0x000000FF);  // use lowest 8 bits from register select
         }
      // other case should not happen so we can leave original instruction which is harmless
      }
   // replace instruction, prepare parameters, run DW2 and restore instruction
   ptyHandle->tyMipsDW2App.pulReadCP0App[ptyHandle->tyMipsDW2App.ulReadCP0InstOffset] = ulReadCP0Instruction;
   iResult = ML_ExecuteDW2(ptyHandle, 
                           (void *)(ptyHandle->tyMipsDW2App.pulReadCP0App), 
                           ptyHandle->tyMipsDW2App.ulReadCP0Instructions * sizeof(unsigned long),
                           NULL, 
                           0, 
                           (void *)pulDataFromProc, 
                           1*sizeof(unsigned long));
   ptyHandle->tyMipsDW2App.pulReadCP0App[ptyHandle->tyMipsDW2App.ulReadCP0InstOffset] = MIPS_INST_MFC0_T0_1;
   if (pulData != NULL)
      {
      if (iResult == ERR_NO_ERROR)
         *pulData = pulDataFromProc[0];
      else
         *pulData = 0;
      }
   return iResult;
}

/****************************************************************************
     Function: MLR_WriteCP0Register
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulRegNumber - register number
               unsigned long ulData - value to write
               unsigned long ulSel - register select
       Output: int - error code
  Description: write MIPS CP0 register using DW2 application
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int MLR_WriteCP0Register(TyMLHandle *ptyHandle, 
                                unsigned long ulRegNumber, 
                                unsigned long ulData, 
                                unsigned long ulSel)
{
   unsigned long pulDataToProc[1];
   unsigned long ulWriteCP0Instruction = MIPS_INST_MTC0_T0_1;
   int iResult = ERR_NO_ERROR;
   // check parameters
   assert(ptyHandle != NULL);
   // we need to replace instruction in DW2 application with proper one with register number and select
   if (ulSel >= 0x100)
      {  // very special case for high and low multiply registers
      if (ulRegNumber == REG_MIPS_LO)
         ulWriteCP0Instruction = MIPS_INST_MTLO_T0;
      else if (ulRegNumber == REG_MIPS_HI)
         ulWriteCP0Instruction = MIPS_INST_MTHI_T0;
      // other case should not happen so we can leave original instruction which is harmless
      }
   else
      {
      if (ulRegNumber <= 31)
         {
         ulWriteCP0Instruction &= 0xFFFF07FF;            // clear bits 11 to 15
         ulWriteCP0Instruction |= (ulRegNumber << 11);   // replace bits 11 to 15 with register number
         ulWriteCP0Instruction |= (ulSel & 0x000000FF);  // use lowest 8 bits from register select
         }
      // other case should not happen so we can leave original instruction which is harmless
      }
   // replace instruction, prepare parameters, run DW2 and restore instruction
   ptyHandle->tyMipsDW2App.pulWriteCP0App[ptyHandle->tyMipsDW2App.ulWriteCP0InstOffset] = ulWriteCP0Instruction;
   pulDataToProc[0] = ulData;
   iResult = ML_ExecuteDW2(ptyHandle, 
                           (void *)(ptyHandle->tyMipsDW2App.pulWriteCP0App), 
                           ptyHandle->tyMipsDW2App.ulWriteCP0Instructions * sizeof(unsigned long),
                           (void *)pulDataToProc, 
                           1*sizeof(unsigned long), 
                           NULL, 
                           0);
   ptyHandle->tyMipsDW2App.pulWriteCP0App[ptyHandle->tyMipsDW2App.ulWriteCP0InstOffset] = MIPS_INST_MTC0_T0_1;
   return iResult;
}

/****************************************************************************
     Function: MLR_ReadACXRegister
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long *pulData - pointer to data
       Output: int - error code
  Description: read ACX register for SmartMIPS using DW2 application
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLR_ReadACXRegister(TyMLHandle *ptyHandle, unsigned long *pulData)
{
   int iResult = ERR_NO_ERROR;
   unsigned long pulDataFromProc[1];
   // check parameters
   assert(ptyHandle != NULL);
   // call DW2 application and check result
   iResult = ML_ExecuteDW2(ptyHandle, (void *)(ptyHandle->tyMipsDW2App.pulReadACXApp), ptyHandle->tyMipsDW2App.ulReadACXInstructions * sizeof(unsigned long),
                           NULL, 0, (void *)pulDataFromProc, 1*sizeof(unsigned long));
   if (pulData != NULL)
      {
      if (iResult == ERR_NO_ERROR)
         *pulData = pulDataFromProc[0];
      else
         *pulData = 0;
      }
   return iResult;
}

/****************************************************************************
     Function: MLR_WriteACXRegister
     Engineer: Vitezslav Hola
        Input: TyMLHandle *ptyHandle - handle to connection
               unsigned long ulData - value to write
       Output: int - error code
  Description: write ACX register for SmartMIPS using DW2 application
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static int MLR_WriteACXRegister(TyMLHandle *ptyHandle, unsigned long ulData)
{
   int iResult = ERR_NO_ERROR;
   unsigned long pulDataToProc[1];
   // check parameters
   assert(ptyHandle != NULL);
   // prepare parameters and call DW2
   pulDataToProc[0] = ulData;
   iResult = ML_ExecuteDW2(ptyHandle, (void *)(ptyHandle->tyMipsDW2App.pulWriteACXApp), ptyHandle->tyMipsDW2App.ulWriteACXInstructions * sizeof(unsigned long),
                           (void *)pulDataToProc, 1*sizeof(unsigned long), NULL, 0);
   return iResult;
}

