/****************************************************************************
       Module: ash_iface.h
     Engineer: Nikolay Chokoev
  Description: Header for Opella-XD MIPS Top Layer MDI.
Date           Initials    Description
04-Jul-2008    NCH          Initial
****************************************************************************/
#ifndef __ASH_IFACE_H__
#define __ASH_IFACE_H__

void SetupErrorMessage(int iErrorNumber);

#if defined( MDI_LIB )
  /* MDILib, do extern function declarations */
#ifdef __LINUX
#define ASH_DLL_EXPORT 
#else
//#define ASH_DLL_EXPORT __declspec(dllexport)
#define ASH_DLL_EXPORT
#endif
#elif defined( MDILOAD_DEFINE )
  /* debugger, do function pointer definitions */
#ifdef __LINUX
#define ASH_DLL_EXPORT 
#else
#define ASH_DLL_EXPORT __declspec(dllexport)
#endif
#else
  /* debugger, do extern function pointer declarations */
#ifdef __LINUX
#define ASH_DLL_EXPORT 
#else
#define ASH_DLL_EXPORT __declspec(dllexport)
#endif
#endif

#endif   // __ASH_IFACE_H__

