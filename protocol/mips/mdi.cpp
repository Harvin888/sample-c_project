/****************************************************************************
       Module: mdi.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD MIPS MDI interface.
               MDI interface as specificed in v2.12.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#define MDI_LIB               // We are an MDI Library not a debugger

#ifdef __LINUX
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#else
#include <windows.h>
#endif


#include <string.h>
#include "protocol/common/drvopxd.h"
#include "commdefs.h"
#include "gerror.h"
#include "midlayer.h"
#include "mdi.h"
#include "toplayer.h"
#include "ash_iface.h"
#include "debug.h"
#include "../../utility/debug/debug.h"

#define MAX_NUM_CONNECTIONS   1
#define MDI_NOT_CONNECTED     0x11111111
#define HAS_ATTRIBUTE(field,attribute) ((field & attribute) == attribute)
#define CALC_LENGTH(Start,End)         ((End-Start)+0x1)

int   bASHDMAClient;

TyLocMdiParams_t tyLocMdiPrms;

#ifdef __LINUX
void __attribute__ ((constructor)) my_load(void);
void __attribute__ ((destructor))  my_unload(void);
#endif


#ifndef __LINUX
/****************************************************************************
     Function: DllMain
     Engineer: Nikolay Chokoev
        Input: HINSTANCE hInstDll - DLL handle
               DWORD dwReason - reason whay this function was called
               LPVOID lpReserved - reserved
       Output: BOOL - TRUE if everythink is ok
  Description: DLL main entry point, called when DLL is loaded/unloaded
Date           Initials    Description
10-Jul-2007    NCH         Initial
****************************************************************************/
extern "C" BOOL APIENTRY DllMain(HINSTANCE hInstDll, DWORD dwReason, LPVOID lpReserved)
{
   lpReserved = lpReserved;         // make lint happy
   hInstDll = hInstDll;
   switch (dwReason)
      {
	  case DLL_PROCESS_ATTACH:      
         // loading DLL, this is time to get current path
         char pszLocDllPath[_MAX_PATH];
         // get DLL path name (without mdi.dll)
		 if (!GetModuleFileName(hInstDll, pszLocDllPath,_MAX_PATH))
			ML_SetDllPath("");
		 else
			ML_SetDllPath(pszLocDllPath);

		 tyLocMdiPrms.tyCurrentDeviceID = 0xDEADDEAD;
		 tyLocMdiPrms.iNumberOfMDIConnectionsEstablished = 0;
         break;
	  case DLL_PROCESS_DETACH:
		 //todo
         break;
      case DLL_THREAD_ATTACH:
      case DLL_THREAD_DETACH:
      default:
         break;
      }
   return TRUE;
}
#else
/****************************************************************************
     Function: Sleep
     Engineer: Nikolay Chokoev
        Input: mseconds
       Output: none
  Description: 
Date           Initials    Description
18-Dec-2007    NCH         Initial
*****************************************************************************/
void Sleep(unsigned int  mseconds )
{
    // Windows Sleep uses miliseconds
    // linux usleep uses microsecond
    usleep( mseconds * 1000 );
}

/****************************************************************************
     Function: my_load
     Engineer: Nikolay Chokoev
        Input: mseconds
       Output: none
  Description: This function will be called when the library is loaded 
               and before dlopen() returns
Date           Initials    Description
18-Dec-2007    NCH         Initial
*****************************************************************************/
void my_load(void)
{
	#define SIZE 260
	FILE *f;
	char line[SIZE];
   char* pszPath;


	f = fopen ("/proc/self/maps", "r");

	while (!feof (f)) 
   {
	   if (fgets (line, SIZE, f) == NULL)
		   break;

	   /* Sanity check. */
	   if (strstr (line, "opxdmips.so") == NULL)
		   continue;
    
      pszPath = strchr (line, '/');
      
      ML_SetDllPath(pszPath);
	}

	fclose (f);
}

/****************************************************************************
     Function: my_unload
     Engineer: Nikolay Chokoev
        Input: mseconds
       Output: none
  Description: This function will be called when the library is unloaded and 
               before dlclose() returns
Date           Initials    Description
18-Dec-2007    NCH         Initial
*****************************************************************************/
void my_unload(void)
{
// Add clean-up code�
}
#endif

/****************************************************************************
     Function: MDIVersion
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIVersion(MDIVersionRangeT * pVersion)
{
   pVersion->oldest = MDIOldestRevision;
   pVersion->newest = MDICurrentRevision;

   return MDISuccess;
}

/****************************************************************************
     Function: MDIConnect
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIConnect (MDIVersionT Version ,
								MDIHandleT 	*pMDIHandle,
								MDIConfigT 	*pMDIConfig)
{
   int iErrRet = MDISuccess;
   (void)TL_MDILogInitAndCheckSettings(NULL);

   bASHDMAClient = 0; //todo FALSE/TRUE or ASH_FALSE/ASHTRUE defines...

   if ( Version > MDICurrentRevision || 
		Version < MDIOldestRevision)
      {
	  return MDIErrVersion;
      }

   if (tyLocMdiPrms.iNumberOfMDIConnectionsEstablished > MAX_NUM_CONNECTIONS)
      {
      ASSERT_NOW();
//      SetupErrorMessage(ERR_MDICON_TOO_MANY_CONNECTIONS);
//      return MDIErrVersion;
      iErrRet = MDISuccess;
      }

   tyLocMdiPrms.iNumberOfMDIConnectionsEstablished=1;
   strcpy(pMDIConfig->Implementer,"ASHLING Opella-XD MDI Library");
   pMDIConfig->MDICapability = ( MDICAP_NoParser | MDICAP_NoDebugOutput );

   tyLocMdiPrms.tyCurrentMDIHandle = 
		 (MDIHandleT)(MDI_NOT_CONNECTED + tyLocMdiPrms.iNumberOfMDIConnectionsEstablished); 
   *pMDIHandle = tyLocMdiPrms.tyCurrentMDIHandle;

   if (strncmp(pMDIConfig->User, "ASH_DMA", 0x7) == 0x0)
      bASHDMAClient = 1;//todo FALSE/TRUE or ASH_FALSE/ASHTRUE defines...

   // TODO: Handle MDI callbacks especially MDICBPeriodic()
   return iErrRet;
}

/****************************************************************************
     Function: MDIDisconnect
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIDisconnect (MDIHandleT  	MDIHandle, 
								   MDIUint32 	Flags)
{
   if (MDIHandle != tyLocMdiPrms.tyCurrentMDIHandle)
      return MDIErrMDIHandle;
   if (Flags == MDICurrentState)
      {
      // Close all open target groups and target devices
      //iNumberOfMDIConnectionsEstablished--;
      tyLocMdiPrms.iNumberOfMDIConnectionsEstablished = 0;
      tyLocMdiPrms.tyCurrentMDIHandle = MDI_NOT_CONNECTED;
      return MDISuccess;
      }

   if (Flags == MDIResetState)
      {
      // Place all open target groups in reset, then close all open
      // target groups and target devices

      // TODO: RESET DEVICE HERE

      tyLocMdiPrms.iNumberOfMDIConnectionsEstablished = 0;
      tyLocMdiPrms.tyCurrentMDIHandle = MDI_NOT_CONNECTED;
      return MDISuccess;
      }

   // We should have been passed at least one of those flags
   return MDIErrParam;
}

/****************************************************************************
     Function: MDITGQuery
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITGQuery (MDIHandleT, 
								MDIInt32 *HowMany, 
								MDITGDataT *)
{
   HowMany = HowMany;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITGOpen
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITGOpen (MDIHandleT, 
							   MDITGIdT, 
							   MDIUint32, MDIHandleT *)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITGClose
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITGClose (MDIHandleT, MDIUint32)
{
      return MDIErrFailure;
}

/****************************************************************************
     Function: MDITGExecute
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITGExecute (MDIHandleT)
{
      return MDIErrFailure;
}

/****************************************************************************
     Function: MDITGStop
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITGStop (MDIHandleT)
{
      return MDIErrFailure;
}

/****************************************************************************
     Function: MDIDQuery
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIDQuery (MDIHandleT  	MDIHandle,
							   MDIInt32 	*pHowMany,
							   MDIDDataT	*pDeviceData)
{
   int  iErrRet;
   if (MDIHandle != tyLocMdiPrms.tyCurrentMDIHandle)
      {
      ASSERT_NOW();
      if (MDIHandle != 0xffffffff)
         return MDIErrTGHandle;
      }

   iErrRet = TL_QuerySupportedDevices(pHowMany, pDeviceData);   // Lets keep this layer small

   if (iErrRet != ERR_NO_ERROR)
      {
      SetupErrorMessage(iErrRet);
      return MDIErrParam;
      }
   // We should have been passed at least one of those flags
   return MDISuccess;
}

/****************************************************************************
     Function: MDIOpen
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIOpen (MDIHandleT  	MDIHandle,
							 MDIDeviceIdT 	DeviceID,
							 MDIUint32 		Flags,
							 MDIHandleT		*pDeviceHandle)
{

   int iErrRet;
   if (MDIHandle != tyLocMdiPrms.tyCurrentMDIHandle)
      {
      return MDIErrTGHandle;
      }

   if (MDISharedAccess == Flags)
      {// Shared Access not supported
      return MDIErrParam;  
      }

   if (!(MDIExclusiveAccess == Flags))
      {// If not Exclusive then what ??
      return MDIErrParam;  
      }

   // TODO: Error Check for maximum devices.

   tyLocMdiPrms.tyCurrentDeviceID = DeviceID; // Set local copy of Device ID
   // We don't do anything in particular with it for now, but it might be useful later.

   *pDeviceHandle = tyLocMdiPrms.tyCurrentDeviceID;

   iErrRet = TL_DeviceConnect();
   if (iErrRet != ERR_NO_ERROR)
      {
	  (void)TL_DeviceDisconnect();
      tyLocMdiPrms.bFailedToConnectToTarget = 1; //todo TRUE...
      SetupErrorMessage(iErrRet);   
      return MDIErrFailure;
      }
   return MDISuccess;
}

/****************************************************************************
     Function: MDIClose
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIClose (MDIHandleT  	DeviceHandle,
							  MDIUint32 	Flags)
{
   int iErrRet = ERR_NO_ERROR;
   if (DeviceHandle != tyLocMdiPrms.tyCurrentDeviceID)
      {
      ASSERT_NOW();
      return MDIErrParam;
      }

   switch (Flags)
      {
      case MDICurrentState:
         // Close Specific Device
         tyLocMdiPrms.tyCurrentDeviceID  = MDI_NO_DEVICE_OPEN;
         break;
      case MDIResetState:
         // Close Specific Device
         // TODO: Reset Device Here
         tyLocMdiPrms.tyCurrentDeviceID  = MDI_NO_DEVICE_OPEN;
         break;
      default:
         // We should have been passed at least one of those flags
         ASSERT_NOW();
         return MDIErrParam;
      }

   if (!tyLocMdiPrms.bFailedToConnectToTarget)
      {
      (void)TL_ClearAllBP();
      }

   iErrRet = TL_DeviceDisconnect();
   if (iErrRet != 0) //todo error code
      {
      DLOG((LOGFILE,"\n\tMDIClose -> Failed to Disconnect Device"));
      SetupErrorMessage(iErrRet);   
      return MDIErrFailure;
      }

   return MDISuccess;
}

/****************************************************************************
     Function: MDIRead
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIRead(MDIHandleT   Device,
							MDIResourceT SrcResource,
							MDIOffsetT   SrcOffset,
							void *       pBuff,
							MDIUint32    ObjectSize,
							MDIUint32    Count)
{
   
   int iErrRet = MDISuccess;

   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }

   switch (SrcResource)
      {
      case MDIMIPCPU:
         // 32 general Purpose Registers
         iErrRet=TL_RegisterRead(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPPC:
         // PC Register
         iErrRet=TL_RegisterRead(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPHILO:
         // Multiplier Hi and Low Registers
         iErrRet=TL_RegisterRead(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPCP0:
         // MIPS CP0 Regisers (up to 256 banks of 32 registers)
         iErrRet=TL_RegisterRead(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPGVIRTUAL:
         // MIPS Global Virtual Memory Space
         if (ObjectSize == 0)
            {
            iErrRet = TL_MemoryRead(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
            }
         else
            {
            // Specific ObjectSize, handle down in Midlayer
            iErrRet=TL_MDIRead(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
            }
         break;
      case MDIMIPPHYSICAL:
         // MIPS Physical Memory, handle down in Midlayer
         iErrRet=TL_MDIRead(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
         break;
      case MDIMIPEJTAG:
         // MIPS EJTAG Memory, handle down in Midlayer
         iErrRet=TL_MDIRead(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
         break;
      case MDIMIPSRS:
         // MIPS register from 1 of MIPS Shadow Register Set (SRS)
         iErrRet=TL_RegisterRead(SrcResource,SrcOffset,pBuff,Count);
         break;
      default:
         // Handle Every Other case down in Midlayer
         iErrRet=TL_MDIRead(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
         break;
      }
   if (iErrRet != ERR_NO_ERROR)
      {
      SetupErrorMessage(iErrRet);   
      iErrRet = MDIErrFailure;
      }
   return iErrRet;
}

/****************************************************************************
     Function: MDIWrite
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIWrite(MDIHandleT   Device,
							 MDIResourceT SrcResource,
							 MDIOffsetT   SrcOffset,
							 void *       pBuff,
							 MDIUint32    ObjectSize,
							 MDIUint32    Count)
{
   
   int iErrRet = MDISuccess;

   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }
   switch (SrcResource)
      {
      case MDIMIPCPU:
         // 32 general Purpose Registers
         iErrRet=TL_RegisterWrite(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPPC:
         // PC Register
         iErrRet=TL_RegisterWrite(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPHILO:
         // Multiplier Hi and Low Registers
         iErrRet=TL_RegisterWrite(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPCP0:
         // MIPS CP0 Regisers (up to 256 banks of 32 registers)
         iErrRet=TL_RegisterWrite(SrcResource,SrcOffset,pBuff,Count);
         break;
      case MDIMIPGVIRTUAL:
         // MIPS Global Virtual Memory Space
         if (ObjectSize == 0)
            {
            iErrRet=TL_MemoryWrite(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
            }
         else
            {
            // Specific ObjectSize, handle down in Midlayer
            iErrRet=TL_MDIWrite(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
            }
         break;
      case MDIMIPPHYSICAL:
         // MIPS Physical Memory, handle down in Midlayer
         iErrRet=TL_MDIWrite(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
         break;
      case MDIMIPEJTAG:
         // MIPS EJTAG Memory, handle down in Midlayer
         iErrRet=TL_MDIWrite(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
         break;
      case MDIMIPSRS:
         // MIPS register from 1 of MIPS Shadow Register Set (SRS)
         iErrRet=TL_RegisterWrite(SrcResource,SrcOffset,pBuff,Count);
         break;
      default:
         // Handle every other case down in Midlayer
         iErrRet=TL_MDIWrite(SrcResource,SrcOffset,pBuff,Count,ObjectSize);
         break;
      }
   if (iErrRet != 0) //todo error code
      {
      SetupErrorMessage(iErrRet);   
      iErrRet=MDIErrFailure;
      }
   return iErrRet;
}

/****************************************************************************
     Function: MDIReadList
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIReadList (MDIHandleT Device, 
								 MDIUint32 	ObjectSize, 
								 MDICRangeT *SrcList,
								 MDIUint32 	ListCount, 
								 void 		*Buffer )
{
   
   NOREF(Device);
   NOREF(ObjectSize);
   NOREF(SrcList);
   NOREF(ListCount);
   NOREF(Buffer);
   #if 0
   int iErrRet = MDISuccess;
   unsigned int i;
   unsigned long tempBuffer[100];
   char *tempPtr;
   tempPtr = (char*)Buffer;
   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }

   for (i=0;i<ListCount;i++)
      {
      iErrRet = MDIRead(Device,SrcList[i].Resource,SrcList[i].Offset,(void *)tempBuffer,ObjectSize,SrcList[i].Count);
      if(iErrRet != MDISuccess)
         {
         return iErrRet;
         }
      memcpy(tempPtr,(void*)tempBuffer,ObjectSize*SrcList[i].Count );
      tempPtr += ObjectSize*SrcList[i].Count;
      }
   #endif
   return MDISuccess;
}

/****************************************************************************
     Function: MDIWriteList
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIWriteList (MDIHandleT, 
								  MDIUint32 	ObjectSize, 
								  MDICRangeT 	*DstList,
								  MDIUint32 	ListCount, 
								  void 			*Buffer )
{
   ObjectSize = ObjectSize;
   DstList = DstList;
   ListCount = ListCount;
   Buffer = Buffer;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIMove
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIMove (MDIHandleT, 
							 MDIResourceT 	SrcR, 
							 MDIOffsetT 	SrcO,
							 MDIResourceT 	DstR, 
							 MDIOffsetT 	DstO, 
							 MDIUint32 		ObjectSize,
							 MDIUint32 		Count, 
							 MDIUint32 		Flag)
{
   SrcR = SrcR;
   SrcO = SrcO;
   DstR = DstR;
   DstO = DstO;
   ObjectSize = ObjectSize;
   Count = Count;
   Flag = Flag;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIFill
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIFill (MDIHandleT, 
							 MDIResourceT 	DstR, 
							 MDIRangeT 		DstRng,
							 void 			*Buffer, 
							 MDIUint32 		ObjectSize, 
							 MDIUint32 		Count)
{
   DstR = DstR;
   DstRng = DstRng;
   Buffer = Buffer;
   ObjectSize = ObjectSize;
   Count = Count;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIFind
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIFind (MDIHandleT, 
							 MDIResourceT 	SrcR, 
							 MDIRangeT 		SrcRng,
							 void 			*Buffer, 
							 void 			*MaskBuffer, 
							 MDIUint32 		ObjectSize,
							 MDIUint32 		Count, 
							 MDIOffsetT 	*FoundOffset, 
							 MDIUint32 		Mode)
{
   SrcR = SrcR;
   SrcRng = SrcRng;
   Buffer = Buffer;
   MaskBuffer = MaskBuffer;
   ObjectSize = ObjectSize;
   Count = Count;
   FoundOffset = FoundOffset;
   Mode = Mode;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIExecute
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIExecute(MDIHandleT   Device)
{
   
   int iErrRet = MDISuccess;

   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }

   iErrRet=TL_Execute();   
   if (iErrRet != 0)//todo error code
      {
      SetupErrorMessage(iErrRet);
      iErrRet = MDIErrFailure; 
      }

   return iErrRet;
}

/****************************************************************************
     Function: MDIStep
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIStep(MDIHandleT  Device, 
							MDIUint32 	Steps, 
							MDIUint32 	Mode)
{
   
   int iErrRet = MDISuccess;
   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }
   iErrRet=TL_Step(Steps,Mode);  
   if (iErrRet != 0) //todo error code
      {
      SetupErrorMessage(iErrRet);
      iErrRet = MDIErrFailure; 
      }

   return iErrRet;
}

/****************************************************************************
     Function: MDIStop
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIStop(MDIHandleT   Device)
{
   
   int iErrRet = MDISuccess;
   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }
   iErrRet=TL_Stop();  
   if (iErrRet != 0) //todo error code
      {
      SetupErrorMessage(iErrRet);
      iErrRet = MDIErrFailure; 
      }

   return iErrRet;
}

/****************************************************************************
     Function: MDIStop
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIReset(MDIHandleT Device,
							 MDIUint32 	Mode)
{
   
   int iErrRet = 0; //todo error code
   // On Reset we do a spot check of the Registry to see if logging is still enabled.
   (void)TL_MDILogInitAndCheckSettings(NULL);

   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }

   iErrRet = TL_Reset(Mode);  
   if (iErrRet != 0) //todo error code
      {
      SetupErrorMessage(iErrRet);
      iErrRet = MDIErrFailure; 
      }

   return iErrRet;
}

/****************************************************************************
     Function: MDICacheQuery
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDICacheQuery (MDIHandleT, 
								   MDICacheInfoT *CacheInfo)
{
   CacheInfo=CacheInfo;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDICacheFlush
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
30-Aug-2011    SPC         Initial
****************************************************************************/
extern "C" MDIInt32 MDICacheFlush (MDIHandleT Device, 
								   MDIUint32 Type, 
								   MDIUint32 Flag)
{
   int iErrRet = MDISuccess;
   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
	  {
	  return MDIErrDevice;
	  }
   iErrRet = TL_CacheFlush(Type, Flag);
   if (iErrRet !=0)
	  {
	  return MDIErrFailure;
	  }
   else
	  {
	  return MDISuccess;
	  }
}

/****************************************************************************
     Function: MDIRunState
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIRunState(MDIHandleT 		Device, 
								MDIInt32 		WaitTime, 
								MDIRunStateT 	*pRunState)
{
   
   int iErrRet = MDISuccess;
   TyStatus tyStatus;
   static char szPowerDownString[100];
   WaitTime = WaitTime;
   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }
   
   iErrRet=TL_GetStatus(&tyStatus); // Should be safe enough to use target memory if needed

   if (iErrRet!= ERR_NO_ERROR)
      {
      SetupErrorMessage(iErrRet);
      return MDIErrFailure; 
      }

   if (tyStatus.bInDebugMode == 1) //todo TRUE
      {
      if (tyStatus.cobCause == COB_SINGLE_STEP)
         {
         pRunState->Status = MDIStatusStepsDone;
         }
      if (tyStatus.cobCause == COB_USER_HALT)
         {
         pRunState->Status = MDIStatusHalted;
         }
      if (tyStatus.cobCause == COB_BP_OPCODE)
         {
         pRunState->Status = MDIStatusBPHit;
         pRunState->Info.value = tyStatus.ulBPId;
         }
      if (tyStatus.cobCause == COB_BP_HARDWARE)
         {
         pRunState->Status = MDIStatusInstBPHit;      // Warning Not STD MDI
         pRunState->Info.value = tyStatus.ulBPId;
         }
      if (tyStatus.cobCause == COB_BP_DATA_RD)
         {
         pRunState->Status = MDIStatusDataReadBPHit;  // Warning Not STD MDI
         pRunState->Info.value = tyStatus.ulBPId;
         }
      if (tyStatus.cobCause == COB_BP_DATA_WR)
         {
         pRunState->Status = MDIStatusDataWriteBPHit; // Warning Not STD MDI
         pRunState->Info.value = tyStatus.ulBPId;
         }
      if (tyStatus.cobCause == COB_BP_WATCHPOINT_MATCH)
         {
         pRunState->Status = MDIStatusInstBPHit; // Warning Not STD MDI, same as Hardware BP
         pRunState->Info.value = tyStatus.ulBPId;
         }
      if (tyStatus.cobCause == COB_BP_WATCHPOINT_MISS)
         {
		 // we need to re-execute processor
         pRunState->Status = MDIStatusRunning;
		 iErrRet = MDIExecute(Device);
         }
      if (tyStatus.cobCause == COB_UNKNOWN)
         {
         pRunState->Status = MDIStatusNotRunning; // Don't know why
         }
      }
   else
      {
      // Still Running
      pRunState->Status = MDIStatusRunning;
      if (tyStatus.bExternalResetDetected)
         {
         pRunState->Status |= MDIStatusWasReset;
         }
      }
   // Note: The following conditions are not dealt with yet
   // 1. Non MDI standard handling for HW/SW/DATA breakpoints
   // 2. Our MDILib doesn't detect MDIStatusUsrBPHit ( User opcode breakpoint)
   // 3. Our MDILib doesn't detect MDIStatusExited (System call exit app)
   // 4. Our MDILib doesn't detect MDITraceFull (Guess why!)
   // 5. The reset flags are ignored for now.
   if (tyStatus.bNoClockPresent)
      {
      pRunState->Status |= MDIStatusReset;
      }

   if (tyStatus.bReducedPowerMode)
      {
      pRunState->Status |= MDIStatusDescription;   
      strcpy(szPowerDownString,POWER_DOWN_MODE_STRING);
      pRunState->Info.ptr = szPowerDownString;
      }
   return iErrRet;
}

/****************************************************************************
     Function: MDISetBp
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDISetBp(MDIHandleT   	Device,
							 MDIBpDataT 	*pBpData)
{
   
   int            iErrRet = MDISuccess;
   unsigned long  ulBpVirtAddress;
   unsigned long  ulBpAccessAddress;
   unsigned long  ulBPSize;
   TyBPType       bptBPType;  
   int            bComplexHwBp = 0;//todo FALSE

   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }

   ulBpVirtAddress      = (unsigned long)pBpData->Range.Start;
   ulBpAccessAddress    = (unsigned long)pBpData->Range.Start;

   switch (pBpData->Type & MDIBPT_TypeMask)
      {
      case MDIBPT_SWInstruction:
         // Standard software instruction breakpoint
         bptBPType  = BP_SW;
         if(HAS_ATTRIBUTE(pBpData->Type, MDIBPT_SWFlg_UseNonStandardAddress))
            {
            ulBpAccessAddress = (unsigned long)pBpData->Data; // Hack for read-inhibit areas
            }
         break;
      case MDIBPT_HWInstruction:
         // Complex hardware instruction breakpoint
         // If neither MDIBPT_HWFlg_AddrMask or MDIBPT_HWFlg_AddrRange
         // is set then it is a simple hardware breakpoint (i.e not complex)
         // Simple hardware breakpoints should be passed through TL_SetBP
         bptBPType  = BP_HW;
         if (
               HAS_ATTRIBUTE(pBpData->Type, MDIBPT_HWFlg_AddrMask) || 
               HAS_ATTRIBUTE(pBpData->Type, MDIBPT_HWFlg_AddrRange)
            )
            {
            // Complex Hardware Instruction Breakpoint
            bComplexHwBp= 1;//todo TRUE
            }
         break;
      case MDIBPT_HWData:
         // Complex hardware Data Breakpoint
         bptBPType      = BP_HW;
         bComplexHwBp   = 1;//todo TRUE
         break;
      default:
         bptBPType  = BP_NO;
         break;
      }

   // If this breakpoint generates a trigger then treat it as a complex breakpoint
   if((pBpData->Type & MDIBPT_HWFlg_Trigger) == MDIBPT_HWFlg_Trigger)
      bComplexHwBp   = 1;//todo TRUE
   if((pBpData->Type & MDIBPT_HWFlg_TriggerOnly) == MDIBPT_HWFlg_TriggerOnly)
      bComplexHwBp   = 1;//todo TRUE

   // Determine breakpoint size
   if (ulBpVirtAddress & MDIMIP_Flg_MIPS16)
      {
      // This is a MIPS16 Breakpoint
      ulBPSize = 2;
      ulBpVirtAddress   = ulBpVirtAddress & (~MDIMIP_Flg_MIPS16);  // Sanatise the address.
      ulBpAccessAddress = ulBpAccessAddress & (~MDIMIP_Flg_MIPS16);  // Sanatise the address.
      }
   else
      {
      ulBPSize = 4;
      }

   if (!bComplexHwBp)
      {
      iErrRet= TL_SetBP(ulBpVirtAddress,ulBpAccessAddress,bptBPType,ulBPSize);

      if (iErrRet != 0)//todo error code
         {
         SetupErrorMessage(iErrRet);
         return MDIErrFailure; 
         }
      // If breakpoint set correctly set ID to value of Address of Breakpoint
      pBpData->Id = ulBpVirtAddress;         // Use Address as ID
      }
   else
      {
      // If breakpoint set correctly set ID to value of Address of Breakpoint
      TyBpFlags      tyBreakpointFlags;

      memset(&tyBreakpointFlags, 0x0, sizeof(tyBreakpointFlags));

      if (pBpData->Resource & MDIMIPVIRTUAL)
         {
         // It's an ASID specifc breakpoint/trigger
         tyBreakpointFlags.bMatchASID     = 1;//todo TRUE;
         tyBreakpointFlags.ulASIDToMatch  = (pBpData->Resource & 0xFF);
         }

      // Data or Instruction Breakpoint??
      if(HAS_ATTRIBUTE(pBpData->Type, MDIBPT_HWData))
         {
         tyBreakpointFlags.bDataBp = 1;//todo TRUE;
         }
      // Address of breakpoint
      tyBreakpointFlags.ulBpAddress = ulBpVirtAddress;
      // Mask of breakpoint address
      if(HAS_ATTRIBUTE(pBpData->Type,MDIBPT_HWFlg_AddrMask))
         {
         // We have the mask already
         tyBreakpointFlags.ulBpAddressMask = (unsigned long)pBpData->Range.End;
         }
      else
         {
         // TO_DO Check this again. This is shit
         // We must calculte the mask
         ASSERT_NOW();
         unsigned long ulLength;
		 ulLength = (unsigned long)CALC_LENGTH(ulBpVirtAddress, pBpData->Range.End);
         // Length has to be a multiple of two for this to work
         tyBreakpointFlags.ulBpAddressMask = ulLength - 0x1;
         }

      // Generate trigger/breakpoints
      tyBreakpointFlags.bGenBp      = 1;//todo TRUE;
      if (HAS_ATTRIBUTE(pBpData->Type,MDIBPT_HWFlg_TriggerOnly))
         {
         tyBreakpointFlags.bGenBp      = 0;//todo FALSE;
         tyBreakpointFlags.bGenTrigger = 1;//todo TRUE;
         }
      else
         if (HAS_ATTRIBUTE(pBpData->Type,MDIBPT_HWFlg_Trigger))
            {
            tyBreakpointFlags.bGenTrigger = 1;//todo TRUE;
            }

      if(tyBreakpointFlags.bDataBp)
         {
         // Specific to data breakpoints
         // Data Value and mask
         tyBreakpointFlags.ucByteAccessIgnore = 0; // Ignore no bytes
         if(!HAS_ATTRIBUTE(pBpData->Type, MDIBPT_HWFlg_DataValue))
            {
            tyBreakpointFlags.ucByteLaneMask = 0xF; // Ignore all bytes
            }
         else
            {
            // We are Comparing use Data Value Specified
            // Mask bytes specified
            tyBreakpointFlags.ulDataValue    = (unsigned long)pBpData->Data;
            tyBreakpointFlags.ucByteLaneMask = (unsigned char)pBpData->DataMask; 
            }

         // Fufill on load
         if(HAS_ATTRIBUTE(pBpData->Type,MDIBPT_HWFlg_DataRead))
            {
            tyBreakpointFlags.bFufillOnLoad = 1;//todo TRUE;
            }
         // Fufil on store
         if(HAS_ATTRIBUTE(pBpData->Type, MDIBPT_HWFlg_DataWrite))
            {
            tyBreakpointFlags.bFufillOnStore = 1;//TRUE;
            }
         }

      tyBreakpointFlags.bMIPS16Breakpoint = (unsigned char)(ulBPSize == 2);

      iErrRet = TL_SetComplexHWBP(&tyBreakpointFlags);
      pBpData->Id = tyBreakpointFlags.ulBreakpointID; 

      if (iErrRet != 0)//todo error code
         {
         SetupErrorMessage(iErrRet);
         iErrRet = MDIErrFailure; 
         }
      }
   return iErrRet;
}

/****************************************************************************
     Function: MDISetBp
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDISetSWBp (MDIHandleT Device,
								MDIResourceT Resource,
								MDIOffsetT Offset, MDIBpIdT *BpId)
{
	int iErrRet = MDISuccess;
     NOREF(Resource);
     NOREF(Offset);
     NOREF(BpId);
     NOREF(Device);
     #if 0
    TyBPType bptBPType;
	unsigned long ulBPSize;
   
	if (Device != tyLocMdiPrms.tyCurrentDeviceID) {
		return MDIErrDevice;
	}

	bptBPType = BP_SW;

	// Determine breakpoint size
	if (Offset & MDIMIP_Flg_MIPS16)
	   {
	   // This is a MIPS16 Breakpoint
	   ulBPSize = 2;
	   Offset   = Offset & (~MDIMIP_Flg_MIPS16);  // Sanatise the address.
	   }
	else
	   {
	   ulBPSize = 4;
	   }
   
	iErrRet = TL_SetBP(Offset, Offset, bptBPType, ulBPSize);

	*BpId = Offset;
    #endif
	return iErrRet;
}

/****************************************************************************
     Function: MDISetBp
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIClearBp(MDIHandleT   Device,
							   MDIBpIdT 	tyBpId)
{
   unsigned long  ulAddress = 0;
   unsigned long  ulBPSize;
   int  iErrRet = 0;//todo NO_ERROR;
   TyBPType  bptBPType;

   if (Device != tyLocMdiPrms.tyCurrentDeviceID)
      {
      return MDIErrDevice;
      }

   // Note: We assume that in every case the Breakpoint ID will allways be the 
   // Address of the Instruction breakpoint.

   ulAddress = (unsigned long) tyBpId;

   iErrRet= TL_ClearBP(ulAddress, &bptBPType, &ulBPSize, NULL);
   if (iErrRet != 0) //todo error code
      {
      SetupErrorMessage(iErrRet);
      iErrRet = MDIErrFailure;
      }

   return iErrRet;
}

/****************************************************************************
     Function: MDIEnableBp
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIEnableBp (MDIHandleT, MDIBpIdT)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIDisableBp
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIDisableBp (MDIHandleT, MDIBpIdT)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIBpQuery
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIBpQuery (MDIHandleT, 
								MDIInt32 *HowMany, 
								MDIBpDataT *)
{
   HowMany = HowMany;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIDoCommand
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIDoCommand (MDIHandleT, 
								  char *Buffer)
{
   Buffer = Buffer;
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIAbort
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDIAbort ( MDIHandleT )
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITraceEnable
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITraceEnable (MDIHandleT)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITraceDisable
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITraceDisable (MDIHandleT)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITraceClear
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITraceClear (MDIHandleT)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITraceStatus
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITraceStatus (MDIHandleT, MDIUint32 *)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITraceCount
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITraceCount (MDIHandleT, MDIUint32 *)
{
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITraceRead
     Engineer: Nikolay Chokoev
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
04-Jul-2008    NCH         Initial
****************************************************************************/
extern "C" MDIInt32 MDITraceRead (MDIHandleT, 
								  MDIUint32, 
								  MDIUint32, 
								  MDIUint32,
								  MDITrcFrameT *)
{
   return MDIErrFailure;
}
#if 0
/****************************************************************************
     Function: MDISetTC
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDISetTC (MDIHandleT, MDITCIdT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIGetTC
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIGetTC (MDIHandleT, MDITCIdT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITCQuery
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITCQuery (MDIHandleT,
                                                            MDIInt32*,
                                                            MDITCDataT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrUnsupported;
}

/****************************************************************************
     Function: MDISetRunMode
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDISetRunMode (MDIHandleT, MDITCIdT,   MDIUint32,   MDIUint32)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITeamCreate
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITeamCreate (MDIHandleT, MDITeamIdT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIQueryTeams
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIQueryTeams (MDIHandleT, MDIInt32*, MDITeamIdT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}


/****************************************************************************
     Function: MDIHwBpQuery
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIHwBpQuery (MDIHandleT, MDIInt32*, MDIBpInfoT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDICacheOp
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDICacheOp (MDIHandleT,MDIResourceT,MDIInt32,MDIResourceT,MDIOffsetT,MDIUint32)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDICacheSync
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDICacheSync (MDIHandleT, MDIResourceT, MDIOffsetT, MDIUint32 )
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDICacheInfo
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDICacheInfo (MDIHandleT, MDIResourceT, MDICacheInfoT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITeamClear
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITeamClear (MDIHandleT, MDITeamIdT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITeamDestroy
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITeamDestroy (MDIHandleT, MDITeamIdT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITMAttach
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITMAttach (MDIHandleT, MDITeamIdT, MDITMDataT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITMDetach
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITMDetach (MDIHandleT, MDITeamIdT, MDITMDataT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIQueryTM
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIQueryTM (MDIHandleT, MDITeamIdT, MDIInt32*, MDITMDataT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDITeamExecute
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDITeamExecute (MDIHandleT, MDITeamIdT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDISetBpPrimingCondition
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDISetBpPrimingCondition (MDIHandleT, MDIBpIdT, MDIPrimingConditionT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIGetBpPrimingCondition
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIGetBpPrimingCondition (MDIHandleT, MDIBpIdT, MDIPrimingConditionT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDICbtConfigQuery
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDICbtConfigQuery (MDIHandleT, MDICbtConfigTypeT, MDICbtBPTypeT,
                                                                   MDICbtIndexT, MDIInt32*, MDICbtConfigT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIGetStopWatchValue
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIGetStopWatchValue (MDIHandleT, MDIStopWatchValueT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIClearStopWatch
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIClearStopWatch (MDIHandleT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDISetStopWatchMode
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDISetStopWatchMode (MDIHandleT, MDIStopWatchModeT, MDIPairIndexT,
                                                                     MDIStartIndexT, MDIStopIndexT)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}

/****************************************************************************
     Function: MDIGetStopWatchMode
     Engineer: Suresh P.C
        Input: As per MDI Spec
       Output: As per MDI Spec
  Description: As per MDI Spec
Date           Initials    Description
27-May-2010    SPC         Initial
****************************************************************************/
extern "C" int __declspec(dllexport) __stdcall  MDIGetStopWatchMode (MDIHandleT, MDIStopWatchModeT*, MDIPairIndexT*,
                                                                     MDIStartIndexT*, MDIStopIndexT*)
{
   printf("%s called\n",__FUNCTION__);
   return MDIErrFailure;
}
#endif
