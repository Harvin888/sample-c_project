/****************************************************************************
       Module: mips.h
     Engineer: Vitezslav Hola
  Description: Header for MIPS.
Date           Initials    Description
08-Feb-2008    VH          Initial
10-Aug-2009    SPC         Added KSEG and KUSEG section addresses
****************************************************************************/
#ifndef MIPS_H_
#define MIPS_H_

// global constants
#define ASHLING_INT_REG                      0xFFFFFFFF                 // special register number

// MIPS memory segments
#define KSEG0_START                          0x80000000
#define KSEG0_END                            0x9FFFFFFF
#define KSEG1_START                          0xA0000000
#define KSEG1_END                            0xBFFFFFFF
#define KSEG2_START                          0xC0000000
#define KSEG2_END                            0xDFFFFFFF
#define KSEG3_START                          0xE0000000
#define KSEG3_END                            0xFFFFFFFF
#define KUSEG_START                          0x00000000
#define KUSEG_END                            0x7FFFFFFF
#define PHY_MASK                             0x1FFFFFFF
// debug segments
#define MIPS_DMSEG                           0xFF200000
#define MIPS_DRSEG                           0xFF300000                 // start of EJTAG memory area
#define MIPS_DE_VECT                         (MIPS_DMSEG + 0x00000200)  // MIPS debug exception vector in DMSEG
#define MIPS_DCR_ADDRESS                     (MIPS_DRSEG + 0x0)

// MIPS EJTAG defines
#define MIPS_EJTAG_IR_LENGTH                 5                          // default IR length for MIPS (be careful, some target may use different)
#define MIPS_EJTAG_IDCODE_INST               0x00000001                 // EJTAG IDCODE instruction
#define MIPS_EJTAG_IMPCODE_INST              0x00000003                 // EJTAG IMPCODE instruction
#define MIPS_EJTAG_ADDRESS_INST              0x00000008                 // EJTAG ADDRESS instruction
#define MIPS_EJTAG_DATA_INST                 0x00000009                 // EJTAG DATA instruction
#define MIPS_EJTAG_CONTROL_INST              0x0000000A                 // EJTAG CONTROL instruction
#define MIPS_EJTAG_ALL_INST                  0x0000000B                 // EJTAG ALL instruction
#define MIPS_EJTAG_EJTAGBOOT_INST            0x0000000C                 // EJTAG EJTAGBOOT instruction
#define MIPS_EJTAG_NORMALBOOT_INST           0x0000000D                 // EJTAG NORMALBOOT instruction
#define MIPS_EJTAG_FASTDATA_INST             0x0000000E                 // EJTAG FASTDATA instruction
#define MIPS_EJTAG_PCTRACE_INST              0x00000010                 // EJTAG PCTRACE instruction
#define MIPS_EJTAG_TCBCONTROLA_INST          0x00000010                 // EJTAG TCBCONTROLA instruction (EJTAG 2.60 and later)
#define MIPS_EJTAG_TCBCONTROLB_INST          0x00000011                 // EJTAG TCBCONTROLB instruction (EJTAG 2.60 and later)
#define MIPS_EJTAG_TCBDATA_INST              0x00000012                 // EJTAG TCBDATA instruction (EJTAG 2.60 and later)
#define MIPS_EJTAG_TCBCONTROLC_INST          0x00000013                 // EJTAG TCBCONTROLB instruction (EJTAG 3.10 and later)
#define MIPS_EJTAG_PCSAMPLE_INST             0x00000014                 // EJTAG PCSAMPLE instruction (EJTAG 3.10 and later)
#define MIPS_EJTAG_EJWATCH_INST              0x0000001C                 // EJTAG EWATCH instruction (only AMD Au1x00)
#define MIPS_EJTAG_BYPASS_INST               0xFFFFFFFF                 // EJTAG BYPASS instruction (enough bits for sure)

#define MIPS_EJTAG_IDCODE_LENGTH             32                         // IDCODE length
#define MIPS_EJTAG_IMPCODE_LENGTH            32                         // IMPCODE length
#define MIPS_EJTAG_CONTROL_LENGTH            32                         // CONTROL length
#define MIPS_EJTAG_ADDRESS_LENGTH            32                         // ADDRESS length (default value, may differ for some MIPS)
#define MIPS_EJTAG_DATA_LENGTH               32                         // DATA length (default value, may differ for some MIPS)
#define MIPS_EJTAG_BYPASS_LENGTH             1                          // BYPASS length
#define MIPS_EJTAG_TCBCONTROLA_LENGTH        32                         // TCBCONTROLA length
#define MIPS_EJTAG_TCBCONTROLB_LENGTH        32                         // TCBCONTROLB length
#define MIPS_EJTAG_TCBDATA_LENGTH            32                         // TCBDATA length
#define MIPS_EJTAG_EJWATCH_LENGTH            8                          // EJWATCH length

// EJTAG version
#define EJTAG_VERSION_20                     0                          // EJTAG version 1.0 and 2.0 (older than 2.0)
#define EJTAG_VERSION_25                     1                          // EJTAG version 2.5
#define EJTAG_VERSION_26                     2                          // EJTAG version 2.6
#define EJTAG_VERSION_31                     3                          // EJTAG version 3.1

// MIPS register numbering
#define REG_START_OF_MIPS_GP                 0x0                        // all MIPS general purpose registers should be sequential from here
#define REG_MIPS_ZERO                        0x0                        // common MIPS general registers (core registers)
#define REG_MIPS_AT                          0x1
#define REG_MIPS_V0                          0x2
#define REG_MIPS_V1                          0x3
#define REG_MIPS_A0                          0x4
#define REG_MIPS_A1                          0x5
#define REG_MIPS_A2                          0x6
#define REG_MIPS_A3                          0x7
#define REG_MIPS_T0                          0x8
#define REG_MIPS_T1                          0x9
#define REG_MIPS_T2                          0xA
#define REG_MIPS_T3                          0xB
#define REG_MIPS_T4                          0xC
#define REG_MIPS_T5                          0xD
#define REG_MIPS_T6                          0xE
#define REG_MIPS_T7                          0xF
#define REG_MIPS_S0                          0x10
#define REG_MIPS_S1                          0x11
#define REG_MIPS_S2                          0x12
#define REG_MIPS_S3                          0x13
#define REG_MIPS_S4                          0x14
#define REG_MIPS_S5                          0x15
#define REG_MIPS_S6                          0x16
#define REG_MIPS_S7                          0x17
#define REG_MIPS_T8                          0x18
#define REG_MIPS_T9                          0x19
#define REG_MIPS_K0                          0x1A
#define REG_MIPS_K1                          0x1B
#define REG_MIPS_GP                          0x1C
#define REG_MIPS_SP                          0x1D
#define REG_MIPS_FP                          0x1E
#define REG_MIPS_RA                          0x1F
#define REG_START_OF_MIPS_SHADOW_REGISTERS   0x20                       // all MIPS general purpose shadow registers should be sequential from here
#define REG_MIPS_SRS1_ZERO                   0x20                       // MIPS shadow register set 1
#define REG_MIPS_SRS1_AT                     0x21  
#define REG_MIPS_SRS1_V0                     0x22
#define REG_MIPS_SRS1_V1                     0x23
#define REG_MIPS_SRS1_A0                     0x24
#define REG_MIPS_SRS1_A1                     0x25
#define REG_MIPS_SRS1_A2                     0x26
#define REG_MIPS_SRS1_A3                     0x27
#define REG_MIPS_SRS1_T0                     0x28
#define REG_MIPS_SRS1_T1                     0x29
#define REG_MIPS_SRS1_T2                     0x2A
#define REG_MIPS_SRS1_T3                     0x2B
#define REG_MIPS_SRS1_T4                     0x2C
#define REG_MIPS_SRS1_T5                     0x2D
#define REG_MIPS_SRS1_T6                     0x2E
#define REG_MIPS_SRS1_T7                     0x2F
#define REG_MIPS_SRS1_S0                     0x30
#define REG_MIPS_SRS1_S1                     0x31
#define REG_MIPS_SRS1_S2                     0x32
#define REG_MIPS_SRS1_S3                     0x33
#define REG_MIPS_SRS1_S4                     0x34
#define REG_MIPS_SRS1_S5                     0x35
#define REG_MIPS_SRS1_S6                     0x36
#define REG_MIPS_SRS1_S7                     0x37
#define REG_MIPS_SRS1_T8                     0x38
#define REG_MIPS_SRS1_T9                     0x39
#define REG_MIPS_SRS1_K0                     0x3A
#define REG_MIPS_SRS1_K1                     0x3B
#define REG_MIPS_SRS1_GP                     0x3C
#define REG_MIPS_SRS1_SP                     0x3D
#define REG_MIPS_SRS1_FP                     0x3E
#define REG_MIPS_SRS1_RA                     0x3F
#define REG_MIPS_SRS2_ZERO                   0x40                       // MIPS shadow register set 2
#define REG_MIPS_SRS2_AT                     0x41
#define REG_MIPS_SRS2_V0                     0x42
#define REG_MIPS_SRS2_V1                     0x43
#define REG_MIPS_SRS2_A0                     0x44
#define REG_MIPS_SRS2_A1                     0x45
#define REG_MIPS_SRS2_A2                     0x46
#define REG_MIPS_SRS2_A3                     0x47
#define REG_MIPS_SRS2_T0                     0x48
#define REG_MIPS_SRS2_T1                     0x49
#define REG_MIPS_SRS2_T2                     0x4A
#define REG_MIPS_SRS2_T3                     0x4B
#define REG_MIPS_SRS2_T4                     0x4C
#define REG_MIPS_SRS2_T5                     0x4D
#define REG_MIPS_SRS2_T6                     0x4E
#define REG_MIPS_SRS2_T7                     0x4F
#define REG_MIPS_SRS2_S0                     0x50
#define REG_MIPS_SRS2_S1                     0x51
#define REG_MIPS_SRS2_S2                     0x52
#define REG_MIPS_SRS2_S3                     0x53
#define REG_MIPS_SRS2_S4                     0x54
#define REG_MIPS_SRS2_S5                     0x55
#define REG_MIPS_SRS2_S6                     0x56
#define REG_MIPS_SRS2_S7                     0x57
#define REG_MIPS_SRS2_T8                     0x58
#define REG_MIPS_SRS2_T9                     0x59
#define REG_MIPS_SRS2_K0                     0x5A
#define REG_MIPS_SRS2_K1                     0x5B
#define REG_MIPS_SRS2_GP                     0x5C
#define REG_MIPS_SRS2_SP                     0x5D
#define REG_MIPS_SRS2_FP                     0x5E
#define REG_MIPS_SRS2_RA                     0x5F
#define REG_MIPS_SRS3_ZERO                   0x60                       // MIPS shadow register set 3
#define REG_MIPS_SRS3_AT                     0x61
#define REG_MIPS_SRS3_V0                     0x62
#define REG_MIPS_SRS3_V1                     0x63
#define REG_MIPS_SRS3_A0                     0x64
#define REG_MIPS_SRS3_A1                     0x65
#define REG_MIPS_SRS3_A2                     0x66
#define REG_MIPS_SRS3_A3                     0x67
#define REG_MIPS_SRS3_T0                     0x68
#define REG_MIPS_SRS3_T1                     0x69
#define REG_MIPS_SRS3_T2                     0x6A
#define REG_MIPS_SRS3_T3                     0x6B
#define REG_MIPS_SRS3_T4                     0x6C
#define REG_MIPS_SRS3_T5                     0x6D
#define REG_MIPS_SRS3_T6                     0x6E
#define REG_MIPS_SRS3_T7                     0x6F
#define REG_MIPS_SRS3_S0                     0x70
#define REG_MIPS_SRS3_S1                     0x71
#define REG_MIPS_SRS3_S2                     0x72
#define REG_MIPS_SRS3_S3                     0x73
#define REG_MIPS_SRS3_S4                     0x74
#define REG_MIPS_SRS3_S5                     0x75
#define REG_MIPS_SRS3_S6                     0x76
#define REG_MIPS_SRS3_S7                     0x77
#define REG_MIPS_SRS3_T8                     0x78
#define REG_MIPS_SRS3_T9                     0x79
#define REG_MIPS_SRS3_K0                     0x7A
#define REG_MIPS_SRS3_K1                     0x7B
#define REG_MIPS_SRS3_GP                     0x7C
#define REG_MIPS_SRS3_SP                     0x7D
#define REG_MIPS_SRS3_FP                     0x7E
#define REG_MIPS_SRS3_RA                     0x7F
#define REG_END_OF_MIPS_SHADOW_REGISTERS     0x7F                       // end of MIPS general purpose shadow registers

#define REG_MIPS_PC                          0x80
#define UNUSED_REG                           0x81                       // start of unused register numbering

#define REG_START_OF_MIPS_CP                 (UNUSED_REG)               // all MIPS co-processor registers should be here
#define REG_MIPS_C0_R0                       (UNUSED_REG)               // CP0 registers
#define REG_MIPS_C0_R1                       (UNUSED_REG+0x1)
#define REG_MIPS_C0_R2                       (UNUSED_REG+0x2)
#define REG_MIPS_C0_R3                       (UNUSED_REG+0x3)
#define REG_MIPS_C0_R4                       (UNUSED_REG+0x4)
#define REG_MIPS_C0_R5                       (UNUSED_REG+0x5)
#define REG_MIPS_C0_R6                       (UNUSED_REG+0x6)
#define REG_MIPS_C0_R7                       (UNUSED_REG+0x7)
#define REG_MIPS_C0_R8                       (UNUSED_REG+0x8)
#define REG_MIPS_C0_R9                       (UNUSED_REG+0x9)
#define REG_MIPS_C0_R10                      (UNUSED_REG+0xA)
#define REG_MIPS_C0_R11                      (UNUSED_REG+0xB)
#define REG_MIPS_C0_R12                      (UNUSED_REG+0xC)
#define REG_MIPS_C0_R13                      (UNUSED_REG+0xD)
#define REG_MIPS_C0_R14                      (UNUSED_REG+0xE)
#define REG_MIPS_C0_R15                      (UNUSED_REG+0xF)
#define REG_MIPS_C0_R16                      (UNUSED_REG+0x10)
#define REG_MIPS_C0_R17                      (UNUSED_REG+0x11)
#define REG_MIPS_C0_R18                      (UNUSED_REG+0x12)
#define REG_MIPS_C0_R19                      (UNUSED_REG+0x13)
#define REG_MIPS_C0_R20                      (UNUSED_REG+0x14)
#define REG_MIPS_C0_R21                      (UNUSED_REG+0x15)
#define REG_MIPS_C0_R22                      (UNUSED_REG+0x16)
#define REG_MIPS_C0_R23                      (UNUSED_REG+0x17)
#define REG_MIPS_C0_R24                      (UNUSED_REG+0x18)
#define REG_MIPS_C0_R25                      (UNUSED_REG+0x19)
#define REG_MIPS_C0_R26                      (UNUSED_REG+0x1A)
#define REG_MIPS_C0_R27                      (UNUSED_REG+0x1B)
#define REG_MIPS_C0_R28                      (UNUSED_REG+0x1C)
#define REG_MIPS_C0_R29                      (UNUSED_REG+0x1D)
#define REG_MIPS_C0_R30                      (UNUSED_REG+0x1E)
#define REG_MIPS_C0_R31                      (UNUSED_REG+0x1F)

#define REG_START_OF_MIPS_SPECIAL            (UNUSED_REG+0x20)          // all special MIPS registers should start here
#define REG_MIPS_IDCODE                      (UNUSED_REG+0x20)
#define REG_MIPS_IMPCODE                     (UNUSED_REG+0x21)
#define REG_MIPS_CONTROL                     (UNUSED_REG+0x22)
#define REG_MIPS_DEBUG                       (UNUSED_REG+0x23)
#define REG_MIPS_DCR                         (UNUSED_REG+0x24)

// MIPS hardware breakpoint registers
#define REG_MIPS_IBS                         (UNUSED_REG+0x25)
#define REG_MIPS_IBA0                        (UNUSED_REG+0x26)
#define REG_MIPS_IBM0                        (UNUSED_REG+0x27)
#define REG_MIPS_IBASID0                     (UNUSED_REG+0x28)
#define REG_MIPS_IBC0                        (UNUSED_REG+0x29)
#define REG_MIPS_IBA1                        (UNUSED_REG+0x2A)
#define REG_MIPS_IBM1                        (UNUSED_REG+0x2B)
#define REG_MIPS_IBASID1                     (UNUSED_REG+0x2C)
#define REG_MIPS_IBC1                        (UNUSED_REG+0x2D)
#define REG_MIPS_IBA2                        (UNUSED_REG+0x2E)
#define REG_MIPS_IBM2                        (UNUSED_REG+0x2F)
#define REG_MIPS_IBASID2                     (UNUSED_REG+0x30)
#define REG_MIPS_IBC2                        (UNUSED_REG+0x31)
#define REG_MIPS_IBA3                        (UNUSED_REG+0x32)
#define REG_MIPS_IBM3                        (UNUSED_REG+0x33)
#define REG_MIPS_IBASID3                     (UNUSED_REG+0x34)
#define REG_MIPS_IBC3                        (UNUSED_REG+0x35)
// MIPS data hardware breakpoint registers
#define REG_MIPS_DBS                         (UNUSED_REG+0x36)
#define REG_MIPS_DBA0                        (UNUSED_REG+0x37)
#define REG_MIPS_DBM0                        (UNUSED_REG+0x38)
#define REG_MIPS_DBASID0                     (UNUSED_REG+0x39)
#define REG_MIPS_DBC0                        (UNUSED_REG+0x3A)
#define REG_MIPS_DBV0                        (UNUSED_REG+0x3B)
#define REG_MIPS_DBA1                        (UNUSED_REG+0x3C)
#define REG_MIPS_DBM1                        (UNUSED_REG+0x3D)
#define REG_MIPS_DBASID1                     (UNUSED_REG+0x3E)
#define REG_MIPS_DBC1                        (UNUSED_REG+0x3F)
#define REG_MIPS_DBV1                        (UNUSED_REG+0x40)
#define REG_MIPS_DBA2                        (UNUSED_REG+0x41)
#define REG_MIPS_DBM2                        (UNUSED_REG+0x42)
#define REG_MIPS_DBASID2                     (UNUSED_REG+0x43)
#define REG_MIPS_DBC2                        (UNUSED_REG+0x44)
#define REG_MIPS_DBV2                        (UNUSED_REG+0x45)
#define REG_MIPS_DBA3                        (UNUSED_REG+0x46)
#define REG_MIPS_DBM3                        (UNUSED_REG+0x47)
#define REG_MIPS_DBASID3                     (UNUSED_REG+0x48)
#define REG_MIPS_DBC3                        (UNUSED_REG+0x49)
#define REG_MIPS_DBV3                        (UNUSED_REG+0x4A)
// MIPS CP0 registers
#define REG_START_OF_MIPS_CP_S1              (UNUSED_REG+0x4B)          // all MIPS CP0 0 registers, select 1 should be here
#define REG_MIPS_C0S1_R0                     (UNUSED_REG+0x4B)
#define REG_MIPS_C0S1_R1                     (UNUSED_REG+0x4C) 
#define REG_MIPS_C0S1_R2                     (UNUSED_REG+0x4D)
#define REG_MIPS_C0S1_R3                     (UNUSED_REG+0x4E)
#define REG_MIPS_C0S1_R4                     (UNUSED_REG+0x4F)
#define REG_MIPS_C0S1_R5                     (UNUSED_REG+0x50)
#define REG_MIPS_C0S1_R6                     (UNUSED_REG+0x51)
#define REG_MIPS_C0S1_R7                     (UNUSED_REG+0x52)
#define REG_MIPS_C0S1_R8                     (UNUSED_REG+0x53)
#define REG_MIPS_C0S1_R9                     (UNUSED_REG+0x54)
#define REG_MIPS_C0S1_R10                    (UNUSED_REG+0x55)
#define REG_MIPS_C0S1_R11                    (UNUSED_REG+0x56)
#define REG_MIPS_C0S1_R12                    (UNUSED_REG+0x57)
#define REG_MIPS_C0S1_R13                    (UNUSED_REG+0x58)
#define REG_MIPS_C0S1_R14                    (UNUSED_REG+0x59)
#define REG_MIPS_C0S1_R15                    (UNUSED_REG+0x5A)
#define REG_MIPS_C0S1_R16                    (UNUSED_REG+0x5B)
#define REG_MIPS_C0S1_R17                    (UNUSED_REG+0x5C)
#define REG_MIPS_C0S1_R18                    (UNUSED_REG+0x5D)
#define REG_MIPS_C0S1_R19                    (UNUSED_REG+0x5E)
#define REG_MIPS_C0S1_R20                    (UNUSED_REG+0x5F)
#define REG_MIPS_C0S1_R21                    (UNUSED_REG+0x60)
#define REG_MIPS_C0S1_R22                    (UNUSED_REG+0x61)
#define REG_MIPS_C0S1_R23                    (UNUSED_REG+0x62)
#define REG_MIPS_C0S1_R24                    (UNUSED_REG+0x63)
#define REG_MIPS_C0S1_R25                    (UNUSED_REG+0x64)
#define REG_MIPS_C0S1_R26                    (UNUSED_REG+0x65)
#define REG_MIPS_C0S1_R27                    (UNUSED_REG+0x66)
#define REG_MIPS_C0S1_R28                    (UNUSED_REG+0x67)
#define REG_MIPS_C0S1_R29                    (UNUSED_REG+0x68)
#define REG_MIPS_C0S1_R30                    (UNUSED_REG+0x69)
#define REG_MIPS_C0S1_R31                    (UNUSED_REG+0x6A)
  
#define REG_MIPS_HI                          (UNUSED_REG+0x6B)
#define REG_MIPS_LO                          (UNUSED_REG+0x6C)

#define REG_MIPS_EXECUTION_CTRL              (UNUSED_REG+0x6D)
// MIPS TCB registers
#define REG_MIPS_TCBCONTROLA                 (UNUSED_REG+0x6E)
#define REG_MIPS_TCBCONTROLB                 (UNUSED_REG+0x6F)
#define REG_MIPS_TCBDATA                     (UNUSED_REG+0x70)
#define REG_MIPS_TCBCONFIG                   (UNUSED_REG+0x71)
#define REG_MIPS_TCBTW                       (UNUSED_REG+0x72)
#define REG_MIPS_TCBRDP                      (UNUSED_REG+0x73)
#define REG_MIPS_TCBWRP                      (UNUSED_REG+0x74)
#define REG_MIPS_TCBSTP                      (UNUSED_REG+0x75)
#define REG_MIPS_TCBTRIG0                    (UNUSED_REG+0x76)
#define REG_MIPS_TCBTRIG1                    (UNUSED_REG+0x77)
#define REG_MIPS_TCBTRIG2                    (UNUSED_REG+0x78)
#define REG_MIPS_TCBTRIG3                    (UNUSED_REG+0x79)
#define REG_MIPS_TCBTRIG4                    (UNUSED_REG+0x7A)
#define REG_MIPS_TCBTRIG5                    (UNUSED_REG+0x7B)
#define REG_MIPS_TCBTRIG6                    (UNUSED_REG+0x7C)
#define REG_MIPS_TCBTRIG7                    (UNUSED_REG+0x7D)

#define REG_MIPS_CACHE_FLUSH_RTN_ADDR        (UNUSED_REG+0x7E)
// MIPS DW diagnostic registers (for internal use)
#define REG_MIPS_DW_DIAG0                    (UNUSED_REG+0x7F)
#define REG_MIPS_DW_DIAG1                    (UNUSED_REG+0x80)
#define REG_MIPS_DW_DIAG2                    (UNUSED_REG+0x81)
#define REG_MIPS_DW_DIAG3                    (UNUSED_REG+0x82)
#define REG_MIPS_DW_DIAG4                    (UNUSED_REG+0x83)
#define REG_MIPS_DW_DIAG5                    (UNUSED_REG+0x84)
#define REG_MIPS_DW_DIAG6                    (UNUSED_REG+0x85)
#define REG_MIPS_DW_DIAG7                    (UNUSED_REG+0x86)
#define REG_MIPS_DW_DIAG8                    (UNUSED_REG+0x87)
#define REG_MIPS_DW_DIAG9                    (UNUSED_REG+0x88)
#define REG_MIPS_DW_DIAGA                    (UNUSED_REG+0x89)
#define REG_MIPS_DW_DIAGB                    (UNUSED_REG+0x8A)
#define REG_MIPS_DW_DIAGC                    (UNUSED_REG+0x8B)
#define REG_MIPS_DW_DIAGD                    (UNUSED_REG+0x8C)
#define REG_MIPS_DW_DIAGE                    (UNUSED_REG+0x8D)
#define REG_MIPS_DW_DIAGF                    (UNUSED_REG+0x8E)

#define REG_MIPS_SW_BP_WRITEBACK             (UNUSED_REG+0x8F)
#define REG_MIPS_EJTAGDATA                   (UNUSED_REG+0x90)
#define REG_MIPS_EJTAGADDR                   (UNUSED_REG+0x91)
#define REG_MIPS_BYPASS                      (UNUSED_REG+0x92)
#define REG_MIPS_MIPS16E_ENABLED             (UNUSED_REG+0x93)    

#define REG_MIPS_INVALIDATE_CACHE_NOW        (UNUSED_REG+0x94)
#define REG_MIPS_WRITE_ENABLE_REG            (UNUSED_REG+0x95)
#define REG_MIPS_WRITE_ENABLE_REFRESH_REG    (UNUSED_REG+0x96)
#define REG_MIPS_ENABLE_CODECOVERAGE         (UNUSED_REG+0x97)
#define REG_MIPS_CLEAR_CODECOVERAGE          (UNUSED_REG+0x98)

// MIPS CP0 registers
#define REG_NUM_STATUS                       12                         // CP0 number for status
#define REG_NUM_ERRCTRL                      26                         // CP0 number for debug
#define REG_NUM_TRACECONTROL                 0x37                       // CP0 reg 23, select 1
#define REG_NUM_TRACECONTROL2                0x57                       // CP0 reg 23, select 2
#define REG_NUM_TRACEBPC                     0x97                       // CP0 reg 23, select 3

// MIPS Zephyr Trace Conf registers
//TODO: Need to verify the numbers
#define REG_NUM_ZEPHYR_TRACECONTROL          0x37                       // CP0 reg 23, select 1
#define REG_NUM_ZEPHYR_TRACECONTROL2         0x57                       // CP0 reg 23, select 2
#define REG_NUM_ZEPHYR_TRACECONTROL3         0x77                       // CP0 reg 23, select 3


// MIPS instruction codes
#define MIPS_INST_MTC0_T0_1                  0x40880800                 // opcode for mtc0 $t0,$1
#define MIPS_INST_MFC0_T0_1                  0x40080800                 // opcode for mfc0 $t0,$1
#define MIPS_INST_MTLO_T0                    0x01000013                 // opcode for mtlo $t0
#define MIPS_INST_MTHI_T0                    0x01000011                 // opcode for mthi $t0
#define MIPS_INST_MFLO_T0                    0x00004012                 // opcode for mflo $t0
#define MIPS_INST_MFHI_T0                    0x00004010                 // opcode for mfhi $t0
#define MIPS_INST_SDBBP                      0x7000003F                 // opcode for 32-bit software breakpoint
#define MIPS_INST_SDBBP16                    0xE801                     // opcode for 16-bit software breakpoint
// MIPS DSU defines (HW breakpoints)
// EJTAG version 2.5 and newer
#define DRSEG_OFFSET_EJ25_IBS                0x00001000
#define DRSEG_OFFSET_EJ25_IBA(x)             (0x00001100 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_IBM(x)             (0x00001108 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_IBASID(x)          (0x00001110 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_IBC(x)             (0x00001118 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_DBS                0x00002000
#define DRSEG_OFFSET_EJ25_DBA(x)             (0x00002100 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_DBM(x)             (0x00002108 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_DBASID(x)          (0x00002110 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_DBC(x)             (0x00002118 + ((x) * 0x00000100))
#define DRSEG_OFFSET_EJ25_DBV(x)             (0x00002120 + ((x) * 0x00000100))
// EJTAG version 2.0 and older
#define DRSEG_OFFSET_EJ20_IBS                0x00000004
#define DRSEG_OFFSET_EJ20_IBA(x)             (0x00000100 + ((x) * 0x00000010))
#define DRSEG_OFFSET_EJ20_IBC(x)             (0x00000104 + ((x) * 0x00000010))
#define DRSEG_OFFSET_EJ20_IBM(x)             (0x00000108 + ((x) * 0x00000010))
#define DRSEG_OFFSET_EJ20_DBS                0x00000008
#define DRSEG_OFFSET_EJ20_DBA(x)             (0x00000200 + ((x) * 0x00000010))
#define DRSEG_OFFSET_EJ20_DBC(x)             (0x00000204 + ((x) * 0x00000010))
#define DRSEG_OFFSET_EJ20_DBM(x)             (0x00000208 + ((x) * 0x00000010))
#define DRSEG_OFFSET_EJ20_DB(x)              (0x0000020C + ((x) * 0x00000010))
// general constants
#define BIT_0                                0x00000001
#define BIT_1                                0x00000002
#define BIT_2                                0x00000004
#define BIT_3                                0x00000008
#define BIT_4                                0x00000010
#define BIT_5                                0x00000020
#define BIT_6                                0x00000040
#define BIT_7                                0x00000080
#define BIT_8                                0x00000100
#define BIT_9                                0x00000200
#define BIT_10                               0x00000400
#define BIT_11                               0x00000800
#define BIT_12                               0x00001000
#define BIT_13                               0x00002000
#define BIT_14                               0x00004000
#define BIT_15                               0x00008000
#define BIT_16                               0x00010000
#define BIT_17                               0x00020000
#define BIT_18                               0x00040000
#define BIT_19                               0x00080000
#define BIT_20                               0x00100000
#define BIT_21                               0x00200000
#define BIT_22                               0x00400000
#define BIT_23                               0x00800000
#define BIT_24                               0x01000000
#define BIT_25                               0x02000000
#define BIT_26                               0x04000000
#define BIT_27                               0x08000000
#define BIT_28                               0x10000000
#define BIT_29                               0x20000000
#define BIT_30                               0x40000000
#define BIT_31                               0x80000000

#endif   // MIPS_H_

