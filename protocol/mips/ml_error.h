/****************************************************************************
       Module: ml_error.h
     Engineer: Vitezslav Hola
  Description: Header with error constants for MIPS MDI midlayer.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#ifndef ML_ERROR_H_
#define ML_ERROR_H_

//#define ML_ERR_NO_ERROR                      0                    // no error occured
#define ML_ERR_INSUFFICIENT_MEMORY           1000                 // cannot allocate memory for dynamic structures



// midlayer errors related to MIPS processor state or access
//#define ML_ERR_UNALLIGNED_MEMORY_ACCESS      8000                 // unalligned memory access attempt
//#define ML_ERR_PROCESSOR_NOT_IN_DEBUG_MODE   8001                 // cannot do operation because processor is not in debug mode
//#define ML_ERR_INVALID_REGISTER_READ         8002                 // invalid register identifier
//#define ML_ERR_INVALID_REGISTER_WRITE        8003                 // invalid register identifier
//#define ML_ERR_INVALID_MEMORY_LENGTH         8004                 // invalid memory length (too many bytes)
//#define ML_ERR_DMA_ACCESS_NOT_SUPPORTED      8005                 // target does not support DMA core
//#define ML_ERR_DMA_OUTSIDE_VALID_RANGE       8006                 // access to DMA core outside valid range
//#define ML_ERR_MEM_ACCESS_WHILE_RUNNING      8007                 // memory access attempt while processor is running
//#define ML_ERR_MEM_ACCESS_OVER_CACHEROUTINE  8008                 // memory access collidates with cache routine in memory
#define ML_ERR_INSUFFICIENT_STRUCTURE_SIZE   8009                 // insufficent size in local structures
                                                                  
// midlayer errors related to DW2 application and SREC files
//#define ML_ERR_INVALID_DW2_APP               10000                // invalid or too large DW2 application to handle
//#define ML_ERR_MISSING_SREC_FILE             10001                // some of SREC file is missing or cannot be open
//#define ML_ERR_INVALID_SREC_FILE             10002                // invalid SREC file occured when parsing it

// midlayer errors related to breakpoints
//#define ML_ERR_SET_INVALID_BP                12000                // invalid type of breakpoint to set
//#define ML_ERR_SET_NO_BP                     12001                // trying to set no breakpoint
//#define ML_ERR_SET_BP_WHILE_RUNNING          12002                // trying to set breakpoint when processor is executing
//#define ML_ERR_SET_BP_FAILED                 12003                // failed to set breakpoint
//#define ML_ERR_CLEAR_BP_WHILE_RUNNING        12004                // trying to clear breakpoint when processor is executing
//#define ML_ERR_CLEAR_BP_FAILED               12005                // failed to clear breakpoint
//#define ML_ERR_NO_HW_BP_LEFT                 12006                // no HW breakpoint left (all in use)

// midlayer errors related to breakpoints
//#define ML_ERR_UNKNOWN_CACHE_ERROR           13000                //unspecified cache error
// unknown error
#define ML_ERR_UNKNOWN_ERROR                 65535                // unspecified error

#endif   // ML_ERROR_H_
