/****************************************************************************
       Module: loadsrec.h
     Engineer: Vitezslav Hola
  Description: Header for loading SREC files.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#ifndef LOADSREC_H_
#define LOADSREC_H_

#define MAX_SYMBOL_NAME 40

typedef struct
{
   unsigned long        ulSymbolAddress;     // Left as Struct in case more members are needed
} TyMIPSSymbol;                                  // Used to store Symbol Identifier Versus Address

typedef struct
{
   unsigned long ulStartAddress;
   unsigned long ulEndAddress;
   unsigned char pucDataRecord[MAX_APPLICATION_SIZE]; // Max Application size = 1KB
   int bRecordValid;
   TyMIPSSymbol tySymbolArray[MAGIC_LAST_ENTRY];
} TyApplicationRecord;

typedef struct
{
   TySymbolIdentifier   tySymbolIdentifier;
   char        szSymbolName[MAX_SYMBOL_NAME];
} TySymbolLookup;                     // Used by LoadSrec to find the Symbol Identifier from the Name.

class CLoadSrecFile 
{
public:

   CLoadSrecFile();
   TyError ProcessSRecFile(char           *pszFilename, 
						   unsigned long  Rel_Offset, 
						   unsigned long  CodeMemStart, 
						   unsigned long  CodeMemEnd,
						   unsigned long  ulNewLocation,
                           unsigned long  **pulApp,
                           unsigned long  *pulAppSize,
                           unsigned long  *pulAppStartAddress,
                           unsigned long  *pulInstrOffset,
                           TySymbolIdentifier tySimbol);
   TyError BufferedWrite(unsigned long ulStartAddress,
						 unsigned long ulLength,
                         unsigned char *pucData);
   TyError InitialiseWriteBuffer(void);
   TyError ProcessWriteBuffer(unsigned long  ulNewLocation,
                              unsigned long  **pulApp,
                              unsigned long  *pulAppSize,
                              unsigned long  *pulAppStartAddress);
   TyError ProcessSymbolBuffer(TySymbolIdentifier tySymbol,
                               unsigned long  *pulInstrOffset);
   TyError UpdateDW2SymbolRecord(char           *pszSymbolName,
                                 unsigned long  ulSymbolAddress);
protected:

   TyError Initialise(int argc, char *argv);
   unsigned char AsciiHexToUbyte(char cHexVal);

private:
 
   #define  SRECINFILE_EXT                 ".SRE"
   #define  MAX_CHARS_IN_SREC_RECORD       255
   #define  MAX_DATA_BYTES_IN_DATA_RECORD  255

   // S-Record Types supported
   #define  S_Record_TYPE_S0               0x00
   #define  S_Record_TYPE_S1               0x01
   #define  S_Record_TYPE_S2               0x02
   #define  S_Record_TYPE_S3               0x03
   #define  S_Record_TYPE_S4               0x04
   #define  S_Record_TYPE_S5               0x05
   #define  S_Record_TYPE_S7               0x07
   #define  S_Record_TYPE_S8               0x08
   #define  S_Record_TYPE_S9               0x09

   char   szModuleName[_MAX_PATH];
   char   szInFileName[_MAX_PATH];

   FILE           *pInFile; 
   int            bRelocate; 
   signed long    slRelocation; 

   TyApplicationRecord ptyApplicationArray[1];


};


#endif   // LOADSREC_H_
