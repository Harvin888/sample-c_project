/****************************************************************************
       Module: opxdjapi.h
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
  Description: Exported header for Ashling Opella-XD JTAG API library
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
#ifndef OPXDJAPI_H_
#define OPXDJAPI_H_

// Opella-XD JTAG API library name and version
#define OPXD_JTAGAPI_NAME              "Ashling Opella-XD JTAG API"  // library name
#define OPXD_JTAGAPI_VERSION           "11-Jan-2007 v0.0.1"          // library version

// Opella-XD JTAG API function names
#define OPXD_JTAGINIT_FUNC             "OPXD_JTAGInit"               // Initialize connection with Opella-XD and obtain handle
#define OPXD_JTAGCLOSE_FUNC            "OPXD_JTAGClose"              // Close the connection
#define OPXD_LISTPROBES_FUNC           "OPXD_ListProbes"             // List all Opella-XD currently connected to PC
#define OPXD_GETLIBRARYINFO_FUNC       "OPXD_GetLibraryInfo"         // Obtain information about Opella-XD JTAG API library (version, name)
#define OPXD_GETPROBEINFO_FUNC         "OPXD_GetProbeInfo"           // Obtain information about Opella-XD (firmware, diskware, etc.)
#define OPXD_GETTAPSTATE_FUNC          "OPXD_GetTAPState"            // Get current state of JTAG TAP state machine
#define OPXD_JTAGPULSES_FUNC           "OPXD_JTAGPulses"             // Send defined patterns for JTAG signals (TDI, TMS, etc.)
#define OPXD_GETJTAGPINS_FUNC          "OPXD_GetJTAGPins"            // Get current status of JTAG pins
#define OPXD_TAPRESET_FUNC             "OPXD_TAPReset"               // Reset JTAG TAP state machine
#define OPXD_TARGETRESET_FUNC          "OPXD_TargetReset"            // Control target reset pin (nRST or nSRST)
#define OPXD_JTAGSCAN_FUNC             "OPXD_JTAGScan"               // Single access to JTAG register (IR or DR)
#define OPXD_JTAGMULTIPLESCAN_FUNC     "OPXD_JTAGMultipleScan"       // Multiple accesses to JTAG registers (IR/DR)
#define OPXD_SETJTAGFREQUENCY_FUNC     "OPXD_SetJTAGFrequency"       // Select JTAG frequency for Opella-XD
#define OPXD_SETRTCKTIMEOUT_FUNC       "OPXD_SetRTCKTimeout"         // Set timeout for adaptive frequency
#define OPXD_SETTARGETVOLTAGE_FUNC     "OPXD_SetTargetVoltage"       // Select target voltage for TPA cable
#define OPXD_TOGGLETARGETSTATUS_FUNC   "OPXD_ToggleTargetStatus"     // Turn red Opella-XD "Target Status" LED for a while

// callback function type
typedef void (*PFCallback)(unsigned int uiProgress);

// states for JTAG TAP state machine
typedef enum { 
  TAP_TLR,                                               // Test-Logic-Reset
  TAP_RTI,					                             // Run-Test/Idle
  TAP_SELECT_DR,                                         // Select-DR Scan
  TAP_CAPTURE_DR,                                        // Capture-DR
  TAP_SHIFT_DR,                                          // Shift-DR
  TAP_EXIT1_DR,                                          // Exit1-DR
  TAP_PAUSE_DR,                                          // Pause-DR
  TAP_EXIT2_DR,                                          // Exit2-DR
  TAP_UPDATE_DR,                                         // Update-DR
  TAP_SELECT_IR,                                         // Select-IR Scan
  TAP_CAPTURE_IR,                                        // Capture-IR
  TAP_SHIFT_IR,                                          // Shift-IR
  TAP_EXIT1_IR,                                          // Exit1-IR
  TAP_PAUSE_IR,                                          // Pause-IR
  TAP_EXIT2_IR,                                          // Exit2-IR
  TAP_UPDATE_IR                                          // Update-IR
} TyJTAGState;

// General Opella-XD JTAG API function prototypes
typedef int (*PFuncOPXD_JTAGInit)(const char *pszProbeInstance, int *piHandle, PFCallback pfCallback,
                                  unsigned int uiFrequencyHz, unsigned short usTargetVoltage);        // OPXD_JTAGInit prototype
typedef int (*PFuncOPXD_JTAGClose)(int iHandle);                                                      // OPXD_JTAGClose prototype
typedef int (*PFuncOPXD_ListProbes)(unsigned int uiMaxInstances, char ppszProbeInstances[][16],
                                    unsigned int *puiConnectedProbes);                                // OPXD_ListProbes prototype
typedef int (*PFuncOPXD_GetLibraryInfo)(char *pszName, char *pszVersion, unsigned int uiSize);        // OPXD_GetLibraryInfo prototype
typedef int (*PFuncOPXD_GetProbeInfo)(int iHandle, char *pszFirmware, char *pszDiskware, 
                                      char *pszFpgaware, unsigned int uiSize);                        // OPXD_GetProbeInfo prototype
// Low-level Opella-XD JTAG API function prototypes
typedef TyJTAGState (*PFuncOPXD_GetTAPState)(int iHandle);                                            // OPXD_GetTAPState prototype
typedef int (*PFuncOPXD_JTAGPulses)(int iHandle, unsigned int uiNumberOfPulses, unsigned int *puiTDI,
                                    unsigned int *puiTMS, unsigned int *puiTDO);                      // OPXD_JTAGPulses prototype
typedef int (*PFuncOPXD_GetJTAGPins)(int iHandle, unsigned char *pucTDI, unsigned char *pucTDO,
                                     unsigned char *pucTMS, unsigned char *pucTCK);                   // OPXD_GetJTAGPins prototype
typedef int (*PFuncOPXD_TAPReset)(int iHandle, unsigned char bUseTRST);                               // OPXD_TAPReset
typedef int (*PFuncOPXD_TargetReset)(int iHandle, unsigned char bAssertReset);                        // OPXD_TargetReset
// High-level Opella-XD JTAG API function prototypes
typedef int (*PFuncOPXD_JTAGScan)(int iHandle, unsigned char usScanAttributes, unsigned int uiLength,
                                  unsigned int *puiDataOut, unsigned int *puiDataIn);                 // OPXD_JTAGScan
typedef int (*PFuncOPXD_JTAGMultipleScan)(int iHandle, unsigned int uiNumberOfScans, unsigned char *pusScanAttributes, unsigned int *puiLength, 
                                          unsigned int **ppuiDataOut, unsigned int **ppuiDataIn);     // OPXD_JTAGMultipleScan
// Miscellaneous Opella-XD JTAG API function prototypes
typedef int (*PFuncOPXD_SetJTAGFrequency)(int iHandle, unsigned int uiFrequencyHz);                   // OPXD_SetJTAGFrequency
typedef int (*PFuncOPXD_SetRTCKTimeout)(int iHandle, unsigned int uiTimeoutMs);                       // OPXD_SetRTCKTimeout
typedef int (*PFuncOPXD_SetTargetVoltage)(int iHandle, unsigned short usTargetVoltage);               // OPXD_SetTargetVoltage
typedef int (*PFuncOPXD_ToggleTargetStatus)(int iHandle);                                             // OPXD_ToggleTargetStatus

// JTAG API scan attribute flags
#define SCAN_IR                           0x01           // scan JTAG IR register
#define SCAN_DR                           0x00           // scan JTAG DR register
#define SCAN_AVOID_RTI                    0x02           // avoid Run-Test/Idle state after Update-IR/DR state
#define SCAN_VIA_RTI                      0x00           // go through Run-Test/Idle state after Update-IR/DR state
#define SCAN_FINISH_PAUSE                 0x04           // finish in Pause-IR/DR state after current scan
#define SCAN_FINISH_RTI                   0x00           // finish in Run-Test/Idle state after current scan
// scan attributes are combinations of 3 flags ((SCAN_IR or SCAN_DR) | (SCAN_AVOID_RTI or SCAN_VIA_RTI) | (SCAN_FINISH_PAUSE or SCAN_FINISH_RTI))

// Opella-XD JTAG frequency contants
#define FREQUENCY_ADAPTIVE                0              // adaptive frequency for Opella-XD
#define MINIMUM_FREQUENCY                 1000           // minimum Opella-XD JTAG frequency is 1 kHz
#define MAXIMUM_FREQUENCY                 100000000      // maximum Opella-XD JTAG frequency is 100 MHz
#define MINIMUM_RTCK_TIMEOUT              1              // minimum timeout for adaptive clock (1 ms)
#define MAXIMUM_RTCK_TIMEOUT              20000          // maximum timeout for adaptive clock (20 s)

// Opella-XD target voltage constants
#define VTPA_FIXED_3V3                    3300           // 3.3V target voltage
#define VTPA_FIXED_2V5                    2500           // 2.5V target voltage
#define VTPA_FIXED_1V8                    1800           // 1.8V target voltage
#define VTPA_FIXED_1V5                    1500           // 1.5V target voltage
#define VTPA_FIXED_1V2                    1200           // 1.2V target voltage
#define VTPA_MATCH                        65535          // match target voltage with Vtref pin
#define VTPA_DISABLE                      0              // disable output JTAG signals on TPA

// Opella-XD JTAG API constants
#define MAXIMUM_NUMBER_OF_PULSES          16384          // maximum number of pulses for one payload
#define MAXIMUM_SCAN_LENGTH               16384          // maximum number of bits for single scan into JTAG register
#define MAXIMUM_NUMBER_OF_SCANS           256            // maximum number of scans for multiple scan function

// JTAG API error codes (int type)
#define ERR_NO_ERROR                      0              // no error occured in JTAG API function
#define ERR_INVALID_HANDLE                1              // invalid handle used for JTAG API function
#define ERR_INVALID_PARAMETERS            2              // invalid function parameters (NULL pointer, etc.)
#define ERR_TOO_MANY_INSTANCES_USED       3              // too many instances of Opella-XD currently used
#define ERR_PROBE_NOT_CONFIGURED          4              // probe is not configured correctly
#define ERR_FREQUENCY_OUT_OF_RANGE        5              // invalid JTAG frequency value
#define ERR_PROBE_INSTANCE_NOT_SPECIFIED  6              // probe serial number has not been specified
#define ERR_NO_PROBE_CONNECTED            7              // no Opella-XD probe connected to PC
#define ERR_CANNOT_USE_PROBE              8              // cannot use probe (open for connection)
#define ERR_CANNOT_CONFIGURE_PROBE        9              // cannot configure probe for connection
#define ERR_INVALID_TPA                   10             // invalid or missing TPA connected to Opella-XD
#define ERR_MEMORY_ALLOCATION_FAILED      11             // failed to allocate memory for internal structures
#define ERR_USB_DRIVER_ERROR              20             // USB driver error (when sending or receiving data)
#define ERR_TOO_LARGE_DATA                21             // too large data for JTAG API to handle
#define ERR_RTCK_TIMEOUT_EXPIRED          22             // timeout for RTCK expired (target does not provide RTCK)
#define ERR_INCORRECT_JTAG_TAP_STATE      100            // incorrect JTAG TAP state (i.e calling high-level function when outside RTI, TLR or Pause-IR/DR)
#define ERR_UNSPECIFIED_ERROR             1000           // unspecified error in Opella-XD JTAG API


#endif   // OPXDJAPI_H_
