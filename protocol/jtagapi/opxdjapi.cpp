/****************************************************************************
       Module: opxdjapi.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of Opella-XD JTAG API library
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
#ifndef __LINUX
// Win specific defines
#include <windows.h>
#include <stdio.h>
#include <string.h>
#else
// Linux specific defines
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#endif
#include "opxdjapi.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdarc.h"
#include "protocol/common/drvopxdarm.h"
#include "protocol/common/drvopxdmips.h"

// library defines (depending on host system)
#ifndef __LINUX
// compiling Windows DLL
#define INTERFACE_LIBRARY           "opxdjapi.dll"                   // library name for Windows (DLL)
#define DLLEXPORT                   __declspec(dllexport)
#else
// compiling Linux .so library
#define INTERFACE_LIBRARY           "opxdjapi.so"                    // library name for Linux (SO)
#define DLLEXPORT                   
#endif

#define MAX_LIBRARY_PATH                                             512
#define MAX_PROBE_INSTANCES                                          (MAX_DEVICES_SUPPORTED)
#define CONNECTION_HANDLE_INVALID                                    (MAX_PROBE_INSTANCES)

#define OPXD_FIRMWARE_FILENAME                                       "opxdfw.bin"
#define OPXD_ARC_DISKWARE_FILENAME                                   "opxddarc.bin"
#define OPXD_ARC_FPGAWARE_FILENAME                                   "opxdfarc.bin"
#define OPXD_ARM_DISKWARE_FILENAME                                   "opxddarm.bin"
#define OPXD_ARM_FPGAWARE_FILENAME                                   "opxdfarm.bin"
#define OPXD_MIPS_DISKWARE_FILENAME                                  "opxddmip.bin"
#define OPXD_MIPS_FPGAWARE_FILENAME                                  "opxdfmip.bin"

#define INFOSTR_LENGTH                    64                         // length for string for diskware, firmware and fpgaware info
#define DEFAULT_RTCK_TIMEOUT              50                         // default RTCK timeout value (50 ms)
#define DEFAULT_FREQUENCY                 1000000                    // default JTAG frequency (1 MHz)
#define DEFAULT_PULSE_PERIOD              1                          // pulse period for default frequency 1 MHz is 1 us

#define SCAN_MASK_FINISH                  0x04                       // masking finish state for scan attributes 
#define SCAN_MASK_TYPE                    0x01                       // masking scan type for scan attributes 
#define SCAN_MASK_PATH                    0x02                       // masking path for scan attributes 

#define MAX_SCAN_PAYLOAD                  (64*1024)                  // maximum multiscan payload is 64 kB

// structure for probe indentifies
typedef struct _TyProbeInstance {
   TyDevHandle tyHandle;                                             // handle to Opella-XD
   TyJTAGState tyCurrentJtagState;                                   // current state of JTAG state machine
   // Opella-XD JTAG frequency and RTCK timeout
   unsigned int uiCurrentFrequency;                                  // current JTAG frequency
   unsigned long ulCurrentPulsePeriod;                               // current pulse period
   unsigned int uiCurrentRTCKTimeout;                                // current adaptive clock timeout
   // Opella-XD TPA voltage
   unsigned short usCurrentVtpa;                                     // current TPA voltage
   // firmware, diskware and FPGAware info
   TyDiskwareType tyCurrentDiskware;                                 // diskware currently used
   TyFpgawareType tyCurrentFpgaware;                                 // FPGAware currently used
   unsigned long ulFirmwareVersion, ulFirmwareDate;                  // firmware version and date
   unsigned long ulDiskwareVersion, ulDiskwareDate;                  // firmware version and date
   unsigned long ulFpgawareVersion, ulFpgawareDate;                  // firmware version and date
   TyMultipleScanParams ptyMultipleScanParams[256];                  // array for parameters to multiple scan function
} TyProbeInstance;

// function prototypes
extern "C" DLLEXPORT int OPXD_JTAGClose(int iHandle);

// local variables
static char pszPathToLoadedLibrary[MAX_LIBRARY_PATH];
static TyProbeInstance *pptyProbeInstances[MAX_PROBE_INSTANCES];     // storing information for all instances used
// local functions
static TyJTAGState NextTAPState(TyJTAGState tyCurrentState, unsigned char ucTMS);
static int ConvertErrorCode(TyError tyErrorCode);
static void ConvertVersionAndDate(char *pszString, unsigned int uiSize, unsigned long ulVersion, unsigned long ulDate);
static void GetFullFilename(char *pszString, unsigned int uiSize, const char *pszFilename);
static int SetJtagFrequency(TyProbeInstance *ptyInstance, unsigned int uiFrequencyHz);
static int SetTargetVoltage(TyProbeInstance *ptyInstance, unsigned short usTargetVoltage);
static int PrepareScanParams(TyProbeInstance *ptyInstance, unsigned int uiCount, unsigned char *pucScanAttributes, unsigned int *puiLength, TyJTAGState *ptyFinalState);
static void MsSleep(unsigned int uiMiliseconds);

/****************************************************************************
*** Opella-XD JTAG API Library functions                                  ***
****************************************************************************/

#ifndef __LINUX
// Windows specific functions

/****************************************************************************
     Function: DllMain
     Engineer: Vitezslav Hola
        Input: HINSTANCE hInstDll - DLL handle
               DWORD dwReason - reason whay this function was called
               LPVOID lpReserved - reserved
       Output: BOOL - TRUE if everythink is ok
  Description: DLL main entry point, called when DLL is loaded/unloaded
               This function is implemented only for Windows DLL.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" BOOL APIENTRY DllMain(HINSTANCE hInstDll, DWORD dwReason, LPVOID lpReserved)
{
   lpReserved = lpReserved;         // make lint happy
   switch (dwReason)
      {
      case DLL_PROCESS_ATTACH:
         {  // loading DLL, initialize local variables
         // set all pointers to NULL, memory will be allocated later
         for (int iIndex = 0; iIndex < MAX_PROBE_INSTANCES; iIndex++)
            pptyProbeInstances[iIndex] = NULL;
         // get module DLL path
         pszPathToLoadedLibrary[0] = 0;                           // empty path
         // get DLL path name
         if (GetModuleFileName(hInstDll, pszPathToLoadedLibrary, MAX_LIBRARY_PATH))
            {  // got name, we must strip DLL name
            pszPathToLoadedLibrary[MAX_LIBRARY_PATH-1] = 0;       // make sure string is terminated
            char *ptrLibName = strstr(pszPathToLoadedLibrary, INTERFACE_LIBRARY);
            if (ptrLibName != NULL)
               *ptrLibName = 0;                                   // cut end of string with name
            }
         else
            pszPathToLoadedLibrary[0] = 0;                        // cannot get path, so try local path
         }
         break;
      case DLL_PROCESS_DETACH:
         {  // unloading DLL, make sure all connections are closed properly
         for (int iIndex = 0; iIndex < MAX_PROBE_INSTANCES; iIndex++)
            {
            if (pptyProbeInstances[iIndex] != NULL)
               (void)OPXD_JTAGClose(iIndex);
            }
         }
         break;
      case DLL_THREAD_ATTACH:
      case DLL_THREAD_DETACH:
      default:
         break;
      }
   return TRUE;
}
#else
// Linux specific functions

/****************************************************************************
     Function: my_init
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Linux specific function called when loading shared library.
Date           Initials    Description
10-Jan-2008    VH          Initial
****************************************************************************/
void __attribute__ ((constructor)) my_init(void)
{
   FILE *pFile;
   // set all pointers to NULL, memory will be allocated later
   for (int iIndex = 0; iIndex < MAX_PROBE_INSTANCES; iIndex++)
      pptyProbeInstances[iIndex] = NULL;
   // get path to shared library
   pszPathToLoadedLibrary[0] = 0;
   pFile = fopen("/proc/self/maps", "r");
   if (pFile != NULL)
      {
      while (!feof(pFile))
         {
         char pszLocLine[MAX_LIBRARY_PATH];
         pszLocLine[0] = 0;
         char *pszLocPath;
         // read line from file and check if filename is part of it
         if (fgets(pszLocLine, MAX_LIBRARY_PATH, pFile) == NULL)
            break;
         if (strstr(pszLocLine, INTERFACE_LIBRARY) == NULL)
            continue;
         pszLocPath = strchr(pszLocLine, '/');
         if ((pszLocPath != NULL) && (strstr(pszLocPath, INTERFACE_LIBRARY) != NULL))
            {
            char *pszEndPath = strstr(pszLocPath, INTERFACE_LIBRARY);
            *pszEndPath = 0;                                         // cut library name in the path
            strncpy(pszPathToLoadedLibrary, pszLocPath, MAX_LIBRARY_PATH);
            pszPathToLoadedLibrary[MAX_LIBRARY_PATH-1] = 0;
            }
         else
            pszPathToLoadedLibrary[0] = 0;
         }
      fclose(pFile);
      }
}

/****************************************************************************
     Function: my_fini
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Linux specific function called when unloading shared library.
Date           Initials    Description
10-Jan-2008    VH          Initial
****************************************************************************/
void __attribute__ ((destructor)) my_fini(void)
{
   // make sure all connections are closed
   for (int iIndex = 0; iIndex < MAX_PROBE_INSTANCES; iIndex++)
      {
      if (pptyProbeInstances[iIndex] != NULL)
         (void)OPXD_JTAGClose(iIndex);
      }
}
#endif

/****************************************************************************
     Function: OPXD_JTAGInit
     Engineer: Vitezslav Hola
        Input: const char *pszProbeInstance - string with Opella-XD serial number
               int *piHandle - pointer to handle
               PFCallback pfCallback - callback function to show progress
               unsigned int uiFrequencyHz - default frequency in Hz
               unsigned short usTargetVoltage - default target voltage
       Output: int - error code
  Description: Open connection to Opella-XD.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_JTAGInit(const char *pszProbeInstance, int *piHandle, PFCallback pfCallback, unsigned int uiFrequencyHz, unsigned short usTargetVoltage)
{
   char pszLocInstance[MAX_SERIAL_NUMBER_LEN];
   char pszLocFilename[MAX_LIBRARY_PATH];
   TyDeviceInfo tyLocDeviceInfo;
   TyProbeInstance *ptyLocProbeInstance;
   int iIndex, iFirmwareDiff;
   unsigned char bReconnectProbe;
   int iNewHandle = CONNECTION_HANDLE_INVALID;
   int iReturn = ERR_NO_ERROR;
   TyDevHandle tyLocHandle = CONNECTION_HANDLE_INVALID;

   if (piHandle == NULL)
      return ERR_INVALID_PARAMETERS;
   *piHandle = MAX_PROBE_INSTANCES;                                  // use invalid handle for a moment
   // find available slot
   for (iIndex = 0; iIndex < MAX_PROBE_INSTANCES; iIndex++)
      {
      if (pptyProbeInstances[iIndex] == NULL)
         iNewHandle = iIndex;                                        // good, we found free slot
      }
   if (iNewHandle == CONNECTION_HANDLE_INVALID)
      return ERR_TOO_MANY_INSTANCES_USED;                            // could not find free slot
   // allocate memory for structure
   ptyLocProbeInstance = (TyProbeInstance *)malloc(sizeof(TyProbeInstance));
   if (ptyLocProbeInstance == NULL)
      return ERR_MEMORY_ALLOCATION_FAILED;
   // initialize structure
   ptyLocProbeInstance->tyCurrentJtagState = TAP_TLR;
   ptyLocProbeInstance->uiCurrentFrequency = DEFAULT_FREQUENCY;
   ptyLocProbeInstance->ulCurrentPulsePeriod = DEFAULT_PULSE_PERIOD;
   ptyLocProbeInstance->uiCurrentRTCKTimeout = DEFAULT_RTCK_TIMEOUT;
   ptyLocProbeInstance->usCurrentVtpa = VTPA_DISABLE;
   // step 1 - check if we have valid serial number for Opella-XD
   if (pszProbeInstance != NULL)
      {
      strncpy(pszLocInstance, pszProbeInstance, MAX_SERIAL_NUMBER_LEN);
      pszLocInstance[MAX_SERIAL_NUMBER_LEN - 1] = 0;
      }
   else
      {  // instance number is NULL, we assume there is only 1 instance connected
      unsigned short usNumberOfDevices = 0;
      char pszCurrentInstances[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN];
      DL_OPXD_GetConnectedDevices(pszCurrentInstances, &usNumberOfDevices, OPELLAXD_USB_PID);
      if (usNumberOfDevices > 1)
         {
         free(ptyLocProbeInstance);
         return ERR_PROBE_INSTANCE_NOT_SPECIFIED;
         }
      else if (usNumberOfDevices == 0)
         {
         free(ptyLocProbeInstance);
         return ERR_NO_PROBE_CONNECTED;
         }
      // there is 1 probe connected so get its serial number
      strncpy(pszLocInstance, pszCurrentInstances[0], MAX_SERIAL_NUMBER_LEN);
      pszLocInstance[MAX_SERIAL_NUMBER_LEN - 1] = 0;
      }
   // now we got serial number of probe to connected to
   // step 2 - get handle to the probe and unconfigure
   if (pfCallback != NULL)
      pfCallback(10);
   if (DL_OPXD_OpenDevice(pszLocInstance, OPELLAXD_USB_PID, false, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      {
      free(ptyLocProbeInstance);
      return ERR_CANNOT_USE_PROBE;
      }
   if (DL_OPXD_UnconfigureDevice(tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      free(ptyLocProbeInstance);
      return ERR_CANNOT_USE_PROBE;
      }
   // step 3 - check firmware version and updgrade if necessary
   if (pfCallback != NULL)
      pfCallback(20);
   GetFullFilename(pszLocFilename, MAX_LIBRARY_PATH, OPXD_FIRMWARE_FILENAME);                // get firmware full filename
   iFirmwareDiff = 0;       bReconnectProbe = 0;
   if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_CHECK_ONLY, pszLocFilename, &bReconnectProbe, &iFirmwareDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      free(ptyLocProbeInstance);
      return ERR_CANNOT_USE_PROBE;
      }
   if (iFirmwareDiff < 0)
      { 
      // we need to upgrade firmware to latest version
      if (pfCallback != NULL)
         pfCallback(25);
      iFirmwareDiff = 0;       bReconnectProbe = 0;
      if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_NEWER_VERSION, pszLocFilename, &bReconnectProbe, &iFirmwareDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
         {
         (void)DL_OPXD_CloseDevice(tyLocHandle);
         free(ptyLocProbeInstance);
         return ERR_CANNOT_USE_PROBE;
         }
      if (pfCallback != NULL)
         pfCallback(40);
      if (bReconnectProbe)
         {
         // wait for 7 seconds
         for (unsigned int uiCnt=0; uiCnt < 7; uiCnt++)
            {
            // show status between 45 and 75
            if (pfCallback != NULL)
               pfCallback(45 + uiCnt*5);
            MsSleep(1000);
            }
         // get handle once more (as device has been reset after firmware upgrade)
         if (DL_OPXD_OpenDevice(pszLocInstance, OPELLAXD_USB_PID, false, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
            {
            free(ptyLocProbeInstance);
            return ERR_CANNOT_USE_PROBE;
            }
         if (DL_OPXD_UnconfigureDevice(tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice(tyLocHandle);
            free(ptyLocProbeInstance);
            return ERR_CANNOT_USE_PROBE;
            }
         }
      }
   ptyLocProbeInstance->tyHandle = tyLocHandle;
   // ok, firmware version is fine and we can continue initialization sequence
   // step 4 - configure probe
   if (pfCallback != NULL)
      pfCallback(80);
   tyLocDeviceInfo.tyTpaConnected = TPA_NONE;
   if (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyLocDeviceInfo) == DRVOPXD_ERROR_NO_ERROR)
      {
      char pszLocFilename2[MAX_LIBRARY_PATH];
      switch (tyLocDeviceInfo.tyTpaConnected)
         {
         case TPA_MIPS14:     // MIPS TPA
            GetFullFilename(pszLocFilename, MAX_LIBRARY_PATH, OPXD_MIPS_DISKWARE_FILENAME); 
            GetFullFilename(pszLocFilename2, MAX_LIBRARY_PATH, OPXD_MIPS_FPGAWARE_FILENAME);
            ptyLocProbeInstance->tyCurrentDiskware = DW_MIPS;
            ptyLocProbeInstance->tyCurrentFpgaware = FPGA_MIPS;
            if (DL_OPXD_ConfigureDevice(tyLocHandle, ptyLocProbeInstance->tyCurrentDiskware, ptyLocProbeInstance->tyCurrentFpgaware, 
                                        pszLocFilename, pszLocFilename2) != DRVOPXD_ERROR_NO_ERROR)
               iReturn = ERR_CANNOT_CONFIGURE_PROBE;
            break;
         case TPA_ARM20:      // ARM TPA
            GetFullFilename(pszLocFilename, MAX_LIBRARY_PATH, OPXD_ARM_DISKWARE_FILENAME); 
            GetFullFilename(pszLocFilename2, MAX_LIBRARY_PATH, OPXD_ARM_FPGAWARE_FILENAME);
            ptyLocProbeInstance->tyCurrentDiskware = DW_ARM;
            ptyLocProbeInstance->tyCurrentFpgaware = FPGA_ARM;
            if (DL_OPXD_ConfigureDevice(tyLocHandle, ptyLocProbeInstance->tyCurrentDiskware, ptyLocProbeInstance->tyCurrentFpgaware, 
                                        pszLocFilename, pszLocFilename2) != DRVOPXD_ERROR_NO_ERROR)
               iReturn = ERR_CANNOT_CONFIGURE_PROBE;
            break;
         case TPA_ARC20:      // ARC TPA
            GetFullFilename(pszLocFilename, MAX_LIBRARY_PATH, OPXD_ARC_DISKWARE_FILENAME); 
            GetFullFilename(pszLocFilename2, MAX_LIBRARY_PATH, OPXD_ARC_FPGAWARE_FILENAME);
            ptyLocProbeInstance->tyCurrentDiskware = DW_ARC;
            ptyLocProbeInstance->tyCurrentFpgaware = FPGA_ARC;
            if (DL_OPXD_ConfigureDevice(tyLocHandle, ptyLocProbeInstance->tyCurrentDiskware, ptyLocProbeInstance->tyCurrentFpgaware, 
                                        pszLocFilename, pszLocFilename2) != DRVOPXD_ERROR_NO_ERROR)
               iReturn = ERR_CANNOT_CONFIGURE_PROBE;
            break;
         case TPA_NONE:
         case TPA_UNKNOWN:
         case TPA_INVALID:
         case TPA_DIAG:
         default:
            iReturn = ERR_INVALID_TPA;
         }
      }
   else
      iReturn = ERR_CANNOT_CONFIGURE_PROBE;
   // error happened
   if (iReturn != ERR_NO_ERROR)
      {
      (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      free(ptyLocProbeInstance);
      return iReturn;
      }
   // step 5 - get probe information
   if (pfCallback != NULL)
      pfCallback(85);
   if (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyLocDeviceInfo) == DRVOPXD_ERROR_NO_ERROR)
      {  // copy all relevant information about probe, diskware, etc.
      ptyLocProbeInstance->ulFirmwareVersion   = tyLocDeviceInfo.ulFirmwareVersion;
      ptyLocProbeInstance->ulFirmwareDate      = tyLocDeviceInfo.ulFirmwareDate;
      ptyLocProbeInstance->ulDiskwareVersion   = tyLocDeviceInfo.ulDiskwareVersion;
      ptyLocProbeInstance->ulDiskwareDate      = tyLocDeviceInfo.ulDiskwareDate;
      ptyLocProbeInstance->ulFpgawareVersion   = tyLocDeviceInfo.ulFpgawareVersion;
      ptyLocProbeInstance->ulFpgawareDate      = tyLocDeviceInfo.ulFpgawareDate;
      }
   else
      {
      (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      free(ptyLocProbeInstance);
      return ERR_CANNOT_CONFIGURE_PROBE;
      }
   // step 6 - set default parameters (frequency, etc.)
   if (pfCallback != NULL)
      pfCallback(90);
   if (DL_OPXD_JtagSetTMS(tyLocHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)                            // set TMS sequence
      iReturn = ERR_CANNOT_CONFIGURE_PROBE;
   if (iReturn == ERR_NO_ERROR)
      iReturn = SetJtagFrequency(ptyLocProbeInstance, uiFrequencyHz);                                                // set JTAG frequency
   if ((ptyLocProbeInstance->tyCurrentDiskware == DW_ARC) && (iReturn == ERR_NO_ERROR) &&                              
       (DL_OPXD_Arc_SelectTarget(tyLocHandle, ARCT_NORMAL) != DRVOPXD_ERROR_NO_ERROR))                               // configure target type (only for ARC)
      iReturn = ERR_CANNOT_CONFIGURE_PROBE;
   if ((iReturn == ERR_NO_ERROR) && (DL_OPXD_JtagConfigMulticore(tyLocHandle, 0, NULL) != DRVOPXD_ERROR_NO_ERROR))   // set multicore
       iReturn = ERR_CANNOT_CONFIGURE_PROBE;
   if (iReturn == ERR_NO_ERROR)
      iReturn = SetTargetVoltage(ptyLocProbeInstance, usTargetVoltage);
   // check if all steps were successful
   if (iReturn != ERR_NO_ERROR)
      {
      (void)DL_OPXD_JtagSetFrequency(tyLocHandle, 1.0, NULL, NULL);
      (void)DL_OPXD_SetVtpaVoltage(tyLocHandle, 0.0, 1, 0);
      (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      free(ptyLocProbeInstance);
      return iReturn;
      }
   // step 7 - finish initialization (copy local structure to the table and return new valid handle)
   if (pfCallback != NULL)
      pfCallback(95);
   pptyProbeInstances[iNewHandle] = ptyLocProbeInstance;             // set pointer to the table
   *piHandle = iNewHandle;
   if (pfCallback != NULL)
      pfCallback(100);                                   // finished initialization
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: OPXD_JTAGClose
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to open connection
       Output: int - error code
  Description: Close active connection to Opella-XD.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_JTAGClose(int iHandle)
{
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // connection is active so we can close it
   // deinitialize connection and close handle
   (void)DL_OPXD_JtagSetFrequency(pptyProbeInstances[iHandle]->tyHandle, 1.0, NULL, NULL);            // set frequency to 1 MHz
   (void)DL_OPXD_SetVtpaVoltage(pptyProbeInstances[iHandle]->tyHandle, 0.0, 1, 0);
   (void)DL_OPXD_UnconfigureDevice(pptyProbeInstances[iHandle]->tyHandle);
   (void)DL_OPXD_CloseDevice(pptyProbeInstances[iHandle]->tyHandle);
   // deallocate structure
   free(pptyProbeInstances[iHandle]);
   pptyProbeInstances[iHandle] = NULL;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: OPXD_ListProbes
     Engineer: Vitezslav Hola
        Input: unsigned int uiMaxInstances - maximum items in ppszProbeInstances
               char ppszProbeInstances[][16] - list for Opella-XD serial numbers
               unsigned int *puiConnectedProbes - pointer to number of Opella-XD instances
       Output: int - error code
  Description: Get list of Opella-XD probes currently connected.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_ListProbes(unsigned int uiMaxInstances, char ppszProbeInstances[][16], unsigned int *puiConnectedProbes)
{
   unsigned short usDeviceConnected = 0;
   // check validity of parameters
   if ((uiMaxInstances > 0) && (ppszProbeInstances != NULL) && (puiConnectedProbes != NULL))
      {
      unsigned short usIndex;
      char ppszDeviceNumbers[MAX_DEVICES_SUPPORTED][16];
      DL_OPXD_GetConnectedDevices(ppszDeviceNumbers, &usDeviceConnected, OPELLAXD_USB_PID);
      // restrict number of copied items if necessary
      if ((unsigned int)usDeviceConnected > uiMaxInstances)
         usDeviceConnected = (unsigned short)uiMaxInstances;
      // copy specified number of items
      for (usIndex=0; usIndex < usDeviceConnected; usIndex++)
         memcpy(ppszProbeInstances[usIndex], ppszDeviceNumbers[usIndex], 16);
      }
   // store number of connected devices
   if (puiConnectedProbes != NULL)
      *puiConnectedProbes = (unsigned int)usDeviceConnected;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: OPXD_GetLibraryInfo
     Engineer: Vitezslav Hola
        Input: char *pszName - string for library name
               char *pszVersion - string for library version
               unsigned int uiSize - number of bytes in pszFirmware, pszDiskware and pszFpgaware
       Output: int - error code
  Description: Copy strings with Opella-XD JTAG API probe information (diskware, firmware and fpgaware 
               version, etc.) into provided buffers.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_GetLibraryInfo(char *pszName, char *pszVersion, unsigned int uiSize)
{
   if (uiSize == 0)
      return ERR_NO_ERROR;
   if (pszName != NULL)
      {  // get library name and ensure string is terminated
      strncpy(pszName, OPXD_JTAGAPI_NAME, uiSize);
      pszName[uiSize-1] = 0;
      }
   if (pszVersion != NULL)
      {  // get library version and ensure string is terminated
      strncpy(pszVersion, OPXD_JTAGAPI_VERSION, uiSize);
      pszVersion[uiSize-1] = 0;
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: OPXD_GetProbeInfo
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               char *pszFirmware - string for firmware information
               char *pszDiskware - string for diskware information
               char *pszFpgaware - string for fpgaware information
               unsigned int uiSize - number of bytes in pszFirmware, pszDiskware and pszFpgaware
       Output: int - error code
  Description: Copy strings with Opella-XD JTAG API probe information (diskware, firmware and fpgaware 
               version, etc.) into provided buffers.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_GetProbeInfo(int iHandle, char *pszFirmware, char *pszDiskware, char *pszFpgaware, unsigned int uiSize)
{
   char pszLocString[INFOSTR_LENGTH];
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   if (uiSize == 0)
      return ERR_NO_ERROR;                   // nothing to fill
   strcpy(pszLocString, "");
   // get firmware info
   if (pszFirmware != NULL)
      ConvertVersionAndDate(pszFirmware, uiSize, pptyProbeInstances[iHandle]->ulFirmwareVersion, pptyProbeInstances[iHandle]->ulFirmwareDate);
   // get diskware info
   if (pszDiskware != NULL)
      ConvertVersionAndDate(pszDiskware, uiSize, pptyProbeInstances[iHandle]->ulDiskwareVersion, pptyProbeInstances[iHandle]->ulDiskwareDate);
   // get fpgaware info
   if (pszFpgaware != NULL)
      ConvertVersionAndDate(pszFpgaware, uiSize, pptyProbeInstances[iHandle]->ulFpgawareVersion, pptyProbeInstances[iHandle]->ulFpgawareDate);
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: OPXD_GetTAPState
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
       Output: TyJTAGState - current TAP state
  Description: Get current state of JTAG TAP state machine. If handle is invalid
               or connection not initialized, function return TAP_TLR.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT TyJTAGState OPXD_GetTAPState(int iHandle)
{
   // check handle validity
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return TAP_TLR;
   // return current state
   return pptyProbeInstances[iHandle]->tyCurrentJtagState;
}

/****************************************************************************
     Function: OPXD_JTAGPulses
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned int uiNumberOfPulses - number of pulses to do (up to MAXIMUM_NUMBER_OF_PULSES)
               unsigned int *puiTDI - pattern for TDI
               unsigned int *puiTMS - pattern for TMS
               unsigned int *puiTDO - captured pattern for TDO
       Output: int - error code
  Description: Generate specified number of pulses on JTAG TCK signal with 
               predefined values of TDI and TMS and capturing TDO.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_JTAGPulses(int iHandle, unsigned int uiNumberOfPulses, unsigned int *puiTDI, unsigned int *puiTMS, unsigned int *puiTDO)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // check validity of parameters
   if ((uiNumberOfPulses > MAXIMUM_NUMBER_OF_PULSES) || (puiTDI == NULL) || (puiTMS == NULL))
      return ERR_INVALID_PARAMETERS;
   // any pulses to generate
   if (uiNumberOfPulses == 0)
      return ERR_NO_ERROR;
   // call Opella-XD diskware function
   tyRes = DL_OPXD_JtagPulses(pptyProbeInstances[iHandle]->tyHandle, uiNumberOfPulses, pptyProbeInstances[iHandle]->ulCurrentPulsePeriod, 
                              (unsigned long *)puiTDI, (unsigned long *)puiTMS, (unsigned long *)puiTDO);
   // mask TDO pattern if not alligned to words
   if ((uiNumberOfPulses % 32) && (puiTDO != NULL))
      puiTDO[uiNumberOfPulses / 32] &= (0xFFFFFFFF >> (32 - (uiNumberOfPulses % 32)));
   // update current state in JTAG state machine
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
      {
      unsigned int uiCurrentPulse;
      unsigned char ucCurrentTMS = 0;
      // go through TMS pattern and update TAP state after each bit using JTAG TA{P state machine diagram
      for (uiCurrentPulse=0; uiCurrentPulse < uiNumberOfPulses; uiCurrentPulse++)
         {
         unsigned int uiBitMask = 0x00000001;
         uiBitMask <<= (uiCurrentPulse % 32);
         // get next value of TMS
         if (puiTMS[uiCurrentPulse / 32] & uiBitMask)
            ucCurrentTMS = 1;
         else
            ucCurrentTMS = 0;
         pptyProbeInstances[iHandle]->tyCurrentJtagState = NextTAPState(pptyProbeInstances[iHandle]->tyCurrentJtagState, ucCurrentTMS);
         }
      }
   // return proper error code
   return ConvertErrorCode(tyRes);
}

/****************************************************************************
     Function: OPXD_GetJTAGPins
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned char *pucTDI - status of TDI pin
               unsigned char *pucTDO - status of TDO pin
               unsigned char *pucTMS - status of TMS pin
               unsigned char *pucTCK - status of TCK pin
       Output: int - error code
  Description: Get current status of JTAG pins.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_GetJTAGPins(int iHandle, unsigned char *pucTDI, unsigned char *pucTDO, unsigned char *pucTMS, unsigned char *pucTCK)
{
   unsigned char ucLocTDI, ucLocTDO, ucLocTMS, ucLocTCK;
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // set local variables
   ucLocTDI = 0;     ucLocTDO = 0;     ucLocTMS = 0;     ucLocTCK = 0;
   tyRes = DL_OPXD_JtagGetPins(pptyProbeInstances[iHandle]->tyHandle, &ucLocTDI, &ucLocTDO, &ucLocTMS, &ucLocTCK);  
   // set result values
   if (pucTDI != NULL)
      *pucTDI = ucLocTDI;
   if (pucTDO != NULL)
      *pucTDO = ucLocTDO;
   if (pucTMS != NULL)
      *pucTMS = ucLocTMS;
   if (pucTCK != NULL)
      *pucTCK = ucLocTCK;
   // return proper error code
   return ConvertErrorCode(tyRes);
}

/****************************************************************************
     Function: OPXD_TAPReset
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned char bUseTrst - TAP reset type
       Output: int - error code
  Description: Reset JTAG TAP state machine using two methods.
               If bUseTrst==0, TMS signal is kept high with at least 5 pulses on TCK.
               If bUseTrst==1, TRST is asserted (held low) for short period of time.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_TAPReset(int iHandle, unsigned char bUseTrst)
{
   TyError tyRes;
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // call function to change target status by sending command to Opella-XD
   tyRes = DL_OPXD_JtagResetTAP(pptyProbeInstances[iHandle]->tyHandle, bUseTrst);
   // we need to change current TAP state to TLR
   pptyProbeInstances[iHandle]->tyCurrentJtagState = TAP_TLR;
   return ConvertErrorCode(tyRes);
}

/****************************************************************************
     Function: OPXD_TargetReset
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned char bAssertReset - assert reset
       Output: int - error code
  Description: Reset target board using RST/SRST pin.
               If bAssertReset==1 target is kept in reset and if bAssertReset==0 target reset is released.
Date           Initials    Description
20-Dec-2007    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_TargetReset(int iHandle, unsigned char bAssertReset)
{
   TyError tyRes;
   int iReturn = ERR_NO_ERROR;
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // since target reset uses diskware specific functions, we need to know what diskware is currently used
   switch (pptyProbeInstances[iHandle]->tyCurrentDiskware)
      {
      case DW_MIPS:
         tyRes = DL_OPXD_Mips_ResetProc(pptyProbeInstances[iHandle]->tyHandle, 0, bAssertReset, NULL);
         iReturn = ConvertErrorCode(tyRes);
         break;
      case DW_ARC:
         tyRes = DL_OPXD_Arc_ResetProc(pptyProbeInstances[iHandle]->tyHandle, bAssertReset, 0); 
         iReturn = ConvertErrorCode(tyRes);
         break;
      case DW_ARM:
         tyRes = DL_OPXD_Arm_ResetProc(pptyProbeInstances[iHandle]->tyHandle, 0, bAssertReset, NULL, NULL);
         iReturn = ConvertErrorCode(tyRes);
         break;
      case DW_NONE:
      case DW_UNKNOWN:
      case DW_TARCH:
      case DW_DIAG:
         iReturn = ERR_PROBE_NOT_CONFIGURED;
         break;
      }
   // we assume target reset brings TAP int TLR state when asserting reset
   if (bAssertReset)
      pptyProbeInstances[iHandle]->tyCurrentJtagState = TAP_TLR;
   return iReturn;
}

/****************************************************************************
     Function: OPXD_JTAGScan
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned char ucScanAttributes - scan attributes (SCAN_xxx constants)
               unsigned int uiLength - number of bits
               unsigned int *puiDataOut - data to be shifted into JTAG register
               unsigned int *puiDataIn - data shifted from JTAG register
       Output: int - error code
  Description: Shift specified number of bits into and from JTAG register depending on scan attributes.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_JTAGScan(int iHandle, unsigned char ucScanAttributes, unsigned int uiLength, unsigned int *puiDataOut, unsigned int *puiDataIn)
{
   int iReturn = ERR_NO_ERROR;
   TyJTAGState tyFinishState = TAP_RTI;
   unsigned long *ppulDataOut[1];
   unsigned long *ppulDataIn[1];
   
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;                                     // handle is invalid
   if (uiLength == 0)
      return ERR_NO_ERROR;                                           // nothing to scan
   ppulDataOut[0] = (unsigned long *)puiDataOut;
   ppulDataIn[0]  = (unsigned long *)puiDataIn;
   // calculate scan parameters and assign proper length
   iReturn = PrepareScanParams(pptyProbeInstances[iHandle], 1, &ucScanAttributes, &uiLength, &tyFinishState);
   if (iReturn != ERR_NO_ERROR)
      return iReturn;
   // call function to send request to Opella-XD
   iReturn = ConvertErrorCode(DL_OPXD_JtagScanMultiple(pptyProbeInstances[iHandle]->tyHandle, 0, 1, 
                                                       pptyProbeInstances[iHandle]->ptyMultipleScanParams, ppulDataOut, ppulDataIn));
   // update finish state
   pptyProbeInstances[iHandle]->tyCurrentJtagState = tyFinishState;
   return iReturn;
}

/****************************************************************************
     Function: OPXD_JTAGMultipleScan
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned int uiNumberOfScans - number of scans (up to 256)
               unsigned char *pucScanAttributes - attay of scan attributes (SCAN_xxx constants)
               unsigned int *puiLength - array of number of bits for each scan
               unsigned int **ppuiDataOut - array of pointers to data to be shifted into JTAG register
               unsigned int **ppuiDataIn - array of pointers to data shifted from JTAG register
       Output: int - error code
  Description: Optimized multiple scans, each as OPXD_JTAGScan.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_JTAGMultipleScan(int iHandle, unsigned int uiNumberOfScans, unsigned char *pucScanAttributes, unsigned int *puiLength, 
                                               unsigned int **ppuiDataOut, unsigned int **ppuiDataIn)
{
   int iReturn = ERR_NO_ERROR;
   TyJTAGState tyFinishState = TAP_RTI;

   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;                                     // handle is invalid
   if (uiNumberOfScans == 0)
      return ERR_NO_ERROR;                                           // nothing to scan
   else if (uiNumberOfScans > MAXIMUM_NUMBER_OF_SCANS)
      return ERR_INVALID_PARAMETERS;                                 // too many scans
   if ((pucScanAttributes == NULL) || (puiLength == NULL) || (ppuiDataOut == NULL) || (ppuiDataIn == NULL))
      return ERR_INVALID_PARAMETERS;
   // calculate scan parameters and assign proper length
   iReturn = PrepareScanParams(pptyProbeInstances[iHandle], uiNumberOfScans, pucScanAttributes, puiLength, &tyFinishState);
   if (iReturn != ERR_NO_ERROR)
      return iReturn;
   // call function to send request to Opella-XD
   iReturn = ConvertErrorCode(DL_OPXD_JtagScanMultiple(pptyProbeInstances[iHandle]->tyHandle, 0, (unsigned long)uiNumberOfScans,
                                                       pptyProbeInstances[iHandle]->ptyMultipleScanParams, 
                                                       (unsigned long **)ppuiDataOut, (unsigned long **)ppuiDataIn));
   // update finish state
   pptyProbeInstances[iHandle]->tyCurrentJtagState = tyFinishState;
   return iReturn;
}

/****************************************************************************
     Function: OPXD_SetJTAGFrequency
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned int uiFrequencyHz - frequency in Hz
       Output: int - error code
  Description: Set JTAG frequency for Opella-XD to selected value.
               Supported range is from 1kHz (1000) to 1000 MHz (100000000).
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_SetJTAGFrequency(int iHandle, unsigned int uiFrequencyHz)
{
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // call local function to set frequency
   return SetJtagFrequency(pptyProbeInstances[iHandle], uiFrequencyHz);
}

/****************************************************************************
     Function: OPXD_SetRTCKTimeout
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned int uiTimeoutMs - timeout in ms
       Output: int - error code
  Description: Set adaptive clock timeout for case target does not provide RTCK.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_SetRTCKTimeout(int iHandle, unsigned int uiTimeoutMs)
{
   int iReturn = ERR_NO_ERROR;
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // check timeout value
   if ((uiTimeoutMs < MINIMUM_RTCK_TIMEOUT) || (uiTimeoutMs > MAXIMUM_RTCK_TIMEOUT))
      return ERR_INVALID_PARAMETERS;
   // store timeout in local structure and update in Opella-XD only if adaptive clock is being currently used
   pptyProbeInstances[iHandle]->uiCurrentRTCKTimeout = uiTimeoutMs;
   if (pptyProbeInstances[iHandle]->uiCurrentFrequency == FREQUENCY_ADAPTIVE)
      {
      iReturn = ConvertErrorCode(DL_OPXD_JtagSetAdaptiveClock(pptyProbeInstances[iHandle]->tyHandle, 
                                                              (unsigned long)(pptyProbeInstances[iHandle]->uiCurrentRTCKTimeout), 1.0));
      }
   return iReturn;
}

/****************************************************************************
     Function: OPXD_SetTargetVoltage
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
               unsigned short usTargetVoltage - target voltage (see VTPA_xxx constants)
       Output: int - error code
  Description: Set TPA target voltage as fixed 
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_SetTargetVoltage(int iHandle, unsigned short usTargetVoltage)
{
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // call local function
   return SetTargetVoltage(pptyProbeInstances[iHandle], usTargetVoltage);
}

/****************************************************************************
     Function: OPXD_ToggleTargetStatus
     Engineer: Vitezslav Hola
        Input: int iHandle - handle to connection
       Output: int - error code
  Description: Turn "Target Status" LED to red on Opella-XD for 500 ms.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
extern "C" DLLEXPORT int OPXD_ToggleTargetStatus(int iHandle)
{
   // check validity of handle
   if ((iHandle < 0) || (iHandle >= MAX_PROBE_INSTANCES) || (pptyProbeInstances[iHandle] == NULL))
      return ERR_INVALID_HANDLE;
   // call function to change target status by sending command to Opella-XD
   return ConvertErrorCode(DL_OPXD_IndicateTargetStatus(pptyProbeInstances[iHandle]->tyHandle, 0, 0));
}

/****************************************************************************
*** Opella-XD JTAG API local functions                                    ***
****************************************************************************/

/****************************************************************************
     Function: NextTAPState
     Engineer: Vitezslav Hola
        Input: TyJTAGState tyCurrentState - current TAP state
               unsigned char ucTMS - value of TMS
       Output: TyJTAGState - next TAP state
  Description: Get value of next JTAG TAP state machine from current state and TMS value.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static TyJTAGState NextTAPState(TyJTAGState tyCurrentState, unsigned char ucTMS)
{
   TyJTAGState tyNextState = TAP_TLR;
   // decide what state is next
   switch (tyCurrentState)
      {
      case TAP_RTI:
         if (ucTMS)
            tyNextState = TAP_SELECT_DR;                    // TMS==1 and RTI -> Select-DR
         else
            tyNextState = TAP_RTI;                          // TMS==0 and RTI -> RTI
         break;
      case TAP_SELECT_DR:
         if (ucTMS)
            tyNextState = TAP_SELECT_IR;                    // TMS==1 and Select-DR -> Select-IR
         else
            tyNextState = TAP_CAPTURE_DR;                   // TMS==0 and Select-DR -> Capture-DR
         break;
      case TAP_SELECT_IR:
         if (ucTMS)
            tyNextState = TAP_TLR;                          // TMS==1 and Select-IR -> TLR
         else
            tyNextState = TAP_CAPTURE_IR;                   // TMS==0 and Select-IR -> Capture-DR
         break;
      case TAP_CAPTURE_DR:
         if (ucTMS)
            tyNextState = TAP_EXIT1_DR;                     // TMS==1 and Capture-DR -> Exit1-DR
         else
            tyNextState = TAP_SHIFT_DR;                     // TMS==0 and Capture-DR -> Shift-DR
         break;
      case TAP_SHIFT_DR:
         if (ucTMS)
            tyNextState = TAP_EXIT1_DR;                     // TMS==1 and Shift-DR -> Exit1-DR
         else
            tyNextState = TAP_SHIFT_DR;                     // TMS==0 and Shift-DR -> Shift-DR
         break;
      case TAP_EXIT1_DR:
         if (ucTMS)
            tyNextState = TAP_UPDATE_DR;                    // TMS==1 and Exit1-DR -> Update-DR
         else
            tyNextState = TAP_PAUSE_DR;                     // TMS==0 and Exit1-DR -> Pause-DR
         break;
      case TAP_PAUSE_DR:
         if (ucTMS)
            tyNextState = TAP_EXIT2_DR;                     // TMS==1 and Pause-DR -> Exit2-DR
         else
            tyNextState = TAP_PAUSE_DR;                     // TMS==0 and Pause-DR -> Pause-DR
         break;
      case TAP_EXIT2_DR:
         if (ucTMS)
            tyNextState = TAP_UPDATE_DR;                    // TMS==1 and Exit2-DR -> Update-DR
         else
            tyNextState = TAP_SHIFT_DR;                     // TMS==0 and Exit2-DR -> Shift-DR
         break;
      case TAP_UPDATE_DR:
         if (ucTMS)
            tyNextState = TAP_SELECT_DR;                    // TMS==1 and Update-DR -> Select-DR
         else
            tyNextState = TAP_RTI;                          // TMS==0 and Update-DR -> RTI
         break;
      case TAP_CAPTURE_IR:
         if (ucTMS)
            tyNextState = TAP_EXIT1_IR;                     // TMS==1 and Capture-IR -> Exit1-IR
         else
            tyNextState = TAP_SHIFT_IR;                     // TMS==0 and Capture-IR -> Shift-IR
         break;
      case TAP_SHIFT_IR:
         if (ucTMS)
            tyNextState = TAP_EXIT1_IR;                     // TMS==1 and Shift-IR -> Exit1-IR
         else
            tyNextState = TAP_SHIFT_IR;                     // TMS==0 and Shift-IR -> Shift-IR
         break;
      case TAP_EXIT1_IR:
         if (ucTMS)
            tyNextState = TAP_UPDATE_IR;                    // TMS==1 and Exit1-IR -> Update-IR
         else
            tyNextState = TAP_PAUSE_IR;                     // TMS==0 and Exit1-IR -> Pause-IR
         break;
      case TAP_PAUSE_IR:
         if (ucTMS)
            tyNextState = TAP_EXIT2_IR;                     // TMS==1 and Pause-IR -> Exit2-IR
         else
            tyNextState = TAP_PAUSE_IR;                     // TMS==0 and Pause-IR -> Pause-IR
         break;
      case TAP_EXIT2_IR:
         if (ucTMS)
            tyNextState = TAP_UPDATE_IR;                    // TMS==1 and Exit2-IR -> Update-IR
         else
            tyNextState = TAP_SHIFT_IR;                     // TMS==0 and Exit2-IR -> Shift-IR
         break;
      case TAP_UPDATE_IR:
         if (ucTMS)
            tyNextState = TAP_SELECT_DR;                    // TMS==1 and Update-IR -> Select-DR
         else
            tyNextState = TAP_RTI;                          // TMS==0 and Update-IR -> RTI
         break;
      case TAP_TLR:
      default:
         if (ucTMS)
            tyNextState = TAP_TLR;                          // TMS==1 and TLR -> TLR
         else
            tyNextState = TAP_RTI;                          // TMS==0 and TLR -> RTI
         break;
      }
   return tyNextState;
}

/****************************************************************************
     Function: ConvertErrorCode
     Engineer: Vitezslav Hola
        Input: TyError tyErrorCode - Opella-XD driver layer error code
       Output: int - JTAG API error code
  Description: Convert driver layer error code into JTAG API error code
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static int ConvertErrorCode(TyError tyErrorCode)
{
   // deal quickly with case there is no error
   if (tyErrorCode == DRVOPXD_ERROR_NO_ERROR)
      return ERR_NO_ERROR;
   else
      {
      int iReturnValue = ERR_UNSPECIFIED_ERROR;

      switch (tyErrorCode)
         {
         case DRVOPXD_ERROR_USB_DRIVER:
            iReturnValue = ERR_USB_DRIVER_ERROR;
            break;
         case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
            iReturnValue = ERR_RTCK_TIMEOUT_EXPIRED;
            break;
         case DRVOPXD_ERROR_INVALID_FREQUENCY:
            iReturnValue = ERR_FREQUENCY_OUT_OF_RANGE;
            break;
         default:
            iReturnValue = ERR_UNSPECIFIED_ERROR;
            break;
         }
      return iReturnValue;
      }
}

/****************************************************************************
     Function: ConvertVersionAndDate
     Engineer: Vitezslav Hola
        Input: char *pszString - buffer to store string
               unsigned int uiSize - number of bytes in buffer
               unsigned long ulVersion - version in firmware/diskware format
               unsigned long ulDate - date in firmware/diskware format
       Output: none
  Description: Convert version and date in Opella-XD format into string.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static void ConvertVersionAndDate(char *pszString, unsigned int uiSize, unsigned long ulVersion, unsigned long ulDate)
{
   char pszLocStr[64];
   char pszLocMonth[8];
   unsigned int uiDay, uiMonth, uiYear, uiVer1, uiVer2, uiVer3, uiVer4;
   // check string
   if ((pszString == NULL) || (uiSize == 0))
      return;
   pszString[0] = 0;             // starting with empty string
   // decode bit fields
   uiDay    = (unsigned int)((ulDate & 0xFF000000) >> 24);
   uiMonth  = (unsigned int)((ulDate & 0x00FF0000) >> 16);
   uiYear   = (unsigned int)(ulDate & 0x0000FFFF);
   uiVer1   = (unsigned int)((ulVersion & 0xFF000000) >> 24);
   uiVer2   = (unsigned int)((ulVersion & 0x00FF0000) >> 16);
   uiVer3   = (unsigned int)((ulVersion & 0x0000FF00) >> 8);
   uiVer4   = (unsigned int)(ulVersion & 0x000000FF);
   // decide on month in date
   switch (uiMonth)
      {
      case 0x01:  strcpy(pszLocMonth, "Jan");   break;
      case 0x02:  strcpy(pszLocMonth, "Feb");   break;
      case 0x03:  strcpy(pszLocMonth, "Mar");   break;
      case 0x04:  strcpy(pszLocMonth, "Apr");   break;
      case 0x05:  strcpy(pszLocMonth, "May");   break;
      case 0x06:  strcpy(pszLocMonth, "Jun");   break;
      case 0x07:  strcpy(pszLocMonth, "Jul");   break;
      case 0x08:  strcpy(pszLocMonth, "Aug");   break;
      case 0x09:  strcpy(pszLocMonth, "Sep");   break;
      case 0x10:  strcpy(pszLocMonth, "Oct");   break;
      case 0x11:  strcpy(pszLocMonth, "Nov");   break;
      case 0x12:
      default:    strcpy(pszLocMonth, "Dec");   break;
      }
   // and create whole string
   if (((uiVer4 >= 'A') && (uiVer4 <= 'Z')) || ((uiVer4 >= 'a') && (uiVer4 <= 'z')))
      {
      sprintf(pszLocStr, "v%x.%x.%x-%c, %02x-%s-%04x", uiVer1, uiVer2, uiVer3, (unsigned char)uiVer4, uiDay, pszLocMonth, uiYear);
      }
   else
      {
      sprintf(pszLocStr, "v%x.%x.%x, %02x-%s-%04x", uiVer1, uiVer2, uiVer3, uiDay, pszLocMonth, uiYear);
      }
   // now copy local string into buffer with proper size
   strncpy(pszString, pszLocStr, uiSize);
   pszString[uiSize - 1] = 0;                      // make sure string has terminating character
}

/****************************************************************************
     Function: GetFullFilename
     Engineer: Vitezslav Hola
        Input: char *pszString - buffer to store string
               unsigned int uiSize - number of bytes in buffer
               const char *pszFilename - filename
       Output: none
  Description: Create full filename including path to library itself.
               This can be used to obtain full path to diskware, firmware, etc.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static void GetFullFilename(char *pszString, unsigned int uiSize, const char *pszFilename)
{
   if ((pszString == NULL) || (uiSize == 0) || (pszFilename == NULL))
      return;
   // get path to library including / character
   strncpy(pszString, pszPathToLoadedLibrary, uiSize);
   pszString[uiSize - 1] = 0;
   if (strlen(pszString) < uiSize)
      {  // add required filename
      strncat(pszString, pszFilename, uiSize - strlen(pszString));
      pszString[uiSize - 1] = 0;
      }
}

/****************************************************************************
     Function: SetJtagFrequency
     Engineer: Vitezslav Hola
        Input: TyProbeInstance *ptyInstance - pointer to instance
               unsigned int uiFrequencyHz - frequency in Hz
       Output: int - error code
  Description: Set frequency for Opella-XD and update structure values.
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static int SetJtagFrequency(TyProbeInstance *ptyInstance, unsigned int uiFrequencyHz)
{
   int iReturn = ERR_NO_ERROR;

   if (ptyInstance == NULL)
      return ERR_INVALID_HANDLE;
   // check frequency range
   if ((uiFrequencyHz != FREQUENCY_ADAPTIVE) && ((uiFrequencyHz < MINIMUM_FREQUENCY) || (uiFrequencyHz > MAXIMUM_FREQUENCY)))
      return ERR_FREQUENCY_OUT_OF_RANGE;
   // call diskware function depending if using fixed or adaptive clock
   if (uiFrequencyHz == FREQUENCY_ADAPTIVE)
      iReturn = ConvertErrorCode(DL_OPXD_JtagSetAdaptiveClock(ptyInstance->tyHandle, (unsigned long)(ptyInstance->uiCurrentRTCKTimeout), 1.0));
   else
      {  // using fixed clock
      double dFrequency = ((double)uiFrequencyHz) / 1000000.0;                      // convert to double in MHz
      iReturn = ConvertErrorCode(DL_OPXD_JtagSetFrequency(ptyInstance->tyHandle, dFrequency, NULL, NULL));
      }
   if (iReturn == ERR_NO_ERROR)
      {  // update local structures
      ptyInstance->uiCurrentFrequency = uiFrequencyHz;
      // calculate pulse period from new frequency as well
      if (uiFrequencyHz <= 1000)
         ptyInstance->ulCurrentPulsePeriod = 1000;
      else if (uiFrequencyHz >= 1000000)
         ptyInstance->ulCurrentPulsePeriod = 1;
      else
         {
         double dPulsePeriod = (double)uiFrequencyHz;
         dPulsePeriod = 1000000.0 / dPulsePeriod;                 // get pulse period in microseconds
         ptyInstance->ulCurrentPulsePeriod = (unsigned long)(dPulsePeriod + 0.5);
         // check if pulse period is in valid range
         if (ptyInstance->ulCurrentPulsePeriod < 1)
            ptyInstance->ulCurrentPulsePeriod = 1;
         else if (ptyInstance->ulCurrentPulsePeriod > 1000)
            ptyInstance->ulCurrentPulsePeriod = 1000;
         }
      }
   return iReturn;
}

/****************************************************************************
     Function: SetTargetVoltage
     Engineer: Vitezslav Hola
        Input: TyProbeInstance *ptyInstance - pointer to instance
               unsigned short usTargetVoltage - target voltage selection
       Output: int - error code
  Description: Set target voltage for TPA cable
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static int SetTargetVoltage(TyProbeInstance *ptyInstance, unsigned short usTargetVoltage)
{
   int iReturn = ERR_NO_ERROR;
   // check pointer
   if (ptyInstance == NULL)
      return ERR_INVALID_HANDLE;
   // select target voltage as required
   switch (usTargetVoltage)
      {
      case VTPA_DISABLE:      // track target, disable buffers
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 3.3, 1, 0));
         break;
      case VTPA_MATCH:        // track target, enable buffers        
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 3.3, 1, 1));
         break;
      case VTPA_FIXED_3V3:    // fixed 3.3V, enable buffers
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 3.3, 0, 1));
         break;
      case VTPA_FIXED_2V5:    // fixed 2.5V, enable buffers
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 2.5, 0, 1));
         break;
      case VTPA_FIXED_1V8:    // fixed 1.8V, enable buffers
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 1.8, 0, 1));
         break;
      case VTPA_FIXED_1V5:    // fixed 1.5V, enable buffers
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 1.5, 0, 1));
         break;
      case VTPA_FIXED_1V2:    // fixed 1.2V, enable buffers
         iReturn = ConvertErrorCode(DL_OPXD_SetVtpaVoltage(ptyInstance->tyHandle, 1.2, 0, 1));
         break;
      default:
         return ERR_INVALID_PARAMETERS;
      }
   if (iReturn == ERR_NO_ERROR)
      ptyInstance->usCurrentVtpa = usTargetVoltage;                         // update voltage
   // we must wait to stabilize target voltage about 500 ms
   MsSleep(500);
   return iReturn;
}

/****************************************************************************
     Function: PrepareScanParams
     Engineer: Vitezslav Hola
        Input: TyProbeInstance *ptyInstance - pointer to instance
               unsigned int uiCount - number of scans
               unsigned char *pucScanAttributes - array of scan attributes
               unsigned int *puiLength - array of length to scan
               TyJTAGState *ptyFinalState - pointer to store final state after all scans
       Output: int - error code
  Description: Prepare parameters for multiple scans (TMS sequences, etc.) in ptyInstance->ptyMultipleScanParams
Date           Initials    Description
04-Jan-2008    VH          Initial
****************************************************************************/
static int PrepareScanParams(TyProbeInstance *ptyInstance, unsigned int uiCount, unsigned char *pucScanAttributes, unsigned int *puiLength, TyJTAGState *ptyFinalState)
{
   unsigned int uiIndex;
   unsigned long ulSizeSend, ulSizeRecv;
   TyJTAGState tyLocCurrentState;
   // check valid parameters
   if ((ptyInstance == NULL) || (pucScanAttributes == NULL) || (ptyFinalState == NULL))
      return ERR_INVALID_HANDLE;
   if ((ptyInstance->tyCurrentJtagState != TAP_RTI) && (ptyInstance->tyCurrentJtagState != TAP_TLR) && 
       (ptyInstance->tyCurrentJtagState != TAP_PAUSE_IR) && (ptyInstance->tyCurrentJtagState != TAP_PAUSE_DR))
      return ERR_INCORRECT_JTAG_TAP_STATE;
   // go through all scans, prepare parameters and verify data size is not too big to handle
   ulSizeSend = 0;   ulSizeRecv = 0;
   tyLocCurrentState = ptyInstance->tyCurrentJtagState;
   for (uiIndex = 0; uiIndex < uiCount; uiIndex++)
      {
      // get scan type, pre TMS and next state
      if ((pucScanAttributes[uiIndex] & SCAN_MASK_TYPE) == SCAN_IR)
         {
         // scanning IR
         ptyInstance->ptyMultipleScanParams[uiIndex].bScanIR = 1;
         // get pre TMS sequence
         if ((tyLocCurrentState == TAP_RTI) || (tyLocCurrentState == TAP_TLR))
            {  // starting from RTI or TLR and scanning IR
            // 5 bit pre TMS sequence 00110 (from RTI or TLR to Shift-IR)
            ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsCount = 5;
            ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsBits = 0x0006;
            }
         else
            {  // starting from Pause-IR or Pause-DR
            // depending which way to take
            if ((pucScanAttributes[uiIndex] & SCAN_MASK_PATH) == SCAN_AVOID_RTI)
               {
               // 6 bit pre TMS sequence 001111 (from Pause-IR/DR to Shift-IR avoiding RTI)
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsCount = 6;
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsBits = 0x000F;
               }
            else
               {
               // 7 bit pre TMS sequence 0011011 (from Pause-IR/DR to Shift-IR via RTI)
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsCount = 7;
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsBits = 0x001B;
               }
            }
         // we will finish in Pause-IR or RTI, update current state after deciding on pre TMS !!!
         if ((pucScanAttributes[uiIndex] & SCAN_MASK_FINISH) == SCAN_FINISH_PAUSE)
            tyLocCurrentState = TAP_PAUSE_IR;
         else
            tyLocCurrentState = TAP_RTI;
         }
      else
         {  
         // scanning DR
         ptyInstance->ptyMultipleScanParams[uiIndex].bScanIR = 0;
         if ((tyLocCurrentState == TAP_RTI) || (tyLocCurrentState == TAP_TLR))
            {  // starting from RTI or TLR and scanning DR
            // 4 bit pre TMS sequence 00010 (from RTI or TLR to Shift-DR)
            ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsCount = 4;
            ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsBits = 0x0002;
            }
         else
            {  // starting from Pause-IR or Pause-DR
            // depending which way to take
            if ((pucScanAttributes[uiIndex] & SCAN_MASK_PATH) == SCAN_AVOID_RTI)
               {
               // 5 bit pre TMS sequence 00111 (from Pause-IR/DR to Shift-DR avoiding RTI)
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsCount = 5;
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsBits = 0x0007;
               }
            else
               {
               // 6 bit pre TMS sequence 001011 (from Pause-IR/DR to Shift-DR via RTI)
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsCount = 6;
               ptyInstance->ptyMultipleScanParams[uiIndex].usPreTmsBits = 0x000B;
               }
            }
         // we will finish in Pause-DR or RTI, update current state after deciding on pre TMS !!!
         if ((pucScanAttributes[uiIndex] & SCAN_MASK_FINISH) == SCAN_FINISH_PAUSE)
            tyLocCurrentState = TAP_PAUSE_DR;
         else
            tyLocCurrentState = TAP_RTI;
         }
      // get post TMS sequence
      if ((pucScanAttributes[uiIndex] & SCAN_MASK_FINISH) == SCAN_FINISH_PAUSE)
         {
         // 4 bit post TMS sequence 0001 (from Shift-IR/DR to Pause-IR/DR)
         ptyInstance->ptyMultipleScanParams[uiIndex].usPostTmsCount = 4;
         ptyInstance->ptyMultipleScanParams[uiIndex].usPostTmsBits = 0x0001;
         }
      else
         {
         // 4 bit post TMS sequence 0011 (from Shift-IR/DR to RTI)
         ptyInstance->ptyMultipleScanParams[uiIndex].usPostTmsCount = 4;
         ptyInstance->ptyMultipleScanParams[uiIndex].usPostTmsBits = 0x0003;
         }
      // get length to scan
      if (puiLength[uiIndex] > MAXIMUM_SCAN_LENGTH)
         return ERR_INVALID_PARAMETERS;
      ptyInstance->ptyMultipleScanParams[uiIndex].usLength = (unsigned short)puiLength[uiIndex];
      // add size in bytes
      if (puiLength[uiIndex] % 32)
         ulSizeSend += (((puiLength[uiIndex] / 32) + 1) * 4);
      else
         ulSizeSend += ((puiLength[uiIndex] / 32) * 4);
      if (puiLength[uiIndex] % 32)
         ulSizeRecv += (((puiLength[uiIndex] / 32) + 1) * 4);
      else
         ulSizeRecv += ((puiLength[uiIndex] / 32) * 4);
      ulSizeSend += 4;                                                        // add also 4 bytes for parameters
      ptyInstance->ptyMultipleScanParams[uiIndex].usInReserved = 0;
      ptyInstance->ptyMultipleScanParams[uiIndex].usOutReserved = 0;
      }
   // check total size
   if ((ulSizeSend > MAX_SCAN_PAYLOAD) || (ulSizeRecv > MAX_SCAN_PAYLOAD))
      return ERR_TOO_LARGE_DATA;
   // get finish state
   *ptyFinalState = tyLocCurrentState;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MsSleep
     Engineer: Vitezslav Hola
        Input: unsigned int uiMiliseconds - number of miliseconds to sleep
       Output: none
  Description: Implementation of sleep function for both Windows and Linux.
Date           Initials    Description
10-Jan-2008    VH          Initial
****************************************************************************/
static void MsSleep(unsigned int uiMiliseconds)
{
   #ifndef __LINUX
   // Windows implementation, just using Win32 API
   Sleep(uiMiliseconds);
   #else
   // Linux implementation, using usleep (time in microseconds)
   usleep(uiMiliseconds * 1000);
   #endif
}

