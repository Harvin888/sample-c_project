/****************************************************************************
       Module: arcsetup.h
     Engineer: Vitezslav Hola
  Description: Headers for ARC setup interface.
Date           Initials    Description
15-Oct-2007    VH          Initial
09-Sep-2009    RS		   Added TAP Reset check box
****************************************************************************/
#ifndef ARC_SETUP_H_
#define ARC_SETUP_H_


// Linux build change...
#include "../../protocol/common/types.h"
// NOTE: This DLL is included from several projects because it is used as interface.
//       Therefore be careful if you are changing something in this header.

// External DLLs or libraries used by the driver
#ifdef __LINUX
// Linux
#define ARCGUI_DLL                           "arcgui.so"
#else
// WinXP/2000/Vista
#define ARCGUI_DLL                           "arcgui.dll"
#endif

#define ARCGUISETUPDLG_NAME                  "AshShowARCSetupWindow"

// default INI filename (setup for ARC)
#define ARC_INI_DEFAULT_NAME                 "opxdarc.ini"
#define GDB_ARC_INI_DEFAULT_NAME					"gdbopxdarc.ini"
#define ARC_INI_ENV_VARIABLE                 "OPXDARC_INI_FILE"

//-------------------------------------------------------------------
// This section defines names for configuration file (item and section names)
//
#define ARC_SETUP_SECT_INIFILE               "InitFileLocation"
#define ARC_SETUP_SECT_TARGETCONFIG          "TargetConfiguration"
#define ARC_SETUP_SECT_MISCOPTIONS           "MiscOptions"
#define ARC_SETUP_SECT_MULTICORE             "MultiCoreDebug"
#define ARC_SETUP_SECT_DEVICE                "Device"
#define ARC_SETUP_SECT_DEVICE1               "Device1"
#define ARC_SETUP_SECT_DEVICE2               "Device2"                        // this may go further
// items in target configuration section
#define ARC_SETUP_ITEM_ADAPTIVECLOCK         "AdaptiveClock"
#define ARC_SETUP_ITEM_TARGETOPTION          "TargetOptions"
#define ARC_SETUP_ITEM_TAPRESET					"Tap Reset"
// items in misc options
#define ARC_SETUP_ITEM_SHOWCONFIG            "ShowConfiguration"
#define ARC_SETUP_ITEM_PROBEINSTANCE         "ProbeInstance"
// items in multicore section
#define ARC_SETUP_ITEM_NUMBEROFCORES         "NumberOfCores"
// items in deviceX sections
#define ARC_SETUP_ITEM_ARCCORE               "ARC"
#define ARC_SETUP_ITEM_IRWIDTH               "IRWidth"
// item values (bool type)
#define ARC_SETUP_ITEM_TRUE                  "1"
#define ARC_SETUP_ITEM_FALSE                 "0"

// restiction to certain values
#define ARC_SETUP_MAX_DEVICES                16                               // maximum Opella-XD devices
#define ARC_SETUP_MAX_SCANCHAIN_CORES        256                              // maximum cores on scanchain
#define ARC_SETUP_MAX_CORE_IR_LENGTH         32                               // maximum length for IR register for each core

//-------------------------------------------------------------------

// type definition
typedef const char *(*PFAshGetID)(void);
typedef void (*PFAshListConnectedProbes)(char ppszInstanceNumber[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances);
typedef void (*PFAshSetSetupItem)(void *pvPtr, const char *pszSection, const char *pszItem, const char *pszValue);
typedef void (*PFAshGetSetupItem)(void *pvPtr, const char *pszSection, const char *pszItem, char *pszValue);
typedef void (*PFAshAutoDetectScanChain)(void *pvPtr);

typedef struct _TyARCSetupInterface {
   // NOTE: when new functions are being added, ALWAYS add them to the end 
   PFAshGetID               pfGetID;					// pointer to func to obtain DLL ID
   PFAshListConnectedProbes pfListConnectedProbes; // pointer to func to get list of probes
   PFAshGetSetupItem        pfGetSetupItem;        // pointer to func to get setup item
   PFAshSetSetupItem        pfSetSetupItem;        // pointer to func to set setup item
	PFAshAutoDetectScanChain pfAutoDetectScanChain;	// pointer to func to auto detected scan chain cores
   // add new functions here if needed
} TyARCSetupInterface;

typedef int32_t (*PFARCSetupFunction)(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt);                

#endif   // ML_ARC_H_

