/****************************************************************************
       Module: ml_arc.h
     Engineer: Vitezslav Hola
  Description: Headers for ARC driver functions (mid layer).
Date           Initials    Description
08-Oct-2007    VH          Initial
09-Sep-2009    RS		   Added TAP Reset check box
****************************************************************************/
#ifndef ML_ARC_H_
#define ML_ARC_H_

#ifdef __LINUX
#include "../../protocol/common/drvopxd.h"
#else
#include "..\..\protocol\common\drvopxd.h"
#endif

#ifdef __LINUX
// Linux
typedef uint32_t       DWORD;
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#endif //__LINUX

#define FREE(ptr)  free(ptr);\
    ptr = NULL;
// error codes for ARC midlayer
#define ERR_ML_ARC_NO_ERROR                                 0           // no error occured
#define ERR_ML_ARC_INVALID_HANDLE                           1           // invalid handle with parameters
#define ERR_ML_ARC_ALREADY_INITIALIZED                      2           // connection already initialized
#define ERR_ML_ARC_TARGET_NOT_SUPPORTED                     3           // functionality not supported for selected target
#define ERR_ML_ARC_USB_DRIVER_ERROR                         4           // driver internal error
#define ERR_ML_ARC_PROBE_NOT_AVAILABLE                      5           // cannot open connection with probe
#define ERR_ML_ARC_PROBE_INVALID_TPA                        6           // no or invalid TPA used
#define ERR_ML_ARC_PROBE_CONFIG_ERROR                       7           // cannot configure probe
#define ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED                     10          // RTCK timeout expired
#define ERR_ML_ARC_INVALID_FREQUENCY                        50          // invalid frequency selection
#define ERR_ML_ARC_ANGEL_BLASTING_FAILED                    100         // blasting to ARCangel failed
#define ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE            101         // invalid response to extended command
#define ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY              102         // invalid PLL frequency
#define ERR_ML_ARC_TEST_SCANCHAIN_FAILED                    200         // failing test scanchain
#define ERR_ML_ARC_DETECT_SCANCHAIN_FAILED                  201         // detection of scanchain structure failed
#define ERR_ML_ARC_INVALID_SCANCHAIN_STRUCTURE              202         // invalid scanchain structure
#define ERR_ML_ARC_TARGET_RESET_DETECTED                    300         // target reset occured
#define ERR_ML_ARC_DEBUG_ACCESS                             400         // access to ARC debug failed
#define ERR_ML_ARC_DEBUG_PDOWN                              401         // access to ARC debug failed due to powerdown
#define ERR_ML_ARC_DEBUG_POFF                               402         // access to ARC debug failed due to power-off

// filenames for ARC diskware and FPGAware
#define OPXD_DISKWARE_ARC_FILENAME                          "opxddarc.bin"
#define OPXD_DISKWARE_ARC_LPC1837_FILENAME                  "opxddarc_nxplpc.bin"
#define OPXD_FPGAWARE_ARC_FILENAME                          "opxdfarc.bin"
#define OPXD_FPGAWARE_ARC_TPA_R1_FILENAME                   "opxdfarc_tpa_r1.bin"
#define OPXD_FPGAWARE_ARC_LPC1837_FILENAME                  "opxdfarc_nxplpc.bin"
#define OPXD_FIRMWARE_FILENAME                              "opxdfw.bin"
#define OPXD_FIRMWARE_FILENAME_LPC1837                      "opxdfw_nxplpc.bin"
#define OPXD_FLASHUTIL_FILENAME_LPC1837						"opxdflsh_nxplpc.bin"

// error messages
#define MSG_ML_NO_ERROR                                     ""
#define MSG_ML_INVALID_DISKWARE_FILENAME                    "Cannot find Opella-XD ARC diskware file\n"
#define MSG_ML_INVALID_FPGAWARE_FILENAME                    "Cannot find Opella-XD ARC FPGAware file\n"
#define MSG_ML_INVALID_FIRMWARE_FILENAME                    "Cannot find Opella-XD firmware file\n"
#define MSG_ML_INVALID_TPA_CONNECTED                        "Missing or invalid type of TPA connected to Opella-XD\n"
#define MSG_ML_USB_DRIVER_ERROR                             "USB driver error occured\n"
#define MSG_ML_INVALID_DRIVER_HANDLE                        "Invalid driver handle used\n"
#define MSG_ML_PROBE_NOT_AVAILABLE                          "Selected Opella-XD is not available for debug session\n"
#define MSG_ML_INVALID_TPA                                  "Missing or invalid TPA type connected to Opella-XD\n"
#define MSG_ML_PROBE_CONFIG_ERROR                           "Cannot configure Opella-XD with ARC diskware and FPGAware\n"
#define MSG_ML_TARGET_NOT_SUPPORTED                         "Requested feature is not supported for current target\n"
#define MSG_ML_INVALID_FREQUENCY                            "Invalid frequency format used\n"
#define MSG_ML_AA_BLASTING_FAILED                           "Cannot blast FPGA into ARCangel\n"
#define MSG_ML_AA_INVALID_EXTCMD_RESPONSE                   "Unexpected response from ARCangel\n"
#define MSG_ML_AA_INVALID_PLL_FREQUENCY                     "Invalid PLL frequency requested for ARCangel\n"
#define MSG_ML_RTCK_TIMEOUT_EXPIRED                         "Cannot detect RTCK signal from target within specified timeout\n"
#define MSG_ML_DEBUG_ACCESS_FAILED                          "Access to ARC debug interface has failed\n"
#define MSG_ML_SCANCHAIN_TEST_FAILED                        "Scanchain loopback test has failed\n"
#define MSG_ML_INVALID_PROPERTY_FORMAT                      "Invalid property format\n"
#define MSG_ML_UNKNOWN_ERROR                                "Unknown error has occured\n"
#define MSG_ML_TARGET_RESET_DETECTED                        "\nA target initiated reset has been detected\n"
#define MSG_ML_TARGET_POWERED_DOWN                          "\nTarget is Powered-down\n"
#define MSG_ML_TARGET_POWERED_OFF                           "\nTarget is Powered-off\n"

// maximum memory block size
#define MAX_ARC_READ_MEMORY                                 0x4000      // in words (total 64 kB)
#define MAX_ARC_WRITE_MEMORY                                0x4000      // in words (total 64 kB)

#define MAX_ARC_CORES                                       256
#define MAX_ON_TARGET_RESET_ACTIONS                         16

// sleep function prototype
typedef void (*TySleepFunc) (unsigned miliseconds);                     // sleep function prototype
typedef void (*TyProgressBarFunc) (void);                               // callback prototype for progress bar (blasting, etc.)   
typedef void (*PfPrintFunc)(const char *);                              // callback for general print messages

// target types
typedef enum {ARC_TGT_ARCANGEL, ARC_TGT_NORMAL, ARC_TGT_RSTDETECT, ARC_TGT_CJTAG_TPA_R1, ARC_TGT_JTAG_TPA_R1 } TyArcTargetType;

// ARC access types
typedef enum { ML_ARC_ACCESS_MEMORY, ML_ARC_ACCESS_CORE, ML_ARC_ACCESS_AUX, ML_ARC_ACCESS_MADI } TyArcAccessType;

// ARC target reset action types
typedef enum { RESACT_NONE, RESACT_RESTORE_AP, RESACT_RTCK_ENABLE, RESACT_RTCK_DISABLE, RESACT_INIT, RESACT_MEMSET, RESACT_DELAY } TyArcResetActionType;

// ARCangel (AA4) clock allocation and clock types
#define AA_GCLK(x)                                          (x)         // index for ARCangel gclk sources (0 to 3)
typedef enum { AA_GCLK_HIGHIMP, AA_GCLK_CRYSTAL, AA_GCLK_DIPS, AA_GCLK_HOST, AA_GCLK_MCLK, AA_GCLK_VCLK } TyArcAAGclk;

// structures used in ARC midlayer
typedef struct _TyARCCoreParams {
   uint32_t ulScanchainCore;                                       // core index on scanchain
   uint32_t ulCoreVersion;                                         // core version
   unsigned short usAccessType;                                         // encoded access parameters
} TyARCCoreParams;

typedef struct _TyScanchainCoreParams {
   // core information
   unsigned char bARCCore;                                              // flag if this core is ARC or not
   uint32_t ulIRLength;                                            // IR length
} TyScanchainCoreParams;

// clock allocation for ARCangel (currently only AA4 supports it)
typedef struct _TyAAClockAllocation {
   TyArcAAGclk ptyGclk[4];                                              // routing for GCLK0, GCLK1, GCLK2 and GCLK3
   double dMclkFrequency;                                               // frequency for PLL MCLK
   double dVclkFrequency;                                               // frequency for PLL VCLK
   unsigned char bAllocateVclkNext;                                     // flag to identify which PLL clock source reallocate next
   unsigned char bRestoreAfterBlasting;                                 // should routing be restored after blasting
} TyAAClockAllocation;

// firmware/diskware version
typedef struct _TyProbeVersion {
   uint32_t ulVersion;                                             // version
   uint32_t ulDate;                                                // date
} TyProbeVersion;

// ARC target reset actions
typedef struct _TyOnTargetResetActions
{
   uint32_t uiNumberOfActions;
   TyArcResetActionType ptyActions[MAX_ON_TARGET_RESET_ACTIONS];
   uint32_t pulActionParam1[MAX_ON_TARGET_RESET_ACTIONS];
   uint32_t pulActionParam2[MAX_ON_TARGET_RESET_ACTIONS];
   uint32_t pulActionParam3[MAX_ON_TARGET_RESET_ACTIONS];
} TyOnTargetResetActions;

typedef struct _TyArcTargetParams {
   unsigned char bConnectionInitialized;
   // target settings
   TyArcTargetType tyArcTargetType;                                     // ARC target type
   double dJtagFrequency;                                               // JTAG frequency in MHz
   unsigned char bAdaptiveClockEnabled;                                 // adaptive clock flag
   unsigned char bDoNotIssueTAPReset;
   uint32_t ulAdaptiveClockTimeout;                                // adaptive clock timeout
   uint32_t ulDelayAfterReset;                                     // delay after reset in ms
   uint32_t ulResetDuration;                                       // reset duration in ms
   // ARCangel specific settings
   double dBlastFrequency;                                              // blasting frequency in MHz
   TyAAClockAllocation tyAAClockAlloc;                                  // allocation of AA4 GCLK sources
   // target supporting reset sensing
   unsigned char bTargetResetDetectRecursive;                           // target reset detection recurse
   TyOnTargetResetActions tyOnTargetResetActions;                       // what to do when target reset detected

   // core(s) settings
   uint32_t ulCurrentARCCore;                                      // current ARC core (starting from 0)
   uint32_t ulNumberOfARCCores;                                    // number of ARC cores on scanchain
   uint32_t ulNumberOfCoresOnScanchain;                            // number of cores on scanchain 
   TyARCCoreParams tyARCCoreParams[MAX_ARC_CORES];                      // array with information about ARC cores
   TyScanchainCoreParams tyScanchainCoreParams[MAX_ARC_CORES];          // array with information about cores on scanchain

   // common parameters for all ARC cores
   unsigned char bCycleStep;                                            // cycle step flag

   // function pointers
   TySleepFunc pfSleepFunc;                                             // pointer to sleep function
   TyProgressBarFunc pfProgressBarFunc;                                 // pointer to progress bar function

   // probe information
   char pszProbeInstanceNumber[16];                                     // probe instance serial number
   int32_t iCurrentHandle;                                                  // current probe handle
   TyProbeVersion tyProbeDiskwareInfo;                                  // probe diskware info
   TyProbeVersion tyProbeFirmwareInfo;                                  // probe firmware info
   TyProbeVersion tyProbeFPGAwareInfo;                                  // probe FPGAware info
   // other information
   int32_t iLastErrorCode;                                                  // last error code (if any error happened)  
   TyMultipleScanParams tyMultipleScanParams;
} TyArcTargetParams, *PTyArcTargetParams;

// function prototypes in ARC midlayer
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances);
int32_t ML_UpgradeFirmware(TyArcTargetParams *ptyArcParams, PfPrintFunc pfPrintFunc);
int32_t ML_OpenConnection(TyArcTargetParams *ptyArcParams);
int32_t ML_CloseConnection(TyArcTargetParams *ptyArcParams);
int32_t ML_TestScanchain(TyArcTargetParams *ptyArcParams, unsigned char bVerifyNumberOfCores);
int32_t ML_DetectScanchain(TyArcTargetParams *ptyArcParams, uint32_t uiMaxCores, uint32_t *puiNumberOfCores, 
                       uint32_t *pulIRWidth, unsigned char *pbARCCores);
int32_t ML_UpdateScanchain(TyArcTargetParams *ptyArcParams);

// function specific for ARCangel targets
int32_t ML_BlastFileToFpga(TyArcTargetParams *ptyArcParams, const char *pszXbfFilename);
int32_t ML_BlastMemToFpga(TyArcTargetParams *ptyArcParams, const char *pszMemDetails);
int32_t ML_AAExtendedCommand(TyArcTargetParams *ptyArcParams, unsigned char ucCommand, uint32_t ulParameter, unsigned short *pusResponse);
int32_t ML_AASetRoboClock(TyArcTargetParams *ptyArcParams, unsigned char ucCommand, unsigned short usValue, unsigned short *pusResponse);
int32_t ML_AAGetClockAllocation(TyArcTargetParams *ptyArcParams, int32_t iGclkSel, TyArcAAGclk *ptyGclkRoute, double *pdPllFrequency);
int32_t ML_AASetClockAllocation(TyArcTargetParams *ptyArcParams, int32_t iGclkSel, TyArcAAGclk tyGclkRoute, double dPllFrequency);
int32_t ML_AACreatePllStream(TyArcAAGclk tyClkSource, double dFrequency, uint32_t *pulPllValue);
int32_t ML_AASetClockConfiguration(TyArcTargetParams *ptyArcParams);

// function supporting target reset
int32_t ML_SetResetProperty(TyArcTargetParams *ptyArcParams, unsigned char bSetResetDelay, uint32_t *piValue);
int32_t ML_ResetTarget(TyArcTargetParams *ptyArcParams);
int32_t ML_SetOnTargetResetAction(TyArcTargetParams *ptyArcParams, TyArcResetActionType tyActionType, uint32_t *pulParameters);
// function setting frequency
int32_t ML_SetJtagFrequency(TyArcTargetParams *ptyArcParams, double dFrequency);
int32_t ML_SetBlastFrequency(TyArcTargetParams *ptyArcParams, double dFrequency);
unsigned char ML_UsingAdaptiveJtagClock(TyArcTargetParams *ptyArcParams);
int32_t ML_ToggleAdaptiveJtagClock(TyArcTargetParams *ptyArcParams, unsigned char bEnableAdaptiveClock);
// read/write functions
int32_t ML_WriteBlock(TyArcTargetParams *ptyArcParams, TyArcAccessType tyAccessType, uint32_t ulAddress, uint32_t ulCount, uint32_t *pulData);
int32_t ML_ReadBlock(TyArcTargetParams *ptyArcParams, TyArcAccessType tyAccessType, uint32_t ulAddress, uint32_t ulCount, uint32_t *pulData);
// function setting target parameters
int32_t ML_InvalidatingICache(TyArcTargetParams *ptyArcParams, unsigned char bDisableICacheInvalidation);
int32_t ML_SetTargetEndian(TyArcTargetParams *ptyArcParams, unsigned char bBigEndianTarget);
unsigned char ML_IsCpuBE(TyArcTargetParams *ptyArcParams);
int32_t ML_SetCurrentCpu(TyArcTargetParams *ptyArcParams, uint32_t ulCpuNum);
int32_t ML_SetCycleStep(TyArcTargetParams *ptyArcParams, unsigned char bCycleStep);
unsigned char ML_GetCycleStep(TyArcTargetParams *ptyArcParams);
int32_t ML_SetAdaptiveClockTimeout(TyArcTargetParams *ptyArcParams, uint32_t ulTimeout);
int32_t ML_SetMemoryAccessOptimization(TyArcTargetParams *ptyArcParams, unsigned char bEnableOptimizedAccess);
int32_t ML_SetAddressAutoincrement(TyArcTargetParams *ptyArcParams, unsigned char bEnableAddressAutoincrement);
unsigned char ML_IsTargetARCangel(TyArcTargetParams *ptyArcParams);
void ML_SetCpuVersionNumber(TyArcTargetParams *ptyArcParams, uint32_t ulVersionNumber);
uint32_t ML_GetCpuVersionNumber(TyArcTargetParams *ptyArcParams);
int32_t ML_IndicateTargetStatusChange(TyArcTargetParams *ptyArcParams);
int32_t ML_UpdateProbeSettings(TyArcTargetParams *ptyArcParams);
int32_t ML_IntializeCJTAG(TyArcTargetParams *ptyArcParams);


// additional functions for JTAG console
int32_t ML_ScanIR(TyArcTargetParams *ptyArcParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
int32_t ML_ScanDR(TyArcTargetParams *ptyArcParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
int32_t ML_ResetTAP(TyArcTargetParams *ptyArcParams,int32_t bResetType);
int32_t ML_ScanMultiple(TyArcTargetParams *ptyArcParams, TyMultipleScanParams *ptyMultipleScanParams,
                    uint32_t *pulDataIn, uint32_t *pulDataOut);
// convert functions
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency);
char* ML_GetErrorMessage(int32_t iReturnCode);

// other functions
void ML_SetDllPath(const char *pszDllPath);
void ML_GetDllPath(char *pszDllPath, uint32_t uiSize);
void ML_Sleep(TyArcTargetParams *ptyArcParams, uint32_t ulMiliseconds);
#ifdef __LINUX
// Linux
//extern void usleep(int32_t);
extern void Sleep(uint32_t  mseconds );
#endif //__LINUX

#endif   // ML_ARC_H_

