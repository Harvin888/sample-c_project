/******************************************************************************
       Module: ARCSetupDlg.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of ARC setup dialog window for GUI DLL
  Date           Initials    Description
  15-Oct-2007    VH          Initial
  09-Sep-2009    RSB         Added checkBox for TAP reset
******************************************************************************/
#include "stdafx.h"
#include "protocol\common\types.h"
#include "arcgui.h"
#include "ARCSetupDlg.h"
#include "autodetect.h"
#include "../arcsetup.h"							// interface header
#include <string.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TARGET_SELECTION_ARCANGEL            0
#define TARGET_SELECTION_ARC                 1
#define TARGET_SELECTION_SIANO               2
#define TARGET_SELECTION_CJTAG_TPA_R1        3
#define TARGET_SELECTION_JTAG_TPA_R1         4

// CARCSetupDlg dialog

/****************************************************************************
     Function: CARCSetupDlg::CARCSetupDlg
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: object constructor
Date           Initials    Description
15-Oct-2007    VH          Initial
09-Sep-2009    RSB         Added checkBox for TAP reset
12-Apr-2012    SPT         Changed initial selection to RC normal target TPA Rev. 0
****************************************************************************/
CARCSetupDlg::CARCSetupDlg(CWnd* pParent /*=NULL*/)	: CDialog(CARCSetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CARCSetupDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	strcpy(m_pszVersionLabel, "");
   m_usProbesConnected   = 0;
   m_usCurrentProbe      = 0;
   m_bAdaptiveClock      = 0;
	m_BDonotIssueTAPReset = 0;
   m_uiTargetSelection   = 1;	// target selected as ARC Normal TPA rev. 0

    // init MC (1 ARC core)
   for (int32_t iIndex=0; iIndex < ARC_SETUP_MAX_SCANCHAIN_CORES; iIndex++)
   {
		m_pbMCARCCores[iIndex]   = 1;                        // assuming ARC cores
      m_puiMCCoreWidth[iIndex] = 4;                       // 4 bits in IR
   }
	m_uiNumberOfCores     = 1;
	m_uiFirstCore         = 0;
	pvARCPtr              = NULL;
	pfListProbes          = NULL;
   pfGetSetupItem        = NULL;
	pfSetSetupItem        = NULL;
	pfAutoDetectScanChain = NULL;

	// initialize array of indexes for MC
	ppuiDlgIndex[0][0] = IDC_LABEL_CORE0; ppuiDlgIndex[0][1] = IDC_CHECK_CORE0;  ppuiDlgIndex[0][2] = IDC_LABEL_LEN_0;   ppuiDlgIndex[0][3] = IDC_COMBO_CORE0;       
	ppuiDlgIndex[1][0] = IDC_LABEL_CORE1; ppuiDlgIndex[1][1] = IDC_CHECK_CORE1;  ppuiDlgIndex[1][2] = IDC_LABEL_LEN_1;   ppuiDlgIndex[1][3] = IDC_COMBO_CORE1;       
	ppuiDlgIndex[2][0] = IDC_LABEL_CORE2; ppuiDlgIndex[2][1] = IDC_CHECK_CORE2;  ppuiDlgIndex[2][2] = IDC_LABEL_LEN_2;   ppuiDlgIndex[2][3] = IDC_COMBO_CORE2;       
	ppuiDlgIndex[3][0] = IDC_LABEL_CORE3; ppuiDlgIndex[3][1] = IDC_CHECK_CORE3;  ppuiDlgIndex[3][2] = IDC_LABEL_LEN_3;   ppuiDlgIndex[3][3] = IDC_COMBO_CORE3;       
	ppuiDlgIndex[4][0] = IDC_LABEL_CORE4; ppuiDlgIndex[4][1] = IDC_CHECK_CORE4;  ppuiDlgIndex[4][2] = IDC_LABEL_LEN_4;   ppuiDlgIndex[4][3] = IDC_COMBO_CORE4;       
}

void CARCSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CARCSetupDlg)
	DDX_Control(pDX, IDC_SCROLLBAR_MC, m_ScrollBar);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CARCSetupDlg, CDialog)
	//{{AFX_MSG_MAP(CARCSetupDlg)
	ON_CBN_DROPDOWN(IDC_COMBO_INSTANCES, OnDropdownComboInstances)
	ON_BN_CLICKED(IDC_BUTTON_AUTO_DETECT, OnButtonAutoDetect)
	ON_BN_CLICKED(IDC_RTCK_CHECK, OnRtckCheck)
	ON_BN_CLICKED(IDC_RADIO_TGTOPT1, OnRadioTgtopt1)
	ON_BN_CLICKED(IDC_RADIO_TGTOPT2, OnRadioTgtopt2)
	ON_BN_CLICKED(IDC_RADIO_TGTOPT3, OnRadioTgtopt3)
	ON_CBN_SELCHANGE(IDC_COMBO_SCAN_CORES, OnSelchangeComboScanCores)
	ON_BN_CLICKED(IDC_CHECK_CORE0, OnCheckCore0)
	ON_BN_CLICKED(IDC_CHECK_CORE1, OnCheckCore1)
	ON_BN_CLICKED(IDC_CHECK_CORE2, OnCheckCore2)
	ON_BN_CLICKED(IDC_CHECK_CORE3, OnCheckCore3)
	ON_BN_CLICKED(IDC_CHECK_CORE4, OnCheckCore4)
	ON_WM_VSCROLL()
	ON_CBN_SELCHANGE(IDC_COMBO_CORE0, OnSelchangeComboCore0)
	ON_CBN_SELCHANGE(IDC_COMBO_CORE1, OnSelchangeComboCore1)
	ON_CBN_SELCHANGE(IDC_COMBO_CORE2, OnSelchangeComboCore2)
	ON_CBN_SELCHANGE(IDC_COMBO_CORE3, OnSelchangeComboCore3)
	ON_CBN_SELCHANGE(IDC_COMBO_CORE4, OnSelchangeComboCore4)
	ON_BN_CLICKED(IDC_CHECK_TAP, OnCheckTap)
	ON_BN_CLICKED(IDC_RADIO_TGTOPT4, OnRadioTgtopt4)
	ON_BN_CLICKED(IDC_RADIO_TGTOPT5, OnRadioTgtopt5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//---------------------------------------------------------------------------
//--- CARCSetupDlg message handlers                                       ---
//---------------------------------------------------------------------------

/****************************************************************************
     Function: CARCSetupDlg::OnOK
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Handler for pressing "OK" button
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnOK() 
{
   // check which probe is currently selected
   if (m_usProbesConnected)
      {
      int32_t iCurProbe = ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->GetCurSel();
      if ((iCurProbe >= 0) && (iCurProbe < (int32_t)m_usProbesConnected))
         m_usCurrentProbe = (unsigned short)iCurProbe;
      else
         m_usCurrentProbe = 0;
      }
   CDialog::OnOK();
}

/****************************************************************************
     Function: CARCSetupDlg::OnInitDialog
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: OnInitDialog event
Date           Initials    Description
15-Oct-2007    VH          Initial
09-Sep-2009    RSB         Added checkBox for TAP reset
10-Apr-2017    AS				Added INI file location
****************************************************************************/
BOOL CARCSetupDlg::OnInitDialog() 
{
	 CDialog::OnInitDialog();
    char pszLocString[10];

    ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT1))->SetCheck(FALSE);
    ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT2))->SetCheck(FALSE);
    ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT3))->SetCheck(FALSE);
    ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT4))->SetCheck(FALSE);
    ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT5))->SetCheck(FALSE);

	 // fill ini file location and set read only
	 ((CEdit *)GetDlgItem(IDC_EDIT_INI_FILE))->SetReadOnly(TRUE);
	 ((CEdit *)GetDlgItem(IDC_EDIT_INI_FILE))->SetWindowText(m_pszIniFileLocation);

    // fill list of connected probes
	 FillConnectedProbeList();

    // fill all combos for IR width
    for (int32_t iRow=0; iRow < 5; iRow++)
       {
       ((CComboBox *)GetDlgItem(ppuiDlgIndex[iRow][3]))->ResetContent();
       for (int32_t iIndex=0; iIndex < ARC_SETUP_MAX_CORE_IR_LENGTH; iIndex++)
          {
          sprintf(pszLocString, "%d", iIndex + 1);
          ((CComboBox *)GetDlgItem(ppuiDlgIndex[iRow][3]))->AddString(pszLocString);
          }
       }
    // define combo for number of cores
    ((CComboBox *)GetDlgItem(IDC_COMBO_SCAN_CORES))->ResetContent();
    for (int32_t iIndex=0; iIndex < ARC_SETUP_MAX_SCANCHAIN_CORES; iIndex++)
       {
       sprintf(pszLocString, "%d", iIndex);
       ((CComboBox *)GetDlgItem(IDC_COMBO_SCAN_CORES))->AddString(pszLocString);
       }

	 ((CComboBox *)GetDlgItem(IDC_COMBO_SCAN_CORES))->SetCurSel(m_uiNumberOfCores);
    
	 // set target selection
    switch (m_uiTargetSelection)
       {
       case TARGET_SELECTION_ARCANGEL:
          ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT1))->SetCheck(TRUE);
          break;
       case TARGET_SELECTION_SIANO:
          ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT3))->SetCheck(TRUE);
          break;
	   case TARGET_SELECTION_CJTAG_TPA_R1:
		   ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT4))->SetCheck(TRUE);
		   break;
	   case TARGET_SELECTION_JTAG_TPA_R1:
		   ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT5))->SetCheck(TRUE);
		   break;	
       case TARGET_SELECTION_ARC:
       default:
          ((CButton *)GetDlgItem(IDC_RADIO_TGTOPT2))->SetCheck(TRUE);
          break;
       }
    ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->EnableWindow(TRUE);
    ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->SetCheck(0);
    if (m_bAdaptiveClock)
       ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->SetCheck(1);
    else
       ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->SetCheck(0);

    if (m_BDonotIssueTAPReset)
		((CButton *)GetDlgItem(IDC_CHECK_TAP))->SetCheck(1);
    else
       ((CButton *)GetDlgItem(IDC_CHECK_TAP))->SetCheck(0);
    // make sure target options are consistent
    CheckTargetOptions();
    // show initial multicore structure
    ShowMCStructure();
	return TRUE;
}

/****************************************************************************
     Function: CARCSetupDlg::FillConnectedProbeList
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: fill list of connected Opella-XD probes into combo box
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::FillConnectedProbeList(void)
{
   unsigned short usIndex;
   // check if any probe is connected
   m_usProbesConnected = 0;
   if (pfListProbes != NULL)
      ((PFAshListConnectedProbes)pfListProbes)(m_pszProbeList, 16, &m_usProbesConnected);

   ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->ResetContent();
   if (m_usProbesConnected)
      {
      ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->ResetContent();
      for (usIndex=0; usIndex < m_usProbesConnected; usIndex++)
         {
         ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->AddString(m_pszProbeList[usIndex]);
         }
      ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->SetCurSel(0);
      m_usCurrentProbe = 0;
      }
   else
      {
      ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->AddString("No Opella-XD Found");
      ((CComboBox *)GetDlgItem(IDC_COMBO_INSTANCES))->SetCurSel(0);
      m_usCurrentProbe = 0;
      }
}

/****************************************************************************
     Function: CARCSetupDlg::CheckTargetOptions
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: check target selection and decide if adaptive clock option is
               available to user
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::CheckTargetOptions(void)
{
	char pszValue[3];

	//target option needs to be saved straight away for auto detect scan chain
	PFAshSetSetupItem pfSetItem = (PFAshSetSetupItem)CARCSetupDlg::pfSetSetupItem;

	sprintf(pszValue, "%d", m_uiTargetSelection);
	pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);

   if (m_uiTargetSelection == TARGET_SELECTION_ARCANGEL)
      {     // using ARCangel, adaptive clock is not available
      m_bAdaptiveClock = 0;
      ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->EnableWindow(FALSE);           // disable option
      ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->SetCheck(0);
      ((CButton *)GetDlgItem(IDC_CHECK_TAP))->EnableWindow(TRUE);
      }
   else if (m_uiTargetSelection == TARGET_SELECTION_CJTAG_TPA_R1)
      {
	   // using ARC TARGET with cJTAG, adaptive clock is not available
	   m_bAdaptiveClock = 0;
	   ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->EnableWindow(FALSE);           // disable option
      ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->SetCheck(0);
      ((CButton *)GetDlgItem(IDC_CHECK_TAP))->EnableWindow(FALSE);           // disable option
      ((CButton *)GetDlgItem(IDC_CHECK_TAP))->SetCheck(1);
      m_BDonotIssueTAPReset=1;
      }
   else
      {     // other target, just ensure option is enabled
      ((CButton *)GetDlgItem(IDC_RTCK_CHECK))->EnableWindow(TRUE);
      ((CButton *)GetDlgItem(IDC_CHECK_TAP))->EnableWindow(TRUE);
      }
}

/****************************************************************************
     Function: CARCSetupDlg::ShowMCStructure
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show information about multicore structure in the dialog
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::ShowMCStructure(void)
{
	uint32_t uiIndex;
   int32_t          iRow, iCol;
   char         pszLocString[20];

   if (m_uiNumberOfCores <= 5)
      m_uiFirstCore = 0;                // too few cores to work with progress bar
   else if (m_uiFirstCore > (m_uiNumberOfCores - 5))
      m_uiFirstCore = 0;                // out of range for progress bar

   // set progress bar size
   if (m_uiNumberOfCores > 5)
      {
      ((CScrollBar *)GetDlgItem(IDC_SCROLLBAR_MC))->SetScrollRange(0, m_uiNumberOfCores - 5);
      ((CScrollBar *)GetDlgItem(IDC_SCROLLBAR_MC))->SetScrollPos(m_uiFirstCore);
      }
   else
      {
      ((CScrollBar *)GetDlgItem(IDC_SCROLLBAR_MC))->SetScrollRange(0, 0);
      ((CScrollBar *)GetDlgItem(IDC_SCROLLBAR_MC))->SetScrollPos(0);
      }

   // start filling information about cores
   uiIndex = m_uiFirstCore;
   iRow = 0;
   for (iRow = 0; iRow < 5; iRow++)
      {
      if (uiIndex < m_uiNumberOfCores)
         {
         // show core type
         if (m_pbMCARCCores[uiIndex])
            {     // this is ARC core
            ((CButton *)GetDlgItem(ppuiDlgIndex[iRow][1]))->SetCheck(1);               // set checkbox
            GetDlgItem(ppuiDlgIndex[iRow][3])->EnableWindow(FALSE);                    // disable IR width for ARC
            m_puiMCCoreWidth[uiIndex] = 4;
            }
         else
            {
            ((CButton *)GetDlgItem(ppuiDlgIndex[iRow][1]))->SetCheck(0);
            GetDlgItem(ppuiDlgIndex[iRow][3])->EnableWindow(TRUE);                     // enable IR width
            }
         // show IR width
         ((CComboBox *)GetDlgItem(ppuiDlgIndex[iRow][3]))->SetCurSel(m_puiMCCoreWidth[uiIndex]-1);
         sprintf(pszLocString, "Device %d", uiIndex + 1);
         GetDlgItem(ppuiDlgIndex[iRow][0])->SetWindowText(pszLocString);               // set device name
         for (iCol=0; iCol < 4; iCol++)
            GetDlgItem(ppuiDlgIndex[iRow][iCol])->ShowWindow(SW_SHOW);                 // show whole line
         }
      else
         {     // hide whole row
         for (iCol=0; iCol < 4; iCol++)
            GetDlgItem(ppuiDlgIndex[iRow][iCol])->ShowWindow(SW_HIDE);
         }
      uiIndex++;
      }

   // just set non-used variables to default
   for (uiIndex=m_uiNumberOfCores; uiIndex < ARC_SETUP_MAX_SCANCHAIN_CORES; uiIndex++)
      {
      m_pbMCARCCores[uiIndex]   = 1; // assuming ARC cores
      m_puiMCCoreWidth[uiIndex] = 4; // 4 bits in IR
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnDropdownComboInstances
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: dropdown event for combo box
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnDropdownComboInstances() 
{
	FillConnectedProbeList();	
}

/****************************************************************************
     Function: OnButtonAutoDetect
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: button event for auto-detect scan chain
Date           Initials    Description
10-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::OnButtonAutoDetect()
{
   char         pszValue[100], pszLocCore[ARC_SETUP_MAX_SCANCHAIN_CORES];
	uint32_t uiIndex;

   PFAshGetSetupItem        pfGetSetupItem        = (PFAshGetSetupItem)CARCSetupDlg::pfGetSetupItem;
   PFAshAutoDetectScanChain pfAutoDetectScanChain = (PFAshAutoDetectScanChain)CARCSetupDlg::pfAutoDetectScanChain;

	CAutoDetect cAutoDetect;

	cAutoDetect.pvARCPtr              = pvARCPtr;
	cAutoDetect.pfGetSetupItem        = pfGetSetupItem;
	cAutoDetect.pfAutoDetectScanChain = pfAutoDetectScanChain;

	if (cAutoDetect.DoModal() == IDCANCEL)
		return;

	//get number of cores
   pfGetSetupItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);
	m_uiNumberOfCores = strtoul(pszValue, NULL, 10);

	((CComboBox *)GetDlgItem(IDC_COMBO_SCAN_CORES))->SetCurSel(m_uiNumberOfCores);

	for (uiIndex = 0; uiIndex < m_uiNumberOfCores; uiIndex++)
   {
		sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiIndex+1);
		pfGetSetupItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, pszValue);

		if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
			m_pbMCARCCores[uiIndex] = 1;
		else
			m_pbMCARCCores[uiIndex] = 0;

		pfGetSetupItem(pvARCPtr, ARC_SETUP_SECT_DEVICE, ARC_SETUP_ITEM_IRWIDTH, pszValue);
		m_puiMCCoreWidth[uiIndex] = strtoul(pszValue, NULL, 10);
   }

	//store modified target option 
	pfGetSetupItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
	m_uiTargetSelection = strtoul(pszValue, NULL, 10);

	ShowMCStructure();

	((CButton *)GetDlgItem(IDC_RADIO_TGTOPT1))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_RADIO_TGTOPT3))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_RADIO_TGTOPT4))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_RADIO_TGTOPT5))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_RADIO_TGTOPT2))->SetCheck(FALSE);

	// set target selection
	switch (m_uiTargetSelection)
	{
		case TARGET_SELECTION_ARCANGEL:
			((CButton *)GetDlgItem(IDC_RADIO_TGTOPT1))->SetCheck(TRUE);
			break;
		case TARGET_SELECTION_SIANO:
			((CButton *)GetDlgItem(IDC_RADIO_TGTOPT3))->SetCheck(TRUE);
			break;
		case TARGET_SELECTION_CJTAG_TPA_R1:
			((CButton *)GetDlgItem(IDC_RADIO_TGTOPT4))->SetCheck(TRUE);
			break;
		case TARGET_SELECTION_JTAG_TPA_R1:
			((CButton *)GetDlgItem(IDC_RADIO_TGTOPT5))->SetCheck(TRUE);
			break;	
		case TARGET_SELECTION_ARC:
		default:
			((CButton *)GetDlgItem(IDC_RADIO_TGTOPT2))->SetCheck(TRUE);
			break;
	}

	CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::LoadConfigurationFromCaller
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: loading setup configuration from calling DLL
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::LoadConfigurationFromCaller(void)
{
   char              pszValue[100];
   uint32_t      uiCores   = 0;
   PFAshGetSetupItem pfGetItem = (PFAshGetSetupItem)CARCSetupDlg::pfGetSetupItem;

   if (pfGetItem == NULL)
      return;
   strcpy(pszValue, "");
   
	pfGetItem(pvARCPtr, ARC_SETUP_SECT_INIFILE, ARC_SETUP_SECT_INIFILE, pszValue);
	if (strcmp(pszValue, ""))
		strcpy(m_pszIniFileLocation, pszValue);
	else
		strcpy(m_pszIniFileLocation, "Unknown");

	// get adaptive clock settings
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, pszValue);
   if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
      m_bAdaptiveClock = 1;
   else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
      m_bAdaptiveClock = 0;
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, pszValue);
   if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
	   m_BDonotIssueTAPReset = 1;
   else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
      m_BDonotIssueTAPReset = 0;
   // get target settings
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
   if (strlen(pszValue) == 1)
      {
      switch (pszValue[0])
         {
         case '0':
            m_uiTargetSelection = TARGET_SELECTION_ARCANGEL;
            break;
         case '1':
            m_uiTargetSelection = TARGET_SELECTION_ARC;
            break;
		 case '3':
            m_uiTargetSelection = TARGET_SELECTION_CJTAG_TPA_R1;
            break;
		 case '4':
            m_uiTargetSelection = TARGET_SELECTION_JTAG_TPA_R1;
            break;
         case '2':
            m_uiTargetSelection = TARGET_SELECTION_SIANO;
            break;
         default:
            break;
         }
      }
   // get information about MC
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);
   if ((sscanf(pszValue, "%d", &uiCores) == 1) && (uiCores <= ARC_SETUP_MAX_SCANCHAIN_CORES))
      {  // got number of cores
      m_uiNumberOfCores = uiCores;
      for (uiCores=0; uiCores < m_uiNumberOfCores; uiCores++)
         {  // get info for each core
         char pszLocCore[20];
         int32_t iWidth = 0;
         sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiCores+1);              // get section name
         pfGetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, pszValue);
         if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
            m_pbMCARCCores[uiCores] = 0;
         else
            m_pbMCARCCores[uiCores] = 1;                                             // if invalid value, we assume ARC core
         pfGetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_IRWIDTH, pszValue);
         if ((sscanf(pszValue, "%d", &iWidth) == 1) && (iWidth >= 1) && (iWidth <= ARC_SETUP_MAX_CORE_IR_LENGTH))
            m_puiMCCoreWidth[uiCores] = iWidth;
         else
            m_puiMCCoreWidth[uiCores] = iWidth;                                       // if invalid value, use 4 bits
         }
      }
}

/****************************************************************************
     Function: CARCSetupDlg::StoreConfigurationToCaller
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: storing setup configuration to calling DLL
Date           Initials    Description
16-Oct-2007    VH          Initial
09-Sep-2009    RSB         Added checkBox for TAP reset
12-Apr-2012    SPT         Added cJTAG support
****************************************************************************/
void CARCSetupDlg::StoreConfigurationToCaller(void)
{
   uint32_t uiIndex;
   char         pszValue[100];

   PFAshSetSetupItem pfSetItem = (PFAshSetSetupItem)CARCSetupDlg::pfSetSetupItem;
   if (pfSetItem == NULL)
      return;

   // store adaptive clock settings
   if (m_bAdaptiveClock)
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, ARC_SETUP_ITEM_TRUE);
   else
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, ARC_SETUP_ITEM_FALSE);
   if (m_BDonotIssueTAPReset)
	   pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, ARC_SETUP_ITEM_TRUE);
   else
	   pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, ARC_SETUP_ITEM_FALSE);

   // store target selection
   switch (m_uiTargetSelection)
      {
      case TARGET_SELECTION_ARCANGEL:           // ARCangel target
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "0");
         break;
      case TARGET_SELECTION_SIANO:           // Siano target
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "2");
         break;
	  case TARGET_SELECTION_CJTAG_TPA_R1:           // ARC Target cJTAG TPA Rev. 1
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "3");
         break;
	  case TARGET_SELECTION_JTAG_TPA_R1:           // ARC target JTAG TPA Rev. 1
		  pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "4");
         break;
      case TARGET_SELECTION_ARC:           // ARC target
      default:
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "1");
         break;
      }
   // store instance number
   if (m_usProbesConnected)
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, m_pszProbeList[m_usCurrentProbe]);
   else
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, "");              // no probe connected
   // store information about MC
   sprintf(pszValue, "%d", m_uiNumberOfCores);
   pfSetItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);             // number of cores in MC
   for (uiIndex=0; uiIndex < m_uiNumberOfCores; uiIndex++)
      {  // set setup for each core
      char pszLocCore[20];
      sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiIndex+1);                                   // set section name
      if (m_pbMCARCCores[uiIndex])                                                                     // set if core is ARC or not
         pfSetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, ARC_SETUP_ITEM_TRUE);
      else
         pfSetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, ARC_SETUP_ITEM_FALSE);
      sprintf(pszValue, "%d", m_puiMCCoreWidth[uiIndex]);                                               // set number of bits in IR
      pfSetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_IRWIDTH, pszValue);
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnRtckCheck
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Click on adaptive clock box
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnRtckCheck() 
{
   if (((CButton *)GetDlgItem(IDC_RTCK_CHECK))->GetCheck())
      m_bAdaptiveClock = 1;
   else
      m_bAdaptiveClock = 0;
}

/****************************************************************************
     Function: CARCSetupDlg::OnRadioTgtopt1
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Click on ARCangel selection
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnRadioTgtopt1() 
{
   m_uiTargetSelection = TARGET_SELECTION_ARCANGEL;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::OnRadioTgtopt2
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Click on ARC selection
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnRadioTgtopt2() 
{
   m_uiTargetSelection = TARGET_SELECTION_ARC;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::OnRadioTgtopt3
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Click on Siano selection
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnRadioTgtopt3() 
{
   m_uiTargetSelection = TARGET_SELECTION_SIANO;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::OnRadioTgtopt4
     Engineer: Shameerudheen P T
        Input: none
       Output: none
  Description: Click on cJTAG selection with TPA Rev. 1
Date           Initials    Description
09-Apr-2012    SPT         Initial
****************************************************************************/
void CARCSetupDlg::OnRadioTgtopt4() 
{
   m_uiTargetSelection = TARGET_SELECTION_CJTAG_TPA_R1;
   CheckTargetOptions();	
}
/****************************************************************************
     Function: CARCSetupDlg::OnRadioTgtopt5
     Engineer: Shameerudheen P T
        Input: none
       Output: none
  Description: Click on JTAG selection with TPA Rev. 1
Date           Initials    Description
09-Apr-2012    SPT         Initial
****************************************************************************/
void CARCSetupDlg::OnRadioTgtopt5() 
{
   m_uiTargetSelection = TARGET_SELECTION_JTAG_TPA_R1;
   CheckTargetOptions();	
}
/****************************************************************************
     Function: CARCSetupDlg::OnSelchangeComboScanCores
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Changed MC cores combo
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnSelchangeComboScanCores() 
{
	uint32_t uiCores = ((CComboBox *)GetDlgItem(IDC_COMBO_SCAN_CORES))->GetCurSel();

   if ((uiCores >= 0) && (uiCores < ARC_SETUP_MAX_SCANCHAIN_CORES))
      {
      m_uiNumberOfCores = uiCores;
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnCheckCore0
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Click on checkbox core 0
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnCheckCore0() 
{
   if (((CButton *)GetDlgItem(IDC_CHECK_CORE0))->GetCheck())
      m_pbMCARCCores[m_uiFirstCore + 0] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 0] = 0;
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::OnCheckCore1
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Click on checkbox core 1
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnCheckCore1() 
{
   if (((CButton *)GetDlgItem(IDC_CHECK_CORE1))->GetCheck())
      m_pbMCARCCores[m_uiFirstCore + 1] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 1] = 0;
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::OnCheckCore2
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Click on checkbox core 2
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnCheckCore2()
{
   if (((CButton *)GetDlgItem(IDC_CHECK_CORE2))->GetCheck())
      m_pbMCARCCores[m_uiFirstCore + 2] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 2] = 0;
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::OnCheckCore3
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Click on checkbox core 3
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnCheckCore3() 
{
   if (((CButton *)GetDlgItem(IDC_CHECK_CORE3))->GetCheck())
      m_pbMCARCCores[m_uiFirstCore + 3] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 3] = 0;
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::OnCheckCore4
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Click on checkbox core 4
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnCheckCore4()
{
   if (((CButton *)GetDlgItem(IDC_CHECK_CORE4))->GetCheck())
      m_pbMCARCCores[m_uiFirstCore + 4] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 4] = 0;
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::OnVScroll
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Scrolling MC structure
Date           Initials    Description
16-Oct-2007    VH          Initial
26-Oct-2014    RR          Fixed Scroll issue (Added handler code for
                           THUMBTRACK also)
****************************************************************************/
void CARCSetupDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{

    static UINT nPrevPos = 0; 
    bool bDown = TRUE;

   if (m_uiNumberOfCores <= 5)
      return ;                                  // there is nothing to scroll

   switch (nSBCode)
      {
      case SB_BOTTOM:
      case SB_PAGEDOWN:
      case SB_LINEDOWN:                         // scrolling down
         if ((m_uiFirstCore+1) <= (m_uiNumberOfCores - 5))
            m_uiFirstCore++;
         break;
      case SB_TOP:
      case SB_PAGEUP:
      case SB_LINEUP:                           // scrolling up
         if (m_uiFirstCore)
            m_uiFirstCore--;
         break;
	  case SB_THUMBTRACK:
		    //	  case SB_THUMBPOSITION:
			{
				if(nPos > nPrevPos)
				{
					if ((m_uiFirstCore + 20) <= (m_uiNumberOfCores - 5))
					    m_uiFirstCore += 20;				  
					else if ((m_uiFirstCore + 1) <= (m_uiNumberOfCores - 5))
						m_uiFirstCore++;
	            }
		        else if (nPos < nPrevPos)
			    {
				    if ((m_uiFirstCore - 20) > 0 )
					    m_uiFirstCore -= 20;
					else if (m_uiFirstCore)
						m_uiFirstCore --;
				}
			}
			break;
      default:
         break;
      }
   ShowMCStructure();

	nPrevPos = nPos;

   CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

/****************************************************************************
     Function: CARCSetupDlg::OnSelchangeComboCore0
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Combo 0 selection
Date           Initials    Description
17-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnSelchangeComboCore0() 
{
   int32_t iWidth = ((CComboBox *)GetDlgItem(IDC_COMBO_CORE0))->GetCurSel();
   if ((iWidth >= 0) && (iWidth < ARC_SETUP_MAX_CORE_IR_LENGTH))
      {
      m_puiMCCoreWidth[m_uiFirstCore + 0] = iWidth + 1;                         // IR width for particular core
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnSelchangeComboCore1
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Combo 1 selection
Date           Initials    Description
17-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnSelchangeComboCore1() 
{
   int32_t iWidth = ((CComboBox *)GetDlgItem(IDC_COMBO_CORE1))->GetCurSel();
   if ((iWidth >= 0) && (iWidth < ARC_SETUP_MAX_CORE_IR_LENGTH))
      {
      m_puiMCCoreWidth[m_uiFirstCore + 1] = iWidth + 1;                         // IR width for particular core
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnSelchangeComboCore2
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Combo 2 selection
Date           Initials    Description
17-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnSelchangeComboCore2() 
{
   int32_t iWidth = ((CComboBox *)GetDlgItem(IDC_COMBO_CORE2))->GetCurSel();
   if ((iWidth >= 0) && (iWidth < ARC_SETUP_MAX_CORE_IR_LENGTH))
      {
      m_puiMCCoreWidth[m_uiFirstCore + 2] = iWidth + 1;                         // IR width for particular core
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnSelchangeComboCore3
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Combo 3 selection
Date           Initials    Description
17-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnSelchangeComboCore3() 
{
   int32_t iWidth = ((CComboBox *)GetDlgItem(IDC_COMBO_CORE3))->GetCurSel();
   if ((iWidth >= 0) && (iWidth < ARC_SETUP_MAX_CORE_IR_LENGTH))
      {
      m_puiMCCoreWidth[m_uiFirstCore + 3] = iWidth + 1;                         // IR width for particular core
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnSelchangeComboCore4
     Engineer: Vitezslav Hola
        Input: default
       Output: default
  Description: Combo 4 selection
Date           Initials    Description
17-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::OnSelchangeComboCore4() 
{
   int32_t iWidth = ((CComboBox *)GetDlgItem(IDC_COMBO_CORE4))->GetCurSel();
   if ((iWidth >= 0) && (iWidth < ARC_SETUP_MAX_CORE_IR_LENGTH))
      {
      m_puiMCCoreWidth[m_uiFirstCore + 4] = iWidth + 1;                         // IR width for particular core
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::OnCheckTap
     Engineer: Rejeesh S Babu
        Input: default
       Output: default
  Description: TAP reset selection
Date           Initials    Description
09-Sep-2009    RSB         Initial
****************************************************************************/
void CARCSetupDlg::OnCheckTap() 
{
   if (((CButton *)GetDlgItem(IDC_CHECK_TAP))->GetCheck())
	   m_BDonotIssueTAPReset=1;
   else
	   m_BDonotIssueTAPReset=0;
	
}

