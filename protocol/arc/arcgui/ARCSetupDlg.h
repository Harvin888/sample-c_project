/******************************************************************************
       Module: ARCSetupDlg.h
     Engineer: Vitezslav Hola
  Description: Header for ARC setup dialog window for GUI DLL
  Date           Initials    Description
  15-Oct-2007    VH          Initial
  09-Sep-2009    RS		     Added TAP Reset check box
******************************************************************************/
#if !defined(AFX_ARCSETUPDLG_H__484C99E6_13AD_4137_AFE0_1CECA0D3B0A7__INCLUDED_)
#define AFX_ARCSETUPDLG_H__484C99E6_13AD_4137_AFE0_1CECA0D3B0A7__INCLUDED_

#include "../arcsetup.h"							// interface header

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_ARC_CORES		256

//---------------------------------------------------------------------------
//--- CARCSetupDlg class                                                  ---
//---------------------------------------------------------------------------
class CARCSetupDlg : public CDialog
{
// Construction
public:
	CARCSetupDlg(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CARCSetupDlg)
	enum { IDD = IDD_DIALOG1 };
	CScrollBar	m_ScrollBar;
	//}}AFX_DATA
// public variables
   char m_pszVersionLabel[100];
   char m_pszIniFileLocation[256];
	char m_pszProbeList[16][16];
   
	unsigned char m_bAdaptiveClock;
	unsigned char m_BDonotIssueTAPReset;

	unsigned short m_usProbesConnected;
	unsigned short m_usCurrentProbe;

   uint32_t m_uiTargetSelection;
	uint32_t m_uiNumberOfCores;
	uint32_t m_uiFirstCore;

	unsigned char m_pbMCARCCores[ARC_SETUP_MAX_SCANCHAIN_CORES];
	uint32_t  m_puiMCCoreWidth[ARC_SETUP_MAX_SCANCHAIN_CORES];
	uint32_t  ppuiDlgIndex[5][4];                        // 5 lines, 4 colums

	void *pvARCPtr;
	void *pfListProbes;
	void *pfGetSetupItem;
	void *pfSetSetupItem;
	void *pfAutoDetectScanChain;

// public functions
	void ShowMCStructure(void);
	void FillConnectedProbeList(void);
	void CheckTargetOptions(void);
	void LoadConfigurationFromCaller(void);
	void StoreConfigurationToCaller(void);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CARCSetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CARCSetupDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDropdownComboInstances();
	afx_msg void OnButtonAutoDetect();
	afx_msg void OnRtckCheck();
	afx_msg void OnRadioTgtopt1();
	afx_msg void OnRadioTgtopt2();
	afx_msg void OnRadioTgtopt3();
	afx_msg void OnSelchangeComboScanCores();
	afx_msg void OnCheckCore0();
	afx_msg void OnCheckCore1();
	afx_msg void OnCheckCore2();
	afx_msg void OnCheckCore3();
	afx_msg void OnCheckCore4();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSelchangeComboCore0();
	afx_msg void OnSelchangeComboCore1();
	afx_msg void OnSelchangeComboCore2();
	afx_msg void OnSelchangeComboCore3();
	afx_msg void OnSelchangeComboCore4();
	afx_msg void OnCheckTap();
	afx_msg void OnRadioTgtopt4();
	afx_msg void OnRadioTgtopt5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ARCSETUPDLG_H__484C99E6_13AD_4137_AFE0_1CECA0D3B0A7__INCLUDED_)

