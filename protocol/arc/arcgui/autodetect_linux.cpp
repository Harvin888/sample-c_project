#include "autodetect_linux.h"
#include "ui_autodetect.h"
#include "../arcsetup.h"

CAutoDetect::CAutoDetect(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::CAutoDetect)
{
   ui->setupUi(this);

   ui->OK->setDisabled(true);
}

CAutoDetect::~CAutoDetect()
{
   delete ui;
}

void CAutoDetect::on_Start_clicked()
{
   char pszText[100];

   PFAshGetSetupItem        pfGetSetupItem        = (PFAshGetSetupItem)CAutoDetect::pfGetSetupItem;
   PFAshAutoDetectScanChain pfAutoDetectScanChain = (PFAshAutoDetectScanChain)CAutoDetect::pfAutoDetectScanChain;

   ui->StatusList->clear();

   pfAutoDetectScanChain(pvARCPtr);

   ui->StatusList->insertPlainText("Auto-detection finished.\n\n");

   //get number of cores
   pfGetSetupItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, m_pszCoreNumbers);

   if (!strcmp(m_pszCoreNumbers, "0"))
   {
      sprintf(pszText, "%s cores detected!\n",m_pszCoreNumbers);
      ui->StatusList->insertPlainText(pszText);
      sprintf(pszText, "The Ashling Opella-XD for ARC Configuration will not be updated!\n");
      ui->StatusList->insertPlainText(pszText);
   }
   else
   {
      sprintf(pszText, "%s cores detected.\n",m_pszCoreNumbers);
      ui->StatusList->insertPlainText(pszText);
   }

   ui->Start->setDisabled(true);
   ui->OK->setDisabled(false);

}

void CAutoDetect::on_OK_clicked()
{
   if (!strcmp(m_pszCoreNumbers, "0"))
      this->reject();
   else
      this->accept();
}
