#include <string.h>
#include <stdio.h>
#include "arcsetupdlg_linux.h"
#include "ui_arcsetupdlg.h"
#include "autodetect_linux.h"
#include "../arcsetup.h"							// interface header

#define TARGET_SELECTION_ARCANGEL            0
#define TARGET_SELECTION_ARC                 1
#define TARGET_SELECTION_SIANO               2
#define TARGET_SELECTION_CJTAG_TPA_R1        3
#define TARGET_SELECTION_JTAG_TPA_R1         4

CARCSetupDlg::CARCSetupDlg(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::CARCSetupDlg)
{
   ui->setupUi(this);

   strcpy(m_pszVersionLabel, "");
   m_usProbesConnected   = 0;
   m_usCurrentProbe      = 0;
   m_bAdaptiveClock      = 0;
   m_BDonotIssueTAPReset = 0;
   m_uiTargetSelection   = 1;	// target selected as ARC Normal TPA rev. 0

    // init MC (1 ARC core)
   for (int32_t iIndex=0; iIndex < ARC_SETUP_MAX_SCANCHAIN_CORES; iIndex++)
   {
      m_pbMCARCCores[iIndex]   = 1;                        // assuming ARC cores
      m_puiMCCoreWidth[iIndex] = 4;                       // 4 bits in IR
   }

   m_uiNumberOfCores     = 1;
   m_uiFirstCore         = 0;
   pvARCPtr              = NULL;
   pfListProbes          = NULL;
   pfGetSetupItem        = NULL;
   pfSetSetupItem        = NULL;
   pfAutoDetectScanChain = NULL;

}

CARCSetupDlg::~CARCSetupDlg()
{
   delete ui;
}

/****************************************************************************
     Function: CARCSetupDlg::on_PB_OK_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Handler for pressing "OK" button
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_PB_OK_clicked()
{
   if (m_usProbesConnected)
      {
      int32_t iCurProbe = ui->CB_SERIAL_NUMBER->currentIndex();
      if ((iCurProbe >= 0) && (iCurProbe < (int32_t)m_usProbesConnected))
         m_usCurrentProbe = (unsigned short)iCurProbe;
      else
         m_usCurrentProbe = 0;
      }

   this->accept();
}

void CARCSetupDlg::on_PB_CANCEL_clicked()
{
   this->reject();
}

/****************************************************************************
     Function: CARCSetupDlg::OnInitDialog
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: OnInitDialog function
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
bool CARCSetupDlg::OnInitDialog()
{
    char           pszLocString[10];
    unsigned short usIndex;

    ui->RB_TGTOPT1->setChecked(false);
    ui->RB_TGTOPT2->setChecked(false);
    ui->RB_TGTOPT3->setChecked(false);
    ui->RB_TGTOPT4->setChecked(false);
    ui->RB_TGTOPT5->setChecked(false);

    // fill ini file location and set read only
    ui->LE_INI_FILE_LOCATION->setReadOnly(true);
    ui->LE_INI_FILE_LOCATION->setText(m_pszIniFileLocation);

    // fill list of connected probes
    m_usCurrentProbe = 0;

    for (usIndex=0; usIndex < ARC_SETUP_MAX_DEVICES; usIndex++)
    {
       strcpy(m_pszProbeList[usIndex], "");
       strcpy(m_pszPrevProbeList[usIndex], "");
    }

    FillConnectedProbeList();

    m_bFillCoreListStarted = 1;

    // define combo for number of cores
    ui->CB_SCAN_CORES->clear();
    for (int32_t iIndex=0; iIndex < ARC_SETUP_MAX_SCANCHAIN_CORES; iIndex++)
    {
       sprintf(pszLocString, "%d", iIndex);
       ui->CB_SCAN_CORES->addItem(pszLocString);
    }

    ui->CB_SCAN_CORES->setCurrentIndex(m_uiNumberOfCores);

    m_bFillCoreListStarted = 0;

    ui->LE_DEVICE1->setText("Device1");
    ui->LE_DEVICE2->setText("Device2");
    ui->LE_DEVICE3->setText("Device3");
    ui->LE_DEVICE4->setText("Device4");
    ui->LE_DEVICE5->setText("Device5");

    ui->CB_IR_WIDTH1->clear();
    ui->CB_IR_WIDTH2->clear();
    ui->CB_IR_WIDTH3->clear();
    ui->CB_IR_WIDTH4->clear();
    ui->CB_IR_WIDTH5->clear();

    for (int32_t iIndex=0; iIndex < ARC_SETUP_MAX_CORE_IR_LENGTH; iIndex++)
       {
       sprintf(pszLocString, "%d", iIndex + 1);
       ui->CB_IR_WIDTH1->addItem(pszLocString);
       ui->CB_IR_WIDTH2->addItem(pszLocString);
       ui->CB_IR_WIDTH3->addItem(pszLocString);
       ui->CB_IR_WIDTH4->addItem(pszLocString);
       ui->CB_IR_WIDTH5->addItem(pszLocString);
       }

    // set target selection
    switch (m_uiTargetSelection)
       {
       case TARGET_SELECTION_ARCANGEL:
          ui->RB_TGTOPT1->setChecked(true);
          break;
       case TARGET_SELECTION_SIANO:
          ui->RB_TGTOPT3->setChecked(true);
          break;
      case TARGET_SELECTION_CJTAG_TPA_R1:
         ui->RB_TGTOPT4->setChecked(true);
         break;
      case TARGET_SELECTION_JTAG_TPA_R1:
          ui->RB_TGTOPT5->setChecked(true);
         break;
       case TARGET_SELECTION_ARC:
       default:
          ui->RB_TGTOPT2->setChecked(true);
          break;
       }

    if (m_bAdaptiveClock)
       ui->CB_EnableRTCK->setChecked(true);
    else
       ui->CB_EnableRTCK->setChecked(false);

    if (m_BDonotIssueTAPReset)
       ui->CB_TAPReset->setChecked(true);
    else
       ui->CB_TAPReset->setChecked(false);

    // make sure target options are consistent
    CheckTargetOptions();

    // show initial multicore structure
    ShowMCStructure();

    return true;
}

/****************************************************************************
     Function: CARCSetupDlg::FillConnectedProbeList
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: fill list of connected Opella-XD probes into combo box
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::FillConnectedProbeList(void)
{
   char           pszSerialNumber[16];
   unsigned short usIndex;

   m_bFillProbeListStarted = 1;

   // check if any probe is connected
   m_usProbesConnected = 0;
   if (pfListProbes != NULL)
      ((PFAshListConnectedProbes)pfListProbes)(m_pszProbeList, 16, &m_usProbesConnected);

   //clear device list
   for (usIndex=0; usIndex < ARC_SETUP_MAX_DEVICES; usIndex++)
      strcpy(pszSerialNumber, "");

   for (usIndex=0; usIndex < m_usProbesConnected; usIndex++)
   {
      if (strcmp(m_pszProbeList[usIndex], m_pszPrevProbeList[usIndex]))
      {
        ui->CB_SERIAL_NUMBER->clear();
        break;
      }
   }

   if (m_usProbesConnected)
   {
      for (usIndex=0; usIndex < m_usProbesConnected; usIndex++)
      {
         if (strcmp(m_pszProbeList[usIndex], m_pszPrevProbeList[usIndex]))
         {
            strcpy(pszSerialNumber, m_pszProbeList[usIndex]);
            strcpy(m_pszPrevProbeList[usIndex], m_pszProbeList[usIndex]);
            ui->CB_SERIAL_NUMBER->addItem(pszSerialNumber);
         }
      }
   }
   else
   {
      ui->CB_SERIAL_NUMBER->addItem("No Opella-XD Found");
      ui->CB_SERIAL_NUMBER->setCurrentIndex(m_usCurrentProbe);
   }

   m_bFillProbeListStarted = 0;
}

/****************************************************************************
     Function: CARCSetupDlg::CheckTargetOptions
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: check target selection and decide if adaptive clock option is
               available to user
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::CheckTargetOptions(void)
{
   char pszValue[3];

   //target option needs to be saved straight away for auto detect scan chain
   PFAshSetSetupItem pfSetItem = (PFAshSetSetupItem)CARCSetupDlg::pfSetSetupItem;

   sprintf(pszValue, "%d", m_uiTargetSelection);
   pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);

   if (m_uiTargetSelection == TARGET_SELECTION_ARCANGEL)
      {     // using ARCangel, adaptive clock is not available
      m_bAdaptiveClock = 0;
      ui->CB_EnableRTCK->setChecked(false);
      ui->CB_EnableRTCK->setEnabled(false);
      ui->CB_TAPReset->setEnabled(false);
      }
   else if (m_uiTargetSelection == TARGET_SELECTION_CJTAG_TPA_R1)
      {
      // using ARC TARGET with cJTAG, adaptive clock is not available
      m_bAdaptiveClock = 0;
      ui->CB_EnableRTCK->setChecked(false);
      ui->CB_EnableRTCK->setEnabled(false);
      ui->CB_TAPReset->setChecked(false);
      ui->CB_TAPReset->setEnabled(true);
      m_BDonotIssueTAPReset=1;
      }
   else
      {     // other target, just ensure option is enabled
      ui->CB_EnableRTCK->setEnabled(true);
      ui->CB_TAPReset->setEnabled(true);
      }
}

/****************************************************************************
     Function: CARCSetupDlg::ShowMCStructure
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: show information about multicore structure in the dialog
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::ShowMCStructure(void)
{
   uint32_t uiIndex;
   int32_t          iRow;
   char         pszLocString[20];

   if (m_uiNumberOfCores <= 5)
      m_uiFirstCore = 0;                // too few cores to work with progress bar
   else if (m_uiFirstCore > (m_uiNumberOfCores - 5))
      m_uiFirstCore = 0;                // out of range for progress bar

   // set progress bar size
   if (m_uiNumberOfCores > 5)
      {
      ui->SB_SCROLLBAR_MC->setRange(0, m_uiNumberOfCores - 5);
      ui->SB_SCROLLBAR_MC->setValue(m_uiFirstCore);
      }
   else
      {
      ui->SB_SCROLLBAR_MC->setRange(0, 0);
      ui->SB_SCROLLBAR_MC->setValue(0);
      }

   // start filling information about cores
   uiIndex = m_uiFirstCore;
   for (iRow = 0; iRow < 5; iRow++)
   {
      if (uiIndex < m_uiNumberOfCores)
      {
         switch(iRow)
         {
            case 0:
               ui->LE_DEVICE1->show();
               ui->CB_ARC_CORE1->show();
               ui->LB_IR_WIDTH1->show();
               ui->CB_IR_WIDTH1->show();
               // show core type
               if (m_pbMCARCCores[uiIndex]) // this is ARC core
               {
                  ui->CB_ARC_CORE1->setChecked(true);    // set checkbox
                  ui->CB_IR_WIDTH1->setEnabled(false);   // disable IR width for ARC
                  m_puiMCCoreWidth[uiIndex] = 4;
               }
               else
               {
                  ui->CB_ARC_CORE1->setChecked(false);
                  ui->CB_IR_WIDTH1->setEnabled(true);    // enable IR width
               }
               // show IR width
               ui->CB_IR_WIDTH1->setCurrentIndex(m_puiMCCoreWidth[uiIndex]-1);
               sprintf(pszLocString, "Device %d", m_uiFirstCore + 1);
               ui->LE_DEVICE1->setText(pszLocString);// set device name
               break;
            case 1:
               // show core type
               ui->LE_DEVICE2->show();
               ui->CB_ARC_CORE2->show();
               ui->LB_IR_WIDTH2->show();
               ui->CB_IR_WIDTH2->show();
               if (m_pbMCARCCores[uiIndex]) // this is ARC core
               {
                  ui->CB_ARC_CORE2->setChecked(true);    // set checkbox
                  ui->CB_IR_WIDTH2->setEnabled(false);   // disable IR width for ARC
                  m_puiMCCoreWidth[uiIndex] = 4;
               }
               else
               {
                  ui->CB_ARC_CORE2->setChecked(false);
                  ui->CB_IR_WIDTH2->setEnabled(true);    // enable IR width
               }
               // show IR width
               ui->CB_IR_WIDTH2->setCurrentIndex(m_puiMCCoreWidth[uiIndex]-1);
               sprintf(pszLocString, "Device %d", m_uiFirstCore + 2);
               ui->LE_DEVICE2->setText(pszLocString);// set device name
               break;
            case 2:
               // show core type
               ui->LE_DEVICE3->show();
               ui->CB_ARC_CORE3->show();
               ui->LB_IR_WIDTH3->show();
               ui->CB_IR_WIDTH3->show();
               if (m_pbMCARCCores[uiIndex]) // this is ARC core
               {
                  ui->CB_ARC_CORE3->setChecked(true);    // set checkbox
                  ui->CB_IR_WIDTH3->setEnabled(false);   // disable IR width for ARC
                  m_puiMCCoreWidth[uiIndex] = 4;
               }
               else
               {
                  ui->CB_ARC_CORE3->setChecked(false);
                  ui->CB_IR_WIDTH3->setEnabled(true);    // enable IR width
               }
               // show IR width
               ui->CB_IR_WIDTH3->setCurrentIndex(m_puiMCCoreWidth[uiIndex]-1);
               sprintf(pszLocString, "Device %d", m_uiFirstCore + 3);
               ui->LE_DEVICE3->setText(pszLocString);// set device name
               break;
            case 3:
               // show core type
               ui->LE_DEVICE4->show();
               ui->CB_ARC_CORE4->show();
               ui->LB_IR_WIDTH4->show();
               ui->CB_IR_WIDTH4->show();
               if (m_pbMCARCCores[uiIndex]) // this is ARC core
               {
                  ui->CB_ARC_CORE4->setChecked(true);    // set checkbox
                  ui->CB_IR_WIDTH4->setEnabled(false);   // disable IR width for ARC
                  m_puiMCCoreWidth[uiIndex] = 4;
               }
               else
               {
                  ui->CB_ARC_CORE4->setChecked(false);
                  ui->CB_IR_WIDTH4->setEnabled(true);    // enable IR width
               }
               // show IR width
               ui->CB_IR_WIDTH4->setCurrentIndex(m_puiMCCoreWidth[uiIndex]-1);
               sprintf(pszLocString, "Device %d", m_uiFirstCore + 4);
               ui->LE_DEVICE4->setText(pszLocString);// set device name
               break;
            case 4:
               // show core type
               ui->LE_DEVICE5->show();
               ui->CB_ARC_CORE5->show();
               ui->LB_IR_WIDTH5->show();
               ui->CB_IR_WIDTH5->show();
               if (m_pbMCARCCores[uiIndex]) // this is ARC core
               {
                  ui->CB_ARC_CORE5->setChecked(true);    // set checkbox
                  ui->CB_IR_WIDTH5->setEnabled(false);   // disable IR width for ARC
                  m_puiMCCoreWidth[uiIndex] = 4;
               }
               else
               {
                  ui->CB_ARC_CORE5->setChecked(false);
                  ui->CB_IR_WIDTH5->setEnabled(true);    // enable IR width
               }
               // show IR width
               ui->CB_IR_WIDTH5->setCurrentIndex(m_puiMCCoreWidth[uiIndex]-1);
               sprintf(pszLocString, "Device %d", m_uiFirstCore + 5);
               ui->LE_DEVICE5->setText(pszLocString);// set device name
               break;
         }
      }
      else
      {     // hide whole row
         switch(iRow)
         {
            case 0:
               ui->LE_DEVICE1->hide();
               ui->CB_ARC_CORE1->hide();
               ui->LB_IR_WIDTH1->hide();
               ui->CB_IR_WIDTH1->hide();
               break;
            case 1:
               ui->LE_DEVICE2->hide();
               ui->CB_ARC_CORE2->hide();
               ui->LB_IR_WIDTH2->hide();
               ui->CB_IR_WIDTH2->hide();
               break;
               break;
            case 2:
               ui->LE_DEVICE3->hide();
               ui->CB_ARC_CORE3->hide();
               ui->LB_IR_WIDTH3->hide();
               ui->CB_IR_WIDTH3->hide();
               break;
               break;
            case 3:
               ui->LE_DEVICE4->hide();
               ui->CB_ARC_CORE4->hide();
               ui->LB_IR_WIDTH4->hide();
               ui->CB_IR_WIDTH4->hide();
               break;
               break;
            case 4:
               ui->LE_DEVICE5->hide();
               ui->CB_ARC_CORE5->hide();
               ui->LB_IR_WIDTH5->hide();
               ui->CB_IR_WIDTH5->hide();
               break;
         }
      }
      uiIndex++;
   }

   // just set non-used variables to default
   for (uiIndex=m_uiNumberOfCores; uiIndex < ARC_SETUP_MAX_SCANCHAIN_CORES; uiIndex++)
   {
      m_pbMCARCCores[uiIndex]   = 1; // assuming ARC cores
      m_puiMCCoreWidth[uiIndex] = 4; // 4 bits in IR
   }
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_SERIAL_NUMBER_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: dropdown event for combo box
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_SERIAL_NUMBER_currentIndexChanged(int32_t index)
{
   if (m_bFillProbeListStarted == 0)
   {
      m_usCurrentProbe = index;
      ui->CB_SERIAL_NUMBER->setCurrentIndex(m_usCurrentProbe);
   }
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_SERIAL_NUMBER_highlighted
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: highlight event for combo box
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_SERIAL_NUMBER_highlighted(int32_t index)
{
   if (m_bFillProbeListStarted == 0)
      FillConnectedProbeList();
}

/****************************************************************************
     Function: on_PB_AUTO_DETECT_clicked
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: button event for auto-detect scan chain
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_PB_AUTO_DETECT_clicked()
{
   char         pszValue[100], pszLocCore[ARC_SETUP_MAX_SCANCHAIN_CORES];
   uint32_t uiIndex;

   CAutoDetect cAutoDetect;

   PFAshGetSetupItem        pfGetItem             = (PFAshGetSetupItem)CARCSetupDlg::pfGetSetupItem;
   PFAshAutoDetectScanChain pfAutoDetectScanChain = (PFAshAutoDetectScanChain)CARCSetupDlg::pfAutoDetectScanChain;

   cAutoDetect.pvARCPtr              = pvARCPtr;
   cAutoDetect.pfGetSetupItem        = (void*)pfGetSetupItem;
   cAutoDetect.pfAutoDetectScanChain = (void*)pfAutoDetectScanChain;

   cAutoDetect.exec();

   if (cAutoDetect.result() == QDialog::Rejected)
      return;

   //get number of cores
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);
   m_uiNumberOfCores = strtoul(pszValue, NULL, 10);

   ui->CB_SCAN_CORES->setCurrentIndex(m_uiNumberOfCores);

   for (uiIndex = 0; uiIndex < m_uiNumberOfCores; uiIndex++)
   {
      sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiIndex+1);
      pfGetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, pszValue);

      if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
         m_pbMCARCCores[uiIndex] = 1;
      else
         m_pbMCARCCores[uiIndex] = 0;

      pfGetItem(pvARCPtr, ARC_SETUP_SECT_DEVICE, ARC_SETUP_ITEM_IRWIDTH, pszValue);
      m_puiMCCoreWidth[uiIndex] = strtoul(pszValue, NULL, 10);
   }

   //store modified target option
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
   m_uiTargetSelection = strtoul(pszValue, NULL, 10);

   ShowMCStructure();

   ui->RB_TGTOPT1->setChecked(false);
   ui->RB_TGTOPT2->setChecked(false);
   ui->RB_TGTOPT3->setChecked(false);
   ui->RB_TGTOPT4->setChecked(false);
   ui->RB_TGTOPT5->setChecked(false);

   // set target selection
   switch (m_uiTargetSelection)
   {
      case TARGET_SELECTION_ARCANGEL:
         ui->RB_TGTOPT1->setChecked(true);
         break;
      case TARGET_SELECTION_SIANO:
         ui->RB_TGTOPT3->setChecked(true);
         break;
      case TARGET_SELECTION_CJTAG_TPA_R1:
         ui->RB_TGTOPT4->setChecked(true);
         break;
      case TARGET_SELECTION_JTAG_TPA_R1:
         ui->RB_TGTOPT5->setChecked(true);
         break;
      case TARGET_SELECTION_ARC:
      default:
         ui->RB_TGTOPT2->setChecked(true);
         break;
   }

   CheckTargetOptions();

   //AfxMessageBox("Auto-detect scan chain finished.", MB_OK | MB_ICONINFORMATION, 0);
}

/****************************************************************************
     Function: CARCSetupDlg::LoadConfigurationFromCaller
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: loading setup configuration from calling DLL
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
void CARCSetupDlg::LoadConfigurationFromCaller(void)
{
   char              pszValue[100];
   uint32_t      uiCores   = 0;
   PFAshGetSetupItem pfGetItem = (PFAshGetSetupItem)CARCSetupDlg::pfGetSetupItem;

   if (pfGetItem == NULL)
      return;
   strcpy(pszValue, "");

   pfGetItem(pvARCPtr, ARC_SETUP_SECT_INIFILE, ARC_SETUP_SECT_INIFILE, pszValue);
   if (strcmp(pszValue, ""))
      strcpy(m_pszIniFileLocation, pszValue);
   else
      strcpy(m_pszIniFileLocation, "Unknown");

   // get adaptive clock settings
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, pszValue);
   if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
      m_bAdaptiveClock = 1;
   else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
      m_bAdaptiveClock = 0;
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, pszValue);
   if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
      m_BDonotIssueTAPReset = 1;
   else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
      m_BDonotIssueTAPReset = 0;
   // get target settings
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
   if (strlen(pszValue) == 1)
      {
      switch (pszValue[0])
         {
         case '0':
            m_uiTargetSelection = TARGET_SELECTION_ARCANGEL;
            break;
         case '1':
            m_uiTargetSelection = TARGET_SELECTION_ARC;
            break;
       case '3':
            m_uiTargetSelection = TARGET_SELECTION_CJTAG_TPA_R1;
            break;
       case '4':
            m_uiTargetSelection = TARGET_SELECTION_JTAG_TPA_R1;
            break;
         case '2':
            m_uiTargetSelection = TARGET_SELECTION_SIANO;
            break;
         default:
            break;
         }
      }
   // get information about MC
   pfGetItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);
   if ((sscanf(pszValue, "%d", &uiCores) == 1) && (uiCores <= ARC_SETUP_MAX_SCANCHAIN_CORES))
      {  // got number of cores
      m_uiNumberOfCores = uiCores;
      for (uiCores=0; uiCores < m_uiNumberOfCores; uiCores++)
         {  // get info for each core
         char pszLocCore[20];
         int32_t iWidth = 0;
         sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiCores+1);              // get section name
         pfGetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, pszValue);
         if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
            m_pbMCARCCores[uiCores] = 0;
         else
            m_pbMCARCCores[uiCores] = 1;                                             // if invalid value, we assume ARC core
         pfGetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_IRWIDTH, pszValue);
         if ((sscanf(pszValue, "%d", &iWidth) == 1) && (iWidth >= 1) && (iWidth <= ARC_SETUP_MAX_CORE_IR_LENGTH))
            m_puiMCCoreWidth[uiCores] = iWidth;
         else
            m_puiMCCoreWidth[uiCores] = iWidth;                                       // if invalid value, use 4 bits
         }
      }
}

/****************************************************************************
     Function: CARCSetupDlg::StoreConfigurationToCaller
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: storing setup configuration to calling DLL
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::StoreConfigurationToCaller(void)
{
   uint32_t uiIndex;
   char         pszValue[100];

   PFAshSetSetupItem pfSetItem = (PFAshSetSetupItem)CARCSetupDlg::pfSetSetupItem;
   if (pfSetItem == NULL)
      return;

   pfSetItem(pvARCPtr, ARC_SETUP_SECT_INIFILE, ARC_SETUP_SECT_INIFILE, m_pszIniFileLocation);

   // store adaptive clock settings
   if (m_bAdaptiveClock)
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, ARC_SETUP_ITEM_TRUE);
   else
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, ARC_SETUP_ITEM_FALSE);
   if (m_BDonotIssueTAPReset)
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, ARC_SETUP_ITEM_TRUE);
   else
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, ARC_SETUP_ITEM_FALSE);

   // store target selection
   switch (m_uiTargetSelection)
      {
      case TARGET_SELECTION_ARCANGEL:           // ARCangel target
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "0");
         break;
      case TARGET_SELECTION_SIANO:           // Siano target
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "2");
         break;
     case TARGET_SELECTION_CJTAG_TPA_R1:           // ARC Target cJTAG TPA Rev. 1
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "3");
         break;
     case TARGET_SELECTION_JTAG_TPA_R1:           // ARC target JTAG TPA Rev. 1
        pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "4");
         break;
      case TARGET_SELECTION_ARC:           // ARC target
      default:
         pfSetItem(pvARCPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "1");
         break;
      }
   // store instance number
   if (m_usProbesConnected)
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, m_pszProbeList[m_usCurrentProbe]);
   else
      pfSetItem(pvARCPtr, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, "");              // no probe connected
   // store information about MC
   sprintf(pszValue, "%d", m_uiNumberOfCores);
   pfSetItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);             // number of cores in MC
   for (uiIndex=0; uiIndex < m_uiNumberOfCores; uiIndex++)
      {  // set setup for each core
      char pszLocCore[20];
      sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiIndex+1);                                   // set section name
      if (m_pbMCARCCores[uiIndex])                                                                     // set if core is ARC or not
         pfSetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, ARC_SETUP_ITEM_TRUE);
      else
         pfSetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_ARCCORE, ARC_SETUP_ITEM_FALSE);
      sprintf(pszValue, "%d", m_puiMCCoreWidth[uiIndex]);                                               // set number of bits in IR
      pfSetItem(pvARCPtr, pszLocCore, ARC_SETUP_ITEM_IRWIDTH, pszValue);
      }
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_EnableRTCK_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Click on adaptive clock box
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_EnableRTCK_clicked(bool checked)
{
   m_bAdaptiveClock = checked;
}

/****************************************************************************
     Function: CARCSetupDlg::on_RB_TGTOPT1_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Click on ARCangel selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_RB_TGTOPT1_clicked()
{
   ui->RB_TGTOPT1->setChecked(true);
   m_uiTargetSelection = TARGET_SELECTION_ARCANGEL;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::on_RB_TGTOPT2_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Click on ARC selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_RB_TGTOPT2_clicked()
{
   ui->RB_TGTOPT2->setChecked(true);
   m_uiTargetSelection = TARGET_SELECTION_ARC;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::on_RB_TGTOPT3_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Click on Siano selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_RB_TGTOPT3_clicked()
{
   ui->RB_TGTOPT3->setChecked(true);
   m_uiTargetSelection = TARGET_SELECTION_SIANO;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::on_RB_TGTOPT4_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Click on cJTAG selection with TPA Rev. 1
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_RB_TGTOPT4_clicked()
{
   ui->RB_TGTOPT4->setChecked(true);
   m_uiTargetSelection = TARGET_SELECTION_CJTAG_TPA_R1;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::on_RB_TGTOPT5_clicked
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Click on JTAG selection with TPA Rev. 1
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_RB_TGTOPT5_clicked()
{
   ui->RB_TGTOPT5->setChecked(true);
   m_uiTargetSelection = TARGET_SELECTION_JTAG_TPA_R1;
   CheckTargetOptions();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_SCAN_CORES_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Changed MC cores combo
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_SCAN_CORES_currentIndexChanged(int32_t index)
{
   uint32_t uiCores;

   if (m_bFillCoreListStarted == 1)
      return;

   uiCores = index;

   if (uiCores < ARC_SETUP_MAX_SCANCHAIN_CORES)
      {
      m_uiNumberOfCores = uiCores;
      ShowMCStructure();
      }
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_ARC_CORE1_clicked
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Click on checkbox core 1
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_ARC_CORE1_clicked(bool checked)
{
   if (checked == true)
      m_pbMCARCCores[m_uiFirstCore + 0] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 0] = 0;

   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_ARC_CORE2_clicked
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Click on checkbox core 2
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_ARC_CORE2_clicked(bool checked)
{
   if (checked == true)
      m_pbMCARCCores[m_uiFirstCore + 1] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 1] = 0;

   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_ARC_CORE3_clicked
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Click on checkbox core 3
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_ARC_CORE3_clicked(bool checked)
{
   if (checked == true)
      m_pbMCARCCores[m_uiFirstCore + 2] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 2] = 0;

   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_ARC_CORE4_clicked
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Click on checkbox core 4
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_ARC_CORE4_clicked(bool checked)
{
   if (checked == true)
      m_pbMCARCCores[m_uiFirstCore + 3] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 3] = 0;

   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_ARC_CORE5_clicked
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Click on checkbox core 5
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_ARC_CORE5_clicked(bool checked)
{
   if (checked == true)
      m_pbMCARCCores[m_uiFirstCore + 4] = 1;
   else
      m_pbMCARCCores[m_uiFirstCore + 4] = 0;

   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_SB_SCROLLBAR_MC_sliderMoved
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: Scrolling MC structure
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_SB_SCROLLBAR_MC_valueChanged(int32_t value)
{
   if (m_uiNumberOfCores <= 5)
      return ;                                  // there is nothing to scroll

   m_uiFirstCore = value;

   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_IR_WIDTH1_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: IR Width Combo 1 selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_IR_WIDTH1_currentIndexChanged(int32_t index)
{
   m_puiMCCoreWidth[m_uiFirstCore + 0] = index + 1;                         // IR width for particular core
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_IR_WIDTH2_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: IR Width Combo 2 selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_IR_WIDTH2_currentIndexChanged(int32_t index)
{
   m_puiMCCoreWidth[m_uiFirstCore + 1] = index + 1;                         // IR width for particular core
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_IR_WIDTH3_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: IR Width Combo 3 selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_IR_WIDTH3_currentIndexChanged(int32_t index)
{
   m_puiMCCoreWidth[m_uiFirstCore + 2] = index + 1;                         // IR width for particular core
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_IR_WIDTH4_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: IR Width Combo 4 selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_IR_WIDTH4_currentIndexChanged(int32_t index)
{
   m_puiMCCoreWidth[m_uiFirstCore + 3] = index + 1;                         // IR width for particular core
   ShowMCStructure();
}

/****************************************************************************
     Function: CARCSetupDlg::on_CB_IR_WIDTH5_currentIndexChanged
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: IR Width Combo 5 selection
Date           Initials    Description
25-Apr-2017    AS          Initial
****************************************************************************/
void CARCSetupDlg::on_CB_IR_WIDTH5_currentIndexChanged(int32_t index)
{
   m_puiMCCoreWidth[m_uiFirstCore + 4] = index + 1;                         // IR width for particular core
   ShowMCStructure();
}

void CARCSetupDlg::on_CB_TAPReset_clicked(bool checked)
{
   if (checked == true)
      m_BDonotIssueTAPReset=1;
   else
      m_BDonotIssueTAPReset=0;
}
