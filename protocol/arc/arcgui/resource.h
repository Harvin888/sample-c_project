//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by arcgui.rc
//
#define IDD_DIALOG1                     3000
#define IDD_ARCGUI_DIALOG               3000
#define IDC_VERSION_LABEL               3001
#define IDC_GROUP1                      3002
#define IDI_ICON1                       3002
#define IDC_COMBO_INSTANCES             3003
#define IDC_GROUP5                      3004
#define IDD_AUTO_DETECT                 3004
#define IDC_STATIC2                     3005
#define IDC_GROUP2                      3006
#define IDC_GROUP3                      3007
#define IDC_GROUP4                      3008
#define IDC_RADIO_TGTOPT1               3009
#define IDC_RADIO_TGTOPT2               3010
#define IDC_COMBO_SCAN_CORES            3011
#define IDC_RADIO_TGTOPT3               3012
#define IDC_RTCK_CHECK                  3013
#define IDC_LABEL_MC1                   3014
#define IDC_GROUP6                      3015
#define IDC_LABEL_CORE0                 3016
#define IDC_LABEL_CORE1                 3017
#define IDC_CHECK_CORE0                 3018
#define IDC_COMBO_CORE0                 3019
#define IDC_LABEL_LEN_0                 3020
#define IDC_CHECK_CORE1                 3021
#define IDC_COMBO_CORE1                 3022
#define IDC_LABEL_LEN_1                 3023
#define IDC_LABEL_CORE2                 3024
#define IDC_CHECK_CORE2                 3025
#define IDC_COMBO_CORE2                 3026
#define IDC_LABEL_LEN_2                 3027
#define IDC_LABEL_CORE3                 3028
#define IDC_CHECK_CORE3                 3029
#define IDC_COMBO_CORE3                 3030
#define IDC_LABEL_LEN_3                 3031
#define IDC_LABEL_CORE4                 3032
#define IDC_CHECK_CORE4                 3033
#define IDC_COMBO_CORE4                 3034
#define IDC_LABEL_LEN_4                 3035
#define IDC_TAP                         3036
#define IDC_RADIO_TGTOPT4               3036
#define IDC_RADIO_TGTOPT5               3037
#define IDC_STATIC3                     3039
#define IDC_SCROLLBAR_MC                3040
#define IDC_CHECK_TAP                   3042
#define IDC_EDIT_INI_FILE               3049
#define IDC_BUTTON_AUTO_DETECT          3050
#define IDC_STATUS_LIST                 3051
#define IDC_START                       3052

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        3005
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         3053
#define _APS_NEXT_SYMED_VALUE           3000
#endif
#endif
