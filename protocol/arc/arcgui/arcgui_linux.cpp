#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>

#include "arcgui_linux.h"
#include "arcsetupdlg_linux.h"

#include <QApplication>
#include <QSharedMemory>

arcgui::arcgui()
{

}

unsigned char opxdarc_exist()
{
   DIR* dir;
   struct dirent* ent;
   char buf[512];

   int32_t  pid;
   char pname[100] = {0,};
   char state;
   FILE *fp=NULL;

   if (!(dir = opendir("/proc")))
   {
        perror("can't open /proc");
        return -1;
   }

   while((ent = readdir(dir)) != NULL)
   {
      int32_t lpid = atol(ent->d_name);

      if(lpid < 0)
         continue;

      snprintf(buf, sizeof(buf), "/proc/%ld/stat", lpid);

      fp = fopen(buf, "r");

      if (fp)
      {
         if ( (fscanf(fp, "%ld (%[^)]) %c", &pid, pname, &state)) != 3 )
         {
            printf("fscanf failed \n");
            fclose(fp);
            closedir(dir);
            return 0;
         }

         if (!strcmp(pname, "opxdarc"))
         {
            fclose(fp);
            closedir(dir);
            return 1;
         }

         fclose(fp);
      }
   }

   closedir(dir);
   return 0;
}

extern "C" Q_DECL_EXPORT bool AshShowARCSetupWindow(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt)
{
   int argc = 1;
   char * argv[] = {"MDB_UI_BSCI_RTT", NULL};
   unsigned char bQtApplicationStarted = 0;

   QApplication *pqApp;

   //check if opxdarc is running
   //if not then start application
   if (!opxdarc_exist())
   {
      pqApp = new QApplication(argc, argv);
      bQtApplicationStarted = 1;
   }

   CARCSetupDlg cARCSetupDlg;                            // dialog class

   // get calling DLL version and name
   strcpy(cARCSetupDlg.m_pszVersionLabel, ptyARCSetupInt->pfGetID());

   // get callback functions for setup interface
   cARCSetupDlg.pvARCPtr              = pvPtr;
   cARCSetupDlg.pfListProbes          = (void *)(ptyARCSetupInt->pfListConnectedProbes);
   cARCSetupDlg.pfGetSetupItem        = (void *)(ptyARCSetupInt->pfGetSetupItem);
   cARCSetupDlg.pfSetSetupItem        = (void *)(ptyARCSetupInt->pfSetSetupItem);
   cARCSetupDlg.pfAutoDetectScanChain = (void *)(ptyARCSetupInt->pfAutoDetectScanChain);

   // load ARC setup configuration from caller
   cARCSetupDlg.LoadConfigurationFromCaller();
   // now show dialog and see if user hits OK button
   cARCSetupDlg.OnInitDialog();

   if (bQtApplicationStarted == 1)
   {

      cARCSetupDlg.show();
      pqApp->exec();
   }
   else
      cARCSetupDlg.exec();

   if (cARCSetupDlg.result() == QDialog::Rejected)
      return false;                                     // when cancel or just close dialog, do not update settings and return false

   // store ARC setup from dialog to caller
   cARCSetupDlg.StoreConfigurationToCaller();

   return true;
}
