#if !defined(AFX_AUTODETECT_H__E7433903_62ED_43F2_BF0D_945537B975A4__INCLUDED_)
#define AFX_AUTODETECT_H__E7433903_62ED_43F2_BF0D_945537B975A4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// autodetect.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAutoDetect dialog

class CAutoDetect : public CDialog
{
// Construction
public:
	CAutoDetect(CWnd* pParent = NULL);   // standard constructor

	char m_pszCoreNumbers[2];

	void *pvARCPtr;
	void *pfGetSetupItem;
	void *pfAutoDetectScanChain;

// Dialog Data
	//{{AFX_DATA(CAutoDetect)
	enum { IDD = IDD_AUTO_DETECT };
	CButton	m_OkButton;
	CButton	m_StartButton;
	CListBox	m_StatusList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAutoDetect)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAutoDetect)
	afx_msg void OnStart();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTODETECT_H__E7433903_62ED_43F2_BF0D_945537B975A4__INCLUDED_)
