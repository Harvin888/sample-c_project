// autodetect.cpp : implementation file
//

#include "stdafx.h"
#include "arcgui.h"
#include "autodetect.h"
#include "../arcsetup.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAutoDetect dialog


CAutoDetect::CAutoDetect(CWnd* pParent /*=NULL*/)
	: CDialog(CAutoDetect::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAutoDetect)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAutoDetect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoDetect)
	DDX_Control(pDX, IDOK, m_OkButton);
	DDX_Control(pDX, IDC_START, m_StartButton);
	DDX_Control(pDX, IDC_STATUS_LIST, m_StatusList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAutoDetect, CDialog)
	//{{AFX_MSG_MAP(CAutoDetect)
	ON_BN_CLICKED(IDC_START, OnStart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoDetect message handlers

BOOL CAutoDetect::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_OkButton.EnableWindow(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAutoDetect::OnStart() 
{
   char pszText[100];

   PFAshGetSetupItem        pfGetSetupItem        = (PFAshGetSetupItem)CAutoDetect::pfGetSetupItem;
   PFAshAutoDetectScanChain pfAutoDetectScanChain = (PFAshAutoDetectScanChain)CAutoDetect::pfAutoDetectScanChain;

	m_StatusList.ResetContent();
	
	pfAutoDetectScanChain(pvARCPtr);

	m_StatusList.InsertString(0, "Auto-detection finished.");

	//insert empty line
	m_StatusList.InsertString(1, "");

	//get number of cores
   pfGetSetupItem(pvARCPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, m_pszCoreNumbers);

	if (!strcmp(m_pszCoreNumbers, "0"))
	{
		sprintf(pszText, "%s cores detected!",m_pszCoreNumbers);
		m_StatusList.InsertString(2, pszText);
		sprintf(pszText, "The Ashling Opella-XD for ARC Configuration will not be updated!");
		m_StatusList.InsertString(3, pszText);
	}
	else
	{
		sprintf(pszText, "%s cores detected.",m_pszCoreNumbers);
		m_StatusList.InsertString(2, pszText);
	}

	m_StartButton.EnableWindow(false);
	m_OkButton.EnableWindow(true);
}

void CAutoDetect::OnOK() 
{
	if (!strcmp(m_pszCoreNumbers, "0"))
		CDialog::OnCancel();
	else
		CDialog::OnOK();
}
