/******************************************************************************
       Module: arcgui.cpp
     Engineer: Vitezslav Hola
  Description: Main file for Opella-XD ARC GUI DLL
  Date           Initials    Description
  15-Oct-2007    VH          Initial
******************************************************************************/
#include "stdafx.h"
#include "arcgui.h"
#include "ARCSetupDlg.h"
#include "../arcsetup.h"							// interface header

#ifdef _DEBUG
#define new DEBUG_NEW 
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CArcguiApp
BEGIN_MESSAGE_MAP(CArcguiApp, CWinApp)
	//{{AFX_MSG_MAP(CArcguiApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// CArcguiApp construction
CArcguiApp::CArcguiApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CArcguiApp object
CArcguiApp theApp;

/****************************************************************************
     Function: AshShowARCSetupWindow
     Engineer: Vitezslav Hola
        Input: void *pvPtr - pointer to internal structure in calling DLL which should be passed into some interface functions
               TyARCSetupInterface *ptyARCSetupInt - structure with pointers to interface functions into calling DLL
       Output: none
  Description: This function is exported and it is called when ARC driver needs to show target setup dialog window
               to the user. This function may/should use some of caller's API functions (i.e. listing connected probes, obtaining
               current setup items and settings items when closing dialog).
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
extern "C" __declspec(dllexport) BOOL AshShowARCSetupWindow(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt)
{
   // this macro MUST be first for exported functions before any declaration of variables, etc. inside function
   AFX_MANAGE_STATE(AfxGetStaticModuleState());
   CARCSetupDlg cARCSetupDlg;                            // dialog class
   // get calling DLL version and name
   strcpy(cARCSetupDlg.m_pszVersionLabel, ptyARCSetupInt->pfGetID());

   // get callback functions for setup interface
   cARCSetupDlg.pvARCPtr              = pvPtr;
   cARCSetupDlg.pfListProbes          = (void *)(ptyARCSetupInt->pfListConnectedProbes);
   cARCSetupDlg.pfGetSetupItem        = (void *)(ptyARCSetupInt->pfGetSetupItem);
   cARCSetupDlg.pfSetSetupItem        = (void *)(ptyARCSetupInt->pfSetSetupItem);
	cARCSetupDlg.pfAutoDetectScanChain = (void *)(ptyARCSetupInt->pfAutoDetectScanChain);

   // load ARC setup configuration from caller
   cARCSetupDlg.LoadConfigurationFromCaller();
   // now show dialog and see if user hits OK button
   if (cARCSetupDlg.DoModal() != IDOK)
      return FALSE;                                     // when cancel or just close dialog, do not update settings and return false
	// store ARC setup from dialog to caller
   cARCSetupDlg.StoreConfigurationToCaller();
   return TRUE;
}

