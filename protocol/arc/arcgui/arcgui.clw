; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAutoDetect
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "arcgui.h"
LastPage=0

ClassCount=4
Class1=CArcguiApp
Class2=CARCSetupDlg

ResourceCount=4
Resource1=IDD_DIALOG1 (English (Ireland))
Resource2=IDD_DIALOG1
Class3=CAutoDetectMessage
Resource3=IDD_ARCGUI_DIALOG
Class4=CAutoDetect
Resource4=IDD_AUTO_DETECT

[CLS:CArcguiApp]
Type=0
BaseClass=CWinApp
HeaderFile=arcgui.h
ImplementationFile=arcgui.cpp

[CLS:CARCSetupDlg]
Type=0
BaseClass=CDialog
HeaderFile=ARCSetupDlg.h
ImplementationFile=ARCSetupDlg.cpp
LastObject=CARCSetupDlg
Filter=D
VirtualFilter=dWC

[DLG:IDD_DIALOG1]
Type=1
Class=CARCSetupDlg
ControlCount=39
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_GROUP1,button,1342177287
Control4=IDC_COMBO_INSTANCES,combobox,1344339971
Control5=IDC_STATIC2,static,1342308352
Control6=IDC_GROUP2,button,1342177287
Control7=IDC_GROUP3,button,1342177287
Control8=IDC_GROUP4,button,1342308359
Control9=IDC_RTCK_CHECK,button,1342242819
Control10=IDC_LABEL_MC1,static,1342308352
Control11=IDC_COMBO_SCAN_CORES,combobox,1344339971
Control12=IDC_GROUP6,button,1342177287
Control13=IDC_LABEL_CORE0,static,1342312961
Control14=IDC_CHECK_CORE0,button,1342242851
Control15=IDC_COMBO_CORE0,combobox,1344339971
Control16=IDC_LABEL_LEN_0,static,1342308352
Control17=IDC_LABEL_CORE1,static,1342312961
Control18=IDC_CHECK_CORE1,button,1342242851
Control19=IDC_COMBO_CORE1,combobox,1344339971
Control20=IDC_LABEL_LEN_1,static,1342308352
Control21=IDC_LABEL_CORE2,static,1342312961
Control22=IDC_CHECK_CORE2,button,1342242851
Control23=IDC_COMBO_CORE2,combobox,1344339971
Control24=IDC_LABEL_LEN_2,static,1342308352
Control25=IDC_LABEL_CORE3,static,1342312961
Control26=IDC_CHECK_CORE3,button,1342242851
Control27=IDC_COMBO_CORE3,combobox,1344339971
Control28=IDC_LABEL_LEN_3,static,1342308352
Control29=IDC_LABEL_CORE4,static,1342312961
Control30=IDC_CHECK_CORE4,button,1342242851
Control31=IDC_COMBO_CORE4,combobox,1344339971
Control32=IDC_LABEL_LEN_4,static,1342308352
Control33=IDC_SCROLLBAR_MC,scrollbar,1342177281
Control34=IDC_CHECK_TAP,button,1342242819
Control35=IDC_RADIO_TGTOPT1,button,1342177289
Control36=IDC_RADIO_TGTOPT2,button,1342177289
Control37=IDC_RADIO_TGTOPT3,button,1342177289
Control38=IDC_RADIO_TGTOPT4,button,1342177289
Control39=IDC_RADIO_TGTOPT5,button,1342177289

[DLG:IDD_DIALOG1 (English (Ireland))]
Type=1
Class=CARCSetupDlg
ControlCount=39
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_GROUP1,button,1342177287
Control4=IDC_COMBO_INSTANCES,combobox,1344339971
Control5=IDC_STATIC2,static,1342308352
Control6=IDC_GROUP2,button,1342177287
Control7=IDC_GROUP3,button,1342177287
Control8=IDC_GROUP4,button,1342308359
Control9=IDC_RTCK_CHECK,button,1342242819
Control10=IDC_LABEL_MC1,static,1342308352
Control11=IDC_COMBO_SCAN_CORES,combobox,1344339971
Control12=IDC_GROUP6,button,1342177287
Control13=IDC_LABEL_CORE0,static,1342312961
Control14=IDC_CHECK_CORE0,button,1342242851
Control15=IDC_COMBO_CORE0,combobox,1344339971
Control16=IDC_LABEL_LEN_0,static,1342308352
Control17=IDC_LABEL_CORE1,static,1342312961
Control18=IDC_CHECK_CORE1,button,1342242851
Control19=IDC_COMBO_CORE1,combobox,1344339971
Control20=IDC_LABEL_LEN_1,static,1342308352
Control21=IDC_LABEL_CORE2,static,1342312961
Control22=IDC_CHECK_CORE2,button,1342242851
Control23=IDC_COMBO_CORE2,combobox,1344339971
Control24=IDC_LABEL_LEN_2,static,1342308352
Control25=IDC_LABEL_CORE3,static,1342312961
Control26=IDC_CHECK_CORE3,button,1342242851
Control27=IDC_COMBO_CORE3,combobox,1344339971
Control28=IDC_LABEL_LEN_3,static,1342308352
Control29=IDC_LABEL_CORE4,static,1342312961
Control30=IDC_CHECK_CORE4,button,1342242851
Control31=IDC_COMBO_CORE4,combobox,1344339971
Control32=IDC_LABEL_LEN_4,static,1342308352
Control33=IDC_SCROLLBAR_MC,scrollbar,1342177281
Control34=IDC_CHECK_TAP,button,1342242819
Control35=IDC_RADIO_TGTOPT1,button,1342177289
Control36=IDC_RADIO_TGTOPT2,button,1342177289
Control37=IDC_RADIO_TGTOPT3,button,1342177289
Control38=IDC_RADIO_TGTOPT4,button,1342177289
Control39=IDC_RADIO_TGTOPT5,button,1342177289

[DLG:IDD_ARCGUI_DIALOG]
Type=1
Class=?
ControlCount=43
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_GROUP1,button,1342177287
Control4=IDC_COMBO_INSTANCES,combobox,1344339971
Control5=IDC_STATIC2,static,1342308352
Control6=IDC_GROUP2,button,1342177287
Control7=IDC_GROUP3,button,1342177287
Control8=IDC_GROUP4,button,1342308359
Control9=IDC_RTCK_CHECK,button,1342242819
Control10=IDC_LABEL_MC1,static,1342308352
Control11=IDC_COMBO_SCAN_CORES,combobox,1344339971
Control12=IDC_GROUP6,button,1342177287
Control13=IDC_LABEL_CORE0,static,1342312961
Control14=IDC_CHECK_CORE0,button,1342242851
Control15=IDC_COMBO_CORE0,combobox,1344339971
Control16=IDC_LABEL_LEN_0,static,1342308352
Control17=IDC_LABEL_CORE1,static,1342312961
Control18=IDC_CHECK_CORE1,button,1342242851
Control19=IDC_COMBO_CORE1,combobox,1344339971
Control20=IDC_LABEL_LEN_1,static,1342308352
Control21=IDC_LABEL_CORE2,static,1342312961
Control22=IDC_CHECK_CORE2,button,1342242851
Control23=IDC_COMBO_CORE2,combobox,1344339971
Control24=IDC_LABEL_LEN_2,static,1342308352
Control25=IDC_LABEL_CORE3,static,1342312961
Control26=IDC_CHECK_CORE3,button,1342242851
Control27=IDC_COMBO_CORE3,combobox,1344339971
Control28=IDC_LABEL_LEN_3,static,1342308352
Control29=IDC_LABEL_CORE4,static,1342312961
Control30=IDC_CHECK_CORE4,button,1342242851
Control31=IDC_COMBO_CORE4,combobox,1344339971
Control32=IDC_LABEL_LEN_4,static,1342308352
Control33=IDC_SCROLLBAR_MC,scrollbar,1342177281
Control34=IDC_CHECK_TAP,button,1342242819
Control35=IDC_RADIO_TGTOPT1,button,1342177289
Control36=IDC_RADIO_TGTOPT2,button,1342177289
Control37=IDC_RADIO_TGTOPT3,button,1342177289
Control38=IDC_RADIO_TGTOPT4,button,1342177289
Control39=IDC_RADIO_TGTOPT5,button,1342177289
Control40=IDC_GROUP5,button,1342177287
Control41=IDC_STATIC3,static,1342308352
Control42=IDC_EDIT_INI_FILE,edit,1350631552
Control43=IDC_BUTTON_AUTO_DETECT,button,1342242816

[DLG:IDD_AUTO_DETECT]
Type=1
Class=CAutoDetect
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDC_STATUS_LIST,listbox,1352728835
Control3=IDC_START,button,1342242816

[CLS:CAutoDetect]
Type=0
HeaderFile=autodetect.h
ImplementationFile=autodetect.cpp
BaseClass=CDialog
Filter=D
LastObject=IDOK
VirtualFilter=dWC

