#ifndef ARCGUI_GLOBAL_H
#define ARCGUI_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ARCGUI_LIBRARY)
#  define ARCGUISHARED_EXPORT Q_DECL_EXPORT
#else
#  define ARCGUISHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ARCGUI_GLOBAL_H
