/******************************************************************************
       Module: arcgui.h
     Engineer: Vitezslav Hola
  Description: Header file for Opella-XD ARC GUI DLL
  Date           Initials    Description
  15-Oct-2007    VH          Initial
******************************************************************************/
#if !defined(AFX_ARCGUI_H__794386B8_D086_4B8D_A006_60E64E32D292__INCLUDED_)
#define AFX_ARCGUI_H__794386B8_D086_4B8D_A006_60E64E32D292__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

//---------------------------------------------------------------------------
//--- CArcguiApp                                                          ---
//---------------------------------------------------------------------------
class CArcguiApp : public CWinApp
{
public:
	CArcguiApp();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CArcguiApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CArcguiApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//---------------------------------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ARCGUI_H__794386B8_D086_4B8D_A006_60E64E32D292__INCLUDED_)

