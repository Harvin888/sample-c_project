#ifndef AUTODETECT_LINUX_H
#define AUTODETECT_LINUX_H

#include <QDialog>

namespace Ui {
class CAutoDetect;
}

class CAutoDetect : public QDialog
{
   Q_OBJECT

public:
   explicit CAutoDetect(QWidget *parent = 0);
   ~CAutoDetect();

   char m_pszCoreNumbers[2];

   void *pvARCPtr;
   void *pfGetSetupItem;
   void *pfAutoDetectScanChain;

private slots:
   void on_Start_clicked();
   void on_OK_clicked();

private:
   Ui::CAutoDetect *ui;
};

#endif // AUTODETECT_LINUX_H
