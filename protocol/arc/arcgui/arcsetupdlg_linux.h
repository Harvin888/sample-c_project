#ifndef ARCSETUPDLG_LINUX_H
#define ARCSETUPDLG_LINUX_H

#include <QDialog>
#include "../arcsetup.h"							// interface header
#include "../../common/types.h"
#define MAX_ARC_CORES		256

namespace Ui {
class CARCSetupDlg;
}

class CARCSetupDlg : public QDialog
{
   Q_OBJECT

public:
   explicit CARCSetupDlg(QWidget *parent = 0);
   ~CARCSetupDlg();

   char m_pszVersionLabel[100];
   char m_pszIniFileLocation[256];
   char m_pszProbeList[16][16];
   char m_pszPrevProbeList[16][16];

   unsigned char m_bAdaptiveClock;
   unsigned char m_BDonotIssueTAPReset;
   unsigned char m_bFillCoreListStarted;
   unsigned char m_bFillProbeListStarted;

   unsigned short m_usProbesConnected;
   unsigned short m_usCurrentProbe;

   uint32_t m_uiTargetSelection;
   uint32_t m_uiNumberOfCores;
   uint32_t m_uiFirstCore;

   unsigned char m_pbMCARCCores[ARC_SETUP_MAX_SCANCHAIN_CORES];
   uint32_t  m_puiMCCoreWidth[ARC_SETUP_MAX_SCANCHAIN_CORES];
   uint32_t  ppuiDlgIndex[5][4];                        // 5 lines, 4 colums

   void *pvARCPtr;
   void *pfListProbes;
   void *pfGetSetupItem;
   void *pfSetSetupItem;
   void *pfAutoDetectScanChain;

// public functions
   bool OnInitDialog();
   void ShowMCStructure(void);
   void FillConnectedProbeList(void);
   void CheckTargetOptions(void);
   void LoadConfigurationFromCaller(void);
   void StoreConfigurationToCaller(void);

private slots:
   void on_CB_SERIAL_NUMBER_currentIndexChanged(int32_t index);
   void on_CB_SERIAL_NUMBER_highlighted(int32_t index);
   void on_CB_SCAN_CORES_currentIndexChanged(int32_t index);
   void on_PB_AUTO_DETECT_clicked();
   void on_CB_ARC_CORE1_clicked(bool checked);
   void on_CB_ARC_CORE2_clicked(bool checked);
   void on_CB_ARC_CORE3_clicked(bool checked);
   void on_CB_ARC_CORE4_clicked(bool checked);
   void on_CB_ARC_CORE5_clicked(bool checked);
   void on_CB_IR_WIDTH1_currentIndexChanged(int32_t index);
   void on_CB_IR_WIDTH2_currentIndexChanged(int32_t index);
   void on_CB_IR_WIDTH3_currentIndexChanged(int32_t index);
   void on_CB_IR_WIDTH4_currentIndexChanged(int32_t index);
   void on_CB_IR_WIDTH5_currentIndexChanged(int32_t index);
   void on_RB_TGTOPT1_clicked();
   void on_RB_TGTOPT2_clicked();
   void on_RB_TGTOPT3_clicked();
   void on_RB_TGTOPT4_clicked();
   void on_RB_TGTOPT5_clicked();
   void on_CB_EnableRTCK_clicked(bool checked);
   void on_CB_TAPReset_clicked(bool checked);
   void on_PB_OK_clicked();
   void on_PB_CANCEL_clicked();
   void on_SB_SCROLLBAR_MC_valueChanged(int32_t value);

private:
   Ui::CARCSetupDlg *ui;
};

#endif // ARCSETUPDLG_LINUX_H
