/****************************************************************************
Module: interface.cpp
Engineer: Vitezslav Hola
Description: Implementation of ARC MetaWare Processor Interface for Opella-XD
(as required by MetaWare in ARCINT.H)
Date           Initials    Description
08-Oct-2007    VH          Initial
16-Jan-2009    RSB          Modified for Unlock jtag support.
09-Sep-2009    RSB         TAP reset selection added
****************************************************************************/
#ifndef __LINUX
// Win specific defines
    #include "windows.h"
    #include "stdio.h"
	 #include "protocol\common\types.h"
    #include "..\..\utility\debug\debug.h"
// ARC interface header (exclude lint from these files)
//lint -save -e*
	//#define _MSC_VER 0   //We build it with GCC
    #define ASH_ARC_FOR_WINDOWS_GCC 1                              // required when building driver for MetaWare for Windows !!! with GCC compiler
    #define OEM_USE_OF_DEBUGGER_HEADER_FILES 1                     // this is required by arcint.h
    #include "arcint.h"
//lint -restore
#else
// Linux specific defines
//
///* SUSv2 version */
//#define _XOPEN_SOURCE 500
//
    #include <dlfcn.h>
    #include <unistd.h>
    #include <stdlib.h>
    #include <stdio.h>
    #include <malloc.h>
    #include <string.h>
	 #include "protocol/common/types.h"
// ARC interface header (exclude lint from these files)
//lint -save -e*
    #define ASH_ARC_FOR_WINDOWS_GCC 1                              // required when building driver for MetaWare for Windows !!! with GCC compiler
    #define OEM_USE_OF_DEBUGGER_HEADER_FILES 1                     // this is required by arcint.h
    #include "arcint.h"
//lint -restore
    #include "../../utility/debug/debug.h"
    #define  LoadLibrary(lib)                 dlopen(lib, RTLD_NOW)
    #define  GetProcAddress(handle, proc)     dlsym(handle, proc)
    #define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#endif // Linux specific defines


// must be included before other headers
#include "interface.h"
#include "ml_arc.h"      
#include "arcsetup.h"    

#define JTAG_SYSTEM_MODE      0x7

#ifdef __LINUX
// __LINUX
void __attribute__ ((constructor)) my_load(void);
void __attribute__ ((destructor))  my_unload(void);
#else
// Windows specific
extern "C" BOOL APIENTRY DllMain(HINSTANCE hInstDll, DWORD dwReason, LPVOID lpReserved);
#endif

extern "C" ASH_DLL_EXPORT struct TyArcInstance * get_ARC_interface(void);
extern "C" ASH_DLL_EXPORT struct TyArcInstance * get_ARC_interface_ex(PFARCSetupFunction pfExternalARCSetup);
extern "C" ASH_DLL_EXPORT void ASH_ListConnectedProbes(char ppszInstanceNumber[][16], unsigned short usMaxInstances, 
                                                       unsigned short *pusConnectedInstances);
extern "C" ASH_DLL_EXPORT const char *ASH_GetInterfaceID(void);
extern "C" ASH_DLL_EXPORT void ASH_SetSetupItem(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, const char *pszValue); 
extern "C" ASH_DLL_EXPORT void ASH_GetSetupItem(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue);
extern "C" ASH_DLL_EXPORT void ASH_GetLastErrorMessage(struct TyArcInstance *p, char *pszMessage, uint32_t uiSize);
extern "C" ASH_DLL_EXPORT int32_t ASH_TestScanchain(struct TyArcInstance *p);
extern "C" ASH_DLL_EXPORT int32_t ASH_AutoDetectScanChain(struct TyArcInstance *p);
extern "C" ASH_DLL_EXPORT int32_t ASH_DetectScanchain(struct TyArcInstance *p, uint32_t uiMaxCores, uint32_t *puiNumberOfCores, 
                                                  uint32_t *pulIRWidth, unsigned char *pbARCCores);
extern "C" ASH_DLL_EXPORT void ASH_UpdateScanchain(struct TyArcInstance *p);
extern "C" ASH_DLL_EXPORT void ASH_SetGeneralMessagePrint(PfPrintFunc pfFunc);
extern "C" ASH_DLL_EXPORT int32_t ASH_GDBScanIR(struct TyArcInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
extern "C" ASH_DLL_EXPORT int32_t ASH_GDBScanDR(struct TyArcInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
extern "C" ASH_DLL_EXPORT int32_t ASH_ScanIR(struct TyArcInstance *p, int32_t iLength, uint32_t *pulDataIn, uint32_t *pulDataOut, 
                                         unsigned char ucStartState, unsigned char ucEndState);
extern "C" ASH_DLL_EXPORT int32_t ASH_ScanDR(struct TyArcInstance *p, int32_t iLength, uint32_t *pulDataIn, uint32_t *pulDataOut, 
                                         unsigned char ucStartState, unsigned char ucEndState);
extern "C" ASH_DLL_EXPORT int32_t ASH_TMSReset(struct TyArcInstance *p);
extern "C" ASH_DLL_EXPORT int32_t ASH_TRSTReset(struct TyArcInstance *p);


// interface function prototypes
extern "C" int32_t DummyFunction(struct TyArcInstance *p);
extern "C" void destroy(struct TyArcInstance *p);
extern "C" int32_t version(struct TyArcInstance *p);
extern "C" const char * id(struct TyArcInstance *p);
extern "C" const char * additional_possibilities(struct TyArcInstance *p);
extern "C" void * additional_information(struct TyArcInstance *p, unsigned unused);
extern "C" int32_t is_simulator(struct TyArcInstance *p);
extern "C" int32_t process_property(struct TyArcInstance *p, const char *property, const char *value);
extern "C" int32_t prepare_for_new_program(struct TyArcInstance *p, int32_t will_download);
extern "C" int32_t run(struct TyArcInstance *p);
extern "C" int32_t step(struct TyArcInstance *p);
extern "C" int32_t read_reg(struct TyArcInstance *p, int32_t r, uint32_t *value);
extern "C" int32_t write_reg(struct TyArcInstance *p, int32_t r, uint32_t value);
extern "C" int32_t read_banked_reg(struct TyArcInstance *p, int32_t bank, int32_t r, uint32_t *value);
extern "C" int32_t write_banked_reg(struct TyArcInstance *p, int32_t bank, int32_t r, uint32_t *value);
extern "C" int32_t read_memory(struct TyArcInstance *p, uint32_t adr, void *buf, uint32_t amount, int32_t context);
extern "C" int32_t write_memory(struct TyArcInstance *p, uint32_t adr, void *buf, uint32_t amount, int32_t context);
extern "C" int32_t set_reg_watchpoint(struct TyArcInstance *p, int32_t r, int32_t length);
extern "C" int32_t remove_reg_watchpoint(struct TyArcInstance *p, int32_t r, int32_t length);
extern "C" int32_t set_mem_watchpoint(struct TyArcInstance *p, uint32_t addr, int32_t length);
extern "C" int32_t remove_mem_watchpoint(struct TyArcInstance *p, uint32_t addr, int32_t length);
extern "C" int32_t stopped_at_watchpoint(struct TyArcInstance *p);
extern "C" int32_t stopped_at_exception(struct TyArcInstance *p);
extern "C" int32_t set_breakpoint(struct TyArcInstance *p, unsigned addr, void *cookie);
extern "C" int32_t remove_breakpoint(struct TyArcInstance *p, unsigned addr, void *cookie);
extern "C" int32_t retrieve_breakpoint_code(struct TyArcInstance *p, unsigned address, char *dest, unsigned len, void *cookie_buf);
extern "C" int32_t breakpoint_cookie_len(struct TyArcInstance *p);
extern "C" int32_t at_breakpoint(struct TyArcInstance *p);
extern "C" unsigned memory_size(struct TyArcInstance *p);
extern "C" int32_t set_memory_size(struct TyArcInstance *p, unsigned S);
extern "C" int32_t define_displays(struct TyArcInstance *p, struct Register_display *rd);
extern "C" int32_t fill_memory(struct TyArcInstance *p, uint32_t adr, void *buf, uint32_t amount, uint32_t repeat_count, int32_t context);
extern "C" int32_t instruction_trace_count(struct TyArcInstance *p);
extern "C" void get_instruction_traces(struct TyArcInstance *p, uint32_t *traces);
extern "C" void receive_callback(struct TyArcInstance *p, ARC_callback *callback);
extern "C" int32_t supports_feature(struct TyArcInstance *p);
extern "C" uint32_t data_exchange(struct TyArcInstance *p, uint32_t what_to_do, uint32_t caller_endian, uint32_t send_amount, 
                                       void *send_buf, uint32_t max_receive_amount, void *receive_buf);
extern "C" int32_t in_same_process_as_debugger(struct TyArcInstance *p);
extern "C" uint32_t max_data_exchange_transfer(struct TyArcInstance *p);
extern "C" int32_t set_mem_watchpoint2(struct TyArcInstance *p, ARC_ADDR_TYPE addr, int32_t length, unsigned options, void **cookie);

// Local variables
static PfPrintFunc pfGeneralPrintFunc = NULL;
static int32_t iInterfaces=0;

// Local function prototypes
static unsigned short local_write_byte(struct TyArcInstance *p, uint32_t ulAddress, unsigned char ucData);
static int32_t get_free_actionpoint(struct TyArcInstance *p, int32_t *piAPNumber);
static int32_t remove_actionpoint(struct TyArcInstance *p, int32_t iAPNumber);
static int32_t recover_from_target_reset(struct TyArcInstance *p);
static void restore_all_actionpoint(struct TyArcInstance *p);
static void ConvertVersionDate(uint32_t ulValue, unsigned char bVersionFormat, char *pszBuffer, uint32_t uiSize);
int32_t ShowARCSetupWindowFromDll(struct TyArcInstance *p);
int32_t CallExternalARCSetup(struct TyArcInstance *p, PFARCSetupFunction pfExternalARCSetup);
void ShowDllStartupMessage(struct TyArcInstance *p);
void ShowDllHelpMessage(struct TyArcInstance *p);
void ShowProbeInformation(struct TyArcInstance *p, unsigned char bToDebugger);
void TRACE_PRINT(struct TyArcInstance *p, const char * strToFormat, ...);
void ERROR_PRINT(struct TyArcInstance *p, const char * strToFormat, ...);
void MESSAGE_PRINT(struct TyArcInstance *p, const char * strToFormat, ...);
void DEBUGGER_SLEEP(struct TyArcInstance *p, unsigned miliseconds);
void GENERAL_MESSAGE_PRINT(const char *pszMessage);
void GENERAL_PRINTF(const char * strToFormat, ...);
void StoreINIFile(struct TyArcInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename);
int32_t  LoadINIFile(struct TyArcInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename);
void ShowAAClockConfiguration(struct TyArcInstance *p);
static void SetCurrentCore(struct TyArcInstance *p);
static void SetupPrintMessage(struct TyArcInstance *p, const char *pszMessage, int32_t iSetupPrint);
static char pszTempMessage[255];

/****************************************************************************
*** MetaWare ARC Processor Interface functions                            ***
****************************************************************************/
#ifndef __LINUX
/****************************************************************************
Function: DllMain
Engineer: Vitezslav Hola
Input: HINSTANCE hInstDll - DLL handle
DWORD dwReason - reason whay this function was called
LPVOID lpReserved - reserved
Output: BOOL - TRUE if everythink is ok
Description: DLL main entry point, called when DLL is loaded/unloaded
Date           Initials    Description
08-Oct-2007    VH          Initial
****************************************************************************/
extern "C" BOOL APIENTRY DllMain(HINSTANCE hInstDll, DWORD dwReason, LPVOID lpReserved)
{
    lpReserved = lpReserved;         // make lint happy
    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        {  // loading DLL, this is time to get current path
            char pszLocDllPath[_MAX_PATH];
            // get DLL path name (without opxdarc.dll)
            if (!GetModuleFileName(hInstDll, pszLocDllPath,_MAX_PATH))
                ML_SetDllPath("");
            else
                ML_SetDllPath(pszLocDllPath);
            pfGeneralPrintFunc = NULL;
        }
        break;
    case DLL_PROCESS_DETACH:
        pfGeneralPrintFunc = NULL;
        if (ptyMLHandle != NULL)
        {  // close probe connection
            (void)ML_CloseConnection(ptyMLHandle);
            FREE(ptyMLHandle);
        }
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    default:
        break;
    }
    return TRUE;
}
#else
/****************************************************************************
Function: Sleep
Engineer: Nikolay Chokoev
Input: mseconds
Output: none
Description: 
Date           Initials    Description
18-Dec-2007    NCH         Initial
*****************************************************************************/
void Sleep(uint32_t  mseconds )
{
    // Windows Sleep uses miliseconds
    // linux usleep uses microsecond
    usleep( mseconds * 1000 );
}

// This function will be called when the library is loaded and before dlopen() returns
void my_load(void)
{
#define SIZE 260
    FILE *f;
    char line[SIZE];
    char* pszPath;


    f = fopen ("/proc/self/maps", "r");

    while (!feof (f))
    {
        if (fgets (line, SIZE, f) == NULL)
            break;

        /* Sanity check. */
        if (strstr (line, "opxdarc.so") == NULL)
            continue;

        pszPath = strchr (line, '/');

        ML_SetDllPath(pszPath);
    }

    fclose (f);

    // Add initialization code�
}

// This function will be called when the library is unloaded and before dlclose() returns
void my_unload(void)
{
    // Add clean-up code�
}
#endif

/****************************************************************************
Function: get_ARC_interface
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Initial function called by MetaWare debugger. 
Must return prepared structure with debugger interface functions.
This function MUST be exported from DLL so debugger can call it.
Date           Initials    Description
10-Oct-2007    VH          Initial
04-Dec-2007    VH          Modified to use extended function
****************************************************************************/
extern "C" ASH_DLL_EXPORT struct TyArcInstance * get_ARC_interface(void)
{
    // just call externded function with NULL parameter
    return get_ARC_interface_ex(NULL);
}

/****************************************************************************
Function: get_ARC_interface_ex
Engineer: Vitezslav Hola
Input: PFARCSetupFunction pfExternalARCSetup - external callback function (NULL for default ARC function)
Output: struct TyArcInstance * - pointer to allocated debug structure
Description: Initial function called by MetaWare debugger. 
Must return prepared structure with debugger interface functions.
This function MUST be exported from DLL so debugger can call it.
Date           Initials    Description
04-Dec-2007    VH          Initial
12-Apr-2012    SPT         Changed initial selection of target to ARC normal TPA Rev. 0
****************************************************************************/
extern "C" ASH_DLL_EXPORT struct TyArcInstance * get_ARC_interface_ex(PFARCSetupFunction pfExternalARCSetup)
{
    int32_t iRes;
    int32_t iError =0;
    TyArcTargetParams *ptyLocMLHandle = NULL;
    TyFunctionTable *ptyLocFunctionTable = NULL;
    struct TyArcInstance *ptyLocArcInstance = NULL;
    char ppszLocInstanceNumber[16][16];
    unsigned short usConnectedOpellaXD;
    unsigned char bConnectionOk = 0;

    //ensure no device handle is assigned
    //ptyMLHandle = NULL;

    // first allocate memory for all dynamic structures and check if available
    ptyLocArcInstance = (struct TyArcInstance*)malloc(sizeof(struct TyArcInstance));
    ptyLocFunctionTable = (TyFunctionTable *)malloc(sizeof(TyFunctionTable));
    if ((ptyLocArcInstance == NULL) || (ptyLocFunctionTable == NULL))
    {  // we can do anything but return with error
        if (ptyLocArcInstance != NULL)
            FREE(ptyLocArcInstance);
        if (ptyLocFunctionTable != NULL)
            FREE(ptyLocFunctionTable);
        return NULL;
    }
    // clear all allocated structures just for sure
    (void)memset(ptyLocArcInstance, 0, sizeof(struct TyArcInstance));
    (void)memset(ptyLocFunctionTable, 0, sizeof(TyFunctionTable));

    // allocation of all structures was successful so link them together
    ptyLocArcInstance->ptyFunctionTable = ptyLocFunctionTable;
    // now fill pointers to function table
    ptyLocArcInstance->ptyFunctionTable->DummyFunction             = DummyFunction;
    ptyLocArcInstance->ptyFunctionTable->destroy                   = destroy;
    ptyLocArcInstance->ptyFunctionTable->version                   = version;
    ptyLocArcInstance->ptyFunctionTable->id                        = id;
    ptyLocArcInstance->ptyFunctionTable->additional_possibilities  = additional_possibilities;
    ptyLocArcInstance->ptyFunctionTable->additional_information    = additional_information;
    ptyLocArcInstance->ptyFunctionTable->is_simulator              = is_simulator;
    ptyLocArcInstance->ptyFunctionTable->process_property          = process_property;
    ptyLocArcInstance->ptyFunctionTable->run                       = run;
    ptyLocArcInstance->ptyFunctionTable->prepare_for_new_program   = prepare_for_new_program;
    ptyLocArcInstance->ptyFunctionTable->step                      = step;
    ptyLocArcInstance->ptyFunctionTable->read_reg                  = read_reg;
    ptyLocArcInstance->ptyFunctionTable->write_reg                 = write_reg;
    ptyLocArcInstance->ptyFunctionTable->read_memory               = read_memory;
    ptyLocArcInstance->ptyFunctionTable->write_memory              = write_memory;
    ptyLocArcInstance->ptyFunctionTable->read_banked_reg           = read_banked_reg;
    ptyLocArcInstance->ptyFunctionTable->write_banked_reg          = write_banked_reg;
    ptyLocArcInstance->ptyFunctionTable->set_reg_watchpoint        = set_reg_watchpoint;
    ptyLocArcInstance->ptyFunctionTable->remove_reg_watchpoint     = remove_reg_watchpoint;
    ptyLocArcInstance->ptyFunctionTable->set_mem_watchpoint        = set_mem_watchpoint;
    ptyLocArcInstance->ptyFunctionTable->remove_mem_watchpoint     = remove_mem_watchpoint;
    ptyLocArcInstance->ptyFunctionTable->stopped_at_watchpoint     = stopped_at_watchpoint;
    ptyLocArcInstance->ptyFunctionTable->stopped_at_exception      = stopped_at_exception;
    ptyLocArcInstance->ptyFunctionTable->set_breakpoint            = set_breakpoint;
    ptyLocArcInstance->ptyFunctionTable->remove_breakpoint         = remove_breakpoint;
    ptyLocArcInstance->ptyFunctionTable->retrieve_breakpoint_code  = retrieve_breakpoint_code;
    ptyLocArcInstance->ptyFunctionTable->breakpoint_cookie_len     = breakpoint_cookie_len;
    ptyLocArcInstance->ptyFunctionTable->at_breakpoint             = at_breakpoint;
    ptyLocArcInstance->ptyFunctionTable->memory_size               = memory_size;
    ptyLocArcInstance->ptyFunctionTable->set_memory_size           = set_memory_size;
    ptyLocArcInstance->ptyFunctionTable->define_displays           = define_displays;
    ptyLocArcInstance->ptyFunctionTable->fill_memory               = fill_memory;
    ptyLocArcInstance->ptyFunctionTable->instruction_trace_count   = instruction_trace_count;
    ptyLocArcInstance->ptyFunctionTable->get_instruction_traces    = get_instruction_traces;
    ptyLocArcInstance->ptyFunctionTable->receive_callback          = receive_callback;
    ptyLocArcInstance->ptyFunctionTable->supports_feature          = supports_feature;
    ptyLocArcInstance->ptyFunctionTable->data_exchange             = data_exchange;
    ptyLocArcInstance->ptyFunctionTable->in_same_process_as_debugger = in_same_process_as_debugger;
    ptyLocArcInstance->ptyFunctionTable->max_data_exchange_transfer  = max_data_exchange_transfer;
    ptyLocArcInstance->ptyFunctionTable->set_mem_watchpoint2       = set_mem_watchpoint2;

    // initialize APs
    ptyLocArcInstance->ulAPUsed = 0;
    ptyLocArcInstance->bAPFullSet = 0;
    ptyLocArcInstance->uiPMVersion = 0;
    ptyLocArcInstance->ucAPVersion = 0;
    ptyLocArcInstance->iNumberOfAP = 0;
    // default memory size as max
    ptyLocArcInstance->ulMemorySize = 0xFFFFFFFF;
    // initialize callback function pointer
    ptyLocArcInstance->pcbCallbackFunction = NULL;
    // no trace messages
    ptyLocArcInstance->bTraceMessagesEnabled = FALSE;

    // GUI used only when not using external setup callback
    if (pfExternalARCSetup == NULL)
        ptyLocArcInstance->bUseGUI = TRUE;
    else
        ptyLocArcInstance->bUseGUI = FALSE;

    ptyLocArcInstance->bStartupInfoShown = FALSE;

    // assuming this interface belongs to core 1.
    ptyLocArcInstance->uiCoreNumber = 1;
    // let use simple option, take first connected Opella-XD in the list and try to use it
    bConnectionOk       = 0;
    usConnectedOpellaXD = 0;

    // check if it is the first interface to be opened
    if (iInterfaces == 0)
    {
        // Prepare pointer to ML handle for target params
        ptyLocMLHandle = (TyArcTargetParams *)malloc(sizeof(TyArcTargetParams));
        if (ptyLocMLHandle == NULL)
        {// we can do anything but return with error
            FREE(ptyLocArcInstance);
            FREE(ptyLocFunctionTable);
            return NULL;
        }
        (void)memset(ptyLocMLHandle, 0, sizeof(TyArcTargetParams));
        ptyMLHandle = ptyLocMLHandle;

        // get connected Opella-XD and pick first
        ML_GetListOfConnectedProbes(ppszLocInstanceNumber, 16, &usConnectedOpellaXD);
        if (usConnectedOpellaXD)
        {
            strncpy(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0], 15);
            ptyMLHandle->pszProbeInstanceNumber[15] = 0;
        }

        // ini file location
        if (pszIniFileName == NULL)
        {
            memset(ptyLocArcInstance->pszIniFileLocation, 0, 256);
            ML_GetDllPath(ptyLocArcInstance->pszIniFileLocation, _MAX_PATH);
            strncat(ptyLocArcInstance->pszIniFileLocation, "/", _MAX_PATH - strlen(ptyLocArcInstance->pszIniFileLocation));
            strncat(ptyLocArcInstance->pszIniFileLocation, ARC_INI_DEFAULT_NAME, _MAX_PATH - strlen(ptyLocArcInstance->pszIniFileLocation));
        }
        else
            strcpy(ptyLocArcInstance->pszIniFileLocation, pszIniFileName);

        // now it is time to initialize probe settings
        // note we do not bother with default values since it will be set in ML_OpenConnection
        // we need only specify Opella-XD instance number and connection flag should be 0
        ptyMLHandle->bConnectionInitialized = 0;
        // select ARC target type
        ptyMLHandle->tyArcTargetType       = ARC_TGT_NORMAL;        // starting with ARC normal TPA rev. 0
        ptyMLHandle->bAdaptiveClockEnabled = FALSE;                 // no adaptive clock
        ptyMLHandle->bDoNotIssueTAPReset   = TRUE;                     // no TAP reset
        // define muticore structure
        ptyMLHandle->ulNumberOfCoresOnScanchain          = 1;     // 1 core on scanchain
        ptyMLHandle->tyScanchainCoreParams[0].bARCCore   = TRUE;  // it is ARC core
        ptyMLHandle->tyScanchainCoreParams[0].ulIRLength = 4;     // IR length

        // now we need to get ARC setup, using either external GUI DLL 
        // or external setup callback. INI file is used in both cases.
        (void)LoadINIFile(ptyLocArcInstance, ARC_INI_ENV_VARIABLE, ARC_INI_DEFAULT_NAME);

        // setup only from the first interface
        if (pfExternalARCSetup != NULL)
        {
            if (!CallExternalARCSetup(ptyLocArcInstance, pfExternalARCSetup))
            { // ARC setup selection via external callback
                FREE(ptyLocArcInstance);
                FREE(ptyLocFunctionTable);
                FREE(ptyLocMLHandle);
                ptyMLHandle = NULL;
                return NULL;
            }
        }
        else
        {
            unsigned char ucIndex;

            for (ucIndex = 0; ucIndex < 16; ucIndex++)
            {
                if (strncmp(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[ucIndex], 15) == 0)
                    break; //we have a match 
            }

            if (   (ucIndex >= 16)
                   && (usConnectedOpellaXD == 1))
            {
                sprintf(pszTempMessage,
                        "OPXDARC.INI configuration file uses Opella-XD serial number:%s which is not connected. Continuing with Opella-XD serial number:%s.\n",
                        ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0]);
                GENERAL_MESSAGE_PRINT(pszTempMessage);
                SetupPrintMessage(NULL, pszTempMessage, 0); //Save message
                strncpy(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0], 15);
                ptyMLHandle->pszProbeInstanceNumber[15] = 0;
            }

            if (ptyLocArcInstance->bUseGUI)
            { // call GUI (supported only for Windows when not using external callback)
                if (!ShowARCSetupWindowFromDll(ptyLocArcInstance))
                { // ARC setup selection has been canceled, so cancel whole initialization
                    FREE(ptyLocArcInstance);
                    FREE(ptyLocFunctionTable);
                    FREE(ptyLocMLHandle);
                    ptyMLHandle = NULL;
                    return NULL;
                }
            }
        }

        // try to store current configuration into file for next session
        StoreINIFile(ptyLocArcInstance, ARC_INI_ENV_VARIABLE, ARC_INI_DEFAULT_NAME);
        // first check probe firmware
        // now there must be valid Opella-XD serial number, so try opening connection
        iRes = ML_UpgradeFirmware(ptyMLHandle, GENERAL_MESSAGE_PRINT);
        if ((iRes == ERR_ML_ARC_NO_ERROR) || 
            (iRes == ERR_ML_ARC_ALREADY_INITIALIZED))
        {
            /* Added to solve the reconnect issue in some machine if Auto-Scan button is not clicked. */
            for (int32_t i = 0 ;  i < 10; i++)
            {
                //ML_Sleep(NULL, 1000);
                iRes = ML_OpenConnection(ptyMLHandle);
                if ((iRes == ERR_ML_ARC_NO_ERROR) || 
                    (iRes == ERR_ML_ARC_ALREADY_INITIALIZED))
                    bConnectionOk = 1;
                else
                {
                    GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));
                }

                if (bConnectionOk == 1)
                {
                    break;
                }
            }
        }
        else
            GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));

        // check if connection is ok
        if (!bConnectionOk)
        {  // there is no probe or error occured when openning connection
            FREE(ptyLocArcInstance);
            FREE(ptyLocFunctionTable);
            FREE(ptyLocMLHandle);
            ptyMLHandle = NULL;
            return NULL;
        }
    }

    //    if (iInterfaces+1 > (int32_t)ptyMLHandle->ulNumberOfCoresOnScanchain)
    //       {  // somebody try to open more interfaces than the cores in a chain
    //       free(ptyLocArcInstance);
    //       free(ptyLocFunctionTable);
    //       // we are going to close the only interface
    //       if((ptyLocMLHandle!=NULL) && (iInterfaces < 1))
    //          free(ptyLocMLHandle);
    //       return NULL;
    //       }

    // Interface successfuly created. Increase interface number
    iInterfaces++;
    if (!ptyMLHandle->bDoNotIssueTAPReset)
    {
        iError = ML_ResetTAP(ptyMLHandle,0);
        if (iError != ERR_ML_ARC_NO_ERROR)
            return ASH_FALSE;
    }

    return ptyLocArcInstance;
}

/****************************************************************************
Function: DummyFunction
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Dummy function, just returning 0.
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
extern "C" int32_t DummyFunction(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "DummyFunction\n");
    return 0;
}

/****************************************************************************
Function: destroy
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Free all allocated memory (from get_ARC_interface) and 
close connection.
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" void destroy(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "destroy\n");
    // free allocated memory
    if (p != NULL)
    {
        if (p->ptyFunctionTable != NULL)
            FREE(p->ptyFunctionTable);
        //if it is the last interface - close the door

        if (iInterfaces == 1)
        {
            if (ptyMLHandle != NULL)
            {  // close probe connection
                (void)ML_CloseConnection(ptyMLHandle);
                FREE(ptyMLHandle);
            }
        }

        FREE(p);
        iInterfaces--;
        //this should never happen but ...
        if (iInterfaces < 0)
            iInterfaces = 0;
    }
}

/****************************************************************************
Function: version
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Get interface version
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
extern "C" int32_t version(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "version\n");
    return ARCINT_TRACE_VERSION;
}

/****************************************************************************
Function: id
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
This function is used by MetaWare debugger to obtain interface 
name as string. It is used in some info messages by MetaWare.
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
extern "C" const char * id(struct TyArcInstance *p)
{
    static char szCompleteVersionString  [_MAX_PATH];

    TRACE_PRINT(p, "id\n");
    strncpy(szCompleteVersionString, INTERFACE_NAME, _MAX_PATH-1);
    szCompleteVersionString[_MAX_PATH-1] = 0;    // ensure string is terminated
    return szCompleteVersionString;
}

/****************************************************************************
Function: additional_possibilities
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
extern "C" const char * additional_possibilities(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "additional_possibilities\n");
    return NULL;
}

/****************************************************************************
Function: additional_information
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
extern "C" void * additional_information(struct TyArcInstance *p, unsigned unused)
{
    unused = unused;
    TRACE_PRINT(p, "additional_information\n");
    return NULL;
}

/****************************************************************************
Function: is_simulator
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
extern "C" int32_t is_simulator(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "is_simulator\n");
    return ASH_FALSE;
}

/****************************************************************************
Function: process_property
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Processing special requests from MetaWare debugger using 
properties.
Date           Initials    Description
16-Jul-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
09-Sep-2009    RSB         TAP reset done after Target reset.
09-May-2016    ST          Added BSCI_SystemModeReg property
****************************************************************************/
extern "C" int32_t process_property(struct TyArcInstance *p, const char *property, const char *value)
{
    TRACE_PRINT(p, "process_property [%s] [%s]\n", property, value);

    // check property string, check its value and do the action
    if (property != NULL && value != NULL)
    {
        // "trace_messages" property
        if (!strcmp(property, "trace_messages"))
        {     // enabling/disabling debug messages from driver
            uint32_t uiTraceMessages = 0;
            if (sscanf(value, "%d", &uiTraceMessages) != 1)
            {
                ERROR_PRINT(p, MSG_ML_INVALID_PROPERTY_FORMAT);
                return ASH_TRUE;
            }
            if ((uiTraceMessages != 0) &&
                (uiTraceMessages != 1))      // check validity
            {
                ERROR_PRINT(p, MSG_ML_INVALID_PROPERTY_FORMAT);
                return ASH_TRUE;
            }

            if (uiTraceMessages == 0)
                p->bTraceMessagesEnabled = FALSE;
            else
                p->bTraceMessagesEnabled = TRUE;

            return ASH_TRUE;
        }

        // "BSCI_SystemModeReg" property
        if (!strcmp(property, "BSCI_SystemModeReg"))
        {
            Sleep(1000);
            // system mode must be configured through the System TAP
            p->uiCoreNumber = 1;
            SetCurrentCore(p);
            uint32_t ulDataIn;

            uint32_t uiBSCISystemModeReg = 0;
            sscanf(value, "%x", &uiBSCISystemModeReg);

            // write command into transaction register (update system mode)
            ulDataIn = JTAG_SYSTEM_MODE;            
            (void)ASH_ScanIR(p, 4, &ulDataIn, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_DR);
            ulDataIn = uiBSCISystemModeReg;
            (void)ASH_ScanDR(p, 16, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_RTI);  

            //switch back to the ARC TAP
            p->uiCoreNumber = 2;
            SetCurrentCore(p);

            return ASH_TRUE;
        }

        // "load INI file" property
        if (!strcmp(property, "ini_file"))
        {
            char pszTextMessage[500];

            //store current values
            TyArcTargetParams    *ptyLocMLHandle    = NULL;
            struct TyArcInstance *ptyLocArcInstance = NULL;

            ptyLocMLHandle = (TyArcTargetParams *)malloc(sizeof(TyArcTargetParams));
            if (ptyLocMLHandle == NULL)
                return ASH_FALSE;

            ptyLocArcInstance = (struct TyArcInstance*)malloc(sizeof(struct TyArcInstance));
            if (ptyLocArcInstance == NULL)
            {
                free(ptyLocMLHandle);
                free(ptyLocArcInstance);
                return ASH_FALSE;
            }

            memcpy(ptyLocMLHandle, ptyMLHandle, sizeof(TyArcTargetParams));
            memcpy(ptyLocArcInstance, p, sizeof(TyArcInstance));

            //close current connection
            if (ptyMLHandle->bConnectionInitialized == 1)
            {
                (void)ML_CloseConnection(ptyMLHandle);
                ptyMLHandle->bConnectionInitialized = 0;
            }

            strcpy(p->pszIniFileLocation, value);

            //load INI file
            if (LoadINIFile(p, NULL, ARC_INI_DEFAULT_NAME) == -1)
            {
                //restore values
                memcpy(ptyMLHandle, ptyLocMLHandle, sizeof(TyArcTargetParams));
                memcpy(p, ptyLocArcInstance, sizeof(TyArcInstance));

                free(ptyLocMLHandle);
                free(ptyLocArcInstance);

                sprintf(pszTextMessage, "\n\nUnable to switch to INI file: %s!!!\n\n", value);
                MESSAGE_PRINT(p, pszTextMessage);
                GENERAL_MESSAGE_PRINT(pszTextMessage);

                return ASH_FALSE;
            }

            //re-open connection
            if (ML_OpenConnection(ptyMLHandle) != ERR_ML_ARC_NO_ERROR)
            {
                ptyMLHandle->bConnectionInitialized = 0;

                free(ptyLocMLHandle);
                free(ptyLocArcInstance);

                return ASH_FALSE;
            }
            else
            {
                ptyMLHandle->bConnectionInitialized = 1;

                free(ptyLocMLHandle);
                free(ptyLocArcInstance);

                sprintf(pszTextMessage, "\n\nSuccessfully switched to INI file: %s!!!\n\n", value);
                MESSAGE_PRINT(p, pszTextMessage);
                GENERAL_MESSAGE_PRINT(pszTextMessage);

                return ASH_TRUE;
            }
        }

        // -------------------------------------------------------------------------------
        // Following properties are specific for ARCangel target
        // 
        // "blast" property
        if (!strcmp(property, "blast"))
        {     // blast FPGA from XBF file into ARCangel
            int32_t iRes;
            MESSAGE_PRINT(p, "Blasting ARCangel FPGA at %.1f MHz, please wait ...\n", ptyMLHandle->dBlastFrequency);
            iRes = ML_BlastFileToFpga(ptyMLHandle, value);
            if (iRes != ERR_ML_ARC_NO_ERROR)
            {
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
                MESSAGE_PRINT(p, "Blasting has failed.\n");
            }
            else
                MESSAGE_PRINT(p, "Blasting has been finished successfully.\n");
            return ASH_TRUE;
        }

        // "mblast" property
        if (!strcmp(property, "mblast"))
        {     // blast FPGA from memory into ARCangel
            int32_t iRes;
            MESSAGE_PRINT(p, "Blasting ARCangel FPGA at %.1f MHz, please wait ...\n", ptyMLHandle->dBlastFrequency);
            iRes = ML_BlastMemToFpga(ptyMLHandle, value);
            if (iRes != ERR_ML_ARC_NO_ERROR)
            {
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
                MESSAGE_PRINT(p, "Blasting has failed.\n");
            }
            else
                MESSAGE_PRINT(p, "Blasting has been finished successfully.\n");
            return ASH_TRUE;
        }

        // "extcmd" property
        if (!strcmp(property, "extcmd"))
        {
            int32_t iRes;
            uint32_t uiCmd = 0;
            uint32_t uiValue = 0;
            unsigned short usResult = 0;

            TRACE_PRINT(p, "extcmd [%x][%x]\n", uiCmd, uiValue);
            if (!ML_IsTargetARCangel(ptyMLHandle))
                return ASH_TRUE;
            if ((sscanf(value, "%x,%x", &uiCmd, &uiValue) != 2) || (uiCmd > 0xff))
            {
                ERROR_PRINT(p, MSG_ML_INVALID_PROPERTY_FORMAT);
                return ASH_TRUE;
            }
            switch (uiCmd)
            {
            case 0x4:
                MESSAGE_PRINT(p, "WARNING: Changing PLL settings directly is not recommended. Please use gclk properties instead.\n");
                iRes = ML_AAExtendedCommand(ptyMLHandle, 0x04, (uint32_t)uiValue, &usResult);
                break;
            case 0x5:
                {
                    MESSAGE_PRINT(p, "WARNING: Changing clock routing directly is not recommended. Please use gclk properties instead.\n");
                    iRes = ML_AAExtendedCommand(ptyMLHandle, 0x05, (uint32_t)uiValue, &usResult);
                    break;
                }
            default:
                {
                    iRes = ML_AAExtendedCommand(ptyMLHandle, (unsigned char)uiCmd, (uint32_t)uiValue, &usResult);
                    break;
                }
            }
            // show result and error handling
            if (iRes != ERR_ML_ARC_NO_ERROR)
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            else
                MESSAGE_PRINT(p, "extended command [%x]\n", usResult);
            return ASH_TRUE;
        }

        // "gclk" properties
        if ((!strcmp(property, "gclk")) || (!strcmp(property, "gclk0")) || (!strcmp(property, "gclk1")) || 
            (!strcmp(property, "gclk2")) || (!strcmp(property, "gclk3"))
           )
        {
            TRACE_PRINT(p, "gclk [%s] [%s]\n", property, value);
            if (!ML_IsTargetARCangel(ptyMLHandle))
                return ASH_TRUE;

            if (!strcmp(value, ""))
            {  // just display current configuration
                ShowAAClockConfiguration(p);
            }
            else
            {
                int32_t iRes;
                int32_t iGclk = 0;
                TyArcAAGclk tyAAGclk = AA_GCLK_HIGHIMP;
                double dFrequency = 0.0;
                // which GCLK should we set ?
                if (!strcmp(property, "gclk0"))
                    iGclk = 0;
                else if (!strcmp(property, "gclk1"))
                    iGclk = 1;
                else if (!strcmp(property, "gclk2"))
                    iGclk = 2;
                else  // "gclk" or "gclk3"
                    iGclk = 3;
                // what source to use for particular GCLK
                if (!strcmp(value, "highimp"))
                    tyAAGclk = AA_GCLK_HIGHIMP;
                else if (!strcmp(value, "crystal"))
                    tyAAGclk = AA_GCLK_CRYSTAL;
                else if (!strcmp(value, "dips"))
                    tyAAGclk = AA_GCLK_DIPS;
                else if (!strcmp(value, "host"))
                    tyAAGclk = AA_GCLK_HOST;
                else
                {
                    tyAAGclk = AA_GCLK_MCLK;      // does not matter if MCLK or VCLK, proper allocation will happen later
                    dFrequency = strtod(value, NULL);
                }
                iRes = ML_AASetClockAllocation(ptyMLHandle, iGclk, tyAAGclk, dFrequency);
                if (iRes != ERR_ML_ARC_NO_ERROR)
                    ERROR_PRINT(p, ML_GetErrorMessage(iRes));
                ShowAAClockConfiguration(p);
            }
            return ASH_TRUE;
        }

        // "roboSDRAM" property
        if ((!strcmp(property, "roboSDRAM")) || (!strcmp(property, "robosdram")))
        {
            int32_t iRes;
            unsigned short usResponse;
            uint32_t uiValue = 0;
            // int32_t iRes = ERR_ML_ARC_NO_ERROR;
            if (!ML_IsTargetARCangel(ptyMLHandle))
                return ASH_TRUE;
            // decode command parameters
            if ((sscanf(value, "%x", &uiValue) != 1) || (uiValue > 0xFFFF))
            {
                ERROR_PRINT(p, MSG_ML_INVALID_PROPERTY_FORMAT);
                return ASH_TRUE;
            }
            iRes = ML_AASetRoboClock(ptyMLHandle, 0x06, (unsigned short)uiValue, &usResponse);
            if (iRes != ERR_ML_ARC_NO_ERROR)
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            else
                MESSAGE_PRINT(p, "roboSDRAM set to [%x]\n", uiValue);
            TRACE_PRINT(p, "response [%x]\n", usResponse);
            return ASH_TRUE;
        }

        // "roboSSRAM" property
        if ((!strcmp(property, "roboSSRAM")) || (!strcmp(property, "robossram")))
        {
            int32_t iRes;
            unsigned short usResponse;
            uint32_t uiValue = 0;
            // int32_t iRes = ERR_ML_ARC_NO_ERROR;
            if (!ML_IsTargetARCangel(ptyMLHandle))
                return ASH_TRUE;
            // decode command parameters
            if ((sscanf(value, "%x", &uiValue) != 1) || (uiValue > 0xFFFF))
            {
                ERROR_PRINT(p, MSG_ML_INVALID_PROPERTY_FORMAT);
                return ASH_TRUE;
            }
            iRes = ML_AASetRoboClock(ptyMLHandle, 0x07, (unsigned short)uiValue, &usResponse);
            if (iRes != ERR_ML_ARC_NO_ERROR)
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            else
                MESSAGE_PRINT(p, "roboSSRAM set to [%x]\n", uiValue);
            TRACE_PRINT(p, "response [%x]\n", usResponse);
            return ASH_TRUE;
        }

        // "roboSSRAMEXP" property
        if ((!strcmp(property, "roboSSRAMEXP")) || (!strcmp(property, "robossramexp")))
        {
            int32_t iRes;
            unsigned short usResponse;
            uint32_t uiValue = 0;
            // int32_t iRes = ERR_ML_ARC_NO_ERROR;
            if (!ML_IsTargetARCangel(ptyMLHandle))
                return ASH_TRUE;
            // decode command parameters
            if ((sscanf(value, "%x", &uiValue) != 1) || (uiValue > 0xFFFF))
            {
                ERROR_PRINT(p, MSG_ML_INVALID_PROPERTY_FORMAT);
                return ASH_TRUE;
            }
            iRes = ML_AASetRoboClock(ptyMLHandle, 0x08, (unsigned short)uiValue, &usResponse);
            if (iRes != ERR_ML_ARC_NO_ERROR)
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            else
                MESSAGE_PRINT(p, "roboSSRAMEXP set to [%x]\n", uiValue);
            TRACE_PRINT(p, "response [%x]\n", usResponse);
            return ASH_TRUE;
        }
        // 
        // End of properties specific for ARCangel target
        // -------------------------------------------------------------------------------


        // -------------------------------------------------------------------------------
        // Following properties are specific for ARC target with reset pin
        // 
        // "reset_duration" property
        if (!strcmp(property, "reset_duration"))
        {
            uint32_t uiValue = 0xFFFFFFFF;
            if (sscanf(value, "%d", &uiValue) == 1)
                (void)ML_SetResetProperty(ptyMLHandle, FALSE, &uiValue);
            else
                (void)ML_SetResetProperty(ptyMLHandle, FALSE, NULL);              // if parsing fails, use default settings
            return ASH_TRUE;
        }

        // "reset_delay" property
        if (!strcmp(property, "reset_delay"))
        {
            uint32_t uiValue = 0xFFFFFFFF;
            if (sscanf(value, "%d", &uiValue) == 1)
                (void)ML_SetResetProperty(ptyMLHandle, TRUE, &uiValue);
            else
                (void)ML_SetResetProperty(ptyMLHandle, TRUE, NULL);               // if parsing fails, use default settings
            return ASH_TRUE;
        }

        // "reset_board"/"reset_target" property	  
        if ((!strcmp(property, "reset_target")) || (!strcmp(property, "reset_board")))
        {     // reset target
            //		 MESSAGE_PRINT(p, "Target reset .\n");

            if (ptyMLHandle->tyArcTargetType == ARC_TGT_CJTAG_TPA_R1)
                (void)ML_ResetTarget(ptyMLHandle);

            if (!ptyMLHandle->bDoNotIssueTAPReset)
            {
                (void)ML_ResetTarget(ptyMLHandle);
                (void)ML_ResetTAP(ptyMLHandle,0);
            }
            return ASH_TRUE;
        }
        // 
        // End of properties specific for ARC target with reset pin
        // -------------------------------------------------------------------------------


        // -------------------------------------------------------------------------------
        // Following properties are general properties used by MetaWare debugger
        //
        // We do not care about error values from ML_xxx functions for these properties 
        // 
        // "dll_help" property
        if (!strcmp(property, "dll_help"))
        {     // just show DLL help message to used (list of supported commands)
            ShowDllHelpMessage(p);
            return ASH_TRUE;
        }

        // "probe_info" property
        if (!strcmp(property, "probe_info"))
        {     // show information about probe (version, etc.)
            MESSAGE_PRINT(p, "*** %s\n", INTERFACE_NAME_AND_VERSION);               // first show DLL info
            ShowProbeInformation(p, TRUE);
            return ASH_TRUE;
        }

        // "port" property
        if (!strcmp(property, "port"))
        {     // we do not use LPT port but just tell debugger we are fine
            return ASH_TRUE;
        }

        // "cpunum" property
        if (!strcmp(property, "cpunum"))
        {     // set cpu number for debug session
            uint32_t uiValue = 0;
            if (sscanf(value, "%d", &uiValue) != 1)
                return ASH_TRUE;
            p->uiCoreNumber=uiValue;
            SetCurrentCore(p);
            return ASH_TRUE;
        }

        // "cycle_step" property
        if (!strcmp(property, "cycle_step"))
        {
            uint32_t uiValue = 0;
            if (sscanf(value, "%d", &uiValue) != 1)
                return ASH_TRUE;
            (void)ML_SetCycleStep(ptyMLHandle, (uiValue)? 1 : 0);
            return ASH_TRUE;
        }

        // "invalidate_icache" toggle
        if (!strcmp(property, "invalidate_icache"))
        {     // enable/disable skipping of icache invalidation
            uint32_t uiValue = 0;
            if (sscanf(value, "%d", &uiValue) != 1)
                return ASH_TRUE;
            if (!uiValue)
            {     // skip icache invalidation after memory writes
                (void)ML_InvalidatingICache(ptyMLHandle, TRUE);
                MESSAGE_PRINT(p, "WARNING: with invalidate_icache off, software breakpoints most likely will fail.\n");
            }
            else
                (void)ML_InvalidatingICache(ptyMLHandle, FALSE);
            return ASH_TRUE;
        }

        // "endian" property
        if (!strcmp(property, "endian"))
        {     // setting endianess of target (LE or BE), LE is default
            if (!strcmp(value, "BE") || !strcmp(value, "be"))
                (void)ML_SetTargetEndian(ptyMLHandle, TRUE);                         // target is BE
            else if (!strcmp(value, "LE") || !strcmp(value, "le"))
                (void)ML_SetTargetEndian(ptyMLHandle, FALSE);                        // target is LE
            return ASH_TRUE;
        }
        // 
        // End of general properties for MetaWare debugger
        // -------------------------------------------------------------------------------

        // -------------------------------------------------------------------------------
        // Following properties are specific for target with reset detection
        // 
        // "on_target_reset" property
        if (!strcmp(property, "on_target_reset"))
        {
            uint32_t pulLocParams[3];
            pulLocParams[0] = 0x0;  pulLocParams[1] = 0x0;  pulLocParams[2] = 0x0;
            if (!strcmp(value, "init"))
                (void)ML_SetOnTargetResetAction(ptyMLHandle, RESACT_INIT, pulLocParams);
            else if (!strcmp(value, "restore_ap"))
                (void)ML_SetOnTargetResetAction(ptyMLHandle, RESACT_RESTORE_AP, pulLocParams);
            else if (!strcmp(value, "disable_rtck"))
                (void)ML_SetOnTargetResetAction(ptyMLHandle, RESACT_RTCK_DISABLE, pulLocParams);
            else if (!strcmp(value, "enable_rtck"))
                (void)ML_SetOnTargetResetAction(ptyMLHandle, RESACT_RTCK_ENABLE, pulLocParams);
            else if (strstr(value, "delay,") != NULL)
            {
                uint32_t uiLocDelay;
                if ((sscanf(value + strlen("delay,"), "%u", &uiLocDelay) == 1) && (uiLocDelay <= 10000))
                {  // wait some delay (maximum 10 s)
                    pulLocParams[0] = (uint32_t)uiLocDelay;    pulLocParams[1] = 0x0;     pulLocParams[2] = 0x0;
                    (void)ML_SetOnTargetResetAction(ptyMLHandle, RESACT_DELAY, pulLocParams);
                }
            }
            else
            {
                uint32_t uiLocAddress, uiLocSetBits, uiLocClearBits;
                if (sscanf(value, "%x,%x,%x", &uiLocAddress, &uiLocSetBits, &uiLocClearBits) == 3)
                {  // we want to write to specific address, providing bits to be clear and bits to be set
                    pulLocParams[0] = (uint32_t)uiLocAddress;
                    pulLocParams[1] = (uint32_t)uiLocSetBits;
                    pulLocParams[2] = (uint32_t)uiLocClearBits;
                    (void)ML_SetOnTargetResetAction(ptyMLHandle, RESACT_MEMSET, pulLocParams);
                }
            }

            return ASH_TRUE;
        }
        // 
        // End of properties specific for target with reset detection
        // -------------------------------------------------------------------------------

        // -------------------------------------------------------------------------------
        // Following properties are specific for probe (Opella-XD)
        // 
        // "jtag_optimise" property
        if (!strcmp(property, "jtag_optimise"))
        {
            uint32_t uiValue = 0xFFFFFFFF;
            if ((sscanf(value, "%d", &uiValue) != 1) || ((uiValue != 0) && (uiValue != 1)))
                return ASH_TRUE;
            (void)ML_SetMemoryAccessOptimization(ptyMLHandle, (uiValue) ? 1 : 0);
            return ASH_TRUE;
        }

        // "auto_address" property
        if (!strcmp(property, "auto_address"))
        {
            uint32_t uiValue = 0xFFFFFFFF;

            if ((sscanf(value, "%d", &uiValue) != 1) || ((uiValue != 0) && (uiValue != 1)))
                return ASH_TRUE;
            (void)ML_SetAddressAutoincrement(ptyMLHandle, (uiValue) ? 1 : 0);
            return ASH_TRUE;
        }

        // "jtag_frequency" property
		//TODO: Find a method to ensure that jtag_freq and cjtag init are not done if they are already done (by another instance of GDB server)
        if (!strcmp(property, "jtag_frequency"))
        {
	        int32_t iRes;
		    double dNewFrequency = 0.0;

			// check if requesting adaptive clock
			if (!ML_ConvertFrequencySelection(value, &dNewFrequency))
			{
				ERROR_PRINT(p, MSG_ML_INVALID_FREQUENCY);
				return ASH_TRUE;
			}
			else
				iRes = ML_SetJtagFrequency(ptyMLHandle, dNewFrequency);         // set new frequency value

			if (iRes != ERR_ML_ARC_NO_ERROR)
				ERROR_PRINT(p, ML_GetErrorMessage(iRes));

			// Initialize CJTAG - Fix done for CJTAG-fixed frequency issue
			// The issue was, TAP7 controller is always initialized at 1 MHz and
			// the other operations is done at the user selected frequency.
			// Now the TAP7 controller is initiallized using the user selected frequency.
			// It was already implemented by Jeenus in Ultra-XD, copied here
			iRes = ML_IntializeCJTAG(ptyMLHandle);
			if (iRes != ERR_ML_ARC_NO_ERROR)
			{
				ERROR_PRINT(p, ML_GetErrorMessage(iRes));
			}

            return ASH_TRUE;
        }

        // "blast_frequency" property
        if (!strcmp(property, "blast_frequency"))
        {
            int32_t iRes;
            double dNewFrequency = 0.0;
            if (!ML_ConvertFrequencySelection(value, &dNewFrequency))
            {
                ERROR_PRINT(p, MSG_ML_INVALID_FREQUENCY);
                return ASH_TRUE;
            }
            else
                iRes = ML_SetBlastFrequency(ptyMLHandle, dNewFrequency);
            if (iRes != ERR_ML_ARC_NO_ERROR)
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            return ASH_TRUE;
        }

        // "rtck_timeout" property
        if (!strcmp(property, "rtck_timeout"))
        {
            uint32_t uiValue = 0;
            if (sscanf(value, "%d", &uiValue) == 1)
                (void)ML_SetAdaptiveClockTimeout(ptyMLHandle, (uint32_t)uiValue);
            return ASH_TRUE;
        }

        // "rtck_enable" property
        if (!strcmp(property, "rtck_enable"))
        {
            uint32_t uiValue = 0;
            if (sscanf(value, "%d", &uiValue) != 1)
                return ASH_TRUE;
            (void)ML_ToggleAdaptiveJtagClock(ptyMLHandle, (uiValue) ? 1 : 0);
            return ASH_TRUE;
        }

        // "test_scanchain" property
        if (!strcmp(property, "test_scanchain"))
        {
            // run scanchain loopback test and print result (useful for diagnostics)
            int32_t iRes = ML_TestScanchain(ptyMLHandle, 1);
            //		 MESSAGE_PRINT(p, "TAP Reset Done.\n");
            switch (iRes)
            {
            case ERR_ML_ARC_NO_ERROR:
                MESSAGE_PRINT(p, "JTAG scanchain loopback test passed.\n");
                break;
            case ERR_ML_ARC_TEST_SCANCHAIN_FAILED:
                MESSAGE_PRINT(p, "JTAG scanchain loopback test failed.\n");
                break;
            default:
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
                MESSAGE_PRINT(p, "JTAG scanchain loopback test failed.\n");
                break;
            }
            return ASH_TRUE;
        }
        // 
        // End of properties specific for probe (Opella-XD)
        // -------------------------------------------------------------------------------

    }
    return ASH_FALSE;
}

/****************************************************************************
Function: EnableArcApClk
Engineer: Andre Schmiel
Input: TyArcInstance
Output: Error
Description: Enable clk for actionpoints.
as some costumers use this option.
Date           Initials    Description
10-Aug-2010    AS          Initial
****************************************************************************/
int32_t EnableArcApClk(struct TyArcInstance *p)
{
    uint32_t ulValue;

    if (read_reg(p, reg_IDENTITY, &ulValue) == ASH_FALSE)
        return ASH_FALSE;

    ulValue &= 0xf0;

    if (   (ulValue == 0x20)   //ARC_600
           || (ulValue == 0x30))  //ARC_700
    {
        if (read_reg(p, reg_DEBUG, &ulValue) == ASH_FALSE)
            return ASH_FALSE;

        if (write_reg(p, reg_DEBUG, (ulValue | 0x1000000)) == ASH_FALSE) //set bit24
            return ASH_FALSE;
    }
    else if (   (ulValue == 0x40)   //ARC_EM
                || (ulValue == 0x50))  //ARC_HS
    {
        if (read_reg(p, reg_DEBUG, &ulValue) == ASH_FALSE)
            return ASH_FALSE;

        if (write_reg(p, reg_DEBUG, (ulValue | 0x100000)) == ASH_FALSE) //set bit20
            return ASH_FALSE;
    }

    return ASH_TRUE;
}

/****************************************************************************
Function: prepare_for_new_program
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
09-Sep-2009    RSB         TAP reset done based on check box selection.
06-Mar-2014    SV          Commented out code for clearing AP's.
****************************************************************************/
extern "C" int32_t prepare_for_new_program(struct TyArcInstance *p, int32_t will_download)
{
    uint32_t ulCpuVersion = 0;

    TRACE_PRINT(p, "prepare_for_new_program [%X]\n", will_download);

    //set current CPU
    SetCurrentCore(p);

    // shown startup message if starting debug session
    if (!p->bStartupInfoShown)
    {
        ShowDllStartupMessage(p);
        p->bStartupInfoShown = TRUE;
    }
    // force the ARC to halt
    if (write_reg(p, reg_DEBUG, FH_M_BIT) == ASH_FALSE)
        return ASH_FALSE;
    // read ARC version number from IDENTITY register
    if (read_reg(p, reg_IDENTITY, &ulCpuVersion) == ASH_FALSE)
        return ASH_FALSE;
    dbgprint("PrepareForNewProgram: IDENTITY :%08lX\n",ulCpuVersion);
    // mask version number (only lower byte to identify ARC core type) and store into local structure
    ulCpuVersion &= 0x000000FF;
    ML_SetCpuVersionNumber(ptyMLHandle, ulCpuVersion);
    // initialize AP settings
    p->iNumberOfAP = 0;
    p->ucAPVersion = 0;
    p->uiPMVersion = 0;
    p->bAPFullSet = 0;
    //    if ((ulCpuVersion > 0x2F) &&
    //        (ulCpuVersion < 0x40))
    {// ARC700
        uint32_t ulAP_build = 0;
        // read PMU_build register
        if (read_reg(p, reg_PMU_build, &ulAP_build) == ASH_FALSE)
            return ASH_FALSE;
        // get PMU version configuration
        p->uiPMVersion = (uint32_t)(ulAP_build & PMU_BUILD_VERSION_MASK);
    }
    // check AP support from AP_build register (version, number of AP, type), APs supported from ARC 0x7 and greater
    if (ulCpuVersion >= 0x7)
    {
        uint32_t ulAP_build = 0;

        // read AP_build register
        if (read_reg(p, reg_AP_build, &ulAP_build) == ASH_FALSE)
            return ASH_FALSE;
        dbgprint("PrepareForNewProgram: AP_BUILD :%08lX\n",ulAP_build);
        // get AP version and type
        p->ucAPVersion = (unsigned char)(ulAP_build & AP_BUILD_VERSION_MASK);
        // currently supported AP version is 0x1, 0x2, 0x3 and 0x4 (version 0x0 means AP not supported)
        // check number of AP's and type
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {  // ARCtangent-A4 and ARCtangent-A5 targets
            p->bAPFullSet = 0;
            switch ((ulAP_build & AP_BUILD_TYPE_MASK) >> AP_BUILD_TYPE_SHIFT)
            {
            case 0x0:
                p->iNumberOfAP = 2;
                break;
            case 0x1:
                p->iNumberOfAP = 4;
                break;
            case 0x2:
                p->iNumberOfAP = 8;
                break;
            default:
                break;
            }
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        { // ARC600, ARC700, ARC-EM and ARC-HS targets
            switch ((ulAP_build & AP_BUILD_TYPE_MASK) >> AP_BUILD_TYPE_SHIFT)
            {
            case 0x0:
                p->iNumberOfAP = 2;
                p->bAPFullSet = 1;
                break;
            case 0x1:
                p->iNumberOfAP = 4;
                p->bAPFullSet = 1;
                break;
            case 0x2:
                p->iNumberOfAP = 8;
                p->bAPFullSet = 1;
                break;
            case 0x4:
                p->iNumberOfAP = 2;
                p->bAPFullSet = 0;
                break;
            case 0x5:
                p->iNumberOfAP = 4;
                p->bAPFullSet = 0;
                break;
            case 0x6:
                p->iNumberOfAP = 8;
                p->bAPFullSet = 0;
                break;
            default:
                p->bAPFullSet = 0;
                break;
            }
        }
    }
    // remove any AP currently used (initialize them)
    p->ulAPUsed = 0;
    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
    {  // clear APs when using ACR register
        for (uint32_t uiIndex = 0; uiIndex < (uint32_t)p->iNumberOfAP; uiIndex++)
        {
            /*if (write_reg(p, reg_ACR, ((uiIndex << AN_V) | AP_DIS)) == ASH_FALSE)
            return ASH_FALSE;*/
            p->ptyAPTable[uiIndex].type = UNUSED;
        }
    }
    else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5)) //ARC600, ARC700, ARC-EM and ARC-HS
    {
        // clear APs when using AP_ACx registers
        /*uint32_t ulAP_AC = 0x0;           // disable AP via AP_ACx register
        switch (p->iNumberOfAP)
        {
        case 8:  // clear all AP from 4 to 7
        if ((write_reg(p, reg_AP_AC(7), ulAP_AC) == ASH_FALSE) || 
        (write_reg(p, reg_AP_AC(6), ulAP_AC) == ASH_FALSE) || 
        (write_reg(p, reg_AP_AC(5), ulAP_AC) == ASH_FALSE) ||
        (write_reg(p, reg_AP_AC(4), ulAP_AC) == ASH_FALSE))
        return ASH_FALSE;
        // no break
        case 4:  // clear all AP from 2 to 3
        if ((write_reg(p, reg_AP_AC(3), ulAP_AC) == ASH_FALSE) || 
        (write_reg(p, reg_AP_AC(2), ulAP_AC) == ASH_FALSE)) 
        return ASH_FALSE;
        // no break
        case 2:  // clear all AP from 0 to 1
        if ((write_reg(p, reg_AP_AC(1), ulAP_AC) == ASH_FALSE) || 
        (write_reg(p, reg_AP_AC(0), ulAP_AC) == ASH_FALSE)) 
        return ASH_FALSE;
        // no break
        default:
        break;
        }*/
        // update also internal structure
        for (int32_t iIndex = 0; iIndex < p->iNumberOfAP; iIndex++)
            p->ptyAPTable[iIndex].type = UNUSED;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: run
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t run(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "run\n");

    // Set current CPU number
    SetCurrentCore(p);

    if (ML_GetCpuVersionNumber(ptyMLHandle) >= 10)
    {     // later ARC version

        uint32_t ulStatusRegisterValue = 0;
        uint32_t ulDebugRegisterValue = 0;
        // read status32 register and clear halt bit
        if (read_reg(p, reg_AC_STATUS32, &ulStatusRegisterValue) == ASH_FALSE)
            return ASH_FALSE;
        ulStatusRegisterValue = ulStatusRegisterValue & AC_ZERO_HALT_BIT;
        // read debug register 
        if (read_reg(p, reg_DEBUG, &ulDebugRegisterValue) == ASH_FALSE)
            return ASH_FALSE;
        if (ulDebugRegisterValue & DOMAIN_HALT_BIT)
        {  // we must update both debug and status register
            if (write_reg(p, reg_AC_STATUS32, ulStatusRegisterValue) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_DEBUG, (ulDebugRegisterValue & ~DOMAIN_HALT_BIT)) == ASH_FALSE)
                return ASH_FALSE;
        }
        else
        {  // we can update only status register
            if (write_reg(p, reg_AC_STATUS32, ulStatusRegisterValue) == ASH_FALSE)
                return ASH_FALSE;
        }
    }
    else
    {     // early ARC version
        uint32_t ulStatusRegisterValue = 0;
        // to run CPU, read status register, clear halt bit and write status register
        if (read_reg(p, reg_STATUS, &ulStatusRegisterValue) == ASH_FALSE)
            return ASH_FALSE;
        ulStatusRegisterValue &= ZERO_HALT_BIT;
        if (write_reg(p, reg_STATUS, ulStatusRegisterValue) == ASH_FALSE)
            return ASH_FALSE;
    }
    // we should also indicate target status change on probe
    (void)ML_IndicateTargetStatusChange(ptyMLHandle);
    return ASH_TRUE;
}

/****************************************************************************
Function: step
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t step(struct TyArcInstance *p)
{
    unsigned char bActionpointEnabled, bCycleStep;
    uint32_t ulCpuVersion, lpDebug;

    TRACE_PRINT(p, "step\n");
    // Set current CPU number
    SetCurrentCore(p);

    // get ARC configuration
    ulCpuVersion = ML_GetCpuVersionNumber(ptyMLHandle);
    bCycleStep = ML_GetCycleStep(ptyMLHandle);
    // get current debug register value and check AP enable bit
    if (read_banked_reg (p, reg_bank_aux, 0x5, &lpDebug) == ASH_FALSE)
        return ASH_FALSE;
    bActionpointEnabled = ((lpDebug & ED_M_BIT) != 0);
    // If ARC verison is 10 or above and cycle stepping is needed, then simply cycle step the ARC. 
    // Debugger will deal with the extra intelligence, i.e extra step if Limm....
    if ((ulCpuVersion >= 10) && bCycleStep)
    {     // single step on ARC
        lpDebug |= SS_M_BIT;
        if (bActionpointEnabled)
            lpDebug |= ED_M_BIT;
        if (write_banked_reg (p, reg_bank_aux, 0x5, &lpDebug) == ASH_FALSE)
            return ASH_FALSE;
    }
    else if ((ulCpuVersion >= 7) && !bCycleStep)
    {
        // If ARC version greater or equal than 7, use the instruction step mechanism 
        // unless the user wants to use the old mechanism.
        lpDebug |= SS_M_BIT | IS_M_BIT;
        if (bActionpointEnabled)
            lpDebug |= ED_M_BIT;

        if (write_banked_reg (p, reg_bank_aux, 0x5, &lpDebug) == ASH_FALSE)
            return ASH_FALSE;
    }
    else
    {     // otherwise, we must decode instructions
        unsigned char fieldB, fieldC;
        uint32_t lpStart, lpEnd, lpCount, lpC, cPC, startPC, instruction;
        unsigned char bContinueLoop;
        unsigned char limmInstr = ASH_FALSE;
        uint32_t lpDebugBkp;

        // read status register to get current PC
        if (read_banked_reg (p,  reg_bank_aux,  0, &startPC) == ASH_FALSE)
            return ASH_FALSE;
        startPC &= PC_ADDRESS_MASK;
        // now read the instruction...
        if (read_memory(p, startPC*4, (char *)&instruction, 4,0) != 4)
            return ASH_FALSE;
        // switch on opcode which is tricky when for BE/LE
        // ARCompact ISA defines following order of bytes (in memory)
        // 16bit instructions in 1 word
        // LE   | ins1 b0 | ins1 b1 | ins2 b0 | ins2 b1 |
        // BE   | ins1 b1 | ins1 b0 | ins2 b1 | ins2 b0 |
        // 32bit instruction
        // LE   | ins1 b2 | ins1 b3 | ins1 b0 | ins1 b1 |
        // BE   | ins1 b3 | ins1 b2 | ins1 b1 | ins1 b0 |
        // for decoding purpose, we assume that we are running this DLL on LE
        // we do not have to bother with BE/LE issue since diskware takes care of it and
        // all words given by diskware has already been converted into LE

        // switch on opcode
        switch ( (instruction >> 27) & OPCODE )
        {     //cannot have LIMM data on these instructions
        case 4:      // Bcc instructions
        case 5:      // BLcc instructions
        case 6:      // LPcc instructions
            break;
        case 0:   
        case 3:
        case 7:
            fieldB = (char)( instruction >> 15) & LIMM_MASK;      // get the individual operand
            if ( fieldB == LIMM )
                limmInstr = ASH_TRUE;
            break;
        default:
            // get individual operands and check limm data
            fieldB = (char)( instruction >> 15) & LIMM_MASK; 
            fieldC = (char)( instruction >> 9) & LIMM_MASK;
            if ( (fieldB == LIMM) || (fieldC == LIMM))
                limmInstr = ASH_TRUE;
            break;
        }
        // get address of start of loop (value won't be used if not inside a loop)
        if (read_banked_reg (p, reg_bank_aux,  2, &lpStart) == ASH_FALSE)
            return ASH_FALSE;
        // get address of end of loop (value won't be used if not inside a loop)
        if (read_banked_reg (p, reg_bank_aux,  3, &lpEnd) == ASH_FALSE)
            return ASH_FALSE;
        // read the loop count
        if (read_banked_reg (p, reg_bank_core, 60, &lpCount) == ASH_FALSE)
            return ASH_FALSE;
        bContinueLoop = 1;

        lpDebugBkp = lpDebug;
        while (bContinueLoop)
        {     // single step ARC
            lpDebug = lpDebugBkp | SS_M_BIT;
            if (bActionpointEnabled)
                lpDebug |= ED_M_BIT;
            if (write_banked_reg (p, reg_bank_aux, 0x5, &lpDebug) == ASH_FALSE)
                return ASH_FALSE;
            // get current value of loop counter
            if (read_banked_reg (p, reg_bank_core, 60, &lpC) == ASH_FALSE)
                return ASH_FALSE;
            // get current pc value
            if (read_banked_reg (p,  reg_bank_aux,  0, &cPC) == ASH_FALSE)
                return ASH_FALSE;
            cPC = cPC & PC_ADDRESS_MASK;                 // interested in address   
            if (limmInstr)
            {
                if ((cPC != startPC) && (cPC != startPC + 1))
                    bContinueLoop = 0;                     // finish loop
            }
            else
            {
                if (cPC != startPC)
                    bContinueLoop = 0;                     // finish loop
            }
            if (cPC >= lpStart && cPC <= lpEnd)
            {
                if (lpCount != lpC)
                    bContinueLoop = 0;                     // finish loop
            }
        }
    }
    // we should also indicate target status change on probe
    (void)ML_IndicateTargetStatusChange(ptyMLHandle);
    return ASH_TRUE;
}

/****************************************************************************
Function: read_memory
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
11-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
****************************************************************************/
extern "C" int32_t read_memory(struct TyArcInstance *p, uint32_t adr, void *buf, uint32_t amount, int32_t context)
{
    int32_t iRes = ERR_ML_ARC_NO_ERROR;
    uint32_t ulBlockSize, ulWordsRead;
    uint32_t *pulData = (uint32_t *)buf;

    TRACE_PRINT(p, "read_memory [%lX] [%lX] [%X]\n", adr, amount, context);
    // Set current CPU number
    SetCurrentCore(p);

    if (!amount)
        return 0;               // just return if nothing to read
    // as spec of read_memory, we assumes amount comes alligned to 0 mod 4 boundary, so just convert to words
    amount /= 4;
    ulWordsRead = 0;
    while (ulWordsRead < amount)
    {
        ulBlockSize = amount - ulWordsRead;
        if (ulBlockSize > MAX_ARC_READ_MEMORY)
            ulBlockSize = MAX_ARC_READ_MEMORY;
        iRes = ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_MEMORY, adr, ulBlockSize, pulData);
        if (iRes != ERR_ML_ARC_NO_ERROR)
            break;
        adr += (ulBlockSize * 4);              // update address
        pulData += ulBlockSize;                // update pointer to data (note pulData is pointer to words)
        ulWordsRead += ulBlockSize;
    }
    // check result
    if (iRes == ERR_ML_ARC_DEBUG_PDOWN)
        return ASH_PDOWN;

    if (iRes == ERR_ML_ARC_DEBUG_POFF)
        return ASH_PDOWN;

    if (iRes != ERR_ML_ARC_NO_ERROR)
    {
        if (iRes == ERR_ML_ARC_TARGET_RESET_DETECTED)
        {
            (void)recover_from_target_reset(p);
            if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                MESSAGE_PRINT(p, ML_GetErrorMessage(iRes));
        }
        else
            ERROR_PRINT(p, ML_GetErrorMessage(iRes));
    }

    return(int32_t)(ulWordsRead * 4);
}

/****************************************************************************
Function: write_memory
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
11-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
****************************************************************************/
extern "C" int32_t write_memory(struct TyArcInstance *p, uint32_t adr, void *buf, uint32_t amount, int32_t context)
{
    uint32_t ulBlockSize, ulBytesWritten;
    unsigned char *pucData = (unsigned char *)buf;

    TRACE_PRINT(p, "write_memory [%lX] [%lX] [%X]\n", adr, amount, context);

    // Set current CPU number
    SetCurrentCore(p);

    if (!amount)
        return 0;               // just return if nothing to write

    ulBytesWritten = 0;
    // first write any non-alligned bytes
    while ((adr % 4) && (amount > 0))
    {
        if (local_write_byte(p, adr, *pucData++) != 1)
        {
            int32_t iRes = ptyMLHandle->iLastErrorCode;

            // check result
            if (iRes == ERR_ML_ARC_DEBUG_PDOWN)
                return ASH_PDOWN;

            if (iRes == ERR_ML_ARC_DEBUG_POFF)
                return ASH_PDOWN;

            if (iRes == ERR_ML_ARC_TARGET_RESET_DETECTED)
            {
                (void)recover_from_target_reset(p);
                if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                    MESSAGE_PRINT(p, ML_GetErrorMessage(iRes));
            }
            else
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            return((int32_t)ulBytesWritten);
        }
        adr++;
        amount--;
        ulBytesWritten++;
    }
    // now write whole words
    while (amount >= 4)
    {
        ulBlockSize = (amount / 4);
        if (ulBlockSize > MAX_ARC_WRITE_MEMORY)
            ulBlockSize = MAX_ARC_WRITE_MEMORY;
        if (ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_MEMORY, adr, ulBlockSize, (uint32_t *)pucData) != ERR_ML_ARC_NO_ERROR)
        {
            int32_t iRes = ptyMLHandle->iLastErrorCode;

            // check result
            if (iRes == ERR_ML_ARC_DEBUG_PDOWN)
                return ASH_PDOWN;

            if (iRes == ERR_ML_ARC_DEBUG_POFF)
                return ASH_PDOWN;

            if (iRes == ERR_ML_ARC_TARGET_RESET_DETECTED)
            {
                (void)recover_from_target_reset(p);
                if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                    MESSAGE_PRINT(p, ML_GetErrorMessage(iRes));
            }
            else
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            return((int32_t)ulBytesWritten);
        }
        adr += (ulBlockSize * 4);
        amount -= (ulBlockSize * 4);
        ulBytesWritten += (ulBlockSize * 4);
        pucData += (ulBlockSize * 4);
    }

    // finally, we may need to write rest of bytes
    while (amount)
    {
        if (local_write_byte(p, adr, *pucData++) != 1)
        {
            int32_t iRes = ptyMLHandle->iLastErrorCode;

            // check result
            if (iRes == ERR_ML_ARC_DEBUG_PDOWN)
                return ASH_PDOWN;

            if (iRes == ERR_ML_ARC_DEBUG_POFF)
                return ASH_PDOWN;

            if (iRes == ERR_ML_ARC_TARGET_RESET_DETECTED)
            {
                (void)recover_from_target_reset(p);
                if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                    MESSAGE_PRINT(p, ML_GetErrorMessage(iRes));
            }
            else
                ERROR_PRINT(p, ML_GetErrorMessage(iRes));
            return((int32_t)ulBytesWritten);
        }
        adr++;
        amount--;
        ulBytesWritten++;
    }
    return((int32_t)ulBytesWritten);
}

/****************************************************************************
Function: read_reg
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
11-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
****************************************************************************/
extern "C" int32_t read_reg(struct TyArcInstance *p, int32_t r, uint32_t *value)
{
    int32_t iRet = ERR_ML_ARC_NO_ERROR;
    // Set current CPU number
    SetCurrentCore(p);

    TRACE_PRINT(p, "uiCoreNumber: %08x\n", p->uiCoreNumber);

    // read register from ARC (core or aux)
    if (((uint32_t)r) >= AUX_BASE)
    {
        r -= AUX_BASE;                   // get offset of AUX register
        TRACE_PRINT(p, "read_reg AUX[%X]\n", r);
        TRACE_PRINT(p, "p->uiPMVersion:%08X\n",p->uiPMVersion);
        iRet = ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_AUX, (uint32_t)r, 1, value); 
    }
    else
    {     // core register
        TRACE_PRINT(p, "read_reg [%X]\n", r);
        TRACE_PRINT(p, "p->uiPMVersion:%08X\n",p->uiPMVersion);
        iRet = ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_CORE, (uint32_t)r, 1, value); 
    }

    // if powerdown check if requested register is PM_STATUS which can be read
    if (iRet == ERR_ML_ARC_DEBUG_PDOWN)
        return ASH_PDOWN;

    if (iRet == ERR_ML_ARC_DEBUG_POFF)
        return ASH_POFF;

    if (iRet != ERR_ML_ARC_NO_ERROR)
    {
        if (iRet == ERR_ML_ARC_TARGET_RESET_DETECTED)
        {
            (void)recover_from_target_reset(p);
            if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                MESSAGE_PRINT(p, ML_GetErrorMessage(iRet));
        }
        else
            ERROR_PRINT(p, ML_GetErrorMessage(iRet));
        return ASH_FALSE;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: write_reg
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
11-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
****************************************************************************/
extern "C" int32_t write_reg(struct TyArcInstance *p, int32_t r, uint32_t value)
{
    int32_t iRet = ERR_ML_ARC_NO_ERROR;
    // Set current CPU number
    SetCurrentCore(p);

    // write register to ARC (core or aux)
    if (((uint32_t)r) >= AUX_BASE)
    {
        r -= AUX_BASE;                   // get offset of AUX register
        TRACE_PRINT(p, "write_reg AUX[%X] [%lX]\n", r, value);
        iRet = ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_AUX, (uint32_t)r, 1, &value); 
    }
    else
    {     // core register
        TRACE_PRINT(p, "write_reg [%X] [%lX]\n", r, value);
        iRet = ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_CORE, (uint32_t)r, 1, &value); 
    }
    // if powerdown check if requested register is PM_STATUS which can be writed
    if (iRet == ERR_ML_ARC_DEBUG_PDOWN)
        return ASH_PDOWN;

    if (iRet == ERR_ML_ARC_DEBUG_POFF)
        return ASH_POFF;

    // check if error happened
    if (iRet != ERR_ML_ARC_NO_ERROR)
    {
        if (iRet == ERR_ML_ARC_TARGET_RESET_DETECTED)
        {
            (void)recover_from_target_reset(p);
            if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                MESSAGE_PRINT(p, ML_GetErrorMessage(iRet));
        }
        else
            ERROR_PRINT(p, ML_GetErrorMessage(iRet));
        return ASH_FALSE;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: read_banked_reg
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
11-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
****************************************************************************/
extern "C" int32_t read_banked_reg(struct TyArcInstance *p, int32_t bank, int32_t r, uint32_t *value)
{
    int32_t iRet = ERR_ML_ARC_NO_ERROR;

    TRACE_PRINT(p, "read_banked_reg [%X] [%X]\n", bank, r);
    // Set current CPU number
    SetCurrentCore(p);
    switch (bank)
    {
    case reg_bank_core:
        iRet = ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_CORE, (uint32_t)r, 1, value);
        break;
    case reg_bank_aux :
        iRet = ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_AUX, (uint32_t)r, 1, value);
        break;
    case reg_bank_madi:
        iRet = ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_MADI, (uint32_t)r, 1, value);
        break;
    default:
        *value = 0xDEADDEAD;
        return ASH_FALSE;
    }
    // if powerdown check if requested register is PM_STATUS which can be read
    if (iRet == ERR_ML_ARC_DEBUG_PDOWN)
        return ASH_PDOWN;

    if (iRet == ERR_ML_ARC_DEBUG_POFF)
        return ASH_POFF;

    // check if error happened
    if (iRet != ERR_ML_ARC_NO_ERROR)
    {
        if (iRet == ERR_ML_ARC_TARGET_RESET_DETECTED)
        {
            (void)recover_from_target_reset(p);
            if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                MESSAGE_PRINT(p, ML_GetErrorMessage(iRet));
        }
        else
            ERROR_PRINT(p, ML_GetErrorMessage(iRet));
        return ASH_FALSE;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: write_banked_reg
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
11-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
20-Mar-2008    NCH         Add PMU support
****************************************************************************/
extern "C" int32_t write_banked_reg(struct TyArcInstance *p, int32_t bank, int32_t r, uint32_t *value)
{
    int32_t iRet = ERR_ML_ARC_NO_ERROR;

    TRACE_PRINT(p, "write_banked_reg [%X] [%X]\n", bank, r);
    // Set current CPU number
    SetCurrentCore(p);
    switch (bank)
    {
    case reg_bank_core:
        iRet = ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_CORE, (uint32_t)r, 1, value);
        break;
    case reg_bank_aux :
        iRet = ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_AUX, (uint32_t)r, 1, value);
        break;
    case reg_bank_madi:
        iRet = ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_MADI, (uint32_t)r, 1, value);
        break;
    default:
        *value = 0xDEADDEAD;
        return ASH_FALSE;
    }
    // if powerdown check if requested register is PM_STATUS which can be writed
    if (iRet == ERR_ML_ARC_DEBUG_PDOWN)
        return ASH_PDOWN;

    if (iRet == ERR_ML_ARC_DEBUG_POFF)
        return ASH_POFF;

    // check if error happened
    if (iRet != ERR_ML_ARC_NO_ERROR)
    {
        if (iRet == ERR_ML_ARC_TARGET_RESET_DETECTED)
        {
            (void)recover_from_target_reset(p);
            if (ptyMLHandle->bTargetResetDetectRecursive == 0)
                MESSAGE_PRINT(p, ML_GetErrorMessage(iRet));
        }
        else
            ERROR_PRINT(p, ML_GetErrorMessage(iRet));
        return ASH_FALSE;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: set_reg_watchpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
07-Dec-2007    VH          Added support for AP on ARC600 and ARC700
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t set_reg_watchpoint(struct TyArcInstance *p, int32_t r, int32_t length)
{
    int32_t iAvailableAP = 0;
    uint32_t ulAPreg = (uint32_t)r;

    TRACE_PRINT(p, "set_reg_watchpoint [%X] [%X]\n", r, length);
    // Set current CPU number
    SetCurrentCore(p);
    if ((p->iNumberOfAP == 0) || (r < 0))
        return ASH_FALSE;
    // core registers are not supported for some versions of AP
    if (((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5)) && (r < AUX_BASE)) //ARC600, ARC700, ARC-EM and ARC-HS
        return ASH_FALSE;
    // try to get a free actionpoint number, update AP table
    if (get_free_actionpoint(p, &iAvailableAP) == ASH_FALSE)
        return ASH_FALSE;
    p->ptyAPTable[iAvailableAP].type = WATCH_REG;
    p->ptyAPTable[iAvailableAP].data.reg = (uint32_t)r;
    p->ptyAPTable[iAvailableAP].paired_ap = -1;
    // write AP settings to HW registers
    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
    {
        uint32_t ulAPaction = (((uint32_t)iAvailableAP) << AN_V) | CORE_REG_MODE;
        // check if register is core or aux
        if (ulAPreg >= AUX_BASE)
        {
            ulAPreg -= AUX_BASE;
            ulAPaction = (((uint32_t)iAvailableAP) << AN_V) | AUX_REG_MODE;
        }
        // write to AP registers
        if (write_reg(p, reg_ADCR, ulAPreg) == ASH_FALSE)
            return ASH_FALSE;
        if (write_reg(p, reg_ACR, ulAPaction) == ASH_FALSE)
            return ASH_FALSE;
    }
    else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
    { // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
        if (EnableArcApClk(p) == ASH_FALSE)
            return ASH_FALSE;

        ulAPreg -= AUX_BASE;                                                          // it must be aux register
        if (write_reg(p, reg_AP_AMV(iAvailableAP), ulAPreg) == ASH_FALSE)             // address of register
            return ASH_FALSE;
        if (write_reg(p, reg_AP_AMM(iAvailableAP), 0x0) == ASH_FALSE)                 // mask should be 0
            return ASH_FALSE;
        if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_AUXWP_VALUE) == ASH_FALSE)    // trigger action on write transaction to aux register
            return ASH_FALSE;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: remove_reg_watchpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t remove_reg_watchpoint(struct TyArcInstance *p, int32_t r, int32_t length)
{
    TRACE_PRINT(p, "remove_reg_watchpoint [%X] [%X]\n", r, length);
    // Set current CPU number
    SetCurrentCore(p);
    if (p->iNumberOfAP > 0)
    {
        // determine which actionpoint has been selected for this watchpoint
        for (int32_t iIndex = 0; iIndex < p->iNumberOfAP; iIndex++)
        {
            if ((p->ptyAPTable[iIndex].type == WATCH_REG) && (p->ptyAPTable[iIndex].data.reg == (uint32_t)r))
                return remove_actionpoint(p, iIndex);
        }
    }
    return ASH_FALSE;                   // no actionpoints matching this watchreg
}

/****************************************************************************
Function: set_mem_watchpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
07-Dec-2007    VH          Added support for ARC600 and ARC700 AP (including range checking)
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t set_mem_watchpoint(struct TyArcInstance *p, uint32_t addr, int32_t length)
{
    int32_t iAvailableAP, iAvailableAP2;
    uint32_t ulAPvalue;

    TRACE_PRINT(p, "set_mem_watchpoint [%lX] [%X]\n", addr, length);
    // Set current CPU number
    SetCurrentCore(p);
    // currently, supporting only 1 or 2 words (could be extended in the future)
    // address must be alligned to word and length 4 or 8 bytes
    if ((addr % 4) || (length % 4) || (length > 8))
        return ASH_FALSE;
    if (p->iNumberOfAP == 0)
        return ASH_FALSE;                                              // AP not supported

    if (length == 4)
    {     // set watchpoint to single word
        if (get_free_actionpoint(p, &iAvailableAP) == ASH_FALSE)
            return ASH_FALSE;
        // update AP table
        p->ptyAPTable[iAvailableAP].type = WATCH_MEM;
        p->ptyAPTable[iAvailableAP].data.address = addr;
        p->ptyAPTable[iAvailableAP].paired_ap = -1;                    // no paired AP
        // set memory address to watch and AP mode
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {
            ulAPvalue = (((uint32_t)iAvailableAP) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
            if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_ACR, ulAPvalue) == ASH_FALSE)
                return ASH_FALSE;
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        {     // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
            if (EnableArcApClk(p) == ASH_FALSE)
                return ASH_FALSE;

            if (write_reg(p, reg_AP_AMV(iAvailableAP), addr) == ASH_FALSE)                // address in memory
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AMM(iAvailableAP), 0x0) == ASH_FALSE)                 // mask should be 0
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMWP_VALUE) == ASH_FALSE)    // trigger action on write transaction to memory (LD instruction)
                return ASH_FALSE;
        }
    }
    else
    {     // set two AP as paired watchpoint
        if (get_free_actionpoint(p, &iAvailableAP) == ASH_FALSE)
            return ASH_FALSE;
        if (get_free_actionpoint(p, &iAvailableAP2) == ASH_FALSE)
        {     // remove first one already allocated
            (void)remove_actionpoint(p, iAvailableAP2);
            return ASH_FALSE;
        }
        // program first AP
        p->ptyAPTable[iAvailableAP].type = WATCH_MEM;
        p->ptyAPTable[iAvailableAP].data.address = addr;
        p->ptyAPTable[iAvailableAP].paired_ap = iAvailableAP2;
        // write AP register
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {
            ulAPvalue = (((uint32_t)iAvailableAP) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
            if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_ACR, ulAPvalue) == ASH_FALSE)
                return ASH_FALSE;
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        {     // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
            if (EnableArcApClk(p) == ASH_FALSE)
                return ASH_FALSE;

            if (write_reg(p, reg_AP_AMV(iAvailableAP), addr) == ASH_FALSE)                // address in memory
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AMM(iAvailableAP), 0x0) == ASH_FALSE)                 // mask should be 0
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMWP_VALUE) == ASH_FALSE)    // trigger action on write transaction to memory (LD instruction)
                return ASH_FALSE;
        }
        // next address and AP
        addr += 4;
        ulAPvalue = (((uint32_t)iAvailableAP2) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
        // program second AP
        p->ptyAPTable[iAvailableAP2].type = WATCH_MEM;
        p->ptyAPTable[iAvailableAP2].data.address = addr;
        p->ptyAPTable[iAvailableAP2].paired_ap = iAvailableAP;
        // write AP register
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {
            ulAPvalue = (((uint32_t)iAvailableAP2) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
            if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_ACR, ulAPvalue) == ASH_FALSE)
                return ASH_FALSE;
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        {     // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
            if (EnableArcApClk(p) == ASH_FALSE)
                return ASH_FALSE;

            if (write_reg(p, reg_AP_AMV(iAvailableAP2), addr) == ASH_FALSE)               // address in memory
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AMM(iAvailableAP2), 0x0) == ASH_FALSE)                // mask should be 0
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AC(iAvailableAP2), AP_AC_MEMWP_VALUE) == ASH_FALSE)   // trigger action on write transaction to memory (LD instruction)
                return ASH_FALSE;
        }
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: remove_mem_watchpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t remove_mem_watchpoint(struct TyArcInstance *p, uint32_t addr, int32_t length)
{
    TRACE_PRINT(p, "remove_mem_watchpoint [%lX] [%X]\n", addr, length);
    // Set current CPU number
    SetCurrentCore(p);
    if (p->iNumberOfAP > 0)
    {     // determine which actionpoint has been programmed for this watchpoint
        for (int32_t iIndex = 0; iIndex < p->iNumberOfAP; iIndex++)
        {
            if ((p->ptyAPTable[iIndex].type == WATCH_MEM) && (p->ptyAPTable[iIndex].data.address == addr))
            {     // find out if this wachmem used two actionpoints
                if ((p->ptyAPTable[iIndex].paired_ap != -1) && (length > 4))
                {     // we also remove associated actionpoint if there is one
                    if (remove_actionpoint(p, p->ptyAPTable[iIndex].paired_ap) == ASH_FALSE)
                        return ASH_FALSE;
                }
                if (remove_actionpoint(p, iIndex) == ASH_FALSE)
                    return ASH_FALSE;
                return ASH_TRUE;
            }
        }
    }
    return ASH_FALSE;
}

/****************************************************************************
Function: stopped_at_watchpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t stopped_at_watchpoint(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "stopped_at_watchpoint\n");
    // Set current CPU number
    SetCurrentCore(p);
    // we need to recognize if AP caused halt
    if (p->iNumberOfAP > 0)
    {
        uint32_t ulDebugValue = 0;
        // check AH bit from debug register
        if (read_reg(p, reg_DEBUG, &ulDebugValue) == ASH_FALSE)
            return ASH_FALSE;
        if (ulDebugValue & AP_DEBUG_AH_BIT)
        {  // AP caused processor halt
            uint32_t ulAPMask = p->ulAPUsed;
            uint32_t ulAPCause = (ulDebugValue & AP_DEBUG_ASR_MASK) >> AP_DEBUG_ASR_SHIFT;       // get ASR bit field from debug (AP caused halt)
            // go through all APs that caused halt, are used as BKT and their address matches expected value
            for (uint32_t ulIndex=0; ulIndex < (uint32_t)p->iNumberOfAP; ulIndex++)
            {
                if ((ulAPMask & 0x00000001) && (ulAPCause & 0x00000001) &&                                         // AP is used and caused current halt
                    ((p->ptyAPTable[ulIndex].type == WATCH_REG) || (p->ptyAPTable[ulIndex].type == WATCH_MEM)))    // and AP is configured as watchpoint
                    return ASH_TRUE;
                // next AP
                ulAPMask >>= 1;
                ulAPCause >>= 1;
            }
        }
    }
    // no AP caused current halt
    return ASH_FALSE;
}

/****************************************************************************
Function: stopped_at_exception
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t stopped_at_exception(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "stopped_at_exception\n");
    return ASH_FALSE;
}

/****************************************************************************
Function: set_breakpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
07-Dec-2007    VH          Modified for APs in ARC600 and ARC700
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t set_breakpoint(struct TyArcInstance *p, unsigned addr, void *cookie)
{
    int32_t iAvailableAP;

    cookie = cookie;
    TRACE_PRINT(p, "set_breakpoint [%X]\n", addr);

    // Set current CPU number
    SetCurrentCore(p);

    // check if this ARC has got AP
    if (p->iNumberOfAP == 0)
        return ASH_FALSE;
    // if we've still got unused AP, then retrieve its number and mark it as used
    if (get_free_actionpoint(p, &iAvailableAP) == ASH_FALSE)
        return ASH_FALSE;
    // let set available AP as breakpoint (halt when PC matches given value)
    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
    {
        uint32_t ulBkp = (((uint32_t)iAvailableAP) << AN_V) | BKP_MODE;
        // write the data value for comparison
        if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
            return ASH_FALSE;
        // set the actionpoint mode and program AP
        if (write_reg(p, reg_ACR, ulBkp) == ASH_FALSE)
            return ASH_FALSE;
    }
    else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
    {  // ARC600, ARC700, ARC-EM and ARC-HS
        if (EnableArcApClk(p) == ASH_FALSE)
            return ASH_FALSE;

        if (write_reg(p, reg_AP_AMV(iAvailableAP), addr) == ASH_FALSE)             // address of breakpoint
            return ASH_FALSE;
        if (write_reg(p, reg_AP_AMM(iAvailableAP), 0x0) == ASH_FALSE)              // mask should be 0
            return ASH_FALSE;
        if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_BRK_VALUE) == ASH_FALSE)   // trigger action (halt when PC matches address for read transaction)
            return ASH_FALSE;
    }
    // update AP table
    p->ptyAPTable[iAvailableAP].type = BKP;
    p->ptyAPTable[iAvailableAP].data.address = addr;
    return ASH_TRUE;
}

/****************************************************************************
Function: remove_breakpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t remove_breakpoint(struct TyArcInstance *p, unsigned addr, void *cookie)
{
    cookie = cookie;
    TRACE_PRINT(p, "remove_breakpoint [%X]\n", addr);
    // Set current CPU number
    SetCurrentCore(p);

    // check ARC version and and if this ARC's got AP
    if (p->iNumberOfAP > 0)
    {
        int32_t iAPNumber = 0;
        while (iAPNumber < p->iNumberOfAP)    // for each ap
        {
            if ((p->ptyAPTable[iAPNumber].type == BKP) && (p->ptyAPTable[iAPNumber].data.address == addr))
            {
                return remove_actionpoint(p, iAPNumber);
            }
            iAPNumber++;                        // next AP
        }
    }
    return ASH_FALSE;
}

/****************************************************************************
Function: retrieve_breakpoint_code
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t retrieve_breakpoint_code(struct TyArcInstance *p, unsigned address, char *dest, unsigned len, void *cookie_buf)
{
    dest = dest;      cookie_buf = cookie_buf;
    TRACE_PRINT(p, "retrieve_breakpoint_code [%X] [%X]\n", address, len);
    return ASH_TRUE;
}

/****************************************************************************
Function: breakpoint_cookie_len
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t breakpoint_cookie_len(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "breakpoint_cookie_len\n");
    return 0;
}

/****************************************************************************
Function: at_breakpoint
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
07-Dec-2007    VH          Modified for APs in ARC600 and ARC700
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t at_breakpoint(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "at_breakpoint\n");
    // Set current CPU number
    SetCurrentCore(p);

    // we need to recognize if AP caused halt
    if (p->iNumberOfAP > 0)
    {
        uint32_t ulDebugValue = 0;
        // check AH bit from debug register
        if (read_reg(p, reg_DEBUG, &ulDebugValue) == ASH_FALSE)
            return ASH_FALSE;
        if (ulDebugValue & AP_DEBUG_AH_BIT)
        {  // AP caused processor halt
            uint32_t ulAPMask = p->ulAPUsed;
            uint32_t ulAPCause = (ulDebugValue & AP_DEBUG_ASR_MASK) >> AP_DEBUG_ASR_SHIFT;       // get ASR bit field from debug (AP caused halt)
            // go through all APs that caused halt, are used as BKT and their address matches expected value
            for (uint32_t ulIndex=0; ulIndex < (uint32_t)p->iNumberOfAP; ulIndex++)
            {
                if ((ulAPMask & 0x00000001) && (ulAPCause & 0x00000001) &&                             // AP is used and caused current halt
                    (p->ptyAPTable[ulIndex].type == BKP))                                              // and AP is configured as breakpoint
                {
                    // ok, let check address
                    uint32_t ulHaltAddress = 0;
                    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
                    {
                        if (read_reg(p, reg_APCR, &ulHaltAddress) == ASH_FALSE)
                            return ASH_FALSE;
                    }
                    else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
                    { // ARC600, ARC700, ARC-EM and ARC-HS
                        if (read_reg(p, reg_AP_AMV(ulIndex), &ulHaltAddress) == ASH_FALSE)
                            return ASH_FALSE;
                    }
                    // check address
                    if (p->ptyAPTable[ulIndex].data.address == ulHaltAddress)
                        return ASH_TRUE;
                }
                // next AP
                ulAPMask >>= 1;
                ulAPCause >>= 1;
            }
        }
    }
    // no AP caused current halt
    return ASH_FALSE;
}

/****************************************************************************
Function: memory_size
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" unsigned memory_size(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "memory_size\n");
    return p->ulMemorySize;
}

/****************************************************************************
Function: set_memory_size
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t set_memory_size(struct TyArcInstance *p, unsigned S)
{
    TRACE_PRINT(p, "set_memory_size [%lX]\n", S);
    p->ulMemorySize = (uint32_t)S;
    return ASH_TRUE;
}

/****************************************************************************
Function: define_displays
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t define_displays(struct TyArcInstance *p, struct Register_display *rd)
{
    rd = rd;
    TRACE_PRINT(p, "define_displays\n");
    return ASH_FALSE;
}

/****************************************************************************
Function: fill_memory
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
****************************************************************************/
extern "C" int32_t fill_memory(struct TyArcInstance *p, uint32_t adr, void *buf, uint32_t amount, uint32_t repeat_count, int32_t context)
{
    int32_t iWritten = 0;
    unsigned char pucFillMemoryBuffer[0x10000];

    TRACE_PRINT(p, "fill_memory [%X] [%X] [%X] [%X]\n", adr, amount, repeat_count, context);
    // Set current CPU number
    SetCurrentCore(p);

    if ((amount * repeat_count) <= 0x10000)
    {  // copy the data repeatedly into buffer and send at the end... (optimized version)
        uint32_t uiIndex;
        for (uiIndex = 0; uiIndex < repeat_count; uiIndex++)
            memcpy(&pucFillMemoryBuffer[uiIndex * amount], buf, amount);
        iWritten += write_memory(p, adr, pucFillMemoryBuffer, amount * repeat_count, context);
    }
    else
    {  // write out data multiple times
        for (uint32_t ulIndex = 0; ulIndex < repeat_count; ulIndex++, adr += amount)
        {
            iWritten += write_memory(p, adr, buf, amount, context);
            if (iWritten != (int32_t)amount)
                break;
        }
    }
    return iWritten;
}

/****************************************************************************
Function: instruction_trace_count
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t instruction_trace_count(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "instruction_trace_count\n");
    return 0;
}

/****************************************************************************
Function: get_instruction_traces
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" void get_instruction_traces(struct TyArcInstance *p, uint32_t *traces)
{
    traces = traces;
    TRACE_PRINT(p, "get_instruction_traces\n");
}

/****************************************************************************
Function: receive_callback
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" void receive_callback(struct TyArcInstance *p, ARC_callback *callback)
{
    TRACE_PRINT(p, "receive_callback\n");
    p->pcbCallbackFunction = callback;
    SetupPrintMessage(p, pszTempMessage, 1); //Print if there is a message waiting
}

/****************************************************************************
Function: supports_feature
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t supports_feature(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "supports_feature\n");
    return(ARC_FEATURE_fill_memory   | ARC_FEATURE_instruction_trace | ARC_FEATURE_data_exchange | ARC_FEATURE_banked_reg);
}

/****************************************************************************
Function: data_exchange
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" uint32_t data_exchange(struct TyArcInstance *p, uint32_t what_to_do, uint32_t caller_endian, uint32_t send_amount, 
                                       void *send_buf, uint32_t max_receive_amount, void *receive_buf)
{
    what_to_do = what_to_do;      caller_endian = caller_endian;               send_amount = send_amount;
    send_buf = send_buf;          max_receive_amount = max_receive_amount;     receive_buf = receive_buf;
    TRACE_PRINT(p, "data_exchange\n");
    return DATA_XCHG_UNRECOGNIZED_OPCODE;
}

/****************************************************************************
Function: in_same_process_as_debugger
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" int32_t in_same_process_as_debugger(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "in_same_process_as_debugger\n");
    return ASH_TRUE;
}

/****************************************************************************
Function: max_data_exchange_transfer
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" uint32_t max_data_exchange_transfer(struct TyArcInstance *p)
{
    TRACE_PRINT(p, "max_data_exchange_transfer\n");
    return 0xFFFFFFFE;
}

/****************************************************************************
*** Local functions                                                       ***
****************************************************************************/
/****************************************************************************
Function: SetCurrentCore
Engineer: Nikolay Chokoev
Input: struct TyArcInstance *p - debugger structure
Output: none
Description: local function to change the current core
Date           Initials    Description
27-Feb-2008    NCH         Initial
****************************************************************************/
static void SetCurrentCore(struct TyArcInstance *p)
{
    (void)ML_SetCurrentCpu(ptyMLHandle, (uint32_t)p->uiCoreNumber);
}

/****************************************************************************
Function: local_write_byte
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - debugger structure
uint32_t ulAddress - address to write
unsigned char ucData - byte to write
Output: uint32_t - number of bytes successfully written (0 for error)
Description: local function to write single byte
Date           Initials    Description
11-Oct-2007    VH          Initial
****************************************************************************/
static unsigned short local_write_byte(struct TyArcInstance *p, uint32_t ulAddress, unsigned char ucData)
{
    uint32_t ulDataValue = 0;
    uint32_t ulNewData = ((uint32_t)ucData) & 0x000000FF;

    p=p;
    // first read whole word (alligned address)
    if (ML_ReadBlock(ptyMLHandle, ML_ARC_ACCESS_MEMORY, (ulAddress & 0xFFFFFFFC), 1, &ulDataValue) != ERR_ML_ARC_NO_ERROR)
        return 0;
    // modify particular byte in word (we are working in LE format)
    // if target is BE, diskware should take care of everything, on PC side, all values are already converted into LE
    switch (ulAddress & 0x00000003)
    {
    case 0x0:   // first byte
        ulDataValue &= 0xFFFFFF00;
        ulDataValue |= ulNewData;
        break;
    case 0x1:   // second byte
        ulDataValue &= 0xFFFF00FF;
        ulDataValue |= (ulNewData << 8);
        break;
    case 0x2:   // third byte
        ulDataValue &= 0xFF00FFFF;
        ulDataValue |= (ulNewData << 16);
        break;
    default:    // ok, it is last byte
        ulDataValue &= 0x00FFFFFF;
        ulDataValue |= (ulNewData << 24);
        break;
    }
    // now let rewrite whole word
    if (ML_WriteBlock(ptyMLHandle, ML_ARC_ACCESS_MEMORY, (ulAddress & 0xFFFFFFFC), 1, &ulDataValue) != ERR_ML_ARC_NO_ERROR)
        return 0;
    // ok, everything went fine
    return 1;
}

/****************************************************************************
Function: get_free_actionpoint
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
int32_t *ap_number - pointer to allocated AP
Output: int32_t - ASH_TRUE if success
Description: try to allocate available AP
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
static int32_t get_free_actionpoint(struct TyArcInstance *p, int32_t *piAPNumber)
{
    uint32_t ulLocAPUsed = p->ulAPUsed;

    TRACE_PRINT(p, "get_free_actionpoint\n");
    // go through bit mask and check for 0 bit
    for (int32_t iIndex=0; iIndex< p->iNumberOfAP; iIndex++)
    {
        if ((ulLocAPUsed & 0x00000001) == 0)
        {  // we have found free AP, set bit in global mask and return index
            p->ulAPUsed |= (0x00000001 << iIndex);
            *piAPNumber = iIndex;
            return ASH_TRUE;
        }
        else
            ulLocAPUsed >>= 1;                        // try next AP
    }
    return ASH_FALSE;
}

/****************************************************************************
Function: remove_actionpoint
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
int32_t ap_number - AP number
Output: int32_t - ASH_TRUE is success
Description: remove action point
Date           Initials    Description
10-Oct-2007    VH          Initial
07-Dec-2007    VH          Added support for ARC600 and ARC700 AP's
****************************************************************************/
static int32_t remove_actionpoint(struct TyArcInstance *p, int32_t iAPNumber)
{
    uint32_t ulLocMask = (0x00000001 << iAPNumber);

    TRACE_PRINT(p, "remove_actionpoint\n");
    p->ulAPUsed &= ~ulLocMask;                   // clear bit related to selected AP, update AP vector
    // update AP table
    p->ptyAPTable[iAPNumber].type = UNUSED;
    p->ptyAPTable[iAPNumber].paired_ap = -1;
    // disable AP via HW registers
    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
    {  // clear AP using ACR register
        uint32_t ulACR = (((uint32_t) iAPNumber) << AN_V) | AP_DIS;
        if (write_reg(p, reg_ACR, ulACR) == ASH_FALSE)
            return ASH_FALSE;
    }
    else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5)) // ARC600, ARC700, ARC-EM and ARC-HS
    {
        // clear APs when using AP_ACx registers
        if (EnableArcApClk(p) == ASH_FALSE)
            return ASH_FALSE;

        if (write_reg(p, reg_AP_AC(iAPNumber), 0x0) == ASH_FALSE)
            return ASH_FALSE;
    }
    return ASH_TRUE;
}

/****************************************************************************
Function: recover_from_target_reset
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
Output: int32_t - ASH_TRUE is success
Description: recover from target reset when detected
Date           Initials    Description
12-Nov-2007    VH          Initial
****************************************************************************/
static int32_t recover_from_target_reset(struct TyArcInstance *p)
{
    uint32_t uiCurrentAction;
    // check if makes sense to do any recovery (i.e. target does not support it, already doing that, no action to do, etc.)
    if ((p == NULL) || (ptyMLHandle == NULL) || (ptyMLHandle->tyArcTargetType != ARC_TGT_RSTDETECT) || 
        (ptyMLHandle->bTargetResetDetectRecursive) || (ptyMLHandle->tyOnTargetResetActions.uiNumberOfActions == 0))
        return ASH_TRUE;
    // this is important as we cannot recover from reset in recursive function (recovering uses register/memory access which 
    // uses recover function itself => recursion)
    // when this function called in recursion, it will not continue next code
    ptyMLHandle->bTargetResetDetectRecursive = 1;                                 
    // first wait 3 seconds for target to finish reset
    // now do action as predefined when reset happens
    for (uiCurrentAction=0; uiCurrentAction < ptyMLHandle->tyOnTargetResetActions.uiNumberOfActions; uiCurrentAction++)
    {
        switch (ptyMLHandle->tyOnTargetResetActions.ptyActions[uiCurrentAction])
        {
        case RESACT_DELAY:                                                            // wait for given number of ms
            DEBUGGER_SLEEP(p, ptyMLHandle->tyOnTargetResetActions.pulActionParam1[uiCurrentAction]);
            break;
        case RESACT_MEMSET:                                                           // set/clear given bits in memory address
            {
                uint32_t ulLocValue = 0;
                uint32_t ulLocMemAddress = ptyMLHandle->tyOnTargetResetActions.pulActionParam1[uiCurrentAction];
                (void)read_memory(p, ulLocMemAddress, &ulLocValue, 4, 0);                  // read current value from memory
                ulLocValue &= ~(ptyMLHandle->tyOnTargetResetActions.pulActionParam3[uiCurrentAction]);    // clear specified bits
                ulLocValue |= ptyMLHandle->tyOnTargetResetActions.pulActionParam2[uiCurrentAction];       // set specified bits
                (void)write_memory(p, ulLocMemAddress, &ulLocValue, 4, 0);
            }
            break;
        case RESACT_RESTORE_AP:                                                       // restore debug session context (actionpoints, etc.)
            restore_all_actionpoint(p);
            break;
        case RESACT_RTCK_ENABLE:                                                      // enable RTCK
            (void)ML_ToggleAdaptiveJtagClock(ptyMLHandle, 1);
            break;
        case RESACT_RTCK_DISABLE:                                                     // disable RTCK
            (void)ML_ToggleAdaptiveJtagClock(ptyMLHandle, 0);
            break;
        case RESACT_NONE:
        case RESACT_INIT:
        default:
            break;                                                                     // do nothing
        }
    }
    ptyMLHandle->bTargetResetDetectRecursive = 0;                                    // re-enable recursion
    return ASH_TRUE;
}

/****************************************************************************
Function: restore_all_actionpoint
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
Output: none
Description: restore all actionpoints in AP registers
Date           Initials    Description
07-Dec-2007    VH          Initial
****************************************************************************/
static void restore_all_actionpoint(struct TyArcInstance *p)
{
    int32_t iIndex;
    uint32_t ulAP = p->ulAPUsed;

    if (p == NULL)
        return;

    // restore all action points
    for (iIndex=0; iIndex < p->iNumberOfAP; iIndex++)
    {
        // check if AP is used
        if (ulAP & 0x00000001)
        {
            if (p->ptyAPTable[iIndex].type == WATCH_REG)
            {  // register access watchpoint
                if (p->ptyAPTable[iIndex].data.reg < AUX_BASE)
                {
                    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
                    {
                        (void)write_reg(p, reg_ADCR, p->ptyAPTable[iIndex].data.reg);
                        (void)write_reg(p, reg_ACR, (((uint32_t)iIndex) << AN_V) | CORE_REG_MODE);
                    }
                    // core register watchpoint not supported for other cores
                }
                else
                {
                    if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
                    {
                        (void)write_reg(p, reg_ADCR, p->ptyAPTable[iIndex].data.reg - AUX_BASE);
                        (void)write_reg(p, reg_ACR, (((uint32_t)iIndex) << AN_V) | AUX_REG_MODE);
                    }
                    else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
                    { // ARC600, ARC700, ARC-EM and ARC-HS
                        (void) EnableArcApClk(p);

                        (void)write_reg(p, reg_AP_AMV(iIndex), (uint32_t)(p->ptyAPTable[iIndex].data.reg - AUX_BASE));
                        (void)write_reg(p, reg_AP_AMM(iIndex), 0x0);
                        (void)write_reg(p, reg_AP_AC(iIndex), AP_AC_AUXWP_VALUE);
                    }
                }
            }
            else if (p->ptyAPTable[iIndex].type == WATCH_MEM)
            {  // memory access watchpoint
                if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
                {
                    (void)write_reg(p, reg_ADCR, p->ptyAPTable[iIndex].data.address);
                    (void)write_reg(p, reg_ACR, (((uint32_t)iIndex) << AN_V) | AC_EQ | AT_MWA | AP_ENA);
                }
                else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
                { // ARC600, ARC700, ARC-EM and ARC-HS
                    (void) EnableArcApClk(p);

                    (void)write_reg(p, reg_AP_AMV(iIndex), p->ptyAPTable[iIndex].data.address);
                    (void)write_reg(p, reg_AP_AMM(iIndex), 0x0);
                    (void)write_reg(p, reg_AP_AC(iIndex), AP_AC_MEMWP_VALUE);
                }
            }
            else if (p->ptyAPTable[iIndex].type == BKP)
            {  // breakpoint watchpoint
                if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
                {
                    (void)write_reg(p, reg_ADCR, p->ptyAPTable[iIndex].data.address);
                    (void)write_reg(p, reg_ACR, (((uint32_t)iIndex) << AN_V) | BKP_MODE);
                }
                else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
                { // ARC600, ARC700, ARC-EM and ARC-HS
                    (void) EnableArcApClk(p);

                    (void)write_reg(p, reg_AP_AMV(iIndex), p->ptyAPTable[iIndex].data.address);
                    (void)write_reg(p, reg_AP_AMM(iIndex), 0x0);
                    (void)write_reg(p, reg_AP_AC(iIndex), AP_AC_BRK_VALUE);
                }
            }
        }
        ulAP >>= 1;
    }
}

/****************************************************************************
Function: ConvertVersionDate
Engineer: Vitezslav Hola
Input: uint32_t ulValue - value with version or date
unsigned char bVersionFormat - TRUE to convert version and FALSE to convert date
char *pszBuffer - buffer for converted string
uint32_t uiSize - maximum number of bytes in buffer
Output: none
Description: remove action point
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
static void ConvertVersionDate(uint32_t ulValue, unsigned char bVersionFormat, char *pszBuffer, uint32_t uiSize)
{
    if ((pszBuffer == NULL) || (uiSize == 0))
        return;
    pszBuffer[0] = 0;             // empty string
    if (bVersionFormat)
    {  // converting version into string
        uint32_t ulVer1 = (ulValue & 0xFF000000) >> 24;
        uint32_t ulVer2 = (ulValue & 0x00FF0000) >> 16;
        uint32_t ulVer3 = (ulValue & 0x0000FF00) >> 8;
        uint32_t ulVer4 = (ulValue & 0x000000FF);
        // is it major release
        if (((ulVer4 >= 'A') && (ulVer4 <= 'Z')) || ((ulVer4 >= 'a') && (ulVer4 <= 'z')))
            (void)_snprintf(pszBuffer, uiSize, "v%x.%x.%x-%c", (uint32_t)ulVer1, (uint32_t)ulVer2, (uint32_t)ulVer3, (unsigned char)ulVer4);
        else
            (void)_snprintf(pszBuffer, uiSize, "v%x.%x.%x", (uint32_t)ulVer1, (uint32_t)ulVer2, (uint32_t)ulVer3);
    }
    else
    {  // converting date into string (i.e. 02-Nov-2007)
        char pszLocMonth[16];
        uint32_t ulDate, ulMonth, ulYear;
        ulDate   = (ulValue & 0xFF000000) >> 24;
        ulMonth  = (ulValue & 0x00FF0000) >> 16;
        ulYear   = (ulValue & 0x0000FFFF);
        switch (ulMonth)
        {
        case 0x01:  strcpy(pszLocMonth, "Jan");   break;
        case 0x02:  strcpy(pszLocMonth, "Feb");   break;
        case 0x03:  strcpy(pszLocMonth, "Mar");   break;
        case 0x04:  strcpy(pszLocMonth, "Apr");   break;
        case 0x05:  strcpy(pszLocMonth, "May");   break;
        case 0x06:  strcpy(pszLocMonth, "Jun");   break;
        case 0x07:  strcpy(pszLocMonth, "Jul");   break;
        case 0x08:  strcpy(pszLocMonth, "Aug");   break;
        case 0x09:  strcpy(pszLocMonth, "Sep");   break;
        case 0x10:  strcpy(pszLocMonth, "Oct");   break;
        case 0x11:  strcpy(pszLocMonth, "Nov");   break;
        case 0x12:
        default:    strcpy(pszLocMonth, "Dec");   break;
        }
        (void)_snprintf(pszBuffer, uiSize, "%02x-%s-%04x", (uint32_t)ulDate, pszLocMonth, (uint32_t)ulYear);
    }
    pszBuffer[uiSize - 1] = 0;                         // ensure string is always terminated correctly
}

/****************************************************************************
Function: TRACE_PRINT
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *strToFormat - string format + parameters (like printf)
Output: None
Description: The function is called when DLL wants to print trace
message to debugger window
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void TRACE_PRINT(struct TyArcInstance *p, const char * strToFormat, ...)
{
    if ((p != NULL) && (p->bTraceMessagesEnabled != FALSE) && (p->pcbCallbackFunction != NULL))
    {
        char *pszLocPtr;
        char buffer[0x120];
        va_list ArgList;

        strcpy(buffer, "OPELLA-XD ARC TRACE: ");
        pszLocPtr = &(buffer[strlen(buffer)]);
        va_start(ArgList, strToFormat);
        (void)vsprintf(pszLocPtr, strToFormat, ArgList); 
        va_end(ArgList);
        (void)p->pcbCallbackFunction->printf(buffer);
    }

#ifdef DEBUG
    char *pszLocPtr;
    char buffer[0x120];
    va_list ArgList;
    FILE * fp;

    strcpy(buffer, "OPELLA-XD ARC TRACE: ");
    pszLocPtr = &(buffer[strlen(buffer)]);
    va_start(ArgList, strToFormat);
    (void)vsprintf(pszLocPtr, strToFormat, ArgList); 
    va_end(ArgList);

    fp = fopen ("arc_debug.txt", "a");
    if (fp != NULL)
    {
        fprintf(fp, buffer);

        fclose(fp);
    }
#endif

}

/****************************************************************************
Function: MESSAGE_PRINT
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *strToFormat - string format + parameters (like printf)
Output: None
Description: The function is called when DLL wants to print normal message 
to debugger window
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void MESSAGE_PRINT(struct TyArcInstance *p, const char * strToFormat, ...)
{
    if ((p != NULL) && (p->pcbCallbackFunction != NULL))
    {
        char buffer[0x100];
        va_list ArgList;

        va_start(ArgList, strToFormat);
        (void)vsprintf(buffer, strToFormat, ArgList); 
        va_end(ArgList);
        (void)p->pcbCallbackFunction->printf(buffer);
    }
}

/****************************************************************************
Function: GENERAL_PRINTF
Engineer: Vitezslav Hola
Input: char *strToFormat - string format + parameters (like printf)
Output: None
Description: Function to print general messages like printf
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
void GENERAL_PRINTF(const char * strToFormat, ...)
{
    char buffer[0x100];
    va_list ArgList;

    va_start(ArgList, strToFormat);
    (void)vsprintf(buffer, strToFormat, ArgList); 
    va_end(ArgList);
    GENERAL_MESSAGE_PRINT(buffer);
}

/****************************************************************************
Function: GENERAL_MESSAGE_PRINT
Engineer: Vitezslav Hola
Input: const char *pszMessage - message to print
Output: None
Description: general message print 
Date           Initials    Description
31-Oct-2007    VH          Initial
****************************************************************************/
void GENERAL_MESSAGE_PRINT(const char *pszMessage)
{
    // call general print function
    if (pfGeneralPrintFunc == NULL){
        fprintf(stdout,"%s\n", pszMessage);
        fflush(stdout);
    } else {
        pfGeneralPrintFunc(pszMessage);
    }
}

/****************************************************************************
Function: ERROR_PRINT
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *strToFormat - string format + parameters (like printf)
Output: None
Description: The function is called when DLL wants to print error message
(either to GUI or debugger window)
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void ERROR_PRINT(struct TyArcInstance *p, const char * strToFormat, ...)
{
    if (p != NULL)
    {
        if ((p->pcbCallbackFunction != NULL))
        {
            char *pszLocPtr;
            char buffer[0x120];
            va_list ArgList;
            // printing to debugger window
            strcpy(buffer, "OPELLA-XD ARC ERROR: ");
            pszLocPtr = &(buffer[strlen(buffer)]);
            va_start(ArgList, strToFormat);
            (void)vsprintf(pszLocPtr, strToFormat, ArgList); 
            va_end(ArgList);
            (void)p->pcbCallbackFunction->printf(buffer);
        }
    }
}

/****************************************************************************
Function: ShowDllHelpMessage
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
Output: None
Description: Show DLL help messages using debugger window output
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void ShowDllHelpMessage(struct TyArcInstance *p)
{
    MESSAGE_PRINT(p, "     prop dll_help -- show this help\n");
    MESSAGE_PRINT(p, "     prop probe_info -- show information about Opella-XD\n");
    MESSAGE_PRINT(p, "     prop trace_messages=1 -- show interface debug messages\n");
    MESSAGE_PRINT(p, "          (=0 to turn off)\n");
    MESSAGE_PRINT(p, "     prop test_scanchain -- run JTAG scanchain loopback test\n");
    MESSAGE_PRINT(p, "     -off=invalidate_icache -- skip invalidating icache on memory writes\n");
    MESSAGE_PRINT(p, "     prop reset_duration=ms -- set duration of time when reset is asserted\n");
    MESSAGE_PRINT(p, "                            (ms is number of miliseconds)\n");
    MESSAGE_PRINT(p, "     prop reset_delay=ms -- set delay after deasserting reset before continuing\n");
    MESSAGE_PRINT(p, "                         (ms is number of miliseconds)\n");
    // now show help specific for different target types
    if (ptyMLHandle->tyArcTargetType == ARC_TGT_ARCANGEL)
    {  // show following help only if target is ARCangel
        MESSAGE_PRINT(p, "     prop blast_frequency=val -- set blasting clock frequency\n");
        MESSAGE_PRINT(p, "                             (val is frequency value, i.e 8MHz)\n");
        MESSAGE_PRINT(p, "     prop extcmd=num,val -- send extended command into ARCangel4 target\n");
        MESSAGE_PRINT(p, "                         (num is command number in hex format, i.e. 0x1)\n");
        MESSAGE_PRINT(p, "                         (val is command parameter in hex format, i.e. 0x0)\n");
        MESSAGE_PRINT(p, "     prop gclk0=source -- configure GCLK0 for ARCangel4 target\n");
        MESSAGE_PRINT(p, "     prop gclk1=source -- configure GCLK1 for ARCangel4 target\n");
        MESSAGE_PRINT(p, "     prop gclk2=source -- configure GCLK2 for ARCangel4 target\n");
        MESSAGE_PRINT(p, "     prop gclk3=source -- configure GCLK3 for ARCangel4 target\n");
        MESSAGE_PRINT(p, "     prop gclk=source  -- same as GCLK3, source can be:\n");
        MESSAGE_PRINT(p, "                       crystal - use directly crystal\n");
        MESSAGE_PRINT(p, "                       dips - use crystal divided by DIP switch\n");
        MESSAGE_PRINT(p, "                       highimp - not routed (tri-state)\n");
        MESSAGE_PRINT(p, "                       host - strobe from host interface\n");
        MESSAGE_PRINT(p, "                       value - PLL frequency in MHz (i.e. 60.0 for 60MHz)\n");
        MESSAGE_PRINT(p, "     prop robosdram=val -- set roboSDRAM clock in ARCangel4 target\n");
        MESSAGE_PRINT(p, "     prop robossram=val -- set roboSSRAM clock in ARCangel4 target\n");
        MESSAGE_PRINT(p, "     prop robossramexp=val -- set roboSSRAMEXP register in ARCangel4 target\n");
        MESSAGE_PRINT(p, "                           (val is register value in hex format)\n");
    }
    else if (ptyMLHandle->tyArcTargetType == ARC_TGT_RSTDETECT)
    {  // target is ARC with target reset detection
        MESSAGE_PRINT(p, "     prop rtck_enable=val -- use adaptive JTAG clock or fixed frequency\n");
        MESSAGE_PRINT(p, "                          (val is 1 for adaptive clock or 0 for fixed frequency)\n");
        MESSAGE_PRINT(p, "     prop rtck_timeout=val -- set timeout for adaptive JTAG clock\n");
        MESSAGE_PRINT(p, "                           (val is timeout value in miliseconds)\n");
        MESSAGE_PRINT(p, "     prop on_target_reset=action -- add next action on the list triggered\n");
        MESSAGE_PRINT(p, "                                    by target reset, action can be: \n");
        MESSAGE_PRINT(p, "                  init - initialize list of actions\n");
        MESSAGE_PRINT(p, "                  restore_ap - restore all breakpoints and watchpoints\n");
        MESSAGE_PRINT(p, "                  disable_rtck - disable adaptive JTAG clock\n");
        MESSAGE_PRINT(p, "                  enable_rtck - enable adaptive JTAG clock\n");
        MESSAGE_PRINT(p, "                  delay,<ms> - wait for given number of miliseconds\n");
        MESSAGE_PRINT(p, "                  <address>,<set>,<clr> - set and clear specified bits in word\n");
        MESSAGE_PRINT(p, "                                          at given memory location\n");
    }
    else if (ptyMLHandle->tyArcTargetType == ARC_TGT_CJTAG_TPA_R1)
    {  // target is ARC with cJTAG
        MESSAGE_PRINT(p, "     prop rtck_enable=0 -- use fixed frequency, do not set adaptive clock\n");
        MESSAGE_PRINT(p, "                          (val is 1 for adaptive clock or 0 for fixed frequency)\n");
    }
    else if (ptyMLHandle->tyArcTargetType == ARC_TGT_JTAG_TPA_R1)
    {  // target is normal ARC with TPA Revision 1
        MESSAGE_PRINT(p, "     prop rtck_enable=val -- use adaptive JTAG clock or fixed frequency\n");
        MESSAGE_PRINT(p, "                          (val is 1 for adaptive clock or 0 for fixed frequency)\n");
        MESSAGE_PRINT(p, "     prop rtck_timeout=val -- set timeout for adaptive JTAG clock\n");
        MESSAGE_PRINT(p, "                           (val is timeout value in miliseconds)\n");
    }
    else
    {  // target is normal ARC
        MESSAGE_PRINT(p, "     prop rtck_enable=val -- use adaptive JTAG clock or fixed frequency\n");
        MESSAGE_PRINT(p, "                          (val is 1 for adaptive clock or 0 for fixed frequency)\n");
        MESSAGE_PRINT(p, "     prop rtck_timeout=val -- set timeout for adaptive JTAG clock\n");
        MESSAGE_PRINT(p, "                           (val is timeout value in miliseconds)\n");
    }
}

/****************************************************************************
Function: ShowProbeInformation
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
unsigned char bToDebugger - TRUE to print into debugger instance, FALSE to general messages
Output: None
Description: show information about probe (serial number, diskware and firmware version, etc.)
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
void ShowProbeInformation(struct TyArcInstance *p, unsigned char bToDebugger)
{
    if ((p == NULL) || (ptyMLHandle == NULL))
        return;
    else
    {
        char pszLocVersion[20];
        char pszLocDate[20];
        // show information about serial number
        if (bToDebugger)
            MESSAGE_PRINT(p, "Opella-XD serial number : %s\n", ptyMLHandle->pszProbeInstanceNumber);
        else
            GENERAL_PRINTF("Opella-XD serial number : %s\n", ptyMLHandle->pszProbeInstanceNumber);
        // show information about firmware
        ConvertVersionDate(ptyMLHandle->tyProbeFirmwareInfo.ulDate, FALSE, pszLocDate, 20);
        ConvertVersionDate(ptyMLHandle->tyProbeFirmwareInfo.ulVersion, TRUE, pszLocVersion, 20);
        if (bToDebugger)
            MESSAGE_PRINT(p, "Opella-XD firmware info : %s, %s\n", pszLocVersion, pszLocDate);
        else
            GENERAL_PRINTF("Opella-XD firmware info : %s, %s\n", pszLocVersion, pszLocDate);
        // show information about diskware
        ConvertVersionDate(ptyMLHandle->tyProbeDiskwareInfo.ulDate, FALSE, pszLocDate, 20);
        ConvertVersionDate(ptyMLHandle->tyProbeDiskwareInfo.ulVersion, TRUE, pszLocVersion, 20);
        if (bToDebugger)
            MESSAGE_PRINT(p, "Opella-XD diskware info : %s, %s\n", pszLocVersion, pszLocDate);
        else
            GENERAL_PRINTF("Opella-XD diskware info : %s, %s\n", pszLocVersion, pszLocDate);
        // show information about FPGAware
        ConvertVersionDate(ptyMLHandle->tyProbeFPGAwareInfo.ulDate, FALSE, pszLocDate, 20);
        ConvertVersionDate(ptyMLHandle->tyProbeFPGAwareInfo.ulVersion, TRUE, pszLocVersion, 20);
        if (bToDebugger)
            MESSAGE_PRINT(p, "Opella-XD FPGAware info : %s, %s\n", pszLocVersion, pszLocDate);
        else
            GENERAL_PRINTF("Opella-XD FPGAware info : %s, %s\n", pszLocVersion, pszLocDate);
    }
}

/****************************************************************************
Function: ShowAAClockConfiguration
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
Output: None
Description: Show information about ARCangel clock configuration
Date           Initials    Description
19-Oct-2007    VH          Initial
****************************************************************************/
void ShowAAClockConfiguration(struct TyArcInstance *p)
{
    int32_t iIndex;
    TyArcAAGclk ptyAAGclk[4];
    double pdFrequency[4];
    int32_t iRes = ERR_ML_ARC_NO_ERROR;

    for (iIndex=0; iIndex < 4; iIndex++)
    {
        ptyAAGclk[iIndex] = AA_GCLK_HIGHIMP;     pdFrequency[iIndex] = 0.0;
        if (iRes == ERR_ML_ARC_NO_ERROR)
            iRes = ML_AAGetClockAllocation(ptyMLHandle, iIndex, &ptyAAGclk[iIndex], &pdFrequency[iIndex]);
    }

    MESSAGE_PRINT(p, "ARCangel GCLK configuration\n");

    if (iRes == ERR_ML_ARC_NO_ERROR)
    {
        for (iIndex=0; iIndex < 4; iIndex++)
        {
            switch (ptyAAGclk[iIndex])
            {
            case AA_GCLK_VCLK:
                MESSAGE_PRINT(p, "    gclk%d - PLL VCLK %.1fMHz\n", iIndex, pdFrequency[iIndex]);
                break;
            case AA_GCLK_MCLK:
                MESSAGE_PRINT(p, "    gclk%d - PLL MCLK %.1fMHz\n", iIndex, pdFrequency[iIndex]);
                break;
            case AA_GCLK_HOST:
                MESSAGE_PRINT(p, "    gclk%d - host strobe\n", iIndex);
                break;
            case AA_GCLK_DIPS:
                MESSAGE_PRINT(p, "    gclk%d - crystal + dips\n", iIndex);
                break;
            case AA_GCLK_CRYSTAL:
                MESSAGE_PRINT(p, "    gclk%d - crystal (direct)\n", iIndex);
                break;
            case AA_GCLK_HIGHIMP:
            default:
                MESSAGE_PRINT(p, "    gclk%d - highimp\n", iIndex);
                break;
            }
        }
    }
    else
        MESSAGE_PRINT(p, "Cannot obtain GCLK configuration\n");
}

/***************************************************************************1*
Function: ShowDllStartupMessage
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
Output: None
Description: Showing stratup message for this DLL (version number and help command).
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void ShowDllStartupMessage(struct TyArcInstance *p)
{
    MESSAGE_PRINT(p, "*** %s\n", INTERFACE_NAME_AND_VERSION);
    MESSAGE_PRINT(p, "*** Say 'prop dll_help' for internal help.\n");
}

/****************************************************************************
Function: DEBUGGER_SLEEP
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
unsigned miliseconds - number of miliseconds to sleep
Output: None
Description: sleep for given number of miliseconds using debugger function
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void DEBUGGER_SLEEP(struct TyArcInstance *p, unsigned miliseconds)
{
    if (!miliseconds)
        return;
    // if debugger sleep implementation is available, use it otherwise use alternative
    if ((p != NULL) && (p->pcbCallbackFunction != NULL) && (p->pcbCallbackFunction->version() >= 3))
        p->pcbCallbackFunction->sleep(miliseconds);
    else
        Sleep(miliseconds);        // use alternative from Windows
}

/****************************************************************************
Function: ShowARCSetupWindowFromDll
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
Output: int32_t - TRUE if new setting is valid, FALSE to cancel
Description: show ARC setup dialog window in GUI, called from external DLL
Date           Initials    Description
15-Oct-2007    VH          Initial
26-Feb-2008    NCH         
****************************************************************************/
int32_t ShowARCSetupWindowFromDll(struct TyArcInstance *p)
{
    char                pszDllPath[_MAX_PATH];
    int32_t                 iRetValue = FALSE;

    TyARCSetupInterface tyARCSetupInterface;
    PFARCSetupFunction  pfARCSetupWindow = NULL;
    HINSTANCE           hARCGuiDll = NULL;

    ML_GetDllPath(pszDllPath, _MAX_PATH);
    if (!strcmp(pszDllPath, ""))
        strcpy(pszDllPath, ARCGUI_DLL);
    else
    {
#ifdef __LINUX
        strcat(pszDllPath, "/");
#else
        strcat(pszDllPath, "\\");
#endif
        strcat(pszDllPath, ARCGUI_DLL);
    }
    p = p;
    hARCGuiDll = LoadLibrary(pszDllPath);
    if (hARCGuiDll == NULL)
        return FALSE;                             // cannot load DLL
    // get pointer to function
    pfARCSetupWindow = (PFARCSetupFunction)GetProcAddress(hARCGuiDll, ARCGUISETUPDLG_NAME);
    // call function
    if (pfARCSetupWindow != NULL)
    {
        // set pointers to exported functions so they can be called from external DLL if required
        tyARCSetupInterface.pfGetID               = ASH_GetInterfaceID;
        tyARCSetupInterface.pfListConnectedProbes = ASH_ListConnectedProbes;
        tyARCSetupInterface.pfGetSetupItem        = (PFAshGetSetupItem)ASH_GetSetupItem;              //lint !e611
        tyARCSetupInterface.pfSetSetupItem        = (PFAshSetSetupItem)ASH_SetSetupItem;              //lint !e611
        tyARCSetupInterface.pfAutoDetectScanChain = (PFAshAutoDetectScanChain)ASH_AutoDetectScanChain;

        if (pfARCSetupWindow((void*)p, &tyARCSetupInterface))
            iRetValue = TRUE;
    }
    (void)FreeLibrary(hARCGuiDll);
    return iRetValue;
}

/****************************************************************************
Function: CallExternalARCSetup
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
PFARCSetupFunction pfExternalARCSetup - external callback function
Output: int32_t - TRUE if new setting is valid, FALSE to cancel
Description: call external ARC setup callback function
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/
int32_t CallExternalARCSetup(struct TyArcInstance *p, PFARCSetupFunction pfExternalARCSetup)
{
    TyARCSetupInterface tyARCSetupInterface;
    int32_t iRetValue = FALSE;

    p = p;
    // check pointer to external callback
    if (pfExternalARCSetup == NULL)
        return FALSE;
    // prepare structure with local pointers
    tyARCSetupInterface.pfGetID               = ASH_GetInterfaceID;
    tyARCSetupInterface.pfListConnectedProbes = ASH_ListConnectedProbes;
    tyARCSetupInterface.pfGetSetupItem        = (PFAshGetSetupItem)ASH_GetSetupItem; //lint !e611
    tyARCSetupInterface.pfSetSetupItem        = (PFAshSetSetupItem)ASH_SetSetupItem; //lint !e611
    tyARCSetupInterface.pfAutoDetectScanChain = (PFAshAutoDetectScanChain)ASH_AutoDetectScanChain;
    // call callback with prepared structure
    if (pfExternalARCSetup((void*)p, &tyARCSetupInterface))
        iRetValue = TRUE;
    return iRetValue;
}

/****************************************************************************
Function: StoreINIFile
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
const char *pszEnvVarName - name of optional environment variable with INI path
const char *pszDefaultIniFilename - default name of INI file
Output: none
Description: store ARC setup configuration into INI file
Date           Initials    Description
17-Oct-2007    VH          Initial
09-Sep-2009    RSB         TAP reset selection stored to ini file
****************************************************************************/
void StoreINIFile(struct TyArcInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename)
{
    int32_t   iCoreIndex;
    char  pszValue[100];
    char *pszEnvVarValue;
    FILE *pIniFile;

    if ((p == NULL) || (pszEnvVarName == NULL) || (pszDefaultIniFilename == NULL))
        return;

    // check if environment variable exists and possibly use it as INI filename, try to open INI file
    pszEnvVarValue = getenv(pszEnvVarName);
    if (pszEnvVarValue != NULL)
        strcpy(p->pszIniFileLocation, pszEnvVarValue);

    pIniFile = fopen(p->pszIniFileLocation, "wt");

    if (pIniFile == NULL)
        return;

    // writing target configuration section
    fprintf(pIniFile, "[%s]\n", ARC_SETUP_SECT_TARGETCONFIG);                                 // section name
    ASH_GetSetupItem(p, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
    if (strcmp(pszValue, ""))
        fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_TARGETOPTION, pszValue);                   // store target selection
    ASH_GetSetupItem(p, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, pszValue);
    if (strcmp(pszValue, ""))
        fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_ADAPTIVECLOCK, pszValue);                  // store adaptive clock selection
    ASH_GetSetupItem(p, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, pszValue);
    if (strcmp(pszValue, ""))
        fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_TAPRESET, pszValue);                  // store TAP reset selection

    // writing misc options
    fprintf(pIniFile, "[%s]\n", ARC_SETUP_SECT_MISCOPTIONS);                                  // section name
    ASH_GetSetupItem(p, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_SHOWCONFIG, pszValue);
    if (strcmp(pszValue, ""))
        fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_SHOWCONFIG, pszValue);                     // using GUI option
    ASH_GetSetupItem(p, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, pszValue);
    if (strcmp(pszValue, ""))
        fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_PROBEINSTANCE, pszValue);                  // probe instance
    // writing multicore section
    fprintf(pIniFile, "[%s]\n", ARC_SETUP_SECT_MULTICORE);                                    // section name
    ASH_GetSetupItem(p, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);
    if (strcmp(pszValue, ""))
        fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);                  // using GUI option
    // write information about each core
    for (iCoreIndex=0; iCoreIndex < (int32_t)ptyMLHandle->ulNumberOfCoresOnScanchain; iCoreIndex++)
    {
        char pszSect[20];
        sprintf(pszSect, "%s%d", ARC_SETUP_SECT_DEVICE, iCoreIndex + 1);
        fprintf(pIniFile, "[%s]\n", pszSect);
        ASH_GetSetupItem(p, pszSect, ARC_SETUP_ITEM_ARCCORE, pszValue);
        if (strcmp(pszValue, ""))
            fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_ARCCORE, pszValue);                     // is it ARC core ?
        ASH_GetSetupItem(p, pszSect, ARC_SETUP_ITEM_IRWIDTH, pszValue);
        if (strcmp(pszValue, ""))
            fprintf(pIniFile, "%s=%s\n", ARC_SETUP_ITEM_IRWIDTH, pszValue);                     // IR length
    }

    fclose(pIniFile);                         // close INI file
}

/****************************************************************************
Function: LoadINIFile
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p           - pointer to instance structure
const char *pszEnvVarName         - name of optional environment variable with INI path
const char *pszDefaultIniFilename - default name of INI file
const char *pszIniFileLocation    - location of INI file
Output: none
Description: load ARC setup configuration from INI file
Date           Initials    Description
17-Oct-2007    VH          Initial
18-Jan-2017    AS          Added INI file location
****************************************************************************/
int32_t LoadINIFile(struct TyArcInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename)
{
    FILE *pIniFile;
    char *pszEnvVarValue = NULL;
    char  pszCurrentSection[32];
    char  pszIniLine[_MAX_PATH];

    if ((p == NULL) || (pszDefaultIniFilename == NULL))
        return -1;

    // check if environment variable exists and possibly use it as INI filename, try to open INI file
    if (pszEnvVarName != NULL)
        pszEnvVarValue = getenv(pszEnvVarName);

    if (pszEnvVarValue != NULL)
        strcpy(p->pszIniFileLocation, pszEnvVarValue);

    pIniFile = fopen(p->pszIniFileLocation, "rt");// we should use default name in same directory as DLL

    if (pIniFile == NULL)
        return -1;

    strcpy(pszCurrentSection, "");

    // read INI file line by line
    while (fgets(pszIniLine, _MAX_PATH, pIniFile) != NULL)
    {  // got line so try to parse it, first we need to remove any '\r' and '\n' characters (just strap them)
        char *pszLabelEnd = NULL;

        pszLabelEnd = strchr(pszIniLine, '\n');
        if (pszLabelEnd != NULL)
            *pszLabelEnd = 0;
        pszLabelEnd = strchr(pszIniLine, '\r');
        if (pszLabelEnd != NULL)
            *pszLabelEnd = 0;
        if ((pszIniLine[0] == '[') && (strchr(pszIniLine, ']') != NULL))
        {  // got section label
            pszLabelEnd = strchr(pszIniLine, ']');
            if (pszLabelEnd != NULL)
                *pszLabelEnd = 0;                                           // cut end of label
            strncpy(pszCurrentSection, &(pszIniLine[1]), 32);                // copy label name into local 
            pszCurrentSection[31] = 0;
        }
        else if (strchr(pszIniLine, '=') != NULL)
        {  // got variable assignment
            pszLabelEnd = strchr(pszIniLine, '=');
            if (pszLabelEnd != NULL)
            {
                char *pszValue;
                pszValue = pszLabelEnd + 1;                                    // value after '=' character to the end of line
                *pszLabelEnd = 0;                                              // cut '=' from string
                ASH_SetSetupItem(p, pszCurrentSection, pszIniLine, pszValue);   // leave parsing to AshSetSetupItem, just call it
            }
        }
    }

    fclose(pIniFile);

    return 1;
}

/****************************************************************************
*** Additional exported functions from DLL with ASH_ prefix               ***
****************************************************************************/

/****************************************************************************
Function: ASH_ListConnectedProbes
Engineer: Vitezslav Hola
Input: char ppszInstanceNumber[][16] - buffer for list of connected instances
unsigned short usMaxInstances - maximum number of instances in list
unsigned short *pusConnectedInstances - current number of connected instances
Output: none
Description: Get list with serial numbers of connected instances of Opella-XD probe.
Caller should provide buffer for list and specify maximum number of instance for the list.
This function detects number of connected probes and fills list with their serial numbers.
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_ListConnectedProbes(char ppszInstanceNumber[][16], unsigned short usMaxInstances, 
                                                       unsigned short *pusConnectedInstances)
{
    // check connected instances of probe (Opella-XD)
    ML_GetListOfConnectedProbes(ppszInstanceNumber, usMaxInstances, pusConnectedInstances);
}

/****************************************************************************
Function: ASH_GetInterfaceID
Engineer: Vitezslav Hola
Input: None
Output: const char * - string with interface name and version
Description: return string with interface name and version number
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT const char *ASH_GetInterfaceID(void)
{
    return INTERFACE_NAME_AND_VERSION;
}

/****************************************************************************
Function: ASH_SetSetupItem
Engineer: Vitezslav Hola
Input: struct TyArcInstance *ptyPtr - pointer to ARC instance structure
const char *pszSection - string with section name
const char *pszItem - string with item name
const char *pszValue - string with value
Output: none
Description: set ARC setup item specified by section and item name to 
particular value in ARC structure (pointer passed as parameter)
Date           Initials    Description
15-Oct-2007    VH          Initial
09-Sep-2009    RSB         TAP reset selection added
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_SetSetupItem(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, const char *pszValue)
{
    if (pszSection == NULL)
        return;

    // check for valid parameters
    if (strcmp(pszSection, ARC_SETUP_SECT_INIFILE))
    {
        if ((ptyPtr == NULL) || (ptyMLHandle == NULL) || 
            (pszItem == NULL) || (pszValue == NULL))
            return;
    }

    // parse section name
    if (!strcmp(pszSection, ARC_SETUP_SECT_INIFILE))
        pszIniFileName = (char*)pszValue;
    else if (!strcmp(pszSection, ARC_SETUP_SECT_TARGETCONFIG))
    {  // this is target configuration section
        if (!strcmp(pszItem, ARC_SETUP_ITEM_TARGETOPTION))
        {     // check for target options, value must be string with 1 character
            if (strlen(pszValue) == 1)
            {
                switch (pszValue[0])
                {
                case '0':   // ARCangel target
                    ptyMLHandle->tyArcTargetType = ARC_TGT_ARCANGEL;
                    break;
                case '1':   // ARC target (do not determine between target with/without reset)
                    ptyMLHandle->tyArcTargetType = ARC_TGT_NORMAL;
                    break;
                case '2':   // ARCangel target with reset detection
                    ptyMLHandle->tyArcTargetType = ARC_TGT_RSTDETECT;
                    break;
                case '3':   // ARC target with CJTAG with TPA Revision 1
                    ptyMLHandle->tyArcTargetType = ARC_TGT_CJTAG_TPA_R1;
                    break;
                case '4':   // ARC target with JTAG with TPA Revision 1
                    ptyMLHandle->tyArcTargetType = ARC_TGT_JTAG_TPA_R1;
                    break;
                default:    // invalid option, do nothing
                    break;
                }
            }
        }
        else if (!strcmp(pszItem, ARC_SETUP_ITEM_ADAPTIVECLOCK))
        {
            if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
                ptyMLHandle->bAdaptiveClockEnabled = 1;
            else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
                ptyMLHandle->bAdaptiveClockEnabled = 0;
        }
        else if (!strcmp(pszItem, ARC_SETUP_ITEM_TAPRESET))
        {
            if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
                ptyMLHandle->bDoNotIssueTAPReset = 1;
            else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
                ptyMLHandle->bDoNotIssueTAPReset = 0;
            // else do nothing (invalid value)
        }

    }
    else if (!strcmp(pszSection, ARC_SETUP_SECT_MISCOPTIONS))
    {  // misc section
        if (!strcmp(pszItem, ARC_SETUP_ITEM_SHOWCONFIG))   // displaying GUI
        {
            if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
                ptyPtr->bUseGUI = 1;
            else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
                ptyPtr->bUseGUI = 0;
        }
        else if (!strcmp(pszItem, ARC_SETUP_ITEM_PROBEINSTANCE))    // which Opella-XD instance is selected
        {
            strncpy(ptyMLHandle->pszProbeInstanceNumber, pszValue, 16);
            ptyMLHandle->pszProbeInstanceNumber[15] = 0;
        }
    }
    else if (!strcmp(pszSection, ARC_SETUP_SECT_MULTICORE))
    {  // multicore section
        if (!strcmp(pszItem, ARC_SETUP_ITEM_NUMBEROFCORES))
        {
            int32_t iNumber = 0;
            //            if ((sscanf(pszValue, "%d", &iNumber) == 1) && (iNumber >= 1) && (iNumber <= MAX_ARC_CORES))
            //changed with introduction of auto detect scan chain
            if ((sscanf(pszValue, "%d", &iNumber) == 1) && (iNumber <= MAX_ARC_CORES))
            {
                ptyMLHandle->ulNumberOfCoresOnScanchain = (uint32_t)iNumber;
            }
        }
    }
    else if (strstr(pszSection, ARC_SETUP_SECT_DEVICE) == pszSection)
    {  // section about particular core
        int32_t iDeviceNum = 0;
        char *pszNum = NULL;
        pszNum = (char *)&(pszSection[strlen(ARC_SETUP_SECT_DEVICE)]);      // get pointer to number in string
        if ((sscanf(pszNum, "%d", &iDeviceNum) == 1) && (iDeviceNum >= 1) && 
            (iDeviceNum <= ARC_SETUP_MAX_SCANCHAIN_CORES))
        {
            iDeviceNum--;
            if (!strcmp(pszItem, ARC_SETUP_ITEM_ARCCORE))
            {  // is it ARC core or not
                if (!strcmp(pszValue, ARC_SETUP_ITEM_TRUE))
                    ptyMLHandle->tyScanchainCoreParams[iDeviceNum].bARCCore = 1;
                else if (!strcmp(pszValue, ARC_SETUP_ITEM_FALSE))
                    ptyMLHandle->tyScanchainCoreParams[iDeviceNum].bARCCore = 0;
            }
            else if (!strcmp(pszItem, ARC_SETUP_ITEM_IRWIDTH))
            {
                int32_t iWidth = 0;
                if ((sscanf(pszValue, "%d", &iWidth) == 1) && 
                    (iWidth >= 1) && 
                    (iWidth <= ARC_SETUP_MAX_CORE_IR_LENGTH))
                    ptyMLHandle->tyScanchainCoreParams[iDeviceNum].ulIRLength = (uint32_t)iWidth;
            }
        }
    }
}

/****************************************************************************
Function: ASH_GetSetupItem
Engineer: Vitezslav Hola
Input: struct TyArcInstance *ptyPtr - pointer to ARC instance structure
const char *pszSection - string with section name
const char *pszItem - string with item name
const char *pszValue - string with value
Output: none
Description: get ARC setup item specified by section and item name
from ARC structure (pointer passed as parameter)
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_GetSetupItem(struct TyArcInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue)
{
    // check for valid parameters
    if (pszValue == NULL)
        return;
    // first copy empty string
    strcpy(pszValue, "");
    if ((ptyPtr == NULL) || (ptyMLHandle == NULL) || (pszSection == NULL) || (pszItem == NULL))
        return;

    // parse section name
    if (!strcmp(pszSection, ARC_SETUP_SECT_INIFILE))
        strcpy(pszValue, ptyPtr->pszIniFileLocation);
    else if (!strcmp(pszSection, ARC_SETUP_SECT_TARGETCONFIG))
    {  // this is target configuration section
        if (!strcmp(pszItem, ARC_SETUP_ITEM_TARGETOPTION))
        {
            switch (ptyMLHandle->tyArcTargetType)                  // target options
            {
            case ARC_TGT_ARCANGEL:
                strcpy(pszValue, "0");        // ARCangel target
                break;
            case ARC_TGT_RSTDETECT:
                strcpy(pszValue, "2");        // ARC target with reset detection
                break;
            case ARC_TGT_CJTAG_TPA_R1:
                strcpy(pszValue, "3");        // ARC target with cJTAG and TPA Revision 1
                break;
            case ARC_TGT_JTAG_TPA_R1:
                strcpy(pszValue, "4");        // ARC target with JTAG and TPA Revision 1
                break;
            case ARC_TGT_NORMAL:
            default:                         // so it must be normal ARC target
                strcpy(pszValue, "1");
                break;
            }
        }
        else if (!strcmp(pszItem, ARC_SETUP_ITEM_ADAPTIVECLOCK))
        {
            if (ptyMLHandle->bAdaptiveClockEnabled)                // using adaptive clock or not
                strcpy(pszValue, ARC_SETUP_ITEM_TRUE);
            else
                strcpy(pszValue, ARC_SETUP_ITEM_FALSE);
        }
        else if (!strcmp(pszItem, ARC_SETUP_ITEM_TAPRESET))
        {
            if (ptyMLHandle->bDoNotIssueTAPReset)                // using adaptive clock or not
                strcpy(pszValue, ARC_SETUP_ITEM_TRUE);
            else
                strcpy(pszValue, ARC_SETUP_ITEM_FALSE);
        }
    }
    else if (!strcmp(pszSection, ARC_SETUP_SECT_MISCOPTIONS))
    {  // misc section
        if (!strcmp(pszItem, ARC_SETUP_ITEM_SHOWCONFIG))
        {
            if (ptyPtr->bUseGUI)                                          // showing configuration dialog or not
                strcpy(pszValue, ARC_SETUP_ITEM_TRUE);
            else
                strcpy(pszValue, ARC_SETUP_ITEM_FALSE);
        }
        else if (!strcmp(pszItem, ARC_SETUP_ITEM_PROBEINSTANCE))          // which Opella-XD instance is selected
            strcpy(pszValue, ptyMLHandle->pszProbeInstanceNumber);
    }
    else if (!strcmp(pszSection, ARC_SETUP_SECT_MULTICORE))
    {  // multicore section
        if (!strcmp(pszItem, ARC_SETUP_ITEM_NUMBEROFCORES))
            sprintf(pszValue, "%d", (int32_t)ptyMLHandle->ulNumberOfCoresOnScanchain);
    }
    else if (strstr(pszSection, ARC_SETUP_SECT_DEVICE) == pszSection)
    {  // section about particular core
        int32_t iDeviceNum = 0;
        char *pszNum = NULL;
        pszNum = (char *)&(pszSection[strlen(ARC_SETUP_SECT_DEVICE)]);      // get pointer to number in string
        if ((sscanf(pszNum, "%d", &iDeviceNum) == 1) && (iDeviceNum >= 1) && (iDeviceNum <= ARC_SETUP_MAX_SCANCHAIN_CORES))
        {
            iDeviceNum--;
            if (!strcmp(pszItem, ARC_SETUP_ITEM_ARCCORE))
            {
                if (ptyMLHandle->tyScanchainCoreParams[iDeviceNum].bARCCore)    // is it ARC core or not
                    strcpy(pszValue, ARC_SETUP_ITEM_TRUE);
                else
                    strcpy(pszValue, ARC_SETUP_ITEM_FALSE);
            }
            else if (!strcmp(pszItem, ARC_SETUP_ITEM_IRWIDTH))
                sprintf(pszValue, "%d", (int32_t)ptyMLHandle->tyScanchainCoreParams[iDeviceNum].ulIRLength);
        }
    }
}

/****************************************************************************
Function: ASH_GetLastErrorMessage
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *pszMessage - pointer to buffer with message
int32_t iSize - number of bytes in buffer
Output: none
Description: get text description of last error
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_GetLastErrorMessage(struct TyArcInstance *p, char *pszMessage, uint32_t uiSize)
{
    // validate parameters
    if ((p == NULL) || (pszMessage == NULL) || (uiSize == 0))
        return;
    else
    {
        char *pszLocMsg = ML_GetErrorMessage(ptyMLHandle->iLastErrorCode);            // get message
        strncpy(pszMessage, pszLocMsg, uiSize);
        pszMessage[uiSize-1] = 0;                                                        // terminate string (just for sure)
    }
}

/****************************************************************************
Function: ASH_TestScanchain
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC structure
Output: int32_t - 1 if test passes, 0 if fails
Description: test scanchain loopback (TDI to TDO)
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_TestScanchain(struct TyArcInstance *p)
{
    //   MESSAGE_PRINT(p, "TAP Reset Done.\n");
    if (p != NULL)
    {
        if (ML_TestScanchain(ptyMLHandle, 1) != ERR_ML_ARC_NO_ERROR)
            return ASH_FALSE;
        else
            return ASH_TRUE;
    }
    else
        return ASH_FALSE;
}

/****************************************************************************
Function: ASH_AutoDetectScanChain
Engineer: Andre Schmiel
Input: struct TyArcInstance *p - pointer to ARC structure
Output: int32_t - 1
Description: detect scanchain structure and decide about number of cores on scanchain and IR length

Date           Initials    Description
10-Apr-2017    AS          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_AutoDetectScanChain(struct TyArcInstance *p)
{
    uint32_t  uiNumberOfCores, uiIndex;
    uint32_t pulIRWidth[MAX_ARC_CORES];
    unsigned char pbARCCores[MAX_ARC_CORES];
    unsigned char ucConnectionOpened = ptyMLHandle->bConnectionInitialized;
    char          pszValue[5];
    char          pszLocCore[20];
    double        dNewFrequency      = 0.0;
    double        dOldFrequency      = ptyMLHandle->dJtagFrequency;

    if (ucConnectionOpened == 0)
    {
        if (ML_OpenConnection(ptyMLHandle) != ERR_ML_ARC_NO_ERROR)
            return ASH_FALSE;
    }

    //we need to select a low frequency (BSCI works with 300kHz)
    (void)ML_ConvertFrequencySelection("100kHz", &dNewFrequency);

    if (ML_SetJtagFrequency(ptyMLHandle, dNewFrequency) != ERR_ML_ARC_NO_ERROR) // set new frequency value
    {
        if (ucConnectionOpened == 0)
            (void)ML_CloseConnection(ptyMLHandle);

        return ASH_FALSE;
    }

    //get current target option 
    ASH_GetSetupItem(p, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
    ptyMLHandle->tyArcTargetType = (TyArcTargetType)strtoul(pszValue, NULL, 16);
    if (ptyMLHandle->tyArcTargetType == ARC_TGT_CJTAG_TPA_R1)
        ML_IntializeCJTAG(ptyMLHandle);

    if (ASH_DetectScanchain(p, MAX_ARC_CORES, &uiNumberOfCores, pulIRWidth, pbARCCores) != ASH_TRUE)
    {
        ptyMLHandle->ulNumberOfCoresOnScanchain = 0;
        ASH_SetSetupItem(p, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, "0");

        if (ucConnectionOpened == 0)
            (void)ML_CloseConnection(ptyMLHandle);
        else
            (void)ML_SetJtagFrequency(ptyMLHandle, dOldFrequency);

        return ASH_FALSE;
    }
    else
    {
        sprintf(pszValue, "%d", uiNumberOfCores);
        ASH_SetSetupItem(p, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, pszValue);

        for (uiIndex = 0; uiIndex < uiNumberOfCores; uiIndex++)
        {
            sprintf(pszLocCore, "%s%d", ARC_SETUP_SECT_DEVICE, uiIndex+1);

            if (pbARCCores[uiIndex])
                ASH_SetSetupItem(p, pszLocCore, ARC_SETUP_ITEM_ARCCORE, ARC_SETUP_ITEM_TRUE);
            else
                ASH_SetSetupItem(p, pszLocCore, ARC_SETUP_ITEM_ARCCORE, ARC_SETUP_ITEM_FALSE);

            sprintf(pszValue, "%d", pulIRWidth[uiIndex]);
            ASH_SetSetupItem(p, pszLocCore, ARC_SETUP_ITEM_IRWIDTH, pszValue);
        }

        //store modified target option 
        sprintf(pszValue, "%d", ptyMLHandle->tyArcTargetType);
        ASH_SetSetupItem(p, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, pszValue);
    }

    //switch back to old frequency settings
    if (ML_SetJtagFrequency(ptyMLHandle, dOldFrequency) != ERR_ML_ARC_NO_ERROR) // set old frequency value
        return ASH_FALSE;

    if (ucConnectionOpened == 0)
        (void)ML_CloseConnection(ptyMLHandle);

    return ASH_TRUE;
}

/****************************************************************************
Function: ASH_DetectScanchain
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC structure
uint32_t uiMaxCores - maximum number of cores supported
uint32_t *puiNumberOfCores - pointer to number of detected cores
uint32_t *pulIRLength - array of IR length for each core, array must have uiMaxCores items
unsigned char *pbARCcores - array of boolean indicating ARC core, array must have uiMaxCores items
Output: int32_t - 1 if detection was successful, 0 if detection failed
Description: detect scanchain structure and decide about number of cores on scanchain and IR length

Date           Initials    Description
05-Nov-2007    VH          Initial
11-Jan-2017    AS          Added update of Scanchain parameters
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_DetectScanchain(struct TyArcInstance *p, uint32_t uiMaxCores, uint32_t *puiNumberOfCores, 
                                                  uint32_t *pulIRWidth, unsigned char *pbARCCores)
{
    uint32_t pulLocIRWidth[MAX_ARC_CORES];
    unsigned char pbLocARCCores[MAX_ARC_CORES];
    uint32_t  uiLocNumberOfCores = 0;

    if ((p == NULL) || (uiMaxCores == 0))
        return ASH_FALSE;

    // detect scanchain structure
    if (ML_DetectScanchain(ptyMLHandle, MAX_ARC_CORES, &uiLocNumberOfCores, pulLocIRWidth, pbLocARCCores) != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;

    // set information about scanchain structure
    if (uiLocNumberOfCores > uiMaxCores)
        return ASH_FALSE;
    else
    {
        uint32_t uiIndex;

        if (uiLocNumberOfCores == 0)
            return ASH_FALSE;

        *puiNumberOfCores                       = uiLocNumberOfCores;
        ptyMLHandle->ulNumberOfCoresOnScanchain = uiLocNumberOfCores;

        if (pulIRWidth != NULL)
        {
            for (uiIndex=0; uiIndex < uiLocNumberOfCores; uiIndex++)
            {
                pulIRWidth[uiIndex]                                    = pulLocIRWidth[uiIndex];
                ptyMLHandle->tyScanchainCoreParams[uiIndex].ulIRLength = pulLocIRWidth[uiIndex];
            }
        }

        if (pbARCCores != NULL)
        {
            for (uiIndex=0; uiIndex < uiLocNumberOfCores; uiIndex++)
            {
                pbARCCores[uiIndex]                                  = pbLocARCCores[uiIndex];
                ptyMLHandle->tyScanchainCoreParams[uiIndex].bARCCore = pbLocARCCores[uiIndex];
            }
        }
    }

    return ASH_TRUE;
}

/****************************************************************************
Function: ASH_UpdateScanchain
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC structure
Output: none
Description: update scanchain settings in Opella-XD
Date           Initials    Description
05-Nov-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_UpdateScanchain(struct TyArcInstance *p)
{
    if (p == NULL)
        return;
    (void)ML_UpdateScanchain(ptyMLHandle);
}

/****************************************************************************
Function: ASH_SetGeneralMessagePrint
Engineer: Vitezslav Hola
Input: PfPrintfFunc pfFunc - pointer to printf-like function
Output: none
Description: set pointer for general message print function
if pfFunc is NULL, default printf will be used
Date           Initials    Description
31-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_SetGeneralMessagePrint(PfPrintFunc pfFunc)
{
    pfGeneralPrintFunc = pfFunc;
}

/****************************************************************************
Function: ASH_GDBScanIR
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - ARC structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int32_t - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG IR register
Date           Initials    Description
04-Dec-2007    VH          Initial
03-Sept-2008   NCH         Renamed to ASH_GDBScanIR
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_GDBScanIR(struct TyArcInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
    if ((p == NULL) || (ptyMLHandle == NULL))
        return ASH_FALSE;
    if (ulLength == 0)
        return ASH_TRUE;
    if (ML_ScanIR(ptyMLHandle, ulLength, pulDataIn, pulDataOut) != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;
    return ASH_TRUE;
}

/****************************************************************************
Function: ASH_GDBScanDR
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - ARC structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int32_t - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
04-Dec-2007    VH          Initial
03-Sept-2008   NCH         Renamed to ASH_GDBScanDR
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_GDBScanDR(struct TyArcInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
    if ((p == NULL) || (ptyMLHandle == NULL))
        return ASH_FALSE;
    if (ulLength == 0)
        return ASH_TRUE;
    if (ML_ScanDR(ptyMLHandle, ulLength, pulDataIn, pulDataOut) != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;
    return ASH_TRUE;
}

/****************************************************************************
Function: ASH_ScanIR
Engineer: Nikolay Chokoev
Input: struct TyArcInstance *p - ARC structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int32_t - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG IR register
Date           Initials    Description
03-Sept-2008   NCH         Initial
16-Jan -2009    RS         Modified to add new JTAG states.
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_ScanIR(struct TyArcInstance *p, int32_t iLength, uint32_t *pulDataIn, uint32_t *pulDataOut, 
                                         unsigned char ucStartState, unsigned char ucEndState)
{
    int32_t iError;
    //   MESSAGE_PRINT(p, "TAP Reset Done.\n");
    if ((p == NULL) || (ptyMLHandle == NULL))
        return ASH_FALSE;
    if (iLength == 0)
        return ASH_TRUE;
    switch (ucStartState)
    {
    case ARC_START_STATE_FROM_CURRENT:
        //assuming we are in Select-IR and have to go to Shift-IR
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 2;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0;
        break;
    case ARC_START_STATE_FROM_RTI:
        //assuming we are in Idle and have to go to Shift-IR
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 5;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x6;
        break;
    case ARC_START_STATE_FROM_DR:
        //assuming we are in Select-DR and have to go to Shift-IR
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 3;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x1;
        break;
    case ARC_START_STATE_FROM_EXIT1_DR_RTI:
        //assuming we are in Exit1-DR and have to go to Shift-IR via RTI
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 8;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x36;
        break;
    default:
        //wrong parameter
        return ASH_FALSE;
    }

    switch (ucEndState)
    {
    case ARC_END_STATE_TO_RTI:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0xD;
        break;
    case ARC_END_STATE_TO_DR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1D;
        break;
    case ARC_END_STATE_TO_IR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x3D;
        break;
    case ARC_END_STATE_TO_RTI_DR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x2D;
        break;
    case ARC_END_STATE_TO_RTI_IR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 7;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x6D;
        break;
    case ARC_END_STATE_TO_RTI_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x03;
        break;
    case ARC_END_STATE_TO_DR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x07;
        break;
    case ARC_END_STATE_TO_IR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0F;
        break;
    case ARC_END_STATE_TO_RTI_DR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0B;
        break;
    case ARC_END_STATE_TO_RTI_IR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1B;
        break;
    case ARC_END_STATE_TO_EXIT1_IR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 1;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1;
        break;
    default:
        //wrong parameter
        return ASH_FALSE;
    }

    ptyMLHandle->tyMultipleScanParams.bScanIR = 1; 
    ptyMLHandle->tyMultipleScanParams.usLength = (unsigned short)iLength;
    ptyMLHandle->tyMultipleScanParams.usInReserved = 0;
    ptyMLHandle->tyMultipleScanParams.usOutReserved = 0;

    if (pulDataIn != NULL)
        dbgprint("IR:*pulDataIn:%08lX\n",(uint32_t)*pulDataIn);
    else
        dbgprint("IR:*pulDataIn:<null>\n");
    iError = ML_ScanMultiple(ptyMLHandle, &ptyMLHandle->tyMultipleScanParams,
                             pulDataIn, pulDataOut);
    if (pulDataOut != NULL)
        dbgprint("IR:*pulDataOut:%08lX\n",(uint32_t)*pulDataOut);
    else
        dbgprint("IR:*pulDataOut:<null>\n");
    dbgprint("IR:iError:%08lX\n",iError);

    if (iError != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;
    return ASH_TRUE;
}

/****************************************************************************
Function: ASH_ScanDR
Engineer: Nikolay Chokoev
Input: struct TyArcInstance *p - ARC structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int32_t - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
03-Sept-2008   NCH         Initial
16-Jan -2009   RS          Modified to add new JTAG states
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_ScanDR(struct TyArcInstance *p, int32_t iLength, uint32_t *pulDataIn, uint32_t *pulDataOut, 
                                         unsigned char ucStartState, unsigned char ucEndState)
{
    int32_t iError;
    if ((p == NULL) || (ptyMLHandle == NULL))
        return ASH_FALSE;
    if (iLength == 0)
        return ASH_TRUE;

    switch (ucStartState)
    {
    case ARC_START_STATE_FROM_CURRENT:
        //assuming we are in Select-DR and have to go to Shift-DR
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 2;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0;
        break;
    case ARC_START_STATE_FROM_RTI:
        //assuming we are in Idle and have to go to Shift-DR
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 4;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x2;
        break;
    case ARC_START_STATE_FROM_EXIT1_IR_RTI:
        //assuming we are in Exit1-IR and have to go to Shift-DR via RTI
        ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 7;
        ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x16;
        break;

    case ARC_START_STATE_FROM_DR:
        //wrong state - see documentation
    default:
        //wrong parameter
        return ASH_FALSE;
    }

    switch (ucEndState)
    {
    case ARC_END_STATE_TO_RTI:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0xD;
        break;
    case ARC_END_STATE_TO_DR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1D;
        break;
    case ARC_END_STATE_TO_IR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x3D;
        break;
    case ARC_END_STATE_TO_RTI_DR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x2D;
        break;
    case ARC_END_STATE_TO_RTI_IR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 7;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x6D;
        break;
    case ARC_END_STATE_TO_RTI_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x03;
        break;
    case ARC_END_STATE_TO_DR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x07;
        break;
    case ARC_END_STATE_TO_IR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0F;
        break;
    case ARC_END_STATE_TO_RTI_DR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0B;
        break;
    case ARC_END_STATE_TO_RTI_IR_SKIPPAUSE:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1B;
        break;
    case ARC_END_STATE_TO_EXIT1_DR:
        ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 1;
        ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1;
        break;
    default:
        //wrong parameter
        return ASH_FALSE;
    }

    ptyMLHandle->tyMultipleScanParams.bScanIR = 0; 
    ptyMLHandle->tyMultipleScanParams.usLength = (unsigned short)iLength;
    ptyMLHandle->tyMultipleScanParams.usInReserved = 0;
    ptyMLHandle->tyMultipleScanParams.usOutReserved = 0;

    if (pulDataIn != NULL)
        dbgprint("DR:*pulDataIn:%08lX\n",(uint32_t)*pulDataIn);
    else
        dbgprint("DR:*pulDataIn:<null>\n");
    iError = ML_ScanMultiple(ptyMLHandle, &ptyMLHandle->tyMultipleScanParams,
                             pulDataIn, pulDataOut);
    if (pulDataOut != NULL)
        dbgprint("DR:*pulDataOut:%08lX\n",(uint32_t)*pulDataOut);
    else
        dbgprint("DR:*pulDataOut:<null>\n");
    if (iError != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;
    return ASH_TRUE;
}

/****************************************************************************
Function: ASH_TMSReset
Engineer: Nikolay Chokoev
Input: struct TyArcInstance *p - ARC structure
Output: int32_t - TRUE if success, otherwise FALSE
Description: Reset JTAG state machine and go to RTI state.
Date           Initials    Description
03-Sept-2008   NCH         Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_TMSReset(struct TyArcInstance *p)
{
    int32_t iError;
    //   MESSAGE_PRINT(p, "TAP Reset Done.\n");
    if (p == NULL)
        return ASH_FALSE;
    iError = ML_ResetTAP(ptyMLHandle,0);
    if (iError != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;
    return ASH_TRUE;
}
/****************************************************************************
Function: ASH_TRSTReset
Engineer: Rejeesh S Babu
Input: struct TyArcInstance *p - ARC structure
Output: int32_t - TRUE if success, otherwise FALSE
Description: Reset JTAG state machine using TRST pin.
Date           Initials   		 Description
19-Jan-2009    	  RS  	           Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int32_t ASH_TRSTReset(struct TyArcInstance *p)
{
    int32_t iError;
    //   MESSAGE_PRINT(p, "TAP Reset Done.\n");
    if (p == NULL)
        return ASH_FALSE;
    iError = ML_ResetTAP(ptyMLHandle,1);
    if (iError != ERR_ML_ARC_NO_ERROR)
        return ASH_FALSE;
    return ASH_TRUE;
}

/****************************************************************************
Function: SetupPrintMessage
Engineer: Nikolay Chokoev
Input: TyArcInstance *p - ARC structure.
Can be NULL if only store the message
pszMessage - Message to print
iSetupPrint - 0 - Save message; 1 - Print the message
Output: none
Description: Save message for later print. The size of the string should be 
no more than _MAX_PATH.
Date           Initials    Description
22-Aug-2008    NCH         Initial
****************************************************************************/
static void SetupPrintMessage(struct TyArcInstance *p, 
                              const char *pszMessage, 
                              int32_t iSetupPrint)
{
    static int32_t bIsMessage=0;
    static char pszSavedMessage[_MAX_PATH];
    if (iSetupPrint == 0)
    {
        sprintf(pszSavedMessage,"%s",pszMessage);
        bIsMessage = 1; //there is a message
    }

    else if ((p->pcbCallbackFunction != NULL) && (bIsMessage == 1))
    {
        (void)p->pcbCallbackFunction->printf(pszSavedMessage);
        bIsMessage = 0; // there is no a message to display anymore
    }
}

/****************************************************************************
Function: set_mem_watchpoint2
Engineer: Shameerudheen P T
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
07-Jan-2013    SPT         Initial
****************************************************************************/
extern "C" int32_t set_mem_watchpoint2(struct TyArcInstance *p, ARC_ADDR_TYPE addr, int32_t length, unsigned options, void **cookie)
{
    int32_t iAvailableAP, iAvailableAP2;
    uint32_t ulAPvalue;
    cookie = cookie;

    TRACE_PRINT(p, "set_mem_watchpoint2 [%lX] [%X] [%X]\n", addr, length, options);
    // Set current CPU number
    SetCurrentCore(p);

    if (length > 8)
        return ASH_FALSE;

    if (p->iNumberOfAP == 0)
        return ASH_FALSE; // AP not supported

    if (length < 8)
    {     // set watchpoint to single word
        if (get_free_actionpoint(p, &iAvailableAP) == ASH_FALSE)
            return ASH_FALSE;
        // update AP table
        p->ptyAPTable[iAvailableAP].type = WATCH_MEM;
        p->ptyAPTable[iAvailableAP].data.address = addr;
        p->ptyAPTable[iAvailableAP].paired_ap = -1;                    // no paired AP
        // set memory address to watch and AP mode
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {
            ulAPvalue = (((uint32_t)iAvailableAP) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
            if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_ACR, ulAPvalue) == ASH_FALSE)
                return ASH_FALSE;
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        { // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
            if (EnableArcApClk(p) == ASH_FALSE)
                return ASH_FALSE;

            if (write_reg(p, reg_AP_AMV(iAvailableAP), addr) == ASH_FALSE)                // address in memory
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AMM(iAvailableAP), 0x0) == ASH_FALSE)                 // mask should be 0
                return ASH_FALSE;

            dbgprint("WP2:Watchpoint Mode:%08lX\n",options);

            switch (options)
            {
            case ARC_WATCHPOINT_read:
                if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMRP_VALUE) == ASH_FALSE)    // trigger action on read transaction to memory (LD instruction)
                    return ASH_FALSE;
                break;
            case ARC_WATCHPOINT_write:
                if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMWP_VALUE) == ASH_FALSE)    // trigger action on write transaction to memory (LD instruction)
                    return ASH_FALSE;
                break;
            default:
                if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMAP_VALUE) == ASH_FALSE)    // trigger action on read or write transaction to memory (LD instruction)
                    return ASH_FALSE;
            }
        }
    }
    else
    {     // set two AP as paired watchpoint
        if (get_free_actionpoint(p, &iAvailableAP) == ASH_FALSE)
            return ASH_FALSE;
        if (get_free_actionpoint(p, &iAvailableAP2) == ASH_FALSE)
        {     // remove first one already allocated
            (void)remove_actionpoint(p, iAvailableAP2);
            return ASH_FALSE;
        }
        // program first AP
        p->ptyAPTable[iAvailableAP].type = WATCH_MEM;
        p->ptyAPTable[iAvailableAP].data.address = addr;
        p->ptyAPTable[iAvailableAP].paired_ap = iAvailableAP2;
        // write AP register
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {
            ulAPvalue = (((uint32_t)iAvailableAP) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
            if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_ACR, ulAPvalue) == ASH_FALSE)
                return ASH_FALSE;
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        { // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
            if (EnableArcApClk(p) == ASH_FALSE)
                return ASH_FALSE;

            if (write_reg(p, reg_AP_AMV(iAvailableAP), addr) == ASH_FALSE)                // address in memory
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AMM(iAvailableAP), 0x0) == ASH_FALSE)                 // mask should be 0
                return ASH_FALSE;
            dbgprint("WP2:Watchpoint Mode:%08lX\n",options);
            switch (options)
            {
            case ARC_WATCHPOINT_read:
                if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMRP_VALUE) == ASH_FALSE)    // trigger action on read transaction to memory (LD instruction)
                    return ASH_FALSE;
                break;
            case ARC_WATCHPOINT_write:
                if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMWP_VALUE) == ASH_FALSE)    // trigger action on write transaction to memory (LD instruction)
                    return ASH_FALSE;
                break;
            default:
                if (write_reg(p, reg_AP_AC(iAvailableAP), AP_AC_MEMAP_VALUE) == ASH_FALSE)    // trigger action on read or write transaction to memory (LD instruction)
                    return ASH_FALSE;
            }
        }
        // next address and AP
        addr += 4;
        ulAPvalue = (((uint32_t)iAvailableAP2) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
        // program second AP
        p->ptyAPTable[iAvailableAP2].type = WATCH_MEM;
        p->ptyAPTable[iAvailableAP2].data.address = addr;
        p->ptyAPTable[iAvailableAP2].paired_ap = iAvailableAP;
        // write AP register
        if ((p->ucAPVersion == 0x1) || (p->ucAPVersion == 0x2))
        {
            ulAPvalue = (((uint32_t)iAvailableAP2) << AN_V) |  AC_EQ | AT_MWA | AP_ENA;
            if (write_reg(p, reg_ADCR, addr) == ASH_FALSE)
                return ASH_FALSE;
            if (write_reg(p, reg_ACR, ulAPvalue) == ASH_FALSE)
                return ASH_FALSE;
        }
        else if ((p->ucAPVersion == 0x3) || (p->ucAPVersion == 0x4) || (p->ucAPVersion == 0x5))
        { // ARC600, ARC700, ARC-EM and ARC-HS APs, only aux registers are supported for AP on these cores
            if (EnableArcApClk(p) == ASH_FALSE)
                return ASH_FALSE;

            if (write_reg(p, reg_AP_AMV(iAvailableAP2), addr) == ASH_FALSE)               // address in memory
                return ASH_FALSE;
            if (write_reg(p, reg_AP_AMM(iAvailableAP2), 0x0) == ASH_FALSE)                // mask should be 0
                return ASH_FALSE;

            switch (options)
            {
            case ARC_WATCHPOINT_read:
                if (write_reg(p, reg_AP_AC(iAvailableAP2), AP_AC_MEMRP_VALUE) == ASH_FALSE)    // trigger action on read transaction to memory (LD instruction)
                    return ASH_FALSE;
                break;
            case ARC_WATCHPOINT_write:
                if (write_reg(p, reg_AP_AC(iAvailableAP2), AP_AC_MEMWP_VALUE) == ASH_FALSE)    // trigger action on write transaction to memory (LD instruction)
                    return ASH_FALSE;
                break;
            default:
                if (write_reg(p, reg_AP_AC(iAvailableAP2), AP_AC_MEMAP_VALUE) == ASH_FALSE)    // trigger action on read or write transaction to memory (LD instruction)
                    return ASH_FALSE;
            }
        }
    }
    return ASH_TRUE;
}


