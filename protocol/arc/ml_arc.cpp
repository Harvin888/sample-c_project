/****************************************************************************
       Module: ml_arc.cpp
     Engineer: Vitezslav Hola
  Description: Implementation of ARC driver (midlayer) for Opella-XD
Date           Initials    Description
08-Oct-2007    VH          Initial
16-Jan-2009     RS          Modified for Reset using  TRST pin.
****************************************************************************/
#include <stdio.h>
#include "ml_arc.h"
#include "protocol/common/drvopxd.h"
#include "protocol/common/drvopxdarc.h"

//andre: needed for maximum buffer size of 16kB
#include "protocol/common/libusb_dyn.h"
#include "protocol/common/drvopxdcom.h"

#ifndef __LINUX
// Win specific defines
    #include "windows.h"    
    #include "..\..\utility\debug\debug.h"  
#else
// Linux specific defines
    #include <stdlib.h>
    #include <stdio.h>
    #include <malloc.h>
    #include <string.h>
    #include "../../utility/debug/debug.h"
#endif

#ifdef __LINUX
// Linux
typedef uint32_t       DWORD;
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#endif //__LINUX

// default values
#define DEFAULT_ARC_DELAY_AFTER_RESET			            100               // 100 ms default delay after reset
#define DEFAULT_ARC_RESET_DURATION				            50                // 50 ms default reset pulse
#define MAX_ARC_RESET_VALUE                                 5000              // maximum 5s for delay and duration

#define DEFAULT_ARC_RTCK_TIMEOUT                            50                // default timeout is 50 ms
#define MIN_ARC_RTCK_TIMEOUT                                5                 // minimum timeout is 5 ms
#define MAX_ARC_RTCK_TIMEOUT                                20000             // maximum timeout is 20 s

#define DEFAULT_ACCESS_TYPE                                 (ARC_ACCESS_OPTIMIZE_ENABLE | ARC_ACCESS_AUTOINC_AUTO)

// Opella-XD ARC frequency limits and default values
#define MAX_ARC_JTAG_AND_BLAST_FREQUENCY                    100.0             // maximum frequency is 100 MHz
#define MIN_ARC_JTAG_AND_BLAST_FREQUENCY                    0.001             // minimum frequency is 1 kHz
#define DEFAULT_ARC_JTAG_FREQUENCY                          1.0               // 1 MHz as default JTAG clock (safer)
#define DEFAULT_ARC_BLAST_FREQUENCY                         6.0               // 6 MHz as default blast frequency (compatibility with OpellaUSB)

// ARCangel4 PLL restriction constants
#define AA_PLL_MAX_MCLK_FREQ                                120.0             // maximum and minimum absolute frequency
#define AA_PLL_MIN_MCLK_FREQ                                0.390
#define AA_PLL_MAX_VCLK_FREQ                                120.0
#define AA_PLL_MIN_VCLK_FREQ                                0.390
#define AA_PLL_MIN_MVCO_FREQ                                51.9              // maximum and minimum VCO frequency
#define AA_PLL_MAX_MVCO_FREQ                                120.0
#define AA_PLL_MIN_VVCO_FREQ                                51.9
#define AA_PLL_MAX_VVCO_FREQ                                120.0
#define AA_PLL_MIN_P                                        4                 // maximum and minimum P, Q and D ratios
#define AA_PLL_MAX_P                                        130
#define AA_PLL_MIN_Q                                        3
#define AA_PLL_MAX_Q                                        129
#define AA_PLL_MAX_D                                        7
#define AA_PLL_MIN_REFOVERQ                                 0.2               // maximum and minimum fREF / Q
#define AA_PLL_MAX_REFOVERQ                                 1.0
#define AA_PLL_REF_FREQ                                     14.31818;         // PLL reference frequency
#define AA_PLL_VCO_PRESET                                   13                // VCO preset boundaries count
// ARCangel clock routing register bits
#define AA_GCLK_BITS_HIGHIMP                                0x0               
#define AA_GCLK_BITS_PLLMCLK                                0x1
#define AA_GCLK_BITS_PLLVCLK                                0x2
#define AA_GCLK_BITS_CRYSTAL                                0x3
#define AA_GCLK_BITS_HOST                                   0x6
#define AA_GCLK_BITS_DIPS                                   0x7

// test scanchain
// number of bits for testing scanchain (MUST be div by 32) -> max 256 cores
#define ARC_TSTSC_BITS                                      4000     // It was 8192. But did not work as expected in case of CJTAG. So changed o 4000
#define ARC_TSTSC_WORDS                                     (ARC_TSTSC_BITS / 32) 

// local variables
#define DLLPATH_LENGTH                                      (_MAX_PATH)
char pszGlobalDllPath[DLLPATH_LENGTH];                                        // contains DLL path
// buffer for blasting
#define ARC_BLASTING_BUFFER_LEN                             (64*1024)         // 64 kB blasting buffer
unsigned char pucBlastingBuffer[ARC_BLASTING_BUFFER_LEN];

// local functions
static int32_t CompareArrayOfWords(uint32_t *pulArray1, uint32_t *pulArray2, uint32_t ulNumberOfBits);
static void ShiftArrayOfWordsRight(uint32_t *pulArray, uint32_t ulWords);

/****************************************************************************
     Function: StringToLwr
     Engineer: Vitezslav Hola
        Input: char *pszString - string to convert
       Output: none
  Description: Convert string to lower case.
Date           Initials    Description
07-Dec-2007    NCH         Initial
*****************************************************************************/
void StringToLwr(char *pszString)
{
   while(*pszString)
      {
      if ((*pszString >= 'A') && (*pszString <= 'Z')) 
          *pszString += 'a'-'A';
      pszString++;
      }
}

/****************************************************************************
     Function: ML_GetListOfConnectedProbes
     Engineer: Vitezslav Hola
        Input: char ppszInstanceNumbers[][16] - pointer to list for instance numbers

       Output: none
  Description: get list of connected Opella-XD probe (list of serial numbers)
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances)
{
   unsigned short usIndex;
   unsigned short usNumberOfDevices = 0;
   char ppszLocalInstanceNumbers[MAX_DEVICES_SUPPORTED][16];

   // check connected Opella-XDs
   DL_OPXD_GetConnectedDevices(ppszLocalInstanceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);

   // copy list of devices up to maximum number
   if (usNumberOfDevices > usMaxInstances)
      usNumberOfDevices = usMaxInstances;

   for (usIndex=0; usIndex < usNumberOfDevices; usIndex++)
      strcpy(ppszInstanceNumbers[usIndex], ppszLocalInstanceNumbers[usIndex]);

   if (pusConnectedInstances != NULL)
      *pusConnectedInstances = usNumberOfDevices;
}

/****************************************************************************
	 Function: ML_ConfigureOpellaXD
	 Engineer: Rejeesh Shaji Babu
		Input: TyDevHandle tyDevHandle - handle to open device
			   TyDeviceInfo* ptyDeviceInfo - pointer to device info structure
			   bool bConfigureFpga - flag to decide whether to configure FPGA or not
			                         Do not confgiure FPGA if another diskware is already running
	   Output: status - err/success
  Description: Configure Opella-XD (load & configure FPGA, load and execute diskware)
Date           Initials    Description
24-Jun-2019    RSB          Rejeesh
****************************************************************************/
int ML_ConfigureOpellaXD(TyDevHandle tyDevHandle, TyDeviceInfo* ptyDeviceInfo, bool bConfigureFpga)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	unsigned int uiTPARev;

	ptyDeviceInfo->tyTpaConnected = TPA_NONE;
	if ((DL_OPXD_UnconfigureDevice(tyDevHandle) != DRVOPXD_ERROR_NO_ERROR) ||
		(DL_OPXD_GetDeviceInfo(tyDevHandle, ptyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR))
	{
		(void)DL_OPXD_CloseDevice(tyDevHandle);
		return ERR_ML_ARC_USB_DRIVER_ERROR;
	}
	if (ptyDeviceInfo->tyTpaConnected != TPA_ARC20)
	{
		(void)DL_OPXD_CloseDevice(tyDevHandle);
		return ERR_ML_ARC_PROBE_INVALID_TPA;
	}

	if (!strcmp(pszGlobalDllPath, "")) //empty path
	{
		uiTPARev = (unsigned int)(((ptyDeviceInfo->ulTpaRevision) & 0xFF000000) >> 24);
		switch (uiTPARev)
		{
		case 1: //R1 TPA
			if (true == bConfigureFpga)
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000) //R2 Opella-XD				
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, OPXD_FPGAWARE_ARC_TPA_R1_FILENAME);
				else //R3 Opella-XD
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_LPC1837_FILENAME, OPXD_FPGAWARE_ARC_LPC1837_FILENAME);
			}
			else
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000) //R2 Opella-XD				
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, "");
				else //R3 Opella-XD
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_LPC1837_FILENAME, "");
			}
			break;
		case 0: // Old TPA ?
		default:
			if (true == bConfigureFpga)
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, OPXD_FPGAWARE_ARC_FILENAME);
				else
					return ERR_ML_ARC_USB_DRIVER_ERROR;
			}
			else
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, OPXD_DISKWARE_ARC_FILENAME, "");
				else
					return ERR_ML_ARC_USB_DRIVER_ERROR;
			}
		}
	}
	else //path not empty
	{
		char pszLocDiskwareName[_MAX_PATH];
		char pszLocFpgawareName[_MAX_PATH];
		strcpy(pszLocDiskwareName, pszGlobalDllPath);
#ifdef __LINUX
		strcat(pszLocDiskwareName, "/");
#else
		strcat(pszLocDiskwareName, "\\");
#endif
		if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
			strcat(pszLocDiskwareName, OPXD_DISKWARE_ARC_FILENAME);
		else
			strcat(pszLocDiskwareName, OPXD_DISKWARE_ARC_LPC1837_FILENAME);

		// create full fpgaware filename
		strcpy(pszLocFpgawareName, pszGlobalDllPath);
#ifdef __LINUX
		strcat(pszLocFpgawareName, "/");
#else
		strcat(pszLocFpgawareName, "\\");
#endif
		uiTPARev = (unsigned int)(((ptyDeviceInfo->ulTpaRevision) & 0xFF000000) >> 24);
		switch (uiTPARev)
		{
		case 1:
			if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
				strcat(pszLocFpgawareName, OPXD_FPGAWARE_ARC_TPA_R1_FILENAME);
			else
				strcat(pszLocFpgawareName, OPXD_FPGAWARE_ARC_LPC1837_FILENAME);
			break;
		case 0:
		default:
			if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
				strcat(pszLocFpgawareName, OPXD_FPGAWARE_ARC_FILENAME);
			else
				return ERR_ML_ARC_USB_DRIVER_ERROR;
			break;
		}
		if (true == bConfigureFpga)
		{
			tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, pszLocDiskwareName, pszLocFpgawareName);
		}
		else
		{
			tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_ARC, FPGA_ARC, pszLocDiskwareName, "");
		}
	}

	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyDevHandle);
		return ERR_ML_ARC_PROBE_CONFIG_ERROR;
	}
	return tyRes;
}

/****************************************************************************
	 Function: ML_ArcVParamsInit
	 Engineer: Rejeesh Shaji Babu
		Input: TyArcTargetParams* ptyArcParams - structure to initialise
			   TyDevHandle tyDevHandle - handle to open device
			   TyDeviceInfo* ptyDeviceInfo - pointer to device info structure
			   bool bMCconfig - flag to decide whether Opella-XD MC configuration needs to be done
	   Output: none
  Description: Initialize ArcParams struct
Date           Initials    Description
24-Jun-2019    RSB          Rejeesh
****************************************************************************/
void ML_ArcParamsInit(TyArcTargetParams* ptyArcParams, TyDevHandle tyDevHandle, TyDeviceInfo* ptyDeviceInfo, bool bMCconfig)
{
	uint32_t ulIndex;

	// obtain information about device
	if (DL_OPXD_GetDeviceInfo(tyDevHandle, ptyDeviceInfo, DW_ARC) == DRVOPXD_ERROR_NO_ERROR)
	{  // fill information to local structure
		ptyArcParams->tyProbeFirmwareInfo.ulVersion = ptyDeviceInfo->ulFirmwareVersion;
		ptyArcParams->tyProbeFirmwareInfo.ulDate = ptyDeviceInfo->ulFirmwareDate;
		ptyArcParams->tyProbeDiskwareInfo.ulVersion = ptyDeviceInfo->ulDiskwareVersion;
		ptyArcParams->tyProbeDiskwareInfo.ulDate = ptyDeviceInfo->ulDiskwareDate;
		ptyArcParams->tyProbeFPGAwareInfo.ulVersion = ptyDeviceInfo->ulFpgawareVersion;
		ptyArcParams->tyProbeFPGAwareInfo.ulDate = ptyDeviceInfo->ulFpgawareDate;
	}
	else
	{
		ptyArcParams->tyProbeFirmwareInfo.ulVersion = 0x0;
		ptyArcParams->tyProbeFirmwareInfo.ulDate = 0x0;
		ptyArcParams->tyProbeDiskwareInfo.ulVersion = 0x0;
		ptyArcParams->tyProbeDiskwareInfo.ulDate = 0x0;
		ptyArcParams->tyProbeFPGAwareInfo.ulVersion = 0x0;
		ptyArcParams->tyProbeFPGAwareInfo.ulDate = 0x0;
	}

	// prepare multicore settings
	ptyArcParams->ulCurrentARCCore = 0;
	ptyArcParams->ulNumberOfARCCores = 0;
	for (ulIndex = 0; ulIndex < ptyArcParams->ulNumberOfCoresOnScanchain; ulIndex++)
	{
		if (ptyArcParams->tyScanchainCoreParams[ulIndex].bARCCore)
		{  // found next ARC core
			ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulScanchainCore = ulIndex;        // link ARC core with particular core on scanchain
			ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulCoreVersion = 0x0;
			ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType = DEFAULT_ACCESS_TYPE;
			ptyArcParams->ulCurrentARCCore++;
		}
	}
	ptyArcParams->ulNumberOfARCCores = ptyArcParams->ulCurrentARCCore;
	ptyArcParams->ulCurrentARCCore = 0;                                              // starting with first ARC core
	if (!ptyArcParams->ulNumberOfARCCores)
	{  // we could not find ARC core, in that case, use first core
		ptyArcParams->tyARCCoreParams[0].ulScanchainCore = 0;
		ptyArcParams->tyARCCoreParams[0].ulCoreVersion = 0x0;
		ptyArcParams->tyARCCoreParams[0].usAccessType = DEFAULT_ACCESS_TYPE;
		ptyArcParams->ulNumberOfARCCores = 1;
	}

	if (true == bMCconfig)
	{
		if (ptyArcParams->ulNumberOfCoresOnScanchain > 1)
		{  // more than 2 cores, we need to configure probe
			TyTAPConfig ptyScanchainConfig[MAX_ARC_CORES];
			uint32_t pulBypass[MAX_ARC_CORES];
			// create structure with core information
			for (ulIndex = 0; ulIndex < ptyArcParams->ulNumberOfCoresOnScanchain; ulIndex++)
			{
				pulBypass[ulIndex] = 0xFFFFFFFF;                                  // all 1's as bypass code
				ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyArcParams->tyScanchainCoreParams[ulIndex].ulIRLength);
				ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;               // does not matter
				ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(pulBypass[ulIndex]);    // bypass code
			}
			(void)DL_OPXD_JtagConfigMulticore(tyDevHandle, ptyArcParams->ulNumberOfCoresOnScanchain, ptyScanchainConfig);
		}
		else
			(void)DL_OPXD_JtagConfigMulticore(tyDevHandle, 0, NULL);                 // do not set MC support, we have just 1 core
	}

	// initialize rest of items in structure
	ptyArcParams->bCycleStep = FALSE;
	ptyArcParams->ulDelayAfterReset = DEFAULT_ARC_DELAY_AFTER_RESET;
	ptyArcParams->ulResetDuration = DEFAULT_ARC_RESET_DURATION;
	ptyArcParams->bTargetResetDetectRecursive = 0;
	ptyArcParams->tyOnTargetResetActions.uiNumberOfActions = 0;
	// initialize AA4 clock allocation structure
	ptyArcParams->tyAAClockAlloc.ptyGclk[AA_GCLK(0)] = AA_GCLK_HIGHIMP;           // GCLK0 is highimp as default
	ptyArcParams->tyAAClockAlloc.ptyGclk[AA_GCLK(1)] = AA_GCLK_CRYSTAL;           // GCLK1 is crystal direct as default
	ptyArcParams->tyAAClockAlloc.ptyGclk[AA_GCLK(2)] = AA_GCLK_HOST;              // GCLK2 is host strobe as default
	ptyArcParams->tyAAClockAlloc.ptyGclk[AA_GCLK(3)] = AA_GCLK_DIPS;              // GCLK3 is divided by dips as default
	ptyArcParams->tyAAClockAlloc.dVclkFrequency = 30.0;                           // 30 MHz as default for VCLK
	ptyArcParams->tyAAClockAlloc.dMclkFrequency = 30.0;                           // 30 MHz as default for MCLK
	ptyArcParams->tyAAClockAlloc.bAllocateVclkNext = 0;                           // first allocation for MCLK
	ptyArcParams->tyAAClockAlloc.bRestoreAfterBlasting = 0;                       // do not restore clock routing after blasting
	// we came up here so initialization has been successful
	ptyArcParams->bConnectionInitialized = 1;
	ptyArcParams->iCurrentHandle = (int32_t)tyDevHandle;
	// initialize last error
	ptyArcParams->iLastErrorCode = ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_OpenConnection
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: open connection to debugger (initialize Opella-XD, initialize all 
               data structures, etc.)
               Must be called before any other ML_ functions.
               Opella-XD instance number has to be already filled in the structure
               and initialization flag should be 0 when calling this function.
Date           Initials    Description
10-Oct-2007    VH          Initial
04-Jul-2008    NCH         TPA Init Voltage
10-Apr-2012    SPT         added cJTAG support
10-May-2013    SV          Issued reset before cJTAG mode connection
****************************************************************************/
int32_t ML_OpenConnection(TyArcTargetParams *ptyArcParams)
{
   TyDeviceInfo tyLocDeviceInfo;
   TyDevHandle tyLocHandle = MAX_DEVICES_SUPPORTED;
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;

   // check parameters
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;

   if (ptyArcParams->bConnectionInitialized)
      return ERR_ML_ARC_ALREADY_INITIALIZED;

   // let try to open device with serial number from structure to get a handle
   if (DL_OPXD_OpenDevice(ptyArcParams->pszProbeInstanceNumber, OPELLAXD_USB_PID, false, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_PROBE_NOT_AVAILABLE;

   //Opella-XD R2 FW/DW still dont support CMD_CODE_DISKWARE_STATUS command, so dont use that for R2
   if (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyLocDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
   {
	   (void)DL_OPXD_CloseDevice(tyLocHandle);
	   return ERR_ML_ARC_USB_DRIVER_ERROR;
   }

   if (tyLocDeviceInfo.ulBoardRevision < 0x03000000)
   {
	   //R2
	   tyLocDeviceInfo.uiActiveDiskwares = 0;
	   tyLocDeviceInfo.bOtherDiskwareRunning = 0;
	   (void)DL_OPXD_TerminateDW(tyLocHandle, ARC);
   }
   else
   {
	   //Check if diskware is already running (could be that its loaded by another insatnce of GDB server)
	   if (DL_OPXD_GetDiskwareStatus(tyLocHandle, &tyLocDeviceInfo, ARC) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }
   }

   if (tyLocDeviceInfo.uiActiveDiskwares > 0)
   {
	   //ARC diskware is already running.
	   fprintf(stdout, "ARC diskware (%d) is already running in Opella-XD, hence skipping probe initialization.\n", tyLocDeviceInfo.uiActiveDiskwares);
	   fflush(stdout);

	   //So just do local struct inits
	   ML_ArcParamsInit(ptyArcParams, tyLocHandle, &tyLocDeviceInfo, false);

	   // Call CMD_CODE_EXECUTE_DISKWARE so that Opella-XD firmware is aware that diskware is being used.
	   if (DL_OPXD_ExecuteDW(tyLocHandle, ARC) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }
	   return ERR_ML_ARC_NO_ERROR;
   }
   else if (tyLocDeviceInfo.bOtherDiskwareRunning)
   {
	   //Some other diskware is running.
	   fprintf(stdout, "\nSome other diskware is running in Opella-XD, hence skipping probe initialization.\n");
	   fflush(stdout);

	   //Load and execute ARC diskware, no FPGA config, JTAG freq setting, VTPA setting etc
	   if (ML_ConfigureOpellaXD(tyLocHandle, &tyLocDeviceInfo, false) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }

	   // Just do local struct inits
	   // NOTE: Since another diskware is running, MC config is not done. 
	   // This is assuming that other diskware should have done MC config and that remains same for this also. e.g.: controllerx soc
	   ML_ArcParamsInit(ptyArcParams, tyLocHandle, &tyLocDeviceInfo, false);

	   return ERR_ML_ARC_NO_ERROR;
   }
   else
   {
	   // No diskware running in target, so do all stuff like FPGA config, diskware load, JTAG freq, VTPA set etc...
	   if (ML_ConfigureOpellaXD(tyLocHandle, &tyLocDeviceInfo, true) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }

	   ML_ArcParamsInit(ptyArcParams, tyLocHandle, &tyLocDeviceInfo, true);

	   // ok, probe is configured, with correct TPA, etc. so let prepare for debug session with default settings
       // set target type, TMS sequence, default JTAG frequency, no multicore, fixed voltage of 3.3V
	   ptyArcParams->dJtagFrequency = DEFAULT_ARC_JTAG_FREQUENCY;
	   ptyArcParams->dBlastFrequency = DEFAULT_ARC_BLAST_FREQUENCY;
	   ptyArcParams->ulAdaptiveClockTimeout = DEFAULT_ARC_RTCK_TIMEOUT;
	   if (!ptyArcParams->bAdaptiveClockEnabled)
		   tyRes = DL_OPXD_JtagSetFrequency(tyLocHandle, ptyArcParams->dJtagFrequency, NULL, NULL);              // set default fixed frequency
	   else                                                                                                     // else set adaptive frequency
		   tyRes = DL_OPXD_JtagSetAdaptiveClock(tyLocHandle, ptyArcParams->ulAdaptiveClockTimeout, ptyArcParams->dJtagFrequency);
	   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_TerminateDW(tyLocHandle, ARC);
		   (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_PROBE_CONFIG_ERROR;
	   }

	   /*Commenting because OpellaXD-R2 is not working if enabled*/
       /*if (DL_OPXD_JtagSetTMS(tyLocHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_TerminateDW(tyLocHandle, ARC);
		   (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_PROBE_CONFIG_ERROR;
	   }*/

	   // depending on target type
	   tyRes = DRVOPXD_ERROR_NO_ERROR;
	   switch (ptyArcParams->tyArcTargetType)
	   {
	   case ARC_TGT_ARCANGEL:
		   tyRes = DL_OPXD_Arc_SelectTarget(tyLocHandle, ARCT_ARCANGEL);
		   break;
	   case ARC_TGT_RSTDETECT:
		   tyRes = DL_OPXD_Arc_SelectTarget(tyLocHandle, ARCT_RSTDETECT);
		   break;
	   case ARC_TGT_CJTAG_TPA_R1:
		   tyRes = DL_OPXD_Arc_SelectTarget(tyLocHandle, ARCT_CJTAG_TPA_R1);
		   break;
	   case ARC_TGT_JTAG_TPA_R1:
		   tyRes = DL_OPXD_Arc_SelectTarget(tyLocHandle, ARCT_JTAG_TPA_R1);
		   break;
	   case ARC_TGT_NORMAL:
	   default:
		   tyRes = DL_OPXD_Arc_SelectTarget(tyLocHandle, ARCT_NORMAL);
		   break;
	   }

	   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_TerminateDW(tyLocHandle, ARC);
		   (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_PROBE_CONFIG_ERROR;
	   }
	   // set Vtpa voltage depending on target
	   if (ptyArcParams->tyArcTargetType == ARC_TGT_ARCANGEL)
		   tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 0, 0);  // fixed 3.3V used for ARCangel (because of passive adaptor board)
	   else
		   tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 1, 0);  // use Vtref from target connector for other targets
	   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_TerminateDW(tyLocHandle, ARC);
		   (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_PROBE_CONFIG_ERROR;
	   }
	   // wait to stabilize voltage
	   ML_Sleep(ptyArcParams, 400);                                // 400 ms
	   // enable drivers
	   if (ptyArcParams->tyArcTargetType == ARC_TGT_ARCANGEL)
		   tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 0, 1);  // fixed 3.3V used for ARCangel (because of passive adaptor board)
	   else
		   tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 1, 1);  // use Vtref from target connector for other targets
	   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_TerminateDW(tyLocHandle, ARC);
		   (void)DL_OPXD_UnconfigureDevice(tyLocHandle);
		   (void)DL_OPXD_CloseDevice(tyLocHandle);
		   return ERR_ML_ARC_PROBE_CONFIG_ERROR;
	   }

	   ML_Sleep(ptyArcParams, 500);

	   if (ptyArcParams->tyArcTargetType == ARC_TGT_JTAG_TPA_R1)
	   {
		   tyRes = DL_OPXD_JtagResetTAP(tyLocHandle, FALSE);
	   }
   }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_UpgradeFirmware
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               PfPrintFunc pfPrintFunc - print message function
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: open connection and verify firmware version
               automatically upgrade if necessary
Date           Initials    Description
31-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_UpgradeFirmware(TyArcTargetParams *ptyArcParams, PfPrintFunc pfPrintFunc)
{
   char          pszLocFirmwareName[_MAX_PATH];
   char          pszLocFlashUtilName[_MAX_PATH];
   int32_t           iFirmwareDiff = 0;
   unsigned char bReconnectNeeded = 0;

   TyDeviceInfo tyDeviceInfo;
   
   TyDevHandle tyLocHandle = MAX_DEVICES_SUPPORTED;
   
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;

   if (ptyArcParams->bConnectionInitialized)
      return ERR_ML_ARC_ALREADY_INITIALIZED;

   // let try to open device with serial number from structure to get a handle
   if (DL_OPXD_OpenDevice(ptyArcParams->pszProbeInstanceNumber, OPELLAXD_USB_PID, false, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_PROBE_NOT_AVAILABLE;

   // got handle so do configuration steps
   if (DL_OPXD_UnconfigureDevice(tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_USB_DRIVER_ERROR;
      }

   // get firmware full path
   if (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DRVOPXD_ERROR_USB_DRIVER;

   if (!strcmp(pszGlobalDllPath, ""))
   {
	   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
		   strcpy(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);
	   else
	   {
		   strcpy(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME_LPC1837);
		   strcat(pszLocFlashUtilName, OPXD_FLASHUTIL_FILENAME_LPC1837);
	   }
   }
   else
   {     // there is stored some path so append \ and filenames
	   strcpy(pszLocFirmwareName, pszGlobalDllPath);
	   strcpy(pszLocFlashUtilName, pszGlobalDllPath);

#ifdef __LINUX
	   strcat(pszLocFirmwareName, "/");
	   strcat(pszLocFlashUtilName, "/");
#else
	   strcat(pszLocFirmwareName, "\\");
	   strcat(pszLocFlashUtilName, "\\");
#endif

	   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
		   strcat(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);
	   else
	   {
		   strcat(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME_LPC1837);
		   strcat(pszLocFlashUtilName, OPXD_FLASHUTIL_FILENAME_LPC1837);
	   }
}
   // check firmware version if we need to upgrade
   if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_CHECK_ONLY, pszLocFirmwareName, pszLocFlashUtilName,&bReconnectNeeded, &iFirmwareDiff, NULL, NULL, ptyArcParams->pszProbeInstanceNumber, pfPrintFunc) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_USB_DRIVER_ERROR;
      }
   if (iFirmwareDiff == 0)
      {
      // great, probe has same firmware as host pc, so close handle and return
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_NO_ERROR;
      }
   // we need to upgrade firmware
   bReconnectNeeded = 0;
	if (tyDeviceInfo.ulBoardRevision < 0x03000000)
		printf("Updating Opella-XD firmware...\n");
   if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_DIFF_VERSION, pszLocFirmwareName, pszLocFlashUtilName, &bReconnectNeeded, &iFirmwareDiff, NULL, NULL, ptyArcParams->pszProbeInstanceNumber, pfPrintFunc) != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyLocHandle);
      return ERR_ML_ARC_USB_DRIVER_ERROR;
      }
   if (bReconnectNeeded)
      {
      uint32_t uiCnt;
	  if (pfPrintFunc != NULL)
		  pfPrintFunc("\tResetting and reconnecting to Opella-XD...\n");
      // wait for 7 seconds to reconnect probe
      for (uiCnt=0; uiCnt < 7; uiCnt++)
         ML_Sleep(ptyArcParams, 1000);                                        // 100 ms
      }
   else
      (void)DL_OPXD_CloseDevice(tyLocHandle);                                 // close handle

   if (pfPrintFunc != NULL)
      pfPrintFunc("Opella-XD firmware has been updated\n");
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_CloseConnection
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: close connection to debugger
               Must be called as last of ML_ functions.
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_CloseConnection(TyArcTargetParams *ptyArcParams)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // if device is already closed, just return
   if (!ptyArcParams->bConnectionInitialized || (ptyArcParams->iCurrentHandle >= MAX_DEVICES_SUPPORTED))
      return ERR_ML_ARC_NO_ERROR;   

   //TODO: Handle error
   (void)DL_OPXD_TerminateDW(ptyArcParams->iCurrentHandle, ARC);

   (void)DL_OPXD_CloseDevice(ptyArcParams->iCurrentHandle);
   ptyArcParams->bConnectionInitialized = 0;                            // connection is closed
   ptyArcParams->iCurrentHandle = MAX_DEVICES_SUPPORTED;                // handle to usb is now invalid
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_TestScanchain
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bVerifyNumberOfCores - verify number of cores with MC settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: run scanchain loopback test
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_TestScanchain(TyArcTargetParams* ptyArcParams, unsigned char bVerifyNumberOfCores)
{
    uint32_t ulIndex;
    uint32_t uiShiftIndex;
    unsigned char bPatternMatch = 0;
    uint32_t pulBypassValue[ARC_TSTSC_WORDS];
    uint32_t pulAAPattern[ARC_TSTSC_WORDS];
    uint32_t pul55Pattern[ARC_TSTSC_WORDS];
    uint32_t pulCCPattern[ARC_TSTSC_WORDS];
    uint32_t pul33Pattern[ARC_TSTSC_WORDS];
    uint32_t pul00Pattern[ARC_TSTSC_WORDS];
    uint32_t pulOutValue1[ARC_TSTSC_WORDS];
    uint32_t pulOutValue2[ARC_TSTSC_WORDS];
    uint32_t pulOutValue3[ARC_TSTSC_WORDS];
    uint32_t pulOutValue4[ARC_TSTSC_WORDS];

    int32_t iResult = ERR_ML_ARC_NO_ERROR;
    // check parameters
    if (ptyArcParams == NULL)
        return ERR_ML_ARC_INVALID_HANDLE;
    // testing scanchain is based on putting all cores into bypass mode and shifting different patterns via DR
    // all output patterns should correspond input patterns shifted by number of cores on scanchain (apply also if there is no core)
    if (!ptyArcParams->bDoNotIssueTAPReset) {
        // 1st step - reset TAPs using TMS sequence
        if (DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle, 0) != DRVOPXD_ERROR_NO_ERROR)
            iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
        if (iResult != ERR_ML_ARC_NO_ERROR) {
            return ERR_ML_ARC_TEST_SCANCHAIN_FAILED;
        }
    }

    // 2nd step - disable MC
    (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, 0, NULL);

    // 3rd step - prepare patterns
    // define bypass pattern, 0xAA pattern and 0x55 pattern
    memset((void*)pulBypassValue, 0xff, ARC_TSTSC_WORDS * sizeof(uint32_t));
    memset((void*)pulAAPattern, 0xaa, ARC_TSTSC_WORDS * sizeof(uint32_t));
    memset((void*)pul55Pattern, 0x55, ARC_TSTSC_WORDS * sizeof(uint32_t));
    memset((void*)pulCCPattern, 0xcc, ARC_TSTSC_WORDS * sizeof(uint32_t));
    memset((void*)pul33Pattern, 0x33, ARC_TSTSC_WORDS * sizeof(uint32_t));
    memset((void*)pul00Pattern, 0x00, ARC_TSTSC_WORDS * sizeof(uint32_t));

    // first shift bypass to IR and shift 0's to DR, ignore this step
    (void)DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulBypassValue, NULL);
    (void)DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pul00Pattern, NULL);

    // shift bypass to IR (DR == 1 bit) and shift AA pattern to DR
    (void)DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulBypassValue, NULL);
    (void)DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulAAPattern, pulOutValue1);

    // shift bypass to IR (DR == 1 bit) and shift 55 pattern to DR
    (void)DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulBypassValue, NULL);
    (void)DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pul55Pattern, pulOutValue2);

    // shift bypass to IR (DR == 1 bit) and shift CC pattern to DR
    (void)DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulBypassValue, NULL);
    (void)DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulCCPattern, pulOutValue3);

    // shift bypass to IR (DR == 1 bit) and shift 33 pattern to DR
    (void)DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulBypassValue, NULL);
    (void)DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pul33Pattern, pulOutValue4);

    for (uiShiftIndex = 0; uiShiftIndex < ARC_TSTSC_BITS; uiShiftIndex++) {
        if (!CompareArrayOfWords(pulAAPattern, pulOutValue1, ARC_TSTSC_BITS - uiShiftIndex) &&
            !CompareArrayOfWords(pul55Pattern, pulOutValue2, ARC_TSTSC_BITS - uiShiftIndex) &&
            !CompareArrayOfWords(pulCCPattern, pulOutValue3, ARC_TSTSC_BITS - uiShiftIndex) &&
            !CompareArrayOfWords(pul33Pattern, pulOutValue4, ARC_TSTSC_BITS - uiShiftIndex)
            )
            // both patterns shifted by the same number of bits revealed on output
            bPatternMatch = TRUE;

        if (bVerifyNumberOfCores && (uiShiftIndex == ptyArcParams->ulNumberOfCoresOnScanchain))
            break;

        // shift all arrays
        ShiftArrayOfWordsRight(pulOutValue1, ARC_TSTSC_WORDS);
        ShiftArrayOfWordsRight(pulOutValue2, ARC_TSTSC_WORDS);
        ShiftArrayOfWordsRight(pulOutValue3, ARC_TSTSC_WORDS);
        ShiftArrayOfWordsRight(pulOutValue4, ARC_TSTSC_WORDS);
    }

    // 5th step - restore MC settings
    if (ptyArcParams->ulNumberOfCoresOnScanchain > 1) {  // more than 2 cores, we need to configure probe
        TyTAPConfig ptyScanchainConfig[MAX_ARC_CORES];
        uint32_t pulBypass[MAX_ARC_CORES];
        // create structure with core information
        for (ulIndex = 0; ulIndex < ptyArcParams->ulNumberOfCoresOnScanchain; ulIndex++) {
            pulBypass[ulIndex] = 0xFFFFFFFF;                                  // all 1's as bypass code
            ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyArcParams->tyScanchainCoreParams[ulIndex].ulIRLength);
            ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;               // does not matter
            ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(pulBypass[ulIndex]);    // bypass code
        }
        (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, ptyArcParams->ulNumberOfCoresOnScanchain, ptyScanchainConfig);
    } else
        (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, 0, NULL);

    if (!ptyArcParams->bDoNotIssueTAPReset) {
        // 6th step - reset TAPs using TMS sequence (always)
        (void)DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle, 0);

    }

    if (!bPatternMatch)
        iResult = ERR_ML_ARC_TEST_SCANCHAIN_FAILED;

    return iResult;
}

/****************************************************************************
     Function: ML_DetectScanchain
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               uint32_t uiMaxCores - maximum number of cores supported
               uint32_t *puiNumberOfCores - number of cores detected
               uint32_t *pulIRWidth - pointer to array with IR length for each core
               unsigned char *pbARCCores - pointer to array of boolean if particular core is ARC
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Detect scanchain structure and decide about number of cores on scanchain.
Date           Initials    Description
06-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_DetectScanchain(TyArcTargetParams *ptyArcParams, uint32_t uiMaxCores, uint32_t *puiNumberOfCores, 
                       uint32_t *pulIRWidth, unsigned char *pbARCCores)
{
    int32_t           iResult = ERR_ML_ARC_NO_ERROR;
    TyArcTargetType tyLocTargetOtion;
    TyARCTargetType tyLocARCTargetType;
    TyDeviceInfo  tyDeviceInfo;

    if ((ptyArcParams == NULL) || (puiNumberOfCores == NULL) || (pulIRWidth == NULL) || (pbARCCores == NULL) || (uiMaxCores == 0))
        return ERR_ML_ARC_INVALID_HANDLE;

    //take a copy of current target option
    tyLocTargetOtion = ptyArcParams->tyArcTargetType;

    //the auto detect scan chain only works with the correct TPA
    //detect the currently connected TPA and adjust the target option accordingly
    if (DL_OPXD_GetDeviceInfo(ptyArcParams->iCurrentHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
        return ERR_ML_ARC_DETECT_SCANCHAIN_FAILED;

    if (tyDeviceInfo.ulTpaRevision >= 0x01000000)
    {	//R1
        switch((unsigned char)tyLocTargetOtion)
        {
            case 0:  //ARC_TGT_ARCANGEL  (R0)
            case 1:  //ARC_TGT_NORMAL    (R0)
            case 2:  //ARC_TGT_RSTDETECT (R0)
            case 4:  //ARCT_JTAG_TPA_R1  (R1)
            default:
                ptyArcParams->tyArcTargetType = ARC_TGT_JTAG_TPA_R1;
                tyLocARCTargetType            = ARCT_JTAG_TPA_R1;
                break;
            case 3://ARC_TGT_CJTAG_TPA_R1 (R1)
                ptyArcParams->tyArcTargetType = ARC_TGT_CJTAG_TPA_R1;
                tyLocARCTargetType            = ARCT_CJTAG_TPA_R1;
                break;
        }
    }
    else
    {	//R0
        switch((unsigned char)tyLocTargetOtion)
        {
            case 0:  //ARC_TGT_ARCANGEL  (R0)
                ptyArcParams->tyArcTargetType = ARC_TGT_ARCANGEL;
                tyLocARCTargetType            = ARCT_ARCANGEL;
                break;
            case 1:  //ARC_TGT_NORMAL    (R0)
            case 3://ARC_TGT_CJTAG_TPA_R1 (R1)
            case 4:  //ARCT_JTAG_TPA_R1  (R1)
            default:
                ptyArcParams->tyArcTargetType = ARC_TGT_NORMAL;
                tyLocARCTargetType            = ARCT_NORMAL;
                break;
            case 2:  //ARC_TGT_RSTDETECT (R0)
                ptyArcParams->tyArcTargetType = ARC_TGT_RSTDETECT;
                tyLocARCTargetType            = ARCT_RSTDETECT;
                break;
        }
    }

    if (DL_OPXD_Arc_SelectTarget(ptyArcParams->iCurrentHandle, tyLocARCTargetType) != DRVOPXD_ERROR_NO_ERROR)
        return ERR_ML_ARC_DETECT_SCANCHAIN_FAILED;

    if(!ptyArcParams->bDoNotIssueTAPReset)
    {
        // 1st step - reset TAPs using TMS sequence
        if (DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle, 0) != DRVOPXD_ERROR_NO_ERROR)
            iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
    }
   
    // 2nd step - disable MC
    (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, 0, NULL);
   
    // 3rd step - shift patterns via IR and DR to determine number of cores and scanchain structure
    *puiNumberOfCores = 0;
    if (iResult == ERR_ML_ARC_NO_ERROR)
    {
        uint32_t ulIndex;
        TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
        uint32_t pulDRPatternOut1[ARC_TSTSC_WORDS];
        uint32_t pulDRPatternOut2[ARC_TSTSC_WORDS];
        uint32_t pulDRPatternIn1[ARC_TSTSC_WORDS];
        uint32_t pulDRPatternIn2[ARC_TSTSC_WORDS];
        uint32_t pulIRPatternIn[ARC_TSTSC_WORDS];

        // prepare test patterns
        for (ulIndex=0; ulIndex < ARC_TSTSC_WORDS; ulIndex++)
        {
            pulDRPatternOut1[ulIndex] = 0xAAAAAAAA; pulDRPatternOut2[ulIndex] = 0x55555555;
            pulDRPatternIn1[ulIndex]  = 0x0;        pulDRPatternIn2[ulIndex]  = 0x0;
            pulIRPatternIn[ulIndex]   = 0xFFFFFFFF;
        }
      
        // place cores into bypass and shift 2 test patterns via DR and finally get pattern from IR
        tyRes = DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, NULL, NULL);
        if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            tyRes = DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulDRPatternOut1, pulDRPatternIn1);
        if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            tyRes = DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, pulDRPatternOut2, pulDRPatternIn2);
        if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            tyRes = DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, 0, ARC_TSTSC_BITS, NULL, pulIRPatternIn);

        switch (tyRes)
        {
            case DRVOPXD_ERROR_NO_ERROR:        // no error, just go ahead
                break;
            case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
                iResult = ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED;
                break;
            default:                            // any error in driver
                iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
                break;
        }
      
        // determine number of cores from DR shifts
        if (iResult == ERR_ML_ARC_NO_ERROR)
        {  // verify results
            uint32_t ulNumberOfBits = ARC_TSTSC_BITS;
         
            // check if pattern matches (test up to supported number of cores)
            for (ulIndex=0; ulIndex < (uiMaxCores); ulIndex++)
            {
                if (!CompareArrayOfWords(pulDRPatternOut1, pulDRPatternIn1, ulNumberOfBits) &&
                    !CompareArrayOfWords(pulDRPatternOut2, pulDRPatternIn2, ulNumberOfBits)                  )
                {  // found pattern match
                    break;
                }

                ShiftArrayOfWordsRight(pulDRPatternIn1, ARC_TSTSC_WORDS);
                ShiftArrayOfWordsRight(pulDRPatternIn2, ARC_TSTSC_WORDS);
                ulNumberOfBits--;
                (*puiNumberOfCores)++;
            }
         
            if (!ulNumberOfBits)
                iResult = ERR_ML_ARC_DETECT_SCANCHAIN_FAILED;
        }
      
        // analyze patterns
        if (iResult == ERR_ML_ARC_NO_ERROR)
        {
            //to know the exact IR register length we would need to read the
            //ID register of each device, i.e. we assume they all are ARC processors
            if (   (*puiNumberOfCores == 0)
				|| (*puiNumberOfCores >= uiMaxCores))
                iResult = ERR_ML_ARC_DETECT_SCANCHAIN_FAILED;
            else
            {
                for (ulIndex = 0; ulIndex < *puiNumberOfCores; ulIndex++)
                {
                    pulIRWidth[ulIndex] = 4;
                    pbARCCores[ulIndex] = 1;
                }
            }
        }
    }
   
    // 4th step - restore MC settings
    if (ptyArcParams->ulNumberOfCoresOnScanchain > 1)
    {  // more than 2 cores, we need to configure probe
        TyTAPConfig ptyScanchainConfig[MAX_ARC_CORES];
        uint32_t pulBypass[MAX_ARC_CORES];
        uint32_t ulIndex;

        // create structure with core information
        for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfCoresOnScanchain; ulIndex++)
        {
            pulBypass[ulIndex] = 0xFFFFFFFF;                                  // all 1's as bypass code
            ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyArcParams->tyScanchainCoreParams[ulIndex].ulIRLength);
            ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;               // does not matter
            ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(pulBypass[ulIndex]);    // bypass code
        }

        (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, ptyArcParams->ulNumberOfCoresOnScanchain, ptyScanchainConfig);
    }
    else
        (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, 0, NULL);

    if(!ptyArcParams->bDoNotIssueTAPReset)
    {
        // 5th step - reset TAPs using TMS sequence (always)
        (void)DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle, 0);
    }

    return iResult;
}

/****************************************************************************
     Function: ML_UpdateScanchain
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Update scanchain settings (multicore) with new values
Date           Initials    Description
06-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_UpdateScanchain(TyArcTargetParams *ptyArcParams)
{
   uint32_t ulIndex;
   // check handle
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // update multicore settings based on number of cores and tyARCCoreParams
   ptyArcParams->ulCurrentARCCore = 0;
   ptyArcParams->ulNumberOfARCCores = 0;
   for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfCoresOnScanchain; ulIndex++)
   {
      if (ptyArcParams->tyScanchainCoreParams[ulIndex].bARCCore)
      {  // found next ARC core
         ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulScanchainCore = ulIndex;        // link ARC core with particular core on scanchain
         ptyArcParams->tyARCCoreParams[0].ulCoreVersion = 0x0;
         ptyArcParams->tyARCCoreParams[0].usAccessType = DEFAULT_ACCESS_TYPE;
         ptyArcParams->ulCurrentARCCore++;
      }
   }

   ptyArcParams->ulNumberOfARCCores = ptyArcParams->ulCurrentARCCore;
   ptyArcParams->ulCurrentARCCore = 0;                          // starting with first ARC core

   if (!ptyArcParams->ulNumberOfARCCores)
      {  // we could not find ARC core, in that case, use first core
      ptyArcParams->tyARCCoreParams[0].ulScanchainCore = 0;
      ptyArcParams->tyARCCoreParams[0].ulCoreVersion = 0x0;
      ptyArcParams->tyARCCoreParams[0].usAccessType = DEFAULT_ACCESS_TYPE;
      ptyArcParams->ulNumberOfARCCores = 1;
      }
   // update multicore config in Opella-XD
   if (ptyArcParams->ulNumberOfCoresOnScanchain > 1)
      {  // more than 2 cores, we need to configure probe
      TyTAPConfig ptyScanchainConfig[MAX_ARC_CORES];
      uint32_t pulBypass[MAX_ARC_CORES];
      // create structure with core information
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfCoresOnScanchain; ulIndex++)
         {
         pulBypass[ulIndex] = 0xFFFFFFFF;                                              // all 1's as bypass code
         ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyArcParams->tyScanchainCoreParams[ulIndex].ulIRLength);
         ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;                           // does not matter
         ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(pulBypass[ulIndex]);       // bypass code
         }
      (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, ptyArcParams->ulNumberOfCoresOnScanchain, ptyScanchainConfig);
      }
   else
      (void)DL_OPXD_JtagConfigMulticore(ptyArcParams->iCurrentHandle, 0, NULL);        // do not set MC support, we have just 1 core

   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetJtagFrequency
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               double dFrequency - frequency in MHz to set
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Set fixed frequency from 10kHz to 100MHz or adaptive JTAG clock.
Date           Initials    Description
08-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_SetJtagFrequency(TyArcTargetParams *ptyArcParams, double dFrequency)
{
   int32_t iResult = ERR_ML_ARC_NO_ERROR;
   TyDeviceInfo tyLocDeviceInfo;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;

   if ((dFrequency > MAX_ARC_JTAG_AND_BLAST_FREQUENCY) || (dFrequency < MIN_ARC_JTAG_AND_BLAST_FREQUENCY))
      return ERR_ML_ARC_INVALID_FREQUENCY;

   //Opella-XD R2 FW/DW still dont support CMD_CODE_DISKWARE_STATUS command, so dont use that for R2
   if (DL_OPXD_GetDeviceInfo(ptyArcParams->iCurrentHandle, &tyLocDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
   {
	   (void)DL_OPXD_CloseDevice(ptyArcParams->iCurrentHandle);
	   return ERR_ML_ARC_USB_DRIVER_ERROR;
   }

   if (tyLocDeviceInfo.ulBoardRevision < 0x03000000)
   {
	   tyLocDeviceInfo.uiActiveDiskwares = 0;
	   tyLocDeviceInfo.bOtherDiskwareRunning = 0;
   }
   else
   {
	   //Check if diskware is already running (could be that its loaded by another instance of GDB server)
	   if (DL_OPXD_GetDiskwareStatus(ptyArcParams->iCurrentHandle, &tyLocDeviceInfo, ARC) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice(ptyArcParams->iCurrentHandle);
		   return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }
   }

   if ((tyLocDeviceInfo.uiActiveDiskwares > 1) || tyLocDeviceInfo.bOtherDiskwareRunning)
   {
	   //Some diskware is running, so dont change JTAG freq now!
	   return ERR_ML_ARC_NO_ERROR;
   }

   // frequency is correct range so we can store frequency value and try to set clock in Opella-XD probe
   if (ptyArcParams->bAdaptiveClockEnabled)
      {  // adaptive clock is being used, so use frequency as alternative
      if (DL_OPXD_JtagSetAdaptiveClock(ptyArcParams->iCurrentHandle, ptyArcParams->ulAdaptiveClockTimeout, dFrequency) != DRVOPXD_ERROR_NO_ERROR)
         iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
      else
         ptyArcParams->dJtagFrequency = dFrequency;               // update frequency in local variable
      }
   else
      {
      if (DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, dFrequency, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
         iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
      else
         ptyArcParams->dJtagFrequency = dFrequency;               // update frequency in local variable
      }
   return iResult;
}
/****************************************************************************
     Function: ML_IntializeCJTAG
     Engineer: Jeenus C K
        Input: TyArcTargetParams *ptyArcParams
       Output: none
  Description: Initialize CJTAG
Date           Initials    Description
15-Sep-2014     JCK          Initial
17-Nov-2014     RR           Modified for Opella-XD
****************************************************************************/
int32_t ML_IntializeCJTAG(TyArcTargetParams *ptyArcParams)
{
    TyError tyRes = DRVOPXD_ERROR_NO_ERROR;

    if (ptyArcParams->tyArcTargetType == ARC_TGT_CJTAG_TPA_R1)
    {  
        tyRes = DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle,FALSE);
        if(tyRes == DRVOPXD_ERROR_NO_ERROR)
            {
           /* FILE *fpCJTGA = fopen ("CJTAG.txt","w");
            if (fpCJTGA)
                {
                fprintf (fpCJTGA,"Before DL_OPXD_cJtagInitTap7Controller");
                fclose (fpCJTGA);
                }*/
            tyRes = DL_OPXD_cJtagInitTap7Controller(ptyArcParams->iCurrentHandle);
            }

        if (tyRes != DRVOPXD_ERROR_NO_ERROR)
        {
			(void)DL_OPXD_TerminateDW(ptyArcParams->iCurrentHandle, ARC);
            (void)DL_OPXD_UnconfigureDevice(ptyArcParams->iCurrentHandle);
            return ERR_ML_ARC_PROBE_CONFIG_ERROR;
        }
    }

    return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_UsingAdaptiveJtagClock
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: unsigned char - TRUE if adaptive clock is being used, FALSE otherwise
  Description: is adaptive clock currently being used
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
unsigned char ML_UsingAdaptiveJtagClock(TyArcTargetParams *ptyArcParams)
{
   if ((ptyArcParams != NULL) && (ptyArcParams->bAdaptiveClockEnabled))
      return TRUE;
   return FALSE;
}

/****************************************************************************
     Function: ML_ToggleAdaptiveJtagClock
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: unsigned char - TRUE if adaptive clock is being used, FALSE otherwise
  Description: is adaptive clock currently being used
Date           Initials    Description
06-Nov-2007    VH          Initial
****************************************************************************/
int32_t ML_ToggleAdaptiveJtagClock(TyArcTargetParams *ptyArcParams, unsigned char bEnableAdaptiveClock)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // check if this feature is supported for current target
   if ((ptyArcParams->tyArcTargetType != ARC_TGT_NORMAL) && (ptyArcParams->tyArcTargetType != ARC_TGT_RSTDETECT))
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;
   // check if we need to do some action
   if ((ptyArcParams->bAdaptiveClockEnabled && bEnableAdaptiveClock) ||
       (!ptyArcParams->bAdaptiveClockEnabled && !bEnableAdaptiveClock))
      return ERR_ML_ARC_NO_ERROR;                              // nothing to do
   // we need to change 
   if (bEnableAdaptiveClock)
      {  // switch to adaptive clock
      (void)DL_OPXD_JtagSetAdaptiveClock(ptyArcParams->iCurrentHandle, ptyArcParams->ulAdaptiveClockTimeout, ptyArcParams->dJtagFrequency);
      }
   else
      {  // switch to fixed clock
      (void)DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, ptyArcParams->dJtagFrequency, NULL, NULL);
      }
   ptyArcParams->bAdaptiveClockEnabled = bEnableAdaptiveClock;
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetBlastFrequency
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               double dFrequency - frequency in MHz to set
               unsigned char bUseAdaptiveClocking - using adaptive or fixed JTAG frequency
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Set blast frequency frequency from 10kHz to 100MHz.
Date           Initials    Description
08-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_SetBlastFrequency(TyArcTargetParams *ptyArcParams, double dFrequency)
{
   int32_t iResult = ERR_ML_ARC_NO_ERROR;
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if ((dFrequency > MAX_ARC_JTAG_AND_BLAST_FREQUENCY) || (dFrequency < MIN_ARC_JTAG_AND_BLAST_FREQUENCY))
      return ERR_ML_ARC_INVALID_FREQUENCY;
   // frequency is correct range, so just update variable (we will use this value when blasting FPGA)
   ptyArcParams->dBlastFrequency = dFrequency;
   return iResult;
}

/****************************************************************************
     Function: ML_WriteBlock
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               TyArcAccessType tyAccessType - ARC core access type
               uint32_t ulAddress - address to write
               uint32_t ulCount - number of words to write (max. 0x4000 of words)
               uint32_t *pulData - pointer to data
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Write block of data from ARC core.
Date           Initials    Description
11-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_WriteBlock(TyArcTargetParams *ptyArcParams, TyArcAccessType tyAccessType, uint32_t ulAddress, uint32_t ulCount, uint32_t *pulData)
{
   TyError        tyRes = DRVOPXD_ERROR_NO_ERROR;
   int32_t            iResult = ERR_ML_ARC_NO_ERROR;
   uint32_t  ulSize, ulOffset;
   unsigned short usAccessType = ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType & ~ARC_ACCESS_TYPE_MASK;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // set proper access type
   switch (tyAccessType)
      {
      case ML_ARC_ACCESS_MEMORY:
         usAccessType |= ARC_ACCESS_TYPE_MEMORY;
         break;
      case ML_ARC_ACCESS_CORE:
         usAccessType |= ARC_ACCESS_TYPE_COREREG;
         break;
      case ML_ARC_ACCESS_AUX:
         usAccessType |= ARC_ACCESS_TYPE_AUXREG;
         break;
      case ML_ARC_ACCESS_MADI:
      default:
         usAccessType |= ARC_ACCESS_TYPE_MADI;
         break;
      }

   ulOffset = 0;

   do
   {
      if (ulCount >= (BUFFER_PAYLOAD / 4))
         ulSize = (BUFFER_PAYLOAD / 4);
      else
         ulSize = ulCount;

      // call diskware function
      tyRes = DL_OPXD_Arc_WriteBlock(ptyArcParams->iCurrentHandle,                                                      // handle to probe
                                     ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulScanchainCore,     // core number
                                     usAccessType, ulAddress, ulSize, (uint32_t*)&pulData[ulOffset]);              // other params
      ulAddress += ulSize * 4;
      ulOffset  += ulSize;
      ulCount   -= ulSize;

   }while (   (ulCount > 0)
           && (tyRes   == DRVOPXD_ERROR_NO_ERROR));

   // check what caused memory write failing
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARC_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED;
         break;
      case DRVOPXD_ERROR_ARC_DEBUG_ACCESS:
         iResult = ERR_ML_ARC_DEBUG_ACCESS;
         break;
      case DRVOPXD_ERROR_ARC_DEBUG_PDOWN:
         iResult = ERR_ML_ARC_DEBUG_PDOWN;
         break;
      case DRVOPXD_ERROR_ARC_DEBUG_POFF:
         iResult = ERR_ML_ARC_DEBUG_POFF;
         break;
      case DRVOPXD_ERROR_ARC_TARGET_RESET_OCCURED:
         iResult = ERR_ML_ARC_TARGET_RESET_DETECTED;
         break;
      default:
         iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
         break;
      }
   ptyArcParams->iLastErrorCode = iResult;
   return (ptyArcParams->iLastErrorCode);
}

/****************************************************************************
     Function: ML_ReadBlock
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               TyArcAccessType tyAccessType - ARC core access type
               uint32_t ulAddress - address to read
               uint32_t ulCount - number of words to read (max. 0x4000 of words)
               uint32_t *pulData - pointer to data
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Read block of data from ARC core.
Date           Initials    Description
11-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_ReadBlock(TyArcTargetParams *ptyArcParams, TyArcAccessType tyAccessType, 
                 uint32_t ulAddress, uint32_t ulCount, uint32_t *pulData)
{
   TyError        tyRes = DRVOPXD_ERROR_NO_ERROR;
   int32_t            iResult = ERR_ML_ARC_NO_ERROR;
   uint32_t  ulSize, ulOffset;
   unsigned short usAccessType = ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType & ~ARC_ACCESS_TYPE_MASK;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // set proper access type
   switch (tyAccessType)
      {
      case ML_ARC_ACCESS_MEMORY:
         usAccessType |= ARC_ACCESS_TYPE_MEMORY;
         break;
      case ML_ARC_ACCESS_CORE:
         usAccessType |= ARC_ACCESS_TYPE_COREREG;
         break;
      case ML_ARC_ACCESS_AUX:
         usAccessType |= ARC_ACCESS_TYPE_AUXREG;
         break;
      case ML_ARC_ACCESS_MADI:
      default:
         usAccessType |= ARC_ACCESS_TYPE_MADI;
         break;
      }

   ulOffset = 0;

   do
   {
      if (ulCount >= (BUFFER_PAYLOAD / 4))
         ulSize = (BUFFER_PAYLOAD / 4);
      else
         ulSize = ulCount;

      
      // call diskware function
      tyRes = DL_OPXD_Arc_ReadBlock(ptyArcParams->iCurrentHandle, 
                                    ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulScanchainCore, 
                                    usAccessType, ulAddress, ulSize, (uint32_t*)&pulData[ulOffset]);

      ulAddress += ulSize * 4;
      ulOffset  += ulSize;
      ulCount   -= ulSize;

   }while (   (ulCount > 0)
           && (tyRes   == DRVOPXD_ERROR_NO_ERROR));

   // check what caused memory read failing
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARC_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED;
         break;
      case DRVOPXD_ERROR_ARC_DEBUG_ACCESS:
         iResult = ERR_ML_ARC_DEBUG_ACCESS;
         break;
      case DRVOPXD_ERROR_ARC_TARGET_RESET_OCCURED:
         iResult = ERR_ML_ARC_TARGET_RESET_DETECTED;
         break;
      case DRVOPXD_ERROR_ARC_DEBUG_PDOWN:
         iResult = ERR_ML_ARC_DEBUG_PDOWN;
         break;
      case DRVOPXD_ERROR_ARC_DEBUG_POFF:
         iResult = ERR_ML_ARC_DEBUG_POFF;
         break;
      default:
         iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
         break;
      }
   ptyArcParams->iLastErrorCode = iResult;
   return (ptyArcParams->iLastErrorCode);
}

/****************************************************************************
     Function: ML_BlastFileToFpga
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               const char *pszXbfFilename - XBF filename to blast
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Blast FPGA in ARCangel target from XBF file.
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_BlastFileToFpga(TyArcTargetParams *ptyArcParams, const char *pszXbfFilename)
{
   TyARCangelMode tyAAPins;
   uint32_t ulProgressLength, ulBytesRead;
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   unsigned char ulBlastingSuccessful = 1;
   FILE *fp = NULL;
   int32_t iResult = ERR_ML_ARC_NO_ERROR;
   // check parameters and current target type
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;
   // so lets start blasting ARCangel target from file
   fp = fopen(pszXbfFilename, "rb");
   if (fp == NULL)
      return ERR_ML_ARC_ANGEL_BLASTING_FAILED;
   // because same clock is used for blasting and JTAG engine, we must set Opella-XD clock to blasting frequency before
   // and restore to JTAG clock after blasting FPGA
   // ARCangel does not support adaptive clock so do not bother with that and assume only fixed clock can be used here
   if (DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, ptyArcParams->dBlastFrequency, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   // prepare for blasting (enable D0 and D1)
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 1; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   // initialize FPGA by settings SS0 = SS1 = CNT = 0
   tyAAPins.bSS0 = 0;   tyAAPins.bSS1 = 0; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   // set download FPGA data mode SS0 = 1 and SS1 = CNT = 0 and check OP
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 0; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, NULL, &tyAAPins,  1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   else if (tyAAPins.bOP)
      ulBlastingSuccessful = 0;              // OP must be low
   // now we must wait 200 ms
   ML_Sleep(ptyArcParams, 200);
   // start blasting
   ulProgressLength = 0;
   // read data from file and blast them into FPGA
   while ((ulBytesRead = (uint32_t)fread(pucBlastingBuffer, 1, ARC_BLASTING_BUFFER_LEN, fp)) != 0)
      {
      ulProgressLength += ulBytesRead;
      if (ulProgressLength >= (64*1024))
         {  // update progress bar after every 64 kB
         if (ptyArcParams->pfProgressBarFunc != NULL)
            ptyArcParams->pfProgressBarFunc();
         ulProgressLength = 0;
         }
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_Arc_AABlast(ptyArcParams->iCurrentHandle, ulBytesRead*8, pucBlastingBuffer);    // send next buffer of bits to probe
      }
   // we are finished, check result and also check OP, must be high after successful blasting
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, NULL, &tyAAPins,  1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   else if (!tyAAPins.bOP)
      ulBlastingSuccessful = 0;
   // return to normal mode and disable blasting (always ensure ending in normal mode, even if error happens)
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 1; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 0) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   if (fp != NULL)
      fclose(fp);
   if ((iResult == ERR_ML_ARC_NO_ERROR) && !ulBlastingSuccessful)
      iResult = ERR_ML_ARC_ANGEL_BLASTING_FAILED;
   // restore frequency settings for target
   (void)DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, ptyArcParams->dJtagFrequency, NULL, NULL);
   // we need also restore PLL settings
   if ((ptyArcParams->tyAAClockAlloc.bRestoreAfterBlasting) && (iResult == ERR_ML_ARC_NO_ERROR))
      iResult = ML_AASetClockConfiguration(ptyArcParams);
   // return result
   return iResult;
}

/****************************************************************************
     Function: ML_BlastMemToFpga
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               const char *pszMemDetails - memory details
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: Blast FPGA in ARCangel target from memory.
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_BlastMemToFpga(TyArcTargetParams *ptyArcParams, const char *pszMemDetails)
{
   TyARCangelMode tyAAPins;
   uint32_t ulProgressLength, ulBytesRead;
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   unsigned char ulBlastingSuccessful = 1;
   unsigned long ulBlastAddress; // 32bit/64bit aware
   uint32_t ulBlastLength;
   char szFileInfo[_MAX_PATH];
   int32_t iResult = ERR_ML_ARC_NO_ERROR;

   // check parameters and current target type
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;
   // obtain details from parameter
   sscanf(pszMemDetails, "%lX:%X:%s", &ulBlastAddress, &ulBlastLength, szFileInfo);      //lint !e566 !e412 !e561
   // because same clock is used for blasting and JTAG engine, we must set Opella-XD clock to blasting frequency before
   // and restore to JTAG clock after blasting FPGA
   // ARCangel does not support adaptive clock so do not bother with that and assume only fixed clock can be used here
   if (DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, ptyArcParams->dBlastFrequency, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   // prepare for blasting (enable D0 and D1)
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 1; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   // initialize FPGA by settings SS0 = SS1 = CNT = 0
   tyAAPins.bSS0 = 0;   tyAAPins.bSS1 = 0; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   // set download FPGA data mode SS0 = 1 and SS1 = CNT = 0 and check OP
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 0; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, NULL, &tyAAPins,  1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   else if (tyAAPins.bOP)
      ulBlastingSuccessful = 0;              // OP must be low
   // now we must wait 200 ms
   ML_Sleep(ptyArcParams, 200);
   // start blasting
   ulProgressLength = 0;
   // read data from file and blast them into FPGA
   while (ulBlastLength)
      {
      ulBytesRead = ulBlastLength;
      if (ulBytesRead > ARC_BLASTING_BUFFER_LEN)
         ulBytesRead = ARC_BLASTING_BUFFER_LEN;
      ulBlastLength -= ulBytesRead;
      memcpy(pucBlastingBuffer, (void *)(ulBlastAddress), ulBytesRead);
      ulBlastAddress += ulBytesRead;
      ulProgressLength += ulBytesRead;
      if (ulProgressLength >= (64*1024))
         {  // update progress bar after every 64 kB
         if (ptyArcParams->pfProgressBarFunc != NULL)
            ptyArcParams->pfProgressBarFunc();
         ulProgressLength = 0;
         }
      if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         tyRes = DL_OPXD_Arc_AABlast(ptyArcParams->iCurrentHandle, ulBytesRead*8, pucBlastingBuffer);    // send next buffer of bits to probe
      }
   // we are finished, check result and also check OP, must be high after successful blasting
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, NULL, &tyAAPins,  1) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   else if (!tyAAPins.bOP)
      ulBlastingSuccessful = 0;
   // return to normal mode and disable blasting (always ensure ending in normal mode, even if error happens)
   tyAAPins.bSS0 = 1;   tyAAPins.bSS1 = 1; tyAAPins.bCNT = 0;
   if (DL_OPXD_Arc_AAMode(ptyArcParams->iCurrentHandle, &tyAAPins, NULL, 0) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   if ((iResult == ERR_ML_ARC_NO_ERROR) && !ulBlastingSuccessful)
      iResult = ERR_ML_ARC_ANGEL_BLASTING_FAILED;
   // restore frequency settings for target
   (void)DL_OPXD_JtagSetFrequency(ptyArcParams->iCurrentHandle, ptyArcParams->dJtagFrequency, NULL, NULL);
   // we need also restore PLL settings
   if ((ptyArcParams->tyAAClockAlloc.bRestoreAfterBlasting) && (iResult == ERR_ML_ARC_NO_ERROR))
      iResult = ML_AASetClockConfiguration(ptyArcParams);
   // return result
   return iResult;
}

/****************************************************************************
     Function: ML_AAExtendedCommand
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char ucCommand - extended command code
               uint32_t ulParameter - parameter for extended command
               unsigned short *pusResponse - pointer to response
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: send extended command into ARCangel4 and show response
               parameter is 16-bit value except for PLL (command 0x04) where parameter
               is 32-bit value (lowest 21 bits as PLL value and next 3 bits as PLL address)
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_AAExtendedCommand(TyArcTargetParams *ptyArcParams, unsigned char ucCommand, uint32_t ulParameter, unsigned short *pusResponse)
{
   TyARCangelExtcmd tyExtCmd;
   uint32_t pulLocParameters[2];
   int32_t iResult = ERR_ML_ARC_NO_ERROR;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (pusResponse != NULL)
      *pusResponse = 0;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)                  // supported only for ARCangel
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;
   // make decision based on command code
   if (ucCommand != 0x4)
      {     // not PLL command, just pass to diskware
      pulLocParameters[0] = ulParameter & 0x0000FFFF;                      // use only 16 bits from parameter
      pulLocParameters[1] = 0;
      }
   else
      {     // this is PLL command so we need to encode PLL stream
      int32_t iBit;
      uint32_t ulEncMask0 = 0x00004000;          // encoded 0 at bits 14 and 15
      uint32_t ulEncMask1 = 0x00008000;          // encoded 1 at bits 14 and 15
      // initialize PLL stream
      // PLL stream has 64-bits with following layout
      // bit number
      // bit            63 62 61 60 59 58 57 56 55 54 53 52 51 50 49 48 47 46 45 44 43 42 41 40 39 38 37 36 35 34 33 32
      // value           1  1  a  a  a  a  a  a  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v
      // description    |stop|| encoded        ||               encoded PLL value                                      |
      //                |bits|| address        ||               2nd part                                               |
      // bit            31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
      // value           v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  v  0  0  0  1  1  1  1  1  1  1  1  1  1  1
      // description    |              encoded PLL value                     ||      unlock code (14 bits)             |
      //                |              1st part                              ||  first 11 bits as 1 followed by 3 zeros|
      // encoded parts uses Manchester coding scheme where bit 0 is represented as "01" and bit 1 as "10"
      pulLocParameters[0] = 0x000007FF;                     // initialized with unlock code
      pulLocParameters[1] = 0xC0000000;                     // initialized with stop bits
      ulParameter &= 0x00FFFFFF;                            // mask lowest 24 bits (PLL value and address)
      // first encode 9 lower bits from PLL value (finish LocParameters[0])
      for (iBit=0; iBit < 9; iBit++)
         {
         if (ulParameter & 0x1)
            pulLocParameters[0] |= ulEncMask1;           // added encoded bit 1
         else
            pulLocParameters[0] |= ulEncMask0;           // added encoded bit 0
         ulEncMask0 <<= 2;    ulEncMask1 <<= 2;          // shift encoding patterns and value
         ulParameter >>= 1;
         }
      // now do same for remaining 15 bits and pulLocStream[1]
      ulEncMask0 = 0x00000001;                           // encoded 0 at bits 0 and 1
      ulEncMask1 = 0x00000002;                           // encoded 1 at bits 0 and 1
      for (iBit=0; iBit < 15; iBit++)
         {
         if (ulParameter & 0x1)
            pulLocParameters[1] |= ulEncMask1;           // added encoded bit 1
         else
            pulLocParameters[1] |= ulEncMask0;           // added encoded bit 0
         ulEncMask0 <<= 2;    ulEncMask1 <<= 2;          // shift encoding patterns and value
         ulParameter >>= 1;
         }
      }
   // initialize structure for extended command
   tyExtCmd.ucCommandCode = ucCommand;
   tyExtCmd.pulParameter[0] = pulLocParameters[0];    tyExtCmd.pulParameter[1] = pulLocParameters[1];
   tyExtCmd.usResponse = 0x0;
   if (DL_OPXD_Arc_AAExtendedCommand(ptyArcParams->iCurrentHandle, &tyExtCmd) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   else
      {
      if (pusResponse != NULL)
         *pusResponse = tyExtCmd.usResponse;             // extended command successful, get its response
      }
   return iResult;
}

/****************************************************************************
     Function: ML_AASetRoboClock
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char ucCommand - command identifying type of robo clock
               unsigned short usValue - value for robo clock
               unsigned short *pusResponse - pointer to response (can be NULL)
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set robo clock settings in ARCangel4 target
               type of clock depends on ucCommand (0x6 for roboSDRAM, 
               0x7 for roboSSRAM and 0x8 for roboSSRAMEXP)
Date           Initials    Description
18-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_AASetRoboClock(TyArcTargetParams *ptyArcParams, unsigned char ucCommand, unsigned short usValue, unsigned short *pusResponse)
{
   unsigned short usResponse = 0;
   int32_t iRes = ERR_ML_ARC_NO_ERROR;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)                  // supported only for ARCangel
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;

   // send extended command and compare expended response for each of them
   switch (ucCommand)
      {
      case 0x6:      // roboSDRAM
         iRes = ML_AAExtendedCommand(ptyArcParams, 0x6, (uint32_t)usValue, &usResponse);
         if ((iRes == ERR_ML_ARC_NO_ERROR) && (usResponse != 0xF5F5))      // expecting response 0xF5F5 to command 0x6               
            iRes = ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE;
         break;
      case 0x7:      // roboSSRAM
         iRes = ML_AAExtendedCommand(ptyArcParams, 0x7, (uint32_t)usValue, &usResponse);
         if ((iRes == ERR_ML_ARC_NO_ERROR) && (usResponse != 0x0505))      // expecting response 0x0505 to command 0x7
            iRes = ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE;
         break;
      case 0x8:      // roboSSRAMEXP
         iRes = ML_AAExtendedCommand(ptyArcParams, 0x8, (uint32_t)usValue, &usResponse);
         if ((iRes == ERR_ML_ARC_NO_ERROR) && (usResponse != 0xA0A0))      // expecting response 0xA0A0 to command 0x8               
            iRes = ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE;
         break;
      default:
         break;
      }
   if (pusResponse != NULL)
      *pusResponse = usResponse;
   return iRes;
}

/****************************************************************************
     Function: ML_AAGetClockAllocation
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               int32_t iGclkSel - clock selection (use AA_GCLK(x)) constants
               TyArcAAGclk *ptyGclkRoute - pointer to type of clock source (AA_GCLK_xx)
               double *pdPllFrequency - pointer to response (can be NULL)
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: get information about routing for particular GCLK 
               (only ARCangel4 target supports it)
Date           Initials    Description
18-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_AAGetClockAllocation(TyArcTargetParams *ptyArcParams, int32_t iGclkSel, TyArcAAGclk *ptyGclkRoute, double *pdPllFrequency)
{
   TyArcAAGclk tyLocArcAAGclk = AA_GCLK_HIGHIMP;
   double dLocFrequency = 0.0;

   switch (iGclkSel)
      {
      case AA_GCLK(0):
      case AA_GCLK(1):
      case AA_GCLK(2):
      case AA_GCLK(3):
         tyLocArcAAGclk = ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkSel];                 // get clock source
         // if using PLL source, also obtain proper frequency in MHz
         if (tyLocArcAAGclk == AA_GCLK_MCLK)                   
            dLocFrequency = ptyArcParams->tyAAClockAlloc.dMclkFrequency;
         else if (tyLocArcAAGclk == AA_GCLK_VCLK)
            dLocFrequency = ptyArcParams->tyAAClockAlloc.dVclkFrequency;
         break;
      default:
         break;
      }
   // pass info to caller
   if (ptyGclkRoute != NULL)
      *ptyGclkRoute = tyLocArcAAGclk;
   if (pdPllFrequency != NULL)
      *pdPllFrequency = dLocFrequency;
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_AACreatePllStream
     Engineer: Vitezslav Hola
        Input: TyArcAAGclk tyClkSource - clock source
               double dFrequency - requested frequency in MHz
               uint32_t *pulPllValue - pointer to PLL value
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: generate bit stream for settings given source of PLL synthesizer
Date           Initials    Description
18-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_AACreatePllStream(TyArcAAGclk tyClkSource, double dFrequency, uint32_t *pulPllValue)
{
   uint32_t ulPllValue = 0;
   int32_t iRes = ERR_ML_ARC_NO_ERROR;

   if (((tyClkSource == AA_GCLK_MCLK) && ((dFrequency < AA_PLL_MIN_MCLK_FREQ) || (dFrequency > AA_PLL_MAX_MCLK_FREQ))) ||
       ((tyClkSource == AA_GCLK_VCLK) && ((dFrequency < AA_PLL_MIN_VCLK_FREQ) || (dFrequency > AA_PLL_MAX_VCLK_FREQ))))
      iRes = ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY;                 // frequency out of range
   else
      {     // frequency in range, try to find optimum PLL settings
      int32_t iIndex, iPValue, iQValue, iDValue, iTrialP, iTrialQ, iFirstQ, iLastQ;
      double dPOverQ, dMinDelta, dMinVcoFreq, dMaxVcoFreq, dVcoFreq;
      double pdVcoPresetBoundaries[AA_PLL_VCO_PRESET];
      double dPllRefFreq = AA_PLL_REF_FREQ;

      if (tyClkSource == AA_GCLK_MCLK)
         {
         dMinVcoFreq = AA_PLL_MIN_MVCO_FREQ;      dMaxVcoFreq = AA_PLL_MAX_MVCO_FREQ;
         }
      else
         {
         dMinVcoFreq = AA_PLL_MIN_VVCO_FREQ;      dMaxVcoFreq = AA_PLL_MAX_VVCO_FREQ;
         }
      // first try to find VCO inside its limit (we need proper D ratio)
      iDValue = 0;      dVcoFreq = dFrequency;
      while ((dVcoFreq < dMinVcoFreq) && (iDValue < AA_PLL_MAX_D))
         {
         dVcoFreq *= 2;       iDValue++;        // VCO is divided by 2^D
         }
      // check we have found VCO freq
      if ((dVcoFreq < dMinVcoFreq) || (dVcoFreq > dMaxVcoFreq))
         return ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY;
      // calculate ration needed for p/q to get VCO frequency from reference frequency
      dPOverQ = dVcoFreq / (dPllRefFreq * 2.0);
      // so lets try to find p and q to minimize frequency error
      dMinDelta = 1.0;     iPValue = 0;      iQValue = 0;
      iFirstQ = max((int32_t)((dPllRefFreq / (double)AA_PLL_MAX_REFOVERQ) + 0.999999), AA_PLL_MIN_Q);
      iLastQ = min((int32_t)((dPllRefFreq / (double)AA_PLL_MIN_REFOVERQ)), AA_PLL_MAX_Q);
      for (iTrialQ = iFirstQ; iTrialQ <= iLastQ; iTrialQ++)
         {
         double dDelta, dTemp;
         // calculate value P needed for current Q
         dTemp = dPOverQ * (double)iTrialQ;
         iTrialP = (int32_t)(dTemp + 0.5);                            // round estimated P
         if (iTrialP < AA_PLL_MIN_P)                              // P is too small, try next iteration
            continue;
         if (iTrialP > AA_PLL_MAX_P)                              // P is too big, other iterations would be useless
            break;
         // check what is P error for current ratios (get absolute error)
         dDelta = 1.0 - ((double)iTrialP / dTemp);
         if (dDelta < 0.0)
            dDelta *= (-1.0);
         if (dDelta < dMinDelta)
            {
            dMinDelta = dDelta;
            iPValue = iTrialP;
            iQValue = iTrialQ;
            }
         }
      // now we need to look for suitable Index (VCO preset)
      pdVcoPresetBoundaries[0] = 51.0;    pdVcoPresetBoundaries[1] = 53.2;    pdVcoPresetBoundaries[2] = 58.5;    
      pdVcoPresetBoundaries[3] = 60.7;    pdVcoPresetBoundaries[4] = 64.4;    pdVcoPresetBoundaries[5] = 66.8;    
      pdVcoPresetBoundaries[6] = 73.5;    pdVcoPresetBoundaries[7] = 75.6;    pdVcoPresetBoundaries[8] = 80.9;    
      pdVcoPresetBoundaries[9] = 83.2;    pdVcoPresetBoundaries[10] = 91.5;   pdVcoPresetBoundaries[11] = 100.0;  
      pdVcoPresetBoundaries[12] = 120.0;
      iIndex = 0;
      for (; iIndex < AA_PLL_VCO_PRESET; iIndex++)
         {
         if (pdVcoPresetBoundaries[iIndex] > dVcoFreq)
            break;
         }
      // check index value
      if (iIndex >= AA_PLL_VCO_PRESET)
         iIndex = (AA_PLL_VCO_PRESET - 1);

      // now assemble PLL value
      ulPllValue = (uint32_t)iIndex & 0x0000000F;                                 // highest 4 bits represent index
      ulPllValue = (ulPllValue << 7) | ((uint32_t)(iPValue - 3) & 0x0000007F);    // next 7 bits represent P value
      ulPllValue = (ulPllValue << 3) | ((uint32_t)iDValue & 0x00000007);          // next 3 bits represent divider D
      ulPllValue = (ulPllValue << 7) | ((uint32_t)(iQValue - 2) & 0x0000007F);    // lowest 7 bits represent Q value
      // done, we got 21 bits PLL value
      }

   // add PLL address to PLL value
   if (tyClkSource == AA_GCLK_MCLK)
      ulPllValue |= 0x00600000;                       // using MCLK source, PLL address is 0x3 in bits 21 to 24
   else
      ulPllValue |= 0x00000000;                       // using VCLK source, PLL address is 0x0 in bits 21 to 24
   if (pulPllValue != NULL)
      *pulPllValue = ulPllValue;                      // assign result
   return iRes;
}

/****************************************************************************
     Function: ML_AASetClockConfiguration
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set ARCangel clock configuration to the target
Date           Initials    Description
18-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_AASetClockConfiguration(TyArcTargetParams *ptyArcParams)
{
   int32_t iGclkIndex;
   uint32_t ulRouteReg;
   unsigned short usResponse = 0;
   int32_t iRes = ERR_ML_ARC_NO_ERROR;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_NO_ERROR;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;

   // get value for routing register
   ulRouteReg = 0;
   for (iGclkIndex=0; iGclkIndex < 4; iGclkIndex++)
      {
      switch (ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkIndex])
         {
         case AA_GCLK_CRYSTAL: 
            ulRouteReg |= (AA_GCLK_BITS_CRYSTAL << (iGclkIndex * 3));
            break;
         case AA_GCLK_HOST:
            ulRouteReg |= (AA_GCLK_BITS_HOST << (iGclkIndex * 3));
            break;
         case AA_GCLK_DIPS:
            ulRouteReg |= (AA_GCLK_BITS_DIPS << (iGclkIndex * 3));
            break;
         case AA_GCLK_MCLK:
            ulRouteReg |= (AA_GCLK_BITS_PLLMCLK << (iGclkIndex * 3));
            break;
         case AA_GCLK_VCLK:
            ulRouteReg |= (AA_GCLK_BITS_PLLVCLK << (iGclkIndex * 3));
            break;
         case AA_GCLK_HIGHIMP:               
         default:
            ulRouteReg |= (AA_GCLK_BITS_HIGHIMP << (iGclkIndex * 3));
            break;
         }
      }
   ulRouteReg &= 0x0FFF;                           // just ensure correct mask
   // first set PLL (none, one or both if needed), extended command 0x04 is used for setting PLL
   for (iGclkIndex=0; iGclkIndex < 4; iGclkIndex++)
      {
      uint32_t ulPllValue = 0;
      if (ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkIndex] == AA_GCLK_MCLK)
         {  // one of clock sources uses PLL MCLK
         if (iRes == ERR_ML_ARC_NO_ERROR)
            iRes = ML_AACreatePllStream(AA_GCLK_MCLK, ptyArcParams->tyAAClockAlloc.dMclkFrequency, &ulPllValue);
         if (iRes == ERR_ML_ARC_NO_ERROR)
            iRes = ML_AAExtendedCommand(ptyArcParams, 0x04, ulPllValue, &usResponse);
         if ((iRes == ERR_ML_ARC_NO_ERROR) && (usResponse != 0xA5A5))
            iRes = ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE;
         }
      else if (ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkIndex] == AA_GCLK_VCLK)
         {  // one of clock sources uses PLL VCLK
         if (iRes == ERR_ML_ARC_NO_ERROR)
            iRes = ML_AACreatePllStream(AA_GCLK_VCLK, ptyArcParams->tyAAClockAlloc.dVclkFrequency, &ulPllValue);
         if (iRes == ERR_ML_ARC_NO_ERROR)
            iRes = ML_AAExtendedCommand(ptyArcParams, 0x04, ulPllValue, &usResponse);
         if ((iRes == ERR_ML_ARC_NO_ERROR) && (usResponse != 0xA5A5))
            iRes = ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE;
         }
      }
   // finally set routing register (extended command 0x5)
   if (iRes == ERR_ML_ARC_NO_ERROR)
      iRes = ML_AAExtendedCommand(ptyArcParams, 0x05, ulRouteReg, &usResponse);
   if ((iRes == ERR_ML_ARC_NO_ERROR) && (usResponse != 0xA5A5))
      iRes = ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE;
   return iRes;
}

/****************************************************************************
     Function: ML_AASetClockAllocation
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               int32_t iGclkSel - GCLK selection
               TyArcAAGclk tyGclkRoute - clock source for GCLK
               double dPllFrequency - frequency in MHz (when using PLL)
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: allocate GCLK source for particular clock
               when using PLL, both MCLK or VCLK can be past, this function decides
               which source will be used to each clock
Date           Initials    Description
18-Oct-2007    VH          Initial
****************************************************************************/
int32_t ML_AASetClockAllocation(TyArcTargetParams *ptyArcParams, int32_t iGclkSel, TyArcAAGclk tyGclkRoute, double dPllFrequency)
{
   int32_t iIndex;
   TyAAClockAllocation tyPrevAllocation;
   int32_t iRes = ERR_ML_ARC_NO_ERROR;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_NO_ERROR;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;

   // first store previous allocation, if something goes bad, we can restore it
   tyPrevAllocation.bAllocateVclkNext = ptyArcParams->tyAAClockAlloc.bAllocateVclkNext;
   tyPrevAllocation.bRestoreAfterBlasting = ptyArcParams->tyAAClockAlloc.bRestoreAfterBlasting;
   tyPrevAllocation.dMclkFrequency = ptyArcParams->tyAAClockAlloc.dMclkFrequency;
   tyPrevAllocation.dVclkFrequency = ptyArcParams->tyAAClockAlloc.dVclkFrequency;
   for (iIndex=0; iIndex < 4; iIndex++)
      tyPrevAllocation.ptyGclk[iIndex] = ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex];

   switch (tyGclkRoute)
      {
      case AA_GCLK_MCLK:
      case AA_GCLK_VCLK:               // this can cause trouble, we need to look which PLL source to use
         {
         if ((ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkSel] == AA_GCLK_VCLK) || (ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkSel] == AA_GCLK_MCLK))
            tyGclkRoute = ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkSel];           // easy, this GCLK already uses some PLL, so just change its frequency
         else
            {  // lets look if any PLL is available at the moment
            unsigned char bVClkUsed = 0;
            unsigned char bMClkUsed = 0;

            for (iIndex=0; iIndex<4; iIndex++)
               {
               if (ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] == AA_GCLK_VCLK)
                  bVClkUsed = 1;
               else if (ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] == AA_GCLK_MCLK)
                  bMClkUsed = 1;
               }
            if (!bVClkUsed)
               tyGclkRoute = AA_GCLK_VCLK;         
            else if (!bMClkUsed)
               tyGclkRoute = AA_GCLK_MCLK;
            else
               {  // ok, this is worst scenario, we need to reallocate one of PLL sources and set default source for original GCLK
               // lets take PLL source assigned as first from MCLK and VCLK
               if (ptyArcParams->tyAAClockAlloc.bAllocateVclkNext)
                  {
                  ptyArcParams->tyAAClockAlloc.bAllocateVclkNext = 0;
                  tyGclkRoute = AA_GCLK_VCLK;         
                  }
               else
                  {
                  ptyArcParams->tyAAClockAlloc.bAllocateVclkNext = 1;
                  tyGclkRoute = AA_GCLK_MCLK;
                  }
               // in addition, we must set original GCLK to default
               for (iIndex=0; iIndex<4; iIndex++)
                  {
                  if (ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] == tyGclkRoute)
                     {  // got it
                     switch (iIndex)
                        {
                        case AA_GCLK(0):
                           ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] = AA_GCLK_HIGHIMP;
                           break;
                        case AA_GCLK(1):
                           ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] = AA_GCLK_CRYSTAL;
                           break;
                        case AA_GCLK(2):
                           ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] = AA_GCLK_HOST;
                           break;
                        case AA_GCLK(3):
                        default:
                           ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] = AA_GCLK_DIPS;
                           break;
                        }
                     break;            // finish cycle, no need to continue
                     }
                  }
               }
            //ok, we managed rearange and allocate new PLL clock source
            }
         // update new clock source and update new frequency
         ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkSel] = tyGclkRoute;
         if (tyGclkRoute == AA_GCLK_MCLK)
            ptyArcParams->tyAAClockAlloc.dMclkFrequency = dPllFrequency;
         else
            ptyArcParams->tyAAClockAlloc.dVclkFrequency = dPllFrequency;
         }
         break;

      case AA_GCLK_HIGHIMP:
      case AA_GCLK_CRYSTAL:
      case AA_GCLK_DIPS:
      case AA_GCLK_HOST:
      default:                         // no big deal, just update clock routing register settings
         ptyArcParams->tyAAClockAlloc.ptyGclk[iGclkSel] = tyGclkRoute;
         break;
      }
   // we have updated clock allocation, now try to set allocation in ARCangel target
   if (iRes == ERR_ML_ARC_NO_ERROR)
      iRes = ML_AASetClockConfiguration(ptyArcParams);

   if (iRes == ERR_ML_ARC_NO_ERROR)
      {
      ptyArcParams->tyAAClockAlloc.bRestoreAfterBlasting = 1;     // configuration update will be needed also after blasting
      }
   else
      {  // something wrong, restore previous configuration
      ptyArcParams->tyAAClockAlloc.bAllocateVclkNext = tyPrevAllocation.bAllocateVclkNext;
      ptyArcParams->tyAAClockAlloc.bRestoreAfterBlasting = tyPrevAllocation.bRestoreAfterBlasting;
      ptyArcParams->tyAAClockAlloc.dMclkFrequency = tyPrevAllocation.dMclkFrequency;
      ptyArcParams->tyAAClockAlloc.dVclkFrequency = tyPrevAllocation.dVclkFrequency;
      for (iIndex=0; iIndex < 4; iIndex++)
         ptyArcParams->tyAAClockAlloc.ptyGclk[iIndex] = tyPrevAllocation.ptyGclk[iIndex];
      }

   return iRes;
}

/****************************************************************************
     Function: ML_SetResetProperty
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bSetResetDelay - setting reset delay(!=0x0) or reset duration (0x0)
               uint32_t *piValue - pointer to value to set (NULL for default)
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set target reset parameters (reset duration and delay after reset)
               nRST       _____               ___________
                               |             |
                               |_____________|
                               <- duration -> <- delay ->(debug communication)

Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_SetResetProperty(TyArcTargetParams *ptyArcParams, unsigned char bSetResetDelay, uint32_t *piValue)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;

   if (bSetResetDelay)
      {
      // updating delay after reset (in ms)
      if ((piValue != NULL) && (*piValue <= MAX_ARC_RESET_VALUE))
         ptyArcParams->ulDelayAfterReset = (uint32_t)(*piValue);
      else
         ptyArcParams->ulDelayAfterReset = DEFAULT_ARC_DELAY_AFTER_RESET;
      }
   else
      {
      // updating reset duration (in ms), duration cannot be 0 (at least 1 ms)
      if ((piValue != NULL) && (*piValue <= MAX_ARC_RESET_VALUE) && (*piValue > 0))
         ptyArcParams->ulResetDuration = (uint32_t)(*piValue);
      else
         ptyArcParams->ulResetDuration = DEFAULT_ARC_RESET_DURATION;
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_ResetTarget
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: reset ARC target if supported
               this function assert nRST pin for certain number of ms and also waits
               after deasserting (delays can be set by ML_SetResetProperty function)
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_ResetTarget(TyArcTargetParams *ptyArcParams)
{
   int32_t iResult = ERR_ML_ARC_NO_ERROR;
   // check valid pointer and if reset is supported for current target
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->tyArcTargetType == ARC_TGT_RSTDETECT)
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;
   // let assert reset pin and wait certain time
   if (DL_OPXD_Arc_ResetProc(ptyArcParams->iCurrentHandle, 1, 0) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   ML_Sleep(ptyArcParams, ptyArcParams->ulResetDuration);
   // now we can deassert reset and wait certain time
   if (DL_OPXD_Arc_ResetProc(ptyArcParams->iCurrentHandle, 0, 0) != DRVOPXD_ERROR_NO_ERROR)
      iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
   ML_Sleep(ptyArcParams, ptyArcParams->ulDelayAfterReset);
   // we done, so return result
   return iResult;
}

/****************************************************************************
     Function: ML_SetOnTargetResetAction
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               TyArcResetActionType tyActionType - type of action
               uint32_t *pulParameters - array with 3 parameters for action
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: add next action after target reset detection
               if all 16 actions are filled, other attempts will be ignored
               to clear list of actions, call this function with RESACT_INIT
Date           Initials    Description
12-Nov-2007    VH          Initial
****************************************************************************/
int32_t ML_SetOnTargetResetAction(TyArcTargetParams *ptyArcParams, TyArcResetActionType tyActionType, uint32_t *pulParameters)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_RSTDETECT)
      return ERR_ML_ARC_TARGET_NOT_SUPPORTED;

   if (tyActionType == RESACT_INIT)
      ptyArcParams->tyOnTargetResetActions.uiNumberOfActions = 0;                      // clear list of actions
   else if (ptyArcParams->tyOnTargetResetActions.uiNumberOfActions < MAX_ON_TARGET_RESET_ACTIONS)
      {  // there is still room for another action so add it to the list
      uint32_t uiNextItem = ptyArcParams->tyOnTargetResetActions.uiNumberOfActions;
      ptyArcParams->tyOnTargetResetActions.ptyActions[uiNextItem] = tyActionType;
      if (pulParameters != NULL)
         {
         ptyArcParams->tyOnTargetResetActions.pulActionParam1[uiNextItem] = pulParameters[0];
         ptyArcParams->tyOnTargetResetActions.pulActionParam2[uiNextItem] = pulParameters[1];
         ptyArcParams->tyOnTargetResetActions.pulActionParam3[uiNextItem] = pulParameters[2];
         }
      else
         {
         ptyArcParams->tyOnTargetResetActions.pulActionParam1[uiNextItem] = 0x0;
         ptyArcParams->tyOnTargetResetActions.pulActionParam2[uiNextItem] = 0x0;
         ptyArcParams->tyOnTargetResetActions.pulActionParam3[uiNextItem] = 0x0;
         }
      (ptyArcParams->tyOnTargetResetActions.uiNumberOfActions)++;                      // action added
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_InvalidatingICache
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bDisableICacheInvalidation - enable skipping icache invalidation
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: enable/disable skipping of icache invalidation after writting to memory
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_InvalidatingICache(TyArcTargetParams *ptyArcParams, unsigned char bDisableICacheInvalidation)
{
   uint32_t ulIndex;
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;

   if (bDisableICacheInvalidation)
      {  // set for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType |= ARC_ACCESS_INVSKIP_SKIP;
      }
   else
      {  // set for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType &= ~ARC_ACCESS_INVSKIP_MASK;
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetTargetEndian
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bBigEndianTarget - TRUE if target is BE, FALSE if target is LE
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set target endianess, little endian (LE) is default
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_SetTargetEndian(TyArcTargetParams *ptyArcParams, unsigned char bBigEndianTarget)
{
   uint32_t ulIndex;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (bBigEndianTarget)
      {  // set BE for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType |= ARC_ACCESS_ENDIAN_BIG;
      }
   else
      {  // set LE for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType &= ~ARC_ACCESS_ENDIAN_MASK;
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetCurrentCpu
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               uint32_t ulCpuNum - set current core number
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set cpu number for debug session
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_SetCurrentCpu(TyArcTargetParams *ptyArcParams, uint32_t ulCpuNum)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // parameter ulCpuNum starts from 1 up to n (if n cores are available)
   if (ulCpuNum)
      {     // convert to index from 0 and assing to local structure
      ulCpuNum--;    
      if (ulCpuNum < ptyArcParams->ulNumberOfARCCores)
         ptyArcParams->ulCurrentARCCore = ulCpuNum;
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetCycleStep
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bCycleStep - using cycle step
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set if cycle step is used
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
int32_t ML_SetCycleStep(TyArcTargetParams *ptyArcParams, unsigned char bCycleStep)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // modify item in current structure and update settings for all cores
   ptyArcParams->bCycleStep = bCycleStep;
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_GetCycleStep
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: unsigned char - cycle step status
  Description: set if cycle step is used
Date           Initials    Description
11-Oct-2007    VH          Initial
****************************************************************************/
unsigned char ML_GetCycleStep(TyArcTargetParams *ptyArcParams)
{
   return (ptyArcParams->bCycleStep);
}

/****************************************************************************
     Function: ML_SetCycleStep
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bCycleStep - using cycle step
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: set if cycle step is used
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_SetAdaptiveClockTimeout(TyArcTargetParams *ptyArcParams, uint32_t ulTimeout)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // update local variable
   if ((ulTimeout < MIN_ARC_RTCK_TIMEOUT) || (ulTimeout > MAX_ARC_RTCK_TIMEOUT))
      ptyArcParams->ulAdaptiveClockTimeout = DEFAULT_ARC_RTCK_TIMEOUT;
   else
      ptyArcParams->ulAdaptiveClockTimeout = ulTimeout;
   // if adaptive clock is being used then we need to send the timeout also to probe
   if (ptyArcParams->bAdaptiveClockEnabled)
      (void)DL_OPXD_JtagSetAdaptiveClock(ptyArcParams->iCurrentHandle, ptyArcParams->ulAdaptiveClockTimeout, ptyArcParams->dJtagFrequency);
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetMemoryAccessOptimization
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               unsigned char bEnableOptimizedAccess - TRUE if using optimized memory accesses
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: enable/disable optimized memory accesses (optimized means no checking of status bit
               which means faster access)
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
int32_t ML_SetMemoryAccessOptimization(TyArcTargetParams *ptyArcParams, unsigned char bEnableOptimizedAccess)
{
   uint32_t ulIndex;
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (bEnableOptimizedAccess)
      {  // set for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType |= ARC_ACCESS_OPTIMIZE_ENABLE;
      }
   else
      {  // set for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType &= ~ARC_ACCESS_OPTIMIZE_MASK;
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetAddressAutoincrement
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               bEnableAddressAutoincrement - TRUE if using access where core automatically increment address
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: specifying if core is able to automatically increment address during memory accesses
               (if yes, memory accesses are faster)
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int32_t ML_SetAddressAutoincrement(TyArcTargetParams *ptyArcParams, unsigned char bEnableAddressAutoincrement)
{
   uint32_t ulIndex;
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (bEnableAddressAutoincrement)
      {  // set for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType |= ARC_ACCESS_AUTOINC_AUTO;
      }
   else
      {  // set for all ARC cores
      for (ulIndex=0; ulIndex < ptyArcParams->ulNumberOfARCCores; ulIndex++)
         ptyArcParams->tyARCCoreParams[ulIndex].usAccessType &= ~ARC_ACCESS_AUTOINC_MASK;
      }
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_IsTargetARCangel
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: unsigned char - TRUE if target is ARCangel, FALSE otherwise
  Description: return information if target is ARCangel
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
unsigned char ML_IsTargetARCangel(TyArcTargetParams *ptyArcParams)
{
   if (ptyArcParams == NULL)
      return FALSE;
   if (ptyArcParams->tyArcTargetType != ARC_TGT_ARCANGEL)
      return FALSE;
   // otherwise target is really ARCangel
   return TRUE;
}

/****************************************************************************
     Function: ML_GetCpuVersionNumber
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: uint32_t - 
  Description: return information if target is ARCangel
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
uint32_t ML_GetCpuVersionNumber(TyArcTargetParams *ptyArcParams)
{
   uint32_t ulValue;

   if (ptyArcParams != NULL)
   {
      if (ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulCoreVersion == 0)
      {
         (void)ML_ReadBlock(ptyArcParams, ML_ARC_ACCESS_AUX, (uint32_t)0x04, 1, &ulValue); 
         ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulCoreVersion = ulValue & 0xff;
      }

      return (ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulCoreVersion);
   }
      
   return 0;
}

/****************************************************************************
     Function: ML_SetCpuVersionNumber
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               uint32_t ulVersionNumber - version register content (lowest byte)
       Output: none
  Description: return information if target is ARCangel
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
void ML_SetCpuVersionNumber(TyArcTargetParams *ptyArcParams, uint32_t ulVersionNumber)
{
   ulVersionNumber &= 0x000000FF;                              // mask core number
   if (ptyArcParams == NULL)
      return;
   ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulCoreVersion = ulVersionNumber;
   // check ARC core type
   ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType &= ~ARC_ACCESS_CORE_MASK;
   if (ulVersionNumber <= 0x0F)
      ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType |= ARC_ACCESS_CORE_ARCTANGETA4;        // it is ARCtanget-A4
   else if (ulVersionNumber <= 0x1F)
      ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType |= ARC_ACCESS_CORE_ARCTANGETA5;        // it is ARCtanget-A5
   else if (ulVersionNumber <= 0x2F)
      ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType |= ARC_ACCESS_CORE_ARC600;             // it is ARC600
   else if (ulVersionNumber <= 0x3F)
      ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType |= ARC_ACCESS_CORE_ARC700;             // so it must be ARC700
   else if (ulVersionNumber <= 0x4F)
      ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType |= ARC_ACCESS_CORE_ARCV2EM;            // so it must be ARC700
}

/****************************************************************************
     Function: ML_IsCpuBE
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: unsigned char - TRUE if ARC is in BE (big endian), FALSE for LE
  Description: return information if current mode is BE or LE
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
unsigned char ML_IsCpuBE(TyArcTargetParams *ptyArcParams)
{
   if (ptyArcParams == NULL)
       return FALSE;
   if ((ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].usAccessType & ARC_ACCESS_ENDIAN_MASK) == ARC_ACCESS_ENDIAN_BIG)
      return TRUE;
   return FALSE;
}

/****************************************************************************
     Function: ML_IndicateTargetStatusChange
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - error code ERR_ML_ARC_xxx
  Description: send probe request to display target status change
Date           Initials    Description
12-Oct-2006    VH          Initial
****************************************************************************/
int32_t ML_IndicateTargetStatusChange(TyArcTargetParams *ptyArcParams)
{
   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   if (DL_OPXD_IndicateTargetStatus(ptyArcParams->iCurrentHandle, 0, 1) != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_USB_DRIVER_ERROR;
   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_ConvertFrequencySelection
     Engineer: Vitezslav Hola
        Input: const char *pszSelection - string with frequency selection
               double *pdFrequency - pointer to converted frequency
       Output: unsigned char - TRUE if frequency is valid and in supported range
  Description: return information if target is ARCangel
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency)
{
   double dFrequency = 0.0;
   int32_t iValue = 0;
   char pszLocSelection[20];

   if ((pszSelection == NULL) || (strlen(pszSelection) >= 20)) 
      return FALSE;
   else
      {  // convert string to lower case in local buffer
      uint32_t uiIndex = 0;
      while (uiIndex < strlen(pszSelection))
         {
         if ((pszSelection[uiIndex] >= 'A') && (pszSelection[uiIndex] <= 'Z'))
            pszLocSelection[uiIndex] = pszSelection[uiIndex] + ('a' - 'A');
         else
            pszLocSelection[uiIndex] = pszSelection[uiIndex];
         uiIndex++;
         }
      pszLocSelection[uiIndex] = 0;        // terminate string
      }
   if (pdFrequency != NULL)
      *pdFrequency = 0.0;

   // first check frequency selection as index (backward compatibility with OpellaUSB)
   if (strlen(pszLocSelection) == 1)
      {  // selection made by index so check value
      switch (pszLocSelection[0])
         {
         case '0':               // 24 MHz
            dFrequency = 24.0;
            break;
         case '1':               // 12 MHz
            dFrequency = 12.0;
            break;
         case '2':               //  8 MHz
            dFrequency = 8.0;
            break;
         case '3':               //  6 MHz
            dFrequency = 6.0;
            break;
         case '4':               //  4 MHz
            dFrequency = 4.0;
            break;
         case '5':               //  3 MHz
            dFrequency = 3.0;
            break;
         case '6':               //  2 MHz
            dFrequency = 2.0;
            break;
         case '7':               //  1 MHz
            dFrequency = 1.0;
            break;
         default:
            return FALSE;        // error in selection
         }
      }
   else if ((strstr(pszLocSelection, "khz") != NULL) && (sscanf(pszLocSelection, "%dkhz", &iValue) == 1))
      {  // value specified in kHz
      dFrequency = (double)iValue;                    // frequency in kHz
      dFrequency /= 1000.0;                           // convert to MHz
      }
   else if ((strstr(pszLocSelection, "mhz") != NULL) && (sscanf(pszLocSelection, "%dmhz", &iValue) == 1))
      {  // value in MHz
      dFrequency = (double)iValue;                    // frequency in MHz, no need to further conversion
      }
   else
      return FALSE;

   // otherwise frequency has been correctly parsed
   if (pdFrequency != NULL)
      *pdFrequency = dFrequency;

   return TRUE;
}

/****************************************************************************
     Function: ML_GetErrorMessage
     Engineer: Vitezslav Hola
        Input: int32_t iReturnCode - error code ML_xxx
       Output: char * - pointer to string with message
  Description: get error message
Date           Initials    Description
05-Nov-2007    VH          Initial
****************************************************************************/
char* ML_GetErrorMessage(int32_t iReturnCode)
{
   char *pszLocMsg;
   static char cTempMsg[50];
   
   switch (iReturnCode)
      {
      case ERR_ML_ARC_NO_ERROR:                       // no error
         pszLocMsg = (char *) MSG_ML_NO_ERROR;                 break;
      case ERR_ML_ARC_INVALID_HANDLE:                 // invalid driver handle
         pszLocMsg = (char *) MSG_ML_INVALID_DRIVER_HANDLE;    break;
      case ERR_ML_ARC_TARGET_NOT_SUPPORTED:           // target does not support selected feature
         pszLocMsg = (char *) MSG_ML_TARGET_NOT_SUPPORTED;     break;
      case ERR_ML_ARC_USB_DRIVER_ERROR:               // USB driver error (Opella-XD did not respond or USB error)
         pszLocMsg = (char *) MSG_ML_USB_DRIVER_ERROR;         break;
      case ERR_ML_ARC_PROBE_NOT_AVAILABLE:            // Opella-XD is not available for debug session
         pszLocMsg = (char *) MSG_ML_PROBE_NOT_AVAILABLE;      break;
      case ERR_ML_ARC_PROBE_INVALID_TPA:              // missing or invalid TPA
         pszLocMsg = (char *) MSG_ML_INVALID_TPA;              break;
      case ERR_ML_ARC_PROBE_CONFIG_ERROR:             // probe configuration error (missing diskware or FPGAware)
         pszLocMsg = (char *) MSG_ML_PROBE_CONFIG_ERROR;       break;
      case ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED:           // RTCK timeout expired (problem with RTCK)
         pszLocMsg = (char *) MSG_ML_RTCK_TIMEOUT_EXPIRED;     break;
      case ERR_ML_ARC_DEBUG_ACCESS:                   // access to ARC JTAG debug register failed
         pszLocMsg = (char *) MSG_ML_DEBUG_ACCESS_FAILED;      break;
      case ERR_ML_ARC_INVALID_FREQUENCY:              // invalid frequency format
         pszLocMsg = (char *) MSG_ML_INVALID_FREQUENCY;        break;
      case ERR_ML_ARC_ANGEL_BLASTING_FAILED:          // blasting FPGA into ARCangel failed
         pszLocMsg = (char *) MSG_ML_AA_BLASTING_FAILED;       break;
      case ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE:  // invalid response to certain extended commands (setting clocks on ARCangel4)
         pszLocMsg = (char *) MSG_ML_AA_INVALID_EXTCMD_RESPONSE;  break;
      case ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY:    // cannot set PLL frequency on ARCangel4 (requested frequency is not supported)
         pszLocMsg = (char *) MSG_ML_AA_INVALID_PLL_FREQUENCY; break;
      case ERR_ML_ARC_TEST_SCANCHAIN_FAILED:          // failing scanchain loopback test
         pszLocMsg = (char *) MSG_ML_SCANCHAIN_TEST_FAILED;    break;
      case ERR_ML_ARC_TARGET_RESET_DETECTED:          // target reset detected
         pszLocMsg = (char *) MSG_ML_TARGET_RESET_DETECTED;    break;
      case ERR_ML_ARC_DEBUG_PDOWN:                    // target is powered-down
         pszLocMsg = (char *) MSG_ML_TARGET_POWERED_DOWN;      break;
      case ERR_ML_ARC_DEBUG_POFF:                    // target is powered-off
         pszLocMsg = (char *) MSG_ML_TARGET_POWERED_OFF;      break;
      default:                                        // unspecified error
         sprintf(cTempMsg,"%s (Error no.: %d)", (char *) MSG_ML_UNKNOWN_ERROR, iReturnCode);
         pszLocMsg = cTempMsg;
         break;
      }
   return pszLocMsg;
}

/****************************************************************************
*** Other MidLayer Functions                                              ***
****************************************************************************/

/****************************************************************************
     Function: ML_SetDllPath
     Engineer: Vitezslav Hola
        Input: const char *pszDllPath - DLL full path
       Output: none
  Description: extract path to DLL folder and store into global variable
               (to be used with ML_CreateFullFilename and others)
Date           Initials    Description
09-Oct-2007    VH          Initial
****************************************************************************/
void ML_SetDllPath(const char *pszDllPath)
{
   char *pszPtr;

   pszGlobalDllPath[0] = 0;
   // copy string to buffer and convert to lower case
   strncpy(pszGlobalDllPath, pszDllPath, DLLPATH_LENGTH - 1);
   pszGlobalDllPath[DLLPATH_LENGTH - 1] = 0;

#ifdef __LINUX
   // try to find .so extension
   pszPtr = strstr(pszGlobalDllPath,".so");
   if (pszPtr != NULL)
      {     // found .so so go backwards to nearest '/' (or to start of string)
      *pszPtr=0;
      while ((*pszPtr != '/') && (pszPtr > pszGlobalDllPath))
         pszPtr--;
      *pszPtr = 0;   // cut string (remove dll name+extension and \ character
      }
#else
   StringToLwr(pszGlobalDllPath);
   // try to find .dll extension
   pszPtr = strstr(pszGlobalDllPath,".dll");
   if (pszPtr != NULL)
      {     // found .dll so go backwards to nearest '\\' (or to start of string)
      *pszPtr=0;
      while ((*pszPtr != '\\') && (pszPtr > pszGlobalDllPath))
         pszPtr--;
      *pszPtr = 0;   // cut string (remove dll name+extension and \ character
      }
#endif
   else
      pszGlobalDllPath[0] = 0;   // wrong format, so use empty string
}

/****************************************************************************
     Function: ML_GetDllPath
     Engineer: Vitezslav Hola
        Input: char *pszDllPath - full path to DLL
               uint32_t uiSize - maximum number of bytes in pszDllPath
       Output: none
  Description: copy full DLL path into provided string
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
void ML_GetDllPath(char *pszDllPath, uint32_t uiSize)
{
   if (uiSize)
      {
      strncpy(pszDllPath, pszGlobalDllPath, uiSize);
      pszDllPath[uiSize-1] = 0;
      }
}

/****************************************************************************
     Function: ML_Sleep
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               uint32_t ulMiliseconds - number of miliseconds to sleep
       Output: none
  Description: sleep version in MidLayer, calling debugger implementation if provided
Date           Initials    Description
09-Oct-2007    VH          Initial
****************************************************************************/
void ML_Sleep(TyArcTargetParams *ptyArcParams, uint32_t ulMiliseconds)
{
   if ((ptyArcParams == NULL) || (ptyArcParams->pfSleepFunc == NULL))
      {
      Sleep((DWORD)ulMiliseconds);  // we must use own implementation
      }
   else
      ptyArcParams->pfSleepFunc((unsigned)ulMiliseconds);
}

/****************************************************************************
     Function: ML_ScanIR
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               uint32_t ulLength - number of bits to scan
               uint32_t *pulDataIn - data to scanchain
               uint32_t *pulDataOut - data from scanchain
       Output: int32_t - result
  Description: scan JTAG IR register for current core
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/
int32_t ML_ScanIR(TyArcTargetParams *ptyArcParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int32_t iResult = ERR_ML_ARC_NO_ERROR;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // call function to scan IR, parameters are checked inside function
   tyRes = DL_OPXD_JtagScanIR(ptyArcParams->iCurrentHandle, ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulScanchainCore,
                              ulLength, pulDataIn, pulDataOut);
   // check what caused function to fail
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARC_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED;
         break;
      default:
         iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
         break;
      }
   return iResult;
}

/****************************************************************************
     Function: ML_ScanDR
     Engineer: Vitezslav Hola
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
               uint32_t ulLength - number of bits to scan
               uint32_t *pulDataIn - data to scanchain
               uint32_t *pulDataOut - data from scanchain
       Output: int32_t - result
  Description: scan JTAG DR register for current core
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/
int32_t ML_ScanDR(TyArcTargetParams *ptyArcParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int32_t iResult = ERR_ML_ARC_NO_ERROR;

   if (ptyArcParams == NULL)
      return ERR_ML_ARC_INVALID_HANDLE;
   // call function to scan DR, parameters are checked inside function
   tyRes = DL_OPXD_JtagScanDR(ptyArcParams->iCurrentHandle, ptyArcParams->tyARCCoreParams[ptyArcParams->ulCurrentARCCore].ulScanchainCore,
                              ulLength, pulDataIn, pulDataOut);
   // check what caused function to fail
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARC_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED;
         break;
      default:
         iResult = ERR_ML_ARC_USB_DRIVER_ERROR;
         break;
      }
   return iResult;
}

/****************************************************************************
     Function: ML_ResetTAP
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - result
  Description: Reset the TAP statemachine
Date           Initials    Description
03-Sept-2008   NCH         Initial
16-Jan  -2009    RS           Added Reset using TRST pin.
****************************************************************************/
int32_t ML_ResetTAP(TyArcTargetParams *ptyArcParams,int32_t bResetType)
{
   int32_t iRes = DRVOPXD_ERROR_NO_ERROR;
   double dFrequency;
   uint32_t ulCurrentPulsePeriod;
   uint32_t ulTDI;
   uint32_t ulTMS;
   uint32_t ulTDO;

   TyDeviceInfo tyLocDeviceInfo;

   //Opella-XD R2 FW/DW still dont support CMD_CODE_DISKWARE_STATUS command, so dont use that for R2
   if (DL_OPXD_GetDeviceInfo(ptyArcParams->iCurrentHandle, &tyLocDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
   {
	   (void)DL_OPXD_CloseDevice(ptyArcParams->iCurrentHandle);
	   return ERR_ML_ARC_USB_DRIVER_ERROR;
   }

   if (tyLocDeviceInfo.ulBoardRevision < 0x03000000)
   {
	   tyLocDeviceInfo.uiActiveDiskwares = 0;
	   tyLocDeviceInfo.bOtherDiskwareRunning = 0;
   }
   else
   {
	   //Check if diskware is already running (could be that its loaded by another instance of GDB server)
	   if (DL_OPXD_GetDiskwareStatus(ptyArcParams->iCurrentHandle, &tyLocDeviceInfo, ARC) != DRVOPXD_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice(ptyArcParams->iCurrentHandle);
		   return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }
   }

   if ((tyLocDeviceInfo.uiActiveDiskwares > 1) || tyLocDeviceInfo.bOtherDiskwareRunning)
   {
	   //Some diskware is running, so dont change JTAG freq now!
	   return ERR_ML_ARC_NO_ERROR;
   }
   dFrequency = ptyArcParams->dJtagFrequency;

   if (dFrequency <= 0.001)
      ulCurrentPulsePeriod = 1000;
   else if (dFrequency >= 1.0)
      ulCurrentPulsePeriod = 1;
   else
      {
      double dPulsePeriod = dFrequency;   //frequency in MHz
      dPulsePeriod = 1.0 / dPulsePeriod;  // get pulse period in microseconds
      ulCurrentPulsePeriod = (uint32_t)(dPulsePeriod + 0.5);
      // check if pulse period is in valid range
      if (ulCurrentPulsePeriod < 1)
         ulCurrentPulsePeriod = 1;
      else if (ulCurrentPulsePeriod > 1000)
         ulCurrentPulsePeriod = 1000;
      }

   if (bResetType) 
	   {
		   //reset the TAP using TRST pin.
	   iRes = DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle, 1);
	   if(iRes != DRVOPXD_ERROR_NO_ERROR)
		  return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }
   else
	   {
	   //reset the TAP with TMS sequence
	   iRes = DL_OPXD_JtagResetTAP(ptyArcParams->iCurrentHandle, 0);
	   if(iRes != DRVOPXD_ERROR_NO_ERROR)
		  return ERR_ML_ARC_USB_DRIVER_ERROR;
	   }
   //now have to go to RTI
   ulTDI = 0; ulTMS = 0;
   iRes = DL_OPXD_JtagPulses(ptyArcParams->iCurrentHandle, 1, ulCurrentPulsePeriod,
                             &ulTDI, &ulTMS, &ulTDO);
   if(iRes != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_USB_DRIVER_ERROR;

   return ERR_ML_ARC_NO_ERROR;
}

/****************************************************************************
     Function: ML_ScanMultiple
     Engineer: Nikolay Chokoev
        Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
       Output: int32_t - result
  Description: 
Date           Initials    Description
03-Sept-2008   NCH         Initial
****************************************************************************/
int32_t ML_ScanMultiple(TyArcTargetParams *ptyArcParams, TyMultipleScanParams *ptyMultipleScanParams,
                    uint32_t *pulDataIn, uint32_t *pulDataOut)
{
   int32_t iRes = DRVOPXD_ERROR_NO_ERROR;
   uint32_t *ppulDataIn[1];
   uint32_t *ppulDataOut[1];

   ppulDataOut[0] = (uint32_t *)pulDataOut;
   ppulDataIn[0]  = (uint32_t *)pulDataIn;

   dbgprint("SM:ulCurrentARCCore:%08lX\n",ptyArcParams->ulCurrentARCCore);
   if(ppulDataIn[0] != NULL)
      dbgprint("SM:*ppulDataIn[0]:%08lX\n",(uint32_t)*ppulDataIn[0]);
   else
      dbgprint("SM:*ppulDataIn[0]:<null>\n");
   iRes = DL_OPXD_JtagScanMultiple(ptyArcParams->iCurrentHandle, ptyArcParams->ulCurrentARCCore, 1,
                                   ptyMultipleScanParams, ppulDataIn, ppulDataOut);
   dbgprint("SM:iRes:%08lX\n",iRes);
   if(ppulDataOut[0] != NULL)
      dbgprint("SM:*ppulDataOut[0]:%08lX\n",(uint32_t)*ppulDataOut[0]);
   else
      dbgprint("SM:*ppulDataOut[0]:<null>\n");
   if(iRes != DRVOPXD_ERROR_NO_ERROR)
      return ERR_ML_ARC_USB_DRIVER_ERROR;
   return ERR_ML_ARC_NO_ERROR;
}

//----------------------------------------------------------------------------
//--- Local functions                                                      ---
//----------------------------------------------------------------------------

/****************************************************************************
     Function: ShiftArrayOfWordsRight
     Engineer: Vitezslav Hola
        Input: uint32_t *pulArray - pointer to array of words
               uint32_t ulWords - number of words in array
       Output: none
  Description: shift array of words by 1 bit right
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
static void ShiftArrayOfWordsRight(uint32_t *pulArray, uint32_t ulWords)
{
   uint32_t ulIndex;
   uint32_t ulTransfer = 0;

   if ((!ulWords) || (pulArray == NULL))
      return;

   for (ulIndex = ulWords; ulIndex > 0; ulIndex--)
      {
      uint32_t ulTemp = (pulArray[ulIndex-1] & 0x1);   // get LSB
      pulArray[ulIndex-1] >>= 1;
      pulArray[ulIndex-1] |= ulTransfer;
      ulTransfer = ulTemp << 31;    // carry bit for next word
      }
}

/****************************************************************************
     Function: CompareArrayOfWords
     Engineer: Vitezslav Hola
        Input: uint32_t *pulArray1 - pointer to 1st array of words
               uint32_t *pulArray2 - pointer to 2nd array of words
               uint32_t ulNumberOfBits - number of bits to compare (least significant)
       Output: int32_t - 0 if arrays are same, otherwise value different than 0
  Description: compare two array of words taking only certain number of bits for comparison
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
static int32_t CompareArrayOfWords(uint32_t *pulArray1, uint32_t *pulArray2, uint32_t ulNumberOfBits)
{
    if ((pulArray1 == NULL) || (pulArray2 == NULL) || (ulNumberOfBits == 0))
        return 0;

    ulNumberOfBits -= 32;

    // compare whole words
    while (ulNumberOfBits>=32)
    {
        if (*pulArray1 != *pulArray2)
            return 1;
        pulArray1++;
        pulArray2++;
        ulNumberOfBits -= 32;
    }
    // compare parts of last word with mask
    if (ulNumberOfBits)
    {
        uint32_t ulMask = 0xFFFFFFFF >> (32 - ulNumberOfBits);
        if ((*pulArray1 & ulMask) != (*pulArray2 & ulMask))
            return 1;
    }
    // otherwise both arrays are same
    return 0;
}
