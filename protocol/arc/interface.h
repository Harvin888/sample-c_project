/****************************************************************************
       Module: interface.h
     Engineer: Vitezslav Hola
  Description: Header with Opella-XD ARC MetaWare debugger driver
Date           Initials    Description
08-Oct-2007    VH          Initial
****************************************************************************/
#ifndef INTERFACE_H_
#define INTERFACE_H_

#include "ml_arc.h"

// MetaWare debugger driver version strings
#ifdef __LINUX
// Linux
#define INTERFACE_NAME_AND_VERSION  "Ashling Opella-XD ARC Driver, 1-June-2018 v1.2.3"
#define INTERFACE_NAME              "Ashling Opella-XD ARC Driver"
#define INTERFACE_VERSION           "1-June-2018 v1.2.3"
#else
// WinXP/2000/Vista
#define INTERFACE_NAME_AND_VERSION  "Ashling Opella-XD ARC Driver, 1-June-2018 v1.2.3"
#define INTERFACE_NAME              "Ashling Opella-XD ARC Driver"
#define INTERFACE_VERSION           "1-June-2018 v1.2.3"
#endif

//AUX PM Registers
#define REG_PM_STATUS   0x450

// MetaWare ARC Processor Interface defines
// return values
#define ASH_POFF        4
#define ASH_PDOWN       2
#define ASH_TRUE        1
#define ASH_FALSE       0

// 
#ifdef __LINUX
// Linux
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
typedef int32_t BOOL;
#define TRUE 1
#define FALSE 0
#define _snprintf snprintf
//extern void usleep(int32_t);
void Sleep(uint32_t  mseconds );
#define ASH_DLL_EXPORT 
#else
// WinXP/2000/Vista
#define ASH_DLL_EXPORT __declspec(dllexport)
#endif

// --- Debugger Interface Function table ---
typedef struct _TyFunctionTable  {
   int32_t            (*DummyFunction)                 (struct TyArcInstance*);
   int32_t            (*version)                       (struct TyArcInstance*);
   const char *   (*id)                            (struct TyArcInstance*);
   void           (*destroy)                       (struct TyArcInstance*);
   const char *   (*additional_possibilities)      (struct TyArcInstance*);
   void*          (*additional_information)        (struct TyArcInstance*, unsigned);
   int32_t            (*prepare_for_new_program)       (struct TyArcInstance*, int32_t);
   int32_t            (*process_property)              (struct TyArcInstance*, const char *, const char *);
   int32_t            (*is_simulator)                  (struct TyArcInstance*);
   int32_t            (*step)                          (struct TyArcInstance*);
   int32_t            (*run)                           (struct TyArcInstance*);
   int32_t            (*read_memory)                   (struct TyArcInstance*, uint32_t, void *, uint32_t, int32_t);
   int32_t            (*write_memory)                  (struct TyArcInstance*, uint32_t, void *, uint32_t, int32_t );
   int32_t            (*read_reg)                      (struct TyArcInstance*, int32_t, uint32_t *);
   int32_t            (*write_reg)                     (struct TyArcInstance*, int32_t, uint32_t);
   unsigned       (*memory_size)                   (struct TyArcInstance*);
   int32_t            (*set_memory_size)               (struct TyArcInstance*, unsigned);
   int32_t            (*set_reg_watchpoint)            (struct TyArcInstance*, int32_t, int32_t);
   int32_t            (*remove_reg_watchpoint)         (struct TyArcInstance*, int32_t, int32_t);
   int32_t            (*set_mem_watchpoint)            (struct TyArcInstance*, uint32_t, int32_t);
   int32_t            (*remove_mem_watchpoint)         (struct TyArcInstance*, uint32_t, int32_t);
   int32_t            (*stopped_at_watchpoint)         (struct TyArcInstance*);
   int32_t            (*stopped_at_exception)          (struct TyArcInstance*);
   int32_t            (*set_breakpoint)                (struct TyArcInstance*, unsigned, void*);
   int32_t            (*remove_breakpoint)             (struct TyArcInstance*, unsigned, void*);
   int32_t            (*retrieve_breakpoint_code)      (struct TyArcInstance*, unsigned, char *, unsigned, void *);
   int32_t            (*breakpoint_cookie_len)         (struct TyArcInstance*);
   int32_t            (*at_breakpoint)                 (struct TyArcInstance*);
   int32_t            (*define_displays)               (struct TyArcInstance*, struct Register_display *);
   int32_t            (*fill_memory)                   (struct TyArcInstance*, uint32_t, void *, uint32_t, uint32_t, int32_t);
   int32_t            (*instruction_trace_count)       (struct TyArcInstance*);
   void           (*get_instruction_traces)        (struct TyArcInstance*, uint32_t *);
   void           (*receive_callback)              (struct TyArcInstance*, ARC_callback*);
   int32_t            (*supports_feature)              (struct TyArcInstance*);
   uint32_t  (*data_exchange)                 (struct TyArcInstance*, uint32_t, uint32_t, uint32_t, void *, uint32_t, void *);
   int32_t            (*in_same_process_as_debugger)   (struct TyArcInstance*);
   uint32_t  (*max_data_exchange_transfer)    (struct TyArcInstance*);
   int32_t            (*read_banked_reg)               (struct TyArcInstance*, int32_t, int32_t, uint32_t *);
   int32_t            (*write_banked_reg)              (struct TyArcInstance*, int32_t, int32_t, uint32_t *);
   int32_t            (*set_mem_watchpoint2)           (struct TyArcInstance*, ARC_ADDR_TYPE, int32_t, unsigned , void **);
} TyFunctionTable;

// --- Action points ---
#define MAX_ACTIONPOINTS                           8 
typedef enum   {UNUSED, WATCH_MEM, WATCH_REG, BKP} TyActionpointType;
typedef union
{
   uint32_t address;
   uint32_t reg;
} TyActionpointData;

typedef struct _TyActionpointInfo
{
   TyActionpointType type;                                     // action point type
   TyActionpointData data;                                     // action point info (address, etc)
   int32_t paired_ap;                                              // associate another ap in case of 8 bytes memory wp
} TyActionpointInfo;

// --- Debugger Interface Identifier structure ---
typedef struct TyArcInstance
{
   TyFunctionTable *ptyFunctionTable;                          // pointer to interface function table
   uint32_t ulMemorySize;                                 // memory size

   uint32_t ulAPUsed;                                     // up to 32 bits, represent a vector of used AP
   unsigned char bAPFullSet;                                   // AP type support (full or minimum set of targets)
   unsigned char ucAPVersion;                                  // version of AP mechanism (as defined in AP_build)
   uint32_t  uiPMVersion;                                  // PU BCR 
   int32_t iNumberOfAP;                                            // number of actionpoints built in an ARC
   TyActionpointInfo ptyAPTable[MAX_ACTIONPOINTS];             // records information on the actionpoints in use

   ARC_callback *pcbCallbackFunction;                          // pointer to callback function

   unsigned char bTraceMessagesEnabled;                        // flag enabling trace messages
   unsigned char bUseGUI;                                      // flag indicating using GUI (dialog boxes, etc.)
   unsigned char bStartupInfoShown;                            // flag indicating if starting message has been shown
   uint32_t  uiCoreNumber;                                 // which core the interface belongs to
	char          pszIniFileLocation[260];
} TyArcInstanceStruct;

char *pszIniFileName;

// defines for start_state parameter
#define ARC_START_STATE_FROM_CURRENT               0x00        // staying in current state (Select-IR-Scan or Select-DR-Scan state)
#define ARC_START_STATE_FROM_RTI                   0x01        // move from Run-Test/Idle into Select-IR-Scan or Select-DR-Scan state before scan
#define ARC_START_STATE_FROM_DR					   0x02        // move from Select-DR-Scan into Select-IR-Scan or Select-DR-Scan state before scan
#define ARC_START_STATE_FROM_EXIT1_IR_RTI          0x03		   // move from Exit1-IR state to shift-DR state via RTI 
#define ARC_START_STATE_FROM_EXIT1_DR_RTI		   0x04		   // move from Exit1-DR state to shift-IR state via RTI
// defines for end_state parameter
#define ARC_END_STATE_TO_RTI                       0x00        // move to Run-Test/Idle state after scan
#define ARC_END_STATE_TO_DR                        0x01        // move to Select-DR-Scan state after scan
#define ARC_END_STATE_TO_IR                        0x02        // move to Select-IR-Scan state after scan
#define ARC_END_STATE_TO_RTI_DR                    0x03        // move to Run-Test/Idle state and continue to Select-DR-Scan state after scan
#define ARC_END_STATE_TO_RTI_IR                    0x04        // move to Run-Test/Idle state and continue to Select-IR-Scan state after scan
#define ARC_END_STATE_TO_RTI_SKIPPAUSE             0x05        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states after scan
#define ARC_END_STATE_TO_DR_SKIPPAUSE              0x06        // move to Select-DR-Scan state skipping PAUSE and EXIT2 states after scan
#define ARC_END_STATE_TO_IR_SKIPPAUSE              0x07        // move to Select-IR-Scan state skipping PAUSE and EXIT2 states after scan
#define ARC_END_STATE_TO_RTI_DR_SKIPPAUSE          0x08        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states and continue to Select-DR-Scan state after scan
#define ARC_END_STATE_TO_RTI_IR_SKIPPAUSE          0x09        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states and continue to Select-IR-Scan state after scan
#define ARC_END_STATE_TO_EXIT1_IR				   0x0A		   // move to Exit1-IR state after scan
#define ARC_END_STATE_TO_EXIT1_DR				   0x0B        // move to Exit1-DR state after scan
TyArcTargetParams *ptyMLHandle = NULL;                             // pointer to structure used in MidLayer

// debug register bit field
#define AN_V                                          12L
#define AC_EQ                                         0x0L
#define AT_MWA                                        0x4L
#define AP_ENA                                        0x1L
#define AP_DIS                                        0x0L
#define BKP_MODE                                      0x1L
#define CORE_REG_MODE                                 0xdL
#define AUX_REG_MODE                                  0x15L
#define MEM_MODE                                      0x5L

#define PMU_BUILD_VERSION_MASK                        0x00000007
#define PMU_BUILD_CK_PRESENT                          0x1
#define PMU_BUILD_PD_PRESENT                          0x2
#define PMU_BUILD_DVF_PRESENT                         0x4

#define AP_BUILD_VERSION_MASK                         0x000000FF
#define AP_BUILD_TYPE_MASK                            0x00000F00
#define AP_BUILD_TYPE_SHIFT                           8
#define AP_DEBUG_AH_BIT                               0x00000004
#define AP_DEBUG_ASR_MASK                             0x000007F8
#define AP_DEBUG_ASR_SHIFT                            3

#define reg_AP_AMV(x)                                 (reg_AP_AMV0 + 0x00000003 * ((uint32_t)(x) & 0xFF))
#define reg_AP_AMM(x)                                 (reg_AP_AMM0 + 0x00000003 * ((uint32_t)(x) & 0xFF))
#define reg_AP_AC(x)                                  (reg_AP_AC0 + 0x00000003 * ((uint32_t)(x) & 0xFF))

#define AP_AC_BRK_VALUE                               0x00000020
#define AP_AC_MEMWP_VALUE                             0x00000012
#define AP_AC_AUXWP_VALUE                             0x00000014
#define AP_AC_MEMRP_VALUE                             0x00000022
#define AP_AC_MEMAP_VALUE                             0x00000032
#define AP_AC_AUXRP_VALUE                             0x00000024
#define AP_AC_AUXAP_VALUE                             0x00000034

// register bits
#define DOMAIN_HALT_BIT                               0x08000000
#define ZERO_HALT_BIT                                 0xFDFFFFFF
#define AC_ZERO_HALT_BIT                              0xFFFFFFFE
// bit masks
#define OPCODE                                        0x0000001F
#define LIMM                                          0x3E
#define LIMM_MASK                                     0x3F
#define PC_ADDRESS_MASK                               0x00FFFFFF
#define ED_M_BIT                                      0x01000000
#define RA_M_BIT                                      0x00400000
#define IS_M_BIT                                      0x00000800
#define FH_M_BIT                                      0x00000002
#define SS_M_BIT                                      0x00000001

#endif   // INTERFACE_H_
