/*
 * ARM RDI - error codes : rdi_err.h
 * Copyright (C) 1998 Advanced RISC Machines Ltd. All rights reserved.
 */

/*
 * RCS $Revision: 1.4.24.1 $
 * Checkin $Date: 2000/01/26 14:40:43 $
 * Revising $Author: hbullman $
 */

#ifndef rdi_err_h
#define rdi_err_h

/*
 * Error codes
 */

/* THESE ARE DUPLICATED IN adp.h */

/*
 * Errors pertinent to all RDI versions
 */

#define RDIError_NoError                0

#define RDIError_Reset                  1
#define RDIError_UndefinedInstruction   2
#define RDIError_SoftwareInterrupt      3
#define RDIError_PrefetchAbort          4
#define RDIError_DataAbort              5
#define RDIError_AddressException       6
#define RDIError_IRQ                    7
#define RDIError_FIQ                    8
#define RDIError_Error                  9
#define RDIError_BranchThrough0         10

#define RDIError_NotInitialised         128
#define RDIError_UnableToInitialise     129
#define RDIError_WrongByteSex           130
#define RDIError_UnableToTerminate      131
#define RDIError_BadInstruction         132
#define RDIError_IllegalInstruction     133
#define RDIError_BadCPUStateSetting     134
#define RDIError_UnknownCoPro           135
#define RDIError_UnknownCoProState      136
#define RDIError_BadCoProState          137
#define RDIError_BadPointType           138
#define RDIError_UnimplementedType      139
#define RDIError_BadPointSize           140
#define RDIError_UnimplementedSize      141
#define RDIError_NoMorePoints           142
#define RDIError_BreakpointReached      143
#define RDIError_WatchpointAccessed     144
#define RDIError_NoSuchPoint            145
#define RDIError_ProgramFinishedInStep  146
#define RDIError_UserInterrupt          147
#define RDIError_CantSetPoint           148
#define RDIError_IncompatibleRDILevels  149

#define RDIError_CantLoadConfig         150
#define RDIError_BadConfigData          151
#define RDIError_NoSuchConfig           152
#define RDIError_BufferFull             153
#define RDIError_OutOfStore             154
#define RDIError_NotInDownload          155
#define RDIError_PointInUse             156
#define RDIError_BadImageFormat         157
#define RDIError_TargetRunning          158
#define RDIError_DeviceWouldNotOpen     159
#define RDIError_NoSuchHandle           160
#define RDIError_ConflictingPoint       161
#define RDIError_TargetBroken           162
#define RDIError_TargetStopped          163



 /*
 * RDI 1.51 errors
 */

#define RDIError_BadValue               164
#define RDIError_Unset                  165

/*
 * RDI 1.51 rt/tx errors
 */

#define RDIError_NotAllowedWhilstExecuting 166
#define RDIError_BadFormat                 167
#define RDIError_Executing                 168
#define RDIError_ExecutingLittleEndian     169
#define RDIError_ExecutingBigEndian        170
#define RDIError_ReentrantDuringProxy      171
#define RDIError_Busy                      172

/*
 * RDI 1.51 asynch errors
 */
#define RDIError_NotExecuting              173
#define RDIError_ProgramFinished           174
#define RDIError_AlreadyExecuting          175
#define RDIError_NoSuchCallback            176

/*
 * Errors pertinent to all versions
 */

#define RDIError_LinkTimeout            200  /* data link timeout error */
#define RDIError_OpenTimeout            201  /* open timeout (c.f link timeout, on an open */
#define RDIError_LinkDataError          202  /* data error (read/write) on link */
#define RDIError_Interrupted            203  /* (e.g. boot) interrupted */
#define RDIError_NoModules              204   /* e.g. empty target */

#define RDIError_LittleEndian           240
#define RDIError_BigEndian              241
#define RDIError_SoftInitialiseError    242

/* New error: ReadOnly, when state can't be written (RDI 1.50 and later) */
#define RDIError_ReadOnly               252
#define RDIError_InsufficientPrivilege  253
#define RDIError_UnimplementedMessage   254
#define RDIError_UndefinedMessage       255

/* Range of numbers that are reserved for RDI implementations */
#define RDIError_TargetErrorBase        256
#define RDIError_TargetErrorTop         (0x1000 - 1)
// ARM Simulator Specific Errors (ARMulator)
#define ARMulErr_NoLicense              260  //Armulator Target specific error messages
#define ARMulErr_Bad_Proc_Type          264  //Armulator Target specific error messages
// leave a gap for other ARMulator specific errors 

// The following are Genia / Vitra specific errors. (as part of the 
// communications within the common directory)...
#define RDIError_PortNotAvailable            300
#define RDIError_TxRxFailed                  301
#define RDIError_ConnectFailed               302
#define RDIError_DisconnectFailed            303
#define RDIError_TxRxEtherFailed             304
#define RDIError_UsbConnectFailed            305
#define RDIError_TxRxUsbFailed               306
#define RDIError_Dw1FileFailed               307
#define RDIError_Fpga1FileFailed             308
#define RDIError_NoToolConfig                309
#define RDIError_IncorrectProcType           310
#define RDIError_Comms                       311
#define RDIError_InitTarget_Opella           312
#define RDIError_LoopBack_Opella             313
#define RDIError_UnknownDiskware             314

#define RDIError_LoopBack_EVBA7              316
#define RDIError_InitTarget_EVBA7            317
#define RDIError_TargetBroken_Opella         318
#define RDIError_TargetBroken_EVBA7          319
#define RDIError_Comms_Opella                320
#define RDIError_Comms_EVBA7                 321

// Genia/Vitra
#define RDIError_LoopBack                    322
#define RDIError_InitTarget                  323

//Opella USB and EVBA7
#define RDIError_IncorrectUSBDriver          324

#define RDIError_CannotWriteToVectorTable    325
#define RDIError_CannotWriteDCCHandler       326

// Real Monitor errors...
#define RDIError_RealMonFailed                  327


#define RDIError_EVBA7_With_ThirdPartyDebugger  328

#define RDIError_Non_OKI_Processor              329

#define RDIError_Failed_To_Send_USB_Packet      330
#define RDIError_Failed_To_Receive_USB_Packet   331
#define RDIError_Failed_To_Start_ASHPORT        332
#define RDIError_Failed_To_Init_PP_Port         333
#define RDIError_Diskware_File_Not_Found        334
#define RDIError_FPGA_File_Not_Found            335
#define RDIError_Incorrect_Diskware_File        336
#define RDIError_Failed_To_Write_Opella_Mem     337
#define RDIError_Failed_To_Allocate_PC_Memory   338


//Errors used for UE32 Diagnostics
#define RDIError_TestFailed                     339
#define RDIError_TestStopped                    340
#define RDIError_IncorrectFirmwareVersion       341


#define RDIError_InvalidSerialNumber            342

// Cause of Break
#define RDIError_TriggerOcuured            		343
#define RDIError_DataAbort_Writing_Cache_Clean  344
#define RDIError_CannotStopAtResetVector        345

//RTCK error
#define RDIError_NoRTCKSupport                  346

//Setting SWBP in ROM Error (for GDB)
#define RDIError_NonEditablePoint				347
#define RDIError_TPANotConnected                348

#define RDIError_DeviceAlreadyInUse             349

// this number can be incremented if necessary, but is used to
// check if the error number could reasonably be an RDI error
#define MAX_RDI_ERROR_NUMBER                    400
#endif /* rdi_err_h */


