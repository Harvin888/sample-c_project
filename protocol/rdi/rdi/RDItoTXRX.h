/****************************************************************************
       Module: RDItoTXRX.h
     Engineer: Brian Connolly
  Description: Stubs for all txrx functions being called from symbols database
               These functions, where appropriate, are transform to RDI calls
Version     Date           Initials    Description
0.9.0       19-APR-2000    BC         Initial
****************************************************************************/

//function Prototypes

static TyError RDITXRXReadMem
(
      TXRXMemTypes               MemType,
      TXRXMemAccessTypes         MemReadType,
      TyBankNo                   *pBankNo,
      adtyp                      StartAddr,
      adtyp                      ReadLen,
      ubyte                      *pBuffer,
      adtyp                      ObjectSize
);

static TyError RDITXRXWriteMem
(
      TXRXMemTypes               MemType,
      TXRXMemAccessTypes         MemWriteType,
      adtyp                      StartAddr,
      adtyp                      WriteLen,
      ubyte                      *pBuffer,
      adtyp                      BufLen,
      adtyp                      ObjectSize
);

static TyError RDITXRXReadTimer
(
      double                     *pdTime
);

static TyError RDITXRXReset
(
      TXRXResetTypes             ResetType
);

static TyError RDITXRXMapMem
(
      TXRXMemTypes               MemType,
      TXRXMapTypes               MapType,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      adtyp                      MapLen
);

static TyError RDITXRXDisplayMap
(
      TXRXMemTypes               MemType,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      ulong                      *pulLen,
      TXRXMapTypes               *pMapType,
      uint16                     *puCodeRAM,
      uint16                     *puDataRAM
);


static TyError RDITXRXReadReg
(
      TXRXReg                    *pSpecifiedReg,
      ulong                      *pulRegValues
);

static TyError RDITXRXWriteReg
(
      TXRXReg                    Reg,
      adtyp                      Value
);


static TyError RDITXRXReadPC
(
      ulong                      *pulRegValues
);

static TyError RDITXRXRepBPStat
(
      TXRXBPStatus               *pBPStat
);

static TyError RDITXRXRepPCPStat
(
      TXRXPCPStatus              *pPCPStat
);

static TyError RDITXRXDisplayBP
(
      TXRXBPTypes                BPTypes,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      ulong                      *pulLen,
      bool                       *pbBPSet,
      ubyte                      *pubCocoMap
);

static TyError RDITXRXSetClearBP
(
      TXRXBPAction               BPAction,
      TXRXBPTypes                BPTypes,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      adtyp                      Len
);

static TyError RDITXRXRepCauseOfBrk
(
      TXRXCauseOfBreak           *pCauseOfBrk
);

static TyError RDITXRXEnDiErBP
(
      TXRXBPAction               BPAction,
      TXRXBPTypes                BPTypes
);

static TyError RDITXRXClock
(
      TXRXClockAction            ClockAction,
      TXRXClockSource            *pClockSource,
      double                     *pdClockFreq,
      double                     *pdClockAFreq,
      double                     *pdClockBFreq,
      double                     *pdClockCFreq
);

static TyError RDITXRXGoStepHalt
(
      TXRXGoStepHaltAction       GoStepHaltAction,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      double                     dTime
);

static TyError RDITXRXSetupDualPortRam
(
      TXRXDualPortRamAction      DPAction,
      adtyp                      StartAddr,
      adtyp                      uLen
);

static TyError RDITXRXProcType
(
      TXRXProcTypeAction         ProcTypeAction,
      ProcessorConfig            *pProcCfg,
      bool                       SetVoltageOnly
);

static TyError RDITXRXGetDwFwVer
(
      char                       *pszDiskWare1,
      char                       *pszDiskWare2,
      char                       *pszFirmWare,
      char                       *pszSeaWare,
      FpgaVerString              *pszFpgaWare
);

static TyError RDITXRXRepEmulStatus
(
      bool                       *pbInEmulation,
      TyEmulStatus               *ptyEmulStatus
);

static TyError RDISendCommand
(
      ubyte                      TxByteCount,
      ubyte                      *pRxByteCount,
      ubyte                      RxByteCount
);

static TyError RDIHandleEmulatorError
(
      ubyte                      ubError
);

static TyError RDITXRXEnDiTrc
(
      bool                       bEnable
);

static TyError RDITXRXFindTrcFrame
(
      TXRXTraceFindMode          FindMode,
      frtype                     StartFrame,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      frtype                     NumFrames,
      bool                       *pbFound,
      frtype                     *pFoundFrame
);

static TyError RDITXRXReadTrcFrames
(
      frtype                     StartFrame,
      ulong                      *puNumFrames,
      TyTrFrame huge             *pFrameBuffer
);

static TyError RDITXRXGetTraceSyncFrame
(
      BOOL                       bSearchForward,
      unsigned long              ulCurrentFrame,
      BOOL                       *pbSyncFound,
      unsigned long              *pulSyncFrame
);

static TyError RDITXRXRepTrigTrcStatus
(
      bool                       *pbTrigSpecified,
      bool                       *pbTraceEnabled,
      bool                       *pTraceData,
      frtype                     *pTraceBufSize,
      bktype                     *pNumBlocks,
      frtype                     *pNumFrames,
      frtype                     *pBlockSize,
      uint                       *puLinesTraced,
      frtype                     *pNumStarts,
      frtype                     *pNumStops
);

static TyError RDITXRXSpecTrigCond
(
      trig_type                  *pTrigger
);

static TyError RDITXRXSpecTrigEvent
(
      TXRXTrigEvents             EventNum,
      trig_type                  *pTrigger
);

static TyError RDITXRXSpecTrigTrc
(
      trig_type                  *pTrigger
);

static TyError RDITXRXSetupCommsSpeed
(
      TyBaudRate                 Speed
);

static TyError RDITXRXReadTriggerCounts
(
      uint16                     *pStartTrigCount,
      uint16                     *pStopTrigCount
);

static TyError RDITXRXEmulatorRevisionNumbers
(
      TXRXBoardType              BoardType,
      char                       *pcBoardRevision,
      char                       *pcMechanicalRevision,
      char                       *pszElectricalRevision
);

#if ULTRAXA
static TyError RDITXRXReadSmartXAConfig
(
      TySmartCfg                 *pCfgStruct
);

#elif ULTRA66
static TyError RDITXRXEnableMulSel
(
      bool                       bSet
);
#if _WIN32
static TyError RDITXRXEnablePDBreak
(
      bool                       bEnable
);

static TyError RDITXRXEnableDDBreak
(
      bool                       bEnable
);

#endif
#elif ULTRA77
static TyError RDITXRXPositionMemory
(
      TXRXMemPosAction           MemPosAction,
      adtyp                      *padStartIC,
      adtyp                      *padSizeIC,
      adtyp                      *padStartSIMM,
      adtyp                      *padSizeSIMM,
      bool                       *pbMemPosEnabled
);

#elif ULTRAM16C
static TyError RDITXRXPositionMemory
(
      TXRXMemPosAction           MemPosAction,
      ubyte                      ubStartSegment,
      uint16                     *puiNumberOfSegments,
      bool                       *pbMemoryAvailable
);
#elif ULTRA51
static TyError RDITXRXSetupBanking
(
      void                       *pBankConfig
);
#elif TRICORE
static TyError RDITXRXSetupHardwareBreakpoints
(
      void
);

static TyError RDITXRXSetupTriggerEvents
(
      void
);
static TyError RDITXRXPCPGoStep
(
      TXRXGoStepHaltAction       GoStepAction,
      BOOL                       *pbSingleStepWasOverAnExit
);

TyError RDITXRXPCPRepCauseOfBrk
(
      TXRXCauseOfBreak           *pCauseOfBrk
);

static TyError RDITXRXPCPReadPC
(
      ulong                      *pulRegValues
);
#endif




/****************************************************************************
     Function: InitialiseRDITXRXFunctionPointers
     Engineer: Brian Connolly
        Input: None
       Output: None
  Description: Sets up the TXRX function pointers to point to functions that
               communicate using the RDI defined PathFinder\Diskware
               txrx format

Version     Date           Initials    Description
0.9.0       17-APR-2000    BC          Initial
*****************************************************************************/
void InitialiseRDITXRXFunctionPointers(void)
{
   TXRXReadMem                   = RDITXRXReadMem;
   TXRXWriteMem                  = RDITXRXWriteMem;
   TXRXReadTimer                 = RDITXRXReadTimer;
   TXRXReset                     = RDITXRXReset;
   TXRXMapMem                    = RDITXRXMapMem;
   TXRXDisplayMap                = RDITXRXDisplayMap;
   TXRXReadReg                   = RDITXRXReadReg;
   TXRXWriteReg                  = RDITXRXWriteReg;
   TXRXReadPC                    = RDITXRXReadPC;
   TXRXRepBPStat                 = RDITXRXRepBPStat;
   TXRXRepPCPStat                = RDITXRXRepPCPStat;
   TXRXDisplayBP                 = RDITXRXDisplayBP;
   TXRXSetClearBP                = RDITXRXSetClearBP;
   TXRXRepCauseOfBrk             = RDITXRXRepCauseOfBrk;
   TXRXEnDiErBP                  = RDITXRXEnDiErBP;
   TXRXClock                     = RDITXRXClock;
   TXRXGoStepHalt                = RDITXRXGoStepHalt;
   TXRXSetupDualPortRam          = RDITXRXSetupDualPortRam;
   TXRXProcType                  = RDITXRXProcType;
   TXRXGetDwFwVer                = RDITXRXGetDwFwVer;
   TXRXRepEmulStatus             = RDITXRXRepEmulStatus;
   SendCommand                   = RDISendCommand;
   HandleEmulatorError           = RDIHandleEmulatorError;
   TXRXEnDiTrc                   = RDITXRXEnDiTrc;
   TXRXFindTrcFrame              = RDITXRXFindTrcFrame;
   TXRXReadTrcFrames             = RDITXRXReadTrcFrames;
   TXRXGetTraceSyncFrame         = RDITXRXGetTraceSyncFrame;
   TXRXRepTrigTrcStatus          = RDITXRXRepTrigTrcStatus;
   TXRXSpecTrigCond              = RDITXRXSpecTrigCond;
   TXRXSpecTrigEvent             = RDITXRXSpecTrigEvent;
   TXRXSpecTrigTrc               = RDITXRXSpecTrigTrc;
   TXRXSetupCommsSpeed           = RDITXRXSetupCommsSpeed;
   TXRXReadTriggerCounts         = RDITXRXReadTriggerCounts;
   TXRXEmulatorRevisionNumbers   = RDITXRXEmulatorRevisionNumbers;
#if ULTRAXA
   TXRXReadSmartXAConfig         = RDITXRXReadSmartXAConfig;
#elif ULTRA66
   TXRXEnableMulSel              = RDITXRXEnableMulSel;
#if _WIN32
   TXRXEnablePDBreak             = RDITXRXEnablePDBreak;
   TXRXEnableDDBreak             = RDITXRXEnableDDBreak;
#endif							   
#elif ULTRA77
   TXRXPositionMemory            = RDITXRXPositionMemory;
#elif ULTRAM16C
   TXRXPositionMemory            = RDITXRXPositionMemory;
#elif ULTRA51
   TXRXSetupBanking              = RDITXRXSetupBanking;
#elif TRICORE
   TXRXSetupHardwareBreakpoints  = RDITXRXSetupHardwareBreakpoints;
   TXRXSetupTriggerEvents        = RDITXRXSetupTriggerEvents;
   TXRXPCPGoStep                 = RDITXRXPCPGoStep;
   TXRXPCPRepCauseOfBrk          = RDITXRXPCPRepCauseOfBrk;
   TXRXPCPReadPC                 = RDITXRXPCPReadPC;
   TXRXSetupTriggerEvents        = RDITXRXSetupTriggerEvents;
#endif
}
