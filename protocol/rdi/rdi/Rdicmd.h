#ifdef __cplusplus
extern "C"
{
#endif
#define RDI_VERSION 151
#include "\pathfndr\txrxcmd\rdi\armtypes.h"
#include "\pathfndr\txrxcmd\rdi\toolconf.h"
#include "\pathfndr\txrxcmd\rdi\rdi.h"
#include "\pathfndr\txrxcmd\rdi\rdi150.h"
#ifndef GENIA                        
//lint -e620
#include "\pathfndr\txrxcmd\rdi\rdi_rti.h"
//lint +e620
#include "\pathfndr\txrxcmd\rdi\winrdi.h"
#endif
#include "\pathfndr\txrxcmd\rdi\rdi_hif.h"
#ifndef GENIA
typedef unsigned long ARMWord;
#endif
TyError GetRDIEndianess(ARMword *Endianess); 
TyError CheckForFatalRDIError(TyError ErrorNum);
TyError ConfigureSemiHosting(uint32 uiSemiState, 
                             uint32 uiSemiVector,
                             uint32 uiARMSWI,
                             uint32 uiThumbSWI,
                             uint32 uiTopOfMemory,
                             uint32 uiDCCSetAddress);
TyError SetSemiHostingState(uint uiSemiState);
TyError GetSemiHostingState(uint *uiSemiState);

   

typedef enum
{
   OP1 = 1,
   OP2,
   OP3,
   OP4,
   IP1,
   IP2,
   IP3,
   IP4
} TyAuxIOPin;


#ifndef GENIA

#define FOUR_BIT_ETM_BUS    0x0
#define EIGHT_BIT_ETM_BUS   0x1
#define SIXTEEN_BIT_ETM_BUS 0x2

typedef struct 
   {
   unsigned char framesperrack;
   uint32 size;
   RDI_TraceIndex start;
   }RDI_TraceOffset;

#endif

#ifdef __cplusplus
}
#endif
