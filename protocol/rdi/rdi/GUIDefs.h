/****************************************************************************
       Module: GUIDEFS.H
     Engineer: Suraj S
  Description: Header File which contains definitions and typedefs for 
               OpxdConfig and OpxdConfigMgr source files
Date           Initials    Description
14-Aug-2007      SJ        Initial
****************************************************************************/

#ifndef _GUI_DEFS_H_
#define _GUI_DEFS_H_

#define MAX_CORES_SUPPORTED   0xA

//Definitions for Multiple USB devices
#define MAX_USB_DEVICES_SUPPORTED      8
#define MAX_OPELLA_USB_USER_ID_LENGTH  0x20
#define MAX_SERIAL_NUMBER_LEN 16 

#if _LINUX
typedef unsigned short  uint16;
typedef signed short    sint16;
typedef unsigned int    uint;
typedef signed int      sint;
typedef unsigned char   byte;
typedef unsigned char   boolean;
typedef unsigned char   UCHAR;
typedef char            CHAR;
typedef char*           PCHAR;
typedef const char*     LPCSTR;
typedef unsigned short  WORD;
typedef unsigned short  USHORT;
typedef unsigned long   ULONG;
typedef unsigned long   DWORD;
typedef WORD*           LPWORD;
typedef DWORD*          LPDWORD;
typedef void*           HANDLE;
typedef void*           HINSTANCE;
typedef void*           FARPROC;
typedef void*           LPVOID;
#define INVALID_HANDLE_VALUE ((HANDLE)-1)
#elif _WIN32
typedef unsigned short  uint16;
typedef signed short    sint16;
typedef unsigned int    uint;
typedef signed int      sint;
#else
typedef unsigned int    uint16;
typedef signed int      sint16;
typedef unsigned int    uint;
typedef signed int      sint;
// Not defined for the 16 bit compiler
typedef unsigned char  byte;
typedef unsigned char  boolean;
#endif


typedef unsigned long  uint32;
typedef long           sint32;
typedef float          float16;
typedef float          float32; 
typedef char *         string ;
typedef unsigned char  ubyte ;
typedef char           sbyte ;
typedef unsigned char  uchar ;
typedef char           schar ;
typedef long           slong;
typedef unsigned long  ulong;
typedef ubyte          uint8 ;
typedef sbyte          sint8 ;
typedef uint8          bool8 ; 
typedef uint16         bool16 ;
typedef uint32         bool32 ;

#if _LINUX
typedef  long long int           sint64;
typedef  unsigned long long int  uint64;
#elif _WIN32
typedef  __int64                 sint64;
typedef  unsigned __int64        uint64;
#endif

typedef unsigned long  uint32;
typedef long           sint32;

// TyError is a signed 32 bit interger no matter who asks
//typedef sint32         TyError;

typedef struct
   {
   unsigned char bARMCore;
   unsigned char ucIRLength;
   unsigned char ucDeviceNumber;
   }TyJtagNode;

typedef struct
   {
   unsigned char ucNumJtagNodes;
   TyJtagNode ptyJtagNode[MAX_CORES_SUPPORTED];

   } TyJtagScanChain;

typedef struct
   {
   unsigned long  ulInstanceNumber;
   //unsigned short usProductID;
   //unsigned long  ulSerialNumber;
   char           szOpellaXDSerialNumber[MAX_SERIAL_NUMBER_LEN];
   //char           szUserID [MAX_OPELLA_USB_USER_ID_LENGTH];
   }TyUSBDeviceInstance;


typedef struct
   {
   unsigned short ulNumberOfUSBDevices;
   unsigned char ucSelectedDevice;
   char          szOpellaXDSerialNumber[MAX_USB_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN];
   TyUSBDeviceInstance tyUSBDeviceInstance[MAX_USB_DEVICES_SUPPORTED];
   } TyMultipleUSBDevices;


#endif // _GUI_DEFS_H_

