/****************************************************************************
       Module: RDICMD.CPP
     Engineer: Brian Connolly
  Description: Stubs for all txrx functions being called from symbols database
               These functions, where appropriate, are transform to RDI calls
Date           Initials    Description
17-APR-2000    BC          Initial
10-Apr-2001    PJB         Added Genia Aux IO
23-AUG-2001    BC          m_consoleWindow no longer a PFWAppObject  
27-JUL-2004    PL          Added support for multi-core
29-11-2004     PL          Return the RDI error number if the error is not handled 
****************************************************************************/

#include "\PATHFNDR\WINMFC\stdafx.h"
#include "\PATHFNDR\TRACE\COMMON\TRACEGLB.H"
#include "\PATHFNDR\TRACE\COMMON\TRACE.H"
#include "\PATHFNDR\TRACE\UE32\TRACE.H"
#include "\PATHFNDR\WINMFC\EMULCNFG\regutil.h"
#include "\PATHFNDR\WINMFC\DIALOGS\COMMON\TRACEFND\UE32FIND.H"
#include "\PATHFNDR\disa\disaarm.h"
#include "\pathfndr\txrxcmd\rdi\rdi_rti.h"
#include "\pathfndr\txrxcmd\rdi\rdi_prop.h"

// some #defines used in Reset functions
#define TOP_26BIT_MASK           0xFFFFFF00
#define ARM_SUPERVISOR_MODE_MASK 0x13
#define INTERRUPTS_DISABLE  		0xC0

#define NO_RESET           		0x0
#define DEBUG_FROM_RESET   		0x1
#define USE_RESET          		0x2
#define HOT_PLUG           		0x3
#define USE_RESET_AND_DELAY		0x4

extern "C"
{
#include "\pathfndr\exphndlr\evaluate.h"
#include "\PATHFNDR\cmdhndlr\grpfile.h"   //added for macro variable support 
}

#define NUM_BYTES_IN_A_RACK      16
#define READ_MEMORY_SIZE         0x80
#define MEMORY_CACHE_SIZE        0x10000
#define MEMORY_CACHE_MASK        0x0FFFF
#define MAX_NUM_BPS              0xff
#define MAX_NUM_REGS             0x16
#define CP15                     0x0f


//Global Variables
static TyPathFinderTarget                 GlobalPathFndrTarget; 
static TXRXProcTypes                      GlobalProcType;
static HINSTANCE                          hGlobalInstRdiDll             = NULL;

BOOL                                      bARMSingleStepInProgress      = FALSE;
static BOOL                               bGlobalCacheSelection         = FALSE;
static BOOL                               bGlobalMemoryRegionSelection  = FALSE;
static unsigned int                       uiGlobalRdiCauseOfBreak       = RDIError_Reset;

static RDI_ConfigPointer                  ptyGlobalCfgPtr;
static RDI_AgentHandle                    tyGlobalAgent;
static RDI_ModuleDesc                     tyGlobalArmModule;
static RDI_ModuleDesc                     tyGlobalEtmModule;

static RDI150_ProcVec                     *pRDIAgentVectorTable;
static const RDI150_ProcVec               *pRDIModuleVectorTable;
static RDI150_ProcVec                     *pETMVectorTable;

static WinRDI_funcGetRDIProcVec           GetRDIProcVec;
static WinRDI_funcGetRDIProcVec           GetETMProcVec;
static WinRDI_funcRegister_Yield_Callback Register_Yield_Callback; 
static WinRDI_funcGetVersion              GetDLLRDIVersion; 
static WinRDI_funcSetVersion              SetDLLRDIVersion;

static double                             ulGlobalExecutionTime = 0;
static BOOL                               bGlobalRdiTraceArmed  = FALSE;
static BOOL                               bGloablEtmEnabled     = FALSE;
static unsigned long                      ulGlobalEtmBusWidth   = 0;
static struct RDI_HostosInterface         tyGlobalhostif;

static unsigned char                      pucGlobalBuffer[MEMORY_CACHE_SIZE];


//Local function prototypes

//Semihosting

void LocRDI_Hif_DbgPrint  (RDI_Hif_DbgArg *arg,
                           const char *format, 
                           va_list ap
                           );

void LocRDI_Hif_DbgPause   (
                           RDI_Hif_DbgArg *arg
                           );

void LocRDI_Hif_WriteC     (
                           RDI_Hif_HostosArg *arg, 
                           int c
                           );

int  LocRDI_Hif_ReadC      (
                           RDI_Hif_HostosArg *arg
                           );

int  LocRDI_Hif_Write      (
                           RDI_Hif_HostosArg *arg, 
                           char const *buffer, 
                           int len
                           );

char *LocRDI_Hif_GetS      (
                           RDI_Hif_HostosArg *arg, 
                           char *buffer, 
                           int len
                           );

void LocRDI_Hif_ResetProc  (
                           RDI_Hif_ResetArg *arg
                           );

#if RDI_VERSION == 151  
RDI_Hif_UserMessageReturn LocRDI_Hif_UserMessageProc(
                                                    RDI_Hif_UserMessageArg *arg, 
                                                    char const *msg, 
                                                    uint32 type
                                                    );
#endif
//various
boolean GetModeAndMaskForReg(TXRXReg TyRegister, unsigned long *pMode,unsigned long *pRegMask);
boolean GetModeAndMaskForETMReg(TXRXReg TyRegister, unsigned long *pMode,unsigned long *pRegMask);
TyError SingleStep(RDI_AgentHandle agent);
TyError IsAddressInARMOrThumbSpace(adtyp ulAddress, TyAddrRangeAttribute *ptyAddrRangeAttribute);
TyError GetCPNumAndMaskForReg(TXRXReg Register, unsigned long *pNum,unsigned long *pRegMask, boolean *pbFoundIt);
void  InitAuxMacroVariables(void);
TyError RDIGetTraceFramesCount(unsigned int *pulNumberOfFrames);
static TyError RDIIsTraceArmed(BOOL *pbTraceArmed);
void FromHostWriteDataToController(void *arg,ARMword *data, int *valid );
void  ToHostReadDataFromController(void *arg,ARMword data);
TyError ConfigureDCC(void); 
TyError ReportUSBDriverVersionMismatch(void);
static TyError CommonResetOrStartupCode(void);

// BOOT UP
//          four bits
//    bit 0    : Boot Level 
//    bit 1    : CommsReset 
//    bit 3..2 : Byte Sex 

#define WARM_BOOT                0x1
#define COMMS_RESET              0x2
#define BIG_ENDIAN_MASK          0x4
#define LITTLE_ENDIAN_MASK       0x0
#define DONT_CARE_ENDIAN_MASK    0x8
#define DONT_HALT_TARGET	     0x10
static  ARMword EndianessType  = LITTLE_ENDIAN_MASK;


// BREAKPONT WATCHPOINT defines
#define BP_COND_MASK     0xf0
#define WP_MASK          0x0f
#define REG_MASK         0x00FF


#define     REGISTER_MASK     0x1f
#define     MAX_REGISER_MASK 0x77FFF

//REGISTER MASKS
#define     R0_MASK      0x1
#define     R1_MASK      0x2
#define     R2_MASK      0x4
#define     R3_MASK      0x8
#define     R4_MASK      0x10
#define     R5_MASK      0x20
#define     R6_MASK      0x40
#define     R7_MASK      0x80
#define     R8_MASK      0x100
#define     R9_MASK      0x200
#define     R10_MASK     0x400
#define     R11_MASK     0x800
#define     R12_MASK     0x1000
#define     R13_MASK     0x2000
#define     R14_MASK     0x4000
#define     R15_MASK     0x10000  // note bit 15 is never set
#define     R16_MASK     0x20000
#define     R17_MASK     0x40000

//ETM REGISTER BANKS
#define     BANK0   0x0
#define     BANK1   0x1
#define     BANK2   0x2
#define     BANK3   0x3
#define     BANK4   0x4
#define     BANK5   0x5
#define     BANK6   0x6
#define     BANK7   0x7

//ETM REGISTER MASKS
#define     R0_ETM_MASK      0x1
#define     R1_ETM_MASK      0x2
#define     R2_ETM_MASK      0x4
#define     R3_ETM_MASK      0x8
#define     R4_ETM_MASK      0x10
#define     R5_ETM_MASK      0x20
#define     R6_ETM_MASK      0x40
#define     R7_ETM_MASK      0x80
#define     R8_ETM_MASK      0x100
#define     R9_ETM_MASK      0x200
#define     R10_ETM_MASK     0x400
#define     R11_ETM_MASK     0x800
#define     R12_ETM_MASK     0x1000
#define     R13_ETM_MASK     0x2000
#define     R14_ETM_MASK     0x4000
#define     R15_ETM_MASK     0x8000  // note bit 15 set

// may need to continue up to R32 in the future

//Structures
typedef struct
{
   unsigned char pucCacheMemory[MEMORY_CACHE_SIZE];
   unsigned long pucCacheAddress[MEMORY_CACHE_SIZE];
   BOOL          bMemoryCacheValid;
   BOOL          bMemoryCacheEnabled;
} TyMemoryCacheStruct;

typedef union
{
   struct
      {
      __int64 ucStat1        : 1;  //sync    
      __int64 ucFlags1       : 3;  //pipe stat
      __int64 usPacket1      : 16; //bus trace
      __int64 ucStat2        : 1;
      __int64 ucFlags2       : 3;
      __int64 usPacket2      : 16;
      __int64 ucStat3        : 1;
      __int64 ucFlags3       : 3;
      __int64 usPacket3      : 16;
      __int64 unused         : 4;
      } Bits;
   unsigned char pucData[0x8];
} ThreeBusFrames;  //sixteen bit ETM port

typedef union
{
   struct
      {
      __int64 ucStat1        : 1;  //sync
      __int64 ucFlags1       : 3;  //pipe stat
      __int64 usPacket1      : 8;  //bus trace
      __int64 ucStat2        : 1;
      __int64 ucFlags2       : 3;
      __int64 usPacket2      : 8;
      __int64 ucStat3        : 1;
      __int64 ucFlags3       : 3;
      __int64 usPacket3      : 8;
      __int64 ucStat4        : 1;
      __int64 ucFlags4       : 3;
      __int64 usPacket4      : 8;
      __int64 ucStat5        : 1;
      __int64 ucFlags5       : 3;
      __int64 usPacket5      : 8;
      __int64 unused         : 4;
      } Bits;
   unsigned char pucData[0x8];
} FiveBusFrames; //for Eight bit ETM port

typedef union
{
   struct
      {
      __int64 ucStat1        : 1;  //sync
      __int64 ucFlags1       : 3;  //pipe stat
      __int64 usPacket1      : 4;  //bus trace
      __int64 ucStat2        : 1;
      __int64 ucFlags2       : 3;
      __int64 usPacket2      : 4;
      __int64 ucStat3        : 1;
      __int64 ucFlags3       : 3;
      __int64 usPacket3      : 4;
      __int64 ucStat4        : 1;
      __int64 ucFlags4       : 3;
      __int64 usPacket4      : 4;
      __int64 ucStat5        : 1;
      __int64 ucFlags5       : 3;
      __int64 usPacket5      : 4;
      __int64 ucStat6        : 1;
      __int64 ucFlags6       : 3;
      __int64 usPacket6      : 4;
      __int64 ucStat7        : 1;
      __int64 ucFlags7       : 3;
      __int64 usPacket7      : 4;
      __int64 ucStat8        : 1;
      __int64 ucFlags8       : 3;
      __int64 usPacket8      : 4;
      } Bits;
  unsigned char pucData[0x8];
} EightBusFrames;  //four bit ETM port

#define MAXREGISTERS 18

typedef struct
{
   unsigned long pulUSER          [MAXREGISTERS];
   unsigned long pulSYSTEM        [MAXREGISTERS];
   unsigned long pulFIQ           [MAXREGISTERS];
   unsigned long pulIRQ           [MAXREGISTERS];
   unsigned long pulSUPERVISOR    [MAXREGISTERS];
   unsigned long pulABORT         [MAXREGISTERS];
   unsigned long pulUNDEFINED     [MAXREGISTERS];
   BOOL          bUSER      ;
   BOOL          bSYSTEM    ;
   BOOL          bFIQ       ;
   BOOL          bIRQ       ;
   BOOL          bSUPERVISOR;
   BOOL          bABORT     ;
   BOOL          bUNDEFINED ;

   BOOL          bRegisterCacheEnabled;
} TyARMRegisterCacheStruct;

static TyMemoryCacheStruct    MemoryCacheStruct;
static TyARMRegisterCacheStruct  RegisterCacheStruct;


// RDITXRX Function Prototypes
static TyError RDITXRXSetupTriggerEvents
(
      void
);

static TyError RDITXRXSetupBanking
(
      void                       *pBankConfig
);

static TyError RDITXRXReadMem
(
      TXRXMemTypes               MemType,
      TXRXMemAccessTypes         MemReadType,
      TyBankNo                   *pBankNo,
      adtyp                      StartAddr,
      adtyp                      ReadLen,
      ubyte                      *pBuffer,
      adtyp                      ObjectSize
);

static TyError RDITXRXWriteMem
(
      TXRXMemTypes               MemType,
      TXRXMemAccessTypes         MemWriteType,
      adtyp                      StartAddr,
      adtyp                      WriteLen,
      ubyte                      *pBuffer,
      adtyp                      BufLen,
      adtyp                      ObjectSize
);

static TyError RDITXRXReadTimer
(
      double                     *pdTime
);

// this is a soft reset
static TyError RDITXRXReset
(
      TXRXResetTypes             ResetType
);

// hard reset
static TyError RDITXRXHardReset
(
 void
);

static TyError RDITXRXMapMem
(
      TXRXMemTypes               MemType,
      TXRXMapTypes               MapType,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      adtyp                      MapLen
);

static TyError RDITXRXDisplayMap
(
      TXRXMemTypes               MemType,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      ulong                      *pulLen,
      TXRXMapTypes               *pMapType,
      uint16                     *puCodeRAM,
      uint16                     *puDataRAM
);


static TyError RDITXRXReadReg
(
      TXRXReg                    *pSpecifiedReg,
      ulong                      *pulRegValues
);

static TyError RDITXRXReadRegisters
(
      ulong                      *pulRegValues
);


static TyError RDITXRXWriteReg
(
      TXRXReg                    Reg,
      adtyp                      Value
);


static TyError RDITXRXReadPC
(
      ulong                      *pulRegValues
);

static TyError RDITXRXRepBPStat
(
      TXRXBPStatus               *pBPStat
);

static TyError RDITXRXRepPCPStat
(
      TXRXPCPStatus              *pPCPStat
);

static TyError RDITXRXDisplayBP
(
      TXRXBPTypes                BPTypes,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      ulong                      *pulLen,
      bool                       *pbBPSet,
      ubyte                      *pubCocoMap
);

static TyError RDITXRXGetBPList
(
      TyBreakpointElement        *ptyBreakpointList
);

static TyError RDITXRXGetBPCount
(
      unsigned long              *pulCount
);

static TyError RDITXRXSetClearBP
(
      TXRXBPAction               BPAction,
      TXRXBPTypes                BPTypes,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      adtyp                      Len,
      adtyp                      Bound,
      uint32                     DataType,
      unsigned short             usCount
);

static TyError RDITXRXRepCauseOfBrk
(
      TXRXCauseOfBreak           *pCauseOfBrk
);

static TyError RDITXRXEnDiErBP
(
      TXRXBPAction               BPAction,
      TXRXBPTypes                BPTypes
);

static TyError RDITXRXClock
(
      TXRXClockAction            ClockAction,
      TXRXClockSource            *pClockSource,
      double                     *pdClockFreq,
      double                     *pdClockAFreq,
      double                     *pdClockBFreq,
      double                     *pdClockCFreq
);

static TyError RDITXRXGoStepHalt
(
      TXRXGoStepHaltAction       GoStepHaltAction,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      double                     dTime
);

static TyError RDITXRXSetupDualPortRam
(
      TXRXDualPortRamAction      DPAction,
      adtyp                      StartAddr,
      adtyp                      uLen
);

static TyError RDITXRXProcType
(
      TXRXProcTypeAction         ProcTypeAction,
      ProcessorConfig            *pProcCfg,
      bool                       SetVoltageOnly
);

static TyError RDITXRXGetDwFwVer
(
      char                       *pszDiskWare1,
      char                       *pszDiskWare2,
      char                       *pszFirmWare,
      char                       *pszSeaWare,
      FpgaVerString              *pszFpgaWare,
      char                       *pszSimVersion,
      char                       *pszUSBCorrectVersion,
      char                       *pszUSBIncorrectVersion
);

static TyError RDITXRXRepEmulStatus
(
      bool                       *pbInEmulation,
      TyEmulStatus               *ptyEmulStatus
);

static TyError RDISendCommand
(
      ubyte                      TxByteCount,
      ubyte                      *pRxByteCount,
      ubyte                      RxByteCount
);

static TyError RDIHandleEmulatorError
(
      ubyte                      ubError
);

static TyError RDITXRXEnDiTrc
(
      bool                       bEnable
);

static TyError RDITXRXFindTrcFrame
(
      TXRXTraceFindMode          FindMode,
      frtype                     StartFrame,
      TyBankNo                   StartBank,
      adtyp                      StartAddr,
      frtype                     NumFrames,
      bool                       *pbFound,
      frtype                     *pFoundFrame
);

static TyError RDITXRXReadTrcFrames
(
      frtype                     StartFrame,
      ulong                      *puNumFrames,
      TyTrFrame huge             *pFrameBuffer
);

static TyError RDITXRXRepTrigTrcStatus
(
      bool                       *pbTrigSpecified,
      bool                       *pbTraceEnabled,
      bool                       *pTraceData,
      frtype                     *pTraceBufSize,
      bktype                     *pNumBlocks,
      frtype                     *pNumFrames,
      frtype                     *pBlockSize,
      uint                       *puLinesTraced,
      frtype                     *pNumStarts,
      frtype                     *pNumStops
);

static TyError RDITXRXSpecTrigCond
(
      trig_type                  *pTrigger
);

static TyError RDITXRXSpecTrigEvent
(
      TXRXTrigEvents             EventNum,
      trig_type                  *pTrigger
);

static TyError RDITXRXSpecTrigTrc
(
      trig_type                  *pTrigger
);

static TyError RDITXRXSetupCommsSpeed
(
      TyBaudRate                 Speed
);

static TyError RDITXRXReadTriggerCounts
(
      uint16                     *pStartTrigCount,
      uint16                     *pStopTrigCount
);

static TyError RDITXRXEmulatorRevisionNumbers
(
      TXRXBoardType              BoardType,
      char                       *pcBoardRevision,
      char                       *pcMechanicalRevision,
      char                       *pszElectricalRevision
);

static TyError GetMemoryCacheData
(
   unsigned long ulStartAddr,
   unsigned long ulReadLen,
   unsigned char *pucBuffer
);
static TyError GetRegisterCacheData
(
   unsigned long ulMode,
   unsigned long ulMask,
   unsigned long *pulRegValue
);



static TyError LoadRdiLibrary
(
   TyPathFinderTarget PathFndrTarget, 
   TXRXProcTypes      ProcType,
   const char *       pszProcessorName
);


static TyError FreeRdiLibrary
(
   void
);


static void RDIGetErrorMessage
(
   char *pszErrorText, 
   BOOL *pbErrorDetected, 
   BOOL *pbWasInEmulation
);


static TyError RDIEnableMemoryCache
(
   BOOL bEnable
);

static BOOL RDIMemoryCachingEnabled
(
   void
);

static TyError RDIEnableRegisterCache
(
   BOOL bEnable
);


static BOOL RDIRegisterCachingEnabled
(
   void
);


static BOOL RDIWithinExecContinueLoop
(
   void
);

static TyError RDITXRXReadCurrentEthernetValues(int        iPortNumber      ,
                                                TyBaudRate tyBaudRate       ,
                                                char       *pszIPAddress      ,
                                                char       *pszSubnetAddress  ,
                                                char       *pszGatewayAddress ,
                                                char       *pszEthernetAddress);

static TyError RDITXRXWriteNewEthernetValues(   int iPortNumber,
                                                TyBaudRate tyBaudRate,
                                                char *pszIPAddress,
                                                char *pszSubnetAddress,
                                                char *pszGatewayAddress );
static TyError RDITXRXReadETMReg
(
      TXRXReg                    *pSpecifiedReg,
      ulong                      *pulRegValues
);

static TyError RDITXRXSetGetVectorCatch
(
      adtyp                      *ulVectorMask,
      boolean                    bSet
);

static TyError RDITXRXWriteETMReg
(
      TXRXReg                    Reg,
      adtyp                      Value
);


static TyError RDITXRXGetBPResourceInfo
(
      adtyp                      StartAddr,
      bool                       *bHardWare,
      uint32                     *ResourceNumber,
      uint32                     *DataType,
      bool                       *bThereAreTwo,
      adtyp                      *DataValue
);


 /* DiReturnT */
typedef enum
{
    /* These must be ordered from OK to FATAL */
    DI_OK                  =  0,
    DI_ERR_STATE           =  1,
    DI_ERR_PARAM           =  2,
    DI_ERR_COMMUNICATION   =  3,
    DI_ERR_NOTSUPPORTED    =  4,
    DI_ERR_NONFATAL        =  5,
    DI_ERR_CANCEL          =  6,
    DI_ERR_FATAL           =  7
} DiReturnT, *pDiReturnT;


//TyDiWriteEtherDetails is a function pointer type def'n, with the specified parameters
// and the specified return type
typedef DiReturnT (*TyDiWriteEtherDetails)      (int iPortNumber, TyBaudRate tyBaudRate, char *pszIPAddress, char *pszSubnetAddress, char *pszGatewayAddress);
typedef DiReturnT (*TyDiReadEtherDetails)       (int iPortNumber, TyBaudRate tyBaudRate, char *pszIPAddress, char *pszSubnetAddress, char *pszGatewayAddress, char *pszEthernetAddress);

//define instances function pointers
static int iIP1AuxIOState =0;

//single stepping
#define BX_PC 0x4778
static unsigned long RegValues[UNUSED_REG];

//ETM
TyError GetAddressCompatatorResourcRegisters(unsigned int uiRangeNumber,
                                             TXRXReg      *StartRangeReg,
                                             TXRXReg      *EndRangeReg,
                                             TXRXReg      *AccessTypeReg0,
                                             TXRXReg      *AccessTypeReg1
                                           );


extern CConsoleWindow          m_ConsoleWindow;            // Console I/O Window

static CArray<TyBreakpointElement,TyBreakpointElement&> tyBreakpointArray;
                                                           
/****************************************************************************
     Function: CheckForFatalRDIError
     Engineer: Brian Connolly
        Input: RDI Error number
       Output: TyError: NO_ERROR or IDERR_RDI_ERROR 
  Description: 

Date     Initials    Description
29-06-2000  B.C      Initial
29-11-2004  P.L      Return the RDI error number if the error is not handled 
*****************************************************************************/
TyError CheckForFatalRDIError(TyError ErrorNum)                   
{
   TyError ErrRet = NO_ERROR;

   // there is a possibility that if this function gets called twice,
   //  the already assigned error string (eg IDERR_RDI_TGT_COMMS) can
   //  get over-written. So if we have an error that is outside of the
   //  "sensible" range of RDI errors, just return it unchanged
   if (ErrorNum > MAX_RDI_ERROR_NUMBER)
      {
      // This should never happen. Only one call of CheckForFatalRDIError is
      // allowed after each RDI function.
      ASSERT(FALSE);
      return ErrorNum;
      }

   if (ErrorNum != 0)
      ErrRet = NO_ERROR;

   switch (ErrorNum)
      {
      case RDIError_NoError:
         return NO_ERROR;
      case RDIError_UnimplementedMessage:
      case RDIError_UnimplementedType:
         if (GlobalPathFndrTarget != RDISimulator)
            // we want to trap this unimplemented message whilst debugging
            ASSERT_NOW();
      case RDIError_BreakpointReached:
      case RDIError_UndefinedInstruction:
      case RDIError_TargetStopped:
      case RDIError_UserInterrupt:
      case RDIError_PrefetchAbort:
      case RDIError_DataAbort:
      case RDIError_AddressException:
      case RDIError_FIQ:
      case RDIError_WatchpointAccessed:
      case RDIError_BranchThrough0:
      case RDIError_SoftwareInterrupt:
      case RDIError_IRQ:
      case RDIError_TriggerOcuured:
         // These are actually 'cause of break' messages...
         uiGlobalRdiCauseOfBreak = ErrorNum;
         return NO_ERROR;

      case RDIError_Reset:
         ErrRet = RDITXRXHardReset();
         if (ErrRet != NO_ERROR)
            return IDERR_RDI_TGT_COMMS;
         return IDERR_RDI_TGT_RST;

      case RDIError_TargetRunning:
      case RDIError_NotAllowedWhilstExecuting:
         return IDERR_RDI_TGT_RUNNING;

      case RDIError_UnableToInitialise:
      case RDIError_Comms:
      case RDIError_TargetBroken:
      case RDIError_Comms_EVBA7:
      case RDIError_TargetBroken_Opella:
      case RDIError_Comms_Opella:
         return IDERR_RDI_TGT_COMMS;
      
      case RDIError_LoopBack_Opella:
         return IDERR_RDI_FAIL_LOOPBACK_OPELLA;
      
      case RDIError_LoopBack_EVBA7:
         return IDERR_RDI_FAIL_LOOPBACK_EVBA7;

      case RDIError_TargetBroken_EVBA7:
         return IDERR_RDI_TGT_COMMS_EVBA7;

      case RDIError_InitTarget_Opella:
         return IDERR_RDI_FAIL_INIT_TGT_OPELLA;

      case RDIError_InitTarget_EVBA7:
         return IDERR_RDI_FAIL_INIT_TGT_EVBA7;

      case RDIError_BadPointType:
         return IDERR_RDI_BP_NOT_VALID;

      case ARMulErr_NoLicense:
         return IDERR_RDI_NO_LICENSE;

      case RDIError_IncorrectProcType:
         return IDERR_RDI_TGT_BAD_PROC;

      case RDIError_CantSetPoint:
         return IDERR_RDI_NO_POINT_RES;

      case RDIError_NoSuchPoint:
         return IDERR_RDI_CLEAR_POINT_RES;

      case RDIError_PointInUse:
         return IDERR_RDI_USED_POINT_RES;

      case RDIError_PortNotAvailable:
         return IDERR_RDI_PORT_NOT_AVAIL;

      case RDIError_TxRxFailed:
      case RDIError_TxRxEtherFailed:
      case RDIError_TxRxUsbFailed:
         return IDERR_RDI_TXRX_FAILED;

      case RDIError_ConnectFailed:
      case RDIError_DisconnectFailed:
      case RDIError_UsbConnectFailed:
         return IDERR_RDI_CONNECT_FAILED;

      case RDIError_Dw1FileFailed:
         return IDERR_RDI_DW1_FAILED;

      case RDIError_Fpga1FileFailed:
         return IDERR_RDI_FPGA1_FAILED;

      case RDIError_NoToolConfig:
         return IDERR_RDI_NO_CONFIG;

      case RDIError_UnknownDiskware:
         return IDERR_RDI_UNKNOWN_DISKWARE;

      case RDIError_IncorrectUSBDriver:
         return ReportUSBDriverVersionMismatch();
      
      case RDIError_CannotWriteToVectorTable:
         return IDERR_RDI_CANNOT_WRITE_VECTOR_TABLE;
      
      case RDIError_CannotWriteDCCHandler:
         return IDERR_RDI_CANNOT_WRITE_DCCHANDLER;

      case RDIError_Executing:
		   in_ddc = TRUE;
		   return NO_ERROR;

      case RDIError_DataAbort_Writing_Cache_Clean:
		   return IDERR_RDI_CANNOT_WRITE_CACHECLEAN;

      case RDIError_CannotStopAtResetVector:
		   return IDERR_RDI_CANNOT_STOP_RESET_VECTOR;


      default:
		   CString csErrorStr;
		   CString csTitle;

         csErrorStr.Format("RDI Error %d occurred\n",ErrorNum);
         (void)csTitle.LoadString(AFX_IDS_APP_TITLE);
         (void)GetPFWAppObject()->m_pMainWnd->MessageBox(csErrorStr, csTitle, MB_ICONWARNING | MB_OK);

         return NO_ERROR;
      }

   return IDERR_RDI_ERROR; 

}  /* CheckForFatalRDIError */



/****************************************************************************
     Function: GetDatabaseFilename
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: Returns the path and filename for the database file.
Date           Initials    Description
12-May-2005    MOH         Initial
****************************************************************************/
static void GetDatabaseFilename(char *pszPathName, TyPathFinderTarget PathFndrTarget)
{
   char szDatabaseFilename[_MAX_PATH] = {0};

   // Get the module filename...
   GetModuleFileName(NULL,szDatabaseFilename,0x100);

   // Strip off the executable name....
   while (strlen(szDatabaseFilename) != 0x0)
      {
      if (szDatabaseFilename[strlen(szDatabaseFilename) - 1] == '\\')
         break;

      szDatabaseFilename[strlen(szDatabaseFilename) - 1] = '\0';
      }

   if (strlen(szDatabaseFilename) == 0x0)
      strcpy(szDatabaseFilename, "C:\\");

   // Append on the database name...
   switch(PathFndrTarget)
      {
      default:
         // Assert and flow through....
         ASSERT(FALSE);
      case Vitra:
      case Genia:
         strcat(szDatabaseFilename, "Genia.cnf");
         break;
      case Opella:
         strcat(szDatabaseFilename, "Opella.cnf");
         break;
      case EVBA7:
         strcat(szDatabaseFilename, "Evba7.cnf");
         break;
      case RDISimulator:
         strcat(szDatabaseFilename, "armulate.cnf");
         break;
      }

   // Now return it....
   strcpy(pszPathName,szDatabaseFilename);
}

/*****************************************************************************
     Function: ReportUSBDriverVersionMismatch
     Engineer: Colm Burke
     Input:    void
     Output:   Error code
  Description: Utility function to retrieve the necessary information and report error

Version     Date           Initials    Description
1.0.5-P     22 Mar 2003    CB          Initial
******************************************************************************/
TyError ReportUSBDriverVersionMismatch(void)
{
   CString csTitle;
   char szErrorText[MAX_STRING_SIZE*2];
   char szUSBCorrectVersion[MAX_STRING_SIZE];
   char szUSBIncorrectVersion[MAX_STRING_SIZE];
   TyError ErrRet = NO_ERROR;

   szUSBCorrectVersion[0] = '\0';
   szUSBIncorrectVersion[0] = '\0';

   (void)csTitle.LoadString(AFX_IDS_APP_TITLE);
   
   ErrRet = RDITXRXGetDwFwVer(NULL, NULL, NULL, NULL, NULL, NULL, szUSBCorrectVersion, szUSBIncorrectVersion);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   sprintf(szErrorText, "The currently loaded USB drivers for your Ashling Opella/EVBA7 are out of date. PathFinder requires a USB driver at version %s (the currently installed version is %s ). Please update to the latest versions (as supplied with this release). After updating your drivers via the Control Panel, please ensure you remove both the power and USB cable from your Opella/EVBA7 and wait 3 seconds before reconnecting. This ensures the Opella/EVBA7 firmware is correctly upgraded.", szUSBCorrectVersion, szUSBIncorrectVersion);

   (void)GetPFWAppObject()->m_pMainWnd->MessageBox(szErrorText, csTitle, MB_ICONWARNING | MB_OK);

   return IDERR_RDI_INCORRECT_USB_DRIVER;
}

/****************************************************************************
     Function: RDITXRXSetGetClearAuxIO
     Engineer: Brian Connolly
       Input:  OpellaIOType, (def-> txrxarm.h)
       Output: ErrRet
  Description: used for setting the external i/o bits ip1 op1 op2 on opella,
               and op1,2,3,4, ip1,2,3,4 on genia.
               TyAuxIOPin -> defined in rdicmd.h:
               TyOpellaIOType -> def in Action.H
Version     Date           Initials    Description
0.9.0       29 Nov 2000    BC          Initial
1.0.1       9 April 2001   PJB         Added Genia IO.
****************************************************************************/
TyError RDITXRXSetGetClearAuxIO(unsigned char  ucAuxIOType, BOOL  *ptyAuxIOState)
{
   TyError ErrRet;

   TyAuxIOPin        AuxIOPin;
   unsigned char     AuxIOAddress;
   unsigned long     AuxIOState  = 0x0;

   // Is I/O supported in the target...
   ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_IOBitsSupported,NULL,NULL);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   if ((ucAuxIOType == SET_OP1)   ||
       (ucAuxIOType == SET_OP2)   ||
       (ucAuxIOType == SET_OP3)   ||
       (ucAuxIOType == SET_OP4)   ||
       (ucAuxIOType == CLEAR_OP1) ||
       (ucAuxIOType == CLEAR_OP2) ||
       (ucAuxIOType == CLEAR_OP3) ||
       (ucAuxIOType == CLEAR_OP4))
      {
      switch(ucAuxIOType)
         {
         default:
            ASSERT(FALSE);
         case SET_OP1 :
            AuxIOState   = 1;
            AuxIOPin     = OP1;
            AuxIOAddress = OP1_ADDRESS;
            break;
         case SET_OP2 :
            AuxIOState   = 1;
            AuxIOPin     = OP2;
            AuxIOAddress = OP2_ADDRESS;
            break;
         case SET_OP3 :
            AuxIOState   = 1;
            AuxIOPin     = OP3;
            AuxIOAddress = OP3_ADDRESS;
            break;
         case SET_OP4 :
            AuxIOState   = 1;
            AuxIOPin     = OP4;
            AuxIOAddress = OP4_ADDRESS;
            break;
         case CLEAR_OP1 :
            AuxIOState   = 0;
            AuxIOPin     = OP1;
            AuxIOAddress = OP1_ADDRESS;
            break;
         case CLEAR_OP2 :
            AuxIOState   = 0;
            AuxIOPin     = OP2;
            AuxIOAddress = OP2_ADDRESS;
            break;
         case CLEAR_OP3 :
            AuxIOState   = 0;
            AuxIOPin     = OP3;
            AuxIOAddress = OP3_ADDRESS;
            break;
         case CLEAR_OP4 :
            AuxIOState   = 0;
            AuxIOPin     = OP4;
            AuxIOAddress = OP4_ADDRESS;
            break;
         }
      ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_SetIOBits,(unsigned long *)&AuxIOPin,(unsigned long *)&AuxIOState);
      if (ErrRet != RDIError_UnimplementedType)
         {
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }
      //read the state
      ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_GetIOBits,(unsigned long *)&AuxIOPin,(unsigned long *)&AuxIOState);
      if (ErrRet != RDIError_UnimplementedType)
         {
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }
      }


   if ((ucAuxIOType == GET_OP1) ||
       (ucAuxIOType == GET_OP2) ||
       (ucAuxIOType == GET_OP3) ||
       (ucAuxIOType == GET_OP4) ||
       (ucAuxIOType == GET_IP1) ||
       (ucAuxIOType == GET_IP2) ||
       (ucAuxIOType == GET_IP3) ||
       (ucAuxIOType == GET_IP4))
      {
      switch(ucAuxIOType)
         {
         default:
            ASSERT(FALSE);
         case GET_OP1 :
            AuxIOPin     = OP1;
            AuxIOAddress = OP1_ADDRESS;
            break;
         case GET_OP2 :
            AuxIOPin     = OP2;
            AuxIOAddress = OP2_ADDRESS;
            break;
         case GET_OP3 :
            AuxIOPin     = OP3;
            AuxIOAddress = OP3_ADDRESS;
            break;
         case GET_OP4 :
            AuxIOPin     = OP4;
            AuxIOAddress = OP4_ADDRESS;
            break;
         case GET_IP1 :
            AuxIOPin     = IP1;
            AuxIOAddress = IP1_ADDRESS;
            break;
         case GET_IP2 :
            AuxIOPin     = IP2;
            AuxIOAddress = IP2_ADDRESS;
            break;
         case GET_IP3 :
            AuxIOPin     = IP3;
            AuxIOAddress = IP3_ADDRESS;
            break;
         case GET_IP4 :
            AuxIOPin     = IP4;
            AuxIOAddress = IP4_ADDRESS;
            break;
         }

      // Read the state
      ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_GetIOBits,(unsigned long *)&AuxIOPin,(unsigned long *)&AuxIOState);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }

   // Return the current state 
   *ptyAuxIOState = (AuxIOState == 1);

   return NO_ERROR;

}  /* RDITXRXSetGetClearAuxIO */

/****************************************************************************
     Function: InitialiseRDITXRXFunctionPointers
     Engineer: Brian Connolly
        Input: None
       Output: None
  Description: Sets up the TXRX function pointers to point to functions that
               communicate using the RDI defined PathFinder\Diskware
               txrx format

Version     Date           Initials    Description
0.9.0       17-APR-2000    BC          Initial
*****************************************************************************/
void InitialiseRDITXRXFunctionPointers(void)
{
   TXRXGetBPResourceInfo         = RDITXRXGetBPResourceInfo;
   TXRXReadMem                   = RDITXRXReadMem;
   TXRXWriteMem                  = RDITXRXWriteMem;
   TXRXReadTimer                 = RDITXRXReadTimer;
   TXRXReset                     = RDITXRXReset;
   TXRXHardReset                 = RDITXRXHardReset;
   TXRXMapMem                    = RDITXRXMapMem;
   TXRXDisplayMap                = RDITXRXDisplayMap;
   TXRXReadReg                   = RDITXRXReadReg;
   TXRXWriteReg                  = RDITXRXWriteReg;
   TXRXReadPC                    = RDITXRXReadPC;
   TXRXRepBPStat                 = RDITXRXRepBPStat;
   TXRXRepPCPStat                = RDITXRXRepPCPStat;
   TXRXDisplayBP                 = RDITXRXDisplayBP;
   TXRXGetBPList                 = RDITXRXGetBPList;
   TXRXGetBPCount                = RDITXRXGetBPCount;
   TXRXSetClearBP                = RDITXRXSetClearBP;
   TXRXRepCauseOfBrk             = RDITXRXRepCauseOfBrk;
   TXRXEnDiErBP                  = RDITXRXEnDiErBP;
   TXRXClock                     = RDITXRXClock;
   TXRXGoStepHalt                = RDITXRXGoStepHalt;
   TXRXSetupDualPortRam          = RDITXRXSetupDualPortRam;
   TXRXProcType                  = RDITXRXProcType;
   TXRXGetDwFwVer                = RDITXRXGetDwFwVer;
   TXRXRepEmulStatus             = RDITXRXRepEmulStatus;
   SendCommand                   = RDISendCommand;
   HandleEmulatorError           = RDIHandleEmulatorError;
   TXRXEnDiTrc                   = RDITXRXEnDiTrc;
   TXRXFindTrcFrame              = RDITXRXFindTrcFrame;
   TXRXReadTrcFrames             = RDITXRXReadTrcFrames;
   TXRXRepTrigTrcStatus          = RDITXRXRepTrigTrcStatus;
   TXRXSpecTrigCond              = RDITXRXSpecTrigCond;
   TXRXSpecTrigEvent             = RDITXRXSpecTrigEvent;
   TXRXSpecTrigTrc               = RDITXRXSpecTrigTrc;
   TXRXSetupCommsSpeed           = RDITXRXSetupCommsSpeed;
   TXRXReadTriggerCounts         = RDITXRXReadTriggerCounts;
   TXRXEmulatorRevisionNumbers   = RDITXRXEmulatorRevisionNumbers;
   TXRXSetupBanking              = RDITXRXSetupBanking;
   TXRXReadRegisters             = RDITXRXReadRegisters;


   TXRXReadCurrentEthernetValues = RDITXRXReadCurrentEthernetValues; 
   TXRXWriteNewEthernetValues    = RDITXRXWriteNewEthernetValues;    

   TXRXFreeTargetLibrary         = FreeRdiLibrary;
   TXRXLoadTargetLibrary         = LoadRdiLibrary;

   TXRXEnableRegisterCache       = RDIEnableRegisterCache;
   TXRXEnableMemoryCache         = RDIEnableMemoryCache;
   TXRXMemoryCachingEnabled      = RDIMemoryCachingEnabled;
   TXRXRegisterCachingEnabled    = RDIRegisterCachingEnabled;

   TXRXWithinExecContinueLoop    = RDIWithinExecContinueLoop;
   TXRXWriteETMReg               = RDITXRXWriteETMReg;
   TXRXReadETMReg                = RDITXRXReadETMReg;
   TXRXSetupTriggerEvents        = RDITXRXSetupTriggerEvents;
   TXRXSetGetVectorCatch         = RDITXRXSetGetVectorCatch;
   TXRXSetGetClearAuxIO          = RDITXRXSetGetClearAuxIO;

}  /* InitialiseRDITXRXFunctionPointers */

/****************************************************************************
     Function: LoadRdiLibrary
     Engineer: Brian Connolly
        Input: iTarget : Target dll to load
       Output: Error value or NO_ERROR
  Description: Initialisation of a single processor system.
               Loads the specified RDI DLL. 
               obtains function pointers for initialisation etc.
Date           Initials    Description
16-June-2000   B.C         Initial
22-Jan-2001    PJB         Added RDI 150 support.
 3-Jul-2003    PJB         Added CurrentMemoryRegion support..
01-Mar-2004    PN          Added pszProcessorName
07-Mar-2005    Pl          Write the cache clean address to the tool conf
****************************************************************************/
static TyError LoadRdiLibrary(TyPathFinderTarget PathFndrTarget, 
                              TXRXProcTypes      ProcType,
							  const char *       pszProcessorName)
{
   TyError  ErrRet;

   // initialise
   GetRDIProcVec          = NULL;
   GetETMProcVec          = NULL;
   Register_Yield_Callback= NULL;
   GetDLLRDIVersion       = NULL;
   SetDLLRDIVersion       = NULL;
   
   // Store in global
   GlobalPathFndrTarget = PathFndrTarget; 
   GlobalProcType       = ProcType;

   // Ensure no Target Specific DLL is currently loaded
   if (hGlobalInstRdiDll != NULL)
      {
      // Close the libary
      ErrRet = TXRXFreeTargetLibrary();
      if(ErrRet!=NO_ERROR)
         return ErrRet;
      }

   // Load the New Target Specific DLL
   switch (PathFndrTarget)
      {
      case RDISimulator:            
         hGlobalInstRdiDll = LoadLibrary("ARMULATE.DLL");
         break;
      case Opella:
         hGlobalInstRdiDll = LoadLibrary("OPELLA.DLL");
         break;
      case Genia:
         hGlobalInstRdiDll = LoadLibrary("GENIA.DLL");
         break;
      case Vitra:
         hGlobalInstRdiDll = LoadLibrary("GENIA.DLL");
         break;
      case EVBA7:
         hGlobalInstRdiDll = LoadLibrary("EVBA7.DLL");
         break;
      default:
         ASSERT_NOW();
         break;
      }

      
   // failed to load target specific DLL
   if (hGlobalInstRdiDll == NULL) 
      {
      switch (PathFndrTarget)
         {
         // identify specific dll we are having the problem with
         case RDISimulator:
            ErrRet =  IDERR_LOAD_ARM_DLL_ARMULATOR;
            break;
         case Opella:
            ErrRet =  IDERR_LOAD_ARM_DLL_OPELLA;
            break;
         case Genia:
            ErrRet =  IDERR_LOAD_ARM_DLL_GENIA;
            break;
         case Vitra:
            ErrRet =  IDERR_LOAD_ARM_DLL_VITRA;
            break;
         case Ultra:
            ErrRet =  IDERR_LOAD_ARM_DLL_ULTRA;
            break;
         default:
            ErrRet =  IDERR_LOAD_ARM_DLL;
            break;
         }
      return ErrRet;
      }

         
   // Get Pointers to WinRDI 
   GetDLLRDIVersion          =  (WinRDI_funcGetVersion)GetProcAddress(hGlobalInstRdiDll,WinRDI_strGetVersion);
   SetDLLRDIVersion          =  (WinRDI_funcSetVersion)GetProcAddress(hGlobalInstRdiDll,WinRDI_strSetVersion);
   GetRDIProcVec             =  (WinRDI_funcGetRDIProcVec)GetProcAddress(hGlobalInstRdiDll,WinRDI_strGetRDIProcVec);
   Register_Yield_Callback   =  (WinRDI_funcRegister_Yield_Callback)GetProcAddress(hGlobalInstRdiDll,WinRDI_strRegister_Yield_Callback);

   if (PathFndrTarget == Vitra)
      GetETMProcVec          =  (WinRDI_funcGetRDIProcVec)GetProcAddress(hGlobalInstRdiDll,"WinRDI_GetETMProcVec");
   
   // Check all function pointers...ignore the version functions for RDISimulator (Armulator)
   if ( ((SetDLLRDIVersion        == NULL) && (PathFndrTarget != RDISimulator))   ||
        ((GetDLLRDIVersion        == NULL) && (PathFndrTarget != RDISimulator))   ||
        ((GetETMProcVec           == NULL) && (PathFndrTarget == Vitra))          ||
        ((GetRDIProcVec           == NULL))                                       ||
        ((Register_Yield_Callback == NULL)))
      {
      switch (PathFndrTarget)
         { // identify specific dll we are having the problem with
         case RDISimulator:
            ErrRet =  IDERR_LOAD_ARM_DLL_ARMULATOR;
            break;
         case Opella:
            ErrRet =  IDERR_LOAD_ARM_DLL_OPELLA;
            break;
         case Genia:
            ErrRet =  IDERR_LOAD_ARM_DLL_GENIA;
            break;
         case Vitra:
            ErrRet =  IDERR_LOAD_ARM_DLL_VITRA;
            break;
         case Ultra:
            ErrRet =  IDERR_LOAD_ARM_DLL_ULTRA;
            break;
         default:
            ErrRet =  IDERR_LOAD_ARM_DLL;
            break;
         }
      return ErrRet;
      }
   


   // get a pointer to the exported ProcVector entry points
   pRDIAgentVectorTable = GetRDIProcVec();
   
   if (PathFndrTarget == Vitra)
      pETMVectorTable = GetETMProcVec();


   ErrRet = CommonResetOrStartupCode();
   if (ErrRet != NO_ERROR)
      return ErrRet;

   return NO_ERROR;     

}  /* LoadRdiLibrary */

/****************************************************************************
     Function: OpenProcessorModule
     Engineer: Colm Burke
        Input: None
       Output: Error value or NO_ERROR
  Description: This function opens (or re-opens) the Processor Module. This
               is called during OnFileLoad, so that a cleanup is performed
               after loading a program.

Version     Date           Initials    Description
1.0.5       22-Nov-2002    C.B.        Initial
****************************************************************************/
TyError OpenProcessorModule(void)
{
   TyError ErrRet;
   unsigned int type = WARM_BOOT|EndianessType;
   
   ErrRet = pRDIModuleVectorTable->open(tyGlobalArmModule.handle, type, ptyGlobalCfgPtr, &tyGlobalhostif, NULL);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;
   return NO_ERROR;

}

/****************************************************************************
     Function: RDILoadStartInfo
     Engineer: Colm Burke
        Input: None
       Output: Error value or NO_ERROR
  Description: Calls RDIInfo_LoadStart

Version     Date           Initials    Description
1.0.6       08-Apr-2003    CB          Initial
****************************************************************************/
TyError RDILoadStartInfo(void)
{
   TyError ErrRet = NO_ERROR;

   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_LoadStart,NULL,(unsigned long *)&tyGlobalArmModule);

   ErrRet = CheckForFatalRDIError(ErrRet);

   return ErrRet;
}

/****************************************************************************
     Function: RDILoadEndInfo
     Engineer: Colm Burke
        Input: None
       Output: Error value or NO_ERROR
  Description: Calls RDIInfo_LoadEnd

Version     Date           Initials    Description
1.0.6       08-Apr-2003    CB          Initial
****************************************************************************/
TyError RDILoadEndInfo(void)
{
   TyError  ErrRet                    = NO_ERROR;
   uint32   uiProcHasSetInternalState = FALSE;

   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_LoadEnd,&uiProcHasSetInternalState,(unsigned long *)&tyGlobalArmModule);

   ErrRet = CheckForFatalRDIError(ErrRet);

   return ErrRet;
}

/****************************************************************************
     Function: ConfigureDCC
     Engineer: Brian Connolly
        Input: None
       Output: Error value or NO_ERROR
  Description: setup Debug Communication Channel callbacks
               setup handler address for DCC based semihosting

Version     Date           Initials    Description
1.0.2       25-Jul-2001    B.C         Initial
****************************************************************************/
TyError ConfigureDCC(void) 
{
   TyError ErrRet;
   uint32  uiTargetCapabilities;

   // Read the target information to See if the target supports DCC
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_Target,&uiTargetCapabilities,(unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Does the target support  DCC
   if ((uiTargetCapabilities&RDITarget_HasCommsChannel)==RDITarget_HasCommsChannel)
      {
      int handle=1;
   
      //register the callback functions
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                           RDICommsChannel_FromHost,
                                           (unsigned long *)FromHostWriteDataToController,
                                           (unsigned long *)&handle);
      ErrRet =  CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
   
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                           RDICommsChannel_ToHost,
                                           (unsigned long *)ToHostReadDataFromController,
                                           (unsigned long *)&handle);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }
   return NO_ERROR;

}  /* ConfigureDCC */

/****************************************************************************
     Function: ConfigureSemiHosting
     Engineer: Brian Connolly
        Input: None
       Output: Error value or NO_ERROR
  Description: setup semihosting interface

Date           Initials    Description
25-Jul-2001    B.C         Initial
15-Jan-2003    PJB         Added Top Of Memory.
10-Feb-2005    PL          Semi-hosting state should only be checked when the target is halted
****************************************************************************/
TyError ConfigureSemiHosting(uint32 uiSemiState, 
                             uint32 uiSemiVector, 
                             uint32 uiARMSWI, 
                             uint32 uiThumbSWI,
                             uint32 uiTopOfMemory,
                             uint32 uiDCCSetAddress)
{
   TyError ErrRet =NO_ERROR;
   uint32    uiSemiHostDummy=0;
   uint32    uiReadSemiState;

   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDIInfo_SemiHosting,
                                        &uiSemiHostDummy,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != RDIError_NoError)
      return ErrRet;
      
   // set the semi hosting vector, 
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_SetVector,
                                        &uiSemiVector,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;
   
   // set the DCC handler address
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_SetDCCHandlerAddress,
                                        &uiDCCSetAddress,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Can ARM SWI be set
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDIInfo_SemiHostingSetARMSWI,
                                        &uiSemiHostDummy,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Set the SWI value
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_SetARMSWI,
                                        &uiARMSWI,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;                  
   
   // Can Thumb SWI be set
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDIInfo_SemiHostingSetThumbSWI,
                                        &uiSemiHostDummy,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;                  

   //Set the SWI value
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_SetThumbSWI,
                                        &uiThumbSWI,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;                  
   
   // ************************
   // Can Top Of Memory be set
   // ************************
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDIInfo_SetTopMem,
                                        &uiSemiHostDummy,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   //Set the Top Of Memory value
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDIInfo_SetTopMem,
                                        &uiTopOfMemory,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;                  

   // set the semi hosting state, enable,disable, via DCC
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_SetState,
                                        &uiSemiState,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Cannot read the semi-hosting state if the target is executing
   if (!in_ddc)
      {
      // Check that semi-hosting was successfully set
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                           RDISemiHosting_GetState,
                                           &uiReadSemiState,
                                           (unsigned long *)&tyGlobalArmModule);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      
      if(uiReadSemiState != uiSemiState)
         return IDERR_RDI_NO_POINT_RES;
      }

   return NO_ERROR;

}  /* ConfigureSemiHosting */
/****************************************************************************
     Function: GetSemiHostingState
     Engineer: Patrick Lavin
        Input: None
       Output: Error value or NO_ERROR
  Description: Read semi-hosting state

Date           Initials    Description
01-Sep-2004    P.L         Initial
****************************************************************************/
TyError GetSemiHostingState(uint *uiSemiState)
{
   TyError ErrRet =NO_ERROR;

   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_GetState,
                                        (unsigned long *)uiSemiState,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   return ErrRet;
}  /* GetSemiHostingState */
/****************************************************************************
     Function: SetSemiHostingState

     Engineer: Patrick Lavin
        Input: None
       Output: Error value or NO_ERROR
  Description: Set the semi-hosting state

Date           Initials    Description
01-Sep-2004    P.L         Initial
****************************************************************************/
TyError SetSemiHostingState(uint uiSemiState)
{
   TyError ErrRet =NO_ERROR;

   // set the semi hosting state, enable,disable, via DCC
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,
                                        RDISemiHosting_SetState,
                                        (unsigned long *)&uiSemiState,
                                        (unsigned long *)&tyGlobalArmModule);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   return ErrRet;
}  /* GetSemiHostingState */
/****************************************************************************
     Function: FreeRdiLibrary
     Engineer: Brian Connolly
        Input: None
       Output: Error value or NO_ERROR
  Description: Frees the currently loaded RDI library

Date           Initials    Description
16-June-2000   B.C         Initial
01-June-2005   MOH         Make sure no errors are returned by
                           this function
****************************************************************************/
static TyError FreeRdiLibrary(void)
{
   if (tyGlobalArmModule.handle!=NULL)
      {
      //close the debug module  
      (void)pRDIModuleVectorTable->close(tyGlobalArmModule.handle);
      if(bGloablEtmEnabled)
         {
         (void)pETMVectorTable->close(tyGlobalEtmModule.handle);
         }
      }

   if (tyGlobalAgent)
      {
      //close the debug module  
      (void)pRDIAgentVectorTable->closeagent(tyGlobalAgent);
      }

   if (hGlobalInstRdiDll !=NULL)  
      (void)FreeLibrary(hGlobalInstRdiDll);
   
   tyGlobalArmModule.handle=NULL;
   tyGlobalAgent = NULL;

   return NO_ERROR;

}  /* FreeRdiLibrary */

/****************************************************************************
     Function: RDIEnableMemoryCache
     Engineer: Brian Connolly
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: 

Version     Date           Initials    Description
0.9.0       16-June-2000   B.C         Initial
*****************************************************************************/
static TyError RDIEnableMemoryCache(BOOL bEnable)
{

   MemoryCacheStruct.bMemoryCacheEnabled  = bEnable;
   MemoryCacheStruct.bMemoryCacheValid    = FALSE;
   MemoryCacheStruct.pucCacheMemory[0]    = MemoryCacheStruct.pucCacheMemory[0];  //lint
   MemoryCacheStruct.pucCacheAddress[0]   = MemoryCacheStruct.pucCacheAddress[0]; //lint
   return NO_ERROR;
}

/****************************************************************************
     Function: RDIMemoryCachingEnabled
     Engineer: Brian Connolly
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: 

Version           Date           Initials    Description
0.9.0          16-June-2000   B.C         Initial
*****************************************************************************/
static BOOL RDIMemoryCachingEnabled(void)
{
   return MemoryCacheStruct.bMemoryCacheEnabled;
}

/****************************************************************************
     Function: RDIEnableRegisterCache
     Engineer: Brian Connolly
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
0.9.0          16-June-2000   B.C         Initial
****************************************************************************/
static TyError RDIEnableRegisterCache(BOOL bEnable)
{
   RegisterCacheStruct.bRegisterCacheEnabled = bEnable;
   RegisterCacheStruct.bUSER                 = FALSE;
   RegisterCacheStruct.bSYSTEM               = FALSE;
   RegisterCacheStruct.bFIQ                  = FALSE;
   RegisterCacheStruct.bIRQ                  = FALSE;
   RegisterCacheStruct.bSUPERVISOR           = FALSE;
   RegisterCacheStruct.bABORT                = FALSE;
   RegisterCacheStruct.bUNDEFINED            = FALSE;
   return NO_ERROR;
}


/****************************************************************************
     Function: RDIRegisterCachingEnabled
     Engineer: Brian Connolly
        Input: None
       Output: True if Register Caching is enabled/available, else FALSE
  Description: Currently only available for TriCore
Version        Date           Initials    Description
0.9.0          16-June-2000   B.C         Initial
****************************************************************************/
static BOOL RDIRegisterCachingEnabled(void)
{
   return RegisterCacheStruct.bRegisterCacheEnabled;
}

/****************************************************************************
     Function: RDITXRXReadMem
     Engineer: Brian Connolly
        Input: MemType: type of memory
               StartAddr: start address of memory to read
               ReadLen: length of memory to read
               pBuffer *: pointer to storage for memory read
               ObjectSize: size in bytes of objects to be read
                           0=1byte : supported by all targets
                           1=1byte, 2=2byte, 4=4byte : supported by some targets
       Output: TyError: NO_ERROR or error number
  Description: Reads data from specified emulator memory MemType at address
               StartAddr length ReadLen in to pBuffer. *pBufLen is
               set to memory read length.
Version        Date           Initials    Description
0.9.0          17-APR-2000    BC          Initial
****************************************************************************/
static TyError RDITXRXReadMem
(
      TXRXMemTypes               MemType,
      TXRXMemAccessTypes         MemReadType,
      TyBankNo                   *pBankNo,
      adtyp                      StartAddr,
      adtyp                      ReadLen,
      ubyte                      *pBuffer,
      adtyp                      ObjectSize
)
{
   
   TyError ErrRet             =  NO_ERROR;
   unsigned int uiReadLen     =  0;

   // since the ARM7 uses a von Neuman architecture 
   // we dont car about the access type Data/Code
   RDI_AccessType Access_Type =  RDIAccess_Code;

   MemReadType =  MemReadType;   //lint
   pBankNo     =  pBankNo;       //lint
   MemType     =  MemType;       //lint
   Access_Type =  Access_Type;   //Lint
   ObjectSize  =  ObjectSize;    //Lint

   //see if we are configured for little or bit endian
   uiReadLen      = ReadLen;
   
   if (MemoryCacheStruct.bMemoryCacheEnabled)
      {
      ErrRet = GetMemoryCacheData(StartAddr, uiReadLen, (unsigned char *)pBuffer);
      if (ErrRet == RDIError_DataAbort)
         ErrRet = NO_ERROR;
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }
   else
      {
      ErrRet = pRDIModuleVectorTable->read(tyGlobalArmModule.handle,StartAddr , (void *) pBuffer, &uiReadLen, (RDI_AccessType) RDIAccess_Code);
      if (ErrRet == RDIError_DataAbort)
         ErrRet = NO_ERROR;
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }
   return NO_ERROR;

}  /* RDITXRXReadMem */

/****************************************************************************
     Function: RDIWithinExecContinueLoop
     Engineer: Brian Connolly
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version        Date           Initials    Description
0.9.0          17-APR-2000    BC          Initial
****************************************************************************/
BOOL RDIWithinExecContinueLoop(void)
{
   return in_ddc;
}

/****************************************************************************
     Function: RDITXRXWriteMem
     Engineer: Brian Connolly
        Input: MemType: type of memory
               TXRXMemAccessTypes         MemWriteType,
               StartAddr: start address of memory to write to
               WriteLen: length of memory to write to
               pBuffer *: pointer to data to write
               BufLen: length of buffer
               ObjectSize: size in bytes of objects to be written
                           0=1byte : supported by all targets
                           1=1byte, 2=2byte, 4=4byte : supported by some targets
       Output: TyError: NO_ERROR or error number
  Description: Writes data from pBuffer length uBufLen to specified emulator
               memory MemType at address StartAddr for length WriteLen (got that!).
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXWriteMem(TXRXMemTypes                MemType,
                                TXRXMemAccessTypes          MemWriteType,
                                adtyp                       StartAddr,
                                adtyp                       WriteLen,
                                ubyte                       *pBuffer,
                                adtyp                       BufLen,
                                adtyp                       ObjectSize)
{
   TyError        ErrRet         =  NO_ERROR;
   unsigned int   uiWriteLen     =  0;
   unsigned int   uiBufLen       =  0;
   unsigned int   uiRepeat       =  0;
   unsigned int   uiRemainder    =  0;
   unsigned int   LoopCount      =  1;
   unsigned long  ulCurrentAddr  =  StartAddr;
   RDI_AccessType Access_Type    =  RDIAccess_Code;

   //Store as Local Variables
   uiBufLen    =  BufLen;
   uiWriteLen  =  WriteLen;
   MemType     =  MemType; //lint
   Access_Type =  Access_Type;   //Lint

   // see how many times you want to write the buffer
   uiRepeat=uiWriteLen/BufLen;
   
   // See if there is a remainder
   uiRemainder=uiWriteLen%BufLen;

   // Write the data in even sized blocks
   if(uiRepeat)
      {
      for(LoopCount=1;LoopCount<=uiRepeat;LoopCount++)
         {
         ErrRet = pRDIModuleVectorTable->write(tyGlobalArmModule.handle, (void const *) pBuffer ,ulCurrentAddr ,&uiBufLen , (RDI_AccessType)RDIAccess_Code);
         ErrRet =  CheckForFatalRDIError(ErrRet);
         if(ErrRet!=NO_ERROR)
            return ErrRet;
         ulCurrentAddr+=(adtyp)uiBufLen; //increment the start address
         }
      }

   // Write the remaining bit if there are any
   if(uiRemainder) 
      {
      ErrRet = pRDIModuleVectorTable->write(tyGlobalArmModule.handle, (void const *) pBuffer ,ulCurrentAddr ,&uiRemainder , (RDI_AccessType)RDIAccess_Code);
      ErrRet =  CheckForFatalRDIError(ErrRet);
      if(ErrRet!=NO_ERROR)
         return ErrRet;
      }

   return NO_ERROR;

}  /* RDITXRXWriteMem */

/****************************************************************************
     Function: RDITXRXReadTimer
     Engineer: Brian Connolly
        Input: pdTime *: storage for time 
                       
       Output: TyError: NO_ERROR or error number
  Description: 
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
0.9.0          9-Nov-2000     BC          rewrite
****************************************************************************/
static  TyError RDITXRXReadTimer(double *pdTime)
{
   *pdTime=1000*ulGlobalExecutionTime;
   return NO_ERROR;
}

/****************************************************************************
     Function: RDITXRXReset
     Engineer: Brian Connolly
        Input: ResetType: type of reset
       Output: TyError: NO_ERROR or error number
  Description: Resets emulator/target 
               we dont care about the reset type requested (RES_EMULATOR/RES_PROCESSOR) 
            both are handled the same at this time

Version        Date           Initials    Description
0.9.0          30-JUN-2000    BC          Initial
0.9.0          07-Nov-2000    BC          rewrite now call reset directly in DLL
1.0.5          14-Nov-2002    CB          This function now behaves as a Soft Reset
****************************************************************************/
static TyError RDITXRXReset(TXRXResetTypes ResetType)
{
   TyError       ErrRet;
   unsigned long ulCPSR    = 0;
   TXRXReg       tyReg     = REG_ARM_CPSR;
   unsigned int  uiStartupType  = 0;
   ProcessorConfig LocalProcConfig;

   // set the mode back to ARM supervisor mode, read current value first
   ErrRet = RDITXRXReadReg(&tyReg, &ulCPSR);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // blank the last eight bits of the CPSR.
   ulCPSR = ulCPSR & TOP_26BIT_MASK;
   // Write the pattern 0x13 into the cpsr to set the mode to 13 (T bit is zeroed above)
   ulCPSR = ulCPSR | ARM_SUPERVISOR_MODE_MASK|INTERRUPTS_DISABLE;
   // write the new CPSR back
   ErrRet = RDITXRXWriteReg(tyReg, ulCPSR);
   if (ErrRet != NO_ERROR)
      return ErrRet;
      
   // get ptr to procCfg
   LocalProcConfig = *(GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig());
   
   uiStartupType = LocalProcConfig.Processor._ARM.uiStartupType; 
   LocalProcConfig.Processor._ARM.uiStartupType = NO_RESET;
   GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig(LocalProcConfig);

   // setup registers
   ErrRet = GetPFWAppObject()->PathFinder.GetProcCfgObject()->SetupRegisterSettings();
   if (ErrRet != NO_ERROR)
      return ErrRet;

   LocalProcConfig.Processor._ARM.uiStartupType = uiStartupType;
   GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig(LocalProcConfig);

   return NO_ERROR;     

}  /* RDITXRXReset */

/****************************************************************************
     Function: RDITXRXHardReset
     Engineer: Colm Burke
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: Does a hard target reset 

Version        Date           Initials    Description
1.0.5          14-Nov-2002    CB          This function performs a Hard Reset
****************************************************************************/
static TyError RDITXRXHardReset(void)
{
   TyError              ErrRet;

   // Since we are recovering from a hard reset, we cannot be in emulation....
   in_ddc = FALSE;

   // set the global variable to report that we are recovering from a hardware reset
   uiGlobalRdiCauseOfBreak = RDIError_Reset;

   // OK, let's close the existing handles we have....
   if (tyGlobalArmModule.handle!=NULL)
      {
      //close the debug module  
      ErrRet = pRDIModuleVectorTable->close(tyGlobalArmModule.handle);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      if (bGloablEtmEnabled)
         {
         ErrRet = pETMVectorTable->close(tyGlobalEtmModule.handle);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
             return ErrRet;
         }
      }

   if (tyGlobalAgent)
      {
      //close the debug module  
      ErrRet = pRDIAgentVectorTable->closeagent(tyGlobalAgent);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }

   tyGlobalArmModule.handle=NULL;
   tyGlobalAgent = NULL;


   ErrRet = CommonResetOrStartupCode();
	if(ErrRet!=NO_ERROR)
	   return ErrRet;

   if (!in_ddc)
		{
		// Setup registers...
		ErrRet = GetPFWAppObject()->PathFinder.GetProcCfgObject()->SetupRegisterSettings();
		if(ErrRet!=NO_ERROR)
		   return ErrRet;

		// Udpate windows etc.
		ErrRet = CleanupAfterExitingEmulation();
		if (ErrRet != NO_ERROR)
		   return ErrRet;
		}

   return NO_ERROR;          
}  /* RDITXRXHardReset */

/****************************************************************************
     Function: RDITXRXReadReg
     Engineer: Brian Connolly
        Input: TXRXReg *pSpecifiedReg : Register to be read (NULL for block read)
               ulong   *pulRegValues  : Pointer to return values
       Output: TyError: NO_ERROR or error number
  Description: Reads registers from emulator.
                  
                    NOTE: ulRegister the Register mask bit 15 is reserved by ARM

Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXReadReg(TXRXReg *pSpecifiedReg,
                               ulong   *pulRegValues)
{
   TyError ErrRet=NO_ERROR;
   unsigned long ulMode=0;
   unsigned long ulRegMask=0;
   unsigned long ulControlProcNum=0;
   boolean       bFoundIt;

   // Is the register a Core Reg, Determine the mode and mask to pass to DLL
   if (GetModeAndMaskForReg(*pSpecifiedReg,&ulMode,&ulRegMask))
      {
      //Read one register
      if (RegisterCacheStruct.bRegisterCacheEnabled)
         {
         ErrRet = GetRegisterCacheData(ulMode, ulRegMask, pulRegValues);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }
      else
         {
         ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle,ulMode,ulRegMask, pulRegValues);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }
      }
   else
      {
      // Not a member of the Core registers, try the peripheral registers
      ErrRet = GetCPNumAndMaskForReg(*pSpecifiedReg, &ulControlProcNum,&ulRegMask, &bFoundIt);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      if (bFoundIt)
         {
         ErrRet = pRDIModuleVectorTable->CPread(tyGlobalArmModule.handle,ulControlProcNum,ulRegMask, pulRegValues);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }   
      else
         {
         // Register not found...
         // should return some error here?

         // Yes, we should Brian.
         ASSERT_NOW();
         }
      }
   
   return NO_ERROR;

}  /* RDITXRXReadReg */

/****************************************************************************
     Function: RDITXRXWriteReg
     Engineer: Brian Connolly
        Input: Reg: register to write to
               Value: value to write
       Output: TyError: NO_ERROR or error number
  Description: Write Value to specified register Reg
               (See comments for RDITXRXReadReg)
Version        Date           Initials    Description           
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXWriteReg(TXRXReg Reg,
                                adtyp   Value)
{
   TyError        ErrRet;
   unsigned long  ulMode=0;
   unsigned long  ulRegMask=0;
   unsigned long  ulControlProcNum=0;
   boolean        bFoundIt;
   
   //is it a Core register
   if (GetModeAndMaskForReg(Reg, &ulMode,&ulRegMask))
      {
      //Write to the register
      ErrRet = pRDIModuleVectorTable->CPUwrite(tyGlobalArmModule.handle,ulMode,ulRegMask,(const unsigned long *)&Value);
      ErrRet =  CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }
   else
      {
      // Not a member of the Core registers, try the peripheral registers...
      ErrRet = GetCPNumAndMaskForReg(Reg, &ulControlProcNum,&ulRegMask, &bFoundIt);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      if (bFoundIt)
         {
         // Write Co-processor register to the register
         ErrRet = pRDIModuleVectorTable->CPwrite(tyGlobalArmModule.handle,ulControlProcNum,ulRegMask,(const unsigned long *)&Value);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if(ErrRet !=NO_ERROR)
            return ErrRet;
         }   
      else   
         {
         //register not found
         ASSERT_NOW();
         }
      }
   return NO_ERROR;

}  /* RDITXRXWriteReg */

/****************************************************************************
     Function: RDITXRXReadPC
     Engineer: Brian Connolly
        Input: pReg: pointer to storage for registers
       Output: TyError: NO_ERROR or error number
  Description: Reads PC from emulator
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXReadPC(ulong *pulRegValues)
{
  TyError ErrRet;
  unsigned int uiRegister= REG_ARM_R15;
  
  ErrRet = RDITXRXReadReg((TXRXReg *)&uiRegister ,&pulRegValues[REG_PC]);
  if(ErrRet!=NO_ERROR)
      return ErrRet;

  return NO_ERROR;
}
/****************************************************************************
     Function: RDITXRXEnDiErBP
     Engineer: MArtin Hannon
        Input: BPAction:breakpoint action
               BPTypes: Type of breakpoint to apply action to
       Output: TyError: NO_ERROR or error number
  Description: Performs specified BP action on BP type
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXEnDiErBP(TXRXBPAction BPAction,
                                TXRXBPTypes  BPTypes)
{
   TyError              ErrRet;
   TyBreakpointElement  tyBreakpointElement;

   if (BPAction == ER_BP)
      {
      switch (BPTypes)
         {
         case START_BP:
         case ALL_BP:
            while(tyBreakpointArray.GetSize())
               {
               tyBreakpointElement = tyBreakpointArray.GetAt(tyBreakpointArray.GetSize()-1);
         
         
               ErrRet = RDITXRXSetClearBP(CLR_BP,
                                          OPCODE_BP,
                                          0xFFFF,
                                          tyBreakpointElement.Address,
                                          1,
                                          0,
                                          tyBreakpointElement.ulDataType,
                                          0);
         
               if (ErrRet != NO_ERROR)
                  return ErrRet;
               }
            tyBreakpointArray.RemoveAll();
            break;
         default:
            ASSERT(FALSE);
            break;
         }
      }

   return NO_ERROR;

}  /* RDITXRXEnDiErBP */

/****************************************************************************
     Function: RDITXRXRepBPStat
     Engineer: Brian Connolly
        Input: pBPStat *:storage for  status
       Output: TyError: NO_ERROR or error number
  Description: Returns BP status 
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXRepBPStat(TXRXBPStatus *pBPStat)
{
   pBPStat->bOpcode        = TRUE;
   pBPStat->bDataRd        = FALSE;
   pBPStat->bDataWr        = FALSE;
   pBPStat->bPage       = FALSE;
   pBPStat->bCoco       = FALSE;
   pBPStat->uCodeBpRAM     = 0;
   pBPStat->uDataRdBpRAM   = 0;
   pBPStat->uDataWrBpRAM   = 0;
   pBPStat->uCocoRAM    = 0;

   return NO_ERROR;
}


/****************************************************************************
     Function: RDITXRXGetBPCount
     Engineer: Martin Hannon
        Input: BPTypes: Type of breakpoint to display
       Output: TyError: NO_ERROR or error number
  Description: Determines if BPs set or cleared in requested range.
Version        Date           Initials    Description
0.9.5-PFPPPC   12-NOV-2001    MOH         Initial
****************************************************************************/
static TyError RDITXRXGetBPCount(unsigned long   *pulCount)
{
   *pulCount = tyBreakpointArray.GetSize();

   return NO_ERROR;
}

/****************************************************************************
     Function: RDITXRXGetBPList
     Engineer: Martin Hannon
        Input: BPTypes: Type of breakpoint to display
       Output: TyError: NO_ERROR or error number
  Description: Determines if BPs set or cleared in requested range.
Version        Date           Initials    Description
0.9.5-PFPPPC   12-NOV-2001    MOH         Initial
****************************************************************************/
static TyError RDITXRXGetBPList(TyBreakpointElement  *ptyBreakpointList)
{
   unsigned long i;

   for (i=0; i < tyBreakpointArray.GetSize(); i++)
      ptyBreakpointList[i] = tyBreakpointArray.GetAt(i);

   return NO_ERROR;
}

/****************************************************************************
     Function: RDITXRXDisplayBP
     Engineer: Brian Connolly
        Input: BPTypes: Type of breakpoint to display
               StartBank: start bank of memory to display from
               StartAddr: start address of memory to display from
               pulLen *: length of memory to display from 
               pbBPSet *: storage for BP status
       Output: TyError: NO_ERROR or error number
  Description: Determines if BPs set or cleared in requested range.
               On return:
                     pulLen is set to length from specified StartAddr that BPs 
                     are of status pbBPSet

                     pulLen can be set to MAX_MEMORY + 1 indicating
                     complete memory space

               Multiple calls may be necessary to determine BP status
               over a desired range.

Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXDisplayBP(TXRXBPTypes BPTypes,
                                 TyBankNo    StartBank,
                                 adtyp       StartAddr,
                                 ulong       *pulLen,
                                 bool        *pbBPSet,
                                 ubyte       *pubCOCOMap)
{
   TyBreakpointElement  tyBreakpointElement;
   unsigned long        ulLength;
   unsigned long        i;
   unsigned long        j;
   bool                 bFirstSet;
   bool                 bNextSet;


   *pbBPSet = FALSE;

   if (pulLen == NULL)
      ulLength = 1;
   else
      ulLength = *pulLen;

   if (BPTypes == OPCODE_BP)
      {
      bFirstSet = FALSE;
      for (i=0; i < tyBreakpointArray.GetSize(); i++)
         {
         tyBreakpointElement = tyBreakpointArray.GetAt(i);
         if ((tyBreakpointElement.Address == StartAddr) && 
             (tyBreakpointElement.ulDataType == 0))
            {
            bFirstSet = TRUE;
            break;
            }
         }

      for (j=4; j < ulLength; j+=4)
         {
         bNextSet = FALSE;
         for (i=0; i < tyBreakpointArray.GetSize(); i++)
            {
            tyBreakpointElement = tyBreakpointArray.GetAt(i);
            if ((tyBreakpointElement.Address == StartAddr+j) &&
               (tyBreakpointElement.ulDataType == 0))
               {
               bNextSet = TRUE;
               break;
               }
            }
         if (bFirstSet != bNextSet)
            break;
         }

      *pbBPSet = bFirstSet;
      if (pulLen)
         *pulLen = min(j, ulLength);
      }

   return NO_ERROR;

}  /* RDITXRXDisplayBP */

/****************************************************************************
     Function: RDITXRXSetClearBP
     Engineer: Brian Connolly
        Input: BPAction:breakpoint action
               BPType: Type of breakpoint to apply action to
               StartBank: start bank of memory to set/clear from
               StartAddr: start address of memory to set/clear from
               Len: length of memory to set/clear 
       Output: TyError: NO_ERROR or error number
  Description: Sets/Clears BP as specified
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
0.9.3          08-Feb-2001    BC          function passed RDI breakpoint types not TXRX
****************************************************************************/
static  TyError RDITXRXSetClearBP(TXRXBPAction   BPAction,
                                  TXRXBPTypes    BPType,
                                  TyBankNo       StartBank,
                                  adtyp          StartAddr,
                                  adtyp          Len,
                                  adtyp          DataValue,    // used for data WPs
                                  uint32         DataType,
                                  unsigned short usCount)
{
   TyError ErrRet = NO_ERROR;
   unsigned long i;
   unsigned long ulLength = Len;
   TyBreakpointElement tyBreakpointElement;
   boolean bFoundPoint = FALSE;
   unsigned int    uiPointType = 0;
   TyAddrRangeAttribute AddrRangeAttr;
   RDI_PointHandle BpPointHandle;

   memset(&tyBreakpointElement,0x0,sizeof(tyBreakpointElement));
   if ((BPType == OPCODE_BP) && (BPAction == SET_BP))
      {
      // OK. It's up to us to decide whether to set a 16 or 32 bit BP.
      // First find out it we have symbolic information...
      ErrRet = IsAddressInARMOrThumbSpace(StartAddr, &AddrRangeAttr);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      if (AddrRangeAttr == CodeRange32Bit)
         {
         BPType = OPCODE_32BIT;
         }
      else if (AddrRangeAttr == CodeRange16Bit)
         {
         BPType = OPCODE_16BIT;
         }
      else
         {
         // We haven't got symbolic info. We need to check if the current
         // instruction is a BX instruction,. If it is, figure out whether
         // it is jumping to ARM or thumb.
         unsigned int      nbytes;
         unsigned char     pucMemoryData[MAX_INSTR_BYTES];
         DisaInst          CurrentInst;
         unsigned long     ulPC       ;

         // Get the current PC
         ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle,USER,R15_MASK,&ulPC);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet !=NO_ERROR)
            return ErrRet;
   
         // Read the memory at the current PC for the next instructions...
         nbytes = MAX_INSTR_BYTES;
         ErrRet = pRDIModuleVectorTable->read(tyGlobalArmModule.handle,ulPC, pucMemoryData,&nbytes, (RDI_AccessType) RDIAccess_Code);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;

         // Disassemble the memory, and determine the destination address .. 
         ErrRet = Disassemble(pucMemoryData,
                              &CurrentInst,
                              0,
                              ulPC,
                              NULL,
                              DISA_FULL,
                              (ProcDisaType)0);
         if (ErrRet != NO_ERROR)
            return ErrRet;

         if (CurrentInst.ucBXInstType == BX_INST_TO_ARM_CODE)
            {
            BPType = OPCODE_32BIT;
            }
         else if (CurrentInst.ucBXInstType == BX_INST_TO_THUMB_CODE)
            {
            BPType = OPCODE_16BIT;
            }
         else
            {
            // The last option is to use the current operating mode...
            if (GetARMOperatingMode())
               BPType = OPCODE_16BIT;
            else 
               BPType = OPCODE_32BIT;
            }
         // Final check if the address is not word-aligned, then this is a thumb instruction...
         if ((StartAddr & 0x3) != 0)
            BPType = OPCODE_16BIT;
         }
      }


   // do any mapping required
   switch (BPType)
      {
      case OPCODE_16BIT:
         uiPointType = RDIPoint_16Bit;
         break;
      case OPCODE_32BIT:
         uiPointType = RDIPoint_EQ;
         break;
      case ARM_DATA_ADDR_WP:
         uiPointType = RDIDataAndAddressPoint;
         break;
      case ARM_DATA_WP:
         uiPointType = RDIDataPoint;
         break;
      default:
         break;
      }

   if ((BPAction != SET_BP) && (BPAction != CLR_BP))
      {
      ASSERT_NOW();
      return NO_ERROR;
      }


   while(ulLength)
      {
      if (BPAction == CLR_BP)
         {
         for (i=0; i < tyBreakpointArray.GetSize(); i++)
            {
            tyBreakpointElement = tyBreakpointArray.GetAt(i);
            //should we also be checking the ulDataType
            if ((tyBreakpointElement.Address == StartAddr)&&(tyBreakpointElement.ulDataType == DataType))
               {
               bFoundPoint=TRUE;
               break;
               }
            }
      
         if( bFoundPoint)
            {
            // If there is a data mask  ->then its a WP
            if (DataType) 
               ErrRet = pRDIModuleVectorTable->clearwatch(tyGlobalArmModule.handle,tyBreakpointElement.ulIdentifier);
            else
               ErrRet = pRDIModuleVectorTable->clearbreak(tyGlobalArmModule.handle,tyBreakpointElement.ulIdentifier);
            ErrRet =  CheckForFatalRDIError(ErrRet);
            if (ErrRet != NO_ERROR)
               return ErrRet;
            tyBreakpointArray.RemoveAt(i);
            }
         }

      if (BPAction == SET_BP)
         {
         // If data is specified what type of watchpoint is being set
         if (DataType)
            {
            // Set the Watchpoint 
            ErrRet=pRDIModuleVectorTable->setwatch(tyGlobalArmModule.handle,
                                             StartAddr,
                                             uiPointType,
                                             DataType,
                                             DataValue,  // for data WPs
                                             0, 
                                             &BpPointHandle);
            }
         else
            {
            // Set the Breakpoint 
            ErrRet=pRDIModuleVectorTable->setbreak(tyGlobalArmModule.handle,
                                             StartAddr,
                                             uiPointType, 
                                             0,
                                             0, 
                                             &BpPointHandle);
            }
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)                    
            return ErrRet;                      
         tyBreakpointElement.BPTypes        = BPType;
         tyBreakpointElement.Address        = StartAddr;
         tyBreakpointElement.ulIdentifier   = BpPointHandle;
         tyBreakpointElement.ulDataType     = DataType;
         tyBreakpointElement.ulDataValue    = DataValue;

         // add the element to the array
         tyBreakpointArray.Add(tyBreakpointElement);
         }
      ulLength--;
      StartAddr+=1;
      bFoundPoint=FALSE;
      }

   return NO_ERROR;

}  /* RDITXRXSetClearBP */

/************************************************************************************
    Function    : RDITXRXGetBPResourceInfo
    engineer    : Brian Conolly
    input/output:
                  StartAddr: address of intrest/ point handle identifier 

                  *Resource :which of the two macrocells is used to set the breakpoint 
                  
                  *bHardware: is the breakpoint hardware/software

    description : called in "viewbps.cpp" where the breakpoint information window
                  is filled to describe the breakpoint type inforamtion, to obtain
                  the required information the dll calls "RDI_PointStatus_Break" and passes
                  it the following parameters
                  
                     arg2 :
                     (arg2 == 0) => software
                     (arg2 == 1) => hardware
                     
                     arg1 :
                     Target specific number identifying the hardware resource of
                     the breakpoint (the point handle to identify the breakpoit). 
                     The target returns -1 if it is a software model (ie. simulator).
                    
                     *bThereAreTwo;
                     Input  True => look for the second BP at an address
                     Output True => two BP's were found at the specified address

Version        Date           Initials    Description           
1.0.3          19-Sep-2001    BC          Initial
************************************************************************************/
static TyError RDITXRXGetBPResourceInfo(adtyp  StartAddr,
                                        bool   *bHardWare,
                                        uint32 *ResourceNumber,
                                        uint32 *DataType,
                                        bool   *bThereAreTwo,
                                        adtyp  *DataValue
                                        )
{
   TyError ErrRet = NO_ERROR;
   unsigned long i=0;
   unsigned long j=0;
   bool bGotTwo = FALSE;
 
   TyBreakpointElement tyBreakpointElement1;
   TyBreakpointElement tyBreakpointElement2;

   memset(&tyBreakpointElement1,0x0,sizeof(tyBreakpointElement1));
   memset(&tyBreakpointElement2,0x0,sizeof(tyBreakpointElement2));

   bool ResType = 0; //initialise to Software
   unsigned long ResNum  = StartAddr; //on input this is the Point handle 
   unsigned long ulNumBPs  = tyBreakpointArray.GetSize(); 
                
   for (i=0; i <ulNumBPs ; i++)
      {
      tyBreakpointElement1 = tyBreakpointArray.GetAt(i);
      if (tyBreakpointElement1.Address == StartAddr)//Ok there is one BP at the address 
         {                                         //lets see if there is another one
         //j=i+1;
         for (j=i+1; j < ulNumBPs; j++)
            {
            tyBreakpointElement2 = tyBreakpointArray.GetAt(j);
            if (tyBreakpointElement2.Address == StartAddr)//Ok there is one BP at the address 
               {
               bGotTwo=TRUE;
               break;
               }
            }
         break;
         }
      }

   if ((*bThereAreTwo)&&(bGotTwo))
      {
      ResNum = tyBreakpointElement2.ulIdentifier;
      }

   if(!*bThereAreTwo)
      {
      ResNum = tyBreakpointElement1.ulIdentifier;
      }

   //get details for a BP  
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIPointStatus_Break,(unsigned long *)&ResNum,(unsigned long *)&ResType);
   if ((ErrRet==RDIError_NoSuchHandle) || (ErrRet == RDIError_NoSuchPoint))
      {
      //if it doesnt exist as a BP see is it a WP  
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIPointStatus_Watch,(unsigned long *)&ResNum,(unsigned long *)&ResType);
      }
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;
   
   
   // Fill out pointers
   *ResourceNumber   = (uint32)ResNum;
   *bHardWare        = ResType;

   if((*bThereAreTwo)&&(bGotTwo))
      {
      *DataType         = tyBreakpointElement2.ulDataType;
      // for data WPs
      *DataValue        = tyBreakpointElement2.ulDataValue;
      }
   else
      {
      *DataType         = tyBreakpointElement1.ulDataType;
      // for data WPs
      *DataValue        = tyBreakpointElement1.ulDataValue;
      }
   
     
   if (*ResourceNumber == 0xffffffff)   //Software model
      *bHardWare = FALSE;  //arg2 

   *bThereAreTwo=bGotTwo;

   return NO_ERROR;

}  /* RDITXRXGetBPResourceInfo */

/****************************************************************************
     Function: RDITXRXRepCauseOfBrk
     Engineer: Brian Connolly
        Input: pCauseOfBrk *:storage for cause of break
       Output: TyError: NO_ERROR or error number
  Description: Reports last cause of break.
Version        Date           Initials    Description
0.9.0          04-JULY-2000      BC          Initial
****************************************************************************/
static TyError RDITXRXRepCauseOfBrk(TXRXCauseOfBreak *pCauseOfBrk)
{
   switch (uiGlobalRdiCauseOfBreak)
      {
      case SINGLE_STEP:
         *pCauseOfBrk=SINGLE_STEP;
         break;
      case RDIError_BreakpointReached:
         *pCauseOfBrk=BP_OPCODE;
         break;
      case RDIError_UndefinedInstruction:
         *pCauseOfBrk=ILLEGAL_OPCODE;
         break;
      case RDIError_TargetStopped:
      case RDIError_UserInterrupt:
         *pCauseOfBrk=USER_HALT;
         break;
      case RDIError_SoftwareInterrupt:
         *pCauseOfBrk=COB_SOFTWARE_INTERRUPT;
         break;
      case RDIError_PrefetchAbort:
         *pCauseOfBrk=COB_PREFETCH_ABORT;
         break;
      case RDIError_DataAbort:
         *pCauseOfBrk=COB_DATA_ABORT;
         break;
      case RDIError_AddressException:
         *pCauseOfBrk=COB_ADDRESS_EXCEPTION;
         break;
      case RDIError_IRQ:
         *pCauseOfBrk=COB_IRQ;
         break;
      case RDIError_FIQ:
         *pCauseOfBrk=COB_FIQ;
         break;
      case RDIError_WatchpointAccessed:
         *pCauseOfBrk=COB_WATCHPOINT;
         break;            
      case RDIError_BranchThrough0:
         *pCauseOfBrk=COB_BRANCH_THROUGH0;
         break;
      case RDIError_Reset:
         *pCauseOfBrk=GEN_TGT_PROC_RST;
         break;
      case RDIError_TriggerOcuured:
         *pCauseOfBrk=STOP_TRIGGER;
         break;
      default:
         *pCauseOfBrk=COB_UNKNOWN;
         break;
      }
   return NO_ERROR;

}  /* RDITXRXRepCauseOfBrk */

/****************************************************************************
     Function: RDIExecContinue
     Engineer: Brian Connolly
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
0.9.0       28-Jun-1998    BC         Initial
****************************************************************************/
static TyError RDIContinue(void)
{
   TyError           ErrRet;
   RDI_PointHandle   PointHandle;

   ErrRet = PrepareForEnteringEmulation();
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // If trace enabled...
   if (bGlobalRdiTraceArmed)
      {
      // Hide the modeless trace find dialog while filling the trace...
      ShowUE32TraceFindDialog(FALSE);
      }

   // Execute, this returns the cause of break...
   ulGlobalExecutionTime = GetTickCount();
   ErrRet = pRDIAgentVectorTable->execute(tyGlobalAgent,&tyGlobalArmModule.handle,FALSE,&PointHandle);
   ulGlobalExecutionTime = GetTickCount() - ulGlobalExecutionTime;
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      goto ErrorHandler;

   if (bGlobalRdiTraceArmed)
      {
      frtype NumFrames;
      ErrRet = TXRXRepTrigTrcStatus(NULL,NULL,NULL,&NumFrames,NULL,NULL,NULL,NULL,NULL,NULL);
      if (ErrRet != NO_ERROR)
         goto ErrorHandler;
      if (NumFrames != 0)
         {
         // We have just halted. If the trace was armed, we need to disassemble
         // the trace....
         if (GetPFWAppObject()->PathFinder.GetProcCfgObject()->UE32TraceSupported())
            {
            ErrRet = ReadAndDisassembleTrace();
            if (ErrRet != NO_ERROR)
               goto ErrorHandler;
            ResetUE32FindTraceContents();
            }
         }
      }
   
   ErrRet = CleanupAfterExitingEmulation();
   if (ErrRet != NO_ERROR)
      return ErrRet;
   return NO_ERROR;

ErrorHandler:
   (void)CleanupAfterExitingEmulation();
   return ErrRet;

}  /* RDIContinue */

/****************************************************************************
     Function: RDIClrBpStepSetBp
     Engineer: Brian Connolly
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
0.9.0       30-Jun-2000    BC          Initial
****************************************************************************/
static TyError RDIClrBpStepSetBp(TyBankNo    StartBank,
                                 adtyp       StartAddr)
{
   TyError         ErrRet = NO_ERROR;
   TyError    FirstErrRet = NO_ERROR; // the first reported error after clearing original BP

   // Remove the Bp
   ErrRet = TXRXSetClearBP(CLR_BP,
                           OPCODE_BP,
                           StartBank,
                           StartAddr,
                           1,
                           0,
                           0,
                           0);
   if (ErrRet != NO_ERROR)                    
      return ErrRet;

   // Write PC to step address
   ErrRet = RDITXRXWriteReg((TXRXReg) REG_PC, StartAddr);
   if (ErrRet != NO_ERROR)
      {
      // store the first reported error
      FirstErrRet = ErrRet;
      goto ErrorHandler;
      }
       
   // Step...
   ErrRet = SingleStep(tyGlobalAgent);
   if (ErrRet != NO_ERROR) 
      {
      // store the first reported error
      FirstErrRet = ErrRet;
      goto ErrorHandler;
      }

ErrorHandler:
   // we always want to try to restore the original Bp
   ErrRet = TXRXSetClearBP(SET_BP,
                           OPCODE_BP,
                           StartBank,
                           StartAddr,
                           1,
                           0,
                           0,
                           0);
   if ( (ErrRet != NO_ERROR) && (FirstErrRet == NO_ERROR) )
      return ErrRet;

   return FirstErrRet;        

}  /* RDIClrBpStepSetBp */

/****************************************************************************
     Function: RDITXRXGoStepHalt
     Engineer: Brian Connolly
        Input: GoStepHaltAction: action to perform
               StartBank: start bank to go/step from
               StartAddr: start address to go/step from
               dTime: time to go for in  units of 1/TXRX_CPU_TIMER_CLK microseconds
                      (only when GoStepHaltAction == GO_FOR_TIME)
       Output: TyError: NO_ERROR or error number
  Description: Go, Step or halt as requested.
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
0.9.1          06-JUN-2006    VH          Fixing problem when performing step over breakpoint
****************************************************************************/
static TyError RDITXRXGoStepHalt(TXRXGoStepHaltAction GoStepHaltAction,
                                 TyBankNo             StartBank,
                                 adtyp                StartAddr,
                                 double               dTime)
{
   TyError           ErrRet;
   bool              bBreakpointSet;
   unsigned long     ulProcessorNum=1;
   static bool		 bHaltInSingleStep = FALSE;                // needs to be static
   
   switch (GoStepHaltAction)
      {
      case GO_FROM_ADDR:
		 // starting execution or single step
		 bHaltInSingleStep = FALSE;
         // Write the PC to the new Start Address
         ErrRet = RDITXRXWriteReg((TXRXReg) REG_PC, StartAddr);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         // Check is there an OP_CODE BP at the address 
         ErrRet = TXRXDisplayBP(OPCODE_BP,
                                StartBank,
                                StartAddr,
                                NULL,
                                &bBreakpointSet,
                                NULL);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         if (bBreakpointSet)
            {
            ErrRet = PrepareForEnteringEmulation();
            if (ErrRet != NO_ERROR)
               return ErrRet;
            // we need to be careful, application could be trapped during single step
            ErrRet = RDIClrBpStepSetBp(StartBank,StartAddr);
            if (ErrRet != NO_ERROR)
               {
               (void)CleanupAfterExitingEmulation();
               return ErrRet;
               }
            // check if user pressed halt during single step
            // if did, do not continue
			if (bHaltInSingleStep)
               {
               bHaltInSingleStep = FALSE;
               if ((ErrRet = CleanupAfterExitingEmulation()) != NO_ERROR)
                  return ErrRet;
               return NO_ERROR;
               }
            }
         // Better setup those triggers....
         ErrRet = RDITXRXSetupTriggerEvents();
		   if (ErrRet != NO_ERROR)
            return ErrRet;

		   ErrRet = RDIContinue();
		   if (ErrRet != NO_ERROR)
            return ErrRet;
         break;
	  case GO_TARGET_RUNNING:
		 ErrRet = RDIContinue();
		 if (ErrRet != NO_ERROR)
		    return ErrRet;
		 break;
      case GO_FROM_RESET:
         ErrRet=RDITXRXReset((TXRXResetTypes) 0);
         if (ErrRet != NO_ERROR)
            return ErrRet;

         // Better setup those triggers....
         ErrRet = RDITXRXSetupTriggerEvents();
		   if (ErrRet != NO_ERROR)
            return ErrRet;

         ErrRet= RDIContinue();
         if (ErrRet != NO_ERROR)
            return ErrRet;
         break;
      case GO_FROM_HARD_RESET:
         ErrRet = TXRXHardReset();
			if (ErrRet != NO_ERROR)
            return ErrRet;

         // Better setup those triggers....
         ErrRet = RDITXRXSetupTriggerEvents();
		   if (ErrRet != NO_ERROR)
            return ErrRet;

         ErrRet= RDIContinue();
         if (ErrRet != NO_ERROR)
            return ErrRet;
         break;
      case STEP_FROM_ADDR:
         // Write the PC to the new Start Address
         ErrRet = RDITXRXWriteReg((TXRXReg) REG_PC, StartAddr);
         if (ErrRet != NO_ERROR)
            return ErrRet;

         // Check is there an OP_CODE BP at the address 
         ErrRet = TXRXDisplayBP(OPCODE_BP,
                                StartBank,
                                StartAddr,
                                NULL,
                                &bBreakpointSet,
                                NULL);
         if (ErrRet != NO_ERROR)
            return ErrRet;

         ErrRet = PrepareForEnteringEmulation();
         if (ErrRet != NO_ERROR)
            return ErrRet;

         if(bBreakpointSet)
            {  
            ErrRet = RDIClrBpStepSetBp(StartBank,StartAddr);
            if (ErrRet != NO_ERROR)
               {
               CleanupAfterExitingEmulation();
               return ErrRet;
               }
            }
         else
            {
            ErrRet = SingleStep(tyGlobalAgent);
            if (ErrRet != NO_ERROR)
               {
               CleanupAfterExitingEmulation();
               return ErrRet;
               }
            }

         if ((ErrRet = CleanupAfterExitingEmulation()) != NO_ERROR)
            return ErrRet;
         break;
      case HALT:
         if (bARMSingleStepInProgress)
	        bHaltInSingleStep = TRUE;
		 ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDISignal_Stop,(unsigned long *)&ulProcessorNum,NULL);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;                      
         uiGlobalRdiCauseOfBreak = RDIError_TargetStopped;
         break;
      default:
         return OPTION_NOT_IMPLEMENTED;
         break;
      }
   return NO_ERROR;

}  /* RDITXRXGoStepHalt */

/****************************************************************************
     Function: RDITXRXGetDwFwVer
     Engineer: Brian Connolly
        Input: pszDiskWare1 *: storage for Diskware1 rev
               pszDiskWare2 *: storage for Diskware2 rev
               pszFirmWare  *: storage for Firmare rev
               pszFpgaWare  *: storage for Fpgaware(s) rev
               
       Output: TyError: NO_ERROR or error number
  Description: Returns strings for DW and FW versions

               do the "Ashling specific" call to genia to retrieve the version numbers
               pass a pointer to the below structure for genia to fill out 

                struct
                  {
                  char szSimVersion[100];
                  char szDiskWare1[100];
                  char szDiskWare2[100];
                  char szFirmWare[100];
                  char szSeaWare[100];
                  char szFpgaWare[100];
                  } TyAshlingVersion;

  
    
Version        Date           Initials    Description
1.0.3          19-Sep-2001    BC          Initial
*****************************************************************************/
static  TyError RDITXRXGetDwFwVer(char          *pszDiskWare1,
                                  char          *pszDiskWare2,
                                  char          *pszFirmWare,
                                  char          *pszSeaWare,
                                  FpgaVerString *pszFpgaWare,
                                  char          *pszSimVersion,
                                  char          *pszUSBCorrectVersion,
                                  char          *pszUSBIncorrectVersion)
{
   TyError ErrRet = NO_ERROR;
#ifdef ARM
   TyAshlingVersion AshlingVersion;
   
   switch (GlobalPathFndrTarget)
      {
      case RDISimulator:
         break;
      default:
         // do the "Ashling specific" call to genia to retrieve the version numbers
         // pass a structure for genia to fill out 
         ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_AshlingVersion,(unsigned long *)&AshlingVersion,NULL);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;

         if ( (strlen(AshlingVersion.szSimVersion) != 0) && (pszSimVersion != NULL) )
            strcpy (pszSimVersion,           AshlingVersion.szSimVersion);
         if ( (strlen(AshlingVersion.szDiskWare1) != 0) && (pszDiskWare1 != NULL) )
            strcpy (pszDiskWare1,            AshlingVersion.szDiskWare1);
         if ( (strlen(AshlingVersion.szDiskWare2) != 0) && (pszDiskWare2 != NULL) )
            strcpy (pszDiskWare2,            AshlingVersion.szDiskWare2);
         if ( (strlen(AshlingVersion.szFirmWare) != 0) && (pszFirmWare != NULL) )
            strcpy (pszFirmWare,             AshlingVersion.szFirmWare);
         if ( (strlen(AshlingVersion.szSeaWare) != 0) && (pszSeaWare != NULL) )
            strcpy (pszSeaWare,              AshlingVersion.szSeaWare);
         if ( (strlen(AshlingVersion.szFpgaWare) != 0) && (pszFpgaWare[0].Version != NULL) )
            strcpy (pszFpgaWare[0].Version,  AshlingVersion.szFpgaWare);
         if ( (strlen(AshlingVersion.szUSBDriverCorrectVersion) != 0) && (pszUSBCorrectVersion != NULL) )
            strcpy (pszUSBCorrectVersion,    AshlingVersion.szUSBDriverCorrectVersion);
         if ( (strlen(AshlingVersion.szUSBDriverInCorrectVersion) != 0) && (pszUSBIncorrectVersion != NULL) )
            strcpy (pszUSBIncorrectVersion,   AshlingVersion.szUSBDriverInCorrectVersion);
         break;
      }
#endif
   
   return ErrRet;

}  /* RDITXRXGetDwFwVer */

/****************************************************************************
     Function: RDITXRXRepEmulStatus
     Engineer: Brian Connolly
        Input: pbInEmulation*: storage for in emulation status
                              Pass NULL if not interested.
       Output: TyError: NO_ERROR or error number
  Description: Reports emulator status. 
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
****************************************************************************/
static  TyError RDITXRXRepEmulStatus(bool *pbInEmulation,
                                     TyEmulStatus *ptyEmulStatus)
{
   if (pbInEmulation != NULL)
      *pbInEmulation = RDIWithinExecContinueLoop() == 1;

   return NO_ERROR;
}

/****************************************************************************
     Function: RDITXRXEnDiTrc
     Engineer: Brian Connolly
        Input: bEnable: TRUE: enable trace
                        FALSE: disable trace
       Output: TyError: NO_ERROR or error number
  Description: Unsupported function (not able to support this using RDI)

Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
*****************************************************************************/
static TyError RDITXRXEnDiTrc(bool bEnable)
{
   TyError       ErrRet;
   unsigned long ulState;

   // If not setup
   if (!pETMVectorTable)
      return NO_ERROR;

   if (bEnable)
      ulState = (unsigned long)RDITrace_Start;
   else
      ulState = (unsigned long)RDITrace_Stop;
   
   ErrRet = pETMVectorTable->info(tyGlobalAgent,RDITrace_Control,(unsigned long *)&ulState, NULL);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   bGlobalRdiTraceArmed = bEnable;

   if (InEmulation())
      {
      if (!bEnable)
         {
         ErrRet = ReadAndDisassembleTrace();
         if (ErrRet != NO_ERROR)
            return ErrRet;
         ResetUE32FindTraceContents();
         }
      else
         {
         // Hide the modeless trace find dialog while filling the trace...
         ShowUE32TraceFindDialog(FALSE);
         }
      }

   return NO_ERROR;

}  /* RDITXRXEnDiTrc */

/****************************************************************************
     Function: RDITXRXReadTrcFrames
     Engineer: Brian Connolly
        Input: StartFrame: start frame to read
               puNumFrames: *number of frames to read (set to actual frames read)
               pFrameBuffer *:storage for frames
       Output: TyError: NO_ERROR or error number
  Description: Unsupported function (not able to support this using RDI)
Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
*****************************************************************************/
#define SYMC_MASK          0x1
#define PIP_STAT_MASK      0x7
#define TRACE_BUS16_MASK   0xffff
#define TRACE_BUS8_MASK    0xff
#define TRACE_BUS4_MASK    0xf

static TyError LocalReadTrcFrames(frtype         StartFrame,
                                    ulong          *puNumFrames,
                                    TyTrFrame huge *pFrameBuffer)
{
   TyError ErrRet = NO_ERROR;
   unsigned char  *pBuffer;
   unsigned long  i=0;
   unsigned long ulNumFramesPerRack;

   unsigned long ulRackNumber=0;
   unsigned long ulRackOffset=0;
   RDI_TraceOffset LocRDI_TraceOffset;
   TyTrFrame LocFrame;
   
   ThreeBusFrames threebusframes;
   FiveBusFrames  fivebusframes;
   EightBusFrames eightbusframes;
   
   /*
      diskware returns a buffer that can be broken down into
      128 bit fields (16 bytes), each of these fileds can be
      described as follows:

      bits    |    Data
      ----------------------------------------------
      127..88 |   Time Stamp (of first frame in field)

// 16 Bit ETM Port
      59...40 |   frame1
      39...20 |   frame2
      19....0 |   frame3

      each frame can be broken down as follows

      bit   |  Data
      ------|--------
      19..4 |  Trace Bus
      3...1 |  Pipe stat
      0     |  Sync

//8 Bit ETM Port
      59...48 |   frame0
      47...36 |   frame1
      35...24 |   frame2
      23...12 |   frame3
      11....0 |   frame4

      each frame can be broken down as follows

      bit   |  Data
      ------|--------
      11..4 |  Trace Bus
       3..1 |  Pipe stat
       0    |  Sync

//4 Bit ETM Port
      63...56 |   frame0
      55...48 |   frame1
      47...40 |   frame1
      39...32 |   frame1
      31...24 |   frame1
      23...16 |   frame2
      15....8 |   frame3
       7....0 |   frame4

      each frame can be broken down as follows
      bit   |  Data
      ------|--------
      7...4 |  Trace Bus
      3...1 |  Pipe stat
      0     |  Sync
   */
   

   if (ulGlobalEtmBusWidth == SIXTEEN_BIT_ETM_BUS)
      ulNumFramesPerRack = 3;
   else if (ulGlobalEtmBusWidth==EIGHT_BIT_ETM_BUS)
      ulNumFramesPerRack = 5;
   else
      ulNumFramesPerRack = 8;

   LocRDI_TraceOffset.framesperrack = ulNumFramesPerRack;
   LocRDI_TraceOffset.start         = StartFrame - (StartFrame % ulNumFramesPerRack);
   LocRDI_TraceOffset.size          = (*puNumFrames / ulNumFramesPerRack) * ulNumFramesPerRack;
   // add an additional 2 racks, just to be sure...
   LocRDI_TraceOffset.size         += 2 * ulNumFramesPerRack;
      
   // Allocate a storage buffer
   pBuffer= (unsigned char *)malloc((LocRDI_TraceOffset.size / ulNumFramesPerRack) * NUM_BYTES_IN_A_RACK);

   ErrRet = pETMVectorTable->info(tyGlobalAgent,RDITrace_Read,(unsigned long *)pBuffer,(unsigned long *)&LocRDI_TraceOffset);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // These will never change...
   LocFrame.BlockNumber  = 0;
   LocFrame.StartTrigger = 0;
   LocFrame.StopTrigger  = 0;
   LocFrame.ErrRet       = 0;

   // Read the racks...
   if (ulGlobalEtmBusWidth==SIXTEEN_BIT_ETM_BUS) //3 frames per Rack
      {
      for (i=0;  i < *puNumFrames; i++)
         {
         ulRackNumber = (i+(StartFrame - LocRDI_TraceOffset.start)) / 3;
         ulRackOffset = (i+(StartFrame - LocRDI_TraceOffset.start)) % 3;

         *(unsigned long *)&threebusframes.pucData[0] = *(unsigned long *)&pBuffer[(ulRackNumber * 16) + 0];
         *(unsigned long *)&threebusframes.pucData[4] = *(unsigned long *)&pBuffer[(ulRackNumber * 16) + 4];
         
         // Get Pipe Stat, Sync, ETM port Data
         switch (ulRackOffset)
            {
            default:
            case 0:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)threebusframes.Bits.ucStat3;       //sync
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)threebusframes.Bits.ucFlags3;  //pipe stat
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS16_MASK&(unsigned short)threebusframes.Bits.usPacket3;
               break;
            case 1:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)threebusframes.Bits.ucStat2;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)threebusframes.Bits.ucFlags2;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS16_MASK&(unsigned short)threebusframes.Bits.usPacket2;
               break;
            case 2:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)threebusframes.Bits.ucStat1;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)threebusframes.Bits.ucFlags1;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS16_MASK&(unsigned short)threebusframes.Bits.usPacket1;
               break;
            }
         // Get Time Stamp
         LocFrame.Frame.RDIFrame.time.lo= *(unsigned long *)&pBuffer[(ulRackNumber * 16)+11];
         LocFrame.Frame.RDIFrame.time.hi= *(unsigned char *)&pBuffer[(ulRackNumber * 16)+15];

         LocFrame.Frame.RDIFrame.time.hi <<= 2;
         LocFrame.Frame.RDIFrame.time.hi |= (LocFrame.Frame.RDIFrame.time.lo >> 30);
         LocFrame.Frame.RDIFrame.time.lo <<= 2;
         LocFrame.Frame.RDIFrame.time.lo += ulRackOffset;

         LocFrame.FrameNumber  = i + StartFrame;
         pFrameBuffer[i]=LocFrame; 
         }
      }
   if (ulGlobalEtmBusWidth==EIGHT_BIT_ETM_BUS) //5 frames per Rack
      {
      for (i=0;  i < *puNumFrames; i++)
         {
         ulRackNumber = (i+(StartFrame - LocRDI_TraceOffset.start)) / 5;
         ulRackOffset = (i+(StartFrame - LocRDI_TraceOffset.start)) % 5;
         
         *(unsigned long *)&fivebusframes.pucData[0] = *(unsigned long *)&pBuffer[(ulRackNumber * 16) + 0];
         *(unsigned long *)&fivebusframes.pucData[4] = *(unsigned long *)&pBuffer[(ulRackNumber * 16) + 4];
         
         //get Pipe Stat, Sync, ETM port Data
         switch (ulRackOffset)
            {
            default:
            case 0:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)fivebusframes.Bits.ucStat5;       //sync
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)fivebusframes.Bits.ucFlags5;  //pipe stat
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS8_MASK&(unsigned short)fivebusframes.Bits.usPacket5;
               break;
            case 1:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)fivebusframes.Bits.ucStat4;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)fivebusframes.Bits.ucFlags4;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS8_MASK&(unsigned short)fivebusframes.Bits.usPacket4;
               break;
            case 2:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)fivebusframes.Bits.ucStat3;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)fivebusframes.Bits.ucFlags3;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS8_MASK&(unsigned short)fivebusframes.Bits.usPacket3;
               break;
            case 3:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)fivebusframes.Bits.ucStat2;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)fivebusframes.Bits.ucFlags2;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS8_MASK&(unsigned short)fivebusframes.Bits.usPacket2;
               break;
            case 4:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)fivebusframes.Bits.ucStat1;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)fivebusframes.Bits.ucFlags1;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS8_MASK&(unsigned short)fivebusframes.Bits.usPacket1;
               break;
            }

         // Get Time Stamp
         LocFrame.Frame.RDIFrame.time.lo = *(unsigned long *)&pBuffer[(ulRackNumber * 16)+11];
         LocFrame.Frame.RDIFrame.time.hi = *(unsigned char *)&pBuffer[(ulRackNumber * 16)+15];

         LocFrame.Frame.RDIFrame.time.hi <<= 2;
         LocFrame.Frame.RDIFrame.time.hi |= (LocFrame.Frame.RDIFrame.time.lo >> 30);
         LocFrame.Frame.RDIFrame.time.lo <<= 2;
         LocFrame.Frame.RDIFrame.time.lo += ulRackOffset;

         LocFrame.FrameNumber  = i + StartFrame;
         pFrameBuffer[i]=LocFrame; 
         }        
      }

   if (ulGlobalEtmBusWidth==FOUR_BIT_ETM_BUS) //8 frames per Rack
      {
      for (i=0;  i < *puNumFrames; i++)
         {
         ulRackNumber = (i+(StartFrame - LocRDI_TraceOffset.start)) / 8;
         ulRackOffset = (i+(StartFrame - LocRDI_TraceOffset.start)) % 8;
         
         *(unsigned long *)&eightbusframes.pucData[0] = *(unsigned long *)&pBuffer[(ulRackNumber * 16) + 0];
         *(unsigned long *)&eightbusframes.pucData[4] = *(unsigned long *)&pBuffer[(ulRackNumber * 16) + 4];
         
         // Get Pipe Stat, Sync, ETM port Data
         switch (ulRackOffset)
            {
            default:
            case 0:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat8;       //sync
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags8;  //pipe stat
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket8;
               break;
            case 1:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat7;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags7;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket7;
               break;
            case 2:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat6;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags6;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket6;
               break;
            case 3:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat5;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags5;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket5;
               break;
            case 4:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat4;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags4;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket4;
               break;
            case 5:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat3;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags3;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket3;
               break;
            case 6:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat2;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags2;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket2;
               break;
            case 7:
               LocFrame.Frame.RDIFrame.event.ucStat =(unsigned char)SYMC_MASK&(unsigned char)eightbusframes.Bits.ucStat1;
               LocFrame.Frame.RDIFrame.event.ucFlags=(unsigned char)PIP_STAT_MASK&(unsigned char)eightbusframes.Bits.ucFlags1;
               LocFrame.Frame.RDIFrame.event.usPacket=(unsigned short)TRACE_BUS4_MASK&(unsigned short)eightbusframes.Bits.usPacket1;
               break;
            }
         // Get Time Stamp
         LocFrame.Frame.RDIFrame.time.lo= *(unsigned long *)&pBuffer[(ulRackNumber * 16)+11];
         LocFrame.Frame.RDIFrame.time.hi= *(unsigned char *)&pBuffer[(ulRackNumber * 16)+15];

         LocFrame.Frame.RDIFrame.time.hi <<= 2;
         LocFrame.Frame.RDIFrame.time.hi |= (LocFrame.Frame.RDIFrame.time.lo >> 30);
         LocFrame.Frame.RDIFrame.time.lo <<= 2;
         LocFrame.Frame.RDIFrame.time.lo += ulRackOffset;

         LocFrame.FrameNumber  = i + StartFrame;
         pFrameBuffer[i]=LocFrame; 
         }            
      }
   free(pBuffer);

   return ErrRet;

}  /* LocalReadTrcFrames */

/****************************************************************************
     Function: RDITXRXReadTrcFrames
     Engineer: Brian Connolly
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: 
Version        Date           Initials    Description
1.0.2          18-Jul-2001    BC          Initial
****************************************************************************/
static TyError RDITXRXReadTrcFrames(frtype         StartFrame,
                                    ulong          *puNumFrames,
                                    TyTrFrame huge *pFrameBuffer)
{
   TyError       ErrRet;
   frtype        ulCurrentStartFrame;
   unsigned long ulFrameBufferIndex;
   unsigned long ulNumberOfFrames;
   unsigned long ulFramesRemaining;

   ulCurrentStartFrame = StartFrame;
   ulFrameBufferIndex  = 0;
   ulFramesRemaining   = *puNumFrames;

   do
      {
      ulNumberOfFrames = min(ulFramesRemaining, MAX_TXCMD_RD_TRC_LEN);

      ErrRet = LocalReadTrcFrames(ulCurrentStartFrame,
                                    &ulNumberOfFrames,
                                    &pFrameBuffer[ulFrameBufferIndex]);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      ulCurrentStartFrame += ulNumberOfFrames;
      ulFrameBufferIndex  += ulNumberOfFrames;
      ulFramesRemaining   -= ulNumberOfFrames;
      }
   while (ulFramesRemaining != 0);

   return NO_ERROR;

}  /* RDITXRXReadTrcFrames */

/****************************************************************************
     Function: RDITXRXRepTrigTrcStatus
     Engineer: Brian Connolly
        Input: pbTrigSpecified *: storage for trigger specified status
                              Pass NULL if not interested.
               pbTraceEnabled *:storage for trace enabled status
                              Pass NULL if not interested.
               pbTraceData *: storage for trace data available status
                              Pass NULL if not interested.
               pTraceBufSize *: storage for trace capacity in frames
                              Pass NULL if not interested.
               puNumBlocks *: storage for number of blocks in current trace data
                              Pass NULL if not interested.
               pNumFrames  *: storage for number of frames in current trace data
                              Pass NULL if not interested.
               puBlockSize *: storage for size (in frames) of blocks in current trace data
                              Pass NULL if not interested.
               puLinesTraced *: storage for lines traced
                              Pass NULL if not interested.
       Output: TyError: NO_ERROR or error number
  Description: Unsupported function (not able to support this using RDI)

Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
*****************************************************************************/
static TyError RDITXRXRepTrigTrcStatus(bool    *pbTrigSpecified,
                                       bool    *pbTraceEnabled,
                                       bool    *pTraceData,
                                       frtype  *pTraceBufSize,
                                       bktype  *pNumBlocks,
                                       frtype  *pNumFrames,
                                       frtype  *pBlockSize,
                                       uint    *puLinesTraced,
                                       frtype  *pNumStarts,
                                       frtype  *pNumStops)
{
   TyError        ErrRet=NO_ERROR;
   TXRXReg        tyEtmRegister;
   unsigned long  ulRegValue;
   bool           bEnabled=FALSE;

   if (pbTraceEnabled)
      {
      if ((ErrRet = RDIIsTraceArmed((BOOL *)&bEnabled)) != NO_ERROR)
         return ErrRet;
      *pbTraceEnabled = bEnabled;
      }

   if (pTraceBufSize)
      {
      *pTraceBufSize = 0xFFFF;

	   tyEtmRegister = REG_ARM_ETM_BANK0_R1;
      ErrRet = TXRXReadETMReg(&tyEtmRegister,&ulRegValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      // If the register value comes back as 0, we assume that ETM is not present....
      if (ulRegValue == 0x0)
         *pTraceBufSize = 0x0;
      }

   if (pNumFrames)
      {
      if ((ErrRet = RDIGetTraceFramesCount((unsigned int *)pNumFrames)) != NO_ERROR)
         return ErrRet;
      }

   if (pTraceData)
      {
      frtype NumFrames;
      if (!pNumFrames)
         {
         if ((ErrRet = RDIGetTraceFramesCount((unsigned int *)&NumFrames)) != NO_ERROR)
            return ErrRet;
         }
      else
         NumFrames = *pNumFrames;

      if (NumFrames)
         *pTraceData = TRUE;
      else
         *pTraceData = FALSE;
      }

   if (pNumBlocks)
      *pNumBlocks = 0;

   if (pBlockSize)
      *pBlockSize = 0xFFFF;

   return NO_ERROR;

}  /* RDITXRXRepTrigTrcStatus */

/****************************************************************************
     Function: RDIGetTraceFramesCount
     Engineer: Brian Connolly
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
1.0.1       27-Mar-2001     BC          Initial
****************************************************************************/
TyError RDIGetTraceFramesCount(unsigned int *pulNumberOfFrames)
{
   TyError ErrRet;
   RDI_TraceBufferExtentAction Action =  RDITrace_GetHigh;

   // Read the max num frames traced...
   ErrRet = pETMVectorTable->info(tyGlobalAgent,RDITrace_BufferExtent,(unsigned long *)&Action,(unsigned long *)pulNumberOfFrames);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   return NO_ERROR;
}

/****************************************************************************
     Function: RDIIsTraceArmed
     Engineer: Martin Hannon
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
0.9.0       27-Mar-2001    BC         Initial
****************************************************************************/
static TyError RDIIsTraceArmed(BOOL *pbTraceArmed)
{
   *pbTraceArmed = bGlobalRdiTraceArmed;
   return NO_ERROR;
}

/****************************************************************************
     Function: RDITXRXSetupBanking
     Engineer: Brian Connolly
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: Unsupported function (not able to support this using RDI)

Version        Date           Initials    Description
0.9.0          18-APR-2000    BC          Initial
*****************************************************************************/
static TyError RDITXRXSetupBanking(void *pBankConfig)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: GetRDiEndianess
     Engineer: Brian Connolly
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: return the Endianess of the system

Version        Date            Initials    Description
0.9.0          04-JULY-2000    BC          Initial
*****************************************************************************/
extern "C" TyError GetRDIEndianess(ARMword *pEndianess)
{
   *pEndianess=EndianessType;
   return NO_ERROR;
}
/****************************************************************************
     Function: GetCPNumAndMaskForReg
     Engineer: Brian Connolly
        Input: enumerate register, pointer 
       Output: boolean -> TRUE=found register
  Description: returns a Coprocessor Number and an acess mask 

Version        Date            Initials    Description
0.9.3          15-Feb-2001     BC          Initial
1.0.5          20-Oct-2002     PJB         Added ARM946 support
1.0.8           3-Jul-2003    PJB         Added 940/926 support
*****************************************************************************/
TyError GetCPNumAndMaskForReg(TXRXReg Register, unsigned long *pNum,unsigned long *pRegMask, boolean *pbFoundIt)
{
   TyError        ErrRet;
   unsigned long  ulCacheSelected                = 0x0;
   unsigned long  ulCacheSelectedSet             = 0x0;
   unsigned long  ulCurrentMemoryAreaSelected    = 0x0;
   unsigned long  ulCurrentMemoryAreaSelectedSet = 0x0;


   *pbFoundIt = TRUE;

   switch (GlobalProcType)
      {
      case ARM7DI  :
      case ARM7TDMI:
      case ARM9TDMI:
      default:
           *pbFoundIt=FALSE;
           break;

      case ARM720T: 
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               break;
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R1_MASK;
               break;
               
            case  REG_CP15_R2   : 
               *pNum=CP15;
               *pRegMask=R2_MASK;
               break;
               
            case  REG_CP15_R3   :
               *pNum=CP15;
               *pRegMask=R3_MASK;
               break;
               
            case  REG_CP15_R4   : 
               *pNum=CP15;
               *pRegMask=R5_MASK;
               break;
               
            case  REG_CP15_R5   :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               break;
               
            case  REG_CP15_R6   : 
               *pNum=CP15;
               *pRegMask=R13_MASK;
               break;
               
            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;

      case ARM740T: 
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               break;
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R1_MASK;
               break;
               
            case  REG_CP15_R2   : 
               *pNum=CP15;
               *pRegMask=R2_MASK;
               break;
               
            case  REG_CP15_R3   :
               *pNum=CP15;
               *pRegMask=R3_MASK;
               break;
               
            case  REG_CP15_R4   : 
               *pNum=CP15;
               *pRegMask=R5_MASK;
               break;
               
            case  REG_CP15_R5   :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               break;
               
            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;


      case ARM920T: 
      case ARM922T: 
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 0;
               break;       
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R2   : 
               *pNum=CP15;
               *pRegMask=R1_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R3   :
               *pNum=CP15;
               *pRegMask=R2_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R4   : 
               *pNum=CP15;
               *pRegMask=R2_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R5   :
               *pNum=CP15;
               *pRegMask=R3_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R6   : 
               *pNum=CP15;
               *pRegMask=R3_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R7   :
               *pNum=CP15;
               *pRegMask=R5_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R8   : 
               *pNum=CP15;
               *pRegMask=R5_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R9   :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R10  : 
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R11  :
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R12  : 
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R13  :
               *pNum=CP15;
               *pRegMask=R10_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R14  : 
               *pNum=CP15;
               *pRegMask=R10_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R15  :
               *pNum=CP15;
               *pRegMask=R13_MASK;
               ulCacheSelected = 0;
               break;

            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;

      case ARM926EJS: 
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 0;
               break;       
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R2   : 
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 2;
               break;
               
            case  REG_CP15_R3   :
               *pNum=CP15;
               *pRegMask=R1_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R4   : 
               *pNum=CP15;
               *pRegMask=R2_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R5   :
               *pNum=CP15;
               *pRegMask=R3_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R6   : 
               *pNum=CP15;
               *pRegMask=R5_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R7   :
               *pNum=CP15;
               *pRegMask=R5_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R8   : 
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R9  : 
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R10  :
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R11  : 
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 2;
               break;
               
            case  REG_CP15_R12  :
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 3;
               break;
               
            case  REG_CP15_R13  : 
               *pNum=CP15;
               *pRegMask=R10_MASK;
               ulCacheSelected = 0;
               break;
               
            case  REG_CP15_R14  :
               *pNum=CP15;
               *pRegMask=R13_MASK;
               ulCacheSelected = 0;
               break;

            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;

      case ARM940T: 
      case ARM946ES: 
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;       
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCurrentMemoryAreaSelected = 0;
               ulCacheSelected = 1;
               break;
               
            case  REG_CP15_R2   : 
               *pNum=CP15;
               *pRegMask=R0_MASK;
               ulCacheSelected = 2;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R3   :
               *pNum=CP15;
               *pRegMask=R1_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R4   : 
               *pNum=CP15;
               *pRegMask=R2_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R5   :
               *pNum=CP15;
               *pRegMask=R2_MASK;
               ulCacheSelected = 1;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R6   : 
               *pNum=CP15;
               *pRegMask=R3_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R7   :
               *pNum=CP15;
               *pRegMask=R5_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R8   : 
               *pNum=CP15;
               *pRegMask=R5_MASK;
               ulCacheSelected = 1;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R9   :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            case  REG_CP15_R10  : 
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 1;
               break;
               
            case  REG_CP15_R11  :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 2;
               break;
               
            case  REG_CP15_R12  : 
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 3;
               break;
               
            case  REG_CP15_R13  :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 4;
               break;
               
            case  REG_CP15_R14  : 
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 5;
               break;
               
            case  REG_CP15_R15  :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 6;
               break;

            case  REG_CP15_R16  :
               *pNum=CP15;
               *pRegMask=R6_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 7;
               break;

            case  REG_CP15_R17  :
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 0;
               ulCurrentMemoryAreaSelected = 0;
               break;

            case  REG_CP15_R18  :
               *pNum=CP15;
               *pRegMask=R9_MASK;
               ulCacheSelected = 1;
               ulCurrentMemoryAreaSelected = 0;
               break;
               
            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;
      
      case ARM966ES:
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               break;
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R1_MASK;
               break;
               
            case  REG_CP15_R2   : 
               *pNum=CP15;
               *pRegMask=R15_MASK;
               break;
               
            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;
         
      case ARM968ES:
         switch(Register)
            {
            case  REG_CP15_R0   :
               *pNum=CP15;
               *pRegMask=R0_MASK;
               break;
               
            case  REG_CP15_R1   :
               *pNum=CP15;
               *pRegMask=R1_MASK;
               break;
                          
            default :
               *pbFoundIt=FALSE;
               break;
            }
         break;

      break;
      }

      
   if (bGlobalCacheSelection) //Processor supports Register cache selection
      {            
      //write
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_SetCP15CacheSelected,&ulCacheSelected,NULL);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      //read back to check
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_GetCP15CacheSelected,&ulCacheSelectedSet,NULL);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      // Problem ?
      if( ulCacheSelected != ulCacheSelectedSet)
         *pbFoundIt=FALSE;
      }

   if (bGlobalMemoryRegionSelection) //Processor supports Register cache selection
      {            
      //write
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_SetCP15CurrentMemoryArea,&ulCurrentMemoryAreaSelected,NULL);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      //read back to check
      ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_GetCP15CurrentMemoryArea,&ulCurrentMemoryAreaSelectedSet,NULL);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      // Problem ?
      if( ulCurrentMemoryAreaSelected != ulCurrentMemoryAreaSelectedSet)
         *pbFoundIt=FALSE;
      }

   return NO_ERROR;

}  /* GetCPNumAndMaskForReg */

/****************************************************************************
     Function: GetModeAndMaskForReg
     Engineer: Brian Connolly
        Input: enumerate register, pointer 
       Output: boolean -> TRUE=found register
  Description: returns a register mode and a mask from the enumerate type

Version        Date            Initials    Description
0.9.0          09-Nov-2000     BC          Initial
*****************************************************************************/
static boolean GetModeAndMaskForReg(TXRXReg Register, unsigned long *pMode,unsigned long *pRegMask)
{
   unsigned long pulRegisterMasks[] = {R0_MASK,
                                       R1_MASK,
                                       R2_MASK,
                                       R3_MASK,
                                       R4_MASK,
                                       R5_MASK,
                                       R6_MASK,
                                       R7_MASK,
                                       R8_MASK,
                                       R9_MASK,
                                       R10_MASK,
                                       R11_MASK,
                                       R12_MASK,
                                       R13_MASK,
                                       R14_MASK,
                                       R15_MASK};

   if ((Register >= REG_ARM_R0) && (Register <= REG_ARM_R15))
      {
      *pMode    = USER;
      *pRegMask = pulRegisterMasks[Register];
      }
   else
      {
      switch(Register)
         {
         case  REG_ARM_CPSR : 
            *pMode=USER;
            *pRegMask=R16_MASK;
            break;
         case  REG_ARM_R8_FIQ  :
            *pMode=FIQ;
            *pRegMask=R8_MASK;
            break;
         case  REG_ARM_R9_FIQ  : 
            *pMode=FIQ;
            *pRegMask=R9_MASK;
            break;
         case  REG_ARM_R10_FIQ :  
            *pMode=FIQ;
            *pRegMask=R10_MASK;
            break;
         case  REG_ARM_R11_FIQ :
            *pMode=FIQ;
            *pRegMask=R11_MASK;
            break;
         case  REG_ARM_R12_FIQ :    
            *pMode=FIQ;
            *pRegMask=R12_MASK;
            break;
         case  REG_ARM_R13_FIQ :
            *pMode=FIQ;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_FIQ :  
            *pMode=FIQ;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_FIQ :
            *pMode=FIQ;
            *pRegMask=R17_MASK;
            break;
         case  REG_ARM_R13_SVC :  
            *pMode=SUPERVISOR;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_SVC :
            *pMode=SUPERVISOR;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_SVC :  
            *pMode=SUPERVISOR;
            *pRegMask=R17_MASK;
            break;
         case  REG_ARM_R13_ABT :
            *pMode=ABORT;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_ABT :  
            *pMode=ABORT;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_ABT :
            *pMode=ABORT;
            *pRegMask=R17_MASK;
            break;
         case  REG_ARM_R13_IRQ :  
            *pMode=IRQ;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_IRQ :
            *pMode=IRQ;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_IRQ :  
            *pMode=IRQ;
            *pRegMask=R17_MASK;
            break;
         case  REG_ARM_R13_UND :
            *pMode=UNDEF;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_UND :  
            *pMode=UNDEF;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_UND :
            *pMode=UNDEF;
            *pRegMask=R17_MASK;
            break;
         case  REG_ARM_R13_SYS :
            *pMode=SYSTEM;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_SYS :  
            *pMode=SYSTEM;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_SYS :
            *pMode=SYSTEM;
            *pRegMask=R17_MASK;
            break;
         case  REG_ARM_R13_USR :
            *pMode=USER;
            *pRegMask=R13_MASK;
            break;
         case  REG_ARM_R14_USR :  
            *pMode=USER;
            *pRegMask=R14_MASK;
            break;
         case  REG_ARM_SPSR_USR :
            *pMode=USER;
            *pRegMask=R17_MASK;
            break;
         case  REG_THUMB_SP :  
            *pMode=USER;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR :
            *pMode=USER;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_PC :  
            *pMode=USER;
            *pRegMask=R15_MASK;
            break;
         case  REG_THUMB_SP_FIQ :
            *pMode=FIQ;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_FIQ :  
            *pMode=FIQ;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_SP_SVC :
            *pMode=SUPERVISOR;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_SVC :  
            *pMode=SUPERVISOR;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_SP_ABT :
            *pMode=ABORT;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_ABT :  
            *pMode=ABORT;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_SP_IRQ :
            *pMode=IRQ;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_IRQ :  
            *pMode=IRQ;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_SP_UND :
            *pMode=UNDEF;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_UND :  
            *pMode=UNDEF;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_SP_SYS :
            *pMode=SYSTEM;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_SYS :  
            *pMode=SYSTEM;
            *pRegMask=R14_MASK;
            break;
         case  REG_THUMB_SP_USR :
            *pMode=USER;
            *pRegMask=R13_MASK;
            break;
         case  REG_THUMB_LR_USR :  
            *pMode=USER;
            *pRegMask=R14_MASK;
            break;
         default :
            return FALSE;
            break;
         }
      }

   return TRUE;

}  /* GetModeAndMaskForReg */

/****************************************************************************
     Function: GetModeAndMaskForETMReg
     Engineer: Brian Connolly
        Input: enumerate register, pointer 
       Output: boolean -> TRUE=found register
  Description: returns a register mode and a mask from the enumerate type
               
               mode -> bank to access
               mask -> register in mask

Version        Date            Initials    Description
1.0.1          18-Apr-2001     BC          Initial
*****************************************************************************/
static boolean GetModeAndMaskForETMReg(TXRXReg Register, unsigned long *pMode,unsigned long *pRegMask)
{
   unsigned long pulRegisterMasks[] = {R0_ETM_MASK,
                                       R1_ETM_MASK,
                                       R2_ETM_MASK,
                                       R3_ETM_MASK,
                                       R4_ETM_MASK,
                                       R5_ETM_MASK,
                                       R6_ETM_MASK,
                                       R7_ETM_MASK,
                                       R8_ETM_MASK,
                                       R9_ETM_MASK,
                                       R10_ETM_MASK,
                                       R11_ETM_MASK,
                                       R12_ETM_MASK,
                                       R13_ETM_MASK,
                                       R14_ETM_MASK,
                                       R15_ETM_MASK};

   if ((Register >= REG_ARM_ETM_BANK0_R0) && (Register <= REG_ARM_ETM_BANK0_R15))
      {
      *pMode    = BANK0;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK0_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK1_R0) && (Register <= REG_ARM_ETM_BANK1_R15))
      {
      *pMode    = BANK1;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK1_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK2_R0) && (Register <= REG_ARM_ETM_BANK2_R15))
      {
      *pMode    = BANK2;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK2_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK3_R0) && (Register <= REG_ARM_ETM_BANK3_R15))
      {
      *pMode    = BANK3;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK3_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK4_R0) && (Register <= REG_ARM_ETM_BANK4_R15))
      {
      *pMode    = BANK4;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK4_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK5_R0) && (Register <= REG_ARM_ETM_BANK5_R11))
      {
      *pMode    = BANK5;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK5_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK6_R0) && (Register <= REG_ARM_ETM_BANK6_R11))
      {
      *pMode    = BANK6;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK6_R0];
      }
   else if ((Register >= REG_ARM_ETM_BANK7_R0) && (Register <= REG_ARM_ETM_BANK7_R7))
      {
      *pMode    = BANK7;
      *pRegMask = pulRegisterMasks[Register - REG_ARM_ETM_BANK7_R0];
      }
   else
      return FALSE;

   return TRUE;

}  /* GetModeAndMaskForETMReg */

/****************************************************************************
     Function: GetMemoryCacheData
     Engineer: Martin Hannon
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: 

Version        Date           Initials    Description
*****************************************************************************/
static TyError GetMemoryCacheData(unsigned long ulStartAddr,
                                  unsigned long ulReadLen,  
                                  unsigned char *pucBuffer)
{
   TyError ErrRet = NO_ERROR;
   unsigned long CacheIndex=0;
   unsigned long i=0;
   BOOL bNeedtoRefillCache=FALSE;

   // If the cache is invalid...
   if (!MemoryCacheStruct.bMemoryCacheValid)
      {
      // Invalidate all the cache frames...
      for (CacheIndex=0; CacheIndex < MEMORY_CACHE_SIZE; CacheIndex++)
         MemoryCacheStruct.pucCacheAddress[CacheIndex] = CacheIndex+1;

      MemoryCacheStruct.bMemoryCacheValid = TRUE;
      }

   bNeedtoRefillCache = FALSE;

   for (i=0; i < ulReadLen; i++)
      {
      if ((ulStartAddr+i) != MemoryCacheStruct.pucCacheAddress[(ulStartAddr+i) & MEMORY_CACHE_MASK])
         {
         bNeedtoRefillCache = TRUE;
         break;
         }
      }

   if (bNeedtoRefillCache)
      {
      unsigned int uiReadLen;

      uiReadLen = max(READ_MEMORY_SIZE, ulReadLen);

      ErrRet = pRDIModuleVectorTable->read(tyGlobalArmModule.handle,
                                    ulStartAddr,
                                    pucGlobalBuffer,
                                    &uiReadLen,
                                    (RDI_AccessType) RDIAccess_Code);
      
      if (ErrRet != RDIError_DataAbort)
         {
         ErrRet =  CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
             return ErrRet;
         }

      for (i=0; i < (unsigned long)uiReadLen; i++)
         {
         MemoryCacheStruct.pucCacheMemory [(i+ulStartAddr) & MEMORY_CACHE_MASK] = pucGlobalBuffer[i];
         MemoryCacheStruct.pucCacheAddress[(i+ulStartAddr) & MEMORY_CACHE_MASK] = i+ulStartAddr;
         }
      }

   for (i=0; i < ulReadLen; i++)
      pucBuffer[i] = MemoryCacheStruct.pucCacheMemory[(ulStartAddr + i) & MEMORY_CACHE_MASK];

   return ErrRet;

}  /* GetMemoryCacheData */

/****************************************************************************
     Function: GetRegisterCacheData
     Engineer: Martin Hannon
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
0.9.0       5-Nov-1998     MOH         Initial
****************************************************************************/
static TyError GetRegisterCacheData(unsigned long ulMode,
                                    unsigned long ulMask,
                                    unsigned long *pulRegValue)
{
   TyError       ErrRet=NO_ERROR;
   unsigned long ulRegisterArrayIndex=0;

   switch (ulMask)
      {
      case R0_MASK  :
         ulRegisterArrayIndex = 0;
         break;
      case R1_MASK  :
         ulRegisterArrayIndex = 1;
         break;
      case R2_MASK  :
         ulRegisterArrayIndex = 2;
         break;
      case R3_MASK  :
         ulRegisterArrayIndex = 3;
         break;
      case R4_MASK  :
         ulRegisterArrayIndex = 4;
         break;
      case R5_MASK  :
         ulRegisterArrayIndex = 5;
         break;
      case R6_MASK  :
         ulRegisterArrayIndex = 6;
         break;
      case R7_MASK  :
         ulRegisterArrayIndex = 7;
         break;
      case R8_MASK  :
         ulRegisterArrayIndex = 8;
         break;
      case R9_MASK  :
         ulRegisterArrayIndex = 9;
         break;
      case R10_MASK :
         ulRegisterArrayIndex = 10;
         break;
      case R11_MASK :
         ulRegisterArrayIndex = 11;
         break;
      case R12_MASK :
         ulRegisterArrayIndex = 12;
         break;
      case R13_MASK :
         ulRegisterArrayIndex = 13;
         break;
      case R14_MASK :
         ulRegisterArrayIndex = 14;
         break;
      case R15_MASK :
         ulRegisterArrayIndex = 15;
         break;
      case R16_MASK :
         ulRegisterArrayIndex = 16;
         break;
      default :
         ASSERT(FALSE);
         break;
      case R17_MASK :
         ulRegisterArrayIndex = 17;
         break;
      }

   switch (ulMode)
      {
      case USER        :
      case SYSTEM      :
         if (!RegisterCacheStruct.bUSER)
            {
            ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, RegisterCacheStruct.pulUSER);
            ErrRet = CheckForFatalRDIError(ErrRet);
            if (ErrRet !=NO_ERROR)
               return ErrRet;
            }
         RegisterCacheStruct.bUSER = TRUE;
         *pulRegValue = RegisterCacheStruct.pulUSER[ulRegisterArrayIndex];
         break;
      case FIQ         :
         if (!RegisterCacheStruct.bFIQ)
            {
            ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, RegisterCacheStruct.pulFIQ);
            ErrRet = CheckForFatalRDIError(ErrRet);
            if (ErrRet !=NO_ERROR)
               return ErrRet;
            }
         RegisterCacheStruct.bFIQ = TRUE;
         *pulRegValue = RegisterCacheStruct.pulFIQ[ulRegisterArrayIndex];
         break;
      case IRQ         :
         if (!RegisterCacheStruct.bIRQ)
            {
            ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, RegisterCacheStruct.pulIRQ);
            ErrRet = CheckForFatalRDIError(ErrRet);
            if (ErrRet !=NO_ERROR)
               return ErrRet;
            }
         RegisterCacheStruct.bIRQ = TRUE;
         *pulRegValue = RegisterCacheStruct.pulIRQ[ulRegisterArrayIndex];
         break;
      case SUPERVISOR  :
         if (!RegisterCacheStruct.bSUPERVISOR)
            {
            ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, RegisterCacheStruct.pulSUPERVISOR);
            ErrRet = CheckForFatalRDIError(ErrRet);
            if (ErrRet !=NO_ERROR)
               return ErrRet;
            }
         RegisterCacheStruct.bSUPERVISOR = TRUE;
         *pulRegValue = RegisterCacheStruct.pulSUPERVISOR[ulRegisterArrayIndex];
         break;
      case ABORT       :
         if (!RegisterCacheStruct.bABORT)
            {
            ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, RegisterCacheStruct.pulABORT);
            ErrRet = CheckForFatalRDIError(ErrRet);
            if (ErrRet !=NO_ERROR)
               return ErrRet;
            }
         RegisterCacheStruct.bABORT = TRUE;
         *pulRegValue = RegisterCacheStruct.pulABORT[ulRegisterArrayIndex];
         break;
      default          :
         ASSERT(FALSE);
         break;
      case UNDEF:
         if (!RegisterCacheStruct.bUNDEFINED)
            {
            ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, RegisterCacheStruct.pulUNDEFINED);
            ErrRet = CheckForFatalRDIError(ErrRet);
            if (ErrRet !=NO_ERROR)
               return ErrRet;
            }
         RegisterCacheStruct.bUNDEFINED = TRUE;
         *pulRegValue = RegisterCacheStruct.pulUNDEFINED[ulRegisterArrayIndex];
         break;
      }

   return NO_ERROR;

}  /* GetRegisterCacheData */

/****************************************************************************
     Function: RDITXRXReadRegisters
     Engineer: Brian Connolly
               ulong   *pulRegValues  : Pointer to return values
       Output: TyError: NO_ERROR or error number
  Description: read the registers that are vaild in the current Processor mode 

               to speedup operation read registers from emulator before disassembler is called,
               then pass the registers to the disassembler
               called from "\common\assembly.cpp"

Version        Date           Initials    Description
0.9.2          22-Jan-2001    BC          Initial
****************************************************************************/
static TyError RDITXRXReadRegisters(ulong *pulRegValues)
{
   TyError            ErrRet         = NO_ERROR;
   unsigned int  iValue;
   unsigned long ulMode;
   unsigned long CPSR;


   // Read the CPSR register  in USER MODE
   iValue = REG_ARM_CPSR;  
   ErrRet = TXRXReadReg((TXRXReg *)&iValue,&CPSR);
   if (ErrRet != NO_ERROR)
     return ErrRet;

   //determine the mode from the register vaule
   ulMode = CPSR & REGISTER_MASK;

   // Read registers via CPU read for all regs in that mode...
   ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle, ulMode, MAX_REGISER_MASK, pulRegValues);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   return NO_ERROR;

}  /* RDITXRXReadRegisters */

/****************************************************************************
     Function: SingleStep
     Engineer: Brian Connolly
        Input:        
       Output: TyError: NO_ERROR or error number
  Description: Single steps the program


Version        Date           Initials    Description
0.9.3          07-Feb-2001    BC          Initial
1.0.2          02-Jul-2001    PJB         Remove first BP if second BP cannot be set.
****************************************************************************/
TyError SingleStep(RDI_AgentHandle agent)
{
   TyError           ErrRet;
   unsigned int      nbytes;
   unsigned char     pucMemoryData[MAX_INSTR_BYTES];
   DisaInst          CurrentInst;
   unsigned long     ulPC       ;
   RDI_PointHandle   PointHandle;
   unsigned long     ulFirstAddress;
   unsigned long     ulSecondAddress;
   unsigned char     bFirstAddressBPSet  = FALSE;
   unsigned char     bSecondAddressBPSet = FALSE;
   TXRXBPTypes       tyFirstBPType       = OPCODE_BP;
   TXRXBPTypes       tySecondBPType      = OPCODE_BP;
   bool              bHasBPAtAddr        = FALSE;
   TyAddrRangeAttribute AddrRangeAttr;
   
   // set global flag 
   bARMSingleStepInProgress = TRUE;

   // Get the current PC
   ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule.handle,USER,R15_MASK,&ulPC);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet !=NO_ERROR)
      {
      bARMSingleStepInProgress = FALSE;
      return ErrRet;
      }

   
   // Read the memory at the current PC for the next instructions...
   nbytes = MAX_INSTR_BYTES;
   ErrRet = pRDIModuleVectorTable->read(tyGlobalArmModule.handle,ulPC, pucMemoryData,&nbytes, (RDI_AccessType) RDIAccess_Code);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      {
      bARMSingleStepInProgress = FALSE;
      return ErrRet;
      }

   // Disassemble the memory, and determine the destination address .. 
   ErrRet = Disassemble(pucMemoryData,
                        &CurrentInst,
                        0,
                        ulPC,
                        NULL,
                        DISA_STEPPING,
                        (ProcDisaType)0);
   if (ErrRet != NO_ERROR)
      {
      bARMSingleStepInProgress = FALSE;
      return ErrRet;
      }

   ulFirstAddress  = ulPC + CurrentInst.InstLen;
   ulSecondAddress = CurrentInst.DestAddr;

   if (CurrentInst.ucBXInstType == BX_INST_TO_ARM_CODE)
      {
      tyFirstBPType   = OPCODE_32BIT;
      tySecondBPType  = OPCODE_32BIT;
      }
   else if (CurrentInst.ucBXInstType == BX_INST_TO_THUMB_CODE)
      {
      tyFirstBPType   = OPCODE_16BIT;
      tySecondBPType  = OPCODE_16BIT;
      }
   else
      {
      // The last option is to use the current operating mode...
      if (GetARMOperatingMode())
         {
         tyFirstBPType   = OPCODE_16BIT;
         tySecondBPType  = OPCODE_16BIT;
         }
      else 
         {
         tyFirstBPType   = OPCODE_32BIT;
         tySecondBPType  = OPCODE_32BIT;
         }
      }
   // Final check if the address is not word-aligned, then this is a thumb instruction...
   if ((ulFirstAddress & 0x3) != 0)
      tyFirstBPType   = OPCODE_16BIT;
   if ((ulSecondAddress & 0x3) != 0)
      tySecondBPType   = OPCODE_16BIT;

   // OK, set the first BP....
   ErrRet = IsAddressInARMOrThumbSpace(ulFirstAddress, &AddrRangeAttr);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   if (AddrRangeAttr != ConstDataRange)
      {
      ErrRet = TXRXDisplayBP(OPCODE_BP,
                             DEFAULT_COMMON_BANK,
                             ulFirstAddress,
                             NULL,
                             &bHasBPAtAddr,
                             NULL);
      if (ErrRet != NO_ERROR)
         {
         bARMSingleStepInProgress = FALSE;
         return ErrRet;
         }


      if (!bHasBPAtAddr)
         {
         ErrRet = TXRXSetClearBP(SET_BP,
                                 tyFirstBPType,
                                 DEFAULT_COMMON_BANK,
                                 ulFirstAddress,
                                 1,
                                 0,
                                 0,
                                 0);
         if (ErrRet != NO_ERROR)
            {
            bARMSingleStepInProgress = FALSE;
            return ErrRet;
            }


         bFirstAddressBPSet = TRUE;
         }
      }

   // Set the second BP if we need to...
   if (ulFirstAddress != ulSecondAddress)
      {
      ErrRet = IsAddressInARMOrThumbSpace(ulSecondAddress, &AddrRangeAttr);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      if (AddrRangeAttr != ConstDataRange)
         {
         ErrRet = TXRXDisplayBP(OPCODE_BP,
                                DEFAULT_COMMON_BANK,
                                ulSecondAddress,
                                NULL,
                                &bHasBPAtAddr,
                                NULL);
         if (ErrRet != NO_ERROR)
            goto SingleStepErrorHandler;
            
         if (!bHasBPAtAddr)
            {
            ErrRet = TXRXSetClearBP(SET_BP,
                                    tySecondBPType,
                                    DEFAULT_COMMON_BANK,
                                    ulSecondAddress,
                                    1,
                                    0,
                                    0,
                                    0);
            if (ErrRet != NO_ERROR)
               goto SingleStepErrorHandler;
               
            bSecondAddressBPSet = TRUE;
            }
         }
      }

   // Now, start executing...
   ulGlobalExecutionTime = GetTickCount();
   ErrRet = pRDIAgentVectorTable->execute(tyGlobalAgent,&tyGlobalArmModule.handle,FALSE,&PointHandle);
   ulGlobalExecutionTime = GetTickCount()-ulGlobalExecutionTime;
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet !=NO_ERROR)
      goto SingleStepErrorHandler;

   uiGlobalRdiCauseOfBreak = SINGLE_STEP;

   // remove temporary breakpoints
   if (bFirstAddressBPSet)
      {
      ErrRet = TXRXSetClearBP(CLR_BP,
                              OPCODE_BP,
                              DEFAULT_COMMON_BANK,
                              ulFirstAddress,
                              1,
                              0,
                              0,
                              0);
      if (ErrRet != NO_ERROR)
         goto SingleStepErrorHandler;
      }
   if (bSecondAddressBPSet)
      {
      ErrRet = TXRXSetClearBP(CLR_BP,
                              OPCODE_BP,
                              DEFAULT_COMMON_BANK,
                              ulSecondAddress,
                              1,
                              0,
                              0,
                              0);
      if (ErrRet != NO_ERROR)
         goto SingleStepErrorHandler;
      }

   // everything ok, normal exit
   bARMSingleStepInProgress = FALSE;
   return ErrRet;

SingleStepErrorHandler:
   bARMSingleStepInProgress = FALSE;
   // We've hit an error, so here we want to remove any temporary bps, 
   //  but do not change the value of ErrRet
   if (bFirstAddressBPSet)
      {
      TXRXSetClearBP(CLR_BP,
                     OPCODE_BP,
                     DEFAULT_COMMON_BANK,
                     ulFirstAddress,
                     1,
                     0,
                     0,
                     0);
      }

   if (bSecondAddressBPSet)
      {
      TXRXSetClearBP(CLR_BP,
                     OPCODE_BP,
                     DEFAULT_COMMON_BANK,
                     ulSecondAddress,
                     1,
                     0,
                     0,
                     0);
      }

   // report the error caused originally
   return ErrRet;

}  /* SingleStep */


/****************************************************************************
     Function: IsAddressInARMOrThumbSpace
     Engineer: Brian Connolly
        Input: Address to check       
       Output: Boolean TRUE= ARM , FALSE = THUMB
  Description:  is address in  16-bit or 32-bit Code (THUMB/ARM)

Version        Date           Initials    Description
0.9.3          13-Feb-2001    BC          Initial
****************************************************************************/
TyError IsAddressInARMOrThumbSpace(adtyp ulAddress, TyAddrRangeAttribute *ptyAddrRangeAttribute)
{
   *ptyAddrRangeAttribute = UnDefinedRange;

   return GetAddressRangeAttribute(NILVALUE,
                                   (adtyp)ulAddress,
                                   ptyAddrRangeAttribute);
}  /* IsAddressInARMOrThumbSpace */

/****************************************************************************
     Function: InitAuxMacroVariables
     Engineer: Brian Connolly
        Input: void       
       Output: void
  Description: clear the two outputs "OP1", "OP2", 
               ensure the macro variables "%OP1" "%OP2" and "%IP1" are up to date

Version        Date           Initials    Description
0.9.3          07-MAR-2001    BC          Initial
****************************************************************************/
void InitAuxMacroVariables(void)
{
   int iState=0;

   // Clear all outputs
   (void)TXRXSetGetClearAuxIO(CLEAR_OP1, (int*) &iState);
   (void)TXRXSetGetClearAuxIO(CLEAR_OP2, (int*) &iState);
   (void)TXRXSetGetClearAuxIO(CLEAR_OP3, (int*) &iState);
   (void)TXRXSetGetClearAuxIO(CLEAR_OP4, (int*) &iState);

   // Get IP1
   (void)TXRXSetGetClearAuxIO(GET_IP1,   (int*) &iState);
}

/****************************************************************************
     Function: RDIReadCurrentEthernetValues
     Engineer: Brian Connolly
        Input: PortNumber       : Serial port to read values over.
               tyBaudRate       : Serial baud to read values over.
               pszIPAddress     : Storage for TcpIp address.
               pszSubnetAddress : Storage for Subnet mask
               pszGatewayAddress: Storage for Gateway address
       Output: Error value or NO_ERROR
  Description: Reads the existing ethernet values over the serial port.

Version        Date           Initials    Description
0.9.3          08-Mar-2001     BC         taken from GDI
****************************************************************************/
static TyError RDITXRXReadCurrentEthernetValues(int        iPortNumber        ,
                                                TyBaudRate tyBaudRate         ,
                                                char       *pszIPAddress      ,
                                                char       *pszSubnetAddress  ,
                                                char       *pszGatewayAddress ,
                                                char       *pszEthernetAddress)
{
   TyError ErrRet;
   HINSTANCE            LocalhinstGdiDll          ;
   TyDiReadEtherDetails LocalDLLDiReadEtherDetails;

   LocalhinstGdiDll = LoadLibrary("GENIA.DLL");
   if (LocalhinstGdiDll == NULL)
      return IDERR_LOAD_ARM_DLL_GENIA;

   LocalDLLDiReadEtherDetails  = (TyDiReadEtherDetails)GetProcAddress(LocalhinstGdiDll, "ReadEtherDetails");
   if (LocalDLLDiReadEtherDetails == NULL)
      {
      (void)FreeLibrary(LocalhinstGdiDll);
      return IDERR_LOAD_ARM_DLL_GENIA;
      }

   ErrRet = LocalDLLDiReadEtherDetails(iPortNumber, tyBaudRate, pszIPAddress, pszSubnetAddress, pszGatewayAddress, pszEthernetAddress);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      {
      (void)FreeLibrary(LocalhinstGdiDll);
      return ErrRet;
      }

   (void)FreeLibrary(LocalhinstGdiDll);

   return NO_ERROR;

}  /* RDITXRXReadCurrentEthernetValues */

/****************************************************************************
     Function: RDITXRXWriteNewEthernetValues
     Engineer: Brian Connolly
        Input: PortNumber       : Serial port to program values over.
               tyBaudRate       : Serial baud to program values over.
               pszIPAddress     : New TcpIp address.
               pszSubnetAddress : New Subnet mask
               pszGatewayAddress: New Gateway address
       Output: Error value or NO_ERROR
  Description: Writes the new ethernet values over the serial port.
Version     Date           Initials    Description
0.9.3       08-Mar-2001    BC          Taken from GDI
****************************************************************************/
static TyError RDITXRXWriteNewEthernetValues(int iPortNumber,
                                             TyBaudRate tyBaudRate,
                                             char *pszIPAddress,
                                             char *pszSubnetAddress,
                                             char *pszGatewayAddress)
{
   TyError ErrRet;
   HINSTANCE             LocalhinstGdiDll;
   TyDiWriteEtherDetails LocalDLLDiWriteEtherDetails;

   LocalhinstGdiDll = LoadLibrary("GENIA.DLL");
   if (LocalhinstGdiDll == NULL)
      return IDERR_LOAD_ARM_DLL_GENIA;

   LocalDLLDiWriteEtherDetails  = (TyDiWriteEtherDetails)GetProcAddress(LocalhinstGdiDll, "WriteEtherDetails");
   if (LocalDLLDiWriteEtherDetails == NULL)
      {
      (void)FreeLibrary(LocalhinstGdiDll);
      return IDERR_LOAD_ARM_DLL_GENIA;
      }

   ErrRet = LocalDLLDiWriteEtherDetails(iPortNumber, tyBaudRate, pszIPAddress, pszSubnetAddress, pszGatewayAddress);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      {
      (void)FreeLibrary(LocalhinstGdiDll);
      return ErrRet;
      }

   (void)FreeLibrary(LocalhinstGdiDll);

   return NO_ERROR;

}  /* RDITXRXWriteNewEthernetValues */

/****************************************************************************
     Function: RDIGetARMEventValue
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: Given an index from the high level combo-box,  returns the
               the resource value to be programmed into the event register
Date           Initials    Description
18-Jul-2002    MOH         Initial
23-Jul-2004    PL          Disabled External Inputs 
****************************************************************************/
static unsigned long RDIGetARMEventValue(TyARMETMConfigReg  tyETMConfigValue, unsigned long ulIndex)
{
   unsigned long iStart;
   unsigned long i;

   iStart = 0;

   if (ulIndex == 0)
      return (6 << 4) + 15;

   iStart = 1;
   // Check  all address pair comparators...
   for (i=0; i < tyETMConfigValue.ulAddrComps; i++)
      {
      if (ulIndex == (iStart + i))
         return (1 << 4) + i;
      }

   iStart = tyETMConfigValue.ulAddrComps+1;
   // Check all address comparators...
   for (i=0; i < (tyETMConfigValue.ulAddrComps*2); i++)
      {
      if (ulIndex == (iStart + i))
         return (0 << 4) + i;
      }

   iStart = (tyETMConfigValue.ulAddrComps*2)+tyETMConfigValue.ulAddrComps+1;
/*
   // Check  all counters...
   for (i=0; i < tyETMConfigValue.ulCounters; i++)
      {
      if (ulIndex == (iStart + i))
         return (4 << 4) + i;
      }

   iStart = tyETMConfigValue.ulCounters+(tyETMConfigValue.ulAddrComps*2)+tyETMConfigValue.ulAddrComps+1;
*/
/*
   // Check  all Ex Inputs...
   for (i=0; i < tyETMConfigValue.ulExInputs; i++)
      {
      if (ulIndex == (iStart + i))
         return (6 << 4) + i;
      }
*/
   // Should never get this far !
   return 0x0;

}  /* RDIGetARMEventValue */

/****************************************************************************
     Function: RDITXRXSetupTriggerEvents
     Engineer: Martin Hannon (rewritten)
        Input: 
       Output: Error value or NO_ERROR
  Description: 
Version     Date           Initials    Description
1.0.0       19-Jul-2002    MOH         Initial
****************************************************************************/
static TyError RDITXRXSetupTriggerEvents(void)
{
   TyError                 ErrRet;
   unsigned long           i;
   unsigned long           ulValue;
   unsigned long           ulTraceStartStopReason;
   TyARMETMControlReg      tyETMControlValue;
   TyARMETMConfigReg       tyETMConfigValue;
   TyARMETMTraceEnReg      tyARMETMTraceEnReg;
   TyARMETMAddrAccessReg   tyARMETMAddrAccessReg;
   TXRXReg                 tyETMControlReg;
   TXRXReg                 tyETMConfigReg ;
   unsigned long           ulTraceCount;

   // no point in setting up trace config for anything other than a Vitra
   if (GlobalPathFndrTarget != Vitra)
      return NO_ERROR;
   
   tyETMControlReg = REG_ARM_ETM_BANK0_R0;
   tyETMConfigReg  = REG_ARM_ETM_BANK0_R1;

   // First read the ETM Confi register...
   ErrRet = TXRXReadETMReg(&tyETMConfigReg,
                           &tyETMConfigValue.ulRegValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Now read the ETM Control register...
   ErrRet = TXRXReadETMReg(&tyETMControlReg,
                           &tyETMControlValue.ulRegValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      Set the programming bit...
   */
   // Now set the programming bit...
   tyETMControlValue.ulETMProg = 1;

   // Now, write it out...
   ErrRet = TXRXWriteETMReg(tyETMControlReg,
                            tyETMControlValue.ulRegValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      Trigger Event Setup...
   */
   ulValue  = RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTrigger.ulEnableEventA);
   ulValue += RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTrigger.ulEnableEventB) << 7;
   ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTrigger.ulEnableEncode << 14;

   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R2,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      Trace Enable Setup...
   */
   ulValue  = RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.ulEnableEventA);
   ulValue += RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.ulEnableEventB) << 7;
   ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.ulEnableEncode << 14;

   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R8,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      If start / stop tracing is supported set it up...
   */
   ulValue = 0;
   if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bStartStopTracing)
      {
      for (i=0; i < ARM_MAX_ADDR_RESOURCES; i++)
         {
         ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bStartAddresses[i] << i;
         ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bStopAddresses[i]  << (i+16);
         }
      }                                                                                             
   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R6,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      Setup trace enable for ranges...
   */
   tyARMETMTraceEnReg.ulStartStopEnable = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bStartStopTracing;
   tyARMETMTraceEnReg.ulExcludeAddress  = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bExcludeAddresses;
   tyARMETMTraceEnReg.ulMemMapDecodes   = 0;
   tyARMETMTraceEnReg.ulAddressRange    = 0;
   for (i=0; i < ARM_MAX_ADDR_RESOURCES/2; i++)
      tyARMETMTraceEnReg.ulAddressRange += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bAddressesRanges[i] << i;
   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R9,
                            tyARMETMTraceEnReg.ulRegValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      Setup trace enable for single addresses...
   */
   ulValue = 0;
   for (i=0; i < ARM_MAX_ADDR_RESOURCES; i++)
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceEnable.bSingleAddresses[i] << i;
   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R7,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      View Data Setup...
   */
   ulValue  = RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.ulEnableEventA);
   ulValue += RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.ulEnableEventB) << 7;
   ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.ulEnableEncode << 14;

   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R12,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   ulValue = 0;

   for (i=0; i < ARM_MAX_ADDR_RESOURCES; i++)
      {
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.bSingleIncludeAddresses[i] << i;
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.bSingleExcludeAddresses[i] << (i+16);
      }
   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R13,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   ulValue = 0;
   for (i=0; i < ARM_MAX_ADDR_RESOURCES/2; i++)
      {
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.bIncludeAddressesRanges[i] << i;
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.bExcludeAddressesRanges[i] << (i+8);
      }
   ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMViewData.bExcludeAddressesOnly << 16;
   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R15,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;


   /*
      Address comparators...
   */
   for (i=0; i < ARM_MAX_ADDR_RESOURCES; i++)
      {
      if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].tyArmMask == ARM_NO_BITS)
         ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK1_R0 + i),
                                 GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].ulAddress);
      else if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].tyArmMask == ARM_ONE_BIT)
         ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK1_R0 + i),
                                 GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].ulAddress & 0xFFFFFFFE);
      else 
         ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK1_R0 + i),
                                 GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].ulAddress & 0xFFFFFFFC);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      tyARMETMAddrAccessReg.ulRegValue = 0;

      tyARMETMAddrAccessReg.ulAccessType  = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].tyArmAccess;
      tyARMETMAddrAccessReg.ulSizeMask    = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].tyArmMask;
      tyARMETMAddrAccessReg.ulDataCompare = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMAddressResource[i].tyArmData;
      tyARMETMAddrAccessReg.ulExactMatch  = FALSE;

      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK2_R0 + i),
                              tyARMETMAddrAccessReg.ulRegValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }

   /*
      Data comparators...
   */
   for (i=0; i < ARM_MAX_DATA_RESOURCES; i++)
      {
      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK3_R0 + i),
                              GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMDataResource[i].ulValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK4_R0 + i),
                              GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMDataResource[i].ulMask);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }

   /*
      Counters comparators...
   */
   for (i=0; i < ARM_MAX_COUNT_RESOURCES; i++)
      {
      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK5_R0 + i),
                              GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulInitialValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      ulValue  = RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulEnableEventA);
      ulValue += RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulEnableEventB) << 7;
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulEnableEncode << 14;

      // Counter is enabled by event...
      ulValue += 1L << 17;

      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK5_R4 + i),
                               ulValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      ulValue  = RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulReloadEventA);
      ulValue += RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulReloadEventB) << 7;
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMCountResource[i].ulReloadEncode << 14;

      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK5_R8 + i),
                               ulValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }

   /*
      External outputs...
   */
   for (i=0; i < ARM_MAX_EXT_RESOURCES; i++)
      {
      ulValue  = RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceExt[i].ulEnableEventA);
      ulValue += RDIGetARMEventValue(tyETMConfigValue, GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceExt[i].ulEnableEventB) << 7;
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceExt[i].ulEnableEncode << 14;

      ErrRet = TXRXWriteETMReg((TXRXReg)(REG_ARM_ETM_BANK6_R8 + i),
                               ulValue);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }

   /*
      FIFO region register...
   */
   ulValue = 0;

   for (i=0; i < ARM_MAX_ADDR_RESOURCES/2; i++)
      {
      ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceFifo.bAddressesRanges[i] << i;
      }
   ulValue += GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMTraceFifo.bExcludeAddresses << 24;

   ErrRet = TXRXWriteETMReg(REG_ARM_ETM_BANK0_R10,
                            ulValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;


   /*
      ETM Control registers...
   */
   tyETMControlValue.ulPowerDown        = 0;
   tyETMControlValue.ulMonCRTs          = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bMonRegTransfers;
   tyETMControlValue.ulDataAccess       = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.tyArmDataTrace;
   tyETMControlValue.ulPortSize         = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.tyArmPortWidth;
   tyETMControlValue.ulStallProc        = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bStallOnFifoFull;
   tyETMControlValue.ulBranchAddr       = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bBroadcastAllBranches;
   tyETMControlValue.ulDebugReq         = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bHaltProcOnTrig;
   tyETMControlValue.ulETMProg          = 1;
   tyETMControlValue.ulETMPortSel       = 1;
   tyETMControlValue.ulCycleAccurate    = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bCycleAccurate;
   if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bTraceHalfRate)
      tyETMControlValue.ulHalfRateClocking = 1;
   else 
      tyETMControlValue.ulHalfRateClocking = 0;
   tyETMControlValue.ulContextID        = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.tyArmContextTrace;
   tyETMControlValue.ulPortMode         = 0x0;

   // Now, write it out...
   ErrRet = TXRXWriteETMReg(tyETMControlReg,
                            tyETMControlValue.ulRegValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Finally clear the programming bit...
   tyETMControlValue.ulETMProg          = 0;

   // Now, write it out...
   ErrRet = TXRXWriteETMReg(tyETMControlReg,
                            tyETMControlValue.ulRegValue);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   /*
      OK, the unsigned long we send down is formatted as follows....

      BIT 31    : bStartOnTrigger
      BIT 30    : bStopOnTrigger
      BIT 29    : bStopOnBufferFull
      BIT 28    : Rising Clock Edge
      BIT 15..0 : ulFrameCount;

      This is Ashling specific !!!! Will need to be changed if using a 3rd
      party software / hardware.
   */

   ulTraceStartStopReason = 0;

   if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMStartStop.bStartOnTigger)
      ulTraceStartStopReason += 0x80000000;

   if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMStartStop.bStopOnTrigger)
      ulTraceStartStopReason += 0x40000000;

   if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMStartStop.bStopOnBufferFull)
      ulTraceStartStopReason += 0x20000000;

   if (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.bTraceClockEdge)
      ulTraceStartStopReason += 0x10000000;

   switch (GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMEtmConfig.tyArmPortWidth)
      {
      default:
         // Assert and flow through...
         ASSERT(FALSE);
      case ARM_PORT_BITS_4 :
         ulTraceCount = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMStartStop.ulFrameCount / 8;
         break;
      case ARM_PORT_BITS_8 :
         ulTraceCount = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMStartStop.ulFrameCount / 5;
         break;
      case ARM_PORT_BITS_16:
         ulTraceCount = GetPFWAppObject()->m_TriggersSetup.RDITrigger.tyARMStartStop.ulFrameCount / 3;
         break;
      }

   // Always round up !!!!
   ulTraceCount++;

   // Note that the counter is an up counter !!!!
   ulTraceStartStopReason += (unsigned short)(0x0 - ulTraceCount);

   // Set TRigger Position
   ErrRet = pETMVectorTable->info(tyGlobalEtmModule.handle,RDITrace_SetTriggerPosition,&ulTraceStartStopReason,NULL);
   ErrRet =  CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   return NO_ERROR;

}  /* RDITXRXSetupTriggerEvents */

/****************************************************************************
     Function: RDITXRXReadETMReg
     Engineer: Brian Connolly
        Input: TXRXReg *pSpecifiedReg : Register to be read (NULL for block read)
               ulong   *pulRegValues  : Pointer to return values
       Output: TyError: NO_ERROR or error number
  Description: Reads ETM registers from emulator.
                  
                    NOTE: ulRegister the Register mask bit 15 is reserved by ARM

Version        Date           Initials    Description
1.0.1          18-APR-2001    BC          Initial
****************************************************************************/
static  TyError RDITXRXReadETMReg(TXRXReg *pSpecifiedReg,
                               ulong   *pulRegValues)
{
   // if not setup
   if (!pETMVectorTable)
      return NO_ERROR;

   //ulRegister Holds the register position in the Register mask (NB: position 15 is reserved by ARM)
   //ulMode holds the mode in which the register is valid
 
   TyError       ErrRet;
   unsigned long ulMode=0;
   unsigned long ulRegMask=0;
   unsigned long ulControlProcNum=0;
   boolean       bFoundIt;

   // Is the register a Core Reg, Determine the mode and mask to pass to DLL
   if (GetModeAndMaskForETMReg(*pSpecifiedReg,&ulMode,&ulRegMask))
      {
      //Read one register
      
      if (RegisterCacheStruct.bRegisterCacheEnabled)
         {
         ErrRet = GetRegisterCacheData(ulMode, ulRegMask, pulRegValues);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }
      else
         {
         ErrRet = pETMVectorTable->CPUread(tyGlobalEtmModule.handle,ulMode,ulRegMask, pulRegValues);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }
      }
   //Not a member of the Core registers, try the peripheral registers
   else
      {
      ErrRet = GetCPNumAndMaskForReg(*pSpecifiedReg, &ulControlProcNum,&ulRegMask, &bFoundIt);
      if (ErrRet != NO_ERROR)
         return ErrRet;

      if (bFoundIt)
         {
         ErrRet = pETMVectorTable->CPread(tyGlobalEtmModule.handle,ulControlProcNum,ulRegMask, pulRegValues);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }   
      else
         {
         // Register not found...
         ASSERT_NOW();
         }
      }

   return NO_ERROR;

}  /* RDITXRXReadETMReg */

/****************************************************************************
     Function: RDITXRXSetGetVectorCatch
     Engineer: Brian Connolly
        Input: 
       Output: TyError: NO_ERROR or error number
  Description: 
Version        Date           Initials    Description           
1.0.3          19-Oct-2001    BC          Initial
****************************************************************************/
static TyError RDITXRXSetGetVectorCatch(adtyp   *ulVectorMask,
                                        boolean bSet)
{
   TyError ErrRet =NO_ERROR;
   static unsigned long ulMask=0;

   // Set the vector catch mask
   if (bSet)
      {
	  ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIVector_Catch,ulVectorMask,NULL);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet!=NO_ERROR)
		{
        ulMask=0;			// Clear the mask of we unable to set the specified Vector catch
		return ErrRet;
		}
      }
   // Get the vector catch mask
   else     
      {
      // Return the last known state
      *ulVectorMask = ulMask;
      }

   ulMask = *ulVectorMask;

   return NO_ERROR;

}  /* RDITXRXSetGetVectorCatch */

/****************************************************************************
     Function: RDITXRXWriteETMReg
     Engineer: Brian Connolly
        Input: Reg: register to write to
               Value: value to write
       Output: TyError: NO_ERROR or error number
  Description: Write Value to specified register Reg
               (See comments for RDITXRXReadReg)
Version        Date           Initials    Description           
1.0.1          18-APR-2001    BC          Initial
****************************************************************************/
static  TyError RDITXRXWriteETMReg(TXRXReg Reg,
                                   adtyp   Value)
{
   // if not setup
   if (!pETMVectorTable)
       return NO_ERROR;

   TyError       ErrRet;
   unsigned long ulMode=0;
   unsigned long ulRegMask=0;
   unsigned long ulControlProcNum=0;
   boolean       bFoundIt;
   
   // Is it a Core register
   if (GetModeAndMaskForETMReg(Reg, &ulMode,&ulRegMask))
      {
      // If writing to the control reg, update the global
      if(Reg==REG_ARM_ETM_BANK0_R0)
         {
         unsigned long ulRegValue = Value;
         ulRegValue&=0x70;
         ulRegValue>>=4;
         switch(ulRegValue)
            {
            default:
            case 2:
               ulGlobalEtmBusWidth=SIXTEEN_BIT_ETM_BUS;
               break;
            case 1:
               ulGlobalEtmBusWidth=EIGHT_BIT_ETM_BUS;
               break;
            case 0:
               ulGlobalEtmBusWidth=FOUR_BIT_ETM_BUS;
               break;
            }
         }
      // Write to the register
      ErrRet = pETMVectorTable->CPUwrite(tyGlobalEtmModule.handle,ulMode,ulRegMask,(const unsigned long *)&Value);
      ErrRet = CheckForFatalRDIError(ErrRet);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      }
   else
      {
      //Not a member of the Core registers, try the peripheral registers
      ErrRet = GetCPNumAndMaskForReg(Reg, &ulControlProcNum,&ulRegMask, &bFoundIt);
      if (ErrRet != NO_ERROR)
         return ErrRet;
      if (bFoundIt)
         {
         // Write Co-processor register to the register
         ErrRet = pETMVectorTable->CPwrite(tyGlobalEtmModule.handle,ulControlProcNum,ulRegMask,(const unsigned long *)&Value);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }   
      else   
         {
         //register not found
         ASSERT_NOW();
         }
      }

   return NO_ERROR;

}  /* RDITXRXWriteETMReg */

/****************************************************************************
     Function: GetAddressCompatatorResourcRegisters
     Engineer: Brian Connolly
        Input: 
               
       Output: 
  Description: 
               
Version        Date           Initials    Description           
1.0.1          18-APR-2001    BC          Initial
****************************************************************************/
TyError GetAddressCompatatorResourcRegisters(unsigned int uiRangeNumber,
                                             TXRXReg      *StartRangeReg,
                                             TXRXReg      *EndRangeReg,
                                             TXRXReg      *AccessTypeReg0,
                                             TXRXReg      *AccessTypeReg1)
{
   #define MAX_COMPARE_SUPPORTED 8
   TXRXReg pStartRangeAddresses    [MAX_COMPARE_SUPPORTED] = {REG_ARM_ETM_BANK1_R0 ,
                                                              REG_ARM_ETM_BANK1_R2 ,
                                                              REG_ARM_ETM_BANK1_R4 ,
                                                              REG_ARM_ETM_BANK1_R6 ,
                                                              REG_ARM_ETM_BANK1_R8 ,
                                                              REG_ARM_ETM_BANK1_R10,
                                                              REG_ARM_ETM_BANK1_R12,
                                                              REG_ARM_ETM_BANK1_R14};
   TXRXReg pEndRangeAddresses      [MAX_COMPARE_SUPPORTED] = {REG_ARM_ETM_BANK1_R1 ,
                                                              REG_ARM_ETM_BANK1_R3 ,
                                                              REG_ARM_ETM_BANK1_R5 ,
                                                              REG_ARM_ETM_BANK1_R7 ,
                                                              REG_ARM_ETM_BANK1_R9 ,
                                                              REG_ARM_ETM_BANK1_R11,
                                                              REG_ARM_ETM_BANK1_R13,
                                                              REG_ARM_ETM_BANK1_R15};
   TXRXReg pAccessTypeReg0Addresses[MAX_COMPARE_SUPPORTED] = {REG_ARM_ETM_BANK2_R0 ,
                                                              REG_ARM_ETM_BANK2_R2 ,
                                                              REG_ARM_ETM_BANK2_R4 ,
                                                              REG_ARM_ETM_BANK2_R6 ,
                                                              REG_ARM_ETM_BANK2_R8 ,
                                                              REG_ARM_ETM_BANK2_R10,
                                                              REG_ARM_ETM_BANK2_R12,
                                                              REG_ARM_ETM_BANK2_R14};
   TXRXReg pAccessTypeReg1Addresses[MAX_COMPARE_SUPPORTED] = {REG_ARM_ETM_BANK2_R1 ,
                                                              REG_ARM_ETM_BANK2_R3 ,
                                                              REG_ARM_ETM_BANK2_R5 , 
                                                              REG_ARM_ETM_BANK2_R7 ,
                                                              REG_ARM_ETM_BANK2_R9 ,
                                                              REG_ARM_ETM_BANK2_R11,
                                                              REG_ARM_ETM_BANK2_R13,
                                                              REG_ARM_ETM_BANK2_R15};

   if (uiRangeNumber < MAX_COMPARE_SUPPORTED)
      {
      *StartRangeReg  = pStartRangeAddresses    [uiRangeNumber];
      *EndRangeReg    = pEndRangeAddresses      [uiRangeNumber];
      *AccessTypeReg0 = pAccessTypeReg0Addresses[uiRangeNumber];
      *AccessTypeReg1 = pAccessTypeReg1Addresses[uiRangeNumber];
      }
   else
      {
      ASSERT_NOW();
      }

   return NO_ERROR;

}  /* GetAddressCompatatorResourcRegisters */


/****************************************************************************
    Function    : ToHostReadDataFromController
    engineer    : Brian Conolly
    description : read data from target
        arg     -> handle, provided by the debug controller
        data    -> Data read from Debug Comtroller
Version        Date           Initials    Description           
1.0.2       18-Jul-2001    BC          Initial
1.0.2       15-AUG-2001    PJS         call to console window
1.0.2       23-AUG-2001    BC          m_consoleWindow no longer a PFWAppObject  
****************************************************************************/
void  ToHostReadDataFromController(void *arg,ARMword data)
{
   char szData[sizeof(ARMword)+1];

   memcpy(szData, &data, sizeof(ARMword));
   
   szData[sizeof(ARMword)] = EOS;
   
   (void)m_ConsoleWindow.OutputString(szData);
   
   return;

}  /* ToHostReadDataFromController */

/****************************************************************************
    Function    : FromHostWriteDataToController
    engineer    : Brian Conolly
    description : Write data to target
        arg     -> handle, provided by the debug controller
        data    -> Data to Write Debug Comtroller
        valid   -> zero if there was no data to read else nonzero
Version        Date           Initials    Description           
1.0.2       18-Jul-2001    BC          Initial
1.0.2       15-AUG-2001    PJS         call to console window
1.0.2       23-AUG-2001    BC          m_consoleWindow no longer a PFWAppObject  
****************************************************************************/
void FromHostWriteDataToController(void *arg,ARMword *data, int *valid )
{
   char szData[sizeof(ARMword)+1];           // plus 1 for zero byte
   
   memset(szData, 0, sizeof(szData));
   
   (void)m_ConsoleWindow.InputString(szData, sizeof(szData));
   
   memcpy(data, szData, sizeof(ARMword));    
   
   *valid = ((szData[0] == EOS) ? 0 : 1);
   return;

}  /* FromHostWriteDataToController */

/****************************************************************************
    Function    : LocRDI_Hif_Write
    engineer    : Brian Conolly
    description : Write string to Console
Version        Date           Initials    Description           
1.0.2          22-Aug-2001    BC          Initial
****************************************************************************/
int LocRDI_Hif_Write(RDI_Hif_HostosArg *arg, char const *buffer, int len)
{
   char * szStringToDisplay;  //char pointer
   CArray <char, char> szData; //defina a CArray of zero length

   //lint 
   arg=arg;
   // add to the CArray
   for (int i=0;i<len;i++)
      szData.Add((char) *(buffer+i));

   //we are pointing at the end so  just append an EOS
   szData.Add('\0');

   //point the char * to the begining of the CArray
   szStringToDisplay=&szData[0];

   //write string to console
   (void)m_ConsoleWindow.OutputString((char *)szStringToDisplay);
   
   return NO_ERROR;

}  /* LocRDI_Hif_Write */

/****************************************************************************
    Function    : LocRDI_Hif_WriteC
    engineer    : Brian Conolly
    description : Write char to Console
Version        Date           Initials    Description           
1.0.2          22-Aug-2001    BC          Initial
****************************************************************************/
//write a charachter to console
void LocRDI_Hif_WriteC(RDI_Hif_HostosArg *arg, int c)
{
   //Lint
   arg=arg;
   
   char szData[2]; 

   //copy char to array
   memcpy(szData, &c,1);
   
   //append and EOS charachter
   szData[1] = EOS;
   
   //write string to console
   (void)m_ConsoleWindow.OutputString(szData);
   
   //done
   return;

}  /* LocRDI_Hif_WriteC */

/****************************************************************************
    Function    : LocRDI_Hif_ReadC
    engineer    : Brian Conolly
    description : Read a charachter from console
Version        Date           Initials    Description           
1.0.2          22-Aug-2001    BC          Initial
****************************************************************************/
int LocRDI_Hif_ReadC(RDI_Hif_HostosArg *arg)
{
   char szData[2];// 2 bytes , 1 for EOS
   
   arg=arg;
   
   memset(szData, 0, sizeof(szData));
   (void)m_ConsoleWindow.InputString(szData, 2);
   
   return szData[0];

}  /* LocRDI_Hif_ReadC */

/****************************************************************************
    Function    : LocRDI_Hif_Gets
    engineer    : Brian Conolly
    description : Read a string from console
Version        Date           Initials    Description           
1.0.2          22-Aug-2001    BC          Initial
****************************************************************************/
char *LocRDI_Hif_GetS(RDI_Hif_HostosArg *arg, char *buffer, int len)
{
   size_t size;
   (void)m_ConsoleWindow.InputString(buffer, len);
   size=strlen(buffer);
   if((int)size<len) //if size returned is less than length 
      {
      // Requested terminate with a '0x0a'
      buffer[(int)size]=0x0a;
      }
   else if ((int)size==0) //if size returned is zero return NULL   
      {
      buffer[len-1]=0x00;
      }
   else if ((int)size>=len) //if size returned is greater than length asked for  
      {
      // Requested terminate with a '0x0a'
      buffer[len-1]=0x0a;
      }
   return buffer;

}  /* LocRDI_Hif_GetS */

/****************************************************************************
    Function    : LocRDI_Hif_ResetProc
    engineer    : Brian Conolly
    description : not implemented
Version        Date           Initials    Description           
1.0.3          24-Sep-2001    BC          Initial
****************************************************************************/
void LocRDI_Hif_ResetProc(RDI_Hif_ResetArg *arg)
{
}

/****************************************************************************
    Function    : LocRDI_Hif_UserMessageProc
    engineer    : Brian Conolly
    description : not implemented
Version        Date           Initials    Description           
1.0.3          24-Sep-2001    BC          Initial
****************************************************************************/
RDI_Hif_UserMessageReturn LocRDI_Hif_UserMessageProc(RDI_Hif_UserMessageArg *arg, 
                                                     char const *msg, 
                                                     uint32 type)
{
   return RDI_MBANSWER_OK;
}

/****************************************************************************
    Function    : LocRDI_Hif_DbgPause
    engineer    : Brian Conolly
    description : wait forever until the user presses a key, 
Version        Date           Initials    Description           
1.0.2          22-Aug-2001    BC          Initial
****************************************************************************/
void LocRDI_Hif_DbgPause(RDI_Hif_DbgArg *arg)
{
   // Wait for a key to be pressed
   while (!LocRDI_Hif_ReadC((RDI_Hif_HostosArgStr *)arg) );
}

/****************************************************************************
    Function    : LocRDI_Hif_DbgPrint
    engineer    : 
    description : 
Version        Date           Initials    Description           
1.0.2          22-Aug-2001    BC          Initial
****************************************************************************/
void LocRDI_Hif_DbgPrint(RDI_Hif_DbgArg *arg, const char *format, va_list ap)
{

}
/****************************************************************************
    Function    : RDI_ConfigureForceHWBpts
    engineer    : 
    description : Force PF to use HW BPs
Date           Initials    Description           
22-Aug-2001    BC          Initial
02-Mar-2005    PL          Check the operation was successful
****************************************************************************/
TyError RDI_SetClearForceHWBpts(BOOL bSetForceHWBpts)
{
   int	ErrRet  = 0x00; 
   ARMword	arg1	= NULL;
   ARMword	arg2	= NULL;
   RDIProperty_GroupDesc tyPropertyGroup;
   RDIProperty_Desc      tyPropertyDesc;

	// Does the module support RDI properties
	ErrRet = pRDIAgentVectorTable->info(tyGlobalArmModule.handle,RDIInfo_Properties,(ARMword *)&arg1,(ARMword *)&arg2);
	if(ErrRet != RDIError_NoError)
		{
		ASSERT_NOW();
		return ErrRet;
		}

   // Request information about the subgroup
	ErrRet = pRDIAgentVectorTable->info(tyGlobalArmModule.handle,RDIProperty_RequestGroups,RDIPropertyGroup_SuperGroup,(unsigned long *)&tyPropertyGroup);
	if(strcmp(tyPropertyGroup.name,"Super Group"))
		{
		ASSERT_NOW();
		return ErrRet;
		}

   // Request information about the property
	ErrRet = pRDIAgentVectorTable->info(tyGlobalArmModule.handle,RDIProperty_RequestDescriptions,(ARMword *)&arg1,(unsigned long *)&tyPropertyDesc);
	if(strcmp(tyPropertyDesc.name,"use_only_HW_breakpoints"))
		{
		ASSERT_NOW();
		return ErrRet;
		}
   
   // Read the current value of the property
	ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIProperty_GetAsNumeric,(unsigned long *)tyPropertyDesc.id,(ARMword *)&arg2);
  	
	// Write the new value of the property
	arg2 = bSetForceHWBpts;
	ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIProperty_SetAsNumeric,(unsigned long *)tyPropertyDesc.id,(ARMword *)&arg2);
   
    // Verify that the value has been set correctly
	ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIProperty_GetAsNumeric,(unsigned long *)tyPropertyDesc.id,(ARMword *)&arg2);
	if (arg2 != bSetForceHWBpts)
	   return RDIError_CantSetPoint;

	return ErrRet;
}


static TyError RDITXRXEmulatorRevisionNumbers(TXRXBoardType BoardType,char *pcBoardRevision,char *pcMechanicalRevision,char *pszElectricalRevision)
{
   return OPTION_NOT_IMPLEMENTED;
}
static  TyError RDITXRXDisplayMap(TXRXMemTypes MemType,TyBankNo StartBank,adtyp StartAddr,ulong *pulLen,TXRXMapTypes *pMapType,uint16 *puCodeRAM,uint16 *puDataRAM)
{
   return OPTION_NOT_IMPLEMENTED;
}
static  TyError RDITXRXClock(TXRXClockAction ClockAction,TXRXClockSource *pClockSource,double *pdClockFreq,double *pdClockAFreq,double *pdClockBFreq,double *pdClockCFreq)
{
   return OPTION_NOT_IMPLEMENTED;
}
static  TyError RDITXRXMapMem(TXRXMemTypes MemType,TXRXMapTypes MapType,TyBankNo StartBank, adtyp StartAddr,adtyp MapLen)
{
   return OPTION_NOT_IMPLEMENTED;
}
static  TyError RDITXRXProcType(TXRXProcTypeAction ProcTypeAction, ProcessorConfig *pProcCfg, bool SetVoltageOnly)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXSetupDualPortRam(TXRXDualPortRamAction DPAction, adtyp StartAddr, adtyp Len)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXReadTriggerCounts(uint16 *pStartTrigCount, uint16 *pStopTrigCount)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXFindTrcFrame(TXRXTraceFindMode FindMode, frtype StartFrame,TyBankNo StartBank,adtyp StartAddr,frtype NumFrames,bool *pbFound,frtype *pFoundFrame)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXSpecTrigTrc(trig_type *pTrigger)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXSpecTrigEvent(TXRXTrigEvents EventNum, trig_type  *pTrigger)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXSpecTrigCond(trig_type *pTrigger)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDISendCommand(ubyte TxByteCount, ubyte *pRxByteCount, ubyte RxByteCount)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDIHandleEmulatorError(ubyte ubError)
{
   return OPTION_NOT_IMPLEMENTED;
}
static  TyError RDITXRXRepPCPStat(TXRXPCPStatus *pPCPStat)
{
   return OPTION_NOT_IMPLEMENTED;
}
static TyError RDITXRXSetupCommsSpeed(TyBaudRate Speed)
{
   return OPTION_NOT_IMPLEMENTED;
}
BOOL IsValidRDIAgentHandle()
{
   if(tyGlobalAgent)
      return TRUE;
   else
	  return FALSE;
}
   



/****************************************************************************
     Function: CommonResetOrStartupCode
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: This functions contains the command sections of code between
               the initial startup and a subsequent recover from hard reset.
Date           Initials    Description
12-May-2004    MOH         Initial
*****************************************************************************/
static TyError CommonResetOrStartupCode(void)
{
   TyError              ErrRet;
   _ARMSpecific  tyARMSettings;
   struct               RDI_Hif_HostosArgStr    *HostArg    = NULL;
   struct               RDI_Hif_DbgArgStr       *DebugArg   = NULL;
   struct               RDI_Hif_ResetArgStr     *ResetArg   = NULL;
   struct               RDI_Hif_UserMessageArg  *MessageArg = NULL;
   unsigned int         type                                = 0;
   unsigned int         uiRDIVersion                        = 0;
   unsigned long        nProcs                              = 0;
   unsigned long        ulProcessorNumber                   = 0;
   char                 szBaud[100]                         = {0};
   char                 szTarget[10]                        = {0};
   unsigned long        ulAddress                           = 0;
   int                  i;
   char                 szDatabaseFilename[_MAX_PATH];


   // Create a Toolconf Database
   ptyGlobalCfgPtr=ToolConf_New(-1);
   // check it is valid
   if (ptyGlobalCfgPtr==NULL)
      return IDERR_RDI_CONFIG_CREATE;  // failed to create database

   // Fill out configuration database
   // add the tags to the toolconf
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUGGER_NAME", tcnf_String,"PFARM");
   
   if (GetPFWAppObject()->PathFinder.bLittleEndian)
      {
      ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"BYTESEX", tcnf_String,"L");
      EndianessType = LITTLE_ENDIAN_MASK;
      }
   else
      {
      ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"BYTESEX", tcnf_String,"B");
      EndianessType = BIG_ENDIAN_MASK;
      }

   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGETCONNECTION",tcnf_UInt, GetPFWAppObject()->PathFinder.m_TargetConnection);

   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"PARALLELPORT", tcnf_UInt, GetPFWAppObject()->PathFinder.iLptNumber);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"ETHERNET_ADDRESS",tcnf_String,GetPFWAppObject()->PathFinder.szEthernetAddr);

   sprintf(szBaud,"%d",(int)GetPFWAppObject()->PathFinder.tyBaudRate);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SERIAL_BAUD_RATE", tcnf_String,szBaud);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SERIAL_PORT", tcnf_UInt, GetPFWAppObject()->PathFinder.iPortNumber);
   

   // Take a local copy of the arm settings...
   tyARMSettings = GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig()->Processor._ARM;

   switch (tyARMSettings.uiStartupType)
      {
      default:
         // Assert and flow through...
         ASSERT(FALSE);
      case NO_RESET:
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_HARD_RESET",  tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUG_FROM_RESET",tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"STARTUP_DELAY",   tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"HOT_PLUG",        tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGET_VOLTAGE",  tcnf_UInt,0x0);
         break;
      case DEBUG_FROM_RESET:
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_HARD_RESET",  tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUG_FROM_RESET",tcnf_UInt,0x1);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"STARTUP_DELAY",   tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"HOT_PLUG",        tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGET_VOLTAGE",  tcnf_UInt,0x0);
         break;
      case USE_RESET:
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_HARD_RESET",  tcnf_UInt,0x1);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUG_FROM_RESET",tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"STARTUP_DELAY",   tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"HOT_PLUG",        tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGET_VOLTAGE",  tcnf_UInt,0x0);
         break;
      case USE_RESET_AND_DELAY:
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_HARD_RESET",  tcnf_UInt,0x1);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUG_FROM_RESET",tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"STARTUP_DELAY",   tcnf_UInt,tyARMSettings.uiResetDelay);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"HOT_PLUG",        tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGET_VOLTAGE",  tcnf_UInt,0x0);
         break;
      case HOT_PLUG:
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_HARD_RESET",  tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUG_FROM_RESET",tcnf_UInt,0x0);
         ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"STARTUP_DELAY",   tcnf_UInt,0x0);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"HOT_PLUG",        tcnf_UInt,0x1);
	      ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGET_VOLTAGE",  tcnf_UInt,tyARMSettings.uiTargetVoltage);
         break;
      }

   // Cache Clean Address  
   ptyGlobalCfgPtr=ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DATA_CACHE_CLEAN_ADDRESS", tcnf_UInt, tyARMSettings.ulCacheCleanAddress);

   // Safe Non-vector Address
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SAFE_NON_VECTOR_ADDRESS", tcnf_UInt, tyARMSettings.ulSafeNonVectorAddress);
   
   // Compiler Type
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_GNU_SEMIHOSTING", tcnf_UInt, tyARMSettings.bCompilerType);
   
   // Use nTRST
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"USE_NTRST", tcnf_UInt, tyARMSettings.bUseNTRST);

   // Use DBGRQ - Not a valid option for the EVBA7
   if (GetPFWAppObject()->PathFinder.m_PathFinderTarget != EVBA7)
      ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"DEBUG_REQUEST", tcnf_UInt, tyARMSettings.bUseDBGRQ);

   // JTAG clock speed rate index (basically a "divide-by" value)
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TCK_RATE", tcnf_UInt, GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig()->uiJTAGFreqIndex);

   //Processor Type i.e. ARM7TDMI
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"PROCESSOR", tcnf_String,GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig()->szProcName);
   
   // add TAP number (0/1), user can tell us to use a different scan chain (e.g. Globespan Virata)
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr, (tag_t)"DEBUG_CORE_NO", tcnf_UInt, tyARMSettings.uiSelectedCoreNumber);
   
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr, (tag_t)"NUMBER_OF_DEVICES", tcnf_UInt, tyARMSettings.tyJtagScanChain.ucNumJtagNodes);

   for(i=0; i < tyARMSettings.tyJtagScanChain.ucNumJtagNodes; i++)
	   {
      char szTemp[MAX_STRING_SIZE];

      sprintf (szTemp, "CORE_%X_IRLENGTH", i);
	   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr, (tag_t)szTemp, tcnf_UInt, tyARMSettings.tyJtagScanChain.ptyJtagNode[i].ucIRLength);
      
	   sprintf (szTemp, "CORE_%X", i);
      if (tyARMSettings.tyJtagScanChain.ptyJtagNode[i].bARMCore)
	      ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)szTemp, tcnf_String, "ARM_CORE");
	   else
	      ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)szTemp, tcnf_String, "NON_ARM_CORE");
	   }

   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SHIFT_IR_PRE_TCKS",  tcnf_UInt,tyARMSettings.ucShiftIRPreTCKs);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SHIFT_IR_POST_TCKS", tcnf_UInt,tyARMSettings.ucShiftIRPostTCKs);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SHIFT_DR_PRE_TCKS",  tcnf_UInt,tyARMSettings.ucShiftDRPreTCKs);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"SHIFT_DR_POST_TCKS", tcnf_UInt,tyARMSettings.ucShiftDRPostTCKs);

   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"OPELLA_USB_INSTANCE",      tcnf_UInt,  tyARMSettings.uiOpUSBInst);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"OPELLA_USB_USER_ID",       tcnf_String,tyARMSettings.szOpUSBUserID);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"OPELLA_USB_PRODUCT_ID",    tcnf_UInt,  tyARMSettings.uiOpUSBProdID);
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"OPELLA_USB_SERIAL_NUMBER", tcnf_UInt,  tyARMSettings.uiOpUSBSerialNo);

   //Tell the DLL what target is connected
   switch (GlobalPathFndrTarget)
      {
      default:
         // Assert and flow through...
         ASSERT(FALSE);
      case RDISimulator:
         strcpy( szTarget, "ARMULATE");
         break;
      case Opella:
         strcpy( szTarget, "OPELLA");
         break;
      case Genia:
         strcpy( szTarget, "GENIA");
         break;
      case Vitra:
         strcpy( szTarget, "VITRA");
         break;
      case Ultra:
         strcpy( szTarget, "ULTRA");
         break;
      case EVBA7:
         strcpy( szTarget, "EVBA7");
         break;
      }
   
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr,(tag_t)"TARGET", tcnf_String,szTarget);

   // This TARGET_DIR field is mandatory, according to the RDI spec. It is definitely
   //  required by armulate.dll
   ptyGlobalCfgPtr = ToolConf_AddTyped(ptyGlobalCfgPtr, (tag_t)"TARGET_DIR", tcnf_String, GetPFWAppObject()->PathFinder.szARMPath);

   // Get the database filename...
   GetDatabaseFilename(szDatabaseFilename, GlobalPathFndrTarget);

   // Write the Toolconf database to the file...
   if (!ToolConf_Write(ptyGlobalCfgPtr, szDatabaseFilename))
      return IDERR_RDI_CONFIG_WRITE;

   // Check the RDI version (except for Armulator/RDISimulator)
   if (GlobalPathFndrTarget != RDISimulator)
      {
      uiRDIVersion=GetDLLRDIVersion();
      
      // If the DLL returns version number 150 then we must try to set version 151
      // and check that the version has changed to RDI 151. 
      if (uiRDIVersion!=(unsigned int)RDI_VERSION)
         {
         SetDLLRDIVersion(RDI_VERSION);
         uiRDIVersion=GetDLLRDIVersion();
         //should return version number 151
         if(uiRDIVersion!=(unsigned int)RDI_VERSION)
            return IDERR_RDI_VERSION;
         }
      }


  
   // Register the Pathfinder callback
   Register_Yield_Callback((WinRDI_YieldProc *)ProcessRDIAppEvents,NULL);
   
   // this may change in the future
   tyGlobalhostif.dbgprint  = LocRDI_Hif_DbgPrint;//NULL;
   tyGlobalhostif.dbgpause  = LocRDI_Hif_DbgPause;
   tyGlobalhostif.dbgarg    = DebugArg;
   tyGlobalhostif.writec    = LocRDI_Hif_WriteC;
   tyGlobalhostif.readc     = LocRDI_Hif_ReadC;
   tyGlobalhostif.write     = LocRDI_Hif_Write;
   tyGlobalhostif.gets      = LocRDI_Hif_GetS;
   tyGlobalhostif.hostosarg = HostArg;
   tyGlobalhostif.reset     = LocRDI_Hif_ResetProc;
   tyGlobalhostif.resetarg  = ResetArg;

#if RDI_VERSION == 151
   tyGlobalhostif.message   = LocRDI_Hif_UserMessageProc;//NULL;
   tyGlobalhostif.messagearg= MessageArg;
#endif

   // Zero the BP count
   tyBreakpointArray.RemoveAll();

   // set the type of boot required and Initialisation info
   // We want to do a cold boot if:
   //  - the user has selected "Issue a Hard Reset on PathFinder configuration" at initialisation
   if (GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig()->bEnableTargetReset)
      {
      // we are initialising and we want the target to be reset
      tyGlobalAgent = NULL;
      type= COMMS_RESET|EndianessType;
      }
   else
      {
      //warm boot, proc already initialised earlier, or do not want target reset
      type= WARM_BOOT|EndianessType;
      }

   //Step 1.  Identify the Debug Agent     
   ErrRet = pRDIAgentVectorTable->openagent(&tyGlobalAgent,type,ptyGlobalCfgPtr,&tyGlobalhostif,NULL);
   ErrRet =  CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   //Step 2. Get the number of processors
   nProcs = 0; //initialise to zero
   ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&nProcs,NULL);
   ErrRet =  CheckForFatalRDIError(ErrRet);
   if(ErrRet != NO_ERROR)
      return ErrRet;
   
   // Step 3. Get details on the processor, Module handle etc.
   ulProcessorNumber=1;
   ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&ulProcessorNumber,(unsigned long *)&tyGlobalArmModule);
   ErrRet =  CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;        

   // if the processor provides it's own RDI_ProcVec, use that one, else use the agent's one
   if (tyGlobalArmModule.rdi != NULL)
      pRDIModuleVectorTable = tyGlobalArmModule.rdi;
   else
      pRDIModuleVectorTable = pRDIAgentVectorTable;
       
   bGloablEtmEnabled = FALSE;
   if (GlobalPathFndrTarget == Vitra) //ETM treated as a Co-Processor by RDI
      {
      if (nProcs==2)
         {
         ulProcessorNumber=2;
         ErrRet = pETMVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&ulProcessorNumber,(unsigned long *)&tyGlobalEtmModule);
         ErrRet = CheckForFatalRDIError(ErrRet);
         if (ErrRet != NO_ERROR)
            return ErrRet;
         
         // ETM valid
         bGloablEtmEnabled=TRUE;
         }
      }

   //Step 4. Open the processor
   // use the module/processor procvec->open function
   if(!GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig()->Processor._ARM.bHaltARMCore)
      {
	  // Do not halt the target
	  type |= DONT_HALT_TARGET;
	  }

   ErrRet = pRDIModuleVectorTable->open(tyGlobalArmModule.handle, type, ptyGlobalCfgPtr, &tyGlobalhostif, NULL);
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   if(bGloablEtmEnabled)
     {
     ErrRet = pETMVectorTable->open(tyGlobalEtmModule.handle,type,ptyGlobalCfgPtr,&tyGlobalhostif,NULL);
     ErrRet =  CheckForFatalRDIError(ErrRet);
     if (ErrRet != NO_ERROR)
        return ErrRet;
     }

   //initialise the Auxilliary output macro variables
   InitAuxMacroVariables();    

   // Restore VectorCatch Settings
   unsigned long ulTemp=(unsigned long)GetPFWAppObject()->PathFinder.uiVectorCatch;
   ErrRet = RDITXRXSetGetVectorCatch(&ulTemp,(boolean)TRUE);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   // Debug Comms Channel /Semihosting
   if (GetPFWAppObject()->PathFinder.m_PathFinderTarget != RDISimulator)  //if not a Simulator       
      {
      if (GetPFWAppObject()->PathFinder.iSemiHost == 3) // configure for Standard DCC *only*
         {
         //Configure DCC Options
         ErrRet = ConfigureDCC(); 
         if (ErrRet != NO_ERROR)
            return ErrRet;
         }

      // must have a value of either: 
      //    0 - disable DCC and semihosting
      //    1 - enable standard semihosting with swi
      //    2 - enable semihosting via DCC
      //    3 - Standard DCC
      //    4 - RealMonitor DCC
      // ConfigureSemiHosting is always called in order to set
      //   the necessary attribute in Midlayer.
      // Configure SemiHosting Options
      ErrRet = ConfigureSemiHosting(GetPFWAppObject()->PathFinder.iSemiHost,
                                    GetPFWAppObject()->PathFinder.uiVectorTrap,
                                    GetPFWAppObject()->PathFinder.uiArmSwi,    
                                    GetPFWAppObject()->PathFinder.uiThumbSwi,
                                    GetPFWAppObject()->PathFinder.uiMemSize,
                                    GetPFWAppObject()->PathFinder.uiDccHandlerAddress);
      if (ErrRet != NO_ERROR)
         return ErrRet;
   
      // CACHE CLEAN ADDRESS
      // we want to set the cache clean address for some cores
      switch (GlobalProcType)
         {
         case ARM946ES:
         case ARM920T:
         case ARM922T:
         case ARM940T:
            // get the address
            ulAddress = GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig()->Processor._ARM.ulCacheCleanAddress;
            // set the address
            ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_SetARM9RestartCodeAddress,&ulAddress,NULL);
            // error checking
            ErrRet =  CheckForFatalRDIError(ErrRet);
            if (ErrRet != NO_ERROR)
               return ErrRet;
            break;
         default:
            break;
         }
      }  // end of if(!RDISimulator)

   //Does Processor support Register cache selection
   bGlobalCacheSelection = TRUE;
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_CP15CacheSelection,NULL,NULL);
   if (ErrRet==RDIError_UnimplementedMessage)
      {
      bGlobalCacheSelection = FALSE;
      ErrRet = RDIError_NoError;
      }
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   //Does Processor support Register memory region selection
   bGlobalMemoryRegionSelection = TRUE;
   ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule.handle,RDIInfo_CP15CurrentMemoryArea,NULL,NULL);
   if (ErrRet==RDIError_UnimplementedMessage)
      {
      bGlobalMemoryRegionSelection = FALSE;
      ErrRet = RDIError_NoError;
      }
   ErrRet = CheckForFatalRDIError(ErrRet);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   return NO_ERROR;

}
