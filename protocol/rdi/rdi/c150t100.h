/*
 * RDI 1.5 -> 1.0 protocol converter - c150t100.h
 * External interface to RDI 1.5 -> 1.0 converter
 * Copyright (C) 1998, ARM Ltd. All rights reserved.
 */

/*
 * RCS $Revision: 1.7 $
 * Checkin $Date: 1999/02/22 17:48:21 $
 * Revising $Author: bclarke $
 */

#ifndef c150t100_h
#define c150t100_h

#include "rdi.h"

#if (RDI_VERSION == 0)
#undef RDI_VERSION
#define RDI_VERSION 151
#endif

#if (RDI_VERSION < 150) || (RDI_VERSION >= 200)
#error RDI 1.5x -> 1.0 converter must only be used in an RDI 1.5x environment
#endif
/* Initialise an instance of the RDI protocol converter
 * Parameters:  rdi100 - pointer to the RDI 1.0 vector
 *              rdi150 - write-back pointer for new vector
 *              config - toolconf to use
 * Returns:     An RDI_AgentHandle (pointer to a ConvState)
 */
extern RDI_AgentHandle RDIConv_150To100(
    RDI_ProcVec const *rdi100, RDI_ProcVec const **rdi150,
    RDI_ConfigPointer config);

/* Return a pointer to the configuration handle for this conv_state
 * Parameters: handle - AgentHandle, as returned from RDIConv_150To100
 * Returns:    A pointer to the ConfigBlock inside the converter's state
 */
extern RDI_ConfigPointer RDIConv_GetConfig(RDI_AgentHandle handle);

/* Update the passed in toolconf to reflect the current state of the target
 * Parameters: handle - AgentHandle, as returned from RDIConv_150To100
 *             config - toolconf to be updated
 * Returns:    Updated toolconf
 */
extern RDI_ConfigPointer RDIConv_GetToolconf(
    RDI_AgentHandle handle, RDI_ConfigPointer tcnf);

/* Update the configuration block in the converter's state from a possibly
 * updated ToolConf.
 * Parameters: handle - AgentHandle, as returned from RDIConv_150To100
 *             config - toolconf to update from
 */
extern void RDIConv_UpdateConfig(
    RDI_AgentHandle handle, RDI_ConfigPointer tcnf);

/* Free an instance of the converter. Must NOT be called after CloseAgent,
 * as the CloseAgent call does this anyway. Call if the instance needs to
 * be freed without being used.
 * Parameters: handle - AgentHandle, as returned from RDIConv_150To100
 */
extern void RDIConv_Free(RDI_AgentHandle handle);

/* Call RDI_DriverNames in a given RDI 1.0 vector
 * Parameters: rdi100 - pointer to the RDI 1.0 vector
 * Returns:    NULL if the RDI does not support this call;
 *             a list of driver names otherwise
 */
extern RDI_NameList const *RDIConv_DriverNames(RDI_ProcVec const *rdi100);

/* Call RDI_CPUNames in a given RDI 1.0 vector. Note, if RDIConv_150To100 has already
 * been called, you should call cpunames via the returned RDI 1.5 vector.
 * Parameters: rdi100 - pointer to the RDI 1.0 vector
 * Returns:    NULL if the RDI does not support this call;
 *             a list of CPU names otherwise
 */
extern RDI_NameList const *RDIConv_CPUNames(RDI_ProcVec const *rdi100);

#endif /* c150t100_h */

/* EOF c150t100.h */
