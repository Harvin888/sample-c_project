/****************************************************************************
       Module: MDILAYER.CPP
     Engineer: Suraj S
  Description: All commands within Middle Layer have the same API as the 
               RDI specification.
Date           Initials    Description
3-Jul-2007      SJ          Initial
27-Feb-2008     SJ          Added single stepping support
13-Mar-2008     JCK         Implementation of Single Step for ARM7 core
19-Mar-2008     JR          Added support for performance improvement
24-Mar-2008		 SJ		    Added 968 core support
31-Mar-2008	    VK		    Added RDI JTAG console Support for ARM GDB Server
31-Mar-2008	    RS		    Added global Variables required for ARM GDB Server
06-Jun-2008		RS          Added Linux Support
31-Jul-208      VK/RS       Added ARM11 Support
22-Aug-2009     RTR         ARM11 features addedr
02-Nov-2009		JCK			Cortex-M3 Support added
****************************************************************************/

#ifndef __LINUX
// Win specific defines
#include "stdafx.h"
//#include "windows.h"
#include "stdio.h"
#include <windows.h>
#else
// Linux specific defines
#include "stdio.h"
#include <unistd.h>
#define _MAX_PATH   260
#define __min(a,b)  (((a) < (b)) ? (a) : (b))
#define NO_ERROR 0L 
#endif
#include <string.h>


#include "common.h"
#include "caches.h"
#include "realmon.h"
#include "../../../protocol/rdi/opxdarmdll/ARMdefs.h"
#include "../../../protocol/common/drvopxd.h"
#include "../../../protocol/common/drvopxdarm.h"
#include "../rdi/rdi.h"
#include "semihost.h"
#include "midlayerarm.h"
#include "DISA.h"

#define BYPASS_PATTERN        0xFFFFFFFF
/* TODO:
#if !GDBSERVERDLL
   static CProgressControl    *pOpenUSBProgress;
#endif
*/

extern unsigned long ulGlobalExecuting;
extern unsigned long tyGlobalCauseOfBreak;
extern boolean       bGDBServer;   
extern boolean       bSetSafeNonVectorAddress;

char szOpXDDiskwareFilename[MAX_DRIVER_NAME] = "opxddarm.bin";
char szOpXDFpgawareFilename[MAX_DRIVER_NAME] = "opxdfarm.bin";
char szOpXDFirmwareFilename[MAX_DRIVER_NAME] = "opxdfw.bin";

//Holds the currently selected scan chain.
unsigned long ulGlobalScanChain = FORCE_SCANCHAIN_SELECT;

boolean       bGlobalThumb      = FALSE;
static boolean bWatchPoint      = FALSE;
static boolean bUserHalt        = FALSE;

//This is the address where the PC should be set when performing system speed acceses.
static unsigned long ulGlobalSafeNonVectorAddress = 0x10000;

//This is the base address of the exception table - it is normally 0x0 but can change on  
//certain processors ie ARM720T
static unsigned long ulVectorTableBaseAddress   = 0;

static unsigned long ulEmbeddedICELockedPoints  = 0;

// When semi hosting is set on an ARM9 processor it modifies the Vector register just before
// execution then restores it after halt. This variable holds the value to restore.
static unsigned long ulGlobalVectorCatch = 0x0;
static unsigned long ulTopOfMemory =0;

static unsigned long ARM11_DataArray [6];

//Global variable holds the path to the DLL.
char szGlobalDLLPathName[_MAX_PATH];

static TyBreakPointArray      *pBPArray = NULL;
static TyWatchPointArray      *pWPArray = NULL;

static TyCMBreakPointArray *pCortexBPArray = NULL;

static boolean bGlobalStartedExecution  = FALSE;
unsigned char ucR15 = 0;

//Keeps the reset status
static unsigned char ucLatchedStatus=0; 

// Global variable set when a USB device is selected. 
boolean bUSBDevice = FALSE;

//This stores the number of breakpoints set so far in this debug session
//so that all handles are unique.
static unsigned int usNumberOfBreakpoints = 0x0;
static unsigned int usNumberOfWatchpoints = 0x0;

TyDevHandle tyCurrentDevice     = MAX_DEVICES_SUPPORTED;
#define  MAX_WRITE_BOUNDARY	0x1000
// The hardware breakpoint resources can be locked by the debug host 
// using these values for the mask.
#define WP0LockMask   0x1
#define WP1LockMask   0x2

unsigned long RegArray [MAXMODES] [MAXREGISTERS] = {0};
unsigned long CortexMRegArray[CM_MAXREGISTERS] = {0};

static TXRXCauseOfBreak    tyLocalCauseOfBreak  = COB_UNKNOWN;

static TyVectorCatchStructure    tyVectorCatchConfig[0x9] = {0};

static TyBPResourceStructure  tyBPResourceConfig   = {0};
static TyBPResourceARM11Structure tyBPResourceARM11Config   = {0};
static TyWPResourceARM11Structure tyWPResourceARM11Config = {0};

/* Added for Cortex-M support */
static TyBPResourceCortexMStructure tyArmv7MBreakpointResArray = {MAX_NUM_COMPARATORS};
TyCortexMWpResStructArray tyArmv7MWatchpointResArray = {MAX_NUM_WATCHPOINT_COMP,0};

/* Added for Cortex-A support */
TyArmv7ABreakpointResArray tyArmv7ABreakpointResArray = {MAX_NUM_COMPARATORS, 0};
TyArmv7AWatchpointResArray tyArmv7AWatchpointResArray = {MAX_NUM_COMPARATORS, 0};

static unsigned char ucBitInverse[] =
{
0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, 
0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0, 
0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, 
0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, 
0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, 
0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4, 
0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, 
0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC, 
0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, 
0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2, 
0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, 
0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA, 
0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, 
0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, 
0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, 
0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE, 
0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, 
0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1, 
0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, 
0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9, 
0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, 
0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5, 
0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, 
0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD, 
0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, 
0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3, 
0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, 
0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB, 
0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, 
0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7, 
0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, 
0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF, 
};

// SemiHosting configuration
typedef struct
{
  unsigned long ulSemiHostingType;
  unsigned long ulSHVectorAddress;
  unsigned long ulDCCServiceRoutineAddress;
  unsigned long ulBreakpointHandle;
  boolean       bBreakpointSet;
  unsigned long ulARMSWIInstruction;
  unsigned long ulThumbSWIInstruction;
  boolean       bSemiHostingSet;

} TySemiHostingConfig;

static TySemiHostingConfig    tySemiHostingConfig  = {0x1,0x8,0x70000,0,FALSE,0x123456,0xab};

// This holds the processor type information ie arm9, co-processor, dcc etc.
TyProcessorConfig	tyProcessorConfig    = {0};

TyTargetConfig tyTargetConfig;

TyCoreConfig ptyCoreConfig[16];
unsigned long  pulBypassIRPattern[16];
unsigned long pulIRLengthArray[16] = {0};
boolean bSWIInstruction = 0;
extern boolean bStep;
extern TyError RDI_Convert_MidLayerErr_ToRDIErr (TyError tyMidLayerErr);

/* Local Function prototypes */ /* Added For CortexM support */
static TyError ML_SetCortexMHardwareBreakPoint (unsigned long ulAddress, 
                                                 boolean       bARMBP, 
                                                 unsigned long ulResourceNo);

static TyError ML_SetArmv7AHardwareBreakPoint(unsigned long ulAddress,
												boolean bARMBP,
												unsigned long ulResourceNo);

static TyError ML_SetCortexMHardwareWatchPoint (unsigned long ulAddress, 
                                                 unsigned long ulType, 
                                                 unsigned long ulDataType, 
                                                 unsigned long ulBound, 
                                                 unsigned long ulResourceNo);

static TyError ML_SetArmv7AHardwareWatchPoint  (unsigned long ulAddress, 
                                                 unsigned long ulType, 
                                                 unsigned long ulDataType, 
                                                 unsigned long ulBound, 
                                                 unsigned long ulResourceNo);


static void ML_ClearCortexMWPResource (unsigned long ulResourceNo,
										unsigned long ulCurrBreakNum);
void SleepFunc(unsigned long ulDelay)
{
#ifdef __LINUX
      usleep(ulDelay* 1000);
      #endif
      #ifdef _WINDOWS
      Sleep(ulDelay);
      #endif
}


/****************************************************************************
    Function: ML_StoreFullPathForDLL
    Engineer: Suraj S
       Input: Storage for path name.
      Output: None
 Description: Stores the path name to a global variable
Date          Initials   Description
3-Jul-2007      SJ       Initial
****************************************************************************/
void ML_StoreFullPathForDLL(char *pszPathName)
{
	strcpy (szGlobalDLLPathName,pszPathName);
	strcat (szGlobalDLLPathName, "/");
}

/****************************************************************************
     Function: ML_CheckOpellaXDInstance
     Engineer: Suraj S
        Input: char szSerialNumber - string with Opella-XD serial number
       Output: none
  Description: fill valid instance number if string is empty
               this is temporary solution
Date           Initials    Description
3-Jul-2007      SJ         Initial
****************************************************************************/
extern "C" void ML_CheckOpellaXDInstance (char pszSerialNumber[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN],
										  unsigned short *usNumberOfDevices)
{
	unsigned short usDeviceCount;
	char ppszLocDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN];

   // if string is not empty, then exit (everything is fine)
	if (strcmp(pszSerialNumber[0], ""))
	  return;

	DL_OPXD_GetConnectedDevices(ppszLocDeviceNumbers, &usDeviceCount, OPELLAXD_USB_PID);
	if (usDeviceCount)
	  {
      *usNumberOfDevices = usDeviceCount;
      //copy all the connected devices serial nos
      for(unsigned short i=0; i<usDeviceCount; i++)
      {
         strcpy(pszSerialNumber[i],ppszLocDeviceNumbers[i]);
      }
	  }
}

/****************************************************************************
     Function: ML_GetFreeResources
     Engineer: Roshan T R
        Input: unsigned long *ulFreeResource1 - Pointer to the free resource1
		       unsigned long *ulFreeResource2 - Pointer to the free resource2
       Output: iFreeResources - Number of resources available to use
  Description: check for at least two free resources for range checking
Date           Initials    Description
24-Aug-2009    RTR         Initial
****************************************************************************/
static int ML_GetFreeResources(unsigned char *ucFreeResource1, unsigned char *ucFreeResource2)
{
	int iFreeResources = 0;

	if ((tyBPResourceARM11Config.bBP0Locked == FALSE)     &&
		(tyBPResourceARM11Config.ucBP0Used  == NOT_USED))
	{
		*ucFreeResource1 = HARDWARE_RESOURCE0;
		++iFreeResources;
	}

	if ((tyBPResourceARM11Config.bBP1Locked == FALSE)     &&
		(tyBPResourceARM11Config.ucBP1Used  == NOT_USED))
	{
		if(iFreeResources == 0)
		{
			*ucFreeResource1 = HARDWARE_RESOURCE1;
		}
		else
		{
			*ucFreeResource2 = HARDWARE_RESOURCE1;
		}
		if(++iFreeResources >= 2) 
			return iFreeResources;
	}

	if ((tyBPResourceARM11Config.bBP2Locked == FALSE)     &&
		(tyBPResourceARM11Config.ucBP2Used  == NOT_USED))
	{
		if(iFreeResources == 0)
		{
			*ucFreeResource1 = HARDWARE_RESOURCE2;
		}
		else
		{
			*ucFreeResource2 = HARDWARE_RESOURCE2;
		}
		if(++iFreeResources >= 2) 
			return iFreeResources;
	}

	if ((tyBPResourceARM11Config.bBP3Locked == FALSE)     &&
		(tyBPResourceARM11Config.ucBP3Used  == NOT_USED))
	{
		if(iFreeResources == 0)
		{
			*ucFreeResource1 = HARDWARE_RESOURCE3;
		}
		else
		{
			*ucFreeResource2 = HARDWARE_RESOURCE3;
		}
		if(++iFreeResources >= 2) 
			return iFreeResources;
	}

	if ((tyBPResourceARM11Config.bBP4Locked == FALSE)     &&
		(tyBPResourceARM11Config.ucBP4Used  == NOT_USED))
	{
		if(iFreeResources == 0)
		{
			*ucFreeResource1 = HARDWARE_RESOURCE4;
		}
		else
		{
			*ucFreeResource2 = HARDWARE_RESOURCE4;
		}
		if(++iFreeResources >= 2) 
			return iFreeResources;
	}

	if ((tyBPResourceARM11Config.bBP5Locked == FALSE)     &&
		(tyBPResourceARM11Config.ucBP5Used  == NOT_USED))
	{
		if(iFreeResources == 0)
		{
			*ucFreeResource1 = HARDWARE_RESOURCE5;
		}
		else
		{
			*ucFreeResource2 = HARDWARE_RESOURCE5;
		}
		++iFreeResources;
	}

	return iFreeResources;
}

/****************************************************************************
     Function: ML_UpdateOpellaXDFirmware
     Engineer: Suraj S
        Input: TyOpenOpellaXDCallback tyCallback - callback function for progres
               const char szSerialNumber - serial number of Opella-XD
       Output: TyError - Mid Layer error code
  Description: check for Opella-XD firmware upgrade and upgrade device if newer version is available
Date           Initials    Description
3-Jul-2007      SJ         Initial
7-Apr-2008		RS		Modified for GDBServerForARM
****************************************************************************/
TyError ML_UpdateOpellaXDFirmware(TyOpenOpellaXDCallback tyCallback, const char *pszSerialNumber)
{
	int iFwDiff;
	TyDevHandle tyHandle;
	char szFirmwareFullPath[_MAX_PATH];
	unsigned char bReconnect = 0;
	TyError ErrRet = ML_ERROR_NO_ERROR;
   char szErrorMsg[_MAX_PATH];

	// get full path to Opella-XD firmware
	strcpy(szFirmwareFullPath, szGlobalDLLPathName);
	strcat(szFirmwareFullPath, szOpXDFirmwareFilename);

	if (tyCallback != NULL)
	  tyCallback(0);
	switch (DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle))
	  {
	  case DRVOPXD_ERROR_NO_ERROR:
		 break;
	  case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
		 ErrRet = ML_ERROR_INVALID_SERIAL_NO;
	  #ifndef __LINUX
      if(!bGDBServer)
         { 
         sprintf(szErrorMsg,"Opella-XD with Serial no. %s is not connected",pszSerialNumber);
          //MessageBox(NULL,"Opella-XD is not connected. Connect Opella-XD and Retry",OPXDCAPTION,MB_OK|MB_ICONWARNING);
         MessageBox(NULL,szErrorMsg,OPXDCAPTION,MB_OK|MB_ICONWARNING);
		 //exit(0);
          }
	  #endif
		 return ErrRet;
		 break;
	  case DRVOPXD_ERROR_DEVICE_ALREADY_IN_USE:
		  ErrRet = ML_ERROR_DEVICE_ALREADY_IN_USE;
	  #ifndef __LINUX
	  if(!bGDBServer)
		{ 
		sprintf(szErrorMsg,"Opella-XD with Serial no. %s is already in use",pszSerialNumber);
		//MessageBox(NULL,"Opella-XD is not connected. Connect Opella-XD and Retry",OPXDCAPTION,MB_OK|MB_ICONWARNING);
		MessageBox(NULL,szErrorMsg,OPXDCAPTION,MB_OK|MB_ICONWARNING);
		//exit(0);
		}
	  #endif
		  return ErrRet;
		  break;
	  default:
		 ErrRet = ML_ERROR_OPELLA_FAILURE;
		 return ErrRet;
		 break;
	  }
	if (tyCallback != NULL)
	  tyCallback(5);
	(void)DL_OPXD_UnconfigureDevice(tyHandle);
	if (tyCallback != NULL)
	  tyCallback(10);
	// try to upgrade firmware
	if (DL_OPXD_UpgradeDevice(tyHandle, UPG_NEWER_VERSION, szFirmwareFullPath, &bReconnect, &iFwDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
	  {
	  (void)DL_OPXD_CloseDevice(tyHandle);
	  if (tyCallback != NULL)
		 tyCallback(100);
	  ErrRet = ML_ERROR_OPELLA_FAILURE;
	  }

	if (bReconnect)
	  {
	  unsigned char ucRestartCnt;

	  (void)DL_OPXD_CloseDevice(tyHandle);
	  for (ucRestartCnt=0; ucRestartCnt<8; ucRestartCnt++)
		 {
		 if (tyCallback != NULL)
			tyCallback((unsigned char)(15 + (ucRestartCnt*10)));
		 #ifdef __LINUX
           usleep(1000* 1000);
         #endif
          #ifdef _WINDOWS
		 Sleep(1000);
          #endif
		 }
	  if ((DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyHandle) != DRVOPXD_ERROR_NO_ERROR) ||
		  (DL_OPXD_UpgradeDevice(tyHandle, UPG_CHECK_ONLY, szFirmwareFullPath, &bReconnect, &iFwDiff, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR) || 
		  (iFwDiff != 0))
		 {
		 ErrRet = ML_ERROR_OPELLA_FAILURE;
		 }
	  }
	else
	  {
	  if (tyCallback != NULL)
		 tyCallback(95);
	  }
	// close handle
	(void)DL_OPXD_CloseDevice(tyHandle);
	if (tyCallback != NULL)
	  tyCallback(100);

	return ErrRet;
}

/****************************************************************************
     Function: ML_OpenOpellaXD
     Engineer: Suraj S
        Input: TyOpenOpellaXDCallback tyCallback - callback function for progres
               const char szSerialNumber - serial number of Opella-XD
       Output: TyError - Mid Layer error code
  Description: open Opella-XD device and set handle
Date           Initials    Description
3-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_OpenOpellaXD(TyOpenOpellaXDCallback tyCallback, const char *pszSerialNumber)
{
	char szDiskwareFullPath[_MAX_PATH];
	char szFpgawareFullPath[_MAX_PATH];
	TyError ErrRet = ML_ERROR_NO_ERROR;

	// get full path to Opella-XD diskware and FPGAware for ARM
	strcpy(szDiskwareFullPath, szGlobalDLLPathName);
	strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
	strcpy(szFpgawareFullPath, szGlobalDLLPathName);
	strcat(szFpgawareFullPath, szOpXDFpgawareFilename);

	if (tyCallback != NULL)
		tyCallback(0);
	switch (DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyCurrentDevice))
	{
		case DRVOPXD_ERROR_NO_ERROR:
			bUSBDevice = TRUE;
		break;
		case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
			tyCurrentDevice = MAX_DEVICES_SUPPORTED;
			ErrRet = ML_ERROR_INVALID_SERIAL_NO;
		break;
		default:
			tyCurrentDevice = MAX_DEVICES_SUPPORTED;
			ErrRet = ML_ERROR_OPELLA_FAILURE;
		break;
	}
	(void)DL_OPXD_UnconfigureDevice(tyCurrentDevice);

   //if (DL_OPXD_ConfigureDevice(tyCurrentDevice, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
		//ErrRet = ML_ERROR_OPELLA_FAILURE;
	ErrRet = DL_OPXD_ConfigureDevice(tyCurrentDevice, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath);
   return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
}

/****************************************************************************
    Function: ML_ResetCheck
    Engineer: Suraj S
       Input: TyError tyOriginalErrorMessage - original error message
      Output: Error Status
 Description: Checks whether target reset has been detected.
Date          Initials      Description
3-Jul-2007       SJ         Initial
****************************************************************************/
TyError ML_ResetCheck (TyError tyOriginalErrorMessage)
{
   TyError ErrRet;

      ErrRet = DL_OPXD_Arm_ResetProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 0, &ucLatchedStatus, NULL);
      if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
	  {
		  ErrRet= ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
		  return RDI_Convert_MidLayerErr_ToRDIErr(ErrRet);
	  }

   //Check whether target has been reset
   if ((ucLatchedStatus & TARGET_RESET) == TARGET_RESET)
      {
      // Wait for reset to go inactive again, before continuing...
      ML_Delay (100);

      ErrRet = ML_Initialise_Debugger(&tyTargetConfig);
                                      //ptyCoreConfig);

      if (ErrRet != ML_ERROR_NO_ERROR)
         return (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
      }

   return tyOriginalErrorMessage;
}

/****************************************************************************
    Function: ML_AllocateMemForBreakpoints
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Allocates the memory for breakpoint structure.
Date          Initials      Description
5-Jul-2007       SJ         Initial
****************************************************************************/
TyError ML_AllocateMemForBreakpoints (void)
{
	//Allocate memory if not already allocated.
	if ((tyProcessorConfig.bCortexM3) ||
		(tyProcessorConfig.bCortexA8) ||
		(tyProcessorConfig.bCortexA9))
	{
		if (NULL == pCortexBPArray)
		{
			pCortexBPArray = (TyCMBreakPointArray  *)malloc(sizeof(TyCMBreakPointArray ));

			if (pCortexBPArray == NULL)
				return ML_Error_InitTarget_Opella;
			
			memset (pCortexBPArray,0x0,sizeof(TyBreakPointArray));
		}
	}
	else
	{
		if (pBPArray == NULL)
		{
			pBPArray = (TyBreakPointArray *)malloc(sizeof(TyBreakPointArray));
		
			if (pBPArray == NULL)
				return ML_Error_InitTarget_Opella;
		
			memset (pBPArray,0x0,sizeof(TyBreakPointArray));
		}
	}

	

	// Since we are allocating memory for breakpoints, we should forget about
	// any breakpoints we had set in the past....
	usNumberOfBreakpoints = 0x0;
	
	// We have just removed all the breakpoints so clear the flag...
	tySemiHostingConfig.bBreakpointSet = FALSE;
	
	// We also need to forget about all vector breakpoints...
	memset(tyVectorCatchConfig, 0x0, sizeof(tyVectorCatchConfig));
	
	return ML_ERROR_NO_ERROR;   
}

/****************************************************************************
    Function: ML_AllocateMemForWatchPoints
    Engineer: Roshan T R
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Allocates the memory for watchpoint structure.
Date          Initials      Description
27-Aug-2009   RTR           Initial
****************************************************************************/
TyError ML_AllocateMemForWatchpoints (void)
{
	//Allocate memory for watchpoint if not already allocated.
	if (pWPArray == NULL)
	{
		pWPArray = (TyWatchPointArray *)malloc(sizeof(TyWatchPointArray));
		
		if (pWPArray == NULL)
			return ML_Error_InitTarget_Opella;
		
		memset (pWPArray,0x0,sizeof(TyWatchPointArray));
	}
	
	// Since we are allocating memory for watchpoints, we should forget about
	// any watchpoints we had set in the past....
	usNumberOfWatchpoints = 0x0;

	return ML_ERROR_NO_ERROR;   
}

/****************************************************************************
    Function: ML_DeAllocateMemForBreakpoints
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Deallocates memory for breakpoint structure.
Date          Initials      Description
10-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_DeAllocateMemForBreakpoints (void)
{
	if (pBPArray != NULL)
	{
		free (pBPArray);
		pBPArray  = NULL;
	}

	if (pCortexBPArray != NULL)
	{
		free (pCortexBPArray);
		pCortexBPArray = NULL;
	}

	// Since we are deallocating memory for breakpoints, we should forget 
	// about any breakpoints we had set in the past....
	usNumberOfBreakpoints = 0x0;
	
	// We have just removed all the breakpoints so clear the flag...
	tySemiHostingConfig.bBreakpointSet = FALSE;
	
	// We also need to forget about all vector breakpoints...
	memset(tyVectorCatchConfig, 0x0, sizeof(tyVectorCatchConfig));
	
	return ML_ERROR_NO_ERROR;   
}

/****************************************************************************
    Function: ML_DeAllocateMemForWatchPoints
    Engineer: Roshan T R
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Deallocates memory for watchpoint structure.
Date          Initials      Description
27-Aug-2009   RTR           Initial
****************************************************************************/
TyError ML_DeAllocateMemForWatchpoints (void)
{
	if (pWPArray != NULL)
	{
		free (pWPArray);
		pWPArray  = NULL;
	}

	// Since we are deallocating memory for watchpoints, we should forget 
	// about any watchpoints we had set in the past....
	usNumberOfWatchpoints = 0;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_RemoveAllBreakpoints
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Removes all the breakpoints and Watchpoints
Date          Initials      Description
10-Jul-2007   SJ            Initial
26-Aug-2009   RTR           Added ARM11 support
****************************************************************************/
TyError ML_RemoveAllBreakpoints (void)
{  
	TyError ErrRet  = ML_ERROR_NO_ERROR;

	// Can't clear breakpoints if we are running...
	if (bGlobalStartedExecution)
		return ML_Error_Executing;

	while (usNumberOfBreakpoints != 0)
	{
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			if (pCortexBPArray->BreakpointStructures[0].bWatchPoint)
			{
				ErrRet = ML_ClearWatchProc(pCortexBPArray->BreakpointStructures[0].usBreakpointId);
				if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;
			}
			else 
			{
				ErrRet = ML_ClearBreakProc(pCortexBPArray->BreakpointStructures[0].usBreakpointId);
				if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;
			}
		} 
		else
		{
			if (pBPArray->BreakpointStructures[0].bWatchPoint)
			{
				ErrRet = ML_ClearWatchProc(pBPArray->BreakpointStructures[0].usBreakpointId);
				if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;
			}
			else 
			{
				ErrRet = ML_ClearBreakProc(pBPArray->BreakpointStructures[0].usBreakpointId);
				if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;
			}
		}
		
	}

	// Now all the breakpoints have been removed to cope with crashes we zero all 
	// breakpoint resources.
    if ((tyProcessorConfig.bARM7) ||
        (tyProcessorConfig.bARM9))
	{
		tyBPResourceConfig.ulWP0AddressValue   = 0;
		tyBPResourceConfig.ulWP0AddressMask    = 0;
		tyBPResourceConfig.ulWP0DataValue      = 0;
		tyBPResourceConfig.ulWP0DataMask       = 0;
		tyBPResourceConfig.ulWP0CtrlValue      = 0;
		tyBPResourceConfig.ulWP0CtrlMask       = 0;
		tyBPResourceConfig.ulWP1AddressValue   = 0;
		tyBPResourceConfig.ulWP1AddressMask    = 0;
		tyBPResourceConfig.ulWP1DataValue      = 0;
		tyBPResourceConfig.ulWP1DataMask       = 0;
		tyBPResourceConfig.ulWP1CtrlValue      = 0;
		tyBPResourceConfig.ulWP1CtrlMask       = 0;

		tyBPResourceConfig.ucWP0Used           = 0;
		tyBPResourceConfig.ucWP1Used           = 0;
		tyBPResourceConfig.bWP0Locked          = FALSE;
		tyBPResourceConfig.bWP1Locked          = FALSE;
	}
    else if (tyProcessorConfig.bARM11)
	{
		tyBPResourceARM11Config.ulBP0AddressValue = 0;
		tyBPResourceARM11Config.ulBP0CtrlValue = 0;
		tyBPResourceARM11Config.ulBP1AddressValue = 0;
		tyBPResourceARM11Config.ulBP1CtrlValue = 0;
		tyBPResourceARM11Config.ulBP2AddressValue = 0;
		tyBPResourceARM11Config.ulBP2CtrlValue = 0;
		tyBPResourceARM11Config.ulBP3AddressValue = 0;
		tyBPResourceARM11Config.ulBP3CtrlValue = 0;
		tyBPResourceARM11Config.ulBP4AddressValue = 0;
		tyBPResourceARM11Config.ulBP4CtrlValue = 0;
		tyBPResourceARM11Config.ulBP5AddressValue = 0;
		tyBPResourceARM11Config.ulBP5CtrlValue = 0;

		tyBPResourceARM11Config.ucBP0Used = 0;
		tyBPResourceARM11Config.ucBP1Used = 0;
		tyBPResourceARM11Config.ucBP2Used = 0;
		tyBPResourceARM11Config.ucBP3Used = 0;
		tyBPResourceARM11Config.ucBP4Used = 0;
		tyBPResourceARM11Config.ucBP5Used = 0;
		tyBPResourceARM11Config.bBP0Locked = FALSE;
		tyBPResourceARM11Config.bBP1Locked = FALSE;
		tyBPResourceARM11Config.bBP2Locked = FALSE;
		tyBPResourceARM11Config.bBP3Locked = FALSE;
		tyBPResourceARM11Config.bBP4Locked = FALSE;
		tyBPResourceARM11Config.bBP5Locked = FALSE;
	}

    else if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
    {

    }
	// We have just removed all the breakpoints so clear the flag...
	tySemiHostingConfig.bBreakpointSet = FALSE;

	// We also need to forget about all vector breakpoints...
	memset(tyVectorCatchConfig, 0x0, sizeof(tyVectorCatchConfig));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_GetUseOnlyHardwareBreakPoints
    Engineer: Suraj S
       Input: unsigned long *pulUseHWBreakpoints - pointer to store the 
                                                   current HW BP's
      Output: NONE
 Description: Gets the currently used hardware BP's.
Date          Initials      Description
17-Jul-2007   SJ            Initial
26-Aug-2009   RTR           Added ARM11 support 
21-Apr-2010   JCK           Cortex-A8 support added
****************************************************************************/
void ML_GetUseOnlyHardwareBreakPoints (unsigned long *pulUseHWBreakpoints)
{
	if((tyProcessorConfig.bARM7) ||
		(tyProcessorConfig.bARM9))
	{
		*pulUseHWBreakpoints = (unsigned long) tyBPResourceConfig.bUseOnlyHWBreakPoints;
	}
	else if(tyProcessorConfig.bARM11)
	{
		*pulUseHWBreakpoints = (unsigned long) tyBPResourceARM11Config.bUseOnlyHWBreakPoints;
	}
	else if (tyProcessorConfig.bCortexM3)
	{
		*pulUseHWBreakpoints = (unsigned long) tyArmv7MBreakpointResArray.bUseOnlyHWBreakPoints;
	}

	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		*pulUseHWBreakpoints = (unsigned long) tyArmv7ABreakpointResArray.bUseOnlyHWBreakPoints;
	}
}

/****************************************************************************
    Function: ML_SetUseOnlyHardwareBreakPoints
    Engineer: Suraj S
       Input: unsigned long ulUseHWBreakpoints - current HW BP's
      Output: NONE
 Description: Sets the currently used hardware BP's.
Date          Initials      Description
17-Jul-2007   SJ            Initial
26-Aug-2009   RTR           Added ARM11 support
21-Apr-2010   JCK           Cortex-A8 support added
****************************************************************************/
void ML_SetUseOnlyHardwareBreakPoints (unsigned long ulUseHWBreakpoints)
{
	if ((tyProcessorConfig.bARM7) ||
		(tyProcessorConfig.bARM9))
	{
		tyBPResourceConfig.bUseOnlyHWBreakPoints = (boolean) ulUseHWBreakpoints;
	}
	else if (tyProcessorConfig.bARM11)
	{
		tyBPResourceARM11Config.bUseOnlyHWBreakPoints = (boolean) ulUseHWBreakpoints;
	}
	else if (tyProcessorConfig.bCortexM3)
	{
		tyArmv7MBreakpointResArray.bUseOnlyHWBreakPoints = (boolean) ulUseHWBreakpoints;
	}	
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		tyArmv7ABreakpointResArray.bUseOnlyHWBreakPoints = (boolean) ulUseHWBreakpoints;
	}	
}

/****************************************************************************
    Function: ML_SetHardwareBreakPoint
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address at which hardware BP is to be set.
              boolean bARMBP - True if ARM BP, False if THUMB BP.
              unsigned long ulResourceNo - Hardware resource number to use. 
              unsigned long ulBound - 
      Output: TyError - Mid Layer error code
 Description: Sets a hardware breakpoint at the given address.
Date          Initials      Description
18-Jul-2007      SJ         Initial
****************************************************************************/
static TyError ML_SetHardwareBreakPoint (unsigned long ulAddress, 
                                         boolean       bARMBP, 
                                         unsigned long ulResourceNo, 
                                         unsigned long ulBound)

{
   //The mask for breakpoint is an XOR mask not an AND mask so if the mask is not 
   //zero then we need to invert it.

   //We mask off bit zero of the address mask so that BX into thumb mode still works.
   //It actually fetches from the address 0x8001 for example, when the instruction is at 0x8000.
   if (ulBound != 0)
      ulBound = ~ulBound;

   if (ulResourceNo == HARDWARE_RESOURCE0)
      {
      // Setup WP0...
      tyBPResourceConfig.ucWP0Used         = HARDWARE_BP; 
      tyBPResourceConfig.ulWP0AddressValue = ulAddress; 
      tyBPResourceConfig.ulWP0AddressMask  = (ulBound | 0x1);
      tyBPResourceConfig.ulWP0DataValue	 = ARM_BP_DONT_CARE; 
      tyBPResourceConfig.ulWP0DataMask		 = ARM_BP_DONT_CARE; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         if (bARMBP)
	   	   tyBPResourceConfig.ulWP0CtrlValue = ARM7TDMI_ARM_SW_BP_VALUE; 
         else
	   	   tyBPResourceConfig.ulWP0CtrlValue = ARM7TDMI_THUMB_SW_BP_VALUE; 

	   	tyBPResourceConfig.ulWP0CtrlMask  = ARM7TDMI_ARM_SW_BP_MASK ; 
         }
      else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
            // Set watchpoint registers for ARM7DI board
            {
	   	   tyBPResourceConfig.ulWP0CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
	   	   tyBPResourceConfig.ulWP0CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
            }
      
      else if (tyProcessorConfig.bARM9) 
      
            // Set watchpoint registers for ARM966ES board
            {
            if (bARMBP)
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
            else
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_THUMB_SW_BP_VALUE; 

	   	   tyBPResourceConfig.ulWP0CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
         }
      }
   else
   if (ulResourceNo == HARDWARE_RESOURCE1)
      {
      // Setup WP1...
      tyBPResourceConfig.ucWP1Used         = HARDWARE_BP; 
      tyBPResourceConfig.ulWP1AddressValue = ulAddress; 
      tyBPResourceConfig.ulWP1AddressMask  = (ulBound | 0x1);
      tyBPResourceConfig.ulWP1DataValue	 = ARM_BP_DONT_CARE; 
      tyBPResourceConfig.ulWP1DataMask		 = ARM_BP_DONT_CARE; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         if (bARMBP)
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7TDMI_ARM_SW_BP_VALUE; 
         else
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7TDMI_THUMB_SW_BP_VALUE; 

	   	tyBPResourceConfig.ulWP1CtrlMask  = ARM7TDMI_ARM_SW_BP_MASK ; 
         }
      else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
            // Set watchpoint registers for ARM7DI board
            {
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
	   	   tyBPResourceConfig.ulWP1CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
            }
      
      else if (tyProcessorConfig.bARM9) 
      
            // Set watchpoint registers for ARM966ES board
            {
            if (bARMBP)
	   	      tyBPResourceConfig.ulWP1CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
            else
	   	      tyBPResourceConfig.ulWP1CtrlValue = ARM9TDMI_THUMB_SW_BP_VALUE; 
	   	
            tyBPResourceConfig.ulWP1CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
         }
      }
   else
      {
//TODO:      ASSERT_NOW();
      return ML_Error_CantSetPoint;
      }

 	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetHardwareBreakPointRange
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address at which hardware BP is to be set.
              boolean bARMBP - True if ARM BP, False if THUMB BP.
              unsigned long ulBound - 
      Output: TyError - Mid Layer error code
 Description: Sets a hardware breakpoint from the given address to the address + bound.
Date          Initials      Description
18-Jul-2007      SJ         Initial
****************************************************************************/
static TyError ML_SetHardwareBreakPointRange (unsigned long ulAddress, 
                                              boolean       bARMBP, 
                                              unsigned long ulBound)

{
      // Setup WP0...
      tyBPResourceConfig.ucWP0Used         = HARDWARE_BP; 
      tyBPResourceConfig.ulWP0AddressValue = ulAddress + ulBound; 
      tyBPResourceConfig.ulWP0AddressMask  = 0x0F;
      tyBPResourceConfig.ulWP0DataValue	 = ARM_BP_DONT_CARE; 
      tyBPResourceConfig.ulWP0DataMask		 = ARM_BP_DONT_CARE; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         if (bARMBP)
	   	   tyBPResourceConfig.ulWP0CtrlValue = 0x184;
         else
	   	   tyBPResourceConfig.ulWP0CtrlValue = 0x184;

	   	tyBPResourceConfig.ulWP0CtrlMask  = 0x76;
         }
      else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
            // Set watchpoint registers for ARM7DI board
            {
	   	   tyBPResourceConfig.ulWP0CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
	   	   tyBPResourceConfig.ulWP0CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
            }
      
      else if (tyProcessorConfig.bARM9) 
      
            // Set watchpoint registers for ARM966ES board
            {
            if (bARMBP)
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
            else
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_THUMB_SW_BP_VALUE; 

	   	   tyBPResourceConfig.ulWP0CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
         }
      
   
      // Setup WP1...
      tyBPResourceConfig.ucWP1Used         = HARDWARE_BP; 
      tyBPResourceConfig.ulWP1AddressValue = ulAddress; 
      tyBPResourceConfig.ulWP1AddressMask  = 0x1F;
      tyBPResourceConfig.ulWP1DataValue	 = ARM_BP_DONT_CARE; 
      tyBPResourceConfig.ulWP1DataMask		 = ARM_BP_DONT_CARE; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         if (bARMBP)
	   	   tyBPResourceConfig.ulWP1CtrlValue = 0x004;
         else
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7TDMI_THUMB_SW_BP_VALUE; 

	   	tyBPResourceConfig.ulWP1CtrlMask  = 0x76;
         }
      else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
            // Set watchpoint registers for ARM7DI board
            {
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
	   	   tyBPResourceConfig.ulWP1CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
            }
      
      else if (tyProcessorConfig.bARM9) 
      
            // Set watchpoint registers for ARM966ES board
            {
            if (bARMBP)
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
            else
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_THUMB_SW_BP_VALUE; 
	   	
            tyBPResourceConfig.ulWP1CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
         }

 	return ML_ERROR_NO_ERROR;
}


/****************************************************************************
Function: ML_SetARM11HardwareBreakPoint
Engineer: Venkitakrishnan K
Input: unsigned long ulAddress - Address at which hardware BP is to be set.
boolean bARMBP - True if ARM BP, False if THUMB BP.
unsigned long ulResourceNo - Hardware resource number to use. 
Output: TyError - Mid Layer error code
Description: Sets a hardware breakpoint at the given address.
Date          Initials      Description
31-Jul-2008   VK           Initial
22-Aug-2009   RTR          Address calculation for ARM11 done
****************************************************************************/
static TyError ML_SetARM11HardwareBreakPoint (unsigned long ulAddress, 
                                              boolean       bARMBP, 
                                              unsigned long ulResourceNo)
											  
{
    unsigned long ulBPCtrlWord;
    unsigned long ulBPAddressWord;
	unsigned char ucBytePosition;

	bARMBP = bARMBP;

	ucBytePosition = (unsigned char)(ulAddress % 4);
    ulBPAddressWord = ulAddress - ucBytePosition;

    if(ucBytePosition == 3)
    {
        ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE3_ACCESS_CTRL_VALUE;
    }
    else if(ucBytePosition == 2)
    {
        ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE2_ACCESS_CTRL_VALUE;
    }
    else if(ucBytePosition == 1)
    {
        ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE1_ACCESS_CTRL_VALUE;
    }
	else
    {
      //ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE0_ACCESS_CTRL_VALUE;
		//This is word access now
		ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_WORD_ACCESS_CTRL_VALUE;
    }

    if (ulResourceNo == HARDWARE_RESOURCE0)
    {
        // Setup BP0...
        tyBPResourceARM11Config.ucBP0Used         = HARDWARE_BP; 
        tyBPResourceARM11Config.ulBP0AddressValue = ulBPAddressWord; 
        tyBPResourceARM11Config.ulBP0CtrlValue	= ulBPCtrlWord; 
    }
    else if (ulResourceNo == HARDWARE_RESOURCE1)
    {
        // Setup BP1...
        tyBPResourceARM11Config.ucBP1Used         = HARDWARE_BP; 
        tyBPResourceARM11Config.ulBP1AddressValue = ulBPAddressWord; 
        tyBPResourceARM11Config.ulBP1CtrlValue	= ulBPCtrlWord; 
    }
    else if (ulResourceNo == HARDWARE_RESOURCE2)
    {
        // Setup BP2...
        tyBPResourceARM11Config.ucBP2Used         = HARDWARE_BP; 
        tyBPResourceARM11Config.ulBP2AddressValue = ulBPAddressWord; 
        tyBPResourceARM11Config.ulBP2CtrlValue	= ulBPCtrlWord; 
    }
    else if (ulResourceNo == HARDWARE_RESOURCE3)
    {
        // Setup BP3...
        tyBPResourceARM11Config.ucBP3Used         = HARDWARE_BP; 
        tyBPResourceARM11Config.ulBP3AddressValue = ulBPAddressWord; 
        tyBPResourceARM11Config.ulBP3CtrlValue	= ulBPCtrlWord; 
    }
    else if (ulResourceNo == HARDWARE_RESOURCE4)
    {
        // Setup BP4...
        tyBPResourceARM11Config.ucBP4Used         = HARDWARE_BP; 
        tyBPResourceARM11Config.ulBP4AddressValue = ulBPAddressWord; 
        tyBPResourceARM11Config.ulBP4CtrlValue	= ulBPCtrlWord; 
    }
    else if (ulResourceNo == HARDWARE_RESOURCE5)
    {
        // Setup BP5...
        tyBPResourceARM11Config.ucBP5Used         = HARDWARE_BP; 
        tyBPResourceARM11Config.ulBP5AddressValue = ulBPAddressWord; 
        tyBPResourceARM11Config.ulBP5CtrlValue	= ulBPCtrlWord; 
    }
	else
	{
		return ML_Error_CantSetPoint;
	}
	

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetARM11HardwareBreakPointRange
    Engineer: Roshan T R
       Input: unsigned long ulAddress - Address at which hardware BP is to be set.
              boolean bARMBP - True if ARM BP, False if THUMB BP.
              unsigned long ulBound - 
      Output: TyError - Mid Layer error code
 Description: Sets a hardware breakpoint from the given address to the address + bound.
Date          Initials      Description
24-Aug-2009   RTR           Initial
****************************************************************************/
static TyError ML_SetARM11HardwareBreakPointRange (unsigned long ulAddress, 
												   boolean       bARMBP, 
                                                   unsigned long ulBound)

{
      //TODO
	  // Setup WP0...
      tyBPResourceConfig.ucWP0Used         = HARDWARE_BP; 
      tyBPResourceConfig.ulWP0AddressValue = ulAddress + ulBound; 
      tyBPResourceConfig.ulWP0AddressMask  = 0x0F;
      tyBPResourceConfig.ulWP0DataValue	 = ARM_BP_DONT_CARE; 
      tyBPResourceConfig.ulWP0DataMask		 = ARM_BP_DONT_CARE; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         if (bARMBP)
	   	   tyBPResourceConfig.ulWP0CtrlValue = 0x184;
         else
	   	   tyBPResourceConfig.ulWP0CtrlValue = 0x184;

	   	tyBPResourceConfig.ulWP0CtrlMask  = 0x76;
         }
      else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
            // Set watchpoint registers for ARM7DI board
            {
	   	   tyBPResourceConfig.ulWP0CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
	   	   tyBPResourceConfig.ulWP0CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
            }
      
      else if (tyProcessorConfig.bARM9) 
      
            // Set watchpoint registers for ARM966ES board
            {
            if (bARMBP)
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
            else
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_THUMB_SW_BP_VALUE; 

	   	   tyBPResourceConfig.ulWP0CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
         }
      
   
      // Setup WP1...
      tyBPResourceConfig.ucWP1Used         = HARDWARE_BP; 
      tyBPResourceConfig.ulWP1AddressValue = ulAddress; 
      tyBPResourceConfig.ulWP1AddressMask  = 0x1F;
      tyBPResourceConfig.ulWP1DataValue	 = ARM_BP_DONT_CARE; 
      tyBPResourceConfig.ulWP1DataMask		 = ARM_BP_DONT_CARE; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         if (bARMBP)
	   	   tyBPResourceConfig.ulWP1CtrlValue = 0x004;
         else
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7TDMI_THUMB_SW_BP_VALUE; 

	   	tyBPResourceConfig.ulWP1CtrlMask  = 0x76;
         }
      else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
            // Set watchpoint registers for ARM7DI board
            {
	   	   tyBPResourceConfig.ulWP1CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
	   	   tyBPResourceConfig.ulWP1CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
            }
      
      else if (tyProcessorConfig.bARM9) 
      
            // Set watchpoint registers for ARM966ES board
            {
            if (bARMBP)
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
            else
	   	      tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_THUMB_SW_BP_VALUE; 
	   	
            tyBPResourceConfig.ulWP1CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
         }

 	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetSoftwareBP
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address at which software BP is to be set
              boolean bARMBP - True if ARM BP, False if THUMB BP.
      Output: TyError - Mid Layer error code
 Description: Sets a software breakpoint. ie writes breakpoint pattern to memory.
Date          Initials      Description
18-Jul-2007      SJ         Initial
31-Jul-2008      VK         Added ARM11 support
21-Apr-2010      JCK           Cortex-A8 support added
****************************************************************************/
static TyError ML_SetSoftwareBP (unsigned long ulAddress, boolean bARMBP)

{
	TyError ErrRet            = RDIError_NoError;
	unsigned long ulMemRead   = 0;
	unsigned long ulNumBytes  = 0;
	unsigned long ulBpData;

    if (tyProcessorConfig.bARM11)
    {
        if (bARMBP)
        {
            ulBpData = ARM11_ARM_BKPT_INST;
        }
        else
        {
            ulBpData = ARM11_THUMB_BKPT_INST;
        }
    }
    else if (tyProcessorConfig.bCortexM3)
    {
        ulBpData = CORTEXM_BKPT_INST;
    }
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		if (bARMBP)
		{
			ulBpData = CORTEX_ARM_BKPT_INST;
		} 
		else
		{
			ulBpData = CORTEX_THUMB_BKPT_INST;
		}
	}
    else
    {
		if(FEROCEON == tyTargetConfig.tyDeviceType)
		{
			if (bARMBP)
			{
				ulBpData = ARM11_ARM_BKPT_INST;
			}
			else
			{
				ulBpData = ARM11_THUMB_BKPT_INST;
			}
		}
		else
		{
			ulBpData = BP_INST_DATA;
		}
    }

	if (bARMBP)
		ulNumBytes = ARM_NO_OF_BYTES;   // 32 bit ARM breakpoint
	else
		ulNumBytes = THUMB_NO_OF_BYTES; // 16 bit Thumb breakpoint

	if ((tyProcessorConfig.bCortexM3) ||
		(tyProcessorConfig.bCortexA8) ||
		(tyProcessorConfig.bCortexA9))
	{
		ErrRet = ML_ReadProc(ulAddress,
			(void *)&pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulPreviousInstructionData,
			&ulNumBytes,
			RDIAccess_Data32, TRUE);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ML_Error_CantSetPoint;
	}
	else
	{
		ErrRet = ML_ReadProc(ulAddress,
			(void *)&pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulPreviousInstructionData,
			&ulNumBytes,
			RDIAccess_Data32, TRUE);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ML_Error_CantSetPoint;
	}
	
   
	// Write the new BP data...
	ErrRet = ML_WriteProc(&ulBpData, ulAddress, &ulNumBytes, RDIAccess_Data32, FALSE, TRUE);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ML_Error_CantSetPoint;
   
	// Read the memory location to check that the breakpoint has been set...
	ErrRet = ML_ReadProc(ulAddress,
                        (void *)&ulMemRead,
                        &ulNumBytes,
                        RDIAccess_Data32, TRUE);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ML_Error_CantSetPoint;
   
    if (tyProcessorConfig.bARM11)
    {
        if (((ulNumBytes == THUMB_NO_OF_BYTES) && (ulMemRead == ARM11_THUMB_BKPT_INST)) || ((ulNumBytes == ARM_NO_OF_BYTES) && (ulMemRead == ARM11_ARM_BKPT_INST)))
            return ML_ERROR_NO_ERROR;
        else
            return ML_Error_CantSetPoint;
    }
    else if (tyProcessorConfig.bCortexM3)
    {
		if (ulMemRead == CORTEXM_BKPT_INST)
            return ML_ERROR_NO_ERROR;
        else
            return ML_Error_CantSetPoint;
    }
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		if (((ulNumBytes == THUMB_NO_OF_BYTES) && (ulMemRead == CORTEX_THUMB_BKPT_INST)) || 
			((ulNumBytes == ARM_NO_OF_BYTES) && (ulMemRead == CORTEX_ARM_BKPT_INST)))
            return ML_ERROR_NO_ERROR;
        else
            return ML_Error_CantSetPoint;

	}
    else
    {
		if(FEROCEON == tyTargetConfig.tyDeviceType)
		{
	        if (((ulNumBytes == THUMB_NO_OF_BYTES) && (ulMemRead == ARM11_THUMB_BKPT_INST)) || ((ulNumBytes == ARM_NO_OF_BYTES) && (ulMemRead == ARM11_ARM_BKPT_INST)))
		        return ML_ERROR_NO_ERROR;
			else
				return ML_Error_CantSetPoint;
		}
		else
		{
			// If the read returns the value BP_INST_DATA then the address specified can be 
			// written to and a software breakpoint can be used.
			if (((ulNumBytes == THUMB_NO_OF_BYTES) && (ulMemRead == BP_INST_DATA_HALFWORD)) || ((ulNumBytes == ARM_NO_OF_BYTES) && (ulMemRead == BP_INST_DATA)))
				return ML_ERROR_NO_ERROR;
			else
				return ML_Error_CantSetPoint;
		}
    }
}

/****************************************************************************
    Function: ML_SetSoftwareBPResource
    Engineer: Suraj S
       Input: Resource number to use for software breakpoints.
      Output: None
 Description: Sets up the given resource for software breakpoints.
Date          Initials      Description
18-Jul-2007      SJ         Initial
****************************************************************************/
static void ML_SetSoftwareBPResource (unsigned long ulResourceNo)

{
	if (ulResourceNo == HARDWARE_RESOURCE0)
	{
		// Setup WP0...
		tyBPResourceConfig.ucWP0Used = SOFTWARE_BP;
		tyBPResourceConfig.ulWP0AddressValue = ARM_BP_DONT_CARE; 
		tyBPResourceConfig.ulWP0AddressMask  = ARM_BP_DONT_CARE; 
		tyBPResourceConfig.ulWP0DataValue	 = BP_INST_DATA; 
		tyBPResourceConfig.ulWP0DataMask		 = ARM_BP_DO_CARE; 
      
		// Set watchpoint registers for ARM7TDMI's
		if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
		{
			tyBPResourceConfig.ulWP0CtrlValue = ARM7TDMI_ARM_SW_BP_VALUE; 
			tyBPResourceConfig.ulWP0CtrlMask  = ARM7TDMI_ARM_SW_BP_MASK ; 
		}
		else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
		// Set watchpoint registers for ARM7DI's
		{
			tyBPResourceConfig.ulWP0CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
			tyBPResourceConfig.ulWP0CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
		}
		else if (tyProcessorConfig.bARM9) 

		// Set watchpoint registers for ARM9's
		{
			tyBPResourceConfig.ulWP0CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
			tyBPResourceConfig.ulWP0CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
		}
	}
	else if (tyProcessorConfig.bARM11) 
	{
		//TODO
	}
	else
	if (ulResourceNo == HARDWARE_RESOURCE1)
	{
		// Setup WP1...
		tyBPResourceConfig.ucWP1Used = SOFTWARE_BP;
		tyBPResourceConfig.ulWP1AddressValue = ARM_BP_DONT_CARE; 
		tyBPResourceConfig.ulWP1AddressMask  = ARM_BP_DONT_CARE; 
		tyBPResourceConfig.ulWP1DataValue	 = BP_INST_DATA; 
		tyBPResourceConfig.ulWP1DataMask		 = ARM_BP_DO_CARE; 
      
		// Set watchpoint registers for ARM7TDMI's 
		if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
		{
			tyBPResourceConfig.ulWP1CtrlValue = ARM7TDMI_ARM_SW_BP_VALUE; 
			tyBPResourceConfig.ulWP1CtrlMask  = ARM7TDMI_ARM_SW_BP_MASK ; 
		}
		else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
		// Set watchpoint registers for ARM7DI's
		{
			tyBPResourceConfig.ulWP1CtrlValue = ARM7DI_ARM_SW_BP_VALUE; 
			tyBPResourceConfig.ulWP1CtrlMask  = ARM7DI_ARM_SW_BP_MASK ; 
		}
      
		else if (tyProcessorConfig.bARM9) 
      
		// Set watchpoint registers for ARM9's 
		{
			tyBPResourceConfig.ulWP1CtrlValue = ARM9TDMI_ARM_SW_BP_VALUE; 
			tyBPResourceConfig.ulWP1CtrlMask  = ARM9TDMI_ARM_SW_BP_MASK ; 
		}
		else if (tyProcessorConfig.bARM11) 
		{
			//TODO
		}
	}
	return;
}

/****************************************************************************
    Function: ML_ClearWatchProc
    Engineer: Suraj S
       Input: Watchpoint handle
      Output: TyError - Mid Layer error code
 Description: Clears the watchpoint given by the handle
Date          Initials      Description
10-Jul-2007      SJ         Initial
02-Dec-2009      JCK		Modified for Cortex-M3 support
21-Apr-2010      JCK           Cortex-A8 support added
****************************************************************************/
TyError ML_ClearWatchProc(RDI_PointHandle hHandle)
{
   unsigned int i;

   // Find the watchpoint for the given handle
    if ((tyProcessorConfig.bARM7) ||
        (tyProcessorConfig.bARM9))
	{
		for (i=0; i < usNumberOfBreakpoints; i++)
		{
			if (pBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)hHandle)
				break;
		}
		// If we haven't found it , assert and a return an error
		if (i >= usNumberOfBreakpoints)
		{
			// Something's gone wrong...
			// TODO: ASSERT(FALSE);
			return ML_Error_NoSuchPoint;
		}

		if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_WP0)
		{
			// TODO: ASSERT(tyBPResourceConfig.ucWP0Used == HARDWARE_WP);
			ML_ClearHardwareResource (HARDWARE_RESOURCE0);
		}

		if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_WP1)
		{
			// TODO: ASSERT(tyBPResourceConfig.ucWP1Used == HARDWARE_WP);
			ML_ClearHardwareResource (HARDWARE_RESOURCE1);
		}


		for (i=i+1; i < usNumberOfBreakpoints; i++)
			pBPArray->BreakpointStructures[i-1] = pBPArray->BreakpointStructures[i];

		usNumberOfBreakpoints--;
	}
    else if (tyProcessorConfig.bARM11)
	{
		for (i=0; i < usNumberOfWatchpoints; i++)
		{
			if (pWPArray->WatchpointStructures[i].usWatchpointId == (unsigned short)hHandle)
				break;
      }
		// If we haven't found it , assert and a return an error
		if (i >= usNumberOfWatchpoints)
      {
			// Something's gone wrong...
			// TODO: ASSERT(FALSE);
			return ML_Error_NoSuchPoint;
      }
		
		if (pWPArray->WatchpointStructures[i].ucWPIDNo == HARDWARE_WP0)
      {
			// TODO: ASSERT(tyBPResourceConfig.ucWP0Used == HARDWARE_WP);
			ML_ClearARM11WPResource (HARDWARE_RESOURCE0);
			
      }
		
		if (pWPArray->WatchpointStructures[i].ucWPIDNo == HARDWARE_WP1)
      {
			// TODO: ASSERT(tyBPResourceConfig.ucWP1Used == HARDWARE_WP);
			ML_ClearARM11WPResource (HARDWARE_RESOURCE1);
      }
		
		
		for (i=i+1; i < usNumberOfWatchpoints; i++)
			pWPArray->WatchpointStructures[i-1] = pWPArray->WatchpointStructures[i];
		
		usNumberOfWatchpoints--;
	}
   
    else if (tyProcessorConfig.bCortexM3)
    {
        unsigned long ulWatchPointRes = 0;

		for (i=0; i < usNumberOfBreakpoints; i++)
		{
			if (pCortexBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)hHandle)
				break;
		}        

        // If we haven't found it , assert and a return an error
        if (i >= usNumberOfBreakpoints)
        {            
            return ML_Error_NoSuchPoint;
        }

		ulWatchPointRes = pCortexBPArray->BreakpointStructures[i].ucBPIDNo - WATCHPOINT_RECOURSE_0;
        ML_ClearCortexMWPResource(ulWatchPointRes, i);
		for (i=i+1; i <= usNumberOfBreakpoints; i++)
			pCortexBPArray->BreakpointStructures[i-1] = pCortexBPArray->BreakpointStructures[i];

		usNumberOfBreakpoints--;

    }
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		unsigned long ulWatchPointRes = 0;
		
		for (i=0; i < usNumberOfBreakpoints; i++)
		{
			if (pCortexBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)hHandle)
			{
				ulWatchPointRes = pCortexBPArray->BreakpointStructures[i].ucBPIDNo - WATCHPOINT_RECOURSE_0;
				pCortexBPArray->ucWatchBPResource[ulWatchPointRes] = WP_RES_NOT_USED;
				tyArmv7AWatchpointResArray.tyArmv7AulWatchPointRes[ulWatchPointRes].ulWatchPointControlReg = WCR_WATCH_DISABLE;
				break;
			}
		}        
		
        // If we haven't found it , assert and a return an error
        if (i >= usNumberOfBreakpoints)
        {            
            return ML_Error_NoSuchPoint;
        }
		
		usNumberOfBreakpoints--;
	}

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetBreakProc
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address of breakpoint
              unsigned long ulBPType - Breakpoint type ie hardware / software
              unsigned long ulBound - range of breakpoints ie 0x8000 to 0x9000
              RDI_PointHandle  *phPointHandle  : BP handle 
      Output: TyError - Mid Layer error code
 Description: Sets a breakpoint based on the following algorithm:

              1) A software BP will always be set if possible.
              2) If a SW BP cannot be set then a hardware BP will be set.
              
              This means that if further BP's are needed and they cannot be set as    
              SW BP's then one or more of the resources needs to be freed. Once this
              happens there is no "downgrading" to SW BP's because this has already
              been done.
     
Date          Initials      Description
18-Jul-2007   SJ            Initial
25-Aug-2009   RTR           Added ARM11 support
21-Apr-2010   JCK           Cortex-A8 support added
****************************************************************************/
TyError ML_SetBreakProc (unsigned long    ulAddress,
                         unsigned long    ulBPType,
                         unsigned long    ulBound,
                         RDI_PointHandle  *phPointHandle)

{
    TyError ErrRet=0;
    unsigned short usBPNo;
    unsigned short i                       = 0;
    unsigned char  ucNumberOfResources     = 0;
    boolean        bARMBp                  = TRUE;
    boolean        bMustBeHardwareBP       = FALSE;
    boolean        bHandleAssigned         = FALSE;
    unsigned short usBreakPointHandle      = 0x0;
	unsigned char ucFreeResource1 = 0;
    unsigned char ucFreeResource2 = 0;

    // Check if we are setting an ARM bp or thumb bp...
    bARMBp = (ulBPType & RDIPoint_16Bit) != RDIPoint_16Bit;

    if (!bARMBp)
        ulAddress = ulAddress & HALF_WORD_ALIGNED_MASK;
    else
        ulAddress = ulAddress & WORD_ALIGNED_MASK;

    // Check whether the breakpoint is valid ie ARM BP's must be word aligned and Thumb BP's must be word or
    // half-word aligned.
    if (!bARMBp)
    {
        if ((ulAddress & HALF_WORD_ALIGNED_MASK) != ulAddress)
            return ML_Error_BadPointType;
    }
    else 
    {
        if ((ulAddress & WORD_ALIGNED_MASK) != ulAddress)
            return ML_Error_BadPointType;
    }

    // We need to check that the handle is not already in use...
    while (!bHandleAssigned)   
    {   
        for (i=0; i < usNumberOfBreakpoints; i++)
        {
			if ((tyProcessorConfig.bCortexM3) ||
				(tyProcessorConfig.bCortexA8) ||
				(tyProcessorConfig.bCortexA9))
			{
				if (pCortexBPArray->BreakpointStructures[i].usBreakpointId == usBreakPointHandle)
				{
					usBreakPointHandle ++;
					break;
				}
			}
			else
			{
				if (pBPArray->BreakpointStructures[i].usBreakpointId == usBreakPointHandle)
				{
					usBreakPointHandle ++;
					break;
				}
			}
        }
        if (i >= usNumberOfBreakpoints)
            bHandleAssigned = TRUE;
    }

    // Check for conflicting BP's ...
    for (usBPNo = 0; usBPNo < usNumberOfBreakpoints; usBPNo ++)
    {
        // Check for ARM / Thumb breakpoints which may overlap each other.
        if (!bARMBp)
        {
            // This is a Thumb breakpoint. 
            // Check whether there is an ARM BP at the Word aligned address.
			if ((tyProcessorConfig.bCortexM3) ||
				(tyProcessorConfig.bCortexA8) ||
				(tyProcessorConfig.bCortexA9))
			{
				if ((pCortexBPArray->BreakpointStructures[usBPNo].ulAddress == (ulAddress & WORD_ALIGNED_MASK)) &&
					((pCortexBPArray->BreakpointStructures[usBPNo].ulBPType & RDIPoint_16Bit) != RDIPoint_16Bit))
					return ML_Error_PointInUse;
			}
            else
			{
				if ((pBPArray->BreakpointStructures[usBPNo].ulAddress == (ulAddress & WORD_ALIGNED_MASK)) &&
					((pBPArray->BreakpointStructures[usBPNo].ulBPType & RDIPoint_16Bit) != RDIPoint_16Bit))
					return ML_Error_PointInUse;
			}
        }
        else
        {
			if (tyProcessorConfig.bCortexM3)
			{
				return ML_Error_PointInUse;	/* Cortex-M3 is always in Thumb mode. */
			}
			else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
			{
				// This is an ARM breakpoint. 
				// Check whether there is a Thumb BP at the halfword aligned address.
				if ((pCortexBPArray->BreakpointStructures[usBPNo].ulAddress == (ulAddress      )) &&
					((pCortexBPArray->BreakpointStructures[usBPNo].ulBPType & RDIPoint_16Bit) == RDIPoint_16Bit))
					return ML_Error_PointInUse;
				if ((pCortexBPArray->BreakpointStructures[usBPNo].ulAddress == (ulAddress + 0x2)) &&
					((pCortexBPArray->BreakpointStructures[usBPNo].ulBPType & RDIPoint_16Bit) == RDIPoint_16Bit))
					return ML_Error_PointInUse;
			}
			else
			{
				// This is an ARM breakpoint. 
				// Check whether there is a Thumb BP at the halfword aligned address.
				if ((pBPArray->BreakpointStructures[usBPNo].ulAddress == (ulAddress      )) &&
					((pBPArray->BreakpointStructures[usBPNo].ulBPType & RDIPoint_16Bit) == RDIPoint_16Bit))
					return ML_Error_PointInUse;
				if ((pBPArray->BreakpointStructures[usBPNo].ulAddress == (ulAddress + 0x2)) &&
					((pBPArray->BreakpointStructures[usBPNo].ulBPType & RDIPoint_16Bit) == RDIPoint_16Bit))
					return ML_Error_PointInUse;
			}
        }
    }

    // Now check if the same breakpoint has been set already...
    for (usBPNo = 0; usBPNo < usNumberOfBreakpoints; usBPNo ++)
    {
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			if ((pCortexBPArray->BreakpointStructures[usBPNo].ulAddress          == ulAddress) &&
				(pCortexBPArray->BreakpointStructures[usBPNo].ulBPType           == ulBPType ) &&
				(pCortexBPArray->BreakpointStructures[usBPNo].bWatchPoint        == FALSE    ) && 
				(pCortexBPArray->BreakpointStructures[usBPNo].ucNoOfBPsAtAddress >= 1        ))
			{
				// Increment the counter for multiple BP's...
				pCortexBPArray->BreakpointStructures[usBPNo].ucNoOfBPsAtAddress ++;
				
				// The new breakpoint will be the same as the old one....
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints] = pCortexBPArray->BreakpointStructures[usBPNo];
				
				// Except the ucNoOfBPsAtAddress will be 0...
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucNoOfBPsAtAddress = 0;
				
				// And the breakpoint handle will be different...
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId = usBreakPointHandle;
				
				goto BreakpointSetSuccessfully;
			}
		}
		else
		{
			if ((pBPArray->BreakpointStructures[usBPNo].ulAddress          == ulAddress) &&
				(pBPArray->BreakpointStructures[usBPNo].ulBPType           == ulBPType ) &&
				(pBPArray->BreakpointStructures[usBPNo].bWatchPoint        == FALSE    ) && 
				(pBPArray->BreakpointStructures[usBPNo].ucNoOfBPsAtAddress >= 1        ))
			{
				// Increment the counter for multiple BP's...
				pBPArray->BreakpointStructures[usBPNo].ucNoOfBPsAtAddress ++;

				// The new breakpoint will be the same as the old one....
				pBPArray->BreakpointStructures[usNumberOfBreakpoints] = pBPArray->BreakpointStructures[usBPNo];

				// Except the ucNoOfBPsAtAddress will be 0...
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucNoOfBPsAtAddress = 0;

				// And the breakpoint handle will be different...
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId = usBreakPointHandle;

				goto BreakpointSetSuccessfully;
			}
		}
    }

	if ((tyProcessorConfig.bCortexM3) ||
		(tyProcessorConfig.bCortexA8) ||
		(tyProcessorConfig.bCortexA9))
	{
		// We are setting the first BP at this address.
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId     = usBreakPointHandle; 
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucNoOfBPsAtAddress = 0x1; 
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulAddress          = ulAddress;
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBPType           = ulBPType;
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBound            = ulBound;
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulDataType         = 0x0;
		pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].bWatchPoint        = FALSE;
	} 
	else
	{
		// We are setting the first BP at this address.
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId     = usBreakPointHandle; 
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucNoOfBPsAtAddress = 0x1; 
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulAddress          = ulAddress;
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBPType           = ulBPType;
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBound            = ulBound;
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulDataType         = 0x0;
		pBPArray->BreakpointStructures[usNumberOfBreakpoints].bWatchPoint        = FALSE;
	}
    

    // Check which resources we can use for the breakpoints.
    switch (ulBPType & 0xF)
    {
        case RDIPoint_EQ:
            ucNumberOfResources  = 0x1;
            bMustBeHardwareBP    = FALSE;
            break;
        case RDIPoint_IN:          
            ucNumberOfResources  = 0x2;
            bMustBeHardwareBP    = TRUE;
            break;
        case RDIPoint_MASK:
            ucNumberOfResources  = 0x1;
            bMustBeHardwareBP    = TRUE;
            break;
        default :
            //TODO: ASSERT_NOW();
            break;
    }

    if ((tyProcessorConfig.bARM7) || 
        (tyProcessorConfig.bARM9))//Specifically for ARM9 & ARM7
    {

        // To set a range both resources must be free
        if (ucNumberOfResources == 0x2)    
        { 
            if ((tyBPResourceConfig.bWP0Locked == TRUE)     ||
                (tyBPResourceConfig.ucWP0Used  != NOT_USED) ||
                (tyBPResourceConfig.bWP0Locked == TRUE)     ||
                (tyBPResourceConfig.ucWP0Used  != NOT_USED))
                return ML_Error_CantSetPoint;
            ErrRet = ML_SetHardwareBreakPointRange (ulAddress, TRUE, ulBound);
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ML_Error_CantSetPoint;

            pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BPX;

            goto BreakpointSetSuccessfully;  
        }
        else
        {
            // If either of the hardware resources is set as a software BP 
            // then we can try to add a software BP first.
            if (!tyBPResourceConfig.bUseOnlyHWBreakPoints && !bMustBeHardwareBP)
            {
				if(FEROCEON == tyTargetConfig.tyDeviceType)
				{
					ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
					if (ErrRet == ML_ERROR_NO_ERROR) 
					{
						pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP0;
						goto BreakpointSetSuccessfully;  
					}
				}
				else
				{
					if (tyBPResourceConfig.ucWP0Used == SOFTWARE_BP)
					{
						ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
						if (ErrRet == ML_ERROR_NO_ERROR) 
						{
							pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP0;
							goto BreakpointSetSuccessfully;  
						}
					}
					else if (tyBPResourceConfig.ucWP1Used == SOFTWARE_BP)
					{
						ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
						if (ErrRet == ML_ERROR_NO_ERROR) 
						{
							pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP1;
							goto BreakpointSetSuccessfully;  
						}
					}
					else if ((tyBPResourceConfig.ucWP0Used == NOT_USED) && (!tyBPResourceConfig.bWP0Locked)) 
					{
						ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
						if (ErrRet == ML_ERROR_NO_ERROR) 
						{
							ML_SetSoftwareBPResource (HARDWARE_RESOURCE0);
							pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP0;
							goto BreakpointSetSuccessfully;
						}
					}
					else if ((tyBPResourceConfig.ucWP1Used == NOT_USED) && (!tyBPResourceConfig.bWP1Locked)) 
					{
						ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
						if (ErrRet == ML_ERROR_NO_ERROR) 
						{
							ML_SetSoftwareBPResource (HARDWARE_RESOURCE1);
							pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP1;
							goto BreakpointSetSuccessfully;
						}
					}
				}
            }
            if(bGDBServer)
            {
                if ((ErrRet == ML_Error_CantSetPoint) && (!tySemiHostingConfig.bSemiHostingSet)) 
                    return ML_Error_NonEditablePoint;
            }
      
            // We need to try and set a hardware resource using WP0.
            if ((tyBPResourceConfig.ucWP0Used == NOT_USED) && (!tyBPResourceConfig.bWP0Locked))
            {
                ErrRet = ML_SetHardwareBreakPoint (ulAddress,
													bARMBp, 
													HARDWARE_RESOURCE0, 
													ulBound);
                if (ErrRet == ML_ERROR_NO_ERROR) 
                {
                    pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP0;
                    goto BreakpointSetSuccessfully;
                }
            }
      
            // We need to try and set a hardware resource using WP1.
            if ((tyBPResourceConfig.ucWP1Used == NOT_USED) && (!tyBPResourceConfig.bWP1Locked))
            {
                ErrRet = ML_SetHardwareBreakPoint (ulAddress,
                                                bARMBp, 
                                                HARDWARE_RESOURCE1, 
                                                ulBound);
                if (ErrRet == ML_ERROR_NO_ERROR) 
                {
                    pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP1;
                    goto BreakpointSetSuccessfully;
                }
            }
        }

        // The requested breakpoint cannot be set with the available resources.	
        return ML_Error_CantSetPoint;
    }
    else if (tyProcessorConfig.bARM11)//ARM11
    {
        // To set a range both resources must be free

		if (ucNumberOfResources == 0x2)
		{
			if(ML_GetFreeResources(&ucFreeResource1, &ucFreeResource2) < 2)
			{
				return ML_Error_CantSetPoint;

				ErrRet = ML_SetARM11HardwareBreakPointRange (ulAddress, TRUE, ulBound);
			    if (ErrRet != ML_ERROR_NO_ERROR)
				    return ML_Error_CantSetPoint;
			
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BPX;
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPResourceNo1 = ucFreeResource1;
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPResourceNo2 = ucFreeResource2;
			
				goto BreakpointSetSuccessfully; 
			}
		}
        else
        {
            // If either of the hardware resources is set as a software BP 
            // then we can try to add a software BP first.
            if (!tyBPResourceARM11Config.bUseOnlyHWBreakPoints && !bMustBeHardwareBP)
            {
                    ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
                    if (ErrRet == ML_ERROR_NO_ERROR) 
                    {
                        pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP0;
                        goto BreakpointSetSuccessfully;  
                    }

            }
            if(bGDBServer)
            {
                if ((ErrRet == ML_Error_CantSetPoint) && (!tySemiHostingConfig.bSemiHostingSet)) 
                    return ML_Error_NonEditablePoint;
            }

			// We need to try and set a hardware resource using BP0.
			if ((tyBPResourceARM11Config.ucBP0Used == NOT_USED) && (!tyBPResourceARM11Config.bBP0Locked))
			{
				ErrRet = ML_SetARM11HardwareBreakPoint (ulAddress,bARMBp, HARDWARE_RESOURCE0);
				if (ErrRet == ML_ERROR_NO_ERROR) 
				{
					pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP0;
					goto BreakpointSetSuccessfully;
				}
			}
			// We need to try and set a hardware resource using BP1.
			if ((tyBPResourceARM11Config.ucBP1Used == NOT_USED) && (!tyBPResourceARM11Config.bBP1Locked))
			{
				ErrRet = ML_SetARM11HardwareBreakPoint (ulAddress,bARMBp, HARDWARE_RESOURCE1);
				if (ErrRet == ML_ERROR_NO_ERROR) 
				{
					pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP1;
					goto BreakpointSetSuccessfully;
				}
            }

			// We need to try and set a hardware resource using BP2.
			if ((tyBPResourceARM11Config.ucBP2Used == NOT_USED) && (!tyBPResourceARM11Config.bBP2Locked))
			{
				ErrRet = ML_SetARM11HardwareBreakPoint (ulAddress,bARMBp, HARDWARE_RESOURCE2);
				if (ErrRet == ML_ERROR_NO_ERROR) 
				{
					pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP2;
					goto BreakpointSetSuccessfully;
				}
			}

			// We need to try and set a hardware resource using BP3.
			if ((tyBPResourceARM11Config.ucBP3Used == NOT_USED) && (!tyBPResourceARM11Config.bBP3Locked))
			{
				ErrRet = ML_SetARM11HardwareBreakPoint (ulAddress,bARMBp, HARDWARE_RESOURCE3);
				if (ErrRet == ML_ERROR_NO_ERROR) 
				{
					pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP3;
					goto BreakpointSetSuccessfully;
				}
			}

			// We need to try and set a hardware resource using BP4.
			if ((tyBPResourceARM11Config.ucBP4Used == NOT_USED) && (!tyBPResourceARM11Config.bBP4Locked))
			{
				ErrRet = ML_SetARM11HardwareBreakPoint (ulAddress,bARMBp, HARDWARE_RESOURCE4);
				if (ErrRet == ML_ERROR_NO_ERROR) 
				{
					pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP4;
					goto BreakpointSetSuccessfully;
				}
			}

			// We need to try and set a hardware resource using BP5.
			if ((tyBPResourceARM11Config.ucBP5Used == NOT_USED) && (!tyBPResourceARM11Config.bBP5Locked))
			{
				ErrRet = ML_SetARM11HardwareBreakPoint (ulAddress,bARMBp, HARDWARE_RESOURCE5);
				if (ErrRet == ML_ERROR_NO_ERROR) 
				{
					pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_BP5;
					goto BreakpointSetSuccessfully;
				}
			}
			// The requested breakpoint cannot be set with the available resources.	
			return ML_Error_CantSetPoint;

		}
        //TODO
    }

    else if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
    {
        // To set a range both resources must be free
        if (ucNumberOfResources == 0x2)
        {            
        }
        else
        {
            unsigned char ucCompResourseNum;
            unsigned long ulNumResourses = tyArmv7MBreakpointResArray.ulNumBreakpoint;
            // If either of the hardware resources is set as a software BP 
            // then we can try to add a software BP first.
			if (tyProcessorConfig.bCortexM3)
			{
				if(ulAddress >= 0x20000000)
				{
					tyArmv7MBreakpointResArray.bUseOnlyHWBreakPoints = 0;
				}
				else
				{
					tyArmv7MBreakpointResArray.bUseOnlyHWBreakPoints = 1;
				}

				if (!tyArmv7MBreakpointResArray.bUseOnlyHWBreakPoints && !bMustBeHardwareBP)
				{
					ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
					if (ErrRet == ML_ERROR_NO_ERROR)
					{
						pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP0;
						goto BreakpointSetSuccessfully;  
					}
					//else
					//	return ML_Error_CantSetPoint;
					
				}
			}

			if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
			{
				if (!tyArmv7ABreakpointResArray.bUseOnlyHWBreakPoints && !bMustBeHardwareBP)
				{
					ErrRet = ML_SetSoftwareBP (ulAddress,bARMBp);
					if (ErrRet == ML_ERROR_NO_ERROR)
					{
						pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = SOFTWARE_BP0;
						goto BreakpointSetSuccessfully;  
					}
				}
			}
			            
            if (bGDBServer)
            {
                if ((ErrRet == ML_Error_CantSetPoint) && (!tySemiHostingConfig.bSemiHostingSet))
                    return ML_Error_NonEditablePoint;
            }


            for (ucCompResourseNum = 0; ucCompResourseNum < ulNumResourses; ucCompResourseNum++)
            {
                if (FPC_COMP_NOT_USED == pCortexBPArray->ucHardBPResource[ucCompResourseNum])
                {        
					if (tyProcessorConfig.bCortexM3)
					{
						ErrRet = ML_SetCortexMHardwareBreakPoint (ulAddress,bARMBp, ucCompResourseNum);
						if (ErrRet == ML_ERROR_NO_ERROR)
						{
							pCortexBPArray->ucHardBPResource[ucCompResourseNum] = FPC_COMP_USED;
							pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = (unsigned char)(HARDWARE_BP0 + ucCompResourseNum);
							break;
						}
						else
						{
							return ML_Error_CantSetPoint;
						}
					}
					else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
					{
						ErrRet = ML_SetArmv7AHardwareBreakPoint(ulAddress, bARMBp, ucCompResourseNum);
						if (ML_ERROR_NO_ERROR == ErrRet)
						{
							pCortexBPArray->ucHardBPResource[ucCompResourseNum] = FPC_COMP_USED;
							pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = (unsigned char)(HARDWARE_BP0 + ucCompResourseNum);
							break;
						}
					}
                    
                }

				
            }

            if (ucCompResourseNum == ulNumResourses)
            {
                // The requested breakpoint cannot be set with the available resources.	
                return ML_Error_CantSetPoint;
            }

        }
    }
BreakpointSetSuccessfully:
    // Return the handle...
	if ((tyProcessorConfig.bCortexM3) ||
		(tyProcessorConfig.bCortexA8) ||
		(tyProcessorConfig.bCortexA9))
	{
		*phPointHandle = pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId;
	}
	else
	{
		*phPointHandle = pBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId;
	}
	
    // We have just set a breakpoint, increment the count...
    usNumberOfBreakpoints++;

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_CPUReadProc
    Engineer: Suraj S
       Input: unsigned long  ulMode - Target processor mode
              unsigned long  ulMask - Mask of register to read
              unsigned long  *pulState - Pointer to read the register values
      Output: TyError - Mid Layer error code
 Description: Reads the contents of processor registers
Date          Initials      Description
19-Jul-2007      SJ         Initial
21-Apr-2010      JCK           Cortex-A8 support added
****************************************************************************/
TyError ML_CPUReadProc ( unsigned long  ulMode,
                         unsigned long  ulMask,
                         unsigned long  *pulState)
{  
   unsigned long ulRegNum        = 0;
   unsigned long ulProcMode      = 0;
   unsigned long *pulLocalState  = 0;
   unsigned long ulLocalMask     = 0;
   unsigned long ErrRet = ML_ERROR_NO_ERROR;

   pulLocalState = pulState;
   //if mode is current mode read the CPSR and determine actual processor mode.
   if (ulMode == CURRENT_MODE)
      {
      ulMode = (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_CPSR] & REG_ARRAY_MODE_MASK);
      }
   
   switch (ulMode)
      {
      case FIQ_MODE            :
      case ARMCANTREADSPECS_FIQ_MODE   :
         ulProcMode = REG_ARRAY_FIQ_MODE;
         break;
      case IRQ_MODE  :
      case ARMCANTREADSPECS_IRQ_MODE   :
         ulProcMode = REG_ARRAY_IRQ_MODE;
         break;
      case SVC_MODE  :
      case ARMCANTREADSPECS_SVC_MODE   :
         ulProcMode = REG_ARRAY_SVC_MODE;
         break;
      case ABORT_MODE  :
      case ARMCANTREADSPECS_ABORT_MODE   :
         ulProcMode = REG_ARRAY_ABORT_MODE;
         break;
      case UNDEF_MODE  :
      case ARMCANTREADSPECS_UNDEF_MODE   :
         ulProcMode = REG_ARRAY_UNDEF_MODE;
         break;
      case USR_MODE  :
      case ARMCANTREADSPECS_USR_MODE   :
      case SYS_MODE  :
      case ARMCANTREADSPECS_SYS_MODE   :
         ulProcMode = REG_ARRAY_USR_MODE;
         break;
	  case MON_MODE:
		  ulProcMode = REG_ARRAY_MON_MODE;
		  break;
      default        :
         ulProcMode = REG_ARRAY_USR_MODE;
         break;
      }
   
   ulRegNum   =  REG_ARRAY_R0;
   ulLocalMask = 1;
   while (ulLocalMask != 0)
      {
	   if (ulMask & ulLocalMask) 
		   {
         if (ulRegNum == REG_UNDEFINED)
            {
            *pulLocalState = 0x0;       
            return ML_Error_BadCPUStateSetting;
            }
         else
            // FIQ mode registers R8 to R14 are banked. 
            if ((ulRegNum>=REG_ARRAY_R8) && (ulRegNum <= REG_ARRAY_R14) && (ulProcMode == REG_ARRAY_FIQ_MODE))
               *pulLocalState = (unsigned long)RegArray[REG_ARRAY_FIQ_MODE] [ulRegNum];
            else
               // R0 to R12 are the same for all modes apart from the FIQ mode above.
               if (ulRegNum <= REG_ARRAY_R12)
                  *pulLocalState = (unsigned long)RegArray[REG_ARRAY_USR_MODE] [ulRegNum];
               else
                  // The PC and the CPSR are the same for all modes.
                  if ((ulRegNum==REG_ARRAY_PC) || (ulRegNum == REG_ARRAY_CPSR))
                     {
                     if ((ulRegNum == REG_ARRAY_CPSR) && (bGlobalThumb))
                        *pulLocalState = (unsigned long)(RegArray[REG_ARRAY_USR_MODE] [ulRegNum] |0x20);
                     else
                        *pulLocalState = (unsigned long)RegArray[REG_ARRAY_USR_MODE] [ulRegNum];
                     }
                  else
                     // R13, R14 and the SPSR are all banked registers.
                     *pulLocalState = (unsigned long)RegArray[ulProcMode] [ulRegNum];
         pulLocalState ++;
         }
      ulLocalMask <<=1;
      ulRegNum ++;
      }

   return ErrRet;
}

/****************************************************************************
    Function: ML_CortexMCPUReadProc
    Engineer: Deepa V A
       Input: unsigned long  ulMode - Target processor mode
              unsigned long  ulMask - Mask of register to read
              unsigned long  *pulState - Pointer to read the register values
      Output: TyError - Mid Layer error code
 Description: Reads the contents of processor registers
Date          Initials      Description
29-Oct-2009     DVA         Initial
****************************************************************************/
TyError ML_CortexMCPUReadProc ( unsigned long  ulMode,
                                 unsigned long  ulMask,
                                 unsigned long  *pulState)
{  
    unsigned long ulRegNum        = 0;
    //unsigned long *pulLocalState  = 0;
    unsigned long ulLocalMask     = 0;
    unsigned long ErrRet = ML_ERROR_NO_ERROR;

    if (CURRENT_MODE == ulMode)
    {
        ulMode = (CortexMRegArray[REG_CONTROL] & 0x02);
    }
    
    ulRegNum   =  REG_R0;
    ulLocalMask = 1;

    while (ulLocalMask != 0)
    {
        if (ulMask & ulLocalMask)
        {
            if (ulRegNum == REG_UNDEFINED)
            {
                *pulState = 0x0;       
                return ML_Error_BadCPUStateSetting;
            }
            else
            {			
                *pulState = (unsigned long)CortexMRegArray[ulRegNum];
            }
            pulState ++;
        }
        ulLocalMask <<=1;
        ulRegNum ++;
    }

    return ErrRet;
}
/****************************************************************************
    Function: ML_CPUWriteProc
    Engineer: Suraj S
       Input: unsigned long  ulMode - Target processor mode
              unsigned long  ulMask - Mask of register to read
              unsigned long  *pulState - Pointer to register write values
      Output: TyError - Mid Layer error code
 Description: Writes to processor registers
Date          Initials      Description
19-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_CPUWriteProc (unsigned long  ulMode,
                         unsigned long  ulMask,
                         unsigned long  *pulState)
{  
   unsigned long RegCount     = 0;
   unsigned long ulRegNum     = 0;
   unsigned long ulProcMode   = 0;
   unsigned long ulLocalMask  = 0;

   ulLocalMask = ulMask;
   ulRegNum    = REG_ARRAY_R0;
   RegCount    = REG_ARRAY_R0;

   //if mode is current mode read the CPSR and determine actual processor mode.
   if (ulMode == CURRENT_MODE)
      {
      ulMode = (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_CPSR] & REG_ARRAY_MODE_MASK);
      }

   switch (ulMode)
      {
      case FIQ_MODE  :
      case ARMCANTREADSPECS_FIQ_MODE   :
         ulProcMode = REG_ARRAY_FIQ_MODE;
         break;
      case IRQ_MODE  :
      case ARMCANTREADSPECS_IRQ_MODE   :
         ulProcMode = REG_ARRAY_IRQ_MODE;
         break;
      case SVC_MODE  :
      case ARMCANTREADSPECS_SVC_MODE   :
         ulProcMode = REG_ARRAY_SVC_MODE;
         break;
      case ABORT_MODE  :
      case ARMCANTREADSPECS_ABORT_MODE   :
         ulProcMode = REG_ARRAY_ABORT_MODE;
         break;
      case UNDEF_MODE  :
      case ARMCANTREADSPECS_UNDEF_MODE   :
         ulProcMode = REG_ARRAY_UNDEF_MODE;
         break;
      case USR_MODE  :
      case ARMCANTREADSPECS_USR_MODE   :
      case SYS_MODE  :
      case ARMCANTREADSPECS_SYS_MODE   :
         ulProcMode = REG_ARRAY_USR_MODE;
         break;
	  case MON_MODE:
		 ulProcMode = REG_ARRAY_MON_MODE;
		 break;
      default        :
         ulProcMode = REG_ARRAY_USR_MODE;
         break;

      }

   if (ulLocalMask < 0x10000)
      {
      while (RegCount<=REG_ARRAY_R14)
         {
         if (ulLocalMask & 0x01) //find reg in mask
            {
            ulRegNum = RegCount;
            break;
            }
         RegCount ++;
         ulLocalMask=ulLocalMask>>1;
         }
      }
   else
      {
      switch (ulLocalMask)
         {
         case 0x10000    :
            ulRegNum = REG_ARRAY_PC;
            ulProcMode = REG_ARRAY_USR_MODE;
            break;
         case 0x20000    :
            ulRegNum = REG_ARRAY_CPSR;
            ulProcMode = REG_ARRAY_USR_MODE;
            break;
         case 0x40000    :
            ulRegNum = REG_ARRAY_SPSR;
            break;
         default         :
            // This error code needs to be defined.....
            return ML_Error_BadCPUStateSetting;
         }
      }
   
   //We now have the register and processor mode to write to.
   //We have to check the banked mode registers to make sure that we are writing to the 
   //correct part of RegArray.
   if (((ulProcMode == REG_ARRAY_FIQ_MODE) && (ulRegNum <= REG_ARRAY_R7)) ||
       ((ulProcMode != REG_ARRAY_FIQ_MODE) && (ulRegNum <= REG_ARRAY_R12))) 
      {
      ulProcMode = REG_ARRAY_USR_MODE;
      }


   RegArray[ulProcMode] [ulRegNum] = *pulState;

   // Check if the CPSR is being written to. If the user is changing the Thumb bit
   // then we need to change the bThumb variable.
   if ((ulProcMode == REG_ARRAY_USR_MODE) && (ulRegNum == REG_ARRAY_CPSR))
      {
      if ((*pulState & 0x20) == 0x20)
         {
         //The user is either changing into Thumb mode or they have written a 
         //mode into the CPSR whilst in Thumb mode.
         bGlobalThumb = TRUE;
         RegArray[ulProcMode] [ulRegNum] = (RegArray[ulProcMode] [ulRegNum] & REG_ARRAY_TBIT_MASK);
         }
      else
         bGlobalThumb = FALSE;
      }
    return ML_ERROR_NO_ERROR;
}


/****************************************************************************
    Function: ML_CortexMCPUWriteProc
    Engineer: Deepa V.A
       Input: unsigned long  ulMode - Target processor mode
              unsigned long  ulMask - Mask of register to read
              unsigned long  *pulState - Pointer to register write values
      Output: TyError - Mid Layer error code
 Description: Writes to processor registers
Date          Initials      Description
29-Oct-2009      DVA        Initial
****************************************************************************/
TyError ML_CortexMCPUWriteProc (unsigned long  ulMode,
                                 unsigned long  ulMask,
                                 unsigned long  *pulState)
{  
    unsigned long RegCount     = 0;
    unsigned long ulRegNum     = 0;
    unsigned long ulLocalMask  = 0;
	unsigned long ulCurrMode   = 0;

    ulLocalMask = ulMask;
    ulRegNum    = REG_R0;
    RegCount    = REG_R0;    

    while (RegCount<=REG_LAST)
    {
        if (ulLocalMask & 0x01) //find reg in mask
        {
            ulRegNum = RegCount;
            break;
        }
        RegCount ++;
        ulLocalMask=ulLocalMask>>1;
    }    

	ulCurrMode = (CortexMRegArray[REG_CONTROL] & 0x02);

	if((REG_APSR == ulRegNum) || (REG_IPSR == ulRegNum) ||
		(REG_EPSR == ulRegNum))
	{
		if(REG_APSR == ulRegNum)
		{
			CortexMRegArray[REG_APSR] = ((*pulState) & 0xF8000000);
		}
		else if (REG_IPSR == ulRegNum)
		{
			CortexMRegArray[REG_IPSR] = ((*pulState) & 0x000001FF);
		}
		else
		{
			CortexMRegArray[REG_EPSR] = ((*pulState) & 0x07FFFC00);
		}
		/* xPSR value is if formed by combining these three register values.
		So update xPSR if any of these is updated. */
		CortexMRegArray[REG_xPSR] = ((CortexMRegArray[REG_APSR] & 0xF8000000) |
										(CortexMRegArray[REG_IPSR] & 0x000001FF) |
										(CortexMRegArray[REG_EPSR] & 0x07FFFC00));

	}
	else if (REG_xPSR == ulRegNum)
	{
		/* If xPSR is updated, parse value for the below  three registers 
		from the xPSR value and update them. */
        
		CortexMRegArray[REG_xPSR] = *pulState;

		// Extract APSR From xPSR
        CortexMRegArray[REG_APSR] = ((*pulState) & 0xF8000000);
        // Extract IPSR From xPSR
        CortexMRegArray[REG_IPSR] = ((*pulState) & 0x000001FF);
        // Extract EPSR From xPSR
        CortexMRegArray[REG_EPSR] = ((*pulState) & 0x07FFFC00);

	}
	else if(ulRegNum == REG_CurrSP)
	{
		if ((00 == ulMode) || (0x10 == ulMode) || (0xFF == ulMode)) // Added (0xFF == ulMode) for working with GDB & IAR.
		{
			CortexMRegArray[REG_MSP] = *pulState;
			if (0 == ulCurrMode)
			{
				CortexMRegArray[REG_CurrSP] = CortexMRegArray[REG_MSP];
			}
		}
		else if ((02 == ulMode || 0x12 == ulMode) || (0xFF == ulMode)) // Added (0xFF == ulMode) for working with GDB & IAR.
		{
			CortexMRegArray[REG_PSP] = *pulState;
			if (2 == ulCurrMode)
			{
				CortexMRegArray[REG_CurrSP] = CortexMRegArray[REG_PSP];				
			}
		}
		else
		{
			CortexMRegArray[REG_CurrSP] = *pulState;
		}

				
	}
	else if(ulRegNum == REG_CONTROL)
	{
		CortexMRegArray[REG_CONTROL] = *pulState;

		ulCurrMode = (CortexMRegArray[REG_CONTROL] & 0x02);
		
		if (2 == ulCurrMode)
		{
			CortexMRegArray[REG_CurrSP] = CortexMRegArray[REG_PSP];
		}
		else
		{
			CortexMRegArray[REG_CurrSP] = CortexMRegArray[REG_MSP];
		}
	}

	else
	{
		CortexMRegArray[ulRegNum] = *pulState;
	}


    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ClearBreakProc
    Engineer: Suraj S
       Input: RDI_PointHandle hPointHandle - Handle of breakpoint to remove
      Output: TyError - Mid Layer error code
 Description: Clears a breakpoint
Date          Initials      Description
12-Jul-2007      SJ         Initial
25-Aug-2009      RTR        Added ARM11 support
****************************************************************************/
TyError ML_ClearBreakProc(RDI_PointHandle hPointHandle)
{
    TyError ErrRet;

    TyBreakpointStructure tyBreakpoint = {0};
    unsigned long  ulNumBytes                    = 0;
    unsigned int   i                             = 0;
    unsigned int   usRemoveIndex                 = 0;
    boolean        bLeaveHardwareResource        = FALSE;

    // find the original instruction data and address...
    for (i = 0; i < usNumberOfBreakpoints; i++)
    {
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			if (( pCortexBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short) hPointHandle) &&
				(!pCortexBPArray->BreakpointStructures[i].bWatchPoint))
			{
				tyBreakpoint  = pCortexBPArray->BreakpointStructures[i];
				break;
			}
		} 
		else
		{
			if (( pBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short) hPointHandle) &&
				(!pBPArray->BreakpointStructures[i].bWatchPoint))
			{
				tyBreakpoint  = pBPArray->BreakpointStructures[i];
				break;
			}
		}
        
    }

    if (i >= usNumberOfBreakpoints)
    {
        // Something's gone wrong...
        // TODO: ASSERT(FALSE);
        return ML_Error_NoSuchPoint;
    }

    
    usRemoveIndex = i;

    if (tyBreakpoint.ucNoOfBPsAtAddress == 0)
    {
        // This was a second or further breakpoint.
        // We have to find the original BP and decrement the counter by 1.
        for (i=0; i < usNumberOfBreakpoints; i++)
        {
			if ((tyProcessorConfig.bCortexM3) ||
				(tyProcessorConfig.bCortexA8) ||
				(tyProcessorConfig.bCortexA9))
			{
				if ((pCortexBPArray->BreakpointStructures[i].ulAddress          == tyBreakpoint.ulAddress) &&
					(pCortexBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress >= 1)                      &&
					(pCortexBPArray->BreakpointStructures[i].bWatchPoint        == FALSE))
					// Found it...
                break;
			} 
			else
			{
				if ((pBPArray->BreakpointStructures[i].ulAddress          == tyBreakpoint.ulAddress) &&
					(pBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress >= 1)                      &&
					(pBPArray->BreakpointStructures[i].bWatchPoint        == FALSE))
					// Found it...
                break;
			}
            
        }

        // TODO: ASSERT(i < usNumberOfBreakpoints);
      
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			pCortexBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress --;
			
			// Now that we are finished, we can safely remove the breakpoint from
			// our array...
			for (i = usRemoveIndex+1; i < usNumberOfBreakpoints; i++)
				pCortexBPArray->BreakpointStructures[i-1] = pCortexBPArray->BreakpointStructures[i];
		}
		else
		{
			pBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress --;

			// Now that we are finished, we can safely remove the breakpoint from
			// our array...
			for (i= usRemoveIndex+1; i < usNumberOfBreakpoints; i++)
				pBPArray->BreakpointStructures[i-1] = pBPArray->BreakpointStructures[i];
		}

        usNumberOfBreakpoints--;

        return ML_ERROR_NO_ERROR;
    }

    if (tyBreakpoint.ucNoOfBPsAtAddress > 1)
    {
        // This is the original breakpoint, with other breakpoints set !!!
        // We have to find a second BP and make it the original BP.
        for (i=0; i < usNumberOfBreakpoints; i++)
        {
			if ((tyProcessorConfig.bCortexM3) ||
				(tyProcessorConfig.bCortexA8) ||
				(tyProcessorConfig.bCortexA9))
			{
				if ((pCortexBPArray->BreakpointStructures[i].ulAddress            == tyBreakpoint.ulAddress) &&
					(pCortexBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress   == 0)                      &&
					(pCortexBPArray->BreakpointStructures[i].bWatchPoint          == FALSE))
					// Found it...
					break;
			}
			else
			{
				if ((pBPArray->BreakpointStructures[i].ulAddress            == tyBreakpoint.ulAddress) &&
					(pBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress   == 0)                      &&
					(pBPArray->BreakpointStructures[i].bWatchPoint          == FALSE))
					// Found it...
					break;
			}
        }

        // TODO: ASSERT(i < usNumberOfBreakpoints);

		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			pCortexBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress = (unsigned char)(tyBreakpoint.ucNoOfBPsAtAddress - 1);
			
			// Now that we are finished, we can safely remove the breakpoint from
			// our array...
			for (i = usRemoveIndex+1; i < usNumberOfBreakpoints; i++)
				pCortexBPArray->BreakpointStructures[i-1] = pCortexBPArray->BreakpointStructures[i];

		} 
		else
		{
			pBPArray->BreakpointStructures[i].ucNoOfBPsAtAddress = (unsigned char)(tyBreakpoint.ucNoOfBPsAtAddress - 1);
			
			// Now that we are finished, we can safely remove the breakpoint from
			// our array...
			for (i=usRemoveIndex+1; i < usNumberOfBreakpoints; i++)
				pBPArray->BreakpointStructures[i-1] = pBPArray->BreakpointStructures[i];

		}
        
        usNumberOfBreakpoints--;

        return ML_ERROR_NO_ERROR;
    }


    if ((tyBreakpoint.ucBPIDNo == SOFTWARE_BP0) ||
        (tyBreakpoint.ucBPIDNo == SOFTWARE_BP1))
    {
        // To remove a software breakpoint we need to write the previous 
        // instruction back to the memory...
        if((tyProcessorConfig.bARM7) ||
			(tyProcessorConfig.bARM9))
        {
            ErrRet = ML_Select_Scan_Chain(1,1);

			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
        }

        // Write out the original inst data...
        if (tyBreakpoint.ulBPType & RDIPoint_16Bit)
            ulNumBytes = THUMB_NO_OF_BYTES;
        else 
            ulNumBytes = ARM_NO_OF_BYTES;
      
        ErrRet = ML_WriteProc(&tyBreakpoint.ulPreviousInstructionData, 
                             tyBreakpoint.ulAddress, 
                             &ulNumBytes, 
                             RDIAccess_Data32,
                             FALSE,
                             TRUE);
        if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;

        // If this is followed by a reset, we need to do the write twice. Not too sure
        // why this is. Seems to be that the data we have just wrote is cached within
        // in the ARM before it is actually wrote out.
        if (tyBreakpoint.ulBPType & RDIPoint_16Bit)
            ulNumBytes = THUMB_NO_OF_BYTES;
        else 
            ulNumBytes = ARM_NO_OF_BYTES;
      
        ErrRet = ML_WriteProc(&tyBreakpoint.ulPreviousInstructionData, 
                             tyBreakpoint.ulAddress, 
                             &ulNumBytes, 
                             RDIAccess_Data32,
                             FALSE,
                             TRUE);
        if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;
    }

    // Now that we are finished, we can safely remove the breakpoint from
    // our array...
    for (i=usRemoveIndex+1; i < usNumberOfBreakpoints; i++)
    {
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			pCortexBPArray->BreakpointStructures[i-1] = pCortexBPArray->BreakpointStructures[i];
		} 
		else
		{
			pBPArray->BreakpointStructures[i-1] = pBPArray->BreakpointStructures[i];
		}
        
    }
    usNumberOfBreakpoints--;

    if ((tyProcessorConfig.bARM7) ||
        (tyProcessorConfig.bARM9))
    {
		if(FEROCEON != tyTargetConfig.tyDeviceType)
		{
			// Check for software breakpoints using WP0.
			if (tyBreakpoint.ucBPIDNo == SOFTWARE_BP0)
			{
				// TODO: ASSERT(tyBPResourceConfig.ucWP0Used == SOFTWARE_BP);

				bLeaveHardwareResource = FALSE;

				for (i=0; i < usNumberOfBreakpoints; i++)
				{
					if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_BP0)
					{
						// Leave the resource as it is.
						bLeaveHardwareResource = TRUE;
						break;
					}
				}

				// There are no breakpoints using resource 0 it can be cleared.
				if (!bLeaveHardwareResource)
					ML_ClearHardwareResource(HARDWARE_RESOURCE0);
			}

			// Check for software breakpoints using WP1.
			if (tyBreakpoint.ucBPIDNo == SOFTWARE_BP1)
			{
				// TODO: ASSERT(tyBPResourceConfig.ucWP1Used == SOFTWARE_BP);

	            bLeaveHardwareResource = FALSE;

		        for (i=0; i < usNumberOfBreakpoints; i++)
				{
					if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_BP1)
					{
						// Leave the resource as it is.
						bLeaveHardwareResource = TRUE;
						break;
					}
				}

				// There are no breakpoints using resource 1 it can be cleared.
				if (!bLeaveHardwareResource)
					ML_ClearHardwareResource (HARDWARE_RESOURCE1);
			}
		}
		// Check for hardware breakpoints using WP0.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP0)
        {
            // TODO: ASSERT(tyBPResourceConfig.ucWP0Used == HARDWARE_BP);

            // There are no breakpoints using this resource it can be cleared.
            ML_ClearHardwareResource (HARDWARE_RESOURCE0);
        }

        // Check for hardware breakpoints using WP1.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP1)
        {
            // TODO: ASSERT(tyBPResourceConfig.ucWP1Used == HARDWARE_BP);

            // There are no breakpoints using this resource it can be cleared.
            ML_ClearHardwareResource (HARDWARE_RESOURCE1);
        }

        // Check for hardware breakpoints using both WP0 & WP1.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BPX)
        {
            // TODO: ASSERT(tyBPResourceConfig.ucWP0Used == HARDWARE_BP);
            // TODO: ASSERT(tyBPResourceConfig.ucWP1Used == HARDWARE_BP);

            // There are no breakpoints using this resource it can be cleared.
            ML_ClearHardwareResource (HARDWARE_RESOURCE0);
            ML_ClearHardwareResource (HARDWARE_RESOURCE1);
        }
    }
    else if (tyProcessorConfig.bARM11)//ARM11
    {
        // Check for hardware breakpoints using BP0.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP0)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearARM11BPResource(HARDWARE_RESOURCE0);
        }
		
        // Check for hardware breakpoints using BP1.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP1)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearARM11BPResource(HARDWARE_RESOURCE1);
        }

        // Check for hardware breakpoints using BP2.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP2)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearARM11BPResource(HARDWARE_RESOURCE2);
        }

        // Check for hardware breakpoints using BP3.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP3)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearARM11BPResource(HARDWARE_RESOURCE3);
        }

        // Check for hardware breakpoints using BP4.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP4)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearARM11BPResource(HARDWARE_RESOURCE4);
        }
		
        // Check for hardware breakpoints using BP5.
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BP5)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearARM11BPResource(HARDWARE_RESOURCE5);
        }
		
        // Check for hardware breakpoints using two BPs
        if (tyBreakpoint.ucBPIDNo == HARDWARE_BPX)
        {
            // There are no breakpoints using this resource it can be cleared.
            ML_ClearHardwareResource (tyBreakpoint.ucBPResourceNo1);
            ML_ClearHardwareResource (tyBreakpoint.ucBPResourceNo2);
        }
    }
    else if (tyProcessorConfig.bCortexM3)
    {
        unsigned long ulHbPointRes;

        for (ulHbPointRes = HARDWARE_BP0; ulHbPointRes < tyArmv7MBreakpointResArray.ulNumBreakpoint; ulHbPointRes++)
        {
            if (tyBreakpoint.ucBPIDNo == ulHbPointRes)
            {
				//tyArmv7MBreakpointResArray.ulCompResUsed[ulHbPointRes] = FPC_COMP_NOT_USED;
                // Clear the Cortex-M3 Hardware break point resource.
				pCortexBPArray->ucHardBPResource[ulHbPointRes] = FPC_COMP_NOT_USED;
                tyArmv7MBreakpointResArray.ulFlashPatchCompVal[ulHbPointRes] = FPC_DISABLE;
				break;
                //ML_ClearCortexM3BPResource(ulHbPointRes);
            }

        }
    }

	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		unsigned long ulHbPointRes;
		for (ulHbPointRes = HARDWARE_BP0; ulHbPointRes < tyArmv7ABreakpointResArray.ulNumBreakpoint; ulHbPointRes++)
        {
            if (tyBreakpoint.ucBPIDNo == ulHbPointRes)
            {				
				pCortexBPArray->ucHardBPResource[ulHbPointRes] = FPC_COMP_NOT_USED;
                tyArmv7ABreakpointResArray.tyArmv7ABreakpointRes[ulHbPointRes].ulBreakControlReg = BCR_BREAK_DISABLE;
				break;                
            }
			
        }
	}

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_ExecuteProc
     Engineer: Suraj S
        Input: None
       Output: TyError - Mid Layer error code
  Description: Starts the processor executing
Date          Initials      Description
20-Jul-2007      SJ         Initial
30-Oct-2009	     JCK		Added support for Cortex-M3
****************************************************************************/
TyError ML_ExecuteProc  (void)
{
	unsigned long ulTempReg0            = 0x0;
	//unsigned long ulTempReg0Reversed    = 0x0;
	//unsigned long ulReg0Val = 0x0;
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	ErrRet = DL_OPXD_IndicateTargetStatus(tyCurrentDevice,tyTargetConfig.ulDebugCoreNumber,TRUE);
	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);

	//Initilise the Semihosting just in case the users code overwrote the vector table.
	ErrRet = ML_Initialise_SemiHosting();
	if(ErrRet != ML_ERROR_NO_ERROR)
	{
		bGlobalStartedExecution = FALSE;  return ErrRet;
	}

	//These variables are used to switch back from ARM mode to Thumb Mode.
	if (bGlobalThumb)
	{
		ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];		

		if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;
		}
		else
		{
			RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;
		}
      
		//Now the bit reversing is done in the diskware
		// The data value has to be reversed for ARM7 cores.
		//      ((unsigned char *)&ulTempReg0Reversed)[0]  = ucBitInverse[((unsigned char *)&ulTempReg0)[3]];
		//      ((unsigned char *)&ulTempReg0Reversed)[1]  = ucBitInverse[((unsigned char *)&ulTempReg0)[2]];
		//      ((unsigned char *)&ulTempReg0Reversed)[2]  = ucBitInverse[((unsigned char *)&ulTempReg0)[1]];
		//      ((unsigned char *)&ulTempReg0Reversed)[3]  = ucBitInverse[((unsigned char *)&ulTempReg0)[0]];
	}
	else
	{
		ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];
	}

	if (bUSBDevice) 
	{
        if ((tyProcessorConfig.bARM7) ||
            (tyProcessorConfig.bARM9))
		{

			ErrRet = ML_ARM_SetupExecuteProc(tyBPResourceConfig.ulWP0AddressValue,
											tyBPResourceConfig.ulWP0AddressMask,                       
											tyBPResourceConfig.ulWP0DataValue,    
											tyBPResourceConfig.ulWP0DataMask,     
											tyBPResourceConfig.ulWP0CtrlValue,    
											tyBPResourceConfig.ulWP0CtrlMask,     
											tyBPResourceConfig.ulWP1AddressValue, 
											tyBPResourceConfig.ulWP1AddressMask,  
											tyBPResourceConfig.ulWP1DataValue,	   
											tyBPResourceConfig.ulWP1DataMask,	   
											tyBPResourceConfig.ulWP1CtrlValue,	   
											tyBPResourceConfig.ulWP1CtrlMask);

			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;

			//These functions leave the core in ScanChain 1
			ulGlobalScanChain = 0x1;
		}
        else if (tyProcessorConfig.bARM11)
        {
            ErrRet = ML_ARM_SetupExecuteProcARM11((unsigned long*)&tyBPResourceARM11Config);
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;
        }
        else if (tyProcessorConfig.bCortexM3)
        {
            ErrRet = ML_ARM_SetupExecuteProcCortexM(tyArmv7MBreakpointResArray.ulNumBreakpoint,
                                                     &tyArmv7MBreakpointResArray,
                                                     tyArmv7MWatchpointResArray.ulNumWatchpoint,
                                                     &tyArmv7MWatchpointResArray.tyCortexMWpResStruct[0]);
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;

        }
		else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			ErrRet = ML_ARM_SetupExecuteProcCortexA(tyArmv7ABreakpointResArray.ulNumBreakpoint,
				tyArmv7ABreakpointResArray.tyArmv7ABreakpointRes,
				tyArmv7AWatchpointResArray.ulNumWatchPoint,
				tyArmv7AWatchpointResArray.tyArmv7AulWatchPointRes);
		}
    }
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;
	// Restore the registers before execution.
    if (tyProcessorConfig.bCortexM3)
    {
        ErrRet = ML_CortexMWriteRegisters((unsigned long *)CortexMRegArray);
    }	
    else
    {
        ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
    }

	if(ErrRet != ML_ERROR_NO_ERROR)
	{
		bGlobalStartedExecution = FALSE;  return ErrRet;
	}

	if (bUSBDevice)
	{
		if(tyProcessorConfig.bARM7) 
        {
			if(!tyProcessorConfig.bSynthesisedCore)
				TyProcType = ARM7;
			else if(tyProcessorConfig.bSynthesisedCore)
				TyProcType = ARM7S;
		}
		else if(tyProcessorConfig.bARM9) 
		{
			TyProcType = ARM9;
		}
		else if(tyProcessorConfig.bARM11)
		{
			TyProcType = ARM11;
		}
        else if (tyProcessorConfig.bCortexM3)
        {
            TyProcType = CORTEXM;
        }
		else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			TyProcType = CORTEXA;
		}

		if(tyProcessorConfig.bARM11) 
		{
			ARM11_DataArray[2] = ulTempReg0;
			ErrRet = DL_OPXD_Arm_Leave_Debug_State(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bGlobalThumb, ARM11_DataArray, sizeof(ARM11_DataArray));
			if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
		}
        else if ((tyProcessorConfig.bCortexM3) ||
				 (tyProcessorConfig.bCortexA8) ||
				 (tyProcessorConfig.bCortexA9))
        {	
			ARM11_DataArray[0] = ulTempReg0;
			ErrRet = ML_LeaveDebugState();
            //ErrRet = DL_OPXD_Arm_Leave_Debug_State(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bGlobalThumb, ARM11_DataArray, sizeof(ARM11_DataArray));
            if (ErrRet != ML_ERROR_NO_ERROR)
                return (ErrRet);
        }
		else
		{			
			ErrRet = DL_OPXD_Arm_ExecuteProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,TyProcType, bGlobalThumb, ulTempReg0);
			if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
				return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);

			//These functions leave the core in ScanChain 2
			ulGlobalScanChain = 0x2;
		}
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	// Set global variables .. The core is now executing.
	bGlobalStartedExecution = TRUE;
	bUserHalt =FALSE;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_StepProc
     Engineer: Suraj S
        Input: None
       Output: TyError - Mid Layer error code
  Description: Single stepping 
Date          Initials      Description
27-Feb-2008   SJ            Initial
13-Mar-2008   JCK           Implementation of Single Step for ARM7 core
27-Aug-2009   RTR           Added ARM11 support  
****************************************************************************/
TyError ML_StepProc  (void)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;   
	TyArmProcType TyProcType = NOCORE;   

	ErrRet = DL_OPXD_IndicateTargetStatus(tyCurrentDevice,tyTargetConfig.ulDebugCoreNumber,TRUE);
	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);

	//Initilise the Semihosting just in case the users code overwrote the vector table.
	ErrRet = ML_Initialise_SemiHosting();
	if(ErrRet != ML_ERROR_NO_ERROR)
	{
		bGlobalStartedExecution = FALSE;  return ErrRet;
	}
	if (bUSBDevice)
	{
		if(tyProcessorConfig.bARM7) 
        {
			TyProcType = ARM7;     
			ErrRet = ML_ARM7StepProc();
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;     
		}
		else if(tyProcessorConfig.bARM9) 
		{
			ErrRet = ML_ARM9StepProc();
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
		}
		else if(tyProcessorConfig.bARM11) 
		{
			ErrRet = ML_ARM11StepProc();
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
		}
		else if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMStepProc();
			if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;
		}
		else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{		
			ErrRet = ML_Armv7AStepProc();
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;				
		}
    }
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	// Set global variables .. The core is now executing.
	bGlobalStartedExecution = TRUE;
	bUserHalt =FALSE;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ARM_SetupExecuteProc
    Engineer: Suraj S
       Input: unsigned long ulWP0AddressValue - WP0 Address Register Value
              unsigned long ulWP0AddressMask - WP0 Address Register Mask
              unsigned long ulWP0DataValue - WP0 Data Register Value
              unsigned long ulWP0DataMask - WP0 Data Register Mask
              unsigned long ulWP0CtrlValue - WP0 Control Register Value
              unsigned long ulWP0CtrlMask - WP0 Control Register Mask
              unsigned long ulWP1AddressValue - WP1 Address Register Value
              unsigned long ulWP1AddressMask - WP1 Address Register Mask
              unsigned long ulWP1DataValue - WP1 Data Register Value
              unsigned long ulWP1DataMask - WP1 Data Register Mask
              unsigned long ulWP1CtrlValue - WP1 Control Register Value
              unsigned long ulWP1CtrlMask - WP1 Control Register Mask
      Output: TyError - Mid Layer error code
 Description: Clears the given hardware resource.
Date          Initials      Description
19-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ARM_SetupExecuteProc(unsigned long ulWP0AddressValue, 
                                unsigned long ulWP0AddressMask, 
                                unsigned long ulWP0DataValue, 
                                unsigned long ulWP0DataMask, 
                                unsigned long ulWP0CtrlValue, 
                                unsigned long ulWP0CtrlMask, 
                                unsigned long ulWP1AddressValue, 
                                unsigned long ulWP1AddressMask, 
                                unsigned long ulWP1DataValue, 
                                unsigned long ulWP1DataMask, 
                                unsigned long ulWP1CtrlValue, 
                                unsigned long ulWP1CtrlMask)
{
   
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;
   unsigned long pulWPRegValues[MAX_WP_REG];

   if(tyProcessorConfig.bARM7)
   {
      if(!tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7;
      else if(tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7S;
   }
   else if(tyProcessorConfig.bARM9)
      {
         TyProcType = ARM9;
      }

   pulWPRegValues[0]  = ulWP0AddressValue;
   pulWPRegValues[1]  = ulWP0AddressMask;
   pulWPRegValues[2]  = ulWP0DataValue;
   pulWPRegValues[3]  = ulWP0DataMask;
   pulWPRegValues[4]  = ulWP0CtrlValue;
   pulWPRegValues[5]  = ulWP0CtrlMask;
   pulWPRegValues[6]  = ulWP1AddressValue;
   pulWPRegValues[7]  = ulWP1AddressMask;
   pulWPRegValues[8]  = ulWP1DataValue;
   pulWPRegValues[9]  = ulWP1DataMask;
   pulWPRegValues[10] = ulWP1CtrlValue;
   pulWPRegValues[11] = ulWP1CtrlMask;

   ErrRet = DL_OPXD_Arm_SetupExecuteProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulWPRegValues);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   else
      return ML_ERROR_NO_ERROR;

}

/****************************************************************************
    Function: ML_ARM_SetupExecuteProcARM11
    Engineer: Roshan T R
       Input: unsigned long *ulBPRegArray - Array of values for Break point registers
      Output: TyError - Mid Layer error code
 Description: Setup the hardware resources on execution start
Date          Initials      Description
10-Sep-2009   RTR           Initial
****************************************************************************/
TyError ML_ARM_SetupExecuteProcARM11(unsigned long *ulBPRegArray) 

{
   
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;
   unsigned long pulBPRegValues[MAX_BP_REG_ARM11];
   unsigned long ulCount = 0;

   TyProcType = ARM11;

   do 
   {
		pulBPRegValues[ulCount++]= *ulBPRegArray++;
   } while (ulCount < MAX_BP_REG_ARM11);


   ErrRet = DL_OPXD_Arm_SetupExecuteProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulBPRegValues);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   else
      return ML_ERROR_NO_ERROR;

}


/****************************************************************************
    Function: ML_StopCoreAtZero
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Stops the core at the reset vector.
Date          Initials      Description
20-Jul-2007   SJ            Initial
****************************************************************************/
TyError ML_StopCoreAtZero (void)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TXRXCauseOfBreak tyCauseOfBreak;
	RDI_PointHandle  hPointHandle;
	RDI_PointHandle  tyTempBreakPointHandle1;
	RDI_PointHandle  tyTempBreakPointHandle2;
	unsigned long    ulCurrentUseHWBreakpoints;
	unsigned long    ulCPSRValue;
	unsigned long    ulDebugStatusValue;
	boolean          bExecuting      = FALSE;
	boolean          bCoreDidNotStop = FALSE;

	bGlobalStartedExecution = TRUE;
   
	//Commented for Optimisation
	//This function expects scan chain 2
	/*   ErrRet = ML_Select_Scan_Chain(2,1);
	if (ErrRet!=ML_ERROR_NO_ERROR)
	{
		ErrRet = ML_Select_Scan_Chain(2,1);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ErrRet;
	}*/

	if((tyProcessorConfig.bARM7) ||
		(tyProcessorConfig.bARM9))
	{
		ErrRet = ML_ReadICEBreaker (DBG_STATUS_REG, &ulDebugStatusValue);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ErrRet;
	}

	//Stop the core
	ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
	if (!((ErrRet==ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_TargetStopped)))
		return ErrRet;

	//Check the status of the processor.
	ErrRet = ML_StatusProc(&bExecuting,
	                       &tyCauseOfBreak,
                           &hPointHandle,
						   NULL);
	if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

	//Read the current setting of the used hardware breakpoints so 
	//that we can restore it afterwards.
	ML_GetUseOnlyHardwareBreakPoints (&ulCurrentUseHWBreakpoints);

	//Force the setting of hardware breakpoints.
	ML_SetUseOnlyHardwareBreakPoints (0x1);

	//Set hardware breakpoints at the two possible reset vectors.
	ErrRet = ML_SetBreakProc (0x0, 0x0, 0x0, &tyTempBreakPointHandle1);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ErrRet;

	ErrRet = ML_SetBreakProc (0xFFFF0000, 0x0, 0x0, &tyTempBreakPointHandle2);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ErrRet;

	//Restore the previous value of use hardware breakpoints.
	ML_SetUseOnlyHardwareBreakPoints (ulCurrentUseHWBreakpoints);

	//Read the CPSR
	ErrRet =  ML_CPUReadProc (SVC_MODE, 0x20000, &ulCPSRValue);
	if (ErrRet!=ML_ERROR_NO_ERROR)
		return ErrRet;

	//Set the CPSR to suprevisor mode, IRQ and FIQ disabled.
	ulCPSRValue = (ulCPSRValue | 0x60);

	//Write the value back to the CPSR
    if (tyProcessorConfig.bCortexM3)
    {
        ErrRet = ML_CortexMCPUWriteProc(SVC_MODE, 0x20000, &ulCPSRValue);
    }	
    else
    {
        ErrRet =  ML_CPUWriteProc (SVC_MODE, 0x20000, &ulCPSRValue);
    }
	if (ErrRet!=ML_ERROR_NO_ERROR)
		return ErrRet;

	//Restart the core
	ErrRet = ML_ExecuteProc();
	if (ErrRet!=ML_ERROR_NO_ERROR)
		return ErrRet;

	//Reset the target.
	ErrRet = ML_InfoProc(RDIInfo_ForceSystemReset, NULL, NULL);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ErrRet;

	ErrRet = ML_Initialise_Debugger(&tyTargetConfig);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ErrRet;
   
	//Commented for Optimisation
	/*ErrRet = ML_Select_Scan_Chain(2,1);
	if (ErrRet!=ML_ERROR_NO_ERROR)
	{
		ErrRet = ML_Select_Scan_Chain(2,1);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ErrRet;
	}*/

	ErrRet = ML_WaitForSystemSpeedCompletion();
	if (ErrRet != ML_ERROR_NO_ERROR)
		bCoreDidNotStop = TRUE;
	else
		bCoreDidNotStop = FALSE;

	ErrRet = ML_ClearBreakProc (tyTempBreakPointHandle1);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ErrRet;

	ErrRet = ML_ClearBreakProc (tyTempBreakPointHandle2);
	if (ErrRet != ML_ERROR_NO_ERROR)
		return ErrRet;

	if (bCoreDidNotStop)
		return ML_Error_TargetRunning;
      
	return ErrRet;
}

/****************************************************************************
    Function: ML_ClearHardwareResource
    Engineer: Suraj S
       Input: Hardware resource to clear.
      Output: None
 Description: Clears the given hardware resource.
Date          Initials      Description
10-Jul-2007      SJ         Initial
****************************************************************************/
static void ML_ClearHardwareResource (unsigned long ulResourceNo)

{
    if (ulResourceNo == HARDWARE_RESOURCE0)
    {
        tyBPResourceConfig.ucWP0Used         = NOT_USED; 
        tyBPResourceConfig.ulWP0AddressValue = RESOURCE_CLEARED;
        tyBPResourceConfig.ulWP0AddressMask  = RESOURCE_CLEARED;
        tyBPResourceConfig.ulWP0DataValue	 = RESOURCE_CLEARED; 
        tyBPResourceConfig.ulWP0DataMask	 = RESOURCE_CLEARED; 
        tyBPResourceConfig.ulWP0CtrlValue    = RESOURCE_CLEARED;
        tyBPResourceConfig.ulWP0CtrlMask     = RESOURCE_CLEARED;
    }
    else
    if (ulResourceNo == HARDWARE_RESOURCE1)
    {
        tyBPResourceConfig.ucWP1Used         = NOT_USED; 
        tyBPResourceConfig.ulWP1AddressValue = RESOURCE_CLEARED;
        tyBPResourceConfig.ulWP1AddressMask  = RESOURCE_CLEARED;
        tyBPResourceConfig.ulWP1DataValue	 = RESOURCE_CLEARED; 
        tyBPResourceConfig.ulWP1DataMask	 = RESOURCE_CLEARED; 
        tyBPResourceConfig.ulWP1CtrlValue    = RESOURCE_CLEARED;
        tyBPResourceConfig.ulWP1CtrlMask     = RESOURCE_CLEARED;
    }
/*TODO:   else
      ASSERT_NOW();*/

    return;
}

/****************************************************************************
    Function: ML_ClearARM11BPResource
    Engineer: Roshan T R
       Input: BP resource to clear.
      Output: None
 Description: Clears the specified BP resource for ARM11
Date          Initials      Description
25-Aug-2009   RTR           Initial
****************************************************************************/
static void ML_ClearARM11BPResource (unsigned long ulResourceNo)

{
    if (ulResourceNo == HARDWARE_RESOURCE0)
    {
        tyBPResourceARM11Config.ucBP0Used         = NOT_USED;
        tyBPResourceARM11Config.ulBP0AddressValue = RESOURCE_CLEARED;
        tyBPResourceARM11Config.ulBP0CtrlValue    = RESOURCE_CLEARED;
    }
    else
    if (ulResourceNo == HARDWARE_RESOURCE1)
    {
        tyBPResourceARM11Config.ucBP1Used         = NOT_USED;
        tyBPResourceARM11Config.ulBP1AddressValue = RESOURCE_CLEARED;
        tyBPResourceARM11Config.ulBP1CtrlValue    = RESOURCE_CLEARED;
    }
    else
	if (ulResourceNo == HARDWARE_RESOURCE2)
	{
		tyBPResourceARM11Config.ucBP2Used         = NOT_USED;
		tyBPResourceARM11Config.ulBP2AddressValue = RESOURCE_CLEARED;
		tyBPResourceARM11Config.ulBP2CtrlValue    = RESOURCE_CLEARED;
    }
	else
    if (ulResourceNo == HARDWARE_RESOURCE3)
    {
        tyBPResourceARM11Config.ucBP3Used         = NOT_USED;
        tyBPResourceARM11Config.ulBP3AddressValue = RESOURCE_CLEARED;
        tyBPResourceARM11Config.ulBP3CtrlValue    = RESOURCE_CLEARED;
    }
    else
	if (ulResourceNo == HARDWARE_RESOURCE4)
	{
		tyBPResourceARM11Config.ucBP4Used         = NOT_USED;
		tyBPResourceARM11Config.ulBP4AddressValue = RESOURCE_CLEARED;
		tyBPResourceARM11Config.ulBP4CtrlValue    = RESOURCE_CLEARED;
	}
	else
	if (ulResourceNo == HARDWARE_RESOURCE5)
	{
		tyBPResourceARM11Config.ucBP5Used         = NOT_USED;
		tyBPResourceARM11Config.ulBP5AddressValue = RESOURCE_CLEARED;
		tyBPResourceARM11Config.ulBP5CtrlValue    = RESOURCE_CLEARED;
    }

/*TODO:   else
      ASSERT_NOW();*/

   return;
}

/****************************************************************************
    Function: ML_ClearARM11WPResource
    Engineer: Roshan T R
       Input: WP resource to clear.
      Output: None
 Description: Clears the specified WBP resource for ARM11
Date          Initials      Description
27-Aug-2009   RTR           Initial
****************************************************************************/
static void ML_ClearARM11WPResource (unsigned long ulResourceNo)

{
    if (ulResourceNo == HARDWARE_RESOURCE0)
    {
        tyWPResourceARM11Config.ucWP0Used         = NOT_USED;
        tyWPResourceARM11Config.ulWP0AddressValue = RESOURCE_CLEARED;
        tyWPResourceARM11Config.ulWP0CtrlValue    = RESOURCE_CLEARED;
    }
    else
    if (ulResourceNo == HARDWARE_RESOURCE1)
    {
        tyWPResourceARM11Config.ucWP1Used         = NOT_USED;
        tyWPResourceARM11Config.ulWP1AddressValue = RESOURCE_CLEARED;
        tyWPResourceARM11Config.ulWP1CtrlValue    = RESOURCE_CLEARED;
    }

/*TODO:   else
      ASSERT_NOW();*/

   return;
}
/****************************************************************************
    Function: ML_ReadWord
    Engineer: Suraj S
       Input: unsigned long ulAddress -  Address to read from
              unsigned long *pulReadValue - Pointer to return value read 
      Output: TyError - Mid Layer error code
 Description: Read a single word from processor memory
Date          Initials      Description
10-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ReadWord (unsigned long ulAddress, unsigned long *pulReadValue)
{

	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
	{
		TyProcType = CORTEXM;
	}
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}

   if (bUSBDevice)
      {
         ErrRet = DL_OPXD_Arm_ReadWord(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, pulReadValue);
         if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
            return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
      }
   else
      return ML_ERROR_NO_USB_DEVICE_CONNECTED;
   
   return ErrRet;
}

/****************************************************************************
    Function: ML_ReadMultipleBlocks
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long *ulData - Pointer to read data
              unsigned long ulBlocks - Number of blocks of data to read
      Output: TyError - Mid Layer error code
 Description: Read multiple blocks of data from processor memory
Date           Initials     Description
18-Jul-2007      SJ         Initial
***************************************************************************/
TyError ML_ReadMultipleBlocks (unsigned long ulAddress, unsigned long *ulData, unsigned long ulBlocks)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
	  if(!tyProcessorConfig.bSynthesisedCore)
		 TyProcType = ARM7;
	  else if(tyProcessorConfig.bSynthesisedCore)
		 TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}
   
   if (bUSBDevice)
      {
         ErrRet = DL_OPXD_Arm_ReadMultipleBlocks(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, ulData, ulBlocks);
         if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
            return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
      }
   else
      return ML_ERROR_NO_USB_DEVICE_CONNECTED;

   return ErrRet;
}

/****************************************************************************
    Function: ML_ReadMultipleWords
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long *ulData - Pointer to read data
              unsigned long ulWords - Number of words to read
      Output: TyError - Mid Layer error code
 Description: Read multiple words from processor memory
Date           Initials     Description
18-Jul-2007      SJ         Initial
***************************************************************************/
TyError ML_ReadMultipleWords (unsigned long ulAddress, unsigned long *ulData, unsigned long ulWords)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}

	if (bUSBDevice)
	{
		ErrRet = DL_OPXD_Arm_ReadMultipleWords(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, ulData, ulWords);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	return ErrRet;
}

/****************************************************************************
    Function: ML_ReadByteUsingWordAccess
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to read from
              unsigned char *pucData - Data read from target
      Output: TyError - Mid Layer error code
 Description: Reads a single byte from processor memory using word access.
Date          Initials      Description
18-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ReadByteUsingWordAccess (unsigned long ulAddress, 
                                    unsigned char *pucData)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   unsigned long ulTempWord = 0x0;

   //Read word from word aligned address
   ErrRet =  ML_ReadWord ((ulAddress & 0xFFFFFFFC),  &ulTempWord);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   switch (ulAddress & 0x3)
      {
      default:
      case 0x0:
         *pucData = (unsigned char) (ulTempWord & 0x000000FF);
         break;
      case 0x1:
         *pucData = (unsigned char) ((ulTempWord & 0x0000FF00) >> 8);
         break;
      case 0x2:
         *pucData = (unsigned char) ((ulTempWord & 0x00FF0000) >> 16);
         break;
      case 0x3:
         *pucData = (unsigned char) ((ulTempWord & 0xFF000000) >> 24);
         break;
      }
   
   return ErrRet;
}

/****************************************************************************
    Function: ML_ReadFromTargetMemory
    Engineer: Suraj S
       Input: unsigned long  ulSource - memory address to read from
              void *pulDest -  pointer to read data
              unsigned long  *pulBytes - number of bytes to read 
              RDI_AccessType tyType - Access Type
      Output: TyError - Mid Layer error code
 Description: Read processor memory
Date          Initials      Description
18-Jul-2007      SJ         Initial
19-Mar-2008      JR         Added support for performance improvement
****************************************************************************/
TyError ML_ReadFromTargetMemory(unsigned long  ulSource,
                                void           *pulDest,
                                unsigned long  *pulBytes,
                                RDI_AccessType tyType)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
  
   unsigned long i;
   unsigned char *pucLocalDest;
   unsigned char *pucTempPointer;
   unsigned long *pulLocalDest;
   unsigned long *pulTempPointer;

   unsigned long ulByteReadWord   = 0x0;
   unsigned long ulStartBytes     = 0x0;
   unsigned long ulEndBytes       = 0x0;
   unsigned long ulBlocks         = 0x0;
   unsigned long ulWords          = 0x0;
   unsigned long ulByteNo         = 0x0;
   unsigned long ulSourceBackup;
   
   ulSourceBackup = ulSource;

   //Check the requested access type
   switch (tyType)
      {
      case RDIAccess_Data   :
      case RDIAccess_Code   :
      case RDIAccess_Code32 :
      case RDIAccess_Data32 :
      default:

         // We can use all access types.
         
         // Check how many bytes do we read, until we get to a word-aligned address
         if ((ulSourceBackup & 0x3) != 0)
            ulStartBytes = 4 - (ulSourceBackup & 0x3);
         else 
            ulStartBytes = 0;
         
         if (ulStartBytes > *pulBytes)
            ulStartBytes = *pulBytes;
         
         if  (*pulBytes < 4)
            ulStartBytes = *pulBytes;
         
         ulBlocks   = ( *pulBytes - ulStartBytes) / BLOCKSIZE;
         
         //If we are trying to read from this location do not use Block reads..
         if(tyProcessorConfig.bARM7 && tyProcessorConfig.bSynthesisedCore)
            {
            if ((ulSource >= 0xE0000000)  && (ulSource + *pulBytes  <= 0xF0000000))
               ulBlocks = 0x0;
            }
         
         ulWords    = ((*pulBytes - ulStartBytes) - (ulBlocks * BLOCKSIZE)) / 4;
         ulEndBytes = ((*pulBytes - ulStartBytes) - (ulBlocks * BLOCKSIZE)) - (ulWords * 4);
      

         break;
      
      case RDIAccess_Data16 :
      case RDIAccess_Data64 :
      case RDIAccess_Code16 :
      case RDIAccess_Code64 :
      //case RDIAccess_Code32 :
      //case RDIAccess_Data32 :
      case RDIAccess_Data8  :
      case RDIAccess_Code8  :

         //Dont use Block Read
         // Check how many bytes do we read, until we get to a word-aligned address
         if ((ulSourceBackup & 0x3) != 0)
            ulStartBytes = 4 - (ulSourceBackup & 0x3);
         else 
            ulStartBytes = 0;
         
         if (ulStartBytes > *pulBytes)
            ulStartBytes = *pulBytes;
         
         if  (*pulBytes < 4)
            ulStartBytes = *pulBytes;
         
         //If we are trying to read from this location do not use Block reads..
         if(tyProcessorConfig.bARM7 && tyProcessorConfig.bSynthesisedCore)
            {
            if ((ulSource >= 0xE0000000)  && (ulSource + *pulBytes  <= 0xF0000000))
               ulBlocks = 0x0;
            }
         
         ulWords    = (*pulBytes - ulStartBytes) / 4;
         ulEndBytes = (*pulBytes - ulStartBytes) - (ulWords * 4);
         break;
      
      }
  
   
   pucLocalDest = (unsigned char *)pulDest;
   pulTempPointer = &ulByteReadWord;

   //Commented for Optimisation
   // These functions expects scan chain 1.
   /*ErrRet = ML_Select_Scan_Chain(1,1);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;*/

   for (i=0; i < ulStartBytes; i++)
      {
      ErrRet = ML_ReadByteUsingWordAccess (ulSourceBackup, 
                                           pucLocalDest);
      ulSourceBackup += 0x1;
      pucLocalDest++;
      }

   pulLocalDest = (unsigned long *) pucLocalDest;

   if(tyProcessorConfig.bCortexM3)
   {
	   ulWords = ulWords + ulBlocks * 14;
	   ulBlocks = 0;
   }

      if (ulBlocks > 0)
         {
         // Read Multiple Block/s
         ErrRet = ML_ReadMultipleBlocks(ulSourceBackup, pulLocalDest, ulBlocks);
         if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;

         ulSourceBackup += (BLOCKSIZE * ulBlocks);
         pulLocalDest   += ((BLOCKSIZE * ulBlocks)/4);
         ulBlocks = 0x0;
         }

	  if (ulWords > 0)
	  {
		  pucLocalDest = (unsigned char *)pulLocalDest;
		  
		  if(tyProcessorConfig.bCortexM3)
		  {
			  unsigned long ulWordTemp = ulWords;
			  unsigned long ulNumWordRead;
			  for(ulNumWordRead = 0; ulNumWordRead < ulWords;)
			  {
				  if((ulWordTemp * 4) > MAX_WRITE_BOUNDARY)
				  {
					  ulWordTemp = 0x400;
				  }
				  else
				  {
					  ulWordTemp = ulWords - ulNumWordRead;
				  }
				  unsigned long ulLocalAddressTemp = (ulSourceBackup & 0xFFF) + (ulWordTemp * 4);
				  
				  if(ulLocalAddressTemp > MAX_WRITE_BOUNDARY)
				  {
					  unsigned long ultemp;
					  ultemp = ulLocalAddressTemp - MAX_WRITE_BOUNDARY;
					  ulWordTemp = ulWordTemp - (ultemp /4);
				  }
				  
				  
				  ErrRet = ML_ReadMultipleWords(ulSourceBackup, pulLocalDest, ulWordTemp);
				  if(ErrRet != ML_ERROR_NO_ERROR)
				  {
					  return ErrRet;
				  }
				  
				  ulNumWordRead = ulNumWordRead + ulWordTemp;
				  ulSourceBackup = ulSourceBackup + (ulWordTemp * 4);
				  
				  pulLocalDest = pulLocalDest + (ulWordTemp);
			  }
		  }
		  else
		  {
			  // Read Multiple words
			  ErrRet = ML_ReadMultipleWords(ulSourceBackup, pulLocalDest, ulWords);
			  if(ErrRet != ML_ERROR_NO_ERROR)
			  {
				  return ErrRet;
			  }
			  
			  ulSourceBackup += (ulWords * 0x4);
			  pulLocalDest   += ulWords;
		  }
		  ulWords = 0x0;
	  }


   pucLocalDest = (unsigned char *)pulLocalDest;

   
   // Read byte/s
   for (i=0; i < ulEndBytes; i++)
      {
      // Read a word aligned word.
      ErrRet = ML_ReadWord((ulSourceBackup & WORD_ALIGNED_MASK), pulTempPointer);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
         
      pucTempPointer = (unsigned char *) pulTempPointer;

      for (i=0; i < ulEndBytes; i++)

         {
         ulByteNo = (ulSourceBackup %4);
         *pucLocalDest = *(pucTempPointer+ulByteNo); 
         ulSourceBackup ++;
         pucLocalDest++;
         }
      }
    
   return ErrRet;
}

/****************************************************************************
    Function: ML_WriteByte
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long ulData - Data to write
      Output: TyError - Mid Layer error code
 Description: Write a single byte to processor memory
Date          Initials      Description
10-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_WriteByte (unsigned long ulAddress, unsigned long ulData)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}

	if (bUSBDevice)
	{
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			ErrRet = ML_WriteByteUsingWordAccess(ulAddress,ulData);
		}
		else
		{
			ErrRet = DL_OPXD_Arm_WriteByte(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, (unsigned char *)&ulData);
		}
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	  }
	else
	  return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	return ErrRet;
}

/****************************************************************************
    Function: ML_WriteWord
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long ulData - Data to write
      Output: TyError - Mid Layer error code
 Description: Write a single word to processor memory
Date           Initials     Description
11-Jul-2007      SJ         Initial
***************************************************************************/
TyError ML_WriteWord (unsigned long ulAddress, unsigned long ulData)

{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}

	if (bUSBDevice)
	{
		ErrRet = DL_OPXD_Arm_WriteWord(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, &ulData);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	return ErrRet;
}

/****************************************************************************
    Function: ML_WriteMultipleWords
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long *pulData - Pointer to data to write
              unsigned long ulWords - Number of words to write
      Output: TyError - Mid Layer error code
 Description: Write multiple words to processor memory
Date           Initials     Description
11-Jul-2007      SJ         Initial
***************************************************************************/
TyError ML_WriteMultipleWords (unsigned long ulAddress, unsigned long *pulData, unsigned long ulWords)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}
   
	if (bUSBDevice)
	{
		ErrRet = DL_OPXD_Arm_WriteMultipleWords(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, pulData, ulWords);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	return ErrRet;
}

/****************************************************************************
    Function: ML_WriteMultipleBlocks
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long *pulData - Pointer to data to write
              unsigned long ulBlocks - Number of blocks to write
      Output: TyError - Mid Layer error code
 Description: Write multiple blocks of data to processor memory
Date           Initials     Description
11-Jul-2007      SJ         Initial
***************************************************************************/
TyError ML_WriteMultipleBlocks (unsigned long ulAddress, unsigned long *pulData, unsigned long ulBlocks)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}
   
	if (bUSBDevice)
	{
		ErrRet = DL_OPXD_Arm_WriteMultipleBlocks(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulAddress, pulData, ulBlocks);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		{
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
		}
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_WriteByteUsingWordAccess
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to write to
              unsigned long ulData - Data to write
      Output: TyError - Mid Layer error code
 Description: Write a single byte to processor memory using word access.
Date           Initials     Description
11-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_WriteByteUsingWordAccess (unsigned long ulAddress, unsigned long ulData)
               
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   unsigned long ulTempWord = 0x0;

   //Read word from word aligned address
   ErrRet =  ML_ReadWord ((ulAddress & 0xFFFFFFFC), &ulTempWord);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   switch (ulAddress & 0x3)
      {
      default:
         // Flow through...
      case 0x0:
         ulTempWord = (ulTempWord & 0xFFFFFF00);
         ulTempWord += ulData;
         break;
      case 0x1:
         ulTempWord = (ulTempWord & 0xFFFF00FF);
         ulTempWord += (ulData << 8);
         break;
      case 0x2:
         ulTempWord = (ulTempWord & 0xFF00FFFF);
         ulTempWord += (ulData << 16);
         break;
      case 0x3:
         ulTempWord = (ulTempWord & 0x00FFFFFF);
         ulTempWord += (ulData << 24);
         break;
      }

   ErrRet = ML_WriteWord(ulAddress & 0xFFFFFFFC, 
                         ulTempWord);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_WriteToTargetMemory
     Engineer: Suraj S
        Input: void *pulSource - Source Address 
               unsigned long  ulDest - Target memory address
               unsigned long  *pulBytes - number of bytes to write 
               RDI_AccessType tyType - Type of access
       Output: TyError - Mid Layer error code
  Description: Write processor memory
Date           Initials     Description
11-Jul-2007      SJ         Initial
19-Mar-2008     JR          Added support for performance improvement
****************************************************************************/
TyError ML_WriteToTargetMemory(void           *pulSource,
                               unsigned long  ulDest,
                               unsigned long  *pulBytes,
                               RDI_AccessType tyType)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   unsigned long i;
   unsigned char *pucSourceBackup;
   unsigned char *pucSourceAllocated;
   unsigned long ulStartBytes                = 0x0;
   unsigned long ulStartBytesUsingWordAccess = 0x0;
   unsigned long ulEndBytes                  = 0x0;
   unsigned long ulEndBytesUsingWordAccess   = 0x0;
   unsigned long ulBlocks                    = 0x0;
   unsigned long ulWords                     = 0x0;
   unsigned long ulLocalAddress              = ulDest;
 
      // Allocate a section of memory and copy the contents of the *pulSource into it.
      pucSourceAllocated = (unsigned char *)malloc(*pulBytes);
      pucSourceBackup = pucSourceAllocated;
      memcpy(pucSourceBackup, pulSource, *pulBytes);

      switch (tyType)
         {
         case RDIAccess_Data   :
         case RDIAccess_Code   :
         case RDIAccess_Code32 :
         case RDIAccess_Data32 :
         default:

            // We can use all access types.
         
            // Check how many bytes do we write, until we get to a word-aligned address
            if ((ulLocalAddress & 0x3) != 0)
               ulStartBytes = 4 - (ulDest & 0x3);
            else 
               ulStartBytes = 0;
         
            if (ulStartBytes > *pulBytes)
               ulStartBytes = *pulBytes;
         
            if  (*pulBytes < 4)
               ulStartBytes = *pulBytes;
         
            ulBlocks      = ( *pulBytes - ulStartBytes) / BLOCKSIZE;
            ulWords       = ((*pulBytes - ulStartBytes) - (ulBlocks * BLOCKSIZE)) / 4;
            ulEndBytes    = ((*pulBytes - ulStartBytes) - (ulBlocks * BLOCKSIZE)) - (ulWords * 4);
         
           break;
     
        case RDIAccess_Data16 :
        case RDIAccess_Data64 :
        case RDIAccess_Code16 :
        case RDIAccess_Code64 :
        //case RDIAccess_Code32 :
        //case RDIAccess_Data32 :

           //Don't use Block Write
           ulBlocks = 0x0;

           // Check how many bytes do we write, until we get to a word-aligned address
           if ((ulLocalAddress & 0x3) != 0)
              ulStartBytesUsingWordAccess = 4 - (ulLocalAddress & 0x3);
           else 
              ulStartBytesUsingWordAccess = 0;
        
           if (ulStartBytesUsingWordAccess > *pulBytes)
              ulStartBytesUsingWordAccess = *pulBytes;
        
           if  (*pulBytes < 4)
              ulStartBytesUsingWordAccess = *pulBytes;
        
           ulWords                   = (*pulBytes - ulStartBytesUsingWordAccess) / 4;
           ulEndBytesUsingWordAccess = (*pulBytes - ulStartBytesUsingWordAccess) - (ulWords * 4);
           break;


         case RDIAccess_Data8  :
         case RDIAccess_Code8  :
            //Use byte access only.
            ulStartBytes = *pulBytes;
            break;
         }

      //Commented for Optimization
      // These functions expects scan chain 1.
      /*ErrRet = ML_Select_Scan_Chain(1,1); 
      if(ErrRet != ML_ERROR_NO_ERROR)
      {
         free(pucSourceAllocated);
         return ErrRet;
      }*/

      // Write start bytes first
      for (i=0; i < ulStartBytes; i++)
      {
         ErrRet = ML_WriteByte(ulLocalAddress, 
                               *pucSourceBackup);
         if(ErrRet != ML_ERROR_NO_ERROR)
         {
            free(pucSourceAllocated);
            return ErrRet;
         }

         pucSourceBackup++;
         ulLocalAddress ++;
      }

      // Write start byte's using word access
      for (i=0; i < ulStartBytesUsingWordAccess; i++)
         {
            ErrRet = ML_WriteByteUsingWordAccess(ulLocalAddress, 
                                                 *pucSourceBackup);
            if(ErrRet != ML_ERROR_NO_ERROR)
            {
               free(pucSourceAllocated);
               return ErrRet;
            }
      
            pucSourceBackup++;
            ulLocalAddress ++;
         }

	 /* In case of Cortex-M3 If the Block write is not supported. */
	 if(tyProcessorConfig.bCortexM3)
		{
		ulWords = ulWords + ulBlocks * 14;
		ulBlocks = 0;
		}

      //Write multiple blocks of data if there
      if (ulBlocks > 0)
         {
            ErrRet = ML_WriteMultipleBlocks(ulLocalAddress, (unsigned long *)pucSourceBackup, ulBlocks);
            if(ErrRet != ML_ERROR_NO_ERROR)
            {
               free(pucSourceAllocated);
               return ErrRet;
            }
            ulGlobalScanChain = 0x2;
            pucSourceBackup += (BLOCKSIZE * ulBlocks);
            ulLocalAddress += (BLOCKSIZE * ulBlocks);
            ulBlocks = 0x0;
         }

      //Write multiple words of data if there
	  if (ulWords > 0)
	  {	
		  if(tyProcessorConfig.bCortexM3)
		  {
			  unsigned long ulWordTemp = ulWords;
			  unsigned long ulNumWordWrite;

			  /* Solved the memory download problem in the Cortex-M3 alpha release. */
			  for(ulNumWordWrite = 0; ulNumWordWrite < ulWords;)
			  {
				  if((ulWordTemp * 4) > MAX_WRITE_BOUNDARY)
				  {
					  ulWordTemp = 0x400;
				  }
				  else
				  {
					  ulWordTemp = ulWords - ulNumWordWrite;
				  }
				  unsigned long ulLocalAddressTemp = (ulLocalAddress & 0xFFF) + (ulWordTemp * 4);
				  
				  if(ulLocalAddressTemp > MAX_WRITE_BOUNDARY)
				  {
					  unsigned long ultemp;
					  ultemp = ulLocalAddressTemp - MAX_WRITE_BOUNDARY;
					  ulWordTemp = ulWordTemp - (ultemp /4);
				  }
				  else
				  {
					  ulWordTemp = ulWordTemp;
				  }
				  ErrRet = ML_WriteMultipleWords(ulLocalAddress, (unsigned long *)pucSourceBackup, ulWordTemp);
				  if(ErrRet != ML_ERROR_NO_ERROR)
				  {
					  free(pucSourceAllocated);
					  return ErrRet;
				  }
				  
				  ulNumWordWrite = ulNumWordWrite + ulWordTemp;
				  ulLocalAddress = ulLocalAddress + (ulWordTemp * 4);
				  
				  pucSourceBackup = pucSourceBackup + (ulWordTemp * 4);
			  }
		  }
		  else
		  {
			  ErrRet = ML_WriteMultipleWords(ulLocalAddress, (unsigned long *)pucSourceBackup, ulWords);
			  if(ErrRet != ML_ERROR_NO_ERROR)
			  {
				  free(pucSourceAllocated);
				  return ErrRet;
			  }
			  
			  pucSourceBackup += (ulWords * 0x4);
			  ulLocalAddress += (ulWords * 0x4); 
		  }
		  
		  ulWords = 0x0;
		  ulGlobalScanChain = 0x2;
         }

      // Write end byte's if any
      for (i=0; i < ulEndBytes; i++)
         {
            ErrRet = ML_WriteByte(ulLocalAddress, 
                                  *pucSourceBackup);
            if(ErrRet != ML_ERROR_NO_ERROR)
            {
               free(pucSourceAllocated);
               return ErrRet;
            }

            pucSourceBackup++;
            ulLocalAddress ++;
         }

      // Write end byte's using word access if any
      for (i=0; i < ulEndBytesUsingWordAccess; i++)
         {
            ErrRet = ML_WriteByteUsingWordAccess(ulLocalAddress, 
                                                 *pucSourceBackup);
            if(ErrRet != ML_ERROR_NO_ERROR)
            {
               free(pucSourceAllocated);
               return ErrRet;
            }

            pucSourceBackup++;
            ulLocalAddress ++;
         }

      free(pucSourceAllocated);

   return ML_ERROR_NO_ERROR;

}

/****************************************************************************
    Function: ML_Initialise_Debugger
    Engineer: Suraj S
       Input: double ulFreqKHz - The frequency value in kHz
      Output: TyError - Mid Layer error code   
 Description: Initialise Debugger Hardware
Date          Initials     Description
4-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_Initialise_Debugger (TyTargetConfig* ptyTargetConfig)//, TyCoreConfig *ptyCoreConfig)

{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;
	TyTmsSequence ptyTmsForScanIR, ptyTmsForScanDR, ptyTmsForScanIRandDR_IR, ptyTmsForScanIRandDR_DR;

	//Set the default TMS sequence for ScanIR and ScanDR
	ptyTmsForScanIR.ucPostTmsBits  = 0xD;
	ptyTmsForScanIR.ucPostTmsCount = 0x5;
	ptyTmsForScanIR.ucPreTmsBits   = 0x3;
	ptyTmsForScanIR.ucPreTmsCount  = 0x4;

	ptyTmsForScanIRandDR_IR = ptyTmsForScanIR;

	ptyTmsForScanDR.ucPostTmsBits  = 0xD;
	ptyTmsForScanDR.ucPostTmsCount = 0x5;
	ptyTmsForScanDR.ucPreTmsBits   = 0x1;
	ptyTmsForScanDR.ucPreTmsCount  = 0x3;

	ptyTmsForScanIRandDR_DR = ptyTmsForScanDR;
   
	if(tyProcessorConfig.bARM7)
	{
	if(!tyProcessorConfig.bSynthesisedCore)
	TyProcType = ARM7;
	else if(tyProcessorConfig.bSynthesisedCore)
	TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
	TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
	TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
	TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
	TyProcType = CORTEXA;
	}

	double dFrequenyKHz = (double)ptyTargetConfig->dJTAGFreq;
	double dFrequenyMHz = dFrequenyKHz/KILOHZ;

	ulGlobalScanChain = FORCE_SCANCHAIN_SELECT;
   
   if(bUSBDevice)
   {
      if (!tyTargetConfig.bAdaptiveClockingEnabled)
	      ErrRet = DL_OPXD_JtagSetFrequency(tyCurrentDevice, dFrequenyMHz, NULL, NULL);
      else if(tyTargetConfig.bAdaptiveClockingEnabled)
         ErrRet = DL_OPXD_JtagSetAdaptiveClock(tyCurrentDevice, 1, 1);

      if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
		  if (TRUE == ptyTargetConfig->bTICore)
		  {
			  ErrRet = DL_OPXD_JtagSetTMS(tyCurrentDevice, NULL, NULL, NULL, NULL);   // set default TMS sequence			  
		  }
		  else
		  {
			  ErrRet = DL_OPXD_JtagSetTMS(tyCurrentDevice, &ptyTmsForScanIR, &ptyTmsForScanDR, 
                                     &ptyTmsForScanIRandDR_IR, &ptyTmsForScanIRandDR_DR);   // set default TMS sequence
		  }
		 
      
      if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      {
         if (ptyTargetConfig->dTargetVoltage == 0.0)
            ErrRet = DL_OPXD_SetVtpaVoltage(tyCurrentDevice, 0.0, TRUE, TRUE);
         else
            ErrRet = DL_OPXD_SetVtpaVoltage(tyCurrentDevice, ptyTargetConfig->dTargetVoltage, FALSE, TRUE);             // set tracking voltage and enable drivers to target
      }
      ML_Delay(500);//Assumes a delay is required here for proper functioning of Diskware

	  if (ptyTargetConfig->bCoresightCompliant)
	  {
		  (void)DL_OPXD_ResetDap(tyCurrentDevice);
	  }

      ErrRet = DL_OPXD_JtagConfigMulticore(tyCurrentDevice, 0, NULL);         // set no MC support
	  
	  /*	Do the initialization for TI devices - start	*/
	  if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) && 
		  (TRUE == ptyTargetConfig->bTICore))
	  {
		  if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
			  ErrRet = DL_OPXD_JtagResetTAP(tyCurrentDevice, 0);                      // in addition, ensure TAPs are in initial state

		  if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
				ErrRet = DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType);                      // in addition, ensure TAPs are in initial state

		  if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
			  ErrRet = ML_Config_ICEPICK (ptyTargetConfig->ulTISubPortNum);

		  /* Diskware function is moved to RDI. */
		  //ErrRet = DL_OPXD_InitIcepick ( tyCurrentDevice, 0, ptyTargetConfig->ulTISubPortNum );
	  }
	  /*	Do the initialization for TI devices - end	*/
	  
	  if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
		  ErrRet = DL_OPXD_JtagSetTMS(tyCurrentDevice, &ptyTmsForScanIR, &ptyTmsForScanDR, 
				  &ptyTmsForScanIRandDR_IR, &ptyTmsForScanIRandDR_DR);   // set default TMS sequence
		  

      if(ErrRet == DRVOPXD_ERROR_NO_ERROR)
         {  
         if (tyTargetConfig.ulSelectedNoOfCores>1)
            ErrRet = DL_OPXD_JtagConfigMulticore(tyCurrentDevice, tyTargetConfig.ulSelectedNoOfCores, ptyCoreConfig);         // set no MC support
         else
            {
            ErrRet=DL_OPXD_Arm_GetMultiCoreInfo(tyCurrentDevice, &tyTargetConfig.ulSelectedNoOfCores, pulIRLengthArray);        //for getting the information of no of cores in scanchain and corrosponding IR length
            if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
               {
               if (tyTargetConfig.ulSelectedNoOfCores>1)
                  {
                  for(unsigned int j=0; j< (tyTargetConfig.ulSelectedNoOfCores) ; j++)
                     {
                     pulBypassIRPattern[j] = BYPASS_PATTERN;
                     ptyCoreConfig[j].pulBypassIRPattern = &pulBypassIRPattern[j];
                     ptyCoreConfig[j].usDefaultDRLength  = 0x1;
                     ptyCoreConfig[j].usDefaultIRLength  = (unsigned short)pulIRLengthArray[j];
                     }
                  ErrRet = DL_OPXD_JtagConfigMulticore(tyCurrentDevice, tyTargetConfig.ulSelectedNoOfCores, ptyCoreConfig); 
                  }
               else
                  ErrRet = DL_OPXD_JtagConfigMulticore(tyCurrentDevice, 0, NULL); // set no MC support
               }
            }
         }	  
         
      if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) &&
		  (FALSE == ptyTargetConfig->bTICore))
         ErrRet = DL_OPXD_JtagResetTAP(tyCurrentDevice, 0);	// in addition, ensure TAPs are in initial state
      //ML_Delay(100);//Assumes a delay is required here for proper functioning of Diskware
      if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) &&
		  (FALSE == ptyTargetConfig->bTICore))
         ErrRet = DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType);                      // in addition, ensure TAPs are in initial state
      //ML_Delay(500);//Assumes a delay is required here for proper functioning of Diskware
   }

   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) && (TRUE == ptyTargetConfig->bCoresightCompliant))
	  {
	   ErrRet = DL_OPXD_Config_Coresight(tyCurrentDevice, 
		   ptyTargetConfig->ulDebugCoreNumber, 
		   ptyTargetConfig->ulCoresightBaseAddr,
		   ptyTargetConfig->ulDbgApIndex, 
		   ptyTargetConfig->ulMemApIndex, 
		   ptyTargetConfig->ulJtagApIndex,
		   ptyTargetConfig->ulUseMemApForMemAccess);
	  }
   return ErrRet;
}

/****************************************************************************
    Function: ML_SetupVectorCatch
    Engineer: Suraj S
       Input: Bit mask of the vectors to watch.
      Output: TyError - Mid Layer error code  
 Description: Set the breakpoints needed for vector catch.
Date          Initials     Description
20-Jul-2007      SJ         Initial
31-Jul-2008      RS         Added ARM11 support
05-Nov-2009		 JCK		Added Cortex-M3 support 
****************************************************************************/
TyError ML_SetupVectorCatch (unsigned long *pulVectorCatchConfig)

{  
	TyError         ErrRet                       = ML_ERROR_NO_ERROR;
	unsigned long   i                            = 0;
	unsigned long   ulLocalVectorCatchConfig     = 0;
	unsigned long   ulAddress                    = 0x0;
	unsigned long   ulBound                      = 0x0;
	RDI_PointHandle type                         = RDIPoint_EQ;

	ulLocalVectorCatchConfig = *pulVectorCatchConfig;

	if (tyProcessorConfig.bARM7)
	{
		//Clear all the vector catch BP's
		for (i=RESET_VECTOR_LOCATION; i<=ALL_VECTORS_SET_LOCATION;i++)
		{
			if (tyVectorCatchConfig[i].bValidBP)
			{
				ErrRet = ML_ClearBreakProc(tyVectorCatchConfig[i].ulBreakpointHandle);
				if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
			}
			tyVectorCatchConfig[i].bValidBP           = FALSE;
		}
    
        //Clear all the vector catch BP's
        for (i=RESET_VECTOR_LOCATION; i<=ALL_VECTORS_SET_LOCATION;i++)
        {
            if (tyVectorCatchConfig[i].bValidBP)
            {
                ErrRet = ML_ClearBreakProc(tyVectorCatchConfig[i].ulBreakpointHandle);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
            }
            tyVectorCatchConfig[i].bValidBP           = FALSE;
        }        

		// Set the required Breakpoints.
		for (i=RESET_VECTOR_LOCATION; i<=FIQ_VECTOR_LOCATION; i++)
		{
			tyVectorCatchConfig[i].ulAddress = ulVectorTableBaseAddress + (i*4);
			if ((ulLocalVectorCatchConfig & 0x1 ) == 0x1)
			{
				ErrRet = ML_SetBreakProc((unsigned long)tyVectorCatchConfig[i].ulAddress,
                                         (unsigned long)RDIPoint_EQ,
                                         (unsigned long)NULL,
                                         (RDI_PointHandle *)&tyVectorCatchConfig[i].ulBreakpointHandle);

				// If we can't set the breakpoint, break from the loop....
				if (ErrRet == ML_Error_CantSetPoint)
					break;

				if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;

				tyVectorCatchConfig[i].bValidBP = TRUE;
			}
			ulLocalVectorCatchConfig = ulLocalVectorCatchConfig >> 1;
		}
     
		// Were we not able to set all the breakpoints....
		if (i <= FIQ_VECTOR_LOCATION)
		{
			// One or more of the breakpoints could not be set.
         
			// Clear all the vector catch BP's
			for (i=RESET_VECTOR_LOCATION; i<=ALL_VECTORS_SET_LOCATION;i++)
			{
				if (tyVectorCatchConfig[i].bValidBP)
				{
					ErrRet = ML_ClearBreakProc(tyVectorCatchConfig[i].ulBreakpointHandle);
					if (ErrRet != ML_ERROR_NO_ERROR)
						return ErrRet;
				}
				tyVectorCatchConfig[i].bValidBP           = FALSE;
			}

			// Set the vector catch for all the vectors ie 0x0 to 0x1C.
			// We need to warn the user that we have done this...
			ulAddress            = ulVectorTableBaseAddress;
			ulBound              = CATCH_ALL_VECTORS_BOUND;
			type                 = RDIPoint_MASK;

			ErrRet = ML_SetBreakProc((unsigned long)ulAddress,
                                     (unsigned long)type,
                                     (unsigned long)ulBound,
                                     (RDI_PointHandle *)&tyVectorCatchConfig[ALL_VECTORS_SET_LOCATION].ulBreakpointHandle);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				*pulVectorCatchConfig = 0;
				ulGlobalVectorCatch = 0x0;
				return ErrRet;
			}
			else
			{
				tyVectorCatchConfig[ALL_VECTORS_SET_LOCATION].bValidBP = TRUE;
				*pulVectorCatchConfig = ALL_VECTORS_SET_MASK;
				ulGlobalVectorCatch = ALL_VECTORS_SET_MASK;
			}
        }
	}
	else if (tyProcessorConfig.bARM9)
	{
		ulGlobalVectorCatch = (ulLocalVectorCatchConfig & ARM9_VECTORCATCH_MASK);

		//If Semihosting is selected we need to keep the breakpoint already set.
		if (tySemiHostingConfig.ulSemiHostingType == STANDARD_SEMIHOSTING)
		{
			ulLocalVectorCatchConfig |= 0x4;
		}

		//Vector catch is set using the on chip vector catch resource.
		ErrRet = ML_WriteICEBreaker (VECTOR_CATCH, (ulLocalVectorCatchConfig & ARM9_VECTORCATCH_MASK));
		if (ErrRet!=ML_ERROR_NO_ERROR)
			return ErrRet;
	}

	else if (tyProcessorConfig.bARM11)
	{
		ulGlobalVectorCatch = (ulLocalVectorCatchConfig & ARM11_VECTORCATCH_MASK);

		//If Semihosting is selected we need to keep the breakpoint already set.
		if (tySemiHostingConfig.ulSemiHostingType == STANDARD_SEMIHOSTING)
		{
			ulLocalVectorCatchConfig |= 0x4;
		}

		//Vector catch is set using the on chip vector catch resource.
      ErrRet = ML_WriteCP14DebugRegARM11 (VECTOR_CATCH_REGISTER, (ulLocalVectorCatchConfig & ARM11_VECTORCATCH_MASK));
		if (ErrRet!=ML_ERROR_NO_ERROR)
			return ErrRet;
    }
    else if (tyProcessorConfig.bCortexM3)
    {
        unsigned long DemcrData;        

        if (VC_CORE_RESET == (ulLocalVectorCatchConfig & VC_CORE_RESET))
        {
			ErrRet =  ML_ReadWord (DEMCR_ADDR, &DemcrData);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				return ErrRet;
			}
			
            ulLocalVectorCatchConfig = ulLocalVectorCatchConfig & ~VC_CORE_RESET;

            ulLocalVectorCatchConfig = ulLocalVectorCatchConfig << 3;

            ulLocalVectorCatchConfig = ulLocalVectorCatchConfig | VC_CORE_RESET;

        }
        else
        {
            ulLocalVectorCatchConfig = ulLocalVectorCatchConfig << 3;
        }

        ulGlobalVectorCatch = (ulLocalVectorCatchConfig & CORTEXM3_VECTORCATCH_MASK);

        ErrRet =  ML_ReadWord (DEMCR_ADDR, &DemcrData);
        if (ErrRet != ML_ERROR_NO_ERROR)
        {
            return ErrRet;
        }

        DemcrData = DemcrData & ~CORTEXM3_VECTORCATCH_MASK;
        DemcrData = DemcrData | ulLocalVectorCatchConfig | DMCSR_TRCENA;

        ErrRet = ML_WriteWord(DEMCR_ADDR, DemcrData);
        if (ErrRet != ML_ERROR_NO_ERROR)
        {
            return ErrRet;
        }
    }
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		unsigned long ulVectorCatchData = 0;

		/* Vector Catch configuration */
		if (VECTOR_CATCH_RESET == (ulLocalVectorCatchConfig & VECTOR_CATCH_RESET))
		{
			ulVectorCatchData |= CORTEXA8_VCR_RESET; 	
		}

		if (VECTOR_CATCH_UNDEFINED == (ulLocalVectorCatchConfig & VECTOR_CATCH_UNDEFINED))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_UND_INST_SECURE | CORTEXA8_VCR_UND_INST_N_SECURE); 	
		}

		if (VECTOR_CATCH_SOFTWARE == (ulLocalVectorCatchConfig & VECTOR_CATCH_SOFTWARE))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_SVC_SECURE | CORTEXA8_VCR_SVC_N_SECURE); 	
		}

		if (VECTOR_CATCH_PREFETCH == (ulLocalVectorCatchConfig & VECTOR_CATCH_PREFETCH))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_PREFETCH_ABT_SECURE | 
				CORTEXA8_VCR_PREFETCH_ABT_SECURE_MON | CORTEXA8_VCR_PREFETCH_ABT_N_SECURE); 	
		}

		if (VECTOR_CATCH_DATAABORT == (ulLocalVectorCatchConfig & VECTOR_CATCH_DATAABORT))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_DATA_ABT_SECURE |
				CORTEXA8_VCR_DATA_ABT_SECURE_MON | CORTEXA8_VCR_DATA_ABT_N_SECURE); 	
		}

		if (VECTOR_CATCH_IRQ == (ulLocalVectorCatchConfig & VECTOR_CATCH_IRQ))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_IRQ_SECURE | CORTEXA8_VCR_IRQ_SECURE_MON |
				CORTEXA8_VCR_IRQ_N_SECURE); 	
		}

		if (VECTOR_CATCH_FIQ == (ulLocalVectorCatchConfig & VECTOR_CATCH_FIQ))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_FIQ_SECURE | CORTEXA8_VCR_FIQ_SECURE_MON | 
				CORTEXA8_VCR_FIQ_N_SECURE); 	
		}

		ulGlobalVectorCatch = ulVectorCatchData;

		//If Semihosting is selected we need to keep the breakpoint already set.
		if (tySemiHostingConfig.ulSemiHostingType == STANDARD_SEMIHOSTING)
		{
			ulVectorCatchData |= (CORTEXA8_VCR_SVC_SECURE | CORTEXA8_VCR_SVC_N_SECURE);
		}

		ErrRet = DL_OPXD_Config_VectorCatch(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, CORTEXA, ulVectorCatchData);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	}

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_Initialise_Semihosting
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code 
 Description: Loads the DCC handler to the target if the Semihosting via
              DCC is selected.
Date          Initials    Description
16-Aug-2007     SJ        Initial
****************************************************************************/
TyError  ML_Initialise_SemiHosting(void)
{
   unsigned long ulMemRead    = 0x0;
   unsigned long ulMemRead2   = 0x0;
   unsigned long ulNumBytes   = 0x4;
   unsigned long ulWriteValue = 0x0;
   TyError ErrRet;

   // If Semihosting type is DCC based..
   if (tySemiHostingConfig.ulSemiHostingType == DCC_SEMIHOSTING)
      {
      //Check the Semihosting vector table.
      ErrRet = ML_ReadProc(tySemiHostingConfig.ulSHVectorAddress, &ulMemRead, 
                           &ulNumBytes, 
                           RDIAccess_Data, 
                           TRUE);
      
      if(ErrRet != ML_ERROR_NO_ERROR)
         return ML_Error_CannotWriteDCCHandler;

      ErrRet = ML_ReadProc((tySemiHostingConfig.ulSHVectorAddress + 0x20), &ulMemRead2,
                           &ulNumBytes, 
                           RDIAccess_Data, 
                           TRUE);
      if(ErrRet != ML_ERROR_NO_ERROR)
         return ML_Error_CannotWriteDCCHandler;

      if (!((ulMemRead == SH_JUMP_INSTRUCTION) && (ulMemRead2 == tySemiHostingConfig.ulDCCServiceRoutineAddress)))
         {
         ulWriteValue = SH_JUMP_INSTRUCTION;
         ErrRet = ML_WriteProc(&ulWriteValue, tySemiHostingConfig.ulSHVectorAddress,
                               &ulNumBytes, 
                               RDIAccess_Data, 
                               FALSE,
                               FALSE);

         if(ErrRet != ML_ERROR_NO_ERROR)
            return ML_Error_CannotWriteDCCHandler;

         ErrRet = ML_ReadProc(tySemiHostingConfig.ulSHVectorAddress, &ulMemRead, 
                              &ulNumBytes, 
                              RDIAccess_Data, 
                              TRUE);

         if(ErrRet != ML_ERROR_NO_ERROR)
            return ML_Error_CannotWriteDCCHandler;

         if (ulMemRead != SH_JUMP_INSTRUCTION)
            return ML_Error_CannotWriteDCCHandler;

         ErrRet = ML_WriteProc(&tySemiHostingConfig.ulDCCServiceRoutineAddress, 
                                (tySemiHostingConfig.ulSHVectorAddress + 0x20), 
                                &ulNumBytes, 
                                RDIAccess_Data, 
                                FALSE,
                                FALSE);

         if(ErrRet != ML_ERROR_NO_ERROR)
            return ML_Error_CannotWriteDCCHandler;


         ErrRet = ML_ReadProc((tySemiHostingConfig.ulSHVectorAddress + 0x20), 
                               &ulMemRead, 
                               &ulNumBytes, 
                               RDIAccess_Data, 
                               TRUE);

         if(ErrRet != ML_ERROR_NO_ERROR)
            return ML_Error_CannotWriteDCCHandler;

         if (ulMemRead != tySemiHostingConfig.ulDCCServiceRoutineAddress)
            return ML_Error_CannotWriteDCCHandler;
         }
      }

   return RDIError_NoError;
}

/****************************************************************************
     Function: ML_SemiHostGo
     Engineer: Suraj S
        Input: unsigned long ulLinkRegisterValue - Link Register value
               unsigned long ulSPSRValue - SPSR value
       Output: TyError - Mid Layer error code
  Description: Restarts the processor after a semihost functions.
Date           Initials    Description
17-Aug-2007      SJ        Initial
18-Mar-2008		 JCK	   Added single stepping support
04-Dec-2009		 JCK	   Added support for Cortex-M3
****************************************************************************/
TyError ML_SemiHostGo (unsigned long ulLinkRegisterValue, unsigned long ulSPSRValue)
{
   TyError ErrRet;

   if (tyProcessorConfig.bCortexM3)
   {
	   /* BKPT AB instruction is detected. BKPT is Thumb16 instruction.
	   So increment the PC by 2. */
	   CortexMRegArray[REG_ARRAY_PC] = CortexMRegArray[REG_ARRAY_PC] + 2;
   }   
   else
   {
	   // Put the PC back to the entry point to SWI mode. and execute.
	   RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC]    = ulLinkRegisterValue;
	   RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_CPSR]  = (ulSPSRValue & ARM_MODE_MASK);
   }
   

   // Restart the processor - this is invisible to the user.
   //Handle is not used 
   if(bStep)
   {
        if (tyProcessorConfig.bCortexM3)
        {
            ErrRet = ML_CortexMWriteRegisters((unsigned long *)CortexMRegArray);
        }		
        else
        {
            ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
        }

      if(ErrRet != ML_ERROR_NO_ERROR)
      {
         bGlobalStartedExecution = FALSE;
         return ErrRet;
      }
   }
   else
   {
      ErrRet = ML_ExecuteProc();
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
   }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_CheckForSemiHosting
    Engineer: Suraj S
       Input: TYSemiHostStruct *tySemiHostStruct - pointer to a boolean - TRUE 
              if a Semihosting SWI was detected.
      Output: TyError - Mid Layer error code
 Description: Checks for the occurence of a Semi-hosting SWI and returns 
              the data to the debug host.
Date           Initials    Description
17-Aug-2007      SJ        Initial
****************************************************************************/
TyError ML_CheckForSemihosting(TYSemiHostStruct *tySemiHostStruct)
{
   TyError        ErrRet = ML_ERROR_NO_ERROR;
   unsigned long  ulSemiHostPointertoData = 0;
   unsigned long  ulNumberOfChars=0;
   unsigned long  ulTemp1=0;
   unsigned long  ulTemp2=0;
    unsigned long  ulSemihostDetect = FALSE;

    tySemiHostStruct->bSemiHostDetected = FALSE;                         
    tySemiHostStruct->arg1 = 0;
    tySemiHostStruct->arg2 = 0;
    tySemiHostStruct->arg3 = 0;
    tySemiHostStruct->arg4 = 0;

    if ((tyProcessorConfig.bARM7) ||
        (tyProcessorConfig.bARM9) ||
        (tyProcessorConfig.bARM11)||
		(tyProcessorConfig.bCortexA8)||
		(tyProcessorConfig.bCortexA9))
    {
        if ((RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC] == tySemiHostingConfig.ulSHVectorAddress)
            && (tySemiHostingConfig.ulSemiHostingType == STANDARD_SEMIHOSTING))
        {
            ulSemihostDetect = TRUE;
        }
        else
        {
            ulSemihostDetect = FALSE;
        }

    }
    else if (tyProcessorConfig.bCortexM3)
    {
        if ((BP_OPCODE == tySemiHostStruct->tyCauseOfBreak) && 
            (tySemiHostingConfig.ulSemiHostingType == STANDARD_SEMIHOSTING))
        {
            ulSemihostDetect = TRUE;
        }
        else
        {
            ulSemihostDetect = FALSE;
        }       
    }

    // Check the current PC to see if it is at the semihosting vector address.
    if (TRUE == ulSemihostDetect)
    {
        unsigned long ulStdSemiHostDetect = FALSE;

       if ((tyProcessorConfig.bARM7) ||
			(tyProcessorConfig.bARM9) ||
			(tyProcessorConfig.bARM11)||
			(tyProcessorConfig.bCortexA8)||
			(tyProcessorConfig.bCortexA9))
        {
            //Need to check the instruction that caused the SWI to ensure it is a semihosting SWI
            ulTemp1 = ML_ConvertModeMasktoRegArrayMask (RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_CPSR] & 0x1F);
            tySemiHostStruct->ulLinkRegisterValue = RegArray [ulTemp1] [REG_ARRAY_R14 ];
            tySemiHostStruct->ulSPSRValue         = RegArray [ulTemp1] [REG_ARRAY_SPSR];

            // Check if processor was in ARM or Thumb mode before the SWI was encountered.
            if (tySemiHostStruct->ulSPSRValue  & 0x20)
            {
                //Core was in Thumb mode
                bGlobalThumb = TRUE;
                ulTemp1 = tySemiHostStruct->ulLinkRegisterValue - THUMB_NO_OF_BYTES; 
                ulNumberOfChars = 0x2;
                ErrRet = ML_ReadProc(ulTemp1, &ulTemp2, &ulNumberOfChars, RDIAccess_Data, TRUE);
                if (!((ErrRet == ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_DataAbort)))
                    return ErrRet;
            }
            else
            {
                ulTemp1 = tySemiHostStruct->ulLinkRegisterValue - ARM_NO_OF_BYTES;   
                ulNumberOfChars = 0x4;
                ErrRet = ML_ReadProc(ulTemp1, &ulTemp2, &ulNumberOfChars, RDIAccess_Data, TRUE);
                if (!((ErrRet == ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_DataAbort)))
                    return ErrRet;
            }

            ulNumberOfChars = 0x4;

            if ((((ulTemp2 & LOWER_24BIT_MASK) == tySemiHostingConfig.ulARMSWIInstruction  ) && (!bGlobalThumb)) ||
                (((ulTemp2 & LOWER_8BIT_MASK) == tySemiHostingConfig.ulThumbSWIInstruction) && (bGlobalThumb)))
            {
                ulStdSemiHostDetect = TRUE;
            }
            else
            {
                ulStdSemiHostDetect = FALSE;
            }


        }
        else if (tyProcessorConfig.bCortexM3)
        {
            unsigned long ulPcValue;
            ulNumberOfChars = THUMB_NO_OF_BYTES;

			ulPcValue = CortexMRegArray[REG_ARRAY_PC];
            ErrRet = ML_ReadProc(ulPcValue, &ulTemp2, &ulNumberOfChars, RDIAccess_Data, TRUE);
            if (!((ErrRet == ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_DataAbort)))
                return ErrRet;

            if (BKPT_AB == ulTemp2)
            {
                ulStdSemiHostDetect = TRUE;
            }
            else
            {
                ulStdSemiHostDetect = FALSE;
            }
        }

        if (TRUE == ulStdSemiHostDetect)
        {
            // SWI driven semihosting.
            tySemiHostStruct->bSemiHostDetected = TRUE;
            
			if ((tyProcessorConfig.bARM7) ||
				(tyProcessorConfig.bARM9) ||
				(tyProcessorConfig.bARM11)||
				(tyProcessorConfig.bCortexA8)||
				(tyProcessorConfig.bCortexA9))
			{
				// Pointer to data
				ulSemiHostPointertoData = RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_R1];
				
				//Type of Semihost call.
				tySemiHostStruct->SemiHostingType = RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_R0];
			}
			else if (tyProcessorConfig.bCortexM3)
			{
				// Pointer to data
				ulSemiHostPointertoData = CortexMRegArray[REG_ARRAY_R1];

				//Type of Semihost call.
				tySemiHostStruct->SemiHostingType = CortexMRegArray[REG_ARRAY_R0];
			}            

            switch (tySemiHostStruct->SemiHostingType)
            {
            case  SH_SYS_OPEN         :
                //open a file on the debug host.
                ulNumberOfChars = 0x4;
                //Read address of pointer to file name into arg1
                ErrRet = ML_ReadProc(ulSemiHostPointertoData,
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read file mode into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+4),
                                     &tySemiHostStruct->arg2,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read length of file name into arg3
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+8),
                                     &tySemiHostStruct->arg3,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;

                //Add one character to the file name length so we pick up the Null termination.
                tySemiHostStruct->arg3 +=1;
                break;
            case  SH_SYS_CLOSE        :
                ulNumberOfChars = 0x4;
                //Read file handle into ulTemp1
                ErrRet = ML_ReadProc(ulSemiHostPointertoData,
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_WRITEC       :
                //Write a single character to the debug host
                ulNumberOfChars = 0x1;
                ErrRet = ML_ReadProc(ulSemiHostPointertoData,
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_WRITE0       :
                tySemiHostStruct->arg1 = ulSemiHostPointertoData;                  
                break;
            case  SH_SYS_WRITE        :
                //Write a given number of chars to the to the debug host
                ulNumberOfChars = 0x4;
                //Read file handle into 
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read the address of target data into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+4),
                                     &tySemiHostStruct->arg2,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read the number of characters to read from host into arg3
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+8),
                                     &tySemiHostStruct->arg3,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_READ         :
                //Read a number of characters from the debug host
                ulNumberOfChars = 0x4;
                //Read File handle into arg1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),
                                     &tySemiHostStruct->arg1 ,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read the address of target data into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0x4),
                                     &tySemiHostStruct->arg2,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read number of chars from host into arg3
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0x8),
                                     &tySemiHostStruct->arg3,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read file mode into arg4
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0xC),
                                     &tySemiHostStruct->arg4,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_READC        :
                //Nothing to do here.....
                break;
            case  SH_SYS_ISERROR      :
                tySemiHostStruct->bSemiHostDetected = FALSE;
                return ML_Error_SoftwareInterrupt; 
            case  SH_SYS_ISTTY        :
                //Get the file handle from the target.
                ulNumberOfChars = 0x4;
                //Read file handle into arg1
                ErrRet = ML_ReadProc(ulSemiHostPointertoData,
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;   
            case  SH_SYS_SEEK         :
                //Find a specified position in a file.
                ulNumberOfChars = 0x4;
                //Read file handle into arg1
                ErrRet = ML_ReadProc(ulSemiHostPointertoData,
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read file position into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+4),
                                     &tySemiHostStruct->arg2,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_FLEN         :
                //Return the length of a specified file.
                ulNumberOfChars = 0x4;
                //Read file handle into arg1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),
                                     &tySemiHostStruct->arg1,
                                     &ulNumberOfChars,
                                     RDIAccess_Data,
                                     FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_TMPNAM       :
                //Return the length of a specified file.
                ulNumberOfChars = 0x4;
                //Read file handle into arg1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),&tySemiHostStruct->arg1,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read the address of target data into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0x4),&tySemiHostStruct->arg2,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read number of chars from host into arg3
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0x8),&tySemiHostStruct->arg3,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_REMOVE       :
                //Deletes a specified file.
                ulNumberOfChars = 0x4;
                //Read address of file Name into arg1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),&tySemiHostStruct->arg1,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read length of file Name into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+4),&tySemiHostStruct->arg2,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;

                //Add one character to the file name length so we pick up the Null termination.
                tySemiHostStruct->arg2 +=1;
                break;
            case  SH_SYS_RENAME       :
                //Renames a specified file.
                ulNumberOfChars = 0x4;
                //Read address of file Name into arg1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),&tySemiHostStruct->arg1,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read length of file Name into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0x4),&tySemiHostStruct->arg2,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read address of new file Name into arg3
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0x8),&tySemiHostStruct->arg3,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Read length of new file Name into arg4
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+0xC),&tySemiHostStruct->arg4,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;

                //Add one character to the file name lengths so we pick up the Null termination.
                tySemiHostStruct->arg2 +=1;
                tySemiHostStruct->arg4 +=1;
                break;
            case  SH_SYS_CLOCK        :
                //Nothing to do here.....
                break;
            case  SH_SYS_TIME         :
                //Nothing to do here.....
                break;                  
            case  SH_SYS_SYSTEM       :
                tySemiHostStruct->bSemiHostDetected = FALSE;
                return ML_Error_SoftwareInterrupt; 
            case  SH_SYS_ERRNO        :
                //Nothing to do here.....
                break;                  
            case  SH_SYS_GET_CMDLINE  :
                ulNumberOfChars = 0x4;
                //Put the address of the buffer into arg1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),&tySemiHostStruct->arg1,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                //Put the length of the buffer into arg2
                ErrRet = ML_ReadProc((ulSemiHostPointertoData+4),&tySemiHostStruct->arg2,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_HEAPINFO     :
                ulNumberOfChars = 0x4;
                // Read Address of structure in target memory into ulTemp1
                ErrRet = ML_ReadProc((ulSemiHostPointertoData),&ulTemp1,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                ulTemp2 = 0x0;
                ErrRet = ML_WriteProc(&ulTemp2,(ulTemp1),&ulNumberOfChars, RDIAccess_Data, FALSE, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                ulTemp2 = ulTopOfMemory - 0x4000;
                ErrRet = ML_WriteProc(&ulTemp2,(ulTemp1+0x4),&ulNumberOfChars, RDIAccess_Data, FALSE, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                ulTemp2 = ulTopOfMemory;
                ErrRet = ML_WriteProc(&ulTemp2,(ulTemp1+0x8),&ulNumberOfChars, RDIAccess_Data, FALSE, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                ulTemp2 = ulTopOfMemory - 0x4000;
                ErrRet = ML_WriteProc(&ulTemp2,(ulTemp1+0xC),&ulNumberOfChars, RDIAccess_Data, FALSE, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;

                RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_R1] = ulTemp1; 
                break;

            case SH_ANGEL_SWIREASON_ENTERSVC :
                //Check the processor mode.

                if ((tySemiHostStruct->ulSPSRValue & REG_ARRAY_MODE_MASK) != USR_MODE)
                {
                    //Clear the mode parts
                    tySemiHostStruct->ulSPSRValue = (tySemiHostStruct->ulSPSRValue & ~REG_ARRAY_MODE_MASK);

                    //Set the mode to SVC.
                    tySemiHostStruct->ulSPSRValue = (tySemiHostStruct->ulSPSRValue | SVC_MODE);

                }

                //Disable interrupts
                tySemiHostStruct->ulSPSRValue = (tySemiHostStruct->ulSPSRValue | DISABLE_IRQ_FIQ);
                break;
            case SH_ANGEL_REASONEXCEPTION :
                tySemiHostStruct->arg1 = ulSemiHostPointertoData;
                break;
            case SH_ANGEL_LATE_STARTUP :
                //Nothing to do here.....
                break;
            case  SH_SYS_ELAPSED      :
                //Reads the number of ticks since execution started - always returns zero.
                ulNumberOfChars = 0x4;
                //Read address of file Name into arg1
                ErrRet = ML_ReadProc(ulSemiHostPointertoData,&tySemiHostStruct->arg1,
                                     &ulNumberOfChars, RDIAccess_Data, FALSE);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
                break;
            case  SH_SYS_TICKFREQ     :
                //Nothing to do here.....
                break;
            default :
                tySemiHostStruct->bSemiHostDetected = FALSE;
                return ML_Error_SoftwareInterrupt; 
            }
        }
        else
        {
            // This is a not a semihosting SWI - The vector should have been set to stop this.
            tySemiHostStruct->bSemiHostDetected = FALSE;
            return ML_Error_SoftwareInterrupt; 
        }
    }

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_LoadDCC_SH_Handler
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Loads the DCC Semihost Handler into the target
Date           Initials    Description
17-Aug-2007      SJ        Initial
30-Sep-2009		RTR			Added support for ARM11 DCC handler
****************************************************************************/
TyError ML_LoadDCC_SH_Handler(void)
{
   TyError ErrRet;
   unsigned int i;

   unsigned long pulDCC_SH_Handler[]=
   {
   0xE58F07C8,      /*  STR     R0, SemiHostType                               */
   0xE58F1788,      /*  STR     R1, SemiHostPointer                            */
   0xE59F0620,      /*  LDR     R0, =RegisterBackupArea                        */
   0xE88007FC,      /*  STMIA   R0, {R2-R10}                                   */
   0xE59F861C,      /*  LDR     R8, = SemiHostType                             */
   0xE5980000,      /*  LDR     R0, [R8]                                       */
   0xE588E004,      /*  STR     R14, [R8, #0x4]                                */
   0xE2888004,      /*  ADD     R8, R8, #0x4                                   */
   0xE3500001,      /*  CMP     R0, #0x01   ;SYS_OPEN                          */
   0x059FF60C,      /*  LDREQ   PC, = OPEN                                     */
   0xE3500002,      /*  CMP     R0, #0x02   ;SYS_CLOSE                         */
   0x059FF608,      /*  LDREQ   PC, = CLOSE                                    */
   0xE3500003,      /*  CMP     R0, #0x03   ;SYS_WRITEC                        */
   0x059FF604,      /*  LDREQ   PC, = WRITEC                                   */
   0xE3500004,      /*  CMP     R0, #0x04   ;SYS_WRITEO                        */
   0x059FF600,      /*  LDREQ   PC, = WRITEO                                   */
   0xE3500005,      /*  CMP     R0, #0x05   ;SYS_WRITE                         */
   0x059FF5FC,      /*  LDREQ   PC, = WRITE                                    */
   0xE3500006,      /*  CMP     R0, #0x06   ;SYS_READ                          */
   0x059FF5F8,      /*  LDREQ   PC, = READ                                     */
   0xE3500007,      /*  CMP     R0, #0x07   ;SYS_READC                         */
   0x059FF5F4,      /*  LDREQ   PC, = READC                                    */
   0xE3500008,      /*  CMP     R0, #0x08   ;SYS_ISERROR                       */
   0x059FF5F0,      /*  LDREQ   PC, = ISERROR                                  */
   0xE3500009,      /*  CMP     R0, #0x09   ;SYS_ISTTY                         */
   0x059FF5EC,      /*  LDREQ   PC, = ISTTY                                    */
   0xE350000A,      /*  CMP     R0, #0x0A   ;SYS_SEEK                          */
   0x059FF5E8,      /*  LDREQ   PC, = SEEK                                     */
   0xE350000C,      /*  CMP     R0, #0x0C   ;SYS_FLEN                          */
   0x059FF5E4,      /*  LDREQ   PC, = FLEN                                     */
   0xE350000D,      /*  CMP     R0, #0x0D   ;SYS_TMPNAM                        */
   0x059FF5E0,      /*  LDREQ   PC, = TMPNAM                                   */
   0xE350000E,      /*  CMP     R0, #0x0E   ;SYS_REMOVE                        */
   0x059FF5DC,      /*  LDREQ   PC, = REMOVE                                   */
   0xE350000F,      /*  CMP     R0, #0x0F   ;SYS_RENAME                        */
   0x059FF5D8,      /*  LDREQ   PC, = RENAME                                   */
   0xE3500010,      /*  CMP     R0, #0x10   ;SYS_CLOCK                         */
   0x059FF5D4,      /*  LDREQ   PC, = CLOCK                                    */
   0xE3500011,      /*  CMP     R0, #0x11   ;SYS_TIME                          */
   0x059FF5CC,      /*  LDREQ   PC, = TIME                                     */
   0xE3500012,      /*  CMP     R0, #0x12   ;SYS_SYSTEM                        */
   0x059FF5C8,      /*  LDREQ   PC, = SYSTEM                                   */
   0xE3500013,      /*  CMP     R0, #0x13   ;SYS_ERRNO                         */
   0x059FF5BC,      /*  LDREQ   PC, = ERRNO                                    */
   0xE3500015,      /*  CMP     R0, #0x15   ;SYS_GET_CMDLINE                   */
   0x059FF5B4,      /*  LDREQ   PC, = GET_CMDLINE                              */
   0xE3500016,      /*  CMP     R0, #0x16   ;SYS_HEAPINFO                      */
   0x059FF5AC,      /*  LDREQ   PC, = HEAPINFO                                 */
   0xE3500017,      /*  CMP     R0, #0x17   ;SYS_ENTERSVC                      */
   0x059FF5A4,      /*  LDREQ   PC, = ENTERSVC                                 */
   0xE3500018,      /*  CMP     R0, #0x18   ;SYS_REASON_EXCEPTION              */
   0x059FF59C,      /*  LDREQ   PC, = REASON_EXCEPTION                         */
   0xE3500030,      /*  CMP     R0, #0x30   ;SYS_ELAPSED                       */
   0x059FF594,      /*  LDREQ   PC, = ELAPSED                                  */
   0xE3500031,      /*  CMP     R0, #0x31   ;SYS_TICKFREQ                      */
   0x059FF58C,      /*  LDREQ   PC, = TICKFREQ                                 */
   0xE1A00000,      /*  NOP                                                    */
   0xE59FF5AC,      /*  LDR     PC, = FAILED                                   */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE2877004,      /*  ADD     R7, R7, #0x4                                   */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE5885004,      /*  STR     R5, [R8, #0x4]                                 */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE2888008,      /*  ADD     R8, R8, #0x8                                   */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE5D54000,      /*  LDRB    R4, [R5]                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE1540002,      /*  CMP     R4, R2                                         */
   0x159FF578,      /*  LDRNE   PC, = ReadData                                 */
   0xE2817008,      /*  ADD     R7, R1, #0x8                                   */
   0xE5971000,      /*  LDR     R1, [R7]                                       */
   0xE1A01121,      /*  MOV     R1, R1, LSR #2                                 */
   0xE2811004,      /*  ADD     R1, R1, #0x4                                   */
   0xE59FF540,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE1A04001,      /*  MOV     R4, R1                                         */
   0xE5944000,      /*  LDR     R4, [R4]                                       */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE3A01003,      /*  MOV     R1, #0x3                                       */
   0xE59FF52C,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5917000,      /*  LDR     R7, [R1]                                       */
   0xE5887004,      /*  STR     R7, [R8, #0x4]                                 */
   0xE3A01003,      /*  MOV     R1, #0x3                                       */
   0xE59FF51C,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE5885004,      /*  STR     R5, [R8, #0x4]                                 */
   0xE5975008,      /*  LDR     R5, [R7, #0x8]                                 */
   0xE5885008,      /*  STR     R5, [R8, #0x8]                                 */
   0xE2815004,      /*  ADD     R5, R1, #0x4                                   */
   0xE288800C,      /*  ADD     R8, R8, #0xC                                   */
   0xE5955000,      /*  LDR     R5, [R5]                                       */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE5D54000,      /*  LDRB    R4, [R5]                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE1540002,      /*  CMP     R4, R2                                         */
   0x159FF514,      /*  LDRNE   PC, = ReadData4                                */
   0xE2817008,      /*  ADD     R7, R1, #0x8                                   */
   0xE5971000,      /*  LDR     R1, [R7]                                       */
   0xE1A01121,      /*  MOV     R1, R1, LSR #2                                 */
   0xE2811005,      /*  ADD     R1, R1, #0x5                                   */
   0xE59FF4CC,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE5885004,      /*  STR     R5, [R8, #0x4]                                 */
   0xE5975008,      /*  LDR     R5, [R7, #0x8]                                 */
   0xE5885008,      /*  STR     R5, [R8, #0x8]                                 */
   0xE5914004,      /*  LDR     R4, [R1, #0x4]                                 */
   0xE288800C,      /*  ADD     R8, R8,  #0xC                                  */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE5D46000,      /*  LDRB    R6, [R4]                                       */
   0xE5C86000,      /*  STRB    R6, [R8]                                       */
   0xE2844001,      /*  ADD     R4, R4, #0x1                                   */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE2822001,      /*  ADD     R2, R2, #0x1                                   */
   0xE1550002,      /*  CMP     R5, R2                                         */
   0x159FF4CC,      /*  LDRNE   PC, = ReadData2                                */
   0xE1A01125,      /*  MOV     R1, R5, LSR #2                                 */
   0xE2811005,      /*  ADD     R1, R1, #0x5                                   */
   0xE59FF484,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE5885004,      /*  STR     R5, [R8, #0x4]                                 */
   0xE5975008,      /*  LDR     R5, [R7, #0x8]                                 */
   0xE5885008,      /*  STR     R5, [R8, #0x8]                                 */
   0xE59F64B0,      /*  LDR     R6, = NumberOfBytes                            */
   0xE5865000,      /*  STR     R5, [R6]                                       */
   0xE597500C,      /*  LDR     R5, [R7, #0xC]                                 */
   0xE588500C,      /*  STR     R5, [R8, #0xC]                                 */
   0xE3A01005,      /*  MOV     R1, #0x5                                       */
   0xE59FF458,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE59F84A0,      /*  LDR     R8, =DCCTransferBuffer                         */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE3A01002,      /*  MOV     R1, #0x2                                       */
   0xE59FF448,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5914000,      /*  LDR     R4, [R1]                                       */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE3A01003,      /*  MOV     R1, #0x3                                       */
   0xE59FF438,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5914000,      /*  LDR     R4, [R1]                                       */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE3A01003,      /*  MOV     R1, #0x3                                       */
   0xE59FF428,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5914000,      /*  LDR     R4, [R1]                                       */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE5914004,      /*  LDR     R4, [R1, #0x4]                                 */
   0xE5884008,      /*  STR     R4, [R8, #0x8]                                 */
   0xE3A01004,      /*  MOV     R1, #0x4                                       */
   0xE59FF410,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5914000,      /*  LDR     R4, [R1]                                       */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE3A01003,      /*  MOV     R1, #0x3                                       */
   0xE59FF400,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5914000,      /*  LDR     R4, [R1]                                       */
   0xE2813004,      /*  ADD     R3, R1, #0x4                                   */
   0xE5935000,      /*  LDR     R5, [R3]                                       */
   0xE5885004,      /*  STR     R5, [R8, #0x4]                                 */
   0xE2833004,      /*  ADD     R3, R3, #0x4                                   */
   0xE5933000,      /*  LDR     R3, [R3]                                       */
   0xE5883008,      /*  STR     R3, [R8, #0x8]                                 */
   0xE3A01004,      /*  MOV     R1, #0x4                                       */
   0xE59FF3DC,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE2888008,      /*  ADD     R8, R8, #0x8                                   */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE3A06000,      /*  MOV     R6, #0x0                                       */
   0xE5D54000,      /*  LDRB    R4, [R5]                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE2866001,      /*  ADD     R6, R6, #0x1                                   */
   0xE1540002,      /*  CMP     R4, R2                                         */
   0x159FF410,      /*  LDRNE   PC, = ReadData3                                */
   0xE1A01126,      /*  MOV     R1, R6, LSR #2                                 */
   0xE2811004,      /*  ADD     R1, R1, #0x4                                   */
   0xE59FF39C,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE1A07001,      /*  MOV     R7, R1                                         */
   0xE5975004,      /*  LDR     R5, [R7, #0x4]                                 */
   0xE5885004,      /*  STR     R5, [R8, #0x4]                                 */
   0xE597500C,      /*  LDR     R5, [R7, #0xC]                                 */
   0xE5885008,      /*  STR     R5, [R8, #0x8]                                 */
   0xE288800C,      /*  ADD     R8, R8, #0xC                                   */
   0xE5975000,      /*  LDR     R5, [R7]                                       */
   0xE5976004,      /*  LDR     R6, [R7, #0x4]                                 */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE5D54000,      /*  LDRB    R4, [R5]                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE2822001,      /*  ADD     R2, R2, #0x1                                   */
   0xE1560002,      /*  CMP     R6, R2                                         */
   0x159FF3CC,      /*  LDRNE   PC, = ReadData5                                */
   0xE3A04000,      /*  MOV     R4, #0x0                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE5975008,      /*  LDR     R5, [R7, #0x8]                                 */
   0xE597600C,      /*  LDR     R6, [R7, #0xC]                                 */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE5D54000,      /*  LDRB    R4, [R5]                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE2822001,      /*  ADD     R2, R2, #0x1                                   */
   0xE1560002,      /*  CMP     R6, R2                                         */
   0x159FF39C,      /*  LDRNE   PC, = ReadData6                                */
   0xE3A04000,      /*  MOV     R4, #0x0                                       */
   0xE5C84000,      /*  STRB    R4, [R8]                                       */
   0xE2888001,      /*  ADD     R8, R8, #0x1                                   */
   0xE5976004,      /*  LDR     R6, [R7,#0x4]                                  */
   0xE2866001,      /*  ADD     R6, R6, #0x1                                   */
   0xE1A02126,      /*  MOV     R2, R6, LSR #2                                 */
   0xE597600C,      /*  LDR     R6, [R7,#0xC]                                  */
   0xE2866001,      /*  ADD     R6, R6, #0x1                                   */
   0xE1A01126,      /*  MOV     R1, R6, LSR #2                                 */
   0xE0811002,      /*  ADD     R1, R1, R2                                     */
   0xE2811005,      /*  ADD     R1, R1, #0x5                                   */
   0xE59FF2F8,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE5914004,      /*  LDR     R4, [R1, #0x4]                                 */
   0xE5884004,      /*  STR     R4, [R8, #0x4]                                 */
   0xE3A01003,      /*  MOV     R1, #0x3                                       */
   0xE59FF2E8,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE3A01002,      /*  MOV     R1, #0x2                                       */
   0xE59FF2E0,      /*  LDR     PC, = Send_DCC_Data                            */
   0xE59F2328,      /*  LDR     r2, =DCCTransferBuffer                         */
   0xE1C210B2,      /*  STRH    R1, [R2,#0x2]                                  */
   0xEE104E10,      /*  MRC     p14,0,r4,c0,c0 ; Read control register         */
   0xE3140002,      /*  TST     r4, #2                                         */
   0x159FF350,      /*  LDRNE   PC, = pollout                                  */
   0xE4923004,      /*  LDR     r3,[r2],#4  ; Read word from DCCTransfer       */
   0xEE013E10,      /*  MCR     p14,0,r3,c1,c0 ; Write word from r3            */
   0xE2511001,      /*  SUBS    r1,r1,#1    ; Update counter                   */
   0x159FF340,      /*  LDRNE   PC, = pollout ; Loop if more words to be       */
   0xEE104E10,      /*  MRC     p14,0,r4,c0,c0 ; Read control register         */
   0xE3140001,      /*  TST     r4, #1                                         */
   0x059FF338,      /*  LDREQ   PC, =  pollin ; If R bit clear then loop       */
   0xEE111E10,      /*  MRC     p14,0,r1,c1,c0 ; read word into r1             */
   0xE59F22F4,      /*  LDR     r2,=DCCTransferBuffer                          */
   0xEE104E10,      /*  MRC     p14,0,r4,c0,c0 ; Read control register         */
   0xE3140001,      /*  TST     r4, #1                                         */
   0x059FF328,      /*  LDREQ   PC, =  pollin2 ; If R bit clear then loo       */
   0xEE113E10,      /*  MRC     p14,0,r3,c1,c0 ; read word into r3             */
   0xE5823000,      /*  STR     r3,[r2]     ; Store to memory and update       */
   0xE2822004,      /*  ADD     R2, R2, #0x4                                   */
   0xE2511001,      /*  SUBS    r1,r1,#1    ; Update counter                   */
   0xE3510000,      /*  CMP     r1, #0x0                                       */
   0x159FF310,      /*  LDRNE   PC, = pollin2                                  */
   0xE3500001,      /*  CMP     R0, #0x01   ;SYS_OPEN                          */
   0x059FF280,      /*  LDREQ   PC, = OPEN_HANDLER                             */
   0xE3500002,      /*  CMP     R0, #0x02   ;SYS_CLOSE                         */
   0x059FF27C,      /*  LDREQ   PC, = CLOSE_HANDLER                            */
   0xE3500005,      /*  CMP     R0, #0x05   ;SYS_WRITE                         */
   0x059FF274,      /*  LDREQ   PC, = WRITE_HANDLER                            */
   0xE3500006,      /*  CMP     R0, #0x06   ;SYS_READ                          */
   0x059FF270,      /*  LDREQ   PC, = READ_HANDLER                             */
   0xE3500008,      /*  CMP     R0, #0x08   ;SYS_ISERROR                       */
   0x059FF264,      /*  LDREQ   PC, = ISERROR_HANDLER                          */
   0xE3500009,      /*  CMP     R0, #0x09   ;SYS_ISTTY                         */
   0x059FF25C,      /*  LDREQ   PC, = ISTTY_HANDLER                            */
   0xE350000A,      /*  CMP     R0, #0x0A   ;SYS_SEEK                          */
   0x059FF254,      /*  LDREQ   PC, = SEEK_HANDLER                             */
   0xE350000C,      /*  CMP     R0, #0x0C   ;SYS_FLEN                          */
   0x059FF24C,      /*  LDREQ   PC, = FLEN_HANDLER                             */
   0xE350000D,      /*  CMP     R0, #0x0D   ;SYS_TMPNAM                        */
   0x059FF24C,      /*  LDREQ   PC, = TMPNAM_HANDLER                           */
   0xE350000E,      /*  CMP     R0, #0x0E   ;SYS_REMOVE                        */
   0x059FF23C,      /*  LDREQ   PC, = REMOVE_HANDLER                           */
   0xE350000F,      /*  CMP     R0, #0x0F   ;SYS_RENAME                        */
   0x059FF234,      /*  LDREQ   PC, = RENAME_HANDLER                           */
   0xE3500011,      /*  CMP     R0, #0x11   ;SYS_TIME                          */
   0x059FF22C,      /*  LDREQ   PC, = TIME_HANDLER                             */
   0xE3500010,      /*  CMP     R0, #0x10   ;SYS_CLOCK                         */
   0x059FF224,      /*  LDREQ   PC, = CLOCK_HANDLER                            */
   0xE3500013,      /*  CMP     R0, #0x13   ;SYS_ERRNO                         */
   0x059FF21C,      /*  LDREQ   PC, = ERRNO_HANDLER                            */
   0xE3500015,      /*  CMP     R0, #0x15   ;SYS_GET_CMDLINE                   */
   0x059FF220,      /*  LDREQ   PC, = GET_CMDLINE_HANDLER                      */
   0xE3500016,      /*  CMP     R0, #0x16   ;SYS_HEAPINFO                      */
   0x059FF21C,      /*  LDREQ   PC, = HEAPINFO_HANDLER                         */
   0xE3500017,      /*  CMP     R0, #0x17   ;SYS_HEAPINFO                      */
   0x059FF218,      /*  LDREQ   PC, = ENTERSVC_HANDLER                         */
   0xE3500018,      /*  CMP     R0, #0x18   ;SYS_REASON_EXCEPTION              */
   0x059FF21C,      /*  LDREQ   PC, = RETURN                                   */
   0xE3500030,      /*  CMP     R0, #0x30   ;SYS_ELAPSED                       */
   0x059FF1F4,      /*  LDREQ   PC, = ELAPSED_HANDLER                          */
   0xE3500031,      /*  CMP     R0, #0x31   ;SYS_TICKFREQ                      */
   0x059FF1EC,      /*  LDREQ   PC, = TICKFREQ_HANDLER                         */
   0xE3500003,      /*  CMP     R0, #0x03   ;SYS_WRITEC                        */
   0x059FF204,      /*  LDREQ   PC, = RETURN                                   */
   0xE3500004,      /*  CMP     R0, #0x04   ;SYS_WRITEO                        */
   0x059FF1FC,      /*  LDREQ   PC, = RETURN                                   */
   0xE3500007,      /*  CMP     R0, #0x07   ;SYS_READC                         */
   0x059FF1F4,      /*  LDREQ   PC, = RETURN                                   */
   0xE59F1214,      /*  LDR     R1, = DCCTransferBuffer                        */
   0xE5910000,      /*  LDR     R0, [R1]                                       */
   0xE59F12AC,      /*  LDR     R1, = ReturnValue                              */
   0xE5810000,      /*  STR     R0, [R1]                                       */
   0xE59FF1E0,      /*  LDR     PC, = RETURN                                   */
   0xE59F1200,      /*  LDR     R1, = DCCTransferBuffer                        */
   0xE5910000,      /*  LDR     R0, [R1]                                       */
   0xE59F2298,      /*  LDR     R2, = ReturnValue                              */
   0xE5820000,      /*  STR     R0, [R2]                                       */
   0xE59F3298,      /*  LDR     R3, =SemiHostPointer                           */
   0xE5933000,      /*  LDR     R3, [R3]                                       */
   0xE2833004,      /*  ADD     R3, R3, #0x4                                   */
   0xE5933000,      /*  LDR     R3, [R3]                                       */
   0xE59F61D8,      /*  LDR     R6, = NumberOfBytes                            */
   0xE5966000,      /*  LDR     R6, [R6]                                       */
   0xE3A05000,      /*  MOV     R5, #0x0                                       */
   0xE2811004,      /*  ADD     R1, R1, #0x4                                   */
   0xE5D14000,      /*  LDRB    R4, [R1]                                       */
   0xE5C34000,      /*  STRB    R4, [R3]                                       */
   0xE2833001,      /*  ADD     R3, R3, #0x1                                   */
   0xE2811001,      /*  ADD     R1, R1, #0x1                                   */
   0xE1550006,      /*  CMP     R5, R6                                         */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xB59FF264,      /*  LDRLT   PC, = READ_LOOP                                */
   0xE59FF190,      /*  LDR     PC, = RETURN                                   */
   0xE59F11B0,      /*  LDR     R1, = DCCTransferBuffer                        */
   0xE5910000,      /*  LDR     R0, [R1]                                       */
   0xE59F2248,      /*  LDR     R2, = ReturnValue                              */
   0xE5820000,      /*  STR     R0, [R2]                                       */
   0xE3A02000,      /*  MOV     R2, #0x0                                       */
   0xE2811004,      /*  ADD     R1, R1, #0x4                                   */
   0xE59F4240,      /*  LDR     R4, = SemiHostPointer                          */
   0xE5944000,      /*  LDR     R4, [R4]                                       */
   0xE5944000,      /*  LDR     R4, [R4]                                       */
   0xE5D13000,      /*  LDRB    R3, [R1]                                       */
   0xE5C43000,      /*  STRB    R3, [R4]                                       */
   0xE2811001,      /*  ADD     R1, R1, #0x1                                   */
   0xE2844001,      /*  ADD     R4, R4, #0x1                                   */
   0xE1530002,      /*  CMP     R3, R2                                         */
   0x159FF22C,      /*  LDRNE   PC, = Loop3                                    */
   0xE59FF150,      /*  LDR     PC, = RETURN                                   */
   0xE59F8218,      /*  LDR     R8, = SemiHostPointer                          */
   0xE5987000,      /*  LDR     R7, [R8]                                       */
   0xE5978000,      /*  LDR     R8, [R7]                                       */
   0xE59F0164,      /*  LDR     R0, = DCCTransferBuffer                        */
   0xE890003C,      /*  LDMIA   R0, {R2-R5}                                    */
   0xE888003C,      /*  STMIA   R8, {R2-R5}                                    */
   0xE3A00000,      /*  MOV     R0, #0x0                                       */
   0xE59F11F4,      /*  LDR     R1, = ReturnValue                              */
   0xE5810000,      /*  STR     R0, [R1]                                       */
   0xE59FF128,      /*  LDR     PC, = RETURN                                   */
   0xE14F2000,      /*  MRS     R2, SPSR    ;Store the mode we were in t       */
   0xE202201F,      /*  AND     R2, R2, #0x1F                                  */
   0xE3A000DF,      /*  LDR     R0, =SYS_MODE ;CPSR Value for USR mode         */
   0xE121F000,      /*  MSR     CPSR_c, R0  ;Change into USR mode              */
   0xE1A0100D,      /*  MOV     R1, R13     ;Store the stack pointer           */
   0xE3A000D3,      /*  LDR     R0, =SVC_MODE ;CPSR Value for SVC mode         */
   0xE121F000,      /*  MSR     CPSR_c, R0  ;Change SPSR into SVC mode         */
   0xE161F000,      /*  MSR     SPSR_c, R0  ;Change CPSR into SVC mode         */
   0xE1A0D001,      /*  MOV     R13, R1     ;Write the user mode stack p       */
   0xE3520010,      /*  CMP     R2, #0x10                                      */
   0x1A000001,      /*  BNE     ENTERSVC_RETURN ;if not in user return         */
   0xE3A00010,      /*  MOV     R0, #0x10   ;CPSR Value for USR mode           */
   0xE161F000,      /*  MSR     SPSR_c, R0  ;Change back into USR mode         */
   0xE3A00000,      /*  MOV     R0, #0x0                                       */
   0xE59F11B0,      /*  LDR     R1, = ReturnValue                              */
   0xE5810000,      /*  STR     R0, [R1]                                       */
   0xE59FF0E4,      /*  LDR     PC, = RETURN                                   */
   0xE59F1104,      /*  LDR     R1, = DCCTransferBuffer                        */
   0xE5910000,      /*  LDR     R0, [R1]                                       */
   0xE59F219C,      /*  LDR     R2, = ReturnValue                              */
   0xE5820000,      /*  STR     R0, [R2]                                       */
   0xE59F319C,      /*  LDR     R3, = SemiHostPointer                          */
   0xE5933000,      /*  LDR     R3, [R3]                                       */
   0xE5933000,      /*  LDR     R3, [R3]                                       */
   0xE3A05000,      /*  MOV     R5, #0x0                                       */
   0xE5916004,      /*  LDR     R6, [R1, #0x4]                                 */
   0xE2811008,      /*  ADD     R1, R1, #0x8                                   */
   0xE5D14000,      /*  LDRB    R4, [R1]                                       */
   0xE5C34000,      /*  STRB    R4, [R3]                                       */
   0xE2833001,      /*  ADD     R3, R3, #0x1                                   */
   0xE2811001,      /*  ADD     R1, R1, #0x1                                   */
   0xE1550006,      /*  CMP     R5, R6                                         */
   0xE2855001,      /*  ADD     R5, R5, #0x1                                   */
   0xB59FF188,      /*  LDRLT   PC, = GET_CMDLINE_LOOP                         */
   0xE59FF09C,      /*  LDR     PC, = RETURN                                   */
   0xE59F10BC,      /*  LDR     R1, = DCCTransferBuffer                        */
   0xE5911000,      /*  LDR     R1, [R1]                                       */
   0xE59F0154,      /*  LDR     R0, = ReturnValue                              */
   0xE5801000,      /*  STR     R1, [R0]                                       */
   0xE59FF088,      /*  LDR     PC, = RETURN                                   */
   0xE59F0014,      /*  LDR     R0, =RegisterBackupArea                        */
   0xE89007FC,      /*  LDMIA   R0, {R2-R10}                                   */
   0xE59F0148,      /*  LDR     R0, = SemiHostPointer                          */
   0xE5901000,      /*  LDR     R1, [R0]                                       */
   0xE59F0138,      /*  LDR     R0, = ReturnValue                              */
   0xE5900000,      /*  LDR     R0, [R0]                                       */
   0xE1B0F00E,      /*  MOVS    PC, R14                                        */
   0x000007A0,      /*  DCD     0x00000000                                     */
   0x000007D0,      /*  DCD     0x00000000                                     */
   0x000000E8,      /*  DCD     0x00000000                                     */
   0x00000134,      /*  DCD     0x00000000                                     */
   0x00000148,      /*  DCD     0x00000000                                     */
   0x00000158,      /*  DCD     0x00000000                                     */
   0x000001A8,      /*  DCD     0x00000000                                     */
   0x000001F0,      /*  DCD     0x00000000                                     */
   0x0000021C,      /*  DCD     0x00000000                                     */
   0x0000022C,      /*  DCD     0x00000000                                     */
   0x0000023C,      /*  DCD     0x00000000                                     */
   0x0000024C,      /*  DCD     0x00000000                                     */
   0x00000264,      /*  DCD     0x00000000                                     */
   0x00000274,      /*  DCD     0x00000000                                     */
   0x00000298,      /*  DCD     0x00000000                                     */
   0x000002D8,      /*  DCD     0x00000000                                     */
   0x0000038C,      /*  DCD     0x00000000                                     */
   0x0000037C,      /*  DCD     0x00000000                                     */
   0x00000394,      /*  DCD     0x00000000                                     */
   0x000004A8,      /*  DCD     0x00000000                                     */
   0x00000600,      /*  DCD     0x00000000                                     */
   0x000004BC,      /*  DCD     0x00000000                                     */
   0x0000050C,      /*  DCD     0x00000000                                     */
   0x000005B8,      /*  DCD     0x00000000                                     */
   0x0000054C,      /*  DCD     0x00000000                                     */
   0x00000574,      /*  DCD     0x00000000                                     */
   0x000000E0,      /*  DCD     0x00000000                                     */
   0x00000108,      /*  DCD     0x00000000                                     */
   0x00000614,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x0000017C,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x000001C8,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x0000079C,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x000007D0,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x000002B0,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x000002FC,      /*  DCD     0x00000000                                     */
   0x00000330,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x0000039C,      /*  DCD     0x00000000                                     */
   0x000003B8,      /*  DCD     0x00000000                                     */
   0x000003CC,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000798,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000794,      /*  DCD     0x00000000                                     */
   0x000004EC,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000530,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x000005E0,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000,      /*  DCD     0x00000000                                     */
   0x00000000       /*  DCD     0x00000000                                     */
   };

	/* For ARM11, the DCC communication is handled by the Co-processor C14 registers(DSCR, rDTR and wDTR) 
		Hence the corresponding opcodes have to be replaced for the handler*/

   if((tyProcessorConfig.bARM11) ||
	   (tyProcessorConfig.bCortexA8)||
	   (tyProcessorConfig.bCortexA9))
   {
		pulDCC_SH_Handler[231] = 0xEE104E11;	/*	MRC p14,0,R4,c0,c1,0 -> Read control register(DSCR)	*/
		pulDCC_SH_Handler[232] = 0xE3140202;	/*  TST     r4, #0x20000000                             */
		pulDCC_SH_Handler[235] = 0xEE003E15;	/*	MCR p14,0,R3,c0,c5,0 -> Write word from R3(wDTR)	*/
		pulDCC_SH_Handler[238] = 0xEE104E11;	/*	MRC p14,0,R4,c0,c1,0 -> Read control register(DSCR)	*/
		pulDCC_SH_Handler[239] = 0xE3140101;	/*  TST     r4, #0x40000000                             */
		pulDCC_SH_Handler[241] = 0xEE101E15;	/*	MRC p14,0,R1,c0,c5,0 -> Read word into R1(rDTR)		*/
		pulDCC_SH_Handler[243] = 0xEE104E11;	/*	MRC p14,0,R4,c0,c1,0 -> Read control register(DSCR)	*/
		pulDCC_SH_Handler[244] = 0xE3140101;	/*  TST     r4, #0x40000000                             */
		pulDCC_SH_Handler[246] = 0xEE103E15;	/*	MRC p14,0,R3,c0,c5,0 -> Read word into R3(rDTR)		*/
   }

   
   unsigned long ulNumOfBytes = sizeof (pulDCC_SH_Handler);
   unsigned long pulReadBackDCC_SH_Handler[(sizeof(pulDCC_SH_Handler)/4)];

   //These addresses are relative to the handler entry point.
   for (i = 0x18C; i <= 0x21F; i++)
      {
      if (pulDCC_SH_Handler[i] != 0x0)
         pulDCC_SH_Handler[i] += tySemiHostingConfig.ulDCCServiceRoutineAddress;   
      }
   
   // If Semihosting type is DCC based..
   if (tySemiHostingConfig.ulSemiHostingType == DCC_SEMIHOSTING)
      {
      //Write DCC SemiHost handler to target memory.
      ErrRet =  ML_WriteProc(pulDCC_SH_Handler, 
                             tySemiHostingConfig.ulDCCServiceRoutineAddress ,
                             &ulNumOfBytes,
                             RDIAccess_Code,
                             FALSE,
                             FALSE);
      
      if(ErrRet != ML_ERROR_NO_ERROR)
         return ML_Error_CannotWriteDCCHandler;
      
      //Read the  DCC SemiHost handler from target memory.
      ErrRet = ML_ReadProc(tySemiHostingConfig.ulDCCServiceRoutineAddress, 
                           pulReadBackDCC_SH_Handler, &ulNumOfBytes,
                           RDIAccess_Code, TRUE);
      
      if(ErrRet != ML_ERROR_NO_ERROR)
         return ML_Error_CannotWriteDCCHandler;
      
      //Compare the DCC Handler.
      if (memcmp ((const void *) pulDCC_SH_Handler, (const void *) pulReadBackDCC_SH_Handler, (int)ulNumOfBytes) != 0)
         return ML_Error_CannotWriteDCCHandler;
      }
    
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetSemihostingBP
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Sets the semihosting breakpoint. ARM9 uses a BP if address is not 0x8. 
Date          Initials    Description
20-Aug-2007     SJ        Initial
****************************************************************************/
TyError ML_SetSemihostingBP (void)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;

   // We should never come in here otherwise....
//TODO:   ASSERT(tySemiHostingConfig.ulSemiHostingType == STANDARD_SEMIHOSTING);

   if ((tyProcessorConfig.bARM9) ||
	   (tyProcessorConfig.bARM11) ||
	   (tyProcessorConfig.bCortexA8)||
	   (tyProcessorConfig.bCortexA9))
     {
			if(tyProcessorConfig.bARM9)
			{
				// The ARM 9 has separate vector catch registers so we don't need to set 
				// a normal breakpoint.
				ErrRet = ML_WriteICEBreaker (VECTOR_CATCH, (ulGlobalVectorCatch | SOFTWARE_INTERRUPT_VECTOR_MASK));
				if (ErrRet!=ML_ERROR_NO_ERROR)
				return ErrRet;
			}
			else if(tyProcessorConfig.bARM11)
			{
				// The ARM 11 has separate vector catch registers so we don't need to set 
				// a normal breakpoint.
				ErrRet = ML_WriteCP14DebugRegARM11(VECTOR_CATCH_REGISTER, (ulGlobalVectorCatch | SOFTWARE_INTERRUPT_VECTOR_MASK));
				if (ErrRet!=ML_ERROR_NO_ERROR)
				return ErrRet;
			}
			else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
			{
				unsigned long ulVectorCatchData = ulGlobalVectorCatch | (CORTEXA8_VCR_SVC_SECURE | CORTEXA8_VCR_SVC_N_SECURE);
				ErrRet = DL_OPXD_Config_VectorCatch(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, CORTEXA, ulVectorCatchData);				
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
			}
     }
   else
     {
     if (!ML_BreakpointAlreadySet(tySemiHostingConfig.ulSHVectorAddress, NULL))
        {
        // If there is not already a breakpoint set we need to check if it has been
        // set as part of the vector catch.
        if ((!tyVectorCatchConfig[ALL_VECTORS_SET_LOCATION].bValidBP) && 
            (!tyVectorCatchConfig[SEMIHOSTING_VECTOR_LOCATION].bValidBP))
           {
           ErrRet = ML_SetBreakProc( tySemiHostingConfig.ulSHVectorAddress,0x0,0x0,
                                 &tySemiHostingConfig.ulBreakpointHandle);
           if (ErrRet != ML_Error_CantSetPoint)
              tySemiHostingConfig.bBreakpointSet = TRUE;
           return ErrRet;
           }
        
        }
     }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ClearSemihostingBP
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Clears the semihosting breakpoint
Date          Initials    Description
20-Aug-2007     SJ        Initial
22-Apr-2010		JCK		  Cortex-A8 Support Added.
23-Apr-2010		JCK		  ARM11 Support Added.
****************************************************************************/
TyError ML_ClearSemihostingBP (void)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;

	if ((tyProcessorConfig.bARM9) && 
		((SWI_LOWER_VECTOR_ADDR  == tySemiHostingConfig.ulSHVectorAddress) || 
		 (SWI_HIGHER_VECTOR_ADDR == tySemiHostingConfig.ulSHVectorAddress)))
	{
		ErrRet = ML_WriteICEBreaker (VECTOR_CATCH, ulGlobalVectorCatch);
		if (ErrRet!=ML_ERROR_NO_ERROR)
			return ErrRet;
	}

	else if ((tyProcessorConfig.bARM11)  && 
		((SWI_LOWER_VECTOR_ADDR  == tySemiHostingConfig.ulSHVectorAddress) || 
		 (SWI_HIGHER_VECTOR_ADDR == tySemiHostingConfig.ulSHVectorAddress)))
	{
		//Vector catch is set using the on chip vector catch resource.
		ErrRet = ML_WriteCP14DebugRegARM11 (VECTOR_CATCH_REGISTER, (ulGlobalVectorCatch & ARM11_VECTORCATCH_MASK));
		if (ErrRet!=ML_ERROR_NO_ERROR)
			return ErrRet;
	}
	
	else if (((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9)) && 
		((SWI_LOWER_VECTOR_ADDR  == tySemiHostingConfig.ulSHVectorAddress) || 
		 (SWI_HIGHER_VECTOR_ADDR == tySemiHostingConfig.ulSHVectorAddress)))
	{
		unsigned long ulVectorCatchData = 0;

		/* Vector Catch configuration */
		if (VECTOR_CATCH_RESET == (ulGlobalVectorCatch & VECTOR_CATCH_RESET))
		{
			ulVectorCatchData |= CORTEXA8_VCR_RESET; 	
		}
		
		if (VECTOR_CATCH_UNDEFINED == (ulGlobalVectorCatch & VECTOR_CATCH_UNDEFINED))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_UND_INST_SECURE | CORTEXA8_VCR_UND_INST_N_SECURE); 	
		}
		
		if (VECTOR_CATCH_SOFTWARE == (ulGlobalVectorCatch & VECTOR_CATCH_SOFTWARE))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_SVC_SECURE | CORTEXA8_VCR_SVC_N_SECURE); 	
		}
		
		if (VECTOR_CATCH_PREFETCH == (ulGlobalVectorCatch & VECTOR_CATCH_PREFETCH))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_PREFETCH_ABT_SECURE | 
				CORTEXA8_VCR_PREFETCH_ABT_SECURE_MON | CORTEXA8_VCR_PREFETCH_ABT_N_SECURE); 	
		}
		
		if (VECTOR_CATCH_DATAABORT == (ulGlobalVectorCatch & VECTOR_CATCH_DATAABORT))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_DATA_ABT_SECURE |
				CORTEXA8_VCR_DATA_ABT_SECURE_MON | CORTEXA8_VCR_DATA_ABT_N_SECURE); 	
		}
		
		if (VECTOR_CATCH_IRQ == (ulGlobalVectorCatch & VECTOR_CATCH_IRQ))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_IRQ_SECURE | CORTEXA8_VCR_IRQ_SECURE_MON |
				CORTEXA8_VCR_IRQ_N_SECURE); 	
		}
		
		if (VECTOR_CATCH_FIQ == (ulGlobalVectorCatch & VECTOR_CATCH_FIQ))
		{
			ulVectorCatchData |= (CORTEXA8_VCR_FIQ_SECURE | CORTEXA8_VCR_FIQ_SECURE_MON | 
				CORTEXA8_VCR_FIQ_N_SECURE); 	
		}
		
		ErrRet = DL_OPXD_Config_VectorCatch(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, CORTEXA, ulVectorCatchData);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
		 
	}

	//If there was a breakpoint set clear it.
	if (tySemiHostingConfig.bBreakpointSet)
	{
		ErrRet = ML_ClearBreakProc(tySemiHostingConfig.ulBreakpointHandle);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ErrRet;
		tySemiHostingConfig.bBreakpointSet = FALSE;
	}

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_PointInquiryProc
    Engineer: Suraj S
       Input: unsigned long  *pulAddress - The requested address of query
              unsigned long ulType - watchpoint/breakpoint type
              unsigned long ulDataType - determines which access types to 
              watch/break for 
              unsigned long  *pulBound - This is a mask defined by RDI.h
      Output: TyError - Mid Layer error code
 Description: Not supported yet....
Date          Initials    Description
17-Aug-2007     SJ        Initial
****************************************************************************/
TyError ML_PointInquiryProc(unsigned long  *pulAddress,
                            unsigned long  ulType,
                            unsigned long  ulDataType,
                            unsigned long  *pulBound)
{
   //Keep Lint happy
   pulAddress = pulAddress; 
   ulType     = ulType;    
   ulDataType = ulDataType;
   pulBound   = pulBound;   

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetSafeNonVectorAddress
    Engineer: Suraj S
       Input: unsigned long ulSafeAddress - Address to place pc during system 
              speed access
      Output: TyError - Mid Layer error code
 Description: Sets the global variable SafeNonVectorAddress
              (PC is placed at this address during sys speed accesses)
              Also sets the safe non vector address in the core
Date          Initials    Description
20-Jul-2007      SJ        Initial
****************************************************************************/
TyError ML_SetSafeNonVectorAddress (unsigned long ulSafeAddress)

{
	TyError ErrRet;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}

	ulGlobalSafeNonVectorAddress = ulSafeAddress;

	if (bUSBDevice)
	{
		ErrRet = DL_OPXD_Arm_SetSafeNonVectorAddress(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulSafeAddress, bSetSafeNonVectorAddress);
		if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_ConvertModeMask
     Engineer: Suraj S
        Input: unsigned long ulMode - processor mode mask
       Output: static unsigned long - processor code for given mode
  Description: Converts the mode mask to the processor code. 
Date           Initials    Description
17-Aug-2007      SJ        Initial
****************************************************************************/
static unsigned long ML_ConvertModeMasktoRegArrayMask (unsigned long ulMode)
{
   switch (ulMode)
         {
         case FIQ_MODE  :
            return REG_ARRAY_FIQ_MODE;
         case IRQ_MODE  :
            return REG_ARRAY_IRQ_MODE;
         case SVC_MODE  :
            return REG_ARRAY_SVC_MODE;
         case ABORT_MODE  :
            return REG_ARRAY_ABORT_MODE;
         case UNDEF_MODE  :
            return REG_ARRAY_UNDEF_MODE;
         case USR_MODE  :
            return REG_ARRAY_USR_MODE;
         default        :
            return REG_ARRAY_USR_MODE;
         }
}

/****************************************************************************
     Function: ML_SetupProcType
     Engineer: Suraj S
        Input: Processor type
       Output: TyError - Mid Layer error code
  Description: Sets up processor type
Date           Initials    Description
4-Jul-2007       SJ        Initial
24-Mar-2008		 SJ		    Added 968 core support
31-Jul-2008      VK/RS         Added ARM11 support
02-Nov-2009		 JCK		Added Cortex-M3 support   
****************************************************************************/
TyError ML_SetupProcType(TXRXProcTypes tyProcType)
{
	//TODO:   TyError ErrRet;

	tyProcessorConfig.tyProcType = tyProcType;

	switch (tyProcType)
	{
	case ARM7DI    :
		tyProcessorConfig.bARM7             = TRUE;
		tyProcessorConfig.bARM9             = FALSE;
		tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = NO_COPROCESSOR;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = FALSE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = FALSE;
        break;

    case ARM7TDMI  :
        tyProcessorConfig.bARM7             = TRUE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;  
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = NO_COPROCESSOR;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM7TDMIS  :
        tyProcessorConfig.bARM7             = TRUE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;    
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = NO_COPROCESSOR;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = FALSE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM720T   :
        tyProcessorConfig.bARM7             = TRUE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE; 
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM720T_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM740T   :
        tyProcessorConfig.bARM7             = TRUE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;  
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM740T_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM9TDMI  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;    
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = NO_COPROCESSOR;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM920T  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;    
		tyProcessorConfig.bCortexA8		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM920T_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM922T  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;  
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM922T_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM926EJS  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;    
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM926EJS_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM940T  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM940T_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = FALSE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM946ES  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;  
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM946ES_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM966ES  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE; 
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM966ES_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM968ES  :
        tyProcessorConfig.bARM9             = TRUE;
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;  
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM968ES_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE;
        tyProcessorConfig.bDCC              = TRUE;
        break;

    case ARM1136JFS    :
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = TRUE;
        tyProcessorConfig.bCortexM3         = FALSE; 
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = ARM1136JFS_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE; //Doubt
        tyProcessorConfig.bDCC              = TRUE;
        break;
	case ARM1156T2S    :
		tyProcessorConfig.bARM7             = FALSE;
		tyProcessorConfig.bARM9             = FALSE;
		tyProcessorConfig.bARM11            = TRUE;
		tyProcessorConfig.bCortexM3         = FALSE;
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
		tyProcessorConfig.tyCoProcessorType = ARM1156T2S_CP;
		tyProcessorConfig.bETM              = TRUE;
		tyProcessorConfig.bThumb            = TRUE;
		tyProcessorConfig.bSynthesisedCore  = TRUE;
		tyProcessorConfig.bCheckDRReturn    = TRUE; //Doubt
		tyProcessorConfig.bDCC              = TRUE;
		break;
		
	case ARM1176JZS    :
		tyProcessorConfig.bARM7             = FALSE;
		tyProcessorConfig.bARM9             = FALSE;
		tyProcessorConfig.bARM11            = TRUE;
		tyProcessorConfig.bCortexM3         = FALSE;
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
		tyProcessorConfig.tyCoProcessorType = ARM1176JZS_CP;
		tyProcessorConfig.bETM              = TRUE;
		tyProcessorConfig.bThumb            = TRUE;
		tyProcessorConfig.bSynthesisedCore  = TRUE;
		tyProcessorConfig.bCheckDRReturn    = TRUE; //Doubt
		tyProcessorConfig.bDCC              = TRUE;
		break;

    case CORTEX_M3    :
        tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = TRUE; 
		tyProcessorConfig.bCortexA8		    = FALSE;
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = NO_COPROCESSOR;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE; //Doubt
        tyProcessorConfig.bDCC              = TRUE;
        break;

	case CORTEX_A8:
		tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;
		tyProcessorConfig.bCortexA8         = TRUE; 
		tyProcessorConfig.bCortexA9		    = FALSE;
        tyProcessorConfig.tyCoProcessorType = CORTEXA8_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE; //Doubt
        tyProcessorConfig.bDCC              = TRUE;
        break;

	case CORTEX_A9:
		tyProcessorConfig.bARM7             = FALSE;
        tyProcessorConfig.bARM9             = FALSE;
        tyProcessorConfig.bARM11            = FALSE;
        tyProcessorConfig.bCortexM3         = FALSE;
		tyProcessorConfig.bCortexA8         = FALSE; 
		tyProcessorConfig.bCortexA9         = TRUE; 
        tyProcessorConfig.tyCoProcessorType = CORTEXA9_CP;
        tyProcessorConfig.bETM              = TRUE;
        tyProcessorConfig.bThumb            = TRUE;
        tyProcessorConfig.bSynthesisedCore  = TRUE;
        tyProcessorConfig.bCheckDRReturn    = TRUE; //Doubt
        tyProcessorConfig.bDCC              = TRUE;
        break;

		default:
			// One of the above should have been chosen...
			// TODO: ASSERT_NOW();
			break;
	}

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_DebugRequest
    Engineer: Suraj S
       Input: unsigned char ucDebugReq - 1 for Set Debug request
										           0 for Clear Debug Request
      Output: TyError - Mid Layer error code
 Description: Controls the Debug Request line 
Date          Initials     Description
5-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_DebugRequest(unsigned char ucDebugReq)
{
	TyError ErrRet;

	ErrRet = DL_OPXD_Arm_DebugRequest(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, ucDebugReq);

	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	ML_Delay(1000); //Assumes a delay is required here for proper functioning of Diskware

	return ML_ERROR_NO_ERROR;
}


/****************************************************************************
    Function: ML_InitTarget
    Engineer: Roshan T R
       Input: unsigned long ulTargetType - Target type
      Output: TyError - Mid Layer error code
 Description: Controls the Debug Request line 
Date          Initials     Description
20-Mar-2010   RTR          Initial
****************************************************************************/
TyError ML_InitTarget(unsigned long ulTargetType)
{
	TyError ErrRet;

	ErrRet = DL_OPXD_Arm_InitTarget(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, ulTargetType);

	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_DebugStartup
    Engineer: Suraj S
       Input: boolean *pbExecuting - status of execution
      Output: Error Status
 Description: Reads the ICE breaker DBG_STATUS_REG register value and sets the 
              bGlobalStartedExecution global variable to TRUE. Read the status 
              of the processor 
Date          Initials     Description
20-Jul-2007      SJ         Initial
31-Jul-2008      RS         Added ARM11 support
****************************************************************************/
TyError ML_DebugStartup (boolean *pbExecuting)

{
	unsigned long ulDebugStatusValue;
	TXRXCauseOfBreak Breaktype;
	TyError ErrRet;
	RDI_PointHandle handle;

	if(tyProcessorConfig.bARM11)
	{
		ErrRet = ML_ReadDSCR(&ulDebugStatusValue);
		if (ErrRet != ML_ERROR_NO_ERROR)
			return ErrRet;
	}
    else if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
    {

    }
	else
	{
		ErrRet = ML_ReadICEBreaker (DBG_STATUS_REG, &ulDebugStatusValue);
		if (ErrRet!=ML_ERROR_NO_ERROR)
			return ErrRet;
	}	   

	bGlobalStartedExecution = TRUE;
#ifndef __LINUX
	ErrRet = ML_StatusProc(pbExecuting, &Breaktype, &handle, InitUserRegisterSettings);
#else
	ErrRet = ML_StatusProc(pbExecuting, &Breaktype, &handle, NULL);
#endif
	if (ErrRet!=ML_ERROR_NO_ERROR)
		return ErrRet;

	return ML_ERROR_NO_ERROR;
}
/****************************************************************************
    Function: ML_ResetTAPController
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Resets the TAP controller using nTRST
   Date       Initials     Description
5-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ResetTAPUsingTMS(void)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;

   ErrRet = DL_OPXD_JtagResetTAP(tyCurrentDevice, 0);  // in addition, ensure TAPs are in initial state
   
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ErrRet;
}

/****************************************************************************
    Function: ML_ResetTAPController
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Resets the TAP controller using nTRST
   Date       Initials     Description
5-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ResetTAPController(void)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;

   if(tyProcessorConfig.bARM7)
   {
      if(!tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7;
      else if(tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7S;
   }
   else if(tyProcessorConfig.bARM9)      
      TyProcType = ARM9;
   else if(tyProcessorConfig.bARM11)      
      TyProcType = ARM11;
    else if (tyProcessorConfig.bCortexM3)
        TyProcType = CORTEXM;
	else if (tyProcessorConfig.bCortexM3)
        TyProcType = CORTEXA;

   ErrRet = DL_OPXD_JtagResetTAP(tyCurrentDevice, 1); // reset TAPs using TRST signal

   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      ErrRet = DL_OPXD_JtagResetTAP(tyCurrentDevice, 0);  // in addition, ensure TAPs are in initial state
   else return	(ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      ErrRet = DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType);                      // in addition, ensure TAPs are in initial state

   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ErrRet;
}

/****************************************************************************
    Function: ML_SetJTAGFrequency
    Engineer: Suraj S
       Input: double ulFreqKHz - The frequency value in kHz
      Output: TyError - Mid Layer error code
 Description: Sets the JTAG frequncy according to the given value
   Date       Initials     Description
6-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_SetJTAGFrequency(double ulFreqKHz)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   double dFrequenyKHz = (double)ulFreqKHz;
   double dFrequenyMHz = dFrequenyKHz/KILOHZ;

   if (!tyTargetConfig.bAdaptiveClockingEnabled)
	   ErrRet = DL_OPXD_JtagSetFrequency(tyCurrentDevice, dFrequenyMHz, NULL, NULL);
   else if(tyTargetConfig.bAdaptiveClockingEnabled)
      ErrRet = DL_OPXD_JtagSetAdaptiveClock(tyCurrentDevice, 1, 1);

   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ErrRet;
}


/****************************************************************************
    Function: ML_ProcessCP15Register
    Engineer: Roshan T R
    Input:    unsigned int uiFamily - Processor Family (ARM7, 9 or 11)
			  unsigned int uiCore - Core type
			  unsigned int uiCPNo - The co processor to be read
              unsigned int uiRegIndex - The register index
    Output:   TyError - Mid Layer error code
 Description: Sets the JTAG frequency according to the given value
Date          Initials     Description
7-Dec-2009    RTR          Initial
****************************************************************************/
TyError ML_ProcessCP15Register(unsigned int uiOperation, unsigned int uiCRn,
							   unsigned int uiCRm, unsigned int uiOp1, 
							   unsigned int uiOp2, unsigned long *pulRegValue)
{
    TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;
	unsigned long ulOpcode = 0, ulTmpOp = 0;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;

	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}

	if(ARM11 == TyProcType)
	{
		//Create the Operand to read/Write Co processor
		ulOpcode = (ulOpcode | 0xEE) << POS_MUSTBE11101110;
	
		ulOpcode = ulOpcode | ((ulTmpOp | uiOp1) << POS_OP1); ulTmpOp = 0;
		if(uiOperation)
		{
			ulOpcode = ulOpcode | ((ulTmpOp | 0x1) << POS_READ_WRITE); ulTmpOp = 0;
		}
		ulOpcode = ulOpcode | ((ulTmpOp | uiCRn) << POS_CRN); ulTmpOp = 0;
		ulOpcode = ulOpcode | ((ulTmpOp | 0xF) << POS_MUSTBE1111); ulTmpOp = 0;
		ulOpcode = ulOpcode | ((ulTmpOp | uiOp2) << POS_OP2); ulTmpOp = 0;
		ulOpcode = ulOpcode | ((ulTmpOp | 0x1) << POS_MUSTBE1); ulTmpOp = 0;
		ulOpcode = (ulOpcode | uiCRm);
		//Now Opcode contains MRC or MCR instruction with parameters.
	}
	else if(ARM9 == TyProcType)
	{
		if(ARM926EJS == tyProcessorConfig.tyProcType)
		{
			ulOpcode = (ulOpcode | uiOp1) << 11; 
			ulTmpOp = 0;
			ulOpcode = ulOpcode | ((ulTmpOp | uiOp2) << 8); 
			ulTmpOp = 0;
			ulOpcode = ulOpcode | ((ulTmpOp | uiCRn) << 4); 
			ulTmpOp = 0;
			ulOpcode = ulOpcode | uiCRm;	
			ulTmpOp = 0;
		}
		else
		{
			ulOpcode = (ulOpcode | uiOp1) << 5;
			ulOpcode = ulOpcode | ((ulTmpOp | uiCRn) << 1); ulTmpOp = 0;
			ulOpcode = ulOpcode | uiOp2;
		}
	}

	else if (CORTEXA == TyProcType)
	{
		if (uiOperation)
		{
			ulOpcode =  ARMv7A_MRC_OPCODE;
		} 
		else
		{
			ulOpcode =  ARMv7A_MCR_OPCODE; 
		}
		
		ulOpcode |= (uiCRm & 0xF)| ((uiOp2 & 0x7) << 5) | ((uiCRn & 0xF) << 16)| ((uiOp1 & 0xF) << 21);
	}
	


	ErrRet = DL_OPXD_ProcessCP15Register(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
									TyProcType, tyProcessorConfig.tyProcType, uiOperation, ulOpcode, pulRegValue);

	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ErrRet;
}

/****************************************************************************
    Function: ML_ReadCPRegister
    Engineer: Roshan T R
    Input:    unsigned int uiFamily - Processor Family (ARM7, 9 or 11)
			  unsigned int uiCore - Core type
			  unsigned int uiCPNo - The coprocessor to be read
              unsigned int uiRegIndex - The register index
    Output:   TyError - Mid Layer error code
 Description: Sets the JTAG frequncy according to the given value
Date          Initials     Description
7-Dec-2009    RTR          Initial
****************************************************************************/
TyError ML_ReadCPRegister(unsigned int uiCpNum, 
						  unsigned int uiRegIndex,
						  unsigned int *puiRegValue)

{
    TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;
	
	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;

	ErrRet = DL_OPXD_ReadCPRegister(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
									TyProcType, tyProcessorConfig.tyProcType, uiCpNum, uiRegIndex, puiRegValue);

	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ErrRet;
}

/****************************************************************************
    Function: ML_WriteCPRegister
    Engineer: Roshan T R
    Input:    unsigned int uiFamily - Processor Family (ARM7, 9 or 11)
			  unsigned int uiCore - Core type
			  unsigned int uiCPNo - The coprocessor to be read
              unsigned int uiRegIndex - The register index
			  unsigned int uiValue - The value to be write to register
    Output:   TyError - Mid Layer error code
 Description: Sets the JTAG frequncy according to the given value
Date          Initials     Description
7-Dec-2009    RTR          Initial
****************************************************************************/
TyError ML_WriteCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex,
						  unsigned int uiValue)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;
   if(tyProcessorConfig.bARM7)
   {
	   if(!tyProcessorConfig.bSynthesisedCore)
		   TyProcType = ARM7;
	   else if(tyProcessorConfig.bSynthesisedCore)
		   TyProcType = ARM7S;
   }
   else if(tyProcessorConfig.bARM9)      
	   TyProcType = ARM9;
   else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;

   ErrRet = DL_OPXD_WriteCPRegister(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
									TyProcType, tyProcessorConfig.tyProcType, uiCpNum, uiRegIndex, uiValue);

   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ErrRet;
}

/****************************************************************************
     Function: ML_ScanIR
     Engineer: Venkitakrishan K
        Input: TyDevHandle tyCurrentDeviceHandle - ARM target parameters/settings
               unsigned long ulLength - number of bits to scan
               unsigned long *pulDataIn - data to scanchain
               unsigned long *pulDataOut - data from scanchain
       Output: int - result
  Description: scan JTAG IR register for current core
Date           Initials    Description
31-Mar-2008    VK          Initial
****************************************************************************/
int ML_ScanIR(unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int iResult = ERR_ML_ARM_NO_ERROR;

   // call function to scan IR, parameters are checked inside function
   tyRes = DL_OPXD_JtagScanIR(tyCurrentDevice,0,
                              ulLength, pulDataIn, pulDataOut);
   // check what caused function to fail
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARM_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARM_RTCK_TIMEOUT_EXPIRED;
         break;
      default:
         iResult = ERR_ML_ARM_USB_DRIVER_ERROR;
         break;
      }
   return iResult;
}

/****************************************************************************
     Function: ML_ScanDR
     Engineer: Venkitakrishan K
        Input: TyDevHandle tyCurrentDeviceHandle - ARM target parameters/settings
               unsigned long ulLength - number of bits to scan
               unsigned long *pulDataIn - data to scanchain
               unsigned long *pulDataOut - data from scanchain
       Output: int - result
  Description: scan JTAG DR register for current core
Date           Initials    Description
31-Mar-2008    VK          Initial
****************************************************************************/
int ML_ScanDR(unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut)
{
   TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int iResult = ERR_ML_ARM_NO_ERROR;


   // call function to scan DR, parameters are checked inside function
   tyRes = DL_OPXD_JtagScanDR(tyCurrentDevice,0,
                              ulLength, pulDataIn, pulDataOut);
   // check what caused function to fail
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARM_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARM_RTCK_TIMEOUT_EXPIRED;
         break;
      default:
         iResult = ERR_ML_ARM_USB_DRIVER_ERROR;
         break;
      }
   return iResult;
}

/****************************************************************************
     Function: ML_ScanIRScanDR
     Engineer: Venkitakrishan K
        Input: TyDevHandle tyCurrentDeviceHandle - ARM target parameters/settings
               unsigned long ulTap - Tap number
               unsigned long *pulInBuffer-Data from scanchain
              
       Output: int - result
  Description: scan JTAG IR register for total number of cores,IDcode,IRLength
Date           Initials    Description
02-Apr-2008    VK          Initial
****************************************************************************/
int ML_ScanIRScanDR(unsigned long *pulLength, unsigned long *pulDataIn, unsigned long *pulOutBuffer)
{

	unsigned long pulOutDRBuffer[SCANCHAIN_ULONG_SIZE] = {0x2};
	unsigned long pulOutIRBuffer[SCANCHAIN_ULONG_SIZE] ={0xe};
	unsigned long ulNumberOfBitsIR = {0x4};
	unsigned long ulNumberOfBitsDR = {0x32};
	unsigned long ulTapIndex =0;
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
   int iResult = ERR_ML_ARM_NO_ERROR;
   ulTapIndex = *pulDataIn;
   *pulLength = pulIRLengthArray[ulTapIndex];
   // call function to scan IR, parameters are checked inside function
   tyRes = DL_OPXD_JtagScanIRandDR(tyCurrentDevice, ulTapIndex, ulNumberOfBitsIR, pulOutIRBuffer,ulNumberOfBitsDR, pulOutDRBuffer,pulOutBuffer);
		
   // check what caused function to fail
   switch (tyRes)
      {
      case DRVOPXD_ERROR_NO_ERROR:
         iResult = ERR_ML_ARM_NO_ERROR;
         break;
      case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
         iResult = ERR_ML_ARM_RTCK_TIMEOUT_EXPIRED;
         break;
      default:
		  (void)DL_OPXD_CloseDevice(tyCurrentDevice);
         iResult = ERR_ML_ARM_USB_DRIVER_ERROR;
         break;
      }
   return iResult;
}

/****************************************************************************
    Function: ML_InfoProc
    Engineer: Suraj S
       Input: See RDI Spec
      Output: TyError - Mid Layer error code
 Description: Allows the various Info calls defined in RDI spec to be called.
Date          Initials     Description
9-Jul-2007      SJ         Initial
4-Apr-2008      SJ         ulGlobalExecuting variable is reset for infocall 
                           from ARM GDB Server
****************************************************************************/
TyError ML_InfoProc (unsigned long  ulType,
                     unsigned long  *pulArg1,
                     unsigned long  *pulArg2)

{
	TyError       ErrRet             = ML_ERROR_NO_ERROR;
	unsigned long ulDebugStatusValue = 0;
   
	NOREF(pulArg2);

	switch (ulType)     
	{
		case RDISignal_Stop:
		//Commented for Optimisation
		// Set the ICE breaker register scan chain.  
		/*ErrRet = ML_Select_Scan_Chain(2,1);
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			ulGlobalScanChain = FORCE_SCANCHAIN_SELECT;
			ErrRet = ML_Select_Scan_Chain(2,1);
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
		}*/

		if(tyProcessorConfig.bARM11)
		{
			ErrRet = ML_ReadDSCR(&ulDebugStatusValue);
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
			
			//Check if the core is running
			if ((ulDebugStatusValue & ARM11_DEBUG_MODE) == ARM11_DEBUG_MODE)
			{
				// Core has already stopped - must have hit a breakpoint.
				bUserHalt =FALSE;
				ulGlobalExecuting = 0;
				return ML_Error_TargetStopped;
			}
			else
			{
				// Core is still running.
				// Set flag to show user halt
				bUserHalt =TRUE;

				ErrRet = ML_EnterDebugState();
				if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;

				ErrRet = DL_OPXD_IndicateTargetStatus(tyCurrentDevice,tyTargetConfig.ulDebugCoreNumber,TRUE);
				if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
					return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
			}

		}
        else if ((tyProcessorConfig.bCortexM3) ||
				 (tyProcessorConfig.bCortexA8) ||
				 (tyProcessorConfig.bCortexA9))
        {
            // Core is still running.
            // Set flag to show user halt
            bUserHalt =TRUE;

            ErrRet = ML_EnterDebugState();
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;

            ErrRet = DL_OPXD_IndicateTargetStatus(tyCurrentDevice,tyTargetConfig.ulDebugCoreNumber,TRUE);
            if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
            {
                return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
            }
        }
		else
		{
			ErrRet = ML_ReadICEBreaker(DBG_STATUS_REG, &ulDebugStatusValue);
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;

			// Check if the core is running.
			if ((ulDebugStatusValue & ARM_DEBUG_MODE) == ARM_DEBUG_MODE)
			{
				// Core has already stopped - must have hit a breakpoint.
				bUserHalt =FALSE;
				ulGlobalExecuting = 0;
				return ML_Error_TargetStopped;
			}
			else
			{
				// Core is still running.
				// Set flag to show user halt
				bUserHalt =TRUE;
            
				//There is a know silicon bug on ARM7TDMI-s where the core fails to stop.
				//This is different to the silicon bug in ARM7TDMI.
				if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bSynthesisedCore))
				{
					//We have to set a don't care hardware breakpoint.
					// Set the ICE breaker register scan chain.  
					ErrRet = ML_Disable_EmbeddedICERT ();  JMP_ERR(ML_INFO_ERROR_RETURN);
               
					ErrRet = ML_WriteICEBreaker (WP0_ADDR_V, 0x0);  JMP_ERR(ML_INFO_ERROR_RETURN);

					ErrRet = ML_WriteICEBreaker (WP0_ADDR_M, ARM_BP_DONT_CARE);  JMP_ERR(ML_INFO_ERROR_RETURN);

					ErrRet = ML_WriteICEBreaker (WP0_DATA_V, 0x0);  JMP_ERR(ML_INFO_ERROR_RETURN);

					ErrRet = ML_WriteICEBreaker (WP0_DATA_M, ARM_BP_DONT_CARE);  JMP_ERR(ML_INFO_ERROR_RETURN);

					ErrRet = ML_WriteICEBreaker (WP0_CTRL_V, ARM7TDMIS_ARM_ENABLE_WP);  JMP_ERR(ML_INFO_ERROR_RETURN);

					ErrRet = ML_WriteICEBreaker (WP0_CTRL_M, ARM7TDMIS_ARM_CHECK_IFETCH_ONLY);  JMP_ERR(ML_INFO_ERROR_RETURN);

					ErrRet = ML_Enable_EmbeddedICERT ();  JMP_ERR(ML_INFO_ERROR_RETURN);

					/*if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;*/
				}
				else
				{
					// Write to the Debug Ctrl Reg and set the DBGRQ bit...
					ErrRet = ML_WriteICEBreaker (DBG_CONTROL_REG, SET_DBGR_BIT);  JMP_ERR (ML_INFO_ERROR_RETURN);
					/*if (ErrRet != ML_ERROR_NO_ERROR)
					return ErrRet;*/
				}

				ErrRet = ML_WaitForSystemSpeedCompletion();   JMP_ERR (ML_INFO_ERROR_RETURN);

				ErrRet = DL_OPXD_IndicateTargetStatus(tyCurrentDevice,tyTargetConfig.ulDebugCoreNumber,TRUE);
				if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
				return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);

				ErrRet = ML_DisableWatchpoints ();  JMP_ERR (ML_INFO_ERROR_RETURN);
            
				// Write to the Debug Ctrl Reg and set the INTDIS bit...
				ErrRet = ML_WriteICEBreaker (DBG_CONTROL_REG, INTDIS_DBGACK);   JMP_ERR (ML_INFO_ERROR_RETURN);
			}
		}
		break;
       
      case RDIInfo_ForceSystemReset:
         
         ErrRet = DL_OPXD_Arm_ResetProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 1, NULL, NULL);;
         if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		     ErrRet= ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);

         ML_Delay(100);

         
         ErrRet = DL_OPXD_Arm_ResetProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 0, NULL, NULL);;
         if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		     ErrRet= ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);

         ML_Delay(100);

         // The processor is always in ARM mode after reset...
         bGlobalThumb = FALSE;

         // The processor should be halted after a reset...
         bGlobalStartedExecution = FALSE;
         break;

      case RDISemiHosting_SetState:    
         //Update the global state 
         tySemiHostingConfig.ulSemiHostingType = *pulArg1; 
         tySemiHostingConfig.bSemiHostingSet=(unsigned char)*pulArg1;
         
         switch (tySemiHostingConfig.ulSemiHostingType)
            {
            default:
            case NO_SEMIHOSTING:
               //Clear the semihosting watchpoints if set.
               ErrRet = ML_ClearSemihostingBP (); JMP_ERR (ML_INFO_ERROR_RETURN);
               break;
            case STANDARD_SEMIHOSTING:
               //Set up the semihosting watchpoints if needed.
               ErrRet = ML_SetSemihostingBP ();  JMP_ERR (ML_INFO_ERROR_RETURN);
               tySemiHostingConfig.bSemiHostingSet=0;
               break;
            case DCC_SEMIHOSTING:
               //Clear the semihosting watchpoints if set.
               ErrRet = ML_ClearSemihostingBP (); JMP_ERR (ML_INFO_ERROR_RETURN);
               
               //If DCC SemiHosting is set load the handler and write to vector table.
               ErrRet = ML_Initialise_SemiHosting(); JMP_ERR (ML_INFO_ERROR_RETURN);
               
               ErrRet = ML_LoadDCC_SH_Handler(); JMP_ERR (ML_INFO_ERROR_RETURN);
               break;
            }
         break;         
      case RDISemiHosting_GetState:       
         *pulArg1 = tySemiHostingConfig.ulSemiHostingType;
         break;         
                                     
      case RDISemiHosting_SetVector:      
         tySemiHostingConfig.ulSHVectorAddress = *pulArg1;               
         break;         
                                     
      case RDISemiHosting_GetVector:      
         *pulArg1 = tySemiHostingConfig.ulSHVectorAddress;               
         break;         
                                     
      case RDISemiHosting_SetARMSWI:      
         tySemiHostingConfig.ulARMSWIInstruction   = *pulArg1;
         break;         
                                  
      case RDISemiHosting_SetThumbSWI:      
         tySemiHostingConfig.ulThumbSWIInstruction = *pulArg1;
         break;         
         
      case RDISemiHosting_GetARMSWI:      
         *pulArg1 = tySemiHostingConfig.ulARMSWIInstruction;
         break;         
                                     
      case RDISemiHosting_GetThumbSWI:    
         *pulArg1 = tySemiHostingConfig.ulThumbSWIInstruction;
         break;         

      case RDISemiHosting_SetDCCHandlerAddress:
         
         //If the handler address has changed write the handler and vector table.
         if (tySemiHostingConfig.ulDCCServiceRoutineAddress != *pulArg1)
            {
            tySemiHostingConfig.ulDCCServiceRoutineAddress = *pulArg1;

            ErrRet = ML_Initialise_SemiHosting(); JMP_ERR (ML_INFO_ERROR_RETURN);
            ErrRet = ML_LoadDCC_SH_Handler(); JMP_ERR (ML_INFO_ERROR_RETURN);
            }
         else
            tySemiHostingConfig.ulDCCServiceRoutineAddress = *pulArg1;

         break;

      case RDISemiHosting_GetDCCHandlerAddress:
         *pulArg1 = tySemiHostingConfig.ulDCCServiceRoutineAddress;
         break;

      case RDISemiHosting_SetCompilerType:
         tyTargetConfig.bUseGNUSemihosting = (unsigned char)*pulArg1;
         break;

      default:
         // Nothing to do here...
         break;
      }
   return ML_ERROR_NO_ERROR;

ML_INFO_ERROR_RETURN:

   if (ulType == RDISemiHosting_SetState)
      {
      tySemiHostingConfig.ulSemiHostingType = NO_SEMIHOSTING;
      }

   return ML_ResetCheck(ErrRet);
}

/****************************************************************************
    Function: ML_Select_Scan_Chain
    Engineer: Suraj S
       Input: unsigned long ulScanChain - Number of Scan Chain to select
              unsigned char ucNumIntests - Number of intests to perform 
      Output: TyError - Mid Layer error code.   
 Description: Selects JTAG scan chain 
Date          Initials     Description
9-Jul-2007      SJ         Initial
02-Nov-2009		JCK		   Added support for Cortex-M3
****************************************************************************/
TyError ML_Select_Scan_Chain (unsigned char ulScanChain, unsigned char ucNumIntests)

{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		TyProcType = CORTEXA;

   if(ulScanChain != ulGlobalScanChain)
      ErrRet = DL_OPXD_Arm_SelectScanChain(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, ulScanChain, ucNumIntests, TyProcType);
	
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   ulGlobalScanChain = ulScanChain;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ReadICEBreaker
    Engineer: Suraj S
       Input: unsigned long  ulAddress - Address of ICEBreaker register
              unsigned long *pulReadValue - Pointer to return value read from register 
      Output: TyError - Mid Layer error code     
 Description: Reads a register from the ICEBreaker module.
Date          Initials     Description
9-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ReadICEBreaker (unsigned long  ulAddress, 
                           unsigned long *pulReadValue)

{
   TyError ErrRet;

   // This function expects scan chain 2.
   ErrRet = ML_Select_Scan_Chain(2,1);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   if (pulReadValue == NULL)
      return ML_Error_InitTarget_Opella;

   ErrRet = DL_OPXD_Arm_ReadICEBreaker(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, ulAddress, pulReadValue);
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ML_ERROR_NO_ERROR;
}


/****************************************************************************
    Function: ML_ArmInstnExecEnable
    Engineer: Roshan T R
       Input: void
      Output: TyError - Mid Layer error code     
 Description: Enable Arm instruction execution after a halt(in case of ARM11)
Date          Initials     Description
10-Sep-2009   RTR           Initial
****************************************************************************/
TyError ML_ArmInstnExecEnable (void)
{
	TyError ErrRet;
	TyArmProcType TyProcType = ARM11;
	
	ErrRet = DL_OPXD_Arm_Instn_Exec_Enable(tyCurrentDevice,
										   tyTargetConfig.ulDebugCoreNumber,
											TyProcType);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	
	return ML_ERROR_NO_ERROR;

}

/****************************************************************************
    Function: ML_ReadDSCR
    Engineer: Venkitakrishnan K  
       Input: unsigned long *pulReadValue - Pointer to return value read from register 
      Output: TyError - Mid Layer error code     
 Description: Reads Debug Status Control Register For ARM cores.
Date          Initials     Description
31-Jul-2008   VK           Initial
****************************************************************************/
TyError ML_ReadDSCR (unsigned long *pulReadValue)

{
	TyError ErrRet;
	TyArmProcType TyProcType;

	if (tyProcessorConfig.bARM11)
    {
        TyProcType = ARM11;         
    }    
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
    {
        TyProcType = CORTEXA;
    }
	else
	{
		return ML_Error_BadValue;
	}

	if (pulReadValue == NULL)
		return ML_Error_InitTarget_Opella;

	ErrRet = DL_OPXD_Arm_ReadDebug_Status_Control_Reg(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,TyProcType, pulReadValue);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ReadCPSR
    Engineer: Roshan T R  
       Input: unsigned long *pulReadValue - Pointer to return value read from register 
      Output: TyError - Mid Layer error code     
 Description: Reads CPSR of ARM11 core
Date          Initials     Description
21-Jul-2008   RTR           Initial
****************************************************************************/
TyError ML_ReadCPSR (unsigned long *pulReadValue)

{
	TyError ErrRet;
	TyArmProcType TyProcType = ARM11;

	if (pulReadValue == NULL)
		return ML_Error_InitTarget_Opella;

	ErrRet = DL_OPXD_Arm_ReadCPSR(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,TyProcType, pulReadValue);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ReadCDCR
    Engineer: Roshan T R  
       Input: unsigned long *pulReadValue - Pointer to return value read from register 
      Output: TyError - Mid Layer error code     
 Description: Reads CDCR of ARM11 core
Date          Initials     Description
30-Oct-2009   RTR           Initial
****************************************************************************/
TyError ML_ReadCDCR(unsigned long *pulReadValue)

{
	TyError ErrRet;
	TyArmProcType TyProcType = ARM11;

	if (pulReadValue == NULL)
		return ML_Error_InitTarget_Opella;

	ErrRet = DL_OPXD_Arm_ReadCDCR(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,TyProcType, pulReadValue);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_EnterDebugState
    Engineer: Rejeesh S Babu
       Input:  void
      Output: TyError - Mid Layer error code     
 Description: To change the state of the processor to Debug mode.
Date          Initials     Description
31-Jul-2008   RS           Initial
28-Oct-2009   JCK          Added Support for Cortex-M3
****************************************************************************/
TyError ML_EnterDebugState(void)
{
    TyError ErrRet;
    TyArmProcType TyProcType;
	unsigned long ulDataArray[3];

    if (tyProcessorConfig.bARM11)
    {
        TyProcType = ARM11;         
    }
    else if (tyProcessorConfig.bCortexM3)
    {
        TyProcType = CORTEXM;
    }
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
    {
        TyProcType = CORTEXA;
    }
	else
	{
		return ML_Error_BadValue;
	}

	/* DAP PC Control Passed to diskware because if in future any device found which has 
	found the address different from this needs to configure through FamilyArm.xml. */
	if (tyTargetConfig.bDapPcSupport)
	{
		ulDataArray[0] = TRUE;
		ulDataArray[1] = TI_DAPPCCONTROL_REG_ADDR;
		ulDataArray[2] = TI_DAPPCCONTROL_DAPCONNECT;
	} 
	else
	{
		ulDataArray[0] = FALSE;
		ulDataArray[1] = 0;
		ulDataArray[2] = 0;
	}
	 
	ErrRet = DL_OPXD_Arm_Enter_Debug_State(tyCurrentDevice, 
											tyTargetConfig.ulDebugCoreNumber,
											TyProcType,
											ulDataArray,
											sizeof(ulDataArray));
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
        return(ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	if((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{	
		tyArmv7ABreakpointResArray.ulNumBreakpoint   = ((ulDataArray[0] & DIDR_BRP) >> 24) + 1;
		tyArmv7AWatchpointResArray.ulNumWatchPoint = ((ulDataArray[0] & DIDR_WRP) >> 28) + 1;
		
	}

    //free(pulData);
    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_LeaveDebugState
    Engineer: Suraj S
       Input:  void
      Output: TyError - Mid Layer error code     
 Description: To change the state of the processor to Debug mode.
Date          Initials     Description
11-Mar-2011		SJ			Initial
****************************************************************************/
TyError ML_LeaveDebugState(void)
{
    TyError ErrRet;
    TyArmProcType TyProcType;

    if (tyProcessorConfig.bARM11)
    {
        TyProcType = ARM11;         
    }
    else if (tyProcessorConfig.bCortexM3)
    {
        TyProcType = CORTEXM;
    }
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
    {
        TyProcType = CORTEXA;
    }
	else
	{
		return ML_Error_BadValue;
	}
	 
	ErrRet = DL_OPXD_Arm_Leave_Debug_State(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bGlobalThumb, ARM11_DataArray, sizeof(ARM11_DataArray));
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
        return(ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_WriteCP14DebugRegARM11
    Engineer: Roshan T R
       Input: unsigned long  ulAddress - Address of CP14 register
              unsigned long  ulWriteValue - Value to write to register
      Output: TyError - Mid Layer error code
 Description: Writes to a debug register in the CP14 module.
Date          Initials     Description
22-Aug-2009   RTR          Initial
****************************************************************************/
TyError ML_WriteCP14DebugRegARM11 (unsigned long  ulAddress, 
                            unsigned long  ulWriteValue)

{
	TyError ErrRet;
	TyArmProcType TyProcType = ARM11;
	
	
	ErrRet = DL_OPXD_Arm_WriteCP14DebugRegARM11(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,TyProcType, ulAddress, &ulWriteValue);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	
	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
Function: ML_WriteVectorCacheReg
Engineer: Rejeesh S babu
Input: unsigned long  ulWriteValue - Value to write to register
Output: TyError - Mid Layer error code
Description: Writes to VCR register in the Scan Chain 7.
Date          Initials     Description
31-Jul-2008   RS           Initial
****************************************************************************/
TyError ML_WriteVectorCacheReg (unsigned long  ulWriteValue)

{
	TyError ErrRet;
	TyArmProcType TyProcType = ARM11;
	
	
	ErrRet = DL_OPXD_Arm_WriteVectorCacheReg(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,TyProcType, &ulWriteValue);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	
	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_WriteICEBreaker
    Engineer: Suraj S
       Input: unsigned long  ulAddress - Address of ICEBreaker register
              unsigned long  ulWriteValue - Value to write to register
      Output: TyError - Mid Layer error code
 Description: Writes to a register in the ICEBreaker module.
Date          Initials     Description
9-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_WriteICEBreaker (unsigned long  ulAddress, 
                            unsigned long  ulWriteValue)

{
   TyError ErrRet;

   // This function expects scan chain 2.
   ErrRet = ML_Select_Scan_Chain(2,1);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   // Select the register to write to.
   ErrRet = DL_OPXD_Arm_WriteICEBreaker(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, ulAddress, &ulWriteValue);
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_Disable_EmbeddedICERT
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Disables the EmbeddedICE-RT comparator outputs.
Date          Initials     Description
9-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_Disable_EmbeddedICERT (void)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   unsigned long ulDebugControlReg = 0;
   
   //Commented for Optimisation
   /*ErrRet = ML_Select_Scan_Chain(2,1);
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;*/
   
   if (tyProcessorConfig.bSynthesisedCore)
      {
      ErrRet = ML_ReadICEBreaker (DBG_CONTROL_REG, &ulDebugControlReg);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      
      //Disable EmbeddedICE RT
      ErrRet = ML_WriteICEBreaker (DBG_CONTROL_REG, (ulDebugControlReg | 0x20));
      if (ErrRet!=ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_Enable_EmbeddedICERT
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Enables the EmbeddedICE-RT comparator outputs.
Date          Initials     Description
9-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_Enable_EmbeddedICERT (void)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   
   unsigned long ulDebugControlReg = 0;

   //Commented for Optimisation
   /*ErrRet = ML_Select_Scan_Chain(2,1);
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;*/

   if (tyProcessorConfig.bSynthesisedCore)
      {
      ErrRet = ML_ReadICEBreaker (DBG_CONTROL_REG, &ulDebugControlReg);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      
      //Enable EmbeddedICE RT
      ErrRet = ML_WriteICEBreaker (DBG_CONTROL_REG, (ulDebugControlReg & 0x1F));
      if (ErrRet!=ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetupICEBreakerModule
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Writes 0x0 to all the ICEBreaker module registers.
Date          Initials     Description
20-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_SetupICEBreakerModule (void)

{
   unsigned long i;
   TyError ErrRet;
   
   // Set the ICE breaker register scan chain. 
   ErrRet = ML_Disable_EmbeddedICERT ();
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;

   // Writing 0 to all watchpoint registers
   for (i = WP0_ADDR_V; i <= WP0_CTRL_M; i++)
      {
      ErrRet = ML_WriteICEBreaker (i, 0x00);
      if (ErrRet!=ML_ERROR_NO_ERROR)
         return ErrRet;
      }
   for (i = WP1_ADDR_V; i <= WP1_CTRL_M; i++)
      {
      ErrRet = ML_WriteICEBreaker (i, 0x00);
      if (ErrRet!=ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   ErrRet = ML_Enable_EmbeddedICERT ();
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetHardwareWatchPoint
    Engineer: Suraj S
       Input: unsigned long ulAddress - watch data access to this address.
              unsigned long ulType - watchpoint type
              unsigned long ulDataType - determines which access types to watch for 
              unsigned long ulBound - This is a mask defined by RDI.h
              unsigned long ulResourceNo - Hardware resource number to use. 
      Output: TyError - Mid Layer error code
 Description: Sets a data watchpoint at the given address.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
static TyError ML_SetHardwareWatchPoint (unsigned long ulAddress, 
                                         unsigned long ulType, 
                                         unsigned long ulDataType, 
                                         unsigned long ulBound, 
                                         unsigned long ulResourceNo)

{
   boolean        bByteRead            = FALSE;
   boolean        bHalfRead            = FALSE;
   boolean        bWordRead            = FALSE;
   boolean        bByteWrite           = FALSE;
   boolean        bHalfWrite           = FALSE;
   boolean        bWordWrite           = FALSE;
   unsigned long  ulTempControlValue   = 0x108;
   unsigned long  ulTempControlMask    = 0xF0;
   unsigned long  ulTempDataValue      = ARM_BP_DONT_CARE;
   unsigned long  ulTempDataMask       = ARM_BP_DONT_CARE;
   unsigned long  ulTempAddressValue   = ulAddress;
   unsigned long  ulTempAddressMask    = ARM_BP_DO_CARE;

   switch (ulType & LOWER_4BIT_MASK)
      {
      case RDIPoint_EQ:
      case RDIPoint_MASK:
      case RDIDataAndAddressPoint:
      case RDIDataPoint:
         //There are supported watchpoint types.
         break;
      default:
         return ML_Error_UnimplementedType;
      }

   // Check which type of accesses are to be monitored.
   if ((ulDataType & RDIWatch_ByteRead)  == RDIWatch_ByteRead )
      bByteRead  = TRUE;
   if ((ulDataType & RDIWatch_HalfRead)  == RDIWatch_HalfRead )
      bHalfRead  = TRUE;
   if ((ulDataType & RDIWatch_WordRead)  == RDIWatch_WordRead )
      bWordRead  = TRUE;                
   if ((ulDataType & RDIWatch_ByteWrite) == RDIWatch_ByteWrite)
      bByteWrite = TRUE;                
   if ((ulDataType & RDIWatch_HalfWrite) == RDIWatch_HalfWrite)
      bHalfWrite = TRUE;                
   if ((ulDataType & RDIWatch_WordWrite) == RDIWatch_WordWrite)
      bWordWrite = TRUE;
   
   // Sort these into bit patterns for memory access mask and value.
   //These are the only valid combinations we can set.
   if ((bByteRead  || bHalfRead  || bWordRead) &&   
       (bByteWrite || bHalfWrite || bWordWrite))
      {
      //This means that we don't care about read or write.
      ulTempControlMask   = (ulTempControlMask + WP_MASK_READ_AND_WRITE);
      }
   else
      {
      if  (bByteWrite || bHalfWrite || bWordWrite)
         {
         //This means that we are only looking for write accesses.
         ulTempControlValue  = (ulTempControlValue + WP_VALUE_WRITE_ONLY);
         }
      else
         if  (bByteRead || bHalfRead || bWordRead)
            {
            //This means that we are only looking for read accesses.
            ulTempControlValue  = (ulTempControlValue + WP_VALUE_READ_ONLY);
            }
      }

   // We now need to decide what access type to look for.
   if ((bByteRead || bByteWrite) && !(bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))  
      {
      // This is byte access only.
      ulTempControlValue  = (ulTempControlValue);
      }
   else
      {
      if (!(bByteRead || bByteWrite) && (bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))  
         {
         // This is half-word access only.
         ulTempControlValue  = (ulTempControlValue + WP_VALUE_HALF_ONLY);
         }
      else
         {
         if (!(bByteRead || bByteWrite) && !(bHalfRead || bHalfWrite) && (bWordRead || bWordWrite))  
            {
            // This is word access only.
            ulTempControlValue  = (ulTempControlValue + WP_VALUE_WORD_ONLY);
            }
         else
            {
            if ((bByteRead || bByteWrite) && (bHalfRead || bHalfWrite) && (bWordRead || bWordWrite))  
               {
               // This is don't care about access type.
               ulTempControlMask   = (ulTempControlMask + WP_MASK_BYTE_HALF_AND_WORD);
               }
            else
               return ML_Error_CantSetPoint;
            }
         }
      }
      
   //Check if there is a data value needed.
   switch (ulType)// & LOWER_4BIT_MASK)
      {
      case RDIDataAndAddressPoint:
         ulTempDataValue = ulBound;
         if  ((!bHalfWrite && !bWordWrite && !bHalfRead && !bWordRead) &&
             (bByteWrite || bByteRead))
            {
            ulTempDataMask  = ARM_BP_BYTE_MASK;
            }
         else
         if  ((!bByteWrite && !bWordWrite && !bByteRead && !bWordRead) &&
             (bHalfWrite || bHalfRead))
            {
            ulTempDataMask  = ARM_BP_HALFWORD_MASK;
            }
         else
         if  ((!bByteWrite && !bHalfWrite && !bByteRead && !bHalfRead) &&
             (bWordWrite || bWordRead))
            {
            ulTempDataMask  = ARM_BP_DO_CARE;
            }
         //else
            //TODO: ASSERT_NOW();

         ulTempAddressValue = ulAddress;
         ulTempAddressMask  = ARM_BP_DO_CARE;
         break;

      case RDIDataPoint:
         ulTempDataValue    = ulBound;
         if  ((!bHalfWrite && !bWordWrite && !bHalfRead && !bWordRead) &&
             (bByteWrite || bByteRead))
            {
            ulTempDataMask  = ARM_BP_BYTE_MASK;
            }
         else
         if  ((!bByteWrite && !bWordWrite && !bByteRead && !bWordRead) &&
             (bHalfWrite || bHalfRead))
            {
            ulTempDataMask  = ARM_BP_HALFWORD_MASK;
            }
         else 
         if  ((!bByteWrite && !bHalfWrite && !bByteRead && !bHalfRead) &&
             (bWordWrite || bWordRead))
            {
            ulTempDataMask  = ARM_BP_DO_CARE;
            }
         //else
            //TODO: ASSERT_NOW();

         ulTempAddressValue = ARM_BP_DONT_CARE;
         ulTempAddressMask  = ARM_BP_DONT_CARE;
         break;

      case RDIPoint_EQ:
         ulTempAddressValue = ulAddress;
         ulTempAddressMask = ARM_BP_DO_CARE;
         break;
      case RDIPoint_MASK:
         ulTempAddressMask = ~ulBound;
         break;
      default:
            //TODO: ASSERT_NOW();
         break;
      }

   if (ulResourceNo == HARDWARE_RESOURCE0)
      {
      // Setup WP0...
      tyBPResourceConfig.ucWP0Used           = HARDWARE_WP; 
      tyBPResourceConfig.ulWP0AddressValue   = ulTempAddressValue;
      tyBPResourceConfig.ulWP0AddressMask    = ulTempAddressMask; 
      tyBPResourceConfig.ulWP0DataValue	   = ulTempDataValue; 
      tyBPResourceConfig.ulWP0DataMask		   = ulTempDataMask; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         tyBPResourceConfig.ulWP0CtrlValue  = ulTempControlValue;
         tyBPResourceConfig.ulWP0CtrlMask   = ulTempControlMask;
         }
      else 
      if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
         // Set watchpoint registers for ARM7DI board
         {
         boolean bRWValue = FALSE;
         boolean bRWMask  = FALSE;

         // The 7DI board does not have half word capabilty so the registers only have 
         // byte / word instead ie 1 bit less in the control register.
         bRWValue = (boolean)(ulTempControlValue & (unsigned long)0x1);
         ulTempControlValue = (((ulTempControlValue & WORD_ALIGNED_MASK) >>1)+(bRWValue));
         
         bRWMask = (boolean)(ulTempControlMask & (unsigned long)0x1);
         ulTempControlMask = (((ulTempControlMask & WORD_ALIGNED_MASK) >>1)+(bRWMask));
         
         tyBPResourceConfig.ulWP0CtrlValue  = ulTempControlValue;
	   	tyBPResourceConfig.ulWP0CtrlMask   = ulTempControlMask;
         }
      else 
      if (tyProcessorConfig.bARM9) 
         {
         tyBPResourceConfig.ulWP0CtrlValue  = ulTempControlValue;
         tyBPResourceConfig.ulWP0CtrlMask   = ulTempControlMask;
         }
      
      tyBPResourceConfig.ucWP0Used = HARDWARE_WP;
      }
   else
   if (ulResourceNo == HARDWARE_RESOURCE1)
      {
      // Setup WP1...
      tyBPResourceConfig.ucWP1Used           = HARDWARE_WP; 
      tyBPResourceConfig.ulWP1AddressValue   = ulTempAddressValue;
      tyBPResourceConfig.ulWP1AddressMask    = ulTempAddressMask; 
      tyBPResourceConfig.ulWP1DataValue	   = ulTempDataValue; 
      tyBPResourceConfig.ulWP1DataMask		   = ulTempDataMask; 
      
      // Set watchpoint registers for ARM7TDMI board
      if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
         {
         tyBPResourceConfig.ulWP1CtrlValue  = ulTempControlValue;
         tyBPResourceConfig.ulWP1CtrlMask   = ulTempControlMask;
         }
      else 
      if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
      
         // Set watchpoint registers for ARM7DI board
         {
         boolean bRWValue = FALSE;
         boolean bRWMask  = FALSE;

         // The 7DI board does not have half word capability so the registers only have 
         // byte / word instead ie 1 bit less in the control register.
         bRWValue = (boolean)(ulTempControlValue &(unsigned long) 0x1);
         ulTempControlValue = (((ulTempControlValue & WORD_ALIGNED_MASK) >>1)+(bRWValue));
         
         bRWMask = (boolean)(ulTempControlMask &(unsigned long) 0x1);
         ulTempControlMask = (((ulTempControlMask & WORD_ALIGNED_MASK) >>1)+(bRWMask));
         
         tyBPResourceConfig.ulWP1CtrlValue  = ulTempControlValue;
	   	tyBPResourceConfig.ulWP1CtrlMask   = ulTempControlMask;
         }
      else 
      if (tyProcessorConfig.bARM9) 
         {      
         tyBPResourceConfig.ulWP1CtrlValue  = ulTempControlValue;
         tyBPResourceConfig.ulWP1CtrlMask   = ulTempControlMask;
         }
   
      tyBPResourceConfig.ucWP1Used = HARDWARE_WP;
      }

   //TODO: else
   //TODO: ASSERT_NOW();

 	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetARM11HardwareWatchPoint
    Engineer: Roshan T R
       Input: unsigned long ulAddress - watch data access to this address.
              unsigned long ulType - watchpoint type
              unsigned long ulDataType - determines which access types to watch for 
              unsigned long ulBound - This is a mask defined by RDI.h
              unsigned long ulResourceNo - Hardware resource number to use. 
      Output: TyError - Mid Layer error code
 Description: Sets a data watchpoint at the given address for ARM11
Date          Initials     Description
27-Aug-2009   RTR          Initial
****************************************************************************/
static TyError ML_SetARM11HardwareWatchPoint (unsigned long ulAddress, 
											  unsigned long ulType, 
											  unsigned long ulDataType, 
											  unsigned long ulBound, 
											  unsigned long ulResourceNo)
{
	boolean        bByteRead            = FALSE;
	boolean        bHalfRead            = FALSE;
	boolean        bWordRead            = FALSE;
	boolean        bByteWrite           = FALSE;
	boolean        bHalfWrite           = FALSE;
	boolean        bWordWrite           = FALSE;
	
	unsigned long  ulWPControlWord   = ARM11_ARM_WP_VALUE;
	unsigned long  ulWPAddressWord;
	unsigned char  ucBytePosition;
	
	ulBound = ulBound;
	
	switch (ulType & LOWER_4BIT_MASK)
	{
	case RDIPoint_EQ:
	case RDIPoint_MASK:
	case RDIDataAndAddressPoint:
	case RDIDataPoint:
		//There are supported watchpoint types.
		break;
	default:
		return ML_Error_UnimplementedType;
	}
	
	// Check which type of accesses are to be monitored.
	if ((ulDataType & RDIWatch_ByteRead)  == RDIWatch_ByteRead )
		bByteRead  = TRUE;
	if ((ulDataType & RDIWatch_HalfRead)  == RDIWatch_HalfRead )
		bHalfRead  = TRUE;
	if ((ulDataType & RDIWatch_WordRead)  == RDIWatch_WordRead )
		bWordRead  = TRUE;                
	if ((ulDataType & RDIWatch_ByteWrite) == RDIWatch_ByteWrite)
		bByteWrite = TRUE;                
	if ((ulDataType & RDIWatch_HalfWrite) == RDIWatch_HalfWrite)
		bHalfWrite = TRUE;                
	if ((ulDataType & RDIWatch_WordWrite) == RDIWatch_WordWrite)
		bWordWrite = TRUE;
	
	// Sort these into bit patterns for memory access mask and value.
	//These are the only valid combinations we can set.
	if ((bByteRead  || bHalfRead  || bWordRead) &&   
		(bByteWrite || bHalfWrite || bWordWrite))
	{
		//This means that we don't care about read or write.
		ulWPControlWord   = ulWPControlWord | ARM11_WP_READ_WRITE_CTRL_VALUE;
	}
	else
	{
		if  (bByteWrite || bHalfWrite || bWordWrite)
		{
			//This means that we are only looking for write accesses.
			ulWPControlWord   = ulWPControlWord | ARM11_WP_WRITE_ONLY_CTRL_VALUE;
		}
		else
			if  (bByteRead || bHalfRead || bWordRead)
            {
				//This means that we are only looking for read accesses.
				ulWPControlWord   = ulWPControlWord | ARM11_WP_READ_ONLY_CTRL_VALUE;
            }
	}
	
	ucBytePosition = (unsigned char)(ulAddress % 4);
	ulWPAddressWord = ulAddress - ucBytePosition;
	
	if(ucBytePosition == 3)
	{
		ulWPControlWord = ulWPControlWord | ARM11_WP_BYTE3_ACCESS_CTRL_VALUE;
	}
	else if(ucBytePosition == 2)
	{
		if((bByteRead || bByteWrite) && !(bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))
		{
			ulWPControlWord = ulWPControlWord | ARM11_WP_BYTE2_ACCESS_CTRL_VALUE;
		}
		else if(!(bByteRead || bByteWrite) && (bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))
		{
			ulWPControlWord = ulWPControlWord | ARM11_WP_HALF1_ACCESS_CTRL_VALUE;
		}
	}
	else if(ucBytePosition == 1)
	{
		ulWPControlWord = ulWPControlWord | ARM11_WP_BYTE1_ACCESS_CTRL_VALUE;
	}
	else
	{
		if((bByteRead || bByteWrite) && !(bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))
		{
			ulWPControlWord = ulWPControlWord | ARM11_WP_BYTE0_ACCESS_CTRL_VALUE;
		}
		else if(!(bByteRead || bByteWrite) && (bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))
		{
			ulWPControlWord = ulWPControlWord | ARM11_WP_HALF0_ACCESS_CTRL_VALUE;
		}
	}
	
	// We now need to decide what access type to look for.
	if ((bByteRead || bByteWrite) && !(bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))  
	{
		// This is byte access only.
		ulWPControlWord  = ulWPControlWord;
	}
	else
	{
		if (!(bByteRead || bByteWrite) && (bHalfRead || bHalfWrite) && !(bWordRead || bWordWrite))  
		{
			// This is half-word access only.
			ulWPControlWord  = ulWPControlWord;
		}
		else
		{
			if (!(bByteRead || bByteWrite) && !(bHalfRead || bHalfWrite) && (bWordRead || bWordWrite))  
            {
				// This is word access only.
				ulWPControlWord  = ulWPControlWord | ARM11_WP_WORD_ACCESS_CTRL_VALUE;
            }
			else
            {
				if ((bByteRead || bByteWrite) && (bHalfRead || bHalfWrite) && (bWordRead || bWordWrite))  
				{
					// This is don't care about access type.
					ulWPControlWord  = ulWPControlWord | ARM11_WP_WORD_ACCESS_CTRL_VALUE;
				}
				else
					return ML_Error_CantSetPoint;
            }
		}
	}
	
	if (ulResourceNo == HARDWARE_RESOURCE0)
	{
		// Setup WP0...
		tyWPResourceARM11Config.ucWP0Used = HARDWARE_WP;
		tyWPResourceARM11Config.ulWP0AddressValue = ulWPAddressWord;
		tyWPResourceARM11Config.ulWP0CtrlValue = ulWPControlWord;
	}
	else
		if (ulResourceNo == HARDWARE_RESOURCE1)
		{
			// Setup WP1...
			tyWPResourceARM11Config.ucWP1Used = HARDWARE_WP;
			tyWPResourceARM11Config.ulWP1AddressValue = ulWPAddressWord;
			tyWPResourceARM11Config.ulWP1CtrlValue = ulWPControlWord;
			
		}
		
		//TODO: else
		//TODO: ASSERT_NOW();
		
		return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_SetWatchProc
     Engineer: Suraj S
        Input: unsigned long ulAddress - watch data access to this address.
               unsigned long ulType - watchpoint type
               unsigned long ulDataType - determines which access types to watch for 
               unsigned long ulBound - This is a mask defined by RDI.h
               RDI_PointHandle   *phPointHandle - WP Handle
       Output: TyError - Mid Layer error code
  Description: Sets a hardware watchpoint.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError ML_SetWatchProc (unsigned long     ulAddress,
                         unsigned long     ulType,
                         unsigned long     ulDatatype,
                         unsigned long     ulBound,
                         RDI_PointHandle   *phPointHandle)
{
	TyError        ErrRet             = ML_ERROR_NO_ERROR;
	unsigned long  ulElementNo             = 0;
	boolean        bHandleAssigned    = FALSE;
	unsigned long  i                  = 0;
	unsigned short usWatchPointHandle = 0x0;
	
    if ((tyProcessorConfig.bARM7) ||
        (tyProcessorConfig.bARM9))
	{
		// Check whether there is already a watchpoint at "address"
		for (ulElementNo = 0; ulElementNo < usNumberOfBreakpoints; ulElementNo ++)
		{
			if(ulType == RDIDataPoint)
			{
				if ((pBPArray->BreakpointStructures[ulElementNo].ulBound == ulBound) &&
					(pBPArray->BreakpointStructures[ulElementNo].bWatchPoint) && 
					(RDIDataPoint == pBPArray->BreakpointStructures[ulElementNo].ulBPType))
					// WP already set for This data
					return ML_Error_PointInUse;
			}
			else if(ulType == RDIDataAndAddressPoint)
			{
				if ((pBPArray->BreakpointStructures[ulElementNo].ulBound == ulBound) &&
					(pBPArray->BreakpointStructures[ulElementNo].bWatchPoint) && 
					(RDIDataAndAddressPoint == pBPArray->BreakpointStructures[ulElementNo].ulBPType) &&
					(pBPArray->BreakpointStructures[ulElementNo].ulAddress == ulAddress))
					// WP already set for This data
					return ML_Error_PointInUse;
			}
			else
			{
				if ((pBPArray->BreakpointStructures[ulElementNo].ulAddress == ulAddress) &&
					(pBPArray->BreakpointStructures[ulElementNo].bWatchPoint) &&
					(RDIAddressPoint == pBPArray->BreakpointStructures[ulElementNo].ulBPType))
					// WP already set for This address
					return ML_Error_PointInUse;
			}

		}
		
		/* In case of Feroceon type processor like 88f6281, watchpoint resource 0
		seemingly not implemented for watchpoint. */
		// We need to try and set a hardware resource.
		if ((tyBPResourceConfig.ucWP0Used == NOT_USED) && 
			(!tyBPResourceConfig.bWP0Locked) &&
			(FEROCEON != tyTargetConfig.tyDeviceType))
		{
			ErrRet =  ML_SetHardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, HARDWARE_RESOURCE0);
			if (ErrRet == ML_ERROR_NO_ERROR) 
			{
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_WP0;
				goto WatchpointSetSuccessfully;
			}
		}
		
		// We need to try and set a hardware resource.
		if ((tyBPResourceConfig.ucWP1Used == NOT_USED) && (!tyBPResourceConfig.bWP1Locked))
		{
			ErrRet =  ML_SetHardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, HARDWARE_RESOURCE1);
			if (ErrRet == ML_ERROR_NO_ERROR) 
			{
				pBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = HARDWARE_WP1;
				goto WatchpointSetSuccessfully;
			}
		}
	}
    else if (tyProcessorConfig.bARM11)
	{
		// Check whether there is already a watchpoint at "address"
		for (ulElementNo = 0; ulElementNo < usNumberOfWatchpoints; ulElementNo ++)
		{
			if(ulType == RDIDataPoint)
			{
				if (pWPArray->WatchpointStructures[ulElementNo].ulBound == ulBound)
					// WP already set for This data
					return ML_Error_PointInUse;
			}
			else
			{
				if (pWPArray->WatchpointStructures[ulElementNo].ulAddress == ulAddress)
					// WP already set for This address
					return ML_Error_PointInUse;
			}
		}
		
		// We need to try and set a hardware resource.
		if ((tyWPResourceARM11Config.ucWP0Used == NOT_USED) && (!tyWPResourceARM11Config.bWP0Locked))
		{
			ErrRet =  ML_SetARM11HardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, HARDWARE_RESOURCE0);
			if (ErrRet == ML_ERROR_NO_ERROR) 
			{
				pWPArray->WatchpointStructures[usNumberOfWatchpoints].ucWPIDNo = HARDWARE_WP0;
				goto WatchpointSetSuccessfully;
			}
		}
		
		// We need to try and set a hardware resource.
		if ((tyWPResourceARM11Config.ucWP1Used == NOT_USED) && (!tyWPResourceARM11Config.bWP1Locked))
		{
			ErrRet =  ML_SetARM11HardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, HARDWARE_RESOURCE1);
			if (ErrRet == ML_ERROR_NO_ERROR) 
			{
				pWPArray->WatchpointStructures[usNumberOfWatchpoints].ucWPIDNo = HARDWARE_WP1;
				goto WatchpointSetSuccessfully;
			}
		}
	}
	
    else if (tyProcessorConfig.bCortexM3)
    {
        unsigned char ucWatchpointRes = 0;
		
        // Check whether there is already a watch-point at "address"
        for (ulElementNo = 0; ulElementNo < usNumberOfBreakpoints; ulElementNo++)
        {
            if (RDIDataPoint == ulType)
            {
                if ((pCortexBPArray->BreakpointStructures[ulElementNo].ulBound == ulBound) &&
					(pCortexBPArray->BreakpointStructures[ulElementNo].bWatchPoint) &&
					(RDIDataPoint == pCortexBPArray->BreakpointStructures[ulElementNo].ulBPType))
					// WP already set for This data
					return ML_Error_PointInUse;
			}
            else if (RDIDataAndAddressPoint == ulType)
            {
                if ((pCortexBPArray->BreakpointStructures[ulElementNo].ulBound == ulBound) &&
					(pCortexBPArray->BreakpointStructures[ulElementNo].bWatchPoint) &&
					(RDIDataAndAddressPoint == pCortexBPArray->BreakpointStructures[ulElementNo].ulBPType) &&
					(pCortexBPArray->BreakpointStructures[ulElementNo].ulAddress == ulAddress))
					// WP already set for This data
					return ML_Error_PointInUse;
			}
			else
			{
				if ((pCortexBPArray->BreakpointStructures[ulElementNo].ulAddress == ulAddress) &&
					(pCortexBPArray->BreakpointStructures[ulElementNo].bWatchPoint) &&
					(RDIAddressPoint == pCortexBPArray->BreakpointStructures[ulElementNo].ulBPType))
					// WP already set for This address
					return ML_Error_PointInUse;				
            }
        }  


		if ((ulType == RDIDataPoint) || (ulType == RDIDataAndAddressPoint))
		{
			/* Watch point recourse 0 and 1 are used for setting Data watchpoint */
			if (WP_RES_USED == pCortexBPArray->ucWatchBPResource[WATCHPOINT_RECOURSE_1])			
			{
				return ML_Error_PointInUse;
			}
			
			pCortexBPArray->ucWatchBPResource[WATCHPOINT_RECOURSE_1] = WP_RES_USED;

			for (ulElementNo = tyArmv7MWatchpointResArray.ulNumWatchpoint; ulElementNo > 0 ; ulElementNo--)
			{
				if (WP_RES_NOT_USED == pCortexBPArray->ucWatchBPResource[ulElementNo - 1])
				{
					ucWatchpointRes = (unsigned char)(ulElementNo - 1);
					break;
				}
			}
			
			if (ulElementNo == 0)
			{
				return ML_Error_CantSetPoint;
			}
			ErrRet =  ML_SetCortexMHardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, ucWatchpointRes);
			if (ErrRet == ML_ERROR_NO_ERROR)
			{
				//pCortexBPArray->ucWatchBPResource[WATCHPOINT_RECOURSE_1] = WP_RES_USED;
				pCortexBPArray->ucWatchBPResource[ucWatchpointRes] = WP_RES_USED;
				//pCortexBPArray->BreakpointStructures[WATCHPOINT_RECOURSE_1].ucBPIDNo = WATCHPOINT_RECOURSE_1;
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = ucWatchpointRes;
				goto WatchpointSetSuccessfully;
			}								
		}
		
		else
		{
			for (ulElementNo = tyArmv7MWatchpointResArray.ulNumWatchpoint; ulElementNo > 0 ; ulElementNo--)
			{
				if (WP_RES_NOT_USED == pCortexBPArray->ucWatchBPResource[ulElementNo - 1])
				{
					ucWatchpointRes = (unsigned char)(ulElementNo - 1);
					break;
				}
			}
			
			if (ulElementNo == 0)
			{
				return ML_Error_CantSetPoint;
			}
			
			ErrRet =  ML_SetCortexMHardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, ucWatchpointRes);			
			if (ErrRet == ML_ERROR_NO_ERROR)
			{
				pCortexBPArray->ucWatchBPResource[ucWatchpointRes] = WP_RES_USED;
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = ucWatchpointRes;
				goto WatchpointSetSuccessfully;
			}
		}  

    }

	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		unsigned char ucWatchpointRes = 0;
		
        // Check whether there is already a watch-point at "address"
		if ((RDIDataPoint == ulType) ||
			(RDIDataAndAddressPoint == ulType))
		{
			return ML_Error_CantSetPoint;
		}
#if 0	/* We cann't set Data watchpoint point and Data and address watchpoint. */		
        for (ulElementNo = 0; ulElementNo < usNumberOfBreakpoints; ulElementNo++)
        {
            if (RDIDataPoint == ulType)
            {
                if ((pCortexBPArray->BreakpointStructures[ulElementNo].ulBound == ulBound) &&
					(pCortexBPArray->BreakpointStructures[ulElementNo].bWatchPoint))
					// WP already set for This data
					return ML_Error_PointInUse;
			}
			else
			{
				if ((pCortexBPArray->BreakpointStructures[ulElementNo].ulAddress == ulAddress) &&
					(pCortexBPArray->BreakpointStructures[ulElementNo].bWatchPoint))
					// WP already set for This address
					return ML_Error_PointInUse;				
            }
        }  
		
		
		if ((ulType == RDIDataPoint) || (ulType == RDIDataAndAddressPoint))
		{
			/* Watch point recourse 0 and 1 are used for setting Data watchpoint */
			if (WP_RES_USED == pCortexBPArray->ucWatchBPResource[WATCHPOINT_RECOURSE_1])			
			{
				return ML_Error_PointInUse;
			}
			
			pCortexBPArray->ucWatchBPResource[WATCHPOINT_RECOURSE_1] = WP_RES_USED;
			
			for (ulElementNo = 0; ulElementNo < tyArmv7AWatchpointResArray.ulNumulWatchPoint; ulElementNo++)
			{
				if (WP_RES_NOT_USED == pCortexBPArray->ucWatchBPResource[ulElementNo])
				{
					ucWatchpointRes = (unsigned char)(ulElementNo);
					break;
				}
			}
			
			if (ulElementNo == tyArmv7AWatchpointResArray.ulNumulWatchPoint)
			{
				return ML_Error_CantSetPoint;
			}
			ErrRet =  ML_SetArmv7AHardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, ucWatchpointRes);
			if (ErrRet == ML_ERROR_NO_ERROR)
			{			
				pCortexBPArray->ucWatchBPResource[ucWatchpointRes] = WP_RES_USED;			
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = ucWatchpointRes;
				goto WatchpointSetSuccessfully;
			}								
		}
		
		else
#endif
		{
			for (ulElementNo = 0; ulElementNo < tyArmv7AWatchpointResArray.ulNumWatchPoint; ulElementNo++)
			{
				if (WP_RES_NOT_USED == pCortexBPArray->ucWatchBPResource[ulElementNo])
				{
					ucWatchpointRes = (unsigned char)(ulElementNo);
					break;
				}
			}
			
			if (ulElementNo == tyArmv7AWatchpointResArray.ulNumWatchPoint)
			{
				return ML_Error_CantSetPoint;
			}
			
			ErrRet =  ML_SetArmv7AHardwareWatchPoint (ulAddress, ulType, ulDatatype, ulBound, ucWatchpointRes);			
			if (ErrRet == ML_ERROR_NO_ERROR)
			{
				pCortexBPArray->ucWatchBPResource[ucWatchpointRes] = WP_RES_USED;
				pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ucBPIDNo = ucWatchpointRes;
				goto WatchpointSetSuccessfully;
			}
		}
	}

    // The requested breakpoint cannot be set with the available resources.	
    return ML_Error_CantSetPoint;

WatchpointSetSuccessfully:

    if ((tyProcessorConfig.bARM7) ||
        (tyProcessorConfig.bARM9))
    {
        // We need to check that the handle is not already in use...
        while (!bHandleAssigned)
        {
            for (i=0; i < usNumberOfBreakpoints; i++)
            {
                if (pBPArray->BreakpointStructures[i].usBreakpointId == usWatchPointHandle)
                {
                    usWatchPointHandle ++;
                    break;
                }
            }
            if (i >= usNumberOfBreakpoints)
                bHandleAssigned = TRUE;
        }

        pBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId = usWatchPointHandle;
        pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulAddress      = ulAddress;
        pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBPType       = ulType;
        pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulDataType     = ulDatatype;
        pBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBound        = ulBound;
        pBPArray->BreakpointStructures[usNumberOfBreakpoints].bWatchPoint    = TRUE;

        // Return the handle...
        *phPointHandle = pBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId;

        // We have just set a watchpoint, increment the count...
        usNumberOfBreakpoints++;
    }
    else if (tyProcessorConfig.bARM11)
    {
        // We need to check that the handle is not already in use...
        while (!bHandleAssigned)
        {
            for (i=0; i < usNumberOfWatchpoints; i++)
            {
                if (pWPArray->WatchpointStructures[i].usWatchpointId == usWatchPointHandle)
                {
                    usWatchPointHandle ++;
                    break;
                }
            }
            if (i >= usNumberOfWatchpoints)
                bHandleAssigned = TRUE;
        }

        pWPArray->WatchpointStructures[usNumberOfWatchpoints].usWatchpointId = usWatchPointHandle;
        pWPArray->WatchpointStructures[usNumberOfWatchpoints].ulAddress      = ulAddress;
        pWPArray->WatchpointStructures[usNumberOfWatchpoints].ulWPType       = ulType;
        pWPArray->WatchpointStructures[usNumberOfWatchpoints].ulDataType     = ulDatatype;
        pWPArray->WatchpointStructures[usNumberOfWatchpoints].ulBound        = ulBound;

        // Return the handle...
        *phPointHandle = pWPArray->WatchpointStructures[usNumberOfWatchpoints].usWatchpointId;

        // We have just set a watchpoint, increment the count...
        usNumberOfWatchpoints++;
    }

    else if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
    {
        bHandleAssigned     = FALSE;
        //usWatchPointHandle  = 0;

        /* We need to check that the handle is not already in use. */
        do
        {
            for (i=0; i < usNumberOfBreakpoints; i++)
            {
                if (pCortexBPArray->BreakpointStructures[i].usBreakpointId == usWatchPointHandle)
                {
                    usWatchPointHandle ++;
                    break;
                }
            }
            if (i >= usNumberOfBreakpoints)
            {
                bHandleAssigned = TRUE;
            }
        } while (FALSE == bHandleAssigned);

        pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId = usWatchPointHandle;
        pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulAddress      = ulAddress;
        pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBPType       = ulType;
        pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulDataType     = ulDatatype;
        pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].ulBound        = ulBound;
        pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].bWatchPoint    = TRUE;

        // Return the handle...
        *phPointHandle = pCortexBPArray->BreakpointStructures[usNumberOfBreakpoints].usBreakpointId;

        // We have just set a watch-point, increment the count...
        usNumberOfBreakpoints++;
    }

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetupBPRegsARM11
    Engineer: Roshan T R
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Sets up BVR and BCR registers for ARM11
Date          Initials     Description
22-Aug-2009   RTR          Initial
****************************************************************************/
TyError ML_SetupBPRegsARM11(void)
{
    TyError ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (BP0_VALUE_REGISTER, 
                                        tyBPResourceARM11Config.ulBP0AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (BP0_CTRL_REGISTER, 
		                                tyBPResourceARM11Config.ulBP0CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (BP1_VALUE_REGISTER, 
		                                tyBPResourceARM11Config.ulBP1AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
	
	ErrRet = ML_WriteCP14DebugRegARM11 (BP1_CTRL_REGISTER, 
		                                tyBPResourceARM11Config.ulBP1CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
    
	ErrRet = ML_WriteCP14DebugRegARM11 (BP2_VALUE_REGISTER, 
		                                tyBPResourceARM11Config.ulBP2AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
	
	ErrRet = ML_WriteCP14DebugRegARM11 (BP2_CTRL_REGISTER, 
		                                tyBPResourceARM11Config.ulBP2CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (BP3_VALUE_REGISTER, 
		                                tyBPResourceARM11Config.ulBP3AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
	
	ErrRet = ML_WriteCP14DebugRegARM11 (BP3_CTRL_REGISTER, 
		                                tyBPResourceARM11Config.ulBP3CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (BP4_VALUE_REGISTER, 
		                                tyBPResourceARM11Config.ulBP4AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
	
	ErrRet = ML_WriteCP14DebugRegARM11 (BP4_CTRL_REGISTER, 
		                                tyBPResourceARM11Config.ulBP4CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (BP5_VALUE_REGISTER, 
		                                tyBPResourceARM11Config.ulBP5AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
	
	ErrRet = ML_WriteCP14DebugRegARM11 (BP5_CTRL_REGISTER, 
		                                tyBPResourceARM11Config.ulBP5CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetupWPRegsARM11
    Engineer: Roshan T R
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Sets up WVR and WCR registers for ARM11
Date          Initials     Description
12-Sep-2009   RTR          Initial
****************************************************************************/
TyError ML_SetupWPRegsARM11(void)
{
    TyError ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (WP0_VALUE_REGISTER, 
                                        tyWPResourceARM11Config.ulWP0AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (WP0_CTRL_REGISTER, 
		                                tyWPResourceARM11Config.ulWP0CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;

	ErrRet = ML_WriteCP14DebugRegARM11 (WP1_VALUE_REGISTER, 
		                                tyWPResourceARM11Config.ulWP1AddressValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
	
	ErrRet = ML_WriteCP14DebugRegARM11 (WP1_CTRL_REGISTER, 
		                                tyWPResourceARM11Config.ulWP1CtrlValue);
    if (ErrRet!=ML_ERROR_NO_ERROR)
        return ErrRet;
    
    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetupWatchpoints
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Sets up the ICEBreaker module for watchpoints and breakpoints
              depending on target type.
Date          Initials     Description
23-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_SetupWatchpoints (void)

{
   TyError ErrRet;

   ErrRet = ML_Disable_EmbeddedICERT ();
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;

   ErrRet = ML_WriteICEBreaker (WP0_ADDR_V, tyBPResourceConfig.ulWP0AddressValue );  
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP0_ADDR_M, tyBPResourceConfig.ulWP0AddressMask  );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP0_DATA_V, tyBPResourceConfig.ulWP0DataValue    );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP0_DATA_M, tyBPResourceConfig.ulWP0DataMask     );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
	ErrRet = ML_WriteICEBreaker (WP0_CTRL_V, tyBPResourceConfig.ulWP0CtrlValue    );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP0_CTRL_M, tyBPResourceConfig.ulWP0CtrlMask     );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   
   ErrRet = ML_WriteICEBreaker (WP1_ADDR_V, tyBPResourceConfig.ulWP1AddressValue );  
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP1_ADDR_M, tyBPResourceConfig.ulWP1AddressMask  );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP1_DATA_V, tyBPResourceConfig.ulWP1DataValue	   );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;                                                             
   ErrRet = ML_WriteICEBreaker (WP1_DATA_M, tyBPResourceConfig.ulWP1DataMask	   );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
	ErrRet = ML_WriteICEBreaker (WP1_CTRL_V, tyBPResourceConfig.ulWP1CtrlValue	   );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   ErrRet = ML_WriteICEBreaker (WP1_CTRL_M, tyBPResourceConfig.ulWP1CtrlMask	   );
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   
   ErrRet = ML_Enable_EmbeddedICERT ();
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_WaitForSystemSpeedCompletion
    Engineer: Suraj S
       Input: None
      Output: TyError - Mid Layer error code
 Description: Loops until processor completes the current system speed
              instruction and exits back into debug speed mode.
Date          Initials     Description
9-Jul-2007      SJ         Initial
4-Apr-2008      SJ         ulGlobalExecuting variable is reset for infocall 
                           from ARM GDB Server
****************************************************************************/
TyError ML_WaitForSystemSpeedCompletion(void)

{
	unsigned long ulDebugStatusValue = 0;
	unsigned long ulTimeoutvalue;
	TyError ErrRet;

	ulTimeoutvalue = 0;

	if(tyProcessorConfig.bARM11)
	{
		do 
		{
			ErrRet = ML_ReadDSCR(&ulDebugStatusValue);
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
			ulTimeoutvalue ++;
			if (ulTimeoutvalue >= MAX_LOOP_CYCLES)
				return ML_Error_TargetBroken;

		} while ((ulDebugStatusValue & ARM11_DEBUG_MODE) != ARM11_DEBUG_MODE);
		
	}
	else
	{
		do 
		{
			ErrRet = ML_ReadICEBreaker (DBG_STATUS_REG, &ulDebugStatusValue);
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
			ulTimeoutvalue ++;
			if (ulTimeoutvalue >= MAX_LOOP_CYCLES)
				return ML_Error_TargetBroken;

		} while ((ulDebugStatusValue & ARM_DEBUG_MODE) != ARM_DEBUG_MODE);
		
	}        

	ulGlobalExecuting = 0;

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_DisableWatchpoints.
     Engineer: Suraj S
        Input: NONE
       Output: TyError - Mid Layer error code
  Description: Disables the watchpoints and breakpoints.
Date          Initials     Description
9-Jul-2007      SJ         Initial
****************************************************************************/
static TyError ML_DisableWatchpoints (void)

{
   TyError ErrRet;

   // Set the ICE breaker register scan chain.  
   ErrRet = ML_Disable_EmbeddedICERT ();
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;

   ErrRet = ML_WriteICEBreaker (WP1_CTRL_V, WATCHPOINTS_DISABLED);
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;

   ErrRet = ML_WriteICEBreaker (WP0_CTRL_V, WATCHPOINTS_DISABLED);
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;

   ErrRet = ML_Enable_EmbeddedICERT ();
   if (ErrRet!=ML_ERROR_NO_ERROR)
      return ErrRet;
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_CheckForDataAbort
     Engineer: Suraj S
        Input: None
       Output: TyError - Mid Layer error code
  Description: Checks if a data abort occured
Date           Initials     Description
11-Jul-2007      SJ         Initial
02-Nov-2009		 JCK		Added support for Cortex-M3
****************************************************************************/
TyError ML_CheckForDataAbort (void)

{
	TyError ErrRet;
	TyArmProcType TyProcType = NOCORE;
	unsigned char ucDataAbortOccured = 0;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if (tyProcessorConfig.bCortexM3)
		TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		TyProcType = CORTEXA;

   if (bUSBDevice)
      {
         ErrRet = DL_OPXD_Arm_CheckForDataAbort(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, &ucDataAbortOccured);
         ulGlobalScanChain = 0x1;
         if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
            return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
            
      if (ucDataAbortOccured == 1)
         return ML_Error_DataAbort;
      }
   else
      return ML_ERROR_NO_USB_DEVICE_CONNECTED;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_BreakpointAlreadySet
     Engineer: Suraj S
        Input: unsigned long ulAddress - Address to check for breakpoint
               RDI_PointHandle *pBPHandle - Breakpoint handle
       Output: TRUE if there is already a breakpoint set at address
  Description: Checks if there is already a breakpoint set at mem location
Date           Initials     Description
17-Jul-2007      SJ         Initial
****************************************************************************/
boolean ML_BreakpointAlreadySet(unsigned long ulAddress, 
                                RDI_PointHandle *pBPHandle)

{
	unsigned long i;

	if (pBPHandle != NULL)
		*pBPHandle = RDI_NoHandle;

	// Check if there is a breakpoint set at address.
	for (i=0; i < usNumberOfBreakpoints; i++)
	{
		if ((tyProcessorConfig.bCortexM3) ||
			(tyProcessorConfig.bCortexA8) ||
			(tyProcessorConfig.bCortexA9))
		{
			if ((pCortexBPArray->BreakpointStructures[i].ulAddress == ulAddress) &&
				(pCortexBPArray->BreakpointStructures[i].bWatchPoint == FALSE))
			{
				if (pBPHandle != NULL)
					*pBPHandle = (unsigned long )pCortexBPArray->BreakpointStructures[i].usBreakpointId;
				
				return TRUE;
			}
		}
		else
		{
			if ((pBPArray->BreakpointStructures[i].ulAddress == ulAddress) &&
				(pBPArray->BreakpointStructures[i].bWatchPoint == FALSE))
			{
				if (pBPHandle != NULL)
					*pBPHandle = (unsigned long )pBPArray->BreakpointStructures[i].usBreakpointId;
				
				return TRUE;
			}
		}
		
	}
	return FALSE;
}

/****************************************************************************
    Function: ML_FindAndReplaceAllBreakpointsForWrite
    Engineer: Suraj S
       Input: unsigned long ulStartAddress - Start address of array.
              unsigned char *pucDataValues - Data values to write.
              unsigned long ulDataCount - Count of data values to write.
      Output: NONE
 Description: Checks for the existence of BPs within the data values to write,
              and if found, replaces with original instruction data
Date           Initials     Description
11-Jul-2007      SJ         Initial
29-Apr-2010		 JCK		Cortex-A8 Support Added.
29-Apr-2010		 JCK		ARM11 Support Added
****************************************************************************/
static void ML_FindAndReplaceAllBPsForWrite(unsigned long ulStartAddress, 
                                            unsigned char *pucDataValues, 
                                            unsigned long ulDataCount)

{
   unsigned long ulBPNo;
   unsigned char *pucPreviousInstructionData;
   unsigned long *pulPreviousInstructionData;
   unsigned char *pucLocalDataValues;
   unsigned long ulEndOfBP;
   unsigned long ulEndAddress;
   unsigned long ulLocalStartAddress;
   unsigned long ulByteNo;

   ulEndAddress = ulStartAddress + ulDataCount;
      
   for  (ulBPNo = 0; ulBPNo < usNumberOfBreakpoints; ulBPNo++)
      {
      pucLocalDataValues = pucDataValues;
	  if ((tyProcessorConfig.bCortexM3) ||
		  (tyProcessorConfig.bCortexA8) ||
		  (tyProcessorConfig.bCortexA9))
	  {
		  pulPreviousInstructionData =  &pCortexBPArray->BreakpointStructures[ulBPNo].ulPreviousInstructionData;
	  } 
	  else
	  {
		  pulPreviousInstructionData =  &pBPArray->BreakpointStructures[ulBPNo].ulPreviousInstructionData;
	  }
      
      ulLocalStartAddress = ulStartAddress;

      // The start address is not word aligned.
      
      while (ulLocalStartAddress < ulEndAddress)
         {
         pucPreviousInstructionData = (unsigned char *) pulPreviousInstructionData;

		 if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
		 {
			 ulEndOfBP = ((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress & WORD_ALIGNED_MASK)+3);
			 ulByteNo = (ulLocalStartAddress %4);
			 if ((pCortexBPArray->BreakpointStructures[ulBPNo].ulBPType) & RDIPoint_16Bit)
			 {
				 // These are 16 bit breakpoints.
				 ulEndOfBP = ((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress & HALF_WORD_ALIGNED_MASK)+1);
				 if((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress & 0x2) && (ulByteNo >1)) 
					 // This is a half word aligned 16 bit breakpoint.
					 ulByteNo -=2;
			 }
			 // if there is a software breakpoint at this address write to the array not memory.
			 if ((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress <= ulLocalStartAddress) &&
				 (ulLocalStartAddress <=  ulEndOfBP) &&
				 ((pCortexBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP0) || (pCortexBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP1))) 
			 {
				 // If so, write to the first byte of saved instruction data...
				 *(pucPreviousInstructionData+ulByteNo) = *pucLocalDataValues;

				 if ((pCortexBPArray->BreakpointStructures[ulBPNo].ulBPType) & RDIPoint_16Bit)
				 {
					*pucLocalDataValues =  (unsigned char) (CORTEX_THUMB_BKPT_INST >> (ulByteNo * 8));
				 } 
				 else
				 {
					*pucLocalDataValues =  (unsigned char) (CORTEX_ARM_BKPT_INST >> (ulByteNo * 8));
				 }
				 
				 	
			 }
		 } 
		 else
		 {
			 ulEndOfBP = ((pBPArray->BreakpointStructures[ulBPNo].ulAddress & WORD_ALIGNED_MASK)+3);
			 ulByteNo = (ulLocalStartAddress %4);
			 if ((pBPArray->BreakpointStructures[ulBPNo].ulBPType) & RDIPoint_16Bit)
			 {
				 // These are 16 bit breakpoints.
				 ulEndOfBP = ((pBPArray->BreakpointStructures[ulBPNo].ulAddress & HALF_WORD_ALIGNED_MASK)+1);
				 if((pBPArray->BreakpointStructures[ulBPNo].ulAddress & 0x2) && (ulByteNo >1)) 
					 // This is a half word aligned 16 bit breakpoint.
					 ulByteNo -=2;
			 }
			 // if there is a software breakpoint at this address write to the array not memory.
			 if ((pBPArray->BreakpointStructures[ulBPNo].ulAddress <= ulLocalStartAddress) &&
				 (ulLocalStartAddress <=  ulEndOfBP) &&
				 ((pBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP0) || (pBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP1))) 
			 {
				 // If so, write to the first byte of saved instruction data...
				 *(pucPreviousInstructionData+ulByteNo) = *pucLocalDataValues;

				 if (tyProcessorConfig.bARM11)
				 {
					 if ((pBPArray->BreakpointStructures[ulBPNo].ulBPType) & RDIPoint_16Bit)
					 {
						 *pucLocalDataValues =  (unsigned char) (ARM11_THUMB_BKPT_INST >> (ulByteNo * 8));
					 } 
					 else
					 {
						 *pucLocalDataValues =  (unsigned char) (ARM11_ARM_BKPT_INST >> (ulByteNo * 8));
					 }
					 	
				 } 
				 else
				 {
					 *pucLocalDataValues =  BP_INST_DATA_BYTE;	
				 }
				 
			 }
		 }
         
         pucLocalDataValues ++;
         ulLocalStartAddress ++;
         }
      }  
   return; 
}

/****************************************************************************
     Function: ML_FindAndReplaceAllBreakpointsforRead
     Engineer: Suraj S
        Input: unsigned long ulStartAddress - Start address of array.
               unsigned char *pucDataValues - Data values already read.
               unsigned long ulDataCount - Count of data values read.
       Output: NONE
  Description: Checks for the existence of BPs within the data values read,
               and if found, replace with original instruction data
Date           Initials     Description
18-Jul-2007      SJ         Initial
****************************************************************************/
static void ML_FindAndReplaceAllBPsforRead(unsigned long ulStartAddress, 
                                           unsigned char *pucDataValues, 
                                           unsigned long ulDataCount)

{
   unsigned long ulBPNo;
   unsigned char *pucPreviousInstructionData;
   unsigned long *pulPreviousInstructionData;
   unsigned char *pucLocalDataValues;
   unsigned long ulEndOfBP;
   unsigned long ulEndAddress;
   unsigned long ulLocalStartAddress;
   unsigned long ulByteNo;

   ulEndAddress = ulStartAddress + ulDataCount;
      
   for (ulBPNo = 0; ulBPNo <usNumberOfBreakpoints; ulBPNo++)
      {
      ulLocalStartAddress = ulStartAddress;
	  if ((tyProcessorConfig.bCortexM3) ||
		  (tyProcessorConfig.bCortexA8) ||
		  (tyProcessorConfig.bCortexA9))
	  {
		  pulPreviousInstructionData =  &pCortexBPArray->BreakpointStructures[ulBPNo].ulPreviousInstructionData;
	  } 
	  else
	  {
		  pulPreviousInstructionData =  &pBPArray->BreakpointStructures[ulBPNo].ulPreviousInstructionData;
	  }
      
      pucLocalDataValues = pucDataValues;

      while (ulLocalStartAddress < ulEndAddress)
         {
         pucPreviousInstructionData = (unsigned char *) pulPreviousInstructionData;

		 if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
		 {
			 ulEndOfBP = ((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress & WORD_ALIGNED_MASK)+3);
			 ulByteNo = (ulLocalStartAddress %4);
			 // Check if this is a 16 bit breakpoint and that it is a software BP
			 if (((pCortexBPArray->BreakpointStructures[ulBPNo].ulBPType) & RDIPoint_16Bit) == RDIPoint_16Bit)  
			 {
				 // These are 16 bit breakpoints.
				 ulEndOfBP = ((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress & HALF_WORD_ALIGNED_MASK)+1);
				 if((pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress & 0x2) && (ulByteNo >1)) 
					 // This is a half word aligned 16 bit breakpoint.
					 ulByteNo -=2;
			 }
			 // if there is a software breakpoint at this address read from array not memory.
			 if (( pCortexBPArray->BreakpointStructures[ulBPNo].ulAddress <= ulLocalStartAddress) &&
				 ( ulLocalStartAddress <=  ulEndOfBP) && 
				 ((pCortexBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP0) || (pCortexBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP1))) 
			 {
				 // If so, replace with first byte of saved instruction data...
				 *pucLocalDataValues = *(pucPreviousInstructionData+ulByteNo);
			 }
		 } 
		 else
		 {
			ulEndOfBP = ((pBPArray->BreakpointStructures[ulBPNo].ulAddress & WORD_ALIGNED_MASK)+3);
			ulByteNo = (ulLocalStartAddress %4);
			// Check if this is a 16 bit breakpoint and that it is a software BP
			if (((pBPArray->BreakpointStructures[ulBPNo].ulBPType) & RDIPoint_16Bit) == RDIPoint_16Bit)  
			{
				// These are 16 bit breakpoints.
				ulEndOfBP = ((pBPArray->BreakpointStructures[ulBPNo].ulAddress & HALF_WORD_ALIGNED_MASK)+1);
				if((pBPArray->BreakpointStructures[ulBPNo].ulAddress & 0x2) && (ulByteNo >1)) 
				// This is a half word aligned 16 bit breakpoint.
				ulByteNo -=2;
			}
			// if there is a software breakpoint at this address read from array not memory.
			if (( pBPArray->BreakpointStructures[ulBPNo].ulAddress <= ulLocalStartAddress) &&
				( ulLocalStartAddress <=  ulEndOfBP) && 
				((pBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP0) || (pBPArray->BreakpointStructures[ulBPNo].ucBPIDNo == SOFTWARE_BP1))) 
			{
				// If so, replace with first byte of saved instruction data...
				*pucLocalDataValues = *(pucPreviousInstructionData+ulByteNo);
			}
		 }
         
         pucLocalDataValues ++;
         ulLocalStartAddress ++;
         }
      }  
   return;
}

/****************************************************************************
    Function: ML_GetProcessorInfo
    Engineer: Suraj
       Input: boolean *pbThumbSupport - TRUE if processor supports 16 bit 
              instructions
              boolean *pbCommsChannel - TRUE if processor supports DCC
      Output: NONE
 Description: Allows RDI layer to get processor configuration.
Date           Initials     Description
31-Jul-2007      SJ         Initial
*****************************************************************************/
void ML_GetProcessorInfo (boolean *pbThumbSupport, boolean *pbCommsChannel)
{
	*pbThumbSupport = tyProcessorConfig.bThumb;
	*pbCommsChannel = tyProcessorConfig.bDCC;
}

/****************************************************************************
    Function: ML_ReadRegisters
    Engineer: Suraj S
       Input: unsigned long *pulRegisters - Pointer to register read values
      Output: TyError - Mid Layer error code 
 Description: Reads processor registers from core
Date           Initials     Description
17-Jul-2007      SJ         Initial
24-Mar-2008		 SJ		    Added 968 core support
21-Apr-2010		 JCK		Cortex-A8 Support Added
****************************************************************************/
TyError ML_ReadRegisters (unsigned long *pulRegisters)

{  
	unsigned long i =0, j;
	TyError ErrRet;
	TyArmProcType TyProcType = NOCORE;
	unsigned long *pulRegValues;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
    else if (tyProcessorConfig.bCortexM3)
        TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
        TyProcType = CORTEXA;

	pulRegValues = (unsigned long *)malloc(sizeof(RegArray));

	if (bUSBDevice)
	{
		ErrRet = DL_OPXD_Arm_ReadRegisters(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulRegValues, sizeof(RegArray));
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		{
			free(pulRegValues);
			return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
		}
      
		// Copy over the first 8 registers...
		for (i = REG_ARRAY_R0; i <= REG_ARRAY_R7; i++)
		{
			pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + i] = *(pulRegValues+i);
		}

		// Copy over the PC register
		pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + REG_ARRAY_PC] = *(pulRegValues+i); i++;
   
		// Copy over the CPSR register
		pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + REG_ARRAY_CPSR] = *(pulRegValues+i); i++;
      
		// Copy the USER mode R8 to R14 registers
		for (j=REG_ARRAY_R8; j <=REG_ARRAY_R14; j++)
		{
			pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + j] = *(pulRegValues+i); i++;
		}

		// Copy the FIQ mode R8 to R14 registers
		for (j=REG_ARRAY_R8; j <=REG_ARRAY_R14; j++)
		{
			pulRegisters[(REG_ARRAY_FIQ_MODE * MAXREGISTERS) + j] = *(pulRegValues+i); i++;
		}

		// Copy over the FIQ mode SPSR register
		pulRegisters[(REG_ARRAY_FIQ_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]  = *(pulRegValues+i); 
		i++;

		// Copy over the IRQ mode R13 register
		pulRegisters[(REG_ARRAY_IRQ_MODE * MAXREGISTERS) + REG_ARRAY_R13]   = *(pulRegValues+i); 
		i++;

		// Copy over the IRQ mode R14 register
		pulRegisters[(REG_ARRAY_IRQ_MODE * MAXREGISTERS) + REG_ARRAY_R14]   = *(pulRegValues+i); 
		i++;

		// Copy over the IRQ mode SPSR register
		pulRegisters[(REG_ARRAY_IRQ_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]  = *(pulRegValues+i); 
		i++;

		// Copy over the SVC mode R13 register
		pulRegisters[(REG_ARRAY_SVC_MODE * MAXREGISTERS) + REG_ARRAY_R13]   = *(pulRegValues+i); 
		i++;

		// Copy over the SVC mode R14 register
		pulRegisters[(REG_ARRAY_SVC_MODE * MAXREGISTERS) + REG_ARRAY_R14]   = *(pulRegValues+i); 
		i++;

		// Copy over the SVC mode SPSR register
		pulRegisters[(REG_ARRAY_SVC_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]  = *(pulRegValues+i); 
		i++;

		// Copy over the ABORT mode R13 register
		pulRegisters[(REG_ARRAY_ABORT_MODE * MAXREGISTERS) + REG_ARRAY_R13] = *(pulRegValues+i); 
		i++;

		// Copy over the ABORT mode R14 register
		pulRegisters[(REG_ARRAY_ABORT_MODE * MAXREGISTERS) + REG_ARRAY_R14] = *(pulRegValues+i); 
		i++;

		// Copy over the ABORT mode SPSR register
		pulRegisters[(REG_ARRAY_ABORT_MODE * MAXREGISTERS) + REG_ARRAY_SPSR] = *(pulRegValues+i); 
		i++;

		// Copy over the UNDEF mode R13 register
		pulRegisters[(REG_ARRAY_UNDEF_MODE * MAXREGISTERS) + REG_ARRAY_R13] = *(pulRegValues+i); 
		i++;

		// Copy over the UNDEF mode R14 register
		pulRegisters[(REG_ARRAY_UNDEF_MODE * MAXREGISTERS) + REG_ARRAY_R14] = *(pulRegValues+i); 
		i++;

		// Copy over the UNDEF mode SPSR register
		pulRegisters[(REG_ARRAY_UNDEF_MODE * MAXREGISTERS) + REG_ARRAY_SPSR] = *(pulRegValues+i);
		i++;

		/* Needs to change */
		if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			// Copy over the MON mode R13 register
			pulRegisters[(REG_ARRAY_MON_MODE * MAXREGISTERS) + REG_ARRAY_R13] = *(pulRegValues+i); i++;
			
			// Copy over the MON mode R14 register
			pulRegisters[(REG_ARRAY_MON_MODE * MAXREGISTERS) + REG_ARRAY_R14] = *(pulRegValues+i); i++;
			
			// Copy over the MON mode SPSR register
			pulRegisters[(REG_ARRAY_MON_MODE * MAXREGISTERS) + REG_ARRAY_SPSR] = *(pulRegValues+i);
		}
		

		//Take offset from the PC to account for instructions executed in debug mode.
		//ARM9's
		if (tyProcessorConfig.bARM9)
		{
			if (bWatchPoint)
				RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x2C;                            
			else 
				RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x28;                            
		}
		//ARM7TDMI and ARM7DI
		else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bSynthesisedCore))
		{
			if (bUserHalt)
				RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x28;                            
			else
				RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x2C;                            
		}
		//ARM7TDMI-S
		else if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bSynthesisedCore))
			RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x28;                            
		else if (tyProcessorConfig.bARM11)
		{
				if(bGlobalThumb)
				{
					RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x4; //if ARM
				}
				else
				{
					RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x8; //if THUMB
				}
				//RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x0; //if JAVA
		}
		/* TODO else 
		ASSERT_NOW();*/  
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;

	// Read the co-processor registers.
	switch (tyProcessorConfig.tyCoProcessorType)
	{
		default:
		case NO_COPROCESSOR:
			break;
		case ARM720T_CP:
			ErrRet = CL_ReadARM720CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			break;
		case ARM740T_CP:
			ErrRet = CL_ReadARM740CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			break;
		case ARM920T_CP:
		case ARM922T_CP:
			ErrRet = CL_ReadARM920_922CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			ulGlobalScanChain = 0x1;
			break;
		case ARM926EJS_CP:
			ErrRet = CL_ReadARM926CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			ulGlobalScanChain = 0x1;
			break;
		case ARM940T_CP:
		case ARM946ES_CP:
			ErrRet = CL_ReadARM940_946CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			ulGlobalScanChain = 0x1;
			break;
		case ARM966ES_CP:
			ErrRet = CL_ReadARM966CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}      
			ulGlobalScanChain = 0x1;
			break;

		case ARM968ES_CP:
			ErrRet = CL_ReadARM968CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			ulGlobalScanChain = 0x1;
			break;

		case ARM1136JFS_CP:
			ErrRet = CL_ReadARM1136CoProcRegisters  ();
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				free(pulRegValues);
				return ErrRet;
			}
			ulGlobalScanChain = 0x1;
			break;
    }

    free(pulRegValues);

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_CortexMReadRegisters
    Engineer: Deepa V.A.
       Input: unsigned long *pulRegisters - Pointer to register read values
      Output: TyError - Mid Layer error code 
 Description: Reads processor registers from core
Date           Initials     Description
29-Oct-2009      DVA         Initial
****************************************************************************/
TyError ML_CortexMReadRegisters (unsigned long *pulRegisters)

{  
    unsigned long i =0;
    TyError ErrRet;
    TyArmProcType TyProcType = NOCORE;
    unsigned long *pulRegValues;
    unsigned long ulTempCtrlVal;

    TyProcType = CORTEXM;
    pulRegValues = (unsigned long *)malloc(sizeof(CortexMRegArray));

    if (bUSBDevice)
    {
        ErrRet = DL_OPXD_Arm_ReadRegisters(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulRegValues, sizeof(CortexMRegArray));
        if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
        {
            free(pulRegValues);
            return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
        }

        // Copy over the first 15 registers: R0-R14
        for (i = REG_R0; i <= REG_LR; i++)
        {
            pulRegisters[i] = *(pulRegValues+i);
        }

        // Copy over the PC register: 15th in the incoming array
        pulRegisters[REG_PC] = *(pulRegValues+i);
        i++;

        // Copy over the xPSR register: 16th in the incoming array
        pulRegisters[REG_xPSR] = *(pulRegValues+i);

        /* Below three are parsed from the xPSR value. */
        // Extract APSR From xPSR
        pulRegisters[REG_APSR] = (pulRegisters[REG_xPSR] & 0xF8000000);
        // Extract IPSR From xPSR
        pulRegisters[REG_IPSR] = (pulRegisters[REG_xPSR] & 0x000001FF);
        // Extract EPSR From xPSR
        pulRegisters[REG_EPSR] = (pulRegisters[REG_xPSR] & 0x07FFFC00);

        /* MSP and PSP are put at the end of the register array 
        since they shoul be filled selectively according to the Mode. */
        
		i++; //17th value is MSP
        pulRegisters[REG_MSP] = *(pulRegValues+i);

        i++; //18th value is PSP
        pulRegisters[REG_PSP] = *(pulRegValues+i);

        i++; //19th is not used
		i++;
        /* Parse PRIMASK, FAULTMASK, BASEPRI and CONTROL values from 
        the next value obtained. */
        ulTempCtrlVal = *(pulRegValues+i);
        pulRegisters[REG_PRIMASK]   = (ulTempCtrlVal & 0x00000001);
        pulRegisters[REG_FAULTMASK] = (ulTempCtrlVal & 0x00010000)>>16;
        pulRegisters[REG_BASEPRI]   = (ulTempCtrlVal & 0x0000FF00)>>8;
        pulRegisters[REG_CONTROL]   = (ulTempCtrlVal & 0x03000000)>>24;

        /* TODO: */
        /*else if (tyProcessorConfig.bARM11)
        {
                if(bGlobalThumb)
                {
                    RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x4; //if ARM
                }
                else
                {
                    RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x8; //if THUMB
                }
                //RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -=0x0; //if JAVA
        } */
        /* TODO else 
        ASSERT_NOW();*/  
    }
    else
        return ML_ERROR_NO_USB_DEVICE_CONNECTED;


    free(pulRegValues);
    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
Function: ML_WriteRegisters
Engineer: Suraj S
Input: unsigned long *pulRegisters - Pointer to register write values
Output: TyError - Mid Layer error code 
Description: Writes to processor core registers 
Date           Initials     Description
19-Jul-2007      SJ         Initial
24-Mar-2008		 SJ		    Added 968 core support
21-Apr-2010		 JCK		Cortex-A8 Support Added
15-Mar-2011		 SJ	    	Cortex-A9 Support Added
***************************************************************************/
TyError ML_WriteRegisters (unsigned long *pulRegisters)

{  
	unsigned long i =0, j;
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyArmProcType TyProcType = NOCORE;
	unsigned long *pulRegValues;
	
	//This is the workaround for Cortex-A9(OMAP4430). An additional Cache invalidation 
	//is required here.
	if(tyTargetConfig.bCacheInvalidationNeeded)
	{
		if (0==(strcmp(tyTargetConfig.szProcessorName,"TI_OMAP4430")))
			ErrRet = CL_CleanandDisableCortexACaches (0, 0);
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
	}

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
    else if (tyProcessorConfig.bCortexM3)
        TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
        TyProcType = CORTEXA;
	
	pulRegValues = (unsigned long *)malloc(sizeof(RegArray));
	
	// Restore  the co-processor registers.
	switch (tyProcessorConfig.tyCoProcessorType)
	{
	default:
	case NO_COPROCESSOR:
		break;
	case ARM720T_CP:
		ErrRet = CL_WriteARM720CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			free(pulRegValues);
			return ErrRet;
		}
		break;
	case ARM740T_CP:
		ErrRet = CL_WriteARM740CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			free(pulRegValues);
			return ErrRet;
		}
		break;
	case ARM920T_CP:
	case ARM922T_CP:
		ErrRet = CL_WriteARM920_922CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			free(pulRegValues);
			return ErrRet;
		}
		break;
	case ARM926EJS_CP:
		ErrRet = CL_WriteARM926CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			free(pulRegValues);
			return ErrRet;
		}
		break;
	case ARM940T_CP:
	case ARM946ES_CP:
		ErrRet = CL_WriteARM940_946CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
            free(pulRegValues);
            return ErrRet;
		}
		break;
	case ARM966ES_CP:
		ErrRet = CL_WriteARM966CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
            free(pulRegValues);
            return ErrRet;
		}
		break;
		
	case ARM968ES_CP:
		ErrRet = CL_WriteARM968CoProcRegisters  ();
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
            free(pulRegValues);
            return ErrRet;
		}
		break;
	case ARM1136JFS_CP:
		ErrRet = CL_WriteARM1136CoProcRegisters();
        if (ErrRet != ML_ERROR_NO_ERROR)
        {
            free(pulRegValues);
            return ErrRet;
        }
        break;
    }		
	
	if (bUSBDevice)
	{
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_FIQ_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]; /*printf("FIQ_SPSR = %08x",*(pulRegValues + i));*/ i++;
		
		for (j=REG_ARRAY_R8; j <=REG_ARRAY_R14; j++)
		{
			*(pulRegValues + i) = pulRegisters[(REG_ARRAY_FIQ_MODE * MAXREGISTERS) + j]; /*printf("FIQ_R8-R14 = %08x",*(pulRegValues + i));*/ i++;
		}
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_IRQ_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]; /*printf("IRQ_SPSR = %08x",*(pulRegValues + i));*/ i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_IRQ_MODE * MAXREGISTERS) + REG_ARRAY_R13];  /*printf("IRQ_R13 = %08x",*(pulRegValues + i));*/  i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_IRQ_MODE * MAXREGISTERS) + REG_ARRAY_R14];  /*printf("IRQ_R14 = %08x",*(pulRegValues + i));*/  i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_SVC_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]; /*printf("SVC_SPSR = %08x",*(pulRegValues + i));*/ i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_SVC_MODE * MAXREGISTERS) + REG_ARRAY_R13];  /*printf("SVC_R13 = %08x",*(pulRegValues + i));*/  i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_SVC_MODE * MAXREGISTERS) + REG_ARRAY_R14];  /*printf("SVC_R14 = %08x",*(pulRegValues + i));*/  i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_ABORT_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]; /*printf("ABORT_SPSR = %08x",*(pulRegValues + i));*/ i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_ABORT_MODE * MAXREGISTERS) + REG_ARRAY_R13];  /*printf("ABORT_R13 = %08x",*(pulRegValues + i));*/ i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_ABORT_MODE * MAXREGISTERS) + REG_ARRAY_R14];  /*printf("ABORT_R14 = %08x",*(pulRegValues + i));*/ 
		i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_UNDEF_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]; /*printf("UNDEF_SPSR = %08x",*(pulRegValues + i));*/ 
		i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_UNDEF_MODE * MAXREGISTERS) + REG_ARRAY_R13];  /*printf("UNDEF_R13 = %08x",*(pulRegValues + i));*/ 
		i++;
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_UNDEF_MODE * MAXREGISTERS) + REG_ARRAY_R14];  /*printf("UNDEF_R14 = %08x",*(pulRegValues + i));*/ 
		i++;

		if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			*(pulRegValues + i) = pulRegisters[(REG_ARRAY_MON_MODE * MAXREGISTERS) + REG_ARRAY_SPSR]; /*printf("MON_SPSR = %08x",*(pulRegValues + i));*/ 
			i++;
			
			*(pulRegValues + i) = pulRegisters[(REG_ARRAY_MON_MODE * MAXREGISTERS) + REG_ARRAY_R13];  /*printf("MON_R13 = %08x",*(pulRegValues + i));*/ 
			i++;
			
			*(pulRegValues + i) = pulRegisters[(REG_ARRAY_MON_MODE * MAXREGISTERS) + REG_ARRAY_R14];  /*printf("MON_R14 = %08x",*(pulRegValues + i));*/ 
			i++;
		}
		
		for (j = REG_ARRAY_R8; j <= REG_ARRAY_R14; j++)
		{
			*(pulRegValues + i) = pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + j]; /*printf("USR_R8-R14 = %08x",*(pulRegValues + i));*/ i++;
		}
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + REG_ARRAY_CPSR]; /*printf("USR_CPSR = %08x",*(pulRegValues + i));*/ i++;
		
		for (j = REG_ARRAY_R0; j <= REG_ARRAY_R7; j++)    
		{
			*(pulRegValues + i) = pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + j]; /*printf("USR_R0-R7 = %08x",*(pulRegValues + i));*/ i++;
		}
		
		*(pulRegValues + i) = pulRegisters[(REG_ARRAY_USR_MODE * MAXREGISTERS) + REG_ARRAY_PC]; /*printf("USR_PC = %08x",*(pulRegValues + i));*/
		
		//Restore the normal registers.
		ErrRet = DL_OPXD_Arm_WriteRegisters(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulRegValues, sizeof(RegArray));
		if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		{
			free(pulRegValues);
			return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
		}
	}
	else
		return ML_ERROR_NO_USB_DEVICE_CONNECTED;
	
	free(pulRegValues);
	
	return ML_ERROR_NO_ERROR;
}


/****************************************************************************
Function: ML_CortexMWriteRegisters
Engineer: Deepa V.A.
Input: unsigned long *pulRegisters - Pointer to register write values
Output: TyError - Mid Layer error code 
Description: Writes to CortexM core registers 
Date           Initials     Description
30-Oct-2009     DVA         Initial
***************************************************************************/
TyError ML_CortexMWriteRegisters (unsigned long *pulRegisters)

{  
    unsigned long i =0;
    TyError ErrRet;
    TyArmProcType TyProcType = NOCORE;
    unsigned long *pulRegValues;

    TyProcType = CORTEXM;

    pulRegValues = (unsigned long *)malloc(sizeof(CortexMRegArray));

    if (bUSBDevice)
    {
        for (i=REG_R0; i <=REG_LR; i++)
        {
            *(pulRegValues + i) = pulRegisters[i];
        }

        *(pulRegValues + i) = pulRegisters[REG_PC];
        i++;

        *(pulRegValues + i) = pulRegisters[REG_xPSR];
        i++;

        *(pulRegValues + i) = pulRegisters[REG_MSP];
        i++;

        *(pulRegValues + i) = pulRegisters[REG_PSP];
        i++;

		i++; //19th is not used
        *(pulRegValues + i) = ((pulRegisters[REG_PRIMASK] & 0x00000001) |
                               ((pulRegisters[REG_BASEPRI] & 0x000000FF)  << 8)|
                               ((pulRegisters[REG_FAULTMASK] & 0x00000001) << 16)|
                               ((pulRegisters[REG_CONTROL] & 0x00000003)   << 24));
        i++;

        //Restore the normal registers.
        ErrRet = DL_OPXD_Arm_WriteRegisters(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulRegValues, sizeof(CortexMRegArray));
        if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
        {
            free(pulRegValues);
            return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
        }
    }
    else
        return ML_ERROR_NO_USB_DEVICE_CONNECTED;

    free(pulRegValues);

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_RestoreRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code 
 Description: Restores the registers to target before closing debugger, 
              so that when you re-open debugger, core state is exactly the same.           
Date          Initials     Description
23-Jul-2007     SJ         Initial
****************************************************************************/
TyError ML_RestoreRegisters (void)
{
    TyError ErrRet = ML_ERROR_NO_ERROR;

    if (!bGlobalStartedExecution)
    {
        //Because of the NOPs in the changed back to Thumb mode.
        if (bGlobalThumb)
        {
            if (tyProcessorConfig.bARM9)
                RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -= 0x6;
            else
                RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -= 0x4;
        }

        // Restore the registers before execution.
        if (tyProcessorConfig.bCortexM3)
        {
            ErrRet = ML_CortexMWriteRegisters((unsigned long *)CortexMRegArray);
        }		
        else
        {
            ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
        }

        if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;

        if ((tyProcessorConfig.bARM7) ||
			(tyProcessorConfig.bARM9))
        {

            if (bGlobalThumb)
            {
                ErrRet = ML_ThumbMode ();
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;
            }
        }

        if ((tyProcessorConfig.bARM7) ||
			(tyProcessorConfig.bARM9))
        {
            //Write the values from the array into the EmbeddedICE registers
            ErrRet = ML_SetupWatchpoints ();
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;
            ErrRet = ML_Select_Scan_Chain(2,0);
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;
        }

        else if (tyProcessorConfig.bARM11)        
        {
            ML_SetupBPRegsARM11();
        }
		
		if (tyProcessorConfig.bCortexM3 == FALSE)
		{
			ErrRet = ML_Arm_Restart();
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;	
		}
		
    }

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_Executing
     Engineer: Suraj S
        Input: NONE
       Output: TRUE if the processor is running, FALSE if it is stopped 
  Description: Allows the debugger to check if target is running.
Date           Initials     Description
11-Jul-2007      SJ         Initial
****************************************************************************/
boolean ML_Executing(void)

{
	return bGlobalStartedExecution;
}

/****************************************************************************
	Function: ML_StatusProc
	Engineer: Suraj S
	   Input: boolean *pbExecuting - address of variable showing status -
									ie executing or stopped.
			  TXRXCauseOfBreak *ptyCauseOfBreak - Cause of break:
									User halt or breakpoint.
			  RDI_PointHandle  *phPointHandle - Breakpoint or Watchpoint handle
	  Output: TyError - Mid Layer error code 
 Description: Checks the status of processor.
Date           Initials     Description
17-Jul-2007      SJ         Initial
18-Mar-2008		 JCK	    Added single stepping support
24-Mar-2008		 SJ		    Added 968 core support
31-Mar-2008	     RS		    Added global Variables required for ARM GDB Server
10-09-2009       RTR        Added ARM11 support
02-Nov-2009		 JCK		Added support for Cortex-M3
21-Apr-2010		 JCK		Cortex-A8 Support Added
****************************************************************************/
TyError ML_StatusProc(boolean          *pbExecuting,
                      TXRXCauseOfBreak *ptyCauseOfBreak,
                      RDI_PointHandle  *phPointHandle,
					  pInitUserRegisterSettings pUserRegHandle)
					  
{
	boolean bARM7StopProblem           = FALSE;
	boolean bHalfWordAlignedPC         = FALSE;
	TyError ErrRet;
	TyArmProcType TyProcType = NOCORE;
		
	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
    else if (tyProcessorConfig.bCortexM3)
        TyProcType = CORTEXM;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
        TyProcType = CORTEXA;

    if (bUSBDevice)
    {
        switch (TyProcType )
        {
        case ARM7:
        case ARM7S:
        case ARM9:
            {
                ErrRet = DL_OPXD_Arm_Status_Proc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                 TyProcType, (ulGlobalScanChain != 0x2), bGlobalStartedExecution, 
                                                 &bARM7StopProblem, (boolean *) pbExecuting, &bGlobalThumb, &bWatchPoint);

                if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
                    return(ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
                ulGlobalExecuting=(unsigned long)*pbExecuting;

                //These functions leave the core in scan chain 1.
                ulGlobalScanChain = 0x1;

                // If in THUMB mode then change to ARM mode before carrying on.
                if (bGlobalThumb && !*pbExecuting)
                {
                    // Change processor into ARM mode
                    ErrRet = ML_ARMMode(&bHalfWordAlignedPC);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;

                    //Commented for Optimization
                    /*ErrRet = ML_Select_Scan_Chain(1,1);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;*/

                    ErrRet = ML_PerformARMNops (0x2);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;
                    ErrRet = ML_PerformARMNops (0x2);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;
                }
            }
            break; /* case ARM7, ARM7S and ARM9 */

        case ARM11: 
            {
                unsigned long ulDSCRValue;
                unsigned long ulCPSRValue = 0;

                ErrRet = ML_ReadDSCR(&ulDSCRValue);
                if (ErrRet != ML_ERROR_NO_ERROR)
                    return ErrRet;

                if (((ulDSCRValue & ARM11_DSCR_ENTRY_FIELD_MASK) >> 2) == ARM11_WATCH_POINT)
                    bWatchPoint = TRUE;
                else
                    bWatchPoint = FALSE;
                if (ulDSCRValue & ARM11_DSCR_CORE_HALTED)
                {
                    *pbExecuting = FALSE;
                    ErrRet = ML_ArmInstnExecEnable();

                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;

                    ErrRet = ML_ReadCPSR(&ulCPSRValue);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;


					ErrRet = ML_ReadCDCR(&ARM11_DataArray[6]); 
					if (ErrRet != ML_ERROR_NO_ERROR)
						return ErrRet;
                }
                else
				{
					*pbExecuting = TRUE;
				}

                ulGlobalExecuting=(unsigned long)*pbExecuting;

                /*
                            ErrRet = ML_ReadCPSR(&ulCPSRValue);
                            if (ErrRet != ML_ERROR_NO_ERROR)
                                return ErrRet;
                */

                if (ulCPSRValue & ARM11_CPSR_T_BIT)
                    bGlobalThumb = TRUE;
                else
                    bGlobalThumb = FALSE;


                if (bGlobalThumb && !*pbExecuting)
                {
                    // Change processor into ARM mode
                    ErrRet = ML_ARMMode(&bHalfWordAlignedPC);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;
                }
            }
            break;  /* case ARM11 */

        case CORTEXM:
		case CORTEXA:
            {
                ErrRet = DL_OPXD_Arm_Status_Proc(
                                                tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                TyProcType, (ulGlobalScanChain != 0x2), 
                                                bGlobalStartedExecution, &bARM7StopProblem, 
                                                (boolean *) pbExecuting, &bGlobalThumb, 
                                                &bWatchPoint);

                if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
                {
                    return(ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
                }

				bARM7StopProblem = 0;
            }
            break; /* case CortexM3 */

        default:
            return ML_Error_NotInitialised;
            break;
        }

    }
    else
        return ML_ERROR_NO_USB_DEVICE_CONNECTED;

    if ((*pbExecuting == FALSE) && (bGlobalStartedExecution == TRUE))
    {
		if (tyProcessorConfig.bCortexM3)
        {
            ErrRet = ML_CortexMReadRegisters((unsigned long *)CortexMRegArray);
        }		
        else
        {
            ErrRet = ML_ReadRegisters((unsigned long *)RegArray);
        }
        if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;

        bGlobalStartedExecution = FALSE;    

		if (NULL != pUserRegHandle)
		{
			ErrRet = pUserRegHandle();
			if (ErrRet != ML_ERROR_NO_ERROR)
				return ErrRet;
		}


        switch (tyProcessorConfig.tyCoProcessorType)
        {
        default:
        case NO_COPROCESSOR:
            break;
        case ARM720T_CP:
            ErrRet = CL_FlushandDisableARM720CacheAndTTB ();
            break;
        case ARM740T_CP:
            ErrRet = CL_FlushandDisableARM740CacheAndTTB ();
            break;
        case ARM920T_CP:
        case ARM922T_CP:
            ErrRet = CL_CleanandDisable920_922Caches ();
            break;
        case ARM926EJS_CP:
            ErrRet = CL_CleanandDisable926Caches ();
            break;
        case ARM940T_CP:
        case ARM946ES_CP:
            ErrRet = CL_CleanandDisable940_946Caches ();
            break;
        case ARM966ES_CP:
        case ARM968ES_CP:
            break;
        case ARM1136JFS_CP:	
		case ARM1156T2S_CP:
		case ARM1176JZS_CP:
            ErrRet = CL_CleanandDisable1136_Caches();
            break;
		case CORTEXA8_CP:
		case CORTEXA9_CP:
			ErrRet = CL_CleanandDisableCortexACaches (0, 0);
			break;
        }

        // Check for Data Abort.
        if (ErrRet == ML_Error_DataAbort)
            ErrRet = ML_Error_DataAbort_Writing_Cache_Clean;
        if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;

        // Correct the PC value for the extra instruction needed to regain debug 
        // mode after the stop "problem" happened.
        if (bARM7StopProblem)
        {
            if (tyProcessorConfig.bThumb)
            {
                if (bGlobalThumb)
                    RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -= 0x2;
                else
                    RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC] -= 0x4;
            }
            else
            {
                // The ARM7DI board
                RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC] += 0x8;
            }
        }

        if (bHalfWordAlignedPC && (tyProcessorConfig.bCortexM3 == FALSE))
        {
            //The core stopped on a half word aligned instruction.
            RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC] += 0x2;
        }

		if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			if (bGlobalThumb)
			{
				RegArray[REG_ARRAY_USR_MODE][REG_ARRAY_PC] -= 0x4; 
			}
			else
			{
				RegArray[REG_ARRAY_USR_MODE][REG_ARRAY_PC] -= 0x8; 
			}
			
		}

        //Set the handle to invalid in case the Cause of break was not a BP.
        //We do not return handles for WatchPoints.
        *phPointHandle = RDI_NoHandle;

        if (tyProcessorConfig.bCortexM3)
        {
			unsigned long ulDfsrStatus;

			ErrRet = ML_ReadWord(DFSR_ADDR, &ulDfsrStatus);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				return ErrRet;
			}

			if (EXTERNAL_DBG_REQ == (ulDfsrStatus & EXTERNAL_DBG_REQ))
			{
				tyLocalCauseOfBreak = COB_EXT_RESET;				
			} 
			if (VECTOR_CATCH_OCCUR == (ulDfsrStatus & VECTOR_CATCH_OCCUR))
			{
				unsigned long ulVectorCatch;

				/* Vector catching. That is, to cause debug entry when a specified
				vector is committed for execution */
				ErrRet =  ML_ReadWord (DEMCR_ADDR, &ulVectorCatch);
				if (ErrRet != ML_ERROR_NO_ERROR)
				{
					return ErrRet;
				}
				switch (ulVectorCatch & VECTOR_CATCH_MASK_ARM_V7M)
				{
				case VC_CORE_RESET:	/* Reset Vector Catch. */
					tyLocalCauseOfBreak = COB_BRANCH_THROUGH0;
					break;
				case VC_MMERR:		/* Debug trap on Memory Management faults */
				case VC_NOCPERR:	/* Debug trap on Usage Fault access to Co-processor that is not present or
									marked as not present in CAR register*/
				case VC_CHKERR:		/* Debug trap on Usage Fault enabled checking errors*/
				case VC_STATERR:	/* Debug trap on Usage Fault state errors */
				case VC_BUSERR:		/* Debug Trap on normal Bus error */
				case VC_INTERR:		/* Debug Trap on interrupt/exception service errors */
				case VC_HARDERR:	/* Debug trap on Hard Fault */
					tyLocalCauseOfBreak = COB_EXCEPTION;
					break;
				default:
					tyLocalCauseOfBreak = USER_HALT;
					break;
				}
			} 

			if (DWTTRAP_MATCH == (ulDfsrStatus & DWTTRAP_MATCH))
			{
				tyLocalCauseOfBreak = COB_WATCHPOINT;
			}
			if (BKPT_FLAG == (ulDfsrStatus & BKPT_FLAG))
			{
				tyLocalCauseOfBreak = BP_OPCODE;
			}

			if (HALT_REQ_FLAG == (ulDfsrStatus & HALT_REQ_FLAG))
			{
				if (bUserHalt)
					tyLocalCauseOfBreak = USER_HALT;
				else if (bStep)
					tyLocalCauseOfBreak = SINGLE_STEP;
				else
					tyLocalCauseOfBreak = STOP_TRIGGER;
						
			}
			
			ErrRet = ML_WriteWord(DFSR_ADDR, CLEAR_DFSR_REG);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				return ErrRet;
			}
			
        }
		else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			unsigned long ulDebugStatusVal;

			ErrRet = ML_ReadDSCR(&ulDebugStatusVal);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				return ErrRet;
			}

			unsigned long ulDebugEntry = ulDebugStatusVal & CORTEXA8_DBG_ENTRY;
			switch (ulDebugEntry )
			{
			case CORTEXA8_BREAKPOINT:
			case CORTEXA8_BKPT:
				if (bStep)
					tyLocalCauseOfBreak = SINGLE_STEP;
				else
				{
					// Check for a breakpoint at the current address 
					if (ML_BreakpointAlreadySet(RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC], phPointHandle))
					{
						// We need to check which type of breakpoint caused the halt.
						switch (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC])
						{
						case  RESET_VECTOR :  
							tyLocalCauseOfBreak = COB_BRANCH_THROUGH0;
							break;
						case  UNDEFINED_INSTRUCTION_VECTOR :
							tyLocalCauseOfBreak = ILLEGAL_OPCODE;
							break;
						case  SOFTWARE_INTERRUPT_VECTOR :                
							tyLocalCauseOfBreak = COB_SOFTWARE_INTERRUPT; 
							break;                                           
						case  PREFETCH_ABORT_VECTOR :                    
							tyLocalCauseOfBreak = COB_PREFETCH_ABORT;                  
							break;                                                
						case  DATA_ABORT_VECTOR :                         
							tyLocalCauseOfBreak = COB_DATA_ABORT;                             
							break;                                                       
						case  ADDRESS_EXCEPTION_VECTOR :    
							tyLocalCauseOfBreak = COB_ADDRESS_EXCEPTION;
							break;
						case  IRQ_VECTOR :                  
							tyLocalCauseOfBreak = COB_IRQ;
							break;
						case  FIQ_VECTOR :                  
							tyLocalCauseOfBreak = COB_FIQ;
							break;
						default :
							tyLocalCauseOfBreak = BP_OPCODE;
							break;
						}
					}
					else
					{
						// We need to check which type of breakpoint caused the halt.
						switch (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC])
						{
						case  RESET_VECTOR :  
							if ((ulGlobalVectorCatch & RESET_VECTOR_MASK) == RESET_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_BRANCH_THROUGH0;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;
						case  UNDEFINED_INSTRUCTION_VECTOR :
							if ((ulGlobalVectorCatch & UNDEFINED_INSTRUCTION_VECTOR_MASK) == UNDEFINED_INSTRUCTION_VECTOR_MASK)
								tyLocalCauseOfBreak = ILLEGAL_OPCODE;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;
						case  SOFTWARE_INTERRUPT_VECTOR :                
							if ((ulGlobalVectorCatch & SOFTWARE_INTERRUPT_VECTOR_MASK) == SOFTWARE_INTERRUPT_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_SOFTWARE_INTERRUPT;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;                                           
						case  PREFETCH_ABORT_VECTOR :                    
							if ((ulGlobalVectorCatch & PREFETCH_ABORT_VECTOR_MASK) == PREFETCH_ABORT_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_PREFETCH_ABORT;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;                                                
						case  DATA_ABORT_VECTOR :                         
							if ((ulGlobalVectorCatch & DATA_ABORT_VECTOR_MASK) == DATA_ABORT_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_DATA_ABORT;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;                                                       
						case  ADDRESS_EXCEPTION_VECTOR :    
							if ((ulGlobalVectorCatch & ADDRESS_EXCEPTION_VECTOR_MASK) == ADDRESS_EXCEPTION_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_ADDRESS_EXCEPTION;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;
						case  IRQ_VECTOR :                  
							if ((ulGlobalVectorCatch & IRQ_VECTOR_MASK) == IRQ_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_IRQ;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;
						case  FIQ_VECTOR :                  
							if ((ulGlobalVectorCatch & FIQ_VECTOR_MASK) == FIQ_VECTOR_MASK)
								tyLocalCauseOfBreak = COB_FIQ;
							else
								tyLocalCauseOfBreak = USER_HALT;
							break;
						default :
							if (bUserHalt)
								tyLocalCauseOfBreak = USER_HALT;
							else if (bStep)
								tyLocalCauseOfBreak = SINGLE_STEP;
							else
								tyLocalCauseOfBreak = STOP_TRIGGER;
						break;
						}
					}
				}
				break;
			case CORTEXA8_HALTING_DBG_EVENT:
				if (bStep)
					tyLocalCauseOfBreak = SINGLE_STEP;
				else
					tyLocalCauseOfBreak = STOP_TRIGGER;
				break;			
			case CORTEXA8_VECTOR_CATCH:
				// Check for a breakpoint at the current address 
				// We need to check which type of breakpoint caused the halt.
				switch (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC])
				{
				case  RESET_VECTOR :  
					tyLocalCauseOfBreak = COB_BRANCH_THROUGH0;
					break;
				case  UNDEFINED_INSTRUCTION_VECTOR :
					tyLocalCauseOfBreak = ILLEGAL_OPCODE;
					break;
				case  SOFTWARE_INTERRUPT_VECTOR :                
					tyLocalCauseOfBreak = COB_SOFTWARE_INTERRUPT; 
					break;                                           
				case  PREFETCH_ABORT_VECTOR :                    
					tyLocalCauseOfBreak = COB_PREFETCH_ABORT;                  
					break;                                                
				case  DATA_ABORT_VECTOR :                         
					tyLocalCauseOfBreak = COB_DATA_ABORT;                             
					break;                                                       
				case  ADDRESS_EXCEPTION_VECTOR :    
					tyLocalCauseOfBreak = COB_ADDRESS_EXCEPTION;
					break;
				case  IRQ_VECTOR :                  
					tyLocalCauseOfBreak = COB_IRQ;
					break;
				case  FIQ_VECTOR :                  
					tyLocalCauseOfBreak = COB_FIQ;
					break;
				default :
					tyLocalCauseOfBreak = BP_OPCODE;
					break;
				}
				break;
			case CORTEXA8_OS_UNLOCK:
				tyLocalCauseOfBreak = STOP_TRIGGER;
				break;
			case CORTEXA8_ASYNC_WATCHPOINT:
			case CORTEXA8_SYNC_WATCHPOINT:
				tyLocalCauseOfBreak = COB_WATCHPOINT;
				break;
			default:
				tyLocalCauseOfBreak = USER_HALT;
				break;
			}
		}
		else if ((tyProcessorConfig.bARM7) ||
				 (tyProcessorConfig.bARM9) || 
				 (tyProcessorConfig.bARM11))
		{
			if (bWatchPoint)
				tyLocalCauseOfBreak = COB_WATCHPOINT;
			else
			{
				// Check for a breakpoint at the current address 
				if (ML_BreakpointAlreadySet(RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC], phPointHandle))
				{
					// We need to check which type of breakpoint caused the halt.
					switch (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC])
					{
					case  RESET_VECTOR :  
						tyLocalCauseOfBreak = COB_BRANCH_THROUGH0;
						break;
					case  UNDEFINED_INSTRUCTION_VECTOR :
						tyLocalCauseOfBreak = ILLEGAL_OPCODE;
						break;
					case  SOFTWARE_INTERRUPT_VECTOR :                
						tyLocalCauseOfBreak = COB_SOFTWARE_INTERRUPT; 
						break;                                           
					case  PREFETCH_ABORT_VECTOR :                    
						tyLocalCauseOfBreak = COB_PREFETCH_ABORT;                  
						break;                                                
					case  DATA_ABORT_VECTOR :                         
						tyLocalCauseOfBreak = COB_DATA_ABORT;                             
						break;                                                       
					case  ADDRESS_EXCEPTION_VECTOR :    
						tyLocalCauseOfBreak = COB_ADDRESS_EXCEPTION;
						break;
					case  IRQ_VECTOR :                  
						tyLocalCauseOfBreak = COB_IRQ;
						break;
					case  FIQ_VECTOR :                  
						tyLocalCauseOfBreak = COB_FIQ;
						break;
					default :
						tyLocalCauseOfBreak = BP_OPCODE;
						break;
					}
				}
				else
				{
					// We need to check which type of breakpoint caused the halt.
					switch (RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC])
					{
					case  RESET_VECTOR :  
						if ((ulGlobalVectorCatch & RESET_VECTOR_MASK) == RESET_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_BRANCH_THROUGH0;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;
					case  UNDEFINED_INSTRUCTION_VECTOR :
						if ((ulGlobalVectorCatch & UNDEFINED_INSTRUCTION_VECTOR_MASK) == UNDEFINED_INSTRUCTION_VECTOR_MASK)
							tyLocalCauseOfBreak = ILLEGAL_OPCODE;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;
					case  SOFTWARE_INTERRUPT_VECTOR :                
						if ((ulGlobalVectorCatch & SOFTWARE_INTERRUPT_VECTOR_MASK) == SOFTWARE_INTERRUPT_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_SOFTWARE_INTERRUPT;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;                                           
					case  PREFETCH_ABORT_VECTOR :                    
						if ((ulGlobalVectorCatch & PREFETCH_ABORT_VECTOR_MASK) == PREFETCH_ABORT_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_PREFETCH_ABORT;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;                                                
					case  DATA_ABORT_VECTOR :                         
						if ((ulGlobalVectorCatch & DATA_ABORT_VECTOR_MASK) == DATA_ABORT_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_DATA_ABORT;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;                                                       
					case  ADDRESS_EXCEPTION_VECTOR :    
						if ((ulGlobalVectorCatch & ADDRESS_EXCEPTION_VECTOR_MASK) == ADDRESS_EXCEPTION_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_ADDRESS_EXCEPTION;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;
					case  IRQ_VECTOR :                  
						if ((ulGlobalVectorCatch & IRQ_VECTOR_MASK) == IRQ_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_IRQ;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;
					case  FIQ_VECTOR :                  
						if ((ulGlobalVectorCatch & FIQ_VECTOR_MASK) == FIQ_VECTOR_MASK)
							tyLocalCauseOfBreak = COB_FIQ;
						else
							tyLocalCauseOfBreak = USER_HALT;
						break;
					default :
						if (bUserHalt)
							tyLocalCauseOfBreak = USER_HALT;
						else if (bStep)
							tyLocalCauseOfBreak = SINGLE_STEP;
						else
							tyLocalCauseOfBreak = STOP_TRIGGER;
						break;
					}
				}
			}
		}
    }

    *ptyCauseOfBreak = tyLocalCauseOfBreak;
    tyGlobalCauseOfBreak=(unsigned long)*ptyCauseOfBreak;


    //We do not care if the core was reset during execution.
    //So read the status to reset the latches in the Opella.
    if (!*pbExecuting)
    {
        ErrRet = DL_OPXD_Arm_ResetProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 0, &ucLatchedStatus, NULL);
        if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
            return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
    }

    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ARMMode
    Engineer: Suraj S
       Input: Pointer to return whether the core stopped on a non word aligned address  
      Output: TyError - Mid Layer error code 
 Description: Change from Thumb mode to ARM mode
Date           Initials     Description
12-Jul-2007      SJ         Initial
****************************************************************************/
static TyError ML_ARMMode (boolean *pbOffsetNeeded)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;

   if(tyProcessorConfig.bARM7)
   {
      if(!tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7;
      else if(tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7S;
   }
   else if(tyProcessorConfig.bARM9)      
      TyProcType = ARM9;
   else if(tyProcessorConfig.bARM11)      
      TyProcType = ARM11;


   ErrRet = DL_OPXD_Arm_Change_To_Arm_Mode(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bWatchPoint, bUserHalt, pbOffsetNeeded);
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_ThumbMode
    Engineer: Suraj S
       Input: NONE  
      Output: TyError - Mid Layer error code 
 Description: Change from ARM mode to Thumb mode
Date           Initials     Description
23-Jul-2007      SJ         Initial
****************************************************************************/
static TyError ML_ThumbMode (void)
{
   unsigned long ulReg0;
   unsigned long ulRegPC;
   TyError ErrRet;
   TyArmProcType TyProcType = NOCORE;

   if(tyProcessorConfig.bARM7)
   {
      if(!tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7;
      else if(tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7S;
   }
   else if(tyProcessorConfig.bARM9)      
      TyProcType = ARM9;
   else if(tyProcessorConfig.bARM11)      
      TyProcType = ARM11;

   // These instructions are in ARM mode.

   //Add one to the PC so that we can BX into Thumb mode at the current PC.
   ulRegPC = RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] + 1;                            
   ulReg0  = RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_R0];                            

   ErrRet = DL_OPXD_Arm_Change_To_Thumb_Mode(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulReg0, ulRegPC);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_PerformARMNops
    Engineer: Suraj S
       Input: Number of NOPS to be executed   
      Output: TyError - Mid Layer error code    
 Description: Executes a NOP in ARM mode
Date           Initials     Description
12-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_PerformARMNops(unsigned long ulNoOfNops)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   TyArmProcType TyProcType = NOCORE;

   if(tyProcessorConfig.bARM7)
   {
      if(!tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7;
      else if(tyProcessorConfig.bSynthesisedCore)
         TyProcType = ARM7S;
   }
   else if(tyProcessorConfig.bARM9)      
      TyProcType = ARM9;
   else if(tyProcessorConfig.bARM11)      
      TyProcType = ARM11;

   ErrRet = DL_OPXD_Arm_Perform_ARM_Nops(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulNoOfNops);
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: ML_WriteProc
     Engineer: Suraj S
        Input: void *pulSource - Source address
               unsigned long  ulDest - Destination(Target) address
               unsigned long  *pulBytes - number of bytes to write
               RDI_AccessType tyType - Access type
               boolean bCodeDownload - if true this does not use safe non 
               vector and check for data abort
       Output: TyError - Mid Layer error code 
  Description: Write to processor memory
Date           Initials     Description
12-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_WriteProc(void           *pulSource,
                     unsigned long  ulDest,
                     unsigned long  *pulBytes,
                     RDI_AccessType tyType,
                     boolean        bCodeDownload,
                     boolean        bSkipBreakpoints)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   unsigned char *pucSourceBackup;
   unsigned char *pucSourceAllocated;
   unsigned long ulBufferIndex               = 0x0;
   unsigned long ulCountToWrite              = 0x0;
   unsigned long ulCountWrittenSoFar         = 0x0;

   if (ML_Executing())
      {
      do
         {
         ulCountToWrite = __min(*pulBytes - ulCountWrittenSoFar, MAX_MEMORY_SIZE);
         ErrRet = ExecuteWriteBytes(ulDest + ulCountWrittenSoFar,
                                    ulCountToWrite,
                                    &(((unsigned char *)pulSource)[ulBufferIndex]));
         if (ErrRet != RDIError_NoError)
            return ML_Error_RealMonFailed;
         ulCountWrittenSoFar += ulCountToWrite;
         ulBufferIndex     += ulCountToWrite;
         }
      while (ulCountWrittenSoFar != *pulBytes);
      return ML_ERROR_NO_ERROR;
      }
   else
      {
      // Allocate a section of memory and copy the contents of the *pulSource into it.
      pucSourceAllocated = (unsigned char *)malloc(*pulBytes);
      pucSourceBackup = pucSourceAllocated;
      memcpy(pucSourceBackup, pulSource, *pulBytes);

      if ((usNumberOfBreakpoints != 0) && (bSkipBreakpoints != TRUE))
         {
         ML_FindAndReplaceAllBPsForWrite(ulDest, pucSourceBackup, (unsigned long)*pulBytes);
         }

      //This is added as a workaround for diskware block write. For a blockwrite to happen
      //a word write is to done.
        if (tyProcessorConfig.bARM7 ||
            (tyProcessorConfig.bARM9))
		{
			ErrRet = ML_WriteByte(ulDest, *pucSourceBackup);
		}

      ErrRet = ML_WriteToTargetMemory  (pucSourceBackup,
                                        ulDest,
                                        pulBytes,
                                        tyType);

      free(pucSourceAllocated);

      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;

      if ((!bCodeDownload) && !(ML_Executing()))
         ErrRet =  ML_CheckForDataAbort ();

      return ErrRet;
   }
}

/****************************************************************************
    Function: ML_ReadProc
    Engineer: Suraj S
       Input: unsigned long  ulSource - Memory address to read from
              void *pulDest - Pointer to read the data 
              unsigned long  *pulBytes - Number of bytes to read 
              RDI_AccessType tyType - Acces type
              boolean bCheckForDataAbort -
      Output: TyError - Mid Layer error code
 Description: Read processor memory
Date           Initials     Description
18-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_ReadProc(unsigned long  ulSource,
                    void           *pulDest,
                    unsigned long  *pulBytes,
                    RDI_AccessType tyType,
                    boolean        bCheckForDataAbort)

{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   unsigned long ulBufferIndex    = 0x0;
   unsigned long ulCountToRead    = 0x0;
   unsigned long ulCountReadSoFar = 0x0;

   if (ML_Executing())
      {
      do
         {
         ulCountToRead = __min(*pulBytes - ulCountReadSoFar, MAX_MEMORY_SIZE);
         ErrRet = ExecuteReadBytes(TRUE,
                                   ulSource + ulCountReadSoFar,
                                   TRUE,
                                   ulCountToRead,
                                   &(((unsigned char *)pulDest)[ulBufferIndex]));
         if (ErrRet != RDIError_NoError)
            return ML_Error_RealMonFailed;

         ulCountReadSoFar += ulCountToRead;
         ulBufferIndex    += ulCountToRead;
         }
      while (ulCountReadSoFar != *pulBytes);

      ML_FindAndReplaceAllBPsforRead(ulSource, (unsigned char *)pulDest, *pulBytes);
      }
   else
      {
      ErrRet = ML_ReadFromTargetMemory(ulSource,
                                       pulDest,
                                       pulBytes,
                                       tyType);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;

      ML_FindAndReplaceAllBPsforRead(ulSource, (unsigned char *)pulDest, *pulBytes);
      }
      
   if ((bCheckForDataAbort) && !(ML_Executing()))
      {
      ErrRet =  ML_CheckForDataAbort ();
      }

   return ErrRet;
}

/****************************************************************************
     Function: ML_CheckSemiHostState
     Engineer: Suraj S
        Input: NONE
       Output: unsigned long - Semihosting type
  Description: Returns the value of the semihost option selected 
Date          Initials     Description
23-Jul-2007     SJ         Initial
****************************************************************************/
unsigned long ML_CheckSemiHostState (void)
{
   return tySemiHostingConfig.ulSemiHostingType;
}

/****************************************************************************
    Function: ML_HasResetOccurred
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code 
 Description: Checks whether target reset has been detected.
Date          Initials     Description
23-Jul-2007     SJ         Initial
****************************************************************************/
boolean ML_HasResetOccurred (void)
{
   //Check whether target has been reset
   return ((ucLatchedStatus & TARGET_RESET) == TARGET_RESET);
}

/****************************************************************************
    Function: ML_Arm_Restart
    Engineer: Suraj S
       Input: None   
      Output: TyError - Mid Layer error code  
 Description: Sets TAP state machine to RESTART
Date          Initials     Description
23-Jul-2007     SJ         Initial
****************************************************************************/
TyError ML_Arm_Restart(void)
{  
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Restart(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_CheckDCC
    Engineer: Suraj S
       Input: unsigned long *pulDCCReadData - Data read from target.
              boolean *pbDataToRead - Data has been read from the target.
              unsigned long *pulDCCWriteData - Data to write to target.
              boolean *pbDataWritten - Returns true if the data was written 
              to the target.
      Output: TyError - Mid Layer error code
 Description: checks the DCC register of the core
Date          Initials     Description
15-Aug-2007     SJ         Initial
21-Apr-2010		JCK		Cortex-A8 Support Added
****************************************************************************/
TyError ML_CheckDCC  (unsigned long    *pulDCCReadData,
                      boolean          *pbDataToRead,
                      unsigned long    *pulDCCWriteData,
					  boolean          *pbDataWritten)
{
	TyError ErrRet;
	TyArmProcType TyProcType = NOCORE;

	if(tyProcessorConfig.bARM7)
	{
		if(!tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7;
		else if(tyProcessorConfig.bSynthesisedCore)
			TyProcType = ARM7S;
	}
	else if(tyProcessorConfig.bARM9)      
		TyProcType = ARM9;
	else if(tyProcessorConfig.bARM11)      
		TyProcType = ARM11;
	else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}
	
	//initialize
	if (pbDataWritten != NULL)
		*pbDataWritten = FALSE;
	
	if (pbDataToRead != NULL)
		*pbDataToRead  = FALSE;
	
	
		if (tyProcessorConfig.bDCC)
		{
			if (bUSBDevice)
			{
				if((tyProcessorConfig.bARM7) ||
					(tyProcessorConfig.bARM9))
				{
					// This function expects scan chain 2.
					ErrRet = ML_Select_Scan_Chain(2,1);
					if(ErrRet != ML_ERROR_NO_ERROR)
						return ErrRet;
				}
				
				ErrRet = DL_OPXD_Arm_CheckDCC(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, pulDCCReadData, 
					pbDataToRead, pulDCCWriteData,pbDataWritten);
				ulGlobalScanChain = 0x2;
				if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
					return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
			}
			else
				return ML_ERROR_NO_USB_DEVICE_CONNECTED;
		}
	
	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_BreakpointInquiry
    Engineer: Suraj S
       Input: unsigned long *pulArg1 - Handle of the BP inquired as INPUT
                                     - Hardware resource number aa OUTPUT
              unsigned long *pulArg2 - Breakpoint type ie hardware / software
      Output: TyError - Mid Layer error code   
 Description: This function helps the Debugger to get info about a breakpoint using 
              RDI_InfoProc#RDIPointStatus_Break
Date          Initials     Description
20-Aug-2007     SJ         Initial
21-Apr-2010		JCK		Cortex-A8 Support Added
****************************************************************************/
TyError ML_BreakpointInquiry (unsigned long *pulArg1,unsigned long *pulArg2)
{
   unsigned long i=0;

   // find the original instruction data and address...
   for (i=0; i < MAX_NUM_BREAKPOINTS; i++)
      {
	   if ((tyProcessorConfig.bCortexM3) ||
		   (tyProcessorConfig.bCortexA8) ||
		   (tyProcessorConfig.bCortexA9))
	   {
		   if (i == usNumberOfBreakpoints)
		   {
			   return ML_Error_NoSuchHandle;
		   }

		   if ((pCortexBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)*pulArg1) &&
			   (!pCortexBPArray->BreakpointStructures[i].bWatchPoint))
		   {
				break;		   
		   }
	   }
	   else
	   {
		   if ((pBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)*pulArg1) &&
			   (!pBPArray->BreakpointStructures[i].bWatchPoint))
			break;
	   }
      
      }

   if ((tyProcessorConfig.bCortexM3) ||
	   (tyProcessorConfig.bCortexA8) ||
	   (tyProcessorConfig.bCortexA9))
   {
		if ((i == MAX_NUM_BREAKPOINTS) && (pCortexBPArray->BreakpointStructures[i].usBreakpointId != (unsigned short)*pulArg1))
			return ML_Error_NoSuchHandle;
   }
   else
   {
		if ((i == MAX_NUM_BREAKPOINTS) && (pBPArray->BreakpointStructures[i].usBreakpointId != (unsigned short)*pulArg1))
			return ML_Error_NoSuchHandle;
   }
   
   
	if((tyProcessorConfig.bARM7) ||
		(tyProcessorConfig.bARM9))
	{
	   //This gives the Breakpoint type ie 0 = software, 1 = hardware
	   if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_BP0)
		  {
		  *pulArg1 = HARDWARE_RESOURCE0;
		  *pulArg2 = 0x1;
		  }
	   else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_BP1)
		  {
		  *pulArg1 = HARDWARE_RESOURCE1;
		  *pulArg2 = 0x1;
		  }
	   else if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_BP0)
		  {
		  *pulArg1 = HARDWARE_RESOURCE0;
		  *pulArg2 = 0x0;
		  }
	   else if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_BP1)
		  {
		  *pulArg1 = HARDWARE_RESOURCE1;
		  *pulArg2 = 0x0;
		  }
	   else
		  // The handle given is not a valid breakpoint. (may not have been set or could be a watchpoint)
		  return ML_Error_NoSuchHandle;
	}
	else if (tyProcessorConfig.bARM11)
	{
		//This gives the Breakpoint type ie 0 = software, 1 = hardware
		if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_RESOURCE0)
		{
			*pulArg1 = HARDWARE_RESOURCE0;
			*pulArg2 = 0x1;
		}
		else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_RESOURCE1)
		{
			*pulArg1 = HARDWARE_RESOURCE1;
			*pulArg2 = 0x1;
		}
		else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_RESOURCE2)
		{
			*pulArg1 = HARDWARE_RESOURCE2;
			*pulArg2 = 0x1;
		}
		else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_RESOURCE3)
		{
			*pulArg1 = HARDWARE_RESOURCE3;
			*pulArg2 = 0x1;
		}
		else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_RESOURCE4)
		{
			*pulArg1 = HARDWARE_RESOURCE4;
			*pulArg2 = 0x1;
		}
		else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_RESOURCE5)
		{
			*pulArg1 = HARDWARE_RESOURCE5;
			*pulArg2 = 0x1;
		}
		else if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_BP0)
		{
			*pulArg1 = SOFTWARE_BP0;
			*pulArg2 = 0x0;
		}
		else
		{
			// The handle given is not a valid breakpoint. (may not have been set or could be a watchpoint)
			return ML_Error_NoSuchHandle;
		}
	}

	else if ((tyProcessorConfig.bCortexM3) ||
			 (tyProcessorConfig.bCortexA8) ||
			 (tyProcessorConfig.bCortexA9))
	{
		if (pCortexBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_BP0)
		{
			*pulArg1 = SOFTWARE_BP0;
			*pulArg2 = 0x0;
		}
		else
		{
			unsigned long ulHbPointRes;
			unsigned long ulMaxNumBreakpoint;

			if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
			{
				ulMaxNumBreakpoint = tyArmv7ABreakpointResArray.ulNumBreakpoint;
			} 
			else
			{
				ulMaxNumBreakpoint = tyArmv7MBreakpointResArray.ulNumBreakpoint;
			}

			for (ulHbPointRes = HARDWARE_BP0; ulHbPointRes < ulMaxNumBreakpoint; ulHbPointRes++)
			{
				if (pCortexBPArray->BreakpointStructures[i].ucBPIDNo == ulHbPointRes)
				{
					*pulArg1 = ulHbPointRes;
					*pulArg2 = 0x1;
					break;                
				}
				
			}

			if (ulHbPointRes == tyArmv7MBreakpointResArray.ulNumBreakpoint)
			{
				return ML_Error_NoSuchHandle;
			}
		}
		
	}

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_WatchpointInquiry
    Engineer: Suraj S
       Input: unsigned long *pulArg1 - Handle of the WP inquired as INPUT
                                     - Hardware resource number aa OUTPUT
              unsigned long *pulArg2 - Watchpoint type ie hardware / software
      Output: TyError - Mid Layer error code   
 Description: This function helps the Debugger to get info about a watchpoint using 
              RDI_InfoProc#RDIPointStatus_Watch
Date          Initials     Description
20-Aug-2007     SJ         Initial
21-Apr-2010		JCK		Cortex-A8 Support Added
****************************************************************************/
TyError ML_WatchpointInquiry (unsigned long *pulArg1,unsigned long *pulArg2)
{
   unsigned long i=0;

   if ((tyProcessorConfig.bCortexM3) ||
	   (tyProcessorConfig.bCortexA8) ||
	   (tyProcessorConfig.bCortexA9))
   {
	   // find the original instruction data and address...
	   for (i=0; i < MAX_NUM_BREAKPOINTS; i++)
	   {
		   if ((pCortexBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)*pulArg1) &&
			   (pCortexBPArray->BreakpointStructures[i].bWatchPoint))
			   break;
	   }
	   if ((i == MAX_NUM_BREAKPOINTS) && (pBPArray->BreakpointStructures[i].usBreakpointId != (unsigned short)*pulArg1))
		   return ML_Error_NoSuchHandle;
	   
	   *pulArg1 = pCortexBPArray->BreakpointStructures[i].ucBPIDNo - WATCHPOINT_RECOURSE_0;
	   *pulArg2 = 1;	   

   }
   else
   {
	   // find the original instruction data and address...
	   for (i=0; i < MAX_NUM_BREAKPOINTS; i++)
	   {
		   if ((pBPArray->BreakpointStructures[i].usBreakpointId == (unsigned short)*pulArg1) &&
			   (pBPArray->BreakpointStructures[i].bWatchPoint))
			   break;
	   }
	   if ((i == MAX_NUM_BREAKPOINTS) && (pBPArray->BreakpointStructures[i].usBreakpointId != (unsigned short)*pulArg1))
		   return ML_Error_NoSuchHandle;
	   
	   //This gives the Watchpoint type ie 0 = software, 1 = hardware
	   if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_WP0)
	   {
		   *pulArg1 = HARDWARE_RESOURCE0;
		   *pulArg2 = 0x1;
	   }
	   else if (pBPArray->BreakpointStructures[i].ucBPIDNo == HARDWARE_WP1)
	   {
		   *pulArg1 = HARDWARE_RESOURCE1;
		   *pulArg2 = 0x1;
	   }
	   else if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_WP0)
	   {
		   *pulArg1 = HARDWARE_RESOURCE0;
		   *pulArg2 = 0x0;
	   }
	   else if (pBPArray->BreakpointStructures[i].ucBPIDNo == SOFTWARE_WP1)
	   {
		   *pulArg1 = HARDWARE_RESOURCE1;
		   *pulArg2 = 0x0;
	   }
	   else
		   // The handle given is not a valid breakpoint. (may not have been set or could be a watchpoint)
		   return ML_Error_NoSuchHandle;

   }
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_SetLockedPoints
    Engineer: Suraj S
       Input: unsigned long ulLockedPoints - mask of watchpoint resources 
              (only bits 0 & 1 are used)
      Output: NONE
 Description: This function helps the Debugger to set locks on the watchpoint 
              resources using RDI_InfoProc#RDIIcebreaker_SetLocks
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
void  ML_SetLockedPoints (unsigned long ulLockedPoints)
{
   //Clear the locks.
   ulEmbeddedICELockedPoints = 0;
   tyBPResourceConfig.bWP0Locked= FALSE; 
   tyBPResourceConfig.bWP1Locked= FALSE; 
   
   if ((ulLockedPoints & WP0LockMask) == WP0LockMask)
      {
      //This is locking WP0
      tyBPResourceConfig.bWP0Locked= TRUE; 
      ulEmbeddedICELockedPoints = (ulEmbeddedICELockedPoints | WP0LockMask);
      }
   if ((ulLockedPoints & WP1LockMask) == WP1LockMask)
      {
      //This is locking WP1
      tyBPResourceConfig.bWP1Locked= TRUE; 
      ulEmbeddedICELockedPoints = (ulEmbeddedICELockedPoints | WP1LockMask);
      }
}

/****************************************************************************
    Function: ML_GetLockedPoints
    Engineer: Suraj S
       Input: unsigned long *ulLockedPoints - Pointer to return locked points 
              status
      Output: NONE
 Description: This function helps the Debugger to get locks on the watchpoint 
              resources using RDI_InfoProc#RDIIcebreaker_GetLocks. Returns a 
              bit mask with the hardware resource locks
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
void  ML_GetLockedPoints (unsigned long *ulLockedPoints)
{
   *ulLockedPoints = ulEmbeddedICELockedPoints;
}

/****************************************************************************
    Function: ML_SetTopOfMemory
    Engineer: Suraj S
       Input: unsigned long ulMemorySize - Top of memory address
      Output: NONE.
 Description: This function helps the Debugger to set the top of memory using 
              RDI_InfoProc#RDIInfo_SetTopMem(used for semihosting GetHeapinfo calls)
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
void ML_SetTopOfMemory (unsigned long ulMemorySize)
{
	ulTopOfMemory = ulMemorySize;
}

/****************************************************************************
    Function: ML_GetSafeVectorAddress
    Engineer: Suraj S
       Input: unsigned long *pulSafeAddress - pointer to return address value
      Output: NONE.
 Description: This function helps the Debugger to get the value set as the safe
              non vector address using RDI_InfoProc#RDIInfo_GetSafeNonVectorAddress
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
void ML_GetSafeVectorAddress (unsigned long *pulSafeAddress)
{
	*pulSafeAddress = ulGlobalSafeNonVectorAddress;
	
	return;
}

/****************************************************************************
    Function: ML_QueryMovableVector
    Engineer: Suraj S
       Input: NONE
      Output: boolean
 Description: This function helps the Debugger to know whether the target 
              supports movable vectors using RDI_InfoProc#RDIInfo_MovableVectors
              Returns TRUE if the target supports movable vector address.
              Currently only the ARM720 supports this.
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
boolean ML_QueryMovableVector (void)
{
	return (tyProcessorConfig.tyCoProcessorType ==  ARM720T_CP);
}

/****************************************************************************
    Function: ML_GetVectorAddress
    Engineer: Suraj S
       Input: unsigned long *ulVectorAddress - pointer to vector address
      Output: TyError - Mid Layer error code
 Description: This function helps the Debugger to get the Vector table address
              using RDI_InfoProc#RDIInfo_GetVectorAddress. 
              Gets the Vector table address.
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ML_GetVectorAddress (unsigned long *ulVectorAddress)
{
	*ulVectorAddress = ulVectorTableBaseAddress;
	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_GetDeviceInfo
    Engineer: Suraj S
       Input: TyDeviceInfo tyDeviceInfo - Device info structure
      Output: TyError - Mid Layer error code
 Description: This function stores the opella-xd device information
Date          Initials     Description
27-Sep-2007     SJ         Initial
****************************************************************************/
TyError ML_GetDeviceInfo ( TyDeviceInfo *tyDeviceInfo )
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	
	ErrRet = DL_OPXD_GetDeviceInfo(tyCurrentDevice, tyDeviceInfo);
	
	return ErrRet;
}

/****************************************************************************
    Function: ML_CloseOpellaXD
    Engineer: Suraj S
       Input: None
      Output: TyError - Mid Layer error code   
 Description: Closes the Opella XD target connection
Date          Initials     Description
4-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_CloseOpellaXD (void)

{
	
	if(tyCurrentDevice < MAX_DEVICES_SUPPORTED)
	{
		(void)DL_OPXD_SetVtpaVoltage(tyCurrentDevice, 0, 1, 0);    // disconnect TPA buffers
		(void)DL_OPXD_UnconfigureDevice(tyCurrentDevice);
		(void)DL_OPXD_CloseDevice(tyCurrentDevice);
		tyCurrentDevice = MAX_DEVICES_SUPPORTED;
		bUSBDevice = FALSE;
	}
	
	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_Delay
    Engineer: Suraj S
       Input: Delay in milliseconds.
      Output: NONE
 Description: Delay for OpellaXD.
   Date        Initials    Description
30-Jul-2007      SJ         Initial
****************************************************************************/
void ML_Delay (unsigned long ulDelay)
{
#ifdef _WINDOWS
	unsigned long ulTickCount;
	unsigned long ulEndTime;
	ulTickCount = GetTickCount();
	ulEndTime = (ulTickCount + ulDelay);
	
	while (ulTickCount < (ulEndTime)){
		ulTickCount = GetTickCount();
		Sleep(1);
	}
#elif __LINUX
	usleep(ulDelay*1000);
#endif
}

/****************************************************************************
    Function: ML_Convert_DrvLayerErr_ToMidLayerErr
    Engineer: Suraj S
       Input: DrvLayer Error code
      Output: MidLayer Error code
 Description: Converts DrvLayer Errors to MidLayer Errors
   Date        Initials    Description
4-Jul-2007      SJ         Initial
****************************************************************************/
TyError ML_Convert_DrvLayerErr_ToMidLayerErr (TyError tyDRVLayerErr)

{
	switch (tyDRVLayerErr)
	{
	case DRVOPXD_ERROR_NO_ERROR             : 
		return ML_ERROR_NO_ERROR;
		
	case DRVOPXD_ERROR_NO_USB_DEVICE_CONNECTED	:
	case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
		return ML_ERROR_INVALID_SERIAL_NO;
		
	case DRVOPXD_ERROR_USB_DRIVER           : 
		return ML_ERROR_OPELLA_FAILURE;
		
	case DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE       : 
		return ML_ERROR_FAILED_TO_PROGRAM_FPGA;
		
	case DRVOPXD_ERROR_DEVICE_ALREADY_IN_USE	:
		return ML_ERROR_FAILED_SEND_USB_PACKET;
		
	case DRVOPXD_ERROR_INVALID_FIRMWARE:
	case DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME:
		return ML_ERROR_FAILED_TO_FIND_DISKWARE_FILE;
		
	case DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME:
		return ML_ERROR_FAILED_TO_FIND_FPGA_FILE;
		
  	case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
		return ML_Error_NoRTCKSupport;
			
	case DRVOPXD_ERROR_CONFIGURATION:
		return ML_Error_SoftInitialiseError;
			
	case DRVOPXD_ERROR_ARM_DATA_ABORT           : 
		return ML_Error_DataAbort;

	default:
		return ML_ERROR_OPELLA_FAILURE;
	}
}

/****************************************************************************
    Function: ML_Initialise_Debugger_Third_party
    Engineer: Suraj S
       Input: double ulFreqKHz - The frequency value in kHz
      Output: TyError - Mid Layer error code   
 Description: Initialise Debugger Hardware
Date          Initials     Description
4-Jul-2007      SJ         Initial
****************************************************************************/
#ifndef __LINUX
TyError ML_Initialise_Debugger_Third_party (TyDevHandle tyDevHandle, 
											double ulFreqKHz,
											double dTargetVoltage, 
											TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo)//, TyCoreConfig *ptyCoreConfig)

{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	TyTmsSequence ptyTmsForScanIR, ptyTmsForScanDR, ptyTmsForScanIRandDR_IR, ptyTmsForScanIRandDR_DR;
	TyArmProcType TyProcType = NOCORE;
	unsigned long ulNumCores;
	//unsigned long ulIRLengthArray[16];
	//unsigned long ulTotalIRLength = 0;
	unsigned long ulTotalIRLengthUser = 0;
	unsigned int j = 0;
	
	//unsigned long  pulBypassIRPattern[16];
	//TyCoreConfig ptyCoreConfig[16];
	
	//Set the default TMS sequence for ScanIR and ScanDR
	ptyTmsForScanIR.ucPostTmsBits  = 0xD;
	ptyTmsForScanIR.ucPostTmsCount = 0x5;
	ptyTmsForScanIR.ucPreTmsBits   = 0x3;
	ptyTmsForScanIR.ucPreTmsCount  = 0x4;
	
	ptyTmsForScanIRandDR_IR = ptyTmsForScanIR;
	
	ptyTmsForScanDR.ucPostTmsBits  = 0xD;
	ptyTmsForScanDR.ucPostTmsCount = 0x5;
	ptyTmsForScanDR.ucPreTmsBits   = 0x1;
	ptyTmsForScanDR.ucPreTmsCount  = 0x3;
	
	ptyTmsForScanIRandDR_DR = ptyTmsForScanDR;
	
	
	ulGlobalScanChain = FORCE_SCANCHAIN_SELECT;
	if(ptyRDIOnTargetDiagInfo == NULL )
	{
		double dFrequenyMHz = ulFreqKHz/KILOHZ; 
		ErrRet = DL_OPXD_JtagSetFrequency(tyDevHandle, dFrequenyMHz, NULL, NULL);
	}
	else
	{
		if (ptyRDIOnTargetDiagInfo->tyJTAGClockFreqMode != Adaptive)
		{
			double dFrequenyKHz = (double)ptyRDIOnTargetDiagInfo->ulFreqVal;
			double dFrequenyMHz = dFrequenyKHz/KILOHZ; 
			ErrRet = DL_OPXD_JtagSetFrequency(tyDevHandle, dFrequenyMHz, NULL, NULL);
		}
		else 
			ErrRet = DL_OPXD_JtagSetAdaptiveClock(tyDevHandle, 1, 1);
	}
	
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return ML_ERROR_INVALID_CONFIG;
	
	if (TRUE == ptyRDIOnTargetDiagInfo->bTICore)
	{
		ErrRet = DL_OPXD_JtagSetTMS(tyCurrentDevice, NULL, NULL, NULL, NULL);   // set default TMS sequence			  
	}
	else
	{
		ErrRet = DL_OPXD_JtagSetTMS(tyCurrentDevice, &ptyTmsForScanIR, &ptyTmsForScanDR, 
			&ptyTmsForScanIRandDR_IR, &ptyTmsForScanIRandDR_DR);   // set default TMS sequence
	}
	if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
	{
		if (dTargetVoltage == 0.0)
			ErrRet = DL_OPXD_SetVtpaVoltage(tyDevHandle, 0.0, TRUE, TRUE);
		else
			ErrRet = DL_OPXD_SetVtpaVoltage(tyDevHandle, dTargetVoltage, FALSE, TRUE);             // set tracking voltage and enable drivers to target
	}
	ML_Delay(500);//Assumes a delay is required here for proper functioning of Diskware
	
	tyTargetConfig.ulSelectedNoOfCores = ptyRDIOnTargetDiagInfo->ptyJtagScanChain->ucNumJtagNodes;
	
	ErrRet = DL_OPXD_JtagConfigMulticore(tyDevHandle, 0, NULL);         // set no MC support   
	if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
	{
		ErrRet = DL_OPXD_JtagResetTAP(tyDevHandle, 0);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return ML_ERROR_INVALID_CONFIG;
		
		ulNumCores = tyTargetConfig.ulSelectedNoOfCores;
		
		/*	Do the initialization for TI devices - start	*/
		if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) && 
			(TRUE == ptyRDIOnTargetDiagInfo->bTICore))
		{
			if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
				ErrRet = DL_OPXD_JtagResetTAP(tyDevHandle, 0);                      // in addition, ensure TAPs are in initial state
			
			if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
				ErrRet = DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(tyDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType);                      // in addition, ensure TAPs are in initial state
			
			if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
				DL_OPXD_InitIcepick( tyDevHandle, 
				ptyRDIOnTargetDiagInfo->ulDbgCoreNumber, 
				ptyRDIOnTargetDiagInfo->ulTISubPortNum );
			
			/* Diskware function is moved to RDI. */
			//ErrRet = DL_OPXD_InitIcepick ( tyCurrentDevice, 0, ptyTargetConfig->ulTISubPortNum );
		}
		
#if 0
		ErrRet=DL_OPXD_Arm_GetMultiCoreInfo(tyDevHandle, &ulNumCores, ulIRLengthArray);
		if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
			return ML_ERROR_INVALID_CONFIG;
		
		for(j = 0;j < ulNumCores ; j++)
			ulTotalIRLength +=  ulIRLengthArray[j];      
		
#endif
		if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
		{
			if (tyTargetConfig.ulSelectedNoOfCores>1)
			{	  
				for(j=0; j< (tyTargetConfig.ulSelectedNoOfCores) ; j++)
				{
					pulBypassIRPattern[j] = BYPASS_PATTERN;
					ptyCoreConfig[j].pulBypassIRPattern = &pulBypassIRPattern[j];
					ptyCoreConfig[j].usDefaultDRLength  = 0x1;
					ptyCoreConfig[j].usDefaultIRLength  = (unsigned short)ptyRDIOnTargetDiagInfo->ptyJtagScanChain->ptyJtagNode[j].ucIRLength;
					pulIRLengthArray[j]                 = (unsigned short)ptyRDIOnTargetDiagInfo->ptyJtagScanChain->ptyJtagNode[j].ucIRLength;
					ulTotalIRLengthUser                +=  pulIRLengthArray[j];               
				}
				
#if 0
				if(ulTotalIRLength != ulTotalIRLengthUser)
					return ML_ERROR_INVALID_CONFIG;
#endif
				
				ErrRet = DL_OPXD_JtagConfigMulticore(tyDevHandle, tyTargetConfig.ulSelectedNoOfCores, ptyCoreConfig);         // set no MC support
			}
			else
			{
				ErrRet=DL_OPXD_Arm_GetMultiCoreInfo(tyDevHandle, &tyTargetConfig.ulSelectedNoOfCores, pulIRLengthArray);        //for getting the information of no of cores in scanchain and corrosponding IR length
				
				ulNumCores = tyTargetConfig.ulSelectedNoOfCores;
				if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
				{
					for(j =0; j < tyTargetConfig.ulSelectedNoOfCores; j++)
					{
						if(pulIRLengthArray[j] < 3)
							return ML_ERROR_INVALID_CONFIG;
					}
					if (tyTargetConfig.ulSelectedNoOfCores>1)
					{
						for(j=0; j< (tyTargetConfig.ulSelectedNoOfCores) ; j++)
						{
							pulBypassIRPattern[j] = BYPASS_PATTERN;
							ptyCoreConfig[j].pulBypassIRPattern = &pulBypassIRPattern[j];
							ptyCoreConfig[j].usDefaultDRLength  = 0x1;
							ptyCoreConfig[j].usDefaultIRLength  = (unsigned short)pulIRLengthArray[j];
						}
						ErrRet = DL_OPXD_JtagConfigMulticore(tyDevHandle, tyTargetConfig.ulSelectedNoOfCores, ptyCoreConfig); 
					}
					else
						ErrRet = DL_OPXD_JtagConfigMulticore(tyDevHandle, 0, NULL);         // set no MC support
				}
			} 
			
#if 0
			tyTargetConfig.ulSelectedNoOfCores = ulNumCores;
			for(j=0; j< (tyTargetConfig.ulSelectedNoOfCores) ; j++)           
				pulIRLengthArray[j] = ulIRLengthArray[j];            
#endif
		}
	}  
	
	if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) &&
		(FALSE == ptyRDIOnTargetDiagInfo->bTICore))
		ErrRet = DL_OPXD_JtagResetTAP(tyDevHandle, 0);	// in addition, ensure TAPs are in initial state
	//ML_Delay(100);//Assumes a delay is required here for proper functioning of Diskware
	if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) &&
		(FALSE == ptyRDIOnTargetDiagInfo->bTICore))
		ErrRet = DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(tyDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType);                      // in addition, ensure TAPs are in initial state
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	
	if ((ErrRet == DRVOPXD_ERROR_NO_ERROR) && (TRUE == ptyRDIOnTargetDiagInfo->bCoresightCompliant))
	{
		// 		if (0 == ptyRDIOnTargetDiagInfo->ulCoresightBaseAddr)
		// 		{
		// 			return ML_ERROR_INVALID_CORESIGHT_CONFIG;
		// 		}
		ErrRet = DL_OPXD_Config_Coresight(tyDevHandle, ptyRDIOnTargetDiagInfo->ulDbgCoreNumber, ptyRDIOnTargetDiagInfo->ulCoresightBaseAddr,		   
			ptyRDIOnTargetDiagInfo->ulDbgApIndex, ptyRDIOnTargetDiagInfo->ulMemApIndex,
			ptyRDIOnTargetDiagInfo->ulJtagApIndex, 0);
	}
	
	return ErrRet;
}
#endif
/***************************************************************************************
     Function: ML_ARM9StepProc
     Engineer: Jeenus Challattu Kunnath
        Input: None
       Output: TyError - Mid Layer error code
  Description: Single stepping 
Date          Initials      Description
13-Mar-2008      JCK        Initial
****************************************************************************************/
TyError ML_ARM9StepProc()
{
    int             ErrRet                  = NO_ERROR;
    unsigned long   ulTempReg0              = 0x0;
    unsigned long   ulDebugControlReg       = 0;
    unsigned long   ulDebugStatusValue      = 0;
    unsigned long   ulReg0Val = 0x0;
    
    if (bGlobalThumb)
      {
      ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];
      RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;
      }

    ErrRet = ML_ReadICEBreaker (DBG_STATUS_REG, &ulDebugStatusValue);
    if (ErrRet != ML_ERROR_NO_ERROR)
        return ErrRet;
    if ((ulDebugStatusValue & ARM_DEBUG_MODE) == ARM_DEBUG_MODE)
    {
        ErrRet = ML_ReadICEBreaker (DBG_CONTROL_REG, &ulDebugControlReg);
        if (ErrRet != ML_ERROR_NO_ERROR)
            return ErrRet;      
        //Enable single stepping
        ErrRet = ML_WriteICEBreaker (DBG_CONTROL_REG, (ulDebugControlReg | 0x08));
        if (ErrRet!=ML_ERROR_NO_ERROR)
            return ErrRet;
    }
    // Restore the registers before execution.
    ErrRet = ML_WriteRegisters((unsigned long *)RegArray);

    if(ErrRet != ML_ERROR_NO_ERROR)
    {
        bGlobalStartedExecution = FALSE; 
        return ErrRet;
    }
    ulReg0Val = ulTempReg0;
    ErrRet = DL_OPXD_Arm_ExecuteProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,ARM9, bGlobalThumb, ulReg0Val);
    if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
        return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
    return ErrRet;
}
/***************************************************************************************
     Function: ML_ARM7StepProc
     Engineer: Jeenus Challattu Kunnath
        Input: None
       Output: TyError - Mid Layer error code
  Description: Single stepping 
Date          Initials      Description
13-Mar-2008      JCK        Initial
23-Apr-2010      JCK        
****************************************************************************************/
TyError ML_ARM7StepProc()
{
    unsigned long     ErrRet = ML_ERROR_NO_ERROR;
    unsigned long     ulDest;
    unsigned long     ulBytes = 4;
    unsigned long     ulFirstAddress;
    unsigned long     ulSecondAddress;     
    unsigned long     ulPC; 
    unsigned long     ulTempReg0 = 0;
    unsigned long     ulReg0Val;
    DisaInst          CurrentInst;      
    unsigned long	  ulWP_CTRL_M = 0;
    unsigned long	  ulWP_CTRL_V = 0;
	
    if(bGlobalThumb)
        ulBytes = 2;
	
    ulPC = RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC];
	
    ErrRet = ML_ReadFromTargetMemory(ulPC,
		&ulDest,
		&ulBytes,
		RDIAccess_Code);
    if(ErrRet != ML_ERROR_NO_ERROR)
        return ErrRet;
    if(bGlobalThumb)
    {
        //These variables are used to switch back from ARM mode to Thumb Mode.
        ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];
        //RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;
        ErrRet = Disassemble((unsigned char *)&ulDest,
			&CurrentInst,                       
			ulPC,
			NULL,
			DISA_STEPPING,
			(ProcDisaType)2);
        if(ErrRet != NO_ERROR)
			return ErrRet;
    }
    else
    {         
        ErrRet = Disassemble((unsigned char *)&ulDest,
			&CurrentInst,                       
			ulPC,
			NULL,
			DISA_STEPPING,
			(ProcDisaType)1);
    }
	
    if (ErrRet != NO_ERROR)
        return ErrRet;
	
	
    ulFirstAddress  = ulPC + CurrentInst.InstLen;
    ulSecondAddress = CurrentInst.DestAddr;
	
    if ((tyProcessorConfig.bARM7) && (tyProcessorConfig.bThumb))
    {
        if (!bGlobalThumb)
            ulWP_CTRL_V = 0x104;//ARM7TDMI_ARM_SW_BP_VALUE; 
        else
            ulWP_CTRL_V =  0x102;//ARM7TDMI_THUMB_SW_BP_VALUE;
		
        ulWP_CTRL_M =  0xF6;//ARM7TDMI_ARM_SW_BP_MASK ;
    }
	
    else if ((tyProcessorConfig.bARM7) && (!tyProcessorConfig.bThumb))
		// Set watchpoint registers for ARM7DI board
    {
        ulWP_CTRL_V =  0x82;//ARM7DI_ARM_SW_BP_VALUE; 
        ulWP_CTRL_M =  0x78;//ARM7DI_ARM_SW_BP_MASK; 
    } 
    
    if(ulFirstAddress == ulSecondAddress)
    {
        ErrRet = ML_WriteICEBreaker (WP0_CTRL_V, 0x0);
        if(bSWIInstruction)
        {
            ErrRet = ML_WriteICEBreaker (WP0_ADDR_V, 0x8);
            ErrRet = ML_WriteICEBreaker (WP0_ADDR_M, 0x1);
            ErrRet = ML_WriteICEBreaker (WP0_DATA_V,0xFFFFFFFF );
            ErrRet = ML_WriteICEBreaker (WP0_DATA_M,0xFFFFFFFF );
            ErrRet = ML_WriteICEBreaker (WP0_CTRL_V, ulWP_CTRL_V);
            ErrRet = ML_WriteICEBreaker (WP0_CTRL_M, ulWP_CTRL_M);
        }
		
        ErrRet = ML_WriteICEBreaker (WP1_ADDR_V, ulFirstAddress);
        ErrRet = ML_WriteICEBreaker (WP1_ADDR_M, 0x1);
        ErrRet = ML_WriteICEBreaker (WP1_DATA_V,0xFFFFFFFF );
        ErrRet = ML_WriteICEBreaker (WP1_DATA_M,0xFFFFFFFF );
        ErrRet = ML_WriteICEBreaker (WP1_CTRL_V, ulWP_CTRL_V);
        ErrRet = ML_WriteICEBreaker (WP1_CTRL_M, ulWP_CTRL_M);
		
        ulReg0Val = ulTempReg0;        
        ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
        if(ErrRet != ML_ERROR_NO_ERROR)
        {
            bGlobalStartedExecution = FALSE;  
			return ErrRet;
        }
		
        ErrRet = DL_OPXD_Arm_ExecuteProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,ARM7, bGlobalThumb, ulReg0Val);
        if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
            return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
        if(bSWIInstruction)
        {
            ErrRet = ML_ReadRegisters((unsigned long *)RegArray);
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;
			
            ErrRet = ML_ARM_SetupExecuteProc(tyBPResourceConfig.ulWP0AddressValue,
				tyBPResourceConfig.ulWP0AddressMask,                       
				tyBPResourceConfig.ulWP0DataValue,    
				tyBPResourceConfig.ulWP0DataMask,     
				tyBPResourceConfig.ulWP0CtrlValue,    
				tyBPResourceConfig.ulWP0CtrlMask,     
				tyBPResourceConfig.ulWP1AddressValue, 
				tyBPResourceConfig.ulWP1AddressMask,  
				tyBPResourceConfig.ulWP1DataValue,	   
				tyBPResourceConfig.ulWP1DataMask,	   
				tyBPResourceConfig.ulWP1CtrlValue,	   
				tyBPResourceConfig.ulWP1CtrlMask);
			
            if (ErrRet != ML_ERROR_NO_ERROR)
                return ErrRet;
			
            ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
			
            if(ErrRet != ML_ERROR_NO_ERROR)
            {
                bGlobalStartedExecution = FALSE;  
                return ErrRet;
            }  
        }
    }
    else
    {
        ErrRet = ML_WriteICEBreaker (WP0_ADDR_V, ulFirstAddress);
        ErrRet = ML_WriteICEBreaker (WP0_ADDR_M, 0x1);
        ErrRet = ML_WriteICEBreaker (WP0_DATA_V,0xFFFFFFFF );
        ErrRet = ML_WriteICEBreaker (WP0_DATA_M,0xFFFFFFFF );
        ErrRet = ML_WriteICEBreaker (WP0_CTRL_V, ulWP_CTRL_V);
        ErrRet = ML_WriteICEBreaker (WP0_CTRL_M, ulWP_CTRL_M);
		
        ErrRet = ML_WriteICEBreaker (WP1_ADDR_V, ulSecondAddress);
        ErrRet = ML_WriteICEBreaker (WP1_ADDR_M, 0x1);
        ErrRet = ML_WriteICEBreaker (WP1_DATA_V,0xFFFFFFFF );
        ErrRet = ML_WriteICEBreaker (WP1_DATA_M,0xFFFFFFFF );
        ErrRet = ML_WriteICEBreaker (WP1_CTRL_V, ulWP_CTRL_V);
        ErrRet = ML_WriteICEBreaker (WP1_CTRL_M, ulWP_CTRL_M);
		
        ulReg0Val = ulTempReg0;        
        ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
        if(ErrRet != ML_ERROR_NO_ERROR)
        {
            bGlobalStartedExecution = FALSE;  return ErrRet;
        }
		
        ErrRet = DL_OPXD_Arm_ExecuteProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,ARM7, bGlobalThumb, ulReg0Val);
        if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
            return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);               
    }      

	//These functions leave the core in ScanChain 2
    ulGlobalScanChain = 0x2;       
	
    return ErrRet;
}

/***************************************************************************************
     Function: ML_ARM11StepProc
     Engineer: Roshan T R
        Input: None
       Output: TyError - Mid Layer error code
  Description: Single stepping 
Date          Initials      Description
26-Aug-2009   RTR           Initial
****************************************************************************************/
TyError ML_ARM11StepProc()
{
    unsigned long     ErrRet = ML_ERROR_NO_ERROR;
    unsigned long     ulDest;
    unsigned long     ulBytes = 4;
    unsigned long     ulFirstAddress;
    unsigned long     ulSecondAddress;     
    unsigned long     ulPC; 
    unsigned long     ulTempReg0 = 0;
    DisaInst          CurrentInst;      
	unsigned char     ucBytePosition;
	unsigned long     ulBPAddressWord1 = 0, ulBPAddressWord2 = 0;
	unsigned long     ulBPCtrlWord1 = 0, ulBPCtrlWord2 = 0;

	TyArmProcType	  TyProcType = NOCORE;

	if(tyProcessorConfig.bARM11)
	{
		TyProcType = ARM11;
	}
	
	ErrRet = ML_ARM_SetupExecuteProcARM11((unsigned long*)&tyBPResourceARM11Config);
	
	if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;

    if(bGlobalThumb)
        ulBytes = 2;
	
    ulPC = RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC];
	
    ErrRet = ML_ReadFromTargetMemory(ulPC,
		&ulDest,
		&ulBytes,
		RDIAccess_Code);
    if(ErrRet != ML_ERROR_NO_ERROR)
        return ErrRet;
    if(bGlobalThumb)
    {
        //These variables are used to switch back from ARM mode to Thumb Mode.
        ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];
        RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;
        ErrRet = Disassemble((unsigned char *)&ulDest,
			&CurrentInst,                       
			ulPC,
			NULL,
			DISA_STEPPING,
			(ProcDisaType)2);
        if(ErrRet != NO_ERROR)
			return ErrRet;
    }
    else
    {         
		ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];
        ErrRet = Disassemble((unsigned char *)&ulDest,
			&CurrentInst,                       
			ulPC,
			NULL,
			DISA_STEPPING,
			(ProcDisaType)1);
    }
	
    if (ErrRet != NO_ERROR)
        return ErrRet;
	
	
    ulFirstAddress  = ulPC + CurrentInst.InstLen;
    ulSecondAddress = CurrentInst.DestAddr;
	
	
	if(bGlobalThumb)
	{
		ucBytePosition = (unsigned char)(ulFirstAddress % 4);
		ulBPAddressWord1 = ulFirstAddress - ucBytePosition;
	
		if(ucBytePosition == 3)
		{
			ulBPCtrlWord1 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE3_ACCESS_CTRL_VALUE;
		}
		else if(ucBytePosition == 2)
		{
			ulBPCtrlWord1 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE2_ACCESS_CTRL_VALUE;
		}
		else if(ucBytePosition == 1)
		{
			ulBPCtrlWord1 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE1_ACCESS_CTRL_VALUE;
		}
		else
		{
			//ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE0_ACCESS_CTRL_VALUE;
			//This is word access now
			ulBPCtrlWord1 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_WORD_ACCESS_CTRL_VALUE;
		}
		
		if(ulFirstAddress != ulSecondAddress)
		{
			ucBytePosition = (unsigned char)(ulSecondAddress % 4);
			ulBPAddressWord2 = ulSecondAddress - ucBytePosition;
			if(ucBytePosition == 3)
			{
				ulBPCtrlWord2 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE3_ACCESS_CTRL_VALUE;
			}
			else if(ucBytePosition == 2)
			{
				ulBPCtrlWord2 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE2_ACCESS_CTRL_VALUE;
			}
			else if(ucBytePosition == 1)
			{
				ulBPCtrlWord2 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE1_ACCESS_CTRL_VALUE;
			}
			else
			{
				//ulBPCtrlWord = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_BYTE0_ACCESS_CTRL_VALUE;
				//This is word access now
				ulBPCtrlWord2 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_WORD_ACCESS_CTRL_VALUE;
			}
			
		}
	}
	else
	{
		ulBPAddressWord1 = ulFirstAddress;
		ulBPAddressWord2 = ulSecondAddress;
		ulBPCtrlWord1 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_HALF0_ACCESS_CTRL_VALUE;
		ulBPCtrlWord2 = ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_HALF0_ACCESS_CTRL_VALUE;
	}
	
    
	if(!ucR15)
    {   
        if((ulDest != ARM11_ARM_BKPT_INST) && (ulDest != ARM11_THUMB_BKPT_INST))
        {
            if(ulFirstAddress == ulSecondAddress)
            {
				ErrRet = ML_WriteCP14DebugRegARM11 (BP0_VALUE_REGISTER, 
					0x00);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				ErrRet = ML_WriteCP14DebugRegARM11 (BP0_CTRL_REGISTER, 
					0x00);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				ErrRet = ML_WriteCP14DebugRegARM11 (BP1_VALUE_REGISTER, 
					0x00);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				ErrRet = ML_WriteCP14DebugRegARM11 (BP1_CTRL_REGISTER, 
					0x00);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
                if(bSWIInstruction)
				{
					ErrRet = ML_WriteCP14DebugRegARM11 (BP0_VALUE_REGISTER, 
						0x08);
					if (ErrRet!=ML_ERROR_NO_ERROR)
						return ErrRet;
					
					ErrRet = ML_WriteCP14DebugRegARM11 (BP0_CTRL_REGISTER, 
						ARM11_ARM_HW_BP_VALUE | ARM11_HW_BP_HALF0_ACCESS_CTRL_VALUE);
					if (ErrRet!=ML_ERROR_NO_ERROR)
						return ErrRet;
				}
				ErrRet = ML_WriteCP14DebugRegARM11 (BP1_VALUE_REGISTER, 
					ulBPAddressWord1);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				ErrRet = ML_WriteCP14DebugRegARM11 (BP1_CTRL_REGISTER, 
					ulBPCtrlWord1);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				
				//RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = ulTempReg0;
                ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
                if(ErrRet != ML_ERROR_NO_ERROR)
                {
                    bGlobalStartedExecution = FALSE;  return ErrRet;
                }
				
				ARM11_DataArray[2] = ulTempReg0;
				ErrRet = DL_OPXD_Arm_Leave_Debug_State(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bGlobalThumb, ARM11_DataArray,sizeof(ARM11_DataArray));
                if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
                    return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
				
                if(bSWIInstruction)
                {
                    ErrRet = ML_ReadRegisters((unsigned long *)RegArray);
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;
					
                    ErrRet = ML_ARM_SetupExecuteProcARM11((unsigned long*)&tyBPResourceARM11Config);
					
                    if (ErrRet != ML_ERROR_NO_ERROR)
                        return ErrRet;
					
                    ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
					
                    if(ErrRet != ML_ERROR_NO_ERROR)
                    {
                        bGlobalStartedExecution = FALSE;  
                        return ErrRet;
                    }  
                }
				
            }
            else
            {
				ErrRet = ML_WriteCP14DebugRegARM11 (BP0_VALUE_REGISTER, 
					ulBPAddressWord1);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				ErrRet = ML_WriteCP14DebugRegARM11 (BP0_CTRL_REGISTER, 
					ulBPCtrlWord1);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				ErrRet = ML_WriteCP14DebugRegARM11 (BP1_VALUE_REGISTER, 
					ulBPAddressWord2);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				ErrRet = ML_WriteCP14DebugRegARM11 (BP1_CTRL_REGISTER, 
					ulBPCtrlWord2);
				if (ErrRet!=ML_ERROR_NO_ERROR)
					return ErrRet;
				
				//RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = ulTempReg0;
                ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
                if(ErrRet != ML_ERROR_NO_ERROR)
                {
                    bGlobalStartedExecution = FALSE;  return ErrRet;
                }
				ARM11_DataArray[2] = ulTempReg0;
				ErrRet = DL_OPXD_Arm_Leave_Debug_State(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bGlobalThumb, ARM11_DataArray,sizeof(ARM11_DataArray));
                if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
                    return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);               
            }      
        }   
    }
    else
    {
        RegArray[REG_ARRAY_USR_MODE] [REG_ARRAY_PC] = ulSecondAddress;
        ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
        if(ErrRet != ML_ERROR_NO_ERROR)
        {
            bGlobalStartedExecution = FALSE; 
            return ErrRet;
        }
        ucR15 = 0;
		
    }
	//These functions leave the core in ScanChain 2
    ulGlobalScanChain = 0x2;       
	
    return ErrRet;
}

/***************************************************************************************
     Function: ML_CortexMStepProc
     Engineer: Deepa V A
        Input: None
       Output: TyError - Mid Layer error code
  Description: Single stepping 
Date          Initials      Description
30-Oct-2009      DVA        Initial
****************************************************************************************/
TyError ML_CortexMStepProc(void)
{
    int             ErrRet                  = NO_ERROR;
    unsigned long   ulTempReg0              = 0x0;
    unsigned long   ulReg0Val = 0x0;
	unsigned long	i;
	unsigned long  ulHardBreakPointRes[MAX_NUM_COMPARATORS];
	

	for (i = 0; i < 8; i++)
    {
        ulHardBreakPointRes[i] = FPC_DISABLE;
    }

    ErrRet = DL_OPXD_Arm_SetupHardBreakRes(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,CORTEXM, 8, 8 * sizeof(unsigned long), ulHardBreakPointRes);
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
    {
        return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
    }

    // Restore the registers before execution.
    ErrRet = ML_CortexMWriteRegisters((unsigned long *)CortexMRegArray);
    if (ErrRet != ML_ERROR_NO_ERROR)
    {
        bGlobalStartedExecution = FALSE; 
        return ErrRet;
    }
    ulReg0Val = ulTempReg0;
    ErrRet = DL_OPXD_Arm_StepProc(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,CORTEXM);
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
        return ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
    return ErrRet;
}

/***************************************************************************************
     Function: ML_Armv7AStepProc
     Engineer: Jeenus C.K.
        Input: None
       Output: TyError - Mid Layer error code
  Description: Single stepping of ARMv7-A architecture based processor.
Date          Initials      Description
21-Apr-2010		JCK        Initial
****************************************************************************************/
TyError ML_Armv7AStepProc()
{
	unsigned long     ErrRet = ML_ERROR_NO_ERROR;
    unsigned long     ulDest;
    unsigned long     ulBytes = 4;
    unsigned long     ulFirstAddress;
    unsigned long     ulSecondAddress;     
    unsigned long     ulPC; 
    unsigned long     ulTempReg0 = 0;
    DisaInst          CurrentInst;  
	TyArmProcType  TyProcType = NOCORE;

	if(bGlobalThumb)
        ulBytes = 2;
	
    ulPC = RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC];
	
    ErrRet = ML_ReadFromTargetMemory(ulPC,
		&ulDest,
		&ulBytes,
		RDIAccess_Code);
    if(ErrRet != ML_ERROR_NO_ERROR)
        return ErrRet;
    if(bGlobalThumb)
    {
        //These variables are used to switch back from ARM mode to Thumb Mode.
        ulTempReg0 = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0];		
        //RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;
        ErrRet = Disassemble((unsigned char *)&ulDest,
			&CurrentInst,                       
			ulPC,
			NULL,
			DISA_STEPPING,
			(ProcDisaType)2);
        
		if(ErrRet != NO_ERROR)
			return ErrRet;

		if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC] = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC]+1;
		}
    }
    else
    {         
        ErrRet = Disassemble((unsigned char *)&ulDest,
			&CurrentInst,                       
			ulPC,
			NULL,
			DISA_STEPPING,
			(ProcDisaType)1);
    }
	
    if (ErrRet != NO_ERROR)
        return ErrRet;
	
	
    ulFirstAddress  = ulPC + CurrentInst.InstLen;
    ulSecondAddress = CurrentInst.DestAddr;
	
	if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
	{
		TyProcType = CORTEXA;
	}
	else
	{
		return ML_Error_BadValue;
	}
	
	TyArmv7ABreakpointRes* ptyArmv7ABreakpointRes = tyArmv7ABreakpointResArray.tyArmv7ABreakpointRes;
	unsigned long  ulHardBreakPointRes[MAX_NUM_COMPARATORS * 2];
	
	for(unsigned int i = 0; i < tyArmv7ABreakpointResArray.ulNumBreakpoint; i++)	
	{
		ulHardBreakPointRes[(i * 2) + 1] = BCR_BREAK_DISABLE;
		
		ptyArmv7ABreakpointRes++;
	}

	unsigned long ulControlRegVal = BCR_BREAK_ENABLE | BCR_ANY_ACCESS_CONTROL;

	if (0 == (ulFirstAddress % WORD_ALLIGNED))
	{
		if (bGlobalThumb)
		{
			ulControlRegVal |= BCR_BREAK_AT_BYTE_0;
		} 
		else
		{
			ulControlRegVal |= BCR_BREAK_AT_BYTE_ANY;
		}
		
	}
	else
	{
		ulControlRegVal |= BCR_BREAK_AT_BYTE_2;
	}

	ulHardBreakPointRes[0] =  ulFirstAddress;
	ulHardBreakPointRes[1] =  ulControlRegVal;

	ulControlRegVal = BCR_BREAK_ENABLE | BCR_ANY_ACCESS_CONTROL;

	if (ulFirstAddress != ulSecondAddress)
	{	
		if (0 == (ulSecondAddress % WORD_ALLIGNED))
		{
			if (bGlobalThumb)
			{
				ulControlRegVal |= BCR_BREAK_AT_BYTE_0;
			}	 
			else
			{
				ulControlRegVal |= BCR_BREAK_AT_BYTE_ANY;
			}			
		}
		else
		{
			ulControlRegVal |= BCR_BREAK_AT_BYTE_2;
		}
		

		ulHardBreakPointRes[2] =  ulSecondAddress;
		ulHardBreakPointRes[3] =  ulControlRegVal;				
	} 
	
	else if (bSWIInstruction)
	{
		ulControlRegVal |= BCR_BREAK_AT_BYTE_ANY;			

		ulHardBreakPointRes[2] =  SWI_LOWER_VECTOR_ADDR;
		ulHardBreakPointRes[3] =  ulControlRegVal;

		ulHardBreakPointRes[4] =  SWI_HIGHER_VECTOR_ADDR;
		ulHardBreakPointRes[5] =  ulControlRegVal;

	}
	

	ErrRet = DL_OPXD_Arm_SetupHardBreakRes(tyCurrentDevice, 
											tyTargetConfig.ulDebugCoreNumber, 
											TyProcType, 
											tyArmv7ABreakpointResArray.ulNumBreakpoint, 
											(tyArmv7ABreakpointResArray.ulNumBreakpoint * sizeof(unsigned long) * 2), 
											ulHardBreakPointRes);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
	{
		return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
	}

	ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
	{
		bGlobalStartedExecution = FALSE;  
		return ErrRet;
	}

	ARM11_DataArray[0] = ulTempReg0;
	ErrRet = ML_LeaveDebugState();
	//ErrRet = DL_OPXD_Arm_Leave_Debug_State(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, bGlobalThumb, ARM11_DataArray, sizeof(ARM11_DataArray));
	if (ErrRet != ML_ERROR_NO_ERROR)
                return (ErrRet);

	return ErrRet;
}

/****************************************************************************
	Function: ML_SetCortexM3HardwareBreakPoint
	Engineer: Jeenus C K
	   Input: unsigned long ulAddress - Address at which hardware BP is to be set.
			  boolean bARMBP - True if ARM BP, False if THUMB BP.
			  unsigned long ulResourceNo - Hardware resource number to use. 
	  Output: TyError - Mid Layer error code
 Description: Sets a hardware breakpoint at the given address.
Date          Initials      Description
30-Oct-2009   JCK           Initial
****************************************************************************/
static TyError ML_SetCortexMHardwareBreakPoint (unsigned long ulAddress, 
                                                 boolean       bARMBP, 
                                                 unsigned long ulResourceNo)

{
    unsigned long ulCompatorValue;


    bARMBP = bARMBP;

    if (ulResourceNo > MAX_NUM_COMPARATORS)
    {
        return ML_Error_BadValue;
    }

    /* If address to which the hardware breakpoint is to be set is half word alligned,
    define the behavior when the COMP address matches as breakpoint on upper halfword,
    lower is unaffected. e.g if address is 0x00000242 then COMP field is set as 0x00000240
    and Replace feild as FPC_REPLACE_BKPT_HIGH_HALF_WORD */
    if (HALF_WORD_ALLIGNED == (ulAddress & HALF_WORD_ALLIGNED))
    {
        ulCompatorValue = FPC_REPLACE_BKPT_HIGH_HALF_WORD | 
                          (ulAddress & FPC_COMP)          | 
                          FPC_ENABLE;
    }
    else
    {
        ulCompatorValue = FPC_REPLACE_BKPT_LOW_HALF_WORD | 
                          (ulAddress & FPC_COMP)         | 
                          FPC_ENABLE;
    }

    tyArmv7MBreakpointResArray.ulFlashPatchCompVal[ulResourceNo] = ulCompatorValue;


    return ML_ERROR_NO_ERROR;
}

/****************************************************************************
	Function: ML_SetArmv7AHardwareBreakPoint
	Engineer: Jeenus C K
	   Input: unsigned long ulAddress - Address at which hardware BP is to be set.
			  boolean bARMBP - True if ARM BP, False if THUMB BP.
			  unsigned long ulResourceNo - Hardware resource number to use. 
	  Output: TyError - Mid Layer error code
 Description: Sets a hardware breakpoint at the given address.
Date          Initials      Description
21-Apr-2010   JCK           Initial
****************************************************************************/
static TyError ML_SetArmv7AHardwareBreakPoint(unsigned long ulAddress,
											  boolean bARMBP,
											  unsigned long ulResourceNo)
{
	TyError Erret = ML_ERROR_NO_ERROR;
	unsigned long ulControlRegVal;
	TyArmv7ABreakpointRes* ptyArmv7ABreakpointRes = &tyArmv7ABreakpointResArray.tyArmv7ABreakpointRes[ulResourceNo];

	if (ulResourceNo > MAX_NUM_COMPARATORS)
	{
		return ML_Error_BadValue;
	}

	ptyArmv7ABreakpointRes->ulBreakValueReg = ulAddress & 0xFFFFFFFC;

	ulControlRegVal = BCR_BREAK_ENABLE | BCR_ANY_ACCESS_CONTROL;
	if (0 == (ulAddress % WORD_ALLIGNED))
	{
		if (bARMBP)
		{
			ulControlRegVal |= BCR_BREAK_AT_BYTE_ANY;
		} 
		else
		{
			ulControlRegVal |= BCR_BREAK_AT_BYTE_0;
		}
		
	}
	else
	{
		ulControlRegVal |= BCR_BREAK_AT_BYTE_2;
	}
	
	ptyArmv7ABreakpointRes->ulBreakControlReg = ulControlRegVal;

	return 	Erret;

}

/****************************************************************************
    Function: ML_SetCortexMHardwareWatchPoint
    Engineer: Jeenus Chalattu Kunnath
       Input: unsigned long ulAddress - watch data access to this address.
              unsigned long ulType - watchpoint type
              unsigned long ulDataType - determines which access types to watch for 
              unsigned long ulBound - This is a mask defined by RDI.h
              unsigned long ulResourceNo - Hardware resource number to use. 
      Output: TyError - Mid Layer error code
 Description: Sets a data watchpoint at the given address for Cortex-M3
Date          Initials     Description
01-JCK-2009   JCK          Initial
****************************************************************************/
static TyError ML_SetCortexMHardwareWatchPoint (unsigned long ulAddress, 
                                                 unsigned long ulType, 
                                                 unsigned long ulDataType, 
                                                 unsigned long ulBound, 
                                                 unsigned long ulResourceNo)
{
    TyError        ErrRet           = ML_ERROR_NO_ERROR;
    unsigned long  ulSizeOfData		= 0;
    unsigned long  ulTypeOfAccess	= 0;
    TyCortexMWpResStruct   *ptyCortexMWpResTemp;
	unsigned long  ulMask = DWT_DATAVSIZE_BYTE;

    switch (ulType & LOWER_4BIT_MASK)
    {
    case RDIPoint_EQ:
    case RDIPoint_MASK:
    case RDIDataAndAddressPoint:
    case RDIDataPoint:
        /* There are supported watchpoints types. */
        break;
    default:
        return ML_Error_UnimplementedType;
    }


    /* Check which type of accesses are to be monitored. */
    /* In case of Type of access - Read,Write or read and write - cases are possible. 
    So in all the write cases type of access needs to be ORed. */
    if (RDIWatch_ByteRead  == (ulDataType & RDIWatch_ByteRead))
    {
        ulSizeOfData    = DWT_DATAVSIZE_BYTE;
        ulTypeOfAccess  = DWT_FUNCTION_WP_READ;
        ulAddress       = ulAddress;
		ulMask = BYTE_MASK;
    }
    if (RDIWatch_HalfRead  == (ulDataType & RDIWatch_HalfRead))
    {
        ulSizeOfData    = DWT_DATAVSIZE_HALF_WORD;
        ulTypeOfAccess  = DWT_FUNCTION_WP_READ;
        ulAddress       = ulAddress;
		ulMask = HALF_WORD_MASK;
    }
    if (RDIWatch_WordRead  == (ulDataType & RDIWatch_WordRead))
    {
        ulSizeOfData    = DWT_DATAVSIZE_WORD;
        ulTypeOfAccess  = DWT_FUNCTION_WP_READ;
        ulAddress       = ulAddress;
		ulMask = WORD_MASK;
    }

    if (RDIWatch_ByteWrite == ( ulDataType & RDIWatch_ByteWrite))
    {
        ulSizeOfData    = DWT_DATAVSIZE_BYTE;
        ulTypeOfAccess  |= DWT_FUNCTION_WP_WRITE;
        ulAddress       = ulAddress;
		ulMask = BYTE_MASK;
    }
    if (RDIWatch_HalfWrite == (ulDataType & RDIWatch_HalfWrite))
    {
        ulSizeOfData    = DWT_DATAVSIZE_HALF_WORD;
        ulTypeOfAccess  |= DWT_FUNCTION_WP_WRITE;
        ulAddress       = ulAddress;
		ulMask = HALF_WORD_MASK;
    }
    if (RDIWatch_WordWrite == (ulDataType & RDIWatch_WordWrite))
    {
        ulSizeOfData    = DWT_DATAVSIZE_WORD;
        ulTypeOfAccess  |= DWT_FUNCTION_WP_WRITE;
        ulAddress       = ulAddress;
		ulMask = WORD_MASK;
    }
		
    ptyCortexMWpResTemp = &tyArmv7MWatchpointResArray.tyCortexMWpResStruct[ulResourceNo];

	if ((RDIDataAndAddressPoint == ulType) ||
		(RDIDataPoint == ulType))
	{
		unsigned long ulData = ulBound;

		ptyCortexMWpResTemp->ulComp        = ulAddress;
		ptyCortexMWpResTemp->ulMask        = 0;
		ptyCortexMWpResTemp->ulFunction    = 0;		
		
		/*switch(ulSizeOfData)
		{
		case DWT_DATAVSIZE_BYTE:
			ulBound = ulBound & 0xFF;

			ulData = (ulBound | (ulBound << 8) | (ulBound << 16) | (ulBound << 24));
			break;
		case DWT_DATAVSIZE_HALF_WORD:
			ulBound = ulBound & 0xFFFF;
			
			ulData = (ulBound | (ulBound << 16));
			break;
		case DWT_DATAVSIZE_WORD:
			ulData = ulBound;
			break;
		default:
			return ML_Error_UnimplementedType;
		}*/

		unsigned long ulDataVAddr;

		ulDataVAddr = (ulResourceNo << DATAVADDR0_SHIFT) | (ulResourceNo << DATAVADDR1_SHIFT);
		ptyCortexMWpResTemp = &tyArmv7MWatchpointResArray.tyCortexMWpResStruct[WATCHPOINT_RECOURSE_1];

		ptyCortexMWpResTemp->ulComp        = ulData;
		ptyCortexMWpResTemp->ulMask        = 0;
		ptyCortexMWpResTemp->ulFunction    = ulTypeOfAccess | ulSizeOfData | DWT_DATAVMATCH | ulDataVAddr;
	} 
	else
	{
		ptyCortexMWpResTemp->ulComp        = ulAddress;
		ptyCortexMWpResTemp->ulMask        = ulMask;
		ptyCortexMWpResTemp->ulFunction    = ulTypeOfAccess | ulSizeOfData;
	}
    return ErrRet;
}

/****************************************************************************
    Function: ML_SetArmv7AHardwareWatchPoint
    Engineer: Jeenus Chalattu Kunnath
       Input: unsigned long ulAddress - watch data access to this address.
              unsigned long ulType - watchpoint type
              unsigned long ulDataType - determines which access types to watch for 
              unsigned long ulBound - This is a mask defined by RDI.h
              unsigned long ulResourceNo - Hardware resource number to use. 
      Output: TyError - Mid Layer error code
 Description: Sets a data watchpoint at the given address for ARMv7A architecture
Date          Initials     Description
21-Apr-2010   JCK          Initial
****************************************************************************/
static TyError ML_SetArmv7AHardwareWatchPoint  (unsigned long ulAddress, 
												unsigned long ulType, 
												unsigned long ulDataType, 
												unsigned long ulBound, 
                                                unsigned long ulResourceNo)
{
	TyError        ErrRet            = ML_ERROR_NO_ERROR;
	TyArmv7AWatchpointRes* ptyArmv7AWatchpointRes;
	unsigned long  ulWatchControlVal = 0;
	unsigned long  ulAccessSize = 0;
		
	NOREF(ulBound);

	switch (ulType & LOWER_4BIT_MASK)
    {
    case RDIPoint_EQ:
    case RDIPoint_MASK:
    case RDIDataAndAddressPoint:
    case RDIDataPoint:
        /* There are supported watchpoints types. */
        break;
    default:
        return ML_Error_UnimplementedType;
    }

	ulWatchControlVal = WCR_WATCH_ENABLE | WCR_ANY_PRIVILEGE_ACCESS | WCR_BOTH_SECURE_STATE;

	// Check which type of accesses are to be monitored.
	if ((ulDataType & RDIWatch_ByteRead)  == RDIWatch_ByteRead )
	{
		ulAccessSize		|= WATCH_BYTE_ACCESS;
		ulWatchControlVal	|= WCR_LOAD_ACCESS;
	}
	if ((ulDataType & RDIWatch_HalfRead)  == RDIWatch_HalfRead )
	{
		ulAccessSize		|= WATCH_HALF_WORD_ACCEES;
		ulWatchControlVal	|= WCR_LOAD_ACCESS;
	}
	if ((ulDataType & RDIWatch_WordRead)  == RDIWatch_WordRead )
	{
		ulAccessSize		|= WATCH_WORD_ACCESS;
		ulWatchControlVal	|= WCR_LOAD_ACCESS;
	}
	if ((ulDataType & RDIWatch_ByteWrite) == RDIWatch_ByteWrite)
	{
		ulAccessSize		|= WATCH_BYTE_ACCESS;
		ulWatchControlVal	|= WCR_STORE_ACCESS;
	}
	if ((ulDataType & RDIWatch_HalfWrite) == RDIWatch_HalfWrite)
	{
		ulAccessSize		|= WATCH_HALF_WORD_ACCEES;
		ulWatchControlVal	|= WCR_STORE_ACCESS;
	}
	if ((ulDataType & RDIWatch_WordWrite) == RDIWatch_WordWrite)
	{
        ulAccessSize		|= WATCH_WORD_ACCESS;
		ulWatchControlVal	|= WCR_STORE_ACCESS;
	}

	switch (ulAccessSize)
	{
	case WATCH_BYTE_ACCESS:
		switch (ulAddress & 0x3)
		{
		case 0:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_0;
			break;
		case 1:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_1;
			break;
		case 2:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_2;
			break;
		case 3:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_3;
			break;
		}
		break;
	case WATCH_HALF_WORD_ACCEES:
		switch (ulAddress & 0x3)
		{
		case 0:		
		case 1:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_0;
			break;
		case 2:			
		case 3:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_2;
			break;
		}
		break;
	case WATCH_WORD_ACCESS:
		switch (ulAddress & 0x3)
		{
		case 0:			
		case 1:			
		case 2:			
		case 3:
			ulWatchControlVal |= WCR_WATCH_AT_BYTE_0;
			break;
		}
		break;
	default:
		ulWatchControlVal |= WCR_WATCH_AT_ANY;
		break;
		
	}

	ptyArmv7AWatchpointRes = &tyArmv7AWatchpointResArray.tyArmv7AulWatchPointRes[ulResourceNo];

	ptyArmv7AWatchpointRes->ulWatchPointControlReg = ulWatchControlVal;
	ptyArmv7AWatchpointRes->ulWatchPointValueReg   = ulAddress & 0xFFFFFFFC;

	return ErrRet;

	
}

/****************************************************************************
    Function: ML_ClearCortexM3WPResource
    Engineer: Jeenus C.K.
       Input: WP resource to clear.
      Output: None
 Description: Clears the specified Watchpoint resource
Date          Initials      Description
02-Nov-2009   JCK           Initial
****************************************************************************/
static void ML_ClearCortexMWPResource (unsigned long ulResourceNo,
										unsigned long ulCurrBreakNum)
{
    TyCortexMWpResStruct   *ptyCortexMWPStructTemp;

    if (ulResourceNo > MAX_NUM_WATCHPOINT_COMP)
    {
        return;
    }

    ptyCortexMWPStructTemp     = &tyArmv7MWatchpointResArray.tyCortexMWpResStruct[ulResourceNo];    

	ptyCortexMWPStructTemp->ulFunction = DWT_FUNCTION_DISABLE;
	ptyCortexMWPStructTemp->ulComp		= 0;
	ptyCortexMWPStructTemp->ulMask		= 0;
	pCortexBPArray->ucWatchBPResource[ulResourceNo] = WP_RES_NOT_USED;


	if ((RDIDataAndAddressPoint == pCortexBPArray->BreakpointStructures[ulCurrBreakNum].ulBPType) ||
		(RDIDataPoint == pCortexBPArray->BreakpointStructures[ulCurrBreakNum].ulBPType))
	{
		ptyCortexMWPStructTemp     = &tyArmv7MWatchpointResArray.tyCortexMWpResStruct[WATCHPOINT_RECOURSE_1];

		ptyCortexMWPStructTemp->ulFunction = DWT_FUNCTION_DISABLE;
		ptyCortexMWPStructTemp->ulComp		= 0;
		ptyCortexMWPStructTemp->ulMask		= 0;
		pCortexBPArray->ucWatchBPResource[WATCHPOINT_RECOURSE_1] = WP_RES_NOT_USED;		
	}

}


/****************************************************************************
    Function: ML_ARM_SetupExecuteProcCortexM
    Engineer: Jeenus C.K
       Input: unsigned long ulNumHardBreakPoint - Number of Hardware break point resources
            : TyBPResourceCortexM3Structure* pHardBreakPointRes - Hardware breakpoint data
            : unsigned long ulNumHardWatchPoint - Number of Watch point resources
            : TyCortexM3WpResStruct* pHardWatchPointRes - Watchpoint data
      Output: TyError - Mid Layer error code
 Description: Setup the hardware resources on execution start
Date          Initials      Description
03-Nov-2009   JCK           Initial
****************************************************************************/
TyError ML_ARM_SetupExecuteProcCortexM(
                                       unsigned long ulNumHardBreakPoint,
                                       TyBPResourceCortexMStructure* pHardBreakPointRes,
                                       unsigned long ulNumHardWatchPoint,
                                       TyCortexMWpResStruct* pHardWatchPointRes) 
{
    TyError ErrRet = ML_ERROR_NO_ERROR;
    TyArmProcType  TyProcType = NOCORE;
    unsigned long  ulHardBreakPointRes[MAX_NUM_COMPARATORS];
    unsigned long* pulHardWatchPointRes;
    unsigned long  i;


    if (tyProcessorConfig.bCortexM3)
    {
        TyProcType = CORTEXM;
    }
    else
    {
        return ML_Error_BadValue;
    }

    for (i = 0; i < ulNumHardBreakPoint; i++)
    {
        ulHardBreakPointRes[i] = pHardBreakPointRes->ulFlashPatchCompVal[i];
    }

    ErrRet = DL_OPXD_Arm_SetupHardBreakRes(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulNumHardBreakPoint, ulNumHardBreakPoint * sizeof(unsigned long), ulHardBreakPointRes);
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
    {
        return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
    }

    pulHardWatchPointRes = (unsigned long*)pHardWatchPointRes;

    ErrRet = DL_OPXD_Arm_SetupHardWatchRes(tyCurrentDevice, 
		tyTargetConfig.ulDebugCoreNumber, 
		TyProcType, 
		ulNumHardWatchPoint, 
		(ulNumHardWatchPoint * sizeof(TyCortexMWpResStruct)),
		pulHardWatchPointRes);
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
    {
        ErrRet = ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet);
		return ErrRet;
    }	
		
    return ErrRet;

}

/****************************************************************************
    Function: ML_ARM_SetupExecuteProcCortexA
    Engineer: Jeenus C.K
       Input: unsigned long ulNumHardBreakPoint - Number of Hardware break point resources
            : TyArmv7ABreakpointRes* ptyArmv7ABreakpointRes - Hardware breakpoint data
            : unsigned long ulNumHardWatchPoint - Number of Watch point resources
            : TyArmv7AWatchpointRes* ptyArmv7AWatchpointRes - Watchpoint data
      Output: TyError - Mid Layer error code
 Description: Setup the hardware resources of breakpoint and watchpoint on execution start
Date          Initials      Description
21-Apr-2010   JCK           Initial
****************************************************************************/
TyError ML_ARM_SetupExecuteProcCortexA(unsigned long ulNumBreakPointRes,
										TyArmv7ABreakpointRes* ptyArmv7ABreakpointRes,
										unsigned long ulNumWatchPointRes,
										TyArmv7AWatchpointRes* ptyArmv7AWatchpointRes)
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
    TyArmProcType  TyProcType = NOCORE;
    unsigned long*  pulHardBreakPointRes;
	unsigned long*  pulWatchPointRes;
    unsigned long  i;
	
	if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
    {
        TyProcType = CORTEXA;
    }
    else
    {
        return ML_Error_BadValue;
    }

	pulHardBreakPointRes = (unsigned long*) malloc(ulNumBreakPointRes * 2 * sizeof(unsigned long));
	for(i = 0; i < ulNumBreakPointRes; i++)	
	{
		pulHardBreakPointRes[(i * 2) + 0] = ptyArmv7ABreakpointRes->ulBreakValueReg;
		pulHardBreakPointRes[(i * 2) + 1] = ptyArmv7ABreakpointRes->ulBreakControlReg;

		ptyArmv7ABreakpointRes++;
	}

	ErrRet = DL_OPXD_Arm_SetupHardBreakRes(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulNumBreakPointRes, ulNumBreakPointRes * sizeof(unsigned long) * 2, pulHardBreakPointRes);
	free(pulHardBreakPointRes);
	pulHardBreakPointRes = 0;
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
    {
        return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
    }

	pulWatchPointRes = (unsigned long*) malloc(ulNumWatchPointRes * 2 * sizeof(unsigned long));
	for(i = 0; i < ulNumWatchPointRes; i++)	
	{
		pulWatchPointRes[(i * 2) + 0] = ptyArmv7AWatchpointRes->ulWatchPointValueReg;
		pulWatchPointRes[(i * 2) + 1] = ptyArmv7AWatchpointRes->ulWatchPointControlReg;
		
		ptyArmv7AWatchpointRes++;
	}
	
	ErrRet = DL_OPXD_Arm_SetupHardWatchRes(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType, ulNumWatchPointRes, ulNumWatchPointRes * 2 * sizeof(unsigned long), pulWatchPointRes);
	free(pulWatchPointRes);
	pulWatchPointRes = 0;
    if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
    {
        return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
    }

	return ErrRet;
	
}

/****************************************************************************
    Function: ML_ResetCore
    Engineer: Jeenus C.K
       Input: unsigned long ulResetType
      Output: TyError - Mid Layer error code
 Description: Reset Strrgies of Cortex-M3
Date          Initials      Description
03-Nov-2009   JCK           Initial
****************************************************************************/
TyError ML_ResetCore(unsigned long ulResetType)
{
	TyError		  ErrRet = ML_ERROR_NO_ERROR;
	unsigned long ulData;
	unsigned long ulFirstReadData;
	unsigned long ulSecondReadData;
	if (tyProcessorConfig.bCortexM3)
	{	
		switch(ulResetType)
		{
		case CORE_ONLY:
			/* Enable the vector catch on Reset in order to Halt the core at 
			the reset vector. */
			ulData = VC_CORE_RESET;
			ErrRet = ML_WriteWord(DEMCR_ADDR, ulData);		
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}

			/* Enable the System Reset Bit to reset the Core. */
			ulData = AIRCR_VECTKEY | AIRCR_VECTRESET;
			ErrRet = ML_WriteWord(AIRCR_ADDR, ulData);		
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}
			
			/* Read the DHCSR register and poll the S_RESET_ST bit. 
			S_RESET_ST bit indicates that the core has been reset, or is now being reset, 
			since the last time this bit was read. This a sticky bit that clears on read. 
			So, reading twice and getting 1 then 0 means it was reset in the past. 
			Reading twice and getting 1 both times means that it is being reset now 
			(held in reset still) */
			ErrRet = ML_ReadWord(DHCSR_ADDR, &ulFirstReadData);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}
	
			ErrRet = ML_ReadWord(DHCSR_ADDR, &ulSecondReadData);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}			

			if (((ulFirstReadData & S_RESET_ST) == S_RESET_ST) &&
				((ulSecondReadData & S_RESET_ST) != S_RESET_ST))
			{
				return ML_ERROR_NO_ERROR;
			}
			else if ((S_RESET_ST == (ulFirstReadData & S_RESET_ST)) &&
				 (S_RESET_ST == (ulSecondReadData & S_RESET_ST)))
			{
				return ML_ERROR_NO_ERROR;
			}
			else
			{
				return ML_ERROR_INVALID_CONFIG;
			}
			
			break;
		case CORE_PERIPHERAL:
			/* Enable the vector catch on Reset in order to Halt the core at 
			the reset vector. */
			ulData = VC_CORE_RESET;
			ErrRet = ML_WriteWord(DEMCR_ADDR, ulData);		
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}
			
			/* Enable the System Reset Bit to reset the Core. */
			ulData = AIRCR_VECTKEY | AIRCR_SYSRESETREQ;
			ErrRet = ML_WriteWord(AIRCR_ADDR, ulData);		
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}
			
			ulData = AIRCR_VECTKEY | AIRCR_VECTRESET;
			ErrRet = ML_WriteWord(AIRCR_ADDR, ulData);		
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}

			/* Read the DHCSR register and poll the S_RESET_ST bit. 
			S_RESET_ST bit indicates that the core has been reset, or is now being reset, 
			since the last time this bit was read. This a sticky bit that clears on read. 
			So, reading twice and getting 1 then 0 means it was reset in the past. 
			Reading twice and getting 1 both times means that it is being reset now 
			(held in reset still) */
			ErrRet = ML_ReadWord(DHCSR_ADDR, &ulFirstReadData);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}
			
			ErrRet = ML_ReadWord(DHCSR_ADDR, &ulSecondReadData);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{	
				return ErrRet;					
			}
			
			if (((ulFirstReadData & S_RESET_ST) == S_RESET_ST) &&
				((ulSecondReadData & S_RESET_ST) != S_RESET_ST))
			{
				return ML_ERROR_NO_ERROR;
			}
			
			else if ((S_RESET_ST == (ulFirstReadData & S_RESET_ST)) &&
				(S_RESET_ST == (ulSecondReadData & S_RESET_ST)))
			{
				return ML_ERROR_NO_ERROR;
			}
			
			else
			{
				return ML_ERROR_INVALID_CONFIG;
			}
			break;
		}
	}

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: ML_GetHardwareResInfo
    Engineer: Jeenus C.K
       Input: unsigned long* pulResType - To return the whether same resource 
										- is used for hardware breakpoint and watchpoint
			: unsigned long* pulNumRes  - To return the number of resources.
      Output: TyError - Mid Layer error code
 Description: To return the number of hardware resource for different cores.
Date          Initials      Description
24-May-2010   JCK           Initial
****************************************************************************/
void ML_GetHardwareResInfo(unsigned long* pulResType, 
						   unsigned long* pulNumRes)
{
	if ((tyProcessorConfig.bARM7) ||
		(tyProcessorConfig.bARM9))
	{
		*pulResType = SAME_RES_WP_BP;
		*pulNumRes  = 2;
	} 
	else if (tyProcessorConfig.bARM11)
	{
		*pulResType = DIFF_RES_WP_BP;
		*pulNumRes  = 6;
	} 
	else
	{
		*pulResType = DIFF_RES_WP_BP;

		if (tyProcessorConfig.bCortexM3)
		{	
			*pulNumRes  = tyArmv7MBreakpointResArray.ulNumBreakpoint;
			pulNumRes++;
			*pulNumRes  = tyArmv7MWatchpointResArray.ulNumWatchpoint;
		} 
		else if ((tyProcessorConfig.bCortexA8) || (tyProcessorConfig.bCortexA9))
		{
			*pulNumRes  = tyArmv7ABreakpointResArray.ulNumBreakpoint;
			pulNumRes++;
			*pulNumRes  = tyArmv7AWatchpointResArray.ulNumWatchPoint;
		}
	}
	
	
}

/****************************************************************************
    Function: ML_Config_ICEPICK
    Engineer: Jeenus C.K
       Input: unsigned long ulSubPortNum - Subport number (TAP Number) of the 
										 - ARM Core in the TI Device.  
      Output: TyError - Mid Layer error code
 Description: To configure the ICE-Pick module of the TI Devices.
Date          Initials      Description
24-May-2010   JCK           Initial
****************************************************************************/
TyError ML_Config_ICEPICK( unsigned long ulSubPortNum )
{
	TyError ErrRet = ML_ERROR_NO_ERROR;
	unsigned long ulDatatoScan;
	unsigned long ulDataFromScan;

	ErrRet = ML_ResetTAPUsingTMS ();
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}
	
	ulDatatoScan = 0x7;
	ErrRet = ML_ScanIR (0x6, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

	ulDatatoScan = 0x89;
	ErrRet = ML_ScanDR (8, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}
	ErrRet = ML_ScanDR (8, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

	ulDatatoScan = 0x2;
	ErrRet = ML_ScanIR (0x6, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}
	
	ulDatatoScan = 0xA0002108 | (ulSubPortNum << 24);
	ErrRet = ML_ScanDR (32, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

	ulDatatoScan = 0x20000000 | (ulSubPortNum << 24);
	ErrRet = ML_ScanDR (32, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

	ulDatatoScan = 0x20000000 | (ulSubPortNum << 24);
	ErrRet = ML_ScanDR (32, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

#if 0
	ulDatatoScan = 0xFFFFFFFF;
	ErrRet = ML_ScanIR (32, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

	ulDatatoScan = 0x04E;
	ErrRet = ML_ScanIR (10, &ulDatatoScan, &ulDataFromScan);
	if (ErrRet != ML_ERROR_NO_ERROR)
	{	
		return ErrRet;					
	}

	{
		unsigned long ulTemp[2]; 

		ulDatatoScan = 0;
		ErrRet = ML_ScanDR (64, &ulDatatoScan, ulTemp);
		if (ErrRet != ML_ERROR_NO_ERROR)
		{	
			return ErrRet;					
		}	
	}
#endif
	
	return ErrRet;

}
