/****************************************************************************
       Module: disaaarm.c
     Engineer: Jeenus Chalattu Kunnath
  Description: disassembler for the ARM instruction set
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
06-Jun-2008	   RS		   Added Linux Support
****************************************************************************/
#define  DISA                      // force static definitions etc. in disa.h
           
#include "midlayerarm.h"
#include "DISA.h"
#include  "ARMdefs.h"

extern unsigned long RegArray [MAXMODES] [MAXREGISTERS];
extern boolean bSWIInstruction;
extern unsigned char ucR15;
      
#include <stdio.h>
#include <string.h>
#define NO_ERROR       0x0

typedef unsigned short uint16; 

#define MAKE_UINT(LOW_UBYTE,HIGH_UBYTE)  (((unsigned short)LOW_UBYTE) | (((unsigned short)HIGH_UBYTE)<<8));

//******************
// instruction types
//******************

typedef enum
{
   //V4T instruction set
   SOFTWARE_INTERRUPT               = 0,
   CO_PRO_DATA_OP                   = 1,
   CO_PRO_REG_TRANS                 = 2,
   CO_PRO_DATA_TRANS                = 3,
   BRANCH                           = 4,
   BLOCK_DATA_TRANS                 = 5,
   UNDEFINED                        = 6,
   SINGLE_DATA_TRANS                = 7,
   SINGLE_DATA_SWAP                 = 8,
   MULTIPLY                         = 9,
   MSR_TO_REG                       = 10,
   MSR_TO_FLAG                      = 11,
   MRS                              = 12,
   DATA_PROCESSING                  = 13,
   INVALID                          = 14,
   BRANCHEXCHANGE                   = 15,
   HALFWORD_SIGNED_DATA_TRANSFER_RO = 16,
   HALFWORD_SIGNED_DATA_TRANSFER_IO = 17,
   MUL_LONG_AND_ACCUMUL             = 18,
   MSR_IMM                          = 19,

   //V6TE instruction set
   US_MUL_LONG_AND_ACCUMUL			= 26,
   PARALLEL_ADD_SUB					= 27,
   HALF_WORD_PACK					= 28,
   WORD_SATURATE					= 29,
   PAR_HALF_WORD_SATURATE			= 30,
   REVERSE_INSTRUCTION				= 31,
   SELECT_BYTES						= 32,
   SIGN_ZERO_EXTEND					= 33,
   MULTIPLY_TYPE3					= 34,
   US_SUM_OF_ABS_DIFF				= 35,
   LOAD_STORE_REG_EXCL				= 36,
   CHANGE_PROC_STATE				= 37,
   SET_ENDIAN						= 38,
   SAVE_RETURN_STATE				= 39,
   RET_FROM_EXEPTION				= 40,
   CP_REG_TRANSFER2					= 41

}FormatTypes;              

// temp storage 
static unsigned long CurrentRegValues[UNUSED_REG];
static unsigned long CurrentDisaKind;

// Function Prototypes
static ulArmType GetInstructionFormat(Access32 instruction);
static char *    GetRegisterTransferString (ulArmType reglst);
static char *    GetSingleDataTransferSourceString (ulArmType Rn, ulArmType offset, ulArmType W, ulArmType U, ulArmType P, ulArmType I);

static ConditionPosition   DisassembleSWI (SWI swi);
static ConditionPosition   DisassembleCoProDataTrans (CPDataTrans cpdatatrans);
static ConditionPosition   DisassembleBranch (Branch branch, DisaInst *pDisaType);
static ConditionPosition   DisassembleMsrFlags (MSRToPSRFlags msrtopsrflags);
static ConditionPosition   DisassembleMrsReg (MRSPSRToReg mrspsrtoreg);
static ConditionPosition   DisassembleDataProcessing (DataProc dataproc, DisaInst *pDisaType);
static ConditionPosition   DisassembleUndefinedInst (Access32 instruction);
static ConditionPosition   DisassembleBranchExch (BranchExch branchexch, DisaInst *pDisaType);
static ConditionPosition   DisassembleHWSDTIO (HWaSignedDataTransferIO hwasigneddatatransferio, DisaInst *pDisaType);
static ConditionPosition   DisassembleHWSDTRO (HWaSignedDataTransferRO hwasigneddatatransferro, DisaInst *pDisaType);
static ConditionPosition DisassembleBlockDataTransfer (BlockDataTrans blockdatatrans,
													   DisaInst *pDisaType);


unsigned long EnumerateDataProcessingOperand (ulArmType operand, ulArmType I);
unsigned long EnumerateDataProcessingInstruction(char ucOpCode,unsigned long ulOpReg, unsigned long ulOperand2 );
unsigned long EnumerateSingleDataSourceString (ulArmType Rn, ulArmType offset, ulArmType U, ulArmType P, ulArmType I);
unsigned long CalculateShiftAppliedToRegister (ulArmType Offset);
unsigned long GetPCFromBlockDataTransfer(BlockDataTrans blockdatatrans);

TyError DisassembleARM(unsigned char *pMemory,
                       DisaInst *pDisaInst,
                       unsigned long StartAddr);

#define WORD_MASK_32BIT  0xfffffffc
#define AWALYS  0x0e 

/****************************************************************************
     Function: EnumerateDataProcessingInstruction
     Engineer: Jeenus Chalattu Kunnath
        Input: operand2 , and the immediate operand flag
       Output: unsigned long, The PC after the instruction has executed
  Description: work out the PC from registers
  
      update1:  should use instruction address+8 if the source register is the PC

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
unsigned long EnumerateDataProcessingInstruction(char ucOpCode, 
                                                 unsigned long ulOpReg, 
                                                 unsigned long ulOperand2 )
{
   unsigned long ulRetVal=0;
   unsigned long ulOpRegValue=0;

   if (ulOpReg == 0x0f)
      {
      // if Rn is R15, use instruction address plus 8
      ulOpRegValue = CurrentRegValues[15] + PCPipeBehavour;
      }
   else
      {
      ulOpRegValue = CurrentRegValues[ulOpReg];
      }

   switch(ucOpCode)
      {
      //AND
      case 0x0:
         ulRetVal=ulOpRegValue&ulOperand2;
         break;
      //EOR        EOR
      case 0x01:     // EOR-> (!a & b) | (a & !b)
         ulRetVal=((ulOpRegValue&~ulOperand2)|(~ulOpRegValue&ulOperand2));
         break;
      //SUB
      case 0x02:
         ulRetVal=ulOpRegValue-ulOperand2;
         break;
      //RSB
      case 0x3:
         ulRetVal=ulOperand2-ulOpRegValue;
         break;
      //ADD
      case 0x4:
         ulRetVal=ulOpRegValue+ulOperand2;
         break;
      //ADC
      case 0x5:
         ulRetVal=ulOpRegValue+ulOperand2;
         break;
      //SBC
      case 0x6:
         ulRetVal=ulOpRegValue-ulOperand2;
         break;
      //RSC
      case 0x7:
         ulRetVal=ulOperand2-ulOpRegValue;
         break;
      //TST
      case 0x8:  //dont care about this one doen't produce a result
      //TEQ
      case 0x9:  //dont care about this one doen't produce a result
      //CMP
      case 0xa:  //dont care about this one doen't produce a result
      //CMN
      case 0xb:  //dont care about this one doen't produce a result
         ulRetVal=CurrentRegValues[0xf]+4;
         break;
      //ORR
      case 0xc:
         ulRetVal=ulOpRegValue|ulOperand2;
         break;
      //MOV
      case 0xd:
         //ul OpReg is never used for MOV or MVN instructions 
         ulRetVal=ulOperand2; 
         break;
      //BIC
      case 0xe:
         ulRetVal=ulOpRegValue&~ulOperand2;
         break;
      //MVN
      case 0xf:
         //ul OpReg is never used for MOV or MVN instructions 
         ulRetVal=~ulOperand2; 
         break;
      default:
 //        ASSERT_NOW();
         break;
      }

   return ulRetVal & WORD_MASK_32BIT;
}

/****************************************************************************
     Function: CalculateShiftAppliedToRegister
     Engineer: Jeenus Challattu Kunnath
        Input: ulArmType Offset Offset from current position
       Output: unsigned long 
  Description: register and shift to apply
Date           Initials    Description
29-Apr-2008    JCK         RDI Step Implementation
****************************************************************************/

unsigned long CalculateShiftAppliedToRegister (ulArmType Offset)
{
   unsigned char ucShiftBy=0;
   unsigned long ulRegVal=0,ulRetVal;
   unsigned long ulArithmeticMask=0xffffffff;
   ShiftRm shiftdata;

   shiftdata.operand = Offset;
    
   // Get value in register to be shifted (Rm)
   ulRegVal=CurrentRegValues[shiftdata.shiftreg.Rm];
    
   // shift by the amount (specified in the bottom BYTE of register Rs)
   ucShiftBy=(unsigned char)shiftdata.shiftimmed.shiftconst;

   // Get shift type
   switch (shiftdata.shiftreg.shifttype)
      {
      //logical left
      case 0x00:    
         ulRetVal=ulRegVal<<ucShiftBy;
         break;
      //logical right
      case 0x01:
         ulRetVal=ulRegVal>>ucShiftBy;
         break;
      //arithmetic right
      case 0x02:
         ulRetVal=ulRegVal>>ucShiftBy;
         // if MSB is set
         if((0x80000000&ulRegVal)==0x80000000)
            {
            //work out and then OR in the appropriate mask for the upper bits 
            ulArithmeticMask=ulArithmeticMask<<(32-ucShiftBy);
            ulRetVal=ulRegVal|ulArithmeticMask;
            }
         break;
      //rotate right
      case 0x03:
         while(ucShiftBy)
            {
            //if LSB is a 1
            if (ulRegVal & 1)
               {
               ulRegVal=ulRegVal>>1;
               ulRegVal=ulRegVal|0x80000000;
               }
            else
               ulRegVal=ulRegVal>>1;
       
            ucShiftBy--;
            } //end while
         ulRetVal=ulRegVal;
         break;
      default:
		 ulRetVal = 0x0;         
         break;
      } //end switch
   return (ulRetVal);
}
/****************************************************************************
     Function: EnumerateDataProcessingOperand
     Engineer: Jeenus Chalattu Kunnath
        Input: operand2 , and the immediate operand flag
       Output: unsigned long, value of data processing operand2
  Description: work out how to handle operand2 from i flag 
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
unsigned long EnumerateDataProcessingOperand (ulArmType operand, ulArmType I)
{
   ulArmType data=0, rotate=0,ulRetVal=0;
   ShiftRm temp;
   unsigned char ucShiftBy=0;
   unsigned long ulRegVal;
   unsigned long ulArithmeticMask=0xffffffff;


   if (!I) //shift applied to Rm
      {
      temp.operand = operand;
      if (!temp.shiftimmed.mustbe0)
         {
         /* Immediate */
         if (!temp.shiftimmed.shiftconst)
            {
            //register value only shift applied
            ulRetVal=CurrentRegValues[temp.shiftimmed.Rm] ;
            }
         else //shift register contents by constant value
            {
            //get value in register to be shifted (Rm)
            ulRegVal=CurrentRegValues[temp.shiftreg.Rm];
            
            // shift by the amount (specified in the bottom BYTE of register Rs)
            ucShiftBy=(char)temp.shiftimmed.shiftconst;

            //get shift type
            switch (temp.shiftreg.shifttype)
               {
               //logical left
               case 0x00:
                  ulRetVal=ulRegVal<<ucShiftBy;
                  break;
               
               //logical right
               case 0x01:
                  ulRetVal=ulRegVal>>ucShiftBy;
                  break;
               
               //arithmetic right
               case 0x02:
                  //shift the value
                  ulRetVal=ulRegVal>>ucShiftBy;
                  // if MSB is set
                  if (0x80000000 & ulRegVal)
                     {
                     //work out and then OR in the appropriate mask for the upper bits 
                     ulArithmeticMask=ulArithmeticMask<<(32-ucShiftBy);
                     ulRetVal=ulRegVal|ulArithmeticMask;
                     } //end if
                  break;
               //rotate right
               case 0x03:
                  while(ucShiftBy)
                     {
                     //if LSB is a 1
                     if (ulRegVal & 1)
                        {
                        ulRegVal=ulRegVal>>1;
                        ulRegVal=ulRegVal|0x80000000;
                        }
                     else
                        ulRegVal=ulRegVal>>1;
               
                     ucShiftBy--;
                     } //end while
                  ulRetVal=ulRegVal;
                  break;
               default:
  //                ASSERT_NOW();
                  break;
               } //end switch
            }
         }
      else
         {
         //get value in register to be shifted (Rm)
         ulRegVal=CurrentRegValues[temp.shiftreg.Rm];
            
         // shift by the amount (specified in the bottom BYTE of register Rs)
         ucShiftBy=(char)CurrentRegValues[temp.shiftreg.Rs];

         //get shift type
         switch (temp.shiftreg.shifttype)
            {
            //logical left
            case 0x00:
               ulRetVal=ulRegVal<<ucShiftBy;
               break;

            //logical right
            case 0x01:
               ulRetVal=ulRegVal>>ucShiftBy;
               break;

            //arithmetic right
            case 0x02:
               ulRetVal=ulRegVal>>ucShiftBy;
               // if MSB is set
               if (0x80000000 & ulRegVal)
                  {
                  //shift the value
                  //work out and then OR in the appropriate mask for the upper bits 
                  ulArithmeticMask=ulArithmeticMask<<(32-ucShiftBy);
                  ulRetVal=ulRetVal|ulArithmeticMask;
                  } //end if
               break;
            //rotate right
            case 0x03:
               while(ucShiftBy)
                  {
                  //if LSB is a 1
                  if (ulRegVal & 1)
                     {
                     ulRegVal=ulRegVal>>1;
                     ulRegVal=ulRegVal|0x80000000;
                     }
                  else
                     ulRegVal=ulRegVal>>1;
                  ucShiftBy--;
                  } //end while
               ulRetVal=ulRegVal;
               break;
            default:
    //           ASSERT_NOW();
               break;
            } //end switch
         }  //end else
      } //end I
   else //rotate applied to unsigned 8 bit immediate value
      {
      rotate = (operand >> 8) * 2;
      data = operand & 0xFF;
      ulRetVal=(data >> rotate)|(data<<(32-rotate));
      }
   return (ulRetVal);
}

/****************************************************************************
     Function: DisassembleBranch
     Engineer: Jeenus Chalattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ConditionPosition DisassembleBranch (Branch branch, DisaInst *pDisaType)
{
   int cp=1;
   ulArmType Offset=0;
   unsigned long     InstrAddr;
     
   if (!branch.sign)
      Offset=PCPipeBehavour+(branch.offset<<2);
   else
      Offset=PCPipeBehavour+0x2000000-(branch.offset<<2);
  
   InstrAddr = ((0==branch.sign)?
               PCPipeBehavour+CurrentRegValues[15]+(branch.offset<<2):
               PCPipeBehavour+(CurrentRegValues[15]-(0x2000000-(branch.offset<<2))));
   pDisaType->DestAddr = InstrAddr;
   if (branch.L)
      pDisaType->InstType = FCALL_INST;
   else
      pDisaType->InstType  = FJUMP_INST;
   return (cp);
}

/****************************************************************************
     Function: DisassembleDataProcessing
     Engineer: Jeenus Chalattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ConditionPosition DisassembleDataProcessing (DataProc dataproc, 
                                                    DisaInst *pDisaType)
{
   unsigned long ulOperand=0;
   unsigned char bDontCheckS;


   bDontCheckS = FALSE;
   if (!((dataproc.opcode == 0xD) || (dataproc.opcode == 0xF)))
      {
      if ((dataproc.opcode >= 0x8) && (dataproc.opcode <= 0xB))
         {         
         // These instructions don't use the S field...
         bDontCheckS = TRUE;
         }      
      }

   // if the destination register is PC, and instruction is not of type  "CMP,CMN,TEQ,TST"
   // then we need to calculate the value of the new PC 
   if ((dataproc.Rd==15) && (!((dataproc.opcode >= 0x8) && (dataproc.opcode <= 0xB)) ))
      {
      //set the instruction type
      pDisaType->InstType = RET_INST;

      //enumerate operand 2 
      ulOperand=EnumerateDataProcessingOperand(dataproc.operand2, dataproc.I);
      pDisaType->DestAddr=EnumerateDataProcessingInstruction((unsigned char) dataproc.opcode,(unsigned long)dataproc.Rn,ulOperand );
      }   
   return (3);
}

/****************************************************************************
     Function: DisassembleBranchExch
     Engineer: Jeenus Chalattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ConditionPosition DisassembleBranchExch (BranchExch branchexch, 
                                                DisaInst *pDisaType)
{
   pDisaType->DestAddr = CurrentRegValues[branchexch.Rn];
   
   // Is the LSB set ?
   if ((pDisaType->DestAddr & 0x1))
      pDisaType->ucBXInstType = BX_INST_TO_THUMB_CODE;

   // determine the instruction type
   if (branchexch.Rn==14)
      pDisaType->InstType = RET_INST; // if branch and exchange on the Link register 
                                      // it must be a RET_INST
   else
      pDisaType->InstType = VJUMP_INST;

   return(2);
}

/****************************************************************************
     Function: DisassembleHWSDT  (Halfword and Signed Data Transfer)
     Engineer: Jeenus Chalattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/

static ConditionPosition DisassembleHWSDTIO(HWaSignedDataTransferIO hwasigneddatatransferio, 
                                            DisaInst *pDisaType)
{
   ulArmType ulOffset=0;
   ConditionPosition cp=0;

   // if PC and stack pointer it could be a function return point
   if ((hwasigneddatatransferio.Rn==0x0d)&&(hwasigneddatatransferio.Rd==0xf))
      pDisaType->InstType = RET_INST;

   if(hwasigneddatatransferio.condition<0xe) //there is a condition to be appended                                
      cp=5;
   else
      cp=3;

   ulOffset= ((hwasigneddatatransferio.OffSetHi<<4) | hwasigneddatatransferio.OffSetLo);

   return(3);
}
/****************************************************************************
     Function: DisassembleHWSDTRO (Halfword and Signed Data Transfer Register Offset)
     Engineer: Jeenus Chalattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ConditionPosition DisassembleHWSDTRO(HWaSignedDataTransferRO hwasigneddatatransferro, 
                                            DisaInst *pDisaType)
{
   ConditionPosition cp=0;

   if (hwasigneddatatransferro.condition<0xe) //there is a condition to be appended                               
      cp=5;
   else
      cp=3;

   // if PC and stack pointer it could be a function return point
   if ((hwasigneddatatransferro.Rn==0x0d)&&(hwasigneddatatransferro.Rd==0xf))
      pDisaType->InstType = RET_INST;

   return(3);
}


/****************************************************************************
     Function: GetInstructionFormat
     Engineer: Jeenus Chalattu Kunnath
        Input: 32bit field describing the instruction
       Output: The format type of the 32bit field passed in 
  Description: chesk bit filed for match aginst predefined constant bit patterns

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
ulArmType GetInstructionFormat(Access32 instruction)
{

	//V6T
	if ((0x04 == instruction.usmultiplylongandaccumulatelong.mustbe00000100) && (0x9 == instruction.usmultiplylongandaccumulatelong.mustbe1001))
	{
		// 
		return((ulArmType)US_MUL_LONG_AND_ACCUMUL);
	}
	if ((0x0C == instruction.parralleladdsub.mustbe01100) && (0x01 == instruction.parralleladdsub.mustbe1))
	{
		// 
		return((ulArmType)PARALLEL_ADD_SUB);
	}
	if ((0x68 == instruction.halfwordpack.mustbe01101000) && (0x01 == instruction.halfwordpack.mustbe01))
	{
		// 
		return((ulArmType)HALF_WORD_PACK);
	}
	if ((0x1 == instruction.wordsaturate.mustbe01) && (0x0D == instruction.wordsaturate.mustbe01101) && (0x1 == instruction.wordsaturate.mustbe1))
	{
		// 
		return((ulArmType)WORD_SATURATE);
	}
	if ((0x3 == instruction.parhalfwordsaturate.mustbe0011) && (0xD == instruction.parhalfwordsaturate.mustbe01101) && (0x2 == instruction.parhalfwordsaturate.mustbe10))
	{
		// 
		return((ulArmType)PAR_HALF_WORD_SATURATE);
	}
	if ((0x3 == instruction.reverse.mustbe011) && (0xD == instruction.reverse.mustbe01101) && (0x3 == instruction.reverse.mustbe11))
	{
		// 
		return((ulArmType)REVERSE_INSTRUCTION);
	}
	if ((0x68 == instruction.selectbytes.mustbe01101000) && (0xB == instruction.selectbytes.mustbe1011))
	{
		// 
		return((ulArmType)SELECT_BYTES);
	}
	if ((0xD == instruction.signzeroextend.mustbe01101) && (0x7 == instruction.signzeroextend.mustbe0111))
	{
		// 
		return((ulArmType)SIGN_ZERO_EXTEND);
	}
	if ((0xE == instruction.multiplytype3.mustbe01110) && (0x1 == instruction.multiplytype3.mustbe1))
	{
		// 
		return((ulArmType)MULTIPLY_TYPE3);
	}
	if ((0x1 == instruction.ussumofabsdiff.mustbe0001) && (0x78 == instruction.ussumofabsdiff.mustbe01111000))
	{
		// 
		return((ulArmType)US_SUM_OF_ABS_DIFF);
	}
	if ((0xC == instruction.loadstoreregexclusive.mustbe0001100) && (0x9 == instruction.loadstoreregexclusive.mustbe1001))
	{
		// 
		return((ulArmType)LOAD_STORE_REG_EXCL);
	}
	if ((0x0 == instruction.changeprocstate.mustbe0a) && (0x0 == instruction.changeprocstate.mustbe0b) && (0xF10 == instruction.changeprocstate.mustbe111100010000))
	{
		// 
		return((ulArmType)CHANGE_PROC_STATE);
	}
	if ((0x0 == instruction.setendian.mustbe0000) && (0xF101 == instruction.setendian.mustbe1111000100000001))
	{
		// 
		return((ulArmType)SET_ENDIAN);
	}
	if ((0x0 == instruction.returnfromexeption.mustbe0) &&
		(0x1 == instruction.returnfromexeption.mustbe1) &&
		(0xA == instruction.returnfromexeption.mustbe1010) &&
		(0x7C == instruction.returnfromexeption.mustbe1111100)
		)
	{
		// 
		return((ulArmType)RET_FROM_EXEPTION);
	}
	if ((0x5 == instruction.savereturnstate.mustbe0101) &&
		(0xD == instruction.savereturnstate.mustbe01101) &&
		(0x1 == instruction.savereturnstate.mustbe1) &&
		(0x7C == instruction.savereturnstate.mustbe1111100)
		)
	{
		// 
		return((ulArmType)SAVE_RETURN_STATE);
	}
	
	if (0x62 == instruction.cpregtranstype2.mustbe1100010)
	{
		// 
		return((ulArmType)CP_REG_TRANSFER2);
	}


   //**********************
   //   V4T instructions
   //**********************
   if ((1==instruction.multiplylongandaccumulatelong.mustbe00001) &&
       (0x9==instruction.multiplylongandaccumulatelong.mustbe1001))
      {
      return((ulArmType)MUL_LONG_AND_ACCUMUL);
      }
   if ((1==instruction.hwasigneddatatransferio.B4mustbe1) &&
       (1==instruction.hwasigneddatatransferio.B7mustbe1) &&
       (1==instruction.hwasigneddatatransferio.B22mustbe1)&&
       (0==instruction.hwasigneddatatransferio.mustbe000))
      {
      return((ulArmType)HALFWORD_SIGNED_DATA_TRANSFER_IO);
      }
   if ((0x0 == instruction.multiply.mustbe000000) && (0x9 == instruction.multiply.mustbe1001))
      {
      /* Multiply */
      return((ulArmType)MULTIPLY);
      }
   if ((1==instruction.hwasigneddatatransferro.mustbe1)      &&       
       (1==instruction.hwasigneddatatransferro.mustbe00001)  &&    
       (0==instruction.hwasigneddatatransferro.mustbe000))  
      {
      return((ulArmType)HALFWORD_SIGNED_DATA_TRANSFER_RO);
      }
   if (0x12fff1 == instruction.branchexch.mustbe000100101111111111110001)
      {
      return((ulArmType)BRANCHEXCHANGE);
      }
   if (0xF == instruction.swi.mustbe1111)
      {
      return((ulArmType)SOFTWARE_INTERRUPT);
      }
   if( (0xE == instruction.cpregtrans.mustbe1110)&& (0x1 == instruction.cpregtrans.mustbe1))
      {
      //Coprocessor register transfer
      return((ulArmType)CO_PRO_REG_TRANS);
      }
   if ((0xE == instruction.cpdataop.mustbe1110)&& (0x0== instruction.cpdataop.mustbe0))
      {
      /* Coprocessor Data Operation */
      return((ulArmType)CO_PRO_DATA_OP);
      }
   if (0x6 == instruction.cpdatatrans.mustbe110)
      {
      /* Coprocessor Data Transfer */
      return((ulArmType)CO_PRO_DATA_TRANS);
      }
   if (0x5 == instruction.branch.mustbe101)
      {
      /* Branch */
      return((ulArmType)BRANCH);
      }
   if (0x4 == instruction.blockdatatrans.mustbe100)
      {
      /* Block Data Transfer */
      if (0 == instruction.blockdatatrans.registerlist)
         {
         return((ulArmType)INVALID);
         }
      else
         {
         return((ulArmType)BLOCK_DATA_TRANS);
         }
      }

   if ((0x2 == instruction.singledataswap.mustbe00010) &&
       (0x0 == instruction.singledataswap.mustbe00) &&
       (0x09 == instruction.singledataswap.mustbe00001001))
      {
      /* Single Data Swap */
      if ((15 == instruction.singledataswap.Rd) ||
          (15 == instruction.singledataswap.Rn) ||
          (15 == instruction.singledataswap.Rm))
         {
         return((ulArmType)INVALID);
         }
      else
         {
         return((ulArmType)SINGLE_DATA_SWAP);
         }
      }
   if ((0x2 == instruction.msrregtopsr.mustbe00010) &&
       (0x2 == instruction.msrregtopsr.mustbe10) &&
       (0xF == instruction.msrregtopsr.mustbe1111) &&
       (0x0 == instruction.msrregtopsr.mustbe00000000))
      {
      return((ulArmType)MSR_TO_REG);
      }
   if ((0xF == instruction.msrimmtopsr.mustbe1111) &&
       (0x2 == instruction.msrimmtopsr.mustbe10)   &&
       (0x6 == instruction.msrimmtopsr.mustbe00110))
      {
      return((ulArmType)MSR_IMM);
      }

   if ((0x0 == instruction.msrtopsrflags.mustbe00) &&
       (0x2 == instruction.msrtopsrflags.mustbe10) &&
       (0x28F == instruction.msrtopsrflags.mustbe1010001111))
      {
      /* MSR transfer immed or reg to PSR Flags */
      return((ulArmType)MSR_TO_FLAG);
      }
 
   if ((0x2 == instruction.mrspsrtoreg.mustbe00010) &&
       (0xF == instruction.mrspsrtoreg.mustbe001111) &&
       (0x0 == instruction.mrspsrtoreg.mustbe000000000000))
      {
      /* MSR transfer immed or reg to PSR Flags */
      return((ulArmType)MRS);
      }
   if (0x0 == instruction.dataproc.mustbe00)
      {
      /* Data Processing/PSR Transfer */
      return((ulArmType)DATA_PROCESSING);
      }
   if (0x1 == instruction.singledatatrans.mustbe01)
      {
      /* Single Data Transfer */
      return((ulArmType)SINGLE_DATA_TRANS);
      }

   if ((0x3 == instruction.undefined.mustbe011) && (0x1 == instruction.undefined.mustbe1))
      {
      /* Undefined */
      return((ulArmType)UNDEFINED);
      }
   else
      {
      /* Unknown */
      return((ulArmType)UNDEFINED);
      }
}
/****************************************************************************
     Function: Disassemble
     Engineer: Jeenus Chalattu Kunnath
        Input: pMemory   *: pointer to memory to disassemble
               pDisaInst *: pointer to storage for found instruction
               StartAddr  : instructions start address
               pReg      *: current register values (used for calculating insturctions
                            destination address and symbol)
               ulDisaKind : bit-map defines the kind of dissassembly required
       Output: TyError: NO_ERROR or error number
  Description: Disassembles and returns instruction at pMemory.
               Sets all members of pDisaInst.
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
TyError Disassemble(unsigned char *pMemory,
                    DisaInst *pDisaInst,
                    unsigned long StartAddr,
                    unsigned long *pulReg, 
                    unsigned long ulDisaKind,
                    ProcDisaType ProcDisa)
{
   TyError ErrRet                     = NO_ERROR;
   boolean bThumbMode                 = FALSE;   
   int i = 0;

   // Remember the type of disassemble asked for...
   CurrentDisaKind = ulDisaKind;

   if (ProcDisa == Disa_ARM_Arm)
      {
      bThumbMode = FALSE;
      }
   else //if (ProcDisa == Disa_ARM_Thumb)
      {
      bThumbMode = TRUE;
      }

   if(pulReg!=NULL)
      {
      //assign the global Register array the reg values passed
      (void) memcpy( (void *)&CurrentRegValues[0], (const void *)pulReg, (size_t) UNUSED_REG *4 );
      }    

   // The PC must match the start address of the instruction...
   unsigned long ulCurrentMode;
   unsigned long ulModeIndex;
   ulCurrentMode = RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_CPSR] & REG_ARRAY_MODE_MASK;

   for(i = 0; i <= 7 ;i++)
   {
       CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
   }
   
   switch(ulCurrentMode)
   {
   case USR_MODE:
   case SYS_MODE:
	   for(i = 8; i<=14 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
	   }
		
	   break;
   case FIQ_MODE:
	   for(i = 8; i<=14 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_FIQ_MODE] [i];
	   }
	   break;
   case IRQ_MODE:
	   for(i = 8; i<=12 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
	   }
	   
	   CurrentRegValues[13] = RegArray [REG_ARRAY_IRQ_MODE] [13];
	   CurrentRegValues[14] = RegArray [REG_ARRAY_IRQ_MODE] [14];
	   break;
   case SVC_MODE:
	   for(i = 8; i<=12 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
	   }

	   CurrentRegValues[13] = RegArray [REG_ARRAY_SVC_MODE] [13];
	   CurrentRegValues[14] = RegArray [REG_ARRAY_SVC_MODE] [14];	   
	   
	   break;
   case ABORT_MODE:
	   for(i = 8; i<=12 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
	   }

	   CurrentRegValues[13] = RegArray [REG_ARRAY_ABORT_MODE] [13];
	   CurrentRegValues[14] = RegArray [REG_ARRAY_ABORT_MODE] [14];
	   break;
   case UNDEF_MODE:
	   for(i = 8; i<=12 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
	   }
	   
	   CurrentRegValues[13] = RegArray [REG_ARRAY_UNDEF_MODE] [13];
	   CurrentRegValues[14] = RegArray [REG_ARRAY_UNDEF_MODE] [14];
	   break;	   
   default:
	   for(i = 8; i<=14 ;i++)
	   {
		   CurrentRegValues[i] = RegArray [REG_ARRAY_USR_MODE] [i];
	   }
	   ulModeIndex = REG_ARRAY_USR_MODE;
	   break;

   }   
   
   CurrentRegValues[15] = StartAddr;


   // decide which way we will disassemble
   if (bThumbMode)
      {
      RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_R0] = (RegArray [REG_ARRAY_USR_MODE][REG_ARRAY_PC])+1;

		RegArray [REG_ARRAY_USR_MODE] [REG_ARRAY_PC] = StartAddr;
      //THUMB MODE  function in disathumb.c
      ErrRet=DisassembleTHUMB(pMemory,
                              pDisaInst,
                              StartAddr,
                              &CurrentRegValues[0]);                               
      }
   else
      {
      //ARM MODE
      ErrRet=DisassembleARM(pMemory,
                            pDisaInst,
                            StartAddr);
      }
  

   //ensure dest address is halfword aligned
   pDisaInst->DestAddr&=0xfffffffe;
   return NO_ERROR;  
}

/****************************************************************************
Function: DisassembleBlockDataTransfer
Engineer: Jeenus Chalattu Kunnath
Input	: structure defining the format type 
Output	: position in string of condition code
Description: fill out a string describing the disassembled instruction
Date           Initials    Description
19-Mar-2010    JCK.        Initial
****************************************************************************/
static ConditionPosition DisassembleBlockDataTransfer (BlockDataTrans blockdatatrans,
	DisaInst *pDisaType)
{ 
	ulArmType storetype;
	
	// get address mode in table
	storetype = blockdatatrans.L * 4 + blockdatatrans.P *2 + blockdatatrans.U;
	// is it a stack or non-stack addressing mode?
	if (blockdatatrans.Rn == 13)
		storetype +=8;	
	
	//strncpy(disaarmstr+3+((0x0E != blockdatatrans.condition)?2:0), pszStackAddressingType[storetype], 2);
	// if loading from the stack to the PC then could be an exit point
	if ( (blockdatatrans.L) && (blockdatatrans.registerlist&0x8000) )
	{
		pDisaType->InstType = RET_INST;
		pDisaType->DestAddr=GetPCFromBlockDataTransfer(blockdatatrans);
	}
	
	return (3);
}

/****************************************************************************
Function: GetPCFromBlockDataTransfer(BlockDataTrans blockdatatrans);
Engineer: Jeenus Chalattu Kunnath
Input	: structure defining the format type 
Output	: value of the PC
Description: read the PC from the stack
Date           Initials    Description
19-Mar-2010    JCK.        Initial
****************************************************************************/
unsigned long GetPCFromBlockDataTransfer(BlockDataTrans blockdatatrans)
{ 
	unsigned long ErrRet;
	ulArmType storeagetype;
	unsigned long ulBuffer;
	unsigned long ulRegMask;
	unsigned long ulLoopCount;
	unsigned long ulBitCount;
	unsigned long ulRegVal;
	unsigned long ulBytes;
	
	ulBuffer=0;
	// get addressing mode (in table 4.6 Arm7TDMI Data Sheet) 
	/*
	Tables 5-1 & 5.2 ARM Architecture Reference Manual DDI 0100E
	Non-stack addressing mode  Stack Addressing mode    L-bit P-Bit U-Bit
	STMDA (Decrement After)    STMED (Empty Descending)   0     0     0
	STMIA (Increment After)    STMEA (Empty Ascending)    0     0     1
	STMDB (Decrement Before)   STMFD (Full Descending)    0     1     0
	STMIB (Increment Before)   STMFA (Full Ascending)     0     1     1
	
	  Non-stack addressing mode  Stack Addressing mode    L-bit P-Bit U-Bit
	  LDMDA (Decrement After)    LDMFA (Full Ascending)     1     0     0
	  LDMIA (Increment After)    LDMFD (Full Descending)    1     0     1
	  LDMDB (Decrement Before)   LDMEA (Empty Ascending)    1     1     0
	  LDMIB (Increment Before)   LDMED (Empty Descending)   1     1     1
	  
	*/
	storeagetype = blockdatatrans.L * 4 +
		blockdatatrans.P * 2 +
		blockdatatrans.U;
	// get address of SP / base register
	ulRegVal=CurrentRegValues[blockdatatrans.Rn];
	
	// see how many bits are set in the register list mask
	// in order to obtain the offset from the SP base address
	ulRegMask=1;
	ulBitCount=0;
	for(ulLoopCount=0;ulLoopCount<=15;ulLoopCount++)
	{
		if (ulRegMask&(unsigned long)blockdatatrans.registerlist)
			ulBitCount++;
		ulRegMask=ulRegMask<<1;
	}
	
	// Note that the PC (r15) will be restored from the highest address
	// in the range of addresses formed by the LDM instruction
	ulBytes = 0x4;
	switch (storeagetype)
	{
		// Increment After
	case 5:		
		// Rn points to first value to be restored
		ErrRet = ML_ReadFromTargetMemory((ulRegVal+(4*ulBitCount-4)),
			&ulBuffer,
			&ulBytes,
			RDIAccess_Data);
		break;
		
		// Decrement After
	case 4:
		// Rn points to last value to be restored PC (r15)
		ErrRet = ML_ReadFromTargetMemory(ulRegVal,
			&ulBuffer,
			&ulBytes,
			RDIAccess_Data);
		break;
		
		// Increment Before
	case 7:
		// Rn+4 points to first value to be restored
		ErrRet = ML_ReadFromTargetMemory((ulRegVal+(4*ulBitCount)),
			&ulBuffer,
			&ulBytes,
			RDIAccess_Data);
		break;
		
		// Decrement Before
	case 6:
		// Rn-4 points to last value to be restored PC (r15)
		ErrRet = ML_ReadFromTargetMemory((ulRegVal-4),
			&ulBuffer,
			&ulBytes,
			RDIAccess_Data);

		break;
		
	default:
		break;
	}
	return(ulBuffer);
}


/****************************************************************************
     Function: DisassembleARM
     Engineer: Jeenus Chalattu Kunnath
        Input: pMemory   *: pointer to memory to disassemble
               pDisaInst *: pointer to storage for found instruction
               StartAddr  : instructions start address              
       Output: TyError: NO_ERROR or error number
  Description: Disassembles and returns instruction at pMemory.
               Sets all members of pDisaInst.
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
TyError DisassembleARM(unsigned char *pMemory,
                       DisaInst *pDisaInst,
                       unsigned long StartAddr)
{
   ConditionPosition  condpos;
   Access32           instruction;
   FormatTypes        InstructionFormat;
	unsigned long      ulBytes = 4;
	unsigned long      pulDest[4];
	unsigned long		 ulAddr;
	int                ErrRet = 0;
	unsigned long ulOffset;

   // Do some initialisation...
   pDisaInst->InstType     = OTHER_INST;
   pDisaInst->InstLen      = 0x04;
   pDisaInst->StartAddr    = StartAddr;
   pDisaInst->DestAddr     = StartAddr+4;
   pDisaInst->bConditional = FALSE;
   pDisaInst->ucBXInstType = NOT_A_BX_INST;

   //put the instruction into the instruction union 
   instruction.ularmType = *(unsigned long *)pMemory;
      
   //Get the instruction Format
   InstructionFormat = (FormatTypes)GetInstructionFormat(instruction);

   // Position in disaarmstr for condition - set to 0 to inhibit insertion of condition 

   condpos = 0;     

   // Disassemble the instruction
   switch(InstructionFormat)
      {
        case MUL_LONG_AND_ACCUMUL:
        case CO_PRO_DATA_OP:
        case CO_PRO_REG_TRANS:  
        case CO_PRO_DATA_TRANS: 
        case UNDEFINED: 
        case SINGLE_DATA_SWAP:
        case MULTIPLY: 
        case MSR_TO_REG:
        case MSR_IMM:
        case MSR_TO_FLAG: 
        case MRS:         
		case US_MUL_LONG_AND_ACCUMUL:
		case PARALLEL_ADD_SUB:
		case HALF_WORD_PACK:
		case WORD_SATURATE:
		case PAR_HALF_WORD_SATURATE:
		case REVERSE_INSTRUCTION:
		case SELECT_BYTES:
		case SIGN_ZERO_EXTEND:
		case MULTIPLY_TYPE3:
		case US_SUM_OF_ABS_DIFF:
		case LOAD_STORE_REG_EXCL:
		case CHANGE_PROC_STATE:
		case SET_ENDIAN:
		case SAVE_RETURN_STATE:
		case CP_REG_TRANSFER2:
            break;
		  case SINGLE_DATA_TRANS:				
				if (!(instruction.singledatatrans.I)) // Immediate Offset
					{
					//Post indexed
					if (!(instruction.singledatatrans.P))   //add offset after transfer
						{
						if (instruction.singledatatrans.Rn == 15)
							{
							// if retrieving the PC value we need to use an offset of 8 due to pipelining
							StartAddr = StartAddr + 8;
							ErrRet = ML_ReadFromTargetMemory(StartAddr,
															 pulDest,
															 &ulBytes,
															 RDIAccess_Data);
							
							ulAddr = *pulDest;
							pDisaInst->DestAddr = ulAddr;
							ucR15 = 1;
							}
						}
					//Pre indexed
					else  //add offset before transfer
						{
						if (!(instruction.singledatatrans.U))
							{
							if (15== instruction.singledatatrans.Rn)
								{
								StartAddr = StartAddr - instruction.singledatatrans.offset + 8;
								ErrRet = ML_ReadFromTargetMemory(StartAddr,
																pulDest,
																&ulBytes,
																RDIAccess_Data);
								
								ulAddr = *pulDest; 
								//pDisaInst->DestAddr = StartAddr - (ulAddr & 0xFFF);
								pDisaInst->DestAddr = ulAddr;
									
								ucR15 = 1;
								}
							}
						else
							{
							if (15 == instruction.singledatatrans.Rd)
								{								
								StartAddr = StartAddr + instruction.singledatatrans.offset + 8;
								ErrRet = ML_ReadFromTargetMemory(StartAddr,
																 pulDest,
																 &ulBytes,
																 RDIAccess_Data);

								ulAddr = *pulDest;																
								//pDisaInst->DestAddr = StartAddr + (ulAddr&0xFFF);								
								pDisaInst->DestAddr = *pulDest;
								ucR15 = 1;
								}            
							}
						}
					}// end Immediate Offset
				else //Offset is a shift applied to a register
					{
					ulOffset=CalculateShiftAppliedToRegister(instruction.singledatatrans.offset);
					//Post indexed
					if (!(instruction.singledatatrans.P))   //add offset after transfer
						{
						if (!(instruction.singledatatrans.U))
							{
							if (15==instruction.singledatatrans.Rn)
								{							
								StartAddr = StartAddr - ulOffset + 8;
								ErrRet = ML_ReadFromTargetMemory(StartAddr,
																pulDest,
																&ulBytes,
																RDIAccess_Data); 
								pDisaInst->DestAddr = *pulDest - ulOffset + 8;
								ucR15 = 1;
								}							
							}
						else
							{
							if (15==instruction.singledatatrans.Rn)
								{							
								StartAddr = StartAddr + ulOffset + 8;
								ErrRet = ML_ReadFromTargetMemory(StartAddr,
									pulDest,
									&ulBytes,
																RDIAccess_Data);
								pDisaInst->DestAddr = *pulDest; 
								ucR15 = 1;
								}
							}
						}
					//Pre indexed
					else  //add offset before transfer
						{
						if(0 == (instruction.singledatatrans.U))
							{
							if(15==instruction.singledatatrans.Rn)
								{								
								StartAddr = StartAddr - ulOffset + 8;
								ErrRet = ML_ReadFromTargetMemory(StartAddr,
									pulDest,
									&ulBytes,
																RDIAccess_Data);
								pDisaInst->DestAddr = *pulDest;
								ucR15 = 1;
								}
							}
						else
							{
							if(15==instruction.singledatatrans.Rd)
								{								
								StartAddr = StartAddr + ulOffset + 8;
								ErrRet = ML_ReadFromTargetMemory(StartAddr,
																pulDest,
																&ulBytes,
																RDIAccess_Data);
								pDisaInst->DestAddr = *pulDest;
								ucR15 = 1;
								}
							
							}
						}
					}

            break;
        case HALFWORD_SIGNED_DATA_TRANSFER_IO:
            condpos = DisassembleHWSDTIO (instruction.hwasigneddatatransferio,pDisaInst);
            if (instruction.hwasigneddatatransferio.condition != AWALYS)
                pDisaInst->bConditional = TRUE;
            break;
        case HALFWORD_SIGNED_DATA_TRANSFER_RO:
            condpos = DisassembleHWSDTRO (instruction.hwasigneddatatransferro,pDisaInst);
            if (instruction.hwasigneddatatransferro.condition != AWALYS)
                pDisaInst->bConditional = TRUE;
            break;       
        case SOFTWARE_INTERRUPT:
            bSWIInstruction = 1;         
            break;     
        case BRANCH:          
            condpos = DisassembleBranch (instruction.branch,pDisaInst);
            if (instruction.branch.condition != AWALYS)
                 pDisaInst->bConditional = TRUE;
            break;    
        case DATA_PROCESSING:    
            condpos = DisassembleDataProcessing (instruction.dataproc,pDisaInst);
            if (instruction.dataproc.condition != AWALYS)
                pDisaInst->bConditional = TRUE;
            break;
        case BRANCHEXCHANGE:           
            condpos = DisassembleBranchExch (instruction.branchexch,pDisaInst);
            if (instruction.branchexch.condition != AWALYS)
                pDisaInst->bConditional = TRUE;
            break;
		case BLOCK_DATA_TRANS:  
			condpos = DisassembleBlockDataTransfer (instruction.blockdatatrans, pDisaInst);
			if (instruction.blockdatatrans.condition != AWALYS)
				pDisaInst->bConditional = TRUE;
		break;
        default:
            break;
      }
 
   if (CurrentDisaKind == DISA_TRACE)
      return NO_ERROR;  
   
   return NO_ERROR;
}

