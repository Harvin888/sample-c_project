/****************************************************************************
      Module: Semihost.cpp
    Engineer: Suraj S
 Description: Semihosting Support
Date          Initials     Description
16-Aug-2007     SJ         Initial
06-Jun-2008		RS		  Added Linux Support
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#ifdef _WINDOWS
#include <stdio.h>
#include <windows.h>
#include <io.h>
//#include <io.h>
#elif __LINUX
#include "stdio.h"
#include <unistd.h>
#include <sys/stat.h>
#endif
#include <errno.h>


#include "common.h"
#include "midlayerarm.h"
#include "../rdi/rdi.h"
#include "../rdi/rdi_hif.h"
#include "semihost.h"
#ifdef __LINUX
#define NO_ERROR 0L 
#endif

//File Handles
typedef struct
{
  FILE *tySystemFileHandle;
  short sSemiHostFileHandle;
  unsigned long ulFileMode;
  boolean bFileOpen;
} TyFileHandleStructure;

static unsigned long ulFileIndex  = 0;
static TyFileHandleStructure FileHandleArray[MAX_FILES_OPEN]    ={0};
static int  iLastErrorNo = 0x0;
static RDI_Hif_HostosArgStr       *SemiHostingHandle;
static RDI_HostosInterface const  *pSemiHostingHostosInterface = NULL;

/****************************************************************************
    Function: SH_SYSOpen
    Engineer: Suraj S
       Input: unsigned char *pucFileName - File name
              unsigned long ulFileMode - File read/write mode
      Output: short - File Handle or Error Status
 Description: Opens the file on the system.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_SYSOpen (unsigned char *pucFileName, unsigned long ulFileMode)
{
   char  cFileMode[0x4] ={0};
   boolean bValidFileMode = TRUE;
   boolean bHandleAssigned = FALSE;
   short sNewHandle = 0x0;
   unsigned short i = 0x0;
   const char *pszFileModeStrings[] = {"ra", "rb", "r+", "rb+", "wa", "wb", "w+", "wb+","a", "ab", "a+", "ab+"};
   

#define NUM_ENTRIES sizeof(pszFileModeStrings) / sizeof(char *)


   if ((strcmp ((char *)pucFileName, ":tt")) == 0)
      {
      // This is a call to the console window not a file.
      FileHandleArray[ulFileIndex].ulFileMode = CONSOLE_WINDOW;
      ulFileIndex ++;
      }
   else
      {
      if (ulFileMode < NUM_ENTRIES)
         strcpy(cFileMode, pszFileModeStrings[ulFileMode]);
      else
         bValidFileMode = FALSE;

      if (bValidFileMode)
         {
         // Open file  
         FileHandleArray[ulFileIndex].tySystemFileHandle = fopen((const char *)pucFileName, (const char *)&cFileMode[0] );
         iLastErrorNo = errno;
         if (FileHandleArray[ulFileIndex].tySystemFileHandle == NULL)
            return SH_ERROR;
         else
            {
            FileHandleArray[ulFileIndex].ulFileMode = ulFileMode;
            FileHandleArray[ulFileIndex].bFileOpen  = TRUE;
            ulFileIndex ++;
            }
         }
      else
         // The file mode was not valid - return error.
         return SH_ERROR;
      }                                                           

   // We need assign a handle that is  not already in use...
   while (!bHandleAssigned)   
      {   
      for (i=0; i<ulFileIndex; i++)
         {
         if (FileHandleArray[i].sSemiHostFileHandle == sNewHandle)
            {
            sNewHandle ++;
            break;
            }
         }
      if (i > (ulFileIndex - 1))
         bHandleAssigned = TRUE;
      }

   FileHandleArray[ulFileIndex-1].sSemiHostFileHandle = sNewHandle;      

   return FileHandleArray[ulFileIndex-1].sSemiHostFileHandle;
}

/****************************************************************************
    Function: SH_SYSClose
    Engineer: Suraj S
       Input: unsigned long ulFileHandle - Handle of the file to close 
      Output: short - Error Status
 Description: Closes the file on the system. If handle is NULL return error
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_SYSClose (unsigned long ulFileHandle)
{
   unsigned long i=0;
   int iError;
   boolean bHandleFound = FALSE;

   //Work out which file to close.
   while ((!bHandleFound) && (i <ulFileIndex)) 
      {
      if ((unsigned long) FileHandleArray[i].sSemiHostFileHandle == ulFileHandle)
         bHandleFound = TRUE;
      else
         i++;
      }

   if (bHandleFound) 
      {
      if (FileHandleArray[i].ulFileMode == CONSOLE_WINDOW)
         {
         iError = SH_NO_ERROR;
         goto SH_SYSCLose_Return;
         }
      else
         {
         if (FileHandleArray[i].tySystemFileHandle == NULL)
            {
            //The file handle is NULL - return error to target.
            iLastErrorNo = ENOENT;
            iError = SH_ERROR;
            goto SH_SYSCLose_Return;
            }
         else
            {
            // Close the file.
            iError = fclose(FileHandleArray[i].tySystemFileHandle);
            iLastErrorNo = errno;
            goto SH_SYSCLose_Return;
            }
         }
      }
   else
      {
      iError = SH_ERROR;
      goto SH_SYSCLose_Return;
      }


SH_SYSCLose_Return:

   if (iError == SH_NO_ERROR)
      {
      for (; i < (ulFileIndex -1); i++)
         {
         FileHandleArray[i].tySystemFileHandle  = FileHandleArray[i+1].tySystemFileHandle;
         FileHandleArray[i].sSemiHostFileHandle = FileHandleArray[i+1].sSemiHostFileHandle;
         FileHandleArray[i].ulFileMode          = FileHandleArray[i+1].ulFileMode;
         FileHandleArray[i].bFileOpen           = FileHandleArray[i+1].bFileOpen;
         }

      //Blank the top of the array
      FileHandleArray[i].tySystemFileHandle  = 0;
      FileHandleArray[i].sSemiHostFileHandle = 0;
      FileHandleArray[i].ulFileMode          = 0x0;
      FileHandleArray[i].bFileOpen           = FALSE;

      ulFileIndex --;

      iLastErrorNo = NO_ERROR;
      return SH_NO_ERROR;
      }
   else
      {
      iLastErrorNo = ENOENT;
      return SH_ERROR;
      }
}

/****************************************************************************
    Function: SH_SYSWrite
    Engineer: Suraj S
       Input: unsigned long ulFileHandle - Handle of the File to be written
              unsigned char *pucData - pointer to write data
              unsigned long ulNumberOfBytes - No. of bytes to write
      Output: short - Error Status
 Description: Writes data to file.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_SYSWrite (unsigned long ulFileHandle, 
                   unsigned char *pucData, 
                   unsigned long ulNumberOfBytes)
{
   unsigned long i=0;
   unsigned char ucCharWritten;
   unsigned long ulCharNo=0;

   for (i=0;i < ulFileIndex;i++)
      {
      if (((unsigned long)FileHandleArray[i].sSemiHostFileHandle == ulFileHandle) && 
          (FileHandleArray[i].sSemiHostFileHandle != 0) &&
          (FileHandleArray[i].tySystemFileHandle != 0))
         {
         for (ulCharNo = 0; ulCharNo < ulNumberOfBytes; ulCharNo++)
            {
            ucCharWritten = (unsigned char) fputc(pucData[ulCharNo],FileHandleArray[i].tySystemFileHandle);
            iLastErrorNo = errno;
            if (ucCharWritten != pucData[ulCharNo])
               return SH_ERROR;
            }
          
         return SH_NO_ERROR;
         }
      }

   iLastErrorNo = ENOENT;
   return SH_ERROR;
}

/****************************************************************************
     Function: SH_SYSRead
     Engineer: Suraj S
        Input: unsigned long  ulFileHandle - SemiHosting file handle
               char *pcData - pointer to read data
               unsigned long  ulNumberOfBytes - No. of bytes to read
               unsigned long  ulFileMode - File access mode
               boolean bUseGNU_SemiHosting - TRUE if GNU Semihosting is used
       Output: Number of bytes read or Error Status.
  Description: Reads data from a file.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_SYSRead (unsigned long  ulFileHandle, 
                          char          *pcData, 
                          unsigned long  ulNumberOfBytes, 
                          unsigned long  ulFileMode,
                          boolean        bUseGNU_SemiHosting)
{
   unsigned long i=0;
   unsigned long ulCharNo=0;
   int iChar;

   /*unreferenced formal parameter*/
   NOREF(ulFileMode);

   //Find the file to read from
   for (i=0;i < ulFileIndex;i++)
      {
      if (((unsigned long)FileHandleArray[i].sSemiHostFileHandle == ulFileHandle) && 
          (FileHandleArray[i].sSemiHostFileHandle != 0) &&          
          (FileHandleArray[i].tySystemFileHandle != 0))
         {
         // Read the data from the file.
         for (ulCharNo=0;ulCharNo<ulNumberOfBytes;ulCharNo++)
            {
            iChar = fgetc(FileHandleArray[i].tySystemFileHandle);
            pcData[ulCharNo] = (unsigned char)(iChar & 0xFF);
            iLastErrorNo = errno;
            if (iChar == EOF) 
               {
               if (ulCharNo == 0x0)
                  return ulNumberOfBytes;
               else
                  {
                  if (bUseGNU_SemiHosting)
                     return (ulNumberOfBytes - ulCharNo);
                  else
                     return (ulNumberOfBytes + ulCharNo);
                  }
               }
            }
         return SH_NO_ERROR;
         }
      }

   iLastErrorNo = ENOENT;
   return ulNumberOfBytes;
}

/****************************************************************************
    Function: SH_SYSSeek
    Engineer: Suraj S
       Input: unsigned long ulFileHandle - File handle 
              unsigned long ulFilePosition - Position to set file to
      Output: int - Error status
 Description: Seeks a specified position in the file.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
int SH_SYSSeek (unsigned long ulFileHandle, unsigned long ulFilePosition)
{
   unsigned long i=0;
   int iError = SH_NO_ERROR;
   fpos_t  pos;
   fpos_t *filePosition;

#ifdef __LINUX
   pos.__pos = (_G_off_t)ulFilePosition;
#else
   pos = (fpos_t)ulFilePosition;
#endif
   
   filePosition = &pos;
   for (i=0;i < ulFileIndex;i++)
      {
      if ((unsigned long)FileHandleArray[i].sSemiHostFileHandle == ulFileHandle)
         {
         // Set the file position.
         if (FileHandleArray[i].tySystemFileHandle != NULL)
            {
            iError = fsetpos(FileHandleArray[i].tySystemFileHandle, filePosition);
            iLastErrorNo = errno;
            if (iError !=0)
               return SH_ERROR;
            else
               return SH_NO_ERROR;
            }
         else
            {
            iLastErrorNo = ENOENT;
            iError = SH_ERROR;
            return iError;
            }
         }
      }

   iLastErrorNo = ENOENT;
   return iError;
}

/****************************************************************************
     Function: SH_SYSFlen
     Engineer: Suraj S
        Input: unsigned long ulFileHandle - File Handle
       Output: int - File Length or Error Status
  Description: Returns the length of the given file
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
int SH_SYSFlen (unsigned long ulFileHandle)
{
   unsigned long i=0;
   int iFileLength =0;

   for (i=0;i < ulFileIndex;i++)
      {
      if (((unsigned long)FileHandleArray[i].sSemiHostFileHandle == ulFileHandle) && 
          (FileHandleArray[i].sSemiHostFileHandle != 0) &&          
          (FileHandleArray[i].tySystemFileHandle != 0))
         {
         // Get the file length
#ifdef __LINUX
struct stat buf;
		iLastErrorNo =	fstat(fileno(FileHandleArray[i].tySystemFileHandle), &buf);
		iFileLength = (unsigned long)buf.st_size;
#else
         iFileLength = _filelength(_fileno (FileHandleArray[i].tySystemFileHandle));
		 iLastErrorNo = errno;
#endif

         
         return iFileLength;
         }
      }

   iLastErrorNo = ENOENT;
   return SH_ERROR;
}

/****************************************************************************
     Function: SH_SYSRemove
     Engineer: Suraj
        Input: unsigned char *pucFileName - Name of the file to be removed
       Output: short - Error Status
  Description: Deletes a specified file.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_SYSRemove (unsigned char *pucFileName)
{
   int iError = 0;

   if (pucFileName != NULL)
      {
      iError = remove((const char *)pucFileName);
      iLastErrorNo = errno;
      
      if (iError != 0)
         return SH_ERROR;
      else
         return SH_NO_ERROR;
      }
   else
      return SH_ERROR;
}

/****************************************************************************
     Function: SH_SYSRename
     Engineer: Suraj S
        Input: unsigned char *pucFileName - Current name of the file
               unsigned char *pucNewFileName - New file name to be given
       Output: Error Status
  Description: Renames a specified file.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_SYSRename (unsigned char *pucFileName, unsigned char *pucNewFileName)
{
   int iError =0;

   iError = rename((const char *) pucFileName, (const char *) pucNewFileName);
   iLastErrorNo = errno;
   if (iError != 0)
      return SH_ERROR;
   else
      return SH_NO_ERROR;
}

/****************************************************************************
     Function: SH_CheckForFile
     Engineer: Suraj S
        Input: unsigned long ulFileHandle - File Handle
       Output: short - File Handle type or Error status 
  Description: Checks if the file handle is for a file or the 
               console window.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_CheckForFile (unsigned long ulFileHandle)
{
   unsigned long i=0;

   //Search through the FileHandleArray for a match.
   for (i=0;i != ulFileIndex;i++)
      {
      if ((FileHandleArray[i].ulFileMode == CONSOLE_WINDOW) && 
          ((unsigned long)FileHandleArray[i].sSemiHostFileHandle == ulFileHandle))
         {
         // This is a console session.
         return SH_INTERACTIVE;
         }
      if (((unsigned long)FileHandleArray[i].sSemiHostFileHandle == ulFileHandle) && 
          (FileHandleArray[i].sSemiHostFileHandle != 0))
         {
         // This is a file.
         return SH_NOTINTERACTIVE;
         }
      }
   return SH_ISTTYERROR;
}                       

/****************************************************************************
    Function: SH_SetSemiHostingFunctionCalls
    Engineer: Suraj S
       Input: Pointer to rdi callback functions for semihosting.
      Output: NONE.
 Description:  Sets up the function pointers to semihosting interface.
   Date        Initials    Description
16-Oct-2007      SJ         Initial
****************************************************************************/
void SH_SetSemiHostingFunctionCalls (RDI_HostosInterface const  *pHostosInterface)
{
   pSemiHostingHostosInterface = pHostosInterface;
   SemiHostingHandle = pSemiHostingHostosInterface -> hostosarg;
}

/****************************************************************************
    Function: SH_SYSConsoleWrite
    Engineer: Suraj S
       Input: unsigned char *pucData - pointer to write data 
              unsigned long ulNumberOfBytes - number of bytes to write
      Output: Error status
 Description: Writes data to Console window.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_SYSConsoleWrite (unsigned char *pucData, unsigned long ulNumberOfBytes)
{
   unsigned char *pucLocalData;

   pucLocalData = pucData;

   //Check whether the function write has been exported.
   if (pSemiHostingHostosInterface -> write == NULL)
      {
      return RDIError_SoftwareInterrupt; 
      }
   
   //write the data to the console window.
   pSemiHostingHostosInterface -> write (SemiHostingHandle, 
                                         (const char *)pucData, 
                                         ulNumberOfBytes);
   return SH_NO_ERROR;
}

/****************************************************************************
     Function: SH_SYSConsoleWriteC
     Engineer: Suraj S
        Input: int Char - Character to be written
       Output: unsigned long - Error status
  Description: Writes a single character to the Console window.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_SYSConsoleWriteC (int Char)
{
   //Check whether the function write has been exported.
   if (pSemiHostingHostosInterface -> writec == NULL)
      {
      return RDIError_SoftwareInterrupt; 
      }
   
   //write the data to the console window.
   pSemiHostingHostosInterface -> writec (SemiHostingHandle, Char);
   return SH_NO_ERROR;
}

/****************************************************************************
     Function: SH_ReadCharFromConsole
     Engineer: Suraj S
        Input: unsigned long *ulBuffer - Character buffer
       Output: unsigned long - Error status
  Description: Reads a single character from the console window.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_ReadCharFromConsole (unsigned long *ulBuffer)
{
   //Check whether the function gets has been exported.
   if (pSemiHostingHostosInterface -> readc == NULL)
      {
      return RDIError_SoftwareInterrupt; 
      }
   
   //read the data from the console window.
   *ulBuffer = (unsigned long) pSemiHostingHostosInterface -> readc (SemiHostingHandle);
   return NO_ERROR;
}

/****************************************************************************
     Function: SH_ReadStringFromConsole
     Engineer: Suraj S
        Input: char *ucBuffer - pointer to read data
               unsigned long ulNumberOfCharsFromHost - number of bytes to read
       Output: unsigned long - Data size or Error status
  Description: Reads a string of given length from the console window.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_ReadStringFromConsole (char *ucBuffer, 
                                        unsigned long ulNumberOfCharsFromHost)

{
   unsigned long ulSizeOfData =0;

   //Check whether the function gets has been exported.
   if (pSemiHostingHostosInterface -> gets == NULL)
      {
      return RDIError_SoftwareInterrupt; 
      }
   
   //read the data from the console window.
   pSemiHostingHostosInterface -> gets (SemiHostingHandle, ucBuffer, ulNumberOfCharsFromHost);
   
   while (ucBuffer[ulSizeOfData] != 0)
      {
       ulSizeOfData ++;
      }

   return ulSizeOfData;
}

/****************************************************************************
     Function: SH_CreateTempFile
     Engineer: Suraj S
        Input: char *szTmpFileName - Pointer to buffer for file name
       Output: short - Error Status
  Description: Returns a temporary file name that does not already exist.
               Open files are closed.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
short SH_CreateTempFile(char *szTmpFileName)
{
   char *pzTmpFileName = &szTmpFileName[0];
   FILE *TmpFileHandle = NULL;
   int i = 0;

   for (i = 0; i < MAX_TMP_FILES; i ++)
      {
      //Copy the file name into the buffer.
      pzTmpFileName = &szTmpFileName[0];
      sprintf (pzTmpFileName, "$ASH%.3u.TMP", i);

      //Open the file. 
      TmpFileHandle = fopen(pzTmpFileName, "r" );

      //If it does not exist the name is OK.      
      if (TmpFileHandle == NULL)
         break;
      else
         fclose(TmpFileHandle);
      }

   if ((i == MAX_TMP_FILES) && TmpFileHandle != NULL)
      return SH_ERROR;

   return SH_NO_ERROR;
}

/****************************************************************************
     Function: SH_Reset_SemiHosting
     Engineer: Suraj S
        Input: NONE
       Output: NONE
  Description: Called when a new image is downloaded to the target. All 
               open files are closed.
Date          Initials     Description
17-Aug-2007     SJ         Initial
****************************************************************************/
void SH_Reset_SemiHosting(void)
{
   int i=0;
   int iError=0;

   for (i = 0;i < MAX_FILES_OPEN; i++)
      {
      //Close all open files.
      if (FileHandleArray[i].tySystemFileHandle != NULL)
         {
         iError = fclose(FileHandleArray[i].tySystemFileHandle);
         }

      FileHandleArray[i].tySystemFileHandle  = NULL;
      FileHandleArray[i].sSemiHostFileHandle = 0;
      FileHandleArray[i].ulFileMode          = 0x0;
      FileHandleArray[i].bFileOpen           = FALSE;
      }

   ulFileIndex = 0;
}

/****************************************************************************
     Function: SH_GetTime
     Engineer: Suraj S
        Input: unsigned long *ulTime - Pointer to return value.
       Output: Error status
  Description: Gets the time in seconds since 00:00:00 on Jan 1st 1970
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_GetTime (unsigned long *ulTime)
{
    time_t ltime;
    
    // Get the time from the system.
    time( &ltime );

    *ulTime = (unsigned long) ltime;
    return SH_NO_ERROR;
}

/****************************************************************************
     Function: SH_SYSError
     Engineer: Suraj S
        Input: NONE
       Output: unsigned long - Error status of last system access.
  Description: Returns the error status of the last system access
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
unsigned long SH_SYSError (void)
{
   return iLastErrorNo;
}

