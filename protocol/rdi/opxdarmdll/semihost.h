/****************************************************************************
       Module: SEMIHOST.H
     Engineer: Suraj S
  Description: Header File for Semi Hosting support
Date           Initials    Description
16-Aug-2007      SJ       Initial
****************************************************************************/
#ifndef SEMIHOST_H_
#define SEMIHOST_H_

#define SH_ERROR                       -1
#define SH_NO_ERROR                     0
#define SH_INTERACTIVE                  1
#define SH_NOTINTERACTIVE               0
#define SH_ISTTYERROR                   2

//Semi-hosting and DCC types
#define NO_SEMIHOSTING                 0x0
#define STANDARD_SEMIHOSTING           0x1
#define DCC_SEMIHOSTING                0x2
#define DCC_STANDARD                   0x3
#define DCC_REALMON                    0x4

// This instruction is placed at the semihost vector for DCC based semihoting.
#define SH_JUMP_INSTRUCTION            0xE59FF018

//Semi_hosting instructions - these are defined by ARM....

#define SH_SYS_OPEN                    0x01
#define SH_SYS_CLOSE                   0x02
#define SH_SYS_WRITEC                  0x03
#define SH_SYS_WRITE0                  0x04
#define SH_SYS_WRITE                   0x05
#define SH_SYS_READ                    0x06
#define SH_SYS_READC                   0x07
#define SH_SYS_ISERROR                 0x08
#define SH_SYS_ISTTY                   0x09
#define SH_SYS_SEEK                    0x0A
#define SH_SYS_FLEN                    0x0C
#define SH_SYS_TMPNAM                  0x0D
#define SH_SYS_REMOVE                  0x0E
#define SH_SYS_RENAME                  0x0F
#define SH_SYS_CLOCK                   0x10
#define SH_SYS_TIME                    0x11
#define SH_SYS_SYSTEM                  0x12
#define SH_SYS_ERRNO                   0x13
#define SH_SYS_GET_CMDLINE             0x15
#define SH_SYS_HEAPINFO                0x16
#define SH_ANGEL_SWIREASON_ENTERSVC    0x17
#define SH_ANGEL_REASONEXCEPTION       0x18
#define SH_ANGEL_LATE_STARTUP          0x20
#define SH_SYS_ELAPSED                 0x30
#define SH_SYS_TICKFREQ                0x31

//This is used in the File Mode to indicate a console window session.
#define CONSOLE_WINDOW                 0xFF

#define MAX_FILES_OPEN        0x01FF

#define MAX_TMP_FILES         0x03E7
#define DOS_FILENAME_LENGTH   0x000D

//CPU register masks.
#define REG_R0_MASK                    0x00001
#define REG_R13_MASK                   0x02000
#define REG_PC_MASK                    0x10000
#define REG_CPSR_MASK                  0x20000
#define USR_MODE                       0x10
#define SVC_MODE                       0x13

//These definitions are taken from the angel source headers.
//They are used for Semihosting calls.
#define ADP_Stopped_BranchThroughZero        0x20000
#define ADP_Stopped_UndefinedInstr           0x20001
#define ADP_Stopped_SoftwareInterrupt        0x20002
#define ADP_Stopped_PrefetchAbort            0x20003
#define ADP_Stopped_DataAbort                0x20004
#define ADP_Stopped_AddressException         0x20005
#define ADP_Stopped_IRQ                      0x20006
#define ADP_Stopped_FIQ                      0x20007
#define ADP_Stopped_BreakPoint               0x20020
#define ADP_Stopped_WatchPoint               0x20021
#define ADP_Stopped_StepComplete             0x20022
#define ADP_Stopped_InternalError            0x20024
#define ADP_Stopped_Userinterruption         0x20025
#define ADP_Stopped_ApplicationExit          0x20026

/*
//This stucture holds the data passed between the Execute function and 
//the handle Semihosting function.
typedef struct
   {
   unsigned long     SemiHostingType; 
   unsigned long     arg1;
   unsigned long     arg2; 
   unsigned long     arg3; 
   unsigned long     arg4;
   unsigned long     ulStatus; 
   boolean           bRestartExecution; 
   TXRXCauseOfBreak  tyCauseOfBreak;
   boolean           bSemiHostDetected;
   unsigned long     ulLinkRegisterValue;
   unsigned long     ulSPSRValue;
   } TYSemiHostStruct;
*/

/* Function prototypes*/
short SH_SYSOpen (unsigned char *pucFileName, unsigned long ulFileMode);
short SH_SYSClose (unsigned long ulFileHandle);
short SH_SYSRemove (unsigned char *pucFileName);
short SH_SYSWrite (unsigned long ulFileHandle, 
                   unsigned char *pucData, 
                   unsigned long ulNumberOfBytes);
unsigned long SH_SYSRead (unsigned long  ulFileHandle, 
                          char          *pcData, 
                          unsigned long  ulNumberOfBytes, 
                          unsigned long  ulFileMode,
                          boolean        bUseGNU_SemiHosting);
int SH_SYSSeek (unsigned long ulFileHandle, unsigned long ulFilePosition);
int SH_SYSFlen (unsigned long ulFileHandle);
short SH_SYSRename (unsigned char *pucFileName, unsigned char *pucNewFileName);
short SH_CheckForFile (unsigned long ulFileHandle);
unsigned long SH_SYSConsoleWrite (unsigned char *pucData, unsigned long ulNumberOfBytes);
unsigned long SH_SYSConsoleWriteC (int Char);
unsigned long SH_ReadStringFromConsole (char *ucBuffer, 
                                        unsigned long ulNumberOfCharsFromHost);
unsigned long SH_ReadCharFromConsole (unsigned long *ulBuffer);
short SH_CreateTempFile(char *szTmpFileName);
void SH_Reset_SemiHosting(void);
unsigned long SH_GetTime (unsigned long *ulTime);
unsigned long SH_SYSError (void);
void SH_SetSemiHostingFunctionCalls (RDI_HostosInterface const  *pHostosInterface);

#endif //SEMIHOST_H_
