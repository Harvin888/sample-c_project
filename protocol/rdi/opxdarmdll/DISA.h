/****************************************************************************
       Module: disaarm.h
     Engineer: Jeenus Challattu Kunnath
  Description: Header file for ARM Disassembler
Date           Initials    Description
14-Mar-2008     JCK        Initial
****************************************************************************/
#define DISA
#include "disaarm.h"
#include "disathumb.h"


#define  NO_INSTR_BYTES       4
#define  MAX_INSTR_BYTES      4
#define MAX_INSTR_STR_SIZE    256         // max byte size of a instruction string
#define UNUSED_REG            0x11          // Must also be updated in txrxcmd.h 

#define  NOT_A_BX_INST           0x0
#define  BX_INST_TO_ARM_CODE     0x1
#define  BX_INST_TO_THUMB_CODE   0x2

#define  USER         0x10
#define  SYSTEM       0x1f
#define  FIQ          0x11
#define  IRQ          0x12
#define  SUPERVISOR   0x13
#define  ABORT        0x17
#define  UNDEF        0x1b
#define  MONITOR	  0x16	/* ARM monitor mode */  
#define  CURRENT      0xff
#define  TUSER         0x30
#define  TSYSTEM       0x3f
#define  TFIQ          0x31
#define  TIRQ          0x32
#define  TSUPERVISOR   0x33
#define  TABORT        0x37
#define  TUNDEF        0x3b
#define  TMONITOR	   0x36	/* THUMB monitor mode */  
#define MAXMODES                          0x8
#define MAXREGISTERS                      0x13


#define  NO_INSTR_BYTES       4
#define  MAX_INSTR_BYTES      4
#define MAX_INSTR_STR_SIZE    256         // max byte size of a instruction string
#define UNUSED_REG            0x11          // Must also be updated in txrxcmd.h 

#define  NOT_A_BX_INST           0x0
#define  BX_INST_TO_ARM_CODE     0x1
#define  BX_INST_TO_THUMB_CODE   0x2

// Enum constants that define the kind of dissassembly required

#define     DEFAULT_COMMON_BANK        (unsigned short)0xffff

#define     USE_VALID_BANK             (unsigned short)0xfffe

#define     FORCE_BANK_UPDATE          (unsigned short)0xfffd

#define     MAX_BANK_NUMBER            (unsigned short)0xff

#define     MAX_XDATA_BANK_NUMBER      (unsigned short)0x03

typedef unsigned long ulong; 
typedef unsigned long adtyp; 
typedef unsigned short TyBankNo;
typedef unsigned char ubyte;

// Enum constants that define the kind of dissassembly required

typedef enum
{
   Disa_For_PCP_Proc,
   Disa_For_TriCore_Proc_RiderA,
   Disa_For_TriCore_Proc_RiderB,
   // ARM Disassembly types (Instruction Sets)
   Disa_ARM_Unknown           = 0x00000000,
   Disa_ARM_Arm               = 0x00000001,
   Disa_ARM_Thumb             = 0x00000002
} ProcDisaType; 

typedef struct
   {
   bool  bHasJumpDelaySlot;   // If MIPS16 instruction has a branch delay slot.
   bool  bBranchLikelyInst;   // MIPS - In the unlikely event that a branch likely
                              // event does not branch, the instruction in the 
                              // BDS is nullified. Important for trace disassembly
   bool  bMemoryReadInst;     // Not used yet
   bool  bMemoryWriteInst;    // Not used yet
   //**unsigned char unMemAccessSize;     // Not used yet
   } TyExtraDisaInfo;

typedef enum
   {
   DISA_FULL,    
   DISA_TRACE,   
   DISA_TRACE_NOSYM,   
   DISA_RETFUNC ,
   DISA_STEPPING,
   DISA_WINFILL
   } TyDisaKind;


typedef enum { FJUMP_INST ,           // Fixed Jump      e.g. J 0x1000, Branch -50
               VJUMP_INST ,          // Variable Jump  e.g. J R0
               FCALL_INST ,          // Fixed Call     e.g. JAL 0x1000, Call 0x900
               VCALL_INST ,          // Variable Call  e.g. JAL R0, Call R2
               RET_INST   ,          // Return         e.g. J RA, Ret
               CONST_INST ,          // Constant Data Region e.g. ".const 0xA0000100"
               OTHER_INST            // Anything which isn't one of the above
             } TyInstType;


typedef enum 
   {
      DISA_16,          // Generic 16 Bit Disassembly
      DISA_32           // Generic 32 Bit Disassembly
   } TyDisassemblyType;   // Type of disassembly performed 

typedef struct 
            {
            unsigned short    StartBank;                    // instruction's bank 
            unsigned long     StartAddr;                    // instruction's address 
            unsigned char     InstLen;                      // instruction's length  
            unsigned char     Value[MAX_INSTR_BYTES];       // memory values 
            char              szString[MAX_INSTR_STR_SIZE]; // string descriptor 
            TyInstType        InstType;                     // instruction type
            unsigned short    DestBank;                     // instruction's destination bank
            unsigned long     DestAddr;                     // instruction's destination address
            unsigned long     Reg[UNUSED_REG];                          // current register values when instruction disassembled
            unsigned long     ulDisaKind;                   // kind of dissassembly that was done (e.g. No symbols)
            bool              bConditional;
            unsigned char     ucBXInstType;
            unsigned long     GPRSUsed;                     // The Gpr's used within this instruction.
            TyDisassemblyType tyDisassemblyType;            // Type of disassembly performed (16 or 32 bit)
            TyExtraDisaInfo   tyExtraDisaInfo;              // Any additional useful Information
            } DisaInst;

typedef enum  { AsmRoutine, PLMRoutine, CRoutine, 
                LibRoutine, NullRoutine, ASLDBRoutine }     LanguageTypes ;
typedef enum  { ModuleR, ProcedureR, CodeBlockR, DeletedR } RoutineTypes ;

typedef enum
{
   START_BP       = 0x00,
   OPCODE_BP      = 0x00,
   DATA_RD_BP     = 0x01,
   DATA_WR_BP     = 0x02,
   PAGE_BP        = 0x03,
   XDATA_BP       = 0x04,
   ON_CHIP_RAM_BP = 0x05,
   RAM_BR_RANGE_BP= 0x06,
   ALL_BP         = 0x07,
   SW_BP          = 0x08,
   COCO_PSEN      = 0x09,
   COCO_OPCODE    = 0x0A,
   HW_BP          = 0x0B,
   NO_BP          = 0x0C,
   DATA_BP        = 0x0D,          // Simply a data break on either Read or Write
   TEMP_OPCODE_BP = 0x0E,
   TEMP_HW_BP     = 0x0F,
   OPCODE_16BIT   = 0x10,  // ARM 16 bit breakpoint - NB this 0x10 value matches that RDIPoint_16Bit in rdi.h!
   OPCODE_32BIT   = 0x11,
   WATCH_BYTEREAD = 0x12,
   WATCH_HALFREAD = 0x13,
   WATCH_WORDREAD = 0x14,
   WATCH_BYTEWRITE= 0x15,
   WATCH_HALFWRITE= 0x17,
   WATCH_WORDWRITE= 0x18,
   ARM_DATA_ADDR_WP = 0x18, // ARM Data Watchpoint on a specific address
   ARM_DATA_WP      = 0x19, // ARM Data Watchpoint on any address
   WP_BP          = 0x1A,   // PowerPC watchpoints.
   END_BP         = 0x1B    // Make sure END_BP is last
                            // Note that ValidBPTypeForEmulator() which uses
                            // VALID_BPS (defined in txrx????.h) supports
                            // only 32 BP types!!
} TXRXBPTypes;

// Address Range Attributes
typedef enum{
            UnDefinedRange     = 0x00,
            CodeRange32Bit     = 0x01,
            CodeRange16Bit     = 0x02,
            ConstDataRange     = 0x03
            } TyAddrRangeAttribute;

// Generic Disassembly modes 
// Warning: The order of these types is critical (in assembly mode popup menu)
typedef enum
{
   DISA_SYMBOLIC_MODE,        // Disassemble based on Symbolic Information
   DISA_AUTO_MODE,            // Disassemble based on mode of processor when it halted
   DISA_FIRST_MODE,           // Disassemble always in First Mode 
   DISA_SECOND_MODE,          // Disassemble always in Second Mode
   DISA_UNKNOWN_MODE          // Not sure why this is an option?
}TyCurrentDisassemblyMode;

TyError DisassembleTHUMB(unsigned char  *pMemory,
					DisaInst *pDisaInst,
					unsigned long StartAddr,
					unsigned long *pulReg);	
					


TyError Disassemble(unsigned char *pMemory,DisaInst *pDisaInst,unsigned long StartAddr,unsigned long *pulReg, unsigned long ulDisaKind,ProcDisaType ProcDisa);



			  
//processor modes
