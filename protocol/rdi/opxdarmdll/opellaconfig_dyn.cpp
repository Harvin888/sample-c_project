/******************************************************************************
       Module: opellaconfig_dyn.cpp
     Engineer: Suraj S
  Description: This fle is implemented to compromise on the dependency between 
			   opxdrdi.dll and OpellaXDARMConfig.dll
  Date           Initials    Description
  11-Aug-2010    SJ          Initial
******************************************************************************/

#include <stdafx.h>
#include <windows.h>
#include "dll/opxdarm.h"
#include "OpellaARMConfigInterface.h"
#include "opellainterface.h"

// DLL name
#define OPXDCONFIG_DLL_NAME "OpellaXDARMConfig.dll"

typedef void (* InitOpellaARMConfigGUI_t) (const char *pszARMRDIDriverpath);
typedef int (* ShowOpellaConfigDialog_t)(void);
typedef void (*InitialiseRDIFnPtr_t)(OpxdrdiFnptr *pOpxdrdiFnptr);

// local pointers to functions
static InitOpellaARMConfigGUI_t _InitOpellaARMConfigGUI = NULL;
static ShowOpellaConfigDialog_t _ShowOpellaConfigDialog = NULL;
static InitialiseRDIFnPtr_t _InitialiseRDIFnPtr = NULL;

/****************************************************************************
     Function: InitOpellaARMConfigGUI
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Get the function pointers required by opxdrdi.dll
               and pass the function pointers required by OpellaXDARMConfig.dll
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
int COpxdarmApp::InitOpellaARMConfigGUI(const char *pszARMRDIDriverpath)
{
	OpxdrdiFnptr *pfnptrs;
	
	m_dllHandle = LoadLibrary(OPXDCONFIG_DLL_NAME);

	if(!m_dllHandle)
	  return IDERR_LOAD_ARM_DLL_OPELLAXD;

	_InitOpellaARMConfigGUI = (InitOpellaARMConfigGUI_t)GetProcAddress(m_dllHandle, "InitOpellaARMConfigGUI");
	_ShowOpellaConfigDialog = (ShowOpellaConfigDialog_t)GetProcAddress(m_dllHandle, "ShowOpellaConfigDialog");
	_InitialiseRDIFnPtr		= (InitialiseRDIFnPtr_t)GetProcAddress(m_dllHandle, "InitialiseRDIFnPtr");

	if(_InitOpellaARMConfigGUI)
	   _InitOpellaARMConfigGUI(pszARMRDIDriverpath);

	pfnptrs = (OpxdrdiFnptr *)malloc(sizeof(OpxdrdiFnptr));
	if(pfnptrs)
	{
		GetRDIFnPtrs(pfnptrs);

		if(_InitialiseRDIFnPtr)
			_InitialiseRDIFnPtr(pfnptrs);

		free(pfnptrs);
	}

	return NO_ERROR;
}


/****************************************************************************
Function	: ShowOpellaConfigDialog
Engineer	: Suraj S
Input		: none
Output		: int return TRUE if success else return FALSE
Description	: Show the RDI configuration dialog
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
int ShowOpellaConfigDialog(void)
{
	if(_ShowOpellaConfigDialog)
		return _ShowOpellaConfigDialog();
	else
		return FALSE;
}
