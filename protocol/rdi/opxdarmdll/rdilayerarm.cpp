/****************************************************************************
Module: rdilayerarm.cpp
Engineer: Suraj S
Description: Implementation of ARM RDI Driver Interface for Opella-XD
(as required by ARM RDI Specification)
Date           Initials    Description
13-Jul-2007    SJ          Initial
08-Jan-2008    SJ          Added RDI Diagnostics Support
18-Feb-2008    SJ          Added RDI logging feature
27-Feb-2008    SJ          Added single stepping support
19-Mar-2008    JR          Added info call for performance improvement(GetLoadSize)
24-Mar-2008	   SJ		   Added 968 core support
31-Mar-2008	   VK		   Added RDI JTAG console Support for ARM GDB Server
31-Mar-2008	   RS		   Added info calls required for ARM GDB Server
08-Apr-2008    RS          Modified for Multicore support for ARM GDB Server  
11-Apr-2008    SJ          Moved diagnostic functions to a seperate file
26-May-2008    JCK		   Updated for user register settings
06-Jun-2008	   RS		   Added Linux Support
02-Nov-2009    JCK         Added support for Cortex-M3
05-May-2010    JCK         Added processor selection support. 
12-Aug-2010	   SJ		   Modified to avoid memory leaks.
****************************************************************************/
#ifndef __LINUX
// Win specific defines
#include "stdafx.h"
//#include "windows.h"
#include "usb.h"
#include "OpellaARMConfigInterface.h"
#include "../opxdarmdll/dll/Progress.h"
#include "../opxdarmdll/dll/Resource.h"
#include "config.h"
#include "opellainterface.h"
#else
// Linux specific defines
#include "../rdi/GUIDefs.h"
#include "../rdi/toolconf.h"
#include "../rdi/rdi.h"

#include "stdio.h"
#include <string.h>
#include <unistd.h>

#define _T(x) x
#define _MAX_PATH   260
typedef char TCHAR, *PTCHAR;
#define NO_ERROR 0L 
#endif
#ifndef __LINUX
#include <conio.h>
#endif

#include "rdilayerarm.h"
#include "common.h"
#include "semihost.h"
#include "midlayerarm.h"
#include "realmon.h"
#include "caches.h"
#include "../../../protocol/common/drvopxd.h"
#include "../../../protocol/common/drvopxdarm.h"
#include "../../../protocol/rdi/opxdarmdll/ARMdefs.h"
#include "../../../firmware/export/api_err.h"
#include "../rdi/rdi_hif.h"
#include "../opxdarmconfigmgr/OpellaConfig.h"

#ifdef _WINDOWS
CProgressControl    *pOpenUSBProgress;
#endif

#define TSTSC_LENGTH          512
#define TSTSC_ULONGS          (TSTSC_LENGTH / 32)

#define AGENT_HANDLE_VALUE    0xBBBBBBBB
#define TARGET_MODULE_HANDLE  ARM_CORE1
#define TARGET_MODULE_HANDLE1 ARM_CORE2
#define DEBUGGER_NAME_SIZE    16
#define MAX_PATH_SIZE         0xFF
#define BYPASS_PATTERN        0xFFFFFFFF

char szReturnValue [45];
boolean  bGDBServer  =FALSE;   
//For File logging
#ifndef __LINUX
bool bLogEnableOK;
char szFullPathandNameForLogFile[_MAX_PATH];

typedef struct 
{
	char func_name[50];
	char arg_name[3][50]; 
	char arg_value[3][50];   
	char return_value[50];
	char errormessage[500];
}Tyrdilog;

SYSTEMTIME st_in,st_out;
DWORD Time_in,Time_out,Time_difference;
#endif
static WinRDI_YieldProc   *fpCallBackFunction = NULL; 
static WinRDI_YieldArg    *fpCallBackArg      = NULL;
static void *                 pvDCCHandleToBePassed;

static RDICCProc_FromHost     *fpDCCcallbackFromHost = NULL; 
static RDICCProc_ToHost       *fpDCCcallbackToHost   = NULL;

static RDI_ProcVec tyLocalVec;

static boolean     bGlobalTargetExecuting = FALSE;
static boolean     bAgentOpened           = FALSE;
static boolean     bModuleOpened          = FALSE;
static boolean     bExecutingDueToNonStop = FALSE;
static boolean     bStopAsserted          = FALSE;
static boolean     bGlobalReleaseControl  = FALSE;

static unsigned long       ulNumLongsToWrite            = 0;
static int                 DCCBufferCount               = 0;
static unsigned long       DCCBuffer [0x100]            = {0};

static unsigned long       ulNumberOfModules = 0;
static unsigned long       ulGlobalCurrentVectorCatch   = 0;
static unsigned long       ulGlobalSemiHostingType      = STANDARD_SEMIHOSTING;
static signed long         ulGlobalPollingSpeedDelay = 0;
static unsigned long       ulGlobalDCCSHHandlerAddress  = 0x70000;
static char                szCannotSetSemihost[]        = "Cannot set Semihosting breakpoint. No more ARM EmbeddedICE hardware resources are available. Please free existing hardware resources (remove breakpoints or watchpoints) and retry.";

#ifdef __LINUX
RDI_ProcVec *SoRDI_GetRDIProcVec(void);
int SoRDI_GetVersion(void);
void SoRDI_SetVersion(int version);
void SoRDI_Register_Yield_Callback(WinRDI_YieldProc *yieldcallback, WinRDI_YieldArg *hyieldcallback);
#endif

#ifdef _WINDOWS
static unsigned long       ulStartExecutionTickCount    = 0;
#endif
static unsigned long       ulGlobalTopOfMemory          = 0;

//static CString             csCommandLine                = "";

const TCHAR *pszRDIDriverVersion = _T("v1.0.3-A, 25-Jun-2010");

static const TCHAR *pszFirmwareVersion  = _T("v1.0.0 24-Nov-2007");

static char* pszCommandLine = (char*)"";

//This DLL supports both RDI 150 and 151
#define                    RDI_150 (int) 150
#define                    RDI_151 (int) 151

// This global holds the currently selected RDI version. RDI 150 is the default
// because RDI 151 can negotiate with the DLL to use a different level to the 
// default whereas RDI 150 cannot...
static int iGlobalRDI_Level = RDI_150;

static char szDescriptionString[] = "Ashling Opella-XD ARM RDI DLL v1.0.1";

// This structure is used for code download.
#define MAX_NO_BLOCKS          0x100
#define MAX_BUFFER_ALLOCATED   0x10000

typedef struct
{
	unsigned short usBlockIndex;
	unsigned short usNextBlockIndex;
	unsigned char *pucBlockStartBuffer;
	unsigned char *pucBlockEndBuffer;
	unsigned long  ulBlockStartAddress;
	unsigned long  ulBlockSize;
	boolean        bBlockValid;
} TyMemStruct;

typedef struct
{
	unsigned char  *pucBufferAllocated;
	unsigned char  *pucEndOfBufferAllocated;
	TyMemStruct    tyMemStruct[MAX_NO_BLOCKS + 1];
	boolean        bBufferAllocated;
	boolean        bValid;
	unsigned short usNoOfBlocks;
	
} TyCodeDownload;

static TyCodeDownload tyDownload;
static TyCodeDownload tyOptDownload;

char pszGlobalDllPathName[_MAX_PATH];
extern char szGlobalDLLPathName[_MAX_PATH];

// These defines are used for the Co processor access 
#define RDI_Access_Readable                  1
#define RDI_Access_Writable                  2
#define RDI_Access_CPDT                      4
#define RDI_Dbg_Access_Scan_Chain            8

typedef struct
{
	unsigned short rmin, rmax; 
	unsigned int nbytes; 
	unsigned char access;
	
	union
	{
		struct
		{
			unsigned char read_b0; 
			unsigned char read_b1; 
			unsigned char write_b0;
			unsigned char write_b1;
		} cprt;
		struct
		{
			unsigned char nbit; 
			unsigned char rdbits; 
		} cpdt;
	} accessinst; 
} RDI_CoProRegDesc;



typedef struct
{
	int entries;
	RDI_CoProRegDesc CPStruct[0x10];
} Co_ProcessorDescStructure;

extern TyProcessorConfig	tyProcessorConfig;
extern TyTargetConfig tyTargetConfig;
extern TyDevHandle tyCurrentDevice;
TyDeviceInfo tyDeviceInfo;

boolean bStep = 0;
boolean     bSetSafeNonVectorAddress = FALSE;

RDI_AgentHandle  agent1;
extern TyCoreConfig ptyCoreConfig[16];
extern unsigned long  pulBypassIRPattern[16];

//For new info calls
unsigned long ulGlobalExecuting;
unsigned long tyGlobalCauseOfBreak;
#ifndef __LINUX
//For User Register Settings
ProcessorConfig userregset;
bool bUseUserReg = 0;
bool bUserRegSet = 0;
#endif

TyProcessorList tyProcessorList[] = {
	{
		"ARM7DI", ARM7DI, ISCORE
	},
	{
		"ARM7TDMI", ARM7TDMI, ISCORE
	},
	{
		"ARM7TDMI-S", ARM7TDMIS, ISCORE
	},
	{	
		"ARM720T", ARM720T, ISCORE
	},
	{
		"ARM740T", ARM740T, ISCORE
	},
	{
		"ARM9TDMI", ARM9TDMI, ISCORE 
	},
	{
		"ARM920T", ARM920T, ISCORE
	},
	{
		"ARM922T", ARM922T, ISCORE
	},
	{
		"ARM926EJ-S", ARM926EJS, ISCORE
	},
	{
		"ARM940T", ARM940T, ISCORE 
	},
	{
		"ARM946E-S", ARM946ES, ISCORE
	},
	{
		"ARM966E-S", ARM966ES, ISCORE
	},
	{
		"ARM968E-S", ARM968ES, ISCORE
	},
	{
		"ARM1136JF-S", ARM1136JFS, ISCORE
	},
	{
		"ARM1156T2-S", ARM1156T2S, ISCORE
	},
	{
		"ARM1176JZF-S", ARM1176JZS, ISCORE
	},
	{
		"ARM11-MPCORE", ARM11MP, ISCORE
	},
	{
		"Cortex-M3", CORTEX_M3, ISCORE
	},
	{
		"FEROCEON", ARM926EJS, FEROCEON	/* Extended ARM926EJ-S core */
	},
	{
		"Cortex-A8", CORTEX_A8, ISCORE 
	},
	{
		"Cortex-A9", CORTEX_A9, ISCORE 
	}
};

extern "C" int ASHSetJTAGFrequency(unsigned long ulFreqKHZ,int bMode);
extern "C" int ASH_ScanIR         (unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut);
extern "C" int ASH_ScanDR         (unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut);
extern "C" int ASH_ScanIRScanDR   (unsigned long *pulLength, unsigned long *pulDataIn,unsigned long *pulDataOut);
extern "C" int ASH_PrepareforJTAGConsole(void);
extern "C" int ASH_ProcessCP15Register(unsigned int uiOperation, unsigned int uiCRn, unsigned int uiCRm,
                                      unsigned int uiOp1, unsigned int uiOp2,unsigned long *pulData);
extern "C" int ASH_ReadCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex,
								  unsigned int *puiRegValue);

extern "C" int ASH_WriteCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex, 
								   unsigned int uiValue);

/****************************************************************************
*** Interface function prototypes                                             ***
****************************************************************************/
extern "C" int openagent (RDI_AgentHandle                     *agent,
                          unsigned                            type,
                          RDI150_ConfigPointer                config,
                          struct RDI_HostosInterface  const   *i,
                          RDI_DbgState                        *dbg_state);

extern "C" int closeagent(RDI_AgentHandle agent);
#ifdef _WINDOWS
extern "C" int open(RDI_ModuleHandle                 rdi_handle,
                    unsigned                         type,
                    RDI150_ConfigPointer             config,
                    struct RDI_HostosInterface const *i,
                    RDI_DbgState                     *dbg_state);

extern "C" int close(RDI_ModuleHandle mh);

extern "C" int read(RDI_ModuleHandle  mh,
                    ARMword           ulSource,
                    void              *dest,
                    unsigned          *nbytes,
                    RDI_AccessType    type);

extern "C" int write(RDI_ModuleHandle  mh,
                     void const        *Source,
                     ARMword           dest,
                     unsigned          *nbytes,
                     RDI_AccessType    type);
#elif __LINUX

extern "C" int openRDI(RDI_ModuleHandle                 rdi_handle,
					   unsigned                         type,
					   RDI150_ConfigPointer             config,
					   struct RDI_HostosInterface const *i,
					   RDI_DbgState                     *dbg_state);

extern "C" int closeRDI(RDI_ModuleHandle mh);

extern "C" int readRDI(RDI_ModuleHandle  mh,
					   ARMword           ulSource,
					   void              *dest,
					   unsigned          *nbytes,
					   RDI_AccessType    type);

extern "C" int writeRDI(RDI_ModuleHandle  mh,
						void const        *Source,
						ARMword           dest,
						unsigned          *nbytes,
						RDI_AccessType    type);
#endif


extern "C" int CPUread(RDI_ModuleHandle  mh,
                       unsigned          mode,
                       unsigned32        mask,
                       ARMword           *state);

extern "C" int CPUwrite(RDI_ModuleHandle  mh,
                        unsigned          mode,
                        unsigned32        mask,
                        ARMword const     *state);

extern "C" int CPread(RDI_ModuleHandle  mh,
                      unsigned          CPnum,
                      unsigned32        mask,
                      ARMword           *state);

extern "C" int CPwrite(RDI_ModuleHandle  mh,
                       unsigned          CPnum,
                       unsigned32        mask,
                       ARMword const     *state);

extern "C" int setbreak(RDI_ModuleHandle  mh,
                        ARMword           address,
                        unsigned          type,
                        ARMword           bound,
                        RDI_ThreadHandle  thread,
                        RDI_PointHandle   *handle);

extern "C" int clearbreak(RDI_ModuleHandle  mh,
                          RDI_PointHandle   handle);

extern "C" int setwatch(RDI_ModuleHandle  mh,
                        ARMword           address,
                        unsigned          type,
                        unsigned          DataType,
                        ARMword           bound,
                        RDI_ThreadHandle  thread,
                        RDI_PointHandle   *handle);

extern "C" int clearwatch(RDI_ModuleHandle mh, RDI_PointHandle handle);

extern "C" int execute(RDI_AgentHandle   agent,
                       RDI_ModuleHandle  *mh,
                       RDIbool           stop_others, 
                       RDI_PointHandle   *handle);
#ifdef _WINDOWS
extern "C" int step (RDI_AgentHandle agent,
                     RDI_ModuleHandle  *mh,
                     RDIbool Stop_others,
                     unsigned ninstr,
                     RDI_PointHandle *handle);
#elif __LINUX
extern "C" int stepRDI (RDI_AgentHandle agent,
						RDI_ModuleHandle  *mh,
						RDIbool Stop_others,
						unsigned ninstr,
						RDI_PointHandle *handle);
#endif


extern "C" int info(void     *handle,
                    unsigned type,
                    ARMword  *arg1,
                    ARMword  *arg2);

extern "C" int pointinquiry(RDI_ModuleHandle  handle,
                            ARMword           *address,
                            unsigned          type,
                            unsigned          DataType,
                            ARMword           *bound);

extern "C" int addconfig(RDI_AgentHandle   agent,
                         unsigned32        nbytes);

extern "C" int loadconfigdata(RDI_AgentHandle   agent,
                              unsigned32        nbytes,
                              char const        *data);

extern "C" int selectconfig(RDI_AgentHandle      Handle,
                            RDI_ConfigAspect     aspect,
                            char const           *Name,
                            RDI_ConfigMatchType  matchtype,
                            unsigned             versionreq,
                            unsigned             *versionp);

extern "C" int loadagent(RDI_AgentHandle   Handle,
                         ARMword           dest,
                         unsigned32        size,
                         RDI_GetBufferProc *getb,
                         RDI_GetBufferArg  *getbarg);

extern "C" int targetisdead(RDI_AgentHandle agent);

extern "C" RDI_NameList const * drivernames(RDI_AgentHandle agent);

extern "C" RDI_NameList const * cpunames(RDI_AgentHandle agent);

extern "C" int errmess(RDI_AgentHandle   agent,
                       char              *buf,
                       int               buflen,
                       int               errnum);

					   /****************************************************************************
					   *** Local function prototypes                                             ***
****************************************************************************/
static TyError EnumerateModulesinTarget(ARMword *arg1, ARMword *arg2);
int Register_CommsChannelToHost( RDICCProc_ToHost *callbackToHost, 
								void * HandleToBePassed );
int Register_CommsChannelFromHost( RDICCProc_FromHost *callbackFromHost, 
								  void * HandleToBePassed );
#ifndef __LINUX
int  GetSoftwareVersions (TyAshlingVersion *tyVersionStructure);
#endif
static TyError CheckForFatalErrorMessage(TyError ErrRet);
void OpenUsbDeviceMoveProgress(unsigned char ucProgress);
TyError RDI_Convert_MidLayerErr_ToRDIErr (TyError tyMidLayerErr);
TyError OptimiseAndWriteMemoryBlock(void);
#ifdef _WINDOWS
TyError HandleDCCSemihosting (unsigned long *pulDCCSemihostReadData, 
                              unsigned long *pulDCCSemihostWriteData, 
                              boolean       *bAngelReportException, 
                              unsigned long *ulPCValue);
#endif
TyError HandleSemiHosting (TYSemiHostStruct *tySemiHostStruct, 
                           TXRXCauseOfBreak *tyCauseOfBreak);
void DebuggerCallBack (void);
TyError GetElfHeaderInfoAndSetPC(char * CmdLine);
TyError GetCoProcessorDesc (unsigned long *CPNumber, Co_ProcessorDescStructure *arg1);

void ConvertVersionString(unsigned long ulVersion, char *pszVersionString);
void ConvertDateString(unsigned long ulDate, char *pszDateString);
void ConvertPcbVersionString(unsigned long ulVersion, char *pszVersionString);
#ifndef __LINUX
void RDILogFile(Tyrdilog *tyrdilog);
#endif
void GetAcessType(char ucAccessType[50],RDI_AccessType type);
//void DecToHexConversion(char *hex,unsigned long ulword);
void GetInfoType(char info_call[50],unsigned Infotype);
void GetBreakType(char ucBreakType[50],unsigned type);
void GetDataType(char ucBreakType[50],unsigned type);
#ifndef __LINUX
void GetErrorMessage(char *ErrorType, char *Message);
//For user register settings
void InitiliaseUserRegister();
#endif
/****************************************************************************
Function: WinRDI_GetRDIProcVec
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Setup RDI function pointers
Date           Initials    Description
13-Jul-2007    SJ          Initial
****************************************************************************/
#ifdef _WINDOWS
RDI_ProcVec *WinRDI_GetRDIProcVec(void)
#else
extern "C" RDI_ProcVec *SoRDI_GetRDIProcVec(void)
#endif
{
	DLOG("Entering: WinRDI_GetRDIProcVec()\n");
	
	strcpy (tyLocalVec.rditypename, "Opella-XD");
	
	tyLocalVec.openagent=openagent;
	tyLocalVec.closeagent=closeagent;
#ifdef _WINDOWS
	tyLocalVec.open=open;
	tyLocalVec.close=close;
	tyLocalVec.read=read;
	tyLocalVec.write=write;
#elif __LINUX
	tyLocalVec.open=openRDI;
	tyLocalVec.close=closeRDI;
	tyLocalVec.read=readRDI;
	tyLocalVec.write=writeRDI;
#endif
	
	tyLocalVec.CPUread=CPUread;
	tyLocalVec.CPUwrite=CPUwrite;
	tyLocalVec.CPread=CPread;
	tyLocalVec.CPwrite=CPwrite;
	tyLocalVec.setbreak=setbreak;
	tyLocalVec.clearbreak=clearbreak;
	tyLocalVec.setwatch=setwatch;
	tyLocalVec.clearwatch=clearwatch;
	tyLocalVec.execute=execute;
#ifdef _WINDOWS
	tyLocalVec.step=step;
#elif __LINUX
	tyLocalVec.step=stepRDI;
#endif
	tyLocalVec.info=info;
	tyLocalVec.pointinquiry=pointinquiry;
	tyLocalVec.addconfig=addconfig;
	tyLocalVec.loadconfigdata=loadconfigdata;
	tyLocalVec.selectconfig=selectconfig;
	tyLocalVec.drivernames=drivernames;
	tyLocalVec.cpunames=cpunames;
	tyLocalVec.errmess=errmess;
	tyLocalVec.loadagent=loadagent;
	tyLocalVec.targetisdead=targetisdead;
	
	DLOG("Exiting: WinRDI_GetRDIProcVec()\n");
	
	return &tyLocalVec;
}



/****************************************************************************
Function: WinRDI_Register_Yield_Callback
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Store Debugger callback function pointer
Date          Initials    Description
31-Jul-2007     SJ        Initial
****************************************************************************/
#ifdef _WINDOWS
void WINAPI WinRDI_Register_Yield_Callback(  WinRDI_YieldProc *yieldcallback, 
										   WinRDI_YieldArg *hyieldcallback)
{
	DLOG("Entering: WinRDI_Register_Yield_Callback()\n");
	
	fpCallBackFunction  =  yieldcallback;
	fpCallBackArg       =  hyieldcallback;
	
	DLOG("Exiting: WinRDI_Register_Yield_Callback()\n");
	
	return;
}
#elif __LINUX
extern "C" void  SoRDI_Register_Yield_Callback(  WinRDI_YieldProc *yieldcallback, 
											   WinRDI_YieldArg *hyieldcallback)
{
	DLOG("Entering: WinRDI_Register_Yield_Callback()\n");
	
	fpCallBackFunction  =  yieldcallback;
	fpCallBackArg       =  hyieldcallback;
	
	DLOG("Exiting: WinRDI_Register_Yield_Callback()\n");
	
	return;
}
#endif
/****************************************************************************
Function: WinRDI_GetVersion
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: return DLL RDI version number
Date          Initials    Description
24-Aug-2007     SJ        Initial
****************************************************************************/
#ifdef _WINDOWS
int WINAPI WinRDI_GetVersion(void)
{
	DLOG("Entering: WinRDI_GetVersion()\n");
	
	DLOG("Exiting: WinRDI_GetVersion()\n");
	
	return iGlobalRDI_Level;  
}
#elif __LINUX
extern "C" int SoRDI_GetVersion(void)
{
	DLOG("Entering: WinRDI_GetVersion()\n");
	
	DLOG("Exiting: WinRDI_GetVersion()\n");
	
	return iGlobalRDI_Level;  
}
#endif

/****************************************************************************
Function: WinRDI_SetVersion
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Set the RDI version that the debugger would like to use.
Date          Initials    Description
24-Aug-2007     SJ        Initial
****************************************************************************/
#ifdef _WINDOWS
void WINAPI WinRDI_SetVersion(int version)
{
	DLOG("Entering: WinRDI_SetVersion()\n");
	
	if((version == 150) || (version == 151))
		iGlobalRDI_Level = version;
	//else
	//ASSERT_NOW();
	
	DLOG("Exiting: WinRDI_SetVersion()\n");
}
#elif __LINUX
extern "C" void SoRDI_SetVersion(int version)
{
	DLOG("Entering: WinRDI_SetVersion()\n");
	
	if((version == 150) || (version == 151))
		iGlobalRDI_Level = version;
	//else
	//ASSERT_NOW();
	
	DLOG("Exiting: WinRDI_SetVersion()\n");
}
#endif
/****************************************************************************
Function: WinRDI_Get_DLL_Description
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: return OPELLA-XD DLL description string
Date          Initials    Description
24-Aug-2007     SJ        Initial
****************************************************************************/
char *WinRDI_Get_DLL_Description(void)
{  
	DLOG("Entering: WinRDI_Get_DLL_Description()\n");
	
	DLOG("Exiting: WinRDI_Get_DLL_Description()\n");
	
	return szDescriptionString;
}
#ifdef _WINDOWS

/****************************************************************************
Function: WinRDI_Info
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: 
Date          Initials    Description
24-Aug-2007     SJ        Initial
****************************************************************************/
unsigned WINAPI WinRDI_Info(toolconf config, 
                            unsigned reason,
                            unsigned long *arg1, 
                            unsigned long *arg2)
{
	DLOG("Entering: WinRDI_Info()\n");
	
	NOREF(config);
	
	switch (reason)
	{
	case WinRDIInfo_ControllerCapabilities :
		break;
	case WinRDIInfo_TargetCapabilities :
		*arg1 = 0;
		*arg1 |= RDITargetCapability_CallDuringExecute; 
		*arg1 |= RDITargetCapability_RT;
		
		*arg2 = *arg1;
		break;
	default:
		return RDIError_UnimplementedMessage;
	} 
	
	DLOG("Exiting: WinRDI_Info()\n");
	
	return RDIError_NoError;
}
#endif
/****************************************************************************
Function: WinRDI_Valid_RDI_DLL
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Identify that the DLL is an RDI debug target
Date          Initials    Description
24-Aug-2007     SJ        Initial
****************************************************************************/
BOOL WinRDI_Valid_RDI_DLL(void)
{
	DLOG("Entering: WinRDI_Valid_RDI_DLL()\n");
	
	DLOG("Exiting: WinRDI_Valid_RDI_DLL()\n");
	
	return TRUE;
}
#ifndef __LINUX
/****************************************************************************
Function: WinRDI_Config
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Store configuration dialog information
Date          Initials    Description
11-Sep-2007     SJ        Initial
26-May-2008      JCK		  Updated for user register settings
****************************************************************************/
BOOL WINAPI WinRDI_Config(RDI_ConfigPointer config, HWND hParent)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());  
	
	NOREF(config);
	NOREF(hParent);
	
	boolean bOK = FALSE;
#ifndef __LINUX
	
	InitiliaseUserRegister();				// Update the user register settings in the RDI configuration GUI.
	bUserRegSet = 1;
#endif
	bOK = (unsigned char)ShowOpellaConfigDialog();
	
	return bOK;
}
#endif
/****************************************************************************
Function: openagent
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Opens the agent for debug purposes.
Date          Initials    Description
2-Jul-2007      SJ        Initial
7-Apr-2008		RS		  Modified For GDBServerForARM
05-May-2010     JCK       Added processor selection support. 
12-Aug-2010	    SJ		  Modified to avoid memory leaks.
****************************************************************************/
extern "C" int openagent (RDI_AgentHandle                     *agent,
                          unsigned                            type,
                          RDI150_ConfigPointer                config,
                          struct RDI_HostosInterface  const   *i,
                          RDI_DbgState                        *dbg_state)
{
	
	DLOG("Entering: openagent()\n");
#ifdef _WINDOWS
	AFX_MANAGE_STATE(AfxGetStaticModuleState());  
#endif
	char  szFullPathandNameForConfigFile[_MAX_PATH];
	boolean        bPassedConfigOK           = FALSE;
	COpellaConfig* OpellaConfig              = NULL;
	
	char*  szDebuggerName;
	
	TargetResetMode    ucResetMode           = NoHardReset;
	JTAGClockFreqMode  ucJTAGFreqMode        = Fixed;
	SemiHostingSupport ucSemihostingSupport  = UsingARMCompiler;
	TyError            ErrRet                = ERR_NO_ERROR;
	char ppszOpellaXDNumbers[16][16] = {0};
	unsigned short usNumberOfDevices;
	//unsigned long  pulBypassIRPattern[16];
#ifndef __LINUX
	char szErrorMsg[_MAX_PATH];   
	Tyrdilog tyopenagent = {"openagent",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
#endif
	
	NOREF(dbg_state);
	//NOREF(i);
	NOREF(type);
	
	bGlobalTargetExecuting  = FALSE;
	
	szDebuggerName = (char*) calloc (DEBUGGER_NAME_SIZE,sizeof(char));
	//Save the path of the DLL
#ifdef _WINDOWS
	ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);
#endif
#ifndef __LINUX
	//Create the RDI log file
	strcpy (szFullPathandNameForLogFile, (char *)pszGlobalDllPathName);
	strcat (szFullPathandNameForLogFile, "\\Opxdrdi.log");
#endif
	
	
	//Check the passed config to see if it was created by PathFinder, or Opella or another.
	
	if (config)
	{
		//Check the passed config is valid or not
		COpellaConfig PassedConfigObj(config);
		bPassedConfigOK  = PassedConfigObj.IsValid();
		
		//If we have a valid passed Config then we use this instead of displaying the dialog.
		if (bPassedConfigOK)
		{
            //Read the debugger name.
			if(szDebuggerName)
				PassedConfigObj.GetDebuggerName(szDebuggerName);

            //If the debugger is PFARM always use the passed pointer.
            if ((strcmp (szDebuggerName, "PFARM") == 0) ||
				(strcmp (szDebuggerName, "PFARMDB") == 0)) 
			{
				OpellaConfig = PassedConfigObj.Clone();
#ifndef __LINUX
				//int nResult = ShowOpellaConfigDialog();
				//if(nResult != TRUE)
				//   return RDIError_NoToolConfig;
#endif
				if(szDebuggerName)
					free(szDebuggerName);
			}
            else
				OpellaConfig = NULL;
		}
		else
            OpellaConfig = NULL;
	}
	
	//If the passed config was not valid try to open the last saved OpellaXD.cnf file.
	if (OpellaConfig == NULL)      
	{
		// Not created by PathFinder, try to load the last saved config
#ifdef _WINDOWS
		strcpy (szFullPathandNameForConfigFile, (char *)pszGlobalDllPathName);
		strcat (szFullPathandNameForConfigFile, "\\OpellaXD");
#elif __LINUX
		strcpy (szFullPathandNameForConfigFile,"OpellaXD" );
#endif
		OpellaConfig = new COpellaConfig( szFullPathandNameForConfigFile );
		bPassedConfigOK = OpellaConfig->IsValid();
		
		//The last saved config was either invalid or did not exist - display the dialog
		if (!bPassedConfigOK)
		{
            delete(OpellaConfig);
			
            // If this is PathFinder, return an error, do not display the dialog
            if (strcmp(szDebuggerName, "PFARM") == 0)
            {
				if(szDebuggerName)
					free(szDebuggerName);
				return RDIError_NoToolConfig;
            }
#ifndef __LINUX
            //Show the RDI configuration GUI
			if(!bGDBServer)
			{
				int nResult = ShowOpellaConfigDialog();
				if(nResult != TRUE)
					return RDIError_NoToolConfig;
			}
#endif
			
            OpellaConfig = new COpellaConfig( szFullPathandNameForConfigFile );
			
            bPassedConfigOK = OpellaConfig->IsValid();
            if(szDebuggerName)
				free(szDebuggerName);
            if (!bPassedConfigOK)
				return RDIError_NoToolConfig;
		}
	}
	
	if(szDebuggerName)
		free(szDebuggerName);

	//Use default values
	tyTargetConfig.bDebugFromReset        = FALSE;
	tyTargetConfig.ulStartupDelay         = 0;
	tyTargetConfig.bUseHardReset          = FALSE;
	tyTargetConfig.bHotPlug               = FALSE;
	
	// Read configuration data from OpellaXD.cnf and populate tyTargetConfig structure
	tyTargetConfig.dJTAGFreq              = OpellaConfig->GetFixedJTAGFrequencyInKHz();
	tyTargetConfig.ulDebugCoreNumber      = OpellaConfig->GetSelectedCore();
	OpellaConfig->GetDebuggerName(tyTargetConfig.szDebuggerName);
	OpellaConfig->GetSelectedDevice(tyTargetConfig.szProcessorType);
	OpellaConfig->GetProcessorName(tyTargetConfig.szProcessorName);
	ucResetMode                           = OpellaConfig->GetTargetResetMode();
	tyTargetConfig.dTargetVoltage         = OpellaConfig->GetHotPlugVoltage();
	ulGlobalPollingSpeedDelay             = OpellaConfig->GetTargetExecutionPollingRate();
	ulGlobalPollingSpeedDelay             = ((ulGlobalPollingSpeedDelay - 1) / 10);
	ulGlobalPollingSpeedDelay             = ((10 - (ulGlobalPollingSpeedDelay + 1)) * 10);
	tyTargetConfig.ulCacheCleanAddress    = OpellaConfig->GetCacheCleanStartAddress();
	ucJTAGFreqMode                        = OpellaConfig->GetJTAGClockFreqMode();
	tyTargetConfig.bUseNTRST              = OpellaConfig->IsUsenTRST();
	ucSemihostingSupport                  = OpellaConfig->GetSemiHostingSupport();
	tyTargetConfig.ucShiftIRPreTCKs       = OpellaConfig->GetShiftIRPreTCKs();
	tyTargetConfig.ucShiftIRPostTCKs      = OpellaConfig->GetShiftIRPostTCKs();
	tyTargetConfig.ucShiftDRPreTCKs       = OpellaConfig->GetShiftDRPreTCKs();
	tyTargetConfig.ucShiftDRPostTCKs      = OpellaConfig->GetShiftDRPostTCKs();
	tyTargetConfig.ulSelectedNoOfCores    = OpellaConfig->GetNumberOfCores();
	OpellaConfig->GetOpellaUSBSerialNumber(tyTargetConfig.szOpellaXDSerialNumber);
	tyTargetConfig.ulOpellaXDInstance     = OpellaConfig->GetOpellaUSBInstanceNumber();
	tyTargetConfig.bDebugRequestState     = OpellaConfig->IsSetDBGRQLow();
	tyTargetConfig.bCoresightCompliant    = (boolean)OpellaConfig->IsCoresightCompliant();
	tyTargetConfig.bCacheInvalidationNeeded   = (boolean)OpellaConfig->GetCacheInvalidationParam();

	if (TRUE == tyTargetConfig.bCoresightCompliant)
	{
		tyTargetConfig.ulCoresightBaseAddr	  = OpellaConfig->GetDebugBaseAddress();
		tyTargetConfig.ulDbgApIndex			  = OpellaConfig->GetDebugApIndex();
		tyTargetConfig.ulMemApIndex			  = OpellaConfig->GetMemoryApIndex();
		tyTargetConfig.ulUseMemApForMemAccess = OpellaConfig->GetUseMemApForMemAccess();
	}

	/* Added For TI Core Support */
	tyTargetConfig.bTICore				  = (boolean)OpellaConfig->IsTICore();
	
	if ( TRUE == tyTargetConfig.bTICore )
	{
		tyTargetConfig.ulTISubPortNum		  = OpellaConfig->GetTISubPortNumber();
		tyTargetConfig.bDapPcSupport		  = (boolean)OpellaConfig->IsDapPcSupported();
	}	
	
	if (tyTargetConfig.ulSelectedNoOfCores>1)
	{
		for(unsigned long j=0; j< (tyTargetConfig.ulSelectedNoOfCores) ; j++)
		{
			pulBypassIRPattern[j] = BYPASS_PATTERN;
			ptyCoreConfig[j].pulBypassIRPattern = &pulBypassIRPattern[j];
			ptyCoreConfig[j].usDefaultDRLength  = 0x1;
			ptyCoreConfig[j].usDefaultIRLength  = OpellaConfig->GetIRLength(j);
		}
	}
	bSetSafeNonVectorAddress              = OpellaConfig->IsSafeNVAddressEnabled();
	
	if(bSetSafeNonVectorAddress)
		tyTargetConfig.ulSNVAddress        = OpellaConfig->GetSafeNonVectorAddress();
	else
		tyTargetConfig.ulSNVAddress        = 0x0;
#ifndef __LINUX
	bLogEnableOK							     = OpellaConfig->IsRDILoggingEnabled();
	
	//If RDI logging enabled, log to file
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);      
		char *temp;  
		unsigned ibytes; 
		strcpy(tyopenagent.arg_name[0],"Type  = 0x");  
		ibytes = type;
		temp = tyopenagent.arg_value[0];
		itoa(ibytes,temp,16);
	}
#endif
	
	if (!strcmp(tyTargetConfig.szOpellaXDSerialNumber, ""))
	{
		ML_CheckOpellaXDInstance(ppszOpellaXDNumbers,&usNumberOfDevices);
		strcpy(tyTargetConfig.szOpellaXDSerialNumber,ppszOpellaXDNumbers[0]);
		if(!strcmp(tyTargetConfig.szOpellaXDSerialNumber, ""))
		{
#ifndef __LINUX
			if(!bGDBServer)
            {
				if(bLogEnableOK)
				{
					FILE *fp;
					fp = fopen(szFullPathandNameForLogFile,"w");
					fprintf(fp,"\nRDI Driver Version %s\n",pszRDIDriverVersion);
					fprintf(fp,"\n< %02d:%02d:%03d > Logging Started at Day %d :Month %d :Year %d ::%d Hr :%d Mnt :%d Sec :%03d MilliSec",st_in.wMinute,st_in.wSecond,st_in.wMilliseconds,st_in.wDay,st_in.wMonth,st_in.wYear,st_in.wHour,st_in.wMinute,st_in.wSecond,st_in.wMilliseconds);         
					strcpy(tyopenagent.return_value,"RDIError_LoopBack_Opella");
					strcpy(tyopenagent.errormessage,"Opella-XD is not connected. Connect Opella-XD and Retry");
					Time_out = GetTickCount();
					fclose(fp);
					RDILogFile(&tyopenagent);		      
				}
				MessageBox(NULL,"Opella-XD is not connected. Connect Opella-XD and Retry",OPXDCAPTION,MB_OK|MB_ICONWARNING);
            }
#endif
			return RDIError_LoopBack_Opella;
		}
	}
	
	switch(ucResetMode) 
	{
	case NoHardReset:
		tyTargetConfig.bUseHardReset   = FALSE; 
		break;
	case HardResetAndDelay:
		tyTargetConfig.bUseHardReset   = TRUE;
		tyTargetConfig.ulStartupDelay  = OpellaConfig->GetHardResetDelay();
		
		break;
	case HardResetAndHaltAtRV:
		tyTargetConfig.bUseHardReset   = TRUE;
		tyTargetConfig.bDebugFromReset = TRUE;
		break;
		/*   case HardResetAndGo:
		tyTargetConfig.bUseHardReset   = TRUE;
		break;*/
	case HotPlug:
		tyTargetConfig.bUseHardReset   = FALSE; 
		tyTargetConfig.bHotPlug        = TRUE;
		break;
	case CoreOnly:
		tyTargetConfig.bCoreReset		= TRUE;
		tyTargetConfig.bPeripheralReset = FALSE; 
		break;
	case CoreAndPeripherals:
		tyTargetConfig.bCoreReset		= FALSE;
		tyTargetConfig.bPeripheralReset = TRUE; 
		break;
	default:
		break;
	}
	
	switch(ucJTAGFreqMode)
	{
	case Fixed:
		tyTargetConfig.dJTAGFreq = OpellaConfig->GetFixedJTAGFrequencyInKHz();
		tyTargetConfig.bAdaptiveClockingEnabled = FALSE;
		break;
	case Adaptive:
		tyTargetConfig.bAdaptiveClockingEnabled = TRUE;
		break;
	case Automatic: //TODO: Implement later
	default:
		break;
	}
	
	switch(ucSemihostingSupport)
	{
	case UsingARMCompiler:
		tyTargetConfig.bUseGNUSemihosting = FALSE;
		break;
	default:
	case UsingGNUARMCompiler:
		tyTargetConfig.bUseGNUSemihosting = TRUE;
		break;
	}
	delete(OpellaConfig);

	unsigned long ulNumProcessor = sizeof(tyProcessorList) / sizeof(TyProcessorList); 
	
	unsigned long ulProcessorIndex;

	for (ulProcessorIndex = 0; ulProcessorIndex < ulNumProcessor; ulProcessorIndex++)
	{
		if (0 == strcmp( tyTargetConfig.szProcessorType, tyProcessorList[ulProcessorIndex].szCoreName))
		{
			tyTargetConfig.tyProcType	= tyProcessorList[ulProcessorIndex].tyProcType;
			tyTargetConfig.tyDeviceType = tyProcessorList[ulProcessorIndex].tyDeviceType;
			break;
		}
	}

	if (ulNumProcessor == ulProcessorIndex)
	{
		return RDIError_IncorrectProcType;
	}
	
	//   else 
	//      ASSERT_NOW();
	
	if (!bAgentOpened)
	{
		
#ifndef __LINUX
		if(bLogEnableOK)
		{
			FILE *fp;
			fp = fopen(szFullPathandNameForLogFile,"w");
			fprintf(fp,"\nRDI Driver Version %s\n",pszRDIDriverVersion);
			fprintf(fp,"\n< %02d:%02d:%03d > Logging Started at Day %d :Month %d :Year %d ::%d Hr :%d Mnt :%d Sec :%03d MilliSec",st_in.wMinute,st_in.wSecond,st_in.wMilliseconds,st_in.wDay,st_in.wMonth,st_in.wYear,st_in.wHour,st_in.wMinute,st_in.wSecond,st_in.wMilliseconds);         
			fclose(fp);
		}
#endif
		
		/************************************************************************************/
		/*****       Check for Opella XD firmware and if not latest update it          ******/
		/************************************************************************************/
		
		if(bGDBServer)
			printf("\nChecking Opella-XD firmware.");
#ifndef __LINUX
		else
		{
			// Create the progress bar...
			pOpenUSBProgress = new CProgressControl;
			(void)pOpenUSBProgress->ShowWindow(SW_SHOW);
			((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 100);                  // initialize progress bar
			pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Updating Opella-XD firmware .....");      
			
			//Check for firmware, if not latest update
			OpenUsbDeviceMoveProgress(0);
		}
#endif

		ErrRet = ML_UpdateOpellaXDFirmware(OpenUsbDeviceMoveProgress, tyTargetConfig.szOpellaXDSerialNumber);
		
		if(bGDBServer)
			printf("\n");
#ifndef __LINUX
		else
		{
			//Close progress bar   
			(void)pOpenUSBProgress->ShowWindow(SW_HIDE);
			delete pOpenUSBProgress;
		}
#endif  
		if (ErrRet != ML_ERROR_NO_ERROR)
		{	  
			ErrRet = RDI_Convert_MidLayerErr_ToRDIErr(ErrRet);
			
#ifndef __LINUX
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,szReturnValue);
				if(ErrRet == RDIError_InvalidSerialNumber)
					sprintf(tyopenagent.errormessage,"Invalid serial number (%s) in current configuration file. This does not match the currently connected Opella-XD. Please reconfigure Opella-XD RDI Driver.",tyTargetConfig.szOpellaXDSerialNumber);
				else
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			}
#endif
			return ErrRet;
		}
		
		/************************************************************************************/
		/*****                                 END                                     ******/
		/************************************************************************************/
		
		/************************************************************************************/
		/*****           Configure Opella XD with fpgaware and diskware                ******/
		/************************************************************************************/
		
		// now configure Opella-XD
		if(bGDBServer)
			printf("Configuring Opella-XD.");
#ifndef __LINUX
		else
		{
			// Create the progress bar...
			pOpenUSBProgress = new CProgressControl;
			(void)pOpenUSBProgress->ShowWindow(SW_SHOW);
			((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 100);                  // initialize progress bar
			pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Configuring Opella-XD .....");
			
			
			//Loads the Diskware and the FPGAware
			OpenUsbDeviceMoveProgress(0);
		}
#endif
		
		ErrRet = ML_OpenOpellaXD(OpenUsbDeviceMoveProgress,
			tyTargetConfig.szOpellaXDSerialNumber);
		if(ErrRet == ML_ERROR_NO_ERROR)
			tyTargetConfig.bUSBDevice = TRUE;
		
#ifndef __LINUX
		OpenUsbDeviceMoveProgress(100);
#endif
		
		if(bGDBServer)
			printf("\n");
#ifndef __LINUX
		else
		{
			//Close progress bar   
			(void)pOpenUSBProgress->ShowWindow(SW_HIDE);
			delete pOpenUSBProgress;
		}
#endif
		if (ErrRet != ML_ERROR_NO_ERROR)
		{	  
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyopenagent.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			}
#endif
			return ErrRet;
		}
		
		/************************************************************************************/
		/*****                                 END                                     ******/
		/************************************************************************************/
		
		ErrRet = ML_GetDeviceInfo(&tyDeviceInfo);
		
		if((tyDeviceInfo.tyTpaConnected == TPA_NONE)&&(!tyTargetConfig.bHotPlug))
		{
#ifndef __LINUX
			if(!bGDBServer)
			{         
				sprintf(szErrorMsg,"Check whether the TPA is connected or not with Opella-XD with Serial no. %s.",tyTargetConfig.szOpellaXDSerialNumber);
				MessageBox(NULL,szErrorMsg, OPXDCAPTION, MB_OK|MB_ICONWARNING);
				// Should also close the Opella XD device before exiting...
				(void)ML_CloseOpellaXD();
			}
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,"RDIError_TPANotConnected");            
				strcpy(tyopenagent.errormessage,szErrorMsg);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			}         
#endif
			return RDIError_TPANotConnected;
			
		}
		
		if((tyDeviceInfo.dCurrentVtref<=0.0)&&(!tyTargetConfig.bHotPlug))
		{
#ifndef __LINUX
			if(!bGDBServer)
			{         
				sprintf(szErrorMsg,"Check whether the target is connected or not with Opella-XD with Serial no. %s.",tyTargetConfig.szOpellaXDSerialNumber);
				MessageBox(NULL,szErrorMsg, OPXDCAPTION, MB_OK|MB_ICONWARNING);
				// Should also close the Opella XD device before exiting...
				(void)ML_CloseOpellaXD();
			}
			
			
			
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,"RDIError_TargetBroken");          
				strcpy(tyopenagent.errormessage,szErrorMsg);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			}         
#endif
			return RDIError_TargetBroken;
		}
		
		if (tyTargetConfig.bHotPlug)
		{
#ifndef __LINUX
			if(!bGDBServer)
				MessageBox(NULL,"Opella-XD is configured for Hot Plug. \nConnect Opella-XD to your target.", OPXDCAPTION, MB_OK|MB_ICONWARNING);
#endif 
			if(bGDBServer)
			{
				printf("Opella-XD is configured for Hot Plug.\nConnect Opella-XD to your target and press any key to continue.\n");
#ifndef __LINUX
				getch();
#else
				getchar();
#endif
			}
			ErrRet = ML_GetDeviceInfo(&tyDeviceInfo);
			
			if(tyDeviceInfo.dCurrentVtref == 0.0)
			{
#ifndef __LINUX
				if(!bGDBServer)
					MessageBox(NULL,"No Vtref detected. Reconfigure with correct target voltage.", OPXDCAPTION, MB_OK|MB_ICONWARNING);
#endif
				// Should also close the Opella XD device before exiting...
				(void)ML_CloseOpellaXD();
				
#ifndef __LINUX
				if(bLogEnableOK)
				{
					strcpy(tyopenagent.return_value,"RDIError_TargetBroken");
					strcpy(tyopenagent.errormessage,"No Vtref detected. Reconfigure with correct target voltage.");
					Time_out = GetTickCount();
					RDILogFile(&tyopenagent);
				}   
#endif
				return RDIError_TargetBroken;
			}
		}
		
		//Set up the processor type - Update the internal structures and pass info to diskware
		ErrRet = ML_SetupProcType(tyTargetConfig.tyProcType);
		
		if (ErrRet != ML_ERROR_NO_ERROR)
		{	  
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyopenagent.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			} 
#endif
			return ErrRet;
		}
		
		// Allocate the memory for the breakpoint structure.
		ErrRet = ML_AllocateMemForBreakpoints (); 
		
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyopenagent.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			} 
#endif
		}
		
		if(tyProcessorConfig.bARM11)
		{
			// Allocate the memory for the Watchpoint structure.
			ErrRet = ML_AllocateMemForWatchpoints (); 
			
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				if(bLogEnableOK)
				{
					strcpy(tyopenagent.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopenagent);
				} 
#endif
			}
		}
		
		//Setup the semihosting interface.
		if (i !=NULL)
			SH_SetSemiHostingFunctionCalls (i);
		
		//Initialize the debugger hardware
		ErrRet = ML_Initialise_Debugger(&tyTargetConfig);		
		
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			if(bGDBServer)
				return ErrRet;
			
#ifndef __LINUX
			
			if (ErrRet == ML_Error_NoRTCKSupport)
				MessageBox(NULL,"No RTCK response from the target. Please reconfigure Opella-XD RDI driver to use a fixed frequency.",OPXDCAPTION,MB_OK|MB_ICONWARNING);
			else 
#endif
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tyopenagent.return_value,szReturnValue);
				if (ErrRet == ML_Error_NoRTCKSupport)
					sprintf(tyopenagent.errormessage,"No RTCK response from the target. Please reconfigure Opella-XD RDI driver to use a fixed frequency.",tyTargetConfig.szOpellaXDSerialNumber);
				else
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyopenagent);
			} 	
#endif
			return ErrRet;
		}
		
		//Set the state of debug request.
		//This signal is normally '0' for debug but certain target boards use this
		//signal to select ARM debug instead of boundary scan.
		if (tyTargetConfig.bDebugRequestState)
		{
            ErrRet = ML_DebugRequest(CLEAR_DEBUG_REQUEST);
            if (ErrRet != ML_ERROR_NO_ERROR)
			{			
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopenagent.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopenagent);
				} 	
#endif
				return ErrRet;
			}
		}
		else
		{
            ErrRet = ML_DebugRequest(SET_DEBUG_REQUEST);
            if (ErrRet != ML_ERROR_NO_ERROR)
			{			
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopenagent.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopenagent);
				} 
#endif
				return ErrRet;
			}	
		}
		
		if (tyTargetConfig.bUseHardReset)
		{
			//Now we have opened connection to OpellaXD so we reset the target.
			ErrRet = ML_InfoProc(RDIInfo_ForceSystemReset, NULL, NULL);
			if (ErrRet != ML_ERROR_NO_ERROR)
            {			
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopenagent.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopenagent);
				} 	
#endif
				return ErrRet;
			}	
			
			// We need to pulse TRST before delaying. This will enable the 
			// processor to start executing...
			if (tyTargetConfig.bUseNTRST)
            {
				ErrRet = ML_ResetTAPController();
				if (ErrRet != ML_ERROR_NO_ERROR)
				{			
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyopenagent.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyopenagent.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyopenagent);
					} 
#endif
					return ErrRet;
				}	
            }
			
			//Check if we need a delay to allow target to run startup code.
			if ((tyTargetConfig.ulStartupDelay > 0) && !tyTargetConfig.bDebugFromReset)
            {
#ifndef __LINUX
				if(!bGDBServer)
				{
					pOpenUSBProgress = new CProgressControl;
					(void)pOpenUSBProgress->ShowWindow(SW_SHOW);
					((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 100);
					pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Delaying after Target Hard Reset....");
				}
#endif
				if(bGDBServer)
					printf("Delaying %ld seconds after target hard reset.\n",tyTargetConfig.ulStartupDelay);
				
				
				for (unsigned char j=0; j < 100; j++)
				{
#ifndef __LINUX
					OpenUsbDeviceMoveProgress(j);
#endif
					ML_Delay(tyTargetConfig.ulStartupDelay * 10);
				}
				if(!bGDBServer)
				{
#ifndef __LINUX
					(void)pOpenUSBProgress->ShowWindow(SW_HIDE);
					delete pOpenUSBProgress;
#endif
				}
				
            }
			
		}
		else
		{
			// We need to pulse TRST just in case we are recovering from a hard reset...
			if (tyTargetConfig.bUseNTRST)
			{
				ErrRet = ML_ResetTAPController();
				if (ErrRet != ML_ERROR_NO_ERROR)
				{
					
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyopenagent.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyopenagent.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyopenagent);
					} 
#endif
					return ErrRet;					
				}
			}
			
			
			
			if (tyTargetConfig.bCoreReset)
			{	
				ErrRet = ML_ResetCore(CORE_ONLY);
				if (ErrRet != ML_ERROR_NO_ERROR)
				{
					ErrRet = RDI_Convert_MidLayerErr_ToRDIErr (ErrRet);
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyopenagent.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyopenagent.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyopenagent);
					} 
#endif

					return ErrRet;
				}
			} 
			
			if (tyTargetConfig.bPeripheralReset)
			{
				ErrRet = ML_ResetCore(CORE_PERIPHERAL);
				
				if (ErrRet != ML_ERROR_NO_ERROR)
				{
					ErrRet = RDI_Convert_MidLayerErr_ToRDIErr (ErrRet);
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyopenagent.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyopenagent.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyopenagent);
					} 
#endif

					return ErrRet;
				}				
			}
		}


		// We need to set the device type to the Diskware,if it is a device (not a core)
		if(FEROCEON == tyTargetConfig.tyDeviceType)
		{
			ErrRet = ML_InitTarget((unsigned long)FEROCEON);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				ErrRet = RDI_Convert_MidLayerErr_ToRDIErr (ErrRet);
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopenagent.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopenagent.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopenagent);
				} 
#endif
				return ErrRet;					
			}
		}

		
		// Debug agent is now opened and configured.
		bAgentOpened = TRUE;
		*agent=(RDI_AgentHandle)AGENT_HANDLE_VALUE;
		
		agent1=(RDI_AgentHandle)AGENT_HANDLE_VALUE;
		
		//If RDI logging enabled, log to file
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			Time_out = GetTickCount();
			RDILogFile(&tyopenagent);
		}
		bUseUserReg = 1;                   //Set the user register
#endif
		
		DLOG("Exiting: openagent()\n");
		
		return RDIError_NoError; 
   }
#ifndef __LINUX
   if(bLogEnableOK)
   {
	   Time_out = GetTickCount();
	   RDILogFile(&tyopenagent);
   }
#endif
   
   
   return ErrRet; 
}

/****************************************************************************
Function: closeagent
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Closes the agent
Date          Initials    Description
10-Jul-2007      SJ       Initial
****************************************************************************/
extern "C" int closeagent(RDI_AgentHandle agent)

{
	DLOG("Entering: closeagent()\n");
	
	int ErrRet = RDIError_NoError; 
#ifndef __LINUX
	Tyrdilog tycloseagent = {"closeagent",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
#endif   
	NOREF(agent);
	
#ifndef __LINUX
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
	}
#endif
	
	do
	{
		ErrRet = ML_RemoveAllBreakpoints();
		if (ErrRet != ML_ERROR_NO_ERROR)  
			break;
		
		//DeAllocate the memory for the breakpoint structure.
		ErrRet = ML_DeAllocateMemForBreakpoints();
		if (ErrRet != ML_ERROR_NO_ERROR)  
			break;
		
	}while(0);
	
	if(tyProcessorConfig.bARM11)
	{
		
		do
		{
			//DeAllocate the memory for the watchpoint structure.
			ErrRet = ML_DeAllocateMemForWatchpoints();
			if (ErrRet != ML_ERROR_NO_ERROR)  
				break;
			
		}while(0);
	}
	
	// Should also close the Opella XD device...
	(void)ML_CloseOpellaXD();
	
	bAgentOpened = FALSE;
	
	ErrRet = RDI_Convert_MidLayerErr_ToRDIErr(ErrRet);
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		strcpy(tycloseagent.return_value,szReturnValue);
		GetErrorMessage(szReturnValue,tycloseagent.errormessage);
		Time_out = GetTickCount();
		RDILogFile(&tycloseagent);
		FILE *fp;
		fp = fopen(szFullPathandNameForLogFile,"a");
		fprintf(fp,"\n< %02d:%02d:%03d > Logging Stopped at Day %d :Month %d :Year %d ::%d Hr :%d Mnt :%d Sec :%03d MilliSec",st_in.wMinute,st_in.wSecond,st_in.wMilliseconds,st_in.wDay,st_in.wMonth,st_in.wYear,st_in.wHour,st_in.wMinute,st_in.wSecond,st_in.wMilliseconds);         
		fclose(fp);      
	}
#endif
	DLOG("Exiting: closeagent()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: open
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: "Opens" the specified processor for debug purposes.
Date          Initials    Description
20-Jul-2007      SJ       Initial
26-May-2008      JCK		  Updated for user register settings
****************************************************************************/
#ifdef _WINDOWS
extern "C" int open(RDI_ModuleHandle                 rdi_handle,
                    unsigned                         type,
                    RDI150_ConfigPointer             config,
                    struct RDI_HostosInterface const *i,
                    RDI_DbgState                     *dbg_state)
#elif __LINUX
					extern "C" int openRDI(RDI_ModuleHandle                 rdi_handle,
                    unsigned                         type,
                    RDI150_ConfigPointer             config,
                    struct RDI_HostosInterface const *i,
                    RDI_DbgState                     *dbg_state)
#endif
					
					
{
	DLOG("Entering: open()\n");
	
	int            ErrRet = RDIError_NoError;
	unsigned long  ulTempVectorCatch;
	
	/*unsigned long  ulSize;
	unsigned long  ulAddr;
	unsigned long  ulValue;
	
	unsigned long   count = 0;*/
#ifndef __LINUX
	//ProcessorConfig *userregset;
	
	Tyrdilog tyopen = {"open",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
							 "0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;      
		strcpy(tyopen.arg_name[0],"Type  = 0x");  
		ibytes = type;
		temp = tyopen.arg_value[0];
		itoa(ibytes,temp,16);
	}
#endif
	
	/*unreferenced formal parameters*/
	NOREF(dbg_state);
	NOREF(i);
	NOREF(config);
	/*unreferenced formal parameters*/
	
	if ((type >> RDIOpen_NonStopDebug) == 0x1)
		tyTargetConfig.bNonStopDebugSelected = TRUE;
	else
		tyTargetConfig.bNonStopDebugSelected = FALSE;
	
	if(!bModuleOpened)
	{
		ulNumberOfModules = 0;
		
		//We do not care about the return value.
		(void)CL_SetARM9RestartAddress(&tyTargetConfig.ulCacheCleanAddress);
		if (tyProcessorConfig.bARM7 ||
            (tyProcessorConfig.bARM9))		//Set the safe non vector address. 
		{
			ErrRet = ML_SetSafeNonVectorAddress (tyTargetConfig.ulSNVAddress);
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopen.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopen.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopen);				
				}
#endif
				return ErrRet;
			}


		}

		if (tyTargetConfig.bDebugFromReset)
		{
			// Debug from zero.
			ErrRet = ML_StopCoreAtZero ();
			
			// May need to translate the error message here...
			if (ErrRet == ML_Error_TargetRunning)
            {
#ifndef __LINUX
				if(!bGDBServer)
				MessageBox(NULL,"Unable to halt at Reset Vector(hardware breakpoint failed possibly due to\n   \
Embedded ICE reset).You will need to reconfigure.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
#endif
				//return RDIError_CannotStopAtResetVector;
				//IAR does not handle this if an error occurs. So we need to close the device 
				//and agent to proceed further.
#ifdef _WINDOWS
				(void)close(rdi_handle);
#elif __LINUX
				(void)closeRDI(rdi_handle);
#endif
				(void)closeagent(agent1);
				
				ErrRet = RDI_Convert_MidLayerErr_ToRDIErr(ErrRet);
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopen.return_value,szReturnValue);
					strcpy(tyopen.errormessage,"Unable to halt at Reset Vector(hardware breakpoint failed possibly due to Embedded ICE reset).You will need to reconfigure");
					Time_out = GetTickCount();
					RDILogFile(&tyopen);				
				}
#endif
				return ErrRet;
            }
			
			// Now check the error return...
			if (ErrRet != ML_ERROR_NO_ERROR)
			{
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopen.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopen.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopen);				
				}
#endif
				return ErrRet;
			}
		}
		
		if (tyTargetConfig.bNonStopDebugSelected == FALSE)
		{
			ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
			if (!((ErrRet==ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_TargetStopped)))
			{
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyopen.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyopen.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyopen);				
				}
#endif
				return ErrRet;
			}
			
			if((tyProcessorConfig.bARM7) ||
				(tyProcessorConfig.bARM9))
			{
				ErrRet = ML_SetupICEBreakerModule();
				if (ErrRet != ML_ERROR_NO_ERROR)
				{
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyopen.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyopen.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyopen);				
					}
#endif
					return ErrRet;
				}
			}
		}
		
		ErrRet= ML_DebugStartup (&bExecutingDueToNonStop);
		if (ErrRet != ML_ERROR_NO_ERROR)
		{
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tyopen.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyopen.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyopen);				
			}
#endif
			return ErrRet;
		}
		
		// If we were asked to connect to a running target....
		if (tyTargetConfig.bNonStopDebugSelected == TRUE)
		{
			// And if the target was already halted...
			if (bExecutingDueToNonStop == FALSE)
            {
				// Then forget about the request to connect without halting...
				tyTargetConfig.bNonStopDebugSelected = FALSE;
				// And setup the ICE breaker module as well...
				if((tyProcessorConfig.bARM7) ||
					(tyProcessorConfig.bARM9))
				{
					ErrRet = ML_SetupICEBreakerModule();
					if (ErrRet != ML_ERROR_NO_ERROR)
					{
						ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
						
						if(bLogEnableOK)
						{
							strcpy(tyopen.return_value,szReturnValue);
							GetErrorMessage(szReturnValue,tyopen.errormessage);
							Time_out = GetTickCount();
							RDILogFile(&tyopen);				
						}
#endif
						return ErrRet;
					}
				}
            }
		}
		
		bModuleOpened =TRUE;
      }
	  else
      {
		  if (!((strcmp (tyTargetConfig.szDebuggerName, "IDE") == 0) || (strcmp (tyTargetConfig.szDebuggerName, "PFARM") == 0) || (strcmp (tyTargetConfig.szDebuggerName, "PFARMXD") == 0)))        
		  {
			  // ADW and AXD open the module again after download of code.
			  //whereas Metrowerks and PFARM expect us to keep the BP's during Open module.
			  ErrRet = ML_RemoveAllBreakpoints ();
			  if (ErrRet != ML_ERROR_NO_ERROR)
			  {
				  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				  
				  if(bLogEnableOK)
				  {
					  strcpy(tyopen.return_value,szReturnValue);
					  GetErrorMessage(szReturnValue,tyopen.errormessage);
					  Time_out = GetTickCount();
					  RDILogFile(&tyopen);				
				  }
#endif
				  return ErrRet;
			  }
		  }
		  
		  //Setup the vector catch again.
		  //Use a local copy of vector catch because function can change vectors set if a 
		  //the BP's cannot be set. (ADW supports this but AXD ignores it...)
		  ulTempVectorCatch = ulGlobalCurrentVectorCatch;
		  
		  ErrRet = ML_SetupVectorCatch (&ulTempVectorCatch);
		  if (ErrRet != ML_ERROR_NO_ERROR)
		  {
			  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			  
			  if(bLogEnableOK)
			  {
				  strcpy(tyopen.return_value,szReturnValue);
				  GetErrorMessage(szReturnValue,tyopen.errormessage);
				  Time_out = GetTickCount();
				  RDILogFile(&tyopen);				
			  }
#endif
			  return ErrRet;
		  }
		  
		  //Set the Semihosting type again if Standard Semihosting. This will force the breakpoints to be set for standard
		  //SemiHosting.
		  if (ulGlobalSemiHostingType == STANDARD_SEMIHOSTING)
		  {
			  ErrRet = ML_InfoProc (RDISemiHosting_SetState, 
				  &ulGlobalSemiHostingType, 
				  0);
			  if (ErrRet != ML_ERROR_NO_ERROR)
			  {
				  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
				  
				  if(bLogEnableOK)
				  {
					  strcpy(tyopen.return_value,szReturnValue);
					  GetErrorMessage(szReturnValue,tyopen.errormessage);
					  Time_out = GetTickCount();
					  RDILogFile(&tyopen);				
				  }
#endif
				  return ErrRet;
			  }
		  }
		  
		  //Reset the semihosting variables,
		  SH_Reset_SemiHosting();
      }
	  
	  if (!bExecutingDueToNonStop)
      {  
		  ErrRet = ML_Initialise_SemiHosting();
		  if (ErrRet != ML_ERROR_NO_ERROR)
		  {
			  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			  
			  if(bLogEnableOK)
			  {
				  strcpy(tyopen.return_value,szReturnValue);
				  GetErrorMessage(szReturnValue,tyopen.errormessage);
				  Time_out = GetTickCount();
				  RDILogFile(&tyopen);				
			  }
#endif
			  return ErrRet;
		  }
		  
		  ErrRet = ML_LoadDCC_SH_Handler();
		  if (ErrRet != ML_ERROR_NO_ERROR)
		  {
			  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
#ifndef __LINUX
			  
			  if(bLogEnableOK)
			  {
				  strcpy(tyopen.return_value,szReturnValue);
				  GetErrorMessage(szReturnValue,tyopen.errormessage);
				  Time_out = GetTickCount();
				  RDILogFile(&tyopen);				
			  }
#endif
			  return ErrRet;
		  }
      }
#ifndef __LINUX
	  /*if(bUseUserReg)
	  {	
		  if(!bUserRegSet)					//While Restoring the last saved configuration
			  InitiliaseUserRegister();  //we had to update the user register settings also.      
		  
		  count = userregset.ulUserSpecificVariablesCount;
		  
		  for(unsigned int i = 0;i < count; i++)
		  {
			  ulValue = userregset.pUserSpecificVariables[i].ulVariableValue;
			  ulAddr = userregset.pUserSpecificVariables[i].adVariableAddress;
			  ulSize = userregset.pUserSpecificVariables[i].adVariableSize;
			  ErrRet = ML_WriteProc((void *)&ulValue,
				  ulAddr,
				  (unsigned long *) &ulSize, 
				  RDIAccess_Data32, 
				  FALSE,
				  FALSE);	
			  if(ErrRet != ML_ERROR_NO_ERROR)
			  {
				  ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
				  if(bLogEnableOK)
				  {
					  strcpy(tyopen.return_value,szReturnValue);
					  GetErrorMessage(szReturnValue,tyopen.errormessage);
					  Time_out = GetTickCount();
					  RDILogFile(&tyopen);
				  }
				  return ErrRet;
			  }		
		  }
		  bUseUserReg = 0;
	  }*/
#endif
#ifndef __LINUX
	  
	  
	  if(bLogEnableOK)
	  {
		  Time_out = GetTickCount();
		  if (bExecutingDueToNonStop)
			  strcpy(tyopen.return_value,"RDIError_Executing");
		  RDILogFile(&tyopen);
	  }
#endif
	  
	  DLOG("Exiting: open()\n");
	  
	  if (bExecutingDueToNonStop)
		  return RDIError_Executing;
	  else
		  return RDIError_NoError; 
}

/****************************************************************************
Function: close
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: "Closes" the specified processor after debugging.
Date          Initials    Description
23-Jul-2007      SJ       Initial
****************************************************************************/
#ifdef _WINDOWS
extern "C" int close(RDI_ModuleHandle mh)
#elif __LINUX
extern "C" int closeRDI(RDI_ModuleHandle mh)
#endif
{
	DLOG("Entering: close()\n");
#ifndef __LINUX
	Tyrdilog tyclose = {"close",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	// If the core is not being closed because a reset was detected restore the
	// contents of the registers,
	if (!ML_HasResetOccurred ())
	{
		ErrRet = ML_RestoreRegisters ();
		
		if(ErrRet != ML_ERROR_NO_ERROR)
		{
			ErrRet = RDI_Convert_MidLayerErr_ToRDIErr(ErrRet);
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tyclose.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyclose.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyclose);				
			}
#endif
			return ErrRet;
		}
	}
	
	bModuleOpened = FALSE;
#ifndef __LINUX
	
	
	if(bLogEnableOK)
	{
		Time_out = GetTickCount();
		RDILogFile(&tyclose);
	}
#endif
	DLOG("Exiting: close()\n");
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: read
Engineer: Brian Connolly
Input: See RDI specification.
Output: See RDI specification.
Description: Performs a memory read from the processor.
Date          Initials    Description
23-Jul-2007     SJ         Initial
****************************************************************************/
#ifdef _WINDOWS
extern "C" int read(RDI_ModuleHandle  mh,
                    ARMword           ulSource,
                    void              *dest,
                    unsigned          *nbytes,
                    RDI_AccessType    type)
#elif __LINUX
					extern "C" int readRDI(RDI_ModuleHandle  mh,
                    ARMword           ulSource,
                    void              *dest,
                    unsigned          *nbytes,
                    RDI_AccessType    type)
#endif
					
					
{
	DLOG("Entering: read()\n");
#ifndef __LINUX
	Tyrdilog tyread = {"read",
							 "0","0","0",
							 "0","0","0",
							 "RDIError_NoError",
							 "0"}; 
	
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tyread.arg_name[0],"Addr  = 0x");  
		ibytes = ulSource;
		temp = tyread.arg_value[0];
		itoa(ibytes,temp,16);	
		
		strcpy(tyread.arg_name[1],"Bytes = 0x");
		temp = tyread.arg_value[1];
		ibytes = *nbytes;
		itoa(ibytes,temp,16);    
		
		strcpy(tyread.arg_name[2],"Type  =  ");
		temp = tyread.arg_value[2];      
		GetAcessType(temp,type);
	}
#endif
	
	int ErrRet = RDIError_NoError;
	

	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
	{
#ifndef __LINUX
		if(bLogEnableOK)
		{
			strcpy(tyread.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tyread.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tyread);
		}
#endif
		return RDIError_TargetRunning;
	}
	else if (bGlobalTargetExecuting && (bStopAsserted || !RealMonitorDetected()))
	{
		unsigned long i;
		
		for (i=0; i < *nbytes; i++)
			((unsigned char *)dest)[i] = 0xFF;
	}
	else
		ErrRet = ML_ReadProc(ulSource,dest,(unsigned long *)nbytes, type, TRUE);      
	
	ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));
	
#ifndef __LINUX
	if(bLogEnableOK)
	{
		strcpy(tyread.return_value,szReturnValue);
		Time_out = GetTickCount();
		RDILogFile(&tyread);	
	}
#endif
	
	DLOG("Exiting: read()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: write
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Performs a memory write to the processor. Supports custom load 
for faster code download
Date          Initials    Description
24-Jul-2007     SJ         Initial
****************************************************************************/
#ifdef _WINDOWS
extern "C" int write(RDI_ModuleHandle  mh,
	void const        *Source,
	ARMword           dest,
	unsigned          *nbytes,
	RDI_AccessType    type)
#elif __LINUX
	extern "C" int writeRDI(RDI_ModuleHandle  mh,
	void const        *Source,
	ARMword           dest,
	unsigned          *nbytes,
	RDI_AccessType    type)
#endif
	
	
{
	DLOG("Entering: write()\n");
#ifndef __LINUX
	Tyrdilog tywrite = {"write",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes; 
		
		strcpy(tywrite.arg_name[0],"Addr  = 0x");  
		ibytes = dest;
		temp = tywrite.arg_value[0];
		itoa(ibytes,temp,16);
		
		strcpy(tywrite.arg_name[1],"Bytes = 0x");
		temp = tywrite.arg_value[1];
		ibytes = *nbytes;
		itoa(ibytes,temp,16);
		
		strcpy(tywrite.arg_name[2],"Type  =  ");
		temp = tywrite.arg_value[2];      
		GetAcessType(temp,type);   
	}
#endif
	
	int ErrRet = RDIError_NoError;
	int i      = 0x0;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tywrite.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tywrite.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tywrite);			
		}   
#endif
		return RDIError_TargetRunning;
	}
	else if (bGlobalTargetExecuting && (bStopAsserted || !RealMonitorDetected()))
	{
		// Do not perform the write since we are just about to halt.
	}
	else
	{
		//If custom load is supported by debugger we can buffer the write proc calls,
		//So that we can choose the optimal size for code download blocks.
		if ((tyDownload.bBufferAllocated && *nbytes < MAX_BUFFER_ALLOCATED))
		{
			tyDownload.bValid = TRUE;
			if ((tyDownload.usNoOfBlocks >= MAX_NO_BLOCKS) || 
				((tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].pucBlockStartBuffer + *nbytes ) >= (tyDownload.pucEndOfBufferAllocated - 1)))
			{
				ErrRet =  OptimiseAndWriteMemoryBlock();
				if(ErrRet != RDIError_NoError)
				{               
					ErrRet = CheckForFatalErrorMessage(ErrRet);
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						if(RDIError_Failed_To_Allocate_PC_Memory)
							strcpy(tywrite.return_value,"RDIError_Failed_To_Allocate_PC_Memory");
						else
							strcpy(tywrite.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tywrite.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tywrite);						
					}
#endif
					return ErrRet;					
				}
				tyDownload.tyMemStruct[0].pucBlockStartBuffer = tyDownload.pucBufferAllocated;
				tyDownload.tyMemStruct[0].usNextBlockIndex = 1;
				tyDownload.usNoOfBlocks = 0;
				
				for (i=0; i <= MAX_NO_BLOCKS; i++)
				{
					tyDownload.tyMemStruct[i].bBlockValid = FALSE;
				}
			}
			
			//Store the data in the memory structure
			memcpy(tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].pucBlockStartBuffer, Source, *nbytes);
			tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].ulBlockStartAddress = dest ;
			tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].pucBlockEndBuffer =  (tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].pucBlockStartBuffer + *nbytes - 1);
			tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].ulBlockSize = *nbytes;
			tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].bBlockValid = TRUE;
			
			//Update the pointers for the next block.
			tyDownload.tyMemStruct[tyDownload.usNoOfBlocks + 1].pucBlockStartBuffer =  tyDownload.tyMemStruct[tyDownload.usNoOfBlocks].pucBlockEndBuffer + 0x1;
			tyDownload.usNoOfBlocks ++;
			
		}
		else
		{
			//Just write data to target.
			ErrRet = ML_WriteProc((void *)Source,
				dest,
				(unsigned long *) nbytes, 
				type, 
				FALSE,
				FALSE); 
			if(ErrRet != ML_ERROR_NO_ERROR)
			{
				ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tywrite.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tywrite.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tywrite);
				}
#endif
				return ErrRet;
			}
		}
	}
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		Time_out = GetTickCount();
		RDILogFile(&tywrite);
	}
#endif
	
	DLOG("Exiting: write()\n");
	
	return RDIError_NoError;
}

/****************************************************************************
Function: CPUread
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Performs a register read from the processor.
Date          Initials    Description
24-Jul-2007     SJ         Initial
****************************************************************************/
extern "C" int CPUread(RDI_ModuleHandle  mh,
                       unsigned          mode,
                       unsigned32        mask,
                       ARMword           *state)
					   
{
	DLOG("Entering: CPUread()\n");
#ifndef __LINUX
	Tyrdilog tycpuread = {"CPUread",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"}; 
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tycpuread.arg_name[0],"Mode  = 0x");  
		ibytes = mode;
		temp = tycpuread.arg_value[0];
		itoa(ibytes,temp,16);
		
		strcpy(tycpuread.arg_name[1],"Mask  = 0x");
		temp = tycpuread.arg_value[1];
		ibytes = mask;
		itoa(ibytes,temp,16);                
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tycpuread.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tycpuread.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tycpuread);			
		}   
#endif
		return RDIError_TargetRunning;
	}
	
	if (tyProcessorConfig.bCortexM3)
	{
		ErrRet = ML_CortexMCPUReadProc(mode, mask, state);
	}
	else
	{
		ErrRet = ML_CPUReadProc(mode, mask, state);
	}
	
	ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		Time_out = GetTickCount();      
		strcpy(tycpuread.return_value,szReturnValue);
		if(ErrRet != RDIError_NoError)
			GetErrorMessage(szReturnValue,tycpuread.errormessage);
		RDILogFile(&tycpuread);
	}  
#endif
	
	DLOG("Exiting: CPUread()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: CPUwrite
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Performs a register write to the processor.
Date          Initials    Description
24-Jul-2007     SJ         Initial
****************************************************************************/
extern "C" int CPUwrite(RDI_ModuleHandle  mh,
                        unsigned          mode,
                        unsigned32        mask,
                        ARMword const     *state)
						
{
	DLOG("Entering: CPUwrite()\n");
#ifndef __LINUX
	Tyrdilog tycpuwrite = {"CPUwrite",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tycpuwrite.arg_name[0],"Mode  = 0x");  
		ibytes = mode;
		temp = tycpuwrite.arg_value[0];
		itoa(ibytes,temp,16);
		
		strcpy(tycpuwrite.arg_name[1],"Mask  = 0x");			  
		temp = tycpuwrite.arg_value[1];
		ibytes = mask;
		itoa(ibytes,temp,16);            
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tycpuwrite.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tycpuwrite.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tycpuwrite);			
		}
#endif
		return RDIError_TargetRunning;
	}
	
	if(tyProcessorConfig.bCortexM3)
	{
        ErrRet = ML_CortexMCPUWriteProc(mode, mask, (unsigned long *)state);
	}
	else
	{
        ErrRet =  ML_CPUWriteProc (mode, mask, (unsigned long *)state);
	}
	
	ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
	
	
	if(bLogEnableOK)
	{
		strcpy(tycpuwrite.return_value,szReturnValue);
		GetErrorMessage(szReturnValue,tycpuwrite.errormessage);
		Time_out = GetTickCount();
		RDILogFile(&tycpuwrite);
	}
#endif
	
	DLOG("Exiting: CPUwrite()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: CPread
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Performs a register read from the co-processor.
Date          Initials    Description
25-Jul-2007     SJ         Initial
****************************************************************************/
extern "C" int CPread(RDI_ModuleHandle  mh,
                      unsigned          CPnum,
                      unsigned32        mask,
                      ARMword           *state)
					  
{
	DLOG("Entering: CPread()\n");
#ifndef __LINUX
	Tyrdilog tycpread = {"CPread",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
								"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;  
		
		strcpy(tycpread.arg_name[0],"CPnum = 0x");  
		ibytes = CPnum;
		temp = tycpread.arg_value[0];
		itoa(ibytes,temp,16);	
		
		strcpy(tycpread.arg_name[1],"Mask  = 0x");
		temp = tycpread.arg_value[1];
		ibytes = mask;
		itoa(ibytes,temp,16);
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
    {
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tycpread.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tycpread.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tycpread);			
		}
#endif
		return RDIError_TargetRunning;
	}
	
	ErrRet = CL_CPReadProc(CPnum, mask, (unsigned long *)state);
	
	ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
	
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		strcpy(tycpread.return_value,szReturnValue);
		GetErrorMessage(szReturnValue,tycpread.errormessage);
		Time_out = GetTickCount();
		RDILogFile(&tycpread);
	}
#endif
	
	DLOG("Exiting: CPread()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: CPwrite
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Performs a register write to the co-processor.
Date          Initials    Description
26-Jul-2007     SJ         Initial
****************************************************************************/
extern "C" int CPwrite(RDI_ModuleHandle  mh,
                       unsigned          CPnum,
                       unsigned32        mask,
                       ARMword const     *state)
					   
{
	DLOG("Entering: CPwrite()\n");
#ifndef __LINUX
	Tyrdilog CPwrite = {"CPwrite",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;  
		
		strcpy(CPwrite.arg_name[0],"CPnum = 0x");  
		ibytes = CPnum;
		temp = CPwrite.arg_value[0];
		itoa(ibytes,temp,16);	
		
		strcpy(CPwrite.arg_name[1],"Mask  = 0x");
		temp = CPwrite.arg_value[1];
		ibytes = mask;
		itoa(ibytes,temp,16);
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(CPwrite.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,CPwrite.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&CPwrite);			
		}
#endif
		return RDIError_TargetRunning;
	}
	ErrRet = CL_CPWriteProc(CPnum, mask, (unsigned long *)state);
	
	ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		strcpy(CPwrite.return_value,szReturnValue);
		if(ErrRet != RDIError_NoError)
			GetErrorMessage(szReturnValue,CPwrite.errormessage);
		Time_out = GetTickCount();
		RDILogFile(&CPwrite);
	}
#endif
	
	DLOG("Exiting: CPwrite()\n");
	
	return ErrRet;
}   

/****************************************************************************
Function: setbreak
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Sets a breakpoint at the specified address
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
int setbreak(  RDI_ModuleHandle  mh,
			 ARMword           address,
			 unsigned          type,
			 ARMword           bound,
			 RDI_ThreadHandle  thread,
			 RDI_PointHandle   *handle)
{
	DLOG("Entering: setbreak()\n");
#ifndef __LINUX
	
	Tyrdilog tysetbreak = {"setbreak",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"}; 
    if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tysetbreak.arg_name[0],"Addr  = 0x");  
		ibytes = address;
		temp = tysetbreak.arg_value[0];
		itoa(ibytes,temp,16);
		
		strcpy(tysetbreak.arg_name[1],"Type  =  ");
		temp = tysetbreak.arg_value[1];      
		GetBreakType(temp,type);
	}

	
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	NOREF(thread);
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
	{
#ifndef __LINUX
		if(bLogEnableOK)
		{
			strcpy(tysetbreak.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tysetbreak.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tysetbreak);			
		} 
#endif
		return RDIError_TargetRunning;
	}
	else if (bGlobalTargetExecuting && (bStopAsserted || !RealMonitorDetected()))
	{
		// Do not perform the write since we are just about to halt.
	}
	else if (bGlobalTargetExecuting)
	{
		// OK, let's first halt the application....
		ErrRet = ExecuteStop();
		
		if(ErrRet!=RDIError_NoError)
        {
			ErrRet = CheckForFatalErrorMessage (ErrRet);
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				if(ErrRet == RDIError_RealMonFailed)
					strcpy (tysetbreak.return_value,"RDIError_RealMonFailed");
				else
					strcpy(tysetbreak.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tysetbreak.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tysetbreak);			
			}     
#endif
			return ErrRet;
        }
		
		// Setup the breakpoint in the normal way....
		ErrRet = ML_SetBreakProc(address,
			type,
			bound,
			handle);
		
		if(ErrRet != ML_ERROR_NO_ERROR)
		{		
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tysetbreak.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tysetbreak.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tysetbreak);			
			}        
#endif
			return ErrRet;
		}
		if((tyProcessorConfig.bARM7) ||
			(tyProcessorConfig.bARM9))
		{
            // But we also need to setup the ICE Breaker registers...
            ErrRet = ML_SetupWatchpoints();
		}
		else if(tyProcessorConfig.bARM11)
		{
            //Write BVR and BCR registers
            ErrRet = ML_SetupBPRegsARM11();
		}
		
		if(ErrRet != ML_ERROR_NO_ERROR)
		{		
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tysetbreak.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tysetbreak.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tysetbreak);			
			}       
#endif
			return ErrRet;
		}
		
		/*21-Sep
		if(!tyProcessorConfig.bARM11)
		{
		// OK, now let's restart the application....
		ErrRet = ExecuteGo();
		
		  if(ErrRet!=RDIError_NoError)
		  {
		  ErrRet = CheckForFatalErrorMessage (ErrRet);
		  #ifndef __LINUX
		  
			if(bLogEnableOK)
			{			
			if(ErrRet == RDIError_RealMonFailed)
			strcpy(tysetbreak.return_value,"RDIError_RealMonFailed");
			else
			strcpy(tysetbreak.return_value,szReturnValue);
            GetErrorMessage(szReturnValue,tysetbreak.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tysetbreak);
			}
			#endif
			return ErrRet;
			}
			}
		*/
	}
	else
	{
		ErrRet = ML_SetBreakProc(address,
			type,
			bound,
			handle);
		
		if(ErrRet != ML_ERROR_NO_ERROR)
		{
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tysetbreak.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tysetbreak.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tysetbreak);				
			}
#endif
			return ErrRet;
		}
	}
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		Time_out = GetTickCount();
		RDILogFile(&tysetbreak);
	}
#endif
	
	DLOG("Exiting: setbreak()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: clearbreak
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Clears the speciified breakpoint
Date          Initials    Description
16-Aug-2007     SJ        Initial
****************************************************************************/
int clearbreak(RDI_ModuleHandle  mh,
               RDI_PointHandle   handle)
{
	DLOG("Entering: clearbreak()\n");
#ifndef __LINUX
	Tyrdilog tyclearbreak = {"clearbreak",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tyclearbreak.arg_name[0],"Handle = 0x");  
		ibytes = handle;
		temp = tyclearbreak.arg_value[0];
		itoa(ibytes,temp,16);
	}
#endif
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	int ErrRet = RDIError_NoError;
	
	if (bGlobalTargetExecuting && (ML_CheckSemiHostState() != DCC_REALMON))
		
		
	{   
#ifndef __LINUX  
		if(bLogEnableOK)
		{
			strcpy(tyclearbreak.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tyclearbreak.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tyclearbreak);			
		} 
#endif
		return RDIError_TargetRunning;
	}
	else 
		if (bGlobalTargetExecuting && (bStopAsserted || !RealMonitorDetected()))
		{
			// Do not perform the write since we are just about to halt.
		}
		else 
			if (bGlobalTargetExecuting)
			{
				// OK, let's first halt the application....
				ErrRet = ExecuteStop(); 
				
				if(ErrRet!=RDIError_NoError)
				{
					ErrRet = CheckForFatalErrorMessage (ErrRet);
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						if(ErrRet == RDIError_RealMonFailed)
							strcpy (tyclearbreak.return_value,"RDIError_RealMonFailed");
						else
							strcpy(tyclearbreak.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyclearbreak.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyclearbreak);			
					}      
#endif
					return ErrRet;
				}
				// Clear the breakpoint in the normal way....
				ErrRet = ML_ClearBreakProc(handle);
				
				if(ErrRet != ML_ERROR_NO_ERROR)
				{
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyclearbreak.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyclearbreak.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyclearbreak);			
					}   
#endif
					return ErrRet;
				}
				if((tyProcessorConfig.bARM7) ||
					(tyProcessorConfig.bARM9))
				{
					// But we also need to setup the ICE Breaker registers...
					ErrRet = ML_SetupWatchpoints();
				}
				else if(tyProcessorConfig.bARM11)
				{
					//Write BVR and BCR registers
					ErrRet = ML_SetupBPRegsARM11();
				}
				
				if(ErrRet != ML_ERROR_NO_ERROR)
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyclearbreak.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyclearbreak.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyclearbreak);			
					}  
#endif
					return ErrRet;
				}
				
				// OK, now let's restart the application....
				ErrRet = ExecuteGo();
				
				if(ErrRet!=RDIError_NoError)
				{
					ErrRet = CheckForFatalErrorMessage (ErrRet);
#ifndef __LINUX
					
					if(bLogEnableOK)
					{			
						if(ErrRet == RDIError_RealMonFailed)
							strcpy(tyclearbreak.return_value,"RDIError_RealMonFailed");
						else
							strcpy(tyclearbreak.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyclearbreak.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyclearbreak);
					}
#endif
					
					return ErrRet;
				}      
			}
			else
			{
				ErrRet = ML_ClearBreakProc(handle);
				if (!((ErrRet == ML_Error_NoSuchPoint) || (ErrRet == ML_ERROR_NO_ERROR)))
				{
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyclearbreak.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyclearbreak.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyclearbreak);				
					}
#endif
					return ErrRet;
				}
			}
			
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				Time_out = GetTickCount();
				RDILogFile(&tyclearbreak);
			}
#endif
			
			DLOG("Exiting: clearbreak()\n");
			
			return RDIError_NoError;
}

/****************************************************************************
Function: setwatch
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Sets a watch point at the specified address.
Date          Initials    Description
16-Aug-2007     SJ        Initial
****************************************************************************/
int setwatch(  RDI_ModuleHandle  mh,
			 ARMword           address,
			 unsigned          type,
			 unsigned          DataType,
			 ARMword           bound,
			 RDI_ThreadHandle  thread,
			 RDI_PointHandle   *handle)
{
	DLOG("Entering: setwatch()\n");
#ifndef __LINUX
	
	Tyrdilog tysetwatch = {"setwatch",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tysetwatch.arg_name[0],"Addr = 0x");  
		ibytes = address;
		temp = tysetwatch.arg_value[0];
		itoa(ibytes,temp,16);
		
		strcpy(tysetwatch.arg_name[1],"Type = ");
		temp = tysetwatch.arg_value[1];      
		GetBreakType(temp,type);
		
		strcpy(tysetwatch.arg_name[2],"Data Type = ");
		temp = tysetwatch.arg_value[2];      
		GetDataType(temp,DataType);
	}
#endif
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	NOREF(thread);
	
	if (bGlobalTargetExecuting)
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tysetwatch.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tysetwatch.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tysetwatch);			
		} 
#endif
		return RDIError_TargetRunning;
	}
	
	ErrRet = ML_SetWatchProc(address,type, DataType,bound,handle);
	
	if(ErrRet != ML_ERROR_NO_ERROR)
	{		
		ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tysetwatch.return_value,szReturnValue);
			GetErrorMessage(szReturnValue,tysetwatch.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tysetwatch);			
		}  
#endif
		return ErrRet;
	}
	
	if(tyProcessorConfig.bARM11)
	{
		//Write WVR and WCR registers
		ErrRet = ML_SetupWPRegsARM11();
	}
	
	if(ErrRet != ML_ERROR_NO_ERROR)
	{		
		ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tysetwatch.return_value,szReturnValue);
			GetErrorMessage(szReturnValue,tysetwatch.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tysetwatch);			
		}  
#endif
		return ErrRet;
	}
	
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		Time_out = GetTickCount();
		RDILogFile(&tysetwatch);
	}
#endif
	
	DLOG("Exiting: setwatch()\n");
	
	return RDIError_NoError;
}

/****************************************************************************
Function: clearwatch
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Clears the specified watchpoint
Date          Initials    Description
16-Aug-2007     SJ        Initial
****************************************************************************/
int clearwatch(RDI_ModuleHandle mh, RDI_PointHandle handle)
{
	DLOG("Entering: clearwatch()\n");
#ifndef __LINUX
	
	Tyrdilog tyclearwatch = {"clearwatch",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(tyclearwatch.arg_name[0],"Handle = 0x");  
		ibytes = handle;
		temp = tyclearwatch.arg_value[0];
		itoa(ibytes,temp,16);
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	
	if (bGlobalTargetExecuting)
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tyclearwatch.return_value,"RDIError_TargetRunning");
			GetErrorMessage(szReturnValue,tyclearwatch.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tyclearwatch);			
		} 
#endif
		return RDIError_TargetRunning;
	}
	ErrRet = ML_ClearWatchProc(handle);
	if(ErrRet != ML_ERROR_NO_ERROR)
	{
		ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tyclearwatch.return_value,szReturnValue);
			GetErrorMessage(szReturnValue,tyclearwatch.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tyclearwatch);			
		}  
#endif
		return ErrRet;
	}
	if(tyProcessorConfig.bARM11)
	{
		ML_SetupWPRegsARM11();
		if(ErrRet != ML_ERROR_NO_ERROR)
		{
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tyclearwatch.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyclearwatch.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyclearwatch);			
			}  
#endif
			return ErrRet;
		}
	}
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		Time_out = GetTickCount();
		RDILogFile(&tyclearwatch);			
	}
#endif
	
	DLOG("Exiting: clearwatch()\n");
	
	return RDIError_NoError;
}

/****************************************************************************
Function: execute
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Starts program execution
Date          Initials    Description
16-Aug-2007     SJ        Initial
****************************************************************************/
int execute(RDI_AgentHandle   agent,
            RDI_ModuleHandle  *mh,
            RDIbool           stop_others, 
            RDI_PointHandle   *handle)
{
	DLOG("Entering: execute()\n");
#ifndef __LINUX
	
	Tyrdilog tyexecute = {"execute",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"}; 
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes; 
		
		strcpy(tyexecute.arg_name[0],"StopOthers  = ");  
		ibytes = stop_others;
		temp = tyexecute.arg_value[0];
		if(stop_others == 0)
			strcpy(tyexecute.arg_value[0],"FALSE");
		else
			strcpy(tyexecute.arg_value[0],"TRUE");
	}
#endif
	
	int               ErrRet                        = NO_ERROR;
	boolean           bExecuting                    = FALSE;
	boolean           bDataToRead                   = FALSE;
	boolean           bDataWritten                  = FALSE;
	unsigned long     ulSemiHostOption              = STANDARD_SEMIHOSTING;
	TXRXCauseOfBreak  tyCauseOfBreak                = USER_HALT;
	int               Status                        = 0;
	unsigned long     ulReadData                    = 0;
	unsigned long     ulCallBackCounter             = 0;
	TYSemiHostStruct  tySemiHostStruct              = {0};
	unsigned long     ulDCCSemihostReadData[0x100]  = {0};
	unsigned long     ulDCCSemihostWriteData[0x100] = {0};
	unsigned long     *pulDCCSemihostReadData       = NULL;
	unsigned long     *pulDCCSemihostWriteData      = NULL;
	unsigned long     i                             = 0;
	boolean           bAngelReportException         = FALSE;
	unsigned long     ulPCValue                     = 0x0;
	unsigned long     ulCurrentPCValue              = 0x0;
	unsigned long     ulCallBackFrequency           = 0x0;
	unsigned short    usNumberOfDCCWords            = 0x0;
	unsigned short    j                             = 0x0;
	RDI_PointHandle   tyTempBreakPointHandle        = 0x0;
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	NOREF(stop_others);
	NOREF(agent);
	
	bStep = 0;
	if (tyTargetConfig.bUSBDevice)
		ulCallBackFrequency = 0x10;
	else
		ulCallBackFrequency = 0x1000;
	
	//Check whether Semihosting is enabled. PathFndr only has one console window
	//so we can only have SemiHosting or DCC support - not both.SemiHosting has a higher
	//Priority than DCC.
	ulSemiHostOption = ML_CheckSemiHostState ();
	
	//Start the core if non stop debug is not selected.
	if (tyTargetConfig.bNonStopDebugSelected == FALSE)
	{
		ErrRet = ML_ExecuteProc();
		if(ErrRet != ML_ERROR_NO_ERROR)
		{		
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tyexecute.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tyexecute.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tyexecute);			
			}     
#endif
			return ErrRet;
		}
	}
	
	bGlobalTargetExecuting = TRUE;
	
	if (ulSemiHostOption == DCC_REALMON)
		(void)InitialiseCommsLink();
	
	
	//Once the agent has connected to the module and gained control this flag
	//is not needed.
	tyTargetConfig.bNonStopDebugSelected = FALSE;
	bExecutingDueToNonStop = FALSE;
	
#ifdef _WINDOWS
	//Set the global execution time start counter.
	ulStartExecutionTickCount = GetTickCount();
#endif
	
	bExecuting     = TRUE;
	bStopAsserted  = FALSE;
	
	do
	{
		// Check for DCC calls from core if enabled 
		if (!((fpDCCcallbackFromHost == NULL) || (fpDCCcallbackToHost == NULL)))
		{
			do
            {
				fpDCCcallbackFromHost(pvDCCHandleToBePassed, &DCCBuffer[ulNumLongsToWrite+DCCBufferCount], &Status);
				if (Status !=0)
					ulNumLongsToWrite++;
            }
			while (Status != 0);
			
			if (ulNumLongsToWrite != 0)
            {
				//Write data to the core and check for data read from core.
				ErrRet = ML_CheckDCC  (&ulReadData,
					&bDataToRead,
					&DCCBuffer[DCCBufferCount],
					&bDataWritten);
				
				if(ErrRet != ML_ERROR_NO_ERROR)
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyexecute.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyexecute.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyexecute);			
					}  
#endif
					return ErrRet;
				}
				
				// We have data to write to the core.
				if (bDataWritten)
				{
					ulNumLongsToWrite--;
					DCCBufferCount++;
					// We we have just sent all the words in the current string, reset
					// the buffer-index back to the start of the string.
					if (ulNumLongsToWrite == 0)
						DCCBufferCount = 0;
				}
            }
			else 
            {  
				// We don't have any data to write to core check for data read from the core..
				ErrRet = ML_CheckDCC  (&ulReadData,
					&bDataToRead,
					NULL,
					NULL);
				
				if(ErrRet != ML_ERROR_NO_ERROR)
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyexecute.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyexecute.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyexecute);			
					}  
#endif
					return ErrRet;
				}
            } 
			
			if (bDataToRead)
            {
				if(fpDCCcallbackToHost) 
					fpDCCcallbackToHost(pvDCCHandleToBePassed, ulReadData);
            }
		}
		
		// DCC Based semihosting
		if ((ulSemiHostOption == DCC_SEMIHOSTING) && !bStopAsserted)
		{
			pulDCCSemihostReadData = &ulDCCSemihostReadData[0];
			bDataToRead = TRUE;
			
			//Check whether there is a SemiHosting call to be serviced.
			ErrRet = ML_CheckDCC  (pulDCCSemihostReadData,
				&bDataToRead,
				NULL,
				NULL);
			
			if(ErrRet != ML_ERROR_NO_ERROR)
            {		
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tyexecute.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tyexecute.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tyexecute);			
				}  
#endif
				return ErrRet;
			}
			if (bDataToRead)
            {
				usNumberOfDCCWords = (unsigned short)(ulDCCSemihostReadData[0] >> 16);
				ulDCCSemihostReadData[0] = (ulDCCSemihostReadData[0] & 0xFFFF);
				bDataToRead = FALSE;
				
				for (j=1; j<usNumberOfDCCWords; j++)
				{
					pulDCCSemihostReadData ++;
					//Read all the data from the target.
					do
					{
						ErrRet = ML_CheckDCC  (pulDCCSemihostReadData,
							&bDataToRead,
							NULL,
							NULL);
						
						if(ErrRet != ML_ERROR_NO_ERROR)
						{		
							ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							
							if(bLogEnableOK)
							{
								strcpy(tyexecute.return_value,szReturnValue);
								GetErrorMessage(szReturnValue,tyexecute.errormessage);
								Time_out = GetTickCount();
								RDILogFile(&tyexecute);			
							} 
#endif
							return ErrRet;
						}
					}
					while (!bDataToRead);
				}
				
				if (ulDCCSemihostReadData[0] != 0)
				{
					
					pulDCCSemihostWriteData = &ulDCCSemihostWriteData[0];
#ifndef __LINUX
					//Handle semihosting here then write back data to core....
					ErrRet = HandleDCCSemihosting (&ulDCCSemihostReadData[0], pulDCCSemihostWriteData, &bAngelReportException, &ulPCValue);
					
					if(ErrRet != RDIError_NoError)
					{
						
						
						if(bLogEnableOK)
						{
							if(ErrRet == RDIError_SoftwareInterrupt)
								strcpy(tyexecute.return_value,"RDIError_SoftwareInterrupt");
							else
								strcpy(tyexecute.return_value,szReturnValue);
							GetErrorMessage(szReturnValue,tyexecute.errormessage);
							Time_out = GetTickCount();
							RDILogFile(&tyexecute);
						}
						
						return ErrRet;
					}
#endif
					//The Angel Semihosting call stops the core so force Status proc to be called.
					if (bAngelReportException)
					{
						ulCallBackCounter = 0x0;
					}
					
					if (ulDCCSemihostWriteData[0] > 0x0)
					{
						//Write data to the core; 
						for (i = 0; i <= ulDCCSemihostWriteData[0]; i++)
						{
							do
							{
								ErrRet = ML_CheckDCC  (&ulReadData,
									&bDataToRead,
									&pulDCCSemihostWriteData[i],
									&bDataWritten);
								
								if(ErrRet != ML_ERROR_NO_ERROR)
								{		
									ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
									
									if(bLogEnableOK)
									{
										strcpy(tyexecute.return_value,szReturnValue);
										GetErrorMessage(szReturnValue,tyexecute.errormessage);
										Time_out = GetTickCount();
										RDILogFile(&tyexecute);			
									}     
#endif
									return ErrRet;
								}                           
							}
							while (!bDataWritten);
							
							//There should be no data to read whilst this is happening.
							//TODO: ASSERT(!bDataToRead);
							//TODO: ASSERT(bDataWritten);
						}
					}
					
					ulDCCSemihostReadData[0] = 0;
					ulDCCSemihostReadData[1] = 0;
					ulDCCSemihostReadData[2] = 0;
					ulDCCSemihostWriteData[0] = 0;
					ulDCCSemihostWriteData[1] = 0;
					ulDCCSemihostWriteData[2] = 0;
				}
            }
         }
		 
		 
		 // This determines the frequency of the callbacks to Pathfinder.
		 // This is done to speed up the DCC comms channel.
		 if ((!bDataWritten && !bDataToRead) || (ulCallBackCounter == ulCallBackFrequency) || (ulCallBackCounter == 0x0))
         {
			 //Check the status of the processor.
			 ErrRet = ML_StatusProc(&bExecuting,
				 &tyCauseOfBreak,
				 handle,
				 NULL);
			 
			 if(ErrRet != ML_ERROR_NO_ERROR)
			 {		
				 if(!bExecuting)
					 bGlobalTargetExecuting = FALSE;

				 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				 
				 if(bLogEnableOK)
				 {
					 strcpy(tyexecute.return_value,szReturnValue);
					 GetErrorMessage(szReturnValue,tyexecute.errormessage);
					 Time_out = GetTickCount();
					 RDILogFile(&tyexecute);			
				 }    
#endif
				 return ErrRet;
			 }  
			 
			 // Check for Semihosting if the core is stopped.
			 if (  (!bExecuting) && 
				 (ulSemiHostOption == STANDARD_SEMIHOSTING))
			 {
				 tySemiHostStruct.tyCauseOfBreak = tyCauseOfBreak;
				 ErrRet = ML_CheckForSemihosting(&tySemiHostStruct);
				 if (!((ErrRet == ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_SoftwareInterrupt)))
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tyexecute.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tyexecute.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tyexecute);			
					 }   
#endif
					 return ErrRet;
				 }
				 
				 if (tySemiHostStruct.bSemiHostDetected)
				 {
					 ErrRet = HandleSemiHosting (&tySemiHostStruct,
						 &tyCauseOfBreak);
					 if(ErrRet != RDIError_NoError)
					 {
#ifndef __LINUX
						 
						 if(bLogEnableOK)
						 {
							 strcpy(tyexecute.return_value,szReturnValue);
							 GetErrorMessage(szReturnValue,tyexecute.errormessage);
							 Time_out = GetTickCount();
							 RDILogFile(&tyexecute);			
						 }    
#endif
						 return ErrRet;
					 }
					 
					 if (tySemiHostStruct.bRestartExecution && !bStopAsserted)
					 {
						 ErrRet = ML_SemiHostGo (tySemiHostStruct.ulLinkRegisterValue, 
							 tySemiHostStruct.ulSPSRValue);
						 
						 if(ErrRet != ML_ERROR_NO_ERROR)
						 {		
							 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							 
							 if(bLogEnableOK)
							 {
								 strcpy(tyexecute.return_value,szReturnValue);
								 GetErrorMessage(szReturnValue,tyexecute.errormessage);
								 Time_out = GetTickCount();
								 RDILogFile(&tyexecute);			
							 }  
#endif
							 return ErrRet;
						 }
						 
						 bExecuting = TRUE;
					 }
					 else
					 {
						 //Rewrite the PC and CPSR so the user sees that the SWI instruction has been 
						 //executed properly.
						 if (tyProcessorConfig.bCortexM3)
						 {
							 ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
								 REG_PC_MASK, 
								 &tySemiHostStruct.ulLinkRegisterValue);
						 } 
						 else
						 {
							 // R0 contains status.
							 ErrRet = ML_CPUWriteProc(USR_MODE, 
								 REG_PC_MASK, 
								 &tySemiHostStruct.ulLinkRegisterValue); 					   
						 }                 
						 
						 if(ErrRet != ML_ERROR_NO_ERROR)
						 {		
							 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							 
							 if(bLogEnableOK)
							 {
								 strcpy(tyexecute.return_value,szReturnValue);
								 GetErrorMessage(szReturnValue,tyexecute.errormessage);
								 Time_out = GetTickCount();
								 RDILogFile(&tyexecute);			
							 }  
#endif
							 return ErrRet;
						 }
						 
						 if (tyProcessorConfig.bCortexM3)
						 {
							 ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
								 REG_PC_MASK, 
								 &tySemiHostStruct.ulLinkRegisterValue);							 
							 
						 } 
						 else
						 {
							 ErrRet = ML_CPUWriteProc(USR_MODE, 
								 REG_CPSR_MASK, 
								 &tySemiHostStruct.ulSPSRValue);
						 }  
						 
						 
						 if(ErrRet != ML_ERROR_NO_ERROR)
						 {		
							 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							 
							 if(bLogEnableOK)
							 {
								 strcpy(tyexecute.return_value,szReturnValue);
								 GetErrorMessage(szReturnValue,tyexecute.errormessage);
								 Time_out = GetTickCount();
								 RDILogFile(&tyexecute);			
							 }  
#endif
							 return ErrRet;
						 }
						 tyCauseOfBreak = USER_HALT; 
					 }
               }
            }
			//Callback to the debugger to process Windows messages.
			// Only do this if we are currently executing....
			if (bExecuting) 
				DebuggerCallBack ();
			
			ulCallBackCounter = 1;
			
			if (bStopAsserted && bExecuting)
            {
				ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
				if (!((ErrRet==ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_TargetStopped)))
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tyexecute.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tyexecute.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tyexecute);			
					}    
#endif
					return ErrRet;
				}
				
				ulCallBackCounter = 0x0;
            }
         }
		 ulCallBackCounter++;
		 
		 
		 if (!bExecuting && bStopAsserted && (ulSemiHostOption == DCC_SEMIHOSTING))
         {
			 if (tyProcessorConfig.bCortexM3)
			 {
				 ErrRet = ML_CortexMCPUReadProc(USR_MODE, REG_PC_MASK, &ulCurrentPCValue);
			 }
			 else
			 {
				 ErrRet = ML_CPUReadProc(USR_MODE, 
					 REG_PC_MASK, 
					 &ulCurrentPCValue);
			 }
			 
			 if(ErrRet != ML_ERROR_NO_ERROR)
			 {		
				 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				 
				 if(bLogEnableOK)
				 {
					 strcpy(tyexecute.return_value,szReturnValue);
					 GetErrorMessage(szReturnValue,tyexecute.errormessage);
					 Time_out = GetTickCount();
					 RDILogFile(&tyexecute);			
				 }  
#endif
				 return ErrRet;
			 }
			 
			 if ((ulCurrentPCValue > ulGlobalDCCSHHandlerAddress) && 
				 (ulCurrentPCValue < (ulGlobalDCCSHHandlerAddress + 0x990)))
			 {
				 ErrRet = ML_SetBreakProc (ulPCValue, 
					 0x0, 
					 0x0, 
					 &tyTempBreakPointHandle);
				 
				 if(ErrRet != ML_ERROR_NO_ERROR)
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tyexecute.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tyexecute.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tyexecute);			
					 }   
#endif
					 return ErrRet;
				 }
				 
				 ErrRet = ML_ExecuteProc();
				 
				 if(ErrRet != ML_ERROR_NO_ERROR)
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tyexecute.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tyexecute.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tyexecute);			
					 }    
#endif
					 return ErrRet;
				 }
				 
				 bExecuting    = TRUE;
				 bStopAsserted = FALSE;
			 }
         }
		 
		 //If the user has chosen not to let the debugger check the status as fast as possible.
		 if (ulGlobalPollingSpeedDelay >0)
			 SleepFunc(ulGlobalPollingSpeedDelay);
		 
      }   while (bExecuting && !bGlobalReleaseControl);
	  
	  if (bGlobalReleaseControl)
      {
#ifndef __LINUX
		  
		  if(bLogEnableOK)
			  strcpy(tyexecute.return_value,"RDIError_Executing");	
#endif
		  ErrRet = RDIError_Executing;
      }
	  else
      {
		  //For DCC Semihosting if the core is stopped we have to restore the 
		  //correct PC value.
		  if (ulSemiHostOption == DCC_SEMIHOSTING)
		  {
			  //This is a SemiHosting call which stops the core
			  if (bAngelReportException)
			  {
				  if (tyProcessorConfig.bCortexM3)
				  {
					  ErrRet = ML_CortexMCPUReadProc(USR_MODE, 
						  REG_PC_MASK, 
						  &ulPCValue);
				  }
				  else
				  {
					  ErrRet = ML_CPUReadProc(USR_MODE, 
						  REG_PC_MASK, 
						  &ulPCValue);
				  }				  
				  
				  if(ErrRet != ML_ERROR_NO_ERROR)
				  {		
					  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					  
					  if(bLogEnableOK)
					  {
						  strcpy(tyexecute.return_value,szReturnValue);
						  GetErrorMessage(szReturnValue,tyexecute.errormessage);
						  Time_out = GetTickCount();
						  RDILogFile(&tyexecute);			
					  }   
#endif
					  return ErrRet;
				  }
			  }
			  
			  //This breakpoint is set if the stop button was pressed during a semihost call.
			  if (tyTempBreakPointHandle != 0x0)
			  {
				  ErrRet = ML_ClearBreakProc(tyTempBreakPointHandle);
				  
				  if(ErrRet != ML_ERROR_NO_ERROR)
				  {		
					  ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					  
					  if(bLogEnableOK)
					  {
						  strcpy(tyexecute.return_value,szReturnValue);
						  GetErrorMessage(szReturnValue,tyexecute.errormessage);
						  Time_out = GetTickCount();
						  RDILogFile(&tyexecute);			
					  }  
#endif
					  return ErrRet;
				  }
				  
				  tyCauseOfBreak = USER_HALT;
			  }     
		  }
		  
		  switch (tyCauseOfBreak)
		  {
		  /*case SINGLE_STEP:
		  memset(szReturnValue,'\0',40);
		  strcpy (szReturnValue,"Error_SINGLE_STEP");
		  ErrRet = SINGLE_STEP;
			  break;*/
		  case BP_OPCODE:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_BreakpointReached");
			  ErrRet = RDIError_BreakpointReached;
              break;
		  case ILLEGAL_OPCODE:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_UndefinedInstruction");
			  ErrRet = RDIError_UndefinedInstruction;
			  break;
		  case COB_SOFTWARE_INTERRUPT:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_SoftwareInterrupt");
			  ErrRet = RDIError_SoftwareInterrupt;
			  break;
		  case COB_PREFETCH_ABORT:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_PrefetchAbort");
			  ErrRet = RDIError_PrefetchAbort;
			  break;
		  case COB_DATA_ABORT:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_DataAbort");
			  ErrRet = RDIError_DataAbort;
			  break;
		  case COB_ADDRESS_EXCEPTION:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_AddressException");
			  ErrRet = RDIError_AddressException;
			  break;
		  case COB_IRQ:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_IRQ");
			  ErrRet = RDIError_IRQ;
			  break;
		  case COB_FIQ:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_FIQ");
			  ErrRet = RDIError_FIQ;
			  break;
		  case COB_WATCHPOINT:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_WatchpointAccessed");
			  ErrRet = RDIError_WatchpointAccessed;
			  break;            
		  case COB_BRANCH_THROUGH0:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_BranchThrough0");
			  ErrRet = RDIError_BranchThrough0;
			  break;            
		  case ANGEL_EXCEPTION_HALT:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_TargetStopped");
			  ErrRet = RDIError_TargetStopped;
			  break;            
		  default:
			  // Fall through
		  case USER_HALT:
			  memset(szReturnValue,'\0',40);
			  strcpy (szReturnValue,"RDIError_UserInterrupt");
			  ErrRet = RDIError_UserInterrupt;
			  break;
		  }
      }
	  
	  bGlobalTargetExecuting = FALSE;
#ifndef __LINUX
	  
	  
	  if(bLogEnableOK)
	  {
		  Time_out = GetTickCount();
		  RDILogFile(&tyexecute);
	  }
#endif
	  
	  DLOG("Exiting: execute()\n");
	  
	  return ErrRet;
}

/****************************************************************************
Function: step
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Single step
Date           Initials    Description
15-Aug-2007      SJ        Initial
19-Mar-2008      JCK       Step Implementation
****************************************************************************/
#ifdef _WINDOWS
int step (RDI_AgentHandle agent,
          RDI_ModuleHandle  *mh,
          RDIbool stop_others,
          unsigned ninstr,
          RDI_PointHandle *handle)
#elif __LINUX
		  int stepRDI (RDI_AgentHandle agent,
          RDI_ModuleHandle  *mh,
          RDIbool stop_others,
          unsigned ninstr,
          RDI_PointHandle *handle)
#endif
		  
{
	DLOG("Entering: step()\n");
#ifndef __LINUX
	
	Tyrdilog tystep = {"step",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
							 "0"};
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes; 
		
		strcpy(tystep.arg_name[0],"StopOthers  = ");  
		ibytes = stop_others;
		temp = tystep.arg_value[0];
		if(stop_others == 0)
			strcpy(tystep.arg_value[0],"FALSE");
		else
			strcpy(tystep.arg_value[0],"TRUE");
	}
#endif
	
	/*unreferenced formal parameter*/
	NOREF(mh);
	NOREF(handle);
	NOREF(ninstr);
	NOREF(stop_others);
	NOREF(agent);
	int               ErrRet                        = NO_ERROR;
	boolean           bExecuting                    = FALSE;
	boolean           bDataToRead                   = FALSE;
	boolean           bDataWritten                  = FALSE;
	unsigned long     ulSemiHostOption              = STANDARD_SEMIHOSTING;
	TXRXCauseOfBreak  tyCauseOfBreak                = USER_HALT;
	int               Status                        = 0;
	unsigned long     ulReadData                    = 0;
	unsigned long     ulCallBackCounter             = 0;
	TYSemiHostStruct  tySemiHostStruct              = {0};
	unsigned long     ulDCCSemihostReadData[0x100]  = {0};
	unsigned long     ulDCCSemihostWriteData[0x100] = {0};
	unsigned long     *pulDCCSemihostReadData       = NULL;
	unsigned long     *pulDCCSemihostWriteData      = NULL;
	unsigned long     i                             = 0;
	boolean           bAngelReportException         = FALSE;
	unsigned long     ulPCValue                     = 0x0;
	unsigned long     ulCurrentPCValue              = 0x0;
	unsigned long     ulCallBackFrequency           = 0x0;
	unsigned short    usNumberOfDCCWords            = 0x0;
	unsigned short    j                             = 0x0;
	RDI_PointHandle   tyTempBreakPointHandle        = 0x0;
	
	bStep = 1;        //Enabled to Check RDI call is STEP or EXECUTE.             
	
	if (tyTargetConfig.bUSBDevice)
		ulCallBackFrequency = 0x10;
	else
		ulCallBackFrequency = 0x1000;
	
	//Check whether Semihosting is enabled. PathFndr only has one console window
	//so we can only have SemiHosting or DCC support - not both.SemiHosting has a higher
	//Priority than DCC.
	ulSemiHostOption = ML_CheckSemiHostState ();
	
	//Start the core if non stop debug is not selected.
	if (tyTargetConfig.bNonStopDebugSelected == FALSE)
	{
		ErrRet = ML_StepProc();
		if(ErrRet != ML_ERROR_NO_ERROR)
		{		
			ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			
			if(bLogEnableOK)
			{
				strcpy(tystep.return_value,szReturnValue);
				GetErrorMessage(szReturnValue,tystep.errormessage);
				Time_out = GetTickCount();
				RDILogFile(&tystep);			
			}    
#endif
			return ErrRet;
		}
	}
	
	bGlobalTargetExecuting = TRUE;
	
	if (ulSemiHostOption == DCC_REALMON)
		(void)InitialiseCommsLink();
	
	//Once the agent has connected to the module and gained control this flag
	//is not needed.
	tyTargetConfig.bNonStopDebugSelected = FALSE;
	bExecutingDueToNonStop = FALSE;
#ifdef _WINDOWS
	//Set the global execution time start counter.
	ulStartExecutionTickCount = GetTickCount();
#endif
	
	bExecuting     = TRUE;
	bStopAsserted  = FALSE;
	
	do
	{
		// Check for DCC calls from core if enabled 
		if (!((fpDCCcallbackFromHost == NULL) || (fpDCCcallbackToHost == NULL)))
		{
			do
            {
				fpDCCcallbackFromHost(pvDCCHandleToBePassed, &DCCBuffer[ulNumLongsToWrite+DCCBufferCount], &Status);
				if (Status !=0)
					ulNumLongsToWrite++;
            }
			while (Status != 0);
			
			if (ulNumLongsToWrite != 0)
            {
				//Write data to the core and check for data read from core.
				ErrRet = ML_CheckDCC  (&ulReadData,
					&bDataToRead,
					&DCCBuffer[DCCBufferCount],
					&bDataWritten);
				
				if(ErrRet != ML_ERROR_NO_ERROR)
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tystep.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tystep.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tystep);			
					}    
#endif
					return ErrRet;
				}
				
				// We have data to write to the core.
				if (bDataWritten)
				{
					ulNumLongsToWrite--;
					DCCBufferCount++;
					// We we have just sent all the words in the current string, reset
					// the buffer-index back to the start of the string.
					if (ulNumLongsToWrite == 0)
						DCCBufferCount = 0;
				}
            }
			else 
            {  
				// We don't have any data to write to core check for data read from the core..
				ErrRet = ML_CheckDCC  (&ulReadData,
					&bDataToRead,
					NULL,
					NULL);
				
				if(ErrRet != ML_ERROR_NO_ERROR)
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tystep.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tystep.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tystep);			
					} 
#endif
					return ErrRet;
				}
            } 
			
			if (bDataToRead)
            {
				if(fpDCCcallbackToHost) 
					fpDCCcallbackToHost(pvDCCHandleToBePassed, ulReadData);
            }
		}
		
		// DCC Based semihosting
		if ((ulSemiHostOption == DCC_SEMIHOSTING) && !bStopAsserted)
		{
			pulDCCSemihostReadData = &ulDCCSemihostReadData[0];
			bDataToRead = TRUE;
			
			//Check whether there is a SemiHosting call to be serviced.
			ErrRet = ML_CheckDCC  (pulDCCSemihostReadData,
				&bDataToRead,
				NULL,
				NULL);
			
			if(ErrRet != ML_ERROR_NO_ERROR)
            {		
				ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				
				if(bLogEnableOK)
				{
					strcpy(tystep.return_value,szReturnValue);
					GetErrorMessage(szReturnValue,tystep.errormessage);
					Time_out = GetTickCount();
					RDILogFile(&tystep);			
				}  
#endif
				return ErrRet;
			}
			
			if (bDataToRead)
            {
				usNumberOfDCCWords = (unsigned short)(ulDCCSemihostReadData[0] >> 16);
				ulDCCSemihostReadData[0] = (ulDCCSemihostReadData[0] & 0xFFFF);
				bDataToRead = FALSE;
				
				for (j=1; j<usNumberOfDCCWords; j++)
				{
					pulDCCSemihostReadData ++;
					//Read all the data from the target.
					do
					{
						ErrRet = ML_CheckDCC  (pulDCCSemihostReadData,
							&bDataToRead,
							NULL,
							NULL);
						
						if(ErrRet != ML_ERROR_NO_ERROR)
						{		
							ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							
							if(bLogEnableOK)
							{
								strcpy(tystep.return_value,szReturnValue);
								GetErrorMessage(szReturnValue,tystep.errormessage);
								Time_out = GetTickCount();
								RDILogFile(&tystep);			
							}      
#endif
							return ErrRet;
						}                     
					}
					while (!bDataToRead);
				}
				
				if (ulDCCSemihostReadData[0] != 0)
				{
					
					pulDCCSemihostWriteData = &ulDCCSemihostWriteData[0];
#ifndef __LINUX
					//Handle semihosting here then write back data to core....
					ErrRet = HandleDCCSemihosting (&ulDCCSemihostReadData[0], pulDCCSemihostWriteData, &bAngelReportException, &ulPCValue);
					if(ErrRet != RDIError_NoError)
					{
						
						
						if(bLogEnableOK)
						{
							if(ErrRet == RDIError_SoftwareInterrupt)
								strcpy(tystep.return_value,"RDIError_SoftwareInterrupt");
							else
								strcpy(tystep.return_value,szReturnValue);
							GetErrorMessage(szReturnValue,tystep.errormessage);
							Time_out = GetTickCount();
							RDILogFile(&tystep);
						}
						
						return ErrRet;
					}
#endif
					//The Angel Semihosting call stops the core so force Status proc to be called.
					if (bAngelReportException)
					{
						ulCallBackCounter = 0x0;
					}
					
					if (ulDCCSemihostWriteData[0] > 0x0)
					{
						//Write data to the core; 
						for (i = 0; i <= ulDCCSemihostWriteData[0]; i++)
						{
							do
							{
								ErrRet = ML_CheckDCC  (&ulReadData,
									&bDataToRead,
									&pulDCCSemihostWriteData[i],
									&bDataWritten);
								
								if(ErrRet != ML_ERROR_NO_ERROR)
								{		
									ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
									
									if(bLogEnableOK)
									{
										strcpy(tystep.return_value,szReturnValue);
										GetErrorMessage(szReturnValue,tystep.errormessage);
										Time_out = GetTickCount();
										RDILogFile(&tystep);			
									}  
#endif
									return ErrRet;
								}   
							}
							while (!bDataWritten);
							
							//There should be no data to read whilst this is happening.
							//TODO: ASSERT(!bDataToRead);
							//TODO: ASSERT(bDataWritten);
						}
					}
					
					ulDCCSemihostReadData[0] = 0;
					ulDCCSemihostReadData[1] = 0;
					ulDCCSemihostReadData[2] = 0;
					ulDCCSemihostWriteData[0] = 0;
					ulDCCSemihostWriteData[1] = 0;
					ulDCCSemihostWriteData[2] = 0;
				}
            }
         }
		 
		 
		 // This determines the frequency of the callbacks to Pathfinder.
		 // This is done to speed up the DCC comms channel.
		 if ((!bDataWritten && !bDataToRead) || (ulCallBackCounter == ulCallBackFrequency) || (ulCallBackCounter == 0x0))
         {
			 //Check the status of the processor.
			 ErrRet = ML_StatusProc(&bExecuting,
				 &tyCauseOfBreak,
				 handle,
				 NULL);
			 
			 if(ErrRet != ML_ERROR_NO_ERROR)
			 {
				 if(!bExecuting)
					 bGlobalTargetExecuting = FALSE;

				 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				 
				 if(bLogEnableOK)
				 {
					 strcpy(tystep.return_value,szReturnValue);
					 GetErrorMessage(szReturnValue,tystep.errormessage);
					 Time_out = GetTickCount();
					 RDILogFile(&tystep);			
				 }   
#endif
				 return ErrRet;
			 }  
			 
			 // Check for Semihosting if the core is stopped.
			 if (  (!bExecuting) && 
				 (ulSemiHostOption == STANDARD_SEMIHOSTING))
			 {
				 ErrRet = ML_CheckForSemihosting(&tySemiHostStruct);
				 if (!((ErrRet == ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_SoftwareInterrupt)))
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tystep.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tystep.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tystep);			
					 } 
#endif
					 return ErrRet;
				 }
				 
				 if (tySemiHostStruct.bSemiHostDetected)
				 {
					 ErrRet = HandleSemiHosting (&tySemiHostStruct,
						 &tyCauseOfBreak);
					 if(ErrRet != RDIError_NoError)
					 {
#ifndef __LINUX
						 
						 if(bLogEnableOK)
						 {
							 strcpy(tystep.return_value,szReturnValue);
							 GetErrorMessage(szReturnValue,tystep.errormessage);
							 Time_out = GetTickCount();
							 RDILogFile(&tystep);			
						 }  
#endif
						 return ErrRet;
					 }
					 
					 if (tySemiHostStruct.bRestartExecution && !bStopAsserted)
					 {
						 ErrRet = ML_SemiHostGo (tySemiHostStruct.ulLinkRegisterValue, 
							 tySemiHostStruct.ulSPSRValue);
						 
						 if(ErrRet != ML_ERROR_NO_ERROR)
						 {		
							 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							 
							 if(bLogEnableOK)
							 {
								 strcpy(tystep.return_value,szReturnValue);
								 GetErrorMessage(szReturnValue,tystep.errormessage);
								 Time_out = GetTickCount();
								 RDILogFile(&tystep);			
							 }   
#endif
							 return ErrRet;
						 }
						 bExecuting = TRUE;
					 }
					 else
					 {
						 //Rewrite the PC and CPSR so the user sees that the SWI instruction has been 
						 //executed properly.
						 if (tyProcessorConfig.bCortexM3)
						 {
							 ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
								 REG_PC_MASK, 
								 &tySemiHostStruct.ulLinkRegisterValue);
						 } 
						 else
						 {
							 // R0 contains status.
							 ErrRet = ML_CPUWriteProc(USR_MODE, 
								 REG_PC_MASK, 
								 &tySemiHostStruct.ulLinkRegisterValue); 					   
						 }  
						 
						 if(ErrRet != ML_ERROR_NO_ERROR)
						 {		
							 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							 
							 if(bLogEnableOK)
							 {
								 strcpy(tystep.return_value,szReturnValue);
								 GetErrorMessage(szReturnValue,tystep.errormessage);
								 Time_out = GetTickCount();
								 RDILogFile(&tystep);			
							 }        
#endif
							 return ErrRet;
						 }
						 
						 ErrRet = ML_CPUWriteProc(USR_MODE, 
							 REG_CPSR_MASK, 
							 &tySemiHostStruct.ulSPSRValue);
						 
						 if(ErrRet != ML_ERROR_NO_ERROR)
						 {		
							 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
							 
							 if(bLogEnableOK)
							 {
								 strcpy(tystep.return_value,szReturnValue);
								 GetErrorMessage(szReturnValue,tystep.errormessage);
								 Time_out = GetTickCount();
								 RDILogFile(&tystep);			
							 }        
#endif
							 return ErrRet;
						 }
						 
						 tyCauseOfBreak = USER_HALT; 
					 }
				 }
            }
			//Callback to the debugger to process Windows messages.
			// Only do this if we are currently executing....
			if (bExecuting) 
				DebuggerCallBack ();
			
			ulCallBackCounter = 1;
			
			if (bStopAsserted && bExecuting)
            {
				ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
				if (!((ErrRet==ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_TargetStopped)))
				{		
					ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					
					if(bLogEnableOK)
					{
						strcpy(tystep.return_value,szReturnValue);
						GetErrorMessage(szReturnValue,tystep.errormessage);
						Time_out = GetTickCount();
						RDILogFile(&tystep);			
					}      
#endif
					return ErrRet;
				}
				ulCallBackCounter = 0x0;
            }
         }
		 ulCallBackCounter++;
		 
		 
		 if (!bExecuting && bStopAsserted && (ulSemiHostOption == DCC_SEMIHOSTING))
         {
			 if (tyProcessorConfig.bCortexM3)
			 {
				 ErrRet = ML_CortexMCPUReadProc(USR_MODE, REG_PC_MASK, &ulCurrentPCValue);
			 }
			 else
			 {
				 ErrRet = ML_CPUReadProc(USR_MODE, 
					 REG_PC_MASK, 
					 &ulCurrentPCValue);
			 }
			 
			 if(ErrRet != ML_ERROR_NO_ERROR)
			 {		
				 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				 
				 if(bLogEnableOK)
				 {
					 strcpy(tystep.return_value,szReturnValue);
					 GetErrorMessage(szReturnValue,tystep.errormessage);
					 Time_out = GetTickCount();
					 RDILogFile(&tystep);			
				 }   
#endif
				 return ErrRet;
			 }
			 
			 if ((ulCurrentPCValue > ulGlobalDCCSHHandlerAddress) && 
				 (ulCurrentPCValue < (ulGlobalDCCSHHandlerAddress + 0x990)))
			 {
				 ErrRet = ML_SetBreakProc (ulPCValue, 
					 0x0, 
					 0x0, 
					 &tyTempBreakPointHandle);
				 
				 if(ErrRet != ML_ERROR_NO_ERROR)
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tystep.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tystep.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tystep);			
					 }         
#endif
					 return ErrRet;
				 }
				 
				 ErrRet = ML_ExecuteProc();
				 
				 if(ErrRet != ML_ERROR_NO_ERROR)
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tystep.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tystep.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tystep);			
					 }   
#endif
					 return ErrRet;
				 }
				 
				 bExecuting    = TRUE;
				 bStopAsserted = FALSE;
			 }
         }
		 
		 //If the user has chosen not to let the debugger check the status as fast as possible.
		 if (ulGlobalPollingSpeedDelay >0)
			 SleepFunc(ulGlobalPollingSpeedDelay);
     }
	 
	 while (bExecuting && !bGlobalReleaseControl);
	 
	 if (bGlobalReleaseControl)
	 {
#ifndef __LINUX
		 
		 if(bLogEnableOK)
			 strcpy(tystep.return_value,"RDIError_Executing");	
#endif
         ErrRet = RDIError_Executing;
	 }
	 else
	 {
		 //For DCC Semihosting if the core is stopped we have to restore the 
		 //correct PC value.
		 if (ulSemiHostOption == DCC_SEMIHOSTING)
         {
			 //This is a SemiHosting call which stops the core
			 if (bAngelReportException)
			 {
				 if (tyProcessorConfig.bCortexM3)
				 {
					 ErrRet = ML_CortexMCPUReadProc(USR_MODE, 
						 REG_PC_MASK, 
						 &ulPCValue);
				 }
				 else
				 {
					 ErrRet = ML_CPUReadProc(USR_MODE, 
						 REG_PC_MASK, 
						 &ulPCValue);
				 }
				 
				 if(ErrRet != ML_ERROR_NO_ERROR)
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tystep.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tystep.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tystep);			
					 }  
#endif
					 return ErrRet;
				 }
			 }
			 
			 //This breakpoint is set if the stop button was pressed during a semihost call.
			 if (tyTempBreakPointHandle != 0x0)
			 {
				 ErrRet = ML_ClearBreakProc(tyTempBreakPointHandle);
				 
				 if(ErrRet != ML_ERROR_NO_ERROR)
				 {		
					 ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
					 
					 if(bLogEnableOK)
					 {
						 strcpy(tystep.return_value,szReturnValue);
						 GetErrorMessage(szReturnValue,tystep.errormessage);
						 Time_out = GetTickCount();
						 RDILogFile(&tystep);			
					 }    
#endif
					 return ErrRet;
				 }
				 
				 tyCauseOfBreak = USER_HALT;
			 }     
         }
		 
		 switch (tyCauseOfBreak)
         {
		 case SINGLE_STEP:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_NoError");
			 ErrRet = RDIError_NoError;
			 break;
		 case BP_OPCODE:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_BreakpointReached");
			 ErrRet = RDIError_BreakpointReached;
			 break;
		 case ILLEGAL_OPCODE:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_UndefinedInstruction");
			 ErrRet = RDIError_UndefinedInstruction;
			 break;
		 case COB_SOFTWARE_INTERRUPT:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_SoftwareInterrupt");
			 ErrRet = RDIError_SoftwareInterrupt;
			 break;
		 case COB_PREFETCH_ABORT:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_PrefetchAbort");
			 ErrRet = RDIError_PrefetchAbort;
			 break;
		 case COB_DATA_ABORT:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_DataAbort");
			 ErrRet = RDIError_DataAbort;
			 break;
		 case COB_ADDRESS_EXCEPTION:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_AddressException");
			 ErrRet = RDIError_AddressException;
			 break;
		 case COB_IRQ:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_IRQ");
			 ErrRet = RDIError_IRQ;
			 break;
		 case COB_FIQ:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_FIQ");
			 ErrRet = RDIError_FIQ;
			 break;
		 case COB_WATCHPOINT:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_WatchpointAccessed");
			 ErrRet = RDIError_WatchpointAccessed;
			 break;            
		 case COB_BRANCH_THROUGH0:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_BranchThrough0");
			 ErrRet = RDIError_BranchThrough0;
			 break;            
		 case ANGEL_EXCEPTION_HALT:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_TargetStopped");
			 ErrRet = RDIError_TargetStopped;
			 break;            
         default:
			 // Fall through
		 case USER_HALT:
			 memset(szReturnValue,'\0',40);
			 strcpy (szReturnValue,"RDIError_UserInterrupt");
			 ErrRet = RDIError_UserInterrupt;
			 break;
         }
      }
	  
	  bGlobalTargetExecuting = FALSE;
#ifndef __LINUX
	  
	  
	  if(bLogEnableOK)
	  {
		  Time_out = GetTickCount();
		  RDILogFile(&tystep);
	  }
#endif
	  
	  bStep = 0;
	  
	  DLOG("Exiting: step()\n");
	  
	  return ErrRet;
}

/****************************************************************************
Function: info
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Allows various sub functions to be called (including halt)
Date          Initials    Description
30-Jul-2007     SJ        Initial
19-Mar-2008     JR        Added info call for performance improvement(GetLoadSize)
31-Mar-2008	    RS		  Added info calls required for ARM GDB Server
****************************************************************************/
extern "C" int info(void     *handle,
                    unsigned type,
                    ARMword  *arg1,
                    ARMword  *arg2)
					
{ 
	DLOG("Entering: info()\n");
#ifndef __LINUX
	Tyrdilog tyinfo = {"info",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
							 "0"};
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		
		strcpy(tyinfo.arg_name[0],"Type  = ");
		char *temp;	   
		temp = tyinfo.arg_value[0];      
		GetInfoType(temp,type);
	}
#endif
	
	int                    ErrRet                         = RDIError_NoError;
	boolean                bCommsChannel                  = FALSE;
	boolean                bThumb                         = FALSE;
	RDIProperty_GroupDesc *tyTempGroupDescPointer         = NULL;
	RDIProperty_Desc      *tyTempDescPointer               = NULL;
	unsigned long          ulTemp                         = 0x0;;
	RDI_PointHandle        BPHandle;
	boolean                bExecuting                     = FALSE;
	TXRXCauseOfBreak       tyCauseOfBreak;
	
	bStopAsserted = FALSE;
	
	if (bGlobalTargetExecuting &&
		(!((type == RDISignal_ReleaseControl) || (type == RDISignal_Stop))))
	{
#ifndef __LINUX
		
		if(bLogEnableOK)
		{
			strcpy(tyinfo.return_value,"RDIError_TargetRunning"); 
            GetErrorMessage(szReturnValue,tyinfo.errormessage);
			Time_out = GetTickCount();
			RDILogFile(&tyinfo);
		}
#endif
		return RDIError_TargetRunning;
	}
	
	switch(type)
	{
	default:
	   case RDIInfo_DownLoad:
	   case RDIInfo_AllowedWhilstExecuting:
	   case RDIInfo_IOBitsSupported:
	   case RDIInfo_GetIOBits:
	   case RDIInfo_SetIOBits:
#ifndef __LINUX
		   
		   if(bLogEnableOK)
			   strcpy (tyinfo.return_value,"RDIError_UnimplementedMessage");
#endif
		   
           ErrRet = RDIError_UnimplementedMessage;
		   break;
		   
	   case RDIInfo_SemiHosting:
	   case RDIInfo_SemiHostingSetARMSWI:
	   case RDIInfo_SemiHostingGetARMSWI:
	   case RDIInfo_SemiHostingSetThumbSWI:
	   case RDIInfo_SemiHostingGetThumbSWI:
	   case RDIInfo_SafeNonVectorAddress:
	   case RDIInfo_CanTargetExecute:
	   case RDIInfo_CoPro:	
	   case RDIInfo_DescribeCoPro:	
	   case RDIInfo_Icebreaker:
	   case RDISemiHosting_DCC:
	   case RDIInfo_CanForceSystemReset  : 
           break;
		   
	   case RDISemiHosting_SetVector:
	   case RDISemiHosting_GetVector:
	   case RDISemiHosting_SetARMSWI:
	   case RDISemiHosting_GetARMSWI:
	   case RDISemiHosting_GetThumbSWI:
	   case RDISemiHosting_SetThumbSWI:
	   case RDISemiHosting_SetState:
	   case RDISemiHosting_GetState:
	   case RDISemiHosting_SetDCCHandlerAddress:
	   case RDISemiHosting_GetDCCHandlerAddress:
	   case RDISemiHosting_SetCompilerType:
		   if (!bExecutingDueToNonStop) 
		   {
			   ErrRet = ML_InfoProc (type, arg1, arg2);
			   
			   
			   //Check for Errors before updating the globals
			   if (ErrRet == RDIError_NoError)
               {
				   //Store the SemiHosting type.
				   if (type == RDISemiHosting_SetState)
					   ulGlobalSemiHostingType = *arg1;
				   
				   //Store the Address of the DCC Semihosting vector.
				   if (type == RDISemiHosting_SetDCCHandlerAddress)
					   ulGlobalDCCSHHandlerAddress = *arg1;
               }
			   
			   if ((type == RDISemiHosting_SetState) && (ErrRet == RDIError_CantSetPoint))
               {
#ifndef __LINUX
				   MessageBox(NULL,szCannotSetSemihost,"Message",MB_OK);
#endif
				   if(bGDBServer)
					   printf("Cannot set Semihosting breakpoint. No more ARM EmbeddedICE hardware resources are available. Please free existing hardware resources (remove breakpoints or watchpoints) and retry\n");
				   ErrRet = RDIError_NoError;
               }
		   }
		   break;
	   case RDIInfo_Modules:
		   (void)EnumerateModulesinTarget(arg1, arg2);
		   break;
		   
	   case RDISignal_Stop :
		   //We do not actually stop the core now - just flag to the execute function
		   //to stop at the next debugger callback.
		   bStopAsserted = TRUE;
		   break;
		   
	   case RDIInfo_ForceSystemReset :
		   ulNumLongsToWrite   = 0;
		   DCCBufferCount      = 0;
		   
		   ErrRet = ML_InfoProc(RDIInfo_ForceSystemReset,
			   arg1,
			   arg2);
		   if(ErrRet != ML_ERROR_NO_ERROR)
		   {
			   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			   
			   if(bLogEnableOK)
			   {
				   strcpy(tyinfo.return_value,szReturnValue);
				   Time_out = GetTickCount();
				   RDILogFile(&tyinfo);
			   }
#endif
			   return ErrRet;
		   }
		   
		   if (tyTargetConfig.bUseNTRST)
		   {
			   ErrRet = ML_ResetTAPController();
			   if(ErrRet != ML_ERROR_NO_ERROR)
               {
				   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
				   
				   if(bLogEnableOK)
				   {
					   strcpy(tyinfo.return_value,szReturnValue);
					   Time_out = GetTickCount();
					   RDILogFile(&tyinfo);
				   }        
#endif
				   return ErrRet;
               }
		   }
		   
		   ErrRet = ML_Initialise_Debugger(&tyTargetConfig);
		   if(ErrRet != ML_ERROR_NO_ERROR)
		   {
			   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			   
			   if(bLogEnableOK)
			   {
				   strcpy(tyinfo.return_value,szReturnValue);
				   GetErrorMessage(szReturnValue,tyinfo.errormessage);
				   Time_out = GetTickCount();
				   RDILogFile(&tyinfo);
			   }   
#endif
			   return ErrRet;
		   }
		   
		   ErrRet = ML_StatusProc(&bExecuting,
			   &tyCauseOfBreak,
			   &BPHandle,
			   NULL);
		   if(ErrRet != ML_ERROR_NO_ERROR)
		   {
			   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			   
			   if(bLogEnableOK)
			   {
				   strcpy(tyinfo.return_value,szReturnValue);
				   GetErrorMessage(szReturnValue,tyinfo.errormessage);
				   Time_out = GetTickCount();
				   RDILogFile(&tyinfo);
			   }         
#endif
			   return ErrRet;
		   }
		   
		   ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
		   if (!((ErrRet==ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_TargetStopped)))
		   {
			   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
			   
			   if(bLogEnableOK)
			   {
				   strcpy(tyinfo.return_value,szReturnValue);
				   GetErrorMessage(szReturnValue,tyinfo.errormessage);
				   Time_out = GetTickCount();
				   RDILogFile(&tyinfo);
			   }   
#endif
			   return ErrRet;
		   }
		   break;
		   
		   
	   case RDIInfo_Points:
		   // OpellaXD is advertising that it supports the following 
		   // Watchpoints  / Breakpoints 
		   *arg1  = RDIPointCapability_Status;
		   *arg1 |= RDIPointCapability_Comparison;
		   *arg1 |=(RDIWatch_ByteRead  <<2);
		   *arg1 |=(RDIWatch_HalfRead  <<2);
		   *arg1 |=(RDIWatch_WordRead  <<2);
		   *arg1 |=(RDIWatch_ByteWrite <<2);
		   *arg1 |=(RDIWatch_HalfWrite <<2);
		   *arg1 |=(RDIWatch_WordWrite <<2);
		   *arg1 |= RDIPointCapability_Mask;
		   break;
		   
	   case RDIInfo_Step:
		   *arg1  = RDIStep_Single; 
		   break;
		   
	   case RDIInfo_Target:
		   
		   if ((unsigned int ) handle == TARGET_MODULE_HANDLE)
		   {
			   *arg1  = RDITarget_HW;
			   *arg1  |= RDITarget_CanInquireLoadSize;
			   
			   // Check the Target properties.
			   ML_GetProcessorInfo (&bThumb, &bCommsChannel);
			   
			   if (bThumb)
				   *arg1 |= RDITarget_Code16;
			   
			   if (bCommsChannel)
				   *arg1 |= RDITarget_HasCommsChannel;
			   
			   *arg1 |= RDITarget_CanDebugNonStop;
			   
		   }
		   else
			   if ((unsigned int ) handle == AGENT_HANDLE_VALUE)
			   {
				   *arg1  = RDITarget_NonExecutableProcessor;
				   *arg1 |= RDITarget_HW;
				   *arg1 |= RDITarget_CanDebugNonStop;
				   *arg1 |= RDITarget_CanInquireLoadSize;
			   }
			   else
			   {
				   *arg1 = 0x0;
				   //TODO: ASSERT_NOW();
			   }
			   
			   *arg2=(unsigned int)handle;
			   break;
#ifndef __LINUX
	   case RDIInfo_AshlingVersion:
		   // This will be set if talking to PFW debugger
		   (void)GetSoftwareVersions ((TyAshlingVersion*)arg1);
		   break;
#endif
		   
	   case RDIInfo_StatusProc:
		   *arg1=ulGlobalExecuting;
		   *arg2=tyGlobalCauseOfBreak;
           break;
		   
	   case RDIInfo_TargetReset:
		   ErrRet = ML_InfoProc(RDIInfo_ForceSystemReset, NULL, NULL);
		   ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));            
		   break;
		   
	   case RDIInfo_TargetHalt:
		   ErrRet = ML_InfoProc(RDISignal_Stop, NULL, NULL);
		   ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr (ErrRet));            
		   break;
		   
	   case RDIInfo_Device:
		   ErrRet = ML_GetDeviceInfo(&tyDeviceInfo);
		   *arg1=tyDeviceInfo.ulDiskwareVersion;
		   *arg2=tyDeviceInfo.ulFirmwareVersion;	
		   break;
		   
	   case RDIInfo_DisableGUI:
		   
		   bGDBServer=1;
		   break;
		   
	   case RDICommsChannel_ToHost:
		   (void)Register_CommsChannelToHost((RDICCProc_ToHost *)arg1,
			   (void *)arg2);
		   break;
		   
	   case RDICommsChannel_FromHost:
		   (void)Register_CommsChannelFromHost((RDICCProc_FromHost *)arg1,
			   (void *)arg2);
		   break;
		   
	   case RDIInfo_CP15CacheSelection: 
		   if (!CL_QueryCP15Cache())
		   {
#ifndef __LINUX
			   
			   if(bLogEnableOK)
				   strcpy (tyinfo.return_value,"RDIError_UnimplementedMessage");  
#endif
			   ErrRet = RDIError_UnimplementedMessage;
		   }
		   break;
		   
	   case RDIInfo_SetCP15CacheSelected:  
		   CL_SetCP15CacheSelection(*arg1);
		   break;
		   
	   case RDIInfo_GetCP15CacheSelected:  
		   CL_GetCP15CacheSelection(arg1);
		   break;
		   
	   case RDIInfo_CP15CurrentMemoryArea: 
		   if (!CL_QueryCP15MemoryArea())
		   {
#ifndef __LINUX
			   
			   if(bLogEnableOK)
				   strcpy (tyinfo.return_value,"RDIError_UnimplementedMessage");
#endif
			   ErrRet =  RDIError_UnimplementedMessage;
		   }
		   break;
		   
	   case RDIInfo_SetCP15CurrentMemoryArea: 
		   CL_SetCP15MemoryArea(*arg1);
		   break;
		   
	   case RDIInfo_GetCP15CurrentMemoryArea: 
		   CL_GetCP15MemoryArea(arg1);
		   break;
		   
	   case RDIPointStatus_Break:
		   ErrRet = ML_BreakpointInquiry (arg1, arg2);
		   
		   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));            
		   break;
		   
	   case RDIPointStatus_Watch:
		   ErrRet = ML_WatchpointInquiry (arg1, arg2);
		   
		   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));            
		   break;
		   
	   case RDIIcebreaker_SetLocks:
		   ML_SetLockedPoints (*arg1);
		   break;
		   
	   case RDIIcebreaker_GetLocks:
		   ML_GetLockedPoints (arg1);
		   break;
		   
	   case RDISet_Cmdline:
		   ErrRet = GetElfHeaderInfoAndSetPC((char *) arg1);
#ifndef __LINUX
		   
		   if(bLogEnableOK)
		   {
			   if(ErrRet == RDIError_NoSuchHandle)					
               {
				   strcpy(tyinfo.return_value,"RDIError_NoSuchHandle");	
				   GetErrorMessage(szReturnValue,tyinfo.errormessage);
               }
		   }
#endif
		   //Copy the command line to the global storage .
		   //This is needed for semihosting get_cmdLine calls.
		   
		   // Using 0 as the minimum buffer length causes havoc
		   //strcpy (csCommandLine.GetBuffer(strlen((char*) arg1) + 0x1), (char*) arg1);
		   /*commented for testing*/
		   //strcpy (pszCommandLine, (char*) arg1);    
		   /*commented for testing*/
		   //csCommandLine.ReleaseBuffer();
		   break;
		   
	   case RDIInfo_SetTopMem:
		   ulGlobalTopOfMemory = *arg1;
		   ML_SetTopOfMemory (*arg1);
		   break;
		   
	   case RDIInfo_GetSafeNonVectorAddress:
		   ML_GetSafeVectorAddress (arg1);
		   break;
		   
	   case RDIInfo_SetSafeNonVectorAddress:
		   ErrRet = ML_SetSafeNonVectorAddress (*arg1);
		   
		   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));           
		   break;
		   
	   case RDIInfo_MovableVectors:
		   // Opella supports movable vector tables. We need to
		   // check whether the core supports them.
		   if (!ML_QueryMovableVector())
		   {
#ifndef __LINUX
			   
			   if(bLogEnableOK)					
				   strcpy(tyinfo.return_value,"RDIError_UnimplementedType");
#endif
			   ErrRet = RDIError_UnimplementedType;
		   }
		   break;
		   
	   case RDIInfo_GetVectorAddress:
		   ErrRet = ML_GetVectorAddress (arg1);
		   
		   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));         
		   break;
		   
	   case RDIVector_Catch:
		   if (!bExecutingDueToNonStop)
		   {
			   ErrRet = ML_SetupVectorCatch (arg1);
			   
			   if (ErrRet ==  ML_ERROR_NO_ERROR)
               {
				   //Keep a record of the vector catch.
				   ulGlobalCurrentVectorCatch = *arg1;
               }
			   ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));            
		   }
		   break;
		   
	   case RDIErrorP:
		   *arg1 = *arg1;
		   break;
		   
	   case RDIInfo_GetARM9RestartCodeAddress   :
		   ErrRet = CL_GetARM9RestartAddress((unsigned long *) arg1);
#ifndef __LINUX
		   
		   if(bLogEnableOK)
		   {			
			   if(ErrRet == ML_Error_UnimplementedType)
				   strcpy(tyinfo.return_value,"RDIError_UnimplementedType");
		   }
#endif
		   break;
		   
	   case RDIInfo_SetARM9RestartCodeAddress   :
		   ErrRet = CL_SetARM9RestartAddress((unsigned long *) arg1);
#ifndef __LINUX
		   
		   if(bLogEnableOK)
		   {			
			   if(ErrRet == ML_Error_UnimplementedType)
				   strcpy(tyinfo.return_value,"RDIError_UnimplementedType");
		   }
#endif
		   break;
		   
	   case RDIInfo_RequestCoProDesc:
		   ErrRet = GetCoProcessorDesc (arg1, (Co_ProcessorDescStructure *)arg2);
#ifndef __LINUX
		   
		   if(bLogEnableOK)
		   {
			   if(ErrRet == RDIError_UnknownCoPro)
				   strcpy(tyinfo.return_value,"RDIError_UnknownCoPro");
		   }
#endif
		   break;
		   
	   case RDIInfo_SetLog:
		   break;
		   
	   case RDIInfo_Log:
		   *arg1 = FALSE;
		   break;
		   
	   case RDIInfo_GetLoadSize:
		   *arg1 = 0x10000; //64kB
		   break;
		   
	   case RDIInfo_InfoAllowedWhilstExecuting:
		   ErrRet = RDIError_NotAllowedWhilstExecuting;
#ifndef __LINUX
		   
		   if(bLogEnableOK)			
			   strcpy(tyinfo.return_value,"RDIError_NotAllowedWhilstExecuting");			
#endif
		   break;
		   
	   case RDIInfo_CustomLoad:
		   /*if (strcmp(tyTargetConfig.szDebuggerName,"AXD") == 0)
		   {
				ErrRet = RDIError_UnimplementedMessage;
		   }*/		   
		   break;
		   
	   case RDIInfo_LoadStart:
		   tyDownload.pucBufferAllocated =  (unsigned char *)malloc(MAX_BUFFER_ALLOCATED);
		   
		   if (tyDownload.pucBufferAllocated != NULL)
		   {
			   tyDownload.bValid = FALSE;
			   tyDownload.bBufferAllocated = TRUE; 
			   tyDownload.tyMemStruct[0].pucBlockStartBuffer = tyDownload.pucBufferAllocated;
			   tyDownload.pucEndOfBufferAllocated = (tyDownload.pucBufferAllocated + MAX_BUFFER_ALLOCATED);
		   }
		   else
			   
			   
		   {
#ifndef __LINUX
			   if(bLogEnableOK)
				   strcpy(tyinfo.return_value,"RDIError_Failed_To_Allocate_PC_Memory");				
#endif
			   ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
		   }
		   
		   tyDownload.usNoOfBlocks = 0;
		   break;
		   
	   case RDIInfo_LoadEnd:
		   if(tyDownload.bValid)
		   {
			   //Now we have all the memory writes stored in the buffer check to see if we can combine any of them.
			   ErrRet = OptimiseAndWriteMemoryBlock();
			   if(ErrRet != RDIError_NoError)
			   {
				   if(ErrRet == RDIError_Failed_To_Allocate_PC_Memory)
				   {
#ifndef __LINUX
					   
					   if(bLogEnableOK)                  
						   strcpy (tyinfo.return_value,"RDIError_Failed_To_Allocate_PC_Memory");
#endif
				   }
#ifndef __LINUX
				   
				   if(bLogEnableOK)
				   {
					   Time_out = GetTickCount();
					   RDILogFile(&tyinfo);
				   }
#endif
				   return CheckForFatalErrorMessage(ErrRet);
			   }
			   
			   // free the memory allocated in Optimise
			   free (tyOptDownload.pucBufferAllocated);
			   
			   //We have not set up the processor state. Let the Debugger set the PC and mode.
			   *arg1 = FALSE;
		   }
		   
		   // free the memory allocated in LoadStart
		   free (tyDownload.pucBufferAllocated);
		   
		   tyDownload.bBufferAllocated = FALSE; 
		   break; 
		   
	   case RDIInfo_Properties:
		   break;
		   
	   case RDIProperty_RequestGroups:
		   if (handle == (void *)0xABABABAB)
		   {
			   tyTempGroupDescPointer = (RDIProperty_GroupDesc *) arg2;
			   if (arg1 == RDIPropertyGroup_SuperGroup)
               {
				   tyTempGroupDescPointer->id = RDIPropertyGroup_SuperGroup; 
				   strcpy (tyTempGroupDescPointer->name, "Super Group");
				   strcpy (tyTempGroupDescPointer->description, "Group containing all properties");
				   tyTempGroupDescPointer->numProperties = 1;
				   tyTempGroupDescPointer->numSubGroups = 0;
               }
		   }
		   else
		   {
#ifndef __LINUX
			   
			   if(bLogEnableOK)
				   strcpy(tyinfo.return_value,"RDIError_NoSuchHandle");
#endif
			   ErrRet = RDIError_NoSuchHandle;
		   }
		   break;
		   
	   case RDIProperty_RequestDescriptions:
		   tyTempDescPointer = (RDIProperty_Desc *) arg2;
		   strcpy (tyTempDescPointer->name, "use_only_HW_breakpoints");
		   strcpy (tyTempDescPointer->description, "If set indicates Opella will only set Hardware Breakpoints (Maximum of 2).");
		   tyTempDescPointer->id = 0x0; 
		   tyTempDescPointer->asString = 0x0; 
		   tyTempDescPointer->maxLen = 0x0; 
		   tyTempDescPointer->asNumeric = 0x0; 
		   tyTempDescPointer->isSigned = 0x0; 
		   tyTempDescPointer->width = 0x0; 
		   tyTempDescPointer->readOnly = 0x20; 
		   tyTempDescPointer->monotonicIncreasing = 0x0; 
		   tyTempDescPointer->traceable = 0x0; 
		   tyTempDescPointer->display = RDIProperty_PDT_Standard; 
		   
		   break;
		   
	   case RDIProperty_GetAsNumeric:
		   switch (*(unsigned long *) &arg1)
		   {
		   case 0x0:
               ML_GetUseOnlyHardwareBreakPoints (arg2);
               break;
		   default:
#ifndef __LINUX
			   
			   if(bLogEnableOK)
				   strcpy(tyinfo.return_value,"RDIError_UnimplementedMessage");
#endif
               ErrRet = RDIError_UnimplementedMessage;
               break;
		   }
		   break;
		   
		   case RDIProperty_SetAsNumeric  :
			   switch (*(unsigned long *) &arg1)
			   {
			   case 0x0:
				   ML_SetUseOnlyHardwareBreakPoints (*arg2);
				   
				   //Check whether the value was set correctly.
				   ML_GetUseOnlyHardwareBreakPoints (&ulTemp);
				   
				   if ((*arg2 == 0x1) && ulTemp != 0x1)
				   {
#ifndef __LINUX
					   MessageBox(NULL,"There are more than two breakpoints / watchpoints already set on the core. \nTo enable this function remove all your existing breakpoints / watchpoints.",OPXDCAPTION,MB_OK|MB_ICONWARNING);
#endif
					   if(bGDBServer)
						   printf("There are more than two breakpoints / watchpoints already set on the core. \nTo enable this function remove all your existing breakpoints / watchpoints");
				   }
				   break;
			   default:
#ifndef __LINUX
				   
				   if(bLogEnableOK)
					   strcpy(tyinfo.return_value,"RDIError_UnimplementedMessage");
#endif
				   ErrRet = RDIError_UnimplementedMessage;
				   break;
			   }
			   break;
			   
			case RDI_GetPropertyDisplayDetails:
			   break;
   
			case RDISignal_ReleaseControl:
			   bGlobalReleaseControl = TRUE;
			   break;

			case RDIInfo_GetHardwareRes:
				ML_GetHardwareResInfo(arg1, arg2);
				break;
      }
#ifndef __LINUX
	  
	  if(bLogEnableOK)
	  {
		  Time_out = GetTickCount();
		  RDILogFile(&tyinfo);
	  }
#endif
	  DLOG("Exiting: info()\n");
	  
	  return ErrRet;
}

/****************************************************************************
Function: pointinquiry
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Allows details of breakpoints/watchpoints to be checked.
Date           Initials    Description
17-Aug-2007      SJ        Initial
****************************************************************************/
int pointinquiry( RDI_ModuleHandle  handle,
				 ARMword           *address,
				 unsigned          type,
				 unsigned          DataType,
				 ARMword           *bound)
{
	DLOG("Entering: pointinquiry()\n");
#ifndef __LINUX
	
	
	Tyrdilog typointinquiry = {"pointinquiry",
		"0","0","0",
		"0","0","0",
		"RDIError_NoError",
		"0"};
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);
		char *temp;  
		unsigned ibytes;
		
		strcpy(typointinquiry.arg_name[0],"Addr  = 0x");  
		ibytes = *address;
		temp = typointinquiry.arg_value[0];
		itoa(ibytes,temp,16);	
		
		strcpy(typointinquiry.arg_name[1],"Type  =  ");
		temp = typointinquiry.arg_value[1];      
		GetBreakType(temp,type);
		
		strcpy(typointinquiry.arg_name[2],"Data Type  = ");
		temp = typointinquiry.arg_value[2];      
		GetDataType(temp,type);
	}
#endif
	
	int ErrRet = RDIError_NoError;
	
	/*unreferenced formal parameter*/
	NOREF(handle);
	
	ErrRet = ML_PointInquiryProc(address,type,DataType,bound);
	
	ErrRet = CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
#ifndef __LINUX
	
	if(bLogEnableOK)
	{
		strcpy(typointinquiry.return_value,szReturnValue);
		if(ErrRet != RDIError_NoError)
			GetErrorMessage(szReturnValue,typointinquiry.errormessage);
		Time_out = GetTickCount();
		RDILogFile(&typointinquiry);
	}
#endif
	
	DLOG("Exiting: pointinquiry()\n");
	
	return ErrRet;
}

/****************************************************************************
Function: addconfig
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Allows a new configuration to be added
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
int addconfig( RDI_AgentHandle   agent,
			  unsigned32        nbytes)
{
	DLOG("Entering: addconfig()\n");   
	
	/*
	No implementation yet.
	*/
	
	/*unreferenced formal parameter*/
	NOREF(agent);
	NOREF(nbytes);   
	
	DLOG("Exiting: addconfig()\n");
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: loadconfigdata
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Allows configuration data to be loaded
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
int loadconfigdata(  RDI_AgentHandle   agent,
				   unsigned32        nbytes,
				   char const        *data)
{   
	DLOG("Entering: addconfig()\n");
	
	/*
	No implementation yet.
	*/
	
	/*unreferenced formal parameter*/
	NOREF(agent);
	NOREF(nbytes);
	NOREF(data);
	
	
	DLOG("Exiting: addconfig()\n");
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: selectconfig
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Allows configuration to be selected
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
int selectconfig( RDI_AgentHandle      Handle,
				 RDI_ConfigAspect     aspect,
				 char const           *Name,
				 RDI_ConfigMatchType  matchtype,
				 unsigned             versionreq,
				 unsigned             *versionp)
{   
	
	
/*
No implementation yet.
	*/
	
	/*unreferenced formal parameter*/
	NOREF(Handle);
	NOREF(aspect);
	NOREF(Name);
	NOREF(matchtype);
	NOREF(versionreq);
	NOREF(versionp);
	
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: drivernames
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: 
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
RDI_NameList const * drivernames(RDI_AgentHandle agent)
{  
	
	/*unreferenced formal parameter*/
	NOREF(agent);
	
	return 0;
}

/****************************************************************************
Function: cpunames
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: 
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
RDI_NameList const * cpunames(RDI_AgentHandle agent)
{   
	
	/*unreferenced formal parameter*/
	NOREF(agent);
	
	return 0;
}

/****************************************************************************
Function: errmess
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Allows details of the last error message to be checked
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
int errmess(RDI_AgentHandle   agent,
            char              *buf,
            int               buflen,
            int               errnum)
{
#ifndef __LINUX
	Tyrdilog tyerrmess = {"errmess",
		"0","0","0",
		"0","0","0",
		"0",
		"0"};
	
	
	
	if(bLogEnableOK)
	{
		Time_in = GetTickCount();
		GetLocalTime(&st_in);		
	}
#endif
	
	/*unreferenced formal parameter*/
	NOREF(agent);
	
	switch (errnum)
	{
	case RDIError_Reset:
		strncpy(buf, "Target reset detected", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_Reset");		
#endif
		break;
		
	case RDIError_TargetRunning:
		strncpy(buf, "Option not available while target is running", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_TargetRunning");
#endif
		break;
		
	case RDIError_UnableToInitialise:
	case RDIError_Comms_Opella:
	case RDIError_TargetBroken_Opella:
	case RDIError_TargetBroken:
		strncpy(buf, "Cannot communicate with target. Ensure that Opella-XD is configured correctly, connected to the target and that the target is powered", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_TargetBroken");
#endif
		break;
		
	case RDIError_LoopBack_Opella:         
		strncpy(buf, "The PC is not able to communicate with Opella-XD. Please ensure it is connected to your PC", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_LoopBack_Opella");   
#endif
		break;
		
	case RDIError_InitTarget_Opella:
		strncpy(buf, "Unable to communicate with your target device (can communicate with Opella-XD). Ensure that Opella-XD is configured correctly, connected to the target and that the target is powered", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_InitTarget_Opella");
#endif
		break;
		
	case RDIError_BadPointType:
		strncpy(buf, "Internal error. The requested point type is invalid", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_BadPointType");
#endif
		break;
		
	case RDIError_IncorrectProcType:
		strncpy(buf, "The target device detected is not the same as the device selected", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_IncorrectProcType");
#endif
		break;
		
	case RDIError_CantSetPoint:
		strncpy(buf, "Cannot set breakpoint/watchpoint as there are no more hardware resources available. Please clear existing breakpoint/watchpoints and retry", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_CantSetPoint");
#endif
		break;
		
	case RDIError_NoSuchPoint:
		strncpy(buf, "Cannot clear breakpoint/watchpoint (as it has already been cleared)", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_NoSuchPoint");
#endif
		break;
		
	case RDIError_PointInUse:
		strncpy(buf, "Cannot set breakpoint/watchpoint as one already exists at this address", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_PointInUse");
#endif
		break;
		
	case RDIError_Dw1FileFailed:
		strncpy(buf, "Failed to find Opella-XD diskware file", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_Dw1FileFailed");
#endif
		break;
		
	case RDIError_Fpga1FileFailed:
		strncpy(buf, "Failed to find Opella-XD FPGAware file", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_Fpga1FileFailed");
#endif
		break;
		
	case RDIError_NoToolConfig:
		strncpy(buf, "No tool configuration present", buflen);
#ifndef __LINUX
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_NoToolConfig");
#endif
		break;
		
	case RDIError_UnknownDiskware:
		strncpy(buf, "Unknown diskware error occurred", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_UnknownDiskware");
#endif
		break;
		
	case RDIError_IncorrectUSBDriver:
		strncpy(buf, "The loaded Opella XD USB drivers are out of date. Please upgrade with the latest versions supplied (located in USB subdirectory of Ashling software installation)", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_IncorrectUSBDriver");
#endif
		break;
		
	case RDIError_CannotWriteDCCHandler:
		strncpy(buf, "Cannot write the DCC semi-hosting handler to target memory. Please ensure there is adequate RAM at the specified address", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_CannotWriteDCCHandler");
#endif
		break;
		
	case RDIError_CannotWriteToVectorTable:
		strncpy(buf, "Cannot write to the vector table in target memory. Please check that there is RAM at addresses 0x08 and 0x28", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_CannotWriteToVectorTable");
#endif
		break;
		
	case RDIError_FPGA_File_Not_Found:
		strncpy(buf, "Failed to find Opella-XD FPGAware file", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_FPGA_File_Not_Found");
#endif
		break;
		
	case RDIError_Diskware_File_Not_Found:
		strncpy(buf, "Failed to find Opella-XD diskware file", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_Diskware_File_Not_Found");
#endif
		break;
		
	case RDIError_Incorrect_Diskware_File:     
		strncpy(buf, "Failed to find the correct Opella-XD diskware file", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_Incorrect_Diskware_File");
#endif
		break;
		
	case RDIError_Failed_To_Allocate_PC_Memory:
		strncpy(buf, "Internal error. Failed to allocate adequate PC memory", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_Failed_To_Allocate_PC_Memory");
#endif
		break;
		
	case RDIError_DataAbort_Writing_Cache_Clean:
		strncpy(buf, "A data abort occurred while downloading the data cache invalidation routine. Please ensure that the specified Data cache clean start address points to a valid memory location with at least 128 bytes of available memory", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_DataAbort_Writing_Cache_Clean");
#endif
		break;
		
	case RDIError_NoRTCKSupport:
		strncpy(buf, "No RTCK response from the target. Please reconfigure Opella-XD RDI driver to use a fixed frequency.", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_NoRTCKSupport");
#endif
		break;
		
	case RDIError_InvalidSerialNumber:
		sprintf(buf,"Invalid serial number (%s) in current configuration file. This does not match the currently connected Opella-XD. Please reconfigure Opella-XD RDI Driver.",tyTargetConfig.szOpellaXDSerialNumber);
		//strncpy(buf, "Serial Number do not match. Please recheck the configuration", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_InvalidSerialNumber");
#endif
		break;
	case RDIError_SoftInitialiseError:
		strncpy(buf, "Failed to initialise target. Please reconfigure the Opella-XD RDI driver appropriately for your target (trouble shoot using the Diagnostics option).",buflen);
#ifndef __LINUX
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"RDIError_SoftInitialiseError");
#endif
		break;
	default:
		strncpy(buf, "Unknown error occurred.", buflen);
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (tyerrmess.arg_value[0],"Unknown_Error");
#endif
		break;
      }
#ifndef __LINUX
	  
	  if(bLogEnableOK)
	  {
		  Time_out = GetTickCount();
		  RDILogFile(&tyerrmess);
	  }
#endif
	  
	  return buflen;
}

/****************************************************************************
Function: loadagent
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Loads a particular agent.
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
int loadagent( RDI_AgentHandle   Handle,
			  ARMword           dest,
			  unsigned32        size,
			  RDI_GetBufferProc *getb,
			  RDI_GetBufferArg  *getbarg)
{
	
/*
No implementation yet.
	*/
	
	/*unreferenced formal parameter*/
	NOREF(Handle);
	NOREF(dest);
	NOREF(size);
	NOREF(getb);
	NOREF(getbarg);
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: targetisdead
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Check if the current target is alive
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
int targetisdead(RDI_AgentHandle agent)
{
	
/*
No implementation yet.
	*/
	
	/*unreferenced formal parameter*/
	NOREF(agent);
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: Register_SemihostingCallBacks
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: store PFP callback function pointer
Date        Initials    Description
16-Oct-2007      SJ         Initial
****************************************************************************/
int Register_SemiHostingCallBacks (RDI_HostosInterface const *SemiHostingcallback) 
{
	
	SH_SetSemiHostingFunctionCalls (SemiHostingcallback);
	
	return RDIError_NoError; 
}

/****************************************************************************
Function: OptimiseAndWriteMemoryBlock
Engineer: Suraj S
Input: NONE
Output: TyError - RDI Layer Error code
Description: Organises the code download into the largest sequential blocks.
Date          Initials    Description
24-Jul-2007      SJ        Initial
****************************************************************************/
TyError OptimiseAndWriteMemoryBlock(void)
{
	unsigned long  ulLowestAddress         = 0xFFFFFFFF;
	unsigned short i                       = 0x0;
	unsigned long  j                       = 0x0;
	unsigned short usLocalBlockIndex       = 0x0;
	unsigned long  ulCurrentBlockIndex     = 0x0;
	unsigned long  ulNextBlockIndex        = 0x0;
	unsigned short usBlockNo               = 0x0;
	TyError ErrRet                         = RDIError_NoError;
	
	tyOptDownload.usNoOfBlocks = 0;
	tyOptDownload.pucBufferAllocated =  (unsigned char *)malloc(MAX_BUFFER_ALLOCATED);
	if (tyOptDownload.pucBufferAllocated == NULL)
		return RDIError_Failed_To_Allocate_PC_Memory;
	
	if (tyOptDownload.pucBufferAllocated != NULL)
	{
		tyOptDownload.bBufferAllocated = TRUE; 
		tyOptDownload.tyMemStruct[0].pucBlockStartBuffer = tyOptDownload.pucBufferAllocated;
		tyOptDownload.tyMemStruct[0].usNextBlockIndex    = 0x1;
	}
	tyOptDownload.usNoOfBlocks = 0;
	
	//Search the buffer for the lowest Address.
	for (j = 0; j < tyDownload.usNoOfBlocks; j++)
	{
		i = 0x0;
		ulLowestAddress = 0xFFFFFFFF;
		
		do
		{
			if ((tyDownload.tyMemStruct[i].ulBlockStartAddress < ulLowestAddress) 
				&& (tyDownload.tyMemStruct[i].bBlockValid))
            {
				ulLowestAddress = tyDownload.tyMemStruct[i].ulBlockStartAddress;
				usLocalBlockIndex = i;
            }
			i++;
		}
		while (i <= tyDownload.usNoOfBlocks);
		
		//Remove the lowest block into the new structure
		tyDownload.tyMemStruct[usLocalBlockIndex].bBlockValid = FALSE;
		
		
		//Copy this block into the optimised structure.
		memcpy(tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].pucBlockStartBuffer, 
			tyDownload.tyMemStruct[usLocalBlockIndex].pucBlockStartBuffer,
			tyDownload.tyMemStruct[usLocalBlockIndex].ulBlockSize);
		
		tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].pucBlockEndBuffer     = tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].pucBlockStartBuffer + tyDownload.tyMemStruct[usLocalBlockIndex].ulBlockSize - 1;
		tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].ulBlockStartAddress   = tyDownload.tyMemStruct[usLocalBlockIndex].ulBlockStartAddress;
		tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].ulBlockSize           = tyDownload.tyMemStruct[usLocalBlockIndex].ulBlockSize;
		tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks+1].pucBlockStartBuffer = (tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].pucBlockEndBuffer + 1);
		tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].bBlockValid = TRUE;
		tyOptDownload.tyMemStruct[tyOptDownload.usNoOfBlocks].usNextBlockIndex =  (unsigned short)(tyOptDownload.usNoOfBlocks + 1);
		
		tyOptDownload.usNoOfBlocks ++;
	}
	
	//We now have all the data blocks in memory order.
	//Check if we can combine any of the blocks.
	ulNextBlockIndex  = tyOptDownload.tyMemStruct[ulCurrentBlockIndex].usNextBlockIndex;
	
	for (i = 0; i < tyOptDownload.usNoOfBlocks; i++)
	{
		if ((tyOptDownload.tyMemStruct[ulCurrentBlockIndex].ulBlockStartAddress + tyOptDownload.tyMemStruct[ulCurrentBlockIndex].ulBlockSize) ==
			(tyOptDownload.tyMemStruct[ulNextBlockIndex].ulBlockStartAddress))
		{
			//We have a match........
			tyOptDownload.tyMemStruct[ulCurrentBlockIndex].ulBlockSize += tyOptDownload.tyMemStruct[ulNextBlockIndex]. ulBlockSize;
			tyOptDownload.tyMemStruct[ulCurrentBlockIndex].pucBlockEndBuffer = tyOptDownload.tyMemStruct[ulNextBlockIndex].pucBlockEndBuffer;
			tyOptDownload.tyMemStruct[ulCurrentBlockIndex].usNextBlockIndex =  tyOptDownload.tyMemStruct[ulNextBlockIndex].usNextBlockIndex;  
			tyOptDownload.tyMemStruct[ulNextBlockIndex].bBlockValid = FALSE;
			ulNextBlockIndex  = tyOptDownload.tyMemStruct[ulCurrentBlockIndex].usNextBlockIndex;
		}
		else
		{                       
			//Look at the next block.
			ulCurrentBlockIndex  = tyOptDownload.tyMemStruct[ulCurrentBlockIndex].usNextBlockIndex;
			ulNextBlockIndex  = tyOptDownload.tyMemStruct[ulCurrentBlockIndex].usNextBlockIndex;
		}
	}
	
	// Now Write data to target memory
	usBlockNo = 0x0;
	
	do 
	{
		ErrRet = ML_WriteProc(tyOptDownload.tyMemStruct[usBlockNo].pucBlockStartBuffer,
			tyOptDownload.tyMemStruct[usBlockNo].ulBlockStartAddress,
			&tyOptDownload.tyMemStruct[usBlockNo].ulBlockSize,
			RDIAccess_Code, TRUE, FALSE);
		
		if (ErrRet != ML_ERROR_NO_ERROR)
			return CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
		
		usBlockNo = tyOptDownload.tyMemStruct[usBlockNo].usNextBlockIndex;
	}
	while (tyOptDownload.tyMemStruct[usBlockNo].bBlockValid);
	
	//Reset the buffer
	tyOptDownload.tyMemStruct[0].pucBlockStartBuffer = tyOptDownload.pucBufferAllocated;
	tyOptDownload.tyMemStruct[0].usNextBlockIndex    = 0x1;
	tyOptDownload.usNoOfBlocks = 0;
	
	for (i=0; i <= MAX_NO_BLOCKS; i++)
	{
		tyOptDownload.tyMemStruct[i].bBlockValid = FALSE;
	}
	
	//ErrRet = ML_CheckForDataAbort ();
	return (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
}

/****************************************************************************
Function: HandleDCCSemihosting
Engineer: Suraj S
Input: unsigned long *pulDCCSemihostReadData -
unsigned long *pulDCCSemihostWriteData - 
boolean       *bAngelReportException -
unsigned long *ulPCValue -
Output: TyError - RDI Layer Error code
Description: Handles Semihosting functions for Debug Commms channel Semihosting
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
#ifdef _WINDOWS
TyError HandleDCCSemihosting (unsigned long *pulDCCSemihostReadData, 
                              unsigned long *pulDCCSemihostWriteData, 
                              boolean       *bAngelReportException, 
                              unsigned long *ulPCValue)
{
	TyError        ErrRet = NO_ERROR;
	unsigned long  *pulWriteData     = NULL;
	unsigned char  *pucDataPntr      = NULL;
	unsigned char  *pucDataPntr2     = NULL;
	char           *pcDataPntr       = NULL;
	unsigned long  ulFileHandle      = 0;
	unsigned long  ulStatus          = 0;
	unsigned long  ulNumberOfCharsFromHost = 0;
	unsigned long  ulTemp            = 0;
	
	pulWriteData = pulDCCSemihostWriteData;
	*bAngelReportException = FALSE;
	
	*ulPCValue = pulDCCSemihostReadData[1];
	
	switch (*(unsigned long *)&pulDCCSemihostReadData[0])
	{
	case  SH_SYS_OPEN         :
		pucDataPntr = (unsigned char *) &pulDCCSemihostReadData[3];
		
		//Open the file.
		ulFileHandle = SH_SYSOpen (pucDataPntr, pulDCCSemihostReadData[2]);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulFileHandle;
		
		break;
	case  SH_SYS_CLOSE        :
		ulStatus = SH_SYSClose (pulDCCSemihostReadData[2]);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		break;
	case  SH_SYS_WRITEC       :
		break;
	case  SH_SYS_WRITE0       :
		break;
	case  SH_SYS_WRITE        :
		
		pucDataPntr = (unsigned char *) &pulDCCSemihostReadData[4];
		
		if (SH_CheckForFile (pulDCCSemihostReadData[2]) == SH_NOTINTERACTIVE)
            //write the data to the file.
            ulStatus = SH_SYSWrite (pulDCCSemihostReadData[2], pucDataPntr, pulDCCSemihostReadData[3]);
		else
            //write the data to the console window.
            ulStatus = SH_SYSConsoleWrite (pucDataPntr, pulDCCSemihostReadData[3]);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		
		break;
	case  SH_SYS_READ         :
		pcDataPntr = (char *) &pulDCCSemihostWriteData[2];
		
		memset ((void*)&pulDCCSemihostWriteData[0],0x0,sizeof(pulDCCSemihostWriteData));
		pulDCCSemihostWriteData[2] = 0;
		pulDCCSemihostWriteData[3] = 0;
		pulDCCSemihostWriteData[4] = 0;
		pulDCCSemihostWriteData[5] = 0;
		pulDCCSemihostWriteData[6] = 0;
		pulDCCSemihostWriteData[7] = 0;
		pulDCCSemihostWriteData[8] = 0;
		pulDCCSemihostWriteData[9] = 0;
		
		if (SH_CheckForFile (pulDCCSemihostReadData[2]) == SH_NOTINTERACTIVE)
		{
            //Read the data from the file.
            ulStatus = SH_SYSRead (pulDCCSemihostReadData[2], 
				pcDataPntr,
				pulDCCSemihostReadData[3], 
				pulDCCSemihostReadData[4],
				tyTargetConfig.bUseGNUSemihosting);
		}
		else
		{
            while (*pcDataPntr == 0)
			{
				ulNumberOfCharsFromHost = SH_ReadStringFromConsole (pcDataPntr, pulDCCSemihostReadData[3]);
				DebuggerCallBack (); 
			}
            ulStatus = pulDCCSemihostReadData[3] - ulNumberOfCharsFromHost;
		}
		
		pulDCCSemihostWriteData[0] = (pulDCCSemihostReadData[3] / 4) + 0x3 ;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		
		break;
	case  SH_SYS_READC        :
		break;
	case  SH_SYS_ISERROR      :
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = 0x0;
		break;
	case  SH_SYS_ISTTY        :
		ulStatus = SH_CheckForFile (pulDCCSemihostReadData[2]);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		
		break;   
	case  SH_SYS_SEEK         :
		ulStatus = SH_SYSSeek (pulDCCSemihostReadData[2], pulDCCSemihostReadData[3]);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		break;
	case  SH_SYS_FLEN         :
		ulTemp =  (unsigned long) SH_SYSFlen (pulDCCSemihostReadData[2]);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulTemp;
		break;
	case  SH_SYS_TMPNAM       :
		pcDataPntr = (char *) &pulDCCSemihostWriteData[2];
		
		ulStatus = SH_CreateTempFile(pcDataPntr);
		if ((ulStatus != SH_NO_ERROR) || (pulDCCSemihostReadData[3] < DOS_FILENAME_LENGTH))
			ulStatus = 0xFFFFFFFF; //-1;
		
		pulDCCSemihostWriteData[1] = ulStatus;
		pulDCCSemihostWriteData[0] = 0x5;
		break;
	case  SH_SYS_REMOVE       :
		pucDataPntr = (unsigned char *) &pulDCCSemihostReadData[3];
		
		//Remove the file.
		ulStatus = SH_SYSRemove (pucDataPntr);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		
		break;
	case  SH_SYS_RENAME       :
		pucDataPntr = pucDataPntr2 =(unsigned char *) &pulDCCSemihostReadData[4];
		pucDataPntr2 += (pulDCCSemihostReadData[2] + 1);
		
		ulStatus = SH_SYSRename (pucDataPntr, 
			pucDataPntr2);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulStatus;
		
		break;
	case  SH_SYS_CLOCK        :
		ulTemp = (((GetTickCount()) - ulStartExecutionTickCount) / 10);
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulTemp;
		break;
	case  SH_SYS_TIME         :
		ErrRet = SH_GetTime (&pulDCCSemihostWriteData[1]);
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		break;                  
	case  SH_SYS_SYSTEM       :
		return RDIError_SoftwareInterrupt; 
	case  SH_SYS_ERRNO        :
		ulTemp = SH_SYSError ();
		
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = ulTemp;
		break;                  
	case  SH_SYS_GET_CMDLINE  :
		//         pulDCCSemihostWriteData[0] = ((strlen (csCommandLine.GetBuffer(0))/4) + 3);   //Number of Words to write to target.
		pulDCCSemihostWriteData[0] = ((strlen (pszCommandLine)/4) + 3);   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = 0x0;
		//         pulDCCSemihostWriteData[2] = (strlen (csCommandLine.GetBuffer(0)) + 1);   //Number of Words to write to target.
		pulDCCSemihostWriteData[2] = (strlen (pszCommandLine) + 1);   //Number of Words to write to target.
		//         strcpy ((char*)&pulDCCSemihostWriteData[3],(LPCTSTR) csCommandLine.GetBuffer(0)); 
		strcpy ((char*)&pulDCCSemihostWriteData[3],pszCommandLine); 
		break;
	case  SH_SYS_HEAPINFO     :
		pulDCCSemihostWriteData[0] = 0x4;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = 0x0;
		pulDCCSemihostWriteData[2] = ulGlobalTopOfMemory - 0x4000;
		pulDCCSemihostWriteData[3] = ulGlobalTopOfMemory;
		pulDCCSemihostWriteData[4] = ulGlobalTopOfMemory - 0x4000;
		break;
		
	case SH_ANGEL_SWIREASON_ENTERSVC:
		pulDCCSemihostWriteData[0] = 0x1;   //One Words to write to target.
		pulDCCSemihostWriteData[1] = 0x0;   //No Error
		break;
		
	case SH_ANGEL_REASONEXCEPTION :
		pulDCCSemihostWriteData[0] = 0x0;   //No Words to write to target.
		
		ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
		
		if (!((ErrRet==ML_ERROR_NO_ERROR) || (ErrRet == ML_Error_TargetStopped)))
            return CheckForFatalErrorMessage (RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
		
		*bAngelReportException = TRUE;
		break;
	case SH_ANGEL_LATE_STARTUP :
		break;
	case  SH_SYS_ELAPSED      :
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = 0xFFFFFFFF;  //-1;    //We do not know the value of a tick
		break;
	case  SH_SYS_TICKFREQ     :
		pulDCCSemihostWriteData[0] = 0x1;   //Number of Words to write to target.
		pulDCCSemihostWriteData[1] = 0xFFFFFFFF;  //-1;    //We do not know the value of a tick
		break;
	default :
		return RDIError_SoftwareInterrupt; 
      }
	  
	  return RDIError_NoError;
}
#endif
/****************************************************************************
Function: HandleSemiHosting
Engineer: Suraj S
Input: TYSemiHostStruct *tySemiHostStruct - Pointer to SemiHostStruct
TXRXCauseOfBreak *tyCauseOfBreak - Cause of break stored here 
Output: TyError - RDI Layer Error code
Description: Handles the semihosting function calls.
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError HandleSemiHosting (TYSemiHostStruct *tySemiHostStruct, TXRXCauseOfBreak *tyCauseOfBreak)
{
	TyError ErrRet;
	unsigned long ulTemp=0;
	unsigned long i=0;
	unsigned long ulNumberOfCharsFromHost=0;
	char          *pcBufferAllocated   = NULL;
	unsigned char *pucBufferAllocated  = NULL;
	unsigned char *pucBufferAllocated2 = NULL;
	unsigned long ulStatus=0;
	
	switch (tySemiHostStruct->SemiHostingType)
	{
	case SH_SYS_OPEN  :
		//Read data from target memory, then send to debug host.
		pucBufferAllocated = (unsigned char *)malloc(tySemiHostStruct->arg3);
		if (pucBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		ErrRet = ML_ReadProc(tySemiHostStruct->arg1,
			pucBufferAllocated,
			&tySemiHostStruct->arg3, 
			RDIAccess_Data, 
			FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		//Open the file.
		ulStatus = (unsigned long) SH_SYSOpen (pucBufferAllocated, 
			tySemiHostStruct->arg2);
		free (pucBufferAllocated);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			// R0 contains status.
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_CLOSE  :
		ulStatus = (unsigned long) SH_SYSClose (tySemiHostStruct->arg1);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			// R0 contains status.
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_WRITEC  :
		ulStatus = SH_SYSConsoleWriteC (tySemiHostStruct->arg1);
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_WRITE0    :
		//Write a NULL terminated string to the Console window
		pucBufferAllocated = (unsigned char *)malloc(RDI_HIF_BLOCKSIZE);
		if (pucBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		// Add null termination to start of string.
		pucBufferAllocated[0] = 0x0;
		ulTemp = 1;
		for (i =0; i < RDI_HIF_BLOCKSIZE; i++)
		{
            // Read chars from target memory until NULL termination is found.
            ErrRet = ML_ReadProc((tySemiHostStruct->arg1+i), 
				&tySemiHostStruct->arg2,
				&ulTemp, RDIAccess_Data, 
				FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
            pucBufferAllocated[i] = (unsigned char) tySemiHostStruct->arg2;
            if (tySemiHostStruct->arg2 == 0)
				break;
		}
		ulStatus = SH_SYSConsoleWrite (pucBufferAllocated, i);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			// R0 contains status.
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		free (pucBufferAllocated);
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_WRITE     :
		//Read data from target memory, then send to debug host.
		pucBufferAllocated = (unsigned char *)malloc((size_t) (tySemiHostStruct->arg3 + 0x8));
		if (pucBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		memset ((void *) pucBufferAllocated, 0x0, (size_t) (tySemiHostStruct->arg3 + 0x8));
		
		// Add null termination to start of string.
		pucBufferAllocated[0] = 0x0;
		ErrRet = ML_ReadProc(tySemiHostStruct->arg2,
			pucBufferAllocated,
			&tySemiHostStruct->arg3, 
			RDIAccess_Data, 
			FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		if (SH_CheckForFile (tySemiHostStruct->arg1) == SH_NOTINTERACTIVE)
		{
            //write the data to the file.
            ulStatus = SH_SYSWrite (tySemiHostStruct->arg1, 
				pucBufferAllocated, 
				tySemiHostStruct->arg3);
		}
		else
            //write the data to the console window.
            ulStatus = SH_SYSConsoleWrite (pucBufferAllocated, tySemiHostStruct->arg3);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			// R0 contains status.
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		free (pucBufferAllocated); 
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_READ      :
		//Read data from debug host then send to target memory.
		pcBufferAllocated = (char *)malloc(tySemiHostStruct->arg3);
		if (pcBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		// Clear the string.
		memset (pcBufferAllocated, 0,(size_t) tySemiHostStruct->arg3);
		
		if (SH_CheckForFile (tySemiHostStruct->arg1) == SH_NOTINTERACTIVE)
		{
            //Read the data from the file.
            ulStatus = (short) SH_SYSRead (tySemiHostStruct->arg1, 
				pcBufferAllocated, 
				tySemiHostStruct->arg3, 
				tySemiHostStruct->arg4,
				tyTargetConfig.bUseGNUSemihosting);
			
            ErrRet = ML_WriteProc((void*)pcBufferAllocated, 
				tySemiHostStruct->arg2, 
				&tySemiHostStruct->arg3, 
				RDIAccess_Data, 
				FALSE,
				FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
			if (tyProcessorConfig.bCortexM3)
			{
				ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulStatus);
				
				JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			} 
			else
			{
				// R0 contains status.
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
			}            
		}
		else
		{
            while ((pcBufferAllocated[0] == 0)&& (!bStopAsserted))
			{
				ulNumberOfCharsFromHost = SH_ReadStringFromConsole (pcBufferAllocated, 
					tySemiHostStruct->arg3);
				DebuggerCallBack ();
			}
            ulStatus = tySemiHostStruct->arg3 - ulNumberOfCharsFromHost;
            
            if (!bStopAsserted)
			{
				ErrRet = ML_WriteProc((void*)pcBufferAllocated,
					tySemiHostStruct->arg2, 
					&tySemiHostStruct->arg3, 
					RDIAccess_Data, 
					FALSE,
					FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			}
            if (bStopAsserted)
			{
				if (tyProcessorConfig.bCortexM3)
				{
					ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
						REG_R0_MASK, 
						&tySemiHostStruct->arg3);
					
					JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				} 
				else
				{
					//User hit stop button so semihosting command did not finish.
					ErrRet = ML_CPUWriteProc(USR_MODE, 
						REG_R0_MASK, 
						&tySemiHostStruct->arg3); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
					
				}				
				
				*tyCauseOfBreak = USER_HALT;
			}
		}
		free (pcBufferAllocated); 
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_READC     :
		while ((tySemiHostStruct->arg1 == 0)  && (!bStopAsserted))
		{
            ErrRet = SH_ReadCharFromConsole (&tySemiHostStruct->arg1); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
            DebuggerCallBack ();
		}
		if (bStopAsserted)
		{
            //User hit stop button so semihosting command did not finish.
            tySemiHostStruct->arg1 = 0;
			
			if (tyProcessorConfig.bCortexM3)
			{
				ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&tySemiHostStruct->arg1);
				
				JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			} 
			else
			{
				//User hit stop button so semihosting command did not finish.
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&tySemiHostStruct->arg1); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
			}            
			
            *tyCauseOfBreak = USER_HALT;
            tySemiHostStruct->bRestartExecution = FALSE;
		}
		
		if (!bStopAsserted)
		{
			if (tyProcessorConfig.bCortexM3)
			{
				ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&tySemiHostStruct->arg1);
				
				JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			} 
			else
			{
				//User hit stop button so semihosting command did not finish.
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&tySemiHostStruct->arg1); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
			}            
			
            tySemiHostStruct->bRestartExecution = TRUE;
		}
		break;
	case SH_SYS_ISERROR   :
		//This is not supported.
		break;
	case SH_SYS_ISTTY     :
		ulStatus = (unsigned long) SH_CheckForFile (tySemiHostStruct->arg1);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_SEEK      :
		ulStatus = (unsigned long) SH_SYSSeek (tySemiHostStruct->arg1, 
			tySemiHostStruct->arg2);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_FLEN      :
		ulStatus = (unsigned long) SH_SYSFlen (tySemiHostStruct->arg1);
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_TMPNAM    :
		//Allocate buffer for temp file name.
		pcBufferAllocated = (char *)malloc(tySemiHostStruct->arg3);
		if (pcBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		ulStatus = (unsigned long) SH_CreateTempFile(pcBufferAllocated);
		//Make the buffer is large enough..  and that there were no errors.
		if ((ulStatus != NO_ERROR) || (tySemiHostStruct->arg3 < DOS_FILENAME_LENGTH))
            return ulStatus;
		else
		{
            ulTemp = (DOS_FILENAME_LENGTH);
            //Copy the file name into buffer.
            ErrRet = ML_WriteProc((void *)pcBufferAllocated,
				tySemiHostStruct->arg1, 
				&ulTemp, RDIAccess_Data, 
				FALSE,
				FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
            ErrRet = ML_ResetCheck(ErrRet); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
			if (tyProcessorConfig.bCortexM3)
			{
				ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulStatus);
				
				JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			} 
			else
			{
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
			}            
			
            tySemiHostStruct->bRestartExecution = TRUE;
            free (pcBufferAllocated);
		}
		break;
	case SH_SYS_REMOVE    :
		//Read data from target memory, then send to debug host.
		pucBufferAllocated = (unsigned char *)malloc(tySemiHostStruct->arg2);
		if (pucBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		ErrRet = ML_ReadProc(tySemiHostStruct->arg1,
			pucBufferAllocated,
			&tySemiHostStruct->arg2, 
			RDIAccess_Data, 
			FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		ErrRet = ML_ResetCheck(ErrRet); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		ulStatus = (unsigned long) SH_SYSRemove (pucBufferAllocated);
		free (pucBufferAllocated); 
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_RENAME    :
		//Read file name from target
		pucBufferAllocated = (unsigned char *)malloc(tySemiHostStruct->arg2);
		if (pucBufferAllocated == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}	
		
		ErrRet = ML_ReadProc(tySemiHostStruct->arg1,
			pucBufferAllocated,
			&tySemiHostStruct->arg2,
			RDIAccess_Data, 
			FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		//Read new file name from target
		pucBufferAllocated2 = (unsigned char *)malloc(tySemiHostStruct->arg4);
		if (pucBufferAllocated2 == NULL)
		{
            ErrRet = RDIError_Failed_To_Allocate_PC_Memory;
            goto HANDLE_SEMIHOSTING_ERROR_RETURN;
		}
		
		ErrRet = ML_ReadProc(tySemiHostStruct->arg3,
			pucBufferAllocated2,
			&tySemiHostStruct->arg4, 
			RDIAccess_Data, 
			FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		ulStatus = (unsigned long) SH_SYSRename (pucBufferAllocated, 
			pucBufferAllocated2); 
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulStatus); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		free (pucBufferAllocated);
		free (pucBufferAllocated2);
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
#ifdef _WINDOWS
	case SH_SYS_CLOCK     :
		ulTemp = (((GetTickCount()) - ulStartExecutionTickCount) / 10);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
#endif
	case SH_SYS_TIME      :
		ErrRet = (unsigned long) SH_GetTime (&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_SYSTEM    :
		//This is not supported.
		break;
	case SH_SYS_ERRNO     :
		//Returns the last system error number into R1
		ulTemp = (unsigned long) SH_SYSError ();
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_GET_CMDLINE :
		// Get the image file name from the system and store in buffer.
		ulTemp = 0;
		ErrRet = ML_WriteProc(pszCommandLine/*csCommandLine.GetBuffer(0)*/,
			tySemiHostStruct->arg1,
			&tySemiHostStruct->arg2, 
			RDIAccess_Data, 
			FALSE,
			FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		if (ErrRet != RDIError_NoError)
		{
            ulTemp = 0xFFFFFFFF; //-1;
		}
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(USR_MODE, 
				REG_R0_MASK, 
				&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_SYS_HEAPINFO :
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_ANGEL_SWIREASON_ENTERSVC :
		//Read the User Stack pointer
		ErrRet = ML_CPUReadProc(USR_MODE, 
			REG_R13_MASK, 
			&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		
		
		if (tyProcessorConfig.bCortexM3)
		{
			ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
				REG_R13_MASK, 
				&ulTemp);
			
			JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
		} 
		else
		{
			ErrRet = ML_CPUWriteProc(SVC_MODE, 
				REG_R13_MASK, 
				&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
		}		
		
		tySemiHostStruct->bRestartExecution = TRUE;
		break;
	case SH_ANGEL_REASONEXCEPTION   :
		switch (tySemiHostStruct->arg1)
		{
		case ADP_Stopped_ApplicationExit:
			*tyCauseOfBreak = ANGEL_EXCEPTION_HALT;
			tySemiHostStruct->bRestartExecution = FALSE;
			break;
		default:
			break;
		}
		ulTemp = ulTemp;
		break;
		case SH_SYS_ELAPSED             :
			ulTemp = 0x0; // we do not know the no of ticks..
			tySemiHostStruct->arg2 = 0x8;   //write eight bytes.
			ErrRet = ML_WriteProc(&ulTemp,
				tySemiHostStruct->arg1,
				&tySemiHostStruct->arg2,
				RDIAccess_Data, 
				FALSE,
				FALSE); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			
			if (tyProcessorConfig.bCortexM3)
			{
				// Write -1 into R1 because we don't know the time
				ulTemp = (unsigned long) -1; // we do not know the no of ticks..
				ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulTemp);
				
				JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			} 
			else
			{
				// Write -1 into R1 because we don't know the time
				ulTemp = (unsigned long) -1; // we do not know the no of ticks..
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
			}			
			
			tySemiHostStruct->bRestartExecution = TRUE;
			break;
		case SH_SYS_TICKFREQ            :
			ulTemp = (unsigned long ) -1;
			if (tyProcessorConfig.bCortexM3)
			{				
				ErrRet = ML_CortexMCPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulTemp);
				
				JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
			} 
			else
			{				
				ErrRet = ML_CPUWriteProc(USR_MODE, 
					REG_R0_MASK, 
					&ulTemp); JMP_ERR(HANDLE_SEMIHOSTING_ERROR_RETURN);
				
			}		
			
			tySemiHostStruct->bRestartExecution = TRUE;
			break;
		default :
			//This is not supported.
			break;
      }
	  
	  return NO_ERROR;
	  
HANDLE_SEMIHOSTING_ERROR_RETURN:
	  
	  if (pcBufferAllocated != NULL)
		  free (pcBufferAllocated);
	  
	  if (pucBufferAllocated != NULL)
		  free (pucBufferAllocated);
	  
	  if (pucBufferAllocated2 != NULL)
		  free (pucBufferAllocated2);
	  
	  return CheckForFatalErrorMessage (ErrRet);
}

/****************************************************************************
Function: DebuggerCallBack
Engineer: Suraj S
Input: NONE
Output: NONE
Description: Performs callbacks to debugger
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
void DebuggerCallBack (void)
{
	//RDI 151 allows the debugger to perform its own messages.
	if ((fpCallBackFunction != NULL) && (iGlobalRDI_Level == RDI_151))
	{
		fpCallBackFunction(fpCallBackArg);
	}
#ifdef _WINDOWS
	else 
		//RDI 150 polls the debugger to see if any messages need to be serviced
		if (iGlobalRDI_Level == RDI_150)
		{
#define MAX_NUM_MESSAGES 50
			unsigned long ulCount;
			MSG msg;
			
			ulCount = 0;
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
				{
					// Repost the QUIT message so that it will be retrieved by the
					// primary GetMessage() loop.
					PostQuitMessage((int)msg.wParam);
					break;
				}
				TranslateMessage(&msg); 
				(void)DispatchMessage(&msg); 
				ulCount++;
				if (ulCount > MAX_NUM_MESSAGES)
					break;
			}
		}
#endif
		//else
		//TODO: ASSERT_NOW();
}

/****************************************************************************
Function: EnumerateModulesinTarget
Engineer: Suraj S
Input: arg1 and arg2 passed straight to ML_InfoProc.
Output: static TyError - RDI Error code
Description: Enumerates modules in target.
Date          Initials    Description
30-Jul-2007     SJ        Initial
****************************************************************************/
static TyError EnumerateModulesinTarget(ARMword *arg1, ARMword *arg2)
{
	RDI_ModuleDesc_Extra *pExtraModuleDescriptor;
	RDI_ModuleDesc *pModuleDescriptor;
	
	if (ulNumberOfModules == 0)
	{
		//This is a request to return the total number of processors in the system
		ulNumberOfModules = 0x1;
	}
	
	//We only return one core and one ETM module 
	if ((arg2 != NULL) && (*arg1 > 0x0))
	{
		pModuleDescriptor       = (RDI_ModuleDesc *)arg2;
		pExtraModuleDescriptor  = (RDI_ModuleDesc_Extra *)arg2;
		
		pModuleDescriptor->rdi=&tyLocalVec; 
		strcpy(pModuleDescriptor->type,"ARM");
		pModuleDescriptor->handle=(RDI_OpaqueModuleStateStr *)TARGET_MODULE_HANDLE;
		
		switch (iGlobalRDI_Level)
		{
		default:
		case RDI_150:
            strcpy(pModuleDescriptor->name,"ARM_1");
            break;
		case RDI_151:
            strcpy(pModuleDescriptor->name,"\'");
            pExtraModuleDescriptor->name.longname.string = &tyTargetConfig.szProcessorType[0];
            break;
		}
	}
	
	//Return the number of modules.
	if (arg1 != NULL)
		*arg1 = ulNumberOfModules;
	
	return RDIError_NoError;
}

/****************************************************************************
Function: Register_CommsChannelToHost
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Store Debugger callback function pointer
Date          Initials    Description
31-Jul-2007     SJ        Initial
****************************************************************************/
int Register_CommsChannelToHost( RDICCProc_ToHost *callbackToHost, 
								void * HandleToBePassed )
{
	pvDCCHandleToBePassed = HandleToBePassed;
	fpDCCcallbackToHost   = callbackToHost;
	return RDIError_NoError; 
}

/****************************************************************************
Function: Register_CommsChannelFromHost
Engineer: Suraj S
Input: See RDI specification.
Output: See RDI specification.
Description: Store Debugger callback function pointer
Date          Initials    Description
31-Jul-2007     SJ        Initial
****************************************************************************/
int Register_CommsChannelFromHost( RDICCProc_FromHost *callbackFromHost, 
								  void * HandleToBePassed )
{ 
	/*unreferenced formal parameter*/
	NOREF(HandleToBePassed);
	
	fpDCCcallbackFromHost=callbackFromHost;
	return RDIError_NoError; 
}

/******************************************************************************************
Function: GetElfHeaderInfoAndSetPC
Engineer: Suraj S
Input: char * CmdLine - pointer to arg1
Output: TyError - RDI Error code
Description : This function is only called when debugging through the ARM ADW debugger.
The ARM debugger does not explicidy set the PC before execution instead 
it sends down the ARM image name and the DLL must workout from the ELF 
header info where the Program Entry point is, ie. where to set the PC 
Date           Initials    Description
20-Aug-2007      SJ         Initial
18-Dec-2009		 JCK		Solved the problem with AXD debugger
******************************************************************************************/
TyError GetElfHeaderInfoAndSetPC(char * CmdLine)
{
	
	typedef  unsigned long  Elf32_Addr;
	typedef  unsigned short Elf32_Half;
	typedef  unsigned long  Elf32_Off;
	typedef  long           Elf32_Sword;
	typedef  unsigned long  Elf32_Word;
	unsigned long           ulEntryPoint =   0;
	TyError                 ErrRet       =   NO_ERROR;
	unsigned long           ulLength     =   0; 
#define EI_NIDENT       16
	
	typedef struct
	{
		unsigned char  e_ident[EI_NIDENT];
		Elf32_Half     e_type;
		Elf32_Half     e_machine;
		Elf32_Word     e_version;
		Elf32_Addr     e_entry;
		Elf32_Off      e_phoff;
		Elf32_Off      e_shoff;
		Elf32_Word     e_flags;
		Elf32_Half     e_ehsize;
		Elf32_Half     e_phentsize;
		Elf32_Half     e_phnum;
		Elf32_Half     e_shentsize;
		Elf32_Half     e_shnum;
		Elf32_Half     e_shstrndx;
	} Elf32_Ehdr;
	
	Elf32_Ehdr tyLocElf32_Ehdr;
	
	FILE *hFile;
	//CString cFileName;
	char *pszFileName = 0;
	pszFileName = (char*) malloc(MAX_PATH_SIZE);
	
	if (iGlobalRDI_Level == RDI_150)
	{
		//cFileName = (char *)&CmdLine[0];
		strcpy(pszFileName,(char *)&CmdLine[0]);
	}
	else
		if (iGlobalRDI_Level == RDI_151)
		{ 
			//strip out the extra inverted commas
			//cFileName = (char *)&CmdLine[1];
			strcpy(pszFileName,(char *)&CmdLine[1]);
			//cFileName.SetAt(cFileName.Find('"'), '\0');
			ulLength = strlen(pszFileName);
			for(unsigned int i=0; i<ulLength; i++)
			{
				if(pszFileName[i] == '"')				
				{
					pszFileName[i] = '\0';
					break;
				}
			}
		}
		//else
		//TODO: ASSERT_NOW();
		
		//hFile = fopen(cFileName, "rb");
		hFile = fopen(pszFileName, "rb");
		if (hFile == NULL)
			return RDIError_NoSuchHandle;
		if (fread(&tyLocElf32_Ehdr, 1, sizeof(tyLocElf32_Ehdr), hFile) != sizeof(tyLocElf32_Ehdr))
			return RDIError_NoSuchHandle;
		fclose(hFile);
		free(pszFileName);
		
		//write the PC to the Program Entry point specified by the ELF image
		ulEntryPoint = tyLocElf32_Ehdr.e_entry; 
		if(tyProcessorConfig.bCortexM3)
        {
			ErrRet = ML_CortexMCPUWriteProc((unsigned long)   0xff,
				(unsigned long)   0x10000,
				(unsigned long *)&ulEntryPoint);
        }
		else
        {
			ErrRet =  ML_CPUWriteProc ((unsigned long)   0xff,
				(unsigned long)   0x10000,
				(unsigned long *)&ulEntryPoint);
        }
		ErrRet = ML_CPUWriteProc ((unsigned long)   0xff,
			(unsigned long)   0x10000,
			(unsigned long *)&ulEntryPoint);
		if (ErrRet != RDIError_NoError)
		{
			return ML_ResetCheck(ErrRet);
		}
		
		return RDIError_NoError; 
}

/****************************************************************************
Function: GetCoProcessorDesc
Engineer: Suraj S
Input: unsigned long *CPNumber - CoProcessor number
Co_ProcessorDescStructure *arg1 - pointer to Co_ProcessorDescStructure
Output: TyError - RDI Error code
Description: Fills the co processor description structure
Date           Initials    Description
21-Aug-2007      SJ         Initial
24-Mar-2008	     SJ		    Added 968 core support
****************************************************************************/
TyError GetCoProcessorDesc (unsigned long *CPNumber, Co_ProcessorDescStructure *arg1)
{
	// We only support CP15
	if (*CPNumber != 15)
	{
		return RDIError_UnknownCoPro;
	}
	
	switch (tyTargetConfig.tyProcType)
	{
	default:
	case ARM7DI    :  
	case ARM7TDMI  :
	case ARM7TDMIS :
	case ARM9TDMI  :
		arg1->entries   = 0x0;
		return RDIError_UnknownCoPro;
	case ARM720T:
		arg1->entries                                = 0x7;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C2
		arg1->CPStruct[0x2].rmin                     = 0x2;
		arg1->CPStruct[0x2].rmax                     = 0x2;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		//C3
		arg1->CPStruct[0x3].rmin                     = 0x3;
		arg1->CPStruct[0x3].rmax                     = 0x3;
		arg1->CPStruct[0x3].nbytes                   = 0x4;
		arg1->CPStruct[0x3].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x3].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b1 = 0xFF;
		//C5
		arg1->CPStruct[0x4].rmin                     = 0x5;
		arg1->CPStruct[0x4].rmax                     = 0x5;
		arg1->CPStruct[0x4].nbytes                   = 0x4;
		arg1->CPStruct[0x4].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x4].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b1 = 0xFF;
		//C6
		arg1->CPStruct[0x5].rmin                     = 0x6;
		arg1->CPStruct[0x5].rmax                     = 0x6;
		arg1->CPStruct[0x5].nbytes                   = 0x4;
		arg1->CPStruct[0x5].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x5].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b1 = 0xFF;
		//C13
		arg1->CPStruct[0x6].rmin                     = 0xD;
		arg1->CPStruct[0x6].rmax                     = 0xD;
		arg1->CPStruct[0x6].nbytes                   = 0x4;
		arg1->CPStruct[0x6].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x6].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b1 = 0xFF;
		break;
		
	case ARM740T:
		arg1->entries                                = 0x6;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C2
		arg1->CPStruct[0x2].rmin                     = 0x2;
		arg1->CPStruct[0x2].rmax                     = 0x2;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		//C3
		arg1->CPStruct[0x3].rmin                     = 0x3;
		arg1->CPStruct[0x3].rmax                     = 0x3;
		arg1->CPStruct[0x3].nbytes                   = 0x4;
		arg1->CPStruct[0x3].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x3].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b1 = 0xFF;
		//C5
		arg1->CPStruct[0x4].rmin                     = 0x5;
		arg1->CPStruct[0x4].rmax                     = 0x5;
		arg1->CPStruct[0x4].nbytes                   = 0x4;
		arg1->CPStruct[0x4].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x4].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b1 = 0xFF;
		//C6
		arg1->CPStruct[0x5].rmin                     = 0x6;
		arg1->CPStruct[0x5].rmax                     = 0x6;
		arg1->CPStruct[0x5].nbytes                   = 0x4;
		arg1->CPStruct[0x5].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x5].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b1 = 0xFF;
		break;
		
	case ARM920T:
	case ARM922T:
		arg1->entries                                = 0x9;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C2 TTB Reg
		arg1->CPStruct[0x2].rmin                     = 0x2;
		arg1->CPStruct[0x2].rmax                     = 0x2;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		//C3 Domain Access Control
		arg1->CPStruct[0x3].rmin                     = 0x3;
		arg1->CPStruct[0x3].rmax                     = 0x3;
		arg1->CPStruct[0x3].nbytes                   = 0x4;
		arg1->CPStruct[0x3].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x3].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b1 = 0xFF;
		//C5 Fault Status
		arg1->CPStruct[0x4].rmin                     = 0x5;
		arg1->CPStruct[0x4].rmax                     = 0x5;
		arg1->CPStruct[0x4].nbytes                   = 0x4;
		arg1->CPStruct[0x4].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x4].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b1 = 0xFF;
		//C6 Fault Address
		arg1->CPStruct[0x5].rmin                     = 0x6;
		arg1->CPStruct[0x5].rmax                     = 0x6;
		arg1->CPStruct[0x5].nbytes                   = 0x4;
		arg1->CPStruct[0x5].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x5].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b1 = 0xFF;
		//C9 CacheLockDown
		arg1->CPStruct[0x6].rmin                     = 0x9;
		arg1->CPStruct[0x6].rmax                     = 0x9;
		arg1->CPStruct[0x6].nbytes                   = 0x4;
		arg1->CPStruct[0x6].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x6].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b1 = 0xFF;
		//C10  TLB Loackdown
		arg1->CPStruct[0x7].rmin                     = 0xA;
		arg1->CPStruct[0x7].rmax                     = 0xA;
		arg1->CPStruct[0x7].nbytes                   = 0x4;
		arg1->CPStruct[0x7].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x7].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.write_b1 = 0xFF;
		//C13 FCSE PID Reg
		arg1->CPStruct[0x8].rmin                     = 0xD;
		arg1->CPStruct[0x8].rmax                     = 0xD;
		arg1->CPStruct[0x8].nbytes                   = 0x4;
		arg1->CPStruct[0x8].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x8].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x8].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x8].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x8].accessinst.cprt.write_b1 = 0x00;
		break;
		
	case ARM926EJS:
		arg1->entries                                = 0x9;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C2 TTB Reg
		arg1->CPStruct[0x2].rmin                     = 0x2;
		arg1->CPStruct[0x2].rmax                     = 0x2;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		//C3 Domain Access Control
		arg1->CPStruct[0x3].rmin                     = 0x3;
		arg1->CPStruct[0x3].rmax                     = 0x3;
		arg1->CPStruct[0x3].nbytes                   = 0x4;
		arg1->CPStruct[0x3].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x3].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b1 = 0xFF;
		//C5 Fault Status
		arg1->CPStruct[0x4].rmin                     = 0x5;
		arg1->CPStruct[0x4].rmax                     = 0x5;
		arg1->CPStruct[0x4].nbytes                   = 0x4;
		arg1->CPStruct[0x4].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x4].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b1 = 0xFF;
		//C6 Fault Address
		arg1->CPStruct[0x5].rmin                     = 0x6;
		arg1->CPStruct[0x5].rmax                     = 0x6;
		arg1->CPStruct[0x5].nbytes                   = 0x4;
		arg1->CPStruct[0x5].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x5].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b1 = 0xFF;
		//C9 CacheLockDown
		arg1->CPStruct[0x6].rmin                     = 0x9;
		arg1->CPStruct[0x6].rmax                     = 0x9;
		arg1->CPStruct[0x6].nbytes                   = 0x4;
		arg1->CPStruct[0x6].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x6].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b1 = 0xFF;
		//C10  TLB Lockdown
		arg1->CPStruct[0x7].rmin                     = 0xA;
		arg1->CPStruct[0x7].rmax                     = 0xA;
		arg1->CPStruct[0x7].nbytes                   = 0x4;
		arg1->CPStruct[0x7].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x7].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.write_b1 = 0xFF;
		//C13 FCSE PID Reg
		arg1->CPStruct[0x8].rmin                     = 0xD;
		arg1->CPStruct[0x8].rmax                     = 0xD;
		arg1->CPStruct[0x8].nbytes                   = 0x4;
		arg1->CPStruct[0x8].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x8].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x8].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x8].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x8].accessinst.cprt.write_b1 = 0x00;
		break;
		
	case ARM940T:
		// The processor has a CoProcessor.
		arg1->entries                                = 0x7;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C2 Cacheable Registers
		arg1->CPStruct[0x2].rmin                     = 0x2;
		arg1->CPStruct[0x2].rmax                     = 0x2;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		//C3 Write Buffer Control Register
		arg1->CPStruct[0x3].rmin                     = 0x3;
		arg1->CPStruct[0x3].rmax                     = 0x3;
		arg1->CPStruct[0x3].nbytes                   = 0x4;
		arg1->CPStruct[0x3].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x3].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b1 = 0xFF;
		//C5 Protection registers
		arg1->CPStruct[0x4].rmin                     = 0x5;
		arg1->CPStruct[0x4].rmax                     = 0x5;
		arg1->CPStruct[0x4].nbytes                   = 0x4;
		arg1->CPStruct[0x4].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x4].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b1 = 0xFF;
		//C6 Protection region base and size registers
		arg1->CPStruct[0x5].rmin                     = 0x6;
		arg1->CPStruct[0x5].rmax                     = 0x6;
		arg1->CPStruct[0x5].nbytes                   = 0x4;
		arg1->CPStruct[0x5].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x5].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b1 = 0xFF;
		//C9 Lock Down Registers
		arg1->CPStruct[0x6].rmin                     = 0x9;
		arg1->CPStruct[0x6].rmax                     = 0x9;
		arg1->CPStruct[0x6].nbytes                   = 0x4;
		arg1->CPStruct[0x6].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x6].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b1 = 0xFF;
		break;
		
	case ARM946ES:
		// The processor has a CoProcessor.
		arg1->entries                                = 0x8;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C2 TTB Reg
		arg1->CPStruct[0x2].rmin                     = 0x2;
		arg1->CPStruct[0x2].rmax                     = 0x2;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		//C3 Domain Access Control
		arg1->CPStruct[0x3].rmin                     = 0x3;
		arg1->CPStruct[0x3].rmax                     = 0x3;
		arg1->CPStruct[0x3].nbytes                   = 0x4;
		arg1->CPStruct[0x3].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x3].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x3].accessinst.cprt.write_b1 = 0xFF;
		//C5 Fault Status
		arg1->CPStruct[0x4].rmin                     = 0x5;
		arg1->CPStruct[0x4].rmax                     = 0x5;
		arg1->CPStruct[0x4].nbytes                   = 0x4;
		arg1->CPStruct[0x4].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x4].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x4].accessinst.cprt.write_b1 = 0xFF;
		//C6 Fault Address
		arg1->CPStruct[0x5].rmin                     = 0x6;
		arg1->CPStruct[0x5].rmax                     = 0x6;
		arg1->CPStruct[0x5].nbytes                   = 0x4;
		arg1->CPStruct[0x5].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x5].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x5].accessinst.cprt.write_b1 = 0xFF;
		//C9 CacheLockDown
		arg1->CPStruct[0x6].rmin                     = 0x9;
		arg1->CPStruct[0x6].rmax                     = 0x9;
		arg1->CPStruct[0x6].nbytes                   = 0x4;
		arg1->CPStruct[0x6].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x6].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x6].accessinst.cprt.write_b1 = 0xFF;
		//C13  TLB Lockdown
		arg1->CPStruct[0x7].rmin                     = 0xD;
		arg1->CPStruct[0x7].rmax                     = 0xD;
		arg1->CPStruct[0x7].nbytes                   = 0x4;
		arg1->CPStruct[0x7].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x7].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x7].accessinst.cprt.write_b1 = 0xFF;
		break;
	case ARM966ES:
		// The processor has a CoProcessor.
		arg1->entries                                = 0x3;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		//C15 Test
		arg1->CPStruct[0x2].rmin                     = 0xF;
		arg1->CPStruct[0x2].rmax                     = 0xF;
		arg1->CPStruct[0x2].nbytes                   = 0x4;
		arg1->CPStruct[0x2].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x2].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x2].accessinst.cprt.write_b1 = 0xFF;
		break;
		
	case ARM968ES:
		// The processor has a CoProcessor.
		arg1->entries                                = 0x2;
		
		//C0 ID Register                             
		arg1->CPStruct[0x0].rmin                     = 0x0;
		arg1->CPStruct[0x0].rmax                     = 0x0;
		arg1->CPStruct[0x0].nbytes                   = 0x4;
		arg1->CPStruct[0x0].access                   = RDI_Access_Readable;
		arg1->CPStruct[0x0].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x0].accessinst.cprt.write_b0 = 0x00;
		arg1->CPStruct[0x0].accessinst.cprt.write_b1 = 0x00;
		//C1 Control 
		arg1->CPStruct[0x1].rmin                     = 0x1;
		arg1->CPStruct[0x1].rmax                     = 0x1;
		arg1->CPStruct[0x1].nbytes                   = 0x4;
		arg1->CPStruct[0x1].access                   = RDI_Access_Writable | RDI_Access_Readable;
		arg1->CPStruct[0x1].accessinst.cprt.read_b0  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.read_b1  = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b0 = 0xFF;
		arg1->CPStruct[0x1].accessinst.cprt.write_b1 = 0xFF;
		break;
      }
	  
	  return RDIError_NoError;
}

/****************************************************************************
Function: CheckForFatalErrorMessage
Engineer: Suraj S
Input: Original Error Message
Output: Error Status
Description: Checks whether the error code is fatal. If it is then the USB
driver is closed.
Date           Initials    Description
3-Jul-2007       SJ        Initial
****************************************************************************/
static TyError CheckForFatalErrorMessage(TyError ErrRet)
{
	switch (ErrRet)
	{
	case RDIError_NoError                        :
	case RDIError_DataAbort                      :
	case RDIError_SoftwareInterrupt              :
	case RDIError_BadCPUStateSetting             :
	case RDIError_BadPointType                   :
	case RDIError_PointInUse                     :
	case RDIError_CantSetPoint                   :
	case RDIError_NoSuchPoint                    :
	case RDIError_UnknownCoPro                   :
	case RDIError_UnimplementedType              :
	case RDIError_RealMonFailed                  :
	case RDIError_NoToolConfig                   :
	case RDIError_Reset                          :
	case RDIError_TargetRunning                  :
	case RDIError_TargetStopped                  :
	case RDIError_Executing                      :
	case RDIError_BreakpointReached              :
	case RDIError_UndefinedInstruction           :
	case RDIError_PrefetchAbort                  :
	case RDIError_AddressException               :
	case RDIError_IRQ                            :
	case RDIError_FIQ                            :
	case RDIError_WatchpointAccessed             :
	case RDIError_BranchThrough0                 :
	case RDIError_UserInterrupt                  :
	case RDIError_UnimplementedMessage           :
	case RDIError_NotAllowedWhilstExecuting      :
	case RDIError_Failed_To_Allocate_PC_Memory   :
	case RDIError_NoSuchHandle                   :
	case RDIError_CannotWriteToVectorTable       :
	case RDIError_NonEditablePoint               :
	default:
		//Just return Error message this is fine.
		break;
	case RDIError_FPGA_File_Not_Found            :         
	case RDIError_Diskware_File_Not_Found        :     
	case RDIError_Incorrect_Diskware_File        :    
	case RDIError_SoftInitialiseError            :
		if (tyTargetConfig.bUSBDevice)
            (void)ML_CloseOpellaXD ();
		break;
	case RDIError_TargetBroken                   :
	case RDIError_TargetBroken_Opella            :         
	case RDIError_InitTarget                     :
	case RDIError_InitTarget_Opella              :
	case RDIError_IncorrectUSBDriver             :
	case RDIError_LoopBack_Opella                :       
	case RDIError_NoRTCKSupport                  :
		//Check if the reason why target comms failed was the target reset.
		ErrRet = ML_ResetCheck(ErrRet);
		
		//Close the USB device this is a fatal Error.
		if (tyTargetConfig.bUSBDevice)
            (void)ML_CloseOpellaXD ();
		break;
	}
	return ErrRet;
}

/****************************************************************************
Function: RDI_Convert_MidLayerErr_ToRDIErr
Engineer: Suraj S
Input: MidLayer Error code
Output: RDI Error code
Description: Converts MidLayer Errors to RDIErrors for OPELLAXD
Date       Initials    Description
5-Jul-2007     SJ         Initial
****************************************************************************/
TyError RDI_Convert_MidLayerErr_ToRDIErr (TyError tyMidLayerErr)
{
	switch (tyMidLayerErr)
	{
	case ML_ERROR_DEVICE_ALREADY_IN_USE         :  
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_DeviceAlreadyInUse");	      
#endif
		return RDIError_DeviceAlreadyInUse;
		
	case ML_ERROR_INCORRECT_USB_DRIVER         :  
#ifndef __LINUX
		
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_IncorrectUSBDriver");	      
#endif
		return RDIError_IncorrectUSBDriver;
		
	case ML_ERROR_FAILED_TO_PROGRAM_FPGA       :  
	case ML_ERROR_NO_USB_DEVICE_CONNECTED      :  
	case ML_ERROR_FAILED_SEND_USB_PACKET       :  
	case ML_ERROR_FAILED_RECEIVE_USB_PACKET    :  
	case ML_ERROR_FAILED_TO_WRITE_USB_MEMORY   :  
#ifndef __LINUX
		
	case ML_ERROR_OPELLA_FAILURE:
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_LoopBack_Opella");
#endif
		return RDIError_LoopBack_Opella;
		
	case ML_ERROR_FAILED_TO_FIND_FPGA_FILE     :  
#ifndef __LINUX
		
		if(bLogEnableOK)         
            strcpy (szReturnValue,"RDIError_FPGA_File_Not_Found");
#endif
		return RDIError_FPGA_File_Not_Found;  
		
	case ML_ERROR_FAILED_TO_FIND_DISKWARE_FILE : 
#ifndef __LINUX
		
		if(bLogEnableOK)         
            strcpy (szReturnValue,"RDIError_Diskware_File_Not_Found");
#endif
		return RDIError_Diskware_File_Not_Found;
		
	case ML_ERROR_INCORRECT_DISKWARE_FILE      :  
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Incorrect_Diskware_File");
#endif
		return RDIError_Incorrect_Diskware_File; 
		
	case ML_ERROR_TARGET_RESET                 :
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Reset");
#endif
		return RDIError_Reset;
		
	case ML_ERROR_INVALID_SERIAL_NO:
#ifndef __LINUX
		
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_InvalidSerialNumber");
#endif
		return RDIError_InvalidSerialNumber;
		
	case ML_Error_Reset:
#ifndef __LINUX
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_Reset");
#endif
		return RDIError_Reset;
		
	case ML_Error_UndefinedInstruction:
#ifndef __LINUX
		
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_UndefinedInstruction");
#endif
		return RDIError_UndefinedInstruction;
		
	case ML_Error_SoftwareInterrupt:
#ifndef __LINUX
		
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_SoftwareInterrupt");
#endif
		return RDIError_SoftwareInterrupt;
		
	case ML_Error_PrefetchAbort:
#ifndef __LINUX
		
		if(bLogEnableOK)
            strcpy (szReturnValue,"RDIError_PrefetchAbort");
#endif
		return RDIError_PrefetchAbort;
		
	case ML_Error_DataAbort:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_DataAbort");
#endif
		return RDIError_DataAbort;
		
	case ML_Error_AddressException:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_AddressException");
#endif
		return RDIError_AddressException;
		
	case ML_Error_IRQ:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_IRQ");
#endif
		return RDIError_IRQ;
		
	case ML_Error_FIQ:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_FIQ");
#endif
		return RDIError_FIQ;
		
	case ML_Error_Error:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Error");
#endif
		return RDIError_Error;
		
	case ML_Error_BranchThrough0:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BranchThrough0");
#endif
		return RDIError_BranchThrough0;
		
	case ML_Error_NotInitialised:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NotInitialised");
#endif
		return RDIError_NotInitialised;
		
	case ML_Error_UnableToInitialise:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnableToInitialise");
#endif
		return RDIError_UnableToInitialise;
		
	case ML_Error_WrongByteSex:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_WrongByteSex");
#endif
		return RDIError_WrongByteSex;
		
	case ML_Error_UnableToTerminate:
#ifndef __LINUX
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnableToTerminate");
#endif
		return RDIError_UnableToTerminate;
		
	case ML_Error_BadInstruction:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadInstruction");
#endif
		return RDIError_BadInstruction;
		
	case ML_Error_IllegalInstruction:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_IllegalInstruction");
#endif
		return RDIError_IllegalInstruction;
		
	case ML_Error_BadCPUStateSetting:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadCPUStateSetting");
#endif
		return RDIError_BadCPUStateSetting;
		
	case ML_Error_UnknownCoPro:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnknownCoPro");
#endif
		return RDIError_UnknownCoPro;
		
	case ML_Error_UnknownCoProState:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnknownCoProState");
#endif
		return RDIError_UnknownCoProState;
		
	case ML_Error_BadCoProState:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadCoProState");
#endif
		return RDIError_BadCoProState;
		
	case ML_Error_BadPointType:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadPointType");
#endif
		return RDIError_BadPointType;
		
	case ML_Error_UnimplementedType:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnimplementedType");
#endif
		return RDIError_UnimplementedType;
		
	case ML_Error_BadPointSize:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadPointSize");
#endif
		return RDIError_BadPointSize;
		
	case ML_Error_UnimplementedSize:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnimplementedSize");
#endif
		return RDIError_UnimplementedSize;
		
	case ML_Error_NoMorePoints:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NoMorePoints");
#endif
		return RDIError_NoMorePoints;
		
	case ML_Error_BreakpointReached:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BreakpointReached");
#endif
		return RDIError_BreakpointReached;
		
	case ML_Error_WatchpointAccessed:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_WatchpointAccessed");
#endif
		return RDIError_WatchpointAccessed;
		
	case ML_Error_NoSuchPoint:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NoSuchPoint");
#endif
		return RDIError_NoSuchPoint;
		
	case ML_Error_ProgramFinishedInStep:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ProgramFinishedInStep");
#endif
		return RDIError_ProgramFinishedInStep;
		
	case ML_Error_UserInterrupt:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UserInterrupt");
#endif
		return RDIError_UserInterrupt;
		
	case ML_Error_CantSetPoint:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_CantSetPoint");
#endif
		return RDIError_CantSetPoint;
		
	case ML_Error_IncompatibleRDILevels:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_IncompatibleRDILevels");
#endif
		return RDIError_IncompatibleRDILevels;
		
	case ML_Error_CantLoadConfig:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_CantLoadConfig");
#endif
		return RDIError_CantLoadConfig;
		
	case ML_Error_BadConfigData:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadConfigData");
#endif
		return RDIError_BadConfigData;
		
	case ML_Error_NoSuchConfig:
#ifndef __LINUX
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NoSuchConfig");
#endif
		return RDIError_NoSuchConfig;
		
	case ML_Error_BufferFull:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BufferFull");
#endif
		return RDIError_BufferFull;
		
	case ML_Error_OutOfStore:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_OutOfStore");
#endif
		return RDIError_OutOfStore;
		
	case ML_Error_NotInDownload:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NotInDownload");
#endif
		return RDIError_NotInDownload;
		
	case ML_Error_PointInUse:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_PointInUse");
#endif
		return RDIError_PointInUse;
		
	case ML_Error_BadImageFormat:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadImageFormat");
#endif
		return RDIError_BadImageFormat;
		
	case ML_Error_TargetRunning:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_TargetRunning");
#endif
		return RDIError_TargetRunning;
		
	case ML_Error_DeviceWouldNotOpen:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_DeviceWouldNotOpen");
#endif
		return RDIError_DeviceWouldNotOpen;
		
	case ML_Error_NoSuchHandle:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NoSuchHandle");
#endif
		return RDIError_NoSuchHandle;
		
	case ML_Error_ConflictingPoint:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ConflictingPoint");
#endif
		return RDIError_ConflictingPoint;
		
	case ML_Error_TargetBroken:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_TargetBroken");
#endif
		return RDIError_TargetBroken;
		
	case ML_Error_TargetStopped:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_TargetStopped");
#endif
		return RDIError_TargetStopped;
		
	case ML_Error_BadValue:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadValue");
#endif
		return RDIError_BadValue;
		
	case ML_Error_Unset:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Unset");
#endif
		return RDIError_Unset;
		
	case ML_Error_NotAllowedWhilstExecuting:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NotAllowedWhilstExecuting");
#endif
		return RDIError_NotAllowedWhilstExecuting;
		
	case ML_Error_BadFormat:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BadFormat");
#endif
		return RDIError_BadFormat;
		
	case ML_Error_Executing:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Executing");
#endif
		return RDIError_Executing;
		
	case ML_Error_ExecutingLittleEndian:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ExecutingLittleEndian");
#endif
		return RDIError_ExecutingLittleEndian;
		
	case ML_Error_ExecutingBigEndian:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ExecutingBigEndian");
#endif
		return RDIError_ExecutingBigEndian;
		
	case ML_Error_ReentrantDuringProxy:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ReentrantDuringProxy");
#endif
		return RDIError_ReentrantDuringProxy;
		
	case ML_Error_Busy:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Busy");
#endif
		return RDIError_Busy;
		
	case ML_Error_NotExecuting:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NotExecuting");
#endif
		return RDIError_NotExecuting;
		
	case ML_Error_ProgramFinished:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ProgramFinished");
#endif
		return RDIError_ProgramFinished;
		
	case ML_Error_AlreadyExecuting:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_AlreadyExecuting");
#endif
		return RDIError_AlreadyExecuting;
		
	case ML_Error_NoSuchCallback:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NoSuchCallback");
#endif
		return RDIError_NoSuchCallback;
		
	case ML_Error_LinkTimeout:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_LinkTimeout");
#endif
		return RDIError_LinkTimeout;
		
	case ML_Error_OpenTimeout:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_OpenTimeout");
#endif
		return RDIError_OpenTimeout;
		
	case ML_Error_LinkDataError:
#ifndef __LINUX
		
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_LinkDataError");
#endif
		return RDIError_LinkDataError;
		
	case ML_Error_Interrupted:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_Interrupted");
#endif
		return RDIError_Interrupted;
		
	case ML_Error_NoModules:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_NoModules");
#endif
		return RDIError_NoModules;
		
	case ML_Error_LittleEndian:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_LittleEndian");
#endif
		return RDIError_LittleEndian;
		
	case ML_Error_BigEndian:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_BigEndian");
#endif
		return RDIError_BigEndian;
		
	case ML_Error_SoftInitialiseError:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_SoftInitialiseError");
#endif
		return RDIError_SoftInitialiseError;
		
	case ML_Error_ReadOnly:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_ReadOnly");
#endif
		return RDIError_ReadOnly;
		
	case ML_Error_InsufficientPrivilege:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_InsufficientPrivilege");
#endif
		return RDIError_InsufficientPrivilege;
		
	case ML_Error_UnimplementedMessage:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UnimplementedMessage");
#endif
		return RDIError_UnimplementedMessage;
		
	case ML_Error_UndefinedMessage:
#ifndef __LINUX
		
		if(bLogEnableOK)
			strcpy (szReturnValue,"RDIError_UndefinedMessage");
#endif
		return RDIError_UndefinedMessage;
		
  		case ML_Error_NoRTCKSupport:
#ifndef __LINUX
			
			if(bLogEnableOK)
				strcpy (szReturnValue,"RDIError_NoRTCKSupport");
#endif
			return RDIError_NoRTCKSupport;
			
		case ML_Error_NonEditablePoint:
#ifndef __LINUX
			
			if(bLogEnableOK)
				strcpy (szReturnValue,"RDIError_NonEditablePoint");
#endif
			return RDIError_NonEditablePoint;

		case ML_Error_DataAbort_Writing_Cache_Clean:
#ifndef __LINUX
			
			if(bLogEnableOK)
				strcpy (szReturnValue,"RDIError_DataAbort_Writing_Cache_Clean");
#endif
			return RDIError_DataAbort_Writing_Cache_Clean;

		default:
		case ML_ERROR_NO_ERROR: 
#ifndef __LINUX
			
			if(bLogEnableOK)
				strcpy (szReturnValue,"RDIError_NoError");    
#endif
			return RDIError_NoError;
			
      }
}

/****************************************************************************
Function: OpenUsbDeviceMoveProgress
Engineer: Suraj S
Input: unsigned char ucProgress - progress status (0 to 100)
Output: None
Description: Change progress by 10% when openning USB device
Date           Initials    Description
25-Sep-2007    SJ          Initial
07-Apr-2008	   RS	         Modified for GDBServerForARM
****************************************************************************/
void OpenUsbDeviceMoveProgress(unsigned char ucProgress)
{
#ifndef __LINUX
	if(!bGDBServer)
	{
		(void)((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetPos(ucProgress);
		pOpenUSBProgress->UpdateWindow();
	}
#endif
}


/****************************************************************************
Function: GetRDIDriverVersion
Engineer: Suraj S
Input: NONE
Output: const TCHAR* - RDI Driver version
Description: Returns the current RDI Driver version
Date           Initials    Description
28-Sep-2007     SJ          Initial
****************************************************************************/
const TCHAR* GetRDIDLLVersion(void)
{
	return pszRDIDriverVersion;
}


/****************************************************************************
Function: GetFirmwareInfo
Engineer: Suraj S
Input: NONE
Output: const TCHAR* - Firmware info(version number and date)
Description: Returns the current firmware information
Date           Initials    Description
28-Sep-2007     SJ          Initial
****************************************************************************/
const TCHAR* GetFirmwareInfo(void)
{
/*   char* pszVersion;
char* pszDate;
char pszFirmwareInfo[MAX_STRING_SIZE] = {0};

  pszVersion = (char*) malloc (MAX_STRING_SIZE);
  pszDate    = (char*) malloc (MAX_STRING_SIZE);
  
	strcpy(pszVersion, "");
	ConvertVersionString(tyDeviceInfo.ulFirmwareVersion, pszVersion);
	
	  strcpy(pszDate, "");
	  ConvertDateString(tyDeviceInfo.ulFirmwareDate, pszDate);
	  
		strcat(pszVersion," ");
		strcat(pszVersion,pszDate);
		strcpy(pszFirmwareInfo,pszVersion);
		
	return pszFirmwareInfo;*/
	return pszFirmwareVersion;
}
#ifndef __LINUX
/****************************************************************************
Function: GetSoftwareVersions
Engineer: Suraj S
Input: TyAshlingVersion - pointer to Version structure.
Output: int - Error Status
Description: Gets the versions of USB Driver, Diskware, Firmware and FPGA's
Date        Initials    Description
19-Oct-2007     SJ          Initial
****************************************************************************/
int  GetSoftwareVersions (TyAshlingVersion *tyVersionStructure)
{
	
	const usb_version *tyUSBVersion;
	//char pszUSBVersion[50];
	char pszString1[50];
	char pszString2[50];
	char *szDebuggerName;
	char  szFullPathandNameForConfigFile[_MAX_PATH];
	
	ML_GetDeviceInfo(&tyDeviceInfo);
	
	tyUSBVersion = usb_get_version();
	if((tyUSBVersion->driver.major == -1) | (tyUSBVersion->driver.minor == -1)|
		(tyUSBVersion->driver.micro == -1) | (tyUSBVersion->driver.nano == -1))
		sprintf(tyVersionStructure->szUSBDriverVersion, "USB Driver not loaded");
	else
		sprintf(tyVersionStructure->szUSBDriverVersion, "v%d.%d.%d.%d", tyUSBVersion->driver.major, tyUSBVersion->driver.minor,
		tyUSBVersion->driver.micro, tyUSBVersion->driver.nano);
	
	szDebuggerName = (char*) calloc (DEBUGGER_NAME_SIZE,sizeof(char));
	
	strcpy (szFullPathandNameForConfigFile, (char *)pszGlobalDllPathName);
	strcat (szFullPathandNameForConfigFile, "\\OpellaXD");
	
	COpellaConfig PassedConfigObj(szFullPathandNameForConfigFile);
	//szDebuggerName = PassedConfigObj.GetDebuggerName();
	PassedConfigObj.GetDebuggerName(szDebuggerName);
	
	strcpy (tyVersionStructure->szDiskWare, "");
	strcpy (tyVersionStructure->szFirmWare , "");
	strcpy (tyVersionStructure->szFpgaWare , "");
	strcpy (tyVersionStructure->szOpellaXDSerialNumber , "");
	strcpy (tyVersionStructure->szRDIDriverVersion, "");   
	strcpy (tyVersionStructure->szTPAVersion, "");
	strcpy (tyVersionStructure->szManufactured, "");
	
	if (tyTargetConfig.bUSBDevice)
	{      
		if((strcmp(szDebuggerName,"PATHFINDER") == 0) || (strcmp(szDebuggerName,"PFARMXD") == 0))
		{         
			strcpy (tyVersionStructure->szRDIDriverVersion, pszRDIDriverVersion);
			// Copy Serial Number
			strcpy(tyVersionStructure->szOpellaXDSerialNumber, tyDeviceInfo.pszSerialNumber);
			
			//Get TPA name
			//Get TPA version
			ConvertPcbVersionString(tyDeviceInfo.ulTpaRevision, pszString1);
			sprintf(tyVersionStructure->szTPAVersion,"%s, %s", tyDeviceInfo.pszTpaName, pszString1);	   
			
			//Get the Manufacturing date
			ConvertDateString(tyDeviceInfo.ulManufacturingDate, pszString1);
			//Get the Hardware revision
			ConvertPcbVersionString(tyDeviceInfo.ulBoardRevision, pszString2);
			sprintf(tyVersionStructure->szManufactured,"%s, %s", pszString1, pszString2);
			//strcpy(tyVersionStructure->szManufactured, pszString1); strcat(tyVersionStructure->szManufactured, ", "); strcat(tyVersionStructure->szManufactured, pszString2);
		}
		//Get the Firmware version
		ConvertVersionString(tyDeviceInfo.ulFirmwareVersion, pszString1);
		//Get the Firmware date
		ConvertDateString(tyDeviceInfo.ulFirmwareDate, pszString2);
		sprintf(tyVersionStructure->szFirmWare,"%s, %s", pszString1, pszString2);
		//strcpy(tyVersionStructure->szFirmWare, pszString1); strcat(tyVersionStructure->szFirmWare, ", "); strcat(tyVersionStructure->szFirmWare, pszString2);
		
		//Get the FPGAware version
		ConvertVersionString(tyDeviceInfo.ulFpgawareVersion, pszString1);
		//Get the FPGAware date
		ConvertDateString(tyDeviceInfo.ulFpgawareDate, pszString2);
		sprintf(tyVersionStructure->szFpgaWare,"%s, %s", pszString1, pszString2);
		//strcpy(tyVersionStructure->szFpgaWare, pszString1); strcat(tyVersionStructure->szFpgaWare, ", "); strcat(tyVersionStructure->szFpgaWare, pszString2);
		
		//Get the Diskware version
		ConvertVersionString(tyDeviceInfo.ulDiskwareVersion, pszString1);
		//Get the Diskware date
		ConvertDateString(tyDeviceInfo.ulDiskwareDate, pszString2);
		sprintf(tyVersionStructure->szDiskWare,"%s, %s", pszString1, pszString2);
		//strcpy(tyVersionStructure->szDiskWare, pszString1); strcat(tyVersionStructure->szDiskWare, ", "); strcat(tyVersionStructure->szDiskWare, pszString2);
	}
	if(szDebuggerName)
		free(szDebuggerName);

	return RDIError_NoError;
}
#endif
/****************************************************************************
Function: PerformRDIDiagnostics
Engineer: Suraj S
Input: *tyRdiDiagInfo - pointer to TyRDIDiagInfo structure
Output: TyError - Error code
Description: Performs RDI Diagnostics and give information to RDI GUI
Date           Initials    Description
28-Sep-2007     SJ          Initial
****************************************************************************/
/*TyError PerformRDIDiagnostics(char* pszSerialNumber, TyRDIDiagInfo *tyRdiDiagInfo)
{

  TyError tyError = ML_ERROR_NO_ERROR;
  const usb_version *tyUSBVersion;
  char pszUSBVersion[50];
  char szDiskwareFullPath[_MAX_PATH];
  char szFpgawareFullPath[_MAX_PATH];
  
	tyError = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
	
	  switch (tyError)
	  {
	  case DRVOPXD_ERROR_NO_ERROR:
	  break;
	  case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
	  return DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
	  default:
	  return DRVOPXD_ERROR_USB_DRIVER;
	  }
	  
		tyUSBVersion = usb_get_version();
		if((tyUSBVersion->driver.major == -1) | (tyUSBVersion->driver.minor == -1)|
		(tyUSBVersion->driver.micro == -1) | (tyUSBVersion->driver.nano == -1))
		sprintf(pszUSBVersion, "USB Driver not loaded");
		else
		sprintf(pszUSBVersion, "v%d.%d.%d.%d", tyUSBVersion->driver.major, tyUSBVersion->driver.minor,
		tyUSBVersion->driver.micro, tyUSBVersion->driver.nano);
		
		  //Get the RDI DLL version 
		  strcpy (tyRdiDiagInfo->pszDLLVersion, pszRDIDriverVersion);    
		  
			//Get the USB Driver version
			strcpy (tyRdiDiagInfo->pszUSBDriverVersion, pszUSBVersion);    
			
			  //Save the path of the DLL
			  ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);
			  
				// get full path to Opella-XD diskware and FPGAware for ARM
				strcpy(szDiskwareFullPath, szGlobalDLLPathName);
				strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
				strcpy(szFpgawareFullPath, szGlobalDLLPathName);
				strcat(szFpgawareFullPath, szOpXDFpgawareFilename);
				
				  (void)DL_OPXD_UnconfigureDevice(tyDiagDevHandle);
				  
					if (DL_OPXD_ConfigureDevice(tyDiagDevHandle, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
					{
					(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
					return DIAG_ERROR_OPELLA_FAILURE;
					
					  }
					  //Get device info
					  if (DL_OPXD_GetDeviceInfo(tyDiagDevHandle, &tyDeviceInfo) != DRVOPXD_ERROR_NO_ERROR)
					  {
					  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
					  return(DRVOPXD_ERROR_USB_DRIVER);
					  }
					  
						// Copy Serial Number
						strcpy(tyRdiDiagInfo->pszSerialNumber, tyDeviceInfo.pszSerialNumber);
						
						  tyRdiDiagInfo->bDwRunning = tyDeviceInfo.bDiskwareRunning;
						  //Get the Firmware version
						  ConvertVersionString(tyDeviceInfo.ulFirmwareVersion, tyRdiDiagInfo->pszFwVersion);
						  //Get the FPGAware version
						  ConvertVersionString(tyDeviceInfo.ulFpgawareVersion, tyRdiDiagInfo->pszFpgawareVersion);
						  //Get the Diskware version
						  ConvertVersionString(tyDeviceInfo.ulDiskwareVersion, tyRdiDiagInfo->pszDwVersion);
						  //Get the Firmware date
						  ConvertDateString(tyDeviceInfo.ulFirmwareDate, tyRdiDiagInfo->pszFwDate);
						  //Get the FPGAware date
						  ConvertDateString(tyDeviceInfo.ulFpgawareDate, tyRdiDiagInfo->pszFpgawareDate);
						  //Get the Diskware date
						  ConvertDateString(tyDeviceInfo.ulDiskwareDate, tyRdiDiagInfo->pszDwDate);
						  //Get the Hardware revision
						  ConvertPcbVersionString(tyDeviceInfo.ulBoardRevision, tyRdiDiagInfo->pszBoardRevision);
						  //Get the Manufacturing date
						  ConvertDateString(tyDeviceInfo.ulManufacturingDate, tyRdiDiagInfo->pszManufDate);
						  //Get TPA type connected
						  tyRdiDiagInfo->tyTpaConnected = tyDeviceInfo.tyTpaConnected;
						  //Get TPA name
						  strcpy(tyRdiDiagInfo->pszTpaName, tyDeviceInfo.pszTpaName);
						  //Get TPA version
						  ConvertPcbVersionString(tyDeviceInfo.ulTpaRevision, tyRdiDiagInfo->pszTpaRevision);
						  
							(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
							
							  return DRVOPXD_ERROR_NO_ERROR;
							  }
*/
/****************************************************************************
Function: PerformRDIDiagnosticsOnTarget
Engineer: Suraj S
Input: *tyRdiOnTargetDiagInfo - pointer to TyRDIOnTargetDiagInfo structure
Output: TyError - Error code
Description: Performs RDI Diagnostics on Target and give information to RDI GUI
Date           Initials    Description
08-Jan-2008      SJ         Initial
****************************************************************************/
/*TyError PerformRDIDiagnosticsOnTarget(char* pszSerialNumber,TyRDIOnTargetDiagInfo *tyRdiOnTargetDiagInfo)
{

  TyArmProcType TyProcType = NOCORE;
  TyError ErrRet = DIAG_ERROR_NO_ERROR;
  
	char szDiskwareFullPath[_MAX_PATH];
	char szFpgawareFullPath[_MAX_PATH];
	
	  unsigned long ulTempIDCode1;
	  unsigned long ulTempIDCode2;
	  unsigned long ulTempIDCode3;
	  unsigned char bTpaLoopTest = 0;
	  unsigned long pulOutDRBuffer[SCANCHAIN_ULONG_SIZE] = {0x2};
	  unsigned long pulInDRBuffer[SCANCHAIN_ULONG_SIZE] ;
	  unsigned long pulOutIRBuffer[SCANCHAIN_ULONG_SIZE] ={0xe};
	  unsigned long ulNumberOfBitsIR = {0x4};
	  unsigned long ulNumberOfBitsDR = {0x32};
	  unsigned long ulTempadder = 0;
	  unsigned long ulTempCoreID1 = 0;
	  unsigned long ulTempCoreID2 = 0;
	  unsigned long ulImplmntr = 0;
	  unsigned long ulPpno = 0;
	  unsigned long ulTapID = 0;
	  unsigned long ulTapID1 = 0;
	  unsigned int  uiNumberOfTAPs;
	  unsigned char ucarm7=0;
	  unsigned char ucarm9=0;
	  double dFreq = 1000.00;
	  unsigned long ulCp1Add = DBG_CONTROL_REG;
	  unsigned long ulCp1Data = SET_DBGR_BIT;
	  unsigned long ulTempCoreType = 0;
	  
		usDiagnostics = 1;
		
		  boolean bARM7StopProblem           = FALSE;
		  boolean bGlobalThumb         = FALSE;
		  boolean pbExecuting           = FALSE;
		  boolean bWatchPoint         = FALSE;
		  tyRdiOnTargetDiagInfo->szTgtVoltage = 0.00000;
		  tyRdiOnTargetDiagInfo->szRegCheck = 0;
		  tyRdiOnTargetDiagInfo->szTpaLBResult = 0;
		  tyRdiOnTargetDiagInfo->szCoreMake =0;
		  tyRdiOnTargetDiagInfo->szArm7=0;
		  tyRdiOnTargetDiagInfo->uiTargetTAPNo=0;
		  ulGlobalIDcode0 = 0;
		  ulGlobalIDcode1 = 0;
		  
			//Save the path of the DLL
			ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);
			
			  // get full path to Opella-XD diskware and FPGAware for ARM
			  strcpy(szDiskwareFullPath, szGlobalDLLPathName);
			  strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
			  strcpy(szFpgawareFullPath, szGlobalDLLPathName);
			  strcat(szFpgawareFullPath, szOpXDFpgawareFilename);
			  
				//First open the OpellaXD
				ErrRet = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
				switch (ErrRet)
				{
				case DIAG_ERROR_NO_ERROR:
				break;
				case DIAG_ERROR_INVALID_SERIAL_NUMBER:
				return DIAG_ERROR_INVALID_SERIAL_NUMBER;
				default:
				return DIAG_ERROR_USB_DRIVER;
				}
				
				  (void)DL_OPXD_UnconfigureDevice(tyDiagDevHandle);
				  
					if (DL_OPXD_ConfigureDevice(tyDiagDevHandle, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
					return DIAG_ERROR_OPELLA_FAILURE;
					
					  //Get device info
					  if (DL_OPXD_GetDeviceInfo(tyDiagDevHandle, &tyDeviceInfo) != DRVOPXD_ERROR_NO_ERROR)
					  {
					  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
					  return DIAG_ERROR_USB_DRIVER;
					  }
					  
						if (tyDeviceInfo.tyTpaConnected == TPA_NONE)
						{
						tyRdiOnTargetDiagInfo->tyTpaConnected = TPA_NONE;
						(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
						return DIAG_ERROR_TPA_NOT_CONNECTED;
						}
						
						  //Test for TPA LOOP BACK 
						  ErrRet = DL_OPXD_TpaLoopDiagnosticTest(tyDiagDevHandle, &bTpaLoopTest);
						  if (ErrRet != DIAG_ERROR_NO_ERROR)
						  {
						  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
						  return DIAG_ERROR_NO_ERROR;  //Just return from function. Handle the error in the GUI
						  }
						  if (bTpaLoopTest) 
						  tyRdiOnTargetDiagInfo->szTpaLBResult = 1;
						  
							// Getting Target Volt
							dGlobalTgtvolt = tyDeviceInfo.dCurrentVtref; 
							tyRdiOnTargetDiagInfo->szTgtVoltage=tyDeviceInfo.dCurrentVtref;
							
							  if (tyRdiOnTargetDiagInfo->szTgtVoltage == 0)
							  {
							  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
							  return DIAG_ERROR_NO_ERROR; //Just return from function. Handle the error in the GUI
							  }
							  
								tyTargetConfig.ulDebugCoreNumber = tyRdiOnTargetDiagInfo->ulDbgCoreNumber;
								
								  //Initialise the debugger hardware
								  ErrRet = ML_Initialise_Debugger_Third_party(tyDiagDevHandle, dFreq, tyRdiOnTargetDiagInfo->szTgtVoltage);
								  if (ErrRet != DIAG_ERROR_NO_ERROR)
								  {
								  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
								  return DIAG_ERROR_OPELLA_FAILURE;
								  }
								  
									//Getting total No of cores  
									tyRdiOnTargetDiagInfo->ulTotCores = tyTargetConfig.ulSelectedNoOfCores;
									
									  //Getting multi core IDcodes
									  if ((tyTargetConfig.ulSelectedNoOfCores) > 1)
									  {
									  ErrRet = DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 4 , pulOutIRBuffer ,32 , pulOutDRBuffer,pulInDRBuffer);
									  if (ErrRet != DIAG_ERROR_NO_ERROR)
									  {
									  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
									  return DIAG_ERROR_TAPID1_FAILED; 
									  }
									  
										tyRdiOnTargetDiagInfo->ulIDCode = *pulInDRBuffer;
										ulTempIDCode1 = tyRdiOnTargetDiagInfo->ulIDCode;
										ulTempIDCode2 = (ulTempIDCode1 & 0x00000fff);                       
										ulTempIDCode3 = (ulTempIDCode1 & 0xf0000000);
										
										  if((ulTempIDCode2 == 0x00000f0f) && (ulTempIDCode3 != 0x10000000 )&& (ulTempIDCode3 != 0x40000000 ) )
										  { 
										  TyProcType = ARM7;
										  ucarm7 = 1;
										  }
										  else if((ulTempIDCode2 == 0x00000f0f) && (ulTempIDCode3 == 0x40000000 ))
										  {
										  TyProcType = ARM7S;
										  ucarm7 = 1;
										  }
										  else
										  {
										  TyProcType = ARM9;
										  ucarm9 = 1;
										  }
										  
											//Getting IR length 
											for (unsigned long k=0;k < (tyTargetConfig.ulSelectedNoOfCores);k++)
											ulTempadder += pulIRLengthArray[k];
											
											  tyRdiOnTargetDiagInfo->ulIRLen = ulTempadder;
											  }
											  else
											  {
											  //Getting Single IDCODE
											  ErrRet = DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, ulNumberOfBitsIR, pulOutIRBuffer,ulNumberOfBitsDR, pulOutDRBuffer,pulInDRBuffer);
											  if (ErrRet != DIAG_ERROR_NO_ERROR)
											  {
											  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
											  return DIAG_ERROR_TAPID_FAILED; 
											  }
											  
												tyRdiOnTargetDiagInfo->ulIDCode = *pulInDRBuffer;
												
												  tyRdiOnTargetDiagInfo->ulIRLen = *(pulIRLengthArray);
												  ulTempIDCode1 = tyRdiOnTargetDiagInfo->ulIDCode;
												  ulTempIDCode2 = (ulTempIDCode1 & 0x00000fff);                       
												  ulTempIDCode3 = (ulTempIDCode1 & 0xf0000000);
												  
													if((ulTempIDCode2 == 0x00000f0f) && (ulTempIDCode3 != 0x10000000 )&& (ulTempIDCode3 != 0x40000000 ) )
													{ 
													TyProcType = ARM7;
													ucarm7 = 1;
													}
													else if((ulTempIDCode2 == 0x00000f0f) && (ulTempIDCode3 == 0x40000000 ))
													{
													TyProcType = ARM7S;
													ucarm7 = 1;
													}
													else
													{
													TyProcType = ARM9;
													ucarm9 = 1;
													}
													}
													
													  if (0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM7DI")))
													  tyTargetConfig.tyProcType = ARM7DI;
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM7TDMI")))
													  tyTargetConfig.tyProcType = ARM7TDMI;
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM7TDMI-S")))
													  tyTargetConfig.tyProcType = ARM7TDMIS;
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM720T")))
													  tyTargetConfig.tyProcType = ARM720T;
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM740T")))
													  tyTargetConfig.tyProcType = ARM740T;
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM9TDMI")))
													  tyTargetConfig.tyProcType = ARM9TDMI;
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM920T")))
													  {
													  tyTargetConfig.tyProcType = ARM920T;
													  ulTempCoreType = 0x920;
													  }
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM922T")))
													  {
													  tyTargetConfig.tyProcType = ARM922T;
													  ulTempCoreType = 0x922;
													  }
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM926EJ-S")))
													  {
													  tyTargetConfig.tyProcType = ARM926EJS;
													  ulTempCoreType = 0x926;
													  }
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM940T")))
													  {
													  tyTargetConfig.tyProcType = ARM940T;
													  ulTempCoreType = 0x940;
													  }
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM946E-S")))
													  {
													  tyTargetConfig.tyProcType = ARM946ES;
													  ulTempCoreType = 0x946;
													  }
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM966E-S")))
													  {
													  tyTargetConfig.tyProcType = ARM966ES; 
													  ulTempCoreType = 0x966;
													  }
													  else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM968E-S")))
													  {
													  tyTargetConfig.tyProcType = ARM968ES; 
													  ulTempCoreType = 0x968;
													  }
													  
														switch(tyTargetConfig.tyProcType)
														{
														case ARM720T:
														ErrRet = CL_ReadARM720CoProcRegisters  ();
														if (ErrRet != DIAG_ERROR_NO_ERROR)
														{
														(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
														return DIAG_ERROR_CPREG_FAILED;
														}
														
														  tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
														  tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
														  ucarm7=1;
														  break;
														  
															case ARM740T:
															ErrRet = CL_ReadARM740CoProcRegisters  ();
															if (ErrRet != DIAG_ERROR_NO_ERROR)
															return DIAG_ERROR_CPREG_FAILED;
															
															  tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
															  tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
															  ucarm7=1;
															  break;
															  
																case ARM920T:
																case ARM922T:
																ErrRet = DL_OPXD_Arm_SelectScanChain(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 2, 1, TyProcType);
																if (ErrRet != DIAG_ERROR_NO_ERROR)
																{
																(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																return DIAG_ERROR_OPELLA_FAILURE;
																}
																else 
																ErrRet = DL_OPXD_Arm_WriteICEBreaker(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, ulCp1Add, &ulCp1Data ); 
																if (ErrRet != DIAG_ERROR_NO_ERROR)
																{
																(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																return DIAG_ERROR_OPELLA_FAILURE;
																}
																else 
																ErrRet = DL_OPXD_Arm_ReadICEBreaker(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, DBG_STATUS_REG, &ulCp1Data);
																if (ErrRet != DIAG_ERROR_NO_ERROR)
																{
																(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																return DIAG_ERROR_OPELLA_FAILURE;
																}
																else 
																ErrRet = DL_OPXD_Arm_Status_Proc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType, 0, 1, 
																&bARM7StopProblem, &pbExecuting, &bGlobalThumb, &bWatchPoint);
																if (ErrRet != DIAG_ERROR_NO_ERROR)
																{
																(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																return DIAG_ERROR_OPELLA_FAILURE;
																}
																else 
																ErrRet =  CL_ReadARM920_922CoProcRegisters  ();
																if (ErrRet != DIAG_ERROR_NO_ERROR)
																{
																(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																return DIAG_ERROR_CPREG_FAILED;
																}
																
																  tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
																  tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
																  ucarm9=1;
																  break;
																  
																	case ARM926EJS:
																	ErrRet = DL_OPXD_Arm_SelectScanChain(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 2, 1, TyProcType);
																	if (ErrRet != DIAG_ERROR_NO_ERROR)
																	{
																	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																	return DIAG_ERROR_OPELLA_FAILURE;
																	}
																	else 
																	ErrRet = DL_OPXD_Arm_WriteICEBreaker(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, ulCp1Add, &ulCp1Data ); 
																	if (ErrRet != DIAG_ERROR_NO_ERROR)
																	{
																	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																	return DIAG_ERROR_OPELLA_FAILURE;
																	}
																	else 
																	ErrRet = DL_OPXD_Arm_ReadICEBreaker(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, DBG_STATUS_REG, &ulCp1Data);
																	if (ErrRet != DIAG_ERROR_NO_ERROR)
																	{
																	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																	return DIAG_ERROR_OPELLA_FAILURE;
																	}
																	else 
																	ErrRet = DL_OPXD_Arm_Status_Proc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType, 0, 1, 
																	&bARM7StopProblem, &pbExecuting, &bGlobalThumb, &bWatchPoint);
																	if (ErrRet != DIAG_ERROR_NO_ERROR)
																	{
																	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																	return DIAG_ERROR_OPELLA_FAILURE;
																	}
																	else 
																	ErrRet =   CL_ReadARM926CoProcRegisters  ();
																	if (ErrRet != DIAG_ERROR_NO_ERROR)
																	{
																	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																	return DIAG_ERROR_CPREG_FAILED;
																	}
																	
																	  tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
																	  tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
																	  ucarm9=1;
																	  break;
																	  
																		case ARM940T:
																		ErrRet =   CL_ReadARM940_946CoProcRegisters  ();
																		if (ErrRet != DIAG_ERROR_NO_ERROR)
																		{
																		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																		return DIAG_ERROR_CPREG_FAILED;
																		}
																		tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
																		tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
																		ucarm9=1;
																		break;
																		
																		  case ARM946ES:
																		  ErrRet =   CL_ReadARM940_946CoProcRegisters  ();
																		  if (ErrRet != DIAG_ERROR_NO_ERROR)
																		  {
																		  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																		  return DIAG_ERROR_CPREG_FAILED;
																		  }
																		  
																			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
																			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
																			ucarm9=1;
																			break;
																			
																			  case ARM966ES:
																			  ErrRet = DL_OPXD_Arm_SelectScanChain(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 2, 1, TyProcType);
																			  if (ErrRet != DIAG_ERROR_NO_ERROR)
																			  {
																			  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																			  return DIAG_ERROR_OPELLA_FAILURE;
																			  }
																			  else 
																			  ErrRet = DL_OPXD_Arm_WriteICEBreaker(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, ulCp1Add, &ulCp1Data ); 
																			  if (ErrRet != DIAG_ERROR_NO_ERROR)
																			  {
																			  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																			  return DIAG_ERROR_OPELLA_FAILURE;
																			  }
																			  else 
																			  ErrRet = DL_OPXD_Arm_ReadICEBreaker(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, DBG_STATUS_REG, &ulCp1Data);
																			  if (ErrRet != DIAG_ERROR_NO_ERROR)
																			  {
																			  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																			  return DIAG_ERROR_OPELLA_FAILURE;
																			  }
																			  else 
																			  ErrRet = DL_OPXD_Arm_Status_Proc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType, 0, 1, 
																			  &bARM7StopProblem, &pbExecuting, &bGlobalThumb, &bWatchPoint);
																			  if (ErrRet != DIAG_ERROR_NO_ERROR)
																			  {
																			  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																			  return DIAG_ERROR_OPELLA_FAILURE;
																			  }
																			  else
																			  ErrRet =   CL_ReadARM966CoProcRegisters  ();
																			  if (ErrRet != DIAG_ERROR_NO_ERROR)
																			  {
																			  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																			  return DIAG_ERROR_CPREG_FAILED;
																			  }
																			  
																				tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
																				tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
																				ucarm9=1;
																				break;
																				
																				  case ARM968ES:
																				  ErrRet =   CL_ReadARM968CoProcRegisters  ();
																				  if (ErrRet != DIAG_ERROR_NO_ERROR)
																				  {
																				  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																				  return DIAG_ERROR_CPREG_FAILED;
																				  }
																				  tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
																				  tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
																				  ucarm9=1;
																				  break;
																				  
																					default:
																					tyRdiOnTargetDiagInfo->ulCoreID = 0;
																					tyRdiOnTargetDiagInfo->ulCpReg1 = 0;
																					break;
																					}
																					
																					  //Getting Architecture of core
																					  ulTempCoreID1=tyRdiOnTargetDiagInfo->ulCoreID;
																					  if (ucarm7==1) 
																					  { 
																					  ulTempCoreID2 = ((ulTempCoreID1 & 0x00800000)>>23);
																					  if (ulTempCoreID2==1)
																					  strcpy(tyRdiOnTargetDiagInfo->pszArch,"4T");
																					  else
																					  strcpy(tyRdiOnTargetDiagInfo->pszArch,"3");
																					  
																						ulTapID = tyRdiOnTargetDiagInfo->ulIDCode;
																						ulTapID1 = (ulTapID & 0x00000fff);
																						if ((ulTapID1==0x00000f0f) || (ulTapID1==0x00000061))
																						tyRdiOnTargetDiagInfo->szArm7=1;
																						}
																						else if (tyProcessorConfig.bARM7==1)
																						{
																						ulTapID = tyRdiOnTargetDiagInfo->ulIDCode;
																						ulTapID1 = (ulTapID & 0x00000fff);
																						if ((ulTapID1==0x00000f0f) || (ulTapID1==0x00000061))
																						tyRdiOnTargetDiagInfo->szArm7=1;
																						}
																						else if (ucarm9==1) 
																						{ 
																						ulTempCoreID2 = ((ulTempCoreID1 & 0x000f0000)>>16);
																						ulImplmntr = ((ulTempCoreID1 & 0xff000000)>>24); 
																						ulPpno = ((ulTempCoreID1 & 0x0000fff0)>>4);
																						
																						  if(ulPpno != ulTempCoreType) 
																						  
																							{
																							(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																							return DIAG_ERROR_CPREG_FAILED;
																							}
																							
																							  switch(ulTempCoreID2)
																							  {
																							  case 1:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"4");
																							  break;
																							  case 2:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"4T");
																							  break;
																							  case 3:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"5");
																							  break;
																							  case 4:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"5T");
																							  break;
																							  case 5:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"5TE");
																							  break;
																							  case 6:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"5TEJ");
																							  break;
																							  case 7:
																							  strcpy(tyRdiOnTargetDiagInfo->pszArch,"6");
																							  break;
																							  }
																							  
																								if(ulImplmntr==0x00000041)
																								{
																								tyRdiOnTargetDiagInfo->szCoreMake = 1;
																								tyRdiOnTargetDiagInfo->ulPartno = ulPpno;
																								}
																								}
																								
																								  //Getting Cache Detailes
																								  ulTempCoreID1=tyRdiOnTargetDiagInfo->ulCpReg1;
																								  // for data cache
																								  if (ucarm9==1) 
																								  { 
																								  ulTempCoreID2 = ((ulTempCoreID1 & 0x003c0000)>>18);
																								  switch (ulTempCoreID2)
																								  {
																								  case 0:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"0KB");
																								  break;
																								  case 3:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"4KB");
																								  break;
																								  case 4:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"8KB");
																								  break;
																								  case 5:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"16KB");
																								  break;
																								  case 6:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"32KB");
																								  break;
																								  case 7:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"64KB");
																								  break;
																								  case 8:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"128KB");
																								  break;
																								  case 9:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"256KB");
																								  break;
																								  case 10:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"512KB");
																								  break;
																								  case 11:
																								  strcpy(tyRdiOnTargetDiagInfo->pszDCache,"1MB");
																								  break;
																								  }
																								  }
																								  
																									// for Instruction cache
																									if (ucarm9==1) 
																									{ 
																									ulTempCoreID2 = ((ulTempCoreID1 & 0x000003c0)>>6);
																									switch (ulTempCoreID2)
																									{
																									case 0:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"0KB");
																									break;
																									case 3:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"4KB");
																									break;
																									case 4:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"8KB");
																									break;
																									case 5:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"16KB");
																									break;
																									case 6:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"32KB");
																									break;
																									case 7:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"64KB");
																									break;
																									case 8:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"128KB");
																									break;
																									case 9:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"256KB");
																									break;
																									case 10:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"512KB");
																									break;
																									case 11:
																									strcpy(tyRdiOnTargetDiagInfo->pszICache,"1MB");
																									break;
																									}
																									}
																									
																									  //Testing opella loop back
																									  if (TestTargetLoopBack(&uiNumberOfTAPs)) 
																									  tyRdiOnTargetDiagInfo->uiTargetTAPNo = uiNumberOfTAPs;
																									  else
																									  {
																									  tyRdiOnTargetDiagInfo->uiTargetTAPNo = 0;
																									  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																									  return DIAG_ERROR_TAP_LB_FAILED;
																									  }
																									  
																										//Register Read/Write Test - The error is handled in the GUI
																										ErrRet = DL_OPXD_Arm_RegisterTest(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType,&(tyRdiOnTargetDiagInfo->szRegCheck));
																										if (ErrRet != DIAG_ERROR_NO_ERROR)
																										{
																										(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																										return DIAG_ERROR_OPELLA_FAILURE;
																										}
																										
																										  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
																										  
																											usDiagnostics = 0;
																											
																											  return DRVOPXD_ERROR_NO_ERROR;
}*/

/****************************************************************************
Function: TestTargetLoopBack
Engineer: Suraj S
Input : unsigned int *puiNumberOfTAPs - pointer to value for number of TAPs on SC
Output: unsigned char - TRUE if scan chain is OK
Description: test scanchain, shifts patterns 0xaa, 0x55, 0xcc, 0x33
checks number of TAPs detected in scanchain
Date           Initials    Description
08-Jan-2008       SJ        Initial
****************************************************************************/
/*unsigned char TestTargetLoopBack(unsigned int *puiNumberOfTAPs)
{
unsigned int uiShiftIndex;
unsigned long pulAAPattern[TSTSC_ULONGS];
unsigned long pul55Pattern[TSTSC_ULONGS];
unsigned long pulRandomPattern[TSTSC_ULONGS];
unsigned long pulOutValue1[TSTSC_ULONGS];
unsigned long pulOutValue2[TSTSC_ULONGS];
unsigned long pulOutValue3[TSTSC_ULONGS];

  // restrict SC length for this test
  // define bypass pattern, 0xAA pattern and 0x55 pattern
  memset((void *)pul55Pattern, 0x55, TSTSC_ULONGS * sizeof(unsigned long));
  memset((void *)pulAAPattern, 0xAA, TSTSC_ULONGS * sizeof(unsigned long));
  for (uiShiftIndex=0; uiShiftIndex < TSTSC_ULONGS; uiShiftIndex++)
  pulRandomPattern[uiShiftIndex] = (unsigned long)rand();
  
	// shift all 1s to scanchain
	(void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, NULL, NULL);
	// shift bypass to IR (DR == 1 bit) and shift AA pattern to DR
	
	  (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, pulAAPattern, pulOutValue1);
	  (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, pulRandomPattern, pulOutValue3);
	  (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, pul55Pattern, pulOutValue2);
	  
		// now shift results till get patterns, number of shifts == number of TAPs
		*puiNumberOfTAPs = 0;
		for(uiShiftIndex=0; uiShiftIndex < (64); uiShiftIndex++)
		{
		if(CompareUlongs_Tap(pulAAPattern, pulOutValue1, TSTSC_LENGTH - uiShiftIndex) &&
		CompareUlongs_Tap(pul55Pattern, pulOutValue2, TSTSC_LENGTH - uiShiftIndex) &&
		CompareUlongs_Tap(pulRandomPattern, pulOutValue3, TSTSC_LENGTH - uiShiftIndex)
		)
		{
		// both patterns shifted by the same number of bits revealed on output
		*puiNumberOfTAPs = uiShiftIndex;
		return TRUE;
		}
		// shift all arrays
		ShiftUlongArray_Tap(pulOutValue1, TSTSC_ULONGS, 1, true);
		ShiftUlongArray_Tap(pulOutValue2, TSTSC_ULONGS, 1, true);
		ShiftUlongArray_Tap(pulOutValue3, TSTSC_ULONGS, 1, true);
		}
		// we could not detect scan chain correctly
		return FALSE;
		}
*/
/****************************************************************************
Function: CompareUlongs_Tap
Engineer: Suraj S
Input: unsigned long *pulUlongs1   - first array of ulongs to compare
unsigned long *pulUlongs2   - second array of ulongs to compare
unsigned int uiNumberOfBits - number of bits from ulongs to compare
Output: unsigned char - TRUE if ulongs are identical in specified bits
Description: compares to ulong arrays with specified number of bits
Date           Initials    Description
08-Jan-2008       SJ       Initial
****************************************************************************/
/*unsigned char CompareUlongs_Tap(unsigned long *pulUlongs1, unsigned long *pulUlongs2, unsigned int uiNumberOfBits)
{
unsigned int uiIndex;
unsigned long ulMask;

  for(uiIndex=0; uiIndex < (uiNumberOfBits / 32); uiIndex++)
  {     // compare whole ulongs
  if(*pulUlongs1++ != *pulUlongs2++)
  return FALSE;
  }
  
	uiNumberOfBits = uiNumberOfBits % 32;
	// compare rest of ulong
	if(uiNumberOfBits == 0)
	return TRUE;
	ulMask = 0xffffffff >> (32 - uiNumberOfBits);
	if((*pulUlongs1 & ulMask) != (*pulUlongs2 & ulMask))
	return FALSE;
	
	  return TRUE;
	  }
*/

/****************************************************************************
Function: ShiftUlongArray_Tap
Engineer: Suraj S
Input: unsigned long *pulUlongArray  : array of ulongs
unsigned int uiNumberOfUlongs : number of ulongs in array
unsigned int uiNumberOfBits   : number of bits to be shifted
unsigned char bShiftToRight   : TRUE when shifting right (FALSE for left)
Output: none
Description: shift array of ulongs to right/left (maximum number of  bits to shift is 31)
Date           Initials    Description
18-Jan-2008      SJ        Initial
****************************************************************************/
/*void ShiftUlongArray_Tap(unsigned long *pulUlongArray, unsigned int uiNumberOfUlongs,
unsigned int uiNumberOfBits, unsigned char bShiftToRight)
{
unsigned long ulTempValue, ulTempShift;
unsigned int uiActUlong;

  // check range of parameters, do nothing if incorrect
  if((uiNumberOfUlongs < 1) || (uiNumberOfBits < 1) || (uiNumberOfBits > 31))
  return;
  
	ulTempShift = 0;
	if(bShiftToRight)
	{
	// shifting right
	for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
	{
	ulTempValue = pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] & (0xffffffff >> (32 - uiNumberOfBits));
	pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] = (pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] >> uiNumberOfBits) 
	| ulTempShift;
	ulTempShift = ulTempValue << (32 - uiNumberOfBits);
	}
	}
	else
	{
	// shifting left
	for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
	{
	ulTempValue = pulUlongArray[uiActUlong] & (0xffffffff << (32 - uiNumberOfBits));
	pulUlongArray[uiActUlong] = (pulUlongArray[uiActUlong] << uiNumberOfBits) 
	| ulTempShift;
	ulTempShift = ulTempValue >> (32 - uiNumberOfBits);
	}
	}
	}
*/
/****************************************************************************
Function: PerformTargetHardReset
Engineer: Suraj S
Input: TyRDIHardResetDiagInfo *tyRDIHardResetDiagInfo
Output: TyError - Error code
Description: Performs Hard Reset On Target
Date           Initials    Description
18-Jan-2008       SJ       Initial
****************************************************************************/
/*TyError PerformTargetHardReset(char* pszSerialNumber,TyRDIHardResetDiagInfo *tyRDIHardResetDiagInfo)
{
TyError ErrRet = ML_ERROR_NO_ERROR;
tyRDIHardResetDiagInfo->ucTgtAssertResetStatus = 0;
tyRDIHardResetDiagInfo->ucTgtDeassertResetStatus = 1;

  unsigned char ucTempTgtSts1 = NULL;
  unsigned char ucTempTgtSts2 = NULL;
  double dFreq = 1000.00; 
  
	ErrRet = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
	switch (ErrRet)
	{
	case DRVOPXD_ERROR_NO_ERROR:
	break;
	case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
	return DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
	default:
	return DRVOPXD_ERROR_USB_DRIVER;
	}
	ErrRet = ML_Initialise_Debugger_Third_party(tyDiagDevHandle,dFreq,dGlobalTgtvolt);
	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
	{
	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
	return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
	}
	ErrRet = DL_OPXD_Arm_ResetProc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 1, &ucTempTgtSts1,NULL );
	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
	{
	(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
	return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
	}
	else
	tyRDIHardResetDiagInfo->ucTgtAssertResetStatus = ucTempTgtSts1;
	
	  ML_Delay(100);
	  
		ErrRet = DL_OPXD_Arm_ResetProc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 0, NULL, NULL);
		ML_Delay(400);
		ErrRet = DL_OPXD_Arm_ResetProc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 0, &ucTempTgtSts2, NULL);
		if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		{
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
		}
		else
		tyRDIHardResetDiagInfo->ucTgtDeassertResetStatus = ucTempTgtSts2;
		
		  //Progress bar
		  pOpenUSBProgress = new CProgressControl;
		  (void)pOpenUSBProgress->ShowWindow(SW_SHOW);
		  ((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 20);
		  pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Asserting Reset....");
		  
			for (unsigned char j=0; j < 50; j++)
			{
			OpenUsbDeviceMoveProgress(j);
			ML_Delay(5);
			}
			
			  (void)pOpenUSBProgress->ShowWindow(SW_HIDE);
			  delete pOpenUSBProgress;
			  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
			  
				ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
				if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
				return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
				
				  return ErrRet;
				  }
*/
/****************************************************************************
Function: ConvertVersionString
Engineer: Suraj S
Input: unsigned long ulVersion - version
char *pszVersionString - buffer for version string
Output: none
Description: convert version format to string
Date           Initials    Description
28-Sep-2007     SJ          Initial
****************************************************************************/
/*void ConvertVersionString(unsigned long ulVersion, char *pszVersionString)
{
unsigned int uiVal1, uiVal2, uiVal3;

  //assert(pszVersionString != NULL);
  // decode firmware version
  uiVal1 = (unsigned int)((ulVersion & 0xff000000) >> 24);
  uiVal2 = (unsigned int)((ulVersion & 0x00ff0000) >> 16);
  uiVal3 = (unsigned int)((ulVersion & 0x0000ff00) >> 8);
  if (ulVersion & 0x000000ff)
  sprintf(pszVersionString, "v%x.%x.%x-%c", uiVal1, uiVal2, uiVal3, (char)(ulVersion & 0x000000ff));
  else
  sprintf(pszVersionString, "v%x.%x.%x", uiVal1, uiVal2, uiVal3);
  }
*/
/****************************************************************************
Function: ConvertDateString
Engineer: Suraj S
Input: unsigned long ulDate - date
char *pszDateString - buffer for date string
Output: none
Description: convert date format to string
Date           Initials    Description
28-Sep-2007     SJ          Initial
****************************************************************************/
/*void ConvertDateString(unsigned long ulDate, char *pszDateString)
{
unsigned int uiVal1, uiVal2, uiVal3;

  //assert(pszDateString != NULL);
  uiVal1 = (unsigned int)((ulDate & 0xff000000) >> 24);
  uiVal2 = (unsigned int)((ulDate & 0x00ff0000) >> 16);
  uiVal3 = (unsigned int)(ulDate & 0x0000ffff);
  switch (uiVal2)
  {
  case 1:
  sprintf(pszDateString, "%x-Jan-%x", uiVal1, uiVal3);
  break;
  case 2:
  sprintf(pszDateString, "%x-Feb-%x", uiVal1, uiVal3);
  break;
  case 3:
  sprintf(pszDateString, "%x-Mar-%x", uiVal1, uiVal3);
  break;
  case 4:
  sprintf(pszDateString, "%x-Apr-%x", uiVal1, uiVal3);
  break;
  case 5:
  sprintf(pszDateString, "%x-May-%x", uiVal1, uiVal3);
  break;
  case 6:
  sprintf(pszDateString, "%x-Jun-%x", uiVal1, uiVal3);
  break;
  case 7:
  sprintf(pszDateString, "%x-Jul-%x", uiVal1, uiVal3);
  break;
  case 8:
  sprintf(pszDateString, "%x-Aug-%x", uiVal1, uiVal3);
  break;
  case 9:
  sprintf(pszDateString, "%x-Sep-%x", uiVal1, uiVal3);
  break;
  case 10:
  sprintf(pszDateString, "%x-Oct-%x", uiVal1, uiVal3);
  break;
  case 11:
  sprintf(pszDateString, "%x-Nov-%x", uiVal1, uiVal3);
  break;
  default:
  sprintf(pszDateString, "%x-Dec-%x", uiVal1, uiVal3);
  break;
  }
  }
*/
/****************************************************************************
Function: ConvertPcbVersionString
Engineer: Suraj S
Input: unsigned long ulVersion - version
char *pszVersionString - buffer for version string
Output: none
Description: convert PCB version to string
Date           Initials    Description
20-Oct-2007    SJ          Initial
****************************************************************************/
/*void ConvertPcbVersionString(unsigned long ulVersion, char *pszVersionString)
{
unsigned int uiRVal, uiEVal, uiMVal;

  // decode firmware version
  if (ulVersion & 0x000000FF)
  {  // lowest byte is not 0, set all to 0s
  uiRVal = 0;
  uiEVal = 0;
  uiMVal = 0;
  }
  else
  {  // lowest byte is 0 as expected
  uiRVal = (unsigned int)((ulVersion & 0xFF000000) >> 24);
  uiEVal = (unsigned int)((ulVersion & 0x00FF0000) >> 16);
  uiMVal = (unsigned int)((ulVersion & 0x0000FF00) >> 8);
  }
  sprintf(pszVersionString, "R%d-M%d-E%d", uiRVal, uiMVal, uiEVal);
}*/
/****************************************************************************
Function: GetInfoType
Engineer: Jeenus Chalattu Kunnath
Input: unsigned Infotype
Output: void
Description: To get the info type
Date           Initials    Description
18-Feb-2008    SJ          Initial
****************************************************************************/
void GetInfoType(char info_call[50],unsigned Infotype)
{	  
	switch(Infotype)
	{
	default:
		strcpy(info_call,"UnknownType");
		break;
	case RDIInfo_DownLoad:
		strcpy(info_call,"RDIInfo_DownLoad");
		break;
	case RDIInfo_AllowedWhilstExecuting:
		strcpy(info_call,"RDIInfo_AllowedWhilstExecuting");
		break;
	case RDIInfo_IOBitsSupported:
		strcpy(info_call,"RDIInfo_IOBitsSupported");
		break;
	case RDIInfo_SemiHosting:
		strcpy(info_call,"RDIInfo_SemiHosting");
		break;
	case RDIInfo_SemiHostingSetARMSWI:
		strcpy(info_call,"RDIInfo_SemiHostingSetARMSWI");
		break;
	case RDIInfo_SemiHostingGetARMSWI:
		strcpy(info_call,"RDIInfo_SemiHostingGetARMSWI");
		break;
	case RDIInfo_SemiHostingSetThumbSWI:
		strcpy(info_call,"RDIInfo_SemiHostingSetThumbSWI");
		break;
	case RDIInfo_SemiHostingGetThumbSWI:
		strcpy(info_call,"RDIInfo_SemiHostingGetThumbSWI");
		break;
	case RDIInfo_SafeNonVectorAddress:
		strcpy(info_call,"RDIInfo_SafeNonVectorAddress");
		break;
	case RDIInfo_CanTargetExecute:
		strcpy(info_call,"RDIInfo_CanTargetExecute");
		break;
	case RDIInfo_CoPro:	
		strcpy(info_call,"RDIInfo_CoPro");
		break;
	case RDIInfo_DescribeCoPro:	
		strcpy(info_call,"RDIInfo_DescribeCoPro");
		break;
	case RDIInfo_Icebreaker:
		strcpy(info_call,"RDIInfo_Icebreaker");
		break;
	case RDISemiHosting_DCC:
		strcpy(info_call,"RDISemiHosting_DCC");
		break;
	case RDIInfo_CanForceSystemReset  : 
		strcpy(info_call,"RDIInfo_CanForceSystemReset");
		break;
	case RDISemiHosting_SetVector:
		strcpy(info_call,"RDISemiHosting_SetVector");
		break;
	case RDISemiHosting_GetVector:
		strcpy(info_call,"RDISemiHosting_GetVector");
		break;
	case RDISemiHosting_SetARMSWI:
		strcpy(info_call,"RDISemiHosting_SetARMSWI");
		break;
	case RDISemiHosting_GetARMSWI:
		strcpy(info_call,"RDISemiHosting_GetARMSWI");
		break;
	case RDISemiHosting_GetThumbSWI:
		strcpy(info_call,"RDISemiHosting_GetThumbSWI");
		break;
	case RDISemiHosting_SetThumbSWI:
		strcpy(info_call,"RDISemiHosting_SetThumbSWI");
		break;
	case RDISemiHosting_SetState:
		strcpy(info_call,"RDISemiHosting_SetState");
		break;
	case RDISemiHosting_GetState:
		strcpy(info_call,"RDISemiHosting_GetState");
		break;
	case RDISemiHosting_SetDCCHandlerAddress:
		strcpy(info_call,"RDISemiHosting_SetDCCHandlerAddress");
		break;
	case RDISemiHosting_GetDCCHandlerAddress:
		strcpy(info_call,"RDISemiHosting_GetDCCHandlerAddress");
		break;
	case RDIInfo_Modules:
		strcpy(info_call,"RDIInfo_Modules");
		break;
	case RDISignal_Stop :
		strcpy(info_call,"RDISignal_Stop");
		break;
	case RDIInfo_ForceSystemReset :
		strcpy(info_call,"RDIInfo_ForceSystemReset");
		break;
	case RDIInfo_GetIOBits:
		strcpy(info_call,"RDIInfo_GetIOBits");
		break;
	case RDIInfo_SetIOBits:
		strcpy(info_call,"RDIInfo_SetIOBits");
		break;
	case RDIInfo_Points:
		strcpy(info_call,"RDIInfo_Points");
		break;
	case RDIInfo_Step:
		strcpy(info_call,"RDIInfo_Step");
		break;
	case RDIInfo_Target:
		strcpy(info_call,"RDIInfo_Target");
		break;
	case RDIInfo_AshlingVersion:
		strcpy(info_call,"RDIInfo_AshlingVersion");
		break;
	case RDICommsChannel_ToHost:
		strcpy(info_call,"RDICommsChannel_ToHost");
		break;
	case RDICommsChannel_FromHost:
		strcpy(info_call,"RDICommsChannel_FromHost");
		break;
	case RDIInfo_CP15CacheSelection: 
		strcpy(info_call,"RDIInfo_CP15CacheSelection");
		break;
	case RDIInfo_SetCP15CacheSelected: 
		strcpy(info_call,"RDIInfo_SetCP15CacheSelected");
		break;
	case RDIInfo_GetCP15CacheSelected: 
		strcpy(info_call,"RDIInfo_GetCP15CacheSelected"); 
		break;
	case RDIInfo_CP15CurrentMemoryArea: 
		strcpy(info_call,"RDIInfo_CP15CurrentMemoryArea");
		break;
	case RDIInfo_SetCP15CurrentMemoryArea:
		strcpy(info_call,"RDIInfo_SetCP15CurrentMemoryArea"); 
		break;
	case RDIInfo_GetCP15CurrentMemoryArea: 
		strcpy(info_call,"RDIInfo_GetCP15CurrentMemoryArea");
		break;
	case RDIPointStatus_Break:
		strcpy(info_call,"RDIPointStatus_Break");
		break;
	case RDIPointStatus_Watch:
		strcpy(info_call,"RDIPointStatus_Watch");
		break;
	case RDIIcebreaker_SetLocks:
		strcpy(info_call,"RDIIcebreaker_SetLocks");
		break;
	case RDIIcebreaker_GetLocks:
		strcpy(info_call,"RDIIcebreaker_GetLocks");
		break;
	case RDISet_Cmdline:
		strcpy(info_call,"RDISet_Cmdline");
		break;
	case RDIInfo_SetTopMem:
		strcpy(info_call,"RDIInfo_SetTopMem");
		break;
	case RDIInfo_GetSafeNonVectorAddress:
		strcpy(info_call,"RDIInfo_GetSafeNonVectorAddress");
		break;
	case RDIInfo_SetSafeNonVectorAddress:
		strcpy(info_call,"RDIInfo_SetSafeNonVectorAddress");
		break;
	case RDIInfo_MovableVectors:
		strcpy(info_call,"RDIInfo_MovableVectors");
		break;
	case RDIInfo_GetVectorAddress:
		strcpy(info_call,"RDIInfo_GetVectorAddress");
		break;
	case RDIVector_Catch:
		strcpy(info_call,"RDIVector_Catch");
		break;
	case RDIErrorP:
		strcpy(info_call,"RDIErrorP");
		break;
	case RDIInfo_GetLoadSize:
		strcpy(info_call,"RDIInfo_GetLoadSize");
		break;
	case RDIInfo_GetARM9RestartCodeAddress   :
		strcpy(info_call,"RDIInfo_GetARM9RestartCodeAddress");
		break;
	case RDIInfo_SetARM9RestartCodeAddress   :
		strcpy(info_call,"RDIInfo_SetARM9RestartCodeAddress");
		break;
	case RDIInfo_RequestCoProDesc:
		strcpy(info_call,"RDIInfo_RequestCoProDesc");
		break;
	case RDIInfo_SetLog:
		strcpy(info_call,"RDIInfo_SetLog");
		break;
	case RDIInfo_Log:
		strcpy(info_call,"RDIInfo_Log");
		break;
	case RDIInfo_InfoAllowedWhilstExecuting:
		strcpy(info_call,"RDIInfo_InfoAllowedWhilstExecuting");
		break;
	case RDIInfo_CustomLoad:
		strcpy(info_call,"RDIInfo_CustomLoad");
		break;
	case RDIInfo_LoadStart:
		strcpy(info_call,"RDIInfo_LoadStart");
		break;
	case RDIInfo_LoadEnd:
		strcpy(info_call,"RDIInfo_LoadEnd");
		break; 
	case RDIInfo_Properties:
		strcpy(info_call,"RDIInfo_Properties");
		break;
	case RDIProperty_RequestGroups:
		strcpy(info_call,"RDIProperty_RequestGroups");
		break;
	case RDIProperty_RequestDescriptions:
		strcpy(info_call,"RDIProperty_RequestDescriptions");
		break;
	case RDIProperty_GetAsNumeric:
		strcpy(info_call,"RDIProperty_GetAsNumeric");
		break;
	case RDIProperty_SetAsNumeric  :
		strcpy(info_call,"RDIProperty_SetAsNumeric");
		break;
	case RDI_GetPropertyDisplayDetails:
		strcpy(info_call,"RDI_GetPropertyDisplayDetails");
		break;
	case RDISignal_ReleaseControl:
		strcpy(info_call,"RDISignal_ReleaseControl");
		break;
		}
}

/****************************************************************************
Function: ASHSetJTAGFrequency
Engineer: Venkitakrishnan K
Input: 
Output: 
Description: 
Date           Initials    Description
31-Mar-2008    VK          Initial
****************************************************************************/

extern "C" int  ASHSetJTAGFrequency(unsigned long ulFreqKHZ,int bMode)
{
    if(bMode==TRUE)
        tyTargetConfig.bAdaptiveClockingEnabled=TRUE;
    else
        tyTargetConfig.bAdaptiveClockingEnabled=FALSE;
	
    if((ML_SetJTAGFrequency(ulFreqKHZ)))
        return FALSE;
    else 
        return TRUE;
}
	
/****************************************************************************
     Function: ASH_ReadCPRegister
     Engineer: Roshan T R
        Input: unsigned int uiFamily - Processor Family (ARM7, 9 or 11)
               unsigned int uiCore - Core type
               unsigned int uiCPNo - The coprocessor to be read
               unsigned int uiRegIndex - The register index
       Output: int - TRUE if success, otherwise FALSE
  Description: Read specified coprocessor register
Date           Initials    Description
07-Dec-2009    RTR         Initial
****************************************************************************/
extern "C" int ASH_ReadCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex,
								  unsigned int *puiRegValue)
{
   int iError;

   iError = ML_ReadCPRegister(uiCpNum, uiRegIndex, puiRegValue);

   if (iError != ERR_ML_ARM_NO_ERROR)
      return FALSE;
   return TRUE;
}


/****************************************************************************
     Function: ASH_WriteCPRegister
     Engineer: Roshan T R
        Input: unsigned int uiFamily - Processor Family (ARM7, 9 or 11)
               unsigned int uiCore - Core type
               unsigned int uiCPNo - The coprocessor to be write
               unsigned int uiRegIndex - The register index
			   unsigned int uiValue - The value to be write
       Output: int - TRUE if success, otherwise FALSE
  Description: Read specified coprocessor register
Date           Initials    Description
07-Dec-2009    RTR         Initial
****************************************************************************/
extern "C" int ASH_WriteCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex, 
												  unsigned int uiValue)
{
   int iError;

   iError = ML_WriteCPRegister(uiCpNum, uiRegIndex, uiValue);

   if (iError != ERR_ML_ARM_NO_ERROR)
      return FALSE;
   return TRUE;
}

/****************************************************************************
     Function: ASH_ProcessCP15Register
     Engineer: Roshan T R
        Input: unsigned int uiOperation		: Read/Write
			   unsigned int uiCRn			: Value of CRn in coproc instruction
			   unsigned int uiCRm			: Value of CRm in coproc instruction
			   unsigned int uiOp1			: Value of operand1 in coproc instruction
			   unsigned int uiOp2			: Value of operand2 in coproc instruction
			   unsigned long *pulData		: Pointer to data rad/write from/to coproc
       Output: int - TRUE if success, otherwise FALSE
  Description: Read specified coprocessor register
Date           Initials    Description
07-Dec-2009    RTR         Initial
****************************************************************************/
extern "C" int ASH_ProcessCP15Register(unsigned int uiOperation, unsigned int uiCRn, unsigned int uiCRm,
                                      unsigned int uiOp1, unsigned int uiOp2,unsigned long *pulData)
{
   int iError;

   iError = ML_ProcessCP15Register(uiOperation, uiCRn, uiCRm, uiOp1, uiOp2, pulData);

   if (iError != ERR_ML_ARM_NO_ERROR)
      return FALSE;
   return TRUE;
}

/****************************************************************************
Function: ASH_ScanIR
Engineer: Vitezslav Hola
Input: 
unsigned long ulLength - number of bits to shift
unsigned long *pulDataIn - data into scanchain
unsigned long *pulDataOut - data from scanchain
Output: int - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG IR register
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/

extern "C" int ASH_ScanIR(unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut)
{
	
	if (ulLength == 0)
		return TRUE;
	if (ML_ScanIR(ulLength, pulDataIn, pulDataOut) != ERR_ML_ARM_NO_ERROR)
		return FALSE;
	return TRUE;
}

/****************************************************************************
Function: ASH_ScanDR
Engineer:Venkitakrishnan K
Input: 
unsigned long ulLength - number of bits to shift
unsigned long *pulDataIn - data into scanchain
unsigned long *pulDataOut - data from scanchain
Output: int - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
31-Mar-2008    VK          Initial
****************************************************************************/
extern "C" int ASH_ScanDR( unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut)
{
	
	if (ulLength == 0)
		return TRUE;
	if (ML_ScanDR(ulLength, pulDataIn, pulDataOut) != ERR_ML_ARM_NO_ERROR)
		return FALSE;
	return TRUE;
}


/****************************************************************************
Function: ASH_ScanIRScanDR
Engineer:Venkitakrishnan K
Input: 
unsigned long ulLength - number of bits to shift
unsigned long *pulDataIn - data from scanchain
Output: int - TRUE if success, otherwise FALSE
Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
31-Mar-2008    VK          Initial
****************************************************************************/
extern "C" int ASH_ScanIRScanDR( unsigned long *pulLength,unsigned long *pulDataIn,unsigned long *pulDataOut)
{
	
	
	if (ML_ScanIRScanDR(pulLength,pulDataIn,pulDataOut) != ERR_ML_ARM_NO_ERROR)
		return FALSE;
	return TRUE;
}

/****************************************************************************
Function: ASH_ResetTAPController
Engineer: Amerdas D K
Input: None   
Output: int - TRUE if success, otherwise FALSE
Description: Issue TAP reset
Date           Initials    Description
06-Oct-2009		ADK			initial
****************************************************************************/
extern "C" int ASH_ResetTAPController(void)
{
	
	if (ML_ResetTAPUsingTMS() != ERR_ML_ARM_NO_ERROR)
		return TRUE;
	return FALSE;
}

/****************************************************************************
Function: ASH_PrepareforJTAGConsole
Engineer: Suraj S
Input: void
Output: int - Error Code
Description: Initialise the JTAG Console
Date           Initials    Description
16-Oct-2009    SJ          Initial
****************************************************************************/
extern "C" int ASH_PrepareforJTAGConsole()
{
	
	unsigned short usNumberOfDevices;
	//TyDevHandle tyHandle;
	TyDeviceInfo tyDiagOptions;
	char szDiskwareFullPath[_MAX_PATH];
	char szFpgawareFullPath[_MAX_PATH];
	char ppszLocDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN];
	char szOpXDDiskwareFilename[MAX_DRIVER_NAME] = "opxddarm.bin";
	char szOpXDFpgawareFilename[MAX_DRIVER_NAME] = "opxdfarm.bin";
	int iRet;
	
	iRet = DRVOPXD_ERROR_NO_ERROR;
	
	// determine which Opella-XD to use
	DL_OPXD_GetConnectedDevices(ppszLocDeviceNumbers, &usNumberOfDevices, OPELLAXD_USB_PID);
	
	switch(usNumberOfDevices)
	{
	case 0 : // no device connected
		iRet = DRVOPXD_ERROR_NO_USB_DEVICE_CONNECTED;
		break;
		
	case 1 : // only 1 device, no sn needed
		//if (tyDiagOptions.pszSerialNumber[0] == NULL)
		strcpy(&tyDiagOptions.pszSerialNumber[0], ppszLocDeviceNumbers[0]);
		break;
		
	default : // several devices, sn needed
		if (tyDiagOptions.pszSerialNumber == NULL)
			iRet = DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
		break;
	}
	
	if(iRet != DRVOPXD_ERROR_NO_ERROR)
		return iRet;
	
	// configure seleted device and get handle
	// find device and unconfigure it
	switch (DL_OPXD_OpenDevice(tyDiagOptions.pszSerialNumber, OPELLAXD_USB_PID, false, &tyCurrentDevice))
	{
	case DRVOPXD_ERROR_NO_ERROR:
		break;
	case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
		iRet = DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
		break;
	default:
		iRet = DRVOPXD_ERROR_USB_DRIVER;
		break;
	}
	
	if (DL_OPXD_UnconfigureDevice(tyCurrentDevice)!=DRVOPXD_ERROR_NO_ERROR)
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	//Save the path of the DLL
	ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);
	
	// get full path to Opella-XD diskware and FPGAware for ARM
	strcpy(szDiskwareFullPath, szGlobalDLLPathName);
	strcat(szDiskwareFullPath, szOpXDDiskwareFilename);		
	strcpy(szFpgawareFullPath, szGlobalDLLPathName);
	strcat(szFpgawareFullPath, szOpXDFpgawareFilename);
	
    if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_ConfigureDevice(tyCurrentDevice, DW_ARM, FPGA_ARM, szOpXDDiskwareFilename, szOpXDFpgawareFilename) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_JtagSetFrequency(tyCurrentDevice, 1.0, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	// set TMS sequence (just use default sequences)
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_JtagSetTMS(tyCurrentDevice, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	// set no MC
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_JtagConfigMulticore(tyCurrentDevice, 0, NULL) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	// set tracking voltage and enable drivers to target
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_SetVtpaVoltage(tyCurrentDevice, 0.0, TRUE, TRUE) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	return iRet;
}

#ifndef __LINUX
/****************************************************************************
Function: RDILogFile
Engineer: Jeenus Chalattu Kunnath
Input: Tyrdilog *tyrdilog - structure 			   
Output: void
Description: Writes RDI logging Informations on a file
Date           Initials    Description
23-Apr-2008    JCK          Initial
****************************************************************************/
void RDILogFile(Tyrdilog *tyrdilog)
{
	FILE *fp =NULL;
	unsigned char ucI;	
	unsigned long ulNumChar = 0;               //To find total number of characters printed
	char temp[120];
	Time_difference = Time_out - Time_in;

	fp = fopen(szFullPathandNameForLogFile,"a");	
	if (NULL == fp)
		{
		printf("Can not open Opella-XD RDI Log file!!!\n");
		return;
		}
	
	ulNumChar += fprintf(fp,"\n< %02d:%02d:%03d >        OpellaXD_RDI_",st_in.wMinute,st_in.wSecond,st_in.wMilliseconds);
	ulNumChar += fprintf(fp,"%-15s(",tyrdilog->func_name);
	
	for(ucI=0; ucI < 3; ucI++)
	{
		if((strcmp(tyrdilog->arg_name[ucI],"0")))
		{
			ulNumChar += fprintf(fp,"%s",tyrdilog->arg_name[ucI]);
			if((strcmp(tyrdilog->arg_value[ucI+1],"0")) && ucI < 2)			
				ulNumChar += fprintf(fp,"%s,",tyrdilog->arg_value[ucI]);			
			else
				ulNumChar += fprintf(fp,"%s",tyrdilog->arg_value[ucI]);
		}		
		else if ((strcmp(tyrdilog->arg_value[ucI],"0")))
		{
			if((strcmp(tyrdilog->arg_value[ucI+1],"0")))			
				ulNumChar += fprintf(fp,"%s",tyrdilog->arg_value[ucI]);
		}		
	}
	ulNumChar = 125 - ulNumChar;              //Maximum number of charcters before return value is 125.
	memset(temp,32,ulNumChar);
	temp[ulNumChar] = '\0';
	if(strcmp(tyrdilog->return_value,"0"))
		fprintf(fp,"%s)   returns %-35s",temp,tyrdilog->return_value);         // in case of errmess there is return value
	fprintf(fp,"::(%d ms)::",(Time_difference%10000));
	if(strcmp(tyrdilog->errormessage,"0")) 		
		fprintf(fp,"%s",tyrdilog->errormessage);
	fclose(fp);
}
#endif
/****************************************************************************
Function: GetAcessType
Engineer: Jeenus Chalattu Kunnath
Input: char *phex           - array to store the string equivalent of access type
unsigned long ulword - aceess type
Output: void
Description: To get the string equivalent of access type
Date           Initials    Description
23-Apr-2008    JCK          Initial
****************************************************************************/
void GetAcessType(char ucAccessType[50],RDI_AccessType type)
{
	switch(type)
	{   
	case RDIAccess_Data   :
		strcpy(ucAccessType,"RDIAccess_Data");
		break;
	case RDIAccess_Code   :
		strcpy(ucAccessType,"RDIAccess_Code");
		break;
	case RDIAccess_Data8  :
		strcpy(ucAccessType,"RDIAccess_Data8");
		break;
	case RDIAccess_Code8  :
		strcpy(ucAccessType,"RDIAccess_Code8");
		break;
	case RDIAccess_Code32 :
		strcpy(ucAccessType,"RDIAccess_Code32");
		break;
	case RDIAccess_Data32 :
		strcpy(ucAccessType,"RDIAccess_Data32");
		break;
	case RDIAccess_Data16 :
		strcpy(ucAccessType,"RDIAccess_Data16");
		break;
	case RDIAccess_Data64 :
		strcpy(ucAccessType,"RDIAccess_Data64");
		break;
	case RDIAccess_Code16 :
		strcpy(ucAccessType,"RDIAccess_Code16");
		break;
	case RDIAccess_Code64 :
		strcpy(ucAccessType,"RDIAccess_Code64");
		break;
	default:
		strcpy(ucAccessType,"UnKnownType");
	}
	
}

/****************************************************************************
Function: GetBreakType
Engineer: Jeenus Chalattu Kunnath
Input: char *phex           - array to store the string equivalent of break point type
unsigned   type      - break point type
Output: void
Description: To get the string equivalent of Break point types
Date           Initials    Description
23-Apr-2008    JCK          Initial
****************************************************************************/
void GetBreakType(char ucBreakType[50],unsigned type)
{
	switch(type)
	{   
	case RDIPoint_EQ  :
		strcpy(ucBreakType,"RDIPoint_EQ");
		break;
	case RDIPoint_GT  :
		strcpy(ucBreakType,"RDIPoint_GT");
		break;
	case RDIPoint_GE  :
		strcpy(ucBreakType,"RDIPoint_GE");
		break;
	case RDIPoint_LT  :
		strcpy(ucBreakType,"RDIPoint_LT");
		break;
	case RDIPoint_LE  :
		strcpy(ucBreakType,"RDIPoint_LE");
		break;
	case RDIPoint_IN  :
		strcpy(ucBreakType,"RDIPoint_IN");
		break;
	case RDIPoint_OUT  :
		strcpy(ucBreakType,"RDIPoint_OUT");
		break;
	case RDIPoint_MASK  :
		strcpy(ucBreakType,"RDIPoint_MASK");
		break;
	default :
		strcpy(ucBreakType, "UnKnownType");
	}
}

/****************************************************************************
Function: GetDataType
Engineer: Jeenus Chalattu Kunnath
Input: char ucWatchType[]   - array to store the string equivalent of watch point type
unsigned   type      - watch point type. 
Output: void
Description: To get the string equivalent of Watch point types
Date           Initials    Description
23-Apr-2008    JCK          Initial
****************************************************************************/
void GetDataType(char ucWatchType[50],unsigned type)
{
	switch(type)
	{   
	case RDIWatch_ByteRead  :
		strcpy(ucWatchType,"RDIWatch_ByteRead");
		break;
	case RDIWatch_HalfRead  :
		strcpy(ucWatchType,"RDIWatch_HalfRead");
		break;
	case RDIWatch_WordRead  :
		strcpy(ucWatchType,"RDIWatch_WordRead");
		break;
	case RDIWatch_ByteWrite  :
		strcpy(ucWatchType,"RDIWatch_ByteWrite");
		break;
	case RDIWatch_HalfWrite  :
		strcpy(ucWatchType,"RDIWatch_HalfWrite");
		break;
	case RDIWatch_WordWrite  :
		strcpy(ucWatchType,"RDIWatch_WordWrite");
		break;		
	default :
		strcpy(ucWatchType, "UnKnownType");
	}
}

#ifndef __LINUX
/****************************************************************************
Function: GetErrorMessage
Engineer: Jeenus Chalattu Kunnath
Input: char *szErrorType - Error Type
char *szMessage   - Message which is shown in the log file.
Output: void
Description: To get the string equivalent of Watch point types
Date           Initials    Description
23-Apr-2008    JCK          Initial
****************************************************************************/
void GetErrorMessage(char *szErrorType, char *szMessage)
{
	char szError[256];
	
	if(!(strcmp(szErrorType,"RDIError_Reset")))					
		strcpy(szMessage, "Target reset detected");     				
	
	else if(!(strcmp(szErrorType,"RDIError_TargetRunning")))
		strcpy(szMessage, "Option not available while target is running");        
	
	else if((!strcmp(szErrorType,"RDIError_UnableToInitialise")) ||
		(!strcmp(szErrorType,"RDIError_Comms_Opella"))       ||
		(!strcmp(szErrorType,"RDIError_TargetBroken_Opella"))||
		(!strcmp(szErrorType,"RDIError_TargetBroken")))      
		strcpy(szMessage, "Cannot communicate with target. Ensure that Opella-XD is configured correctly, connected to the target and that the target is powered");        
	
	else if(!(strcmp(szErrorType,"RDIError_LoopBack_Opella")))         
		strcpy(szMessage, "The PC is not able to communicate with Opella-XD. Please ensure it is connected to your PC");
	
    
	else if(!(strcmp(szErrorType,"RDIError_InitTarget_Opella"))) 
		strcpy(szMessage, "Unable to communicate with your target device (can communicate with Opella-XD). Ensure that Opella-XD is configured correctly, connected to the target and that the target is powered");
	
	else if(!(strcmp(szErrorType,"RDIError_BadPointType"))) 
		strcpy(szMessage, "Internal error. The requested point type is invalid");
	
	else if(!(strcmp(szErrorType,"RDIError_IncorrectProcType"))) 
		strcpy(szMessage, "The target device detected is not the same as the device selected");
	
	else if(!(strcmp(szErrorType,"RDIError_CantSetPoint"))) 
		strcpy(szMessage, "Cannot set breakpoint/watchpoint as there are no more hardware resources available. Please clear existing breakpoint/watchpoints and retry");
	
	else if(!(strcmp(szErrorType,"RDIError_NoSuchPoint"))) 
		strcpy(szMessage, "Cannot clear breakpoint/watchpoint (as it has already been cleared)");
	
	else if(!(strcmp(szErrorType,"RDIError_PointInUse"))) 
		strcpy(szMessage, "Cannot set breakpoint/watchpoint as one already exists at this address");
	
	else if(!(strcmp(szErrorType,"RDIError_Dw1FileFailed"))) 
		strcpy(szMessage, "Failed to find Opella-XD diskware file");
	
	else if(!(strcmp(szErrorType,"RDIError_Fpga1FileFailed"))) 
		strcpy(szMessage, "Failed to find Opella-XD FPGAware file");
	
	else if(!(strcmp(szErrorType,"RDIError_NoToolConfig"))) 
		strcpy(szMessage, "No tool configuration present");
	
	else if(!(strcmp(szErrorType,"RDIError_UnknownDiskware"))) 
		strcpy(szMessage, "Unknown diskware error occurred");
	
	else if(!(strcmp(szErrorType,"RDIError_IncorrectUSBDriver"))) 
		strcpy(szMessage, "The loaded Opella XD USB drivers are out of date. Please upgrade with the latest versions supplied (located in USB subdirectory of Ashling software installation)");
	
	else if(!(strcmp(szErrorType,"RDIError_CannotWriteDCCHandler"))) 
		strcpy(szMessage, "Cannot write the DCC semi-hosting handler to target memory. Please ensure there is adequate RAM at the specelse ified address");
	
	else if(!(strcmp(szErrorType,"RDIError_CannotWriteToVectorTable"))) 
		strcpy(szMessage, "Cannot write to the vector table in target memory. Please check that there is RAM at addresses 0x08 and 0x28");
	
	else if(!(strcmp(szErrorType,"RDIError_FPGA_File_Not_Found"))) 
	{
		strcpy(szError,pszGlobalDllPathName);
		(void)strupr(szError);
		sprintf(szMessage,"Failed to find Opella-XD firmware file for ARM in the folder %s",szError);
	}
	
	else if(!(strcmp(szErrorType,"RDIError_Diskware_File_Not_Found")))    
	{
		strcpy(szError,pszGlobalDllPathName);
		(void)strupr(szError);
		sprintf(szMessage,"Failed to find Opella-XD diskware file for ARM in the folder %s",szError);
	}
	
	else if(!(strcmp(szErrorType,"RDIError_Incorrect_Diskware_File")))      
		strcpy(szMessage, "Failed to find the correct Opella-XD diskware file");
	
	else if(!(strcmp(szErrorType,"RDIError_Failed_To_Allocate_PC_Memory"))) 
		strcpy(szMessage, "Internal error. Failed to allocate adequate PC memory");
	
	else if(!(strcmp(szErrorType,"RDIError_DataAbort_Writing_Cache_Clean"))) 
		strcpy(szMessage, "A data abort occurred while downloading the data cache invalidation routine. Please ensure that the specelse ified Data cache clean start address points to a valid memory location with at least 128 bytes of available memory");
	
	else if(!(strcmp(szErrorType,"RDIError_NoRTCKSupport"))) 
		strcpy(szMessage, "No RTCK response from the target. Please reconfigure Opella-XD RDI driver to use a fixed frequency.");
	
	else if(!(strcmp(szErrorType,"RDIError_InvalidSerialNumber"))) 
		//strcpy(szMessage, "Serial Number do not match. Please recheck the configuration");
		sprintf(szMessage,"Invalid serial number (%s) in current configuration file. This does not match the currently connected Opella-XD. Please reconfigure Opella-XD RDI Driver.",tyTargetConfig.szOpellaXDSerialNumber);
	
	else if(!(strcmp(szErrorType,"RDIError_SoftInitialiseError"))) 
		strcpy(szMessage, "Failed to initialise target. Please reconfigure the Opella-XD RDI driver appropriately for your target (trouble shoot using the Diagnostics option).");
	
	
}

/****************************************************************************
Function: InitiliaseUserRegister
Engineer: Jeenus Chalattu Kunnath
Input: void 
Output: void
Description: To initialize the user register settings in the RDI GUI
Date           Initials    Description
23-Apr-2008    JCK          Initial
07-May-2010	   JCK		    Added support for Delay
****************************************************************************/
void InitiliaseUserRegister()
{
	FILE				*tempFile;
	BOOL				bComment;	
	char				szFileString[_MAX_PATH];
	unsigned long	j = 0;   
	char				szUserRegPath[_MAX_PATH];
	
	strcpy(szUserRegPath,pszGlobalDllPathName);
	strcat(szUserRegPath,"\\temp.txt");
	
	userregset.ulUserSpecificVariablesCount = 0;
	if ((tempFile = fopen(szUserRegPath, "rt")) != NULL)
	{
		while (!feof(tempFile))
		{
			if (fgets(szFileString, 255, tempFile) != NULL)
			{
				bComment = TRUE;
				
				for (j=0; j < strlen(szFileString); j++)
				{
					if ((szFileString[j] == 0xA) || (szFileString[j] == 0xD) || (szFileString[j] == '#'))
						break;
					if ((szFileString[j] != ' ') && (szFileString[j] != '\t'))
					{
						bComment = FALSE;
						break;
					}
				}
				
				if (!bComment)
				{	
					_UserSpecific *pUserSpecific = &userregset.pUserSpecificVariables[userregset.ulUserSpecificVariablesCount];

					char* pszCommentStart = strchr(szFileString, '"');
					unsigned long i = 0;
					pszCommentStart = pszCommentStart + 1;
					while (pszCommentStart[i] != '"')
					{
						pUserSpecific->szVariableName[i] = pszCommentStart[i];
						
						i = i + 1;
					}

					sscanf(&szFileString[i + 2], "%s", pUserSpecific->szAction);
					if (0 == strcmp("WriteReg", pUserSpecific->szAction))
					{
						if (sscanf(&szFileString[ i + 2 ], "%s %X %X %X", pUserSpecific->szAction,
							&pUserSpecific->adVariableSize,
							&pUserSpecific->adVariableAddress,
							&pUserSpecific->ulVariableValue) == 4)
						{						
							if ((pUserSpecific->adVariableSize == 1) ||
								(pUserSpecific->adVariableSize == 2) ||
								(pUserSpecific->adVariableSize == 4))
							{
								userregset.ulUserSpecificVariablesCount++;
								
							}
						}
					} 
					else
					{
						if (sscanf(&szFileString[ i + 2 ], "%s %X %X %X", pUserSpecific->szAction,
							&pUserSpecific->adVariableSize,
							&pUserSpecific->adVariableAddress,
							&pUserSpecific->ulVariableValue) == 4)
						{						
							userregset.ulUserSpecificVariablesCount++;
						}
					}
					
				}
			}
		}
	}
	
}

/****************************************************************************
Function: InitUserRegisterSettings
Engineer: Jeenus Chalattu Kunnath
Input: void 
Output: void
Description: To initialize the user register settings
Date           Initials    Description
20-Feb-2010    JCK          Initial
****************************************************************************/
int InitUserRegisterSettings()
{
	int   ErrRet = RDIError_NoError;

	if(bUseUserReg)
	{
		unsigned long  ulSize;
		unsigned long  ulAddr;
		unsigned long  ulValue;
		char		   szAction[10];
		unsigned long   count = 0;

		if(!bUserRegSet)					//While Restoring the last saved configuration
			InitiliaseUserRegister();  //we had to update the user register settings also.      
		
		count = userregset.ulUserSpecificVariablesCount;
		
		for(unsigned int i = 0;i < count; i++)
		{
			ulValue = userregset.pUserSpecificVariables[i].ulVariableValue;
			ulAddr	= userregset.pUserSpecificVariables[i].adVariableAddress;
			ulSize	= userregset.pUserSpecificVariables[i].adVariableSize;
			strcpy(szAction, userregset.pUserSpecificVariables[i].szAction);

			if (0 == strcmp( "WriteReg" , szAction ))
			{
				ErrRet = ML_WriteProc((void *)&ulValue,
					ulAddr,
					(unsigned long *) &ulSize, 
					RDIAccess_Data32, 
					FALSE,
					FALSE);	
				if(ErrRet != ML_ERROR_NO_ERROR)
				{
					ErrRet = CheckForFatalErrorMessage(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
					return ErrRet;
				}
			} 
			else
			{
				ML_Delay(ulValue);				
			}
					
		}
		bUseUserReg = 0;
	  }

	return ErrRet;

}

/****************************************************************************
Function: GetUserRegisterSettings
Engineer: Jeenus Chalattu Kunnath
Input: void 
Output: ProcessorConfig* userregset - Pointer to current user register settings
Description: 
Date           Initials    Description
23-Apr-2008    JCK          Initial
****************************************************************************/
ProcessorConfig* GetUserRegisterSettings(void)
{
	return &userregset;
}

/****************************************************************************
Function: GetRDIFnPtrs
Engineer: Suraj S
Input: *pOpxdrdiFnptr - pointer to return the opxdxrdi.dll function pointers
		used by OpellaXDARMConfig.dll 
Output: none
Description: Returns the functions pointers of the required functions of 
			 opxdrdi.dll
Date           Initials    Description
11-Aug-2010     SJ          Initial
****************************************************************************/

void GetRDIFnPtrs(OpxdrdiFnptr *pOpxdrdiFnptr)
{
	pOpxdrdiFnptr->PerformReadROMTable = PerformReadROMTable;
	pOpxdrdiFnptr->PerformRDIDiagnostics = PerformRDIDiagnostics;
	pOpxdrdiFnptr->PerformRDIDiagnosticsOnTarget = PerformRDIDiagnosticsOnTarget;
	pOpxdrdiFnptr->PerformTargetHardReset = PerformTargetHardReset;
	pOpxdrdiFnptr->GetFirmwareInfo = GetFirmwareInfo;
	pOpxdrdiFnptr->GetRDIDLLVersion = GetRDIDLLVersion;
	pOpxdrdiFnptr->GetUserRegisterSettings = GetUserRegisterSettings;
	pOpxdrdiFnptr->ML_CheckOpellaXDInstance = ML_CheckOpellaXDInstance;
}

#endif
