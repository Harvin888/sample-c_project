/****************************************************************************
      Module: Realmon.cpp
    Engineer: Suraj S
 Description: Real Monitor Support
Date          Initials     Description
23-Jul-2007     SJ         Initial
****************************************************************************/
#include "../../common/drvopxd.h"
#include "common.h"
#include "midlayerarm.h"
#include "realmon.h"
#include "semihost.h"

extern TyError RDI_Convert_MidLayerErr_ToRDIErr (TyError tyMidLayerErr);

static unsigned char bGlobalRealMonitorDetected = FALSE;

/****************************************************************************
    Function: RealMonitorDetected
    Engineer: Suraj S
       Input: NONE
      Output: unsigned char - Real monitor support detected or not 
 Description: Checks for Real Monitor
Date          Initials     Description
23-Jul-2007     SJ         Initial
****************************************************************************/
unsigned char RealMonitorDetected(void)
{
   return bGlobalRealMonitorDetected;
}

/****************************************************************************
    Function: RealMonSendPacket
    Engineer: Suraj S
       Input: TyRealMonPacket tyRealMonPacket - Real Monitor data packet
      Output: TyError - RDI Layer Error code 
 Description: Extracts Real Monitor the data packet and write it.
Date          Initials     Description
15-Aug-2007     SJ         Initial
****************************************************************************/
TyError RealMonSendPacket(TyRealMonPacket tyRealMonPacket)
{
   TyError  ErrRet;
   unsigned long ulWriteData;
   unsigned long i;

   ulWriteData  = (tyRealMonPacket.ulLength       ) & 0x0000FFFF;
   ulWriteData += (tyRealMonPacket.ulOpcode  << 16) & 0x00FF0000;
   ulWriteData += (tyRealMonPacket.ulChannel << 24) & 0xFF000000;

   ErrRet = RealMonWriteSafeDCC(ulWriteData);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   for (i=0; i < tyRealMonPacket.ulLength; i+=4)
      {
      ulWriteData = *(unsigned long *)&tyRealMonPacket.ucData[i];

      ErrRet = RealMonWriteSafeDCC(ulWriteData);
      if (ErrRet != RDIError_NoError)
         return ML_ResetCheck(ErrRet);
      }

   return RDIError_NoError;
}

/****************************************************************************
    Function: RealMonWriteSafeDCC
    Engineer: Suraj S
       Input: unsigned long ulWriteData - DCC Data to write
      Output: TyError - RDI Layer Error code 
 Description: Execute safe DCC write using real monitor
Date          Initials     Description
15-Aug-2007     SJ         Initial
****************************************************************************/
TyError RealMonWriteSafeDCC(unsigned long ulWriteData)
{
   TyError ErrRet;

   ErrRet = RealMonWriteDCC(ulWriteData);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (ulWriteData == ESCAPE_SEQUENCE)
      {
      ErrRet = RealMonWriteDCC(ESCAPE_QUOTE);
      if (ErrRet != RDIError_NoError)
         return ErrRet;
      }

   return RDIError_NoError;
}

/****************************************************************************
    Function: RealMonWriteDCC
    Engineer: Suraj S
       Input: unsigned long ulWriteData - DCC Write data
      Output: TyError - RDI Layer Error code 
 Description: Write DCC data using RealMonitor
Date          Initials     Description
15-Aug-2007     SJ         Initial
****************************************************************************/
TyError RealMonWriteDCC(unsigned long ulWriteData)
{
   TyError       ErrRet;
   boolean       bDataWrote;
   boolean       bDataRead;
   unsigned long ulTemp;

   ErrRet = ML_CheckDCC  (&ulTemp,
                          &bDataRead,
                          &ulWriteData,
                          &bDataWrote);
   
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ML_ResetCheck(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));
   
   if (!bDataWrote)
      return RDIError_RealMonFailed;

   return RDIError_NoError;
}

/****************************************************************************
    Function: RealMonReceivePacket
    Engineer: Suraj S
       Input: TyRealMonPacket *ptyRealMonPacket - pointer to receive Real monitor
              data packet
              unsigned long *pulExceptionStatus - pointer to exception status
      Output: TyError - RDI Layer Error code  
 Description: Receives the real monitor data packet along with exception status
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError RealMonReceivePacket(TyRealMonPacket *ptyRealMonPacket, unsigned long *pulExceptionStatus)
{
   TyError ErrRet;
   unsigned long ulReadData;
   unsigned long i;

   *pulExceptionStatus = 0;

   do
      {
      ErrRet = RealMonReadSafeDCC(&ulReadData);
      if (ErrRet != RDIError_NoError)
         return ErrRet;

      ptyRealMonPacket->ulLength  = (ulReadData & 0x0000FFFF);
      ptyRealMonPacket->ulOpcode  = (ulReadData & 0x00FF0000) >> 16;
      ptyRealMonPacket->ulChannel = (ulReadData & 0xFF000000) >> 24;


      for (i=0; i < ptyRealMonPacket->ulLength; i+=4)
         {
         ErrRet = RealMonReadSafeDCC(&ulReadData);
         if (ErrRet != RDIError_NoError)
            return ML_ResetCheck(ErrRet);


         *(unsigned long *)&ptyRealMonPacket->ucData[i] = ulReadData;
         }

      switch (ptyRealMonPacket->ulOpcode)
         {
         case SWI_STATUS     :
            *pulExceptionStatus |= MASK_SWI_STATUS;
            break;
         case UNDEF_STATUS   :
            *pulExceptionStatus |= MASK_UNDEF_STATUS;
            break;
         case PREFETCH_STATUS:
            *pulExceptionStatus |= MASK_PREFETCH_STATUS;
            break;
         case DATA_STATUS    :
            *pulExceptionStatus |= MASK_DATA_STATUS;
            break;
         default:
            // Hey, these should be handle in the while statement below...
            break;
         }
      }
   while ((ptyRealMonPacket->ulOpcode != OK_STATUS   ) &&
          (ptyRealMonPacket->ulOpcode != ERROR_STATUS));

   return RDIError_NoError;
}

/****************************************************************************
    Function: RealMonReadSafeDCC
    Engineer: Suraj S
       Input: unsigned long *pulReadData - pointer to read data
      Output: TyError - RDI Layer Error code  
 Description: Execute safe DCC read using real monitor 
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError RealMonReadSafeDCC(unsigned long *pulReadData)
{
   TyError       ErrRet;
   unsigned long ulEscapeWord;

   ErrRet = RealMonReadDCC(pulReadData);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (*pulReadData == ESCAPE_SEQUENCE)
      {
      ErrRet = RealMonReadDCC(&ulEscapeWord);
      if (ErrRet != RDIError_NoError)
         return ErrRet;

      switch (ulEscapeWord)
         {
         case ESCAPE_QUOTE:
            // Nothing to do only to swallow this word.
            break;
         case ESCAPE_RESET:

#ifdef DEBUG
            MessageBox(NULL,"Escape Reset Sequence Received",OPXDCAPTION,MB_OK|MB_ICONWARNING);
            //AfxMessageBox("Escape Reset Sequence Received");
#endif
            // Retry readding the word....
            ErrRet = RealMonReadDCC(pulReadData);
            if (ErrRet != RDIError_NoError)
               return ErrRet;
            break;
         case ESCAPE_GO   :
#ifdef DEBUG
            MessageBox(NULL,"Escape Go Sequence Received",OPXDCAPTION,MB_OK|MB_ICONWARNING);
            //AfxMessageBox("Escape Go Sequence Received");
#endif
            // Retry readding the word....
            ErrRet = RealMonReadDCC(pulReadData);
            if (ErrRet != RDIError_NoError)
               return ErrRet;
            break;
         case ESCAPE_PANIC:
#ifdef DEBUG
            MessageBox(NULL,"Escape Panic Sequence Received",OPXDCAPTION,MB_OK|MB_ICONWARNING);
            //AfxMessageBox("Escape Panic Sequence Received");
#endif
            // Retry readding the word....
            ErrRet = RealMonReadDCC(pulReadData);
            if (ErrRet != RDIError_NoError)
               return ErrRet;
            break;
         default:
#ifdef DEBUG
            MessageBox(NULL,"Escape Unknown Sequence Received",OPXDCAPTION,MB_OK|MB_ICONWARNING);
            //AfxMessageBox("Escape Unknown Sequence Received");
#endif
            // Retry readding the word....
            ErrRet = RealMonReadDCC(pulReadData);
            if (ErrRet != RDIError_NoError)
               return ErrRet;
            break;
         }
      }

   return RDIError_NoError;
}

/****************************************************************************
    Function: RealMonReadDCC
    Engineer: Suraj S
       Input: unsigned long *pulReadData - pointer to read data
      Output: TyError - RDI Layer Error code  
 Description: Reads the DCC data
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError RealMonReadDCC(unsigned long *pulReadData)
{
   TyError       ErrRet;
   boolean       bDataRead;
   unsigned long ulTimeout;

   ulTimeout = 0;
   do
      {
      //Write data to the core and check for data read from core.
      ErrRet = ML_CheckDCC(pulReadData,
                           &bDataRead,
                           NULL,
                           NULL);

      if(ErrRet != ML_ERROR_NO_ERROR)
         return ML_ResetCheck(RDI_Convert_MidLayerErr_ToRDIErr(ErrRet));

      ulTimeout ++;
      if (ulTimeout >= MAX_TIMEOUT)
         break;
      }
   while (bDataRead == FALSE);

   if (!bDataRead)
      return RDIError_RealMonFailed;

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteNOP
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code 
 Description: Execute NOP instruction using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteNOP(void)
{
   TyError ErrRet;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = NOP_OPCODE;
   tyRealMonPacket.ulChannel = 0;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)      
      return ErrRet;

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteGetCapabilities
    Engineer: Suraj S
       Input: unsigned long *pulAddress - pointer to store the target capabilities
      Output: TyError - RDI Layer Error code 
 Description: Get the capabilities of target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteGetCapabilities(unsigned long *pulAddress)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = GET_CAPABILITIES_OPCODE;
   tyRealMonPacket.ulChannel = 0;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);
   
   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   *pulAddress = *(unsigned long *)&tyRealMonPacket.ucData[0];

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteStop
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code 
 Description: Stops the target execution using real monitor
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteStop(void)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = STOP_OPCODE;
   tyRealMonPacket.ulChannel = 0;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteGo
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code  
 Description: Starts the target execution using real monitor
Date          Initials     Description
16-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteGo(void)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = GO_OPCODE;
   tyRealMonPacket.ulChannel = 0;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteGetPC
    Engineer: Suraj S
       Input: unsigned long ulCount - 
              unsigned long *pulPCSamples -
      Output: TyError - RDI Layer Error code  
 Description: 
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteGetPC(unsigned long ulCount, unsigned long *pulPCSamples)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long   i;

   tyRealMonPacket.ulLength  = 4;
   tyRealMonPacket.ulOpcode  = GET_PC_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulCount;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   for (i=0; i < ulCount; i++)
      pulPCSamples[i] = *(unsigned long *)&tyRealMonPacket.ucData[i * 4];

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteSyncCaches
    Engineer: Suraj S
       Input: unsigned long ulAddress - 
              unsigned long ulCount - 
      Output: TyError - RDI Layer Error code  
 Description: 
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteSyncCaches(unsigned long ulAddress, unsigned long ulCount)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 8;
   tyRealMonPacket.ulOpcode  = SYNC_CACHES_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;
   *(unsigned long *)&tyRealMonPacket.ucData[4] = ulCount;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ML_ResetCheck(ErrRet);

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteExecuteCode
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code  
 Description: 
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteExecuteCode(void)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = EXECUTE_CODE_OPCODE;
   tyRealMonPacket.ulChannel = 0;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteInitialiseTarget
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code  
 Description: 
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteInitialiseTarget(void)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = INITIALISE_TARGET_OPCODE;
   tyRealMonPacket.ulChannel = 0;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteWriteBytes
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to which data to be written
              unsigned long ulCount - Number of bytes to write
              unsigned char *pucData - pointer to write data
      Output: TyError - RDI Layer Error code  
 Description: Write data to target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteWriteBytes(unsigned long ulAddress,
                          unsigned long ulCount,
                          unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long i;

   tyRealMonPacket.ulLength  = ulCount + 4;
   tyRealMonPacket.ulOpcode  = WRITE_BYTES_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;

   // Copy over the data...
   for (i=0; i < ulCount; i++)
      tyRealMonPacket.ucData[i+4] = pucData[i];

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteReadBytes
    Engineer: Suraj S
       Input: unsigned char bSetupAddress - Boolean to set the default length
              unsigned long ulAddress - Address to read from
              unsigned char bSetupCount - Boolean to set the default length
              unsigned long ulCount - Number of bytes to read
              unsigned char *pucData - pointer to read data
      Output: TyError - RDI Layer Error code  
 Description: Read data from target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteReadBytes(unsigned char bSetupAddress,
                         unsigned long ulAddress,
                         unsigned char bSetupCount,
                         unsigned long ulCount,
                         unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long   i;

   if (bSetupCount != FALSE)
      tyRealMonPacket.ulLength = 8;
   else if (bSetupAddress != FALSE)
      tyRealMonPacket.ulLength = 4;
   else 
      tyRealMonPacket.ulLength = 0;

   tyRealMonPacket.ulOpcode  = READ_BYTES_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;
   *(unsigned long *)&tyRealMonPacket.ucData[4] = ulCount;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      goto ErrorHandler;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      goto ErrorHandler;

   // OK, check that the opcode was OK....
   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      goto ErrorHandler;

   // Check that no exceptions occurred....
#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   // For all the words with this command, swap the bytes
   for (i=0; i < (ulCount & 0xFFFFFFFC); i+=4)
      {
      pucData[i+0] = tyRealMonPacket.ucData[i+3];
      pucData[i+1] = tyRealMonPacket.ucData[i+2];
      pucData[i+2] = tyRealMonPacket.ucData[i+1];
      pucData[i+3] = tyRealMonPacket.ucData[i+0];
      }
   switch (ulCount & 0x3)
      {
      default:
         // Nothing to do...
         break;
      case 0x1:
         // Just copy over the single byte...
         pucData[i]   = tyRealMonPacket.ucData[i];
         break;
      case 0x2:
         // Swap the two bytes...
         pucData[i+0] = tyRealMonPacket.ucData[i+1];
         pucData[i+1] = tyRealMonPacket.ucData[i+0];
         break;
      case 0x3:
         // Swap the three bytes...
         pucData[i+0] = tyRealMonPacket.ucData[i+2];
         pucData[i+1] = tyRealMonPacket.ucData[i+1];
         pucData[i+2] = tyRealMonPacket.ucData[i+0];
         break;
      }

   return RDIError_NoError;

ErrorHandler:
   // If an error ocurred, we do not want to repeatedly display an error.
   for (i=0; i < ulCount; i++)
      pucData[i] = 0xFF;

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteReadHalfWords
    Engineer: Suraj S
       Input: unsigned char bSetupAddress - Boolean to set the default length
              unsigned long ulAddress - Address to read from
              unsigned char bSetupCount - Boolean to set the default length
              unsigned long ulCount - Number of half words to read
              unsigned char *pucData - pointer to read data
      Output: TyError - RDI Layer Error code  
 Description: Read data from target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteReadHalfWords(unsigned char  bSetupAddress,
                             unsigned long  ulAddress,
                             unsigned char  bSetupCount,
                             unsigned long  ulCount,
                             unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long   i;

   // Count is always in bytes !!!
   ulCount *= 2;

   if (bSetupCount != FALSE)
      tyRealMonPacket.ulLength = 8;
   else if (bSetupAddress != FALSE)
      tyRealMonPacket.ulLength = 4;
   else 
      tyRealMonPacket.ulLength = 0;

   tyRealMonPacket.ulOpcode  = READ_HALFWORDS_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;
   *(unsigned long *)&tyRealMonPacket.ucData[4] = ulCount;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      goto ErrorHandler;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      goto ErrorHandler;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      goto ErrorHandler;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   // For all the words with this command, swap the bytes
   for (i=0; i < (ulCount & 0xFFFFFFFC); i+=4)
      {
      pucData[i+0] = tyRealMonPacket.ucData[i+2];
      pucData[i+1] = tyRealMonPacket.ucData[i+3];
      pucData[i+2] = tyRealMonPacket.ucData[i+0];
      pucData[i+3] = tyRealMonPacket.ucData[i+1];
      }
   switch (ulCount & 0x3)
      {
      default:
         // Nothing to do...
         break;
      case 0x2:
         // Swap the two bytes...
         pucData[i+0] = tyRealMonPacket.ucData[i+0];
         pucData[i+1] = tyRealMonPacket.ucData[i+1];
         break;
      }
   for (i=0; i < ulCount; i++)
      pucData[i] = tyRealMonPacket.ucData[i];

   return RDIError_NoError;

ErrorHandler:
   // If an error ocurred, we do not want to repeatedly display an error.
   for (i=0; i < ulCount; i++)
      pucData[i] = 0xFF;

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteWriteHalfWords
    Engineer: Martin Hannon
       Input: unsigned long ulAddress - Address to which data to be written
              unsigned long ulCount - Number of half words to write
              unsigned char *pucData - pointer to write data
      Output: TyError - RDI Layer Error code  
 Description: Write data to target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteWriteHalfWords(unsigned long ulAddress,
                              unsigned long ulCount,
                              unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long i;

   // Count is always in bytes !!!
   ulCount *= 2;

   tyRealMonPacket.ulLength  = ulCount + 4;
   tyRealMonPacket.ulOpcode  = WRITE_HALFWORDS_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;

   for (i=0; i < ulCount; i++)
      tyRealMonPacket.ucData[i+4] = pucData[i];

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteReadWords
    Engineer: Suraj S
       Input: unsigned char bSetupAddress - Boolean to set the default length
              unsigned long ulAddress - Address to read from
              unsigned char bSetupCount - Boolean to set the default length
              unsigned long ulCount - Number of words to read
              unsigned char *pucData - pointer to read data
      Output: TyError - RDI Layer Error code  
 Description: Read data from target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteReadWords(unsigned char  bSetupAddress,
                         unsigned long  ulAddress,
                         unsigned char  bSetupCount,
                         unsigned long  ulCount,
                         unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long   i;

   // Count is always in bytes !!!
   ulCount *= 4;

   if (bSetupCount != FALSE)
      tyRealMonPacket.ulLength = 8;
   else if (bSetupAddress != FALSE)
      tyRealMonPacket.ulLength = 4;
   else 
      tyRealMonPacket.ulLength = 0;

   tyRealMonPacket.ulOpcode  = READ_WORDS_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;
   *(unsigned long *)&tyRealMonPacket.ucData[4] = ulCount;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      goto ErrorHandler;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      goto ErrorHandler;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      goto ErrorHandler;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   for (i=0; i < tyRealMonPacket.ulLength-4; i++)
      pucData[i] = tyRealMonPacket.ucData[i];

   return RDIError_NoError;

ErrorHandler:
   // If an error ocurred, we do not want to repeatedly display an error.
   for (i=0; i < ulCount; i++)
      pucData[i] = 0xFF;

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteWriteWords
    Engineer: Suraj S
       Input: unsigned long ulAddress - Address to which data to be written
              unsigned long ulCount - Number of words to write
              unsigned char *pucData - pointer to write data
      Output: TyError - RDI Layer Error code  
 Description: Write data to target using real monitor
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteWriteWords(unsigned long ulAddress,
                          unsigned long ulCount,
                          unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long i;

   // Count is always in bytes !!!
   ulCount *= 4;

   tyRealMonPacket.ulLength  = ulCount + 4;
   tyRealMonPacket.ulOpcode  = WRITE_WORDS_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulAddress;

   for (i=0; i < ulCount; i++)
      tyRealMonPacket.ucData[i+4] = pucData[i];

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteReadRegisters
    Engineer: Suraj S
       Input: unsigned long ulFirstMask -
              unsigned char bSetupSecondMask - 
              unsigned long ulSecondMask -
              unsigned char *pucData - 
      Output: TyError - RDI Layer Error code  
 Description: Reads the target registers
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteReadRegisters(unsigned long ulFirstMask,
                             unsigned char bSetupSecondMask,
                             unsigned long ulSecondMask,
                             unsigned char *pucData)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long i;


   if (bSetupSecondMask != FALSE)
      tyRealMonPacket.ulLength = 8;
   else 
      tyRealMonPacket.ulLength = 4;

   tyRealMonPacket.ulLength  = 0;
   tyRealMonPacket.ulOpcode  = READ_REGISTERS_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   *(unsigned long *)&tyRealMonPacket.ucData[0] = ulFirstMask;
   *(unsigned long *)&tyRealMonPacket.ucData[4] = ulSecondMask;

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   for (i=0; i < tyRealMonPacket.ulLength; i++)
      pucData[i] = tyRealMonPacket.ucData[i];

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteWriteRegisters
    Engineer: Suraj S
       Input: unsigned long ulCount -
              unsigned long *pulMasks -
              unsigned long *pulValues -
      Output: TyError - RDI Layer Error code  
 Description: Writes to the target registers
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteWriteRegisters(unsigned long ulCount,
                              unsigned long *pulMasks,
                              unsigned long *pulValues)
{
   TyError ErrRet;
   unsigned long   ulExceptionStatus;
   TyRealMonPacket tyRealMonPacket;
   unsigned long i;

   tyRealMonPacket.ulLength  = (ulCount * 8);
   tyRealMonPacket.ulOpcode  = WRITE_REGISTERS_OPCODE;
   tyRealMonPacket.ulChannel = 0;
   for (i=0; i < ulCount; i++)
      {
      *(unsigned long *)&tyRealMonPacket.ucData[(i*8)]   = pulMasks[i];
      *(unsigned long *)&tyRealMonPacket.ucData[(i*8)+4] = pulValues[i];
      }

   ErrRet = RealMonSendPacket(tyRealMonPacket);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   ErrRet = RealMonReceivePacket(&tyRealMonPacket, &ulExceptionStatus);
   if (ErrRet != RDIError_NoError)
      return ErrRet;

   if (tyRealMonPacket.ulOpcode != OK_STATUS)
      return RDIError_RealMonFailed;

#ifdef DEBUG
   if (ulExceptionStatus != 0)
      MessageBox(NULL,"Exception Occurred",OPXDCAPTION,MB_OK|MB_ICONWARNING);
      //AfxMessageBox("Exception Occurred");
#endif

   return RDIError_NoError;
}

/****************************************************************************
    Function: ExecuteReadCPRegister
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code 
 Description: 
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteReadCPRegister(void)
{
   // This command code is currently reserved...
   return RDIError_RealMonFailed;
}

/****************************************************************************
    Function: ExecuteWriteCPRegister
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code 
 Description: 
Date          Initials     Description
20-Aug-2007     SJ         Initial
****************************************************************************/
TyError ExecuteWriteCPRegister(void)
{
   // This command code is currently reserved...
   return RDIError_RealMonFailed;
}

/****************************************************************************
    Function: InitialiseCommsLink
    Engineer: Suraj S
       Input: NONE
      Output: TyError - RDI Layer Error code  
 Description: 
Date          Initials     Description
17-Aug-2007     SJ         Initial
****************************************************************************/
TyError InitialiseCommsLink(void)
{
   TyError ErrRet;
   unsigned long pulQueuedData[0x2] = {0};
   unsigned long ulWriteData;
   unsigned long ulReadData;
   unsigned char bReceivedResetSequence;

   bReceivedResetSequence = FALSE;
   do
      {
      ErrRet = RealMonReadDCC(&ulReadData);
      if (ErrRet == RDIError_NoError)
         {
         // Hey, we read data...
         pulQueuedData[0] = pulQueuedData[1];
         pulQueuedData[1] = ulReadData;

         if ((pulQueuedData[0] == ESCAPE_SEQUENCE) &&
             (pulQueuedData[1] == ESCAPE_RESET   ))
            bReceivedResetSequence = TRUE;
         else 
            bReceivedResetSequence = FALSE;
         }
      }
   while ((ErrRet == RDIError_NoError) && !bReceivedResetSequence);

   if (bReceivedResetSequence)
      {
      // OK, we got a reset sequence, send an OK....
      ulWriteData = ESCAPE_SEQUENCE;

      ErrRet = RealMonWriteDCC(ulWriteData);
      if (ErrRet != RDIError_NoError)
         goto ErrorHandler;

      ulWriteData = ESCAPE_GO;

      ErrRet = RealMonWriteDCC(ulWriteData);
      if (ErrRet != RDIError_NoError)
         goto ErrorHandler;
      }
   else
      {
      // OK, we haven't got a reset sequence, send one....
      ulWriteData = ESCAPE_SEQUENCE;

      ErrRet = RealMonWriteDCC(ulWriteData);
      if (ErrRet != RDIError_NoError)
         goto ErrorHandler;

      ulWriteData = ESCAPE_RESET;

      ErrRet = RealMonWriteDCC(ulWriteData);
      if (ErrRet != RDIError_NoError)
         goto ErrorHandler;

      // Now, try to receive back the OK...
      ErrRet = RealMonReadDCC(&ulReadData);
      if (ErrRet != RDIError_NoError)
         goto ErrorHandler;

      if (ulReadData != ESCAPE_SEQUENCE)
         goto ErrorHandler;

      ErrRet = RealMonReadDCC(&ulReadData);
      if (ErrRet != RDIError_NoError)
         goto ErrorHandler;

      if (ulReadData != ESCAPE_GO)
         goto ErrorHandler;
      }

   bGlobalRealMonitorDetected = TRUE;

   return RDIError_NoError;

ErrorHandler:
   bGlobalRealMonitorDetected = FALSE;
   return RDIError_NoError;

}
