/****************************************************************************
       Module: rdilayerarm.h
     Engineer: Suraj S
  Description: Header for ARM RDI Driver Interface for Opella-XD
Date           Initials    Description
13-Jul-2007    SJ          Initial
06-Jun-2008	   RS		   Added Linux Support
****************************************************************************/
#ifndef RDILAYERARM_H_
#define RDILAYERARM_H_

//#define RDI_VERSION 151

#include "../rdi/armtypes.h"
#include "../rdi/toolconf.h"
#include "../rdi/rdi.h"
#include "../rdi/rdi150.h"
#include "../rdi/rdi_prop.h"
#ifdef _WINDOWS
#include "../rdi/winrdi.h"
#endif
#ifdef __LINUX
#include "../rdi/sordi.h"
#endif

#endif  

 // RDILAYERARM_H_

