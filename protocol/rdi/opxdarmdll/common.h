/****************************************************************************
       Module: COMMON.H
     Engineer: Suraj S
  Description: Common Header file
Date          Initials    Description
3-Jul-2007      SJ        Initial
23-Apr-2010      JCK        Added support for Cortex cores.
****************************************************************************/
#ifndef _COMMON_H_
#define _COMMON_H_

#if defined (DEBUG) || defined (_DEBUG)
#define DLEVEL_NAMES
#endif

#if defined DLEVEL_NAMES
#define DLOG(VARS) OutputDebugString(VARS)
#else
#define DLOG(VARS) ((void)0)
#endif

//MESSAGES
#define OPXDCAPTION "Opella-XD for ARM"

// Noreference
#define NOREF(var)  (var=var)

#define MAX_OPELLA_XD_USER_ID_LENGTH  0x20
#define MAX_SERIAL_NUMBER_LEN 16
#define MAX_STRING_LENGTH   81     /* including the '\0' */

//This is used for Opella XD Serial Numbers
#define NO_SERIAL_NUMBER 0xFFFFFFFF

#define SET_DEBUG_REQUEST 1
#define CLEAR_DEBUG_REQUEST 0

#define BLOCKSIZE        0x38
#define MAX_WP_REG       12
#define MAX_BP_REG_ARM11 12

#ifndef __LINUX
#define strcmpi _strcmpi
#endif

typedef unsigned char boolean;

typedef enum {  
             
	//ARM Section
	ARM7DI      = 0x00,
	ARM7TDMI    = 0x01,
	ARM7TDMIS   = 0x02,
	ARM720T     = 0x03,
	ARM740T     = 0x04,
	ARM9TDMI    = 0x05,
	ARM920T     = 0x06,
	ARM922T     = 0x07,
	ARM926EJS   = 0x08,
	ARM940T     = 0x09,
	ARM946ES    = 0x0A,
	ARM966ES    = 0x0B,
	ARM968ES    = 0x0C,
    ARM1136JFS  = 0x0D,
    ARM1156T2S  = 0x0E,
    ARM1176JZS  = 0x0F,
    ARM11MP     = 0x10,
	CORTEX_M0	= 0x11,
	CORTEX_M1	= 0x12,
	CORTEX_M3	= 0x13,
	CORTEX_M4	= 0x14,
    CORTEX_A5   = 0x15,
	CORTEX_A8	= 0x16,
	CORTEX_A9	= 0x17,
	CORTEX_R4	= 0x18,
} TXRXProcTypes;

typedef enum {  
    ISCORE      = 0x00,         
	//Marvell Cores
	FEROCEON      = 0x01,
	DRAGONITE    = 0x02

} TXRXDeviceTypes;
// ARM processor family types.
typedef enum 
{
	NO_COPROCESSOR  ,
	ARM720T_CP      ,
	ARM740T_CP      ,
	ARM920T_CP      ,
	ARM922T_CP      ,
	ARM926EJS_CP    ,
	ARM940T_CP      ,
	ARM946ES_CP     ,
	ARM966ES_CP     ,
    ARM968ES_CP     ,
    ARM1136JFS_CP   ,
    ARM1156T2S_CP   ,
    ARM1176JZS_CP   ,
    ARM11MP_CP      , 
	CORTEXA8_CP		,
	CORTEXA9_CP
} TyCoProcessorType;

// This structure defines the config data given from the debugger.
typedef struct
{
   boolean         bUSBDevice;
   unsigned long   ulByteSex;
   unsigned long   ulSNVAddress;
   unsigned long   ulCacheCleanAddress;
   double          dJTAGFreq;
   double          dTargetVoltage;
   unsigned long   ulDebugCoreNumber;
   unsigned long   ulStartupDelay;
   unsigned char   ucShiftIRPreTCKs;
   unsigned char   ucShiftIRPostTCKs;
   unsigned char   ucShiftDRPreTCKs;
   unsigned char   ucShiftDRPostTCKs;
   unsigned long   ulSelectedNoOfCores;
   unsigned short  usOpellaXDProductID;
   unsigned long   ulOpellaXDInstance;
   boolean         bUseHardReset;
   boolean         bDebugFromReset;
   boolean		   bCoreReset;
   boolean		   bPeripheralReset;
   boolean         bUseToolConfSemiHost;
   boolean         bAdaptiveClockingEnabled;
   boolean         bNonStopDebugSelected;
   boolean         bUseNTRST;
   boolean         bDebugRequestState;
   boolean         bHotPlug;
   boolean         bHaltCoreOnConnection;
   char            szProcessorType[MAX_STRING_LENGTH];
   char            szDebuggerName[MAX_STRING_LENGTH];
   char  		   szProcessorName[MAX_STRING_LENGTH];
   char            szOpellaXDUserID[MAX_OPELLA_XD_USER_ID_LENGTH];
   char            szOpellaXDSerialNumber[MAX_SERIAL_NUMBER_LEN];
   boolean         bUseGNUSemihosting;
   TXRXProcTypes   tyProcType;
   TXRXDeviceTypes tyDeviceType;
   unsigned long   ulCoresightBaseAddr;
   unsigned long   ulDbgApIndex;
   unsigned long   ulMemApIndex;
   unsigned long   ulUseMemApForMemAccess;
   unsigned long   ulJtagApIndex;
   boolean         bCoresightCompliant;
   boolean         bTICore;
   unsigned long   ulTISubPortNum;
   boolean		   bDapPcSupport;  
   boolean		   bCacheInvalidationNeeded;
} TyTargetConfig;

typedef struct
{
	boolean           bARM9;
	boolean           bARM7;
    boolean           bARM11;
    boolean           bCortexM3;
	boolean			  bCortexA8;
	boolean			  bCortexA9;
	TyCoProcessorType tyCoProcessorType;
	boolean           bETM;
	boolean           bThumb;
	boolean           bDCC;
	boolean           bCheckDRReturn;
	boolean           bSynthesisedCore;
	unsigned long	  ulICEBreakerRevision;
	TXRXProcTypes     tyProcType;
} TyProcessorConfig;

typedef struct  
{
	char			szCoreName[50];
	TXRXProcTypes   tyProcType;
	TXRXDeviceTypes tyDeviceType;
} TyProcessorList;

// Cause of break
typedef enum
{
   LOSS_OF_CLOCK               = 0x00,
   TARGET_PROC_RST             = 0x01,
   TARGET_PROC_POWER_DOWN      = 0x02,
   TRACE_BUF_FULL              = 0x03,
   STOP_TRIGGER                = 0x04,
   TIMER_EXP                   = 0x05,
   BP_OPCODE                   = 0x06,
   BP_DATA_RD                  = 0x07,
   BP_DATA_WR                  = 0x08,
   EXT_HALT                    = 0x09,
   USER_HALT                   = 0x0A,
   SINGLE_STEP                 = 0x0B,
   TARGET_PROC_IDLE            = 0x0C,
   BP_PAGE                     = 0x0D,
   COB_UNKNOWN                 = 0x0E,
   TARGET_PROC_ACK             = 0x0F,
   N_FRAMES_AFTER_STOP_TRIGGER = 0x10,
   ILLEGAL_OPCODE              = 0x11,
   EXEC_OUTSIDE_WIN_SP         = 0x12,
   EXEC_OUTSIDE_IMPLEM_SP      = 0x13,
   EXEC_DURING_FAME_CALC       = 0x14,
   EXEC_DURING_EE_PROG         = 0x15,
   DDCHECK_MISMATCH            = 0x16,
   STEP_OVER_CALL              = 0x17,
   BP_HARDWARE                 = 0x18,
   COB_WATCHPOINT              = 0x19,
   COB_USER_INTERRUPT          = 0x1A,
   COB_BRANCH_THROUGH0         = 0x1B,
   COB_SOFTWARE_INTERRUPT      = 0x1C,
   COB_PREFETCH_ABORT          = 0x1D,
   COB_DATA_ABORT              = 0x1E,
   COB_ADDRESS_EXCEPTION       = 0x1F,
   COB_IRQ                     = 0X20,
   COB_FIQ                     = 0X21,   
   GEN_TGT_PROC_RST            = 0X22,
   ANGEL_EXCEPTION_HALT        = 0x23,
   COB_EXCEPTION               = 0x24,
   COB_DEBUG_EXCEPTION         = 0x25,
   COB_CALVA_XDATA             = 0x26, 
   COB_SFR_COMPARATOR          = 0x27, 
   COB_EXT_RESET               = 0x28, 
   COB_EXT_POWER               = 0x29,
   BP_WATCHPOINT_MATCH         = 0x30,
   BP_WATCHPOINT_MISS          = 0x31
} TXRXCauseOfBreak;

typedef enum
{
	Serial      = 0,
	Ethernet    = 1,
	Parallel    = 3,
	Usb         = 4
} TyTargetConnection;

typedef struct
   {
      char szOpellaXDSerialNumber[100];
      char szTPAVersion[100];
      char szManufactured[100];
      char szRDIDriverVersion[100];
      char szFirmWare[100];
      char szDiskWare[100];
      char szFpgaWare[100];
      char szUSBDriverVersion[100];
   }TyAshlingVersion;

#define MAX_CORESIGHT_COMP 960
enum TyMajorType
{
	Miscellaneous	= 0,
	TraceSink		= 1,
	TraceLink		= 2,
	TraceSource		= 3,
	DebugControl	= 4,
	DebugLogic		= 5
};

enum TySubType
{
	/* Sub Types of Miscellaneous */
	OtherUndefined					= 0,	
	Validationcomponent				= 2,

	/* Sub Types of TraceSink */
	OtherTraceSink					= 0,
	TracePortForExampleTPIU			= 1,
	BufferForExampleETB				= 2,

	/* Sub Types of TraceLink */
	OtherTraceLink					= 0,
	TraceFunnelRouter				= 1,
	Filter							= 2,
	FIFOLargeBuffer					= 3,
	
	/* Sub Types of TraceSource */
	OtherTraceSource				= 0,
	AssociateProcessorCore			= 1,
	AssociateDSP					= 2,
	AssociateDataEngineCoprocessor	= 3,
	AssociateBusStimulusBusActivity = 4,

	/* Sub Types of DebugControl */
	OtherDebugControl				= 0,
	TriggerMatrixECT				= 1,
	DebugAuthenticationModule		= 2,

	/* Sub Types of DebugLogic */
	OtherDebugLogic					= 0,
	ProcessorCore					= 1,
	DSP								= 2,
	DataEngineCoprocessor			= 3,
	
};

typedef struct
{
	TySubType tySubType;
	char szSubTypeDesr[70];
} TySubTypeStruct;

typedef struct
{
	TyMajorType			tyMajorType;
	char				szMajorTypeDesr[30];
	TySubTypeStruct*	ptySubTypeStruct;
} TyDeviceType;

/* Definitions of Device Type Identifier Register, DEVTYPE */
#define MAJOR_TYPE	0xF << 0
#define SUB_TYPE	0xF << 4

#define MAX_NUM_SUB_TYPES	0xF

typedef struct
{
    unsigned long ulEntryPresent;
    unsigned long ulCompBaseAddr;
    unsigned long ulCompClass;
    unsigned long ulCompType;
} TyRomTableEntry;

typedef struct
{
    unsigned long ulNumEntries;
    unsigned long ulIdrRegValue;
    unsigned long ulDbgBaseRegValue;
    unsigned long ulConfiRegValue;
    TyRomTableEntry tyRomTableEntry[MAX_CORESIGHT_COMP];
}TyCoresightComp;

#endif // _COMMON_H_
