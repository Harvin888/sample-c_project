/****************************************************************************
       Module: disaarm.h
     Engineer: Jeenus Challattu Kunnath
  Description: Header file for ARM Disassembler
Date           Initials    Description
14-Mar-2008    JCK          Initial
****************************************************************************/
// main header file for driver layer needs to be included
#include "../../common/drvopxd.h"
#define DISARM_MAXSTRINGSIZE 256

//unsigned long int ulProgramCounter;
#define PCPipeBehavour 0x8

/* Type definitions for instructions */
typedef unsigned int ulArmType; /* 32 bits */
typedef int ConditionPosition;

//*******************************************
// Describe the different instruction formats
//*******************************************

//V4T instruction set
typedef struct  multiplylongandaccumulatelong{
  ulArmType Rm 			: 4;
  ulArmType mustbe1001 	: 4;
  ulArmType Rs   		: 4;
  ulArmType RdLo 		: 4;
  ulArmType RdHi 		: 4;
  ulArmType S 			: 1;
  ulArmType A 			: 1;
  ulArmType U 			: 1;
  ulArmType mustbe00001 : 5;
  ulArmType condition 	: 4;
} MultiplyLongAndAccumulateLong;

typedef struct  hwasigneddatatransferio {
  ulArmType OffSetLo : 4;
  ulArmType B4mustbe1 : 1;
  ulArmType H : 1;
  ulArmType S : 1;
  ulArmType B7mustbe1 : 1;
  ulArmType OffSetHi : 4;
  ulArmType Rd : 4;
  ulArmType Rn : 4;
  ulArmType L : 1;
  ulArmType W : 1;
  ulArmType B22mustbe1 : 1;
  ulArmType U : 1;
  ulArmType P : 1;
  ulArmType mustbe000 : 3;
  ulArmType condition : 4;
} HWaSignedDataTransferIO;

typedef struct  hwasigneddatatransferro {
  ulArmType Rm : 4;
  ulArmType mustbe1 : 1;
  ulArmType H : 1;
  ulArmType S : 1;
  ulArmType mustbe00001 : 5;
  ulArmType Rd : 4;
  ulArmType Rn : 4;
  ulArmType L : 1;
  ulArmType W : 1;
  ulArmType mustbe0 : 1;
  ulArmType U : 1;
  ulArmType P : 1;
  ulArmType mustbe000 : 3;
  ulArmType condition : 4;
} HWaSignedDataTransferRO;

typedef struct swi {
  ulArmType comment : 24;
  ulArmType mustbe1111 : 4;
  ulArmType condition : 4;
} SWI;

typedef struct cpregtrans {
  ulArmType CRm : 4;
  ulArmType mustbe1 : 1;
  ulArmType CP : 3;
  ulArmType CPNum : 4;
  ulArmType Rd : 4;
  ulArmType CRn : 4;
  ulArmType L : 1;
  ulArmType CPOpc : 3;
  ulArmType mustbe1110 : 4;
  ulArmType condition : 4;
} CPRegTrans;

typedef struct cpdataop {
  ulArmType CRm : 4;
  ulArmType mustbe0 : 1;
  ulArmType CP : 3;
  ulArmType CPNum : 4;
  ulArmType CRd : 4;
  ulArmType CRn : 4;
  ulArmType CPOpc : 4;
  ulArmType mustbe1110 : 4;
  ulArmType condition : 4;
} CPDataOp;

typedef struct cpdatatrans {
  ulArmType offset : 8;
  ulArmType CPNum : 4;
  ulArmType CRd : 4;
  ulArmType Rn : 4;
  ulArmType L : 1;
  ulArmType W : 1;
  ulArmType N : 1;
  ulArmType U : 1;
  ulArmType P : 1;
  ulArmType mustbe110 : 3;
  ulArmType condition : 4;
} CPDataTrans;

typedef struct branch {
  ulArmType offset : 23;
  ulArmType sign : 1;
  ulArmType L : 1;
  ulArmType mustbe101 : 3;
  ulArmType condition : 4;
} Branch;

typedef struct blockdatatrans {
  ulArmType registerlist : 16;
  ulArmType Rn : 4;
  ulArmType L : 1;
  ulArmType W : 1;
  ulArmType S : 1;
  ulArmType U : 1;
  ulArmType P : 1;
  ulArmType mustbe100 : 3;
  ulArmType condition : 4;
} BlockDataTrans;

typedef struct undefined {
  ulArmType dontcareA : 4;
  ulArmType mustbe1 : 1;
  ulArmType dontcareB : 20;
  ulArmType mustbe011 : 3;
  ulArmType condition : 4;
} Undefined;

typedef struct singledatatrans {
  ulArmType offset : 12;
  ulArmType Rd : 4;
  ulArmType Rn : 4;
  ulArmType L : 1;
  ulArmType W : 1;
  ulArmType B : 1;
  ulArmType U : 1;
  ulArmType P : 1;
  ulArmType I : 1;
  ulArmType mustbe01 : 2;
  ulArmType condition : 4;
} SingleDataTrans;

typedef struct singledataswap {
  ulArmType Rm : 4;
  ulArmType mustbe00001001 : 8;
  ulArmType Rd : 4;
  ulArmType Rn : 4;
  ulArmType mustbe00 : 2;
  ulArmType B : 1;
  ulArmType mustbe00010 : 5;
  ulArmType condition : 4;
} SingleDataSwap;

typedef struct multiply {
  ulArmType Rm : 4;
  ulArmType mustbe1001 : 4;
  ulArmType Rs : 4;
  ulArmType Rn : 4;
  ulArmType Rd : 4;
  ulArmType S : 1;
  ulArmType A : 1;
  ulArmType mustbe000000 : 6;
  ulArmType condition : 4;
} Multiply;

typedef struct msrregtopsr {
  ulArmType Rm : 4;
  ulArmType mustbe00000000 : 8;
  ulArmType mustbe1111     : 4;
  ulArmType field          : 4;
  ulArmType mustbe10       : 2;
  ulArmType Pd             : 1;
  ulArmType mustbe00010    : 5;
  ulArmType condition      : 4;
} MSRRegToPSR;

typedef struct msrimmtopsr {
  ulArmType immediate      : 8;
  ulArmType rot            : 4;
  ulArmType mustbe1111     : 4;
  ulArmType field          : 4;
  ulArmType mustbe10       : 2;
  ulArmType R              : 1;
  ulArmType mustbe00110    : 5;
  ulArmType condition      : 4;
} MSRImmToPSR;

typedef struct msrtopsrflags {
  ulArmType operand : 12;
  ulArmType mustbe1010001111 : 10;
  ulArmType Pd : 1;
  ulArmType mustbe10 : 2;
  ulArmType I : 1;
  ulArmType mustbe00 : 2;
  ulArmType condition : 4;
} MSRToPSRFlags;

typedef struct mrspsrtoreg {
  ulArmType mustbe000000000000 : 12;
  ulArmType Rd : 4;
  ulArmType mustbe001111 : 6;
  ulArmType Ps : 1;
  ulArmType mustbe00010 : 5;
  ulArmType condition : 4;
} MRSPSRToReg;

typedef struct dataproc {
  ulArmType operand2 : 12;
  ulArmType Rd : 4;
  ulArmType Rn : 4;
  ulArmType S : 1;
  ulArmType opcode : 4;
  ulArmType I : 1;
  ulArmType mustbe00 : 2;
  ulArmType condition : 4;
} DataProc;

typedef struct branchexch {
  ulArmType Rn : 4;
  ulArmType	mustbe000100101111111111110001: 24;
  ulArmType condition : 4;
} BranchExch;


//Added for V6T
typedef struct  usmultiplylongandaccumulatelong{
	ulArmType Rm 			: 4;
	ulArmType mustbe1001 	: 4;
	ulArmType Rs   		: 4;
	ulArmType RdLo 		: 4;
	ulArmType RdHi 		: 4;
	ulArmType mustbe00000100 : 8;
	ulArmType condition 	: 4;
} USMultiplyLongAndAccumulateLong;

typedef struct  parralleladdsub{
	ulArmType Rm        : 4;
	ulArmType mustbe1   : 1;
	ulArmType opc2 		: 3;
	ulArmType SBO 	    : 4;
	ulArmType Rd   		: 4;
	ulArmType Rn 		: 4;
	ulArmType opc1 		: 3;
	ulArmType mustbe01100 : 5;
	ulArmType condition 	: 4;
} ParallelAddSub;

typedef struct  halfwordpack{
	ulArmType Rm        : 4;
	ulArmType mustbe01  : 2;
	ulArmType op        : 1;
	ulArmType shiftimm  : 5;
	ulArmType Rd   		: 4;
	ulArmType Rn 		: 4;
	ulArmType mustbe01101000 : 8;
	ulArmType condition 	: 4;
} HalfWordPack;

typedef struct  wordsaturate{
	ulArmType Rm        : 4;
	ulArmType mustbe01  : 2;
	ulArmType sh        : 1;
	ulArmType shiftimm  : 5;
	ulArmType Rd   		: 4;
	ulArmType satimm	: 5;
	ulArmType mustbe1   : 1;
	ulArmType U         : 1;
	ulArmType mustbe01101 : 5;
	ulArmType condition 	: 4;
} WordSaturate;

typedef struct  parhalfwordsaturate{
	ulArmType Rm        : 4;
	ulArmType mustbe0011: 4;
	ulArmType SBO       : 4;
	ulArmType Rd   		: 4;
	ulArmType satimm	: 4;
	ulArmType mustbe10  : 2;
	ulArmType U         : 1;
	ulArmType mustbe01101 : 5;
	ulArmType condition 	: 4;
} ParHalfWordSaturate;

typedef struct  reverse{
	ulArmType Rm        : 4;
	ulArmType mustbe011 : 3;
	ulArmType status2	: 1;
	ulArmType SBO2      : 4;
	ulArmType Rd   		: 4;
	ulArmType SBO1	    : 4;
	ulArmType mustbe11  : 2;
	ulArmType status1   : 1;
	ulArmType mustbe01101 : 5;
	ulArmType condition   : 4;
} Reverse;

typedef struct  selectbytes{
	ulArmType Rm        : 4;
	ulArmType mustbe1011: 4;
	ulArmType SBO       : 4;
	ulArmType Rd   		: 4;
	ulArmType Rn	    : 4;
	ulArmType mustbe01101000 : 8;
	ulArmType condition 	: 4;
} SelectBytes;

typedef struct  signzeroextend{
	ulArmType Rm        : 4;
	ulArmType mustbe0111: 4;
	ulArmType SBZ       : 2;
	ulArmType rotate    : 2;
	ulArmType Rd   		: 4;
	ulArmType Rn	    : 4;
	ulArmType op2	    : 2;
	ulArmType op1	    : 1;
	ulArmType mustbe01101 : 5;
	ulArmType condition 	: 4;
} SignZeroExtend;

typedef struct  multiplytype3{
	ulArmType Rm        : 4;
	ulArmType mustbe1   : 1;
	ulArmType X			: 1;
	ulArmType opc2      : 2;
	ulArmType Rs   		: 4;
	ulArmType RdLO   	: 4;
	ulArmType RdHI   	: 4;
	ulArmType opc1	    : 3;
	ulArmType mustbe01110 : 5;
	ulArmType condition 	: 4;
} MultiplyType3;

typedef struct  ussumofabsdiff{
	ulArmType Rm        : 4;
	ulArmType mustbe0001: 4;
	ulArmType Rs   		: 4;
	ulArmType Rn    	: 4;
	ulArmType Rd    	: 4;
	ulArmType mustbe01111000 : 8;
	ulArmType condition 	: 4;
} UsSumOfAbsDiff;

typedef struct  loadstoreregexclusive{
	ulArmType Rm        : 4;
	ulArmType mustbe1001: 4;
	ulArmType SBO       : 4;
	ulArmType Rd    	: 4;
	ulArmType Rn   		: 4;
	ulArmType L   		: 1;
	ulArmType mustbe0001100 : 7;
	ulArmType condition 	: 4;
} LoadStoreRegExclusive;

typedef struct  changeprocstate{
	ulArmType mode      : 5;
	ulArmType mustbe0a  : 1;
	ulArmType F			: 1;
	ulArmType I			: 1;
	ulArmType A			: 1;
	ulArmType SBZ		: 7;
	ulArmType mustbe0b  : 1;
	ulArmType mmod		: 1;
	ulArmType imod		: 2;
	ulArmType mustbe111100010000 : 12;
} ChangeProcState;

typedef struct  setendian{
	ulArmType SBZ3      : 4;
	ulArmType mustbe0000: 4;
	ulArmType SBZ2      : 1;
	ulArmType E			: 1;
	ulArmType SBZ1		: 6;
	ulArmType mustbe1111000100000001 : 16;
} SetEndian;

typedef struct  savereturnstate{
	ulArmType mode		: 5;
	ulArmType SBZ2		: 3;
	ulArmType mustbe0101: 4;
	ulArmType SBZ1		: 4;
	ulArmType mustbe01101: 5;
	ulArmType W			: 1;
	ulArmType mustbe1	: 1;
	ulArmType U			: 1;
	ulArmType P			: 1;
	ulArmType mustbe1111100 : 7;
} SaveReturnState;

typedef struct  returnfromexeption{
	ulArmType SBZ2		: 8;
	ulArmType mustbe1010: 4;
	ulArmType SBZ1		: 4;
	ulArmType Rn		: 4;
	ulArmType mustbe1	: 1;
	ulArmType W			: 1;
	ulArmType mustbe0	: 1;
	ulArmType U			: 1;
	ulArmType P			: 1;
	ulArmType mustbe1111100 : 7;
} ReturnFromExeption;

typedef struct cpregtranstype2 {
	ulArmType CRm : 4;
	ulArmType Opc : 4;
	ulArmType CPNum : 4;
	ulArmType Rd : 4;
	ulArmType CRn : 4;
	ulArmType L : 1;
	ulArmType mustbe1100010 : 7;
	ulArmType condition : 4;
} CPRegTransType2;


typedef union access32 {
  ulArmType                         ularmType;
  MultiplyLongAndAccumulateLong     multiplylongandaccumulatelong;
  HWaSignedDataTransferIO	         hwasigneddatatransferio;
  HWaSignedDataTransferRO	         hwasigneddatatransferro;
  DataProc                          dataproc;
  BranchExch                        branchexch;
  MRSPSRToReg                       mrspsrtoreg;
  MSRToPSRFlags                     msrtopsrflags;
  MSRRegToPSR                       msrregtopsr;
  MSRImmToPSR                       msrimmtopsr;
  Multiply                          multiply;
  SingleDataSwap                    singledataswap;
  SingleDataTrans                   singledatatrans;
  Undefined                         undefined;
  BlockDataTrans                    blockdatatrans;
  Branch                            branch;
  CPDataTrans                       cpdatatrans;
  CPDataOp                          cpdataop;
  CPRegTrans                        cpregtrans;
  SWI swi;

  //V6TE instruction set
  USMultiplyLongAndAccumulateLong	usmultiplylongandaccumulatelong;
  ParallelAddSub					parralleladdsub;
  HalfWordPack						halfwordpack;
  WordSaturate						wordsaturate;
  ParHalfWordSaturate				parhalfwordsaturate;
  Reverse							reverse;
  SelectBytes						selectbytes;
  SignZeroExtend					signzeroextend;
  MultiplyType3						multiplytype3;
  UsSumOfAbsDiff					ussumofabsdiff;
  LoadStoreRegExclusive				loadstoreregexclusive;
  ChangeProcState					changeprocstate;
  SetEndian							setendian;
  SaveReturnState					savereturnstate;
  ReturnFromExeption				returnfromexeption;
  CPRegTransType2					cpregtranstype2;
} Access32;

typedef struct shiftreg {
  ulArmType Rm : 4;
  ulArmType mustbe1 : 1;
  ulArmType shifttype : 2;
  ulArmType mustbe0 : 1;
  ulArmType Rs : 4;
} ShiftReg;

typedef struct shiftimmed {
  ulArmType Rm : 4;
  ulArmType mustbe0 : 1;
  ulArmType shifttype : 2;
  ulArmType shiftconst : 5;
} ShiftImmed;


typedef union shiftrm {
  ShiftReg shiftreg;
  ShiftImmed shiftimmed;
  ulArmType operand : 12;
} ShiftRm;









