/****************************************************************************
       Module: diagnostics.cpp
     Engineer: Suraj S
  Description: Implementation of ARM RDI Driver diagnostics functions
Date           Initials    Description
11-Apr-2008    SJ          Initial
****************************************************************************/
#ifndef __LINUX
// Win specific defines
#include "stdafx.h"
//#include "windows.h"
#include "stdio.h"
#else
// Linux specific defines
#include "stdio.h"
#endif

#include "../opxdarmdll/dll/Progress.h"

#include "midlayerarm.h"
#include "ARMdefs.h"
#include "usb.h"

#include "drvopxdarm.h"
#include "caches.h"

extern unsigned long RegArray [MAXMODES] [MAXREGISTERS];
extern TyProcessorConfig  tyProcessorConfig;
extern unsigned long      ulGlobalScanChain;
extern unsigned long ulGlobalIDcode0;
extern unsigned long ulGlobalIDcode1;
extern unsigned long pulIRLengthArray[16];
extern TyTargetConfig tyTargetConfig;
extern char szOpXDDiskwareFilename[MAX_DRIVER_NAME] ;
extern char szOpXDFpgawareFilename[MAX_DRIVER_NAME] ;
extern char szOpXDFirmwareFilename[MAX_DRIVER_NAME] ;
extern char szGlobalDLLPathName[_MAX_PATH];
extern const TCHAR *pszRDIDriverVersion;
extern char pszGlobalDllPathName[_MAX_PATH];
extern TyDeviceInfo tyDeviceInfo;
extern boolean bUSBDevice;

extern CProgressControl    *pOpenUSBProgress;

TyDevHandle tyDiagDevHandle = MAX_DEVICES_SUPPORTED;
TyDevHandle tyCurrentDiagDevHandle;

double dGlobalTgtvolt;
unsigned short usDiagnostics = 0;

//Error codes
#define DIAG_ERROR_NO_ERROR					0x0000
#define DIAG_ERROR_USB_DRIVER				0x0001
#define DIAG_ERROR_INVALID_SERIAL_NUMBER	0x0003
#define DIAG_ERROR_TAPID1_FAILED			0x0004
#define DIAG_ERROR_TAPID2_FAILED			0x0005
#define DIAG_ERROR_TAPID_FAILED				0x0006
#define DIAG_ERROR_CPREG_FAILED				0x0007
#define DIAG_ERROR_TAP_LB_FAILED			0x0008
#define DIAG_ERROR_WRONG_CORE				0x0009
#define DIAG_ERROR_TPA_NOT_CONNECTED		0x000A
#define DIAG_ERROR_INVALID_CONFIG			0x000B
#define DIAG_ERROR_CORE_MISS_MATCH			0x000C
#define DIAG_ERROR_READ_ROM_FAILED			0x000D
#define DIAG_ERROR_ARM_NO_DEBUG_ENTRY_AP	0x000E
#define DIAG_ERROR_ARM_NOT_VALID_ROM_ADDR	0x000F
#define DIAG_ERROR_INVALID_CORESIGHT_CONFIG	0x0010
#define DIAG_ERROR_OPELLA_FAILURE           360

#define TSTSC_LENGTH                        512
#define TSTSC_ULONGS                     (TSTSC_LENGTH / 32)

// JTAG scanchain restrictions
#define MAX_SCANCHAIN_LENGTH                  96          // maximum length for scanchain restricted in Opella fw
// maximum device on scanchain (2 bits is minimum length for IR)
// to test scan chain, we shift unsigned long (32 bits) -> number of taps = max_scan_chain_length - 32
#define MAX_TAPS_ON_SCANCHAIN                64

/* Added for coresight support */
#define DEBUG_REG_OFFSET	0xD00

void ConvertVersionString(unsigned long ulVersion, char *pszVersionString);
void ConvertDateString(unsigned long ulDate, char *pszDateString);
void ConvertPcbVersionString(unsigned long ulVersion, char *pszVersionString);
int TestScanChain(TyDevHandle tyDevHandle, TyRDIOnTargetDiagInfo *tyRdiOnTargetDiagInfo);
int ASH_PrepareforDiagnostics(TyDevHandle tyHandle);
TyError DL_OPXD_ConvertDrvlayerErrToDiagErr(TyError tyError);

extern void OpenUsbDeviceMoveProgress(unsigned char ucProgress);


/****************************************************************************
     Function: PerformRDIDiagnostics
     Engineer: Suraj S
        Input: *tyRdiDiagInfo - pointer to TyRDIDiagInfo structure
       Output: TyError - Error code
  Description: Performs RDI Diagnostics and give information to RDI GUI
Date           Initials    Description
28-Sep-2007     SJ          Initial
****************************************************************************/
TyError PerformRDIDiagnostics(char* pszSerialNumber, TyRDIDiagInfo *tyRdiDiagInfo)
{

	TyError tyError = ML_ERROR_NO_ERROR;
    const usb_version *tyUSBVersion;
	char pszUSBVersion[50];
	char szDiskwareFullPath[_MAX_PATH];
	char szFpgawareFullPath[_MAX_PATH];
 
	if(bUSBDevice)
     (void)DL_OPXD_CloseDevice(tyCurrentDiagDevHandle);
       
   tyError = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
   
   tyCurrentDiagDevHandle = tyDiagDevHandle;

	switch (tyError)
	   {
	   case DRVOPXD_ERROR_NO_ERROR:
		   break;
	   case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
		   return DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
	   default:
		   return DRVOPXD_ERROR_USB_DRIVER;
	   }

	tyUSBVersion = usb_get_version();
   if((tyUSBVersion->driver.major == -1) | (tyUSBVersion->driver.minor == -1)|
      (tyUSBVersion->driver.micro == -1) | (tyUSBVersion->driver.nano == -1))
      sprintf(pszUSBVersion, "USB Driver not loaded");
   else
      sprintf(pszUSBVersion, "v%d.%d.%d.%d", tyUSBVersion->driver.major, tyUSBVersion->driver.minor,
		    	  tyUSBVersion->driver.micro, tyUSBVersion->driver.nano);

	//Get the RDI DLL version 
	strcpy (tyRdiDiagInfo->pszDLLVersion, pszRDIDriverVersion);    

	//Get the USB Driver version
	strcpy (tyRdiDiagInfo->pszUSBDriverVersion, pszUSBVersion);    
   
   	//Save the path of the DLL
	ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);

	// get full path to Opella-XD diskware and FPGAware for ARM
	strcpy(szDiskwareFullPath, szGlobalDLLPathName);
	strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
	strcpy(szFpgawareFullPath, szGlobalDLLPathName);
	strcat(szFpgawareFullPath, szOpXDFpgawareFilename);

   (void)DL_OPXD_UnconfigureDevice(tyDiagDevHandle);

	if (DL_OPXD_ConfigureDevice(tyDiagDevHandle, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
   {
      (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DIAG_ERROR_OPELLA_FAILURE;

   }
	//Get device info
	if (DL_OPXD_GetDeviceInfo(tyDiagDevHandle, &tyDeviceInfo) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DRVOPXD_ERROR_USB_DRIVER;
	}

	// Copy Serial Number
	strcpy(tyRdiDiagInfo->pszSerialNumber, tyDeviceInfo.pszSerialNumber);

	tyRdiDiagInfo->bDwRunning = tyDeviceInfo.bDiskwareRunning;
	//Get the Firmware version
	ConvertVersionString(tyDeviceInfo.ulFirmwareVersion, tyRdiDiagInfo->pszFwVersion);
	//Get the FPGAware version
	ConvertVersionString(tyDeviceInfo.ulFpgawareVersion, tyRdiDiagInfo->pszFpgawareVersion);
	//Get the Diskware version
	ConvertVersionString(tyDeviceInfo.ulDiskwareVersion, tyRdiDiagInfo->pszDwVersion);
	//Get the Firmware date
	ConvertDateString(tyDeviceInfo.ulFirmwareDate, tyRdiDiagInfo->pszFwDate);
	//Get the FPGAware date
	ConvertDateString(tyDeviceInfo.ulFpgawareDate, tyRdiDiagInfo->pszFpgawareDate);
	//Get the Diskware date
	ConvertDateString(tyDeviceInfo.ulDiskwareDate, tyRdiDiagInfo->pszDwDate);
	//Get the Hardware revision
	ConvertPcbVersionString(tyDeviceInfo.ulBoardRevision, tyRdiDiagInfo->pszBoardRevision);
	//Get the Manufacturing date
	ConvertDateString(tyDeviceInfo.ulManufacturingDate, tyRdiDiagInfo->pszManufDate);
	//Get TPA type connected
	tyRdiDiagInfo->tyTpaConnected = tyDeviceInfo.tyTpaConnected;
	//Get TPA name
	strcpy(tyRdiDiagInfo->pszTpaName, tyDeviceInfo.pszTpaName);
	//Get TPA version
	ConvertPcbVersionString(tyDeviceInfo.ulTpaRevision, tyRdiDiagInfo->pszTpaRevision);

	if(!bUSBDevice)
      (void)DL_OPXD_CloseDevice(tyDiagDevHandle);

	return DRVOPXD_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: PerformRDIDiagnosticsOnTarget
     Engineer: Suraj S
        Input: *tyRdiOnTargetDiagInfo - pointer to TyRDIOnTargetDiagInfo structure
       Output: TyError - Error code
  Description: Performs RDI Diagnostics on Target and give information to RDI GUI
Date           Initials    Description
08-Jan-2008      SJ         Initial
****************************************************************************/
TyError PerformRDIDiagnosticsOnTarget(char* pszSerialNumber,TyRDIOnTargetDiagInfo *tyRdiOnTargetDiagInfo)
{
 
	TyArmProcType TyProcType = NOCORE;
	TyError ErrRet = DIAG_ERROR_NO_ERROR;

	char szDiskwareFullPath[_MAX_PATH];
	char szFpgawareFullPath[_MAX_PATH];

	unsigned long ulTempIDCode1;
	unsigned long ulTempIDCode2;
	unsigned long ulTempIDCode3;
    unsigned long ulTempIDCode4;
	unsigned char bTpaLoopTest = 0;
	unsigned long ulTempadder = 0;
	unsigned long ulTempCoreID1 = 0;
	unsigned long ulTempCoreID2 = 0;
	unsigned long ulImplmntr = 0;
	unsigned long ulPpno = 0;
	unsigned long ulTapID = 0;
	unsigned long ulTapID1 = 0;
	unsigned int  uiNumberOfTAPs;
	unsigned char ucarm7=0;
	unsigned char ucarm9=0;
	unsigned char ucarm11=0;
	unsigned char ucCoresight = 0;	
	unsigned long ulCp1Add = DBG_CONTROL_REG;
	unsigned long ulCp1Data = SET_DBGR_BIT;
  	unsigned long ulTempCoreType = 0;   
    double dFreq = 1000.00;
	usDiagnostics = 1;

	boolean bARM7StopProblem           = FALSE;
	boolean bGlobalThumb         = FALSE;
	boolean pbExecuting           = FALSE;
	boolean bWatchPoint         = FALSE;
	tyRdiOnTargetDiagInfo->szTgtVoltage = 0.00000;
	tyRdiOnTargetDiagInfo->szRegCheck = 0;
	tyRdiOnTargetDiagInfo->szTpaLBResult = 0;
	tyRdiOnTargetDiagInfo->szCoreMake =0;
	tyRdiOnTargetDiagInfo->szArm7=0;
	tyRdiOnTargetDiagInfo->uiTargetTAPNo=0;
    ulGlobalIDcode0 = 0;
    ulGlobalIDcode1 = 0;
	
	unsigned long ulIDCodeTmpArray[50]={0};
	unsigned long ulIDCodeArray[30]={0};
	unsigned long ulIDCodeArray1[50]={0};
	unsigned long ulTAPIndex;
	unsigned long ulTAPIndex1;
	unsigned long ulNonJTAGDeviceFound;
	unsigned long ulEnterDebugStateArray[6];
	//volatile unsigned long ulDelay;

	
	//Save the path of the DLL
	ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);

	// get full path to Opella-XD diskware and FPGAware for ARM
	strcpy(szDiskwareFullPath, szGlobalDLLPathName);
	strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
	strcpy(szFpgawareFullPath, szGlobalDLLPathName);
	strcat(szFpgawareFullPath, szOpXDFpgawareFilename);

   if(bUSBDevice)
     (void)DL_OPXD_CloseDevice(tyCurrentDiagDevHandle);   

	//First open the OpellaXD
	ErrRet = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
   
   tyCurrentDiagDevHandle = tyDiagDevHandle;

	switch (ErrRet)
	{
		case DIAG_ERROR_NO_ERROR:
			break;
		case DIAG_ERROR_INVALID_SERIAL_NUMBER:
			return DIAG_ERROR_INVALID_SERIAL_NUMBER;
		default:
			return DIAG_ERROR_USB_DRIVER;
	}

	(void)DL_OPXD_UnconfigureDevice(tyDiagDevHandle);

	if (DL_OPXD_ConfigureDevice(tyDiagDevHandle, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
		return DIAG_ERROR_OPELLA_FAILURE;


	//Get device info
	if (DL_OPXD_GetDeviceInfo( tyDiagDevHandle, &tyDeviceInfo ) != DRVOPXD_ERROR_NO_ERROR)
	   {
		(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
		return DIAG_ERROR_USB_DRIVER;
	   }

   if (tyDeviceInfo.tyTpaConnected == TPA_NONE)
      {
      tyRdiOnTargetDiagInfo->tyTpaConnected = TPA_NONE;
		(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
      return DIAG_ERROR_TPA_NOT_CONNECTED;
      }

	//Test for TPA LOOP BACK 
	ErrRet = DL_OPXD_TpaLoopDiagnosticTest( tyDiagDevHandle, &bTpaLoopTest );
	if (ErrRet != DIAG_ERROR_NO_ERROR)
	   {
		(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
		return DIAG_ERROR_NO_ERROR;  //Just return from function. Handle the error in the GUI
	   }
	if (bTpaLoopTest) 
		tyRdiOnTargetDiagInfo->szTpaLBResult = 1;

	// Getting Target Volt
	dGlobalTgtvolt = tyDeviceInfo.dCurrentVtref; 
	tyRdiOnTargetDiagInfo->szTgtVoltage=tyDeviceInfo.dCurrentVtref;

	if (tyRdiOnTargetDiagInfo->szTgtVoltage == 0)
	   {
		(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
		return DIAG_ERROR_NO_ERROR; //Just return from function. Handle the error in the GUI
	   }

	tyTargetConfig.ulDebugCoreNumber = tyRdiOnTargetDiagInfo->ulDbgCoreNumber;
	
	ErrRet =  ASH_PrepareforDiagnostics( tyDiagDevHandle );
	if (ErrRet != DIAG_ERROR_NO_ERROR)
	  {
	  (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
	  return DIAG_ERROR_TAPID1_FAILED; 
	  }

	//for(ulDelay=0;ulDelay < 0x9FFFFFF;ulDelay++){};
	ML_Delay(500);

	if (tyRdiOnTargetDiagInfo->bCoresightCompliant)
	{
		(void)DL_OPXD_ResetDap( tyDiagDevHandle );
	}
	
	if (tyRdiOnTargetDiagInfo->bTICore)
	{
		ErrRet = DL_OPXD_InitIcepick( tyDiagDevHandle, 
									  tyRdiOnTargetDiagInfo->ulDbgCoreNumber, 
									  tyRdiOnTargetDiagInfo->ulTISubPortNum );
		if (ErrRet != DIAG_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
			return DIAG_ERROR_TAPID1_FAILED; 
		}
	}

	ML_Delay(500);

    if(!TestScanChain( tyDiagDevHandle, tyRdiOnTargetDiagInfo ))
      {
      (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
      return DIAG_ERROR_INVALID_CONFIG; 
      }

   if(tyRdiOnTargetDiagInfo->ptyJtagScanChain->ucNumJtagNodes != tyRdiOnTargetDiagInfo->ulTotCores)
      {
      (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
      return DIAG_ERROR_INVALID_CONFIG; 
      }

   // Reset the TAP Controller
   ErrRet = DL_OPXD_JtagResetTAP( tyDiagDevHandle, FALSE );
   if (ErrRet != DIAG_ERROR_NO_ERROR)
	  {
	  (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
	  return DIAG_ERROR_TAPID1_FAILED; 
	  }

   if (tyRdiOnTargetDiagInfo->bTICore)
   {
	   ErrRet = DL_OPXD_InitIcepick( tyDiagDevHandle, 
								     tyRdiOnTargetDiagInfo->ulDbgCoreNumber, 
								     tyRdiOnTargetDiagInfo->ulTISubPortNum );
	   if (ErrRet != DIAG_ERROR_NO_ERROR)
	   {
		   (void)DL_OPXD_CloseDevice( tyDiagDevHandle ) ;
		   return DIAG_ERROR_TAPID1_FAILED; 
	   }
	}

   ML_Delay(500);

   // Scan in a pattern and scan out the total ID Codes (one after another for all cores(32* no of cores))
   ErrRet = DL_OPXD_JtagScanDR(	tyDiagDevHandle, 
								tyTargetConfig.ulDebugCoreNumber,
								(tyRdiOnTargetDiagInfo->ulTotCores * 32),
								NULL,
								ulIDCodeTmpArray );
   if (ErrRet != DIAG_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
      return DIAG_ERROR_TAPID1_FAILED; 
      }

   //Adjust for JTAG Compliant device is also done

   ulNonJTAGDeviceFound = 0;

   for(ulTAPIndex=0; ulTAPIndex < tyRdiOnTargetDiagInfo->ulTotCores; ulTAPIndex++)
     {
       if(ulIDCodeTmpArray[ulTAPIndex] & (0x00000001 << ulNonJTAGDeviceFound))
       {
           if(ulNonJTAGDeviceFound == 0)
           {
               ulIDCodeArray[ulTAPIndex] = ulIDCodeTmpArray[ulTAPIndex];
           }
           else
           {
               ulIDCodeArray[ulTAPIndex + ulNonJTAGDeviceFound] = (((ulIDCodeTmpArray[ulTAPIndex] >> ulNonJTAGDeviceFound) & (0xFFFFFFFF >> ulNonJTAGDeviceFound)) |
                                                                   ((ulIDCodeTmpArray[ulTAPIndex+1] & (0xFFFFFFFF >> (32 - ulNonJTAGDeviceFound))) << (32 - ulNonJTAGDeviceFound)));
           }
       }
       else
       {
           ulNonJTAGDeviceFound++;
           ulIDCodeArray[ulTAPIndex + ulNonJTAGDeviceFound - 1] = 0x00000000;
           ulIDCodeArray[ulTAPIndex + ulNonJTAGDeviceFound] = (((ulIDCodeTmpArray[ulTAPIndex] >> ulNonJTAGDeviceFound) & (0xFFFFFFFF >> ulNonJTAGDeviceFound)) |
                                                               ((ulIDCodeTmpArray[ulTAPIndex+1] & (0xFFFFFFFF >> (32 - ulNonJTAGDeviceFound))) << (32 - ulNonJTAGDeviceFound)));
       }      
     }
     
   // Reverse the ID code array 	
   ulTAPIndex1=0;
   
   for(ulTAPIndex = tyRdiOnTargetDiagInfo->ulTotCores; ulTAPIndex !=0 ; ulTAPIndex--)
		{
		ulIDCodeArray1[ulTAPIndex1] = ulIDCodeArray[ulTAPIndex-1];
		ulTAPIndex1++;
		}

	//Initialize the debugger hardware
   ErrRet = ML_Initialise_Debugger_Third_party( tyDiagDevHandle, 
											    dFreq, 
											    tyRdiOnTargetDiagInfo->szTgtVoltage, 
											    tyRdiOnTargetDiagInfo );
   if (ErrRet != DIAG_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
	  if (ML_ERROR_INVALID_CORESIGHT_CONFIG == ErrRet)
		{
		return DIAG_ERROR_INVALID_CORESIGHT_CONFIG;
		} 
	  else
		{
		return DIAG_ERROR_INVALID_CONFIG;
		}
      
      }

	
    tyRdiOnTargetDiagInfo->ulIDCode = ulIDCodeArray1[tyTargetConfig.ulDebugCoreNumber];
    //tyRdiOnTargetDiagInfo->ulIDCode = *pulInDRBuffer;
    ulTempIDCode1 = tyRdiOnTargetDiagInfo->ulIDCode;
    ulTempIDCode2 = (ulTempIDCode1 & 0x00000fff);                       
    ulTempIDCode3 = (ulTempIDCode1 & 0xf0000000);
    ulTempIDCode4 = (ulTempIDCode1 & 0x00f00000);

	if (((tyRdiOnTargetDiagInfo->ulIDCode & 0x0FFFFFFF) == 0xBA00477) ||
		 ((tyRdiOnTargetDiagInfo->ulIDCode & 0x0FFFFFFF) == 0x0b6d602f))
	{
		TyProcType = DAP;
		ucCoresight = 1;	
	}
    else 
	{
		if(ulTempIDCode1 == 0x05B0203F)
		{
			TyProcType = ARM9;
			ucarm9 = 1;
		}
		else if(ulTempIDCode4 == 0x00b00000)
		{
			TyProcType = ARM11;
			ucarm11 = 1;
		}
		else if((ulTempIDCode2 == 0x00000f0f) && (ulTempIDCode3 != 0x10000000 )&& (ulTempIDCode3 != 0x40000000 ) )
		{ 
			TyProcType = ARM7;
			ucarm7 = 1;
		}
		else if((ulTempIDCode2 == 0x00000f0f) && (ulTempIDCode3 == 0x40000000 ))
		{
			TyProcType = ARM7S;
			ucarm7 = 1;
		}
		else if(ulTempIDCode4 == 0x00700000)
		{
			TyProcType = ARM7;
			ucarm7 = 1;
		}		
		else
		{
			TyProcType = ARM9;
			ucarm9 = 1;
		}
	}
   //Getting IR length 
   for (unsigned long k=0;k < (tyTargetConfig.ulSelectedNoOfCores);k++)
      ulTempadder += pulIRLengthArray[k];
   
   tyRdiOnTargetDiagInfo->ulIRLen = ulTempadder;

   if (0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM7DI")))
       tyTargetConfig.tyProcType = ARM7DI;
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM7TDMI")))
       tyTargetConfig.tyProcType = ARM7TDMI;
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM7TDMI-S")))
		tyTargetConfig.tyProcType = ARM7TDMIS;
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM720T")))
        tyTargetConfig.tyProcType = ARM720T;
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM740T")))
        tyTargetConfig.tyProcType = ARM740T;
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM9TDMI")))
        tyTargetConfig.tyProcType = ARM9TDMI;
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM920T")))
        {
        tyTargetConfig.tyProcType = ARM920T;
        ulTempCoreType = 0x920;        
        }
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM922T")))
        {
		  tyTargetConfig.tyProcType = ARM922T;
        ulTempCoreType = 0x922;        
        }
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM926EJ-S")))
        {
		  tyTargetConfig.tyProcType = ARM926EJS;
        ulTempCoreType = 0x926;        
        }
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM940T")))
       {
		 tyTargetConfig.tyProcType = ARM940T;
       ulTempCoreType = 0x940;       
       }
      
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM946E-S")))
      {
		tyTargetConfig.tyProcType = ARM946ES;
      ulTempCoreType = 0x946;      
      }
      
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM966E-S")))
      {
		tyTargetConfig.tyProcType = ARM966ES; 
      ulTempCoreType = 0x966;      
      }
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM968E-S")))
        {
		  tyTargetConfig.tyProcType = ARM968ES; 
        ulTempCoreType = 0x968;        
        } 
	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM1136JF-S")))
	{
		tyTargetConfig.tyProcType = ARM1136JFS; 
        ulTempCoreType = 0x1136;        
    }  

	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"ARM1176JZF-S")))
	{
		tyTargetConfig.tyProcType = ARM1176JZS; 
        ulTempCoreType = 0x1176;        
    }

	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-M3")))
	{
		TyProcType = CORTEXM;
		tyTargetConfig.tyProcType = CORTEX_M3;
	}

	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-A8")))
	{
		TyProcType = CORTEXA;
		tyTargetConfig.tyProcType = CORTEX_A8;
	}

	else if(0==(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-A9")))
	{
		TyProcType = CORTEXA;
		tyTargetConfig.tyProcType = CORTEX_A9;
	}
	switch(tyTargetConfig.tyProcType)
	{
		case ARM720T:
			ErrRet = CL_ReadARM720CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
               {
               (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
               return DIAG_ERROR_INVALID_CONFIG;
               }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;

         ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
         if(ulGlobalIDcode0 != 0x7200)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm7=1;
			break;

		case ARM740T:
			ErrRet = CL_ReadARM740CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_INVALID_CONFIG;
            }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
         ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
         if(ulGlobalIDcode0 != 0x7400)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm7=1;
			break;

		case ARM920T:
		case ARM922T:
			ErrRet = DL_OPXD_Arm_SelectScanChain( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  2, 
												  1, 
												  TyProcType );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_WriteICEBreaker( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  ulCp1Add, 
												  &ulCp1Data ); 
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_ReadICEBreaker( tyDiagDevHandle, 
												 tyTargetConfig.ulDebugCoreNumber, 
												 DBG_STATUS_REG, 
												 &ulCp1Data );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			   {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_Status_Proc( tyDiagDevHandle, 
											  tyTargetConfig.ulDebugCoreNumber, 
											  TyProcType, 
											  0, 
											  1, 
											  &bARM7StopProblem, 
											  &pbExecuting, 
											  &bGlobalThumb, 
											  &bWatchPoint );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			   {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet =  CL_ReadARM920_922CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_INVALID_CONFIG;
            }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
         ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
         if(ulTempCoreType == 0x920)
            {
            if(ulGlobalIDcode0 != 0x9200)
               {
               (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
               return DIAG_ERROR_CORE_MISS_MATCH;
               }
            }
         else if(ulTempCoreType == 0x922)
            {
            if(ulGlobalIDcode0 != 0x9220)
               {
               (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
               return DIAG_ERROR_CORE_MISS_MATCH;
               }
            }
			ucarm9=1;
			break;

		case ARM926EJS:
			ErrRet = DL_OPXD_Arm_SelectScanChain( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  2, 
												  1, 
												  TyProcType );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_WriteICEBreaker( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  ulCp1Add, 
												  &ulCp1Data ); 
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_ReadICEBreaker( tyDiagDevHandle, 
												 tyTargetConfig.ulDebugCoreNumber, 
												 DBG_STATUS_REG, 
												 &ulCp1Data );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_Status_Proc( tyDiagDevHandle, 
											  tyTargetConfig.ulDebugCoreNumber, 
											  TyProcType, 
											  0, 
											  1, 
											  &bARM7StopProblem, 
											  &pbExecuting, 
											  &bGlobalThumb, 
											  &bWatchPoint );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet =   CL_ReadARM926CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
            return DIAG_ERROR_INVALID_CONFIG;
            }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
         ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
         //if(ulGlobalIDcode0 != 0x9260)
         if(ulGlobalIDcode0 != 0x9260 && ulGlobalIDcode0 != 0x1310)//Feroceon has ID 0x1310
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm9=1;
			break;

		case ARM940T:
			ErrRet =   CL_ReadARM940_946CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			   {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_INVALID_CONFIG;
            }
			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
         ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
         if(ulGlobalIDcode0 != 0x9400)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm9=1;
			break;

		case ARM946ES:
			ErrRet =   CL_ReadARM940_946CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			   {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_INVALID_CONFIG;
            }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
         ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
         if(ulGlobalIDcode0 != 0x9460)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm9=1;
			break;

		case ARM966ES:
         	ErrRet = DL_OPXD_Arm_SelectScanChain( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  2, 
												  1, 
												  TyProcType ) ;
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			
			ErrRet = DL_OPXD_Arm_WriteICEBreaker( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  ulCp1Add, 
												  &ulCp1Data ); 
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_ReadICEBreaker( tyDiagDevHandle, 
												 tyTargetConfig.ulDebugCoreNumber, 
												 DBG_STATUS_REG, 
												 &ulCp1Data );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
			 
			ErrRet = DL_OPXD_Arm_Status_Proc( tyDiagDevHandle, 
											  tyTargetConfig.ulDebugCoreNumber, 
											  TyProcType, 
											  0, 
											  1, 
											  &bARM7StopProblem, 
											  &pbExecuting, 
											  &bGlobalThumb, 
											  &bWatchPoint );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
            {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_OPELLA_FAILURE;
            }
         
			ErrRet =   CL_ReadARM966CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			   {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
            return DIAG_ERROR_INVALID_CONFIG;
            }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
			ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
			if(ulGlobalIDcode0 != 0x9660)
			{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_CORE_MISS_MATCH;
			}
			ucarm9=1;
			break;

		case ARM968ES:
			ErrRet =   CL_ReadARM968CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			   {
            (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
            }
			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
			ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;
			if(ulGlobalIDcode0 != 0x9680)
            {
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm9=1;
			break;
		case ARM1136JFS:
		case ARM1176JZS:
			// ARM1136 needs to be in Debug State with Instruction Enable bit set to read co-processor registers
			ErrRet = DL_OPXD_Arm_Enter_Debug_State(	tyDiagDevHandle, 
													tyTargetConfig.ulDebugCoreNumber,
													TyProcType,
													ulEnterDebugStateArray,
													sizeof(ulEnterDebugStateArray) );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
            }
			
			ErrRet =   CL_ReadARM1136CoProcRegisters  ();
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
            }
			// Leave the debug state after reading co-processor registers
			ErrRet = DL_OPXD_Arm_Leave_Debug_State( tyDiagDevHandle, 
													tyTargetConfig.ulDebugCoreNumber,
													TyProcType,
													0,
													ulEnterDebugStateArray,
													sizeof(ulEnterDebugStateArray) );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
            }

			tyRdiOnTargetDiagInfo->ulCoreID = ulGlobalIDcode0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = ulGlobalIDcode1;
			ulGlobalIDcode0 = ulGlobalIDcode0 & 0x0000FFF0;

			if((ulGlobalIDcode0 != 0xB360) && (ulGlobalIDcode0 != 0xB760))
            {
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_CORE_MISS_MATCH;
            }
			ucarm11=1;
			break;

		case CORTEX_M0:
		case CORTEX_M1:
		case CORTEX_M3:
		case CORTEX_A5:
		case CORTEX_A8:
		case CORTEX_A9:
		case CORTEX_R4:
			{
			unsigned long ulMainIdRegVal;
			unsigned char Executionstatus = 0;
			unsigned long pbExecuting;

			if (tyRdiOnTargetDiagInfo->bDapPcSupport)
			{
				ulEnterDebugStateArray[0] = TRUE;
				ulEnterDebugStateArray[1] = TI_DAPPCCONTROL_REG_ADDR;
				ulEnterDebugStateArray[2] = TI_DAPPCCONTROL_DAPCONNECT;
			}
			else
			{
				ulEnterDebugStateArray[0] = FALSE;
				ulEnterDebugStateArray[1] = 0;
				ulEnterDebugStateArray[2] = 0;

			}

			ErrRet = DL_OPXD_Arm_Enter_Debug_State(	tyDiagDevHandle, 
													tyTargetConfig.ulDebugCoreNumber,
													TyProcType,
													ulEnterDebugStateArray,
													sizeof(ulEnterDebugStateArray) );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
				{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
				}

			ErrRet = DL_OPXD_Arm_Status_Proc( tyDiagDevHandle, 
											  tyTargetConfig.ulDebugCoreNumber, 
											  TyProcType, 
											  (ulGlobalScanChain != 0x2), 
											  Executionstatus, 
											  &bARM7StopProblem, 
											  (boolean *) &pbExecuting, 
											  &bGlobalThumb, 
                                              &bWatchPoint );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
			}


			ErrRet = DL_OPXD_ReadDebugReg( tyDiagDevHandle, 
										   tyTargetConfig.ulDebugCoreNumber, 
										   tyRdiOnTargetDiagInfo->ulCoresightBaseAddr + DEBUG_REG_OFFSET,
										   tyRdiOnTargetDiagInfo->ulMemApIndex,
										   &ulMainIdRegVal );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
				return DIAG_ERROR_INVALID_CONFIG;
			}

				
			tyRdiOnTargetDiagInfo->ulCoreID = ulMainIdRegVal;

			switch((ulMainIdRegVal & CPUID_PRIMARY_PART_NO) >> 4)
				{
				case 0xC20: /* Cortex-M0 */ /* Needs to check */
					if(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-M0"))
					{
						(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
						return DIAG_ERROR_CORE_MISS_MATCH;
					}
					break;

				case 0xC21: /* Cortex-M1 */
					if(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-M1"))
					{
						(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
						return DIAG_ERROR_CORE_MISS_MATCH;
					}
					break;

				case 0xC23: /* Cortex-M3 */
					if(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-M3"))
					{
						(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
						return DIAG_ERROR_CORE_MISS_MATCH;
					}
					break;

				case 0xC05: /* Cortex-A5 */ /* Needs to check */
					if(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-A5"))
					{
						(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
						return DIAG_ERROR_CORE_MISS_MATCH;
					}
					break;

				case 0xC08: /* Cortex-A8 */
					if(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-A8"))
					{
						(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
						return DIAG_ERROR_CORE_MISS_MATCH;
					}
					break;

				case 0xC09: /* Cortex-A9 */ /* Needs to check */
					if(strcmp(tyRdiOnTargetDiagInfo->pszCoreType,"Cortex-A9"))
					{
						(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
						return DIAG_ERROR_CORE_MISS_MATCH;
					}
					break;

				default:
					(void)DL_OPXD_CloseDevice( tyDiagDevHandle );
					return DIAG_ERROR_CORE_MISS_MATCH;
					//break;
				}
			}
			break;

		default:
			tyRdiOnTargetDiagInfo->ulCoreID = 0;
			tyRdiOnTargetDiagInfo->ulCpReg1 = 0;
			break;
	}

   if(tyRdiOnTargetDiagInfo->ulCoreID == 0x0 && (!((ucarm9 == 1) ^ (ucarm7 == 1) ^ (ucarm11 == 1) ^ (ucCoresight == 1))))
      {
      (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
	   return DIAG_ERROR_CORE_MISS_MATCH;
      }
    
	//Getting Architecture of core
	ulTempCoreID1=tyRdiOnTargetDiagInfo->ulCoreID;
	if (ucarm7==1) 
	{ 
		ulTempCoreID2 = ((ulTempCoreID1 & 0x00800000)>>23);
		if (ulTempCoreID2==1)
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"4T");
		else
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"3");
		
		ulTapID = tyRdiOnTargetDiagInfo->ulIDCode;
		ulTapID1 = (ulTapID & 0x00000fff);
		if ((ulTapID1==0x00000f0f) || (ulTapID1==0x00000061))
			tyRdiOnTargetDiagInfo->szArm7=1;
	}
	else if (tyProcessorConfig.bARM7==1)
	{
		ulTapID = tyRdiOnTargetDiagInfo->ulIDCode;
		ulTapID1 = (ulTapID & 0x00000fff);
		if ((ulTapID1==0x00000f0f) || (ulTapID1==0x00000061))
			tyRdiOnTargetDiagInfo->szArm7=1;
	}
	else if (ucarm9==1) 
	{ 
		ulTempCoreID2 = ((ulTempCoreID1 & 0x000f0000)>>16);
		ulImplmntr = ((ulTempCoreID1 & 0xff000000)>>24); 
		ulPpno = ((ulTempCoreID1 & 0x0000fff0)>>4);      
      
      if(ulPpno != ulTempCoreType)        
         {
         (void)DL_OPXD_CloseDevice( tyDiagDevHandle );
			return DIAG_ERROR_CORE_MISS_MATCH;
         }

		switch(ulTempCoreID2)
		{
			case 1:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"4");
				break;
			case 2:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"4T");
				break;
			case 3:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"5");
				break;
			case 4:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"5T");
				break;
			case 5:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"5TE");
				break;
			case 6:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"5TEJ");
				break;
			case 7:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"6");
				break;
		}

		if(ulImplmntr==0x00000041)
		   {
			tyRdiOnTargetDiagInfo->szCoreMake = 1;
			tyRdiOnTargetDiagInfo->ulPartno = ulPpno;
		   }
	}
	else if (ucarm11==1) 
	{
		strcpy(tyRdiOnTargetDiagInfo->pszArch,"6");
	}
	else if (ucCoresight == 1)
	{
		unsigned long ulArch = tyRdiOnTargetDiagInfo->ulCoreID & CPUID_ARCHITECTURE;

		switch (ulArch)
		{
		case ARCH_4:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"4");
			break;
		case ARCH_4T:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"4T");
			break;
		case ARCH_5:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"5");
			break;
		case ARCH_5T:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"5T");
			break;
		case ARCH_5TE:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"5TE");
			break;
		case ARCH_5TEJ:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"5TEJ");
			break;
		case ARCH_6:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"6");
			break;
		case ARCH_6M:
			strcpy(tyRdiOnTargetDiagInfo->pszArch,"6-M");
			break;
		case ARCH_CPUID_DEPE:						
			switch(tyRdiOnTargetDiagInfo->ulCoreID & CPUID_PARTNO_ARCH)
			{
			case ARCH_v7A:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"7-A");
				break;
			case ARCH_v7R:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"7-R");
				break;
			case ARCH_v7M:
				strcpy(tyRdiOnTargetDiagInfo->pszArch,"7-M");
				break;
			default:
				return DIAG_ERROR_CORE_MISS_MATCH;
			}			
			break;
		default:
			return DIAG_ERROR_CORE_MISS_MATCH;
		}
	}

	//Getting Cache Details
	ulTempCoreID1=tyRdiOnTargetDiagInfo->ulCpReg1;
	if ((ucarm9==1) || (ucarm11==1)) 
	{ 
		// for Data cache
		ulTempCoreID2 = ((ulTempCoreID1 & 0x003c0000)>>18);
		
      if(ulTempCoreID2 == 0)
         strcpy(tyRdiOnTargetDiagInfo->pszDCache,"0KB");
      else
         {
         unsigned long ulCacheSize = 1;
         ulCacheSize = ulCacheSize << (ulTempCoreID2 - 1);
         sprintf(tyRdiOnTargetDiagInfo->pszDCache,"%dKB",ulCacheSize);  //ulTempCoreID2 = (3 => 4KB, 4=>8KB, 5=>16KB,6=>32KB, 7=>64KB, 8=>128KB, 9=>256KB, 10=>512KB, 11=> 1MB
         }

		// for Instruction cache
		ulTempCoreID2 = ((ulTempCoreID1 & 0x000003c0)>>6);
		
      if(ulTempCoreID2 == 0)
         strcpy(tyRdiOnTargetDiagInfo->pszDCache,"0KB");
      else
         {
         unsigned long ulCacheSize = 1;
         ulCacheSize <<= (ulTempCoreID2 - 1);
         sprintf(tyRdiOnTargetDiagInfo->pszICache,"%dKB",ulCacheSize);  //ulTempCoreID2 = (3 => 4KB, 4=>8KB, 5=>16KB,6=>32KB, 7=>64KB, 8=>128KB, 9=>256KB, 10=>512KB, 11=> 1MB
         }
	}
    
	//Testing opella loop back
	if (TestTargetLoopBack(&uiNumberOfTAPs)) 
		tyRdiOnTargetDiagInfo->uiTargetTAPNo = uiNumberOfTAPs;
	else
	   {
		tyRdiOnTargetDiagInfo->uiTargetTAPNo = 0;
      (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DIAG_ERROR_TAP_LB_FAILED;
	   }

	if(TyProcType == ARM11)
	{				
		// ARM1136 needs to be in Debug State with Instruction Enable bit set to read  registers
		ErrRet = DL_OPXD_Arm_Enter_Debug_State( tyDiagDevHandle, 
												tyTargetConfig.ulDebugCoreNumber,
												TyProcType,
												ulEnterDebugStateArray,
												sizeof(ulEnterDebugStateArray) );
		if (ErrRet != DIAG_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
			return DIAG_ERROR_INVALID_CONFIG;
		}

		//Register Read/Write Test - The error is handled in the GUI
		ErrRet = DL_OPXD_Arm11_RegisterTest( tyDiagDevHandle, 
											 tyTargetConfig.ulDebugCoreNumber, 
											 TyProcType,
											 &(tyRdiOnTargetDiagInfo->szRegCheck) );
		if (ErrRet != DIAG_ERROR_NO_ERROR)
		  {
		  (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
			return DIAG_ERROR_INVALID_CONFIG;
		  }
		// Leave the debug state after reading  registers
		ErrRet = DL_OPXD_Arm_Leave_Debug_State( tyDiagDevHandle, 
												tyTargetConfig.ulDebugCoreNumber,
												TyProcType,
												0,
												ulEnterDebugStateArray,
												sizeof(ulEnterDebugStateArray) );
		if (ErrRet != DIAG_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
			return DIAG_ERROR_INVALID_CONFIG;
		}
	}
	else
	{
		if ( CORTEXM == TyProcType )
		{
			ErrRet = DL_OPXD_Cortex_RegisterTest( tyDiagDevHandle, 
												  tyTargetConfig.ulDebugCoreNumber, 
												  TyProcType,
												  &(tyRdiOnTargetDiagInfo->szRegCheck) );
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
				return DIAG_ERROR_INVALID_CONFIG;
			}
		}
		else if (CORTEXA == TyProcType)
		{
			ErrRet = DL_OPXD_CortexA_RegisterTest( tyDiagDevHandle, 
												   tyTargetConfig.ulDebugCoreNumber , 
												   TyProcType, 
												   &(tyRdiOnTargetDiagInfo->szRegCheck) );

		}
		else
		{
			//Register Read/Write Test - The error is handled in the GUI
			ErrRet = DL_OPXD_Arm_RegisterTest(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, TyProcType,&(tyRdiOnTargetDiagInfo->szRegCheck));
			if (ErrRet != DIAG_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
				return DIAG_ERROR_INVALID_CONFIG;
			} 
		}
		
	}	

	
	if(!bUSBDevice)
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);

	usDiagnostics = 0;

	return DRVOPXD_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: TestTargetLoopBack
     Engineer: Suraj S
       Input : unsigned int *puiNumberOfTAPs - pointer to value for number of TAPs on SC
       Output: unsigned char - TRUE if scan chain is OK
  Description: test scan chain, shifts patterns 0xaa, 0x55, 0xcc, 0x33
               checks number of TAPs detected in scan chain
Date           Initials    Description
08-Jan-2008       SJ        Initial
****************************************************************************/
unsigned char TestTargetLoopBack(unsigned int *puiNumberOfTAPs)
{
   unsigned int uiShiftIndex;
   unsigned long pulAAPattern[TSTSC_ULONGS];
   unsigned long pul55Pattern[TSTSC_ULONGS];
   unsigned long pulRandomPattern[TSTSC_ULONGS];
   unsigned long pulOutValue1[TSTSC_ULONGS];
   unsigned long pulOutValue2[TSTSC_ULONGS];
   unsigned long pulOutValue3[TSTSC_ULONGS];

   // restrict SC length for this test
   // define bypass pattern, 0xAA pattern and 0x55 pattern
   memset((void *)pul55Pattern, 0x55, TSTSC_ULONGS * sizeof(unsigned long));
   memset((void *)pulAAPattern, 0xAA, TSTSC_ULONGS * sizeof(unsigned long));
   for (uiShiftIndex=0; uiShiftIndex < TSTSC_ULONGS; uiShiftIndex++)
      pulRandomPattern[uiShiftIndex] = (unsigned long)rand();

   // shift all 1s to scanchain
   (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, NULL, NULL);
   // shift bypass to IR (DR == 1 bit) and shift AA pattern to DR

   (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, pulAAPattern, pulOutValue1);
   (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, pulRandomPattern, pulOutValue3);
   (void)DL_OPXD_JtagScanIRandDR(tyDiagDevHandle, 0, 256, NULL, TSTSC_LENGTH, pul55Pattern, pulOutValue2);

   // now shift results till get patterns, number of shifts == number of TAPs
   *puiNumberOfTAPs = 0;
   for(uiShiftIndex=0; uiShiftIndex < (64); uiShiftIndex++)
      {
      if(CompareUlongs_Tap(pulAAPattern, pulOutValue1, TSTSC_LENGTH - uiShiftIndex) &&
         CompareUlongs_Tap(pul55Pattern, pulOutValue2, TSTSC_LENGTH - uiShiftIndex) &&
         CompareUlongs_Tap(pulRandomPattern, pulOutValue3, TSTSC_LENGTH - uiShiftIndex)
         )
         {
         // both patterns shifted by the same number of bits revealed on output
         *puiNumberOfTAPs = uiShiftIndex;
         return TRUE;
         }
      // shift all arrays
      ShiftUlongArray_Tap(pulOutValue1, TSTSC_ULONGS, 1, true);
      ShiftUlongArray_Tap(pulOutValue2, TSTSC_ULONGS, 1, true);
      ShiftUlongArray_Tap(pulOutValue3, TSTSC_ULONGS, 1, true);
      }
   // we could not detect scan chain correctly
   return FALSE;
}

/****************************************************************************
     Function: CompareUlongs_Tap
     Engineer: Suraj S
        Input: unsigned long *pulUlongs1   - first array of ulongs to compare
               unsigned long *pulUlongs2   - second array of ulongs to compare
               unsigned int uiNumberOfBits - number of bits from ulongs to compare
       Output: unsigned char - TRUE if ulongs are identical in specified bits
  Description: compares to ulong arrays with specified number of bits
Date           Initials    Description
08-Jan-2008       SJ       Initial
****************************************************************************/
unsigned char CompareUlongs_Tap(unsigned long *pulUlongs1, unsigned long *pulUlongs2, unsigned int uiNumberOfBits)
{
   unsigned int uiIndex;
   unsigned long ulMask;

   for(uiIndex=0; uiIndex < (uiNumberOfBits / 32); uiIndex++)
      {     // compare whole ulongs
      if(*pulUlongs1++ != *pulUlongs2++)
         return FALSE;
      }

   uiNumberOfBits = uiNumberOfBits % 32;
   // compare rest of ulong
   if(uiNumberOfBits == 0)
      return TRUE;
   ulMask = 0xffffffff >> (32 - uiNumberOfBits);
   if((*pulUlongs1 & ulMask) != (*pulUlongs2 & ulMask))
      return FALSE;

   return TRUE;
}


/****************************************************************************
     Function: ShiftUlongArray_Tap
     Engineer: Suraj S
        Input: unsigned long *pulUlongArray  : array of ulongs
               unsigned int uiNumberOfUlongs : number of ulongs in array
               unsigned int uiNumberOfBits   : number of bits to be shifted
               unsigned char bShiftToRight   : TRUE when shifting right (FALSE for left)
       Output: none
  Description: shift array of ulongs to right/left (maximum number of  bits to shift is 31)
Date           Initials    Description
18-Jan-2008      SJ        Initial
****************************************************************************/
void ShiftUlongArray_Tap(unsigned long *pulUlongArray, unsigned int uiNumberOfUlongs,
                         unsigned int uiNumberOfBits, unsigned char bShiftToRight)
{
   unsigned long ulTempValue, ulTempShift;
   unsigned int uiActUlong;

   // check range of parameters, do nothing if incorrect
   if((uiNumberOfUlongs < 1) || (uiNumberOfBits < 1) || (uiNumberOfBits > 31))
      return;

   ulTempShift = 0;
   if(bShiftToRight)
      {
      // shifting right
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
         {
         ulTempValue = pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] & (0xffffffff >> (32 - uiNumberOfBits));
         pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] = (pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] >> uiNumberOfBits) 
                                                              | ulTempShift;
         ulTempShift = ulTempValue << (32 - uiNumberOfBits);
         }
      }
   else
      {
      // shifting left
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
         {
         ulTempValue = pulUlongArray[uiActUlong] & (0xffffffff << (32 - uiNumberOfBits));
         pulUlongArray[uiActUlong] = (pulUlongArray[uiActUlong] << uiNumberOfBits) 
                                                              | ulTempShift;
         ulTempShift = ulTempValue >> (32 - uiNumberOfBits);
         }
      }
}

/****************************************************************************
     Function: PerformTargetHardReset
     Engineer: Suraj S
        Input: TyRDIOnTargetDiagInfo *tyRDIOnTargetDiagInfo
       Output: TyError - Error code
  Description: Performs Hard Reset On Target
Date           Initials    Description
18-Jan-2008       SJ       Initial
****************************************************************************/
TyError PerformTargetHardReset(char* pszSerialNumber,TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo)
{
   TyError ErrRet = ML_ERROR_NO_ERROR;
   ptyRDIOnTargetDiagInfo->tyRDIHardResetDiagInfo.ucTgtAssertResetStatus = 0;
   ptyRDIOnTargetDiagInfo->tyRDIHardResetDiagInfo.ucTgtDeassertResetStatus = 1;

   unsigned char ucTempTgtSts1 = NULL;
   unsigned char ucTempTgtSts2 = NULL;
   double dFreq = 1000.00; 

   /*************************************/ /* Added by Jeenus */
   char szDiskwareFullPath[_MAX_PATH];
	char szFpgawareFullPath[_MAX_PATH];
	/**************************************/

	/*ErrRet = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
	switch (ErrRet)
	{
		case DRVOPXD_ERROR_NO_ERROR:
			break;
		case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
			return DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
		default:
			return DRVOPXD_ERROR_USB_DRIVER;
	}*/

	/*************************Added by Jeenus*************************************/
   //Save the path of the DLL
   ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);
   
   // get full path to Opella-XD diskware and FPGAware for ARM
   strcpy(szDiskwareFullPath, szGlobalDLLPathName);
   strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
   strcpy(szFpgawareFullPath, szGlobalDLLPathName);
   strcat(szFpgawareFullPath, szOpXDFpgawareFilename);
   
   if(bUSBDevice)
	   (void)DL_OPXD_CloseDevice(tyCurrentDiagDevHandle);   
   
   //First open the OpellaXD
   ErrRet = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
   
   tyCurrentDiagDevHandle = tyDiagDevHandle;
   
   switch (ErrRet)
   {
   case DIAG_ERROR_NO_ERROR:
	   break;
   case DIAG_ERROR_INVALID_SERIAL_NUMBER:
	   return DIAG_ERROR_INVALID_SERIAL_NUMBER;
   default:
	   return DIAG_ERROR_USB_DRIVER;
   }
   
   (void)DL_OPXD_UnconfigureDevice(tyDiagDevHandle);
   
   if (DL_OPXD_ConfigureDevice(tyDiagDevHandle, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
	   return DIAG_ERROR_OPELLA_FAILURE;
   
   //Get device info
   if (DL_OPXD_GetDeviceInfo(tyDiagDevHandle, &tyDeviceInfo) != DRVOPXD_ERROR_NO_ERROR)
   {
	   (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
	   return DIAG_ERROR_USB_DRIVER;
   }
   
   if (tyDeviceInfo.tyTpaConnected == TPA_NONE)
   {	   
	   return DIAG_ERROR_TPA_NOT_CONNECTED;
   }
   
   //Test for TPA LOOP BACK 
   /*ErrRet = DL_OPXD_TpaLoopDiagnosticTest(tyDiagDevHandle, &bTpaLoopTest);
   if (ErrRet != DIAG_ERROR_NO_ERROR)
   {
	   (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
	   return DIAG_ERROR_NO_ERROR;  //Just return from function. Handle the error in the GUI
   }  */

   /*******************************************************************************************/

   ErrRet = ML_Initialise_Debugger_Third_party(tyDiagDevHandle,dFreq,dGlobalTgtvolt,ptyRDIOnTargetDiagInfo);   
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
      }
   ErrRet = DL_OPXD_Arm_ResetProc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 1, &ucTempTgtSts1,NULL );
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      {
      (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
      return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
      }
   else
      ptyRDIOnTargetDiagInfo->tyRDIHardResetDiagInfo.ucTgtAssertResetStatus = ucTempTgtSts1;

   ML_Delay(100);

   ErrRet = DL_OPXD_Arm_ResetProc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 0, NULL, NULL);
   ML_Delay(400);
   ErrRet = DL_OPXD_Arm_ResetProc(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 0, &ucTempTgtSts2, NULL);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
   {
      (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
      return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   }
   else
      ptyRDIOnTargetDiagInfo->tyRDIHardResetDiagInfo.ucTgtDeassertResetStatus = ucTempTgtSts2;

   //Progress bar
   pOpenUSBProgress = new CProgressControl;
   (void)pOpenUSBProgress->ShowWindow(SW_SHOW);
   ((CProgressCtrl *)pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_CONTROL))->SetRange(0, 20);
   pOpenUSBProgress->GetDlgItem(IDC_OPAVRII_PROGRESS_STATIC)->SetWindowText("Asserting Reset....");

   for (unsigned char j=0; j < 50; j++)
   {
      OpenUsbDeviceMoveProgress(j);
      ML_Delay(5);
   }

   (void)pOpenUSBProgress->ShowWindow(SW_HIDE);
   delete pOpenUSBProgress;
   (void)DL_OPXD_CloseDevice(tyDiagDevHandle);

//    ErrRet = ML_InfoProc (RDISignal_Stop, 0, 0);
//    if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
//       return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   
   return ErrRet;
}

/****************************************************************************
     Function: ConvertVersionString
     Engineer: Vitezslav Hola
        Input: unsigned long ulVersion - version
               char *pszVersionString - buffer for version string
       Output: none
  Description: convert version format to string
Date           Initials    Description
28-Sep-2007     VH          Initial
****************************************************************************/
void ConvertVersionString(unsigned long ulVersion, char *pszVersionString)
{
   unsigned int uiVal1, uiVal2, uiVal3;

   //assert(pszVersionString != NULL);
   // decode firmware version
   uiVal1 = (unsigned int)((ulVersion & 0xff000000) >> 24);
   uiVal2 = (unsigned int)((ulVersion & 0x00ff0000) >> 16);
   uiVal3 = (unsigned int)((ulVersion & 0x0000ff00) >> 8);
   if (ulVersion & 0x000000ff)
      sprintf(pszVersionString, "v%x.%x.%x-%c", uiVal1, uiVal2, uiVal3, (char)(ulVersion & 0x000000ff));
   else
      sprintf(pszVersionString, "v%x.%x.%x", uiVal1, uiVal2, uiVal3);
}

/****************************************************************************
     Function: ConvertDateString
     Engineer: Vitezslav Hola
        Input: unsigned long ulDate - date
               char *pszDateString - buffer for date string
       Output: none
  Description: convert date format to string
Date           Initials    Description
28-Sep-2007     VH          Initial
****************************************************************************/
void ConvertDateString(unsigned long ulDate, char *pszDateString)
{
   unsigned int uiVal1, uiVal2, uiVal3;

   //assert(pszDateString != NULL);
   uiVal1 = (unsigned int)((ulDate & 0xff000000) >> 24);
   uiVal2 = (unsigned int)((ulDate & 0x00ff0000) >> 16);
   uiVal3 = (unsigned int)(ulDate & 0x0000ffff);
   switch (uiVal2)
      {
      case 1:
         sprintf(pszDateString, "%02x-Jan-%x", uiVal1, uiVal3);
         break;
      case 2:
         sprintf(pszDateString, "%02x-Feb-%x", uiVal1, uiVal3);
         break;
      case 3:
         sprintf(pszDateString, "%02x-Mar-%x", uiVal1, uiVal3);
         break;
      case 4:
         sprintf(pszDateString, "%02x-Apr-%x", uiVal1, uiVal3);
         break;
      case 5:
         sprintf(pszDateString, "%02x-May-%x", uiVal1, uiVal3);
         break;
      case 6:
         sprintf(pszDateString, "%02x-Jun-%x", uiVal1, uiVal3);
         break;
      case 7:
         sprintf(pszDateString, "%02x-Jul-%x", uiVal1, uiVal3);
         break;
      case 8:
         sprintf(pszDateString, "%02x-Aug-%x", uiVal1, uiVal3);
         break;
      case 9:
         sprintf(pszDateString, "%02x-Sep-%x", uiVal1, uiVal3);
         break;
      case 10:
         sprintf(pszDateString, "%02x-Oct-%x", uiVal1, uiVal3);
         break;
      case 11:
         sprintf(pszDateString, "%02x-Nov-%x", uiVal1, uiVal3);
         break;
      default:
         sprintf(pszDateString, "%02x-Dec-%x", uiVal1, uiVal3);
         break;
      }
}

/****************************************************************************
     Function: ConvertPcbVersionString
     Engineer: Vitezslav Hola
        Input: unsigned long ulVersion - version
               char *pszVersionString - buffer for version string
       Output: none
  Description: convert PCB version to string
Date           Initials    Description
20-Oct-2007    VH          Initial
****************************************************************************/
void ConvertPcbVersionString(unsigned long ulVersion, char *pszVersionString)
{
   unsigned int uiRVal, uiEVal, uiMVal;

   // decode firmware version
   if (ulVersion & 0x000000FF)
      {  // lowest byte is not 0, set all to 0s
      uiRVal = 0;
      uiEVal = 0;
      uiMVal = 0;
      }
   else
      {  // lowest byte is 0 as expected
      uiRVal = (unsigned int)((ulVersion & 0xFF000000) >> 24);
      uiEVal = (unsigned int)((ulVersion & 0x00FF0000) >> 16);
      uiMVal = (unsigned int)((ulVersion & 0x0000FF00) >> 8);
      }
   sprintf(pszVersionString, "R%d-M%d-E%d", uiRVal, uiMVal, uiEVal);
}

/****************************************************************************
Function: GetBypassInstruction
Engineer: Vitezslav Hola
Input: unsigned long *pulBypassInstruction  : pointer to array of ulongs
at least SCANCHAIN_ULONG_SIZE)
unsigned long *pulLength            : pointer to unsigned long for length of IR
Output: none
Description: determine bypass instruction for scanchain and its length
Date           Initials    Description
31-Mar-2008    VH          Initial
****************************************************************************/
void GetBypassInstruction(unsigned long *pulBypassInstruction, 
                          unsigned long *pulLength)
{
   
   // use different bypass instructions for multicore device
   memset((void *)pulBypassInstruction, 0xff, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   
   *pulLength = MAX_SCANCHAIN_LENGTH;
}

/****************************************************************************
Function: CompareUlongs
Engineer: Vitezslav Hola
Input: unsigned long *pulUlongs1   : first array of ulongs to compare
unsigned long *pulUlongs2   : second array of ulongs to compare
unsigned int uiNumberOfBits : number of bits from ulongs to compare
Output: TRUE : if ulongs are identical in specified bits
Description: compares to unsigned long arrays with specified number of bits
Date           Initials    Description
31-Mar-2008    VH          Initial
****************************************************************************/
int CompareUlongs(unsigned long *pulUlongs1, unsigned long *pulUlongs2, unsigned int uiNumberOfBits)
{
   unsigned int uiIndex;
   unsigned long ulMask;
   
   for(uiIndex=0; uiIndex < (uiNumberOfBits / 32); uiIndex++)
   {     // compare whole ulongs
      if(*pulUlongs1++ != *pulUlongs2++)
         return FALSE;
   }
   
   uiNumberOfBits = uiNumberOfBits % 32;
   // compare rest of unsigned long
   if(uiNumberOfBits == 0)
      return TRUE;
   ulMask = 0xffffffff >> (32 - uiNumberOfBits);
   if((*pulUlongs1 & ulMask) != (*pulUlongs2 & ulMask))
      return FALSE;
   
   return TRUE;
}


/****************************************************************************
Function: ShiftUlongArray
Engineer: Vitezslav Hola
Input: unsigned long *pulUlongArray  : array of ulongs
unsigned int uiNumberOfUlongs : number of ulongs in array
unsigned int uiNumberOfBits   : number of bits to be shifted
int bShiftToRight             : TRUE when shifting right (FALSE for left)
Output: none
Description: shift array of ulongs to right/left (maximum number of  bits to shift is 31)
Date           Initials    Description
31-Mar-2008    VH          Initial
****************************************************************************/
void ShiftUlongArray(unsigned long *pulUlongArray, unsigned int uiNumberOfUlongs,
                     unsigned int uiNumberOfBits, int bShiftToRight)
{
   unsigned long ulTempValue, ulTempShift;
   unsigned int uiActUlong;
     
   // check range of parameters, do nothing if incorrect
   if((uiNumberOfUlongs < 1) || (uiNumberOfBits < 1) || (uiNumberOfBits > 31))
      return;
   
   ulTempShift = 0;
   if(bShiftToRight)
   {
      // shifting right
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
      {
         ulTempValue = pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] & (0xffffffff >> (32 - uiNumberOfBits));
         pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] = (pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] >> uiNumberOfBits) 
            | ulTempShift;
         ulTempShift = ulTempValue << (32 - uiNumberOfBits);
      }
   }
   else
   {
      // shifting left
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
      {
         ulTempValue = pulUlongArray[uiActUlong] & (0xffffffff << (32 - uiNumberOfBits));
         pulUlongArray[uiActUlong] = (pulUlongArray[uiActUlong] << uiNumberOfBits) 
            | ulTempShift;
         ulTempShift = ulTempValue >> (32 - uiNumberOfBits);
      }
   }
}

/****************************************************************************
Function: TestScanChain
Engineer: Vitezslav Hola
Input: unsigned int *puiNumberOfTAPs   : pointer to value for number of TAPs on SC
Output: TRUE: if scan chain is OK
Description: test scanchain, shifts patterns 0xaa, 0x55, 0xcc, 0x33
checks number of TAPs detected in scanchain
Date           Initials    Description
31-Mar-2008    VH          Initial
****************************************************************************/
int TestScanChain(TyDevHandle tyDevHandle, TyRDIOnTargetDiagInfo *tyRdiOnTargetDiagInfo)
{
   unsigned int uiShiftIndex;
   unsigned long pulBypassValue[SCANCHAIN_ULONG_SIZE];
   unsigned long pulAAPattern[SCANCHAIN_ULONG_SIZE];
   unsigned long pul55Pattern[SCANCHAIN_ULONG_SIZE];
   unsigned long pulCCPattern[SCANCHAIN_ULONG_SIZE];
   unsigned long pul33Pattern[SCANCHAIN_ULONG_SIZE];
   unsigned long pul00Pattern[SCANCHAIN_ULONG_SIZE];
   unsigned long pulOutValue1[SCANCHAIN_ULONG_SIZE];
   unsigned long pulOutValue2[SCANCHAIN_ULONG_SIZE];
   unsigned long pulOutValue3[SCANCHAIN_ULONG_SIZE];
   unsigned long pulOutValue4[SCANCHAIN_ULONG_SIZE];
   unsigned long ulTotalIRLength;
   
   // define bypass pattern, 0xAA pattern and 0x55 pattern
   memset((void *)pulBypassValue, 0xff, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   memset((void *)pulAAPattern, 0xaa, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   memset((void *)pul55Pattern, 0x55, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   memset((void *)pulCCPattern, 0xcc, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   memset((void *)pul33Pattern, 0x33, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   memset((void *)pul00Pattern, 0x00, SCANCHAIN_ULONG_SIZE * sizeof(unsigned long));
   
   // get bypass instruction code and scan chain length
   GetBypassInstruction(pulBypassValue, &ulTotalIRLength);
   
   // first shift bypass to IR and shift 0's to DR, ignore this step
   (void)DL_OPXD_JtagScanIR(tyDevHandle, 0, ulTotalIRLength, pulBypassValue, pulOutValue1);
   (void)DL_OPXD_JtagScanDR(tyDevHandle, 0, MAX_SCANCHAIN_LENGTH, pul00Pattern, pulOutValue1);

   // shift bypass to IR (DR == 1 bit) and shift AA pattern to DR
   (void)DL_OPXD_JtagScanIR(tyDevHandle, 0, ulTotalIRLength, pulBypassValue, pulOutValue1);
   (void)DL_OPXD_JtagScanDR(tyDevHandle, 0, MAX_SCANCHAIN_LENGTH, pulAAPattern, pulOutValue1);
   
   // shift bypass to IR (DR == 1 bit) and shift 55 pattern to DR
   (void)DL_OPXD_JtagScanIR(tyDevHandle, 0, ulTotalIRLength, pulBypassValue, pulOutValue2);
   (void)DL_OPXD_JtagScanDR(tyDevHandle, 0, MAX_SCANCHAIN_LENGTH, pul55Pattern, pulOutValue2);

   // shift bypass to IR (DR == 1 bit) and shift CC pattern to DR
   (void)DL_OPXD_JtagScanIR(tyDevHandle, 0, ulTotalIRLength, pulBypassValue, pulOutValue3);
   (void)DL_OPXD_JtagScanDR(tyDevHandle, 0, MAX_SCANCHAIN_LENGTH, pulCCPattern, pulOutValue3);
   
   // shift bypass to IR (DR == 1 bit) and shift 33 pattern to DR
   (void)DL_OPXD_JtagScanIR(tyDevHandle, 0, ulTotalIRLength, pulBypassValue, pulOutValue4);
   (void)DL_OPXD_JtagScanDR(tyDevHandle, 0, MAX_SCANCHAIN_LENGTH, pul33Pattern, pulOutValue4);
   
   // now shift results till get patterns, number of shifts == number of TAPs
   tyRdiOnTargetDiagInfo->ulTotCores = 0;
   for(uiShiftIndex=0; uiShiftIndex < MAX_TAPS_ON_SCANCHAIN; uiShiftIndex++)
   {
      if(CompareUlongs(pulAAPattern, pulOutValue1, MAX_SCANCHAIN_LENGTH - uiShiftIndex) &&
         CompareUlongs(pul55Pattern, pulOutValue2, MAX_SCANCHAIN_LENGTH - uiShiftIndex) && 
         CompareUlongs(pulCCPattern, pulOutValue3, MAX_SCANCHAIN_LENGTH - uiShiftIndex) && 
         CompareUlongs(pul33Pattern, pulOutValue4, MAX_SCANCHAIN_LENGTH - uiShiftIndex))
      {
         // both patterns shifted by the same number of bits revealed on output
         tyRdiOnTargetDiagInfo->ulTotCores = uiShiftIndex;
         break;
      }
      // shift all arrays
      ShiftUlongArray(pulOutValue1, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      ShiftUlongArray(pulOutValue2, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      ShiftUlongArray(pulOutValue3, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      ShiftUlongArray(pulOutValue4, SCANCHAIN_ULONG_SIZE, 1, TRUE);
   }

   if ( MAX_TAPS_ON_SCANCHAIN == uiShiftIndex )
   {
		return FALSE;
   }

   (void)DL_OPXD_JtagScanIR(tyDevHandle, 0, ulTotalIRLength, pulAAPattern, pulOutValue1);
   for(uiShiftIndex=0; uiShiftIndex < ulTotalIRLength; uiShiftIndex++)
   {
	   if(CompareUlongs(pulAAPattern, pulOutValue1, MAX_SCANCHAIN_LENGTH - uiShiftIndex) )
	   {
		   // both patterns shifted by the same number of bits revealed on output
		   tyRdiOnTargetDiagInfo->ulIRLen = uiShiftIndex;
		   return TRUE;
	   }
	   // shift all arrays
	   ShiftUlongArray(pulOutValue1, SCANCHAIN_ULONG_SIZE, 1, TRUE);	   
   }

   // we could not detect scan chain correctly
   return FALSE;
}

/****************************************************************************
     Function: ASH_PrepareforDiagnostics
     Engineer: Suraj S
        Input: tyHandle -Devise Handle

       Output: Error
  Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
17-Oct-2009    SJ          Initial
****************************************************************************/
int ASH_PrepareforDiagnostics(TyDevHandle tyHandle)
{
	int iRet;
	iRet = DRVOPXD_ERROR_NO_ERROR;


    if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_ConfigureDevice(tyHandle, DW_ARM, FPGA_ARM, szOpXDDiskwareFilename, szOpXDFpgawareFilename) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;

	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_JtagSetFrequency(tyHandle, 1.0, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
 
	// set TMS sequence (just use default sequences)
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_JtagSetTMS(tyHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;

	// set no MC
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_JtagConfigMulticore(tyHandle, 0, NULL) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;

	// set tracking voltage and enable drivers to target
	if ((iRet == DRVOPXD_ERROR_NO_ERROR) && (DL_OPXD_SetVtpaVoltage(tyHandle, 0.0, TRUE, TRUE) != DRVOPXD_ERROR_NO_ERROR))
		iRet = DRVOPXD_ERROR_USB_DRIVER;
	
	return iRet;
}


/****************************************************************************
     Function: PerformReadROMTable
     Engineer: Suraj S
        Input: char* pszSerialNumber - Serial number of the opella-XD.
			   unsigned long ulCore  - Core number
               TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo - pointer to 
										TyRDIOnTargetDiagInfo structure
			   TyCoresightComp* ptyCoresightComp - Pointer to the TyCoresightComp
											structure.
       Output: TyError - Error code
  Description: Performs RDI Diagnostics on Target and give information to RDI GUI
Date           Initials    Description
08-Jan-2008      SJ         Initial
****************************************************************************/
TyError PerformReadROMTable(char* pszSerialNumber, 
							unsigned long ulCore,
							TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo,
							TyCoresightComp* ptyCoresightComp)
{
    TyError tyError = ML_ERROR_NO_ERROR;
    char szDiskwareFullPath[_MAX_PATH];
    char szFpgawareFullPath[_MAX_PATH];
    unsigned char bTpaLoopTest = 0;
	double dFreq = 1000.00;

	ptyRDIOnTargetDiagInfo->szTgtVoltage = 0.00000;

	//Save the path of the DLL
	ML_StoreFullPathForDLL ((char *)&pszGlobalDllPathName);
	
	// get full path to Opella-XD diskware and FPGAware for ARM
	strcpy(szDiskwareFullPath, szGlobalDLLPathName);
	strcat(szDiskwareFullPath, szOpXDDiskwareFilename);
	strcpy(szFpgawareFullPath, szGlobalDLLPathName);
	strcat(szFpgawareFullPath, szOpXDFpgawareFilename);
	
    if(bUSBDevice)
        (void)DL_OPXD_CloseDevice(tyCurrentDiagDevHandle);
    
    tyError = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDiagDevHandle);
    
    tyCurrentDiagDevHandle = tyDiagDevHandle;
    
    switch (tyError)
	   {
	   case DRVOPXD_ERROR_NO_ERROR:
           break;
       case DRVOPXD_ERROR_INVALID_SERIAL_NUMBER:
           return DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
       default:
           return DRVOPXD_ERROR_USB_DRIVER;
	   }
	
    (void)DL_OPXD_UnconfigureDevice(tyDiagDevHandle);
    
    if (DL_OPXD_ConfigureDevice(tyDiagDevHandle, DW_ARM, FPGA_ARM, szDiskwareFullPath, szFpgawareFullPath) != DRVOPXD_ERROR_NO_ERROR)
    {
        (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
        return DIAG_ERROR_OPELLA_FAILURE;
        
    }

    //Get device info
    if (DL_OPXD_GetDeviceInfo(tyDiagDevHandle, &tyDeviceInfo) != DRVOPXD_ERROR_NO_ERROR)
    {
        (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
        return DRVOPXD_ERROR_USB_DRIVER;
	}
	
    if (tyDeviceInfo.tyTpaConnected == TPA_NONE)
    {
        (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
        return DIAG_ERROR_TPA_NOT_CONNECTED;
    }
    
    //Test for TPA LOOP BACK 
    tyError = DL_OPXD_TpaLoopDiagnosticTest(tyDiagDevHandle, &bTpaLoopTest);
    if (tyError != DIAG_ERROR_NO_ERROR)
	   {
        (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
        return DIAG_ERROR_NO_ERROR;  //Just return from function. Handle the error in the GUI
	   }

	if (bTpaLoopTest) 
		ptyRDIOnTargetDiagInfo->szTpaLBResult = 1;
	
	// Getting Target Volt
	dGlobalTgtvolt = tyDeviceInfo.dCurrentVtref; 
	ptyRDIOnTargetDiagInfo->szTgtVoltage=tyDeviceInfo.dCurrentVtref;
	
	if (ptyRDIOnTargetDiagInfo->szTgtVoltage == 0)
	   {
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DIAG_ERROR_NO_ERROR; //Just return from function. Handle the error in the GUI
	   }
	
    tyError =  ASH_PrepareforDiagnostics(tyDiagDevHandle);
    if (tyError != DIAG_ERROR_NO_ERROR)
    {
        (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
        return DIAG_ERROR_TAPID1_FAILED; 
	}
	
	//for(unsigned long ulDelay=0;ulDelay < 0x9FFFFFF;ulDelay++){};
	ML_Delay(500);

	(void)DL_OPXD_ResetDap(tyDiagDevHandle);	
	
	if (ptyRDIOnTargetDiagInfo->bTICore)
	{
		tyError = DL_OPXD_InitIcepick( tyDiagDevHandle, 
							 ptyRDIOnTargetDiagInfo->ulDbgCoreNumber, 
							 ptyRDIOnTargetDiagInfo->ulTISubPortNum );
		if (tyError != DIAG_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
			return DIAG_ERROR_TAPID1_FAILED; 
		}
	}

	ML_Delay(500);

    if(!TestScanChain(tyDiagDevHandle, ptyRDIOnTargetDiagInfo))
	{
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DIAG_ERROR_INVALID_CONFIG; 
      }

	// Reset the TAP Controller
	tyError = DL_OPXD_JtagResetTAP(tyDiagDevHandle, FALSE);
	if (tyError != DIAG_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DIAG_ERROR_TAPID1_FAILED; 
	  }

	//Initialize the debugger hardware
	tyError = ML_Initialise_Debugger_Third_party(tyDiagDevHandle, dFreq, ptyRDIOnTargetDiagInfo->szTgtVoltage, ptyRDIOnTargetDiagInfo);
	if (tyError != DIAG_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		return DIAG_ERROR_INVALID_CONFIG;
      }

    tyError = DL_OPXD_ReadROMTable(tyDiagDevHandle, ulCore, ptyRDIOnTargetDiagInfo->ulCoresightBaseAddr,
		ptyRDIOnTargetDiagInfo->ulDbgApIndex,  (unsigned long*)ptyCoresightComp);
    if (tyError != DIAG_ERROR_NO_ERROR)
    {
        (void)DL_OPXD_CloseDevice(tyDiagDevHandle);
		tyError = DL_OPXD_ConvertDrvlayerErrToDiagErr(tyError);
        return tyError;
    }


	if(!bUSBDevice)
		(void)DL_OPXD_CloseDevice(tyDiagDevHandle);
	
	usDiagnostics = 0;
	
	
	
    return tyError;
    
}

TyError DL_OPXD_ConvertDrvlayerErrToDiagErr(TyError tyError)
{
	TyError ErrRet = DIAG_ERROR_NO_ERROR;
	switch (tyError)
	{
	case DRVOPXD_ERROR_ARM_NO_DEBUG_ENTRY_AP:
		ErrRet = DIAG_ERROR_ARM_NO_DEBUG_ENTRY_AP;
		break;
	case DRVOPXD_ERROR_ARM_NOT_VALID_ROM_ADDR:
		ErrRet = DIAG_ERROR_ARM_NOT_VALID_ROM_ADDR;
		break;
	}

	return ErrRet;
}