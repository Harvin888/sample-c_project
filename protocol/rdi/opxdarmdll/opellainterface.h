#ifndef opellainterface_h__
#define opellainterface_h__


#include "common.h"
#include "../opxdarmdll/midlayerarm.h"
#include "config.h"

//This define value is same as that of Pathfinder
#define IDERR_LOAD_ARM_DLL_OPELLAXD 61722

typedef TyError (* PerformReadROMTable_t)(char* pszSerialNumber, 
										  unsigned long ulCore,
										  TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo,
										  TyCoresightComp* ptyCoresightComp);

typedef TyError (* PerformRDIDiagnostics_t)(char* pszSerialNumber, 
											TyRDIDiagInfo *tyRdiDiagInfo);

typedef TyError (* PerformRDIDiagnosticsOnTarget_t)(char* pszSerialNumber,
													TyRDIOnTargetDiagInfo *tyRdiOnTargetDiagInfo);

typedef TyError (* PerformTargetHardReset_t)(char* pszSerialNumber,
											 TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo);

typedef void (* ML_CheckOpellaXDInstance_t)(char pszSerialNumber[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN], 
										  unsigned short *usNumberOfDevices);

typedef const TCHAR* (* GetRDIDLLVersion_t)(void);

typedef const TCHAR* (* GetFirmwareInfo_t)(void);

typedef ProcessorConfig* (* GetUserRegisterSettings_t)(void);

struct OpxdrdiFnptr
{
    PerformReadROMTable_t			PerformReadROMTable;
	PerformRDIDiagnostics_t			PerformRDIDiagnostics;
	PerformRDIDiagnosticsOnTarget_t	PerformRDIDiagnosticsOnTarget;
	PerformTargetHardReset_t		PerformTargetHardReset;
	ML_CheckOpellaXDInstance_t		ML_CheckOpellaXDInstance;
	GetRDIDLLVersion_t				GetRDIDLLVersion;
	GetFirmwareInfo_t				GetFirmwareInfo;
	GetUserRegisterSettings_t		GetUserRegisterSettings;
};

void GetRDIFnPtrs(OpxdrdiFnptr *pOpxdrdiFnptr);

#endif // opellainterface_h__
