/****************************************************************************
       Module: disathumb.c
     Engineer: Jeenus Chlattu Kunnath
  Description: disassembler for the THUMB instruction set

Date          Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
06-Jun-2008	   RS		   Added Linux Support
****************************************************************************/

#include "DISA.h"   
#include "midlayerarm.h"
#include "ARMdefs.h"
#include <stdio.h>
#include <string.h>

#define MAKE_UINT(LOW_UBYTE,HIGH_UBYTE)  (((unsigned short)LOW_UBYTE) | (((unsigned short)HIGH_UBYTE)<<8));

#define NO_ERROR       0x0

typedef int ReturnValue;
typedef unsigned char BOOL;

static char tempstr[DISTHUMB_MAXSTRINGSIZE];
static char disathumbstr[DISTHUMB_MAXSTRINGSIZE];



 //union of structures defining the instruction types
typedef union access16 {
  uThumbType                          uthumbType;
  MoveShiftedRegister                 moveshiftedregister;
  AddSubtract                         addsubtract;
  MoveCompareAddSubtractImmediate     movecompareaddsubtractimmediate;
  ALUOperations                       aluoperations;
  HiRegisterOperationsBranchExchange  hiregisteroperationsbranchexchange;
  PcRelativeLoad                      pcrelativeload;
  LoadStoreWithRegisterOffset         loadstorewithregisteroffset;
  LoadStoreSignExtendebByteHalfword   loadstoresignextendebbytehalfword;
  LoadStoreImmediateOffset            loadstoreimmediateoffset;
  LoadStoreHalfword                   loadstorehalfword;
  SPRelativeLoadStore                 sprelativeloadstore;
  LoadAddress                         loadaddress;
  AddOffsetToStackPointer             addoffsettostackpointer;
  PushPopRegisters                    pushpopregisters;
  MultipleLoadStore                   multipleloadstore;
  ConditionalBranch                   conditionalbranch;
  SoftwareInterrupt                   softwareinterrupt;
  UnconditionalBranch                 unconditionalbranch;
  LongBranchWithLink                  longbranchwithlink;
} Access16;

// instruction formats
typedef enum 
{
   MOVESHIFTEDREGISTER                 = 0,  
   ADDSUBTRACT                         = 1,  
   MOVECOMPAREADDSUBTRACTIMMEDIATE     = 2,  
   ALUOPERATIONS                       = 3,  
   HIREGISTEROPERATIONSBRANCHEXCHANGE  = 4,  
   PCRELATIVELOAD                      = 5,  
   LOADSTOREWITHREGISTEROFFSET         = 6,  
   LOADSTORESIGNEXTENDEBBYTEHALFWORD   = 7,  
   LOADSTOREIMMEDIATEOFFSET            = 8,  
   LOADSTOREHALFWORD                   = 9,  
   SPRELATIVELOADSTORE                 = 10, 
   LOADADDRESS                         = 11, 
   ADDOFFSETTOSTACKPOINTER             = 12, 
   PUSHPOPREGISTERS                    = 13, 
   MULTIPLELOADSTORE                   = 14, 
   CONDITIONALBRANCH                   = 15, 
   SOFTWAREINTERRUPT                   = 16, 
   UNCONDITIONALBRANCH                 = 17, 
   LONGBRANCHWITHLINK                  = 18, 
   UNDEFINEDTHUMB                      = 19
}FormatTypes;  

//Function Prototypes
static ReturnValue DisassembleTHUMBHiRegisterOperationsBranchExchange(HiRegisterOperationsBranchExchange hiregisteroperationsbranchexchange, DisaInst *pDisaType, ulong *pulReg);
static ReturnValue DisassembleTHUMBUnconditionalBranch(UnconditionalBranch unconditionalbranch, DisaInst *pDisaType, ulong *pulReg);
static ReturnValue DisassembleTHUMBSoftwareInterrupt(DisaInst *pDisaType, ulong *pulReg);
static ReturnValue DisassembleTHUMBConditionalBranch(ConditionalBranch  conditionalbranch, DisaInst *pDisaType, ulong *pulReg);
static ReturnValue DisassembleTHUMBMultipleLoadStore(MultipleLoadStore  multipleloadstore);
static ReturnValue DisassembleTHUMBPushPopRegisters(PushPopRegisters pushpopregisters, DisaInst *pDisaType, ulong *pulReg);
static ReturnValue DisassembleTHUMBAddOffsetToStackPointer(AddOffsetToStackPointer addoffsettostackpointer);
static ReturnValue DisassembleTHUMBLoadAddress(LoadAddress loadaddress);
static ReturnValue DisassembleTHUMBLongBranchWithLink(LongBranchWithLink longbranchwithlink,ubyte *pMemory, DisaInst *pDisaType, ulong *pulReg);
uThumbType GetTHUMBInstructionFormat(Access16 instruction);
BOOL IsMisalignedThumbBranchWithLinkInstruction(unsigned char *pMemory);


/****************************************************************************
     Function: DisassembleTHUMBHiRegisterOperationsBranchExchange
     Engineer: Jeenus Chlattu Kunnath 
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ReturnValue DisassembleTHUMBHiRegisterOperationsBranchExchange
                           (HiRegisterOperationsBranchExchange hiregisteroperationsbranchexchange, 
                           DisaInst *pDisaType, 
                           ulong *pulReg )
{
    unsigned long ulDest;
    unsigned long ulBytes = 2;
    TyError  ErrRet = ML_ERROR_NO_ERROR;
   uThumbType SRangeShifter=0,DRangeShifter=0;

   //Decide the register range pointed to 0-7 or 8-15
   if(hiregisteroperationsbranchexchange.H2)  //refers to Rs
      SRangeShifter=8; // shift to the high range
   
   if(hiregisteroperationsbranchexchange.H1)  //refers to Rd
      DRangeShifter=8; // shift to the high range

   //Disassemble
   if(0x0==hiregisteroperationsbranchexchange.Op)//
      {   
      //if the destanation register is the PC
      if(hiregisteroperationsbranchexchange.Rd+DRangeShifter==0x0f)
         {
         unsigned long ulNextPC=0;
         // need to work out what is being added 
         //if the disassembling instruction at current PC 
         ulNextPC=pulReg[hiregisteroperationsbranchexchange.Rd+DRangeShifter]+pulReg[hiregisteroperationsbranchexchange.Rs+SRangeShifter]+4;
         if(hiregisteroperationsbranchexchange.Rs+SRangeShifter==0xf)
            ulNextPC+=4;

         if(pDisaType->StartAddr==pulReg[0xf])
            pDisaType->DestAddr=ulNextPC;
         }
      }  

   if(0x2==hiregisteroperationsbranchexchange.Op)//MOV
      {
      // if destionation Reg is the PC
      if(((hiregisteroperationsbranchexchange.Rd+DRangeShifter)&0xf)==0xf)
         {
         unsigned long ulNextPC=0;
         pDisaType->InstType = RET_INST;

         // need to work out what is being added 
         //if the disassembling instruction at current PC 
         //if the exchange reg id R15, then  add 4 to dest address
         if(hiregisteroperationsbranchexchange.Rs+SRangeShifter==0xf)
            ulNextPC=pulReg[hiregisteroperationsbranchexchange.Rs+SRangeShifter]+4;
         else   
            ulNextPC=pulReg[hiregisteroperationsbranchexchange.Rs+SRangeShifter];

         if(pDisaType->StartAddr==pulReg[0xf])
            pDisaType->DestAddr=ulNextPC;
         }
      }

   if(0x3==hiregisteroperationsbranchexchange.Op)//BX
      {
      unsigned long ulNextPC=0;
      uThumbType uPreviousInst=0;
      Access16 LocInst;
      FormatTypes InstFormat;      
      //if the exchange reg is R15, then  add 4 to dest address
      if(hiregisteroperationsbranchexchange.Rs+SRangeShifter==0xf)
         ulNextPC=pulReg[hiregisteroperationsbranchexchange.Rs+SRangeShifter]+4;
      else
         ulNextPC=pulReg[hiregisteroperationsbranchexchange.Rs+SRangeShifter];
      
      // Note: since this is a BX we need to ensure address is half word aligned
      // pDisaType->DestAddr is ANDed with 0xfffffffe in the high-level disassemble() fn

      //if the PC is at this instruction update the Destination address
      if(pDisaType->StartAddr==pulReg[0xf])
         pDisaType->DestAddr=ulNextPC;
      // instruction type 
      //read the previous two bytes of memory

      ErrRet = ML_ReadFromTargetMemory(pDisaType->StartAddr-2,
                                       &ulDest,
                                       &ulBytes,
                                       RDIAccess_Code);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;

      ///put the instruction into the instruction union 
      LocInst.uthumbType = uPreviousInst;
      
      //Get the instruction Format
      InstFormat = (FormatTypes)GetTHUMBInstructionFormat(LocInst);
      
      //check the instruction format
      //if these bytes are a pop instruction then BX is a RET_INST
      if(PUSHPOPREGISTERS==InstFormat)
         pDisaType->InstType = RET_INST;
      //else BX is a VJUMP_INST
      else
         pDisaType->InstType = VJUMP_INST;

      // If the LSB is cleared, then we are switching into ARM code...
      if (!(pDisaType->DestAddr & 0x1))
         pDisaType->ucBXInstType = BX_INST_TO_ARM_CODE;
      }

   return(0);

}  /* DisassembleTHUMBHiRegisterOperationsBranchExchange */


/****************************************************************************
     Function: DisassembleTHUMBPushPopRegisters
     Engineer: Jeenus Chlattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ReturnValue DisassembleTHUMBPushPopRegisters(PushPopRegisters pushpopregisters, 
                                                    DisaInst *pDisaType, 
                                                    ulong *pulReg)
{

    unsigned long ulBytes = 4;
    unsigned long ulDest;
    TyError  ErrRet = ML_ERROR_NO_ERROR;
   pDisaType->InstType = OTHER_INST;   

   if( (pushpopregisters.L) && (!pushpopregisters.R) )
      {
      pDisaType->InstType = RET_INST;
      }

   if( (pushpopregisters.L)&&(pushpopregisters.R) )
      {
      unsigned int uiBitCount=0,uiLoopCount=0,uiRegMask=1,uiRegList=0;
      unsigned long ulSPBase=0,ulNextPC=0;     

      //set the instruction Type
      pDisaType->InstType = RET_INST;

      //WE NEED TO DO A FEW CALCULATIONS AS TO WHERE THE PC WILL END UP
      
      //determine the number of bits set in the RList mask
      uiRegList=pushpopregisters.Rlist;

      // see how many bits are set in Rlist   
      for(uiLoopCount=0;uiLoopCount<=7;uiLoopCount++)
         {
         if((uiRegMask&uiRegList)==uiRegMask)
            uiBitCount++;
         uiRegMask=uiRegMask<<1;
         }
      //read the address of the Stack Pointer
      ulSPBase=pulReg[13];
      //read the memory pointed to taking into account any offset 
      ErrRet = ML_ReadFromTargetMemory(ulSPBase+(4*uiBitCount),
                                       &ulDest,
                                       &ulBytes,
                                       RDIAccess_Code);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      //if we are disassembling at the current PC 
      if(pDisaType->StartAddr==pulReg[0xf])
         pDisaType->DestAddr=ulNextPC;
      }

   return(0);

}  /* DisassembleTHUMBPushPopRegisters */

/****************************************************************************
     Function: DisassembleTHUMBConditionalBranch
     Engineer: Jeenus Chlattu Kunnath 
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ReturnValue DisassembleTHUMBConditionalBranch(ConditionalBranch conditionalbranch, 
                                                     DisaInst *pDisaType, 
                                                     ulong *pulReg)
{
   signed int SignedOffset=0;   
   *pulReg= *pulReg;

   SignedOffset=conditionalbranch.Soffset8<<1;
   //Compensate for PC will be one word (4Bytes) ahead of current instruction
   //CHECK THE MSB
   if((SignedOffset&0x100)==0x100) //negative number
      {
      SignedOffset = SignedOffset - 0x100;
      SignedOffset = 0x100 - SignedOffset;
      SignedOffset = pDisaType->StartAddr - SignedOffset + PC_ADVANCED_BY_1WORD;
      }
   else
      SignedOffset = pDisaType->StartAddr + SignedOffset + PC_ADVANCED_BY_1WORD; 
   
   pDisaType->InstType = FJUMP_INST;
   pDisaType->DestAddr=SignedOffset;

   return(0);

}  /* DisassembleTHUMBConditionalBranch */

/****************************************************************************
     Function: DisassembleTHUMBSoftwareInterrupt
     Engineer: Jeenus Chlattu Kunnath 
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ReturnValue DisassembleTHUMBSoftwareInterrupt(
                                                     DisaInst *pDisaType, 
                                                     ulong *pulReg)
{
   //if we are disassembling at the current PC 
   if(pDisaType->StartAddr==pulReg[0xf])
      pDisaType->DestAddr=0x8;
   return(0);
}
/****************************************************************************
     Function: DisassembleTHUMBUnconditionalBranch
     Engineer: Jeenus Chlattu Kunnath
	 Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ReturnValue DisassembleTHUMBUnconditionalBranch(UnconditionalBranch unconditionalbranch, 
                                                       DisaInst *pDisaType, 
                                                       ulong *pulReg)
{
   unsigned long SignedOffset=0;  
   //REVISE WHEN ADDING SIMFINDER SUPPORT

   SignedOffset=((unconditionalbranch.Offset11)<<1);
   //CHECK THE MSB
   if((SignedOffset&0x800)==0x800) //negative number
      {
      SignedOffset = SignedOffset - 0x800;
      SignedOffset = 0x800 - SignedOffset;
      SignedOffset = PC_ADVANCED_BY_1WORD + pDisaType->StartAddr - SignedOffset;
      }
   else
      SignedOffset = SignedOffset + PC_ADVANCED_BY_1WORD + pDisaType->StartAddr;  
   
   pDisaType->InstType = FJUMP_INST;
   //if we are disassembling at the current PC 
   if(pDisaType->StartAddr==pulReg[0xf])
      pDisaType->DestAddr=SignedOffset;

   return(0);

}  /* DisassembleTHUMBUnconditionalBranch */

/****************************************************************************
     Function: DisassembleTHUMBLongBranchWithLink
     Engineer: Jeenus Chlattu Kunnath
        Input: structure defining the format type 
       Output: position in string of condition code
  Description: fill out a string describing the disassembled instruction
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
static ReturnValue DisassembleTHUMBLongBranchWithLink(LongBranchWithLink longbranchwithlink,
                                                      unsigned char *pMemory, 
                                                      DisaInst *pDisaType, 
                                                      ulong *pulReg)
{
   signed long   ulHiSignedOffset = 0;
   signed long   ulloSignedOffset = 0;
   signed long   ulSignedOffset   = 0;
   unsigned long ulLinkReg        = 0;
   //boolean       bSymFound        = FALSE;
   unsigned int  uiLocInstruction;
   Access16      instruction;
   //char          szTempString[MAX_INSTR_STR_SIZE];

   // lint
   longbranchwithlink = longbranchwithlink;
   
   //high 12 bits                      
   uiLocInstruction = MAKE_UINT(pMemory[0],pMemory[1]);
   instruction.uthumbType = uiLocInstruction;

   ulHiSignedOffset=((instruction.longbranchwithlink.Offset)<<12);

   //low 12 bits
   uiLocInstruction = MAKE_UINT(pMemory[2],pMemory[3]);
   instruction.uthumbType = uiLocInstruction;
   ulloSignedOffset=((instruction.longbranchwithlink.Offset)<<1);
   
   ulSignedOffset= ulloSignedOffset | ulHiSignedOffset;

   //CHECK THE MSB FOR NEGATIVE NUMBER
   //-- negative number --
   if((ulSignedOffset&0x400000)==0x400000)          
      {
      ulSignedOffset=ulSignedOffset-0x400000;
      ulSignedOffset=0x400000-ulSignedOffset;
      ulLinkReg = pDisaType->StartAddr + PC_ADVANCED_BY_1WORD - ulSignedOffset;
      }
   else
      ulLinkReg = ulSignedOffset + PC_ADVANCED_BY_1WORD + pDisaType->StartAddr;   
   
   if (IsMisalignedThumbBranchWithLinkInstruction(pMemory))
      pDisaType->InstType = VJUMP_INST;//might be FJUMP_INST!!! FCALL_INST;
   else
      pDisaType->InstType = FCALL_INST;

   //if we are disassembling at the current PC 
   if(pDisaType->StartAddr == pulReg[0xf])
      pDisaType->DestAddr = ulLinkReg;

   return(0);

}  /* DisassembleTHUMBLongBranchWithLink */

/****************************************************************************
     Function: IsThumbBranchWithLink
     Engineer: Jeenus Chlattu Kunnath
        Input: Pointer to memory
       Output: Boolean indicating whether this is a branch with link instruction.
  Description: Function required to determine if this is a thumb BL instruction.
               Used by a higher level to determine if the PC has landed on the
               incorrect second "half" of this instruction, and then to rewind
               the PC by 2 bytes.
Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
BOOL IsMisalignedThumbBranchWithLinkInstruction(unsigned char *pMemory)
{
   Access16 instruction;
   FormatTypes InstructionFormat;
   unsigned int uithumbinstruction; 

   uithumbinstruction = MAKE_UINT(pMemory[0],pMemory[1]);
   //put the instruction into the instruction union 
   instruction.uthumbType = uithumbinstruction;
   //Get the instruction Format
   InstructionFormat = (FormatTypes)GetTHUMBInstructionFormat(instruction);

   if (InstructionFormat != LONGBRANCHWITHLINK)
      return FALSE;

   // check the H Bit, if it's zero we're ok, otherwise rewind
   if ( (uithumbinstruction & 0x800) == 0x0 )
      return FALSE;
   else
      return TRUE;

}  /* IsThumbBranchWithLink */

/****************************************************************************
     Function: GetTHUMBInstructionFormat
     Engineer: Jeenus Chlattu Kunnath 
        Input: 16bit field describing the instruction
       Output: The format type of the 32bit field passed in 
  Description: chesk bit filed for match aginst predefined constant bit patterns

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
uThumbType GetTHUMBInstructionFormat(Access16 instruction)
{

   if( 3== instruction.addsubtract.mustbe00011 )
      {
      return((uThumbType)ADDSUBTRACT);
      }

   if(0==instruction.moveshiftedregister.mustbe000)
      {
      return((uThumbType)MOVESHIFTEDREGISTER);
      }  
   
   if(1==instruction.movecompareaddsubtractimmediate.mustbe001)
      {
      return((uThumbType)MOVECOMPAREADDSUBTRACTIMMEDIATE);
      }

   if    (0x10 == instruction.aluoperations.mustbe010000 )
      {
      return((uThumbType)ALUOPERATIONS);
      }

   if(0x11==instruction.hiregisteroperationsbranchexchange.mustbe010001)
      {
      return((uThumbType)HIREGISTEROPERATIONSBRANCHEXCHANGE);
      }

   if(9==instruction.pcrelativeload.mustbe01001 )
      {
      return((uThumbType)PCRELATIVELOAD);
      }

   if
      (
      (5==instruction.loadstorewithregisteroffset.mustbe0101) &&
      (0==instruction.loadstorewithregisteroffset.mustbe0)
      )
      {
      return((uThumbType)LOADSTOREWITHREGISTEROFFSET);
      }

   if
      (
      (5==instruction.loadstoresignextendebbytehalfword.mustbe0101) &&
      (1==instruction.loadstoresignextendebbytehalfword.mustbe1)
      )
      {
      return((uThumbType)LOADSTORESIGNEXTENDEBBYTEHALFWORD);
      }

   if(0x03==instruction.loadstoreimmediateoffset.mustbe011)
      {
      return((uThumbType)LOADSTOREIMMEDIATEOFFSET);
      }

   if(0x08==instruction.loadstorehalfword.mustbe1000)
      {
      return((uThumbType)LOADSTOREHALFWORD);
      }

   if(0x09==instruction.sprelativeloadstore.mustbe1001)
      {
      return((uThumbType)SPRELATIVELOADSTORE);
      }

   if(0x0a==instruction.loadaddress.mustbe1010  )
      {
      return((uThumbType)LOADADDRESS);
      }

   if(0xb0==instruction.addoffsettostackpointer.mustbe10110000)
      {
      return((uThumbType)ADDOFFSETTOSTACKPOINTER);
      }

   if
      (
      (0x02==instruction.pushpopregisters.mustbe10)&&
      (0x0b==instruction.pushpopregisters.mustbe1011)
      )
      {
      return((uThumbType)PUSHPOPREGISTERS);
      }


   if(0x0c==instruction.multipleloadstore.mustbe1100)
      {
      return((uThumbType)MULTIPLELOADSTORE);
      }


   if(0xdf==instruction.softwareinterrupt.mustbe11011111)
      {
      return((uThumbType)SOFTWAREINTERRUPT);
      }

   if(0x0d==instruction.conditionalbranch.mustbe1101)
      {
      return((uThumbType)CONDITIONALBRANCH);
      }

   if(0x1c==instruction.unconditionalbranch.mustbe11100)
      {
      return((uThumbType)UNCONDITIONALBRANCH);
      }

   if(0x0f==instruction.longbranchwithlink.mustbe1111)
      {
      return((uThumbType)LONGBRANCHWITHLINK);
      }

  else
      {
      /* Unknown */
      return((uThumbType)UNDEFINEDTHUMB);
      }

}  /* GetTHUMBInstructionFormat */

/****************************************************************************
     Function: Main
     Engineer: Jeenus Chalattu Kunnath
        Input: pMemory   *: pointer to memory to disassemble
               pDisaInst *: pointer to storage for found instruction
               StartAddr  : instructions start address
               pReg      *: current register values (used for calculating insturctions
                            destination address and symbol)
       Output: void
  Description: 

Date           Initials    Description
14-Mar-2008    JCK         RDI Step Implementation
****************************************************************************/
TyError DisassembleTHUMB(unsigned char *pMemory,
               DisaInst *pDisaInst,
               unsigned long StartAddr,
               unsigned long *pulReg)
{
   Access16 instruction;
   FormatTypes InstructionFormat;
   unsigned int uithumbinstruction; 
   ReturnValue returnvalue = NO_ERROR;

   pDisaInst->InstType     = OTHER_INST;
   pDisaInst->InstLen      = 0x02;
   pDisaInst->StartAddr    = StartAddr;
   pDisaInst->DestAddr     = StartAddr+2;
   pDisaInst->bConditional = FALSE;
   pDisaInst->ucBXInstType = NOT_A_BX_INST;

   
   //store instruction in variable
   uithumbinstruction = MAKE_UINT(pMemory[0],pMemory[1]);   
   
   //put the instruction into the instruction union 
   instruction.uthumbType = uithumbinstruction;
   
   //Get the instruction Format
   InstructionFormat = (FormatTypes)GetTHUMBInstructionFormat(instruction);
   
   //Disassemble the instruction
   switch(InstructionFormat)
      {
      case  MOVESHIFTEDREGISTER:         
      case  ADDSUBTRACT:         
      case  MOVECOMPAREADDSUBTRACTIMMEDIATE:        
      case  ALUOPERATIONS:   
      case  PCRELATIVELOAD:         
      case  LOADSTOREWITHREGISTEROFFSET:
      case  LOADSTORESIGNEXTENDEBBYTEHALFWORD:        
      case  LOADSTOREIMMEDIATEOFFSET:         
      case  LOADSTOREHALFWORD:         
      case  SPRELATIVELOADSTORE:         
      case  LOADADDRESS:         
      case  ADDOFFSETTOSTACKPOINTER:                 
      case  MULTIPLELOADSTORE:   
          break;    
      case  HIREGISTEROPERATIONSBRANCHEXCHANGE:
         returnvalue =  DisassembleTHUMBHiRegisterOperationsBranchExchange(instruction.hiregisteroperationsbranchexchange, pDisaInst, pulReg);
         break;                       
      case  PUSHPOPREGISTERS:
         returnvalue =  DisassembleTHUMBPushPopRegisters(instruction.pushpopregisters, pDisaInst, pulReg);
         break;
      case  CONDITIONALBRANCH:
         returnvalue =  DisassembleTHUMBConditionalBranch(instruction.conditionalbranch,pDisaInst, pulReg);
         pDisaInst->bConditional = TRUE;
         break;
      case  SOFTWAREINTERRUPT:
         returnvalue =  DisassembleTHUMBSoftwareInterrupt(pDisaInst, pulReg);
         break;
      case  UNCONDITIONALBRANCH:
         returnvalue =  DisassembleTHUMBUnconditionalBranch(instruction.unconditionalbranch, pDisaInst, pulReg);
         break;
      case  LONGBRANCHWITHLINK:
         returnvalue =  DisassembleTHUMBLongBranchWithLink(instruction.longbranchwithlink,pMemory, pDisaInst, pulReg);
         break;
      default:
         break;
      }
   // the one exception, this type is made up of four bytes
   if (InstructionFormat==LONGBRANCHWITHLINK)
      pDisaInst->InstLen = 0x04;

   //return the disassembly string
   strcpy(pDisaInst->szString,disathumbstr);

   return returnvalue;

}  /* DisassembleTHUMB */
