// Progress.cpp : implementation file
//
#include "stdafx.h"      
#include "resource.h"
#include "progress.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProgressControl dialog


CProgressControl::CProgressControl(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProgressControl)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

  (void)Create(IDD, pParent);
  CenterWindow();
}


void CProgressControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressControl)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProgressControl, CDialog)
	//{{AFX_MSG_MAP(CProgressControl)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgressControl message handlers
