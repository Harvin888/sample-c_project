// opxdarm.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "stdlib.h"
#include "opxdarm.h"
#include "OpellaARMConfigInterface.h"

extern char pszGlobalDllPathName[_MAX_PATH];
extern TyTargetConfig tyTargetConfig;

//extern "C" { char __declspec(dllexport) pszGlobalDllPathName1[_MAX_PATH]; }
//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// COpxdarmApp

BEGIN_MESSAGE_MAP(COpxdarmApp, CWinApp)
	//{{AFX_MSG_MAP(COpxdarmApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COpxdarmApp construction

COpxdarmApp::COpxdarmApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only COpxdarmApp object

COpxdarmApp theApp;
/****************************************************************************
Function	: InitInstance
Engineer	: Suraj S
Input		: none
Output		: BOOL - return TRUE if success, else FALSE
Description	: This is the entry point when the DLL is loaded
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
BOOL COpxdarmApp::InitInstance()
{
   
   OutputDebugString("OPXDARM.DLL Initialising!\n");
      
   GetFullPathForDLL(pszGlobalDllPathName);
   //strcpy(pszGlobalDllPathName1,pszGlobalDllPathName);

   // Initialize Opella GUI Extension DLL
   if(NO_ERROR != InitOpellaARMConfigGUI(pszGlobalDllPathName))
	   return FALSE;
   
   return CWinApp::InitInstance();  // Initialisation
}

/****************************************************************************
Function	: GetFullPathForDLL
Engineer	: Suraj S
Input		: char *pszPathName - pointer to store the RDI DLL path name
Output		: none
Description	: Retrieves the path of the RDI dll.
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
void COpxdarmApp::GetFullPathForDLL(char *pszPathName)
{
   char *pszPtr;
   if (!GetModuleFileName(theApp.m_hInstance,pszPathName,_MAX_PATH))
      {
      pszPathName[0] = '\0';
      return ;
      }

   /* strip name.dll  */
   (void)_strlwr(pszPathName);

   pszPtr=strstr(pszPathName,".dll");

   if (pszPtr)
      *pszPtr='\0';
   else
      {
      pszPathName[0] = '\0';
      return ;
      }

   while ( *pszPtr != '\\')
      pszPtr--;
   *pszPtr='\0';
}

/****************************************************************************
Function	: ExitInstance
Engineer	: Suraj S
Input		: none
Output		: int - return TRUE if success, else ERROR
Description	: This is the exit point when the DLL is unloaded
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
int COpxdarmApp::ExitInstance()
{
   OutputDebugString("OPXDARM.DLL Terminating!\n");

   /*	Free the OpellaXDARMConfig.dll if it is already loaded	*/
   if(m_dllHandle)
   {
	   FreeLibrary(m_dllHandle);
	   return CWinApp::ExitInstance();
   }
   else
   {
	   CWinApp::ExitInstance();
	   return IDERR_LOAD_ARM_DLL_OPELLAXD;
   }
}