#if !defined(AFX_PROGRESS_H__EBC268E1_EE96_11D6_95AD_0050DA3E4DF5__INCLUDED_)
#define AFX_PROGRESS_H__EBC268E1_EE96_11D6_95AD_0050DA3E4DF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Progress.h : header file
//

#include "../opxdarmdll/dll/Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CProgressControl dialog

class CProgressControl : public CDialog
{
// Construction
public:
	CProgressControl(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CProgressControl)
	enum { IDD = IDD_OPAVRII_PROGRESS_CONTROL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgressControl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CProgressControl)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGRESS_H__EBC268E1_EE96_11D6_95AD_0050DA3E4DF5__INCLUDED_)
