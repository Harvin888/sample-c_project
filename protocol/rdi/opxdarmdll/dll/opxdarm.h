// opxdarm.h : main header file for the OPXDARM DLL
//

#if !defined(AFX_OPXDARM_H__12922E4D_BB1C_42B6_9720_EF24E8189A79__INCLUDED_)
#define AFX_OPXDARM_H__12922E4D_BB1C_42B6_9720_EF24E8189A79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// COpxdarmApp
// See opxdarm.cpp for the implementation of this class
//

class COpxdarmApp : public CWinApp
{
public:
	COpxdarmApp();
	BOOL InitInstance();         //Initialisation
   void GetFullPathForDLL( char *pszPathName );
   virtual int ExitInstance();  //Termination 
   int InitOpellaARMConfigGUI(const char *pszARMRDIDriverpath);
private:
	HINSTANCE m_dllHandle;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COpxdarmApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(COpxdarmApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPXDARM_H__12922E4D_BB1C_42B6_9720_EF24E8189A79__INCLUDED_)
