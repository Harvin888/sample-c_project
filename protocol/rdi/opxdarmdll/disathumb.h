/****************************************************************************
       Module: disathumb.h
     Engineer: Jeenus Challattu Kunnath
  Description: Header file for THUMB Disassembler

Date           	Initials    Description
14-Mar-2008    JCK          Initial
****************************************************************************/
#include "../../common/drvopxd.h"

#define DISTHUMB_MAXSTRINGSIZE 256
#define PC_ADVANCED_BY_1WORD 4
typedef unsigned int uThumbType; /* 16 bits */

typedef struct  moveshiftedregister{
  uThumbType Rd		: 3;
  uThumbType Rs		: 3;
  uThumbType Offset5	: 5;
  uThumbType Op		: 2;
  uThumbType mustbe000	: 3;
} MoveShiftedRegister;

typedef struct  addsubtract{
  uThumbType Rd			: 3;
  uThumbType Rs			: 3;
  uThumbType Rn			: 3;
  uThumbType Op			: 1;
  uThumbType I				: 1;
  uThumbType mustbe00011	: 5;
} AddSubtract;

typedef struct  movecompareasssubtractimmediate{
uThumbType Offset8		:8;
uThumbType Rd	   		:3;
uThumbType Op	   		:2;
uThumbType mustbe001	:3;
}MoveCompareAddSubtractImmediate;

typedef struct aluoperations {
uThumbType Rd	   		:3;
uThumbType Rs	   		:3;
uThumbType Op	   		:4;
uThumbType mustbe010000:6;
}ALUOperations;

typedef struct hiregisteroperationsbranchexchange {
uThumbType Rd	   		:3;
uThumbType Rs	   		:3;
uThumbType H2			:1;
uThumbType H1			:1;
uThumbType Op			:2;
uThumbType mustbe010001:6;
}HiRegisterOperationsBranchExchange ;

typedef struct pcrelativeload {
uThumbType Word8		:8;
uThumbType Rd			:3;
uThumbType mustbe01001	:5;
}PcRelativeLoad;

typedef struct  loadstorewithregisteroffset{
uThumbType Rd	   		:3;
uThumbType Rb	   		:3;
uThumbType Ro	   		:3;
uThumbType mustbe0		:1;
uThumbType B			:1;
uThumbType L			:1;
uThumbType mustbe0101	:4;
}LoadStoreWithRegisterOffset;

typedef struct  loadstoresignextendebbytehalfword{
uThumbType Rd	   		:3;
uThumbType Rb	   		:3;
uThumbType Ro	   		:3;
uThumbType mustbe1		:1;
uThumbType S			:1;
uThumbType H			:1;
uThumbType mustbe0101	:4;
}LoadStoreSignExtendebByteHalfword;

typedef struct  loadstoreimmediateoffset{
uThumbType Rd	   		:3;
uThumbType Rb	   		:3;
uThumbType Offset5	   	:5;
uThumbType L			:1;
uThumbType B			:1;
uThumbType mustbe011	:3;
}LoadStoreImmediateOffset;

typedef struct  loadstorehalfword{
uThumbType Rd	   		:3;
uThumbType Rb	   		:3;
uThumbType Offset5	   	:5;
uThumbType L			:1;
uThumbType mustbe1000	:4;
}LoadStoreHalfword;


typedef struct  sprelativeloadstore{
uThumbType Word8  		:8;
uThumbType Rd	   		:3;
uThumbType L			:1;
uThumbType mustbe1001	:4;
}SPRelativeLoadStore;

typedef struct  loadaddress{
uThumbType Word8  		:8;
uThumbType Rd	   		:3;
uThumbType SP			:1;
uThumbType mustbe1010	:4;
}LoadAddress;

typedef struct  addoffsettostackpointer{
uThumbType SWord7  		:7;
uThumbType S				:1;
uThumbType mustbe10110000	:8;
}AddOffsetToStackPointer;


typedef struct  PushPopRegisters{
uThumbType Rlist		:8;
uThumbType R			:1;
uThumbType mustbe10	:2;
uThumbType L			:1;
uThumbType mustbe1011	:4;
}PushPopRegisters;

typedef struct  multipleloadstore{
uThumbType Rlist		:8;
uThumbType Rb			:3;
uThumbType L			:1;
uThumbType mustbe1100	:4;
}MultipleLoadStore;


typedef struct conditionalbranch {
uThumbType Soffset8	:8;
uThumbType Cond		:4;
uThumbType mustbe1101	:4;
}ConditionalBranch;

typedef struct  softwareinterrupt{
uThumbType Value8			:8;
uThumbType mustbe11011111	:8;
}SoftwareInterrupt;

typedef struct  unconditionalbranch{
uThumbType Offset11	:11;
uThumbType mustbe11100	:5;
}UnconditionalBranch;

typedef struct  longbranchwithlink{
uThumbType Offset		:11;
uThumbType H			:1;
uThumbType mustbe1111	:4;
}LongBranchWithLink;



