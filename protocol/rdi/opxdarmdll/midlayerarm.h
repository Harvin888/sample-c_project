/****************************************************************************
       Module: MDILAYER.H
     Engineer: Suraj S
  Description: Header File for middle layer
Date           Initials    Description
3-Jul-2007      SJ         Initial
31-Mar-2008	    VK		   Added RDI JTAG console Support for ARM GDB Server
09-Apr-2008     RS         Modified for Multicore Support in ARM GDB Server 
06-Jun-2008		RS		   Added Linux Support
****************************************************************************/
#ifndef _MDILAYER_H_
#define _MDILAYER_H_
#ifdef _WINDOWS
#include <tchar.h>
#include "../rdi/GUIDefs.h"
#endif
#include "../../common/drvopxd.h"
#include "../rdi/rdi.h"
#include "common.h"
#ifdef __LINUX
typedef char            TCHAR;
#endif

#define NO_DISKWARE_LOADED    0x0
#define ARM_DISKWARE          0x1

#define INFO_VERSION_DATE_LEN 255

#define SCANCHAIN_ULONG_SIZE     255
#define MAX_DRIVER_NAME 0x40
#define KILOHZ          1000
#define MAX_LOOP_CYCLES 1000

#define ARM_DEBUG_MODE		0x09
#define ARM11_DEBUG_MODE	0x01

#define ARM_CORE1       0xABABABAB
#define ARM_CORE2       0xBABABABA

//ICEBreaker module definifions
#define SET_DBGR_BIT                0x02
#define SET_INTDIS_DBGR_BIT         0x06
#define INTDIS_DBGACK               0x05
#define INTDIS_DBGR_DBGACK          0x07
#define THUMB_MODE_BIT              0x10
#define DBGACK_BIT                  0x01
#define ALL_BITS_CLEAR              0x00

// EmbeddedICE register addresses.
#define DBG_CONTROL_REG             0x00
#define DBG_STATUS_REG              0x01
#define DCC_CONTROL_REG             0x04
#define DCC_DATA_REG                0x05
#define VECTOR_CATCH                0x02     

/* Vector address for SWI */
#define SWI_LOWER_VECTOR_ADDR		0x8
#define SWI_HIGHER_VECTOR_ADDR		0xFFFF0008

// Watchpoint register definitions.
#define WP0_ADDR_V                  0x08
#define WP0_ADDR_M                  0x09
#define WP0_DATA_V                  0x0A
#define WP0_DATA_M                  0x0B
#define WP0_CTRL_V                  0x0C
#define WP0_CTRL_M                  0x0D
                                    
#define WP1_ADDR_V                  0x10
#define WP1_ADDR_M                  0x11
#define WP1_DATA_V                  0x12
#define WP1_DATA_M                  0x13
#define WP1_CTRL_V                  0x14
#define WP1_CTRL_M                  0x15

//ARM11 CP14 reg addresses
#define VECTOR_CATCH_REGISTER       7
#define BP0_VALUE_REGISTER          64
#define BP1_VALUE_REGISTER          65
#define BP2_VALUE_REGISTER          66
#define BP3_VALUE_REGISTER          67
#define BP4_VALUE_REGISTER          68
#define BP5_VALUE_REGISTER          69
#define BP0_CTRL_REGISTER           80
#define BP1_CTRL_REGISTER           81
#define BP2_CTRL_REGISTER           82
#define BP3_CTRL_REGISTER           83
#define BP4_CTRL_REGISTER           84
#define BP5_CTRL_REGISTER           85
#define WP0_VALUE_REGISTER          96
#define WP1_VALUE_REGISTER          97
#define WP0_CTRL_REGISTER           112
#define WP1_CTRL_REGISTER           113


//ARM11 WP  set up codes
#define ARM11_ARM_SW_BP_VALUE			        0x1FF
#define ARM11_ARM_HW_BP_VALUE			        0x00000007
#define ARM11_HW_BP_BYTE0_ACCESS_CTRL_VALUE 0x00000020
#define ARM11_HW_BP_BYTE1_ACCESS_CTRL_VALUE 0x00000040
#define ARM11_HW_BP_BYTE2_ACCESS_CTRL_VALUE 0x00000080
#define ARM11_HW_BP_BYTE3_ACCESS_CTRL_VALUE 0x00000100
#define ARM11_HW_BP_HALF0_ACCESS_CTRL_VALUE 0x00000060
#define ARM11_HW_BP_HALF1_ACCESS_CTRL_VALUE 0x00000180
#define ARM11_HW_BP_WORD_ACCESS_CTRL_VALUE  0x000001E0

#define ARM11_ARM_WP_VALUE               0x00000007

#define ARM11_WP_BYTE0_ACCESS_CTRL_VALUE 0x00000020
#define ARM11_WP_BYTE1_ACCESS_CTRL_VALUE 0x00000040
#define ARM11_WP_BYTE2_ACCESS_CTRL_VALUE 0x00000080
#define ARM11_WP_BYTE3_ACCESS_CTRL_VALUE 0x00000100

#define ARM11_WP_HALF0_ACCESS_CTRL_VALUE 0x00000060
#define ARM11_WP_HALF1_ACCESS_CTRL_VALUE 0x00000180

#define ARM11_WP_WORD_ACCESS_CTRL_VALUE  0x000001E0
#define ARM11_WP_WRITE_ONLY_CTRL_VALUE   0x00000010
#define ARM11_WP_READ_ONLY_CTRL_VALUE    0x00000008
#define ARM11_WP_READ_WRITE_CTRL_VALUE   0x00000018

//ICEBreaker Module Watchpoint setup codes.
#define ARM9TDMI_ARM_BYTE_WP_VALUE        0x0   // not defined yet
#define ARM9TDMI_ARM_HALFWORD_WP_VALUE    0x0   // not defined yet
#define ARM9TDMI_ARM_WORD_WP_VALUE        0x0   // not defined yet
#define ARM9TDMI_ARM_WP_MASK              0x0   // not defined yet
#define ARM9TDMI_ARM_WP_DONTCARE_MASK     0x0   // not defined yet
#define ARM9TDMI_VECTOR_CATCH_ON          0xDF  // Stop on entry to all exception modes.
#define ARM9TDMI_THUMB_SW_BP_VALUE        0x102 // opcode read. Thumb Mode
#define ARM9TDMI_ARM_SW_BP_VALUE          0x100 // opcode read, ARM Mode.
#define ARM9TDMI_ARM_SW_BP_MASK           0xF3  // opcode read.
#define ARM7TDMIS_ARM_ENABLE_WP           0x100 // Enable WP/ BP's
#define ARM7TDMIS_ARM_CHECK_IFETCH_ONLY   0xF7  // Only Stop on Instuction fetches
#define ARM7TDMI_THUMB_SW_BP_VALUE        0x102 // opcode read, half word access.
#define ARM7TDMI_ARM_SW_BP_VALUE          0x104 // opcode read, word access.
#define ARM7TDMI_ARM_SW_BP_MASK           0xF6  // opcode read.
#define ARM7TDMI_ARM_BYTE_WP_VALUE        0x108 // Byte access.
#define ARM7TDMI_ARM_HALFWORD_WP_VALUE    0x10A // Halfword access.
#define ARM7TDMI_ARM_WORD_WP_VALUE        0x10C // Word access.
#define ARM7TDMI_ARM_WP_MASK              0xF1  // Check access size only.
#define ARM7TDMI_ARM_WP_DONTCARE_MASK     0xF7  // dont check access size.
#define ARM7DI_ARM_SW_BP_VALUE            0x82  // opcode read, word access.
#define ARM7DI_ARM_SW_BP_MASK             0x78  // opcode read, word access.
#define ARM7DI_ARM_BYTE_WP_VALUE          0x84  // not defined yet
#define ARM7DI_ARM_WORD_WP_VALUE          0x86  // not defined yet
#define ARM7DI_ARM_WP_MASK                0x79  // Check byte or word and op-code
#define ARM7DI_ARM_WP_DONTCARE_MASK       0x7B  // Check op-code only word or byte access
#define ARM_BP_DONT_CARE                  0xFFFFFFFF // Ignore all bits.
#define ARM_BP_DO_CARE                    0x00000000 // Check all bits.
#define ARM_BP_BYTE_MASK                  0xFFFFFF00 // Only check byte.
#define ARM_BP_HALFWORD_MASK              0xFFFF0000 // Only check half word.

#define WATCHPOINTS_DISABLED              0x00000000 // Watchpoint enable bits cleared. 
#define RESOURCE_CLEARED                  0x00000000 // Watchpoint bits cleared. 

// These definitions are used in the breakpoint structure so that each BP has a type and
//hardware resource stored in the structure.
#define HARDWARE_BP0          0x00
#define HARDWARE_BP1          0x01
#define HARDWARE_BPX          0x02
#define HARDWARE_WP0          0x03
#define HARDWARE_WP1          0x04
#define HARDWARE_WPX          0x05
#define SOFTWARE_BP0          0x08
#define SOFTWARE_BP1          0x09
#define SOFTWARE_WP0          0x08
#define SOFTWARE_WP1          0x09

#define HARDWARE_BP2          0x02
#define HARDWARE_BP3          0x03
#define HARDWARE_BP4          0x04
#define HARDWARE_BP5          0x05
#define HARDWARE_BP6          0x06
#define HARDWARE_BP7          0x07



#define HARDWARE_RESOURCE0    0x00
#define HARDWARE_RESOURCE1    0x01
#define HARDWARE_RESOURCE2    0x02
#define HARDWARE_RESOURCE3    0x03
#define HARDWARE_RESOURCE4    0x04
#define HARDWARE_RESOURCE5    0x05

// Software breakpoints are set on the following codes.
#define BP_INST_DATA                      0xEEEEEEEE
#define	BP_ARM11_INST_DATA				  0xEEEEEEEC
#define BP_INST_DATA_HALFWORD             0xEEEE
#define BP_INST_DATA_BYTE                 0xEE

#define ARM11_ARM_BKPT_INST               0xE1200070
#define ARM11_THUMB_BKPT_INST             0xBE00

#define CORTEXM_BKPT_INST                0xBE01

#define CORTEX_ARM_BKPT_INST               0xE1200070
#define CORTEX_THUMB_BKPT_INST             0xBE00
// These definitions are used in the breakpoint resource structure to know what type of 
// watch / break point the resource is being used for.
#define HARDWARE_BP           0x01
#define SOFTWARE_BP           0x02
#define HARDWARE_WP           0x03
#define NOT_USED              0x00

#define THUMB_NO_OF_BYTES     0x2
#define ARM_NO_OF_BYTES       0x4
#define ARM_MODE_MASK         0xFFFFFFDF

#define WORD_ALIGNED_MASK      0xFFFFFFFC
#define HALF_WORD_ALIGNED_MASK 0xFFFFFFFE

//Watchpoint values 
#define WP_VALUE_HALF_ONLY                0x2
#define WP_VALUE_WORD_ONLY                0x4
#define WP_VALUE_WRITE_ONLY               0x1
#define WP_VALUE_READ_ONLY                0x0
#define WP_MASK_BYTE_HALF_AND_WORD        0x6
#define WP_MASK_READ_AND_WRITE            0x1

//Vector Table addreses expressed as an offset from the base address.
//see ulVectorTableBaseAddress defined above.
#define RESET_VECTOR                         0x00
#define UNDEFINED_INSTRUCTION_VECTOR         0x04
#define SOFTWARE_INTERRUPT_VECTOR            0x08
#define PREFETCH_ABORT_VECTOR                0x0C
#define DATA_ABORT_VECTOR                    0x10
#define ADDRESS_EXCEPTION_VECTOR             0x14
#define IRQ_VECTOR                           0x18
#define FIQ_VECTOR                           0x1C
#define ARM9_VECTORCATCH_MASK                0xDF
#define ARM11_VECTORCATCH_MASK               0xDF
#define CORTEXM3_VECTORCATCH_MASK			 0xDF

//Bit masks for vector catch See RDI Section 7.10.6
#define RESET_VECTOR_MASK                    0x01
#define UNDEFINED_INSTRUCTION_VECTOR_MASK    0x02
#define SOFTWARE_INTERRUPT_VECTOR_MASK       0x04
#define PREFETCH_ABORT_VECTOR_MASK           0x08
#define DATA_ABORT_VECTOR_MASK               0x10
#define ADDRESS_EXCEPTION_VECTOR_MASK        0x20
#define IRQ_VECTOR_MASK                      0x40
#define FIQ_VECTOR_MASK                      0x80

#define LOWER_4BIT_MASK        0x0000000F
#define LOWER_8BIT_MASK        0x000000FF
#define LOWER_24BIT_MASK       0x00FFFFFF

#define DISABLE_IRQ_FIQ        0xC0

//Coprocessor instruction creation. Shift position of each constituent.
#define POS_MUSTBE11101110		24
#define POS_OP1					21
#define POS_READ_WRITE			20
#define POS_CRN					16
#define POS_MUSTBE1111			8
#define POS_OP2					5
#define POS_MUSTBE1				4
#define POS_CRM					0
//Error Messages used in MidLayer.

#define ML_ERROR_NO_ERROR               0

//RDI errors mapped to Mdi errors
#define ML_Error_Reset                  1
#define ML_Error_UndefinedInstruction   2
#define ML_Error_SoftwareInterrupt      3
#define ML_Error_PrefetchAbort          4
#define ML_Error_DataAbort              5
#define ML_Error_AddressException       6
#define ML_Error_IRQ                    7
#define ML_Error_FIQ                    8
#define ML_Error_Error                  9
#define ML_Error_BranchThrough0         10

#define ML_Error_NotInitialised         128
#define ML_Error_UnableToInitialise     129
#define ML_Error_WrongByteSex           130
#define ML_Error_UnableToTerminate      131
#define ML_Error_BadInstruction         132
#define ML_Error_IllegalInstruction     133
#define ML_Error_BadCPUStateSetting     134
#define ML_Error_UnknownCoPro           135
#define ML_Error_UnknownCoProState      136
#define ML_Error_BadCoProState          137
#define ML_Error_BadPointType           138
#define ML_Error_UnimplementedType      139
#define ML_Error_BadPointSize           140
#define ML_Error_UnimplementedSize      141
#define ML_Error_NoMorePoints           142
#define ML_Error_BreakpointReached      143
#define ML_Error_WatchpointAccessed     144
#define ML_Error_NoSuchPoint            145
#define ML_Error_ProgramFinishedInStep  146
#define ML_Error_UserInterrupt          147
#define ML_Error_CantSetPoint           148
#define ML_Error_IncompatibleRDILevels  149

#define ML_Error_CantLoadConfig         150
#define ML_Error_BadConfigData          151
#define ML_Error_NoSuchConfig           152
#define ML_Error_BufferFull             153
#define ML_Error_OutOfStore             154
#define ML_Error_NotInDownload          155
#define ML_Error_PointInUse             156
#define ML_Error_BadImageFormat         157
#define ML_Error_TargetRunning          158
#define ML_Error_DeviceWouldNotOpen     159
#define ML_Error_NoSuchHandle           160
#define ML_Error_ConflictingPoint       161
#define ML_Error_TargetBroken           162
#define ML_Error_TargetStopped          163

#define ML_Error_BadValue               164
#define ML_Error_Unset                  165

#define ML_Error_NotAllowedWhilstExecuting 166
#define ML_Error_BadFormat                 167
#define ML_Error_Executing                 168
#define ML_Error_ExecutingLittleEndian     169
#define ML_Error_ExecutingBigEndian        170
#define ML_Error_ReentrantDuringProxy      171
#define ML_Error_Busy                      172

#define ML_Error_NotExecuting              173
#define ML_Error_ProgramFinished           174
#define ML_Error_AlreadyExecuting          175
#define ML_Error_NoSuchCallback            176

#define ML_Error_LinkTimeout            200  
#define ML_Error_OpenTimeout            201  
#define ML_Error_LinkDataError          202  
#define ML_Error_Interrupted            203  
#define ML_Error_NoModules              204  

#define ML_Error_LittleEndian           240
#define ML_Error_BigEndian              241
#define ML_Error_SoftInitialiseError    242

#define ML_Error_ReadOnly               252
#define ML_Error_InsufficientPrivilege  253
#define ML_Error_UnimplementedMessage   254
#define ML_Error_UndefinedMessage       255

#define ML_Error_TargetErrorBase        256
#define ML_Error_RealMonFailed          327

#define ML_Error_NonEditablePoint       328   

#define ML_Error_TargetErrorTop         (0x1000 - 1)

//Opella specific
#define ML_Error_InitTarget_Opella           312

#define ML_Error_CannotWriteDCCHandler       326
#define ML_Error_DataAbort_Writing_Cache_Clean  344

#define ML_Error_NoRTCKSupport               346

//Mdi error codes specific to opella xd
#define ML_ERROR_INCORRECT_USB_DRIVER           350
#define ML_ERROR_FAILED_TO_PROGRAM_FPGA         351
#define ML_ERROR_NO_USB_DEVICE_CONNECTED        352
#define ML_ERROR_FAILED_TO_FIND_FPGA_FILE       353
#define ML_ERROR_FAILED_TO_FIND_DISKWARE_FILE   354
#define ML_ERROR_INCORRECT_DISKWARE_FILE        355
#define ML_ERROR_FAILED_SEND_USB_PACKET         356
#define ML_ERROR_FAILED_RECEIVE_USB_PACKET      357
#define ML_ERROR_FAILED_TO_WRITE_USB_MEMORY     358
#define ML_ERROR_TARGET_RESET                   359
#define ML_ERROR_OPELLA_FAILURE                 360
#define ML_ERROR_INCORRECT_PIN_NO               361
#define ML_ERROR_FAILED_TO_WRITE_REGISTRY_KEY   362
#define ML_ERROR_INVALID_SERIAL_NO              363
#define ML_ERROR_FPGA_NOT_CONFIGURED			364
#define ML_ERROR_FPGA_CONFIG_NOT_READY		    365
#define ML_ERROR_INVALID_CONFIG                 366
#define ML_ERROR_DEVICE_ALREADY_IN_USE          367
#define ML_ERROR_INVALID_CORESIGHT_CONFIG		368


// This is used for Generic Error handling to improve code readability.
#define JMP_ERR(EXCEPTION_DEST)         if (ErrRet != ML_ERROR_NO_ERROR) \
                                          {                                \
                                             ErrRet = RDI_Convert_MidLayerErr_ToRDIErr (ErrRet); \
                                             if (ErrRet != RDIError_NoError) \
                                             goto EXCEPTION_DEST; \
                                          } 

#define MAX_NUM_BREAKPOINTS    0xa00
#define MAX_NUM_WATCHPOINTS    10
#define ARM11_MAX_BP_RESOURCES 6

#define FORCE_SCANCHAIN_SELECT      0xFFFFFFFF

//tyVectorCatchConfig array locations
#define RESET_VECTOR_LOCATION                0x00
#define SEMIHOSTING_VECTOR_LOCATION          0x02
#define FIQ_VECTOR_LOCATION                  0x07
#define ALL_VECTORS_SET_LOCATION             0x08

#define CATCH_ALL_VECTORS_BOUND              0xFFFFFFE3
#define ALL_VECTORS_SET_MASK                 0xFF

#define MAX_MEMORY_SIZE 0x80

// error codes for ARM midlayer
#define ERR_ML_ARM_NO_ERROR                                 0           // no error occured
#define ERR_ML_ARM_INVALID_HANDLE                           1           // invalid handle with parameters
#define ERR_ML_ARM_ALREADY_INITIALIZED                      2           // connection already initialized
#define ERR_ML_ARM_TARGET_NOT_SUPPORTED                     3           // functionality not supported for selected target
#define ERR_ML_ARM_USB_DRIVER_ERROR                         4           // driver internal error
#define ERR_ML_ARM_PROBE_NOT_AVAILABLE                      5           // cannot open connection with probe
#define ERR_ML_ARM_PROBE_INVALID_TPA                        6           // no or invalid TPA used
#define ERR_ML_ARM_PROBE_CONFIG_ERROR                       7           // cannot configure probe
#define ERR_ML_ARM_RTCK_TIMEOUT_EXPIRED                     10          // RTCK timeout expired
#define ERR_ML_ARM_INVALID_FREQUENCY                        50          // invalid frequency selection
#define ERR_ML_ARM_ANGEL_BLASTING_FAILED                    100         // blasting to ARMangel failed
#define ERR_ML_ARM_ANGEL_INVALID_EXTCMD_RESPONSE            101         // invalid response to extended command
#define ERR_ML_ARM_ANGEL_INVALID_PLL_FREQUENCY              102         // invalid PLL frequency
#define ERR_ML_ARM_TEST_SCANCHAIN_FAILED                    200         // failing test scanchain
#define ERR_ML_ARM_DETECT_SCANCHAIN_FAILED                  201         // detection of scanchain structure failed
#define ERR_ML_ARM_INVALID_SCANCHAIN_STRUCTURE              202         // invalid scanchain structure
#define ERR_ML_ARM_TARGET_RESET_DETECTED                    300         // target reset occured
#define ERR_ML_ARM_DEBUG_ACCESS                             400         // access to ARC debug failed

/* Definition of Cortex-M3 */ /* Added By Jeenus */
/* Number of comparator resourses of Cortex-M3*/
#define MAX_NUM_COMPARATORS			8
#define MAX_NUM_WATCHPOINT_COMP		4

/* To check the address is half word alligned */
#define HALF_WORD_ALLIGNED			2
#define WORD_ALLIGNED				4

/* Flash Patch Comparator Register format. */
#define FPC_REPLACE							    (0x3 << 30)
#define	FPC_REPLACE_REMAP					    (0x0 << 30)
#define	FPC_REPLACE_BKPT_LOW_HALF_WORD			(0x1 << 30)
#define	FPC_REPLACE_BKPT_HIGH_HALF_WORD			(0x2 << 30)
#define	FPC_REPLACE_BKPT_LOW_HIGH_HALF_WORD		(0x3 << 30)
#define FPC_COMP							    (0x7FFFFFF << 2)
#define FPC_ENABLE							    (0x1 << 0)
#define FPC_DISABLE							    (0x0 << 0)

/* Flag to indicate the Comparator is used or not */
#define FPC_COMP_USED						1
#define FPC_COMP_NOT_USED					0

/* Watchpoint resource used status */
#define WP_RES_NOT_USED						0
#define WP_RES_USED							1


#define WATCHPOINT_RECOURSE_0	0
#define WATCHPOINT_RECOURSE_1	1
#define WATCHPOINT_RECOURSE_2	2
#define WATCHPOINT_RECOURSE_3	3

#define SAME_RES_WP_BP 1
#define DIFF_RES_WP_BP 2

/* DWT Function register format */ 
#define DWT_FUNCTION		            (0xF << 0)
#define	DWT_FUNCTION_DISABLE			(0x0 << 0)
#define	DWT_FUNCTION_WP_READ			(0x5 << 0)
#define DWT_FUNCTION_WP_WRITE			(0x6 << 0)
#define	DWT_FUNCTION_WP_READ_WRITE		(0x7 << 0)
#define DWT_DATAVSIZE		            (0x3 << 10)
#define	DWT_DATAVSIZE_BYTE				(0x0 << 10)
#define DWT_DATAVSIZE_HALF_WORD			(0x1 << 10)
#define	DWT_DATAVSIZE_WORD				(0x2 << 10)
#define	DWT_DATAVMATCH		            (0x1 << 8)

#define DATAVADDR0_SHIFT	12
#define DATAVADDR1_SHIFT	16


#define SET_WP_READ_ACCESS					0x1
#define SET_WP_WRITE_ACCESS					0x2
#define SET_WP_READ_WRITE_ACCESS			(SET_WP_READ_ACCESS | SET_WP_WRITE_ACCESS)

#define BYTE_ALLIGN			0xFFFFFFFF
#define HALF_WORD_ALLIGN	0xFFFFFFF0
#define WORD_ALLIGN			0xFFFFFF00

#define BYTE_MASK			0x1
#define HALF_WORD_MASK		0x2
#define WORD_MASK			0x3

/* Vector Catch definitions which from the Pathfinder */
#define VECTOR_CATCH_RESET		(0x1 << 0)
#define VECTOR_CATCH_UNDEFINED	(0x1 << 1)
#define VECTOR_CATCH_SOFTWARE	(0x1 << 2)
#define VECTOR_CATCH_PREFETCH	(0x1 << 3)
#define VECTOR_CATCH_DATAABORT	(0x1 << 4)
#define VECTOR_CATCH_ADDRESS	(0x1 << 5)
#define VECTOR_CATCH_IRQ		(0x1 << 6)
#define VECTOR_CATCH_FIQ		(0x1 << 7)

/* Vector catch for Debug Trap on interrupt/exception service errors. This is used for 
setting the semi-hosting in the Cortex-M3 */
#define VECTOR_CATCH_MASK_ARM_V7M	(0x7F1 << 0)
#define VC_CORE_RESET		(0x1 << 0)
#define VC_MMERR			(0x1 << 4)
#define VC_NOCPERR			(0x1 << 5)
#define VC_CHKERR			(0x1 << 6)
#define VC_STATERR			(0x1 << 7)
#define VC_BUSERR			(0x1 << 8)
#define VC_INTERR			(0x1 << 9)
#define VC_HARDERR			(0x1 << 10)

#define BKPT_AB				0xBEAB
/* Vector catch is set using Debug Exception and Monitor Control Register */
#define DEMCR_ADDR			0xE000EDFC
#define DFSR_ADDR			0xE000ED30
#define AIRCR_ADDR			0xE000ED0C /* Application Interrupt and Reset Control Register */
#define DMCSR_TRCENA		(0x1 << 24)
#define DHCSR_ADDR			0xE000EDF0
#define S_RESET_ST			(0x1 << 25)

/* Application Interrupt and Reset Control Register */
#define AIRCR_VECTKEY		(0x05FA << 16)
#define AIRCR_SYSRESETREQ	(0x1 << 2)
#define AIRCR_VECTRESET		(0x1 << 0)

/* CPU ID Base Register Format and address */
#define CPUID_BASE_REG		0xE000ED00
#define		CPUID_IMPLEMENTER		(0xFF << 24)
#define		CPUID_VARIANT			(0xF << 20)
#define		CPUID_ARCHITECTURE		(0xF << 16)
#define			ARCH_4				(0x1 << 16)
#define			ARCH_4T				(0x2 << 16)
#define			ARCH_5				(0x3 << 16)
#define			ARCH_5T				(0x4 << 16)
#define			ARCH_5TE			(0x5 << 16)
#define			ARCH_5TEJ			(0x6 << 16)
#define			ARCH_6				(0x7 << 16)
#define			ARCH_6M				(0xC << 16)
#define			ARCH_CPUID_DEPE		(0xF << 16)
#define		CPUID_PRIMARY_PART_NO	(0xCFF << 4)
#define			CPUID_PARTNO_FAMILY		(0x3 << 14)
#define			CPUID_PARTNO_VERSION	(0x3 << 12)
#define			CPUID_PARTNO_ARCH		(0x3 << 8)
#define				ARCH_v7M				(0x2 << 8)
#define				ARCH_v7R				(0x1 << 8)
#define				ARCH_v7A				(0x0 << 8)
#define			CPUID_PARTNO_MEMBER		(0x3 << 4)
#define		CPUID_REVISION			(0xF << 0)


#define EXTERNAL_DBG_REQ	(0x1 << 4)
#define VECTOR_CATCH_OCCUR	(0x1 << 3)
#define DWTTRAP_MATCH		(0x1 << 2)
#define BKPT_FLAG			(0x1 << 1)
#define HALT_REQ_FLAG		(0x1 << 0)

/* CORTEX-A8 Definitions */
#define CORTEXA8_DBG_ENTRY				(0xF << 2)
#define CORTEXA8_BREAKPOINT				(0x1 << 2)
#define CORTEXA8_ASYNC_WATCHPOINT		(0x2 << 2)
#define CORTEXA8_BKPT					(0x3 << 2)
#define CORTEXA8_HALTING_DBG_EVENT		(0x4 << 2)
#define CORTEXA8_VECTOR_CATCH			(0x5 << 2)
#define CORTEXA8_OS_UNLOCK				(0x8 << 2)
#define CORTEXA8_SYNC_WATCHPOINT		(0xA << 2)

#define CLEAR_DFSR_REG		0x1F << 0 /* Writing a 1 to a bit clears that bit */

#define CORE_ONLY			0x1
#define CORE_PERIPHERAL		0x2

/* Cortex-A8 definitions for breakpoint Control register */
#define BCR_BREAK_ENABLE		(0x1 << 0)
#define BCR_BREAK_DISABLE		(0x0 << 0)
#define BCR_ANY_ACCESS_CONTROL	(0x3 << 1)
#define BCR_BREAK_AT_BYTE_0		(0x3 << 5)
#define BCR_BREAK_AT_BYTE_2		(0xC << 5)
#define BCR_BREAK_AT_BYTE_ANY	(0xF << 5)

/* Cortex-A8 definitions for Watchpoint Control Register */
#define WCR_WATCH_ENABLE			(0x1 << 0)
#define WCR_WATCH_DISABLE			(0x0 << 0)
#define WCR_ANY_PRIVILEGE_ACCESS	(0x3 << 1)
#define WCR_LOAD_ACCESS				(0x1 << 3)
#define WCR_STORE_ACCESS			(0x2 << 3)
#define WCR_LOAD_STORE_ACCESS		(0x3 << 3)
#define WCR_WATCH_AT_BYTE_0			(0x1 << 5)
#define WCR_WATCH_AT_BYTE_1			(0x2 << 5)
#define WCR_WATCH_AT_BYTE_2			(0x4 << 5)
#define WCR_WATCH_AT_BYTE_3			(0x8 << 5)
#define WCR_WATCH_AT_ANY			(WCR_WATCH_AT_BYTE_0 | WCR_WATCH_AT_BYTE_1 | WCR_WATCH_AT_BYTE_2 | WCR_WATCH_AT_BYTE_3)
#define WCR_BOTH_SECURE_STATE	    (0x0 << 14)

/* Cortex-A8 VCR bits */
#define CORTEXA8_VCR_FIQ_N_SECURE               (0x00000001 << 31)      /* Vector catch enable, FIQ in Non secure state                   */
#define CORTEXA8_VCR_IRQ_N_SECURE               (0x00000001 << 30)      /* Vector catch enable, IRQ in Non secure state                   */
#define CORTEXA8_VCR_DATA_ABT_N_SECURE          (0x00000001 << 28)      /* Vector catch enable, Data Abort in Non secure state            */
#define CORTEXA8_VCR_PREFETCH_ABT_N_SECURE      (0x00000001 << 27)      /* Vector catch enable, Prefetch Abort in Non secure state         */
#define CORTEXA8_VCR_SVC_N_SECURE               (0x00000001 << 26)      /* Vector catch enable, SVC in Non secure state                   */
#define CORTEXA8_VCR_UND_INST_N_SECURE          (0x00000001 << 25)      /* Vector catch enable, Undefined instruction in Non secure state */

#define CORTEXA8_VCR_FIQ_SECURE_MON             (0x00000001 << 15)      /* Vector catch enable, FIQ in Secure state                      */
#define CORTEXA8_VCR_IRQ_SECURE_MON             (0x00000001 << 14)      /* Vector catch enable, IRQ in Secure state                      */
#define CORTEXA8_VCR_DATA_ABT_SECURE_MON        (0x00000001 << 12)      /* Vector catch enable, Data Abort in Secure state               */
#define CORTEXA8_VCR_PREFETCH_ABT_SECURE_MON    (0x00000001 << 11)      /* Vector catch enable, Prefetch Abort in Secure state           */
#define CORTEXA8_VCR_SMC_SECURE_MON             (0x00000001 << 10)      /* Vector catch enable, SMC in Secure state                      */

#define CORTEXA8_VCR_FIQ_SECURE                 (0x00000001 << 7)       /* Vector catch enable, FIQ in Secure state                      */
#define CORTEXA8_VCR_IRQ_SECURE                 (0x00000001 << 6)       /* Vector catch enable, IRQ in Secure state                      */
#define CORTEXA8_VCR_DATA_ABT_SECURE            (0x00000001 << 4)       /* Vector catch enable, Data Abort in Secure state               */
#define CORTEXA8_VCR_PREFETCH_ABT_SECURE        (0x00000001 << 3)       /* Vector catch enable, Prefetch Abort in Secure state           */
#define CORTEXA8_VCR_SVC_SECURE                 (0x00000001 << 2)       /* Vector catch enable, SVC in Secure state                      */
#define CORTEXA8_VCR_UND_INST_SECURE            (0x00000001 << 1)       /* Vector catch enable, Undefined instruction in Secure state    */
#define CORTEXA8_VCR_RESET                      (0x00000001 << 0)       /* Vector catch enable, Reset                                    */

/* Cortex-A8 Definitions for DIDR register */
#define DIDR_BRP	(0xF << 24)
#define DIDR_WRP	(0xF << 28)

/* Coproessor CP15 Instruction */
#define  ARMv7A_MCR_OPCODE 0xEE000F10
#define  ARMv7A_MRC_OPCODE 0xEE100F10

#define WATCH_BYTE_ACCESS			0x1
#define WATCH_HALF_WORD_ACCEES		0x2
#define WATCH_WORD_ACCESS			0x4
#define WATCH_ANY_ACCESS			(WATCH_BYTE_ACCESS | WATCH_HALF_WORD_ACCEES | WATCH_WORD_ACCESS)

#define TI_DAPPCCONTROL_REG_ADDR	0x5401d030
#define TI_DAPPCCONTROL_DAPCONNECT	( 0x1 << 13 )

typedef void (*TyOpenOpellaXDCallback)(unsigned char);

//This structure holds the data passed between the Execute function and 
//the handle Semihosting function.
typedef struct
{
	unsigned long     SemiHostingType; 
	unsigned long     arg1;
	unsigned long     arg2; 
	unsigned long     arg3; 
	unsigned long     arg4;
	unsigned long     ulStatus; 
	boolean           bRestartExecution; 
	TXRXCauseOfBreak  tyCauseOfBreak;
	boolean           bSemiHostDetected;
	unsigned long     ulLinkRegisterValue;
	unsigned long     ulSPSRValue;
} TYSemiHostStruct;


//Breakpoint configuration.
typedef struct
{
   unsigned short usBreakpointId;
   unsigned long  ulPreviousInstructionData;
   unsigned long  ulAddress;
   unsigned long  ulBPType;
   unsigned long  ulDataType;
   unsigned long  ulBound;
   unsigned char  ucBPIDNo;
   unsigned char  ucBPResourceNo1; //for ARM11 more than 2 resources are there. Want to
   unsigned char  ucBPResourceNo2; //identify which resource is allocated for range setting 
   unsigned char  ucNoOfBPsAtAddress;
   boolean        bWatchPoint;
} TyBreakpointStructure;

typedef struct
{
  TyBreakpointStructure BreakpointStructures[MAX_NUM_BREAKPOINTS];

} TyBreakPointArray;

//Watchpoint structure - This want to be used for ARM11 only
//For ARM9 & ARM7 TyBreakpointStructure is used for watchpoint also since it use the same resource
typedef struct
{
	unsigned short usWatchpointId;
	unsigned long  ulAddress;
	unsigned long  ulWPType;
	unsigned long  ulDataType;
	unsigned long  ulBound;
	unsigned char  ucWPIDNo;
	unsigned char  ucNoOfWPsAtAddress;
} TyWatchpointStructure;

typedef struct
{
	TyWatchpointStructure WatchpointStructures[MAX_NUM_WATCHPOINTS];
	
} TyWatchPointArray;

typedef struct
{
   unsigned long ulBreakpointHandle;
   unsigned long ulAddress;
   unsigned char ucBPType;
   boolean       bValidBP;
} TyVectorCatchStructure;

typedef struct
{
   unsigned long ulWP0AddressValue;
   unsigned long ulWP0AddressMask;
   unsigned long ulWP0DataValue;
   unsigned long ulWP0DataMask;
   unsigned long ulWP0CtrlValue;
   unsigned long ulWP0CtrlMask;
   unsigned long ulWP1AddressValue;
   unsigned long ulWP1AddressMask;
   unsigned long ulWP1DataValue;
   unsigned long ulWP1DataMask;
   unsigned long ulWP1CtrlValue;
   unsigned long ulWP1CtrlMask;

   unsigned char ucWP0Used;
   unsigned char ucWP1Used;
   boolean       bWP0Locked;
   boolean       bWP1Locked;
   boolean       bUseOnlyHWBreakPoints;
} TyBPResourceStructure;

typedef struct
{
	unsigned long ulBP0AddressValue;
	unsigned long ulBP0CtrlValue;
	unsigned long ulBP1AddressValue;
	unsigned long ulBP1CtrlValue;
	unsigned long ulBP2AddressValue;
	unsigned long ulBP2CtrlValue;
	unsigned long ulBP3AddressValue;
	unsigned long ulBP3CtrlValue;
	unsigned long ulBP4AddressValue;
	unsigned long ulBP4CtrlValue;
	unsigned long ulBP5AddressValue;
	unsigned long ulBP5CtrlValue;
	unsigned char ucBP0Used;
	unsigned char ucBP1Used;
	unsigned char ucBP2Used;
	unsigned char ucBP3Used;
	unsigned char ucBP4Used;
	unsigned char ucBP5Used;

    boolean bBP0Locked;
    boolean bBP1Locked;
    boolean bBP2Locked;
    boolean bBP3Locked;
    boolean bBP4Locked;
    boolean bBP5Locked;

    boolean       bUseOnlyHWBreakPoints;
} TyBPResourceARM11Structure;

typedef struct
{
	unsigned long ulWP0AddressValue;
	unsigned long ulWP0CtrlValue;
	unsigned long ulWP1AddressValue;
	unsigned long ulWP1CtrlValue;
	unsigned char ucWP0Used;
	unsigned char ucWP1Used;

    boolean bWP0Locked;
    boolean bWP1Locked;
	
} TyWPResourceARM11Structure;

/* Added for Cortex-M support */
typedef struct
{
	unsigned char  ucHardBPResource[MAX_NUM_COMPARATORS]; 
	unsigned char  ucWatchBPResource[MAX_NUM_WATCHPOINT_COMP];
	TyBreakpointStructure BreakpointStructures[MAX_NUM_BREAKPOINTS];	
} TyCMBreakPointArray;

/* Added for Hardware breakpoint structure */
typedef struct
{
	unsigned long ulNumBreakpoint;
	unsigned long ulFlashPatchCompVal[MAX_NUM_COMPARATORS];	
    boolean       bUseOnlyHWBreakPoints;
} TyBPResourceCortexMStructure;

/* Added for Watchpoint structure. */ 
/* This structure is used for sending the watchpoint details to the diskware. */
typedef struct
{
	unsigned long ulComp;
	unsigned long ulMask;
	unsigned long ulFunction;
} TyCortexMWpResStruct;

typedef struct
{
	unsigned long		ulNumWatchpoint;
	TyCortexMWpResStruct tyCortexMWpResStruct[MAX_NUM_WATCHPOINT_COMP];
} TyCortexMWpResStructArray;

/* Cortex-A Definitions */
typedef struct  
{
	unsigned long ulBreakValueReg;
	unsigned long ulBreakControlReg;
} TyArmv7ABreakpointRes;

typedef struct
{
	unsigned long			ulNumBreakpoint;
	TyArmv7ABreakpointRes	tyArmv7ABreakpointRes[MAX_NUM_COMPARATORS];
	boolean					bUseOnlyHWBreakPoints;
}TyArmv7ABreakpointResArray;

typedef struct  
{
	unsigned long ulWatchPointValueReg;
	unsigned long ulWatchPointControlReg;
} TyArmv7AWatchpointRes;

typedef struct
{
	unsigned long			ulNumWatchPoint;
	TyArmv7AWatchpointRes	tyArmv7AulWatchPointRes[MAX_NUM_COMPARATORS];
}TyArmv7AWatchpointResArray;

#ifndef __LINUX
typedef struct _TyRDIDiagInfo {
   // general device information
   char pszSerialNumber[MAX_SERIAL_NUMBER_LEN];
   char pszManufDate[INFO_VERSION_DATE_LEN];
   char pszBoardRevision[INFO_VERSION_DATE_LEN];
   // DLL version
   char pszDLLVersion[INFO_VERSION_DATE_LEN];
   // USBDriver version
   char pszUSBDriverVersion[INFO_VERSION_DATE_LEN];
   // firmware information
   char pszFwVersion[INFO_VERSION_DATE_LEN];
   char pszFwDate[INFO_VERSION_DATE_LEN];
   // diskware information
   unsigned char bDwRunning;
   char pszDwVersion[INFO_VERSION_DATE_LEN];
   char pszDwDate[INFO_VERSION_DATE_LEN];
   // FPGA information
   char pszFpgawareVersion[INFO_VERSION_DATE_LEN];
   char pszFpgawareDate[INFO_VERSION_DATE_LEN];
   // TPA information
   TyTpaType tyTpaConnected;
   char pszTpaName[16];
   char pszTpaRevision[INFO_VERSION_DATE_LEN];
} TyRDIDiagInfo;

typedef struct _TyRDIHardResetDiagInfo {
   unsigned char ucTgtAssertResetStatus;
   unsigned char ucTgtDeassertResetStatus;
} TyRDIHardResetDiagInfo;

typedef struct _TyRDIOnTargetDiagInfo {
    unsigned long ulDbgCoreNumber;
	char pszCoreType[30];
	unsigned long ulIDCode;
	unsigned char szRegCheck;
	char szTpaLBResult;
	double szTgtVoltage;
	unsigned long ulTotCores;
	unsigned long ulIRLen;
	unsigned long ulCoreID;
	unsigned long ulCpReg1;
	char pszArch[8];
	char pszDCache[8];
	char pszICache[8];
	unsigned long ulPartno;
	char szCoreMake;
	char szArm7;
	unsigned int uiTargetTAPNo;
   TyTpaType tyTpaConnected;
   TyJtagScanChain *ptyJtagScanChain;
   enum JTAGClockFreqMode tyJTAGClockFreqMode;
   TyRDIHardResetDiagInfo tyRDIHardResetDiagInfo;
   unsigned long   ulFreqVal;
   unsigned long   ulCoresightBaseAddr;
   unsigned long   ulDbgApIndex;
   unsigned long   ulMemApIndex;
   unsigned long   ulJtagApIndex;
   boolean         bCoresightCompliant;
   boolean         bTICore;
   unsigned long   ulTISubPortNum;
   boolean		   bDapPcSupport;
} TyRDIOnTargetDiagInfo;

typedef struct
{
	enum TyCompType tyCompType;
	char szCompString[256];
}TyComponent;


#endif

/*Functions to be exported*/
extern "C" void ML_CheckOpellaXDInstance(char pszSerialNumber[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN], unsigned short *usNumberOfDevices);
/* Function prototypes */
typedef int (*pInitUserRegisterSettings)();
int InitUserRegisterSettings();
TyError ML_Get_MultiCoreInfo(unsigned long *pulIRLengthArray);
void ML_StoreFullPathForDLL(char *pszPathName);
TyError ML_UpdateOpellaXDFirmware(TyOpenOpellaXDCallback tyCallback, const char *pszSerialNumber);
TyError ML_OpenOpellaXD(TyOpenOpellaXDCallback tyCallback, const char *pszSerialNumber);
TyError ML_ResetCheck (TyError tyOriginalErrorMessage);
TyError ML_Initialise_Debugger (TyTargetConfig* ptyTargetConfig);//, TyCoreConfig *ptyCoreConfig);
TyError ML_SetJTAGFrequency(double ulFreqKHz);
TyError ML_ProcessCP15Register(unsigned int uiOperation, unsigned int uiCRn,
							   unsigned int uiCRm, unsigned int uiOp1, 
							   unsigned int uiOp2, unsigned long *pulRegValue);
TyError ML_ReadCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex,
						  unsigned int *puiRegValue);
TyError ML_WriteCPRegister(unsigned int uiCpNum, unsigned int uiRegIndex,
						  unsigned int uiValue);
TyError ML_InfoProc (unsigned long  ulType, unsigned long  *pulArg1, unsigned long  *pulArg2);
TyError ML_Select_Scan_Chain (unsigned char ulScanChain, unsigned char ucNumIntests);
TyError ML_ReadICEBreaker (unsigned long  ulAddress, unsigned long *pulReadValue);
TyError ML_WriteICEBreaker (unsigned long  ulAddress, unsigned long  ulWriteValue);
TyError ML_Disable_EmbeddedICERT (void);
TyError ML_Enable_EmbeddedICERT (void);
TyError ML_SetupICEBreakerModule (void);
static TyError ML_SetHardwareWatchPoint (unsigned long ulAddress, 
                                         unsigned long ulType, 
                                         unsigned long ulDataType, 
                                         unsigned long ulBound, 
                                         unsigned long ulResourceNo);
TyError ML_SetWatchProc (unsigned long   ulAddress,
                         unsigned long   ulType,
                         unsigned long   ulDatatype,
                         unsigned long   ulBound,
                         RDI_PointHandle *phPointHandle);
TyError ML_SetupWatchpoints (void);
TyError ML_WaitForSystemSpeedCompletion(void);
static TyError ML_DisableWatchpoints (void);
TyError ML_ClearWatchProc(RDI_PointHandle hHandle);
TyError ML_AllocateMemForBreakpoints (void);
TyError ML_DeAllocateMemForBreakpoints (void);
TyError ML_RemoveAllBreakpoints (void);
TyError ML_ClearBreakProc(RDI_PointHandle hPointHandle);
static void ML_ClearHardwareResource (unsigned long ulResourceNo);
TyError ML_DebugRequest(unsigned char ucDebugReq);
TyError ML_InitTarget(unsigned long ulTargetType);
TyError ML_DebugStartup (boolean *pbExecuting);
TyError ML_ResetTAPController(void);
TyError ML_ResetTAPUsingTMS(void);
TyError ML_SetupProcType(TXRXProcTypes tyProcType);
TyError ML_ReadByteUsingWordAccess (unsigned long ulAddress, unsigned char *pucData);
TyError ML_ReadWord (unsigned long ulAddress, unsigned long *pulReadValue);
TyError ML_ReadMultipleWords (unsigned long ulAddress, unsigned long *ulData, unsigned long ulWords);
TyError ML_ReadMultipleBlocks (unsigned long ulAddress, unsigned long *ulData, unsigned long ulBlocks);
TyError ML_ReadFromTargetMemory(unsigned long  ulSource, void *pulDest, unsigned long *pulBytes, RDI_AccessType tyType);
TyError ML_WriteWord (unsigned long ulAddress, unsigned long ulData);
TyError ML_WriteByte (unsigned long ulAddress, unsigned long ulData);
TyError ML_WriteByteUsingWordAccess (unsigned long ulAddress, unsigned long ulData);
TyError ML_WriteMultipleWords (unsigned long ulAddress, unsigned long *pulData, unsigned long ulWords);
TyError ML_WriteMultipleBlocks (unsigned long ulAddress, unsigned long *pulData, unsigned long ulBlocks);
TyError ML_WriteToTargetMemory(void *pulSource, unsigned long ulDest, unsigned long *pulBytes, RDI_AccessType tyType);
TyError ML_WriteProc(void *pulSource, unsigned long ulDest, unsigned long *pulBytes, RDI_AccessType tyType,boolean bCodeDownload, boolean bSkipBreakpoints);
TyError ML_CheckForDataAbort (void);
boolean ML_BreakpointAlreadySet(unsigned long ulAddress, RDI_PointHandle *pBPHandle);
static void ML_FindAndReplaceAllBPsForWrite(unsigned long ulStartAddress, unsigned char *pucDataValues, unsigned long ulDataCount);
static void ML_FindAndReplaceAllBPsforRead(unsigned long ulStartAddress, unsigned char *pucDataValues, unsigned long ulDataCount);
void ML_GetUseOnlyHardwareBreakPoints (unsigned long *pulUseHWBreakpoints);
void ML_SetUseOnlyHardwareBreakPoints (unsigned long ulUseHWBreakpoints);
static TyError ML_SetHardwareBreakPoint (unsigned long ulAddress, boolean bARMBP, unsigned long ulResourceNo, unsigned long ulBound);
static TyError ML_SetHardwareBreakPointRange (unsigned long ulAddress, boolean bARMBP, unsigned long ulBound);
static TyError ML_SetSoftwareBP (unsigned long ulAddress, boolean bARMBP);
static void ML_SetSoftwareBPResource (unsigned long ulResourceNo);
boolean ML_Executing (void);
static TyError ML_ARMMode (boolean *pbOffsetNeeded);
static TyError ML_ThumbMode (void);
TyError ML_PerformARMNops (unsigned long ulNoOfNops);
TyError ML_StatusProc (boolean *pbExecuting, TXRXCauseOfBreak *ptyCauseOfBreak, RDI_PointHandle *phPointHandle, pInitUserRegisterSettings pUserRegHandle);
TyError ML_ReadProc (unsigned long  ulSource, void *pulDest, unsigned long *pulBytes, RDI_AccessType tyType, boolean bCheckForDataAbort);
TyError ML_SetBreakProc (unsigned long ulAddress, unsigned long ulBPType, unsigned long ulBound, RDI_PointHandle  *phPointHandle);
TyError ML_CPUReadProc (unsigned long ulMode, unsigned long ulMask, unsigned long *pulState);
TyError ML_CPUWriteProc (unsigned long ulMode, unsigned long ulMask, unsigned long *pulState);
TyError ML_ARM_SetupExecuteProc(unsigned long ulWP0AddressValue, 
                                unsigned long ulWP0AddressMask, 
                                unsigned long ulWP0DataValue, 
                                unsigned long ulWP0DataMask, 
                                unsigned long ulWP0CtrlValue, 
                                unsigned long ulWP0CtrlMask, 
                                unsigned long ulWP1AddressValue, 
                                unsigned long ulWP1AddressMask, 
                                unsigned long ulWP1DataValue, 
                                unsigned long ulWP1DataMask, 
                                unsigned long ulWP1CtrlValue, 
                                unsigned long ulWP1CtrlMask);
TyError ML_ExecuteProc (void);
TyError ML_StepProc  (void);
TyError ML_StopCoreAtZero (void);
TyError ML_ReadRegisters (unsigned long *pulRegisters);
TyError ML_WriteRegisters (unsigned long *pulRegisters);
TyError ML_RestoreRegisters (void);
TyError ML_SetupVectorCatch (unsigned long *pulVectorCatchConfig);
TyError ML_SetSafeNonVectorAddress (unsigned long ulSafeAddress);
unsigned long ML_CheckSemiHostState (void);
void ML_GetProcessorInfo (boolean *pbThumbSupport, boolean *pbCommsChannel);
TyError ML_Arm_Restart(void);
boolean ML_HasResetOccurred (void);
TyError ML_CheckDCC  (unsigned long    *pulDCCReadData,
                      boolean          *pbDataToRead,
                      unsigned long    *pulDCCWriteData,
							 boolean          *pbDataWritten);
TyError  ML_Initialise_SemiHosting(void);
TyError ML_LoadDCC_SH_Handler(void);
TyError ML_SemiHostGo (unsigned long ulLinkRegisterValue, unsigned long ulSPSRValue);
TyError ML_ClearSemihostingBP (void);
TyError ML_SetSemihostingBP (void);
TyError ML_CheckForSemihosting(TYSemiHostStruct *tySemiHostStruct);
static unsigned long ML_ConvertModeMasktoRegArrayMask (unsigned long ulMode);
TyError ML_PointInquiryProc(unsigned long  *pulAddress,
                            unsigned long  ulType,
                            unsigned long  ulDataType,
                            unsigned long  *pulBound);
TyError ML_BreakpointInquiry (unsigned long *pulArg1,unsigned long *pulArg2);
TyError ML_WatchpointInquiry (unsigned long *pulArg1,unsigned long *pulArg2);
void  ML_SetLockedPoints (unsigned long ulLockedPoints);
void  ML_GetLockedPoints (unsigned long *ulLockedPoints);
void ML_SetTopOfMemory (unsigned long ulMemorySize);
void ML_GetSafeVectorAddress (unsigned long *pulSafeAddress);
boolean ML_QueryMovableVector (void);
TyError ML_GetVectorAddress (unsigned long *ulVectorAddress);
TyError ML_GetDeviceInfo ( TyDeviceInfo *tyDeviceInfo );
TyError ML_CloseOpellaXD (void);
void ML_Delay (unsigned long ulDelay);
TyError ML_Convert_DrvLayerErr_ToMidLayerErr (TyError tyDRVLayerErr);
const TCHAR* GetRDIDLLVersion(void);
const TCHAR* GetFirmwareInfo(void);
#ifndef __LINUX
TyError ML_Initialise_Debugger_Third_party ( TyDevHandle tyDevHandle, double ulFreqKHz, double dTargetVoltage, TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo);//, TyCoreConfig *ptyCoreConfig);//added by us
TyError PerformRDIDiagnostics(char* pszSerialNumber,TyRDIDiagInfo *tyRdiDiagInfo);
TyError PerformRDIDiagnosticsOnTarget(char* pszSerialNumber,TyRDIOnTargetDiagInfo *tyRdiOnTargetDiagInfo);
TyError PerformReadROMTable(char* pszSerialNumber, 
							unsigned long ulCore,
							TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo,
							TyCoresightComp* ptyCoresightComp);
TyError PerformTargetHardReset(char* pszSerialNumber,TyRDIOnTargetDiagInfo *ptyRDIOnTargetDiagInfo);
void GetDeviceType(unsigned long *pulDeviceType);
#endif
unsigned char TestTargetLoopBack(unsigned int *puiNumberOfTAPs);
unsigned char CompareUlongs_Tap(unsigned long *pulUlongs1, unsigned long *pulUlongs2, unsigned int uiNumberOfBits);//added newly..
void ShiftUlongArray_Tap(unsigned long *pulUlongArray, unsigned int uiNumberOfUlongs,
                     unsigned int uiNumberOfBits, unsigned char bShiftToRight);
TyError ML_ARM7StepProc(void);
TyError ML_ARM9StepProc();

// additional functions for JTAG console
int ML_ScanIR( unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut);
int ML_ScanDR( unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut);
int ML_ScanIRScanDR(unsigned long *pulLength, unsigned long *pulDataIn, unsigned long *pulInBuffer);
void SleepFunc(unsigned long ulDelay);

//ARM 11 function declerations
TyError ML_LeaveDebugState(void);
TyError ML_EnterDebugState(void);
TyError ML_ReadDSCR(unsigned long*);
TyError ML_ReadCPSR (unsigned long *);
TyError ML_ReadCDCR(unsigned long *);
TyError ML_SetupBPRegsARM11(void);
TyError ML_SetupWPRegsARM11(void);
TyError ML_WriteVectorCacheReg (unsigned long  ulWriteValue);
TyError ML_ArmInstnExecEnable (void);
static TyError ML_SetARM11HardwareBreakPoint (unsigned long ulAddress, 
                                              boolean       bARMBP, 
                                              unsigned long ulResourceNo);
static TyError ML_SetARM11HardwareBreakPointRange (unsigned long ulAddress, 
												   boolean       bARMBP, 
                                                   unsigned long ulBound);
static TyError ML_SetARM11HardwareWatchPoint (unsigned long ulAddress, 
											  unsigned long ulType, 
											  unsigned long ulDataType, 
											  unsigned long ulBound, 
                                         unsigned long ulResourceNo);
static void ML_ClearARM11WPResource (unsigned long ulResourceNo);
static void ML_ClearARM11BPResource (unsigned long ulResourceNo);
static TyError ML_ARM11StepProc(void);
TyError ML_ARM_SetupExecuteProcARM11(unsigned long *ulBPRegArray);
TyError ML_WriteCP14DebugRegARM11 (unsigned long  ulAddress, 
								   unsigned long  ulWriteValue);

static int ML_GetFreeResources(unsigned char *ucFreeResource1, unsigned char *ucFreeResource2);
TyError ML_AllocateMemForWatchpoints (void);
TyError ML_DeAllocateMemForWatchpoints (void);
TyError ML_CortexMCPUReadProc (unsigned long  ulMode,
                                unsigned long  ulMask,
                                unsigned long  *pulState);
TyError ML_CortexMCPUWriteProc (unsigned long  ulMode,
                         unsigned long  ulMask,
                         unsigned long  *pulState);
TyError ML_CortexMReadRegisters (unsigned long *pulRegisters);
TyError ML_CortexMWriteRegisters (unsigned long *pulRegisters);
TyError ML_CortexMStepProc(void);
TyError ML_Armv7AStepProc();

TyError ML_ARM_SetupExecuteProcCortexM(
	unsigned long ulNumHardBreakPoint,
	TyBPResourceCortexMStructure* pHardBreakPointRes,
	unsigned long ulNumHardWatchPoint,
	TyCortexMWpResStruct* pHardWatchPointRes) ;

TyError ML_ARM_SetupExecuteProcCortexA(unsigned long ulNumBreakPointRes,
										TyArmv7ABreakpointRes* ptyArmv7ABreakpointResArray,
										unsigned long ulNumWatchPointRes,
										TyArmv7AWatchpointRes* ptyArmv7AWatchpointResArray);

TyError ML_ResetCore(unsigned long ulResetType);
void ML_GetHardwareResInfo(unsigned long* pulResType, 
						   unsigned long* pulNumRes);

TyError ML_Config_ICEPICK(unsigned long ulSubPortNum);

#endif // _MDILAYER_H_



