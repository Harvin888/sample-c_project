/****************************************************************************
     Function: USBCache.h
     Engineer: Paul Blackburn
        Input: 
       Output: 
  Description: #Defines used for USB Opella Diskware Functions. 
               These are used in DrvLayer.cpp only.
 Date           Initials    Description
30-Jun-2003       PJB         Initial
****************************************************************************/
#define MAXMODES                          0x8
#define MAXREGISTERS                      0x13
#define CM_MAXREGISTERS                  32

#define REG_ARRAY_R0                      0x0
#define REG_ARRAY_R1                      0x1
#define REG_ARRAY_R7                      0x7
#define REG_ARRAY_R8                      0x8
#define REG_ARRAY_R12                     0xC
#define REG_ARRAY_R13                     0xD
#define REG_ARRAY_R14                     0xE
#define REG_UNDEFINED                     0x0F
#define REG_ARRAY_PC                      0x10
#define REG_ARRAY_CPSR                    0x11
#define REG_ARRAY_SPSR                    0x12

#define REG_ARRAY_USR_MODE                0x0
#define REG_ARRAY_FIQ_MODE                0x1
#define REG_ARRAY_IRQ_MODE                0x2
#define REG_ARRAY_SVC_MODE                0x3
#define REG_ARRAY_ABORT_MODE              0x4
#define REG_ARRAY_UNDEF_MODE              0x5
#define REG_ARRAY_SYS_MODE                0x6
#define REG_ARRAY_MON_MODE				  0x7

#define REG_ARRAY_USR_MEMCPY              0x98
#define REG_ARRAY_FIQ_MEMCPY              0x50
#define REG_ARRAY_IRQ_MEMCPY              0x28
#define REG_ARRAY_SVC_MEMCPY              0x28
#define REG_ARRAY_ABORT_MEMCPY            0x28
#define REG_ARRAY_UNDEF_MEMCPY            0x28

#define THUMB_NOP                         0x1B101B10


// ARM Processor modes                    
                                          
#define USR_MODE                                            0x10
#define FIQ_MODE                                            0x11
#define IRQ_MODE                                            0x12
#define SVC_MODE                                            0x13
#define ABORT_MODE                                          0x17
#define UNDEF_MODE                                          0x1B
#define SYS_MODE                                            0x1F
#define MON_MODE											0x16
#define CURRENT_MODE                                        0xFF
#define SVC_MODE_IRQFIQ_DISABLED                            0xD3
                                                            
#define ARMCANTREADSPECS_USR_MODE                           0x0
#define ARMCANTREADSPECS_FIQ_MODE                           0x1
#define ARMCANTREADSPECS_IRQ_MODE                           0x2
#define ARMCANTREADSPECS_SVC_MODE                           0x3
#define ARMCANTREADSPECS_ABORT_MODE                         0x7
#define ARMCANTREADSPECS_UNDEF_MODE                         0xB
#define ARMCANTREADSPECS_SYS_MODE                           0xF

//This mask is used to determined the processor 
//mode from the CPSR.
#define REG_ARRAY_MODE_MASK                                 0x1F  
#define REG_ARRAY_TBIT_MASK                                 0xFFFFFFDF  


#define ARM720_CP_REG_C0_READ_ID_REG                        0x08F00877
#define ARM720_CP_REG_C1_READ_CONTROL_REG                   0x08F08877
#define ARM720_CP_REG_C2_READ_TTB                           0x08F04877
#define ARM720_CP_REG_C3_READ_DAC                           0x08F0C877
#define ARM720_CP_REG_C5_READ_FSR                           0x08F0A877
#define ARM720_CP_REG_C6_READ_FAR                           0x08F06877
#define ARM720_CP_REG_C13_READ_PID                          0x08F0B877
                                                            
#define ARM720_CP_REG_C1_WRITE_CONTROL_REG                  0x08F08077
#define ARM720_CP_REG_C2_WRITE_TTB                          0x08F04077
#define ARM720_CP_REG_C3_WRITE_DAC                          0x08F0C077
#define ARM720_CP_REG_C5_WRITE_FSR                          0x08F0A077
#define ARM720_CP_REG_C6_WRITE_FAR                          0x08F06077
#define ARM720_CP_REG_C7_INVALIDATE_CACHE_ALL               0xE8F0E077
#define ARM720_CP_REG_C8_INVALIDATE_TTB_ALL                 0xE8F01077
#define ARM720_CP_REG_C13_WRITE_PID                         0x08F0B077


//These istructions are reversed
#define ARM740_CP_REG_C0_READ_ID_REG                        0x08f00877     
#define ARM740_CP_REG_C1_READ_CONTROL_REG                   0x08F08877     
#define ARM740_CP_REG_C2_READ_TTB                           0x08F04877     
#define ARM740_CP_REG_C3_READ_DAC                           0x08F0C877     
#define ARM740_CP_REG_C5_READ_FSR                           0x08F0A877     
#define ARM740_CP_REG_C6_READ_FAR                           0x08F06877     

#define ARM740_CP_REG_C1_WRITE_CONTROL_REG                  0x08F08077     
#define ARM740_CP_REG_C2_WRITE_TTB                          0x08F04077     
#define ARM740_CP_REG_C3_WRITE_DAC                          0x08F0C077     
#define ARM740_CP_REG_C5_WRITE_FSR                          0x08F0A077     
#define ARM740_CP_REG_C6_WRITE_FAR                          0x08F06077     

#define ARM740_CP_REG_C7_INVALIDATE_CACHE_ALL               0xE8F0E077
#define ARM740_CP_REG_C8_INVALIDATE_TTB_ALL                 0xE8F01077


//These are the same commands as above but already reversed for USB Opella.
#define ARM720_REVERSED_CP_REG_C0_READ_ID_REG               0x11E010EF
#define ARM720_REVERSED_CP_REG_C1_READ_CONTROL_REG          0x11E110EF
#define ARM720_REVERSED_CP_REG_C2_READ_TTB                  0x11E090EF
#define ARM720_REVERSED_CP_REG_C3_READ_DAC                  0x11E190EF
#define ARM720_REVERSED_CP_REG_C5_READ_FSR                  0x11E150EF
#define ARM720_REVERSED_CP_REG_C6_READ_FAR                  0x11E0D0EF
#define ARM720_REVERSED_CP_REG_C13_READ_PID                 0x11E170EF
                                                   
#define ARM720_REVERSED_CP_REG_C1_WRITE_CONTROL_REG         0x11E100EF
#define ARM720_REVERSED_CP_REG_C2_WRITE_TTB                 0x11E080EF
#define ARM720_REVERSED_CP_REG_C3_WRITE_DAC                 0x11E180EF
#define ARM720_REVERSED_CP_REG_C5_WRITE_FSR                 0x11E140EF
#define ARM720_REVERSED_CP_REG_C6_WRITE_FAR                 0x11E0C0EF
#define ARM720_REVERSED_CP_REG_C13_WRITE_PID                0x11E160EF

#define ARM740_REVERSED_CP_REG_C0_READ_ID_REG               0x11E010EF
#define ARM740_REVERSED_CP_REG_C1_READ_CONTROL_REG          0x11E110EF
#define ARM740_REVERSED_CP_REG_C2_READ_TTB                  0x11E090EF
#define ARM740_REVERSED_CP_REG_C3_READ_DAC                  0x11E190EF
#define ARM740_REVERSED_CP_REG_C5_READ_FSR                  0x11E150EF
#define ARM740_REVERSED_CP_REG_C6_READ_FAR                  0x11E0D0EF
                                                  
#define ARM740_REVERSED_CP_REG_C1_WRITE_CONTROL_REG         0x11E100EF
#define ARM740_REVERSED_CP_REG_C2_WRITE_TTB                 0x11E080EF
#define ARM740_REVERSED_CP_REG_C3_WRITE_DAC                 0x11E180EF
#define ARM740_REVERSED_CP_REG_C5_WRITE_FSR                 0x11E140EF
#define ARM740_REVERSED_CP_REG_C6_WRITE_FAR                 0x11E0C0EF


#define ARM920_922CP_PHYSICAL_ACCESS                        0x01
#define ARM920_922CP_WRITEMASK                              0x80
#define ARM920_922_CP_REG_C0_ID_REG                         0x00
#define ARM920_922_CP_REG_C0_CACHE_TYPE                     0x02
#define ARM920_922_CP_REG_C1_CONTROL                        0x04
#define ARM920_922_CP_REG_C9_DCACHE_L_DOWN                  0x24
#define ARM920_922_CP_REG_C9_ICACHE_L_DOWN                  0x26
#define ARM920_922_CP_REG_C13_PROCESS_ID                    0x34
#define ARM920_922_CP_REG_C15_TEST_STATE                    0x3C
#define ARM920_922_CP_REG_C15_INST_C_INDEX                  0x36
#define ARM920_922_CP_REG_C15_DATA_C_INDEX                  0x7A

#define ARM920_922_CP_REG_C2_WRITE_I_TTB                    0xEEAF0F51   
#define ARM920_922_CP_REG_C2_READ_I_TTB                     0xEEBF0F51 
#define ARM920_922_CP_REG_C2_WRITE_D_TTB                    0xEEAF0F52
#define ARM920_922_CP_REG_C2_READ_D_TTB                     0xEE120F52 
#define ARM920_922_CP_REG_C3_WRITE_I_DAC                    0xEEAF0F71 
#define ARM920_922_CP_REG_C3_READ_I_DAC                     0xEEBF0F71 
#define ARM920_922_CP_REG_C3_WRITE_D_DAC                    0xEEAF0F72 
#define ARM920_922_CP_REG_C3_READ_D_DAC                     0xEE130F10 
#define ARM920_922_CP_REG_C5_WRITE_I_FSR                    0xEE050F30 
#define ARM920_922_CP_REG_C5_READ_I_FSR                     0xEE150F30 
#define ARM920_922_CP_REG_C5_WRITE_D_FSR                    0xEE050F10 
#define ARM920_922_CP_REG_C5_READ_D_FSR                     0xEE150F10 
#define ARM920_922_CP_REG_C6_WRITE_I_FAR                    0xEE060F30 
#define ARM920_922_CP_REG_C6_READ_I_FAR                     0xEE160F30 
#define ARM920_922_CP_REG_C6_WRITE_D_FAR                    0xEE060F10 
#define ARM920_922_CP_REG_C6_READ_D_FAR                     0xEE160F10 
#define ARM920_922_CP_REG_C7_INVALIDATE_ICACHE_ALL          0xEE070F15 
#define ARM920_922_CP_REG_C7_INVALIDATE_I_AND_D_CACHE       0xEE070F17 
#define ARM920_922_CP_REG_C8_INVALIDATE_ALL_TLBS            0xEE080F17 
#define ARM920_922_CP_REG_C8_INVALIDATE_I_TLB               0xEE080F15 
#define ARM920_922_CP_REG_C8_INVALIDATE_D_TLB               0xEE080F16 
#define ARM920_922_CP_REG_C10_WRITE_I_TLB_LOCKDOWN          0xEE0A0F30         
#define ARM920_922_CP_REG_C10_READ_I_TLB_LOCKDOWN           0xEE1A0F30         
#define ARM920_922_CP_REG_C10_WRITE_D_TLB_LOCKDOWN          0xEE0A0F10 
#define ARM920_922_CP_REG_C10_READ_D_TLB_LOCKDOWN           0xEE1A0F10 
                                                            
                                                            
#define ARM926_CP_REG_C0_ID_REG                             0x0000
#define ARM926_CP_REG_C0_CACHE_TYPE                         0x0100
#define ARM926_CP_REG_C0_TCM_STATUS                         0x0200
#define ARM926_CP_REG_C1_CONTROL                            0x0010
#define ARM926_CP_REG_C2_TTB                                0x0020
#define ARM926_CP_REG_C3_DAC                                0x0030
#define ARM926_CP_REG_C5_DFSR                               0x0050
#define ARM926_CP_REG_C5_IFSR                               0x0150
#define ARM926_CP_REG_C6_FAR                                0x0060
#define ARM926_CLEAN_INVALIDATE_DCACHE                      0x027E
#define ARM926_CP_REG_C9_DCACHE_L_DOWN                      0x0090
#define ARM926_CP_REG_C9_ICACHE_L_DOWN                      0x0190
#define ARM926_CP_REG_C9_DCACHE_TCM                         0x0091
#define ARM926_CP_REG_C9_ICACHE_TCM                         0x0191
#define ARM926_CP_REG_C10_TLB                               0x00A0
#define ARM926_CP_REG_C13_PROCESS_ID                        0x00D0
#define ARM926_INVALIDATE_ICACHE                            0x0075
#define ARM926_INITIATE_NEW_CP_ACCESS                       0x0001
#define ARM926_CP_ACCESS_WRITE_MASK                         0x8000
                                                            
                                                            
#define ARM940_946CP_WRITEMASK                              0x40
#define ARM940_946_CP_REG_C0_ID_REG                         0x00
#define ARM940_946_CP_REG_C0_CACHE_TYPE                     0x01
#define ARM940_946_CP_REG_C0_TCM_SIZE                       0x08
#define ARM940_946_CP_REG_C1_CONTROL                        0x02
#define ARM940_946_CP_REG_C2_DCACHE_BITS                    0x04
#define ARM940_946_CP_REG_C2_ICACHE_BITS                    0x05
#define ARM940_946_CP_REG_C3_WRITEBUF_CTRL                  0x06
#define ARM940_946_CP_REG_C5_DSPACE_PERMISSIONS             0x0A
#define ARM940_946_CP_REG_C5_ISPACE_PERMISSIONS             0x0B
#define ARM940_946_CP_REG_C6_REG0_MEMPROTECTION             0x20
#define ARM940_946_CP_REG_C6_REG1_MEMPROTECTION             0x22
#define ARM940_946_CP_REG_C6_REG2_MEMPROTECTION             0x24
#define ARM940_946_CP_REG_C6_REG3_MEMPROTECTION             0x26
#define ARM940_946_CP_REG_C6_REG4_MEMPROTECTION             0x28
#define ARM940_946_CP_REG_C6_REG5_MEMPROTECTION             0x2A
#define ARM940_946_CP_REG_C6_REG6_MEMPROTECTION             0x2C
#define ARM940_946_CP_REG_C6_REG7_MEMPROTECTION             0x2E
#define ARM940_946_CP_REG_C7_FLUSH_ICACHE                   0x0F
#define ARM940_946_CP_REG_C9_DCACHE_LOCKDOWN                0x12
#define ARM940_946_CP_REG_C9_ICACHE_LOCKDOWN                0x13
                                                            
#define ARM966CP_WRITEMASK                                  0x40
#define ARM966_CP_REG_C0_ID_REG                             0x00
#define ARM966_CP_REG_C1                                    0x02
#define ARM966_CP_REG_C15_1                                 0xFC
#define ARM966_CP_REG_C15_2                                 0xF9
#define ARM966_CP_REG_C15_3                                 0xFD
#define ARM966_CP_REG_C15_4                                 0xFB
#define ARM966_CP_REG_C15_5                                 0xFF

 



#define BLOCKSIZE                                           0x38

#define ARM7_MAX_BLOCK_WRITE_SIZE                           0x04
#define ARM7_MAX_WORD_READ_SIZE                             0x38
#define ARM7_MAX_BLOCK_READ_SIZE                            0x04 

#define ARM9_MAX_BLOCK_READ_SIZE                            0x05 
#define ARM9_MAX_WORD_READ_SIZE                             0x20
#define ARM9_MAX_BLOCK_WRITE_SIZE                           0x300
#define ARM9_MAX_WORD_WRITE_SIZE                            0x11

#define ARM11_DSCR_CORE_HALTED                  0x1
#define ARM11_DSCR_CORE_RESTARTED               0x2

#define ARM11_DSCR_ENTRY_FIELD_MASK        0x0000003C

#define ARM11_HALT_INSTRUCTION      0x0
#define ARM11_BREAK_POINT           0x1
#define ARM11_WATCH_POINT           0x2
#define ARM11_BKPT_INSTRUCTION      0x3
#define ARM11_EDBGRQ                0x4
#define ARM11_VECTOR_CATCH          0x5
#define ARM11_DATA_ABORT            0x6
#define ARM11_INSTRUCTION_ABORT     0x7


// CPSR Related Bits
#define ARM11_CPSR_T_BIT        0x00000020
#define ARM11_CPSR_J_BIT        0x01000000
typedef enum
{
    REG_START       = 0,
    REG_R0          = 0,    /* Register R0 */
    REG_R1          = 1,    /* Register R1 */
    REG_R2          = 2,    /* Register R2 */
    REG_R3          = 3,    /* Register R3 */
    REG_R4          = 4,    /* Register R4 */
    REG_R5          = 5,    /* Register R5 */
    REG_R6          = 6,    /* Register R6 */
    REG_R7          = 7,    /* Register R7 */
    REG_R8          = 8,    /* Register R8 */
    REG_R9          = 9,    /* Register R9 */
    REG_R10         = 10,   /* Register R10 */
    REG_R11         = 11,   /* Register R11 */
    REG_R12         = 12,   /* Register R12 */
    REG_CurrSP      = 13,   /* Register Current SP */
    REG_LR          = 14,   /* Register LR */
    //REG_RESERVED    = 15,
    REG_PC          = 16,   /* Register Debug return address */
    REG_xPSR        = 17,   /* Register xPSR */
    REG_APSR        = 18,
    REG_IPSR        = 19,
    REG_EPSR        = 20,
    REG_IAPSR       = 21,
    REG_EAPSR       = 22,
    REG_IEPSR       = 23,
    REG_PRIMASK     = 24,   /* Register MSP */
    REG_FAULTMASK   = 25,   /* Register PSP */
    REG_BASEPRI     = 26,   /* Register Control */
    REG_BASEPRI_MAX = 27,
    REG_CONTROL     = 28,
    REG_MSP         = 29,
    REG_PSP         = 30,
    REG_LAST        = 31
} Register;
