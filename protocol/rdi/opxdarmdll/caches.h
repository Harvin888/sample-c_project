/****************************************************************************
       Module: CACHES.H
     Engineer: Suraj S
  Description: Header File for Cache layer
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
#ifndef _CACHES_H_
#define _CACHES_H_

#include "../../common/drvopxd.h"
#include "common.h"

#define CL_Error_NoError		   0
#define CL_Error_UnknownCoPro		135

#define MAX_CP_REGISTERS                  0x23
#define MAX_MULTIPLE_CPREGS               0x7

//Co Processor registers definitions.
//These are the positions in CPRegArray where values are stored.
#define CP_REG_ARRAY_C0                   0x0
#define CP_REG_ARRAY_C1                   0x1
#define CP_REG_ARRAY_C2                   0x2
#define CP_REG_ARRAY_C3                   0x3
#define CP_REG_ARRAY_C4                   0x4
#define CP_REG_ARRAY_C5                   0x5
#define CP_REG_ARRAY_C6                   0x6
#define CP_REG_ARRAY_C7                   0x7
#define CP_REG_ARRAY_C8                   0x8
#define CP_REG_ARRAY_C9                   0x9
#define CP_REG_ARRAY_C10                  0xA
#define CP_REG_ARRAY_C11                  0xB
#define CP_REG_ARRAY_C12                  0xC
#define CP_REG_ARRAY_C13                  0xD
#define CP_REG_ARRAY_C14                  0xE
#define CP_REG_ARRAY_C15                  0xF
                                          
#define CP_REG_0                          0x0
#define CP_REG_1                          0x1
#define CP_REG_2                          0x2
#define CP_REG_3                          0x3
#define CP_REG_4                          0x4
#define CP_REG_5                          0x5
#define CP_REG_6                          0x6
#define CP_REG_7                          0x7
#define CP_REG_8                          0x8
#define CP_REG_9                          0x9

//These are AND masks are used to zero unpredictable bits in register Reads.
#define ARM720_CPREG_1_MASK                        0x000043FF
#define ARM720_CPREG_2_MASK                        0xFFFFC000
#define ARM720_CPREG_5_MASK                        0x000000FF
#define ARM720_CPREG_13_MASK                       0xFE000000

#define ARM740_CPREG_1_MASK                        0x3F00008D
#define ARM740_CPREG_2_MASK                        0x000000FF
#define ARM740_CPREG_3_MASK                        0x000000FF
#define ARM740_CPREG_5_MASK                        0x0000FFFF
#define ARM740_CPREG_6_MASK                        0xFFFFF03F

#define ARM920_922_CPREG_1_MASK                    0xC0007FFF   
#define ARM920_922_CPREG_2_MASK                    0xFFFFC000
#define ARM920_922_CPREG_5_MASK                    0x000000FF
#define ARM920_922_CPREG_9_MASK                    0xFC000000
#define ARM920_922_CPREG_10_MASK                   0xFFF00001

#define ARM926_CPREG_2_MASK                        0xFFFFC000
#define ARM926_CPREG_5_MASK                        0x000000FF
#define ARM926_CPREG_9_LDOWN_MASK                  0x0000FFFF
#define ARM926_CPREG_9_TLB_MASK                    0xFFFFF03F
#define ARM926_CPREG_10_MASK                       0x1C000001

#define ARM940_946_CPREG_9_MASK                    0x80000003

#define ARM920_MMU_ON                              0x00000001 
#define ARM920_CACHE_OFF                           0xFFFFEFFB
#define ARM920_ICACHE_ON                           0x00001000
#define ARM920_DCACHE_ON                           0x00000004

#define ARM940_946_CACHE_OFF                       0xFFFFEFFB
#define ARM940_946_ICACHE_ON                       0x00001000
#define ARM940_946_DCACHE_ON                       0x00000004

#define ARM926_CACHE_OFF                           0xFFFFEFFB
#define ARM926_ICACHE_ON                           0x00001000
#define ARM926_DCACHE_ON                           0x00000004

#define ARM720_CACHE_OFF                           0xFFFFFFFB
#define ARM720_CACHE_ON                            0x00000004
#define ARM720_MMU_ON                              0x00000001 

//ARM946 cache sizes.Read from Cache type register.
#define SIZE_0KB                                   0x0
#define SIZE_4KB                                   0x3
#define SIZE_8KB                                   0x4
#define SIZE_16KB                                  0x5
#define SIZE_32KB                                  0x6
#define SIZE_64KB                                  0x7
#define SIZE_128KB                                 0x8
#define SIZE_256KB                                 0x9
#define SIZE_512KB                                 0xA
#define SIZE_1MB                                   0xB
#define ARM946_DCACHE_SIZE_MASK                    0x1C0000

/*Function Prototypes*/
TyError CL_CPReadProc   (unsigned long CPnum,
                         unsigned long mask,
                         unsigned long *state);

TyError CL_CPWriteProc  (unsigned long  CPnum,
                         unsigned long  mask,
                         unsigned long  *state);

TyError CL_SetARM9RestartAddress(unsigned long *arg1);

TyError CL_WriteARM720CoProcRegisters  (void);

TyError CL_WriteARM740CoProcRegisters  (void);

TyError CL_WriteARM920_922CoProcRegisters  (void);

TyError CL_WriteARM926CoProcRegisters  (void);

TyError CL_WriteARM940_946CoProcRegisters  (void);

TyError CL_WriteARM966CoProcRegisters  (void);

TyError CL_ReadARM720CoProcRegisters  (void);

TyError CL_ReadARM740CoProcRegisters  (void);

TyError CL_ReadARM920_922CoProcessor (unsigned char uchRegisterAddress, 
                                      unsigned long *pulRegisterValue);

TyError CL_WriteARM920_922CoProcessor (unsigned char uchRegisterAddress, 
                                       unsigned long ulRegisterValue);

TyError CL_ReadARM920_922CoProcRegisters  (void);

TyError CL_ReadARM926CoProcRegisters  (void);

TyError CL_ReadARM940_946CoProcRegisters  (void);

TyError CL_ReadARM966CoProcRegisters  (void);

TyError CL_ReadARM968CoProcRegisters  (void);

TyError CL_WriteARM968CoProcRegisters  (void);

TyError CL_WriteARM940_946CoProcessor  (unsigned char uchRegisterAddress, 
                                        unsigned long  ulRegisterValue);

TyError CL_WriteARM926CoProcessor (unsigned char uchRegisterAddress, 
                                   unsigned long ulRegisterValue);

TyError CL_CleanandDisable940_946Caches (void);

TyError CL_Clean940DataCache (void);

TyError CL_RunARM9TargetCacheFlush(unsigned long ulNumberOfBytes);

TyError CL_Clean946DataCache (void);

TyError CL_CleanandDisable926Caches (void);

TyError CL_Clean926DataCache (void);

TyError CL_InvalidateARM920_922ICache(void);

TyError CL_CleanandDisable920_922Caches (void);

TyError CL_Clean920_922DataCache (void);

TyError CL_FlushandDisableARM740CacheAndTTB (void);

TyError CL_FlushandDisableARM720CacheAndTTB (void);

TyError CL_WriteARM720_740CoProcessor (unsigned long ulCPInstruction, 
                                       unsigned long ulRegisterValue);

boolean CL_QueryCP15Cache(void);

void CL_SetCP15CacheSelection(unsigned long CacheSelection);

void CL_GetCP15CacheSelection(unsigned long *CacheSelection);

boolean CL_QueryCP15MemoryArea(void);

void CL_SetCP15MemoryArea(unsigned long MemoryArea);

void CL_GetCP15MemoryArea(unsigned long *MemoryArea);

TyError CL_GetARM9RestartAddress(unsigned long *arg1);


TyError CL_WriteARM1136CoProcRegisters (void);

TyError CL_ReadARM1136CoProcRegisters  (void);

TyError CL_CleanandDisable1136_Caches (void);

TyError CL_CleanandDisableCortexACaches( unsigned long ulStartAddr, unsigned long ulEndAddr );
#endif // _CACHES_H_
