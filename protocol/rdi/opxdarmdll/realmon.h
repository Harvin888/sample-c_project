/****************************************************************************
       Module: REALMONITOR.H
     Engineer: Suraj S
  Description: Header File for Real Monitor support
Date           Initials    Description
23-Jul-2007      SJ       Initial
****************************************************************************/
#ifndef REALMON_H_
#define REALMON_H_


#define ESCAPE_SEQUENCE             0xA146F098
#define ESCAPE_QUOTE                0x00000000

#define NOP_OPCODE                  0x0
#define GET_CAPABILITIES_OPCODE     0x1
#define STOP_OPCODE                 0x2
#define GO_OPCODE                   0x3
#define GET_PC_OPCODE               0x4
#define SYNC_CACHES_OPCODE          0x5
#define EXECUTE_CODE_OPCODE         0x6
#define INITIALISE_TARGET_OPCODE    0x7
#define READ_BYTES_OPCODE           0x8
#define WRITE_BYTES_OPCODE          0x9
#define READ_HALFWORDS_OPCODE       0xA
#define WRITE_HALFWORDS_OPCODE      0xB
#define READ_WORDS_OPCODE           0xC
#define WRITE_WORDS_OPCODE          0xD
#define READ_REGISTERS_OPCODE       0x10
#define WRITE_REGISTERS_OPCODE      0x11

#define NOP_STATUS                  0x0
#define OK_STATUS                   0x80
#define ERROR_STATUS                0x81
#define STOPPED_STATUS              0x90
#define SOFT_BREAK_STATUS           0x91
#define HARD_BREAK_STATUS           0x92
#define HARD_WATCH_STATUS           0x93
#define SWI_STATUS                  0x94
#define UNDEF_STATUS                0xA0
#define PREFETCH_STATUS             0xA1
#define DATA_STATUS                 0xA2

#define MASK_SWI_STATUS             0x1
#define MASK_UNDEF_STATUS           0x2
#define MASK_PREFETCH_STATUS        0x4
#define MASK_DATA_STATUS            0x8

#define ESCAPE_SEQUENCE             0xA146F098
#define ESCAPE_QUOTE                0x00000000
#define ESCAPE_RESET                0x00000001
#define ESCAPE_GO                   0x00000002
#define ESCAPE_PANIC                0x00000003

#define MAX_TIMEOUT                 0x100
#define REALMON_CHANNEL             0x0
#define ACCESS_SUCCESFUL            0x00800000

typedef struct
{
   unsigned long ulLength;
   unsigned long ulOpcode;
   unsigned long ulChannel;
   unsigned char ucData[0x100];

} TyRealMonPacket;

unsigned char RealMonitorDetected(void);
TyError RealMonWriteDCC(unsigned long ulWriteData);
TyError RealMonWriteSafeDCC(unsigned long ulWriteData);
TyError RealMonSendPacket(TyRealMonPacket tyRealMonPacket);
TyError RealMonReceivePacket(TyRealMonPacket *ptyRealMonPacket, 
                             unsigned long *pulExceptionStatus);
TyError RealMonReadSafeDCC(unsigned long *pulReadData);
TyError RealMonReadDCC(unsigned long *pulReadData);

TyError ExecuteNOP(void);
TyError ExecuteGetCapabilities(unsigned long *pulAddress);
TyError ExecuteStop(void);
TyError ExecuteGo(void);
TyError ExecuteGetPC(unsigned long ulCount, unsigned long *pulPCSamples);
TyError ExecuteSyncCaches(unsigned long ulAddress, unsigned long ulCount);
TyError ExecuteExecuteCode(void);
TyError ExecuteInitialiseTarget(void);
TyError ExecuteWriteBytes(unsigned long ulAddress,
                          unsigned long ulCount,
                          unsigned char *pucData);
TyError ExecuteReadBytes(unsigned char bSetupAddress,
                         unsigned long ulAddress,
                         unsigned char bSetupCount,
                         unsigned long ulCount,
                         unsigned char *pucData);
TyError ExecuteReadHalfWords(unsigned char  bSetupAddress,
                             unsigned long  ulAddress,
                             unsigned char  bSetupCount,
                             unsigned long  ulCount,
                             unsigned char *pucData);
TyError ExecuteWriteHalfWords(unsigned long ulAddress,
                              unsigned long ulCount,
                              unsigned char *pucData);
TyError ExecuteReadWords(unsigned char  bSetupAddress,
                         unsigned long  ulAddress,
                         unsigned char  bSetupCount,
                         unsigned long  ulCount,
                         unsigned char *pucData);
TyError ExecuteWriteWords(unsigned long ulAddress,
                          unsigned long ulCount,
                          unsigned char *pucData);
TyError ExecuteReadRegisters(unsigned long ulFirstMask,
                             unsigned char bSetupSecondMask,
                             unsigned long ulSecondMask,
                             unsigned char *pucData);
TyError ExecuteWriteRegisters(unsigned long ulCount,
                              unsigned long *pulMasks,
                              unsigned long *pulValues);
TyError ExecuteReadCPRegister(void);
TyError ExecuteWriteCPRegister(void);
TyError InitialiseCommsLink(void);

#endif   
// REALMON_H_
