/****************************************************************************
       Module: CACHES.CPP
     Engineer: Suraj S
  Description: All functionalities related to coprocessor and cache operations
			      are implemented in this module
Date           Initials    Description
25-Jul-2007      SJ       Initial
21-Feb-2008      SJ       Added Diagnostics Support
24-Mar-2008		 SJ		  Added 968 core support
06-Jun-2008		 RS		  Added Linux Support 
****************************************************************************/
#include "caches.h"
//#include "semihost.h"
#include "midlayerarm.h"
#include "ARMdefs.h" //"protocol/rdi/opxdarmdll/ARMdefs.h"
#include "../../common/drvopxd.h" //"protocol/common/drvopxd.h"
#include "../../common/drvopxdarm.h" //"protocol/common/drvopxdarm.h"

// Following is used to remember if align fault detection was turned on
// when the processor is halted.
static unsigned char bGlobalReenableAlignFault;

static unsigned long ulCacheCleanStartAddr    = 0x50;

//Due to Harvard Architecture cores having separate data and instruction caches
//there are more than one value associated with certain registers. ie data and inst.
static unsigned long CPRegArray  [MAX_CP_REGISTERS] [MAX_MULTIPLE_CPREGS]  = {0};
static unsigned long CPRegArrayArm11[4];

//To copy the CPReg0 and CPReg1 values for RDI diagnostics
unsigned long ulGlobalIDcode0=0;
unsigned long ulGlobalIDcode1=0;

//This is used for Harvard architectures where the same CP15 register can
//access both Instruction or data cache.
static unsigned long ulGlobalCP15CacheSelection = 0;
static unsigned long ulGlobalCP15MemoryArea     = 0;


extern unsigned long RegArray [MAXMODES] [MAXREGISTERS];

extern TyDevHandle tyCurrentDevice;
extern TyProcessorConfig  tyProcessorConfig;
extern unsigned long      ulGlobalScanChain;
extern TyTargetConfig tyTargetConfig;
//For Diagnostics only
#ifdef __LINUX
unsigned short usDiagnostics = 0;
TyDevHandle tyDiagDevHandle= 0;
#else
extern unsigned short usDiagnostics;
extern TyDevHandle tyDiagDevHandle;
#endif

/****************************************************************************
    Function: CL_CPReadProc
    Engineer: Suraj S
       Input: unsigned long CPnum  - Co Processor number
              unsigned long mask   - mask giving resgister/s to read from.
              unsigned long *state - pointer to data read.
      Output: TyError - Mid Layer Error code
 Description: Read from co-processor registers.
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_CPReadProc   (unsigned long CPnum,
                         unsigned long mask,
                         unsigned long *state)
{
   unsigned long RegNum;
   unsigned long *pulLocalState;
   unsigned long ulLocalMask;
   unsigned long ulLocalCPBank;

   pulLocalState = state;

   // Coprocessor 15 is the only CP supported.
   if (CPnum !=15)
      return ML_Error_UnknownCoPro;

   RegNum   =  CP_REG_ARRAY_C0;
   ulLocalMask = 1;
   while (ulLocalMask != 0)
      {
      if (mask & ulLocalMask) 
         {
         //Check if core has separate Instruction and Data caches.
         switch (tyProcessorConfig.tyCoProcessorType)
            {
            default:
            case NO_COPROCESSOR:
            case ARM720T_CP:  
            case ARM966ES_CP:
            case ARM968ES_CP:
               ulLocalCPBank = 0x0;
               break;
            
            case ARM920T_CP:
            case ARM922T_CP:
               switch (RegNum)
                  {
                  default:
                  case 1:
                  case 13:
                     ulLocalCPBank = 0x0;
                     break;
                  case 0:
                  case 2:
                  case 3:
                  case 5:
                  case 6:
                  case 9:
                  case 10:
                     if (ulGlobalCP15CacheSelection < 3)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  }
               break;

            case ARM926EJS_CP:
               switch (RegNum)
                  {
                  default:
                  case 1:
                  case 2:
                  case 3:
                  case 10:
                  case 13:
                     ulLocalCPBank = 0x0;
                     break;
                  case 0:
                     if (ulGlobalCP15CacheSelection < 3)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  case 5:
                     if (ulGlobalCP15CacheSelection < 2)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  case 6:
                     if (ulGlobalCP15CacheSelection < 2)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  case 9:
                     if (ulGlobalCP15CacheSelection < 4)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  }
               break;

            case ARM940T_CP:
            case ARM946ES_CP:
               switch (RegNum)
                  {
                  default:
                  case 1:
                     ulLocalCPBank = 0x0;
                     break;
                  case 2:
                  case 5:
                  case 9:
                     if (ulGlobalCP15CacheSelection < 2)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  case 0:
                  case 3:
                     if (ulGlobalCP15CacheSelection < 3)
                        ulLocalCPBank = ulGlobalCP15CacheSelection;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  case 6:
                     if (ulGlobalCP15MemoryArea < 8)
                        ulLocalCPBank = ulGlobalCP15MemoryArea;  
                     else
                        ulLocalCPBank = 0x0;
                     break;
                  }
               break;
            }

         *pulLocalState = (unsigned long)CPRegArray [RegNum] [ulLocalCPBank];
         pulLocalState ++;
         }
      ulLocalMask <<=1;
      RegNum ++;
      }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_CPWriteProc
    Engineer: Suraj S
       Input: unsigned long CPnum  - Co Processor number
              unsigned long mask   - mask giving resgister/s to read from.
              unsigned long *state - pointer to data to write.
      Output: Error status
 Description: Write to co-processor registers.
Date          Initials    Description
26-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_CPWriteProc  (unsigned long  CPnum,
                         unsigned long  mask,
                         unsigned long  *state)
{
   TyError		 ErrRet;
   unsigned long RegCount;
   unsigned long RegNum;
   unsigned long ulLocalMask;
   unsigned long ulLocalCPBank;

   // Coprocessor 15 is the only CP supported.
   if (CPnum !=15)
      return RDIError_UnknownCoPro;

   ulLocalMask = mask;
   RegNum   =  CP_REG_ARRAY_C0;
   RegCount =  CP_REG_ARRAY_C0;

   while (RegCount<MAX_CP_REGISTERS)
      {
      if (ulLocalMask & 0x01) //find reg in mask
         {
         RegNum = RegCount;
         break;
         }
      RegCount ++;
      ulLocalMask=ulLocalMask>>1;
      }

   //Check if core has separate Instruction and Data caches.
   switch (tyProcessorConfig.tyCoProcessorType)
      {
      default:
      case NO_COPROCESSOR:
      case ARM720T_CP:
      case ARM966ES_CP:
      case ARM968ES_CP:
         ulLocalCPBank = 0x0;
         break;

      case ARM920T_CP:
      case ARM922T_CP:
         switch (RegNum)
            {
            default:
            case 1:
            case 13:
               ulLocalCPBank = 0x0;
               break;
            case 0:
            case 2:
            case 3:
            case 5:
            case 6:
            case 9:
            case 10:
               if (ulGlobalCP15CacheSelection < 3)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            }
         break;

      case ARM926EJS_CP:
         switch (RegNum)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 10:
            case 13:
               ulLocalCPBank = 0x0;
               break;
            case 0:
               if (ulGlobalCP15CacheSelection < 3)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            case 5:
               if (ulGlobalCP15CacheSelection < 2)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            case 6:
               if (ulGlobalCP15CacheSelection < 2)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            case 9:
               if (ulGlobalCP15CacheSelection < 4)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            }
         break;

      case ARM940T_CP:
      case ARM946ES_CP:
         switch (RegNum)
            {
            default:
            case 1:
               ulLocalCPBank = 0x0;
               break;
            case 2:
            case 5:
            case 9:
               if (ulGlobalCP15CacheSelection <= 1)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            case 0:
            case 3:
               if (ulGlobalCP15CacheSelection <= 2)
                  ulLocalCPBank = ulGlobalCP15CacheSelection;  
               else
                  ulLocalCPBank = 0x0;
               break;
            case 6:
               if (ulGlobalCP15MemoryArea <= 7)
                  ulLocalCPBank = ulGlobalCP15MemoryArea;  
               else
                  ulLocalCPBank = 0x0;
               break;
            }
         break;
		case ARM1136JFS_CP:
				ulLocalCPBank = 0x0;
				break;
      }

   CPRegArray[RegNum] [ulLocalCPBank] = *state;

   // write cache contents to target
   ErrRet = ML_WriteRegisters((unsigned long *)RegArray);
   if (ErrRet != ML_ERROR_NO_ERROR)
	   return ErrRet;

   // update cache from target
   ErrRet = ML_ReadRegisters((unsigned long *)RegArray);
   if (ErrRet != ML_ERROR_NO_ERROR)
	   return ErrRet;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_SetARM9RestartAddress
    Engineer: Suraj S
       Input: unsigned long *arg1 - Address of memory  
      Output: TyError - Mid Layer Error code. Only needed for ARM920/922 & 
              ARM946/940. All others return unimplemented
 Description: Sets the target memory location for data cache clean handler.      
              RDI defines this as 128 Bytes of target memory that is not 
              used by the target code.                                                          
Date          Initials    Description
17-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_SetARM9RestartAddress(unsigned long *arg1)
{
   TyError ErrRet;

   switch (tyProcessorConfig.tyCoProcessorType)
      {
      default:
      case NO_COPROCESSOR:
      case ARM966ES_CP:
      case ARM968ES_CP:
      case ARM720T_CP:
         ErrRet = ML_Error_UnimplementedType;
         break;
      case ARM920T_CP:
      case ARM922T_CP:
	  case ARM926EJS_CP:
      case ARM940T_CP:
      case ARM946ES_CP:
	  case CORTEXA8_CP:
	  case CORTEXA9_CP:
		 ulCacheCleanStartAddr = *arg1; 
         ErrRet = ML_ERROR_NO_ERROR;
         break;
		case ARM1136JFS_CP:
		case ARM1156T2S_CP:
		case ARM1176JZS_CP:
		case ARM11MP_CP:
			ErrRet = ML_ERROR_NO_ERROR;
			break;
      }

   return ErrRet;
}

/****************************************************************************
    Function: CL_WriteARM720CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM720T
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_WriteARM720CoProcRegisters  (void)
{
   TyError ErrRet;
    
   ErrRet = DL_OPXD_Arm_Write_ARM720CP_Registers(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
                                                 CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],
                                                 CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],
                                                 CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],
                                                 CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],
                                                 CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],
                                                 CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
 
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM740CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM740T
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_WriteARM740CoProcRegisters  (void)
{
   TyError ErrRet;
    
   ErrRet = DL_OPXD_Arm_Write_ARM740CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
                                         CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],              
                                         CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],            
                                         CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],             
                                         CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],             
                                         CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0]);          
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM920_922CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM920T / ARM922T
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_WriteARM920_922CoProcRegisters  (void)
{
   TyError ErrRet;

   // Was alignment fault enable turned on ?
   if (bGlobalReenableAlignFault)
      {
      CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] |= 0x2;
      }
   
   ErrRet = DL_OPXD_Arm_Write_ARM920_922CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 0x1,
                                                      CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
                                                      CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
                                                      CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_1],          
                                                      CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
                                                      CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_1],          
                                                      CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
                                                      CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
                                                      CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                      CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_1],          
                                                      CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
                                                      CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1],          
                                                      CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0],          
                                                      CPRegArray[CP_REG_ARRAY_C10] [CP_REG_1],
                                                      CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
      
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM926CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM926EJ-S
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_WriteARM926CoProcRegisters  (void)
{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Write_ARM926CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                  CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
                                                  CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
                                                  CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
                                                  CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
                                                  CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
                                                  CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                  CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
                                                  CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1],          
                                                  CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_2],          
                                                  CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_3],          
                                                  CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0],          
                                                  CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM940_946CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM940T / ARM946E-S
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_WriteARM940_946CoProcRegisters  (void)
{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Write_ARM940_946CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                      CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0],
                                                      CPRegArray[CP_REG_ARRAY_C2] [CP_REG_0],
                                                      CPRegArray[CP_REG_ARRAY_C2] [CP_REG_1],
                                                      CPRegArray[CP_REG_ARRAY_C3] [CP_REG_0],
                                                      CPRegArray[CP_REG_ARRAY_C5] [CP_REG_0],
                                                      CPRegArray[CP_REG_ARRAY_C5] [CP_REG_1],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_0],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_1],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_2],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_3],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_4],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_5],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_6],
                                                      CPRegArray[CP_REG_ARRAY_C6] [CP_REG_7],
                                                      CPRegArray[CP_REG_ARRAY_C9] [CP_REG_0],
                                                      CPRegArray[CP_REG_ARRAY_C9] [CP_REG_1]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM966CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM966
Date          Initials    Description
25-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_WriteARM966CoProcRegisters  (void)

{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Write_ARM966CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                  CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],  
                                                  CPRegArray[CP_REG_ARRAY_C15] [CP_REG_0],
                                                  CPRegArray[CP_REG_ARRAY_C15] [CP_REG_1],
                                                  CPRegArray[CP_REG_ARRAY_C15] [CP_REG_2],
                                                  CPRegArray[CP_REG_ARRAY_C15] [CP_REG_3],
                                                  CPRegArray[CP_REG_ARRAY_C15] [CP_REG_4]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM968CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer Error code
 Description: Writes Co-processor registers back to an ARM968
Date          Initials    Description
24-Mar-2008     SJ        Initial
****************************************************************************/
TyError CL_WriteARM968CoProcRegisters  (void)

{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Write_ARM968CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                  CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM720CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from ARM720T
Date          Initials    Description
26-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_ReadARM720CoProcRegisters  (void)
{
   TyError ErrRet;
    
   if(usDiagnostics) 
      ErrRet = DL_OPXD_Arm_Read_ARM720CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber,
                                                    &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                    &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],              
                                                    &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],            
                                                    &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],             
                                                    &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],             
                                                    &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                    &CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   else
   ErrRet = DL_OPXD_Arm_Read_ARM720CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],              
                                                 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],            
                                                 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],             
                                                 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],             
                                                 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
 
   //These registers have unpredictable bits so we zero them.
   CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0]  &= ARM720_CPREG_1_MASK; 
   CPRegArray[CP_REG_ARRAY_C2] [CP_REG_0]  &= ARM720_CPREG_2_MASK; 
   CPRegArray[CP_REG_ARRAY_C5] [CP_REG_0]  &= ARM720_CPREG_5_MASK; 
   CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0] &= ARM720_CPREG_13_MASK;

   //To store the IDcode for CheckTarget functionality
   ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];
   ulGlobalIDcode1 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1];
  return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM740CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM740T
Date          Initials    Description
26-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_ReadARM740CoProcRegisters  (void)
{
   TyError ErrRet;
    
   if(usDiagnostics)
       ErrRet = DL_OPXD_Arm_Read_ARM740CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber,
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                     &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],              
                                                     &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],            
                                                     &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],             
                                                     &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],             
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0]);    
   else
   ErrRet = DL_OPXD_Arm_Read_ARM740CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],              
                                                 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],            
                                                 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],             
                                                 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],             
                                                 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0]);          
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
 
   //These registers have unpredictable bits so we zero them.
   CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0]  &= ARM740_CPREG_1_MASK;      
   CPRegArray[CP_REG_ARRAY_C2] [CP_REG_0]  &= ARM740_CPREG_2_MASK;      
   CPRegArray[CP_REG_ARRAY_C3] [CP_REG_0]  &= ARM740_CPREG_3_MASK;      
   CPRegArray[CP_REG_ARRAY_C5] [CP_REG_0]  &= ARM740_CPREG_5_MASK;      
   CPRegArray[CP_REG_ARRAY_C6] [CP_REG_0]  &= ARM740_CPREG_6_MASK;      
   //To store the IDcode for CheckTarget functionality
   ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];
   ulGlobalIDcode1 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1];
                                                                        
  return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM920_922CoProcessor
    Engineer: Suraj S
       Input: unsigned char uchRegisterAddress - CP register to read from 
              unsigned long *pulRegisterValue - pointer to register value 
      Output: TyError - Mid Layer error code
 Description: Reads the specified CP15 register
Date          Initials    Description
26-Jul-2007     SJ        Initial
****************************************************************************/
TyError CL_ReadARM920_922CoProcessor (unsigned char uchRegisterAddress, unsigned long *pulRegisterValue)
{
   TyError ErrRet;
   if(usDiagnostics)
   {  
   ErrRet = DL_OPXD_Arm_Read_ARM920_922CoProcessor(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, (unsigned long)uchRegisterAddress, pulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   }
   else
   {
   ErrRet = DL_OPXD_Arm_Read_ARM920_922CoProcessor(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, (unsigned long)uchRegisterAddress, pulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: CL_WriteARM920_922CoProcessor
     Engineer: Suraj S
        Input: unsigned char uchRegisterAddress - CP register to write to 
               unsigned long  ulRegisterValue - register value 
       Output: TyError - Mid Layer error code
  Description: Writes to specified CP15 register
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError CL_WriteARM920_922CoProcessor (unsigned char uchRegisterAddress, unsigned long ulRegisterValue)
{
   TyError ErrRet;
   if(usDiagnostics)
   {  

   ErrRet = DL_OPXD_Arm_Write_ARM920_922CoProcessor(tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, (unsigned long)uchRegisterAddress, ulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   }
   else
   {  
   ErrRet = DL_OPXD_Arm_Write_ARM920_922CoProcessor(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, (unsigned long)uchRegisterAddress, ulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
   }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM920_922CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM920T / ARM922T
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError CL_ReadARM920_922CoProcRegisters  (void)
{
   TyError ErrRet;
   unsigned long ulTemp;

	if(usDiagnostics)
	{
		ErrRet = DL_OPXD_Arm_Read_ARM920_922CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 
														 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
														 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],   
														 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
														 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C10] [CP_REG_1],
														 &CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
		//To store the IDcode for CheckTarget functionality
		ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];
		ulGlobalIDcode1 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1];
	}
	else
	{
	   ErrRet = CL_ReadARM920_922CoProcessor (ARM920_922_CP_REG_C1_CONTROL, &ulTemp);
	   if (ErrRet != ML_ERROR_NO_ERROR)
		  return ErrRet;

	   // Is alignment fault enable turned on ?
	   if (ulTemp & 0x2)
		  {
		  ulTemp &= 0xFFFFFFFD;
		  ErrRet = CL_WriteARM920_922CoProcessor (ARM920_922_CP_REG_C1_CONTROL, ulTemp);
		  if (ErrRet != ML_ERROR_NO_ERROR)
			 return ErrRet;
		  bGlobalReenableAlignFault = TRUE;
		  }
	   else
		  {
		  bGlobalReenableAlignFault = FALSE;
		  }

	   ErrRet = DL_OPXD_Arm_Read_ARM920_922CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
														 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
														 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],   
														 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
														 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1],          
														 &CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0],          
														 &CPRegArray[CP_REG_ARRAY_C10] [CP_REG_1],
														 &CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
   
	//These registers have unpredictable bits so we zero them.
	CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0] &= ARM920_922_CPREG_1_MASK; 
	CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0] &= ARM920_922_CPREG_2_MASK; 
	CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_1] &= ARM920_922_CPREG_2_MASK; 
	CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0] &= ARM920_922_CPREG_5_MASK; 
	CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1] &= ARM920_922_CPREG_5_MASK; 
	CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0] &= ARM920_922_CPREG_9_MASK; 
	CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1] &= ARM920_922_CPREG_9_MASK; 
	CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0] &= ARM920_922_CPREG_10_MASK; 
	CPRegArray[CP_REG_ARRAY_C10] [CP_REG_1] &= ARM920_922_CPREG_10_MASK; 
	}

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: CL_WriteARM926CoProcessor
     Engineer: Suraj S
        Input: unsigned char uchRegisterAddress - CP register to write to 
               unsigned long  ulRegisterValue - register value 
       Output: TyError - Mid Layer error code
  Description: Writes to specified CP15 register
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_WriteARM926CoProcessor (unsigned char uchRegisterAddress, unsigned long ulRegisterValue)
{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Write_ARM926CoProcessor(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, (unsigned long)uchRegisterAddress, ulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM926CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM926EJ-S
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError CL_ReadARM926CoProcRegisters  (void)
{
   TyError ErrRet;

   if(usDiagnostics)
   ErrRet = DL_OPXD_Arm_Read_ARM926CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],           
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_2],           
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
                                                 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
                                                 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_2],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_3],          
                                                 &CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   else
   ErrRet = DL_OPXD_Arm_Read_ARM926CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],           
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_2],           
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
                                                 &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
                                                 &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_2],          
                                                 &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_3],          
                                                 &CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0],          
                                                 &CPRegArray[CP_REG_ARRAY_C13] [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   //These registers have unpredictable bits so we zero them.
   CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0] &= ARM926_CPREG_2_MASK;      
   CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0] &= ARM926_CPREG_5_MASK;      
   CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0] &= ARM926_CPREG_9_LDOWN_MASK;
   CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1] &= ARM926_CPREG_9_LDOWN_MASK;
   CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_2] &= ARM926_CPREG_9_TLB_MASK;  
   CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_3] &= ARM926_CPREG_9_TLB_MASK;  
   CPRegArray[CP_REG_ARRAY_C10] [CP_REG_0] &= ARM926_CPREG_10_MASK;     

   //To store the IDcode for CheckTarget functionality
   ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];
   ulGlobalIDcode1 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1];
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM940_946CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM940T / ARM946E-S
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError CL_ReadARM940_946CoProcRegisters  (void)
{
   TyError ErrRet;

   if(usDiagnostics)
   ErrRet = DL_OPXD_Arm_Read_ARM940_946CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],   
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_2],   
                                                     &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
                                                     &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_1],          
                                                     &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_1],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_2],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_3],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_4],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_5],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_6],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_7],          
                                                     &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1]);
   else
   ErrRet = DL_OPXD_Arm_Read_ARM940_946CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],   
                                                     &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_2],   
                                                     &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],           
                                                     &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C2]  [CP_REG_1],          
                                                     &CPRegArray[CP_REG_ARRAY_C3]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C5]  [CP_REG_1],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_1],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_2],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_3],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_4],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_5],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_6],          
                                                     &CPRegArray[CP_REG_ARRAY_C6]  [CP_REG_7],          
                                                     &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0],          
                                                     &CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

   //These registers have unpredictable bits so we zero them.
   CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_0] &= ARM940_946_CPREG_9_MASK; 
   CPRegArray[CP_REG_ARRAY_C9]  [CP_REG_1] &= ARM940_946_CPREG_9_MASK; 

   //To store the IDcode for CheckTarget functionality
   ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];
   ulGlobalIDcode1 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1];
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM966CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM966E-S
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError CL_ReadARM966CoProcRegisters  (void)

{
   TyError ErrRet;

   if(usDiagnostics)
   ErrRet = DL_OPXD_Arm_Read_ARM966CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],  
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_1],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_2],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_3],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_4]);
   else
   ErrRet = DL_OPXD_Arm_Read_ARM966CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0],  
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_1],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_2],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_3],
                                                 &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_4]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
   //To store the IDcode for CheckTarget functionality
   ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_ReadARM968CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM968E-S
Date           Initials    Description
24-Mar-2008      SJ        Initial
****************************************************************************/
TyError CL_ReadARM968CoProcRegisters  (void)

{
   TyError ErrRet;

   if(usDiagnostics)
   ErrRet = DL_OPXD_Arm_Read_ARM968CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0]);
   else
   ErrRet = DL_OPXD_Arm_Read_ARM968CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
                                                 &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
                                                 &CPRegArray[CP_REG_ARRAY_C1]  [CP_REG_0]);
   ulGlobalScanChain = 0x1;
   if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
   //To store the IDcode for CheckTarget functionality
   ulGlobalIDcode0 = CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0];

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_WriteARM940_946CoProcessor
    Engineer: Suraj S
        Input: unsigned char uchRegisterAddress - CP register to write to 
               unsigned long  ulRegisterValue - register value 
       Output: TyError - Mid Layer error code
  Description: Writes to specified CP15 register
Date           Initials    Description
01-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_WriteARM940_946CoProcessor  (unsigned char uchRegisterAddress, unsigned long  ulRegisterValue)
{
   TyError ErrRet;
    
   ErrRet = DL_OPXD_Arm_Write_ARM940_946CoProcessor(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, (unsigned long)uchRegisterAddress, ulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
 
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_CleanandDisable940_946Caches
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Cleans ARM946 Instruction and Data caches.
Date          Initials    Description
01-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_CleanandDisable940_946Caches (void)
{
   TyError ErrRet;   
   
   boolean bInvalidateICache     = FALSE;
   boolean bInvalidateDCache     = FALSE;
   unsigned long ulCP1Value = 0;
   
   //If the instruction cache is enabled invalidate it. 
   //This is done in case the location for the handler is in cache.
   //just to be safe...
   bInvalidateICache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM940_946_ICACHE_ON) == ARM940_946_ICACHE_ON);
   bInvalidateDCache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM940_946_DCACHE_ON) == ARM940_946_DCACHE_ON);


   //If the instruction cache is enabled invalidate it.
   //This is done in case the location for the handler is in cache.
   //just to be safe...
   if (bInvalidateICache)
      {
      ErrRet = CL_WriteARM940_946CoProcessor (ARM940_946_CP_REG_C7_FLUSH_ICACHE, 0);
      if (ErrRet != ML_ERROR_NO_ERROR)
        return ErrRet;
      }

   //If the data cache is enabled clean and invalidate it.
   if (bInvalidateDCache)
      {
      switch (tyProcessorConfig.tyCoProcessorType)
         {
         case ARM940T_CP: 
            ErrRet = CL_Clean940DataCache ();
            if (ErrRet != ML_ERROR_NO_ERROR)
              return ErrRet;
            break;
         case ARM946ES_CP:
            ErrRet = CL_Clean946DataCache ();
            if (ErrRet != ML_ERROR_NO_ERROR)
              return ErrRet;
            break;
         default:
            //TODO :ASSERT_NOW();
            break;
         }

      //If the instruction cache is enabled invalidate it again.
      if (bInvalidateICache)
         {
         ErrRet = CL_WriteARM940_946CoProcessor (ARM940_946_CP_REG_C7_FLUSH_ICACHE, 0);
         if (ErrRet != ML_ERROR_NO_ERROR)
           return ErrRet;
         }
      }

   if (bInvalidateICache | bInvalidateDCache)
      {
      // Turn off Caches.
      ulCP1Value = (CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM940_946_CACHE_OFF);
      
      //Write value to CoProcessor register.
      ErrRet = CL_WriteARM940_946CoProcessor (ARM940_946_CP_REG_C1_CONTROL, ulCP1Value);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_Clean940DataCache
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Writes back dirty data to target memory, then invalidates the 
              cache.
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_Clean940DataCache (void)

{
   //Checks for data that has changed since the data was read from memory
   //then writes the current values back to memory.
   //The data cache is then invalidated.
   
   TyError ErrRet;
   
   unsigned long pulClean940DataCache[]=
   {
   0xE1A00000,      /*  NOP										           */         
   0xE3A01000,      /*  LDR     R1, =START  ;Holds current line     */
   0xE3A00000,      /*  LDR     R0, =START  ;Holds Current Segment  */
   0xE1812000,      /*  ORR     R2, R1, R0							     */
   0xEE072F5E,      /*  MCR     p15,0,R2,c7,c14,2					     */
   0xE2800010,      /*  ADD     R0, R0, #0x10						     */
   0xE3500040,      /*  CMP     R0, #0x40							        */     
   0x1AFFFFFA,      /*  BNE     Inner_Loop							     */
   0xE2811640,      /*  ADD     R1, R1, #0x04000000				     */
   0xE3510000,      /*  CMP     R1, #0x0							        */     
   0x1AFFFFF6,      /*  BNE     Outer_Loop						        */
   0xE1A00000,      /*  NOP										           */         
   0xE1A00000       /*  NOP										           */         
   };
   unsigned long ulNumOfBytes = sizeof (pulClean940DataCache);
   
   //Write Cache clean handler to target memory.
   ErrRet =  ML_WriteProc(pulClean940DataCache, ulCacheCleanStartAddr,
                          &ulNumOfBytes, RDIAccess_Data, FALSE, FALSE);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;
   
   ErrRet = CL_RunARM9TargetCacheFlush(ulNumOfBytes);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;
   
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_Clean946DataCache
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Writes back dirty data to target memory, then invalidates the 
              cache.
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_Clean946DataCache (void)

{
   //Checks for data that has changed since the data was read from memory
   //then writes the current values back to memory.
   //The data cache is then invalidated.
   
   TyError ErrRet;
   unsigned long ulCacheSize;
                              
   unsigned long pulClean946DataCache[]=
   {
   0xE1A00000,        /*NOP                                                 */
   0xE3A00000,        /*MOV     R3, 0x0   This value depends on cache size. */ 
   0xE3A01000,        /*MOV     R1, #0x0                                    */ 
   0xE3A00000,        /*MOV     R0, #0x0                                    */ 
   0xE1812000,        /*ORR     R2, R1, R0                                  */ 
   0xEE072F5E,        /*MCR     p15,0,R2,c7,c14,2                           */ 
   0xE2800020,        /*ADD     R0,R0,#0x20                                 */ 
   0xE1500003,        /*CMP     R0, R3                                      */ 
   0x1AFFFFFA,        /*BNE     inner_loop                                  */ 
   0xE2811440,        /*ADD     R1, R1, #0x40000000                         */ 
   0xE3510000,        /*CMP     R1, #0x0                                    */ 
   0x1AFFFFF6,        /*BNE     outer_loop                                  */ 
   0xE1A00000,        /*MOV     R0, R0                                      */ 
   };
   unsigned long ulNumOfBytes = sizeof (pulClean946DataCache);
   

   ulCacheSize = ((CPRegArray[CP_REG_ARRAY_C0] [CP_REG_1] & ARM946_DCACHE_SIZE_MASK) >> 0x12);

   //The cache sizes need different values in R3 of the cache clean routine.
   switch (ulCacheSize)
      {
      case SIZE_1MB:
         pulClean946DataCache [1] =  0xE3A03701;
         break;
      case SIZE_512KB:
         pulClean946DataCache [1] =  0xE3A03802;
         break;
      case SIZE_256KB:
         pulClean946DataCache [1] =  0xE3A03801;
         break;
      case SIZE_128KB:
         pulClean946DataCache [1] =  0xE3A03902;
         break;
      case SIZE_64KB:
         pulClean946DataCache [1] =  0xE3A03901;
         break;
      case SIZE_32KB:
         pulClean946DataCache [0] =  0xE3A03A02;
         break;
      case SIZE_16KB:
         pulClean946DataCache [1] =  0xE3A03A01;
         break;
      case SIZE_8KB:
         pulClean946DataCache [1] =  0xE3A03B02;
         break;
      case SIZE_4KB:
         pulClean946DataCache [1] =  0xE3A03B01;
         break;
      case SIZE_0KB:
      default:
         //Do not clean if there is no cache....
         return RDIError_NoError;
      }
   
   //Write Cache clean handler to target memory.
   ErrRet =  ML_WriteProc(pulClean946DataCache, ulCacheCleanStartAddr,
                          &ulNumOfBytes, RDIAccess_Data, FALSE, FALSE);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;
     
   ErrRet = CL_RunARM9TargetCacheFlush(ulNumOfBytes);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_RunARM9TargetCacheFlush
    Engineer: Suraj S
       Input: Length of the cache clean routine.
      Output: TyError - Mid Layer error code 
 Description: Restarts the core to run cache clean / invalidate routines
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_RunARM9TargetCacheFlush(unsigned long ulNumberOfBytes)
{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_RunARM9TargetCacheFlush(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
                                                ulCacheCleanStartAddr,
                                               (ulCacheCleanStartAddr + ulNumberOfBytes));

   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));

   //The Diskware function leaves the core in scan chain 2
   ulGlobalScanChain = 0x2;

   return ML_ERROR_NO_ERROR;

}

/****************************************************************************
    Function: CL_CleanandDisable926Caches
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code 
 Description: Cleans Instruction and Data cache.Don't write to control reg 
              unless cache or MMU are on.Turn of Caches first so we don't 
              have to flush Icache after cleaning D cache.
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_CleanandDisable926Caches (void)
{
   TyError        ErrRet;   
   boolean        bInvalidateICache    = FALSE;
   boolean        bInvalidateDCache    = FALSE;
   unsigned long  ulControlValue       = 0x0;

   //If the instruction cache is enabled invalidate it. 
   //This is done in case the location for the handler is in cache.
   //just to be safe...
   bInvalidateICache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM926_ICACHE_ON) == ARM926_ICACHE_ON);
   bInvalidateDCache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM926_DCACHE_ON) == ARM926_DCACHE_ON);

   if (bInvalidateICache | bInvalidateDCache)
      {
      // Turn off the Caches.
      ulControlValue  = (CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM926_CACHE_OFF);
      
      ErrRet = CL_WriteARM926CoProcessor (ARM926_CP_REG_C1_CONTROL, ulControlValue);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }


   if (bInvalidateICache)
      {
      ErrRet = CL_WriteARM926CoProcessor (ARM926_INVALIDATE_ICACHE, 0x0);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }
   
   //Commented for Optimisation
   // Select scan chain 1...
/*   ErrRet = ML_Select_Scan_Chain(1,1);
   if (ErrRet != ML_ERROR_NO_ERROR)
      {
      ErrRet = ML_Select_Scan_Chain(1,1);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }*/


   //If the data cache is enabled clean and invalidate it.
   if (bInvalidateDCache)
      {
      ErrRet = CL_Clean926DataCache();
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;

      // Select scan chain 1...
      ErrRet = ML_Select_Scan_Chain(1,1);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }


   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_Clean926DataCache
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Writes back dirty data to target memory, then invalidates the 
              cache.
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_Clean926DataCache (void)
{
   //Checks for data that has changed since the data was read from memory
   //then writes the current values back to memory.
   //The data cache is then invalidated.
   
   TyError ErrRet;

   unsigned long pulClean926DataCache[]=
   {
   0xE1A00000,        /*NOP                                         */
   0xEE17FF7E,        /*MRC p15, 0, r15, c7, c14, 3                 */
   0x1AFFFFFD,        /*BNE LOOP                                    */
   0xE1A00000         /*NOP                                         */
   };
   
   unsigned long ulNumOfBytes    = sizeof (pulClean926DataCache);

   //Write Cache clean handler to target memory.
   ErrRet =  ML_WriteProc(pulClean926DataCache, ulCacheCleanStartAddr,
                          &ulNumOfBytes, RDIAccess_Data, FALSE, FALSE);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;
   
   ErrRet = CL_RunARM9TargetCacheFlush(ulNumOfBytes);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_CleanandDisable920_922Caches
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Cleans Instruction and data cache. Don't write to control reg 
              unless cache or MMU are on
Date          Initials    Description
15-Aug-2007     SJ        Initial
****************************************************************************/
TyError CL_CleanandDisable920_922Caches (void)
{

   TyError ErrRet;   
   boolean bInvalidateICache     = FALSE;
   boolean bInvalidateDCache     = FALSE;
   unsigned long ulControlValue  = 0x0;

   //If the instruction cache is enabled invalidate it. 
   //This is done in case the location for the handler is in cache.
   //just to be safe...
   bInvalidateICache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM920_ICACHE_ON) == ARM920_ICACHE_ON);
   bInvalidateDCache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM920_DCACHE_ON) == ARM920_DCACHE_ON);


   if (bInvalidateICache | bInvalidateDCache)
      {
      //Turn off the Caches.
      ulControlValue  = (CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM920_CACHE_OFF);

      ErrRet = CL_WriteARM920_922CoProcessor (ARM920_922_CP_REG_C1_CONTROL, ulControlValue);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   if (bInvalidateICache)
      {
      ErrRet = CL_InvalidateARM920_922ICache();
      ulGlobalScanChain = 0x1;
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;

      //Update the global variable
      ulGlobalScanChain = 0x1;
      }
   
   //If the data cache is enabled clean and invalidate it.
   if (bInvalidateDCache)
      {
      ErrRet = CL_Clean920_922DataCache ();
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_InvalidateARM920_922ICache
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Invalidates ARM920/922 Instrcution Cache
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_InvalidateARM920_922ICache(void)
{
   TyError ErrRet;
    
   ErrRet = DL_OPXD_Arm_InvalidateARM920_922ICache(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
 
   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_Clean920_922DataCache
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Writes back dirty data to target memory, then invalidates the 
              cache.
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_Clean920_922DataCache (void)
{
   //Checks for data that has changed since the data was read from memory
   //then writes the current values back to memory.
   //The data cache is then invalidated.
   
   TyError ErrRet;

   unsigned long pulClean920_922DataCache[]=
   {
   0xE1A00000,        /*NOP                                         */
   0xE3A00000,        /*LDR     R0, =START                          */
   0xE3A01000,        /*LDR     R1, =START  ;Holds Current Segment  */
   0xE3A02000,        /*LDR     R2, =START  ;Holds Current Index    */
   0xE3A05000,        /*LDR     R5, =START                          */
   0xE3A0603F,        /*LDR     R6, =MAX_INDEX                      */
   0xE3A07007,        /*LDR     R7, =MAX_SEGMENT                    */
   0xE1A00281,        /*MOV     R0,R1, lsl #0x5                     */
   0xE1A05D02,        /*MOV     R5,R2, lsl #0x1A                    */
   0xE0800005,        /*ADD     R0, R0, R5                          */
   0xEE070F5E,        /*MCR     p15,0,R0,c7,c14,2                   */
   0xE1520006,        /*CMP     R2, R6                              */
   0xE2822001,        /*ADD     R2, R2, #0x1                        */
   0xBAFFFFF8,        /*BLT     Index                               */
   0xE3A02000,        /*LDR     R2, = START                         */
   0xE1510007,        /*CMP     R1, R7                              */
   0xE2811001,        /*ADD     R1, R1, #0x1                        */
   0xBAFFFFF4,        /*BLT     Segment                             */
   0xE1A00000,        /*NOP                                         */
   0xEAFFFFFD         /*B Loop                                      */
   };
   
   unsigned long ulNumOfBytes    = sizeof (pulClean920_922DataCache);

   //There are only four segments in an ARM922 cache.
   if (tyProcessorConfig.tyCoProcessorType == ARM922T_CP)
      pulClean920_922DataCache[5] = 0xE3A07003;


   //Write Cache clean handler to target memory.
   ErrRet =  ML_WriteProc(pulClean920_922DataCache, ulCacheCleanStartAddr,
                          &ulNumOfBytes, RDIAccess_Data, FALSE, FALSE);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;
   
   ErrRet = CL_RunARM9TargetCacheFlush(ulNumOfBytes - 0x4);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_FlushandDisableARM740CacheAndTTB
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Flushes the instruction and data cache.Don't write to control 
              reg unless cache or MMU are on.
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_FlushandDisableARM740CacheAndTTB (void)
{
   TyError ErrRet;   
   
   boolean bInvalidateCache     = FALSE;
   boolean bInvalidateTLB       = FALSE;
   unsigned long ulCP1Value     = 0;
   
   //If the instruction cache is enabled invalidate it. 
   //This is done in case the location for the handler is in cache.
   //just to be safe...
   bInvalidateCache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM720_CACHE_ON) == ARM720_CACHE_ON);
   bInvalidateTLB   = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM720_MMU_ON) == ARM720_MMU_ON);

   if (bInvalidateCache)
      {
      ErrRet = CL_WriteARM720_740CoProcessor (ARM720_CP_REG_C7_INVALIDATE_CACHE_ALL, 0x0);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   if (bInvalidateTLB)
      {
      ErrRet = CL_WriteARM720_740CoProcessor (ARM720_CP_REG_C8_INVALIDATE_TTB_ALL, 0x0);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   if (bInvalidateCache | bInvalidateTLB)
      {
      // Turn off the CACHE.
      ulCP1Value = (CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM720_CACHE_OFF);
      
      ErrRet = CL_WriteARM720_740CoProcessor (ARM720_CP_REG_C1_WRITE_CONTROL_REG, ulCP1Value);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   ErrRet = ML_Select_Scan_Chain(1,1);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_FlushandDisableARM720Cache
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Flushes the instruction and data cache. Don't write to control 
              reg unless cache or MMU are on.
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_FlushandDisableARM720CacheAndTTB (void)
{
   TyError ErrRet;   
   
   boolean bInvalidateCache     = FALSE;
   boolean bInvalidateTLB       = FALSE;
   unsigned long ulCP1Value     = 0;
   
   //If the instruction cache is enabled invalidate it. 
   //This is done in case the location for the handler is in cache.
   //just to be safe...
   bInvalidateCache = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM720_CACHE_ON) == ARM720_CACHE_ON);
   bInvalidateTLB   = ((CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM720_MMU_ON) == ARM720_MMU_ON);

   if (bInvalidateCache)
      {
      ErrRet = CL_WriteARM720_740CoProcessor (ARM720_CP_REG_C7_INVALIDATE_CACHE_ALL, 0x0);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }

   if (bInvalidateTLB)
      {
      ErrRet = CL_WriteARM720_740CoProcessor (ARM720_CP_REG_C8_INVALIDATE_TTB_ALL, 0x0);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
      }


   if (bInvalidateCache | bInvalidateTLB)
      {
      // Turn off the CACHE.
      ulCP1Value = (CPRegArray[CP_REG_ARRAY_C1] [CP_REG_0] & ARM720_CACHE_OFF);
      
      ErrRet = CL_WriteARM720_740CoProcessor (ARM720_CP_REG_C1_WRITE_CONTROL_REG, ulCP1Value);
      if (ErrRet != ML_ERROR_NO_ERROR)
         return ErrRet;
   }

   ErrRet = ML_Select_Scan_Chain(1,1);
   if (ErrRet != ML_ERROR_NO_ERROR)
      return ErrRet;

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
     Function: CL_WriteARM720_740CoProcessor
     Engineer: Suraj S
        Input: unsigned long ulCPInstruction - CoProcessor Instruction 
               unsigned long  ulRegisterValue - CoProcessor Register value 
       Output: TyError - Mid Layer error code
  Description: Writes to specified CP15 register. The instruction passed as 
               parameter will tell to which CoProcessor register to write to.
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_WriteARM720_740CoProcessor (unsigned long ulCPInstruction, unsigned long ulRegisterValue)
{
   TyError ErrRet;

   ErrRet = DL_OPXD_Arm_Write_ARM720_740CoProcessor(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, ulCPInstruction , ulRegisterValue);
   if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
      return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));

   return ML_ERROR_NO_ERROR;
}

/****************************************************************************
    Function: CL_CleanandDisable1136_Caches
    Engineer: Roshan T R
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Cleans Instruction and data cache. Don't write to control reg 
              unless cache or MMU are on
Date          Initials    Description
29-Sep-2009   RTR         Initial
****************************************************************************/
TyError CL_CleanandDisable1136_Caches (void)
{

	TyError ErrRet;
	TyArmProcType TyProcType = ARM11;

	ErrRet = DL_OPXD_CleanandDisable1136_Caches(tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, TyProcType);


	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
Function: CL_ReadARM1136CoProcRegisters
    Engineer: Suraj S
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM1136JF-S
Date           Initials    Description
21-Aug-2009      SJ        Initial
****************************************************************************/
TyError CL_ReadARM1136CoProcRegisters  (void)

{
	TyError ErrRet;

/*
   //if(usDiagnostics)
   //ErrRet = DL_OPXD_Arm_Read_ARM1136CP_Registers (tyDiagDevHandle, tyTargetConfig.ulDebugCoreNumber, 
   //                                              &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
   //                                              &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1],
   //                                              &CPRegArray[CP_REG_ARRAY_C15] [CP_REG_0],
   //											 );
   //ErrRet = DL_OPXD_Arm_Read_ARM1136CP_Registers (tyDiagDevHandle,
   //													tyTargetConfig.ulDebugCoreNumber, 
   //													&CPRegArrayArm11);
   //else
   //ErrRet = DL_OPXD_Arm_Read_ARM1136CP_Registers (tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber, 
   //                                              &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_0],
   //                                              &CPRegArray[CP_REG_ARRAY_C0]  [CP_REG_1]);
*/   
	if(usDiagnostics)
	{
		ErrRet = DL_OPXD_Arm_Read_ARM1136CP_Registers (tyDiagDevHandle,
														tyTargetConfig.ulDebugCoreNumber, 
														CPRegArrayArm11);
	}
	else
	{
		ErrRet = DL_OPXD_Arm_Read_ARM1136CP_Registers (tyCurrentDevice,
														tyTargetConfig.ulDebugCoreNumber, 
														CPRegArrayArm11);
	}

   
	ulGlobalScanChain = 0x1;
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));
	//To store the IDcode for CheckTarget functionality
	ulGlobalIDcode0 = CPRegArrayArm11[0];
	ulGlobalIDcode1 = CPRegArrayArm11[1];

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
Function: CL_WriteARM1136CoProcRegisters
    Engineer: Roshan T R
       Input: NONE
      Output: TyError - Mid Layer error code
 Description: Reads Co-processor registers from an ARM1136JF-S
Date           Initials    Description
10-Oct-2009    RTR         Initial
****************************************************************************/
TyError CL_WriteARM1136CoProcRegisters  (void)

{
	TyError ErrRet;

	ErrRet = DL_OPXD_Arm_Write_ARM1136CP_Registers (tyCurrentDevice,
													tyTargetConfig.ulDebugCoreNumber,
													0,
													CPRegArrayArm11,sizeof(CPRegArrayArm11));
   
	if (ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return (ML_Convert_DrvLayerErr_ToMidLayerErr (ErrRet));

	return ML_ERROR_NO_ERROR;
}

/****************************************************************************
Function: CL_CleanandDisableCortexACaches
    Engineer: Jeenus C.K.
	   Input: unsigned long ulStartAddr - Start address
			  unsigned long ulEndAddr   - 
      Output: TyError - Mid Layer error code
 Description: To clean the invalidate and clean instruction and data cache.
Date           Initials    Description
14-May-2010		JCK			Initial
****************************************************************************/
TyError CL_CleanandDisableCortexACaches( unsigned long ulStartAddr, 
										   unsigned long ulEndAddr )
{
	TyError ErrRet;
	
	ErrRet = DL_OPXD_Arm_CortexACacheFlush( tyCurrentDevice, tyTargetConfig.ulDebugCoreNumber,
											ulCacheCleanStartAddr,
											ulStartAddr,
											ulEndAddr);

	if(ErrRet != DRVOPXD_ERROR_NO_ERROR)
		return(ML_Convert_DrvLayerErr_ToMidLayerErr(ErrRet));
	
   return ML_ERROR_NO_ERROR;
	
}
/****************************************************************************
    Function: CL_QueryCP15Cache
    Engineer: Suraj S
       Input: NONE
      Output: boolean - Returns TRUE or FALSE
 Description: Returns TRUE if target has coprocessor CP15 and 
              a Harvard architeture
Date           Initials    Description
31-Jul-2007      SJ        Initial
****************************************************************************/
boolean CL_QueryCP15Cache(void)
{
   boolean bCP15Cache;

   switch (tyProcessorConfig.tyCoProcessorType)
      {
      default:
      case NO_COPROCESSOR:
      case ARM720T_CP:
      case ARM966ES_CP:
      case ARM968ES_CP:
         bCP15Cache = FALSE;
         break;
      case ARM920T_CP:
      case ARM922T_CP:
      case ARM926EJS_CP:
      case ARM940T_CP:
      case ARM946ES_CP:
	case ARM1136JFS_CP:
	case ARM1156T2S_CP:
	case ARM1176JZS_CP:
	case ARM11MP_CP:
		bCP15Cache = TRUE;
		break;
      }

   return bCP15Cache;
}

/****************************************************************************
    Function: CL_SetCP15CacheSelection
    Engineer: Suraj S
       Input: unsigned long CacheSelection - Value of bits 5..7 in MCR/MRC 
              accesses to registers 2, 5, 6 and 8. In effect, this is 0 to 
              control the data cache, and 1 to control the instruction cache
      Output: NONE
 Description: This function helps the Debugger to set the currently selected 
              Cache using RDI_InfoProc#RDIInfo_SetCP15CacheSelected
Date           Initials    Description
31-Jul-2007      SJ        Initial
****************************************************************************/
void CL_SetCP15CacheSelection(unsigned long CacheSelection)
{
   ulGlobalCP15CacheSelection = CacheSelection;
}

/****************************************************************************
    Function: CL_GetCP15CacheSelection
    Engineer: Suraj S
       Input: unsigned long *CacheSelection - Pointer to the read data
      Output: NONE
 Description: This function helps the Debugger to get the currently selected 
              Cache using RDI_InfoProc#RDIInfo_GetCP15CacheSelected
Date           Initials    Description
20-Aug-2007      SJ        Initial
****************************************************************************/
void CL_GetCP15CacheSelection(unsigned long *CacheSelection)
{
   *CacheSelection = ulGlobalCP15CacheSelection;
}

/****************************************************************************
    Function: CL_QueryCP15MemoryArea
    Engineer: Suraj S
       Input: NONE
      Output: boolean - TRUE if CP15 Memory area is there else FALSE
 Description: This function helps the Debugger to check the currently selected 
              processor has CoProcessor memory area using 
              RDI_InfoProc#RDIInfo_CP15CurrentMemoryArea
Date           Initials    Description
20-Aug-2007      SJ        Initial
****************************************************************************/
boolean CL_QueryCP15MemoryArea(void)
{
   boolean bCP15MemoryArea;

   switch (tyProcessorConfig.tyCoProcessorType)
      {
      default:
      case NO_COPROCESSOR:
      case ARM720T_CP:
      case ARM920T_CP:
      case ARM922T_CP:
      case ARM926EJS_CP:
      case ARM966ES_CP:
      case ARM968ES_CP:
	case ARM1136JFS_CP:
	case ARM1156T2S_CP:
	case ARM1176JZS_CP:
	case ARM11MP_CP:
         bCP15MemoryArea = FALSE;
         break;
      case ARM940T_CP:
      case ARM946ES_CP:
         bCP15MemoryArea = TRUE;
         break;
      }

   return bCP15MemoryArea;
}

/****************************************************************************
    Function: CL_SetCP15MemoryArea
    Engineer: Suraj S
       Input: unsigned long MemoryArea - The CP memory area passed by the debugger
      Output: NONE
 Description: This function helps the Debugger to set for the currently selected 
              processor its CoProcessor memory area using 
              RDI_InfoProc#RDIInfo_SetCP15CurrentMemoryArea
Date           Initials    Description
20-Aug-2007      SJ        Initial
****************************************************************************/
void CL_SetCP15MemoryArea(unsigned long MemoryArea)
{
   ulGlobalCP15MemoryArea = MemoryArea;
}

/****************************************************************************
     Function: CL_GetCP15MemoryArea
     Engineer: Suraj S
        Input: unsigned long *MemoryArea - pointer to the read data
       Output: NONE
  Description: This function helps the Debugger to get for the currently selected 
               processor its CoProcessor memory area using 
               RDI_InfoProc#RDIInfo_SetCP15CurrentMemoryArea
Date           Initials    Description
20-Aug-2007      SJ        Initial
****************************************************************************/
void CL_GetCP15MemoryArea(unsigned long *MemoryArea)
{
   *MemoryArea = ulGlobalCP15MemoryArea;
}

/****************************************************************************
    Function: CL_GetARM9RestartAddress
    Engineer: Suraj S
       Input: unsigned long *arg1 - Pointer to return Address of memory                                               
      Output: TyError - Mid Layer error code. Only needed for ARM920_922 & ARM946. 
              All others return unimplemented 
 Description: Returns the target memory location for data cache clean handler.
              RDI defines this as 128 Bytes of target memory that is not used by the
              target code.
Date           Initials    Description
21-Aug-2007      SJ        Initial
****************************************************************************/
TyError CL_GetARM9RestartAddress(unsigned long *arg1)
{
   TyError ErrRet;

   switch (tyProcessorConfig.tyCoProcessorType)
      {
      default:
      case NO_COPROCESSOR:
      case ARM720T_CP:      
      case ARM966ES_CP:
      case ARM968ES_CP:
         ErrRet = ML_Error_UnimplementedType;
         break;
      case ARM920T_CP:
      case ARM922T_CP:
	  case ARM926EJS_CP:
      case ARM940T_CP:
      case ARM946ES_CP:
         *arg1 = ulCacheCleanStartAddr;
         ErrRet = ML_ERROR_NO_ERROR;
         break;
		case ARM1136JFS_CP:
	case ARM1156T2S_CP:
	case ARM1176JZS_CP:
	case ARM11MP_CP:
		ErrRet = ML_ERROR_NO_ERROR;
			break;
      }

   return ErrRet;
}
