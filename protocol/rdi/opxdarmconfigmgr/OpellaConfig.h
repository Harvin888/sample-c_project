/******************************************************************************
  Module: OpellaConfig.h
  Engineer: Jiju George T
  Description: This file contains the declaration of COpellaConfig class.
  Date           Initials    Description
  2007-07-26       JG          Initial
  2008-05-21       SJ          Added function for enabling Safe Non Vector Address
  2010-08-12       SJ		   Modified to avoid memory leaks.
******************************************************************************/

#ifndef _OPELLA_CONFIG_H_
#define _OPELLA_CONFIG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef __LINUX
typedef int                 BOOL;
#endif //__LINUX

enum Endian
{
    BigEndian=0,
    LittleEndian=1
};

enum JTAGClockFreqMode
{
    Fixed=0,
    Adaptive=1,
    Automatic=2
};

enum TargetResetMode
{
    NoHardReset = 0,
    HardResetAndDelay=1,
    HardResetAndHaltAtRV=2,
    //HardResetAndGo=3,
    HotPlug=3,
	CoreOnly=4,
	CoreAndPeripherals=5
};

enum SemiHostingType
{
    Disabled = 0,
    StandardDCC = 1,
    RealMonitorDCC = 2,
    UsingSWI = 3,
    ViaDCC = 4
};

enum SemiHostingSupport
{
    UsingARMCompiler = 0,
    UsingGNUARMCompiler =1
};

typedef struct _TySemiHostingParams
{
    unsigned long ulTopOfMemory;
    unsigned long ulVectorTrapAddress;
    unsigned long ulDCCHandler;
    unsigned long ulARMSWIValue;
    unsigned long ulThumbSWIValue;
}TySemiHostingParams;


/****************************************************************************
  Class	 	 : COpellaConfig
  Engineer	 : Jiju George T
  Input      : void
  Output     : void
  Description: Holds the configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
  2010-08-12       SJ		   Modified to avoid memory leaks.
****************************************************************************/


class COpellaConfig
{
public:
    COpellaConfig();
    COpellaConfig( const char* szConfigFile );
    COpellaConfig( RDI_ConfigPointer Config );
    virtual ~COpellaConfig();

    // Misc
	 bool IsDefault();
    bool IsValid();
    COpellaConfig* Clone();

    // For Device Settings
    unsigned char GetIRLength( int nCore );
    void SetIRLength( int nCore, unsigned char ucLength );
    int GetSelectedCore();
    void SetSelectedCore( int nCore );
    //char* GetSelectedDevice();	
	void GetSelectedDevice( char *szSelectedDevice );
    void SetSelectedDevice( const char* szDeviceName );
	//char* GetProcessorName();
	void GetProcessorName(char *szProcessorName);
	void SetProcessorName( const char* szProcessorName );
    bool IsARMCore( int nCore );
    void SetCoreType( int nCore, unsigned char bArmCore );
    int GetNumberOfCores();
    void SetNumberOfCores( int nNumCores );
    Endian GetEndianness();
    void SetEndianness( Endian eEndian );
	 void SetDeviceRegIndex(unsigned int uiIndex); 	
	 unsigned int GetDeviceRegIndex();
	 void SetArchVersion( const char* szVesrsion);

    // For Target Connection
    //char* GetDebuggerName();
    void GetDebuggerName(char* szDebuggerName);
    void SetDebuggerName(const char* szDebuggerName);
    unsigned long GetOpellaUSBInstanceNumber();
    void SetOpellaUSBInstanceNumber( unsigned long ulInstanceNumber );
    JTAGClockFreqMode GetJTAGClockFreqMode();
    void SetJTAGClockFreqMode( JTAGClockFreqMode eJTAGClockFreqMode );
    int GetFixedJTAGFrequencyInKHz();
    void SetFixedJTAGFrequencyInKHz( int nFrequencyInKHz );
    unsigned long GetOpellaUSBProductID();
    void SetOpellaUSBProductID( unsigned long usProductID );
    bool IsOpellaUSBSerialNumberExists();
    //unsigned long  GetOpellaUSBSerialNumber();
    //char*  GetOpellaUSBSerialNumber();
	void GetOpellaUSBSerialNumber(char* szSerialNumber);
    //void SetOpellaUSBSerialNumber( unsigned long ulSerialNumber );
    void SetOpellaUSBSerialNumber( const char* szSerialNumber );
    

    // For Target Reset
    TargetResetMode GetTargetResetMode();
    void SetTargetResetMode( TargetResetMode eTargetResetMode );
    int GetHardResetDelay();
    void SetHardResetDelay( int nDelayInSec );
    float GetHotPlugVoltage();
    void SetHotPlugVoltage( float fVoltage );
    bool IsHaltCoreOnConnection();
    void HaltCoreOnConnection( BOOL bHalt );
    bool IsUsenTRST();
    void UsenTRST( BOOL bUse );
    bool IsSetDBGRQLow();
    void SetDBGRQLow( BOOL bSet );

    // For Advanced settings
    unsigned long GetCacheCleanStartAddress();
    void SetCacheCleanStartAddress( unsigned long ulAddress );
    unsigned long GetSafeNonVectorAddress();
    void SetSafeNonVectorAddress( unsigned long ulAddress );
    int GetTargetExecutionPollingRate();
    void SetTargetExecutionPollingRate( int nPollingRate );
	void EnableSafeNonVectorAddress( bool bEnable );	
    bool IsSafeNVAddressEnabled();
	void SetCacheInvalidationParam(BOOL bCacheInvalidationNeeded);
	BOOL GetCacheInvalidationParam( void);

    // For Semi Hosting
    SemiHostingType GetSemiHostingType();
    void SetSemiHostingType( SemiHostingType eSemiHostingType );
    SemiHostingSupport GetSemiHostingSupport();
    void SetSemiHostingSupport( SemiHostingSupport  eSemiHostingSupport );
    void GetSemiHostingParams( TySemiHostingParams &tySemiHostingParams );
    void SetSemiHostingParams( TySemiHostingParams &tySemiHostingParams );

    // For Diagnostics
    bool IsRDILoggingEnabled();
    void EnableRDILogging( bool bEnable );

    // For Saving
    bool Save( const char* szFileName );

    // For Shift IR/DR Clock Settings
    unsigned char GetShiftIRPreTCKs();
    void SetShiftIRPreTCKs( unsigned char ucClocks );
    unsigned char GetShiftIRPostTCKs();
    void SetShiftIRPostTCKs( unsigned char ucClocks );
    unsigned char GetShiftDRPreTCKs();
    void SetShiftDRPreTCKs( unsigned char ucClocks );
    unsigned char GetShiftDRPostTCKs();
    void SetShiftDRPostTCKs( unsigned char ucClocks );
	//char* GetArchVersion( void);
	void GetArchVersion( char *szDeviceArch );

    void SetDebugBaseAddress( unsigned int uiDebugBaseAddr);
    unsigned int GetDebugBaseAddress( void);
    void SetDebugApIndex( unsigned int uiDebugApIndex);
    unsigned int GetDebugApIndex( void);
    void SetMemoryApIndex( unsigned int uiMemApIndex);
    unsigned int GetMemoryApIndex( void);
    void SetCoresightCompliant(BOOL bCoresightComplianet);
    BOOL IsCoresightCompliant( void);
	void SetUseMemApForMemAccess( unsigned int uiUseMemApForMemAccess);
	unsigned int GetUseMemApForMemAccess( void);

	unsigned long GetTISubPortNumber( void);
    void SetTISubPortNumber(unsigned long ulTISubPortNum);
    BOOL IsTICore( void);
	void SetTICore(BOOL bTICore);
	void SetDapPcSupport(BOOL bDapPcSupport);
	BOOL IsDapPcSupported( void);

private:
    RDI_ConfigPointer m_RDIConfig;
};

#endif // _OPELLA_CONFIG_H_
