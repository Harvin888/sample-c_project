/******************************************************************************
  Module: OpellaConfig.cpp
  Engineer: Jiju George T
  Description: This file contains the implementation of COpellaConfig class.
  Date           Initials    Description
  2007-08-02       JG          Initial
  2008-05-21       SJ          Added function for enabling Safe Non Vector Address 
  2008-05-28       JCK         Added support for device register
  2010-08-12	   SJ		   Modified to avoid memory leaks	
******************************************************************************/

#ifdef __LINUX
   #include "../rdi/GUIDefs.h"
   #include "../rdi/toolconf.h"
   #include "../rdi/rdi.h"
   #include "../../../gdbserver/arm/gdboptarm.h"
   #include "OpellaConfig.h"
   #include <string.h>
   #define _T(x) x
#else
   #include "stdafx.h"
   #include "OpellaConfig.h"
#endif


//
// *** Tags corresponding to each configuration ***
//
#define TAG_DEBUGGER_NAME					"DEBUGGER_NAME"
// Device / Multi core related
#define TAG_NUMBER_OF_CORES					"NUMBER_OF_CORES"
#define TAG_CORE							"CORE"
#define TAG_SELECTED_DEVICE					"INTERNAL_NAME"
#define TAG_PROCESSOR_NAME					"PROCESSOR"
#define TAG_SELECTED_CORE					"DEBUG_CORE_NO"
#define TAG_IRLENGTH						"IRLENGTH"
#define TAG_ENDIANNESS						"ENDIANNESS"
#define TAG_DEVICE_REG_INDEX				"DEVICE_REG_INDEX"
// Shift IR/DR Clocks
#define TAG_SHIFT_IR_POST_TCKS				"SHIFT_IR_POST_TCKS"
#define TAG_SHIFT_IR_PRE_TCKS				"SHIFT_IR_PRE_TCKS"
#define TAG_SHIFT_DR_POST_TCKS				"SHIFT_DR_POST_TCKS"
#define TAG_SHIFT_DR_PRE_TCKS				"SHIFT_DR_PRE_TCKS"
// Reset related
#define TAG_TARGET_RESET_MODE				"TARGET_RESET_MODE"
#define TAG_HARD_RESET_STARTUP_DELAY		"STARTUP_DELAY"
#define TAG_HOT_PLUG_VOLTAGE				"TARGET_VOLTAGE"
#define TAG_HALT_CORE_ON_CONNECTION			"HALT_CORE_ON_CONNECTION"
#define TAG_USE_NTRST						"USE_NTRST"
#define TAG_SET_DBGRQLOW					"SET_DBGRQLOW"
// Target connection related
#define TAG_OPELLA_SERIAL_NUMBER			"OPELLAXD_SERIAL_NUMBER"
#define TAG_OPELLA_PRODUCT_ID				"OPELLA_USB_PRODUCT_ID"
#define TAG_OPELLA_INSTANCE_NUMBER			"OPELLA_USB_INSTANCE"
#define TAG_JTAG_CLOCK_FREQ_MODE			"JTAG_CLOCK_FREQ_MODE"
#define TAG_FIXED_JTAG_FREQ_IN_KHZ			"FIXED_JTAG_FREQ_IN_KHZ"
// Advanced
#define TAG_CACHE_CLEAN_START_ADDRESS		"DATA_CACHE_CLEAN_START_ADDRESS"
#define TAG_SAFE_NON_VECTOR_ADDRESS			"SAFE_NON_VECTOR_ADDRESS"
#define TAG_TARGET_EXE_POLLING_RATE			"EXECUTION_POLLING_RATE"
#define TAG_ENABLE_SAFE_NON_VECTOR_ADDRESS	"ENABLE_SAFE_NON_VECTOR_ADDRESS"
#define TAG_CACHE_INVALIDATION_NEEDED		"CACHE_INVALIDATION_NEEDED"
// Semi-hosting related
#define TAG_SEMI_HOSTING_TYPE				"SEMI_HOSTING_TYPE"
#define TAG_SEMI_HOSTING_SUPPORT			"SEMI_HOSTING_SUPPORT"
#define TAG_SH_TOP_OF_MEMORY				"SH_TOP_OF_MEMORY"
#define TAG_SH_VECTOR_TRAP_ADDRESS			"SH_VECTOR_TRAP_ADDRESS"
#define TAG_SH_DCC_HANDLER					"SH_DCC_HANDLER"
#define TAG_SH_ARM_SWI_VALUE				"SH_ARM_SWI_VALUE"
#define TAG_SH_THUMB_SWI_VALUE				"SH_THUMB_SWI_VALUE"
// RDI Logging
#define TAG_RDI_LOGGING_ENABLED				"RDI_LOGGING_ENABLED"
//Architecture version
#define TAG_DEVICE_ARCH						"DEVICE_ARCH"
//CoreSight related
#define TAG_DEBUG_BASE						"DEBUG_BASE_ADDRESS"
#define TAG_DEBUG_AP_INDEX					"DEBUG_AP_INDEX"
#define TAG_MEMORY_AP_INDEX					"MEMORY_AP_INDEX"
#define TAG_USE_MEMORY_AP_FOR_MEM_ACCESS	"USE_MEMORY_AP_FOR_MEM_ACCESS"
#define TAG_CORESIGHT_COMPLIANT				"CORESIGHT_COMPLIANT"
//TI specific
#define TAG_TI_COMPLIANT					"TI_COMPLIANT"
#define TAG_TI_SUB_PORT						"TI_SUB_PORT"
#define TAG_TI_DAPPCSUPPORT					"TI_DAPPCSUPPORT"

// 
// *** Default Values for each configuration ***
// [Edit here if you need to change defaults]
//
// Device / Multi core related
const int DEFAULT_NUMBER_OF_CORES = 1;
#define DEFAULT_SELECTED_DEVICE "ARM7DI"
const int DEFAULT_SELECTED_CORE = 0;
const unsigned char DEFAULT_IR_LENGTH = 4;
const Endian DEFAULT_ENDIANNESS = LittleEndian;
const bool DEFAULT_IS_ARM_CORE = true;
// Reset Related
const TargetResetMode DEFAULT_TARGET_RESET_MODE = NoHardReset;
const int DEFAULT_HARD_RESET_STARTUP_DELAY = 5;
const float DEFAULT_HOT_PLUG_VOLTAGE = ( float )0.0;//3.3
const bool DEFAULT_HALT_CORE_ON_CONNECTION  = false;
const bool DEFAULT_USE_NTRST = true;	//false;
const bool DEFAULT_SET_DBGRQLOW = false;
// Target connection related
const unsigned long DEFAULT_OPELLA_SERIAL_NUMBER = 0;
const unsigned long DEFAULT_OPELLA_INSTANCE_NUMBER = 0;
const unsigned short DEFAULT_OPELLA_PRODUCT_ID = 0;
const JTAGClockFreqMode DEFAULT_JTAG_FREQ_MODE = Fixed;
const int DEFAULT_FIXED_JTAG_FREQ_IN_KHZ = 100;
// Advanced
const unsigned long DEFAULT_CACHE_CLEAN_START_ADDRESS = 0x50;
const unsigned long DEFAULT_SAFE_NON_VECTOR_ADDRESS = 0x00010000;
const int DEFAULT_TARGET_EXE_POLLING_RATE = 50;
const bool DEFAULT_SAFE_NV_ADDR_ENABLED = false;
// Semi-hosting releated
const SemiHostingType DEFAULT_SEMI_HOSTING_TYPE = Disabled;
const SemiHostingSupport DEFAULT_SEMI_HOSTING_SUPPORT = UsingGNUARMCompiler; //UsingARMCompiler
const unsigned long DEFAULT_SH_TOP_OF_MEMORY = 0x70000; //0x0;
const unsigned long DEFAULT_SH_VECTOR_TRAP_ADDRESS = 0x8; //0x0;
const unsigned long DEFAULT_SH_DCC_HANDLER = 0x70000; //0x0;
const unsigned long DEFAULT_SH_ARM_SWI_VALUE = 0x123456; //0x0;
const unsigned long DEFAULT_SH_THUMB_SWI_VALUE = 0xAB; //0x0;

// Shift IR/DR Clocks related
const unsigned char DEFAULT_SHIFT_IR_PRE_TCKS = 0;
const unsigned char DEFAULT_SHIFT_IR_POST_TCKS = 0;
const unsigned char DEFAULT_SHIFT_DR_PRE_TCKS = 0;
const unsigned char DEFAULT_SHIFT_DR_POST_TCKS = 0;

// RDI Logging
const bool DEFAULT_RDI_LOGGING_ENABLED = false;

// [Internal string values used in Config File]
// *** String values for different configuration       ***
// *** used inside the configuration file only         ***
// *** These values are internal to this class logic   ***
// *** This may change in subsequent releases          ***
// *** So do not use these outside                     ***

// Endianness values in config file
#define BIGENDIAN "BIG"
#define LITTLEENDIAN "LITTLE"

// Core Type Value
#define ARM_CORE "ARM_CORE"
#define NON_ARM_CORE "NON_ARM_CORE"

// JTAG Clock Frequency Mode Values
#define FIXED "FIXED"
#define ADAPTIVE "ADAPTIVE"
#define AUTOMATIC "AUTOMATIC"

// Target Reset Mode Values
#define NO_HARD_RESET				"NO_HARD_RESET"
#define HARD_RESET_AND_DELAY		"HARD_RESET_AND_DELAY"
#define HARD_RESET_AND_HALT_AT_RV	"HARD_RESET_AND_HALT_AT_RV"
#define HARD_RESET_AND_GO			"HARD_RESET_AND_GO"
#define HOTPLUG						"HOTPLUG"
#define CORE_ONLY					"CORE_ONLY"
#define CORE_AND_PERIPHERAL			"CORE_AND_PERIPHERAL"

// Semi-hosting Type values
#define DISABLED "DISABLED"
#define STANDARD_DCC "STANDARD_DCC"
#define REAL_MONITOR_DCC "REAL_MONITOR_DCC"
#define USING_SWI "USING_SWI"
#define VIA_DCC "VIA_DCC"

// Semi-hosting support values
#define USING_ARM_COMPILER "USING_ARM_COMPILER"
#define USING_GNU_ARM_COMPILER "USING_GNU_ARM_COMPILER"

// [End of Internal String Values in Config File]

const int MAX_TAG_LENGTH = 256;
const int MAX_VALUE_LENGTH = 256;
const int MAX_FILE_NAME_LENGTH = 256;


/****************************************************************************
  Function: Constructor
  Engineer: Jiju George T
  Input      : csConfigFile - Configuration file from which the value 
               or each configuration have to be read.
  Output     : void
  Description: Creates an empty config object to which configurations can 
               be added and then saved.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
COpellaConfig::COpellaConfig() : m_RDIConfig( 0 )
{
    // Create an Empty RDI Config
    m_RDIConfig = ToolConf_New( -1 );
}
/****************************************************************************
  Function   : Constructor
  Engineer   : Jiju George T
  Input      : csConfigFile - Configuration file from which the value 
               or each configuration have to be read.
  Output     : void
  Description: Constructs the configuration object from the given 
			   configuration file. If the file does not exists or cannot read then 
			   creates the object with default value for each configuration. 
			   IsDefault() method can be used after construction to check whether
			   the object will return default values or the contents of the 
			   configuration file.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
COpellaConfig::COpellaConfig( const char* szConfigFile) : m_RDIConfig( 0 )
{
    // Read the give configuration file
    m_RDIConfig = ToolConf_Read( NULL, NULL, szConfigFile );//     **** NB **** 
                              // remember not to add the extension to the config file name, 
                              // first part of the filename only
}

/****************************************************************************
  Function   : Constructor
  Engineer   : Jiju George T
  Input      : Config - Already read config pointer.
  Output     : void
  Description: Constructor which accepts already read config pointer
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
COpellaConfig::COpellaConfig( RDI_ConfigPointer Config )
{
    m_RDIConfig = Config;
}


/****************************************************************************
  Function   : Destructor
  Engineer   : Jiju George T
  Input      : void.
  Output     : void
  Description: Destructs the object
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
COpellaConfig::~COpellaConfig()
{

}

/****************************************************************************
  Function   : IsDefault()
  Engineer   : Jiju George T
  Input      : void.
  Output     : bool
  Description: Checks whether this object is returning the default values 
			   or it uses the contents of the given configuration file.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


bool COpellaConfig::IsDefault()
{
    if( m_RDIConfig )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/****************************************************************************
  Function   : IsValid()
  Engineer   : Jiju George T
  Input      : void.
  Output     : bool
  Description: Checks whether this object is returning the default values 
               or it uses the contents of the given configuration file.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

bool COpellaConfig::IsValid()
{
    if( !m_RDIConfig )
    {
        return false;
    }
    else
    {
      //if (ToolConf_Lookup(m_RDIConfig,(tag_t)TAG_SELECTED_DEVICE) == NULL)
      //   return false;

	  if (ToolConf_Lookup(m_RDIConfig,(tag_t)TAG_PROCESSOR_NAME) == NULL)
		 return false;
      if (ToolConf_Lookup(m_RDIConfig,(tag_t)TAG_DEBUGGER_NAME) == NULL)
         return false;

      return true;
    }
}

/****************************************************************************
  Function   : GetDebuggerName()
  Engineer   : Jiju George T
  Input      : char* szDebuggerName - pointer to store debugger name
  Output     : void
  Description: Returns the Debugger name in the configuration file.
  Date           Initials    Description
  2007-08-02       JG          Initial
  2010-08-12	   SJ		   Modified to avoid memory leaks	
****************************************************************************/


//char* COpellaConfig::GetDebuggerName()
void COpellaConfig::GetDebuggerName(char* szDebuggerName)
{
    //char szDebuggerName = new char [MAX_VALUE_LENGTH];

    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t )TAG_DEBUGGER_NAME );
        if( 0 != szValue )
        {
            if(szDebuggerName)
				strcpy( szDebuggerName , szValue );
        }
    }

    //return szDebuggerName;
}

/****************************************************************************
  Function   : SetDebuggerName()
  Engineer   : Jiju George T
  Input      : void.
  Output     : void
  Description: Sets the Debugger Name to configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetDebuggerName( const char* szDebuggerName )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_DEBUGGER_NAME,
                           tcnf_String,
                           szDebuggerName );
    }
}


/****************************************************************************
  Function   : Clone()
  Engineer   : Jiju George T
  Input      : void.
  Output     : COpellaConfig* - Clone(copy) of current object
  Description: Creates a copy of the object and returns.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

COpellaConfig* COpellaConfig::Clone()
{
     RDI_ConfigPointer CloneConfig = 0;

    if( m_RDIConfig )
    {   
        CloneConfig = ToolConf_Clone( m_RDIConfig );
    }   

    return new COpellaConfig( CloneConfig );
}

/****************************************************************************
  Function   : GetNumberOfCores()
  Engineer   : Jiju George T
  Input      : void.
  Output     : int - Number of cores
  Description: Get number of cores from the configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


int COpellaConfig::GetNumberOfCores()
{
    int nNumberOfCores = DEFAULT_NUMBER_OF_CORES;

    if( m_RDIConfig )
    {
         nNumberOfCores = ToolConf_DLookupInt( m_RDIConfig,
                                               ( tag_t )TAG_NUMBER_OF_CORES,
                                               DEFAULT_NUMBER_OF_CORES ); 
    }

    return nNumberOfCores;
}

/****************************************************************************
  Function   : SetNumberOfCores()
  Engineer   : Jiju George T
  Input      : nNumCores - Number of cores
  Output     : void
  Description: Set number of cores property to given value.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


void COpellaConfig::SetNumberOfCores( int nNumCores )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_NUMBER_OF_CORES,
                           tcnf_UInt, 
                           nNumCores );
    }
}

/****************************************************************************
  Function   : IsARMCore()
  Engineer   : Jiju George T
  Input      : nCore - Core Index (0 based)
  Output     : bool  - true if ARM core. false otherwise
  Description: Checks whether the given core is an ARM core..
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


bool COpellaConfig::IsARMCore( int nCore )
{
    bool bIsARMCore = DEFAULT_IS_ARM_CORE;

    if( m_RDIConfig )
    {
    
        char csTag[MAX_TAG_LENGTH];
        sprintf( csTag, _T("%s_%d"), TAG_CORE, nCore );

        const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t )csTag );
        if ( 0 != szValue )
        {
            if( 0 == strcmp( szValue, ARM_CORE ))
            {
                bIsARMCore = true;
            }
            else
            {
                bIsARMCore = false;
            }
            
        }
    }

    return bIsARMCore;
}



/****************************************************************************
  Function   : SetCoreType()
  Engineer   : Jiju George T
  Input      : nCore    - Core Index ( 0 based )
			   bArmCore - true is ARM core. false otherwise
  Output     : void
  Description: Set the type of core in the configuration
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetCoreType(int nCore, unsigned char bArmCore )
{
    if( m_RDIConfig )
    {
        // Make tag
        char csTag[MAX_TAG_LENGTH];
        sprintf( csTag, _T("%s_%d"), TAG_CORE, nCore );

        // Find core type value to be stored
        char szValue[MAX_VALUE_LENGTH] = ARM_CORE;
        if( !bArmCore )
        {
            strcpy( szValue, NON_ARM_CORE );
        }

        ToolConf_AddTyped( m_RDIConfig,
                          ( tag_t )csTag,
                          tcnf_String,
                          szValue );
    }
}



/****************************************************************************
  Function   : GetSelectedDevice()
  Engineer   : Jiju George T
  Input      : char *szSelectedDevice - pointer to store the Selected Device name
  Output     : void
  Description: Returns the selected device ( processor ).
  Date           Initials    Description
  2007-08-02       JG          Initial
  2010-08-12	   SJ		   Modified to avoid memory leaks	
****************************************************************************/
//char* COpellaConfig::GetSelectedDevice()
void COpellaConfig::GetSelectedDevice( char *szSelectedDevice )
{
    //char *szSelectedDevice = new char [MAX_VALUE_LENGTH];
    if(szSelectedDevice)
		strcpy( szSelectedDevice , DEFAULT_SELECTED_DEVICE );

    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t )TAG_SELECTED_DEVICE );
        if( 0 != szValue )
        {
			if(szSelectedDevice)
				strcpy( szSelectedDevice , szValue );
        }
    }

    //return szSelectedDevice;

}

/****************************************************************************
  Function   : SetSelectedDevice()
  Engineer   : Jiju George T
  Input      : csDeviceName - Name of the selected device
  Output     : void
  Description: Sets the selected device (processor) to configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetSelectedDevice( const char* szDeviceName )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SELECTED_DEVICE,
                           tcnf_String,
                           szDeviceName );
    }
}

/****************************************************************************
  Function   : GetProcessorName()
  Engineer   : Jeenus C.K.
  Input      : char *szProcessorName - pointer to store Processor name
  Output     : void
  Description: Returns the selected device name( processor ).
  Date           Initials    Description
  05-May-2010      JCK          Initial
  2010-08-12	   SJ		   Modified to avoid memory leaks	
****************************************************************************/

//char* COpellaConfig::GetProcessorName()
void COpellaConfig::GetProcessorName(char *szProcessorName)
{
    //char *szProcessorName = new char [MAX_VALUE_LENGTH];
	if(szProcessorName)
		strcpy( szProcessorName , DEFAULT_SELECTED_DEVICE );

    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t )TAG_PROCESSOR_NAME );
        if( 0 != szValue )
        {
			if(szProcessorName)
				strcpy( szProcessorName , szValue );
        }
    }

	/* This is done in order to work previous versions of cnf file. */
	if(szProcessorName)
	{
		if (0 == strlen(szProcessorName))
		{
			if( m_RDIConfig )
			{
				const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t )TAG_SELECTED_DEVICE );
				if( 0 != szValue )
				{
						strcpy( szProcessorName , szValue );
				}
			}
		}
	}

    //return szProcessorName;

}

/****************************************************************************
  Function   : SetProcessorName()
  Engineer   : Jeenus C.K.
  Input      : szProcessorName - Name of the selected processor
  Output     : void
  Description: Sets the selected device name (processor) to configuration.
  Date           Initials    Description
  05-May-2010      JCK          Initial
****************************************************************************/

void COpellaConfig::SetProcessorName( const char* szProcessorName )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_PROCESSOR_NAME,
                           tcnf_String,
                           szProcessorName );
    }
}

/****************************************************************************
  Function   : GetSelectedCore()
  Engineer   : Jiju George T
  Input      : void
  Output     : int - Selected core
  Description: Returns the core selected for debugging.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

int COpellaConfig::GetSelectedCore()
{
    int nSelectedCore = DEFAULT_SELECTED_CORE;

    if( m_RDIConfig )
    {
        nSelectedCore = ( int )ToolConf_DLookupInt( m_RDIConfig,
                                                    ( tag_t ) TAG_SELECTED_CORE,
                                                    DEFAULT_SELECTED_CORE );
    }

    return nSelectedCore;
}

/****************************************************************************
  Function   : GetSelectedCore()
  Engineer   : Jiju George T
  Input      : nCore - Selected core index (0 based)
  Output     : void
  Description: Sets core selected for debugging..
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetSelectedCore( int nCore)
{
    if( m_RDIConfig )
    {

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SELECTED_CORE,
                           tcnf_UInt,
                           nCore );
    }
}

/****************************************************************************
  Function   : GetSelectedCore()
  Engineer   : Jiju George T
  Input      : nCore    - Core index (0 based index)
  Output     : ucIRLength -IR Length of given core
  Description: Return the IR Length of given core.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

unsigned char COpellaConfig::GetIRLength( int nCore )
{
    unsigned char ucIRLength = DEFAULT_IR_LENGTH;

    if( m_RDIConfig )
    {
        char csTag[MAX_TAG_LENGTH];
        sprintf( csTag, _T("%s_%d_%s"), TAG_CORE, nCore ,TAG_IRLENGTH );

        ucIRLength = ( unsigned char )ToolConf_DLookupInt( m_RDIConfig,
                                                           ( tag_t ) csTag,
                                                           DEFAULT_IR_LENGTH );
    }
         
    return ucIRLength;
}

/****************************************************************************
  Function   : SetIRLength()
  Engineer   : Jiju George T
  Input      : nCore    - Core Index of which IR Length has to be set
  Output     : void
  Description: Sets the IR Length for the given core.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/



void COpellaConfig::SetIRLength(int nCore, unsigned char ucLength)
{
    if( m_RDIConfig )
    {
        char csTag[MAX_TAG_LENGTH];
        sprintf( csTag, _T("%s_%d_%s"), TAG_CORE, nCore ,TAG_IRLENGTH );

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )csTag,
                           tcnf_UInt, 
                           ucLength );
    }
}

/****************************************************************************
  Function   : SetIRLength()
  Engineer   : Jiju George T
  Input      : void
  Output     : Endian - BigEndian or LittleEndian
  Description: Returns the configured endianness of the processor.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

Endian COpellaConfig::GetEndianness()
{
    Endian eEndianness = DEFAULT_ENDIANNESS;

    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t ) TAG_ENDIANNESS );
        if ( 0 != szValue )
        {
            if( 0 == strcmp( szValue, BIGENDIAN ))
            {
                eEndianness = BigEndian;
            }
            else if( 0 == strcmp( szValue, LITTLEENDIAN ))
            {
                eEndianness = LittleEndian;
            }
            else
            {
                // Nothing to be done. Default is OK
            }
        }
 
    }

    return eEndianness;
}

/****************************************************************************
  Function   : SetEndianness()
  Engineer   : Jiju George T
  Input      : eEndian - LittleEndian or BigEndian
  Output     : void
  Description: Sets the given endianness.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetEndianness(Endian eEndian)
{
    if( m_RDIConfig )
    {
        if( LittleEndian == eEndian ) 
        {
            ToolConf_AddTyped( m_RDIConfig,
                               ( tag_t )TAG_ENDIANNESS,
                               tcnf_String,
                               LITTLEENDIAN );
        }
        else
        {
            ToolConf_AddTyped( m_RDIConfig,
                               ( tag_t )TAG_ENDIANNESS,
                               tcnf_String,
                               BIGENDIAN );
        }

    }
}

/****************************************************************************
  Function   : IsOpellaUSBSerialNumberExists()
  Engineer   : Jiju George T
  Input      : void
  Output     : bool - true is exists. false otherwise
  Description: Checks whether the serial number exists in the configuration file.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


bool COpellaConfig::IsOpellaUSBSerialNumberExists()
{
    bool bRet = false;

    if( m_RDIConfig )
    {
        // Try to read serial number
        if ( 0 != ToolConf_Lookup( m_RDIConfig,
                                   ( tag_t )TAG_OPELLA_SERIAL_NUMBER ))
        {
            bRet = true;
        }
    }

    return bRet;
}


/****************************************************************************
  Function   : GetOpellaUSBSerialNumber()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Serial number of Opella
  Description: Returns the serial number of Opella in the configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


/*unsigned long COpellaConfig::GetOpellaUSBSerialNumber()
{
    unsigned long ulSerialNumber = DEFAULT_OPELLA_SERIAL_NUMBER;

    if( m_RDIConfig )
    {
        // Try to read serial number
        ulSerialNumber = ToolConf_DLookupUInt( m_RDIConfig,
                                              ( tag_t )TAG_OPELLA_SERIAL_NUMBER,
                                              DEFAULT_OPELLA_SERIAL_NUMBER ); 
    }

    return ulSerialNumber;
}
*/
/****************************************************************************
  Function   : GetOpellaUSBSerialNumber()
  Engineer   : Jiju George T
  Input      : char* szSerialNumber - pointer to store serial number
  Output     : void 
  Description: Returns the serial number of Opella in the configuration file
  Date           Initials    Description
  2007-08-02       JG          Initial
  2010-08-12	   SJ		   Modified to avoid memory leaks	
****************************************************************************/

//char* COpellaConfig::GetOpellaUSBSerialNumber()
void COpellaConfig::GetOpellaUSBSerialNumber(char* szSerialNumber)
{
    //char* szSerialNumber = new char [MAX_VALUE_LENGTH];

    //Initialise to NULL
	if( szSerialNumber )
		strcpy( szSerialNumber , "" );
    if( m_RDIConfig )
    {
        // Try to read serial number
        const char * szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t )TAG_OPELLA_SERIAL_NUMBER );
        if( 0 != szValue )
        {
            if( szSerialNumber )
				strcpy( szSerialNumber , szValue );
        }
    }

    //return szSerialNumber;
}


/****************************************************************************
  Function   : SetOpellaUSBSerialNumber()
  Engineer   : Jiju George T
  Input      : ulSerialNumber - Serial number of Opella
  Output     : void
  Description: Sets the given Opella Serial number to the configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/



/*void COpellaConfig::SetOpellaUSBSerialNumber(unsigned long ulSerialNumber)
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_OPELLA_SERIAL_NUMBER,
                           tcnf_UInt, 
                           ulSerialNumber );
    }
}
*/

/****************************************************************************
  Function   : SetOpellaUSBSerialNumber()
  Engineer   : Jiju George T
  Input      : ulSerialNumber - Serial number of Opella
  Output     : void
  Description: Sets the given Opella Serial number to the configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


void COpellaConfig::SetOpellaUSBSerialNumber(const char* szSerialNumber)
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_OPELLA_SERIAL_NUMBER,
                           tcnf_String, 
                           szSerialNumber );
    }
}


/****************************************************************************
  Function   : GetJTAGClockFreqMode()
  Engineer   : Jiju George T
  Input      : void
  Output     : JTAGClockFreqMode - Clock frequency mode
  Description: Returns the JTAG Clock Frequency Modes.
			   Refer JTAGClockFreqMode enum for possible values.
  
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/



JTAGClockFreqMode COpellaConfig::GetJTAGClockFreqMode()
{
    JTAGClockFreqMode eJTAGClockFreqMode = DEFAULT_JTAG_FREQ_MODE;

    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, 
                                                ( tag_t ) TAG_JTAG_CLOCK_FREQ_MODE );
        if ( 0 != szValue )
        {
            if( 0 == strcmp( szValue, FIXED ))
            {
                eJTAGClockFreqMode = Fixed;
            }
            else if( 0 == strcmp( szValue, ADAPTIVE ))
            {
                eJTAGClockFreqMode = Adaptive;
            }
            else if( 0 == strcmp( szValue, AUTOMATIC ))
            {
                eJTAGClockFreqMode = Automatic;
            }
            else
            {
                // Nothing to be done. Default is OK
            }
        }
    }

    return eJTAGClockFreqMode;
}

/****************************************************************************
  Function   : SetJTAGClockFreqMode()
  Engineer   : Jiju George T
  Input      : eJTAGClockFreqMode - JTAG Clock Frequency mode
  Output     : void
  Description: Sets the JTAG Clock Frequency mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetJTAGClockFreqMode( JTAGClockFreqMode eJTAGClockFreqMode )
{
    if( m_RDIConfig )
    {
        char* szFreqMode = new char[MAX_VALUE_LENGTH];
        strcpy( szFreqMode, FIXED );

        switch ( eJTAGClockFreqMode )
        {
            case Adaptive :
            {
                strcpy( szFreqMode, ADAPTIVE );
                break;
            }

            case Automatic :
            {
                strcpy( szFreqMode, AUTOMATIC );
                break;
            }

            default:
            {
                break;
            }
        }

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_JTAG_CLOCK_FREQ_MODE,
                           tcnf_String,
                           szFreqMode);

        delete[] szFreqMode;
     }
}


/****************************************************************************
  Function   : GetFixedJTAGFrequencyInKHz()
  Engineer   : Jiju George T
  Input      : void
  Output     : int - JTAG Frequency in KHz
  Description: Returns the JTAG Frequency in KHz.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

int COpellaConfig::GetFixedJTAGFrequencyInKHz()
{
    int nJTAGFreqInKHz = DEFAULT_FIXED_JTAG_FREQ_IN_KHZ;

    if( m_RDIConfig )
    {
       nJTAGFreqInKHz = (int)ToolConf_DLookupInt( m_RDIConfig,
                                                  (tag_t)TAG_FIXED_JTAG_FREQ_IN_KHZ,
                                                  DEFAULT_FIXED_JTAG_FREQ_IN_KHZ );
    }

    return nJTAGFreqInKHz;
}


/****************************************************************************
  Function   : SetFixedJTAGFrequencyInKHz()
  Engineer   : Jiju George T
  Input      : nFrequencyInKHz - JTAG clock frequency in KHz
  Output     : void
  Description: Sets the given frequency as the fixed JTAG clock frequency.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetFixedJTAGFrequencyInKHz( int nFrequencyInKHz )
{
    if( m_RDIConfig )
    {
       ToolConf_AddTyped( m_RDIConfig,
                          ( tag_t )TAG_FIXED_JTAG_FREQ_IN_KHZ,
                          tcnf_UInt, 
                          nFrequencyInKHz );
    }
}



/****************************************************************************
  Function   : GetOpellaUSBInstanceNumber()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Instance number
  Description: Return the Opella Instance number
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


unsigned long COpellaConfig::GetOpellaUSBInstanceNumber()
{
    unsigned long ulInstanceNumber = DEFAULT_OPELLA_INSTANCE_NUMBER;

    if( m_RDIConfig )
    {
        // Try to read serial number
        ulInstanceNumber = ToolConf_DLookupUInt( m_RDIConfig,
                                                ( tag_t )TAG_OPELLA_INSTANCE_NUMBER,
                                                DEFAULT_OPELLA_INSTANCE_NUMBER ); 
    }

    return ulInstanceNumber;
}

/****************************************************************************
  Function   : SetOpellaUSBInstanceNumber()
  Engineer   : Jiju George T
  Input      : ulInstanceNumber - Opella Instance Number
  Output     : void
  Description: Sets the given Opella Instance number to configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetOpellaUSBInstanceNumber( unsigned long ulInstanceNumber )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_OPELLA_INSTANCE_NUMBER,
                           tcnf_UInt,
                           ulInstanceNumber );
    }
}

/****************************************************************************
  Function   : GetOpellaUSBProductID()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Product ID
  Description: Returns the Opella Product ID.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

unsigned long COpellaConfig::GetOpellaUSBProductID()
{
    unsigned long ulProductID = DEFAULT_OPELLA_PRODUCT_ID;

    if( m_RDIConfig )
    {
        // Try to read serial number
        ulProductID = ToolConf_DLookupUInt( m_RDIConfig,
                                            ( tag_t )TAG_OPELLA_PRODUCT_ID,
                                            DEFAULT_OPELLA_PRODUCT_ID ); 
    }

    return ulProductID;
}


/****************************************************************************
  Function   : SetOpellaUSBProductID()
  Engineer   : Jiju George T
  Input      : ulProductID - Product ID to be set
  Output     : void
  Description: Sets given Opella Product ID to configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/



void COpellaConfig::SetOpellaUSBProductID( unsigned long ulProductID )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           (tag_t) TAG_OPELLA_PRODUCT_ID,
                           tcnf_UInt,
                           ulProductID );
    }
}

/****************************************************************************
  Function   : GetTargetResetMode()
  Engineer   : Jiju George T
  Input      : void
  Output     : TargetResetMode - Target reset mode
  Description: Returns the tager rest mode.
               Refer TargetResetMode enum for possible values
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

TargetResetMode COpellaConfig::GetTargetResetMode()
{
    TargetResetMode eTargetResetMode = DEFAULT_TARGET_RESET_MODE;

    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, 
                                                ( tag_t ) TAG_TARGET_RESET_MODE );
        if ( 0 != szValue )
        {
            if( 0 == strcmp( szValue, NO_HARD_RESET ))
            {
                eTargetResetMode = NoHardReset;
            }
            else if( 0 == strcmp( szValue, HARD_RESET_AND_DELAY ))
            {
                eTargetResetMode = HardResetAndDelay;
            }
            else if( 0 == strcmp( szValue, HARD_RESET_AND_HALT_AT_RV ))
            {
                eTargetResetMode = HardResetAndHaltAtRV;
            }
/*            else if( 0 == strcmp( szValue, HARD_RESET_AND_GO ))
            {
                eTargetResetMode = HardResetAndGo;
            }*/
            else if( 0 == strcmp( szValue, HOTPLUG ))
            {
                eTargetResetMode = HotPlug;
            }
			else if ( 0 == strcmp( szValue, CORE_ONLY ))
			{
				eTargetResetMode = CoreOnly;
			}

			else if (0 == strcmp( szValue, CORE_AND_PERIPHERAL))
			{
				eTargetResetMode = CoreAndPeripherals;
			}
            else
            {
                // Nothing to be done. Default is OK
            }
        }
    }
    
    return eTargetResetMode;
}


/****************************************************************************
  Function   : SetTargetResetMode()
  Engineer   : Jiju George T
  Input      : eTargetResetMode - target reset mode to be set
  Output     : void
  Description: Sets target reset mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetTargetResetMode( TargetResetMode eTargetResetMode )
{
    if( m_RDIConfig )
    {

        char* szTargetResetMode = new char[MAX_VALUE_LENGTH];
        strcpy( szTargetResetMode, NO_HARD_RESET );

        switch( eTargetResetMode )
        {
            case HardResetAndDelay:
            {
                strcpy( szTargetResetMode, HARD_RESET_AND_DELAY );
                break;
            }

            case HardResetAndHaltAtRV:
            {
                strcpy( szTargetResetMode, HARD_RESET_AND_HALT_AT_RV );
                break;
            }

/*            case HardResetAndGo:
            {
                strcpy( szTargetResetMode, HARD_RESET_AND_GO );
                break;
            }
*/
            case HotPlug:
            {
                strcpy( szTargetResetMode, HOTPLUG );
                break;
            }

			case CoreOnly:
				strcpy(szTargetResetMode, CORE_ONLY);
				break;
			case CoreAndPeripherals:
				strcpy(szTargetResetMode, CORE_AND_PERIPHERAL);
				break;

            default:
            {
                break;
            }
        }

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_TARGET_RESET_MODE,
                           tcnf_String,
                           szTargetResetMode);

        delete[] szTargetResetMode;
    }
}


/****************************************************************************
  Function   : GetHardResetDelay()
  Engineer   : Jiju George T
  Input      : void
  Output     : int - Delay in seconds
  Description: Returns the startup delay in seconds for Hard Rest and Delay mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

int COpellaConfig::GetHardResetDelay()
{
    int nDelay = DEFAULT_HARD_RESET_STARTUP_DELAY;

    //int nDelay = 0; //The default value should be zero.

    if( m_RDIConfig )
    {
        nDelay = ( int )ToolConf_DLookupInt( m_RDIConfig,
                                             ( tag_t ) TAG_HARD_RESET_STARTUP_DELAY,
                                             DEFAULT_HARD_RESET_STARTUP_DELAY);
    }

    return nDelay;
}


/****************************************************************************
  Function   : SetHardResetDelay()
  Engineer   : Jiju George T
  Input      : nDelayInSec - Delay in seconds
  Output     : void
  Description: Sets hard reset startup delay in seconds.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetHardResetDelay( int nDelayInSec )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_HARD_RESET_STARTUP_DELAY,
                           tcnf_UInt,
                           nDelayInSec );
    }
}

/****************************************************************************
  Function   : GetHotPlugVoltage()
  Engineer   : Jiju George T
  Input      : void
  Output     : float - Hot plug voltage
  Description: Returns the voltage for Hot Plug mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

float COpellaConfig::GetHotPlugVoltage()
{
    float fVoltage = DEFAULT_HOT_PLUG_VOLTAGE;

    if( m_RDIConfig )
    {
        const char * szHotPlugVoltage = ToolConf_Lookup( m_RDIConfig, 
                                                         ( tag_t ) TAG_HOT_PLUG_VOLTAGE );
        if ( 0 != szHotPlugVoltage )
        {
            fVoltage = ( float )strtod( szHotPlugVoltage, 0 );
        }
    }
    return fVoltage;
}

/****************************************************************************
  Function   : SetHotPlugVoltage()
  Engineer   : Jiju George T
  Input      : fVoltage - Voltage to be set
  Output     : void
  Description: Sets the given hot plug voltage.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetHotPlugVoltage( float fVoltage )
{
    if( m_RDIConfig )
    {
        char szHotPlugVoltage[MAX_VALUE_LENGTH];
        sprintf( szHotPlugVoltage,  _T( "%1.1f"), fVoltage );

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_HOT_PLUG_VOLTAGE,
                           tcnf_String,
                           szHotPlugVoltage );

    }
}



/****************************************************************************
  Function   : IsHaltCoreOnConnection()
  Engineer   : Jiju George T
  Input      : fVoltage - Voltage to be set
  Output     : bool - true core has to be halted. false otherwise
  Description: Check whether to halt core on connection.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

bool COpellaConfig::IsHaltCoreOnConnection()
{
    bool bHaltCoreOnConnection = DEFAULT_HALT_CORE_ON_CONNECTION;

    if( m_RDIConfig )
    {
        if( ToolConf_DLookupBool( m_RDIConfig, 
                                  ( tag_t )TAG_HALT_CORE_ON_CONNECTION,
                                  DEFAULT_HALT_CORE_ON_CONNECTION ))
        {
            bHaltCoreOnConnection = true;
        }
        else
        {
            bHaltCoreOnConnection = false;
        }
    }

    return bHaltCoreOnConnection;
}

/****************************************************************************
  Function   : HaltCoreOnConnection()
  Engineer   : Jiju George T
  Input      : bHalt - true to enable. false to disable
  Output     : void
  Description: Enables / disables halting core on connection .
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::HaltCoreOnConnection( BOOL bHalt )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig, 
                           ( tag_t )TAG_HALT_CORE_ON_CONNECTION,
                           tcnf_Bool,
                           bHalt );
    }
}

/****************************************************************************
  Function   : IsUsenTRST()
  Engineer   : Jiju George T
  Input      : void
  Output     : bool - true if enabled. false if disabled
  Description: Checks whether nTRST is used.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

bool COpellaConfig::IsUsenTRST()
{
    bool bUsenTRST = DEFAULT_USE_NTRST;

    if( m_RDIConfig )
    {
        if( ToolConf_DLookupBool( m_RDIConfig, 
                                  ( tag_t )TAG_USE_NTRST,
                                  DEFAULT_USE_NTRST ))
        {
            bUsenTRST = true;
        }
        else
        {
            bUsenTRST = false;
        }
    }

    return bUsenTRST;
}

/****************************************************************************
  Function   : UsenTRST()
  Engineer   : Jiju George T
  Input      : bUse - true for enabling. false for disabling.
  Output     : void
  Description: Enables / disables usage of nTRST.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::UsenTRST( BOOL bUse )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig, 
                           ( tag_t )TAG_USE_NTRST,
                           tcnf_Bool,
                           bUse );
    }
}

/****************************************************************************
  Function   : IsSetDBGRQLow()
  Engineer   : Jiju George T
  Input      : void
  Output     : bool - true if enabled. false if disabled
  Description: Checks whether setting DGBRQ to low is enabled.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/



bool COpellaConfig::IsSetDBGRQLow()
{
    bool bSetDBGRQLow = DEFAULT_SET_DBGRQLOW;

    if( m_RDIConfig )
    {
        if( ToolConf_DLookupBool( m_RDIConfig, 
                                  ( tag_t )TAG_SET_DBGRQLOW,
                                  DEFAULT_USE_NTRST ))
        {
            bSetDBGRQLow = true;
        }
        else
        {
            bSetDBGRQLow = false;
        }
    }

    return bSetDBGRQLow;
}

/****************************************************************************
  Function   : SetDBGRQLow()
  Engineer   : Jiju George T
  Input      : bSet - true for enabling. false for disabling.
  Output     : void
  Description: Enables / disables setting DGBRQ to low.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetDBGRQLow( BOOL bSet )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig, 
                           ( tag_t )TAG_SET_DBGRQLOW,
                           tcnf_Bool,
                           bSet );
    }
}


/****************************************************************************
  Function   : GetCacheCleanStartAddress()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - cache clean start address
  Description: Returns the Cache clear start address.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

unsigned long COpellaConfig::GetCacheCleanStartAddress()
{
    unsigned long ulCacheCleanAddress = DEFAULT_CACHE_CLEAN_START_ADDRESS;

    if( m_RDIConfig )
    {
        ulCacheCleanAddress = ToolConf_DLookupUInt( m_RDIConfig,
                                                    ( tag_t )TAG_CACHE_CLEAN_START_ADDRESS,
                                                    DEFAULT_CACHE_CLEAN_START_ADDRESS ); 
    }

    return ulCacheCleanAddress;
}


/****************************************************************************
  Function   : SetCacheCleanStartAddress()
  Engineer   : Jiju George T
  Input      : ulAddress - address to be set
  Output     : void
  Description: Sets the give cache clean start address.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetCacheCleanStartAddress( unsigned long ulAddress )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_CACHE_CLEAN_START_ADDRESS,
                           tcnf_UInt, 
                           ulAddress );
    }
}

/****************************************************************************
  Function   : GetSafeNonVectorAddress()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - safe non vector address
  Description: Returns safe non vector address.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

unsigned long COpellaConfig::GetSafeNonVectorAddress()
{
    unsigned long ulSafeNonVectorAddress = DEFAULT_SAFE_NON_VECTOR_ADDRESS;

    if( m_RDIConfig )
    {
        ulSafeNonVectorAddress = ToolConf_DLookupUInt( m_RDIConfig,
                                                      ( tag_t )TAG_SAFE_NON_VECTOR_ADDRESS,
                                                      DEFAULT_SAFE_NON_VECTOR_ADDRESS ); 
    }

    return ulSafeNonVectorAddress;
}

/****************************************************************************
  Function   : SetSafeNonVectorAddress()
  Engineer   : Jiju George T
  Input      : ulAddress - safe non vector address to be stored
  Output     : void
  Description: Sets given sage non vector address.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetSafeNonVectorAddress( unsigned long ulAddress )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SAFE_NON_VECTOR_ADDRESS,
                           tcnf_UInt, 
                           ulAddress );
    }
}

/****************************************************************************
  Function   : GetTargetExecutionPollingRate()
  Engineer   : Jiju George T
  Input      : void
  Output     : int - target execution polling rate
  Description: Returns the target execution polling rate.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/

int COpellaConfig::GetTargetExecutionPollingRate()
{
    int nTargetExePollingRate = DEFAULT_TARGET_EXE_POLLING_RATE;

    if( m_RDIConfig )
    {
        nTargetExePollingRate = ( int )ToolConf_DLookupInt( m_RDIConfig,
                                                            ( tag_t ) TAG_TARGET_EXE_POLLING_RATE,
                                                            DEFAULT_TARGET_EXE_POLLING_RATE );
    }

    return nTargetExePollingRate;
}

/****************************************************************************
  Function   : SetTargetExecutionPollingRate()
  Engineer   : Jiju George T
  Input      : nPollingRate - polling rate to be set
  Output     : void
  Description: Sets given target execution polling rate.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/


void COpellaConfig::SetTargetExecutionPollingRate( int nPollingRate )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_TARGET_EXE_POLLING_RATE,
                           tcnf_UInt,
                           nPollingRate );
    }
}

/****************************************************************************
  Function   : GetSemiHostingType()
  Engineer   : Jiju George T
  Input      : void
  Output     : SemiHostingType - Semi-hosting type
  Description: Returns Semi-hosting type.
               See SemiHostingType enum for possible values.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

SemiHostingType COpellaConfig::GetSemiHostingType()
{
    SemiHostingType eSemiHostingType = DEFAULT_SEMI_HOSTING_TYPE;
    
    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, 
                                                ( tag_t ) TAG_SEMI_HOSTING_TYPE );
        if ( 0 != szValue )
        {
            if( 0 == strcmp( szValue, DISABLED ))
            {
                eSemiHostingType = Disabled;
            }
            else if( 0 == strcmp( szValue, STANDARD_DCC ))
            {
                eSemiHostingType = StandardDCC;
            }
            else if( 0 == strcmp( szValue, REAL_MONITOR_DCC ))
            {
                eSemiHostingType = RealMonitorDCC;
            }
            else if( 0 == strcmp( szValue, USING_SWI ))
            {
                eSemiHostingType = UsingSWI;
            }
            else if( 0 == strcmp( szValue, VIA_DCC ))
            {
                eSemiHostingType = ViaDCC;
            }
            else
            {
                // Nothing to be done. Default is OK
            }
        }
    }

    return eSemiHostingType;
}


/****************************************************************************
  Function   : SetSemiHostingType()
  Engineer   : Jiju George T
  Input      : eSemiHostingType - Semi-hosting type to be set.
  Output     : void
  Description: Sets the given Semi-hosting type.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetSemiHostingType( SemiHostingType eSemiHostingType )
{
    if( m_RDIConfig )
    {
        char* szSetSemiHostingType = new char[MAX_VALUE_LENGTH];
        strcpy( szSetSemiHostingType, DISABLED );

        switch( eSemiHostingType )
        {
            case StandardDCC:
            {
                strcpy( szSetSemiHostingType, STANDARD_DCC );
                break;
            }

            case RealMonitorDCC:
            {
                strcpy( szSetSemiHostingType, REAL_MONITOR_DCC );
                break;
            }

            case UsingSWI:
            {
                strcpy( szSetSemiHostingType, USING_SWI );
                break;
            }

            case ViaDCC:
            {
                strcpy( szSetSemiHostingType, VIA_DCC );
                break;
            }

            default:
            {
                break;
            }
        }

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SEMI_HOSTING_TYPE,
                           tcnf_String,
                           szSetSemiHostingType );

        delete[] szSetSemiHostingType;

    }
}


/****************************************************************************
  Function   : GetSemiHostingSupport()
  Engineer   : Jiju George T
  Input      : void
  Output     : SemiHostingSupport - Semi-hosting support
  Description: Returns the Semi-hosting support.
               See SemiHostingSupport enum for possible values.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

SemiHostingSupport COpellaConfig::GetSemiHostingSupport()
{
    SemiHostingSupport eSemiHostingSupport = DEFAULT_SEMI_HOSTING_SUPPORT;
    
    if( m_RDIConfig )
    {
        const char * szValue = ToolConf_Lookup( m_RDIConfig, 
                                                ( tag_t ) TAG_SEMI_HOSTING_SUPPORT );
        if ( 0 != szValue )
        {
            if( 0 == strcmp( szValue, USING_ARM_COMPILER ))
            {
                eSemiHostingSupport = UsingARMCompiler;
            }
            else if( 0 == strcmp( szValue, USING_GNU_ARM_COMPILER ))
            {
                eSemiHostingSupport = UsingGNUARMCompiler;
            }
            else
            {
                // Nothing to be done. Default is OK
            }
        }
    }

    return eSemiHostingSupport;
}

/****************************************************************************
  Function   : SetSemiHostingSupport()
  Engineer   : Jiju George T
  Input      : eSemiHostingSupport - Semi-hosting support to be set.
  Output     : void
  Description: Sets the given Semi-hosting support.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetSemiHostingSupport( SemiHostingSupport  eSemiHostingSupport )
{
    if( m_RDIConfig )
    {
        char* szSemiHostingSupport = new char[MAX_VALUE_LENGTH];
        strcpy( szSemiHostingSupport, USING_ARM_COMPILER );

        switch( eSemiHostingSupport )
        {
            case UsingGNUARMCompiler:
            {
                strcpy( szSemiHostingSupport, USING_GNU_ARM_COMPILER );
                break;
            }

            default:
            {
                break;
            }
        }

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SEMI_HOSTING_SUPPORT,
                           tcnf_String,
                           szSemiHostingSupport);

        delete[] szSemiHostingSupport;
    }
}

/****************************************************************************
  Function   : GetSemiHostingParams()
  Engineer   : Jiju George T
  Input      : tySemiHostingParams - [OUT] contains the Semi-hosting parameters
  Output     : void
  Description: Retrieves the Semi-hosting parameteres.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::GetSemiHostingParams( TySemiHostingParams &tySemiHostingParams )
{
    tySemiHostingParams.ulTopOfMemory = DEFAULT_SH_TOP_OF_MEMORY;
    tySemiHostingParams.ulVectorTrapAddress = DEFAULT_SH_VECTOR_TRAP_ADDRESS;
    tySemiHostingParams.ulDCCHandler = DEFAULT_SH_DCC_HANDLER;
    tySemiHostingParams.ulARMSWIValue = DEFAULT_SH_ARM_SWI_VALUE;
    tySemiHostingParams.ulThumbSWIValue = DEFAULT_SH_THUMB_SWI_VALUE;

    if( m_RDIConfig )
    {
        tySemiHostingParams.ulTopOfMemory = ToolConf_DLookupUInt( m_RDIConfig,
                                                                  ( tag_t )TAG_SH_TOP_OF_MEMORY,
                                                                  DEFAULT_SH_TOP_OF_MEMORY );
        tySemiHostingParams.ulVectorTrapAddress = ToolConf_DLookupUInt( m_RDIConfig,
                                                                  ( tag_t )TAG_SH_VECTOR_TRAP_ADDRESS,
                                                                  DEFAULT_SH_VECTOR_TRAP_ADDRESS );
        tySemiHostingParams.ulDCCHandler = ToolConf_DLookupUInt( m_RDIConfig,
                                                                  ( tag_t )TAG_SH_DCC_HANDLER,
                                                                  DEFAULT_SH_DCC_HANDLER );
        tySemiHostingParams.ulARMSWIValue = ToolConf_DLookupUInt( m_RDIConfig,
                                                                  ( tag_t )TAG_SH_ARM_SWI_VALUE,
                                                                  DEFAULT_SH_ARM_SWI_VALUE );
        tySemiHostingParams.ulThumbSWIValue = ToolConf_DLookupUInt( m_RDIConfig,
                                                                  ( tag_t )TAG_SH_THUMB_SWI_VALUE,
                                                                  DEFAULT_SH_THUMB_SWI_VALUE );
                
    }
}   


/****************************************************************************
  Function   : SetSemiHostingParams()
  Engineer   : Jiju George T
  Input      : tySemiHostingParams - contains the Semi-hosting parameters
  Output     : void
  Description: Sets the given semi hosting parameters.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/


void COpellaConfig::SetSemiHostingParams( TySemiHostingParams &tySemiHostingParams )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SH_TOP_OF_MEMORY,
                           tcnf_UInt, 
                           tySemiHostingParams.ulTopOfMemory );

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SH_VECTOR_TRAP_ADDRESS,
                           tcnf_UInt, 
                           tySemiHostingParams.ulVectorTrapAddress );

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SH_DCC_HANDLER,
                           tcnf_UInt, 
                           tySemiHostingParams.ulDCCHandler );

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SH_ARM_SWI_VALUE,
                           tcnf_UInt, 
                           tySemiHostingParams.ulARMSWIValue );

        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SH_THUMB_SWI_VALUE,
                           tcnf_UInt, 
                           tySemiHostingParams.ulThumbSWIValue );
    }
}


/****************************************************************************
  Function   : IsRDILoggingEnabled()
  Engineer   : Jiju George T
  Input      : void
  Output     : bool - true if enabled. false otherwise
  Description: Checked whether RDI Logging is enabled or not.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

bool COpellaConfig::IsRDILoggingEnabled()
{
    bool bRDILoggingEnabled = DEFAULT_RDI_LOGGING_ENABLED;

    if( m_RDIConfig )
    {
        if( ToolConf_DLookupBool( m_RDIConfig, 
                                  ( tag_t )TAG_RDI_LOGGING_ENABLED,
                                  DEFAULT_RDI_LOGGING_ENABLED ))
        {
            bRDILoggingEnabled = true;
        }
        else
        {
            bRDILoggingEnabled = false;
        }
    }

    return bRDILoggingEnabled;
}

/****************************************************************************
  Function   : IsSafeNVAddressEnabled()
  Engineer   : Suraj S
  Input      : void
  Output     : bool - true if enabled. false otherwise
  Description: Checked whether SafeNonVectorAddress is enabled or not.
  Date           Initials    Description  
  2008-05-22       SJ          Initial
****************************************************************************/

bool COpellaConfig::IsSafeNVAddressEnabled()
{
    bool bSafeNVAddrEnabled = DEFAULT_SAFE_NV_ADDR_ENABLED;

    if( m_RDIConfig )
    {
        if( ToolConf_DLookupBool( m_RDIConfig, 
                                  ( tag_t )TAG_ENABLE_SAFE_NON_VECTOR_ADDRESS,
                                  DEFAULT_SAFE_NV_ADDR_ENABLED ))
        {
            bSafeNVAddrEnabled = true;
        }
        else
        {
            bSafeNVAddrEnabled = false;
        }
    }

    return bSafeNVAddrEnabled;
}

/****************************************************************************
  Function   : EnableRDILogging()
  Engineer   : Jiju George T
  Input      : bEnable - pass true for enabling. false for disabling
  Output     : void
  Description: Enables/ Disables RDI Logging.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::EnableRDILogging( bool bEnable )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig, 
                           ( tag_t )TAG_RDI_LOGGING_ENABLED,
                           tcnf_Bool,
                           bEnable );
    }
}

/****************************************************************************
  Function   : EnableSafeNonVectorAddress()
  Engineer   : Suraj S
  Input      : bEnable - pass true for enabling. false for disabling
  Output     : void
  Description: Enables/ Disables SafeNonVectorAddress.
  Date           Initials    Description  
  2008-05-21       SJ          Initial
****************************************************************************/

void COpellaConfig::EnableSafeNonVectorAddress( bool bEnable )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig, 
                           ( tag_t )TAG_ENABLE_SAFE_NON_VECTOR_ADDRESS,
                           tcnf_Bool,
                           bEnable );
    }
}

/****************************************************************************
  Function   : GetShiftIRPreTCKs()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Clocks
  Description: Returns the Shift IR Pre clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

unsigned char COpellaConfig::GetShiftIRPreTCKs()
{
    unsigned char ucClocks = DEFAULT_SHIFT_IR_PRE_TCKS;

    if( m_RDIConfig )
    {
        ucClocks = ( unsigned char ) ToolConf_DLookupUInt( m_RDIConfig,
                                                            ( tag_t )TAG_SHIFT_IR_PRE_TCKS,
                                                            DEFAULT_SHIFT_IR_PRE_TCKS );
    }

    return ucClocks;
}

/****************************************************************************
  Function   : SetShiftIRPreTCKs()
  Engineer   : Jiju George T
  Input      : ucClocks - clocks to be set
  Output     : void
  Description: Sets the Shift IR Pre clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetShiftIRPreTCKs( unsigned char ucClocks )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SHIFT_IR_PRE_TCKS,
                           tcnf_UInt, 
                           ucClocks );
    }
}

/****************************************************************************
  Function   : GetShiftIRPostTCKs()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Clocks
  Description: Returns the Shift IR Post clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

unsigned char COpellaConfig::GetShiftIRPostTCKs()
{
    unsigned char ucClocks = DEFAULT_SHIFT_IR_POST_TCKS;

    if( m_RDIConfig )
    {
        ucClocks = ( unsigned char ) ToolConf_DLookupUInt( m_RDIConfig,
                                                            ( tag_t )TAG_SHIFT_IR_POST_TCKS,
                                                            DEFAULT_SHIFT_IR_POST_TCKS );
    }

    return ucClocks;
}


/****************************************************************************
  Function   : SetShiftIRPostTCKs()
  Engineer   : Jiju George T
  Input      : ucClocks - clocks to be set
  Output     : void
  Description: Sets the Shift IR Post clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetShiftIRPostTCKs( unsigned char ucClocks )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SHIFT_IR_POST_TCKS,
                           tcnf_UInt, 
                           ucClocks );
    }
}

/****************************************************************************
  Function   : GetShiftDRPreTCKs()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Clocks
  Description: Returns the Shift DR Pre clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

unsigned char COpellaConfig::GetShiftDRPreTCKs()
{
    unsigned char ucClocks = DEFAULT_SHIFT_DR_PRE_TCKS;

    if( m_RDIConfig )
    {
        ucClocks = ( unsigned char ) ToolConf_DLookupUInt( m_RDIConfig,
                                                            ( tag_t )TAG_SHIFT_DR_PRE_TCKS,
                                                            DEFAULT_SHIFT_DR_PRE_TCKS );
    }

    return ucClocks;
}


/****************************************************************************
  Function   : SetShiftDRPreTCKs()
  Engineer   : Jiju George T
  Input      : ucClocks - clocks to be set
  Output     : void
  Description: Sets the Shift DR Pre clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetShiftDRPreTCKs( unsigned char ucClocks )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SHIFT_DR_PRE_TCKS,
                           tcnf_UInt, 
                           ucClocks );
    }
}

/****************************************************************************
  Function   : GetShiftDRPostTCKs()
  Engineer   : Jiju George T
  Input      : void
  Output     : unsigned - Clocks
  Description: Returns the Shift DR Post clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

unsigned char COpellaConfig::GetShiftDRPostTCKs()
{
    unsigned char ucClocks = DEFAULT_SHIFT_DR_POST_TCKS;

    if( m_RDIConfig )
    {
        ucClocks = ( unsigned char ) ToolConf_DLookupUInt( m_RDIConfig,
                                                            ( tag_t )TAG_SHIFT_DR_POST_TCKS,
                                                            DEFAULT_SHIFT_DR_POST_TCKS );
    }

    return ucClocks;

}

/****************************************************************************
  Function   : SetShiftDRPostTCKs()
  Engineer   : Jiju George T
  Input      : ucClocks - clocks to be set
  Output     : void
  Description: Sets the Shift DR Post clocks.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/

void COpellaConfig::SetShiftDRPostTCKs( unsigned char ucClocks )
{
    if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_SHIFT_DR_POST_TCKS,
                           tcnf_UInt, 
                           ucClocks );
    }
}


/****************************************************************************
  Function   : Save()
  Engineer   : Jiju George T
  Input      : csFileName - File to which the configuration have to be saved
  Output     : void
  Description: Stores the current configuration to the given file.
  Date           Initials    Description  
  2007-08-02       JG          Initial
****************************************************************************/


bool COpellaConfig::Save( const char* szFileName )
{
    if( !m_RDIConfig )
    {
        return false;
    }

    char szFileNameWithExtension[MAX_FILE_NAME_LENGTH] = { 0 };
    strcpy( szFileNameWithExtension, szFileName );
    strcat( szFileNameWithExtension,".cnf" );
    
    if( ToolConf_Write( m_RDIConfig, szFileNameWithExtension ))
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

/****************************************************************************
  Function    : SetDeviceRegIndex()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : uiIndex - Device register Index to be set
  Output      : void
  Description : Sets the Device register Index.
  Date           Initials    Description  
  2008-05-28       JCK          Initial
****************************************************************************/
void COpellaConfig::SetDeviceRegIndex(unsigned int uiIndex)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
                           ( tag_t )TAG_DEVICE_REG_INDEX,
                           tcnf_UInt, 
                           uiIndex );
    }
}

/****************************************************************************
  Function    : GetDeviceRegIndex()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : Returns the Device register Index.
  Date           Initials    Description  
  2008-05-28       JCK          Initial
****************************************************************************/
unsigned int COpellaConfig::GetDeviceRegIndex()
{

	 unsigned int uiIndex;
	 uiIndex=ToolConf_DLookupUInt( m_RDIConfig,
                                  ( tag_t )TAG_DEVICE_REG_INDEX,
												0 );

	 return uiIndex;
}

/****************************************************************************
  Function    : SetArchVersion()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : Returns the Device register Index.
  Date           Initials    Description  
  2009-11-24       JCK          Initial
****************************************************************************/
void COpellaConfig::SetArchVersion( const char* szVesrsion)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_DEVICE_ARCH,
			tcnf_String, 
			szVesrsion );
    }
}

/****************************************************************************
  Function    : GetArchVersion()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : char *szDeviceArch - pointer to store Device Architecture version
  Output      : none 
  Description : Returns the Device Architecture version.
  Date           Initials    Description  
  2009-11-24       JCK         Initial
  2010-08-12	   SJ		   Modified to avoid memory leaks	
****************************************************************************/
//char* COpellaConfig::GetArchVersion( void)
void COpellaConfig::GetArchVersion( char *szDeviceArch )
{
	//char *szDeviceArch = new char [MAX_VALUE_LENGTH];
	if( m_RDIConfig )
    {
		const char *szValue = ToolConf_Lookup( m_RDIConfig, ( tag_t ) TAG_DEVICE_ARCH );
		
		if( 0 != szValue )
        {
            if( szDeviceArch )
				strcpy( szDeviceArch , szValue );
        }
    }

	//return szDeviceArch; 
}

/****************************************************************************
  Function    : SetDebugBaseAddress()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int uiDebugBaseAddr - Device register Index 
  Description : To set the debug base address.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetDebugBaseAddress( unsigned int uiDebugBaseAddr)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_DEBUG_BASE,
			tcnf_UInt, 
			uiDebugBaseAddr );
    }
}

/****************************************************************************
  Function    : GetDebugBaseAddress()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : To set the debug base address.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
unsigned int COpellaConfig::GetDebugBaseAddress( void)
{
	unsigned int uiDebugBaseAddr = 0x0;
	if( m_RDIConfig )
    {
		uiDebugBaseAddr = ToolConf_DLookupUInt( m_RDIConfig, 
                                                ( tag_t ) TAG_DEBUG_BASE,
                                                0x0);
		
		
    }

	return uiDebugBaseAddr; 
}

/****************************************************************************
  Function    : SetDebugApIndex()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : To set the debug access port index.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetDebugApIndex( unsigned int uiDebugApIndex)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_DEBUG_AP_INDEX,
			tcnf_UInt, 
			uiDebugApIndex );
    }
}

/****************************************************************************
  Function    : GetDebugApIndex()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : Returns the debug access port index.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
unsigned int COpellaConfig::GetDebugApIndex( void)
{
	unsigned int uiDebugApIndex  = 0x0;
	if( m_RDIConfig )
    {
		uiDebugApIndex = ToolConf_DLookupUInt( m_RDIConfig, 
                                               ( tag_t ) TAG_DEBUG_AP_INDEX,
                                               0x0);
		
		
    }

	return uiDebugApIndex; 
}

/****************************************************************************
  Function    : SetMemoryApIndex()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : To set the memory access port index
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetMemoryApIndex( unsigned int uiMemApIndex)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_MEMORY_AP_INDEX,
			tcnf_UInt, 
			uiMemApIndex );
    }
}

/****************************************************************************
  Function    : GetMemoryApIndex()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : Returns the memory Access port index.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
unsigned int COpellaConfig::GetMemoryApIndex( void)
{
	unsigned int uiMemApIndex = 0x0;
	if( m_RDIConfig )
    {
		uiMemApIndex = ToolConf_DLookupUInt( m_RDIConfig,
                                                   ( tag_t ) TAG_MEMORY_AP_INDEX,
                                                   0x0);
		
		
    }

	return uiMemApIndex; 
}

/****************************************************************************
  Function    : SetUseMemApForMemAccess()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiUseMemApIndex -  
  Description : To set the use memory access port index for memory access
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetUseMemApForMemAccess( unsigned int uiUseMemApForMemAccess)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_USE_MEMORY_AP_FOR_MEM_ACCESS,
			tcnf_UInt, 
			uiUseMemApForMemAccess );
    }
}

/****************************************************************************
  Function    : GetUseMemApForMemAccess()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiUseMemApIndex -
  Description : Returns the use memory Access port index or debug access
			  : for memory access.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
unsigned int COpellaConfig::GetUseMemApForMemAccess( void)
{
	unsigned int uiUseMemApForMemAccess = 0x0;
	if( m_RDIConfig )
    {
		uiUseMemApForMemAccess = ToolConf_DLookupUInt( m_RDIConfig,
                                                   ( tag_t ) TAG_USE_MEMORY_AP_FOR_MEM_ACCESS,
                                                   0x0);
		
		
    }

	return uiUseMemApForMemAccess; 
}

/****************************************************************************
  Function    : SetCoresightCompliant()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : BOOL bCoresightComplianet - TRUE/FALSE
  Output      : void 
  Description : To set the Coresight compliant or not.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetCoresightCompliant(BOOL bCoresightCompliant)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_CORESIGHT_COMPLIANT,
			tcnf_Bool, 
			bCoresightCompliant );
    }
}

/****************************************************************************
  Function    : IsCoresightCompliant()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : BOOL - TRUE - if Coresight compliant
					   FALSE - if not Coresight compliant
  Description : Returns whether the target is Coresight compliant or not.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
BOOL COpellaConfig::IsCoresightCompliant( void)
{
	BOOL bCoresightCompliant = FALSE;
	if( m_RDIConfig )
    {
		bCoresightCompliant = ToolConf_DLookupBool( m_RDIConfig, 
                ( tag_t ) TAG_CORESIGHT_COMPLIANT,
                FALSE);
		
    }
    
	return bCoresightCompliant; 
}

/****************************************************************************
  Function    : GetTISubPortNumber()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - uiIndex - Device register Index 
  Description : Returns the Coresight compliant or not.
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
unsigned long COpellaConfig::GetTISubPortNumber( void)
{
	unsigned long ulTISubPortNum;
	if( m_RDIConfig )
    {
		ulTISubPortNum = ToolConf_DLookupUInt( m_RDIConfig, 
											   ( tag_t ) TAG_TI_SUB_PORT,
                                               0x0);
    }
    
	return ulTISubPortNum; 
}

/****************************************************************************
  Function    : SetTISubPortNumber()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : unsigned int - ulTISubPortNum - Port Number of the  
  Description : 
  Date           Initials    Description  
  11-Mar-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetTISubPortNumber(unsigned long ulTISubPortNum)
{
	if( m_RDIConfig )
    {
		ToolConf_AddTyped( m_RDIConfig,
							( tag_t )TAG_TI_SUB_PORT,
							tcnf_UInt, 
							ulTISubPortNum );
    }
}

/****************************************************************************
  Function    : SetTICore()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : BOOL - bTICore - 
  Description : 
  Date           Initials    Description  
  24-May-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetTICore(BOOL bTICore)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_TI_COMPLIANT,
			tcnf_Bool, 
			bTICore );
    }
}

/****************************************************************************
  Function    : IsTICore()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : BOOL - bTICore - 
  Description : Returns the TI compliant or not.
  Date           Initials    Description  
  24-May-2010      JCK          Initial
****************************************************************************/
BOOL COpellaConfig::IsTICore( void)
{
	BOOL bTICore = FALSE;
	if( m_RDIConfig )
    {
		bTICore = ToolConf_DLookupBool( m_RDIConfig, 
                ( tag_t ) TAG_TI_COMPLIANT,
                FALSE);
		
    }
    
	return bTICore; 
}

/****************************************************************************
  Function    : SetDapPcSupport()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : BOOL - bDapPcSupport - DAP PC Control is necessary or not
  Description : In case of saome of the TI Cores, DAP PC control is necessary 
			  : for Debug support. E.g. For OMAP3430 For adding support for 
			  : Cortex-A8 DAP PC Configuration is necessary but for ARM9 
			  : DAP PC Control Configuration is not needed.
  Date           Initials    Description  
  256-May-2010      JCK          Initial
****************************************************************************/
void COpellaConfig::SetDapPcSupport(BOOL bDapPcSupport)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_TI_DAPPCSUPPORT,
			tcnf_Bool, 
			bDapPcSupport );
    }
}

/****************************************************************************
  Function    : IsDapPcSupported()
  Engineer    : Jeenus Chalattu Kunnath
  Input       : void
  Output      : BOOL - bDapPcSupportbTICore - DAP PC control  is needed or not.
  Description : Returns the DAP PC Supported or not.
  Date           Initials    Description  
  26-May-2010      JCK          Initial
****************************************************************************/
BOOL COpellaConfig::IsDapPcSupported( void)
{
	BOOL bDapPcSupport = FALSE;
	if( m_RDIConfig )
    {
		bDapPcSupport = ToolConf_DLookupBool( m_RDIConfig, 
                ( tag_t ) TAG_TI_DAPPCSUPPORT,
                FALSE);
		
    }
    
	return bDapPcSupport; 
}

/****************************************************************************
  Function    : SetCacheInvalidationParam()
  Engineer    : Suraj S
  Input       : BOOL bCacheInvalidationNeeded - TRUE/FALSE
  Output      : void 
  Description : To set whether CacheInvlaidation workaround needed or not.
  Date           Initials    Description  
  11-Mar-2011      SJ          Initial
****************************************************************************/
void COpellaConfig::SetCacheInvalidationParam(BOOL bCacheInvalidationNeeded)
{
	if( m_RDIConfig )
    {
        ToolConf_AddTyped( m_RDIConfig,
			( tag_t )TAG_CACHE_INVALIDATION_NEEDED,
			tcnf_Bool, 
			bCacheInvalidationNeeded );
    }
}

/****************************************************************************
  Function    : GetCacheInvalidationParam()
  Engineer    : Suraj S
  Input       : void
  Output      : BOOL - TRUE - if CacheInvlaidation workaround needed
					   FALSE - if CacheInvlaidation workaround not needed 
  Description : Returns whether CacheInvlaidation workaround needed or not
  Date           Initials    Description  
  11-Mar-2011      SJ          Initial
****************************************************************************/
BOOL COpellaConfig::GetCacheInvalidationParam( void)
{
	BOOL bCacheInvalidationNeeded = FALSE;
	if( m_RDIConfig )
    {
		bCacheInvalidationNeeded = ToolConf_DLookupBool( m_RDIConfig, 
                ( tag_t ) TAG_CACHE_INVALIDATION_NEEDED,
                FALSE);
		
    }
    
	return bCacheInvalidationNeeded; 
}

