; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=AddDelay
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "opellaarmconfig.h"
LastPage=0

ClassCount=18
Class1=CAdvancedDlg
Class2=CDeviceDlg
Class3=CDiagnosticsDlg
Class4=CLoadSaveDlg
Class5=COpellaARMConfigDlg
Class6=COpellaConfigPageBase
Class7=CPropPageFrameDefault
Class8=CPropPageFrameEx
Class9=CResizablePage
Class10=CTargetConnectionDlg
Class11=CTargetResetDlg
Class12=CTreePropSheet
Class13=CTreePropSheetBase
Class14=CTreePropSheetEx
Class15=CTreePropSheetSplitter
Class16=CTreePropSheetTreeCtrl

ResourceCount=16
Resource1=IDD_TARGET_RESET_DLG (English (U.S.))
Resource2=IDD_ADVANCED_DLG (English (U.S.))
Resource3=IDD_TARGET_CONN_DLG (English (U.S.))
Resource4=IDD_DEVICE_DLG (English (U.S.))
Resource5=IDD_LOADSTORE_DLG (English (U.S.))
Resource6=IDD_DIAGNOSTICS_DLG (English (U.S.))
Resource7=IDD_DEVICE_DLG
Resource8=IDD_ADD_ITEM
Resource9=IDD_TARGET_RESET_DLG
Resource10=IDD_ADVANCED_DLG
Resource11=IDD_DIAGNOSTICS_DLG
Resource12=IDD_CORESIGHT_CONFI_DLG
Resource13=IDD_TARGET_CONN_DLG
Resource14=IDD_LOADSTORE_DLG
Class17=CCoresightDlg
Resource15=IDD_USER_REG_DIALOG
Class18=AddDelay
Resource16=IDD_ADD_DELAY

[CLS:CAdvancedDlg]
Type=0
BaseClass=COpellaConfigPageBase
HeaderFile=AdvancedDlg.h
ImplementationFile=AdvancedDlg.cpp
LastObject=IDC_CACHE_CLEAN_ADDRESS

[CLS:CDeviceDlg]
Type=0
BaseClass=COpellaConfigPageBase
HeaderFile=DeviceDlg.h
ImplementationFile=DeviceDlg.cpp
LastObject=IDC_CORESIGHT_CONFIG

[CLS:CDiagnosticsDlg]
Type=0
BaseClass=COpellaConfigPageBase
HeaderFile=DiagnosticsDlg.h
ImplementationFile=DiagnosticsDlg.cpp
LastObject=CDiagnosticsDlg

[CLS:CLoadSaveDlg]
Type=0
BaseClass=CResizablePage
HeaderFile=LoadSaveDlg.h
ImplementationFile=LoadSaveDlg.cpp
LastObject=IDC_SAVE

[CLS:COpellaARMConfigDlg]
Type=0
BaseClass=CTreePropSheetEx
HeaderFile=OpellaARMConfigDlg.h
ImplementationFile=OpellaARMConfigDlg.cpp

[CLS:COpellaConfigPageBase]
Type=0
BaseClass=CResizablePage
HeaderFile=OpellaConfigPageBase.h
ImplementationFile=OpellaConfigPageBase.cpp
LastObject=COpellaConfigPageBase

[CLS:CPropPageFrameDefault]
Type=0
BaseClass=CWnd
HeaderFile=PropPageFrameDefault.h
ImplementationFile=PropPageFrameDefault.cpp

[CLS:CPropPageFrameEx]
Type=0
BaseClass=CWnd
HeaderFile=PropPageFrameEx.h
ImplementationFile=PropPageFrameEx.cpp

[CLS:CResizablePage]
Type=0
BaseClass=CPropertyPage
HeaderFile=ResizablePage.h
ImplementationFile=ResizablePage.cpp

[CLS:CTargetConnectionDlg]
Type=0
BaseClass=CResizablePage
HeaderFile=TargetConnectionDlg.h
ImplementationFile=TargetConnectionDlg.cpp
LastObject=IDC_TARGET_VOLTAGE

[CLS:CTargetResetDlg]
Type=0
BaseClass=COpellaConfigPageBase
HeaderFile=TargetResetDlg.h
ImplementationFile=TargetResetDlg.cpp
LastObject=CTargetResetDlg

[CLS:CTreePropSheet]
Type=0
BaseClass=CPropertySheet
HeaderFile=TreePropSheet.h
ImplementationFile=TreePropSheet.cpp

[CLS:CTreePropSheetBase]
Type=0
BaseClass=CPropertySheet
HeaderFile=TreePropSheetBase.h
ImplementationFile=TreePropSheetBase.cpp

[CLS:CTreePropSheetEx]
Type=0
BaseClass=CTreePropSheetBase
HeaderFile=TreePropSheetEx.h
ImplementationFile=TreePropSheetEx.cpp

[CLS:CTreePropSheetSplitter]
Type=0
BaseClass=CWnd
HeaderFile=TreePropSheetSplitter.h
ImplementationFile=TreePropSheetSplitter.cpp

[CLS:CTreePropSheetTreeCtrl]
Type=0
BaseClass=CTreeCtrl
HeaderFile=TreePropSheetTreeCtrl.h
ImplementationFile=TreePropSheetTreeCtrl.cpp

[DLG:IDD_ADVANCED_DLG]
Type=1
Class=CAdvancedDlg
ControlCount=13
Control1=IDC_ADVCACHE_GROUP,button,1342177287
Control2=IDC_CACHE_CLEAN_ADDRESS,edit,1350631552
Control3=IDC_STATIC,static,1342308352
Control4=IDC_SAFE_NON_VECTOR_ADDRESS,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_ADVSAFENONVECTADDR_GROUP,button,1342177287
Control7=IDC_TARGETEXEPOL_GROUP,button,1342177287
Control8=IDC_EXECUTION_POLING_RATE,edit,1350639744
Control9=IDC_STATIC,static,1342308352
Control10=IDC_SEMHOSTTYPE_GROUP,button,1342177287
Control11=IDC_ARM_SEMIHOSTING_RADIO,button,1342308361
Control12=IDC_GNU_SEMIHOSTING_RADIO,button,1342177289
Control13=IDC_ENABLE_SAFE_NV_ADDR,button,1342242819

[DLG:IDD_DEVICE_DLG]
Type=1
Class=CDeviceDlg
ControlCount=44
Control1=IDC_DEV_SELECTION,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_CORELIST_COMBO,combobox,1344339971
Control4=IDC_STATIC,static,1342308352
Control5=IDC_SEL_CORE_COMBO,combobox,1344340227
Control6=IDC_MC_GROUP,button,1342177287
Control7=IDC_STATIC,static,1342308352
Control8=IDC_NAME_STATIC_0,static,1342312448
Control9=IDC_TYPE_CHECK_0,button,1342242819
Control10=IDC_TYPE_STATIC_0,static,1342308352
Control11=IDC_WIDTH_STATIC_0,static,1342308352
Control12=IDC_DEV_DETAILS_GROUP,button,1342177287
Control13=IDC_NAME_STATIC_1,static,1342312448
Control14=IDC_TYPE_CHECK_1,button,1342242819
Control15=IDC_TYPE_STATIC_1,static,1342308352
Control16=IDC_WIDTH_STATIC_1,static,1342308352
Control17=IDC_NAME_STATIC_2,static,1342312448
Control18=IDC_TYPE_CHECK_2,button,1342242819
Control19=IDC_TYPE_STATIC_2,static,1342308352
Control20=IDC_WIDTH_STATIC_2,static,1342308352
Control21=IDC_NAME_STATIC_3,static,1342312448
Control22=IDC_TYPE_CHECK_3,button,1342242819
Control23=IDC_TYPE_STATIC_3,static,1342308352
Control24=IDC_WIDTH_STATIC_3,static,1342308352
Control25=IDC_NAME_STATIC_4,static,1342312448
Control26=IDC_TYPE_CHECK_4,button,1342242819
Control27=IDC_TYPE_STATIC_4,static,1342308352
Control28=IDC_WIDTH_STATIC_4,static,1342308352
Control29=IDC_DEVICE_COUNT_COMBO,combobox,1344339971
Control30=IDC_WIDTH_COMBO_0,combobox,1344339971
Control31=IDC_WIDTH_COMBO_1,combobox,1344339971
Control32=IDC_WIDTH_COMBO_2,combobox,1344339971
Control33=IDC_WIDTH_COMBO_3,combobox,1344339971
Control34=IDC_WIDTH_COMBO_4,combobox,1344339971
Control35=IDC_SCROLLBAR,scrollbar,1342177281
Control36=IDC_ENDIAN_GROUP,button,1342177287
Control37=IDC_BIGENDIAN_RADIO,button,1342308361
Control38=IDC_LITTLEENDIAN_RADIO2,button,1342177289
Control39=IDC_STATIC,button,1342177287
Control40=IDC_BUTTON_USER_REG_SETTINS,button,1342242816
Control41=IDC_STATIC,button,1342177287
Control42=IDC_DEVICE_COMBO,combobox,1478557699
Control43=IDC_STATIC,button,1342177287
Control44=IDC_CORESIGHT_CONFIG,button,1476460544

[DLG:IDD_DIAGNOSTICS_DLG]
Type=1
Class=CDiagnosticsDlg
ControlCount=8
Control1=IDC_STATIC,button,1342177287
Control2=IDC_OPELLA_CHECK,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_TARGET_STATUS,edit,1353777348
Control5=IDC_TARGET_CHECK,button,1342242816
Control6=IDC_ENABLE_RDI_LOGGING,button,1342242819
Control7=IDC_OPELLA_STATUS,edit,1353777348
Control8=IDC_HARD_RESET,button,1342242816

[DLG:IDD_LOADSTORE_DLG]
Type=1
Class=CLoadSaveDlg
ControlCount=7
Control1=IDC_LOADCONFIG_GROUP,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_CONFIGFILEPATH_EDIT,edit,1350631552
Control4=IDC_BROWSECOFIGLOAD_BTN,button,1342242816
Control5=IDC_LOAD_BTN,button,1342242816
Control6=IDC_SAVECONFIG_GROUP,button,1342177287
Control7=IDC_SAVE,button,1342242816

[DLG:IDD_TARGET_CONN_DLG]
Type=1
Class=CTargetConnectionDlg
ControlCount=9
Control1=IDC_USB_GROUP,button,1342177287
Control2=IDC_DETECTED_OPELLA_COMBO,combobox,1344339971
Control3=IDC_RECHECK_FOR_OPELLA,button,1342242816
Control4=IDC_JTAGCLKFREQ_GROUP,button,1342177287
Control5=IDC_JTAG_FREQ_COMBO,combobox,1344339971
Control6=IDC_FIXED_FREQ_RADIO,button,1342308361
Control7=IDC_ADAPTIVE_FREQ_RADIO,button,1342177289
Control8=IDC_STATIC,button,1342177287
Control9=IDC_TARGET_VOLTAGE,combobox,1344339971

[DLG:IDD_TARGET_RESET_DLG]
Type=1
Class=CTargetResetDlg
ControlCount=11
Control1=IDC_NO_RESET,button,1342308361
Control2=IDC_RESET_AND_DELAY,button,1342177289
Control3=IDC_RESET_AND_HALT,button,1342177289
Control4=IDC_HOT_PLUG,button,1342177289
Control5=IDC_CORE_ONLY,button,1342177289
Control6=IDC_CORE_AND_PERIPHERALS,button,1342177289
Control7=IDC_STARTUP_DELAY,combobox,1478557699
Control8=IDC_STATIC,static,1342308352
Control9=IDC_USE_NTRST,button,1342242819
Control10=IDC_TARGETRST_GROUP,button,1342177287
Control11=IDC_SET_DEBUG_REQUEST,button,1342242819

[DLG:IDD_DEVICE_DLG (English (U.S.))]
Type=1
Class=CDeviceDlg
ControlCount=38
Control1=IDC_DEV_SELECTION,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_CORELIST_COMBO,combobox,1344340227
Control4=IDC_STATIC,static,1342308352
Control5=IDC_SEL_CORE_COMBO,combobox,1344340227
Control6=IDC_MC_GROUP,button,1342177287
Control7=IDC_STATIC,static,1342308352
Control8=IDC_NAME_STATIC_0,static,1342312448
Control9=IDC_TYPE_CHECK_0,button,1342242819
Control10=IDC_TYPE_STATIC_0,static,1342308352
Control11=IDC_WIDTH_STATIC_0,static,1342308352
Control12=IDC_DEV_DETAILS_GROUP,button,1342177287
Control13=IDC_NAME_STATIC_1,static,1342312448
Control14=IDC_TYPE_CHECK_1,button,1342242819
Control15=IDC_TYPE_STATIC_1,static,1342308352
Control16=IDC_WIDTH_STATIC_1,static,1342308352
Control17=IDC_NAME_STATIC_2,static,1342312448
Control18=IDC_TYPE_CHECK_2,button,1342242819
Control19=IDC_TYPE_STATIC_2,static,1342308352
Control20=IDC_WIDTH_STATIC_2,static,1342308352
Control21=IDC_NAME_STATIC_3,static,1342312448
Control22=IDC_TYPE_CHECK_3,button,1342242819
Control23=IDC_TYPE_STATIC_3,static,1342308352
Control24=IDC_WIDTH_STATIC_3,static,1342308352
Control25=IDC_NAME_STATIC_4,static,1342312448
Control26=IDC_TYPE_CHECK_4,button,1342242819
Control27=IDC_TYPE_STATIC_4,static,1342308352
Control28=IDC_WIDTH_STATIC_4,static,1342308352
Control29=IDC_DEVICE_COUNT_COMBO,combobox,1344339971
Control30=IDC_WIDTH_COMBO_0,combobox,1344339971
Control31=IDC_WIDTH_COMBO_1,combobox,1344339971
Control32=IDC_WIDTH_COMBO_2,combobox,1344339971
Control33=IDC_WIDTH_COMBO_3,combobox,1344339971
Control34=IDC_WIDTH_COMBO_4,combobox,1344339971
Control35=IDC_SCROLLBAR,scrollbar,1342177281
Control36=IDC_ENDIAN_GROUP,button,1342177287
Control37=IDC_BIGENDIAN_RADIO,button,1342308361
Control38=IDC_LITTLEENDIAN_RADIO2,button,1342177289

[DLG:IDD_TARGET_CONN_DLG (English (U.S.))]
Type=1
Class=CTargetConnectionDlg
ControlCount=9
Control1=IDC_USB_GROUP,button,1342177287
Control2=IDC_DETECTED_OPELLA_COMBO,combobox,1344339971
Control3=IDC_RECHECK_FOR_OPELLA,button,1342242816
Control4=IDC_JTAGCLKFREQ_GROUP,button,1342177287
Control5=IDC_JTAG_FREQ_COMBO,combobox,1344339971
Control6=IDC_FIXED_FREQ_RADIO,button,1342308361
Control7=IDC_ADAPTIVE_FREQ_RADIO,button,1342177289
Control8=IDC_STATIC,button,1342177287
Control9=IDC_TARGET_VOLTAGE,combobox,1344339971

[DLG:IDD_TARGET_RESET_DLG (English (U.S.))]
Type=1
Class=CTargetResetDlg
ControlCount=9
Control1=IDC_NO_RESET,button,1342308361
Control2=IDC_RESET_AND_DELAY,button,1342177289
Control3=IDC_RESET_AND_HALT,button,1342177289
Control4=IDC_HOT_PLUG,button,1342177289
Control5=IDC_STARTUP_DELAY,combobox,1478557699
Control6=IDC_STATIC,static,1342308352
Control7=IDC_USE_NTRST,button,1342242819
Control8=IDC_TARGETRST_GROUP,button,1342177287
Control9=IDC_SET_DEBUG_REQUEST,button,1342242819

[DLG:IDD_ADVANCED_DLG (English (U.S.))]
Type=1
Class=CAdvancedDlg
ControlCount=12
Control1=IDC_ADVCACHE_GROUP,button,1342177287
Control2=IDC_CACHE_CLEAN_ADDRESS,edit,1350631552
Control3=IDC_STATIC,static,1342308352
Control4=IDC_SAFE_NON_VECTOR_ADDRESS,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_ADVSAFENONVECTADDR_GROUP,button,1342177287
Control7=IDC_TARGETEXEPOL_GROUP,button,1342177287
Control8=IDC_EXECUTION_POLING_RATE,edit,1350639744
Control9=IDC_STATIC,static,1342308352
Control10=IDC_SEMHOSTTYPE_GROUP,button,1342177287
Control11=IDC_ARM_SEMIHOSTING_RADIO,button,1342308361
Control12=IDC_GNU_SEMIHOSTING_RADIO,button,1342177289

[DLG:IDD_LOADSTORE_DLG (English (U.S.))]
Type=1
Class=CLoadSaveDlg
ControlCount=7
Control1=IDC_LOADCONFIG_GROUP,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_CONFIGFILEPATH_EDIT,edit,1350631552
Control4=IDC_BROWSECOFIGLOAD_BTN,button,1342242816
Control5=IDC_LOAD_BTN,button,1342242816
Control6=IDC_SAVECONFIG_GROUP,button,1342177287
Control7=IDC_SAVE,button,1342242816

[DLG:IDD_DIAGNOSTICS_DLG (English (U.S.))]
Type=1
Class=CDiagnosticsDlg
ControlCount=11
Control1=IDC_STATIC,button,1342177287
Control2=IDC_RDI_DRIVER_VERSION,static,1342308352
Control3=IDC_STATIC,button,1342177287
Control4=IDC_OPELLA_CHECK,button,1342242816
Control5=IDC_STATIC,button,1342177287
Control6=IDC_TARGET_STATUS,edit,1352730820
Control7=IDC_TARGET_CHECK,button,1342242816
Control8=IDC_ENABLE_RDI_LOGGING,button,1342242819
Control9=IDC_STATIC,static,1342308352
Control10=IDC_OPELLA_STATUS,edit,1352730820
Control11=IDC_HARD_RESET,button,1342242816

[DLG:IDD_USER_REG_DIALOG]
Type=1
Class=?
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_USER_LIST,SysListView32,1350664205
Control5=IDC_ADDFILE,button,1342242816
Control6=IDC_STOREFILE,button,1342242816
Control7=IDC_ADD,button,1342242816
Control8=IDC_EDIT,button,1342242816
Control9=IDC_REMOVE,button,1342242816
Control10=IDC_REMOVE_ALL,button,1342242816
Control11=IDC_ADD_DELAY,button,1342242816

[DLG:IDD_ADD_ITEM]
Type=1
Class=?
ControlCount=12
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_HELP_TOPIC,button,1342242816
Control4=IDC_STATIC,button,1342177287
Control5=IDC_STATIC,static,1342308352
Control6=IDC_NAME,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_BYTE_SIZE,edit,1350631552
Control9=IDC_STATIC,static,1342308352
Control10=IDC_ADDRESS,edit,1350631552
Control11=IDC_STATIC,static,1342308352
Control12=IDC_VALUE,edit,1350631552

[DLG:IDD_CORESIGHT_CONFI_DLG]
Type=1
Class=CCoresightDlg
ControlCount=16
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_CORESIGHT_ADDRESS,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_DBG_AP_IDX,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_MEM_AP_IDX,edit,1350631552
Control10=IDC_JTAG_AP_IDX,edit,1484849280
Control11=IDC_STATIC,button,1342177287
Control12=IDC_ROM_TABLE_EDIT,edit,1353777348
Control13=IDC_ROM_TABLE_READ,button,1476460544
Control14=IDC_STATIC,button,1342177287
Control15=IDC_ROM_BASE_ADDR,edit,1350631552
Control16=IDC_STATIC,static,1342308352

[CLS:CCoresightDlg]
Type=0
HeaderFile=CoresightDlg.h
ImplementationFile=CoresightDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CCoresightDlg
VirtualFilter=dWC

[DLG:IDD_ADD_DELAY]
Type=1
Class=AddDelay
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_DELAY_EDIT,edit,1350631552
Control4=IDC_DELAY,static,1342308352

[CLS:AddDelay]
Type=0
HeaderFile=AddDelay.h
ImplementationFile=AddDelay.cpp
BaseClass=CDialog
Filter=D
LastObject=AddDelay
VirtualFilter=dWC

