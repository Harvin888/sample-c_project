/****************************************************************************
       Module: DriverMgr.cpp
     Engineer: Jiju George T
  Description: This file contains the implementation of CDriverMgr class
Date           Initials    Description
24-Jul-2007      JG           Initial
12-Aug-2010		 SJ			  Now uses singleton class instance to call RDI functions.
****************************************************************************/

#include "stdafx.h"
#include "DriverMgr.h"
#include "CSingleton.h"

// To be modified with actual Opella-XD RDI layer header
// TODO : Include RDI Driver Header
#include "../opxdarmdll/midlayerarm.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/****************************************************************************
     Function: Constructor
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Show/Hide controls in the given multi core row
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
CDriverMgr::CDriverMgr()
{

}


/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: 
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
CDriverMgr::~CDriverMgr()
{

}

/****************************************************************************
     Function: GetInstance()
     Engineer: Jiju George T
	 Input   : void
	 Output  : CDriverMgr& - Singleton instance
  Description: Returns the singleton instance of this class.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
CDriverMgr& CDriverMgr::GetInstance()
{
    static CDriverMgr SingleInstance;
    return SingleInstance;
}

/****************************************************************************
     Function: DetectConnectedOpellas()
     Engineer: Jiju George T
	 Input   : tyMultipleUSBDevices - [OUT] To be filled will detected
               Opella-XD details
     Output  : void
  Description: Detects the connected Opellas and fills the details to the passed data structure.
Date           Initials    Description
24-Jul-2007      JG          Initial
12-Aug-2010		 SJ			 Now uses singleton class instance to call RDI functions.
****************************************************************************/
void CDriverMgr::DetectConnectedOpellas( TyMultipleUSBDevices& tyMultipleUSBDevices )
{
	CSingleton* instance = CSingleton::getInstance();

    //Blank the array.
/*    for (int nIndex=0; nIndex < MAX_USB_DEVICES_SUPPORTED; ++nIndex )
    {
        tyMultipleUSBDevices.tyUSBDeviceInstance[nIndex].ulInstanceNumber = 0x0;
        memset ((void *) &tyMultipleUSBDevices.tyUSBDeviceInstance[nIndex].szOpellaXDSerialNumber, 0x0,
            (sizeof tyMultipleUSBDevices.tyUSBDeviceInstance[nIndex].szOpellaXDSerialNumber));
    }
*/
    //Reset the serial no. array
    for (int nIndex=0; nIndex < MAX_USB_DEVICES_SUPPORTED; nIndex++)
    strcpy(tyMultipleUSBDevices.szOpellaXDSerialNumber[nIndex], "");

    //Reset the number of devices.
    tyMultipleUSBDevices.ulNumberOfUSBDevices = 0x0;
    tyMultipleUSBDevices.ucSelectedDevice = 0x0;

    // Search for Opella-XD Devices
    // TODO : After integrating with Opella-XD RDI Driver
    //ML_CheckOpellaXDInstance(tyMultipleUSBDevices.szOpellaXDSerialNumber,&tyMultipleUSBDevices.ulNumberOfUSBDevices);
    instance->m_ML_CheckOpellaXDInstance(tyMultipleUSBDevices.szOpellaXDSerialNumber,&tyMultipleUSBDevices.ulNumberOfUSBDevices);
	SetOpellaXDSerialNumber( tyMultipleUSBDevices.szOpellaXDSerialNumber[tyMultipleUSBDevices.ucSelectedDevice] );
}

/****************************************************************************
     Function: GetOpellaStatus()
     Engineer: Jiju George T
	 Input   : tyUSBDeviceInstance - Details of the Opella of which status
               needs to be obtained.
     Output  : CString             - Status of the given Opella
   Description: This method tries to connect to an opella with the given details.
               Returns a multi line formatted string with the result of connection
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
CString CDriverMgr::GetOpellaStatus( TyUSBDeviceInstance &tyUSBDeviceInstance )
{
    // TODO : After integrating with Opella-XD RDI Driver
    return "Working properly.";
}


/****************************************************************************
     Function: GetTargetStatus()
     Engineer: Jiju George T
	 Input   : tyUSBDeviceInstance - Details of the Opella on which the target
               to be tested is conencted.
     Output  : CString             - Status of the target.
  Description: This method tries to connect to the target attached to an opella
			   with the given details.Returns a multi line formatted string with
			   the result of connection.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
CString CDriverMgr::GetTargetStatus( TyUSBDeviceInstance &tyUSBDeviceInstance )
{
    // TODO : After integrating with Opella-XD RDI Driver
    return "Target not connected or powerd up";
}


/****************************************************************************
     Function: GetRDIDriverVersion()
     Engineer: Jiju George T
	 Input   : void
	 Output  : CString - RDI Driver DLL Version
  Description: Should return the current version of ARM RDI Driver DLL 
Date           Initials    Description
24-Jul-2007        JG         Initial
12-Aug-2010		   SJ		  Now uses singleton class instance to call RDI functions.
****************************************************************************/
CString CDriverMgr::GetRDIDriverVersion()
{
	CSingleton* instance = CSingleton::getInstance();

    //return GetRDIDLLVersion();
    return instance->m_GetRDIDLLVersion();
}


/****************************************************************************
     Function: GetFirmwareVersion()
     Engineer: Jiju George T
	 Input   : void
	 Output  : CString - Firmware version
  Description: Should return the current version of OpellaXD Firmware
Date           Initials    Description
24-Jul-2007       JG          Initial
12-Aug-2010		  SJ		  Now uses singleton class instance to call RDI functions.
****************************************************************************/
CString CDriverMgr::GetFirmwareVersion()
{
	CSingleton* instance = CSingleton::getInstance();

    //return GetFirmwareInfo();
    return instance->m_GetFirmwareInfo();
}
