/****************************************************************************
       Module: DeviceDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CDeviceDlg class
Date           Initials    Description
24-Jul-2007       JG         Initial
21-Feb-2008       SJ         Added Support for Diagnostics
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#ifndef _DEVICE_DLG_H_
#define _DEVICE_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OpellaConfigPageBase.h"
#include "UserRegSettings.h"
#include "CoresightDlg.h"

class COpellaConfig;

/****************************************************************************
     Function: CDeviceDlg
     Engineer: Jiju George T
  Description: Displays the device section details
Date           Initials    Description
24-Jul-2007       JG         Initial
21-Feb-2008       SJ         Added Support for Diagnostics 
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/
class CDeviceDlg : public COpellaConfigPageBase
{

// Construction
public:
    void UpdateUI();
    void InitializeUIData ( COpellaConfig &OpellaConfig );
    bool ApplyChanges(COpellaConfig &OpellaConfig);
    CDeviceDlg( COpellaConfig &OpellaConfig );
    CDeviceDlg();
    ~CDeviceDlg();
    CString SelectedCoreType;
    CString GetSelectedCoreDetails();
	CString GetSelectedDeviceName();
    int GetSelectedCoreNumber();
    TyJtagScanChain* GetJtagScanDetails();

// Dialog Data
    //{{AFX_DATA(CDeviceDlg)
    enum { IDD = IDD_DEVICE_DLG };
	CButton	m_CoresightButton;
	 CComboBox	m_DeviceCombo;
	 CComboBox	m_deviceCombo;
    CScrollBar  m_CoreScrollBar;
    CComboBox   m_DeviceCountCombo;
    CComboBox   m_SelCoreCombo;
    CComboBox   m_CoreListCombo;
    int     m_nEndian;
	 int		m_RegFileIndex;
    //}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CDeviceDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CDeviceDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnSelchangeDeviceCountCombo();
    afx_msg void OnTypeCheck0();
    afx_msg void OnTypeCheck1();
    afx_msg void OnTypeCheck2();
    afx_msg void OnTypeCheck3();
    afx_msg void OnTypeCheck4();
    afx_msg void OnSelchangeWidthCombo0();
    afx_msg void OnSelchangeWidthCombo1();
    afx_msg void OnSelchangeWidthCombo2();
    afx_msg void OnSelchangeWidthCombo3();
    afx_msg void OnSelchangeWidthCombo4();
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnSelchangeSelCoreCombo();
    afx_msg void OnSelchangeCorelistCombo();
	afx_msg void OnUserRegisterSettings();
	afx_msg void OnSelchangeDeviceCombo();
	afx_msg void OnCoresightConfig();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    void FillDeviceList();
    void FillSelectedCoreList();
    void FillCoreCountList();
    void FillIRLengthCombos();
    void FillMultiCoreDetails( COpellaConfig OpellaConfig );
    void UpdateMultiCoreDisplay();
    void ShowMutiCoreRow( int nControlIndex ,BOOL bShow, BOOL bEnableARMCheck );
    void UpdateMultiCoreRow( int nControlIndex );
    void UpdateCoreScroll();
    CString GetDebuggerName();
	 void FillDeviceRegList();

     void InitializeCoresight( COpellaConfig &OpellaConfig );
private:
    COpellaConfig &m_OpellaConfig;
    TyJtagScanChain m_tyJtagScanChain;
    int m_nIndexOfCoreDisplayedOnTop;
    int m_nSelectedCore;
    CString m_csSelectedDevice;
    CString m_csDebuggerName;
    CCoresightDlg CoresightConfigDlg;
    BOOL       m_EnableCoreSight;
    BOOL       m_EnableCacheInvalidationNeeded;
	ProcessorConfig   *m_pProcCfg;
	CString	m_csInternalName;
	CString m_csDeviceType;	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _DEVICE_DLG_H_
