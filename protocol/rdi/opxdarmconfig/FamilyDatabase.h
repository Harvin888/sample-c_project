/****************************************************************************
       Module: FamilyDatabase.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CFamilyDatabase class
Date           Initials    Description
03-Aug-2007       JG         Initial
****************************************************************************/

#ifndef _FAMILY_DATABASE_H_
#define _FAMILY_DATABASE_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#import "msxml.dll"


/****************************************************************************
     Function: CFamilyDatabase
     Engineer: Jiju George T
  Description: Holds the ARM family database details
Date           Initials    Description
03-Aug-2007       JG         Initial
****************************************************************************/
class CFamilyDatabase
{
private:
    CFamilyDatabase();

public:
    virtual ~CFamilyDatabase();
    static CFamilyDatabase& GetInstance();
    static void Destroy();
    void GetListOfDevices( CStringArray &ListOfCores );
	void CheckArmArchitVer( CString* pulArmVer, CString CurrDeviceName );
	void CheckDevicesOnChain(TyJtagScanChain* tyJtagScanChain, CString CurrDeviceName );
	bool CheckCacheInvalidationNeeded(CString CurrDeviceName);
    bool CheckCoresightCompliant(CString CurrDeviceName);
	void GetCoresightConfigVal( CString* pcsDebugBaseAddr, 
								CString* pcsDebugApIndex, 
								CString* pcsMemApIndex, 
								CString* pcsJtagApIndex, 
								CString* pcsUseMemApMemAccess, 
								CString CurrDeviceName );
	void GetInternalName( 
		CString* csInternalName,
		CString CurrDeviceName );
	void GetSelectedNodePtr( const char* szSelectedNode, CString CurrDeviceName, MSXML::IXMLDOMNodePtr& NodePtr );

	bool CheckTICore(CString CurrDeviceName);
	void GetSecondaryPortNum( CString* csSecondaryPort, CString CurrDeviceName );
	void GetDapPcSupport( CString* csDapPcControl, CString CurrDeviceName );

private:
    MSXML::IXMLDOMDocument *m_pFamilyDocument;
    MSXML::IXMLDOMElement  *m_pFamilyRoot;
    static CFamilyDatabase  *m_pSingleInstance;

};

#endif // _FAMILY_DATABASE_H_
