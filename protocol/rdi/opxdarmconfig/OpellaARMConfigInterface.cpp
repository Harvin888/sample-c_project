/******************************************************************************
  Module	  : OpellaARMConfigInterface.cpp 
  Engineer    : Jiju George T
  Description : Contains the implemetation of external 
                interface functions.
  Date           Initials    Description
  08-Sep-2007    JG          Initial
  12-Aug-2010	 SJ			 Exported functions so that RDI dll can get 
							 the function pointer
******************************************************************************/

#include "stdafx.h"
#include "OpellaARMConfigInterface.h"

/****************************************************************************
  Function   : ShowOpellaConfigDialog()
  Engineer   : Jiju George T
  Input      : void
  Output     : MYLIB_DLLAPI - Same as return of CDialog::DoModal()
  Description: Non-MFC interface exposed so that non-MFC applications
               can also show the Opella configuration dialog without need to link
               to MFC libs.
  Date           Initials    Description
  08-Sep-2007       JG          Initial
  12-Aug-2010		SJ			Exported the function so that RDI dll can get
								the function pointer
****************************************************************************/
//MYLIB_DLLAPI int ShowOpellaConfigDialog()
extern "C" __declspec( dllexport ) int ShowOpellaConfigDialog()
{
    COpellaARMConfigDlg OpellaXDConfigDlg;
    int nResult = OpellaXDConfigDlg.DoModal();

    return nResult;
}