/****************************************************************************
       Module: OpellaARMConfig.cpp.
     Engineer: Jiju George T
  Description: Defines the initialization routines for the DLL.
Date           Initials    Description
24-Jul-2007       JG          Initial
18-Feb-2008	      SJ		  Added support for getting the DLL installation 
							  path
11-Aug-2010		  SJ		  Modified to avoid the dependency between opxdrdi.dll
							  and OpellaXDARMConfig.dll while building.
****************************************************************************/

#include "stdafx.h"
#include <afxdllx.h>
#include "OpellaARMConfigDlg.h"
#include "opellainterface.h"
#include "CSingleton.h"


AFX_EXTENSION_MODULE OpellaARMConfigDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
    // Remove this if you use lpReserved
    UNREFERENCED_PARAMETER(lpReserved);

    if (dwReason == DLL_PROCESS_ATTACH)
    {
        TRACE0("OPELLAXDARMCONFIG.DLL Initializing!\n");

        // Extension DLL one-time initialization
        if (!AfxInitExtensionModule(OpellaARMConfigDLL, hInstance))
            return 0;

        // Insert this DLL into the resource chain
        // NOTE: If this Extension DLL is being implicitly linked to by
        //  an MFC Regular DLL (such as an ActiveX Control)
        //  instead of an MFC application, then you will want to
        //  remove this line from DllMain and put it in a separate
        //  function exported from this Extension DLL.  The Regular DLL
        //  that uses this Extension DLL should then explicitly call that
        //  function to initialize this Extension DLL.  Otherwise,
        //  the CDynLinkLibrary object will not be attached to the
        //  Regular DLL's resource chain, and serious problems will
        //  result.

        //new CDynLinkLibrary(OpellaARMConfigDLL);

        // Initialize COM
        CoInitialize(NULL);
    }
    else if (dwReason == DLL_PROCESS_DETACH)
    {
        TRACE0("OPELLAXDARMCONFIG.DLL Terminating!\n");

		/* Delete the singleton class instance if already created	*/

		CSingleton* instance = CSingleton::getInstance();

		if(instance)
			instance->destroyInstance();

        // Terminate the library before destructors are called
        AfxTermExtensionModule(OpellaARMConfigDLL);

        // De-Initialize COM
        CoUninitialize();
    }
    return 1;   // ok
}

/****************************************************************************
Function	: InitOpellaARMConfigGUI
Engineer	: Suraj S
Input		: const char * pszARMRDIDriverpath - pointer to RDI DLL path
Output		: none
Description	: Store the path for RDI DLL
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
extern "C" __declspec( dllexport ) void InitOpellaARMConfigGUI(const char * pszARMRDIDriverpath)
{
   // Create a new CDynLinkLibrary for this app.
   new CDynLinkLibrary( OpellaARMConfigDLL );

   // Add other initialization here.
   COpellaARMConfigDlg::SetARMRDIDriverPath( pszARMRDIDriverpath );
}

/****************************************************************************
Function	: InitialiseRDIFnPtr
Engineer	: Suraj S
Input		: OpxdrdiFnptr *pOpxdrdiFnptr - pointer to functions in RDI DLL
Output		: void
Description	: Store the RDI DLL function pointers for future use
Date           Initials    Description
11-Aug-2010    SJ          Initial
****************************************************************************/
extern "C" __declspec( dllexport ) void InitialiseRDIFnPtr(OpxdrdiFnptr *pOpxdrdiFnptr)
{
	CSingleton* instance = CSingleton::getInstance();
	
	instance->m_PerformReadROMTable				= pOpxdrdiFnptr->PerformReadROMTable;
	instance->m_PerformRDIDiagnostics			= pOpxdrdiFnptr->PerformRDIDiagnostics;
	instance->m_PerformRDIDiagnosticsOnTarget	= pOpxdrdiFnptr->PerformRDIDiagnosticsOnTarget;
	instance->m_PerformTargetHardReset			= pOpxdrdiFnptr->PerformTargetHardReset;
	instance->m_GetFirmwareInfo					= pOpxdrdiFnptr->GetFirmwareInfo;
	instance->m_GetRDIDLLVersion				= pOpxdrdiFnptr->GetRDIDLLVersion;
	instance->m_GetUserRegisterSettings			= pOpxdrdiFnptr->GetUserRegisterSettings;
	instance->m_ML_CheckOpellaXDInstance		= pOpxdrdiFnptr->ML_CheckOpellaXDInstance;
}