/****************************************************************************
      Module : OpellaConfigPageBase.h
     Engineer: Jiju George T
  Description: interface for the COpellaConfigPageBase class
Date           Initials    Description
03-Aug-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#if !defined(AFX_OPELLACONFIGPAGEBASE_H__5E6DD5A9_AF73_42B5_AE50_5986BF81A9E2__INCLUDED_)
#define AFX_OPELLACONFIGPAGEBASE_H__5E6DD5A9_AF73_42B5_AE50_5986BF81A9E2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ResizablePage.h"

class COpellaConfigPageBase : public CResizablePage
{

// Dialog Data
    //{{AFX_DATA(COpellaConfigPageBase)
        // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(COpellaConfigPageBase)

    //}}AFX_VIRTUAL
    
// Implementation
protected:
    bool m_bUIDataUpdated;
    
    // Generated message map functions
    //{{AFX_MSG(COpellaConfigPageBase)
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

public:
    COpellaConfigPageBase();
    COpellaConfigPageBase(UINT nIDTemplate, UINT nIDCaption = 0);
    COpellaConfigPageBase(LPCTSTR lpszTemplateName, UINT nIDCaption = 0);

    // This function is called by the main config dialog for all pages in it
    // when user presses Apply/ OK button.
    // All the user configuration in the page should be saved to the passed
    // COpellaConfig object.
    // Derived classes must implement this function.
    virtual  bool ApplyChanges( COpellaConfig &OpellaConfig ) = 0;
    virtual  void InitializeUIData( COpellaConfig &OpellaConfig );
    virtual  void UpdateUI();

    virtual ~COpellaConfigPageBase();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPELLACONFIGPAGEBASE_H__5E6DD5A9_AF73_42B5_AE50_5986BF81A9E2__INCLUDED_)
