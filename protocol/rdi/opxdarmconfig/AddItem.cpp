/******************************************************************************
  Module      : AddItem.cpp
  Engineer    : Jeenus Chalattu Kunnath
  Description : Implementation of CAddItem class
  Date           Initials    Description
  29-May-2008     JCK         Initial
******************************************************************************/
#include "stdafx.h"
#include "AddItem.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/****************************************************************************
     Function: Constructor.
     Engineer: Jeenus Chalattu Knnath
     Input   : pParent - Parent window
     Output  : void
  Description: 
Date           Initials    Description
29-May-2008      JCK          Initial
****************************************************************************/
CAddItem::CAddItem(CWnd* pParent /*=NULL*/)
	: CDialog(CAddItem::IDD, pParent)
{
   m_bEditItem       = FALSE;
   strcpy(m_szName,"");
   m_ulAddress       = 0x0;
   m_ulSize          = 0x4;
   m_ulValue         = 0x0;
	//{{AFX_DATA_INIT(CAddItem)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/****************************************************************************
    Function : DoDataExchange()
     Engineer: Jeenus Chalattu Kunnath
     Input   : pDX  - Holds the exchange information
     Output  : void
  Description: Does the data exchange between controls and corresponding member variables
Date           Initials    Description
01-Aug-2007       JCK         Initial
****************************************************************************/
void CAddItem::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddItem)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddItem, CDialog)
	//{{AFX_MSG_MAP(CAddItem)
	ON_BN_CLICKED(IDC_HELP_TOPIC, OnHelpTopic)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddItem message handlers

/****************************************************************************
     Function: OnHelpTopic()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Help button.               
Date           Initials    Description
29-May-2008       JCK         Initial
****************************************************************************/
void CAddItem::OnHelpTopic() 
{
	// TODO: Add your control notification handler code here
	ShowHelpContents();
	
}

/****************************************************************************
     Function: OnOK()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses OK button.               
Date           Initials    Description
29-May-2008       JCK         Initial
****************************************************************************/
void CAddItem::OnOK() 
{
	// TODO: Add extra validation here
	 char szAddress[MAX_STRING_SIZE];
   char szSize[MAX_STRING_SIZE];
   char szValue[MAX_STRING_SIZE];

   (void)GetDlgItem(IDC_NAME)->GetWindowText(m_szName, MAX_STRING_SIZE);

   (void)GetDlgItem(IDC_ADDRESS)->GetWindowText(szAddress, MAX_STRING_SIZE);
   if (!GetLongValueFromHexString(szAddress, &m_ulAddress))
      {
     // DisplayError(IDERR_INVALID_HEX_VALUE,this);
      (void)GetDlgItem(IDC_ADDRESS)->SetFocus();
      return;
      }
   (void)GetDlgItem(IDC_BYTE_SIZE)->GetWindowText(szSize, MAX_STRING_SIZE);
   if (!GetLongValueFromHexString(szSize, &m_ulSize))
      {
     // DisplayError(IDERR_INVALID_HEX_VALUE,this);
      (void)GetDlgItem(IDC_BYTE_SIZE)->SetFocus();
      return;
      }

   if ((m_ulSize != 1) &&
       (m_ulSize != 2) &&
       (m_ulSize != 4))
      {
      MessageBox("Only sizes of 1, 2 or 4 are allowed","Opella-XD for ARM",MB_OK|MB_ICONWARNING);

      (void)GetDlgItem(IDC_BYTE_SIZE)->SetFocus();
      return;
      }

   (void)GetDlgItem(IDC_VALUE)->GetWindowText(szValue, MAX_STRING_SIZE);
   if (!GetLongValueFromHexString(szValue, &m_ulValue))
      {
     // DisplayError(IDERR_INVALID_HEX_VALUE,this);
      (void)GetDlgItem(IDC_VALUE)->SetFocus();
      return;
      }

   if (((m_ulSize == 1) && (m_ulValue > 0xFF)) ||
       ((m_ulSize == 2) && (m_ulValue > 0xFFFF)) ||
       ((m_ulSize == 4) && (m_ulValue > 0xFFFFFFFF)))
      {
      MessageBox("Specified value too large","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
      (void)GetDlgItem(IDC_VALUE)->SetFocus();
      return;
      }
	
	CDialog::OnOK();
}

/****************************************************************************
     Function: OnInitDialog()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : BOOL - controls the focus on control
  Description: Called when this page is shown for first time
               Does the initialization job. 
Date          Initials    Description
29-May-2008    JCK          Initial
****************************************************************************/
BOOL CAddItem::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
   char szTemp[MAX_STRING_SIZE];

   (void)CDialog::OnInitDialog();

   if (m_bEditItem)
      {
      SetWindowText("Edit Existing Register");
      }

   GetDlgItem(IDC_NAME)->SetWindowText(m_szName);

   sprintf(szTemp, "%lX", m_ulSize);
   GetDlgItem(IDC_BYTE_SIZE)->SetWindowText(szTemp);

   sprintf(szTemp, "%lX", m_ulAddress);
   GetDlgItem(IDC_ADDRESS)->SetWindowText(szTemp);

   sprintf(szTemp, "%lX", m_ulValue);
   GetDlgItem(IDC_VALUE)->SetWindowText(szTemp);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}




