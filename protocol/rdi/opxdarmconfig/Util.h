/****************************************************************************
       Module: Util.h
     Engineer: Jeenus Chalattu kunnath
  Description: This file contains the declaration of CAdvancedDlg class
Date           Initials    Description
24-Jul-2007      JG          Initial
27-Feb-2008      SPC         Added Support for Load/Save config options.
****************************************************************************/

#define MAX_STRING_SIZE     0xFF

BOOL GetLongValueFromHexString(char *pszNumericString, unsigned long *pulValue);
CString SharedLIBGetProfileString(LPCTSTR lpszSection, 
                                  LPCTSTR lpszEntry,
                                  LPCTSTR lpszDefault);

BOOL SharedLIBWriteProfileString(LPCTSTR lpszSection, 
                                 LPCTSTR lpszEntry,
                                 LPCTSTR lpszValue);

void ShowHelpContents(void);



