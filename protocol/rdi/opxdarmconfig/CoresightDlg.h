#if !defined(AFX_CORESIGHTDLG_H__20059F3F_E938_4C9B_8033_649C196891CF__INCLUDED_)
#define AFX_CORESIGHTDLG_H__20059F3F_E938_4C9B_8033_649C196891CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CoresightDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCoresightDlg dialog

class CCoresightDlg : public CDialog
{
// Construction
public:
	CCoresightDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoresightDlg)
	enum { IDD = IDD_CORESIGHT_CONFI_DLG };
	CButton	m_ReadRomTable;
	CEdit	m_RomTableControl;
	CString	m_csRomTable;
	CString	m_DbgBaseAddr;
	CString	m_DbgApIndex;
	CString	m_MemApIndex;
	CString	m_JtagApIndex;
	CString	m_RomTableBase;
	CString	m_UseMemApMemAccess;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoresightDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoresightDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnRomTableRead();
	afx_msg void OnChangeRomBaseAddr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
/* These are back up variables used to save the main variables value.
This is necessary because if user press Read ROM table value, then
main variables will be updated. But if user presses cancel, he expect 
    previous value to be retained. */
    CString	m_DbgBaseAddrBackup;
    CString	m_DbgApIndexBackup;
	CString	m_MemApIndexBackup;
	UINT	m_uiCore;
	char	m_pszCoreType[30];
	TyJtagScanChain *m_ptyJtagScanChain;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CORESIGHTDLG_H__20059F3F_E938_4C9B_8033_649C196891CF__INCLUDED_)
