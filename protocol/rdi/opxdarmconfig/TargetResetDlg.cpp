/******************************************************************************
  Module      : TargetResetDlg.cpp
  Engineer    : Jiju George T
  Description : This file contains the implementation of CTargetResetDlg class.
  Date           Initials    Description
  27-Jul-2007    JG         Initial
  27-Feb-2008    SPC        Added Support for Load/Save config options.
  23-Jun-2008    SJ         MIN_STARTUP_DELAY value modified to '0'      
******************************************************************************/
#include "stdafx.h"
#include "OpellaConfig.h"
#include "DeviceDlg.h"
#include "TargetResetDlg.h"
#include "FamilyDatabase.h"

#define MIN_STARTUP_DELAY 0
#define MAX_STARTUP_DELAY 10

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int DEFAULT_HARD_RESET_STARTUP_DELAY = 5;

/****************************************************************************
  Function   : Constructor
  Engineer   : Jiju George T
  Input      : OpellaConfig - Contains the configuration
  Output     : void
  Description: Creates an empty config object to which configurations can 
               be added and then saved.
  Date           Initials    Description
  2007-08-02       JG          Initial
  27-Feb-2008      SPC         Initializations are changed to another function.
****************************************************************************/
CTargetResetDlg::CTargetResetDlg( COpellaConfig &OpellaConfig, 
								 CDeviceDlg &DeviceDlg):
									COpellaConfigPageBase( CTargetResetDlg::IDD ),
									m_OpellaConfig( OpellaConfig ),
									m_DeviceDlg(DeviceDlg)
{
    //{{AFX_DATA_INIT(CTargetResetDlg)
    m_nTargetResetMode = -1;
    m_bSetDebugRequest = FALSE;
    m_bUsenTRST = FALSE;
    //}}AFX_DATA_INIT

    // To Remove Help button from parent property sheet
    m_psp.dwFlags &= (~PSP_HASHELP);

    InitializeUIData(m_OpellaConfig);
}

/****************************************************************************
  Function   : Destructor.
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Dectructs the Object
  Date           Initials    Description
  2007-08-02       JG          Initial
*****************************************************************************/
CTargetResetDlg::~CTargetResetDlg()
{
}

/****************************************************************************
  Function   : DoDataExchange()
  Engineer   : Jiju George T
  Input      : pDX  - Holds the exchange information
  Output     : void
  Description: Does the data exchange between controls and corresponding member variables.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::DoDataExchange(CDataExchange* pDX)
{
    CResizablePage::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CTargetResetDlg)
    DDX_Control(pDX, IDC_STARTUP_DELAY, m_StartupDelayCombo);
    DDX_Radio(pDX, IDC_NO_RESET, m_nTargetResetMode);
    DDX_Check(pDX, IDC_SET_DEBUG_REQUEST, m_bSetDebugRequest);
    DDX_Check(pDX, IDC_USE_NTRST, m_bUsenTRST);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTargetResetDlg, COpellaConfigPageBase)
    //{{AFX_MSG_MAP(CTargetResetDlg)
    ON_BN_CLICKED(IDC_NO_RESET, OnNoReset)
    ON_BN_CLICKED(IDC_RESET_AND_DELAY, OnResetAndDelay)
    ON_BN_CLICKED(IDC_RESET_AND_HALT, OnResetAndHalt)
    ON_BN_CLICKED(IDC_HOT_PLUG, OnHotPlug)
    ON_CBN_SELCHANGE(IDC_STARTUP_DELAY, OnSelchangeStartupDelay)
	ON_BN_CLICKED(IDC_CORE_ONLY, OnCoreOnly)
	ON_BN_CLICKED(IDC_CORE_AND_PERIPHERALS, OnCoreAndPeripherals)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/****************************************************************************
  Function   : OnInitDialog()
  Engineer   : Jiju George T
  Input      : void
  Output     : BOOL - controls the focus on control
  Description: Called when this page is shown for first time.
               Does the initialization job.
  Date           Initials    Description
  2007-08-02       JG          Initial
  27-Feb-2008      SPC         UI Updates are doing in another function and 
                               it will call on OnShowWindow function.
****************************************************************************/
BOOL CTargetResetDlg::OnInitDialog()
{
    CResizablePage::OnInitDialog();

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}


/****************************************************************************
  Function   : FillStartupDelayCombo()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description:  Fills the startup delay combo with possible values.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::FillStartupDelayCombo()
{
    m_StartupDelayCombo.ResetContent();

    for (int nDelay = MIN_STARTUP_DELAY; nDelay <= MAX_STARTUP_DELAY ; ++nDelay)
    {
        m_StartupDelayCombo.AddString( GetDelayString( nDelay ));
    }

    // Select user configured delay

    int nSelIndex =  m_StartupDelayCombo.FindStringExact( 0, GetDelayString( m_nHardResetDelay ));
//    int nSelIndex =  m_StartupDelayCombo.FindStringExact( 0, GetDelayString( DEFAULT_HARD_RESET_STARTUP_DELAY ));
    if( CB_ERR != nSelIndex )
    {
        m_StartupDelayCombo.SetCurSel( nSelIndex );
    }
}


/****************************************************************************
  Function   :GetDelayString()
  Engineer   : Jiju George T
  Input      : nDelay  - Delay to be converted
  Output     : CString - Converted delay string
  Description: Converts the given delay to string representation in proper display format.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
CString CTargetResetDlg::GetDelayString(int nDelay)
{
    CString csDelay;
    csDelay.Format( _T( "%d s" ), nDelay);
    return csDelay;
}

/****************************************************************************
  Function   :OnNoReset()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects No Hardware Reset mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::OnNoReset()
{
    UpdateData();
    EnableDisableControls();
}

/****************************************************************************
  Function   : OnResetAndDelay()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects Reset & Delay mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::OnResetAndDelay()
{
    UpdateData();
    EnableDisableControls();
}

/****************************************************************************
  Function   : OnResetAndHalt()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects Reset & Halt mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::OnResetAndHalt()
{
    UpdateData();
    EnableDisableControls();
}


/****************************************************************************
  Function   : OnHotPlug()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects Hot Plug mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::OnHotPlug()
{
    UpdateData();
    EnableDisableControls();
}


/****************************************************************************
  Function   : EnableDisableControls()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Enables / Disables the controls based on selected reset mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetResetDlg::EnableDisableControls()
{
    TargetResetMode eTargetResetMode = ( TargetResetMode )m_nTargetResetMode;

    switch( eTargetResetMode )
    {
        case NoHardReset:
        {
            // Disable Delay combo box
            m_StartupDelayCombo.EnableWindow( FALSE );
            break;
        }

        case HardResetAndDelay:
        {
            // Enable Delay Combo
            m_StartupDelayCombo.EnableWindow( TRUE );
            break;
        }

        case HardResetAndHaltAtRV:
        {
            // Disable Delay combo box
            m_StartupDelayCombo.EnableWindow( FALSE );
            break;
        }

/*        case HardResetAndGo:
        {
            // Disable Delay combo box
            m_StartupDelayCombo.EnableWindow( FALSE );
            break;
        }
*/
        case HotPlug:
        {
            // Disable Delay Combo
            m_StartupDelayCombo.EnableWindow( FALSE );
            break;
        }
		case CoreOnly:
		{
			m_StartupDelayCombo.EnableWindow( FALSE );
            break;
		}
		case CoreAndPeripherals:
		{
			m_StartupDelayCombo.EnableWindow( FALSE );
            break;
		}

        default:
        {
            break;
        }
    }

}

/****************************************************************************
  Function   : OnSelchangeStartupDelay()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user changes the startup delay.
  Date           Initials    Description
  2007-08-02       JG          Initial
  2008-06-23       SJ          Added value '0' to combo box
****************************************************************************/
void CTargetResetDlg::OnSelchangeStartupDelay()
{
    m_nHardResetDelay = m_StartupDelayCombo.GetCurSel();
}


/****************************************************************************
  Function   : ApplyChanges()
  Engineer   : Jiju George T
  Input      : OpellaConfig - Configuration object to save configurations
  Output     : bool - Return false if some required information is not
               filled by user (In this case notification about
               the error should be given to user). Else return true.
  Description: This function is called by the parent when user presses Apply.OK button.
               All the user configuration in the page should be saved to the passed
               COpellaConfig object.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
bool CTargetResetDlg::ApplyChanges( COpellaConfig &OpellaConfig )
{
    // Exchange data from controls to members
    if( IsWindow( m_hWnd ))
    {
        UpdateData();
    }

    // Save Target Reset Mode
    TargetResetMode eTargetResetMode = ( TargetResetMode )m_nTargetResetMode;
    OpellaConfig.SetTargetResetMode( eTargetResetMode );

    // Save Startup Delay if mode is Reset&Delay
    if( HardResetAndDelay == eTargetResetMode )
    {
        OpellaConfig.SetHardResetDelay( m_nHardResetDelay );
    }

    // Save Other Misc Settings
    OpellaConfig.UsenTRST( m_bUsenTRST );
    OpellaConfig.SetDBGRQLow( m_bSetDebugRequest );

    return true;
}

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This function will initialize GUI data variables using the 
               configuration object as argument. This function is called by
               Constructor of the class and base class while loading configuration.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CTargetResetDlg::InitializeUIData( COpellaConfig &OpellaConfig )
{
    // Get Target Reset Mode
    m_nTargetResetMode = OpellaConfig.GetTargetResetMode();
    
    // Get Hard Reset Delay
    m_nHardResetDelay = OpellaConfig.GetHardResetDelay();
    
    // Get other settings
    m_bUsenTRST = OpellaConfig.IsUsenTRST();
    m_bSetDebugRequest = OpellaConfig.IsSetDBGRQLow();
    
    // Exchange data to controls if there is a window exists. 
    // It necessary because we are calling UpdateData() in ApplyChanges function. 
    if( IsWindow( m_hWnd ))
    {
        UpdateData( FALSE );
    }

    //Calling base class Initializations. 
    COpellaConfigPageBase::InitializeUIData( OpellaConfig );
}

/****************************************************************************
     Function: UpdateUI()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This function will update user interface with member variable, 
               This function is called by the OnShowWindow member function of 
               the base class.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CTargetResetDlg::UpdateUI()
{
	CString tmpStr;
	CString csCurrArchVer;

    // Fill the combo boxes
    FillStartupDelayCombo();

	m_DeviceDlg.m_CoreListCombo.GetLBText(m_DeviceDlg.m_CoreListCombo.GetCurSel(), tmpStr);
	CFamilyDatabase::GetInstance().CheckArmArchitVer(&csCurrArchVer, tmpStr);
	if("ARMv7M" == csCurrArchVer)
	{
		((CButton*)GetDlgItem(IDC_RESET_AND_HALT))->EnableWindow(FALSE);
		((CButton*)GetDlgItem(IDC_CORE_ONLY))->EnableWindow(TRUE);
		((CButton*)GetDlgItem(IDC_CORE_AND_PERIPHERALS))->EnableWindow(TRUE);
	}
	else
	{
    
		UpdateData(TRUE);
		if((CoreOnly == m_nTargetResetMode) || (CoreAndPeripherals == m_nTargetResetMode))
		{
			m_nTargetResetMode = 0;
		}
		UpdateData(FALSE);

		((CButton*)GetDlgItem(IDC_RESET_AND_HALT))->EnableWindow(TRUE);
		((CButton*)GetDlgItem(IDC_CORE_ONLY))->EnableWindow(FALSE);
		((CButton*)GetDlgItem(IDC_CORE_AND_PERIPHERALS))->EnableWindow(FALSE);
	}

    
    // Exchange data in members to controls
    UpdateData(FALSE);
    
    EnableDisableControls();
}

void CTargetResetDlg::OnCoreOnly() 
{
	// TODO: Add your control notification handler code here
	UpdateData();
    EnableDisableControls();

	
}

void CTargetResetDlg::OnCoreAndPeripherals() 
{
	// TODO: Add your control notification handler code here
    UpdateData();
    EnableDisableControls();
}
