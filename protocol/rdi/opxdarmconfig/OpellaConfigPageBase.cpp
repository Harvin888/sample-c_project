/******************************************************************************
  Module      : OpellaConfigPageBase.cpp
  Engineer    : Jiju George T
  Description : Implementation of the COpellaConfigPageBase class.
  Date           Initials    Description
  27-Jul-2007       JG         Initial
  27-Feb-2008       SPC        Added Support for Load/Save config options.
******************************************************************************/

#include "stdafx.h"
#include "OpellaConfig.h"
#include "OpellaConfigPageBase.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//IMPLEMENT_DYNCREATE( COpellaConfigPageBase, CResizablePage )

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COpellaConfigPageBase::COpellaConfigPageBase()
{
    //{{AFX_DATA_INIT(COpellaConfigPageBase)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT

}

COpellaConfigPageBase::COpellaConfigPageBase( UINT nIDTemplate, UINT nIDCaption ) : 
    CResizablePage( nIDTemplate )
{

}

COpellaConfigPageBase::COpellaConfigPageBase( LPCTSTR lpszTemplateName, UINT nIDCaption ) :
    CResizablePage( lpszTemplateName )
{
}


COpellaConfigPageBase::~COpellaConfigPageBase()
{

}


BEGIN_MESSAGE_MAP(COpellaConfigPageBase, CResizablePage)
//{{AFX_MSG_MAP(COpellaConfigPageBase)
    ON_WM_SHOWWINDOW()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/****************************************************************************
     Function: OnShowWindow()
     Engineer: Suresh P.C
     Input   : bShow: Specifies whether a window is being shown
               nStatus: Specifies the status of the window being shown.
     Output  : None
  Description: This is a window handler function for Show Window. 
               We are calling UpdateUI virtual function from here. 
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void COpellaConfigPageBase::OnShowWindow(BOOL bShow, UINT nStatus) 
{
    CResizablePage::OnShowWindow(bShow, nStatus);
    
    //Check the data's for UI is updated and if updated, repopulates all 
    //data's in the UI
    //if (m_bUIDataUpdated)
    {
        UpdateUI();
        m_bUIDataUpdated = FALSE;
    }

}

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This is the base virtual function of InitializeUIData. This is
               used to set the status of UI data is updated.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void COpellaConfigPageBase::InitializeUIData( COpellaConfig &OpellaConfig )
{
    m_bUIDataUpdated = TRUE;
}

/****************************************************************************
     Function: UpdateUI()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This is the default base virtual function of UpdateUI. 
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void COpellaConfigPageBase::UpdateUI()
{
    //Nothing TODO now.Its for default calling only.
}