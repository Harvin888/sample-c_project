/******************************************************************************
  Module      : Util.cpp
  Engineer    : Jeenus Chalattu Kunnath
  Description : 
  Date           Initials    Description
  29-May-2008     JCK         Initial
******************************************************************************/

#include "stdafx.h"
#include "Util.h"
#include "OpellaARMConfigDlg.h"


/****************************************************************************
     Function: GetLongValueFromHexString()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: 
Date           Initials    Description
29-May-2008    JCK           Initial
****************************************************************************/

BOOL GetLongValueFromHexString(char *pszNumericString, unsigned long *pulValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%x",&ulCalculatedValue) == 0)
      return FALSE;

   *pulValue = ulCalculatedValue;

   return TRUE;
}

/****************************************************************************
     Function: SharedLIBGetProfileString()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: 
Date           Initials    Description
29-May-2008    JCK           Initial
****************************************************************************/
CString SharedLIBGetProfileString(LPCTSTR lpszSection, 
                                  LPCTSTR lpszEntry,
                                  LPCTSTR lpszDefault)
{
   return AfxGetApp()->GetProfileString(lpszSection,lpszEntry,lpszDefault);
}

/****************************************************************************
     Function: SharedLIBWriteProfileString()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description:
Date           Initials    Description
29-May-2008    JCK           Initial
****************************************************************************/
BOOL SharedLIBWriteProfileString(LPCTSTR lpszSection, 
                                 LPCTSTR lpszEntry,
                                 LPCTSTR lpszValue)
{
   return AfxGetApp()->WriteProfileString(lpszSection,lpszEntry,lpszValue);   
}    


/****************************************************************************
     Function: ShowHelpContents()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: called when help button is clcked from user register settings
Date           Initials    Description
29-May-2008    JCK           Initial
****************************************************************************/
void ShowHelpContents(void)
{
  HINSTANCE  retVal;
  CString    dllPath;
  char       actualPath[260]; 
  char       szErrorMessage[MAX_STRING_SIZE];

  COpellaARMConfigDlg::GetARMRDIDriverPath(dllPath);
  const char* fulldllPath = (LPCTSTR)dllPath;
  strcpy(actualPath,fulldllPath);

  strcat(actualPath,"\\OpellaXDARMUserManual.pdf");
  retVal = ShellExecute(NULL, "open", actualPath, NULL, NULL, SW_SHOWNORMAL);

  if ( (int)retVal <= 32)
    {
    switch ((int)retVal)
       {
       case SE_ERR_NOASSOC: // no associated app. ie acroread
           AfxGetMainWnd()->MessageBox("Please ensure that Acrobat Reader is installed","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
          break;
       case SE_ERR_FNF:
			 strupr(actualPath);
          sprintf(szErrorMessage, "Error opening helpfile: %s", actualPath);
          AfxGetMainWnd()->MessageBox(szErrorMessage,"Opella-XD for ARM",MB_OK|MB_ICONWARNING);
          break;
       default:
         sprintf(szErrorMessage, "Unable to open Help file: %s", actualPath);
          AfxGetMainWnd()->MessageBox(szErrorMessage,"Opella-XD for ARM",MB_OK|MB_ICONWARNING);
          break;
       }

    }

}



