// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__B41F91E2_915E_46D7_810F_B7E129664988__INCLUDED_)
#define AFX_STDAFX_H__B41F91E2_915E_46D7_810F_B7E129664988__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN        // Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT


#include <afxdtctl.h>       // MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>         // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "resource.h"

#include <afxmt.h>

#define _MYLIB_DLLAPI_
#define _MYLIB_NOAUTOLIB_

#include "../rdi/GUIDefs.h"
#include "../rdi/toolconf.h"
#include "../rdi/rdi.h"

//#include "/pathfndr/global/stypes.h"
//#include "/pathfndr/global/headers/cplextyp/global/globcplx.h"
//#include "/Pathfndr/GLOBAL/Headers/simple/global/simple.h"
//#include "/Pathfndr/PROTOCOL/Common/Opella/DrvLayer.h"
//#include "/PATHFNDR/TXRXCMD/RDI/toolconf.h"
//#include "/Pathfndr/TXRXCMD/RDI/rdicmd.h"

#include "OpellaARMConfigDlg.h"

#include "HexUtils.h"


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__B41F91E2_915E_46D7_810F_B7E129664988__INCLUDED_)
