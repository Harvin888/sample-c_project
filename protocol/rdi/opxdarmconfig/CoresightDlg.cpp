// CoresightDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoresightDlg.h"
#include "DriverMgr.h"
#include "../opxdarmdll/midlayerarm.h"
#include "FamilyDatabase.h"
#include "opellainterface.h"
#include "CSingleton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//Error codes
#define DIAG_ERROR_NO_ERROR					0x0000
#define DIAG_ERROR_USB_DRIVER				0x0001
#define DIAG_ERROR_INVALID_SERIAL_NUMBER	0x0003
#define DIAG_ERROR_TAPID1_FAILED			0x0004
#define DIAG_ERROR_TAPID2_FAILED			0x0005
#define DIAG_ERROR_TAPID_FAILED				0x0006
#define DIAG_ERROR_CPREG_FAILED				0x0007
#define DIAG_ERROR_TAP_LB_FAILED			0x0008
#define DIAG_ERROR_WRONG_CORE				0x0009
#define DIAG_ERROR_TPA_NOT_CONNECTED		0x000A
#define DIAG_ERROR_INVALID_CONFIG			0x000B
#define DIAG_ERROR_CORE_MISS_MATCH			0x000C
#define DIAG_ERROR_READ_ROM_FAILED			0x000D
#define DIAG_ERROR_ARM_NO_DEBUG_ENTRY_AP	0x000E
#define DIAG_ERROR_ARM_NOT_VALID_ROM_ADDR	0x000F
#define DIAG_ERROR_OPELLA_FAILURE           360


/* Major types description: Ref Coresight Architecture Specification. Version 1.0 Page number 48 */
TySubTypeStruct tyMiscellaneousSubTypeStruct[] = {
	{
		OtherUndefined, "Other, Undefined"
	},
	{
		Validationcomponent, "Validation component"
	}
};

TySubTypeStruct tyTraceSinkSubTypeStrct[] = {
	{
		OtherTraceSink, "Other"
	},
	{
		TracePortForExampleTPIU, "Trace port, for example TPIU"
	},
	{
		BufferForExampleETB, "Buffer, for example ETB"
	}
};

TySubTypeStruct tyTraceLinkSubTypeStrct[] = {
	{
		OtherTraceLink, "Other"
	},
	{
		TraceFunnelRouter, "Trace funnel, Router"
	},
	{
		Filter, "Filter"
	},
	{
		FIFOLargeBuffer, "FIFO, Large Buffer"
	}
};

TySubTypeStruct tyTraceSourceSubTypeStrct[] = {
	{
		OtherTraceSource, "Other"			
	},		
	{				
		AssociateProcessorCore, "Associated with a processor core"
	},
	{
		AssociateDSP, "Associated with a DSP"
	}, 
	{
		AssociateDataEngineCoprocessor, "Associated with a Data Engine or Coprocessor"
	},
	{
		AssociateBusStimulusBusActivity, "Associated with a Bus, stimulus derived from bus activity"
	}
};

TySubTypeStruct tyDebugControlSubTypeStrct[] = {
	{
		OtherDebugControl, "Other"			
	},				
	{				
		TriggerMatrixECT, "Trigger Matrix, for example ECT"
	},
	{
		DebugAuthenticationModule, "Debug Authentication Module"
	}
};

TySubTypeStruct tyDebugLogicSubTypeStrct[] = {
	{
		OtherDebugLogic, "Other Type"		
	},						
	{									
		ProcessorCore, "Processor core"
	},
	{
		DSP, "DSP"
	},
	{
		DataEngineCoprocessor, "Data Engine or Coprocessor"
	}
};

TyDeviceType tyDeviceType[] = {
	{
		Miscellaneous , "Miscellaneous" ,	tyMiscellaneousSubTypeStruct
	},
	{
		TraceSink ,		"Trace Sink   ",	tyTraceSinkSubTypeStrct
	},
	{
		TraceLink ,		"Trace Link   ",	tyTraceLinkSubTypeStrct
	},
	{
		TraceSource,	"Trace Source ",	tyTraceSourceSubTypeStrct
	},
	{
		DebugControl,	"Debug Control",	tyDebugControlSubTypeStrct
	},
	{
		DebugLogic ,	"Debug Logic  ",	tyDebugLogicSubTypeStrct
	}
};



/////////////////////////////////////////////////////////////////////////////
// CCoresightDlg dialog


CCoresightDlg::CCoresightDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoresightDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoresightDlg)
	m_RomTableBase = _T("");
	//}}AFX_DATA_INIT
}


void CCoresightDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoresightDlg)
	DDX_Control(pDX, IDC_ROM_TABLE_READ, m_ReadRomTable);
	DDX_Control(pDX, IDC_ROM_TABLE_EDIT, m_RomTableControl);
	DDX_Text(pDX, IDC_ROM_TABLE_EDIT, m_csRomTable);
	DDX_Text(pDX, IDC_CORESIGHT_ADDRESS, m_DbgBaseAddr);
	DDX_Text(pDX, IDC_DBG_AP_IDX, m_DbgApIndex);
	DDX_Text(pDX, IDC_MEM_AP_IDX, m_MemApIndex);
	DDX_Text(pDX, IDC_JTAG_AP_IDX, m_JtagApIndex);
	DDX_Text(pDX, IDC_ROM_BASE_ADDR, m_RomTableBase);	
	
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoresightDlg, CDialog)
	//{{AFX_MSG_MAP(CCoresightDlg)
	ON_BN_CLICKED(IDC_ROM_TABLE_READ, OnRomTableRead)
	ON_EN_CHANGE(IDC_ROM_BASE_ADDR, OnChangeRomBaseAddr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoresightDlg message handlers

BOOL CCoresightDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_csRomTable = _T("");

	UpdateData(FALSE);

	if (strlen(m_RomTableBase))
	{
		m_ReadRomTable.EnableWindow(TRUE);
	} 
	else
	{
		m_ReadRomTable.EnableWindow(FALSE);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoresightDlg::OnOK() 
{
	UpdateData(TRUE);
    
    m_DbgBaseAddrBackup    = m_DbgBaseAddr;
    m_DbgApIndexBackup     = m_DbgApIndex;
    m_MemApIndexBackup	   = m_MemApIndex;

	CDriverMgr::GetInstance().SetCoresightParam(m_DbgApIndex, 
												m_MemApIndex, 
												m_JtagApIndex, 
												m_DbgBaseAddr); /* Now we are not using the Jtag Ap index */

	CDialog::OnOK();
}

void CCoresightDlg::OnCancel() 
{
    m_DbgBaseAddr = m_DbgBaseAddrBackup;    
    m_DbgApIndex  = m_DbgApIndexBackup;    
    m_MemApIndex  = m_MemApIndexBackup;
	
    CDialog::OnCancel();
}



void CCoresightDlg::OnRomTableRead() 
{
    TyError ErrRet = NO_ERROR;
	
	char *pszOpellaXDSerialNo = new char[MAX_SERIAL_NUMBER_LEN];

	TyRDIOnTargetDiagInfo tyRDIOnTargetDiagInfo;
	TyCoresightComp tyCoresightComp;
	CString szInternalName;
	CString cSelectedDevice;

	CSingleton* instance = CSingleton::getInstance();

	CDriverMgr::GetInstance().GetOpellaXDSerialNumber( pszOpellaXDSerialNo );

    UpdateData(TRUE);          
    m_csRomTable += "--Checking--";
    UpdateData(FALSE);    
	
	tyRDIOnTargetDiagInfo.ulDbgCoreNumber = m_uiCore;	
	tyRDIOnTargetDiagInfo.ptyJtagScanChain = m_ptyJtagScanChain;
    tyRDIOnTargetDiagInfo.tyJTAGClockFreqMode = Fixed;

	cSelectedDevice = m_pszCoreType;

	CFamilyDatabase::GetInstance().GetInternalName( &szInternalName, cSelectedDevice );
	
	strcpy(tyRDIOnTargetDiagInfo.pszCoreType, LPCTSTR (szInternalName));

	tyRDIOnTargetDiagInfo.bCoresightCompliant = TRUE;

	/* Check that the device selected is TI complaint */
	bool bTiCore = CFamilyDatabase::GetInstance().CheckTICore( cSelectedDevice );

	tyRDIOnTargetDiagInfo.bTICore = bTiCore;
	if (TRUE == bTiCore)
	{
		CString csSubPortNum;
		unsigned long ulSubPortNum;
		boolean  bValidData;

		CFamilyDatabase::GetInstance().GetSecondaryPortNum(&csSubPortNum, cSelectedDevice);
		bValidData = CHexUtils::GetValueFromHexString( 	csSubPortNum,
														ulSubPortNum );
		
		if( bValidData )
		{
			tyRDIOnTargetDiagInfo.ulTISubPortNum = ulSubPortNum;
		}

		CString csDapPcSupport;
		unsigned long ulDapPcSupport;		
		CFamilyDatabase::GetInstance().GetDapPcSupport( &csDapPcSupport, cSelectedDevice);
		bValidData = CHexUtils::GetValueFromHexString( 	csDapPcSupport,
														ulDapPcSupport );
		
		if( bValidData )
		{
			tyRDIOnTargetDiagInfo.bDapPcSupport = ulDapPcSupport;
		}
	}
	
	strcpy(tyRDIOnTargetDiagInfo.pszCoreType, LPCTSTR (szInternalName));

	bool bValidData = true;
	unsigned long ulDbgBaseAddr;
	bValidData = CHexUtils::GetValueFromHexString( 	m_RomTableBase,
													ulDbgBaseAddr );

	if( bValidData )
	{
		tyRDIOnTargetDiagInfo.ulCoresightBaseAddr = ulDbgBaseAddr;
	}
	else
	{
		MessageBox("Please enter a valid Debug Base address.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
		return;
    }	

	unsigned long ulDbgApIndex;
	bValidData = CHexUtils::GetValueFromHexString( 	m_DbgApIndex,
													ulDbgApIndex );

	if( bValidData )
	{
		tyRDIOnTargetDiagInfo.ulDbgApIndex = ulDbgApIndex;
	}
	else
	{
		MessageBox("Please enter a valid Debug AP Index.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
		return;
    }
	
	unsigned long ulMemApIndex;
	bValidData = CHexUtils::GetValueFromHexString( 	m_MemApIndex,
													ulMemApIndex );

	if( bValidData )
	{
		tyRDIOnTargetDiagInfo.ulMemApIndex		  = ulMemApIndex;
	}
	else
	{
		MessageBox("Please enter a valid Memory AP Index.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
		return;
    }	

	tyRDIOnTargetDiagInfo.ulFreqVal = 1000;

    int nPos = m_RomTableControl.GetLineCount();
    m_RomTableControl.LineScroll( nPos );
    nPos = m_RomTableControl.GetLineCount();
    m_RomTableControl.LineScroll( nPos );

	if (*pszOpellaXDSerialNo != NULL)
	{
//		ErrRet = PerformReadROMTable(pszOpellaXDSerialNo, m_uiCore, &tyRDIOnTargetDiagInfo, &tyCoresightComp);
		ErrRet = instance->m_PerformReadROMTable(pszOpellaXDSerialNo, m_uiCore, &tyRDIOnTargetDiagInfo, &tyCoresightComp);
		CString csMesssage;		
		
		if (tyRDIOnTargetDiagInfo.szTgtVoltage == 0)
		{
			m_csRomTable.Format( "Target not detected. Ensure target is powered and properly connected.\r\n");
		}
		else
		{
			if (ErrRet != NO_ERROR)
			{
				if ( ErrRet == DIAG_ERROR_TPA_NOT_CONNECTED )
				{
					m_csRomTable.Format( "Opella-XD TPA not connected.\r\n");
				}
				else
				{
					csMesssage.Format("Identification Register = 0x%08X", tyCoresightComp.ulIdrRegValue);
					m_csRomTable = csMesssage;
					
					csMesssage.Format("\r\nDebug Base Address Register = 0x%08X", tyCoresightComp.ulDbgBaseRegValue);			
					m_csRomTable += csMesssage;

					if (DIAG_ERROR_ARM_NO_DEBUG_ENTRY_AP == ErrRet)
					{
						m_csRomTable += "\r\n\r\n   No ROM Table Entries Present\r\n";
					}
					else if (DIAG_ERROR_ARM_NOT_VALID_ROM_ADDR == ErrRet)
					{
						m_csRomTable += "\r\n\r\n   Not a valid ROM Table base address\r\n";
					}			
				}
				
			}
			else
			{			
				csMesssage.Format("%s", "\r\n----------------------------------------------------------------------------------------------------------------");
				m_csRomTable += csMesssage;
				csMesssage.Format("%-10s\t%s%s", "\r\nBase Address","|"," Component\r\n");
				m_csRomTable += csMesssage;
				csMesssage.Format("%-s", "----------------------------------------------------------------------------------------------------------------\r\n");
				m_csRomTable += csMesssage;

				unsigned long ulNumEntries = tyCoresightComp.ulNumEntries;
				unsigned long ulEntryIndex;
				unsigned long i;
				TyMajorType tyMajorType;
				TySubType tySubType;
				unsigned long ulMaxMajorTypes = sizeof(tyDeviceType);
				unsigned long ulMaxSubTypes   = MAX_NUM_SUB_TYPES;

				for (ulEntryIndex = 0; ulEntryIndex < ulNumEntries; ulEntryIndex++ )
				{
					tyMajorType = (TyMajorType) (tyCoresightComp.tyRomTableEntry[ulEntryIndex].ulCompType & MAJOR_TYPE);
					tySubType   = (TySubType) ((tyCoresightComp.tyRomTableEntry[ulEntryIndex].ulCompType & SUB_TYPE) >> 4);

					csMesssage.Format("0x%08X\t%s  ",tyCoresightComp.tyRomTableEntry[ulEntryIndex].ulCompBaseAddr,"|");
					m_csRomTable += csMesssage;


					for(i = 0; i < ulMaxMajorTypes; i++)
					{
						if (tyMajorType == tyDeviceType[i].tyMajorType)					
						{
							csMesssage.Format("%-14s\t : ", tyDeviceType[i].szMajorTypeDesr);
							m_csRomTable += csMesssage;
							for (unsigned long j = 0; j < ulMaxSubTypes; j++)
							{
								if (tyDeviceType[i].ptySubTypeStruct[j].tySubType == tySubType)
								{
									csMesssage.Format("%s", tyDeviceType[i].ptySubTypeStruct[j].szSubTypeDesr);	
									m_csRomTable += csMesssage;
									break;
								}
							}
							
							break;
						}
					}
					
					m_csRomTable += "\r\n";
				}
				
			}
		}
		
	}
	
    else
    {
        CString csErrorMessage1;
        CString csErrorMessage2;
        csErrorMessage1.Format("RDI driver not responding, cannot continue." );
        csErrorMessage2.Format("Check Opella-XD is properly connected.\r\n");
        m_csRomTable.Format("%s\r\n%s\r\n",csErrorMessage1,csErrorMessage2);
		
    }	

	UpdateData(FALSE);

	if(NULL != pszOpellaXDSerialNo)
	{
		delete pszOpellaXDSerialNo;
		pszOpellaXDSerialNo = NULL;
	}

}

void CCoresightDlg::OnChangeRomBaseAddr() 
{
	UpdateData(TRUE);

	if (strlen(m_RomTableBase))
	{
		m_ReadRomTable.EnableWindow(TRUE);		
	} 
	else
	{
		m_ReadRomTable.EnableWindow(FALSE);		
	}
	
}
