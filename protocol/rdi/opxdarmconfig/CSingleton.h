/****************************************************************************
     Module  : CSingleton.h  This file contains the declaration of CSingleton class.
     Engineer: Suraj S
  Description: This file contains the declaration of CSingleton class.
Date           Initials    Description
12-Aug-2010	     SJ		    Initial
****************************************************************************/
#ifndef __CSINGLETON__
#define __CSINGLETON__

#include "opellainterface.h"

class CSingleton {
public:
    static CSingleton* getInstance();
    void destroyInstance();
    void doSomething();

public:
	PerformReadROMTable_t			m_PerformReadROMTable;
	PerformRDIDiagnostics_t			m_PerformRDIDiagnostics;
	PerformRDIDiagnosticsOnTarget_t	m_PerformRDIDiagnosticsOnTarget;
	PerformTargetHardReset_t		m_PerformTargetHardReset;
	ML_CheckOpellaXDInstance_t		m_ML_CheckOpellaXDInstance;
	GetRDIDLLVersion_t				m_GetRDIDLLVersion;
	GetFirmwareInfo_t				m_GetFirmwareInfo;
	GetUserRegisterSettings_t		m_GetUserRegisterSettings;

private:
    CSingleton() {}
    ~CSingleton(){}
    static CSingleton* m_pInstance;
};
#endif