/****************************************************************************
     Module  : CSingleton.cpp  This file contains the implementation of CSingleton class.
     Engineer: Suraj S
  Description: This file contains the implementation of CSingleton class.
Date           Initials    Description
12-Aug-2010	     SJ		    Initial
****************************************************************************/

#include "StdAfx.h"
#include "CSingleton.h"

CSingleton* CSingleton::m_pInstance = NULL;

/****************************************************************************
     Function: getInstance()
     Engineer: Suraj S
  Description: Instantiate the class object and return its pointer
Date           Initials    Description
12-Aug-2010	     SJ		    Initial
****************************************************************************/
CSingleton* CSingleton::getInstance() 
{
     if(NULL == m_pInstance ) 
	 {
            m_pInstance = new CSingleton();
     }
     return m_pInstance;
}

/****************************************************************************
     Function: destroyInstance()
     Engineer: Suraj S
  Description: Destroy the instance of the singleton class previously created
Date           Initials    Description
12-Aug-2010	     SJ		    Initial
****************************************************************************/
void CSingleton::destroyInstance() 
{
     delete m_pInstance;
     m_pInstance = NULL;
}

/****************************************************************************
     Function: doSomething()
     Engineer: Suraj S
  Description: 
Date           Initials    Description
12-Aug-2010	     SJ		    Initial
****************************************************************************/
void CSingleton::doSomething() 
{
	//Do what you need here     
}

