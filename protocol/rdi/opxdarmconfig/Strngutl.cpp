/****************************************************************************
       Module: Strngutl.Cpp
     Engineer: Martin Hannon
  Description: Implementation file for string utility functions 
Version     Date           Initials    Description
0.9.0       20-Oct-1998    MOH         Initial
****************************************************************************/

#include "\PATHFNDR\WINMFC\stdafx.h"
#include "\PATHFNDR\TXRXCMD\gdi\gdi.h"

extern "C"
{
#include "\pathfndr\smrtcard\modeutil.h"
}

#ifdef _DEBUG
#define new DEBUG_NEW
//#undef THIS_FILE
//static char THIS_FILE[] = __FILE__;
#endif

#if CTS51 || CTS11 || SBE66K || ULTRA51 || SUPRA12
#define NIBBLE_SIZE   4          /* determines hyphen location in binary strings */
#else
#define NIBBLE_SIZE   8          /* determines hyphen location in binary strings */
#endif
static unsigned short NormaliseEndianShort(unsigned char * pucArray,bool bBigEndian);
static unsigned long NormaliseEndianLong(unsigned char * pucArray,bool bBigEndian);
static unsigned long NormaliseEndianTripple(unsigned char * pucArray,bool bBigEndian);

/****************************************************************************
     Function: GetStringFromAddressValue
     Engineer: Kevin Aherne
        Input: 
       Output: None
  Description: 
Version        Date           Initials    Description
0.9.0-PFPU51   01-FEB-2000    KA         Initial
****************************************************************************/
void GetStringFromAddressValue(int   DisplayType,
                               char  *pszNewAddressString,
                               ulong ulAddress,
                               bool  bZeroPack)
{
   GetStringFromNumericValue(sizeof(adtyp),
                             FALSE,
                             DisplayType,
                             pszNewAddressString,
                             (ubyte *)&ulAddress,
                             bZeroPack);
}
/****************************************************************************
     Function: GetAddressHexString
     Engineer: Patrick Noonan
        Input: ulong    ulAddress,
               char     *pszAddress,
               ulong    ulMaxAddress
       Output: None
  Description: For use with the address column in Pathfinder windows, addresses
               always in hex, using the minumum amount of zero padding necessary
Version        Date           Initials    Description
2.0.1-PFPU51   05-NOV-2001    PN          Initial
****************************************************************************/
TyError GetAddressHexString(ulong ulAddress,char *pszAddress,ulong ulMaxAddress)
{
   uint16   uiLoop        = 0x0;
   uint16   uiHexDigits   = 8;
   char     szFormat[MAX_STRING_SIZE];
   TyError  ErrRet = NO_ERROR;

   if(ulAddress > ulMaxAddress)
      {
      ASSERT(ulAddress <= ulMaxAddress);
      ErrRet = NUMBER_TOO_LARGE;
      }
   
   ulong  ulMask = 0x1UL << ((sizeof(ulong) * 0x8)- 0x1);
   
   ulMaxAddress |= 0x1; // Ensure that we will terminate sometime!!!
   
   uiLoop = 0x0;
   while (!(ulMask & (ulMaxAddress <<uiLoop)))
      uiLoop++;
   
   uiHexDigits -= (uiLoop /0x4); 
   sprintf(szFormat, "%%0%dlX", uiHexDigits);
   sprintf(pszAddress, szFormat, ulAddress);

   return ErrRet;
}


/****************************************************************************
     Function: GetStringFromNumericValue
     Engineer: Kevin Aherne
        Input: 
       Output: None
  Description: 
Version        Date           Initials    Description
0.9.0-PFPU51   01-FEB-2000    KA         Initial
****************************************************************************/
void GetStringFromNumericValue(int   SizeOfNumericType,
                               bool  bFloatValue,
                               int   DisplayType,
                               char  *pszNewValueString,
                               ubyte *pubValue,
                               bool  bZeroPack,
                               bool  bDisplayBase)
{
   int FillType;

   if (bFloatValue)
      FillType = FILL_DOUBLE;
   else
      {
      switch (SizeOfNumericType)
         {
         case 1:
            FillType = FILL_CHAR;
            break;
         case 2:
            FillType = FILL_SHORT;
            break;
         case 4:
         default:
            FillType = FILL_LONG;
         }
      }

   switch (FillType)
      {
      case FILL_DOUBLE:
         GetStringFromNumericDoubleValue(DisplayType,
                                       pszNewValueString,
                                       *(double *)pubValue,
                                       bZeroPack,
                                       bDisplayBase);
         break;
      case FILL_FLOAT:
         GetStringFromNumericFloatValue(DisplayType,
                                        pszNewValueString,
                                        *(float *)pubValue,
                                        bZeroPack,
                                        bDisplayBase);
         break;
      case FILL_LONG:
         GetStringFromNumericLongValue(DisplayType,
                                       pszNewValueString,
                                       *(unsigned long *)pubValue,
                                       bZeroPack,
                                       bDisplayBase);
         break;
      case FILL_INT:
         GetStringFromNumericIntValue(DisplayType,
                                      pszNewValueString,
                                      *(unsigned int *)pubValue,
                                      bZeroPack,
                                      bDisplayBase);
         break;
      case FILL_SHORT :
         GetStringFromNumericShortValue(DisplayType,
                                       pszNewValueString,
                                       *(unsigned short *)pubValue,
                                       bZeroPack,
                                       bDisplayBase);
          break;
      case FILL_CHAR :
         GetStringFromNumericCharValue(DisplayType,
                                       pszNewValueString,
                                       *(unsigned char *)pubValue,
                                       bZeroPack,
                                       bDisplayBase);
         break;
      default:
         ASSERT_NOW();
         break;
      }
}
     
/****************************************************************************
     Function: GetTimeString
     Engineer: Martin Hannon
        Input: CurrentTimer : Numeric representation of  time value.
       Output: TimeString   : String  representation of  time value.
  Description: Converts from number microseconds to string showing
               number of hours, minutes,seconds, milliseconds, microseconds.

               ie 96125678544 ==>> '24:59:59.999.999'

               Values are only displayed in the following formats:

               Hours   : Minutes   : Seconds
               Minutes : Seconds   : MilliSecs
               Seconds : MilliSecs : MicroSecs
      
Version     Date           Initials    Description
1.0.0       07-MAR-1997    MOH         initial 
1.0.0-D     01-NOV-1997    MOH         Added negative display.
1.0.0-H     04-MAR-1998    MOH         Aligned decimal places.
****************************************************************************/
void GetTimeString(double CurrentTimer, char *TimeString)
{
   double dTime;

   dTime = CurrentTimer / 10000000;

   if (dTime > 1)
      {
      sprintf(TimeString,"%16.7fs",dTime);
      }
   else
      {
      // convert to miliseconds
      dTime = dTime * 1000;
      if (dTime > 1  )
         sprintf(TimeString,"%16.7fms",dTime);
      else
         {
         // convert to microseconds
         dTime = dTime * 1000;
         sprintf(TimeString,"%16.7fus",dTime);
         }
      }
}
void GetStringFromNumericDoubleValue(int     DisplayType,
                                     char    *pszNewValueString,
                                     double  dNewValue,
                                     bool    bZeroPack,
                                     bool    bDisplayBase)
{
   NOREF(bZeroPack);
   NOREF(bDisplayBase);
   if (DisplayType == DISPLAY_E)
      sprintf(pszNewValueString, "%E", dNewValue);
   else if (DisplayType == DISPLAY_F)
      sprintf(pszNewValueString, "%f", dNewValue);
   else if (DisplayType == DISPLAY_G)
      sprintf(pszNewValueString, "%G", dNewValue);
   else
      ASSERT_NOW();
}

void GetStringFromNumericFloatValue(int   DisplayType,
                                    char  *pszNewValueString,
                                    float fNewValue,
                                    bool  bZeroPack,
                                    bool  bDisplayBase)
{
   NOREF(bZeroPack);
   NOREF(bDisplayBase);
   if (DisplayType == DISPLAY_E)
      sprintf(pszNewValueString, "%E", fNewValue);
   else if (DisplayType == DISPLAY_F)
      sprintf(pszNewValueString, "%f", fNewValue);
   else if (DisplayType == DISPLAY_G)
      sprintf(pszNewValueString, "%G", fNewValue);
   else
      ASSERT_NOW();
}
void GetStringFromNumericLongValue(int            DisplayType,
                                   char           *pszNewValueString,
                                   unsigned long  ulNewValue,
                                   bool           bZeroPack,
                                   bool           bDisplayBase)
{
   if (bZeroPack)
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%08lX", ulNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%08lX", ulNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%010lu", ulNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%010ld", (signed long)ulNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%08lo", ulNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 32; i++)
            {
            if ((ulNewValue & 1) == 0)
               pszNewValueString[31 - i] = '0';
            else 
               pszNewValueString[31 - i] = '1';

            ulNewValue >>= 1;

            }
         pszNewValueString[i] = '\0';
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c%c%c%c", (ulNewValue & 0xFF000000) >> 24,
                                                (ulNewValue & 0x00FF0000) >> 16,   
                                                (ulNewValue & 0x0000FF00) >> 8,   
                                                (ulNewValue & 0x000000FF));
         }
      else
         ASSERT_NOW();
      }
   else
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%lX", ulNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%lX", ulNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%lu", ulNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%ld", (signed long)ulNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%lo", ulNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 32; i++)
            {
            if ((ulNewValue & 1) == 0)
               pszNewValueString[31 - i] = '0';
            else 
               pszNewValueString[31 - i] = '1';

            ulNewValue >>= 1;

            }
         pszNewValueString[i] = '\0';

         // Find the first occurence of '1'.
         for (i=0; i < 32; i++)
            if (pszNewValueString[i] == '1')
               break;

         if (i == 32)
            i--;

         strcpy(&pszNewValueString[0], &pszNewValueString[i]);
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c%c%c%c", (ulNewValue & 0xFF000000) >> 24,
                                                (ulNewValue & 0x00FF0000) >> 16,   
                                                (ulNewValue & 0x0000FF00) >> 8,   
                                                (ulNewValue & 0x000000FF));
         }
      else
         ASSERT_NOW();
      }
}
void GetStringFromNumericShortValue(int            DisplayType,
                                    char           *pszNewValueString,
                                    unsigned short usNewValue,
                                    bool           bZeroPack,
                                    bool           bDisplayBase)
{
   if (bZeroPack)
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%04lX", usNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%04lX", usNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%04lu", usNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%04ld", (signed short)usNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%04lo", usNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 16; i++)
            {
            if ((usNewValue & 1) == 0)
               pszNewValueString[15 - i] = '0';
            else 
               pszNewValueString[15 - i] = '1';

            usNewValue >>= 1;

            }
         pszNewValueString[i] = '\0';
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c%c", (usNewValue & 0xFF00) >> 8,
                                            (usNewValue & 0x00FF));
         }
      else
         ASSERT_NOW();
      }
   else
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%lX", usNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%lX", usNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%lu", usNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%ld", (signed short)usNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%lo", usNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 16; i++)
            {
            if ((usNewValue & 1) == 0)
               pszNewValueString[15 - i] = '0';
            else 
               pszNewValueString[15 - i] = '1';

            usNewValue >>= 1;

            }
         pszNewValueString[i] = '\0';

         // Find the first occurence of '1'.
         for (i=0; i < 16; i++)
            if (pszNewValueString[i] == '1')
               break;

         if (i == 16)
            i--;

         strcpy(&pszNewValueString[0], &pszNewValueString[i]);
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c%c", (usNewValue & 0xFF00) >> 8,
                                            (usNewValue & 0x00FF));
         }
      else
         ASSERT_NOW();
      }
}

void GetStringFromNumericIntValue(int            DisplayType,
                                  char           *pszNewValueString,
                                  unsigned int   uiNewValue,
                                  bool           bZeroPack,
                                  bool           bDisplayBase)
{
   if (bZeroPack)
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%08lX", uiNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%08lX", uiNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%08lu", uiNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%08ld", (signed int)uiNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%08lo", uiNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 32; i++)
            {
            if ((uiNewValue & 1) == 0)
               pszNewValueString[31 - i] = '0';
            else 
               pszNewValueString[31 - i] = '1';

            uiNewValue >>= 1;

            }
         pszNewValueString[i] = '\0';
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c%c%c%c", (uiNewValue & 0xFF000000) >> 24,
                                                (uiNewValue & 0x00FF0000) >> 16,
                                                (uiNewValue & 0x0000FF00) >> 8,
                                                (uiNewValue & 0x000000FF));
         }
      else
         ASSERT_NOW();
      }
   else
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%lX", uiNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%lX", uiNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%lu", uiNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%ld", (signed int)uiNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%lo", uiNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 32; i++)
            {
            if ((uiNewValue & 1) == 0)
               pszNewValueString[31 - i] = '0';
            else 
               pszNewValueString[31 - i] = '1';

            uiNewValue >>= 1;
            }
         pszNewValueString[i] = '\0';

         // Find the first occurence of '1'.
         for (i=0; i < 32; i++)
            if (pszNewValueString[i] == '1')
               break;

         if (i == 32)
            i--;

         strcpy(&pszNewValueString[0], &pszNewValueString[i]);
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c%c%c%c", (uiNewValue & 0xFF000000) >> 24,
                                                (uiNewValue & 0x00FF0000) >> 16,
                                                (uiNewValue & 0x0000FF00) >> 8,
                                                (uiNewValue & 0x000000FF));
         }
      else
         ASSERT_NOW();
      }
}

void GetStringFromNumericCharValue(int DisplayType,
                                   char *pszNewValueString,
                                   unsigned char  ucNewValue,
                                   bool           bZeroPack,
                                   bool           bDisplayBase)
{
   if (bZeroPack)
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%02lX", ucNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%02lX", ucNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%02lu", ucNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%02ld", (signed char)ucNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%02lo", ucNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 8; i++)
            {
            if ((ucNewValue & 1) == 0)
               pszNewValueString[7 - i] = '0';
            else 
               pszNewValueString[7 - i] = '1';

            ucNewValue >>= 1;
            }
         pszNewValueString[i] = '\0';
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c", ucNewValue);
         }
      else
         ASSERT_NOW();
      }
   else 
      {
      if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
         sprintf(pszNewValueString, "0x%lX", ucNewValue);
      else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
         sprintf(pszNewValueString, "%lX", ucNewValue);
      else if (DisplayType == DISPLAY_DEC)
         sprintf(pszNewValueString, "%lu", ucNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         sprintf(pszNewValueString, "%ld", (signed char)ucNewValue);
      else if (DisplayType == DISPLAY_OCT)
         sprintf(pszNewValueString, "%lo", ucNewValue);
      else if (DisplayType == DISPLAY_BIN)
         {
         for (unsigned int i=0; i < 8; i++)
            {
            if ((ucNewValue & 1) == 0)
               pszNewValueString[7 - i] = '0';
            else 
               pszNewValueString[7 - i] = '1';

            ucNewValue >>= 1;
            }
         pszNewValueString[i] = '\0';

         // Find the first occurence of '1'.
         for (i=0; i < 8; i++)
            if (pszNewValueString[i] == '1')
               break;

         if (i == 8)
            i--;

         strcpy(&pszNewValueString[0], &pszNewValueString[i]);
         }
      else if (DisplayType == DISPLAY_ASCII)
         {
         sprintf(pszNewValueString, "%c", ucNewValue);
         }
      else
         ASSERT_NOW();
      }
}

void GetStringFromNumericCharValues(int            DisplayType,
                                    char          *pszNewValueString,
                                    unsigned char *pucNewValues,
                                    unsigned long  ulNumValues,
                                    bool           bZeroPack,
                                    bool           bDisplayBase)
{
   char szValueString[MAX_STRING_SIZE];

   strcpy(szValueString,"");
   pszNewValueString[0] = '\0';


   for (unsigned long i = 0; i < ulNumValues; i++)
      {
      if (bZeroPack)
         {
         if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
            sprintf(szValueString, "0x%02lX,", pucNewValues[i]);
         else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
            sprintf(szValueString, "%02lX,", pucNewValues[i]);
         else if (DisplayType == DISPLAY_DEC)
            sprintf(szValueString, "%02lu,", pucNewValues[i]);
         else if (DisplayType == DISPLAY_DEC_SIGNED)
            sprintf(szValueString, "%02ld,", (signed char)pucNewValues[i]);
         else if (DisplayType == DISPLAY_OCT)
            sprintf(szValueString, "%02lo,", pucNewValues[i]);
         else if (DisplayType == DISPLAY_BIN)
            {
            unsigned char ucNewValue;

            ucNewValue = pucNewValues[i];

            for (unsigned int j=0; j < 8; j++)
               {
               if ((ucNewValue & 1) == 0)
                  pszNewValueString[7 - j] = '0';
               else 
                  pszNewValueString[7 - j] = '1';

               ucNewValue >>= 1;
               }
            pszNewValueString[j] = '\0';
            }
         else if (DisplayType == DISPLAY_ASCII)
            {
            sprintf(szValueString, "%c,", pucNewValues[i]);
            }
         else
            ASSERT_NOW();
         }
      else 
         {
         if ((DisplayType == DISPLAY_HEX) && bDisplayBase)
            sprintf(szValueString, "0x%lX,", pucNewValues[i]);
         else if ((DisplayType == DISPLAY_HEX) && !bDisplayBase)
            sprintf(szValueString, "%lX,", pucNewValues[i]);
         else if (DisplayType == DISPLAY_DEC)
            sprintf(szValueString, "%lu,", pucNewValues[i]);
         else if (DisplayType == DISPLAY_DEC_SIGNED)
            sprintf(szValueString, "%ld,", (signed char)pucNewValues[i]);
         else if (DisplayType == DISPLAY_OCT)
            sprintf(szValueString, "%lo,", pucNewValues[i]);
         else if (DisplayType == DISPLAY_BIN)
            {
            unsigned char ucNewValue;

            ucNewValue = pucNewValues[i];

            for (unsigned int j=0; j < 8; j++)
               {
               if ((ucNewValue & 1) == 0)
                  pszNewValueString[7 - j] = '0';
               else 
                  pszNewValueString[7 - j] = '1';

               ucNewValue >>= 1;
               }
            pszNewValueString[j] = '\0';

            // Find the first occurence of '1'.
            for (j=0; j < 8; j++)
               if (pszNewValueString[j] == '1')
                  break;

            if (j == 8)
               j--;

            strcpy(&pszNewValueString[0], &pszNewValueString[j]);
            }
         else if (DisplayType == DISPLAY_ASCII)
            {
            sprintf(szValueString, "%c,", pucNewValues[i]);
            }
         else
            ASSERT_NOW();
         }
      strcat(pszNewValueString, szValueString);
      }

   if (strlen(pszNewValueString) < 2)
      return;

   // Knock off the last "," from the string....
   pszNewValueString[strlen(pszNewValueString) - 1] = '\0';
}

int GetNumericValuesFromString(int FillType,
                               int DisplayType,
                               char *pszNewValueString,
                               unsigned char  *pucNewValue,
                               unsigned short *pusNewValue,
                               unsigned int   *puiNewValue,
                               unsigned long  *pulNewValue,
                               float          *pfNewValue,
                               double         *pdNewValue,
                               unsigned char  *pucDataCount)
{
   int ErrRet;
   unsigned int uiIndex;
   unsigned int uiSubIndex;
   unsigned int uiDataCount;
   unsigned int uiDataIndex;
   char szSubString[MAX_STRING_SIZE]; 

   uiDataCount = 1;
   if (strlen(pszNewValueString) == 0)
      return IDERR_NO_TEXT_VALUE_GIVEN;

   if (pszNewValueString[0] == ',')
      return IDERR_NO_VALUE_BEFORE_COMMA;

   for (uiIndex=0; uiIndex < (strlen(pszNewValueString)-1); uiIndex++)
      {
      if (pszNewValueString[uiIndex] == ',')
         {
         if (pszNewValueString[uiIndex+1] == ',')
            {
            return IDERR_NO_VALUE_BEFORE_COMMA;
            }
         uiDataCount++;
         }
      }

   if (uiDataCount > CHAR_MAX)
      return IDERR_MAX_NUM_VALUES_EXCEEDED;

   uiSubIndex=0;
   uiDataIndex = 0;

   for (uiIndex=0; uiIndex < strlen(pszNewValueString); uiIndex++)
      {
      if (pszNewValueString[uiIndex] == ',')
         {
         szSubString[uiSubIndex] = '\0';
         ErrRet = GetNumericValueFromString(FillType,
                                            DisplayType,
                                            szSubString,
                                            &pucNewValue[uiDataIndex],
                                            &pusNewValue[uiDataIndex],
                                            &puiNewValue[uiDataIndex],
                                            &pulNewValue[uiDataIndex],
                                            &pfNewValue[uiDataIndex],
                                            &pdNewValue[uiDataIndex]);
         if (ErrRet != NO_ERROR)
            {
            return ErrRet;
            }

         uiSubIndex=0;
         uiDataIndex++;
         continue;
         }
      szSubString[uiSubIndex] = pszNewValueString[uiIndex];
      uiSubIndex++;
      }
   szSubString[uiSubIndex] = '\0';
   ErrRet = GetNumericValueFromString(FillType,
                                      DisplayType,
                                      szSubString,
                                      &pucNewValue[uiDataIndex],
                                      &pusNewValue[uiDataIndex],
                                      &puiNewValue[uiDataIndex],
                                      &pulNewValue[uiDataIndex],
                                      &pfNewValue[uiDataIndex],
                                      &pdNewValue[uiDataIndex]);
   if (ErrRet != NO_ERROR)
      return ErrRet;

   uiDataIndex++;

   *pucDataCount = (unsigned char)uiDataIndex;

   return NO_ERROR;
}

int GetNumericValueFromString(int FillType,
                              int DisplayType,
                              char *pszNewValueString,
                              unsigned char  *pucNewValue,
                              unsigned short *pusNewValue,
                              unsigned int   *puiNewValue,
                              unsigned long  *pulNewValue,
                              float          *pfNewValue,
                              double         *pdNewValue)
{
   BOOL bReturn = TRUE;

   // First read and validate the numeric string...
   if (FillType == FILL_DOUBLE)
      {
      // What base type of a value do we expect...
      if (DisplayType == DISPLAY_E)
         bReturn = GetDoubleValueFromEString(pszNewValueString, pdNewValue);
      else if (DisplayType == DISPLAY_F)
         bReturn = GetDoubleValueFromFString(pszNewValueString, pdNewValue);
      else if (DisplayType == DISPLAY_G)
         bReturn = GetDoubleValueFromGString(pszNewValueString, pdNewValue);
      else
         ASSERT_NOW();
      }
   else if (FillType == FILL_FLOAT)
      {
      // What base type of a value do we expect...
      if (DisplayType == DISPLAY_E)
         bReturn = GetFloatValueFromEString(pszNewValueString, pfNewValue);
      else if (DisplayType == DISPLAY_F)
         bReturn = GetFloatValueFromFString(pszNewValueString, pfNewValue);
      else if (DisplayType == DISPLAY_G)
         bReturn = GetFloatValueFromGString(pszNewValueString, pfNewValue);
      else
         ASSERT_NOW();
      }
   else if (FillType == FILL_LONG)
      {
      // What base type of a value do we expect...
      if (DisplayType == DISPLAY_HEX)
         bReturn = GetLongValueFromHexString(pszNewValueString, pulNewValue);
      else if (DisplayType == DISPLAY_DEC)
         bReturn = GetLongValueFromDecString(pszNewValueString, pulNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         bReturn = GetLongValueFromDecSignedString(pszNewValueString, pulNewValue);
      else if (DisplayType == DISPLAY_OCT)
         bReturn = GetLongValueFromOctString(pszNewValueString, pulNewValue);
      else if (DisplayType == DISPLAY_BIN)
         bReturn = GetLongValueFromBinString(pszNewValueString, pulNewValue);
      else if (DisplayType == DISPLAY_ASCII)
         bReturn = GetLongValueFromAscString(pszNewValueString, pulNewValue);
      else
         ASSERT_NOW();
      }
   else if (FillType == FILL_INT)
      {
      // What base type of a value do we expect...
      if (DisplayType == DISPLAY_HEX)
         bReturn = GetIntValueFromHexString(pszNewValueString, puiNewValue);
      else if (DisplayType == DISPLAY_DEC)
         bReturn = GetIntValueFromDecString(pszNewValueString, puiNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         bReturn = GetIntValueFromDecSignedString(pszNewValueString, puiNewValue);
      else if (DisplayType == DISPLAY_OCT)
         bReturn = GetIntValueFromOctString(pszNewValueString, puiNewValue);
      else if (DisplayType == DISPLAY_BIN)
         bReturn = GetIntValueFromBinString(pszNewValueString, puiNewValue);
      else if (DisplayType == DISPLAY_ASCII)
         bReturn = GetIntValueFromAscString(pszNewValueString, puiNewValue);
      else
         ASSERT_NOW();
      }
   else if (FillType == FILL_SHORT)
      {
      // What base type of a value do we expect...
      if (DisplayType == DISPLAY_HEX)
         bReturn = GetShortValueFromHexString(pszNewValueString, pusNewValue);
      else if (DisplayType == DISPLAY_DEC)
         bReturn = GetShortValueFromDecString(pszNewValueString, pusNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         bReturn = GetShortValueFromDecSignedString(pszNewValueString, pusNewValue);
      else if (DisplayType == DISPLAY_OCT)
         bReturn = GetShortValueFromOctString(pszNewValueString, pusNewValue);
      else if (DisplayType == DISPLAY_BIN)
         bReturn = GetShortValueFromBinString(pszNewValueString, pusNewValue);
      else if (DisplayType == DISPLAY_ASCII)
         bReturn = GetShortValueFromAscString(pszNewValueString, pusNewValue);
      else
         ASSERT_NOW();
      }
   else if (FillType == FILL_CHAR)
      {
      // What base type of a value do we expect...
      if (DisplayType == DISPLAY_HEX)
         bReturn = GetCharValueFromHexString(pszNewValueString, pucNewValue);
      else if (DisplayType == DISPLAY_DEC)
         bReturn = GetCharValueFromDecString(pszNewValueString, pucNewValue);
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         bReturn = GetCharValueFromDecSignedString(pszNewValueString, pucNewValue);
      else if (DisplayType == DISPLAY_OCT)
         bReturn = GetCharValueFromOctString(pszNewValueString, pucNewValue);
      else if (DisplayType == DISPLAY_BIN)
         bReturn = GetCharValueFromBinString(pszNewValueString, pucNewValue);
      else if (DisplayType == DISPLAY_ASCII)
         bReturn = GetCharValueFromAscString(pszNewValueString, pucNewValue);
      else
         ASSERT_NOW();
      }
   else
      {
      ASSERT_NOW();
      }

   // If there was an error, load the appropriate string and return false...
   if (bReturn == 0)
      {
      if (DisplayType == DISPLAY_HEX)
         return IDERR_INVALID_HEX_VALUE;
      else if (DisplayType == DISPLAY_DEC)
         return IDERR_INVALID_DEC_VALUE;
      else if (DisplayType == DISPLAY_DEC_SIGNED)
         return IDERR_INVALID_DEC_SIGNED_VALUE;
      else if (DisplayType == DISPLAY_OCT)
         return IDERR_INVALID_OCT_VALUE;
      else if (DisplayType == DISPLAY_BIN)
         return IDERR_INVALID_BIN_VALUE;
      else if (DisplayType == DISPLAY_ASCII)
         return IDERR_INVALID_ASC_VALUE;
      else if (DisplayType == DISPLAY_E)
         return IDERR_INVALID_ETYPE_VALUE;
      else if (DisplayType == DISPLAY_F)
         return IDERR_INVALID_FTYPE_VALUE;
      else if (DisplayType == DISPLAY_G)
         return IDERR_INVALID_GTYPE_VALUE;
      else
         {
         ASSERT_NOW();
         return NO_ERROR;
         }
      }

   // Everthing OK, return true...
   return NO_ERROR;
}
     

BOOL GetLongValueFromHexString(char *pszNumericString, unsigned long *pulValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%x",&ulCalculatedValue) == 0)
      return FALSE;

   *pulValue = ulCalculatedValue;

   return TRUE;
}
BOOL GetLongValueFromDecString(char *pszNumericString, unsigned long *pulValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&ulCalculatedValue) == 0)
      return FALSE;

   *pulValue = ulCalculatedValue;

   return TRUE;
}
BOOL GetLongValueFromDecSignedString(char *pszNumericString, unsigned long *pulValue)
{
   signed long slCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&slCalculatedValue) == 0)
      return FALSE;

   *pulValue = (unsigned long)slCalculatedValue;

   return TRUE;
}
BOOL GetLongValueFromOctString(char *pszNumericString, unsigned long *pulValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%lo",&ulCalculatedValue) == 0)
      return FALSE;

   *pulValue = ulCalculatedValue;

   return TRUE;
}
BOOL GetLongValueFromBinString(char *pszNumericString, unsigned long *pulValue)
{
   unsigned long ulCalculatedValue;
   unsigned long i;

   ulCalculatedValue = 0;

   for (i=0; i < strlen(pszNumericString); i++)
      {
      ulCalculatedValue <<= 1;
      if (pszNumericString[i] == '1')
         ulCalculatedValue |= 1;
      else if (pszNumericString[i] == '0')
         ulCalculatedValue |= 0;
      else 
         return FALSE;
      }

   *pulValue = ulCalculatedValue;

   return TRUE;
}
BOOL GetLongValueFromAscString(char *pszNumericString, unsigned long *pulValue)
{
   unsigned char ucCalcChar1Value;
   unsigned char ucCalcChar2Value;
   unsigned char ucCalcChar3Value;
   unsigned char ucCalcChar4Value;

   if (sscanf(pszNumericString,"%c%c%c%c",&ucCalcChar1Value,
                                          &ucCalcChar2Value,
                                          &ucCalcChar3Value,
                                          &ucCalcChar4Value) == 0)
      return FALSE;

   *pulValue = ((((unsigned long)ucCalcChar1Value) << 24) & 0xFF000000) +
               ((((unsigned long)ucCalcChar2Value) << 16) & 0x00FF0000) +
               ((((unsigned long)ucCalcChar3Value) <<  8) & 0x0000FF00) +
               ((((unsigned long)ucCalcChar4Value)      ) & 0x000000FF);

   return TRUE;
}

BOOL GetIntValueFromHexString(char *pszNumericString, unsigned int *puiValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%x",&ulCalculatedValue) == 0)
      return FALSE;

   *puiValue = (unsigned int)ulCalculatedValue;

   return TRUE;
}
BOOL GetIntValueFromDecString(char *pszNumericString, unsigned int *puiValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&ulCalculatedValue) == 0)
      return FALSE;

   *puiValue = (unsigned int)ulCalculatedValue;

   return TRUE;
}
BOOL GetIntValueFromDecSignedString(char *pszNumericString, unsigned int *puiValue)
{
   signed long slCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&slCalculatedValue) == 0)
      return FALSE;

   *puiValue = (unsigned int)slCalculatedValue;

   return TRUE;
}
BOOL GetIntValueFromOctString(char *pszNumericString, unsigned int *puiValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%lo",&ulCalculatedValue) == 0)
      return FALSE;

   *puiValue = (unsigned int)ulCalculatedValue;

   return TRUE;
}
BOOL GetIntValueFromBinString(char *pszNumericString, unsigned int *puiValue)
{
   unsigned long ulCalculatedValue;
   unsigned long i;

   ulCalculatedValue = 0;

   for (i=0; i < strlen(pszNumericString); i++)
      {
      ulCalculatedValue <<= 1;
      if (pszNumericString[i] == '1')
         ulCalculatedValue |= 1;
      else if (pszNumericString[i] == '0')
         ulCalculatedValue |= 0;
      else 
         return FALSE;
      }

   *puiValue = (unsigned int)ulCalculatedValue;

   return TRUE;
}
BOOL GetIntValueFromAscString(char *pszNumericString, unsigned int *puiValue)
{
   unsigned char ucCalcChar1Value;
   unsigned char ucCalcChar2Value;
   unsigned char ucCalcChar3Value;
   unsigned char ucCalcChar4Value;

   if (sscanf(pszNumericString,"%c%c%c%c",&ucCalcChar1Value,
                                          &ucCalcChar2Value,
                                          &ucCalcChar3Value,
                                          &ucCalcChar4Value) == 0)
      return FALSE;

   *puiValue = ((((unsigned long)ucCalcChar1Value) << 24) & 0xFF000000) +
               ((((unsigned long)ucCalcChar2Value) << 16) & 0x00FF0000) +
               ((((unsigned long)ucCalcChar3Value) <<  8) & 0x0000FF00) +
               ((((unsigned long)ucCalcChar4Value)      ) & 0x000000FF);

   return TRUE;
}

BOOL GetShortValueFromHexString(char *pszNumericString, unsigned short *pusValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%x",&ulCalculatedValue) == 0)
      return FALSE;

   if (ulCalculatedValue > USHRT_MAX)
      return FALSE;

   *pusValue = (unsigned short)ulCalculatedValue;

   return TRUE;
}
BOOL GetShortValueFromDecString(char *pszNumericString, unsigned short *pusValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&ulCalculatedValue) == 0)
      return FALSE;

   if (ulCalculatedValue > USHRT_MAX)
      return FALSE;

   *pusValue = (unsigned short)ulCalculatedValue;

   return TRUE;
}
BOOL GetShortValueFromDecSignedString(char *pszNumericString, unsigned short *pusValue)
{
   signed long slCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&slCalculatedValue) == 0)
      return FALSE;

   if ((slCalculatedValue > SHRT_MAX) || (slCalculatedValue < SHRT_MIN))
      return FALSE;

   *pusValue = (unsigned short)slCalculatedValue;

   return TRUE;
}
BOOL GetShortValueFromOctString(char *pszNumericString, unsigned short *pusValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%lo",&ulCalculatedValue) == 0)
      return FALSE;

   if (ulCalculatedValue > USHRT_MAX)
      return FALSE;

   *pusValue = (unsigned short)ulCalculatedValue;

   return TRUE;
}
BOOL GetShortValueFromBinString(char *pszNumericString, unsigned short *pusValue)
{
   unsigned long ulCalculatedValue;
   unsigned long i;

   ulCalculatedValue = 0;

   for (i=0; i < strlen(pszNumericString); i++)
      {
      ulCalculatedValue <<= 1;
      if (pszNumericString[i] == '1')
         ulCalculatedValue |= 1;
      else if (pszNumericString[i] == '0')
         ulCalculatedValue |= 0;
      else 
         return FALSE;
      }

   if (ulCalculatedValue > USHRT_MAX)
      return FALSE;

   *pusValue = (unsigned short)ulCalculatedValue;

   return TRUE;
}
BOOL GetShortValueFromAscString(char *pszNumericString, unsigned short *pusValue)
{
   unsigned char ucCalcChar1Value;
   unsigned char ucCalcChar2Value;

   if (sscanf(pszNumericString,"%c%c", &ucCalcChar1Value,
                                       &ucCalcChar2Value) == 0)
      return FALSE;

   *pusValue = ((((unsigned short)ucCalcChar1Value) << 8) & 0xFF00) +
               ((((unsigned short)ucCalcChar2Value)     ) & 0x00FF);

   return TRUE;
}

BOOL GetCharValueFromHexString(char *pszNumericString, unsigned char *pucValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%x",&ulCalculatedValue) == 0)
      return FALSE;

   if (sscanf(pszNumericString,"%x",&ulCalculatedValue) == EOF)
      return FALSE;

   if (ulCalculatedValue > UCHAR_MAX)
      return FALSE;

   *pucValue = (unsigned char)ulCalculatedValue;

   return TRUE;
}
BOOL GetCharValueFromDecString(char *pszNumericString, unsigned char *pucValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&ulCalculatedValue) == 0)
      return FALSE;

   if (ulCalculatedValue > UCHAR_MAX)
      return FALSE;

   *pucValue = (unsigned char)ulCalculatedValue;

   return TRUE;
}
BOOL GetCharValueFromDecSignedString(char *pszNumericString, unsigned char *pucValue)
{
   signed long slCalculatedValue;

   if (sscanf(pszNumericString,"%ld",&slCalculatedValue) == 0)
      return FALSE;

   if ((slCalculatedValue > SCHAR_MAX) || (slCalculatedValue < SCHAR_MIN))
      return FALSE;

   *pucValue = (unsigned char)slCalculatedValue;

   return TRUE;
}
BOOL GetCharValueFromOctString(char *pszNumericString, unsigned char *pucValue)
{
   unsigned long ulCalculatedValue;

   if (sscanf(pszNumericString,"%lo",&ulCalculatedValue) == 0)
      return FALSE;

   if (ulCalculatedValue > UCHAR_MAX)
      return FALSE;

   *pucValue = (unsigned char)ulCalculatedValue;

   return TRUE;
}
BOOL GetCharValueFromBinString(char *pszNumericString, unsigned char *pucValue)
{
   unsigned long ulCalculatedValue;
   unsigned long i;

   ulCalculatedValue = 0;

   for (i=0; i < strlen(pszNumericString); i++)
      {
      ulCalculatedValue <<= 1;
      if (pszNumericString[i] == '1')
         ulCalculatedValue |= 1;
      else if (pszNumericString[i] == '0')
         ulCalculatedValue |= 0;
      else 
         return FALSE;
      }

   if (ulCalculatedValue > UCHAR_MAX)
      return FALSE;

   *pucValue = (unsigned char)ulCalculatedValue;

   return TRUE;
}
BOOL GetCharValueFromAscString(char *pszNumericString, unsigned char *pucValue)
{
   unsigned char ucCalcChar1Value;

   if (sscanf(pszNumericString,"%c", &ucCalcChar1Value) == 0)
      return FALSE;

   *pucValue = ucCalcChar1Value;

   return TRUE;
}

BOOL GetDoubleValueFromEString(char *pszNumericString, double *pdValue)
{
   double dValue;

   if (sscanf(pszNumericString,"%lE",&dValue) == 0)
      return FALSE;

   *pdValue = dValue;

   return TRUE;
}
BOOL GetDoubleValueFromFString(char *pszNumericString, double *pdValue)
{
   double dValue;

   if (sscanf(pszNumericString,"%lf",&dValue) == 0)
      return FALSE;

   *pdValue = dValue;

   return TRUE;
}
BOOL GetDoubleValueFromGString(char *pszNumericString, double *pdValue)
{
   double dValue;

   if (sscanf(pszNumericString,"%lG",&dValue) == 0)
      return FALSE;

   *pdValue = dValue;

   return TRUE;
}

BOOL GetFloatValueFromEString(char *pszNumericString, float *pfValue)
{
   float fValue;

   if (sscanf(pszNumericString,"%E",&fValue) == 0)
      return FALSE;

   *pfValue = fValue;

   return TRUE;
}
BOOL GetFloatValueFromFString(char *pszNumericString, float *pfValue)
{
   float fValue;

   if (sscanf(pszNumericString,"%f",&fValue) == 0)
      return FALSE;

   *pfValue = fValue;

   return TRUE;
}
BOOL GetFloatValueFromGString(char *pszNumericString, float *pfValue)
{
   float fValue;

   if (sscanf(pszNumericString,"%G",&fValue) == 0)
      return FALSE;

   *pfValue = fValue;

   return TRUE;
}
void GetResourceString(int ErrRet, char *pszErrorText)
{
   BOOL bResetDetected;
   BOOL bWasInEmulation;

   if (ErrRet == IDERR_GDI_ERROR)
      TXRXGetErrorMessage(pszErrorText, &bResetDetected, &bWasInEmulation);
   else if (ErrRet == IDERR_RDI_ERROR)
      sprintf(pszErrorText, "Unknown RDI Error");
   else if (ErrRet < INTERNAL_ERROR_START)
      {
      CString  Buffer;
      (void)Buffer.LoadString(ErrRet);
      strncpy(pszErrorText,Buffer,MAX_STRING_SIZE);
      }
   else
      sprintf(pszErrorText, "Unknown Error Number %ld\n", ErrRet);
}
int DisplayString(LPCTSTR pszString, unsigned int nType)
{
   CString csTitle;
   if ((GetPFWAppObject() != NULL) &&
       (GetPFWAppObject()->m_pMainWnd != NULL) &&
       (IsWindow(GetPFWAppObject()->m_pMainWnd->m_hWnd)))
      {
      (void)csTitle.LoadString(AFX_IDS_APP_TITLE);
      return MessageBoxCWnd(GetPFWAppObject()->m_pMainWnd,pszString, csTitle,nType);
      }
   return ::MessageBox(NULL, pszString, "PathFinder", nType);
}

/****************************************************************************
     Function: DisplayString
     Engineer: David Nicholls
        Input: ID of Resource message and Type of Message Box
       Output: Return from MessageBox
  Description: Replacement for MessageBox which displays PathFinder title
Date           Initials    Description
16-Jul-2003    DN          Initial
19-Dec-2003    PN          Use new auto close message boxes
****************************************************************************/
int DisplayString(unsigned int uiResourceID, unsigned int nType)
{
   CString csTitle;
   CString csMessage;
   (void)csMessage.LoadString(uiResourceID);
   if ((GetPFWAppObject() != NULL) &&
       (GetPFWAppObject()->m_pMainWnd != NULL) &&
       (IsWindow(GetPFWAppObject()->m_pMainWnd->m_hWnd)))
      {
      (void)csTitle.LoadString(AFX_IDS_APP_TITLE);
      return MessageBoxCWnd(GetPFWAppObject()->m_pMainWnd,csMessage, csTitle,nType);

      }
   return ::MessageBox(NULL, csMessage, "PathFinder", nType);
}

/****************************************************************************
     Function: GetErrorString
     Engineer: Patrick Noonan
        Input: int    iError, 
               char * spszErrorString,
               int    iBufferSize
       Output: None
  Description: Move code from DisplayError to here to allow other functionality
               to be used elsewhere
Date           Initials    Description
16-Dec-2003    PN          Initial
****************************************************************************/
void MdiGetInternalErrorMessage(char *pszErrorText,int iBufferSize); // Nasty but it will do for now!!

extern "C" void GetErrorString(int ErrRet, char * pszErrorString,int iBufferSize)
{
   if (iBufferSize > 0x0)
      {
      BOOL bWasInEmulation = FALSE;
      BOOL bResetDetected  = FALSE;
      char szErrorText[MAX_STRING_SIZE*2];
      bool bInternalError = false;

#ifdef MIPS
      if (ErrRet == IE_MDI_INTERNAL)
         {
         // SPecial case for MIPS internal error handling
         MdiGetInternalErrorMessage(pszErrorString,iBufferSize);
         return;
         }
#endif

      if (ErrRet == IDERR_GDI_ERROR)
         TXRXGetErrorMessage(szErrorText, &bResetDetected, &bWasInEmulation);
      else if (ErrRet == IDERR_GDI_PCP_ERROR)
         TXRXPCPGetErrorMessage(szErrorText, &bResetDetected, &bWasInEmulation);
      else 
         if (ErrRet < INTERNAL_ERROR_START)
            {
            // Should have a resource string
            CString  Buffer;
            if(Buffer.LoadString(ErrRet))
               {
               bInternalError = false;
               strncpy(szErrorText,Buffer,sizeof(szErrorText));
               szErrorText[sizeof(szErrorText) - 0x1] = '\0';
               }
            else
               bInternalError = true;
            }
         else
            bInternalError = true;

      if (bInternalError)
         sprintf(szErrorText, "Internal Error Number %ld\n", ErrRet);
      strncpy(pszErrorString,szErrorText,iBufferSize);
      pszErrorString[iBufferSize - 0x1] = '\0';;
      }
}

static int iTimedMessageErrors[] = {MAX_CODE_SIZE_DOWNLOAD};

void DisplayError(int ErrRet, CWnd *pParentWnd /* = NULL */)
{
   char  szErrorText[MAX_STRING_SIZE*2];
   bool  bTimedError = GetUseTimedMessageBoxes();
   DWORD dwTimeDelay = GetTimeDelayMsgBox();

   for (int iLoop = 0x0;iLoop < (sizeof(iTimedMessageErrors)/sizeof(int));iLoop++)
      {
      if (ErrRet == iTimedMessageErrors[iLoop])
         {
         UseTimedMessageBoxes(TRUE);
         SetTimeDelayMsgBox(10000);
       break;
         }
      }

   GetErrorString(ErrRet,szErrorText,sizeof(szErrorText));
      
   // Finally display the error message....
   if (ErrRet != MEMORY_VERIFY_FAILED)
      {
      if (pParentWnd != NULL)
         {
         CString csTitle;
         (void)csTitle.LoadString(AFX_IDS_APP_TITLE);
         MessageBoxCWnd(pParentWnd,szErrorText, csTitle,MB_ICONWARNING | MB_OK);
         }
      else if ((GetPFWAppObject() != NULL) &&
               (GetPFWAppObject()->m_pMainWnd != NULL) &&
               (IsWindow(GetPFWAppObject()->m_pMainWnd->m_hWnd)))
         {
         CString csTitle;
         HWND hCurWin = GetFocus();
         (void)csTitle.LoadString(AFX_IDS_APP_TITLE);
         (void)MessageBoxCWnd(GetPFWAppObject()->m_pMainWnd,szErrorText, csTitle,MB_ICONWARNING | MB_OK);
         if (::IsWindow(hCurWin))         
            SetFocus(hCurWin);
         }
      else 
         {
         ::MessageBox(NULL, szErrorText, "PathFinder", MB_ICONWARNING | MB_OK);
         }
      }
   // Restore the previous values
   SetTimeDelayMsgBox(dwTimeDelay);
   UseTimedMessageBoxes(bTimedError);
}

/*****************************************************************************
     Function: EvaluateBinaryString
     Engineer: Hugh O'Keeffe
        Input: pMask *: storage for mask value
               pValue *: storage for value
               iFieldSize: number of binary characters -1 
               pszBuffer *: pointer to string
       Output: none
  Description: Evaluates string setting pMask and pValue approp.
               Handles '-' in input string. Assumes pszBuffer points to a
               valid string.  Based on BinaryEntry() from edtrig.c
               NOTE: don't cares 'X's are represented by '0's in mask word.
Date           Initial        Description
24-JAN-1994    HO'K           Initial
24-JAN-1995    HO'K        initial
19-JUN-1995    HO'K        initial
12-SEP-1995    HO'K        initial
19-Dec-2003    PN          Use MatchFamGrp instead of IsCOnfiguredFor funcs
*****************************************************************************/
extern "C" void EvaluateBinaryString(adtyp *pMask,
                                    adtyp *pValue,
                                    int   iFieldSize,
                                    char  *pszBuffer)
{
   int iStringPsn,iBinaryPsn;

   ASSERT(pszBuffer!=NULL);
   ASSERT(pMask!=NULL);
   ASSERT(pValue!=NULL);

   *pMask=*pValue=0;
   iStringPsn=0;              /* position within string */
   iBinaryPsn=0;              /* position within binary word (numbered from m.s.b. to l.s.b) */

   while (pszBuffer[iStringPsn])
      {
      switch (pszBuffer[iStringPsn])
         { 
         case '0': 
            if(MatchFamGrp(ProcCfg.FamilyGroup,SmartMX,NoMatchGroup))
               *pValue= *pValue & (SMARTMX_MAX_PROC_ADDR - (adtyp)power(2,iFieldSize-iBinaryPsn));
            else
               *pValue= *pValue & (MAX_PROC_ADDR - (adtyp)power(2,iFieldSize-iBinaryPsn));
            *pMask = *pMask | (adtyp)power(2,iFieldSize - iBinaryPsn);
            iBinaryPsn++;
            break ;
         case '1': 
            *pValue=*pValue | (adtyp)power(2,iFieldSize - iBinaryPsn);
            *pMask=*pMask | (adtyp)power(2, iFieldSize - iBinaryPsn);
            iBinaryPsn++;
            break ;
         case 'x':
         case 'X': 
            *pMask=*pMask & (MAX_PROC_ADDR - (adtyp)power(2,iFieldSize -iBinaryPsn));
            iBinaryPsn++;
            break ;
         case '-':
            break;
         default:
            ASSERT_NOW();
            break;
         }
      iStringPsn++;
      }
}

/*****************************************************************************
     Function: ValidateBinaryString
     Engineer: Hugh O'Keeffe
        Input: iFieldSize: number of binary characters -1 
               pszBuffer *: pointer to string
       Output: TyError: NO_ERROR or error number
  Description: Ensures binary string is valid.
Version     Date           Initial     Description
1.0.0       24-JAN-1994    HO'K        Initial
0.9.0-PFW66 24-JAN-1995    HO'K        initial
*****************************************************************************/
extern "C" TyError ValidateBinaryString(int iFieldSize,char *pszBuffer)
{
   char szBuffer[SCREENWIDTH],*pszP;

   /* make a local copy as we do some destructive testing */
   strcpy(szBuffer,pszBuffer);
   pszP=szBuffer;

   /* first remove any '-'s */
   while (*pszP)
      {
      if (*pszP=='-')
         StrDel(pszP,1);
      pszP++;
      }

   /* check for valid characters */
   pszP=szBuffer;
   while (*pszP)
      {
      switch (*pszP)
         {
         case 'X':
         case 'x':
         case '1':
         case '0':
            break;
         default:
            return INVALID_CHAR;
         }
      pszP++;
      }

   /* finally, check length */
   if (strlen(szBuffer) != (uint)(iFieldSize+1))
      return INVALID_LEN;
   return NO_ERROR;
}

/*****************************************************************************
     Function: BuildBinaryString
     Engineer: Hugh O'Keeffe
        Input: Mask: mask value
               Value: value
               iFieldSize: number of binary characters -1 
               pszBuffer *: pointer to storage for string
       Output: none
  Description: Builds a binary string placing '-'s every NIBBLE_SIZE chars.
               Based on Write_Field() from trigutil.c.
Version     Date           Initial        Description
1.0.0       24-JAN-1994    HO'K           Initial
0.9.0-PFWXA 22-AUG-1995    HO'K           Initial
*****************************************************************************/
extern "C" void BuildBinaryString(adtyp Mask,adtyp Value,int iFieldSize,char *pszBuffer)
{
   sint iStringPsn,iBinaryPsn,iHyphen;
   char cChar;

   ASSERT(pszBuffer!=NULL);

   iStringPsn=0;              /* position within string */
   iBinaryPsn=0;              /* position within binary word (numbered from m.s.b. to l.s.b) */
   iHyphen=NIBBLE_SIZE;

   do
      {
      if ((Mask & power(2,(iFieldSize-iBinaryPsn))) == 0)
         cChar = 'X';
      else
         cChar = ((Value & power (2,(iFieldSize - iBinaryPsn))) == 0) ? '0' : '1' ;
      pszBuffer[iStringPsn]=cChar;
      iBinaryPsn++;
      iStringPsn++;
      iHyphen--;
      if (!iHyphen)
         {
         pszBuffer[iStringPsn]='-';
         iStringPsn++;
         iHyphen=NIBBLE_SIZE;
         }
      }
   while (!(iBinaryPsn == iFieldSize+1));

   if (pszBuffer[iStringPsn-1] == '-')
      pszBuffer[iStringPsn-1]='\0';
   else
      pszBuffer[iStringPsn]='\0';
}

/***************************************************************************
      Function: SearchComboForData
      Engineer: Patrick Noonan
         Input: CComboBox *pComboBox : pointer to ComboBox
                DWORD Data           : Data to search for
        Output: index of first entry containing this data, otherwise -1
   Description: Search ComboBox fpr first occurence of this data
Version        Date           Initials    Description
0.9.0-PFPU51   08-Dec_1999    PN          initial
****************************************************************************/
int SearchComboForData(CComboBox *pComboBox, DWORD Data)
{
   int iNumEntries   = 0x0;
   int iLoop         = 0x0;
   int iRetVal       = -1;
   
   iNumEntries = pComboBox->GetCount();
   for (iLoop = 0x0;iLoop < iNumEntries ; iLoop++ )
      {
      if (pComboBox->GetItemData(iLoop) == Data)
         {
         iRetVal = iLoop;
         break;
         }
      }
   return iRetVal ;
}
/***************************************************************************
      Function: ComboAddResStringAndData
      Engineer: Patrick Noonan
         Input: CComboBox *pComboBox : pointer to ComboBox
                int     iResID       ; resource id of string to add
                DWORD Data           : Data to add
        Output: None
   Description: Search ComboBox fpr first occurence of this data
                Some strange behaviour encountered when calling AddString
                with a CString parameter, use a char * instead
Version        Date           Initials    Description
0.9.0-PFPU51   08-Dec_1999    PN          initial
****************************************************************************/
void ComboAddResStringAndData(CComboBox *pComboBox,int iResID, DWORD Data)
{
   CString  Buffer;
   char     szBuffer[_MAX_PATH];
   int      iIndex = 0x0;
   
   (void)Buffer.LoadString(iResID);
   strcpy(szBuffer,Buffer);
   iIndex = pComboBox->AddString(szBuffer);
   (void)pComboBox->SetItemData(iIndex,Data);
}
/***************************************************************************
      Function: ComboAddCStringAndData
      Engineer: Patrick Noonan
         Input: CComboBox *pComboBox : pointer to ComboBox
                int     iResID       ; resource id of string to add
                DWORD Data           : Data to add
        Output: None
   Description: Search ComboBox fpr first occurence of this data
Version        Date           Initials    Description
0.9.0-PFPU51   08-Dec_1999    PN          initial
****************************************************************************/
void ComboAddCStringAndData(CComboBox *pComboBox,CString Buffer, DWORD Data)
{
   int      iIndex = 0x0;
   char     szBuffer[_MAX_PATH];
   
   strcpy(szBuffer,Buffer);
   
   iIndex = pComboBox->AddString(szBuffer);
   (void)pComboBox->SetItemData(iIndex,Data);
}
/****************************************************************************
     Function: GeneralOptionEnabled
     Engineer: Patrick Noonan
        Input: uint32 uiInfo
       Output: BOOL
  Description: If bit 0 is set , then option is enabled
               For use in familyxx.cfg files to determine which
               options should be shown in the processor configuration dialog
Version        Date           Initials    Description
2.0.0-PFPXA   11-JUL-2001     PN          Initial
****************************************************************************/
BOOL GeneralOptionEnabled(uint32 uiInfo)
{
   BOOL bRetVal = FALSE;   
   if (uiInfo & 0x1)
      bRetVal = TRUE;
   return(bRetVal);
}
/****************************************************************************
     Function: GeneralOptionVisible
     Engineer: Patrick Noonan
        Input: uint32 uiInfo
       Output: BOOL
  Description: If bit 1 is set , then option is visible
               For use in familyxx.cfg files to determine which
               options should be shown in the processor configuration dialog
Version        Date           Initials    Description
2.0.0-PFPXA   11-JUL-2001     PN          Initial
****************************************************************************/
BOOL GeneralOptionVisible(uint32 uiInfo)
{
   BOOL bRetVal = FALSE;   
   if (uiInfo & 0x2)
      bRetVal = TRUE;
   return(bRetVal);
}
/****************************************************************************
     Function: GeneralOptionDefaultOn
     Engineer: Patrick Noonan
        Input: uint32 uiInfo
       Output: BOOL
  Description: If bit 2 is set , then option is enabled
               For use in familyxx.cfg files to determine which
               options should be shown in the processor configuration dialog
Version        Date           Initials    Description
2.0.0-PFPXA   11-JUL-2001     PN          Initial
****************************************************************************/
BOOL GeneralOptionDefaultOn(uint32 uiInfo)
{
   BOOL bRetVal = FALSE;   
   if (uiInfo & 0x4)
      bRetVal = TRUE;
   return(bRetVal);
}

/****************************************************************************
     Function: GeneralOptionDefaultOn
     Engineer: Patrick Noonan
        Input: uint32 uiInfo
       Output: BOOL
  Description: If bit 2 is set , then option is enabled
               For use in familyxx.cfg files to determine which
               options should be shown in the processor configuration dialog
Version        Date           Initials    Description
2.0.0-PFPXA   11-JUL-2001     PN          Initial
****************************************************************************/
BOOL GeneralOptionGrouped(uint32 uiInfo)
{
   BOOL bRetVal = FALSE;   
   if (uiInfo & 0x8)
      bRetVal = TRUE;
   return(bRetVal);
}

/****************************************************************************
     Function: AddGeneralOptionToCheckableListBox
     Engineer: Patrick Noonan
        Input: uint32         uiInfo,
               CString        sText,
               DWORD          dwItemData,
               CCheckListBox  &cCheckListBox,
               BOOL           *pSet
       Output: None
  Description: Message handler for the ProcMode ComboBox ON_CBN_SELCHANGED
               message.
Version        Date           Initials    Description
2.0.0-PFPXA    11-JUL-2001     PN          Initial
****************************************************************************/
void AddGeneralOptionToCheckableListBox(uint32 uiInfo,CString sText,DWORD dwItemData,CCheckListBox &cCheckListBox,BOOL *pSet)
{
   int               iIndex = 0x0;

   if (GeneralOptionVisible(uiInfo))
      {
      iIndex = cCheckListBox.AddString(sText);
       // Option is available and visible so add check box
      (void)cCheckListBox.SetItemData(iIndex,(DWORD)dwItemData);
      // Use the current value to decide whether to check the box or not
      if (!GeneralOptionEnabled(uiInfo))
         {
         *pSet = GeneralOptionDefaultOn(uiInfo);
         }
      cCheckListBox.SetCheck(iIndex,*pSet);
      cCheckListBox.Enable(iIndex,GeneralOptionEnabled(uiInfo));
      }
   else
      {
      // Not available so use the default option
      *pSet = GeneralOptionDefaultOn(uiInfo);
      }
}
/****************************************************************************
     Function: SplitUpString
     Engineer: Patrick Noonan
        Input: CString  szFullString :  Original string
               char     cSplitChar   :  String delimiter
               CList<CString,CString> *pSplitString  : List for extracted string
       Output: None
  Description: Used to split up the traced port titles string into the constituent ports
Version        Date           Initials    Description
0.9.0-PFPU51   03-APR-2000    PN          initial
****************************************************************************/
void SplitUpString(char * pszFullString , char  cSplitChar, CList<CString,CString> *pSplitString)
{
   char *pszStart       = NULL;
   char *pszSplitChar   = NULL;
   char szBuffer[SCREENWIDTH];
   CString AddItem;
   
   if (pSplitString)
      {
      if (!pSplitString->IsEmpty())
         {
         pSplitString->RemoveAll();
         }
      }
   
   strcpy(szBuffer,pszFullString);
   pszStart = szBuffer;
   for (;;)
      {
      pszSplitChar = strchr(pszStart,cSplitChar);
      if (pszSplitChar)
         *pszSplitChar = EOS;
      else
         {
         AddItem = pszStart;
         break;
         }
      AddItem = pszStart;
      (void)pSplitString->AddTail(AddItem);
      pszStart=pszSplitChar+1;
      }  
   (void)pSplitString->AddTail(AddItem);
}
/****************************************************************************
     Function: GetLastRunFileNameFromRegistry
     Engineer: Patrick Noonan
        Input: CString Key
       Output: None
  Description: In certain circumstances in the past, uninititialised strings 
               were written to the registry in the LAST_RUN field.
               When that garbage was passed as an initial file to a CFileDialog
               the dialog would return immediately with an ID_CANCEL.
               Use this function to check the data in the LAST_RUN key and return
               NULL if it is crap.
Version        Date           Initials    Description
1.0.9-PFPMIPS  26-Jul-2002    PN          initial
****************************************************************************/
CString GetLastRunFileNameFromRegistry(CString Key)
{
   CString     csRetVal = "";
   char        szBuffer[_MAX_PATH * 2];
   uint16      uiLoop   = 0x0;
   BOOL        bBadFileName = FALSE;
   csRetVal = GetPFWAppObject()->GetProfileString(Key, "LAST_RUN", NULL);
   strncpy(szBuffer,csRetVal,sizeof(szBuffer));
   uiLoop = strlen(szBuffer);
   while (uiLoop)
      {
      // If a non printable character exists then it is a crap filename
     uiLoop--;
      if (!isprint(szBuffer[uiLoop]))
         {
         bBadFileName = TRUE;
         break;
         }
      }
   if(bBadFileName)
      csRetVal = "";
   return csRetVal;
}
/****************************************************************************
     Function: GetLastRunFileNameFromRegistry
     Engineer: Patrick Noonan
        Input: CString Key
       Output: None
  Description: In certain circumstances in the past, uninititialised strings 
               were written to the registry in the LAST_RUN field.
               When that garbage was passed as an initial file to a CFileDialog
               the dialog would return immediately with an ID_CANCEL.
               Use this function to check the data in the LAST_RUN key and return
               NULL if it is crap.
Version        Date           Initials    Description
1.0.9-PFPMIPS  26-Jul-2002    PN          initial
****************************************************************************/
CString GetLastEditFileNameFromRegistry(CString Key)
{
   CString     csRetVal = "";
   char        szBuffer[_MAX_PATH * 2];
   uint16      uiLoop   = 0x0;
   BOOL        bBadFileName = FALSE;
   csRetVal = GetPFWAppObject()->GetProfileString(Key, "LAST_EDIT", NULL);
   strncpy(szBuffer,csRetVal,sizeof(szBuffer));
   uiLoop = strlen(szBuffer);
   while (uiLoop)
      {
      // If a non printable character exists then it is a crap filename
     uiLoop--;
      if (!isprint(szBuffer[uiLoop]))
         {
         bBadFileName = TRUE;
         break;
         }
      }
   if(bBadFileName)
      csRetVal = "";
   return csRetVal;
}


/****************************************************************************
     Function: WildCardCompare
     Engineer: Patrick Shiels
        Input: pszString    * : pointer to string to compare
               pszWild      * : pointer to wild card string
               bCaseSensitive : case sensitive compare or not
       Output: TRUE if pszString satisfies the wild card compare, else FALSE
  Description: Some examples:
               pszString : "iGlobalVar"
               pszWild   : "iGlob*"    "?Glob*"    "?Global???"   => TRUE
               pszWild   : "xGlob*"    "iGlob?"    "iGlobalBar"   => FALSE

               We terminate the compare at length _MAX_PATH for sanity.

Version        Date           Initials    Description
1.0.0          29-AUG-2002    PJS         initial
****************************************************************************/
BOOL WildCardCompare(const char * pszString, const char * pszWild, BOOL bCaseSensitive)
{
   int      i, w, iStrLen, iWildLen, iMaxStartPos;
   BOOL     bLeadingStar;
   BOOL     bMatch;

   if ((pszString    == NULL) ||
       (pszWild      == NULL) ||
       (pszString[0] == EOS ) ||
       (pszWild[0]   == EOS ))
      {
      return FALSE;
      }

   if (strlen(pszString) > _MAX_PATH)
      return FALSE;

   if (strlen(pszWild) > _MAX_PATH)
      return FALSE;

   // process all of the wildcard string
   while (pszWild[0] != EOS)
      {
      if (pszWild[0] == '*')
         {
         pszWild++;
         if (pszWild[0] == EOS)
            {
            // wildcard string ends in '*' so matches anything else in pszString
            return TRUE;
            }
         else if (pszWild[0] == '*')
            {
            // wildcard contains "**", just continue with second '*'
            continue;
            }
         bLeadingStar = TRUE;
         }
      else
         {
         bLeadingStar = FALSE;
         }

      iStrLen = strlen(pszString);

      // get wildcard length up to but not including next '*' or EOS
      for (iWildLen=0; (pszWild[iWildLen] != EOS) && (pszWild[iWildLen] != '*'); iWildLen++);

      if (iWildLen == 0)
         return FALSE;

      if (iWildLen > iStrLen)
         return FALSE;

      if (bLeadingStar)
         {
         if (pszWild[iWildLen] == EOS)
            {
            // e.g. "*Func" must match exactly last 4 characters of pszString
            iMaxStartPos = 0;
            pszString += (iStrLen-iWildLen);
            }
         else
            {
            // e.g. "*Func*" can match on any 4 characters of pszString
            iMaxStartPos = iStrLen-iWildLen;
            }
         }
      else
         {
         // e.g. "Func*" or "Func" must match exactly first 4 characters of pszString
         iMaxStartPos = 0;
         }

      bMatch = FALSE;

      for (i=0; i<=iMaxStartPos; i++, pszString++)
         {
         for (w=0; w<iWildLen; w++)
            {
            // need either an exact character match or '?' matches any one char
            if ((pszWild[w] == '?')            ||
                (pszWild[w] == pszString[w])   ||
               (!bCaseSensitive && (toupper(pszWild[w]) == toupper(pszString[w]))))
               {
               // have a match on this character, continue checking for iWildLen
               bMatch = TRUE;
               continue;
               }
            else
               {
               // mismatch, break out to move on one char in pszString and try again
               bMatch = FALSE;
               break;
               }
            }

         // if we matched iWildLen chars, break out to move on iWildLen chars in pszWild
         if (bMatch)
            break;

         // else no match yet, move on one char in pszString and try again
         }


      // did not match on previous iWildLen chars, so no hope of a match
      if (!bMatch)
         return FALSE;

      pszString += iWildLen;
      pszWild   += iWildLen;
      }

   // have processed all of pszWild, if pszString also processed, we match
   if (pszString[0] == EOS)
      return TRUE;

   return FALSE;
}


/****************************************************************************
     Function: StripWhiteSpace
     Engineer: Patrick Shiels
        Input: pszString    * : pointer to string to process
       Output: copy of pszString pointer
  Description: Strip all white space from the string
Version        Date           Initials    Description
1.0.0          06-SEP-2002    PJS         initial
****************************************************************************/
char * StripWhiteSpace(char * pszString)
{
   char * pszTemp = pszString;

   while (*pszTemp)
      {
      if (isspace(*pszTemp))
         strcpy(pszTemp, pszTemp+1);
      else
         pszTemp++;
      }

   return pszString;
}

/****************************************************************************
     Function: HyphenateBinaryString
     Engineer: Patrick Noonan
        Input: char *   pszBinaryString
               int      iHyphenIncrement
       Output: copy of pszBinaryString pointer
  Description: Take a supposed binary string , validate it and insert hyphen every
               iHyphenIncrement binary digits
Version        Date           Initials    Description
1.0.0          24-SEP-2002    PN         initial
****************************************************************************/
extern "C" char * HyphenateBinaryString(char * pszBinaryString,int iHyphenIncrement)
{
   char szBuffer[MAX_STRING_SIZE];
   int iStrIndex  = 0x0;
   int iDstIndex  = 0x0;

   // First of all, dehyphenate the string
   (void)DehyphenateBinaryString(pszBinaryString);

   int iStrLen    = strlen(pszBinaryString);

   for (iStrIndex = 0x0;iStrIndex < iStrLen;)
      {
      szBuffer[iDstIndex++] = pszBinaryString[iStrIndex++];
      if (((iStrIndex % iHyphenIncrement) == 0x0) && (iStrIndex != iStrLen))
         {
         // Only if the next character is a binary digit
         if (pszBinaryString[iStrIndex] == '1' || pszBinaryString[iStrIndex] == '0')
            szBuffer[iDstIndex++] = '-';
         }
      }
   szBuffer[iDstIndex] = '\0';
   strcpy(pszBinaryString,szBuffer);
   return pszBinaryString;

}

/****************************************************************************
     Function: DehyphenateBinaryString
     Engineer: Patrick Noonan
        Input: char *   pszBinaryString
       Output: copy of pszBinaryString pointer
  Description: Remove any hyphens from a binary string
Version        Date           Initials    Description
1.0.0          24-SEP-2002    PN         initial
****************************************************************************/
extern "C" char * DehyphenateBinaryString(char * pszBinaryString)
{
   if (strchr(pszBinaryString,'-'))
      {
      // String contains at least one hyphen
      char szBuffer[MAX_STRING_SIZE];
      int iStrIndex  = 0x0;
      int iDstIndex  = 0x0;
      for (;;)
         {
         if (pszBinaryString[iStrIndex] != '-')
            szBuffer[iDstIndex++] = pszBinaryString[iStrIndex];
         if (pszBinaryString[iStrIndex] == '\0')
            break;
         iStrIndex++;
         }
      strcpy(pszBinaryString,szBuffer);
      }
   return pszBinaryString;
}


/****************************************************************************
     Function: GetComparatorCauseOfBreakString
     Engineer: Rory Cosgrove
        Input: char *   szCauseOfBreak
       Output: 
  Description: 
Version        Date           Initials    Description
2.0.3-G        13-NOV-2002    RC          initial
****************************************************************************/
extern "C" void GetComparatorCauseOfBreakString(char *szCauseOfBreak)
{
   TyBankNo Bank        = DEFAULT_COMMON_BANK;
   ubyte    ubSFRMATCHH = 0x0;
   ubyte    ubSFRMATCHL = 0x0;
   ulong    ulEPTR      = 0x0;
   uint     uiDPTR      = 0x0;
   uint     uiStack     = 0x0;
   char     szValue[20];

   TXRXReadMem(MEM_ESFR, MEM_NORMAL_ACCESS,&Bank,0xDB,1,&ubSFRMATCHH,sizeof(ubyte));
   TXRXReadMem(MEM_ESFR, MEM_NORMAL_ACCESS,&Bank,0xDC,1,&ubSFRMATCHL,sizeof(ubyte));

   ubSFRMATCHH &= 0x7F;   // bit 7 is reserved

   if(ubSFRMATCHH) 
      {
      switch(ubSFRMATCHH)
         {
         case 0x01:
            ulEPTR =  (ubyte)(        GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_EPL].ulMatchValue);
            ulEPTR |= (uint) ((ubyte)(GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_EPM].ulMatchValue) << 0x8);
            ulEPTR |= (ulong)((ubyte)(GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_EPH].ulMatchValue) << 0x10);
            strcat(szCauseOfBreak," EPTR = ");
            sprintf(szValue,"0x%04X",ulEPTR);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x02:
            uiDPTR =  (ubyte)(       GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_DPL].ulMatchValue);
            uiDPTR |= (uint)((ubyte)(GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_DPH].ulMatchValue) << 0x8);
            strcat(szCauseOfBreak," DPTR = ");
            sprintf(szValue,"0x%04X",uiDPTR);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x04:
            uiStack =  (ubyte)(       GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_SP].ulMatchValue);
            uiStack |= (uint)((ubyte)(GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_SPE].ulMatchValue) << 0x8);
            strcat(szCauseOfBreak," SP\\SPE = ");
            sprintf(szValue,"0x%04X",uiStack);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x08:
            strcat(szCauseOfBreak," PSW = ");
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_PSW].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x10:
            strcat(szCauseOfBreak," PSWH = ");
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_PSWH].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x20:
            strcat(szCauseOfBreak," A = ");
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_ACC].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x40:
            strcat(szCauseOfBreak," B = ");
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_B].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         default:
            strcat(szCauseOfBreak," UNKNOWN");
            break;

         }
      }
   else if(ubSFRMATCHL)
      {
      switch(ubSFRMATCHL)
         {
         case 0x01:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x02:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+1].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+1].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x04:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+2].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+2].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x08:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+3].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+3].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x10:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+4].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+4].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x20:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+5].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+5].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         case 0x40:
            sprintf(szCauseOfBreak,"%s %s = ",szCauseOfBreak,GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+6].szRegName);
            sprintf(szValue,"0x%04X",GetPFWAppObject()->m_SmartMXComparatorSetup.ItemList[COMPARATOR_INDEX_POS_USER_START+6].ulMatchValue);
            strcat(szCauseOfBreak, szValue);
            break;
         default:
            strcat(szCauseOfBreak," UNKNOWN");
            break;
         }
       }
}



/****************************************************************************
     Function: GetHexStringFromNumericCharValues
     Engineer: David Nicholls
        Input: String output,Char Array to be converted,
               Number of Characters in the array,
               flag to indicate if (0x) should be put on 
               the string,
               Flag to indicate big or little endian
       Output: Error Code
  Description: convert an array of up to 4 characters into a string
               big or little endian
Date            Initials    Description
12-FEB-2003     DN          initial
20-Aug-2004     PJS         if more than 4 chars truncate to 4 and add ..
****************************************************************************/
TyError GetHexStringFromNumericCharValues(char          *pszNewValueString,
                                          unsigned char *pucNewValues,
                                          unsigned long  ulNumValues,
                                          bool           bDisplayBase,
                                          bool           bEndian)
{
   char szValueString[MAX_STRING_SIZE];

   strcpy(szValueString,"");
   pszNewValueString[0] = '\0';

   switch (ulNumValues)
      {
      case 0:
         return ERR_IE_INVALID_PARAM_GETHEXSTRING;
      case 1:
         if (bDisplayBase)
            sprintf(szValueString, "0x%02lX", pucNewValues[0]);
         else 
            sprintf(szValueString, "%02lX", pucNewValues[0]);
         break;
      case 2:
         if (bDisplayBase)
            sprintf(szValueString, "0x%04lX",NormaliseEndianShort(pucNewValues,bEndian));
         else 
            sprintf(szValueString, "%04lX",NormaliseEndianShort(pucNewValues,bEndian));
         break;
      case 3:
         if (bDisplayBase)
            sprintf(szValueString, "0x%06lX",NormaliseEndianTripple(pucNewValues,bEndian));
         else 
            sprintf(szValueString, "%06lX",NormaliseEndianTripple(pucNewValues,bEndian));
         break;
      case 4:
      default:
         if (bDisplayBase)
            sprintf(szValueString, "0x%08lX",NormaliseEndianLong(pucNewValues,bEndian));
         else 
            sprintf(szValueString, "%08lX",NormaliseEndianLong(pucNewValues,bEndian));
         if (ulNumValues > 4)
            strcat(szValueString, "..");
         break;
      }


   strcpy(pszNewValueString, szValueString);

   return NO_ERROR;

}

/****************************************************************************
     Function: NormaliseEndianShort
     Engineer: David Nicholls
        Input: Array of 2 characters, flag bBigEndian
       Output: Short interpretation of the 2 characters
  Description: convert an array of 2 characters into a short in
               big or little endian
Version        Date           Initials    Description
0.9.0         12-FEB-2003    DN          initial
****************************************************************************/
static unsigned short NormaliseEndianShort(unsigned char * pucArray,bool bBigEndian)
{
   unsigned short usRetVal=0;

   if (bBigEndian)
      {
      usRetVal=( (pucArray[0] << 8) |
               (  pucArray[1])        );
      }
   else
      {
      usRetVal=( (pucArray[1] << 8) |
               (  pucArray[0])        );
      }

   return usRetVal;
}

/****************************************************************************
     Function: NormaliseEndianTripple
     Engineer: David Nicholls
        Input: Array of 3 characters, flag bBigEndian
       Output: long interpretation of the 3 characters
  Description: convert an array of 3 characters into a long in
               big or little endian
Version        Date           Initials    Description
0.9.0         12-FEB-2003     DN           initial
****************************************************************************/
static unsigned long NormaliseEndianTripple(unsigned char * pucArray,bool bBigEndian)
{
   unsigned long ulRetVal=0;

   if (bBigEndian)
      {
      ulRetVal=((pucArray[0] << 16) | 
                (pucArray[1] <<  8) | 
                (pucArray[2])        );
      }
   else
      {
      ulRetVal=((pucArray[2] << 16) |    
                (pucArray[1] <<  8) |    
                (pucArray[0])        );  
      }

   return ulRetVal;
}



/****************************************************************************
     Function: NormaliseEndianLong
     Engineer: David Nicholls
        Input: Array of 4 characters, flag bBigEndian
       Output: Short interpretation of the 4 characters
  Description: convert an array of 4 characters into a long in
               big or little endian
Version        Date           Initials    Description
0.9.0         12-FEB-2003    DN          initial
****************************************************************************/
static unsigned long NormaliseEndianLong(unsigned char * pucArray,bool bBigEndian)
{
   unsigned long ulRetVal=0;

   if (bBigEndian)
      {
      ulRetVal=( (pucArray[0] << 24) |
                 (pucArray[1] << 16) | 
                 (pucArray[2] <<  8) | 
                 (pucArray[3])        );
      }
   else
      {
      ulRetVal=( (pucArray[3] << 24) |    
                 (pucArray[2] << 16) |    
                 (pucArray[1] <<  8) |    
                 (pucArray[0])        );  
      }

   return ulRetVal;
}

