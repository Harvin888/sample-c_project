#if !defined(AFX_ADDITEM_H__D6FDB71F_F1E9_43DD_9610_E3EBFDE9FB56__INCLUDED_)
#define AFX_ADDITEM_H__D6FDB71F_F1E9_43DD_9610_E3EBFDE9FB56__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddItem.h : header file
//
#define MAX_STRING_SIZE     0xFF

/////////////////////////////////////////////////////////////////////////////
// CAddItem dialog

class CAddItem : public CDialog
{
	//DECLARE_DYNCREATE(CAddItem);

// Construction
public:
	CAddItem(CWnd* pParent = NULL);   // standard constructor

	BOOL          m_bEditItem;
   char           m_szName[MAX_STRING_SIZE];
   unsigned long  m_ulSize;
   unsigned long  m_ulAddress;
   unsigned long  m_ulValue;
   
// Dialog Data
	//{{AFX_DATA(CAddItem)
	enum { IDD = IDD_ADD_ITEM };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddItem)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddItem)
	afx_msg void OnHelpTopic();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDITEM_H__D6FDB71F_F1E9_43DD_9610_E3EBFDE9FB56__INCLUDED_)


