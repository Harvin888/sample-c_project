/****************************************************************************
     Module  : Config.h
     Engineer: Kevin Aherne
  Description: Description of basic processor configuration used by PathFinder

Date           Initials    Description
03-DEC-1999    KA          Initial
03-JUL-2000    SC          Added bEbuRegInit to _TriCoreSpecific
03-MAR-2004    PL          Added support for eval version
11-Mar-2004    D.N.        removed simple types (enums) to stypes.h
23-Mar-2004    MOH         Added proc config enumerated typedef for MPC5500
14-Mar-2008    SPC         Added support to set EJTAG Freq in KHz. 
29-Apr-2008    JG          Moved TyPowerCore to /pathfndr/global/globenum.h 
                           to fix Linux compilation error
12-Aug-2010	   SJ		   Now not exporting the function GetUserRegisterSettings()
****************************************************************************/

#ifndef config_h__
#define config_h__

// Include stypes.h if it has not already been done so
//#if !defined(GLOBAL_CONFIG_H__114581A_BB12_99AB_7712_061334466__INCLUDED_)

//#endif
#define MAX_NUM_CUSTOM_VIEWS 0x20
                    
#define MAX_INIT_INFO_ENTRIES 0x20
#define MAX_JTAG_FREEQ_KHZ 100000
#define  MAX_STRING_LENGTH   81     /* including the '\0' */

#ifndef MAX_PATH
#define MAX_PATH    260 /* max. length of full pathname */
#endif



#define CONFIG_FILE     "ARM.cfg"
#define FAMILY_FILE     "Famlyarm.cfg"
#define DEVICES_FILE    "DevList.dat"
/*** Comment out the following lines if not in use ***/
//#define PATHFINDER_EVALUATION_VERSION 1
//#define NETCHIP_BOARD_RELEASE     1
//#define OKI_ML675K_RELEASE            1
/*** ***/



// structure defining current processor configuration
#define MAX_DEVICE_CONFIG_BYTES  0x20
   


typedef struct
{
   char				szAction[10];
   char				szVariableName[MAX_STRING_LENGTH];
   uint32			adVariableAddress;
   uint32			adVariableSize;
   unsigned long	ulVariableValue;
}_UserSpecific;

#define MAX_USER_VARS               0x100


typedef struct          
{ 
   

   ulong             ulUserSpecificVariablesCount;

   _UserSpecific     pUserSpecificVariables[MAX_USER_VARS];

} ProcessorConfig;



ProcessorConfig* GetUserRegisterSettings(void);

// Put a bound on the max number of Peripheral Devices you can list
#define MAX_NUM_PERIPHERAL_DEVICES  20

#endif // config_h__
