/****************************************************************************
     Module  : TargetResetDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CTargetResetDlg class
Date           Initials    Description
27-Jul-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#ifndef _TARGET_RESET_DLG_H_
#define _TARGET_RESET_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OpellaConfigPageBase.h"

class COpellaConfig;

/****************************************************************************
     Function: CTargetResetDlg
     Engineer: Jiju George T
  Description: Handles Target Reset Page operations
Date           Initials    Description
27-Jul-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/
class CTargetResetDlg : public COpellaConfigPageBase
{
// Construction
public:
    void UpdateUI();
    void InitializeUIData ( COpellaConfig &OpellaConfig );
    CTargetResetDlg( COpellaConfig &OpellaConfig, CDeviceDlg &DevicePage );   // standard constructor
    ~CTargetResetDlg();
    bool ApplyChanges( COpellaConfig &OpellaConfig );

// Dialog Data
    //{{AFX_DATA(CTargetResetDlg)
    enum { IDD = IDD_TARGET_RESET_DLG };
    CComboBox   m_StartupDelayCombo;
    int     m_nTargetResetMode;
    BOOL    m_bSetDebugRequest;
    BOOL    m_bUsenTRST;
    //}}AFX_DATA

	CDeviceDlg &m_DeviceDlg;

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTargetResetDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(CTargetResetDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnNoReset();
    afx_msg void OnResetAndDelay();
    afx_msg void OnResetAndHalt();
    afx_msg void OnHotPlug();
    afx_msg void OnSelchangeStartupDelay();
	afx_msg void OnCoreOnly();
	afx_msg void OnCoreAndPeripherals();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    void FillStartupDelayCombo();

private:
    void EnableDisableControls();
    CString GetDelayString( int nDelay );
    COpellaConfig &m_OpellaConfig;
    int m_nHardResetDelay;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _TARGET_RESET_DLG_H_
