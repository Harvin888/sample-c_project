/****************************************************************************
     Module  : DeviceDlg.cpp  This file contains the implementation of CDeviceDlg class.
     Engineer: Jiju George T
  Description: This file contains the implementation of CDeviceDlg class.
Date           Initials    Description
24-Jul-2007      JG          Initial
13-Sep-2007      SJ          Added function for getting the debugger name
21-Feb-2008      SJ          Added Support for Diagnostics
27-Feb-2008      SPC         Added Support for Load/Save config options.
31-Mar-2008		 SJ			 Solved problems in displaying multiple cores 
05-May-2010      JCK         Added processor selection support. 
12-Aug-2010	     SJ		     Modified to avoid memory leaks.
****************************************************************************/
#include "stdafx.h"
#include "OpellaConfig.h"
#include "DeviceDlg.h"
#include "FamilyDatabase.h"
#include "DriverMgr.h"

#define CORE_PREFIX _T("TAP - ")
#define DEFAULT_IR_LENGTH 4
#define DEFAULT_NUM_CORES 1
#define NUM_ROWS_DISPLAYED 5
#define MAX_STRING_SIZE    0xFF
#define EOS            '\0'

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Global arrays of 5 multi core control IDs
unsigned long g_ulTypeCheckIDs  [] = { IDC_TYPE_CHECK_0  ,IDC_TYPE_CHECK_1  ,
                                       IDC_TYPE_CHECK_2  ,IDC_TYPE_CHECK_3  ,
                                       IDC_TYPE_CHECK_4  };
unsigned long g_ulWidthComboIDs [] = { IDC_WIDTH_COMBO_0 , IDC_WIDTH_COMBO_1 ,
                                       IDC_WIDTH_COMBO_2 ,IDC_WIDTH_COMBO_3 ,
                                       IDC_WIDTH_COMBO_4 };
unsigned long g_ulNameStaticIDs [] = { IDC_NAME_STATIC_0 ,IDC_NAME_STATIC_1 ,
                                       IDC_NAME_STATIC_2 ,IDC_NAME_STATIC_3 ,
                                       IDC_NAME_STATIC_4 };
unsigned long g_ulTypeStaticIDs [] = { IDC_TYPE_STATIC_0 ,IDC_TYPE_STATIC_1 ,
                                       IDC_TYPE_STATIC_2 ,IDC_TYPE_STATIC_3 ,
                                       IDC_TYPE_STATIC_4 };
unsigned long g_ulWidthStaticIDs[] = { IDC_WIDTH_STATIC_0,IDC_WIDTH_STATIC_1,
                                       IDC_WIDTH_STATIC_2,IDC_WIDTH_STATIC_3,
                                       IDC_WIDTH_STATIC_4};

/****************************************************************************
     Function: Constructor
     Engineer: Jiju George T
     Input   : OpellaConfig - contains the configuration options
     Output  : void
  Description: 
Date           Initials    Description
24-Jul-2007      JG          Initial
27-Feb-2008      SPC         Initializations are changed to another function.
27-May-2008			 JCK			Added Device Register settings 	
****************************************************************************/
CDeviceDlg::CDeviceDlg(COpellaConfig &OpellaConfig) :
    COpellaConfigPageBase( CDeviceDlg::IDD ),
    m_OpellaConfig( OpellaConfig ),
    m_nIndexOfCoreDisplayedOnTop( 0 ),
    m_nSelectedCore( 0 )
{
    //{{AFX_DATA_INIT(CDeviceDlg)
    m_nEndian = -1;
	m_RegFileIndex = 0;
    //}}AFX_DATA_INIT

    // To Remove Help button from parent property sheet
    m_psp.dwFlags &= (~PSP_HASHELP);
    InitializeUIData(m_OpellaConfig);
}

/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: 
Date           Initials    Description
24-Jul-2007      JG          Initial
****************************************************************************/
CDeviceDlg::~CDeviceDlg()
{
    // Nothing to do
}

/****************************************************************************
     Function: DoDataExchange()
     Engineer: Jiju George T
     Input   : pDX  - Holds the exchange information
     Output  : void
  Description: Does the data exchange between controls and corresponding member variables.
Date           Initials    Description
24-Jul-2007       JG         Initial
27-May-2008			 JCK			Added Device Register settings
****************************************************************************/
void CDeviceDlg::DoDataExchange(CDataExchange* pDX)
{
    CResizablePage::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDeviceDlg)
	DDX_Control(pDX, IDC_DEVICE_COMBO, m_DeviceCombo);
    DDX_Control(pDX, IDC_SCROLLBAR, m_CoreScrollBar);
    DDX_Control(pDX, IDC_DEVICE_COUNT_COMBO, m_DeviceCountCombo);
    DDX_Control(pDX, IDC_SEL_CORE_COMBO, m_SelCoreCombo);
    DDX_Control(pDX, IDC_CORELIST_COMBO, m_CoreListCombo);
    DDX_Radio(pDX, IDC_BIGENDIAN_RADIO, m_nEndian);
	DDX_CBIndex(pDX, IDC_DEVICE_COMBO, m_RegFileIndex);
	DDX_Control(pDX, IDC_CORESIGHT_CONFIG, m_CoresightButton);
	
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDeviceDlg, COpellaConfigPageBase)
    //{{AFX_MSG_MAP(CDeviceDlg)
    ON_CBN_SELCHANGE(IDC_DEVICE_COUNT_COMBO, OnSelchangeDeviceCountCombo)
    ON_BN_CLICKED(IDC_TYPE_CHECK_0, OnTypeCheck0)
    ON_BN_CLICKED(IDC_TYPE_CHECK_1, OnTypeCheck1)
    ON_BN_CLICKED(IDC_TYPE_CHECK_2, OnTypeCheck2)
    ON_BN_CLICKED(IDC_TYPE_CHECK_3, OnTypeCheck3)
    ON_BN_CLICKED(IDC_TYPE_CHECK_4, OnTypeCheck4)
    ON_CBN_SELCHANGE(IDC_WIDTH_COMBO_0, OnSelchangeWidthCombo0)
    ON_CBN_SELCHANGE(IDC_WIDTH_COMBO_1, OnSelchangeWidthCombo1)
    ON_CBN_SELCHANGE(IDC_WIDTH_COMBO_2, OnSelchangeWidthCombo2)
    ON_CBN_SELCHANGE(IDC_WIDTH_COMBO_3, OnSelchangeWidthCombo3)
    ON_CBN_SELCHANGE(IDC_WIDTH_COMBO_4, OnSelchangeWidthCombo4)
    ON_WM_VSCROLL()
    ON_CBN_SELCHANGE(IDC_SEL_CORE_COMBO, OnSelchangeSelCoreCombo)
    ON_CBN_SELCHANGE(IDC_CORELIST_COMBO, OnSelchangeCorelistCombo)
	ON_BN_CLICKED(IDC_BUTTON_USER_REG_SETTINS, OnUserRegisterSettings)
	ON_CBN_SELCHANGE(IDC_DEVICE_COMBO, OnSelchangeDeviceCombo)
	ON_BN_CLICKED(IDC_CORESIGHT_CONFIG, OnCoresightConfig)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/****************************************************************************
     Function: FillMultiCoreDetails()
     Input   : OpellaConfig - contains the configuration options
     Output  : void
     Engineer: Jiju George T
  Description: This function fills the configuration details of each core.
Date           Initials    Description
24-Jul-2007       JG          Initial
27-Feb-2008       SPC         Added an argument for passing OpellaConfig
****************************************************************************/
void CDeviceDlg::FillMultiCoreDetails( COpellaConfig OpellaConfig )
{
    // Get number of cores from configuration file
    m_tyJtagScanChain.ucNumJtagNodes = OpellaConfig.GetNumberOfCores();
    // Get configuration details of each core
    for( int nIndex = 0; nIndex < m_tyJtagScanChain.ucNumJtagNodes; ++nIndex )
    {
        m_tyJtagScanChain.ptyJtagNode[nIndex].bARMCore = OpellaConfig.IsARMCore(nIndex);
        m_tyJtagScanChain.ptyJtagNode[nIndex].ucDeviceNumber = nIndex;
        m_tyJtagScanChain.ptyJtagNode[nIndex].ucIRLength = OpellaConfig.GetIRLength(nIndex);
    }
}

/****************************************************************************
     Function: OnInitDialog()
     Engineer: Jiju George T
     Input   : void
     Output  : BOOL - controls the focus on control
  Description: Called when this page is shown for first time.
               Does the initialization job.
Date           Initials    Description
24-Jul-2007       JG          Initial
27-Feb-2008       SPC         UI Updates are doing in another function and 
                              it will call on OnShowWindow function.
****************************************************************************/
BOOL CDeviceDlg::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	CString DebuggerName = GetDebuggerName();
	if(!(DebuggerName.Compare("PFARMXD")))
	{
		m_DeviceCombo.EnableWindow(TRUE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

/****************************************************************************
     Function: FillDeviceList()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Gets the list of devices from family database and populates the combo.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CDeviceDlg::FillDeviceList()
{
    CStringArray CoreList;
    
    m_CoreListCombo.ResetContent();

    CFamilyDatabase::GetInstance().GetListOfDevices( CoreList );
    int nNumberOfCores = CoreList.GetSize();
    for ( int nIndex = 0; nIndex < nNumberOfCores ; ++nIndex )
    {
        m_CoreListCombo.AddString( CoreList[nIndex] );
    }

    // Select the selected core
    int nSelIndex =  m_CoreListCombo.FindStringExact( 0, m_csSelectedDevice );
    if( CB_ERR != nSelIndex )
    {
        m_CoreListCombo.SetCurSel( nSelIndex );
    }
}

/****************************************************************************
     Function: FillSelectedCoreList()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Get the number of JTAG Nodes and add same number of TAPs.
Date           Initials    Description
24-Jul-2007       JG          Initial
11-Feb-2010		  JCK		  Added support for Coresight devices
****************************************************************************/
void CDeviceDlg::FillSelectedCoreList()
{
    m_SelCoreCombo.ResetContent();

    int nComboItemIndex = 0;

    // Fill the Selected core list with number of cores
    for ( int nIndex=0; nIndex < m_tyJtagScanChain.ucNumJtagNodes; ++nIndex )
    {
        if( m_tyJtagScanChain.ptyJtagNode[nIndex].bARMCore )
        {
            CString csCoreName;
            csCoreName.Format( _T( "%s%d" ), CORE_PREFIX, nIndex );
            m_SelCoreCombo.AddString( csCoreName );

            // Store the Core Index (TAP Number) associated with the
            // added core so that it can be retrieved back easily during save
            m_SelCoreCombo.SetItemData( nComboItemIndex, nIndex );
            ++nComboItemIndex;
        }
    }

    // Select the selected core number
    int nItemsInCombo = m_SelCoreCombo.GetCount();
    int nSelectedItemIndex = 0;
    for( nComboItemIndex = 0; nComboItemIndex < nItemsInCombo; ++nComboItemIndex )
    {
        if( m_nSelectedCore == m_SelCoreCombo.GetItemData( nComboItemIndex ) )
        {
            nSelectedItemIndex = nComboItemIndex;
            break;
        }
    }
    m_SelCoreCombo.SetCurSel( nSelectedItemIndex );

    // Simulate selection change since the CBN_SELCHANGE event will not triggered
    // during programatic selection change
    OnSelchangeSelCoreCombo();
    
}

/****************************************************************************
     Function: FillCoreCountList()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Fills the list containing possible number of cores in the device.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::FillCoreCountList()
{
    m_DeviceCountCombo.ResetContent();
    for ( int nIndex=1; nIndex <= MAX_CORES_SUPPORTED; ++nIndex )
    {
        CString csDeviceCount;
        csDeviceCount.Format( _T( "%d" ), nIndex );
        m_DeviceCountCombo.AddString( csDeviceCount );
    }

    // Select the number configured in the combo
    CString csSelDeviceCount;
    csSelDeviceCount.Format( _T( "%d" ), m_tyJtagScanChain.ucNumJtagNodes );
    int nSelIndex =  m_DeviceCountCombo.FindStringExact( 0, csSelDeviceCount );
    if( CB_ERR != nSelIndex )
    {
        m_DeviceCountCombo.SetCurSel( nSelIndex );
    }
}

/****************************************************************************
     Function: FillIRLengthCombos()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: This function fills the IR Length(Width) combos with possible
               IR Width values.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::FillIRLengthCombos()
{
    // Fill in the IR lengths in the list boxes...
    (( CComboBox* )GetDlgItem(IDC_WIDTH_COMBO_0))->ResetContent();
    (( CComboBox* )GetDlgItem(IDC_WIDTH_COMBO_1))->ResetContent();
    (( CComboBox* )GetDlgItem(IDC_WIDTH_COMBO_2))->ResetContent();
    (( CComboBox* )GetDlgItem(IDC_WIDTH_COMBO_3))->ResetContent();
    (( CComboBox* )GetDlgItem(IDC_WIDTH_COMBO_4))->ResetContent();

    CString csValue;
    for ( int nIndex=0; nIndex < 64; ++nIndex )
    {
        csValue.Format( _T( "%d" ), nIndex + 1 );
        (( CComboBox* )GetDlgItem( IDC_WIDTH_COMBO_0 ))->AddString( csValue );
        (( CComboBox* )GetDlgItem( IDC_WIDTH_COMBO_1 ))->AddString( csValue );
        (( CComboBox* )GetDlgItem( IDC_WIDTH_COMBO_2 ))->AddString( csValue );
        (( CComboBox* )GetDlgItem( IDC_WIDTH_COMBO_3 ))->AddString( csValue );
        (( CComboBox* )GetDlgItem( IDC_WIDTH_COMBO_4 ))->AddString( csValue );
    }
}

/****************************************************************************
     Function: UpdateMultiCoreDisplay()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: This function updates the multi core table display based on the number of
               cores and configuration of each core..
Date           Initials    Description
24-Jul-2007       JG          Initial
31-Mar-2008		  SJ		  Disabling checkbox for a single ARM core commented out
****************************************************************************/
void CDeviceDlg::UpdateMultiCoreDisplay()
{
    // If there is only 1 ARM Core left then do not allow
    // user to uncheck the ARM Core type since we need at least 1 ARM Core
    // So first count the number of ARM Cores
    /*int nARMCoreCount = 0;
    int nLastArmCoreIndex = 0;
    for ( int nIndex=0; nIndex < m_tyJtagScanChain.ucNumJtagNodes; ++nIndex )
    {
        if( m_tyJtagScanChain.ptyJtagNode[nIndex].bARMCore )
        {
            ++nARMCoreCount;
            nLastArmCoreIndex = nIndex;
        }
    }*/

    // Now update display
    for (int nControlIndex=0;nControlIndex<NUM_ROWS_DISPLAYED;++nControlIndex)
    {
        int nIndexOfDisplayedCore = m_nIndexOfCoreDisplayedOnTop + nControlIndex;
        if( nIndexOfDisplayedCore < m_tyJtagScanChain.ucNumJtagNodes )
        {
            // Update the configuration to controls
            UpdateMultiCoreRow(nControlIndex);

            BOOL bEnableARMCheck = TRUE;

            /*if( nARMCoreCount <= 1 && nIndexOfDisplayedCore == nLastArmCoreIndex )
            {
                // Disable check box
                bEnableARMCheck = FALSE;
            }*/

            // Show row of controls
            ShowMutiCoreRow( nControlIndex ,TRUE, bEnableARMCheck );
        }
        else
        {
            ShowMutiCoreRow( nControlIndex ,FALSE, TRUE );
        }
    }


}

/****************************************************************************
     Function: ShowMutiCoreRow()
     Engineer: Jiju George T
     Input   : nControlIndex   - Index of the multi core row to be shown
               bShow           - if true, shows the controls in the row.
                             .   Else hides the controls in the row.
               bEnableARMCheck - if true the check box will be enables.
                                 Else will be disabled.
     Output  : void
  Description: Show/Hide controls in the given multi core row
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::ShowMutiCoreRow( int nControlIndex ,BOOL bShow,
                                  BOOL bEnableARMCheck )
{
    GetDlgItem( g_ulNameStaticIDs[nControlIndex] )->ShowWindow( bShow );
    GetDlgItem( g_ulTypeCheckIDs[nControlIndex] )->ShowWindow( bShow );
    GetDlgItem( g_ulTypeStaticIDs[nControlIndex] )->ShowWindow( bShow );
    GetDlgItem( g_ulWidthComboIDs[nControlIndex] )->ShowWindow( bShow );
    GetDlgItem( g_ulWidthStaticIDs[nControlIndex] )->ShowWindow( bShow );

    GetDlgItem( g_ulTypeCheckIDs[nControlIndex] )->EnableWindow( bEnableARMCheck );
}

/****************************************************************************
     Function: UpdateMultiCoreRow()
     Engineer: Jiju George T
     Input   : nControlIndex - Index of the multi core row to be updated
     Output  : void
  Description: Updated the controls in the given multi core row with its corresponding
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::UpdateMultiCoreRow( int nControlIndex )
{
    // Show device name / core name
    int nCoreIndex = m_nIndexOfCoreDisplayedOnTop + nControlIndex;
    CString csDeviceName;
    csDeviceName.Format( _T( "%s%d" ), CORE_PREFIX,
            m_tyJtagScanChain.ptyJtagNode[nCoreIndex].ucDeviceNumber );
    GetDlgItem( g_ulNameStaticIDs[nControlIndex] )->SetWindowText( csDeviceName );

    // Update IR Width
    (( CComboBox* )GetDlgItem( g_ulWidthComboIDs[nControlIndex] ))->SetCurSel(
                     m_tyJtagScanChain.ptyJtagNode[nCoreIndex].ucIRLength-1 );

    // Check for ARM Core
    if( m_tyJtagScanChain.ptyJtagNode[nCoreIndex].bARMCore )
    {
        // Check Arm Core check box
        (( CButton* )GetDlgItem( g_ulTypeCheckIDs[nControlIndex] ))->SetCheck( TRUE );

        // Disable IR Width
/*
//         GetDlgItem( g_ulWidthStaticIDs[nControlIndex] )->EnableWindow( FALSE );
//         GetDlgItem( g_ulWidthComboIDs[nControlIndex] )->EnableWindow( FALSE );
*/
        // Enable IR Width
        GetDlgItem( g_ulWidthStaticIDs[nControlIndex] )->EnableWindow( TRUE );
        GetDlgItem( g_ulWidthComboIDs[nControlIndex] )->EnableWindow( TRUE );
    }
    else
    {
        // Un Check Arm Core check box
        (( CButton* )GetDlgItem( g_ulTypeCheckIDs[nControlIndex] ))->SetCheck( FALSE );

        // Enable IR Width
        GetDlgItem( g_ulWidthStaticIDs[nControlIndex] )->EnableWindow( TRUE );
        GetDlgItem( g_ulWidthComboIDs[nControlIndex] )->EnableWindow( TRUE );
    }
}

/****************************************************************************
     Function: GetDebuggerName()
     Engineer: Suraj S
     Input   : void
     Output  : CString - Debugger Name
  Description: Gets the name of the debugger and return the same
Date           Initials    Description
13-Sep-2007      SJ           Initial
27-May-2008		  JCK				Updated for PFARMXD support 
****************************************************************************/
CString CDeviceDlg::GetDebuggerName()
{

   char szRootName[255] = {0};
   char m_szDebuggerName[0x10] = {0};

   if (::GetModuleFileName( NULL, szRootName, MAX_STRING_SIZE )!= 0)
   {
      char *pszPtr = NULL;
      (void)_strupr(szRootName); //string to upper case
      pszPtr=strstr(szRootName,"PFARM.EXE"); //find the position of this string
      strcpy (&m_szDebuggerName[0],"UNKNOWN"); // Initialise
      if (pszPtr)
      strcpy (&m_szDebuggerName[0],"PATHFINDER");
      else
      {
         if(!pszPtr)
         pszPtr=strstr(szRootName,"PFARMDB.EXE"); //find the position of this string
         if (pszPtr)
         strcpy (&m_szDebuggerName[0],"PATHFINDER");
			else
         {
            if(!pszPtr)
            pszPtr=strstr(szRootName,"PFARMXD.EXE"); //find the position of this string
            if (pszPtr)
            strcpy (&m_szDebuggerName[0],"PFARMXD");
					else
					{
					if(!pszPtr)
					   pszPtr=strstr(szRootName,"PFARMXDDB.EXE"); //find the position of this string
					if (pszPtr)
					   strcpy (&m_szDebuggerName[0],"PFARMXD");
               else
               {
                  if(!pszPtr)
                  pszPtr=strstr(szRootName,"ADW.EXE"); //find the position of this string
                  if (pszPtr)
                  strcpy (&m_szDebuggerName[0],"ADW");
                  else
                  {
                     if(!pszPtr)
                     pszPtr=strstr(szRootName,"AXD.EXE"); //find the position of this string
                     if (pszPtr)
                     strcpy (&m_szDebuggerName[0],"AXD");
                     else
                     {
                        if(!pszPtr)
                        pszPtr=strstr(szRootName,"IARIDE.EXE"); //find the position of this string
                        if (pszPtr)
                        strcpy (&m_szDebuggerName[0],"IAR");
                        else
                        {
                           if(!pszPtr)
                           pszPtr=strstr(szRootName,"IDE.EXE"); //find the position of this string
                           if (pszPtr)
                           strcpy (&m_szDebuggerName[0],"IDE");
                           else
                           {
                              // Some thing else - just use the executable name
                              pszPtr = strrchr(szRootName,'\\');
                              if (pszPtr)
                              {
                                 char * pcChar = strrchr(pszPtr,'.');
                                 if(pcChar)
                                 *pcChar = EOS; // Remove the extension
                                 pszPtr++;
                                 strncpy (&m_szDebuggerName[0],pszPtr,sizeof(m_szDebuggerName));
                                 // Ensure NULL Termination
                                 m_szDebuggerName[sizeof(m_szDebuggerName) - 0x1] = EOS;
										}
									}
                        }
                     }
                  }
               }
            }
         }
      }
      if (pszPtr)
      {
         pszPtr-=1; //point to previous char to get rid of the "\" in the path
         *pszPtr='\0';
      }
   }
   m_csDebuggerName = m_szDebuggerName;
   return m_csDebuggerName;
}

/****************************************************************************
     Function: FillDeviceRegList()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: This function fills the Device register combos with possible
               Register file names.
Date          Initials    Description
28-May-2008       JCK          Initial
****************************************************************************/
void CDeviceDlg::FillDeviceRegList()
{
	char         szRegFile[_MAX_PATH];
   int          iNumDevices;
	char szRootName[255];
	CArray<CString, CString>      DeviceEntries; 
	CString      sDevicesFile;
	CString      sSectionHeader;
	unsigned long i;

	
	if (::GetModuleFileName( NULL, szRootName, MAX_STRING_SIZE )!= 0)
      {
		*(strrchr(szRootName,'\\'))='\0';
		strcat(szRootName,"\\Devices\\DevList.dat");
		sDevicesFile=CString(szRootName);
	   }
   strcpy(szRegFile, "None");
   DeviceEntries.Add(szRegFile);
   m_DeviceCombo.AddString(szRegFile);
	m_DeviceCombo.SetCurSel(m_RegFileIndex);

   // now add the list of names of Peripheral Devices
   iNumDevices=(int)GetPrivateProfileInt(DEVICES_FILE,"Entries",0, sDevicesFile);
   for (i=0;i < iNumDevices; i++)
      {
      sSectionHeader = "";
      sSectionHeader.Format("%s%02d","Dev",i+1);
      // get Device regfile Name
      if (!(GetPrivateProfileString(sSectionHeader,"DevName","", szRegFile,(DWORD)_MAX_PATH,sDevicesFile)))
         break;     

      DeviceEntries.Add(szRegFile);
		m_DeviceCombo.AddString(szRegFile);
      }
}

/****************************************************************************
     Function: OnSelchangeDeviceCountCombo()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when selection of device count (core count) changes.
Date           Initials    Description
24-Jul-2007       JG          Initial
31-Mar-2008		  SJ		  The bug in displaying multiple core solved
****************************************************************************/
void CDeviceDlg::OnSelchangeDeviceCountCombo()
{
    m_tyJtagScanChain.ucNumJtagNodes = m_DeviceCountCombo.GetCurSel() + 1;

    //If there is only one device selected then it must be an ARM core
	if(m_tyJtagScanChain.ucNumJtagNodes == 0x1)
	{
		m_tyJtagScanChain.ptyJtagNode[0].bARMCore = TRUE;
      m_tyJtagScanChain.ptyJtagNode[0].ucIRLength = DEFAULT_IR_LENGTH;
	}

	// Update selected core combo with selected bumber of cores
    FillSelectedCoreList();

    // Now setup the range of the scrollbar
    UpdateCoreScroll();

    // Reset top row core index since we reset the scroll thumb to start
    m_nIndexOfCoreDisplayedOnTop = 0;

    UpdateMultiCoreDisplay();
}

/****************************************************************************
     Function: OnTypeCheck0()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when ARM Core check box in 4th multi core row is checked/unchecked
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007      JG          Initial
****************************************************************************/
void CDeviceDlg::OnTypeCheck0()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 0].bARMCore =
        ((CButton *)GetDlgItem(IDC_TYPE_CHECK_0))->GetCheck() != 0;
/*
//     if(m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 0].bARMCore)
//        m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 0].ucIRLength = DEFAULT_IR_LENGTH;
*/
    UpdateMultiCoreDisplay();
    FillSelectedCoreList();
}

/****************************************************************************
     Function: OnTypeCheck1()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when ARM Core check box in 4th multi core row is checked/unchecked
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CDeviceDlg::OnTypeCheck1()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 1].bARMCore =
        ((CButton *)GetDlgItem(IDC_TYPE_CHECK_1))->GetCheck() != 0;
/*
//     if(m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 1].bARMCore)
//        m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 1].ucIRLength = DEFAULT_IR_LENGTH;
*/
    UpdateMultiCoreDisplay();
    FillSelectedCoreList();
}

/****************************************************************************
     Function: OnTypeCheck2()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when ARM Core check box in 4th multi core row is checked/unchecked
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnTypeCheck2()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 2].bARMCore =
        ((CButton *)GetDlgItem(IDC_TYPE_CHECK_2))->GetCheck() != 0;

/*
//     if(m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 2].bARMCore)
//        m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 2].ucIRLength = DEFAULT_IR_LENGTH;
*/
    UpdateMultiCoreDisplay();
    FillSelectedCoreList();
}

/****************************************************************************
     Function: OnTypeCheck3()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when ARM Core check box in 4th multi core row is checked/unchecked
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnTypeCheck3()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 3].bARMCore =
        ((CButton *)GetDlgItem(IDC_TYPE_CHECK_3))->GetCheck() != 0;
/*
//     if(m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 3].bARMCore)
//        m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 3].ucIRLength = DEFAULT_IR_LENGTH;
*/
    UpdateMultiCoreDisplay();
    FillSelectedCoreList();
}

/****************************************************************************
     Function: OnTypeCheck4()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when ARM Core check box in 4th multi core row is checked/unchecked
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnTypeCheck4()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 4].bARMCore =
        ((CButton *)GetDlgItem(IDC_TYPE_CHECK_4))->GetCheck() != 0;
/*
//     if(m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 4].bARMCore)
//        m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 4].ucIRLength = DEFAULT_IR_LENGTH;
*/
    UpdateMultiCoreDisplay();
    FillSelectedCoreList();
}

/****************************************************************************
     Function: OnSelchangeWidthCombo0()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when selection of IR Width combo in 0th multi core row is changed
               Updates the multi core display accordingly..
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeWidthCombo0()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 0].ucIRLength =
        ((CComboBox *)GetDlgItem(IDC_WIDTH_COMBO_0))->GetCurSel() + 1;
}


/****************************************************************************
     Function: OnSelchangeWidthCombo1()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when selection of IR Width combo in 1st multi core row is changed
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeWidthCombo1()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 1].ucIRLength =
        ((CComboBox *)GetDlgItem(IDC_WIDTH_COMBO_1))->GetCurSel() + 1;
}

/****************************************************************************
     Function: OnSelchangeWidthCombo2()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when selection of IR Width combo in 2nd multi core row is changed
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeWidthCombo2()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 2].ucIRLength =
        ((CComboBox *)GetDlgItem(IDC_WIDTH_COMBO_2))->GetCurSel() + 1;
}

/****************************************************************************
     Function: OnSelchangeWidthCombo3()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when selection of IR Width combo in 3rd multi core row is changed
               Updates the multi core display accordingly..
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeWidthCombo3()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 3].ucIRLength =
        ((CComboBox *)GetDlgItem(IDC_WIDTH_COMBO_3))->GetCurSel() + 1;
}

/****************************************************************************
     Function: OnSelchangeWidthCombo4()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when selection of IR Width combo in 4th multi core row is changed
               Updates the multi core display accordingly.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeWidthCombo4()
{
    m_tyJtagScanChain.ptyJtagNode[m_nIndexOfCoreDisplayedOnTop + 4].ucIRLength =
        ((CComboBox *)GetDlgItem(IDC_WIDTH_COMBO_4))->GetCurSel() + 1;
}

/****************************************************************************
     Function: UpdateCoreScroll()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Updates the multi core scrollbar with min/max range based on number of cores.
               Resets the scrollbar position to start
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDeviceDlg::UpdateCoreScroll()
{
    if ( m_tyJtagScanChain.ucNumJtagNodes > NUM_ROWS_DISPLAYED )
    {
        m_CoreScrollBar.SetScrollRange( 0,
                         m_tyJtagScanChain.ucNumJtagNodes - NUM_ROWS_DISPLAYED );
        m_CoreScrollBar.SetScrollPos( 0 );
    }
    else
    {
        m_CoreScrollBar.SetScrollRange( 0, 0 );
    }
}

/****************************************************************************
     Function: OnVScroll()
       Input : nSBCode    - Specifies a scroll-bar code that indicates a
                            scrolling request by the user
               nPos       - Contains the current scroll-box position
               pScrollBar - A pointer to the scrollbar control
       Output: void
     Engineer: Jiju George T
  Description: This method is called by the framework when the user clicks the
               multi core scroll bar.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CDeviceDlg::OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
    // Check if scrolling is enabled
    if ( m_tyJtagScanChain.ucNumJtagNodes > NUM_ROWS_DISPLAYED )
    {
        switch ( nSBCode )
        {
            case SB_BOTTOM:
            case SB_PAGEDOWN:
            case SB_LINEDOWN:
            {
                if ( m_nIndexOfCoreDisplayedOnTop <
                     m_tyJtagScanChain.ucNumJtagNodes - NUM_ROWS_DISPLAYED )
                {
                    m_nIndexOfCoreDisplayedOnTop++;
                    pScrollBar->SetScrollPos( m_nIndexOfCoreDisplayedOnTop );
                    UpdateMultiCoreDisplay();
                }
                break;
            }
            case SB_TOP:
            case SB_PAGEUP:
            case SB_LINEUP:
            {
                if ( m_nIndexOfCoreDisplayedOnTop > 0)
                {
                    m_nIndexOfCoreDisplayedOnTop--;
                    pScrollBar->SetScrollPos( m_nIndexOfCoreDisplayedOnTop );
                    UpdateMultiCoreDisplay();
                }
                break;
            }
            default:
                break;
        }
    }

    CResizablePage::OnVScroll( nSBCode, nPos, pScrollBar );
}

/****************************************************************************
     Function: ApplyChanges()
     Engineer: Jiju George T
     Input   : OpellaConfig - Configuration object to save configurations
     Output  : bool -  Return false if some required information is not
               filled by user (In this case notification about
               the error should be given to user). Else return true.
  Description: This function is called by the parent when user presses Apply.OK button.
               All the user configuration in the page should be saved to the passed
               COpellaConfig object.
Date           Initials    Description
24-Jul-2007       JG         Initial
31-Mar-2008		  SJ		 Display message if no ARM core is selected
27-May-2008		  JCK		 Added Device Register settings 
24-Nov-2009		  JCK		 Architecture version is added is updated to cnf file	
11-Mar-2010       JCK        Added supoort for coresight devices.  
05-May-2010       JCK        Added processor selection support.  
****************************************************************************/
bool CDeviceDlg::ApplyChanges( COpellaConfig &OpellaConfig )
{
	CString csArchVer;
    // Exchange data from controls to members
    if( IsWindow( m_hWnd ))
    {
        UpdateData();
    }

	CFamilyDatabase::GetInstance().CheckArmArchitVer(&csArchVer, m_csSelectedDevice);

	// Save Selected Architecture
    OpellaConfig.SetArchVersion( csArchVer );

    // Save Debug Base address    
	bool bValidData = true;
	
	unsigned long ulDbgBaseAddr;
	bValidData = CHexUtils::GetValueFromHexString( 	CoresightConfigDlg.m_DbgBaseAddr,
													ulDbgBaseAddr );
	if( bValidData )
	{
		OpellaConfig.SetDebugBaseAddress(ulDbgBaseAddr);
	}
	else
	{
		MessageBox("Please enter a valid Debug Base address.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
		return false;
    }


    // Save the Debug access port index
	unsigned long ulDbgApIndex;
	bValidData = CHexUtils::GetValueFromHexString( 	CoresightConfigDlg.m_DbgApIndex,
													ulDbgApIndex );
	if( bValidData )
	{
		OpellaConfig.SetDebugApIndex(ulDbgApIndex);
	}
	else
	{
		MessageBox("Please enter a valid Debug AP Index.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
		return false;
    }

    // Save the Memory access port index
	unsigned long ulMemApIndex;
	bValidData = CHexUtils::GetValueFromHexString( 	CoresightConfigDlg.m_MemApIndex,
													ulMemApIndex );
	if( bValidData )
	{
		OpellaConfig.SetMemoryApIndex(ulMemApIndex);
	}
	else
	{
		MessageBox("Please enter a valid Memory AP Index.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
		return false;
    }

	// Save the UseMemApforMemAccess parameter
	unsigned long ulUseMemApforMemAccess;
	bValidData = CHexUtils::GetValueFromHexString( 	CoresightConfigDlg.m_UseMemApMemAccess,
		ulUseMemApforMemAccess );
	if( bValidData )
	{
		OpellaConfig.SetUseMemApForMemAccess(ulUseMemApforMemAccess);
	}

    // Save the Cache Invalidation workaround parameter.
    OpellaConfig.SetCacheInvalidationParam(m_EnableCacheInvalidationNeeded);

    // Save the Coresight Complaint or not.
    OpellaConfig.SetCoresightCompliant(m_EnableCoreSight);

    // Save Selected Device
    //OpellaConfig.SetSelectedDevice( m_csSelectedDevice );
	CFamilyDatabase::GetInstance().GetInternalName( &m_csInternalName, m_csSelectedDevice );
	OpellaConfig.SetSelectedDevice( m_csInternalName );
	OpellaConfig.SetProcessorName(m_csSelectedDevice);	

	BOOL bTICore;

	bTICore = CFamilyDatabase::GetInstance().CheckTICore(m_csSelectedDevice);
	OpellaConfig.SetTICore(bTICore);
	
	if (TRUE == bTICore)
	{
		/* Set the secondary port number */
		CString csSencodaryPortNum;
		unsigned long ulSecPortNum;
		
		CFamilyDatabase::GetInstance().GetSecondaryPortNum( &csSencodaryPortNum, m_csSelectedDevice );		
		CHexUtils::GetValueFromHexString( csSencodaryPortNum, ulSecPortNum );
		OpellaConfig.SetTISubPortNumber( ulSecPortNum );

		/* DAP PC is supported or not. */
		CString csDapPcSupport;

		CFamilyDatabase::GetInstance().GetDapPcSupport( &csDapPcSupport, m_csSelectedDevice );
		
		if ( "1" == csDapPcSupport )
		{
			OpellaConfig.SetDapPcSupport( TRUE );
		}
		else
		{
			OpellaConfig.SetDapPcSupport( FALSE );
		}
		
	}
    // Save Selected Core
    OpellaConfig.SetSelectedCore( m_nSelectedCore );

    // Save Debugger Name
    OpellaConfig.SetDebuggerName( m_csDebuggerName );

    // Save multi core details
    // Also workout the Shift IR /DR Pre/Post Clocks based on selected core details
    unsigned char  ucShiftIRPreTCKs          = 0x0;
    unsigned char  ucShiftIRPostTCKs         = 0x0;
    unsigned char  ucShiftDRPreTCKs          = 0x0;
    unsigned char  ucShiftDRPostTCKs         = 0x0;

    OpellaConfig.SetNumberOfCores( m_tyJtagScanChain.ucNumJtagNodes );
    for( int nCoreIndex=0; nCoreIndex < m_tyJtagScanChain.ucNumJtagNodes;
                                        ++nCoreIndex )
    {
        OpellaConfig.SetCoreType( nCoreIndex,
                m_tyJtagScanChain.ptyJtagNode[nCoreIndex].bARMCore );
        OpellaConfig.SetIRLength( nCoreIndex,
                m_tyJtagScanChain.ptyJtagNode[nCoreIndex].ucIRLength );

        //Work out the post and pre TCK settings for multi-core support.
        //If we are lower down the scan chain than the core we are to debug
        // we are adding post TCK's
        if( m_nSelectedCore > nCoreIndex )
        {
            //Add the length of the current device to the post TCK's
            ucShiftIRPostTCKs += m_tyJtagScanChain.ptyJtagNode[nCoreIndex].ucIRLength;
            //Add one to the DR post TCK's (Devide in bypass)
            ++ucShiftDRPostTCKs;
        }
        //If we are higher up the scan chain than the core we are to debug
        //we are adding pre TCK's
        else if( m_nSelectedCore < nCoreIndex )
        {
            //Add the length of the current device to the pre TCK's
            ucShiftIRPreTCKs += m_tyJtagScanChain.ptyJtagNode[nCoreIndex].ucIRLength;
            //Add one to the DR pre TCK's (Devide in bypass)
            ++ucShiftDRPreTCKs;
        }
        else
        {
            //Do nothing this is the core we are debugging.
        }

    }

   // Now check if there is at least one ARM on the chain....
   for (int i=0; i < m_tyJtagScanChain.ucNumJtagNodes; i++)
      {
      if (m_tyJtagScanChain.ptyJtagNode[i].bARMCore)
         break;
      }

   if (i >= m_tyJtagScanChain.ucNumJtagNodes)
      {
      MessageBox("Please select atleast one ARM core","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
      return false;
      }

    // Save Shift IR /DR Pre/Post Clocks
    OpellaConfig.SetShiftIRPreTCKs( ucShiftIRPreTCKs );
    OpellaConfig.SetShiftIRPostTCKs( ucShiftIRPostTCKs );
    OpellaConfig.SetShiftDRPreTCKs( ucShiftDRPreTCKs );
    OpellaConfig.SetShiftDRPostTCKs( ucShiftDRPostTCKs );

    // Save Endianness
    Endian eEndian = ( Endian )m_nEndian;
    OpellaConfig.SetEndianness(eEndian);
    //Set the device register index only for PFARMXD
	 if(m_DeviceCombo.IsWindowEnabled())		
		OpellaConfig.SetDeviceRegIndex(m_RegFileIndex);
    return true;
}

/****************************************************************************
     Function: OnSelchangeSelCoreCombo()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when user changes the core selection.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeSelCoreCombo()
{
    m_nSelectedCore = m_SelCoreCombo.GetItemData(m_SelCoreCombo.GetCurSel());

}

/****************************************************************************
     Function: OnSelchangeCorelistCombo()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when user changes the device selection.
Date           Initials    Description
24-Jul-2007       JG          Initial
11-Feb-2010		  JCK		  Added support for Coresight devices
05-May-2010       JCK         Added processor selection support. 
****************************************************************************/
void CDeviceDlg::OnSelchangeCorelistCombo()
{
    m_CoreListCombo.GetLBText( m_CoreListCombo.GetCurSel(), m_csSelectedDevice );
	TyJtagScanChain tyJtagScanChain = {0};
	
	CFamilyDatabase::GetInstance().CheckDevicesOnChain(&tyJtagScanChain,m_csSelectedDevice);
	
	if ( tyJtagScanChain.ucNumJtagNodes != 0)
	{
		m_DeviceCountCombo.SetCurSel((tyJtagScanChain.ucNumJtagNodes - 1));
		m_tyJtagScanChain.ucNumJtagNodes = tyJtagScanChain.ucNumJtagNodes;	
		
		for(int i = 0; i < tyJtagScanChain.ucNumJtagNodes; i++)
		{
			m_tyJtagScanChain.ptyJtagNode[i].bARMCore = tyJtagScanChain.ptyJtagNode[i].bARMCore;

			m_tyJtagScanChain.ptyJtagNode[i].ucDeviceNumber = tyJtagScanChain.ptyJtagNode[i].ucDeviceNumber;

			m_tyJtagScanChain.ptyJtagNode[i].ucIRLength = tyJtagScanChain.ptyJtagNode[i].ucIRLength;
		}

		FillSelectedCoreList();

		// Now setup the range of the scrollbar
		UpdateCoreScroll();

		// Reset top row core index since we reset the scroll thumb to start
		m_nIndexOfCoreDisplayedOnTop = 0;

		UpdateMultiCoreDisplay();
	}

    bool bCacheInvalidationNeeded = CFamilyDatabase::GetInstance().CheckCacheInvalidationNeeded(m_csSelectedDevice);

    if (bCacheInvalidationNeeded)
    {	
        m_EnableCacheInvalidationNeeded = TRUE;
	}
	else
	{
        m_EnableCacheInvalidationNeeded = FALSE;
	}

		
	bool bCoresightCompliant = CFamilyDatabase::GetInstance().CheckCoresightCompliant(m_csSelectedDevice);
    
    if (bCoresightCompliant)
    {
        m_CoresightButton.EnableWindow(TRUE);

        m_EnableCoreSight = TRUE;

		CFamilyDatabase::GetInstance().GetCoresightConfigVal( &CoresightConfigDlg.m_DbgBaseAddr, 
															  &CoresightConfigDlg.m_DbgApIndex,
															  &CoresightConfigDlg.m_MemApIndex,
															  &CoresightConfigDlg.m_JtagApIndex,
															  &CoresightConfigDlg.m_UseMemApMemAccess,
															  m_csSelectedDevice);

		CoresightConfigDlg.m_DbgApIndexBackup  = CoresightConfigDlg.m_DbgApIndex;
		CoresightConfigDlg.m_DbgBaseAddrBackup = CoresightConfigDlg.m_DbgBaseAddr;
		CoresightConfigDlg.m_MemApIndexBackup  = CoresightConfigDlg.m_MemApIndex;

		CDriverMgr::GetInstance().SetCoresightParam(CoresightConfigDlg.m_DbgApIndex, 
												CoresightConfigDlg.m_MemApIndex, 
												CoresightConfigDlg.m_JtagApIndex, 
												CoresightConfigDlg.m_DbgBaseAddr);
    }
    else
    {
        m_CoresightButton.EnableWindow(FALSE);

        m_EnableCoreSight = FALSE;
    }
}

/****************************************************************************
     Function: GetSelectedCoreDetails()
     Engineer: Suraj S
     Input   : void
     Output   : CString - Selected Device
  Description: For Diagnostics, the selected device is returned.
Date           Initials    Description
21-Feb-2008       SJ          Initial
****************************************************************************/
CString CDeviceDlg::GetSelectedCoreDetails()
{
    return m_csSelectedDevice;
}

/****************************************************************************
     Function: GetSelectedCoreDetails()
     Engineer: Jeenus C.K.
     Input   : void
     Output   : CString - Selected Device
  Description: For Diagnostics, the selected internal name is returned.
Date           Initials    Description
06-May-2010		JCK			Initial
****************************************************************************/
CString CDeviceDlg::GetSelectedDeviceName()
{
    return m_csInternalName;
}

/****************************************************************************
     Function: GetSelectedCoreNumber()
     Engineer: Suraj S
     Input   : void
     Output   : int - Selected Core Number
  Description: For Diagnostics, the selected core number is returned.
Date           Initials    Description
21-Feb-2008       SJ          Initial
****************************************************************************/
int CDeviceDlg::GetSelectedCoreNumber()
{
    return m_nSelectedCore;
}

/****************************************************************************
     Function: GetJtagScanDetails()
     Engineer: Suraj S
     Input   : void
     Output   : TyJtagScanChain - pointer to multicore structure.
  Description: For Diagnostics, returns the multicore details.
Date           Initials    Description
25-Jun-2008       SJ          Initial
****************************************************************************/
TyJtagScanChain* CDeviceDlg::GetJtagScanDetails()
{
    return &m_tyJtagScanChain;
}

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This function will initialize GUI data variables using the 
               configuration object as argument. This function is called by
               Constructor of the class and base class while loading configuration.
Date           Initials    Description
27-Feb-2008       SPC         Initial
28-May-2008       JCK         Added support for device register
05-May-2010		  JCK		  Added for Device selection support
12-Aug-2010	      SJ		  Modified to avoid memory leaks.
****************************************************************************/
void CDeviceDlg::InitializeUIData(COpellaConfig &OpellaConfig)
{
   CString csDebuggerName;
    // Initialize the Multi core details data structure
    // with default values
    m_tyJtagScanChain.ucNumJtagNodes = DEFAULT_NUM_CORES;
    for(int nIndex=0;nIndex<MAX_CORES_SUPPORTED;++nIndex)
    {
        m_tyJtagScanChain.ptyJtagNode[nIndex].bARMCore = true;
        m_tyJtagScanChain.ptyJtagNode[nIndex].ucDeviceNumber = nIndex;
        m_tyJtagScanChain.ptyJtagNode[nIndex].ucIRLength = DEFAULT_IR_LENGTH;
    }
    
    // Fill the details of selected number of cores
    FillMultiCoreDetails( OpellaConfig );

    InitializeCoresight( OpellaConfig );

    //Set Core display position to the first row
    m_nIndexOfCoreDisplayedOnTop = 0;
    
    // Get Selected Core
    m_nSelectedCore = OpellaConfig.GetSelectedCore();
    
    // Get Selected Device
    //char *szSelectedDevice = OpellaConfig.GetSelectedDevice();
	//char *szSelectedDevice = OpellaConfig.GetProcessorName();
	CString csSelectedDevice;
	OpellaConfig.GetProcessorName(csSelectedDevice.GetBuffer(MAX_STRING_SIZE));
    m_csSelectedDevice = csSelectedDevice;
    //delete[] szSelectedDevice;
    
    // Get Endianess
    Endian eEndian = OpellaConfig.GetEndianness();
    m_nEndian = eEndian;

    // Exchange data to controls if there is a window exists. 
    // It necessary because we are calling UpdateData() in ApplyChanges function. 
    if( IsWindow( m_hWnd ))
    {
        UpdateData( FALSE );
    }

	BOOL bCoresightCompliant = OpellaConfig.IsCoresightCompliant();
	if (bCoresightCompliant)
    {	
        m_EnableCoreSight = TRUE;
    }
    else
    {
        m_EnableCoreSight = FALSE;
    }

	BOOL bCacheInvalidationNeeded = OpellaConfig.GetCacheInvalidationParam();
	if (bCacheInvalidationNeeded)
    {	
        m_EnableCacheInvalidationNeeded = TRUE;
    }
    else
    {
        m_EnableCacheInvalidationNeeded = FALSE;
    }

	 //Get the device register index only for PFARMXD
	 csDebuggerName = GetDebuggerName();
	 
	 if(!(csDebuggerName.Compare("PFARMXD")))
		m_RegFileIndex = OpellaConfig.GetDeviceRegIndex();
    //Calling base class Initializations. 
    COpellaConfigPageBase::InitializeUIData( OpellaConfig );
}

/****************************************************************************
     Function: UpdateUI()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This function will update user interface with member variable, 
               This function is called by the OnShowWindow member function of 
               the base class.
Date           Initials    Description
27-Feb-2008       SPC         Initial
28-May-2008       JCK         Added support for device register
****************************************************************************/
void CDeviceDlg::UpdateUI()
{
    // 1) Fill Device Selection
    // Fill the available core list
    FillDeviceList();
    // Fill Selected Core List
    FillSelectedCoreList();
    
    // 2) Fill Multi-Core Area
    // Fill Devices on Scan Chain combo
    FillCoreCountList();
    // Fill IR Length values
    FillIRLengthCombos();
    // Update Multi Core display based on configuration
    UpdateMultiCoreDisplay();
    // Update Multi Core scroll bar based on the number of cores
    UpdateCoreScroll();

	 if(m_DeviceCombo.IsWindowEnabled())
		{
      m_DeviceCombo.ResetContent();
		FillDeviceRegList();		 
		}
    
	if (m_EnableCoreSight)
	{
		m_CoresightButton.EnableWindow(TRUE);
	}
	else
	{	 
		m_CoresightButton.EnableWindow(FALSE);
	}

    // Exchange data in member variables to controls
    UpdateData( FALSE );
    
}

/****************************************************************************
     Function: OnUserRegisterSettings()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: Called when user clicks the configure button.
Date           Initials    Description
24-Jul-2007      JCK           Initial
****************************************************************************/
void CDeviceDlg::OnUserRegisterSettings() 
{	
	CUserRegSettings cUserRegSettingsDlg(NULL);
   (void)cUserRegSettingsDlg.DoModal();	
}


/****************************************************************************
     Function: OnSelchangeDeviceCombo()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: Called when user changes the device register selection.
Date           Initials    Description
24-Jul-2007      JCK           Initial
****************************************************************************/
void CDeviceDlg::OnSelchangeDeviceCombo() 
{
	m_RegFileIndex = ((CComboBox *)GetDlgItem(IDC_DEVICE_COMBO))->GetCurSel();
}

/****************************************************************************
     Function: OnCoresight()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: Called when Coresight Configuration is called.
Date           Initials    Description
12-Mar-2010      JCK           Initial
****************************************************************************/
void CDeviceDlg::OnCoresightConfig() 
{
	CoresightConfigDlg.m_uiCore = GetSelectedCoreNumber();
	CoresightConfigDlg.m_ptyJtagScanChain = GetJtagScanDetails();
	strcpy(CoresightConfigDlg.m_pszCoreType, GetSelectedCoreDetails());

	CoresightConfigDlg.DoModal();
}
/****************************************************************************
     Function: InitializeCoresight()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : void
  Description: For initializing the Coresight parameters.
Date           Initials    Description
12-Mar-2010      JCK           Initial
****************************************************************************/
void CDeviceDlg::InitializeCoresight( COpellaConfig &OpellaConfig )
{
    CoresightConfigDlg.m_DbgBaseAddr = CHexUtils::GetHexString( OpellaConfig.GetDebugBaseAddress());
    CoresightConfigDlg.m_DbgApIndex  = CHexUtils::GetHexString( OpellaConfig.GetDebugApIndex());
    CoresightConfigDlg.m_MemApIndex  = CHexUtils::GetHexString( OpellaConfig.GetMemoryApIndex());
	CoresightConfigDlg.m_UseMemApMemAccess = CHexUtils::GetHexString( OpellaConfig.GetUseMemApForMemAccess());

    CoresightConfigDlg.m_DbgBaseAddrBackup    = CoresightConfigDlg.m_DbgBaseAddr;
    CoresightConfigDlg.m_DbgApIndexBackup     = CoresightConfigDlg.m_DbgApIndex;
    CoresightConfigDlg.m_MemApIndexBackup	  = CoresightConfigDlg.m_MemApIndex; 

	CDriverMgr::GetInstance().SetCoresightParam(CoresightConfigDlg.m_DbgApIndex, 
												CoresightConfigDlg.m_MemApIndex, 
												CoresightConfigDlg.m_JtagApIndex, 
												CoresightConfigDlg.m_DbgBaseAddr); /* Now we are not using the Jtag Ap index */

}
