/****************************************************************************
     Module  : TargetConnectionDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of
               CTargetConnectionDlg class
Date           Initials    Description
27-Jul-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#ifndef _TARGET_CONNECTION_DLG_H_
#define _TARGET_CONNECTION_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OpellaConfigPageBase.h"

class COpellaConfig;

/****************************************************************************
     Function: CTargetConnectionDlg
     Engineer: Jiju George T
  Description: Handles the Target Connection page
Date           Initials    Description
27-Jul-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/
class CTargetConnectionDlg : public COpellaConfigPageBase
{

// Construction
public:
    void UpdateUI();
    void InitializeUIData ( COpellaConfig &OpellaConfig );
    bool GetSelectedOpellaDetails( TyUSBDeviceInstance &tyUSBDeviceInstance );
    CTargetConnectionDlg( COpellaConfig &OpellaConfig );
    ~CTargetConnectionDlg();
    bool ApplyChanges( COpellaConfig &OpellaConfig );
    JTAGClockFreqMode GetFrequencyMode(); 
    int GetFrequencyValue(); 

// Dialog Data
    //{{AFX_DATA(CTargetConnectionDlg)
    enum { IDD = IDD_TARGET_CONN_DLG };
    CComboBox   m_TargetVoltageCombo;
    CComboBox   m_JTAGFreqCombo;
    CComboBox   m_DetectedOpellasCombo;
    int     m_nJTAGClockFreqMode;
    //}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CTargetConnectionDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CTargetConnectionDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnRecheckForOpella();
    afx_msg void OnFixedFreqRadio();
    afx_msg void OnAdaptiveFreqRadio();
    afx_msg void OnAutoFreqRadio();
    afx_msg void OnSelchangeDetectedOpellaCombo();
    afx_msg void OnSelchangeJtagFreqCombo();
    afx_msg void OnSelchangeTargetVoltage();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    void FillJTAGFixedFrequencies();
    CString GetJTAGFrequencyAsString( int nJTAGFreeqInKhz );
    void FillTargetVoltageCombo();
    CString GetVoltageString( float fVoltage );

private:
    int GetSelectFixedJTAGClockFreqInKHz();
    void SelectPrevSelectedOpella();
    bool HasDuplicateOpellas();
    void DisplayConnectedOpellas();
    void DetectConnectedOpellas();
    void SlelectJTAGClockFrequencyMode();
    COpellaConfig &m_OpellaConfig;
    TyMultipleUSBDevices m_tyMultipleUSBDevices;
    int m_nSelectedJTAGFreqInKHz;
    float m_fTargetVoltage;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _TARGET_CONNECTION_DLG_H_
