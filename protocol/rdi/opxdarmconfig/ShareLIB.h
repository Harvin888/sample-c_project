/****************************************************************************
       Module: ShareLIB.h
     Engineer: Rory Cosgrove
  Description: The purpose of this file is act as a shared header between
               PathFinder and any other project; add definitions which are
               tied into the pathfinder structure which you want to stub
               in your other project, this header is included in Library.CPP
Version        Date           Initials    Description
****************************************************************************/

#ifdef __cplusplus

int SharedLIBUpdateWindowsForChange
(
   unsigned long     ulChangeType,
   BOOL              bTrackPC      = FALSE,
   BOOL              bTrackAddress = FALSE,
   BOOL              bTrackTrace   = FALSE,
   const CAddrRange  *pAddress     = NULL,
   unsigned long     ulTraceFrame  = 0
);

TyError SharedLIBUpdateCacheRelocationAddress
(
   BOOL bWarn = TRUE
);

TyError SharedLIBNotifyRegisterWritten
(
   TXRXReg           Reg,        
   unsigned long     ulDataValue 
);

void SharedLIBChangeToHaltBitmap
(
   BOOL  bHalt
);

void SharedLIBGetSetCurrentDisassemblyMode
(
   TyCurrentDisassemblyMode  *ptyCurrentDisassemblyMode,
   bool                      bSetNotGet
);

ulong SharedLIBGetEvaluatedCacheRelocationAddress  
(
   void
);                    

TyError SharedLIBSetupRegisterSettings               
(
   void
);

TyError SharedLIBSetInitialEndianMode                
(
   void
); 

ProcessorConfig* SharedLIBGetProcCfgPtr                       
(
   void
);

void* SharedLIBGetPathFinderPtr                    
(
   void
);                    

BOOL SharedLIBMemoryAccessInEmulationSupported    
(
   void
);

BOOL SharedLIBTranslateAddress
(
   TXRXMemAccessTypes        OrigReadType,
   TXRXMemAccessTypes        TargetReadType,
   CAddrRange                *pAddress
);

TyBreakpointSetup* SharedLIBGetPtrToBreakpointsSetup            
(
   void
);

TyError SharedLIBUpdateStatusInformation             
(
   void
); 

void* SharedLIBGetMainFramePtr                     
(
   void
);

BOOL SharedLIBIsTLBSupported
(
   void
);     

CRegDef* SharedLIBGetCRegDefforName
(
   char *pszRegisterName,
   BOOL bCaseInsensitive= FALSE,
   BOOL bSearchBits     = FALSE
);

void SharedLIBShowHelpContents
(
   void
);

CString SharedLIBGetProfileString
(
   LPCTSTR lpszSection, 
   LPCTSTR lpszEntry,
   LPCTSTR lpszDefault = NULL
);
   
BOOL SharedLIBWriteProfileString
(
   LPCTSTR lpszSection, 
   LPCTSTR lpszEntry,
   LPCTSTR lpszValue
);

CString SharedLIBGetFamilyFileName
(
   void
);

CString SharedLIBGetDevicesFileName
(
   void
);

CString SharedLIBGetSmartMXCfgFileName
(
   void
);

BOOL SharedLIBSetJtagFrequencySupported
(
   void
);
BOOL SharedLIBIsOpellaUSBSupported
(
   void
);

BOOL SharedLIBIsOpellaParallelSupported
(
   void
);

BOOL SharedLIBEthernetSupported
(
   void
);

uint SharedLIBDefaultJtagFrequency
(
   void
);

void SharedLIBDoWaitCursor
(
   int nCode
);

void SharedLIBDisableRemoteControlInterface
(
   void
);

void SharedLIBEnableRemoteControlInterface
(
   void
);

BOOL SharedLIBIsRemoteControlInterfaceEnabled
(
   void
);

unsigned short SharedLIBGetLastRCISocketFromRegistry
(
   void
);

void SharedLIBDisableKdwpServer
(
   void
);

void SharedLIBEnableKdwpServer
(
   void
);

BOOL SharedLIBIsKdwpServerEnabled
(
   void
);

unsigned short SharedLIBGetLastKdwpSocketFromRegistry
(
   void
);

CString SharedLIBGetDefaultCfgFileName
(
   void
);
BOOL SharedLIBMultipleOpellaUSBSupported
(
   void
);
BOOL SharedLIBIsVitraUSBSupported
(
   void
);
BOOL SharedLIBIsOpellaParallelSupported
(
   void
);
BOOL SharedLIBIsOpellaUSBSupported
(
   void
);
#endif

