#if !defined(AFX_ADDDELAY_H__A5B51C86_34E5_4000_97DF_A3325FF3EFBB__INCLUDED_)
#define AFX_ADDDELAY_H__A5B51C86_34E5_4000_97DF_A3325FF3EFBB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddDelay.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AddDelay dialog

class AddDelay : public CDialog
{
// Construction
public:
	AddDelay(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AddDelay)
	enum { IDD = IDD_ADD_DELAY };
	CString	m_Delay;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AddDelay)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AddDelay)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDDELAY_H__A5B51C86_34E5_4000_97DF_A3325FF3EFBB__INCLUDED_)
