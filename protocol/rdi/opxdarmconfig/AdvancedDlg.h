/****************************************************************************
       Module: AdvancedDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CAdvancedDlg class
Date           Initials    Description
24-Jul-2007      JG          Initial
27-Feb-2008      SPC         Added Support for Load/Save config options.
****************************************************************************/

#ifndef _ADVANCED_DLG_H_
#define _ADVANCED_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OpellaConfigPageBase.h"

/****************************************************************************
     Function: CAdvancedDlg
     Engineer: Jiju George T
  Description: This class handles the Advanced page operations
Date           Initials    Description
24-Jul-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
21-May-2008       SJ         Added function for enabling Safe Non Vector Address 
****************************************************************************/
class CAdvancedDlg : public COpellaConfigPageBase
{

// Construction
public:
    void InitializeUIData ( COpellaConfig &OpellaConfig );
    CAdvancedDlg( COpellaConfig &OpellaConfig );
    ~CAdvancedDlg();
    bool ApplyChanges(COpellaConfig &OpellaConfig);

// Dialog Data
    //{{AFX_DATA(CAdvancedDlg)
	enum { IDD = IDD_ADVANCED_DLG };
    int     m_nTargetExecutionPollingRate;
    CString m_csCacheCleanAddress;
    CString m_csSafeNonVectorAddress;
    int     m_nSemiHostingSupport;
	BOOL	m_bSafeVectorAddressEnable;
	//}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CAdvancedDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CAdvancedDlg)
    virtual BOOL OnInitDialog();
	afx_msg void OnEnableSafeNvAddrClick();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    COpellaConfig &m_OpellaConfig;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _ADVANCED_DLG_H_
