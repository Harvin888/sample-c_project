#if !defined(AFX_USERREGSETTINGS_H__E202CAFF_C73E_4CDE_A6FF_81E3443AF8F6__INCLUDED_)
#define AFX_USERREGSETTINGS_H__E202CAFF_C73E_4CDE_A6FF_81E3443AF8F6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserRegSettings.h : header file
//

#include "config.h"

/////////////////////////////////////////////////////////////////////////////
// CUserRegSettings dialog

class CUserRegSettings : public CDialog
{
// Construction
public:

	void FillDisplayList(void);
	CUserRegSettings(CWnd* pParent = NULL);   // standard constructor



// Dialog Data
	//{{AFX_DATA(CUserRegSettings)
	enum { IDD = IDD_USER_REG_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserRegSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	private:

		

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUserRegSettings)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnAddfile();
	afx_msg void OnEdit();
	afx_msg void OnRemove();
	afx_msg void OnRemoveAll();
	afx_msg void OnStorefile();
	afx_msg void  OnAddDelay(); 
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERREGSETTINGS_H__E202CAFF_C73E_4CDE_A6FF_81E3443AF8F6__INCLUDED_)
