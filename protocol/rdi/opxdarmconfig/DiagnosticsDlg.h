/****************************************************************************
       Module: DiagnosticsDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CDiagnosticsDlg class
Date           Initials    Description
01-Aug-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#ifndef _DIAGNOSTICS_DLH_H_
#define _DIAGNOSTICS_DLH_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OpellaConfigPageBase.h"

/****************************************************************************
     Function: CDiagnosticsDlg
     Engineer: Jiju George T
  Description: This class handles the diagnostics page options
Date           Initials    Description
01-Aug-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/
class CDiagnosticsDlg : public COpellaConfigPageBase
{

// Construction
public:
    void InitializeUIData ( COpellaConfig &OpellaConfig );
    CDiagnosticsDlg( COpellaConfig &OpellaConfig,
                     CTargetConnectionDlg &TargetConnectionPage ,
                     CDeviceDlg &DevicePage);
    ~CDiagnosticsDlg();
    bool ApplyChanges( COpellaConfig &OpellaConfig );

// Dialog Data
    //{{AFX_DATA(CDiagnosticsDlg)
	enum { IDD = IDD_DIAGNOSTICS_DLG };
	CEdit	m_csSetCheckTarget;
	CEdit	m_csSetCheckOpella;
    BOOL    m_bEnableRDILogging;
    CString m_csOpellaStatus;
	CString	m_csTargetStatus;
	//}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CDiagnosticsDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CDiagnosticsDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnOpellaCheck();
    afx_msg void OnTargetCheck();
    afx_msg void OnHardReset();
	afx_msg void OnChangeTargetStatus();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    COpellaConfig &m_OpellaConfig;
    CTargetConnectionDlg &m_TargetConnectionPage;
    CDeviceDlg &m_DevicePage;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _DIAGNOSTICS_DLH_H_
