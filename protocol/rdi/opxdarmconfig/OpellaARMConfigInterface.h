/****************************************************************************
     Function: OpellaARMConfigInterface.h
     Engineer: Jiju George T
  Description: Contains the declaration of interface functions
Date           Initials    Description
09-Aug-2007      JG          Initial
18-Feb-2008		 SJ			 Added support for getting the DLL installation 
							 path
12-Aug-2010		 SJ			 Export all the function required by RDI dll.
							 Avoided the dependency between the two DLL's
							 while building.
****************************************************************************/

#include "opellainterface.h"

// The following will ensure that we are exporting our C++ classes when 
// building the DLL and importing the classes when build an application 
// using this DLL.
#if 0
#ifdef _MYLIB_DLLAPI_
    #define MYLIB_DLLAPI  __declspec( dllexport )
#else
    #define MYLIB_DLLAPI  __declspec( dllimport )
#endif

// The following will ensure that when building an application (or another
// DLL) using this DLL, the appropriate .LIB file will automatically be used
// when linking.

#ifndef _MYLIB_NOAUTOLIB_
#ifdef _DEBUG
#pragma comment(lib, "OpellaXDARMConfig.lib")
#else
#pragma comment(lib, "OpellaXDARMConfig.lib")
#endif
#endif

extern "C" __declspec( dllexport ) void WINAPI InitOpellaARMConfigGUI(const char *pszARMRDIDriverpath);

MYLIB_DLLAPI int ShowOpellaConfigDialog();
#endif

extern "C" __declspec( dllexport ) void InitOpellaARMConfigGUI(const char *pszARMRDIDriverpath);
extern "C" __declspec( dllexport ) int ShowOpellaConfigDialog();
extern "C" __declspec( dllexport ) void InitialiseRDIFnPtr(OpxdrdiFnptr *pOpxdrdiFnptr);
