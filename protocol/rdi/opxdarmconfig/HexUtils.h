/****************************************************************************
     Module  : HexUtils.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CHexUtils class
Date           Initials    Description
03-Aug-2007       JG         Initial
****************************************************************************/

#ifndef _HEX_UTILS_H_
#define _HEX_UTILS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/****************************************************************************
     Function: CHexUtils 
     Engineer: Jiju George T
  Description: Contains Utlity function for Hexadecimal number management
Date           Initials    Description
03-Aug-2007       JG         Initial
****************************************************************************/
class CHexUtils
{
public:
    static bool GetValueFromHexString( CString csHexString,
                                       unsigned long &ulConvertedValue);
    CHexUtils();
    virtual ~CHexUtils();
    static CString GetHexString( unsigned long ulValue );

};

#endif // _HEX_UTILS_H_
