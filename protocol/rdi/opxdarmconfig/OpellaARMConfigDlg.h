/****************************************************************************
     Function: OpellaARMConfigDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of
               COpellaARMConfigDlg class
Date           Initials    Description
24-Jul-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#ifndef _OPELLAARMCONFIGDLG_H_
#define _OPELLAARMCONFIGDLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TreePropSheetEx.h"
#include "OpellaConfig.h"

using namespace TreePropSheet;

class COpellaConfigPageBase;

//TOADD : Add forward declaration of new page classes here
class CDeviceDlg;
class CTargetConnectionDlg;
class CTargetResetDlg;
class CAdvancedDlg;
class CDiagnosticsDlg;
class CLoadSaveDlg;


/****************************************************************************
     Function: COpellaARMConfigDlg
     Engineer: Jiju George T
  Description: Main configuration dialog for Opella ARM
Date           Initials    Description
24-Jul-2007       JG         Initial
****************************************************************************/
class COpellaARMConfigDlg : public CTreePropSheetEx
{
    DECLARE_DYNAMIC(COpellaARMConfigDlg)

// Construction
public:
    COpellaARMConfigDlg( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );

// Attributes
public:

// Operations
public:

// Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(COpellaARMConfigDlg)
    virtual void OnOK();
    //}}AFX_VIRTUAL

// Implementation
public:
    bool LoadConfigurationFile(CString szFilename);
    bool SaveConfigurationToFile( CString szFilename );
    void AddPage(COpellaConfigPageBase* pPage);
    virtual ~COpellaARMConfigDlg();
     static void SetARMRDIDriverPath(const char *pszARMRDIDriverpath);
     static void GetARMRDIDriverPath( CString& csARMRDIDriverpath_o ); 


    // Generated message map functions
protected:
    void Init();
    //{{AFX_MSG(COpellaARMConfigDlg)
    virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    CString GetLastSavedConfigFileName();

private:
    CString GetTitleString();
    COpellaConfig m_OpellaConfig;

    // List of pages to be saved
    CList<COpellaConfigPageBase*, COpellaConfigPageBase*> m_PagesToSave;


    //TOADD
    CDeviceDlg *m_pDeviceDlg;
    CTargetConnectionDlg *m_pTargetConnectionDlg;
    CTargetResetDlg *m_pTargetResetDlg;
    CAdvancedDlg *m_pAdvancedDlg;
    CDiagnosticsDlg *m_pDiagnosticDlg;
    CLoadSaveDlg *m_pLoadSaveDlg;

    //Store the DLL path name here
     static char m_szARMRDIDriverPath[MAX_PATH];

};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif //_OPELLAARMCONFIGDLG_H_
