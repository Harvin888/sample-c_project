/****************************************************************************
       Module: ShareLIB.CPP
     Engineer: Rory Cosgrove
  Description: Implementation of Shared functions between PathFinder and 
               other projects.  These functions have some meaning in PathFinder
               while other projects can abstract this
Date           Initials    Description
20-Jan-2004    RC          Initial
****************************************************************************/

#include "\pathfndr\winmfc\stdafx.h"

#define KDWP_SERVER_KEY 	"KDWP Server"
#define KDWP_SERVER_PORT    8753

extern "C" unsigned short ReturnRCIPortFromAction_C(void);

/****************************************************************************
     Function: SharedLIBUpdateWindowsForChange
     Engineer: Rory Cosgrove
        Input: as UpdateWindowsForChange
       Output: as UpdateWindowsForChange 
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
08-Jan-2003    RC         initial
*****************************************************************************/
int SharedLIBUpdateWindowsForChange(   unsigned long     ulChangeType,
                                       BOOL              bTrackPC,
                                       BOOL              bTrackAddress,
                                       BOOL              bTrackTrace,
                                       const CAddrRange  *pAddress,
                                       unsigned long     ulTraceFrame)

{
   return GetPFWAppObject()->UpdateWindowsForChange(ulChangeType,
                                                    bTrackPC,        
                                                    bTrackAddress,   
                                                    bTrackTrace,     
                                                    pAddress,       
                                                    ulTraceFrame); 
}

/****************************************************************************
     Function: SharedLIBGetProcCfgPtr
     Engineer: Rory Cosgrove
        Input: as CurrentProcessorConfig
       Output: as CurrentProcessorConfig
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
ProcessorConfig* SharedLIBGetProcCfgPtr(void)
{
   return (ProcessorConfig*)(GetPFWAppObject()->PathFinder.GetProcCfgObject()->CurrentProcessorConfig());
}

/****************************************************************************
     Function: SharedLIBGetPathFinderPtr
     Engineer: Rory Cosgrove
        Input: none
       Output: pointer to pathfinder object
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void* SharedLIBGetPathFinderPtr(void)
{
   return ((CPathFinder*)(&(GetPFWAppObject()->PathFinder)));
}

/****************************************************************************
     Function: SharedLIBNotifyRegisterWritten
     Engineer: Rory Cosgrove
        Input: as NotifyRegisterWritten
       Output: as NotifyRegisterWritten
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
TyError SharedLIBNotifyRegisterWritten(TXRXReg Reg, unsigned long ulDataValue )
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->NotifyRegisterWritten(Reg, ulDataValue);
}

/****************************************************************************
     Function: SharedLIBChangeToHaltBitmap
     Engineer: Rory Cosgrove
        Input: as ChangeToHaltBitmap
       Output: as ChangeToHaltBitmap
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void SharedLIBChangeToHaltBitmap(BOOL bHalt)
{
   GetPFWAppObject()->ChangeToHaltBitmap(bHalt);
}

/****************************************************************************
     Function: SharedLIBUpdateCacheRelocationAddress
     Engineer: Rory Cosgrove
        Input: as UpdateCacheRelocationAddress
       Output: as UpdateCacheRelocationAddress
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
TyError SharedLIBUpdateCacheRelocationAddress(BOOL bWarn)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->UpdateCacheRelocationAddress(bWarn);
}

/****************************************************************************
     Function: SharedLIBGetSetCurrentDisassemblyMode
     Engineer: Rory Cosgrove
        Input: as GetSetCurrentDisassemblyMode
       Output: as GetSetCurrentDisassemblyMode
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void SharedLIBGetSetCurrentDisassemblyMode(TyCurrentDisassemblyMode  *ptyCurrentDisassemblyMode,
                                           bool                      bSetNotGet)
{
   GetPFWAppObject()->PathFinder.GetProcCfgObject()->GetSetCurrentDisassemblyMode(ptyCurrentDisassemblyMode,bSetNotGet);
}

/****************************************************************************
     Function: SharedLIBGetEvaluatedCacheRelocationAddress
     Engineer: Rory Cosgrove
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
ulong SharedLIBGetEvaluatedCacheRelocationAddress(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->GetEvaluatedCacheRelocationAddress();
}

/****************************************************************************
     Function: SharedLIBSetupRegisterSettings
     Engineer: Rory Cosgrove
        Input: as SetupRegisterSettings
       Output: as SetupRegisterSettings
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
TyError SharedLIBSetupRegisterSettings(void)                    
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->SetupRegisterSettings();
}

/****************************************************************************
     Function: SharedLIBSetInitialEndianMode
     Engineer: Rory Cosgrove
        Input: as SetInitialEndianMode
       Output: as SetInitialEndianMode
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
TyError SharedLIBSetInitialEndianMode(void)                    
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->SetInitialEndianMode();
}

/****************************************************************************
     Function: SharedLIBMemoryAccessInEmulationSupported
     Engineer: Rory Cosgrove
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBMemoryAccessInEmulationSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->MemoryAccessInEmulationSupported();
}

   
/****************************************************************************
     Function: SharedLIBMemoryAccessInEmulationSupported
     Engineer: Rory Cosgrove
        Input: as MemoryAccessInEmulationSupported 
       Output: as MemoryAccessInEmulationSupported 
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBTranslateAddress(TXRXMemAccessTypes  OrigReadType,
                               TXRXMemAccessTypes  TargetReadType,
                               CAddrRange          *pAddress)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->TranslateAddress(OrigReadType,TargetReadType,pAddress);
}

/****************************************************************************
     Function: SharedLIBGetPtrToBreakpointsSetup 
     Engineer: Rory Cosgrove
        Input: as GetPtrToBreakpointsSetup
       Output: as GetPtrToBreakpointsSetup
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
TyBreakpointSetup* SharedLIBGetPtrToBreakpointsSetup(void)
{
   return &(GetPFWAppObject()->m_BreakpointsSetup); 
}

/****************************************************************************
     Function: SharedLIBUpdateStatusInformation
     Engineer: Rory Cosgrove
        Input: none
       Output: TyError
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
TyError SharedLIBUpdateStatusInformation(void)
{
   return GetPFWAppObject()->pMainFrame->m_wndStatusBar.UpdateStatusInformation();
}

/****************************************************************************
     Function: SharedLIBGetMainFramePtr
     Engineer: Rory Cosgrove
        Input: none
       Output: CMainFrame *
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void* SharedLIBGetMainFramePtr(void)
{
   return GetPFWAppObject()->pMainFrame; 
}

/****************************************************************************
     Function: SharedLIBIsTLBSupported
     Engineer: Rory Cosgrove
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBIsTLBSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->IsTLBSupported();
}

/****************************************************************************
     Function: SharedLIBGetCRegDefforName
     Engineer: Rory Cosgrove
        Input: as GetCRegDefforName
       Output: as GetCRegDefforName
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
CRegDef* SharedLIBGetCRegDefforName(char *pszRegisterName,
                                    BOOL bCaseInsensitive,
                                    BOOL bSearchBits)
{
   return  GetPFWAppObject()->PathFinder.GetProcCfgObject()->GetCRegDefforName(pszRegisterName, 
                                                                               bCaseInsensitive, 
                                                                               bSearchBits);

}

/****************************************************************************
     Function: SharedLIBShowHelpContents
     Engineer: Rory Cosgrove
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void SharedLIBShowHelpContents(void)
{
   GetPFWAppObject()->ShowHelpContents();
}

/****************************************************************************
     Function: SharedLIBGetProfileString
     Engineer: Rory Cosgrove
        Input: as GetProfileString
       Output: as GetProfileString
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
CString SharedLIBGetProfileString(LPCTSTR lpszSection, 
                                  LPCTSTR lpszEntry,
                                  LPCTSTR lpszDefault)
{
   return GetPFWAppObject()->GetProfileString(lpszSection,lpszEntry,lpszDefault);
}

/****************************************************************************
     Function: SharedLIBWriteProfileString
     Engineer: Rory Cosgrove
        Input: as WriteProfileString
       Output: as WriteProfileString
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBWriteProfileString(LPCTSTR lpszSection, 
                                 LPCTSTR lpszEntry,
                                 LPCTSTR lpszValue)
{
   return GetPFWAppObject()->WriteProfileString(lpszSection,lpszEntry,lpszValue);   
}                                                                                                         

/****************************************************************************
     Function: SharedLIBGetFamilyFileName() 
     Engineer: Rory Cosgrove
        Input: none 
       Output: CString of the family file
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
CString SharedLIBGetFamilyFileName(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->sFamilyFile;  
}

/****************************************************************************
     Function: SharedLIBGetDevicesFileName() 
     Engineer: Rory Cosgrove
        Input: none
       Output: CString of the devices file
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
CString SharedLIBGetDevicesFileName(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->sDevicesFile; 
}

/****************************************************************************
     Function: SharedLIBSetJtagFrequencySupported(void) 
     Engineer: Rory Cosgrove
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBSetJtagFrequencySupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->SetJtagFrequencySupported();
}
/****************************************************************************
     Function: SharedLIBIsOpellaUSBSupported(void) 
     Engineer: Patrick Noonan
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
12-may-2004    RN         initial
*****************************************************************************/
BOOL SharedLIBIsOpellaUSBSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->IsOpellaUSBSupported();
}

/****************************************************************************
     Function: SharedLIBIsOpellaParallelSupported
     Engineer: David Nicholls
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
               Reports True if Opella Parallel is supported
Date           Initials    Description
19-may-2004    DN         initial
*****************************************************************************/
BOOL SharedLIBIsOpellaParallelSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->IsOpellaParallelSupported();
}


/****************************************************************************
     Function: SharedLIBEthernetSupported(void) 
     Engineer: Rory Cosgrove
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBEthernetSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->EthernetSupported();
}
           
/****************************************************************************
     Function: SharedLIBDefaultJtagFrequency(void) 
     Engineer: Rory Cosgrove
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
uint SharedLIBDefaultJtagFrequency(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->DefaultJtagFrequency();
}

/****************************************************************************
     Function: SharedLIBDoWaitCursor
     Engineer: Rory Cosgrove
        Input: int nCode
       Output: CString of the devices file
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void SharedLIBDoWaitCursor(int nCode)
{
   GetPFWAppObject()->DoWaitCursor(nCode);
}

/****************************************************************************
     Function: SharedLIBDisableRemoteControlInterface
     Engineer: Rory Cosgrove
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
void SharedLIBDisableRemoteControlInterface(void)
{
   GetPFWAppObject()->WriteProfileInt("Remote Control Interface", "bEnable" , FALSE);
   AddRCIStatusMessage("Disabling Remote Control Interface");
}

/****************************************************************************
     Function: SharedLIBEnableRemoteControlInterface
     Engineer: Rory Cosgrove
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
27-Jan-2003    RC         initial
*****************************************************************************/
void SharedLIBEnableRemoteControlInterface(void)
{
   GetPFWAppObject()->WriteProfileInt("Remote Control Interface", "bEnable" , TRUE);
   GetPFWAppObject()->WriteProfileInt("Remote Control Interface", "ServerSocket" , ReturnRCIPortFromAction_C());
   AddRCIStatusMessage("Enabling Remote Control Interface");
}

/****************************************************************************
     Function: SharedLIBIsRemoteControlInterfaceEnabled
     Engineer: Rory Cosgrove
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
27-Jan-2003    RC         initial
*****************************************************************************/
BOOL SharedLIBIsRemoteControlInterfaceEnabled(void)
{
   return (BOOL)GetPFWAppObject()->GetProfileInt("Remote Control Interface", "bEnable" , FALSE);
}

/****************************************************************************
     Function: SharedLIBGetLastRCISocketFromRegistry
     Engineer: Rory Cosgrove
        Input: none
       Output: ushort
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
27-Jan-2003    RC         initial
*****************************************************************************/
unsigned short SharedLIBGetLastRCISocketFromRegistry(void)
{
   return GetPFWAppObject()->GetProfileInt("Remote Control Interface", "ServerSocket" , 2331);
}


/****************************************************************************
     Function: SharedLIBDisableKdwpServer
     Engineer: David Nicholls
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
02-Feb-2005    DN         initial
*****************************************************************************/
void SharedLIBDisableKdwpServer(void)
{
   GetPFWAppObject()->WriteProfileInt(KDWP_SERVER_KEY, "bEnable" , FALSE);
}

/****************************************************************************
     Function: SharedLIBIsKdwpServerEnabled
     Engineer: David Nicholls
        Input: none
       Output: none
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
02-Feb-2005    DN         initial
*****************************************************************************/
BOOL SharedLIBIsKdwpServerEnabled(void)
{
   return (BOOL)GetPFWAppObject()->GetProfileInt(KDWP_SERVER_KEY, "bEnable" , FALSE);
}

/****************************************************************************
     Function: SharedLIBGetLastKdwpSocketFromRegistry
     Engineer: David Nicholls
        Input: none
       Output: ushort
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
02-Feb-2005    DN         initial
*****************************************************************************/
unsigned short SharedLIBGetLastKdwpSocketFromRegistry(void)
{
   return GetPFWAppObject()->GetProfileInt(KDWP_SERVER_KEY, "ServerSocket" , KDWP_SERVER_PORT);
}


/****************************************************************************
     Function: SharedLIBGetSmartMXCfgFileName()
     Engineer: Rory Cosgrove
        Input: none
       Output: CString
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
09-Jan-2003    RC         initial
*****************************************************************************/
CString SharedLIBGetSmartMXCfgFileName()
{
// Rename this function to be non smartmx specific...we also now use it
// for Theseus processors which are defined in an ini file. The XML file will
// contain one default processor and will be copied for each
#ifdef THESEUS
   return CString("theseus.ini");
#else
   return CString("smartmx.xml");
#endif
}

/****************************************************************************
     Function: SharedLIBGetDefaultCfgFileName 
     Engineer: Patrick Noonan
        Input: none
       Output: CString
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
25-Mar-2007    PN          initial
*****************************************************************************/
CString SharedLIBGetDefaultCfgFileName(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->sDefaultCfgFileName;
}

/****************************************************************************
     Function: SharedLIBMultipleOpellaUSBSupported
     Engineer: Patrick Noonan
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
25-Mar-2007    PN          initial
*****************************************************************************/
BOOL SharedLIBMultipleOpellaUSBSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->MultipleOpellaUSBSupported();
}

/****************************************************************************
     Function: SharedLIBIsVitraUSBSupported
     Engineer: Patrick Noonan
        Input: none
       Output: BOOL
  Description: LIB accessor to facilitate stubbing for other projects
Date           Initials    Description
25-Mar-2007    PN          initial
*****************************************************************************/
BOOL SharedLIBIsVitraUSBSupported(void)
{
   return GetPFWAppObject()->PathFinder.GetProcCfgObject()->IsVitraUSBSupported();
}


