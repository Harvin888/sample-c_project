/****************************************************************************
       Module: HexUtils.cpp
     Engineer: Jiju George T
  Description:This file contains the implementation of CHexUtils class
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/

#include "stdafx.h"
#include "HexUtils.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/****************************************************************************
     Function: Constructor
     Engineer: Jiju George T
	 Input   : void
	 Output  : void
  Description:
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
CHexUtils::CHexUtils()
{

}

/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description:              
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
CHexUtils::~CHexUtils()
{

}

/****************************************************************************
     Function: GetHexString()
     Engineer: Jiju George T
	 Input   : ulValue - value to be converted
	 Output  : CString - converted hex string
  Description: Converts the given value to hex string.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
CString CHexUtils::GetHexString( unsigned long ulValue )
{
    CString csHexValue;
    csHexValue.Format( _T( "0x%08X" ), ulValue );

    return csHexValue;
}
 
/****************************************************************************
     Function: GetValueFromHexString()
     Engineer: Jiju George T
	 Input   : csHexString      - Hex string to be converted
               ulConvertedValue - [OUT] Converted out value
     Output  : bool             - true if conversion success. Else false.
  Description: Parses the given hexadecimal string and converts it to equivalent integer
               value. If the given string is not in proper hex format then return error
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
bool CHexUtils::GetValueFromHexString( CString csHexString,
                                       unsigned long &ulConvertedValue )
{
    bool bRet = true;
    TCHAR *szStopString;

    ulConvertedValue = _tcstoul( csHexString, &szStopString, 16 );
    if ( 0 != _tcscmp( szStopString, _T("") ))
    {
        bRet = false;
    }

    return bRet;
}
