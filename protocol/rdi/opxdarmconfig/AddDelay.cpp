// AddDelay.cpp : implementation file
//

#include "stdafx.h"
//#include "opellaarmconfig.h"
#include "AddDelay.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddDelay dialog


AddDelay::AddDelay(CWnd* pParent /*=NULL*/)
	: CDialog(AddDelay::IDD, pParent)
{
	//{{AFX_DATA_INIT(AddDelay)
	m_Delay = _T("");
	//}}AFX_DATA_INIT
}


void AddDelay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AddDelay)
	DDX_Text(pDX, IDC_DELAY_EDIT, m_Delay);
	DDV_MaxChars(pDX, m_Delay, 8);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AddDelay, CDialog)
	//{{AFX_MSG_MAP(AddDelay)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AddDelay message handlers

void AddDelay::OnOK() 
{
	UpdateData(TRUE);

	
	
	CDialog::OnOK();
}

void AddDelay::OnCancel() 
{
	
	
	CDialog::OnCancel();
}

BOOL AddDelay::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
