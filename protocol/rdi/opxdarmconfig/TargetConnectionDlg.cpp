/******************************************************************************
  Module         : TargetResetDlg.cpp
  Engineer    : Jiju George T
  Description : This file contains the implementation of TargetConnectionDlg class.
  Date          Initials    Description
  27-Jul-2007    JG          Initial
  20-Feb-2008    SJ          Added support for intermediate frequencies of 
                             1kHz,5kHz,10kHz,20kHz,50kHz and 70kHz
  27-Feb-2008    SPC         Added Support for Load/Save config options.
  24-Mar-2008	 SJ		     Solved the problem for writing to file with 
							 frequencies less than 1MHz.
  12-Aug-2010	 SJ		     Avoided possible memory leaks.
**********************************************************************************/
 
#include "stdafx.h"
#include "OpellaConfig.h"
#include "TargetConnectionDlg.h"
#include "DriverMgr.h"

#define MIN_JTAG_FREEQ_KHZ 1
#define MAX_JTAG_FREEQ_KHZ 100000
#define STEP_JTAG_FREEQ_KHZ 100

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

double g_TargetVoltageList[] = { 0.0,
                                 3.3,
                                 2.5,
                                 1.8,
                                 1.2 };
int g_nTargetVoltageCount = sizeof(g_TargetVoltageList) / sizeof(double);


/****************************************************************************
  Function   : Constructor
  Engineer   : Jiju George T
  Input      : OpellaConfig - Contains the configuration
  Output     : void
  Description: Creates an empty config object to which configurations can 
               be added and then saved.
  Date           Initials    Description
  2007-08-02       JG          Initial
  27-Feb-2008      SPC         Initializations are changed to another function.
****************************************************************************/
CTargetConnectionDlg::CTargetConnectionDlg( COpellaConfig &OpellaConfig ) :
    COpellaConfigPageBase(CTargetConnectionDlg::IDD),
    m_OpellaConfig( OpellaConfig ),
    m_nSelectedJTAGFreqInKHz( 0 )
{
    //{{AFX_DATA_INIT(CTargetConnectionDlg)
    m_nJTAGClockFreqMode = -1;
    //}}AFX_DATA_INIT

    m_psp.dwFlags &= (~PSP_HASHELP);

    // Detect connected opellas
    DetectConnectedOpellas();

    InitializeUIData(m_OpellaConfig);
}


/****************************************************************************
  Function   : Destructor
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Destructs the Object
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
CTargetConnectionDlg::~CTargetConnectionDlg()
{
    // Nothing to do
}

/****************************************************************************
  Function   : DoDataExchange()
  Engineer   : Jiju George T
  Input      : pDX  - Holds the exchange information
  Output     : void
  Description: Does the data exchange between controls and corresponding member variables.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::DoDataExchange( CDataExchange* pDX )
{
    CResizablePage::DoDataExchange( pDX );
    //{{AFX_DATA_MAP(CTargetConnectionDlg)
    DDX_Control(pDX, IDC_TARGET_VOLTAGE, m_TargetVoltageCombo);
    DDX_Control(pDX, IDC_JTAG_FREQ_COMBO, m_JTAGFreqCombo);
    DDX_Control(pDX, IDC_DETECTED_OPELLA_COMBO, m_DetectedOpellasCombo);
    DDX_Radio(pDX, IDC_FIXED_FREQ_RADIO, m_nJTAGClockFreqMode);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTargetConnectionDlg, COpellaConfigPageBase)
    //{{AFX_MSG_MAP(CTargetConnectionDlg)
    ON_BN_CLICKED(IDC_RECHECK_FOR_OPELLA, OnRecheckForOpella)
    ON_BN_CLICKED(IDC_FIXED_FREQ_RADIO, OnFixedFreqRadio)
    ON_BN_CLICKED(IDC_ADAPTIVE_FREQ_RADIO, OnAdaptiveFreqRadio)
    ON_BN_CLICKED(IDC_AUTO_FREQ_RADIO, OnAutoFreqRadio)
    ON_CBN_SELCHANGE(IDC_DETECTED_OPELLA_COMBO, OnSelchangeDetectedOpellaCombo)
    ON_CBN_SELCHANGE(IDC_JTAG_FREQ_COMBO, OnSelchangeJtagFreqCombo)
    ON_CBN_SELCHANGE(IDC_TARGET_VOLTAGE, OnSelchangeTargetVoltage)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/****************************************************************************
  Function   : OnInitDialog()
  Engineer   : Jiju George T
  Input      : void
  Output     : BOOL - controls the focus on control
  Description: Called when this page is shown for first time.
               Does the initialization job.
  Date           Initials    Description
  2007-08-02       JG          Initial
  27-Feb-2008      SPC         UI Updates are doing in another function and 
                               it will call on OnShowWindow function.
****************************************************************************/
BOOL CTargetConnectionDlg::OnInitDialog()
{
    CResizablePage::OnInitDialog();

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}


/****************************************************************************
  Function   : FillJTAGFixedFrequencies()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Fills the JTAG Fixed Frequency combo with possible values.
  Date           Initials    Description
  2007-08-02       JG          Initial
  20-Feb-2008      SJ          Added support for intermediate frequencies of 
                               1kHz,5kHz,10kHz,20kHz,50kHz and 70kHz
****************************************************************************/
void CTargetConnectionDlg::FillJTAGFixedFrequencies()
{
    m_JTAGFreqCombo.ResetContent();

    // Fill possible fixed frequency values
    m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( 1 ));
    m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( 5 ));
    m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( 10 ));
    m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( 20 ));
    m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( 50 ));
    m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( 70 ));

    // Fill possible fixed frequency values
    for ( int nJTAGFreeqInKhz = 100;
              nJTAGFreeqInKhz <= MAX_JTAG_FREEQ_KHZ;
              nJTAGFreeqInKhz += STEP_JTAG_FREEQ_KHZ )
    {
        m_JTAGFreqCombo.AddString( GetJTAGFrequencyAsString( nJTAGFreeqInKhz ));
    }

    // Select the selected value
    int nSelIndex =  m_JTAGFreqCombo.FindStringExact( 0, GetJTAGFrequencyAsString( m_nSelectedJTAGFreqInKHz ));
    if( CB_ERR != nSelIndex )
    {
        m_JTAGFreqCombo.SetCurSel( nSelIndex );
    }
    else
    {
      m_JTAGFreqCombo.SetCurSel( 0 );
      m_nSelectedJTAGFreqInKHz = 1;
    }
}


/****************************************************************************
  Function   : GetJTAGFrequencyAsString()
  Engineer   : Jiju George T
  Input      : nJTAGFreqInKhz - Frequency to be converted
  Output     : CString         - Converted frequency in string form
  Description: Converts the given JTAG freqiency to string representation with unit.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
CString CTargetConnectionDlg::GetJTAGFrequencyAsString( int nJTAGFreeqInKhz )
{
    CString csFrequency;

    if( nJTAGFreeqInKhz >= 1000 )
    {
        // Convert to MHz
        float fJTAGFreeqInMHz = nJTAGFreeqInKhz / 1000.0;
        csFrequency.Format( _T( "%3.1f MHz" ), fJTAGFreeqInMHz );
    }
    else
    {
        // Display as kHz
        csFrequency.Format( _T( "%d kHz" ), nJTAGFreeqInKhz );
    }

    return csFrequency;

}

/****************************************************************************
  Function   : OnRecheckForOpella()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects the button in UI to recheck for connected
               Opellas. Queries RDI driver and refreshes the connected Opella list
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnRecheckForOpella()
{
    // Detect connected opellas and display in list
    DetectConnectedOpellas();
    DisplayConnectedOpellas();

    // Select First Opella in the list
    m_DetectedOpellasCombo.SetCurSel(0);

}

/****************************************************************************
  Function   : SlelectJTAGClockFrequencyMode()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Selects the JTAG Clock Frequency mode radio buttons based on the mode
               existing configuration.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::SlelectJTAGClockFrequencyMode()
{
    JTAGClockFreqMode eJTAGClockFreqMode = ( JTAGClockFreqMode )m_nJTAGClockFreqMode;

    // Enable/ Disable the Frequency Combo based on mode
    if( Fixed == eJTAGClockFreqMode )
    {
        GetDlgItem(IDC_JTAG_FREQ_COMBO)->EnableWindow();
    }
    else
    {
        GetDlgItem(IDC_JTAG_FREQ_COMBO)->EnableWindow( FALSE );
    }
}

/****************************************************************************
  Function   : OnFixedFreqRadio()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects the Fixed JTAG Clock Frequency mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnFixedFreqRadio()
{
    // Enable
    GetDlgItem(IDC_JTAG_FREQ_COMBO)->EnableWindow();
}


/****************************************************************************
  Function   : OnAdaptiveFreqRadio()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description:  Called when user selects the Adaptive JTAG Clock Frequency mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnAdaptiveFreqRadio()
{
    // Disable
    GetDlgItem(IDC_JTAG_FREQ_COMBO)->EnableWindow( FALSE );
}


/****************************************************************************
  Function   : OnAutoFreqRadio()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user selects the Auto JTAG Clock Frequency mode.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnAutoFreqRadio()
{
    // Disable
    GetDlgItem(IDC_JTAG_FREQ_COMBO)->EnableWindow( FALSE );
}

/****************************************************************************
  Function   : DetectConnectedOpellas()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Detects the connected Opellas with the help of RDI driver.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::DetectConnectedOpellas()
{
    CDriverMgr::GetInstance().DetectConnectedOpellas( m_tyMultipleUSBDevices );

    // Check for Duplication
    if( HasDuplicateOpellas())
    {
        //AfxMessageBox( _T("Two or more Opella-XD USB's connected to your PC have the same serial number. \n\nTo use multiple Opellas please contact Support@Ashling.com" ));
        MessageBox("Two or more Opella-XD USB's connected to your PC have the same serial number. \n\nTo use multiple Opellas please contact Support@Ashling.com","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
    }

}

/****************************************************************************
  Function   : DisplayConnectedOpellas()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Displays the connected Opellas in the connected Opella combo.
  Date           Initials    Description
  2007-08-02       JG          Initial
*****************************************************************************/
void CTargetConnectionDlg::DisplayConnectedOpellas()
{
    m_DetectedOpellasCombo.ResetContent();

    // Add the Serial Number of connected Opellas to the list
    CString csSerialNumber;

    for(int nOpellaIndex = 0;
            nOpellaIndex < m_tyMultipleUSBDevices.ulNumberOfUSBDevices;
            ++nOpellaIndex)
    {
        csSerialNumber = m_tyMultipleUSBDevices.szOpellaXDSerialNumber[nOpellaIndex];

        if( csSerialNumber.IsEmpty())
        {
            csSerialNumber = _T( "Not Programmed" );
        }

        m_DetectedOpellasCombo.AddString( csSerialNumber );
    }

    // If no Opella-XDs detected then?
    if( 0 == m_tyMultipleUSBDevices.ulNumberOfUSBDevices )
    {
        m_DetectedOpellasCombo.AddString( _T( "No Opella-XD's Found" ));
    }
}

/****************************************************************************
  Function   : HasDuplicateOpellas()
  Engineer   : Jiju George T
  Input      : void
  Output     : bool -true if duplicate serial numner is there. 
               false if no duplicates
  Description: Checkes whether there is any connected Opellas with same serial number.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
bool CTargetConnectionDlg::HasDuplicateOpellas()
{
    bool bRet = false;

    for ( int i = 0; i < m_tyMultipleUSBDevices.ulNumberOfUSBDevices; i++ )
    {
        for ( int j= i+1; j < m_tyMultipleUSBDevices.ulNumberOfUSBDevices; j++ )
        {
           if ( 0 == strcmp(m_tyMultipleUSBDevices.szOpellaXDSerialNumber[i],
                m_tyMultipleUSBDevices.szOpellaXDSerialNumber[j] ))
            {
                bRet = true;
                break;
            }
        }
    }

    return bRet;
}

/****************************************************************************
  Function   : OnSelchangeDetectedOpellaCombo()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user changes the selected Opella.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnSelchangeDetectedOpellaCombo()
{
    m_tyMultipleUSBDevices.ucSelectedDevice = m_DetectedOpellasCombo.GetCurSel();

	CDriverMgr::GetInstance().SetOpellaXDSerialNumber( m_tyMultipleUSBDevices.szOpellaXDSerialNumber[m_tyMultipleUSBDevices.ucSelectedDevice] );
}


/****************************************************************************
  Function   : SelectPrevSelectedOpella()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: reads the USB Serial number of the previously selected Opella and tries
               to select that Opella in the connected Opella list.
               If unable to find a match then displays an informative message to let the
               user understand that previously configured Opella is not connected. In this
               case first connected Opella will be selected.
 
  Date           Initials    Description
  2007-08-02       JG          Initial
  2010-08-12	   SJ		   Modified to avoid memory leaks.
****************************************************************************/
void CTargetConnectionDlg::SelectPrevSelectedOpella()
{
    // Select first Opella as default incase the Opella with previous
    // serial number is not connected now
    m_tyMultipleUSBDevices.ucSelectedDevice = 0;

    if( m_OpellaConfig.IsOpellaUSBSerialNumberExists())
    {
        // Get Previously saved Opella USB Serial Number
        //CString csSerialNumber = m_OpellaConfig.GetOpellaUSBSerialNumber();
		CString csSerialNumber;
		m_OpellaConfig.GetOpellaUSBSerialNumber(csSerialNumber.GetBuffer(MAX_SERIAL_NUMBER_LEN));

        bool bMatched = false;

        for ( int nIndex = 0; nIndex < m_tyMultipleUSBDevices.ulNumberOfUSBDevices; ++nIndex )
        {
            if( csSerialNumber == m_tyMultipleUSBDevices.szOpellaXDSerialNumber[nIndex])
            {
                m_tyMultipleUSBDevices.ucSelectedDevice = nIndex;
                bMatched = true;
            }
        }

        if( !bMatched )
        {
            // Display Information to user
            CString csInformation;
            csInformation.Format(_T( "Previously configured Opella-XD with serial number %s not found!" ),
                                  csSerialNumber);
           // AfxMessageBox(csInformation );
               MessageBox(csInformation ,"Opella-XD for ARM",MB_OK|MB_ICONWARNING);
        }

    }

    m_DetectedOpellasCombo.SetCurSel( m_tyMultipleUSBDevices.ucSelectedDevice );

}


/****************************************************************************
  Function   : ApplyChanges()
  Engineer   : Jiju George T
  Input      : OpellaConfig - Configuration object to save configurations
  Output     : bool -  Return false if some required information is not
               filled by user (In this case notification about
               the error should be given to user). Else return true.
  Description: This function is called by the parent when user presses Apply.OK button.
               All the user configuration in the page should be saved to the passed
               COpellaConfig object.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
bool CTargetConnectionDlg::ApplyChanges( COpellaConfig &OpellaConfig )
{
    // Exchange data from controls to members
    if( IsWindow( m_hWnd ))
    {
        UpdateData();
    }

    // Save selected Opella parameters if there is any Opellas detected and selected
    if( m_tyMultipleUSBDevices.ulNumberOfUSBDevices > 0 )
    {
        // 1) Selected Instance Number
        OpellaConfig.SetOpellaUSBInstanceNumber(
                    m_tyMultipleUSBDevices.ucSelectedDevice );
        // 2) Serial Number of Selected Opella
        OpellaConfig.SetOpellaUSBSerialNumber(
            m_tyMultipleUSBDevices.szOpellaXDSerialNumber[m_tyMultipleUSBDevices.ucSelectedDevice]);
        // 3) Product ID of selected Opella
//        OpellaConfig.SetOpellaUSBProductID(
//            m_tyMultipleUSBDevices.tyUSBDeviceInstance[m_tyMultipleUSBDevices.ucSelectedDevice].usProductID);
    }

    // Save JTAG Clock Frequency Settings
    // 1) Mode
    JTAGClockFreqMode eJTAGClockFreqMode = ( JTAGClockFreqMode )m_nJTAGClockFreqMode;
    OpellaConfig.SetJTAGClockFreqMode( eJTAGClockFreqMode );
    // 2) If Fixed Frequency Mode then store the selected frequency also
    if( Fixed == eJTAGClockFreqMode )
    {
        OpellaConfig.SetFixedJTAGFrequencyInKHz( m_nSelectedJTAGFreqInKHz );
    }

    // Save Target Voltage
    OpellaConfig.SetHotPlugVoltage( m_fTargetVoltage );

    return true;

}

/****************************************************************************
  Function   : GetSelectFixedJTAGClockFreqInKHz()
  Engineer   : Jiju George T
  Input      : void
  Output     : int - frequency in KHz for selected item in JTAG Frequency combo
  Description: Return frequency in KHz corresponding to the item selected in JTAG Frequency
               combo.
  Date           Initials    Description
  2007-08-02       JG          Initial
  20-Feb-2008      SJ          Added support for intermediate frequencies of 
                               1kHz,5kHz,10kHz,20kHz,50kHz and 70kHz
  27-Feb-2008      SJ          Bug in Getting JTAG Frequency fixed.
  24-Mar-2008	   SJ		   Solved the problem for writing to file with 
							   frequencies less than 1MHz.
****************************************************************************/
int CTargetConnectionDlg::GetSelectFixedJTAGClockFreqInKHz()
{
    int nSelIndex = m_JTAGFreqCombo.GetCurSel();
    CString csSelectFixedJTAGClockFreq;
    char* pszSelectFixedJTAGClockFreq = 0;

    csSelectFixedJTAGClockFreq.Empty();
    m_JTAGFreqCombo.GetLBText( nSelIndex, csSelectFixedJTAGClockFreq );
    pszSelectFixedJTAGClockFreq = csSelectFixedJTAGClockFreq.GetBuffer( csSelectFixedJTAGClockFreq.GetLength() + 1 );
    double fSelFreq = atof( pszSelectFixedJTAGClockFreq );
    
	if(nSelIndex >= 0xF) //0xF corresponds to index of 1MHz
		return fSelFreq * 1000;
	else
		return fSelFreq;

    //return nSelIndex * STEP_JTAG_FREEQ_KHZ;
}

/****************************************************************************
  Function   : GetSelectedOpellaDetails()
  Engineer   : Jiju George T
  Input      : tyUSBDeviceInstance - [OUT] Details of the selected Opella is exists
  Output     : bool                - true if any Opellas are detected. false otherwise.
  Description: Returns the details of the currently selected Opella.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
bool CTargetConnectionDlg::GetSelectedOpellaDetails(TyUSBDeviceInstance &tyUSBDeviceInstance)
{
    bool bRet = false;

    if( m_tyMultipleUSBDevices.ulNumberOfUSBDevices > 0 )
    {
        strcpy( tyUSBDeviceInstance.szOpellaXDSerialNumber,
                m_tyMultipleUSBDevices.szOpellaXDSerialNumber[m_tyMultipleUSBDevices.ucSelectedDevice]);
        bRet = true;
    }

    return bRet;
}

/****************************************************************************
  Function   : OnSelchangeJtagFreqCombo()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user changes frequency.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnSelchangeJtagFreqCombo()
{
    m_nSelectedJTAGFreqInKHz = GetSelectFixedJTAGClockFreqInKHz();
}

/****************************************************************************
  Function   : FillTargetVoltageCombo()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Fills the target voltage combo with possible values.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::FillTargetVoltageCombo()
{
    m_TargetVoltageCombo.ResetContent();

    for( int nIndex=0; nIndex < g_nTargetVoltageCount; ++nIndex )
    {

        m_TargetVoltageCombo.AddString(
                            GetVoltageString( g_TargetVoltageList[nIndex] ));
    }

    // Select user configured voltage
    int nSelIndex =  m_TargetVoltageCombo.FindStringExact( 0, GetVoltageString( m_fTargetVoltage ));
    if( CB_ERR != nSelIndex )
    {
        m_TargetVoltageCombo.SetCurSel( nSelIndex );
    }

}

/****************************************************************************
  Function   : GetVoltageString()
  Engineer   : Jiju George T
  Input      : fVoltage - Voltage to be converted
  Output     : CString  - Converted voltage string
  Description: Converts the given voltage to string representation in proper display format.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
CString CTargetConnectionDlg::GetVoltageString( float fVoltage )
{
    CString csVoltage;
    if( 0.0f == fVoltage )
    {
        csVoltage = _T( "Track target" );
    }
    else
    {
        csVoltage.Format( _T( "%1.1f V" ), fVoltage );
    }
    
    return csVoltage;
}

/****************************************************************************
  Function   : OnSelchangeTargetVoltage() 
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user changes the target voltage for hotplug.
  Date           Initials    Description
  2007-08-02       JG          Initial
****************************************************************************/
void CTargetConnectionDlg::OnSelchangeTargetVoltage() 
{
    m_fTargetVoltage =  g_TargetVoltageList[m_TargetVoltageCombo.GetCurSel()];  
}

/****************************************************************************
  Function   : GetFrequencyMode() 
  Engineer   : Jeenus Chalattu Kunnath
  Input      : void
  Output     : JTAGClockFreqMode - Frequency mode
  Description: Returns Frequency mode
  Date           Initials    Description
  2008-06-25       JCK          Initial
****************************************************************************/

JTAGClockFreqMode CTargetConnectionDlg::GetFrequencyMode() 
{
   return ( JTAGClockFreqMode )m_nJTAGClockFreqMode;
}

/****************************************************************************
  Function   : GetFrequencyMode() 
  Engineer   : Jeenus Chalattu Kunnath
  Input      : void
  Output     : int  - Frequency value in KHZ.
  Description: Returns Frequency value
  Date           Initials    Description
  2008-06-25       JCK          Initial
****************************************************************************/

int CTargetConnectionDlg::GetFrequencyValue() 
{
   return m_nSelectedJTAGFreqInKHz;
}

/****************************************************************************
     Function: UpdateUI()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This function will update user interface with member variable, 
               This function is called by the OnShowWindow member function of 
               the base class.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CTargetConnectionDlg::UpdateUI()
{
    // Populate the Fixed JTAG Frequencies in JTAG Fixed Frequency Combo
    FillJTAGFixedFrequencies();

    // Fill Target Voltage values
    FillTargetVoltageCombo();

    // Select the JTAG Clock Frequency Mode
    SlelectJTAGClockFrequencyMode();

    // display detected Opellas in list
    DisplayConnectedOpellas();

    // Select previously selected Opella in the list if possible
    SelectPrevSelectedOpella();

    // Exchange data in member to controls
    UpdateData(FALSE);
}

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This function will initialize GUI data variables using the 
               configuration object as argument. This function is called by
               Constructor of the class and base class while loading configuration.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CTargetConnectionDlg::InitializeUIData( COpellaConfig &OpellaConfig )
{
    // Get Selected JTAG Frequency
    m_nSelectedJTAGFreqInKHz = OpellaConfig.GetFixedJTAGFrequencyInKHz();
    
    // Get selected JTAG frequency mode
    m_nJTAGClockFreqMode = OpellaConfig.GetJTAGClockFreqMode();
    
    // Get Target Voltage for Hot Plug
    m_fTargetVoltage = OpellaConfig.GetHotPlugVoltage();
    
    // Exchange data to controls if there is a window exists. 
    // It necessary because we are calling UpdateData() in ApplyChanges function. 
    if( IsWindow( m_hWnd ))
    {
        UpdateData( FALSE );
    }

    //Calling base class Initializations. 
    COpellaConfigPageBase::InitializeUIData( OpellaConfig );
}
