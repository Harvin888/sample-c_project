/****************************************************************************
       Module: FamilyDatabase.cpp
     Engineer: Jiju George T
  Description: This file contains the implementation of C
               FamilyDatabase class
Date           Initials    Description
24-Jul-2007      JG           Initial
19-Feb-2008		  SJ		      Modified for getting the XML file from the RDI 
                              installation path.
****************************************************************************/

#include "stdafx.h"
#include "FamilyDatabase.h"

#include "OpellaARMConfigDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define XML_FILE_NAME "\\familyarm.xml"

CFamilyDatabase* CFamilyDatabase::m_pSingleInstance = 0;

/****************************************************************************
     Function: Constructor
     Engineer: Jiju George T
	 Input   : void
	 Output  : void
  Description:  
Date           Initials    Description
24-Jul-2007       JG          Initial
19-Feb-2008		   SJ		      Added function to get the RDI DLL path
****************************************************************************/


CFamilyDatabase::CFamilyDatabase() : 
    m_pFamilyDocument(0), m_pFamilyRoot(0)
{
    HRESULT hResult = ::CoCreateInstance( __uuidof( MSXML::DOMDocument ), 0,
                                          CLSCTX_SERVER, __uuidof( MSXML::IXMLDOMDocument),
                                        ( void** )&m_pFamilyDocument );

	
	CString csXMLFile;
   COpellaARMConfigDlg::GetARMRDIDriverPath( csXMLFile );
   csXMLFile += XML_FILE_NAME;

	if( FAILED( hResult ))
    {
         //AfxMessageBox( _T("Unable to load MSXML Parser. Make sure MSXML is propertly installed."));
         MessageBox(NULL,"Unable to load MSXML Parser.Make sure MSXML is propertly installed.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
         return;
    }

    // Try to load the Family database XML
    //if( !m_pFamilyDocument->load( _T( "familyarm.xml" )))
    if( !m_pFamilyDocument->load((const _variant_t)csXMLFile ))
    {
         //AfxMessageBox( _T("Unable to load familyarm.xml"));
         MessageBox(NULL,"Unable to load familyarm.xml","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
         return;
    }

    // Get Root from the Document
    m_pFamilyDocument->get_documentElement( &m_pFamilyRoot );
}

/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
	 Input   : void
	 Output  : void
  Description: 
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
CFamilyDatabase::~CFamilyDatabase()
{
    // Release all XML resources
    if( m_pFamilyRoot )
    {
        m_pFamilyRoot->Release();
        m_pFamilyRoot = 0;
    }

    if( m_pFamilyDocument )
    {
        m_pFamilyDocument->Release();
        m_pFamilyDocument = 0;
    }
}
 
/****************************************************************************
     Function: CFamilyDatabase::GetInstance()
     Engineer: Jiju George T
	 Input   : void
	 Output  : CFamilyDatabase& - singleton instance
  Description: Returns a singleton instance of the class
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
CFamilyDatabase& CFamilyDatabase::GetInstance()
{
    if( 0 == m_pSingleInstance )
    {
        m_pSingleInstance = new CFamilyDatabase();
    }

    return *m_pSingleInstance;
}

/****************************************************************************
     Function: Destroy()
     Engineer: Jiju George T
	 Input   : void
	 Output  : void
  Description: Destroys the singleton instance.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CFamilyDatabase::Destroy()
{
    if( m_pSingleInstance )
    {
        delete m_pSingleInstance;
        m_pSingleInstance = 0;
    }
}

/****************************************************************************
     Function: GetListOfDevices()
     Engineer: Jiju George T
	 Input   : ListOfCores - List to fill the list of cores in database
	 Output  : void
  Description: Returns the list of ARM devices in the database.
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
void CFamilyDatabase::GetListOfDevices( CStringArray &ListOfCores )
{
    if( 0 != m_pFamilyRoot )
    {
        // Parse the <Processor> nodes and get device name
        MSXML::IXMLDOMNodeListPtr ProcessorNodeList;
        ProcessorNodeList = m_pFamilyRoot->GetchildNodes();

        int nCount = ProcessorNodeList->Getlength();
        for( int nIndex = 0; nIndex < nCount; ++nIndex )
        {
            MSXML::IXMLDOMNodePtr ProcessorNode = ProcessorNodeList->Getitem( nIndex );
            MSXML::IXMLDOMNamedNodeMapPtr ProcessorAttrMap = ProcessorNode->Getattributes();
            MSXML::IXMLDOMNodePtr DeviceAttr = ProcessorAttrMap->getNamedItem( "Device" );
            _variant_t vDeviceName = DeviceAttr->GetnodeValue();
            CString csDeviceName = vDeviceName.bstrVal;
            ListOfCores.Add( csDeviceName );
       }
    }
}

/****************************************************************************
     Function: CheckArmArchitVer()
     Engineer: Jeenus C.K.
	 Input   : unsigned long* pulArmVer - To return the ARM architecture version
			 : CString CurrDeviceName
	 Output  : void
  Description: Returns the status of ARM architecture version.
Date           Initials    Description
24-Nov-2009      JCK         Initial
****************************************************************************/
void CFamilyDatabase::CheckArmArchitVer( CString* pulArmVer, CString CurrDeviceName )
{
	*pulArmVer = "0";
	if( 0 != m_pFamilyRoot )
    {
        // Parse the <Processor> nodes and get device name
        MSXML::IXMLDOMNodeListPtr ProcessorNodeList;
        ProcessorNodeList = m_pFamilyRoot->GetchildNodes();
		
        int nCount = ProcessorNodeList->Getlength();
        for( int nIndex = 0; nIndex < nCount; ++nIndex )
        {
            MSXML::IXMLDOMNodePtr ProcessorNode = ProcessorNodeList->Getitem( nIndex );
            MSXML::IXMLDOMNamedNodeMapPtr ProcessorAttrMap = ProcessorNode->Getattributes();
            MSXML::IXMLDOMNodePtr DeviceAttr = ProcessorAttrMap->getNamedItem( "Device" );
            _variant_t vDeviceName = DeviceAttr->GetnodeValue();
            CString csDeviceName = vDeviceName.bstrVal;
            
			if (csDeviceName == CurrDeviceName)
			{
				MSXML::IXMLDOMNodeListPtr ProcessorChildNodeList;
				ProcessorChildNodeList = ProcessorNode->GetchildNodes();
				int NumNodes = ProcessorChildNodeList->Getlength();				
				
				for (int NodeCount = 0; NodeCount < NumNodes; NodeCount++)
				{
					MSXML::IXMLDOMNodePtr ProcessorChildNode = ProcessorChildNodeList->Getitem(NodeCount);
					_bstr_t nodename = ProcessorChildNode->GetnodeName();
					if (NULL == strcmp(nodename, "ArchitectureVer"))
					{
						_variant_t vValue = ProcessorChildNode->GetnodeTypedValue();	
						CString csArchVer = vValue.bstrVal;						
						*pulArmVer = csArchVer;
						break;
					}
					
				}
				
				break;				
			}
		}
    }
}

/****************************************************************************
     Function: CheckCoresightCompliant()
     Engineer: Jeenus C.K.
	 Input   : CString CurrDeviceName - 
	 Output  : void
  Description: To check the coresight compliant or not.
Date           Initials    Description
24-Nov-2009      JCK         Initial
****************************************************************************/
bool CFamilyDatabase::CheckCoresightCompliant(CString CurrDeviceName)
{
	bool bCorsightCompliant = FALSE;

	MSXML::IXMLDOMNodePtr DeviceNode;
	GetSelectedNodePtr( "CoresightCompliant", CurrDeviceName, DeviceNode);	
	if (DeviceNode)
	{
		_variant_t vValue = DeviceNode->GetnodeTypedValue();	
		CString csCorsightCompliant = vValue.bstrVal;						
		if ("1" == csCorsightCompliant)
		{
			bCorsightCompliant = TRUE;
		}
	}		

    return bCorsightCompliant;
}

/****************************************************************************
     Function: CheckTICore()
     Engineer: Jeenus C.K.
	 Input   : CString CurrDeviceName - The name of the device selected.
	 Output  : void
  Description: To check the current device is TI device or not.
Date           Initials    Description
24-Nov-2009      JCK         Initial
****************************************************************************/
bool CFamilyDatabase::CheckTICore(CString CurrDeviceName)
{
	bool bTICore = FALSE;

	MSXML::IXMLDOMNodePtr DeviceNode;
	GetSelectedNodePtr( "TiDevice", CurrDeviceName, DeviceNode);	
	if (DeviceNode)
	{
		_variant_t vValue = DeviceNode->GetnodeTypedValue();	
		CString csTICore = vValue.bstrVal;						
		if ("1" == csTICore)
		{
			bTICore = TRUE;
		}
	}		

    return bTICore;
}

/****************************************************************************
	Function: GetSecondaryPortNum()
	Engineer: Jeenus C.K.
	Input   : CString* csSecondaryPort - To return secondary port number.
			: CString CurrDeviceName - The name of the device selected.
	Output  : void
 Description: To read the values of Secondary Port number of the TI devces.
Date           Initials    Description
05-May-2010		JCK         Initial
****************************************************************************/
void CFamilyDatabase::GetSecondaryPortNum( 
		CString* csSecondaryPort,
		CString CurrDeviceName )
{
	MSXML::IXMLDOMNodePtr ProcessorChildNode;

	GetSelectedNodePtr( "SecondaryTapNum", CurrDeviceName, ProcessorChildNode);	
	if (ProcessorChildNode)
	{
		_variant_t vValue = ProcessorChildNode->GetnodeTypedValue();													
		*csSecondaryPort = vValue.bstrVal;
	}
	else
	{
		*csSecondaryPort = "0";
	}
	
}

/****************************************************************************
	Function: GetDapPcSupport()
	Engineer: Jeenus C.K.
	Input   : CString* csDapPcControl - To return dap pc control is supported
			:						  - or not.
			: CString CurrDeviceName - The name of the device selected.
	Output  : void
 Description: To read DAP PC is supported or not.
Date           Initials    Description
27-May-2010		JCK         Initial
****************************************************************************/
void CFamilyDatabase::GetDapPcSupport( 
		CString* csDapPcControl,
		CString CurrDeviceName )
{
	MSXML::IXMLDOMNodePtr ProcessorChildNode;

	GetSelectedNodePtr( "DAPPCControl", CurrDeviceName, ProcessorChildNode);	
	if (ProcessorChildNode)
	{
		_variant_t vValue = ProcessorChildNode->GetnodeTypedValue();													
		*csDapPcControl = vValue.bstrVal;
	}
	else
	{
		*csDapPcControl = "0";
	}
	
}


/****************************************************************************
     Function: CheckDevicesOnChain()
     Engineer: Roshan T R
	 Input   : unsigned long* pulArmVer - To return the ARM architecture version
	 Output  : void
  Description: Returns the Number of devices on scan chain
Date           Initials    Description
05-Apr-2010    RTR         Initial
****************************************************************************/
void CFamilyDatabase::CheckDevicesOnChain( 
		TyJtagScanChain* tyJtagScanChain, 
		CString CurrDeviceName )
{
	tyJtagScanChain->ptyJtagNode->bARMCore = TRUE;
	tyJtagScanChain->ptyJtagNode->ucDeviceNumber = 0;
	tyJtagScanChain->ptyJtagNode->ucIRLength = 4;
	tyJtagScanChain->ucNumJtagNodes = 1;
	
	if( 0 != m_pFamilyRoot )
    {
        // Parse the <Processor> nodes and get device name
        MSXML::IXMLDOMNodeListPtr ProcessorNodeList;
        ProcessorNodeList = m_pFamilyRoot->GetchildNodes();
		
        int nCount = ProcessorNodeList->Getlength();
        for( int nIndex = 0; nIndex < nCount; ++nIndex )
        {
            MSXML::IXMLDOMNodePtr ProcessorNode = ProcessorNodeList->Getitem( nIndex );
            MSXML::IXMLDOMNamedNodeMapPtr ProcessorAttrMap = ProcessorNode->Getattributes();
            MSXML::IXMLDOMNodePtr DeviceAttr = ProcessorAttrMap->getNamedItem( "Device" );
            _variant_t vDeviceName = DeviceAttr->GetnodeValue();
            CString csDeviceName = vDeviceName.bstrVal;
            
			if (csDeviceName == CurrDeviceName)
			{
				MSXML::IXMLDOMNodeListPtr ProcessorChildNodeList;
				ProcessorChildNodeList = ProcessorNode->GetchildNodes();
				int NumNodes = ProcessorChildNodeList->Getlength();				
				
				for (int NodeCount = 0; NodeCount < NumNodes; NodeCount++)
				{
					MSXML::IXMLDOMNodePtr ProcessorChildNode = ProcessorChildNodeList->Getitem(NodeCount);
					_bstr_t nodename = ProcessorChildNode->GetnodeName();
					if (NULL == strcmp(nodename, "MultiCore"))
					{
						MSXML::IXMLDOMNodeListPtr MultiCoreList;
						MultiCoreList = ProcessorChildNode->GetchildNodes();
						if (MultiCoreList)
						{
							int nCount = MultiCoreList->Getlength();
							tyJtagScanChain->ucNumJtagNodes = nCount;

							for(int i = 0; i < nCount; i++)
							{
								MSXML::IXMLDOMNodePtr CoreNode = MultiCoreList->Getitem( i );
								MSXML::IXMLDOMNamedNodeMapPtr CoreAttrMap = 0;								
								MSXML::IXMLDOMNodePtr CoreChildNode;
								if (CoreNode)
								{
									CoreNode->get_attributes( &CoreAttrMap );
									CoreChildNode = CoreAttrMap->getNamedItem( (_bstr_t)"Number" );
									if(CoreChildNode)
									{
										VARIANT vNumber;
                                        CoreChildNode->get_nodeValue( &vNumber );
										unsigned char ucDevNum;
										CString csNumber = vNumber.bstrVal;
										ucDevNum = atoi(csNumber);
										tyJtagScanChain->ptyJtagNode[i].ucDeviceNumber = ucDevNum;
                                        VariantClear( &vNumber ); 
									}

									CoreChildNode = CoreAttrMap->getNamedItem( (_bstr_t)"IsARMCore" );
									if(CoreChildNode)
									{
										_variant_t vNumber = CoreChildNode->GetnodeValue();
										CString csARM = vNumber.bstrVal;
										if (csARM == "true")
										{
											tyJtagScanChain->ptyJtagNode[i].bARMCore = TRUE;
										}
										else
										{
											tyJtagScanChain->ptyJtagNode[i].bARMCore = FALSE;
										}
									}

									CoreChildNode = CoreAttrMap->getNamedItem( (_bstr_t)"IRlength" );
									if(CoreChildNode)
									{
										_variant_t vNumber = CoreChildNode->GetnodeValue();
										unsigned char ucIRNumber;
										CString cIRLength = vNumber.bstrVal;
										ucIRNumber = atoi(cIRLength);
										
										tyJtagScanChain->ptyJtagNode[i].ucIRLength = ucIRNumber;
									}									
								}
								
								 
							}							
						}

						break;
						
					}
					
				}
				
				break;				
			}
		}
    }
}

/****************************************************************************
	Function: GetCoresightConfigVal()
	Engineer: Jeenus C.K.
	Input   : CString pcsDebugBaseAddr - To return the debug base address.
			  CString pcsDebugApIndex - To return the debug AP index.
			  CString pcsMemApIndex - To return the memory AP index
			  CString pcsJtagApIndex - To return the JTAG AP index
			: CString CurrDeviceName - The name of the device selected.
	Output  : void
 Description: To read the configuration values of coresight components.
Date           Initials    Description
         Initial
****************************************************************************/
void CFamilyDatabase::GetCoresightConfigVal( 
		CString* pcsDebugBaseAddr,
		CString* pcsDebugApIndex,
		CString* pcsMemApIndex,
		CString* pcsJtagApIndex,
		CString* pcsUseMemApMemAccess,
		CString CurrDeviceName )
{
	*pcsDebugBaseAddr = "0";
	*pcsDebugApIndex  = "0";
	*pcsMemApIndex	= "0";
	*pcsJtagApIndex	= "0";	
	*pcsUseMemApMemAccess = "0";

	MSXML::IXMLDOMNodePtr ProcessorChildNode;
	GetSelectedNodePtr( "CoresightConfig", CurrDeviceName, ProcessorChildNode);	
	MSXML::IXMLDOMNodeListPtr CoresightConfig;
	_bstr_t nodename;
	
	if (ProcessorChildNode)
	{
		CoresightConfig = ProcessorChildNode->GetchildNodes();
		if (CoresightConfig)
		{	
			int i;
			int nCount = CoresightConfig->Getlength();
			MSXML::IXMLDOMNodePtr CoresightConfigNode;
			
			for(i = 0; i < nCount; i++)
			{
				CoresightConfigNode = CoresightConfig->Getitem(i);
				
				nodename = CoresightConfigNode->GetnodeName();
				if (NULL == strcmp(nodename, "DebugBaseAddress"))
				{
					_variant_t vValue = CoresightConfigNode->GetnodeTypedValue();	
					CString csDebugBaseAddr  = vValue.bstrVal;					
					unsigned long ulBaseAddr;

					CHexUtils::GetValueFromHexString(csDebugBaseAddr,ulBaseAddr);
					*pcsDebugBaseAddr = CHexUtils::GetHexString(ulBaseAddr);
					break;
				}
			}
			
			for(i = 0; i < nCount; i++)
			{
				CoresightConfigNode = CoresightConfig->Getitem(i);
				
				nodename = CoresightConfigNode->GetnodeName();
				if (NULL == strcmp(nodename, "DebugApIndex"))
				{
					_variant_t vValue = CoresightConfigNode->GetnodeTypedValue();	
					CString csDebugApIndex  = vValue.bstrVal;					
					unsigned long ulDebugApIndex;
					
					CHexUtils::GetValueFromHexString(csDebugApIndex,ulDebugApIndex);
					*pcsDebugApIndex = CHexUtils::GetHexString(ulDebugApIndex);					
					break;
				}
			}
			
			for(i = 0; i < nCount; i++)
			{
				CoresightConfigNode = CoresightConfig->Getitem(i);
				
				nodename = CoresightConfigNode->GetnodeName();
				if (NULL == strcmp(nodename, "MemoryApIndex"))
				{
					_variant_t vValue = CoresightConfigNode->GetnodeTypedValue();	
					CString csMemApIndex  = vValue.bstrVal;					
					unsigned long ulMemApIndex;
					
					CHexUtils::GetValueFromHexString(csMemApIndex,ulMemApIndex);
					*pcsMemApIndex = CHexUtils::GetHexString(ulMemApIndex);						
					break;
				}
			}
			
			
			for(i = 0; i < nCount; i++)
			{
				CoresightConfigNode = CoresightConfig->Getitem(i);
				
				nodename = CoresightConfigNode->GetnodeName();
				if (NULL == strcmp(nodename, "JtagApIndex"))
				{
					_variant_t vValue = CoresightConfigNode->GetnodeTypedValue();	
					CString csJtagApIndex  = vValue.bstrVal;					
					unsigned long ulJtagApIndex;
					
					CHexUtils::GetValueFromHexString(csJtagApIndex,ulJtagApIndex);
					*pcsJtagApIndex = CHexUtils::GetHexString(ulJtagApIndex);		
					break;
				}
			}

			for(i = 0; i < nCount; i++)
			{
				CoresightConfigNode = CoresightConfig->Getitem(i);
				
				nodename = CoresightConfigNode->GetnodeName();
				if (NULL == strcmp(nodename, "UseMemApForMemAccess"))
				{
					_variant_t vValue = CoresightConfigNode->GetnodeTypedValue();	
					CString csUseMemApMemAccess  = vValue.bstrVal;					
					unsigned long ulUseMemApMemAccess;
					
					CHexUtils::GetValueFromHexString(csUseMemApMemAccess,ulUseMemApMemAccess);
					*pcsUseMemApMemAccess = CHexUtils::GetHexString(ulUseMemApMemAccess);		
					break;
				}
			}
			
		}
	}

}

/****************************************************************************
	Function: GetCoresightConfigVal()
	Engineer: Jeenus C.K.
	Input   : CString csInternalName - To return the internal name of the 
									   currently selected device.
			: CString CurrDeviceName - The name of the device selected.
	Output  : void
 Description: To read the configuration values of coresight components.
Date           Initials    Description
05-May-2010		JCK         Initial
****************************************************************************/
void CFamilyDatabase::GetInternalName( 
		CString* csInternalName,
		CString CurrDeviceName )
{
	MSXML::IXMLDOMNodePtr ProcessorChildNode;
	GetSelectedNodePtr( "InternalName", CurrDeviceName, ProcessorChildNode);	
	if (ProcessorChildNode)
	{
		_variant_t vValue = ProcessorChildNode->GetnodeTypedValue();													
		*csInternalName = vValue.bstrVal;
	}
	else
	{
		*csInternalName = CurrDeviceName;
	}
	
}

/****************************************************************************
	Function: GetSelectedNodePtr()
	Engineer: Jeenus C.K.
	Input   : const char* szSelectedNode - To node name.
			: CString CurrDeviceName - The name of the device selected.
			: MSXML::IXMLDOMNodePtr& NodePtr - To return the node pointer.
	Output  : void
 Description: To return the node pointer..
Date           Initials    Description
05-May-2010		JCK         Initial
****************************************************************************/
void CFamilyDatabase::GetSelectedNodePtr( const char* szSelectedNode, 
										  CString CurrDeviceName,
										  MSXML::IXMLDOMNodePtr& NodePtr)
{	
	if( 0 != m_pFamilyRoot )
    {
        // Parse the <Processor> nodes and get device name
        MSXML::IXMLDOMNodeListPtr ProcessorNodeList;
        ProcessorNodeList = m_pFamilyRoot->GetchildNodes();
		
        int nCount = ProcessorNodeList->Getlength();
        for( int nIndex = 0; nIndex < nCount; ++nIndex )
        {
            MSXML::IXMLDOMNodePtr ProcessorNode = ProcessorNodeList->Getitem( nIndex );
            MSXML::IXMLDOMNamedNodeMapPtr ProcessorAttrMap = ProcessorNode->Getattributes();
            MSXML::IXMLDOMNodePtr DeviceAttr = ProcessorAttrMap->getNamedItem( "Device" );
            _variant_t vDeviceName = DeviceAttr->GetnodeValue();
            CString csDeviceName = vDeviceName.bstrVal;
            
			if (csDeviceName == CurrDeviceName)
			{
				MSXML::IXMLDOMNodeListPtr ProcessorChildNodeList;
				MSXML::IXMLDOMNodePtr NodePtrTemp;

				ProcessorChildNodeList = ProcessorNode->GetchildNodes();
				int NumNodes = ProcessorChildNodeList->Getlength();				
				
				for (int NodeCount = 0; NodeCount < NumNodes; NodeCount++)
				{
					NodePtrTemp = ProcessorChildNodeList->Getitem(NodeCount);
					_bstr_t nodename = NodePtrTemp->GetnodeName();
					if (NULL == strcmp(nodename, szSelectedNode))
					{
						NodePtr  = NodePtrTemp;
						break;
					}
					
				}

				if (NodeCount == NumNodes)
				{
					NodePtr = NULL;
				}
				
				break;				
			}
		}
    }
	
}

/****************************************************************************
     Function: CheckCacheInvalidationNeeded()
     Engineer: Suraj S
	 Input   : CString CurrDeviceName - 
	 Output  : BOOL - TRUE - if CacheInvlaidation workaround needed
					  FALSE - if CacheInvlaidation workaround not needed 
  Description: Returns whether CacheInvlaidation workaround needed or not
Date           Initials    Description
11-Mar-2011      SJ         Initial
****************************************************************************/
bool CFamilyDatabase::CheckCacheInvalidationNeeded(CString CurrDeviceName)
{
	bool bCacheInvalidationNeeded = FALSE;

	MSXML::IXMLDOMNodePtr DeviceNode;
	GetSelectedNodePtr( "CacheInvalidationNeeded", CurrDeviceName, DeviceNode);	
	if (DeviceNode)
	{
		_variant_t vValue = DeviceNode->GetnodeTypedValue();	
		CString csCacheInvalidationNeeded = vValue.bstrVal;						
		if ("1" == csCacheInvalidationNeeded)
		{
			bCacheInvalidationNeeded = TRUE;
		}
	}		

    return bCacheInvalidationNeeded;
}