/****************************************************************************
       Module: AdvancedDlg.cpp
     Engineer: Jiju George T
  Description: This file contains the implementation of CAdvancedDlg class
Date           Initials    Description
01-Aug-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
21-May-2008       SJ         Added function for enabling Safe Non Vector Address 
****************************************************************************/
#include "stdafx.h"
#include "OpellaConfig.h"
#include "AdvancedDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int MIN_TARGET_EXE_POL_RATE = 1;
const int MAX_TARGET_EXE_POL_RATE = 100;

/****************************************************************************
     Function: Constructor.
     Engineer: Jiju George T
     Input   : OpellaConfig - contains the configuration options
     Output  : void
  Description: 
Date           Initials    Description
01-Aug-2007      JG          Initial
27-Feb-2008      SPC         Initializations are changed to another function.
****************************************************************************/
CAdvancedDlg::CAdvancedDlg( COpellaConfig &OpellaConfig ) :
    COpellaConfigPageBase( CAdvancedDlg::IDD ),
    m_OpellaConfig( OpellaConfig )

{
    //{{AFX_DATA_INIT(CAdvancedDlg)
    m_nTargetExecutionPollingRate = 0;
    m_csCacheCleanAddress = _T("");
    m_csSafeNonVectorAddress = _T("");
    m_nSemiHostingSupport = -1;
	m_bSafeVectorAddressEnable = FALSE;
	//}}AFX_DATA_INIT

    m_psp.dwFlags &= (~PSP_HASHELP);

    InitializeUIData(m_OpellaConfig);
}

/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: 
Date           Initials    Description
01-Aug-2007      JG          Initial
****************************************************************************/
CAdvancedDlg::~CAdvancedDlg()
{
}

/****************************************************************************
    Function : DoDataExchange()
     Engineer: Jiju George T
     Input   : pDX  - Holds the exchange information
     Output  : void
  Description: Does the data exchange between controls and corresponding member variables
Date           Initials    Description
01-Aug-2007       JG         Initial
****************************************************************************/
void CAdvancedDlg::DoDataExchange(CDataExchange* pDX)
{
    CResizablePage::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CAdvancedDlg)
    DDX_Text(pDX, IDC_EXECUTION_POLING_RATE, m_nTargetExecutionPollingRate);
    DDX_Text(pDX, IDC_CACHE_CLEAN_ADDRESS, m_csCacheCleanAddress);
    DDX_Text(pDX, IDC_SAFE_NON_VECTOR_ADDRESS, m_csSafeNonVectorAddress);
    DDX_Radio(pDX, IDC_ARM_SEMIHOSTING_RADIO, m_nSemiHostingSupport);
	DDX_Check(pDX, IDC_ENABLE_SAFE_NV_ADDR, m_bSafeVectorAddressEnable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAdvancedDlg, COpellaConfigPageBase)
    //{{AFX_MSG_MAP(CAdvancedDlg)
	ON_BN_CLICKED(IDC_ENABLE_SAFE_NV_ADDR, OnEnableSafeNvAddrClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/****************************************************************************
     Function: OnInitDialog()
     Engineer: Jiju George T
     Input   : void
     Output  : BOOL - controls the focus on control
  Description: Called when this page is shown for first time
               Does the initialization job. 
               Initials    Description
Date          
01-Aug-2007      JG          Initial
27-Feb-2008      SPC         UI Updates are doing in another function and 
                             it will call on OnShowWindow function.
****************************************************************************/
BOOL CAdvancedDlg::OnInitDialog()
{
    CResizablePage::OnInitDialog();
	(GetDlgItem( IDC_SAFE_NON_VECTOR_ADDRESS))->EnableWindow( FALSE );
    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

/****************************************************************************
     Function: ApplyChanges()
     Engineer: Jiju George T
     Input   : OpellaConfig - Configuration object to save configurations 
     Output  : bool -  Return false if some required information is not
               filled by user (In this case notification about
               the error should be given to user). Else return true.
  Description: This function is called by the parent when user presses Apply.OK button.
               All the user configuration in the page should be saved to the passed
               COpellaConfig object.
Date           Initials    Description
01-Aug-2007       JG         Initial
****************************************************************************/
bool CAdvancedDlg::ApplyChanges( COpellaConfig &OpellaConfig )
{
    // Exchange data from controls to members
    if( IsWindow( m_hWnd ))
    {
        UpdateData();
    }

    bool bRet = true;
	OpellaConfig.EnableSafeNonVectorAddress( m_bSafeVectorAddressEnable ? TRUE : FALSE );
	
    do
    {
        bool bValidData = true;

        // Write cache clean address
        unsigned long ulAddress = 0x0;
        bValidData = CHexUtils::GetValueFromHexString( m_csCacheCleanAddress,
                                                       ulAddress );
        if( bValidData )
        {
            OpellaConfig.SetCacheCleanStartAddress( ulAddress );
        }
        else
        {
            //AfxMessageBox( _T( "Please enter a valid Data Cache Clean Start Address." ));
            MessageBox("Please enter a valid Data Cache Clean Start Address.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
            bRet = false;
            break;
        }

        ulAddress = 0x0;
        // Write safe non vector address
		if( m_bSafeVectorAddressEnable )
		{
			bValidData = CHexUtils::GetValueFromHexString( m_csSafeNonVectorAddress,
														   ulAddress );
			if( bValidData )
			{
				OpellaConfig.SetSafeNonVectorAddress( ulAddress );
			}
			else
			{
				//AfxMessageBox( _T( "Please enter a valid Safe Non-vector Address." ));
				MessageBox("Please enter a valid Safe Non-vector Address.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
				bRet = false;
				break;
			}

		}
        
        // Write target execution polling rate
        if( m_nTargetExecutionPollingRate >= MIN_TARGET_EXE_POL_RATE &&
            m_nTargetExecutionPollingRate  <= MAX_TARGET_EXE_POL_RATE )
        {
            OpellaConfig.SetTargetExecutionPollingRate( m_nTargetExecutionPollingRate );
        }
        else
        {
            //AfxMessageBox( _T( "Please enter a valid Target Execution Polling Rate." ));
            MessageBox("Please enter a valid Target Execution Polling Rate.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
            bRet = false;
            break;
        }

        // Store Semi-hosting support
        SemiHostingSupport eSemiHostingSupport = ( SemiHostingSupport )m_nSemiHostingSupport;
        OpellaConfig.SetSemiHostingSupport( eSemiHostingSupport );

    } while( 0 );


    return bRet;

}

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This function will initialize GUI data variables using the 
               configuration object as argument. This function is called by
               Constructor of the class and base class while loading configuration.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CAdvancedDlg::InitializeUIData( COpellaConfig &OpellaConfig )
{
    // Get configured value of cache clean address
    m_csCacheCleanAddress = CHexUtils::GetHexString( OpellaConfig.GetCacheCleanStartAddress());

    // Get configured value of safe non-vector address
    m_csSafeNonVectorAddress = CHexUtils::GetHexString( OpellaConfig.GetSafeNonVectorAddress());

    // Get configured value of target execution polling rate
    m_nTargetExecutionPollingRate = OpellaConfig.GetTargetExecutionPollingRate();

    // Initialize Semi hosting support
    m_nSemiHostingSupport = OpellaConfig.GetSemiHostingSupport();

    // Exchange data to controls if there is a window exists. 
    // It necessary because we are calling UpdateData() in ApplyChanges function. 
    if( IsWindow( m_hWnd ))
    {
        UpdateData( FALSE );
    }

    //Calling base class Initializations. 
    COpellaConfigPageBase::InitializeUIData( OpellaConfig );
}


/****************************************************************************
     Function: OnEnableSafeNvAddrClick()
     Engineer: Suraj S
     Input   : None 
     Output  : None
  Description: This function will handle the click of Safe Non vector check box.
Date           Initials    Description
21-May-2008       SJ         Initial
****************************************************************************/
void CAdvancedDlg::OnEnableSafeNvAddrClick() 
{
	UpdateData();
	if( m_bSafeVectorAddressEnable )
	{
		(GetDlgItem( IDC_SAFE_NON_VECTOR_ADDRESS))->EnableWindow( TRUE );
	}
	else
	{
		(GetDlgItem( IDC_SAFE_NON_VECTOR_ADDRESS))->EnableWindow( FALSE );
	}
	
}
