  /******************************************************************************
  Module      : OpellaARMConfigDlg.cpp 
  Engineer    : Jiju George T
  Description : This file contains the implementation of
                COpellaARMConfigDlg class.
  Date          Initials    Description
  24-Jul-2007     JG          Initial
  18-Feb-2008     SJ          Added support for getting the DLL installation 
                              path
  27-Feb-2008     SPC         Added Support for Load/Save config options.
******************************************************************************/

#include "stdafx.h"
#include "OpellaConfig.h"
#include "OpellaARMConfigDlg.h"
#include "FamilyDatabase.h"

//TOADD
#include "DeviceDlg.h"
#include "TargetConnectionDlg.h"
#include "TargetResetDlg.h"
#include "AdvancedDlg.h"
#include "DiagnosticsDlg.h"
#include "LoadSaveDlg.h"

#include "DriverMgr.h"

#define LAST_SAVED_CONFIG_FILE_NAME "\\OpellaXD"

/////////////////////////////////////////////////////////////////////////////
// COpellaARMConfigDlg

IMPLEMENT_DYNAMIC(COpellaARMConfigDlg, CTreePropSheetEx)

char COpellaARMConfigDlg::m_szARMRDIDriverPath[]  ;

/****************************************************************************
  Function   : Constructor
  Engineer   : Jiju George T
  Input      : pszCaption  - Title of the sheet
                  pParentWnd  - Parent window of the sheet  
                 iSelectPage - Number of the page in sheet to be selected initially 
  Output     : void
  Description: Creates an empty config object
  Date           Initials    Description
  24-Jul-2007       JG          Initial
****************************************************************************/
COpellaARMConfigDlg::COpellaARMConfigDlg( CWnd* pParentWnd, UINT iSelectPage)
:CTreePropSheetEx( GetTitleString(), pParentWnd, iSelectPage ),
m_OpellaConfig( COpellaConfig( GetLastSavedConfigFileName()))
{
    Init();
}

/****************************************************************************
  Function   : Destructor
  Engineer   : Jiju George T
  Input      : void 
  Output     : void
  Description: Destroys the Object
  Date           Initials    Description
  24-Jul-2007       JG          Initial
****************************************************************************/
COpellaARMConfigDlg::~COpellaARMConfigDlg()
{
    //TOADD
    delete m_pDeviceDlg;
    delete m_pTargetConnectionDlg;
    delete m_pTargetResetDlg;
    delete m_pAdvancedDlg;
    delete m_pDiagnosticDlg;
    delete m_pLoadSaveDlg;

    // Destroy the Family Database Singleton
    CFamilyDatabase::Destroy();
}

/****************************************************************************
  Function   : Init()
  Engineer   : Jiju George T
  Input      : void 
  Output     : void
  Description: Adds the necessary pages in the configuration dialog.
  Date           Initials    Description
  24-Jul-2007       JG          Initial
****************************************************************************/
void COpellaARMConfigDlg::Init()
{

    // Check whether the last saved configuration file loading was successful
    // If no then that means default settings will be displayed
    // So notify this to user
    if( m_OpellaConfig.IsDefault())
    {
        //AfxMessageBox( _T( "Unable to find last saved configuration. Using default configuration settings." ));
        MessageBox("Unable to find last saved configuration. Using default configuration settings.","Opella-XD for ARM",MB_OK|MB_ICONWARNING);
    }

    //TOADD
    // Add Device Page
    m_pDeviceDlg = new CDeviceDlg( m_OpellaConfig );
    AddPage(m_pDeviceDlg);

    // Add Target Connection page
    m_pTargetConnectionDlg = new CTargetConnectionDlg( m_OpellaConfig );
    AddPage(m_pTargetConnectionDlg);
    

	// Add Target Reset page
    m_pTargetResetDlg = new CTargetResetDlg( m_OpellaConfig, *m_pDeviceDlg );
    AddPage(m_pTargetResetDlg);

    // Add Advanced page
    m_pAdvancedDlg = new CAdvancedDlg( m_OpellaConfig );
    AddPage(m_pAdvancedDlg);

    // Add Diagnostics page
    m_pDiagnosticDlg = new CDiagnosticsDlg( m_OpellaConfig ,
                                            *m_pTargetConnectionDlg ,
                                            *m_pDeviceDlg);
    AddPage(m_pDiagnosticDlg);

    // Add Save / Load page
    m_pLoadSaveDlg = new CLoadSaveDlg( this );
    AddPage(m_pLoadSaveDlg);

    SetTreeViewMode( TRUE, FALSE, FALSE);
    SetEmptyPageText( _T( "Select a sub-page" ));
    SetTreeDefaultImages( IDB_EMPTY_IMAGE_LIST, 16, RGB( 255, 255, 255 ) );
}


BEGIN_MESSAGE_MAP(COpellaARMConfigDlg, CTreePropSheetEx)
    //{{AFX_MSG_MAP(COpellaARMConfigDlg)
    //}}AFX_MSG_MAP
    ON_BN_CLICKED(IDOK, OnOK)
END_MESSAGE_MAP()

/****************************************************************************
  Function   : OnInitDialog()
  Engineer   : Jiju George T
  Input      : void 
  Output     : BOOL - controls the focus on control
  Description: Called when the configuration dialog is shown for first time.
               Does the initialization job.
  Date           Initials    Description
  24-Jul-2007       JG          Initial
****************************************************************************/
BOOL COpellaARMConfigDlg::OnInitDialog()
{
    CTreePropSheetEx::OnInitDialog();

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

/****************************************************************************
  Function   : GetLastSavedConfigFileName()
  Engineer   : Jiju George T
  Input      : void 
  Output     : CString - Name of last saved configuration file
  Description: Returns the full path of the last saved configuration file with
               out extension.
  Date           Initials    Description
  24-Jul-2007       JG          Initial
  19-Feb-2008         SJ             Added function to get the RDI DLL path
****************************************************************************/
CString COpellaARMConfigDlg::GetLastSavedConfigFileName()
{
    CString csTitle;
   COpellaARMConfigDlg::GetARMRDIDriverPath( csTitle );
    csTitle += LAST_SAVED_CONFIG_FILE_NAME;
    return csTitle;
}

/****************************************************************************
  Function   : AddPage()
  Engineer   : Jiju George T
  Input      : pPage - Page to be added
  Output     : void
  Description: Adds a page to the sheet as well as the save page list.
  Date           Initials    Description
  24-Jul-2007       JG          Initial
****************************************************************************/
void COpellaARMConfigDlg::AddPage( COpellaConfigPageBase *pPage )
{
    m_PagesToSave.AddTail( pPage );
    CPropertySheet::AddPage( pPage );
}

/****************************************************************************
  Function   : OnOK()
  Engineer   : Jiju George T
  Input      : void
  Output     : void
  Description: Called when user press the OK button in the configuration dialog.
  Date           Initials    Description
  24-Jul-2007       JG          Initial
  25-Feb-2008       SPC         Saving of the configuration to file is moved to 
                                another function SaveConfigurationToFile()
****************************************************************************/
void COpellaARMConfigDlg::OnOK()
{
    bool bPageSaveSuccess = true;

    if( SaveConfigurationToFile(GetLastSavedConfigFileName()) )
    {
        //Save success so close the property sheet
        CPropertySheet::EndDialog( IDOK );
    }
}

/****************************************************************************
  Function   : GetTitleString()
  Engineer   : Jiju George T
  Input      : void
  Output     : CString - title with RDI DLL version
  Description: Returns the title to be displayed.
  Date           Initials    Description
  24-Jul-2007       JG          Initial
****************************************************************************/
CString COpellaARMConfigDlg::GetTitleString()
{
    CString csTitle = _T( "Ashling Opella-XD ARM RDI driver, ");
    csTitle += CDriverMgr::GetInstance().GetRDIDriverVersion();

    return csTitle;
}

/****************************************************************************
  Function   : SetARMRDIDriverPath()
  Engineer   : Suraj S
  Input      : char *pszARMRDIDriverpath - Pointer to DLL path
  Output     : void
  Description: Stores the DLL path to member variable m_szARMRDIDriverPath 
  Date           Initials    Description
  19-Feb-2008       SJ          Initial
****************************************************************************/
void COpellaARMConfigDlg::SetARMRDIDriverPath(const char *pszARMRDIDriverpath)
{
   strcpy( COpellaARMConfigDlg::m_szARMRDIDriverPath, pszARMRDIDriverpath);
}

/****************************************************************************
  Function   : GetARMRDIDriverPath()
  Engineer   : Suraj S
  Input      : CString - Pointer to store DLL Path 
  Output     : void
  Description: Copies the DLL path to called to the passed pointer 
  Date           Initials    Description
  19-Feb-2008       SJ          Initial
****************************************************************************/
void COpellaARMConfigDlg::GetARMRDIDriverPath( CString& csARMRDIDriverpath_o )
{
   if( 0 != m_szARMRDIDriverPath )
   {
      csARMRDIDriverpath_o = COpellaARMConfigDlg::m_szARMRDIDriverPath ;
   }
}

/****************************************************************************
  Function   : SaveConfigurationToFile()
  Engineer   : Suresh P.C
  Input      : CString - Name for new configuration file.
  Output     : bool - Return TRUE if Save is SUCCESS and FALSE otherwise.
  Description: Saves the RDI configuration to specified file. 
  Date           Initials    Description
  25-Feb-2008       SPC          Initial
****************************************************************************/
bool COpellaARMConfigDlg::SaveConfigurationToFile( CString szFilename )
{
    COpellaConfig OpellaConfig;
    
    // Try to save each page
    bool bPageSaveSuccess = true;
    int nPageCount = m_PagesToSave.GetCount();
    POSITION pos = m_PagesToSave.GetHeadPosition();
    for ( int nIndex = 0; nIndex < nPageCount; ++nIndex )
    {
        COpellaConfigPageBase *pOpellaConfigPageBase = m_PagesToSave.GetNext( pos );
        if( pOpellaConfigPageBase )
        {
            bPageSaveSuccess = pOpellaConfigPageBase->ApplyChanges( OpellaConfig );
            if( !bPageSaveSuccess )
            {
                // Some page has errors. So select that page and abort saving
                if( GetActiveIndex() != nIndex )
                {
                    SetActivePage( nIndex );
                }
                break;
            }
        }
    }
    
    if( bPageSaveSuccess )
    {
        // No error. So save the configuration to file 
        OpellaConfig.Save( szFilename);
    }
    
    return bPageSaveSuccess;
}

/****************************************************************************
  Function   : LoadConfigurationFile()
  Engineer   : Suresh P.C
  Input      : CString - Name of the configuration file.
  Output     : bool - Return TRUE if loading is SUCCESS and FALSE otherwise.
  Description: Load RDI configuration from a specified file. 
  Date           Initials    Description
  25-Feb-2008       SPC          Initial
****************************************************************************/
bool COpellaARMConfigDlg::LoadConfigurationFile(CString szFilename)
{
    COpellaConfig OpellaConf(szFilename);

    //Check weather configuration loaded successfully
    if ( OpellaConf.IsDefault() )
    {
        //loading was not successful. Exiting
        return false;
    }

    int nPageCount = m_PagesToSave.GetCount();
    POSITION pos = m_PagesToSave.GetHeadPosition();
    for ( int nIndex = 0; nIndex < nPageCount; ++nIndex )
    {
        COpellaConfigPageBase *pOpellaConfigPageBase = m_PagesToSave.GetNext( pos );
        if( pOpellaConfigPageBase )
        {
            pOpellaConfigPageBase->InitializeUIData( OpellaConf );
        }
    }

    return true;
}
