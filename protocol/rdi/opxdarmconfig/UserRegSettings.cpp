/******************************************************************************
  Module      : UserRegSettings.cpp
  Engineer    : Jeenus Chalattu Kunnath
  Description : Implementation of CUserRegSettings class
  Date           Initials    Description
  29-May-2008     JCK         Initial
  07-May-2010	  JCK		  Added support for Delay
  12-Aug-2010	  SJ		  Now uses singleton class instance to call RDI functions.
******************************************************************************/

#include "stdafx.h"
#include "AddItem.h"
#include "Util.h"
#include "UserRegSettings.h"
#include "AddDelay.h"
#include "CSingleton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserRegSettings dialog

ProcessorConfig *m_pProcCfgCopy;

ProcessorConfig m_pProcCfgCopy1;

#define WRITE_REG	"WriteReg"
#define DELAY		"Delay"

/****************************************************************************
     Function: Constructor.
     Engineer: Jeenus Chalattu Kunnath
     Input   : pParent - Parent window
     Output  : void
  Description: 
Date           Initials    Description
29-May-2008      JCK          Initial
****************************************************************************/
CUserRegSettings::CUserRegSettings(CWnd* pParent /*=NULL*/)
								  
								   : CDialog(CUserRegSettings::IDD, pParent)
{
	//ASSERT(pProcCfg);
   // m_pProcCfg  = pProcCfg;
    
	//{{AFX_DATA_INIT(CUserRegSettings)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/****************************************************************************
    Function : DoDataExchange()
     Engineer: Jeenus Chalattu Kunnath
     Input   : pDX  - Holds the exchange information
     Output  : void
  Description: Does the data exchange between controls and corresponding member variables
Date           Initials    Description
01-Aug-2007       JCK         Initial
****************************************************************************/
void CUserRegSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserRegSettings)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserRegSettings, CDialog)
	//{{AFX_MSG_MAP(CUserRegSettings)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_ADDFILE, OnAddfile)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_BN_CLICKED(IDC_REMOVE_ALL, OnRemoveAll)
	ON_BN_CLICKED(IDC_STOREFILE, OnStorefile)
	ON_BN_CLICKED(IDC_ADD_DELAY, OnAddDelay)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserRegSettings message handlers


void CUserRegSettings::FillDisplayList(void)
{
   unsigned long  i;
   char           szTextValue[MAX_STRING_SIZE];
   CListCtrl      *pList;

   pList = (CListCtrl *)GetDlgItem(IDC_USER_LIST);

   // Refill the display list...
   (void)pList->DeleteAllItems();

   for (i=0; i < m_pProcCfgCopy1.ulUserSpecificVariablesCount; i++)
      {
	   _UserSpecific* pUserSpecific = &m_pProcCfgCopy1.pUserSpecificVariables[i];	

	   if (0 == strcmp(WRITE_REG ,pUserSpecific ->szAction))
	   {
			(void)pList->InsertItem((int)i,  pUserSpecific->szVariableName);
			sprintf(szTextValue, "0x%lX", pUserSpecific->adVariableSize);
			(void)pList->SetItemText((int)i, 1, szTextValue);
			sprintf(szTextValue, "0x%lX", pUserSpecific->adVariableAddress);
			(void)pList->SetItemText((int)i, 2, szTextValue);
			sprintf(szTextValue, "0x%lX", pUserSpecific->ulVariableValue);
			(void)pList->SetItemText((int)i, 3, szTextValue);
	   } 
	   else
	   {
			(void)pList->InsertItem((int)i,  pUserSpecific->szVariableName);						
			sprintf(szTextValue, "0x%lX", pUserSpecific->ulVariableValue);
			(void)pList->SetItemText((int)i, 3, szTextValue);
	   }
      
      }

   if (m_pProcCfgCopy1.ulUserSpecificVariablesCount == 0)
      {
      (void)GetDlgItem(IDC_REMOVE)->EnableWindow(FALSE);
      (void)GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(FALSE);
      }
   else
      {
      (void)GetDlgItem(IDC_REMOVE)->EnableWindow(TRUE);
      (void)GetDlgItem(IDC_REMOVE_ALL)->EnableWindow(TRUE);

      (void)pList->SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
      }
}


/****************************************************************************
     Function: OnInitDialog()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : BOOL - controls the focus on control
  Description: Called when this page is shown for first time
               Does the initialization job. 
Date          Initials    Description
29-May-2008    JCK          Initial
07-May-2010	   JCK			Added support for Delay
12-Aug-2010	   SJ			Now uses singleton class instance to call RDI functions.
****************************************************************************/
BOOL CUserRegSettings::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CListCtrl *pList;
	unsigned long  i;

	CSingleton* instance = CSingleton::getInstance();

	pList = (CListCtrl *)GetDlgItem(IDC_USER_LIST);

	(void)pList->DeleteAllItems();
	while (pList->DeleteColumn(0)) ;

	(void)pList->InsertColumn(0, "Name");
	(void)pList->InsertColumn(1, "Size");
	(void)pList->InsertColumn(2, "Address");

	(void)pList->InsertColumn(3, "Value");

	(void)pList->SetColumnWidth(0, 70);
	(void)pList->SetColumnWidth(1, 50);
	(void)pList->SetColumnWidth(2, 70);
	(void)pList->SetColumnWidth(3, 70);

	//m_pProcCfgCopy = GetUserRegisterSettings();
	m_pProcCfgCopy = instance->m_GetUserRegisterSettings();
 
	_UserSpecific* pUserSpecific;
	for (i=0; i < m_pProcCfgCopy->ulUserSpecificVariablesCount; i++)
	{	
		pUserSpecific = &m_pProcCfgCopy1.pUserSpecificVariables[i];
		//strcpy(m_pProcCfgCopy1.pUserSpecificVariables[i].szAction, m_pProcCfgCopy->pUserSpecificVariables[i].szAction);

		//strcpy(szVariableName, &m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName[1]);
		//unsigned long ulLength = strlen(szVariableName);

		//if (szVariableName[ulLength - 1] == '"')
		//	szVariableName[ulLength - 1] = '\0';

		char* pszCommentStart = strchr(pUserSpecific->szVariableName, '"');
		char* pszCommentEnd = strrchr(pUserSpecific->szVariableName, '"');		

		if (pszCommentStart && pszCommentEnd)
		{
			unsigned long j = pszCommentEnd - pszCommentStart;
			memset(m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName, '\0', j);
			strncpy(m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName, &m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName[1], j - 1);
		}

		else
		{
			strcpy(m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName, m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName);
		}
		
		strcpy(m_pProcCfgCopy1.pUserSpecificVariables[i].szAction, m_pProcCfgCopy->pUserSpecificVariables[i].szAction);
		m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize = m_pProcCfgCopy->pUserSpecificVariables[i].adVariableSize;
		m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress = m_pProcCfgCopy->pUserSpecificVariables[i].adVariableAddress;
		m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue = m_pProcCfgCopy->pUserSpecificVariables[i].ulVariableValue;
	}

	m_pProcCfgCopy1.ulUserSpecificVariablesCount = m_pProcCfgCopy->ulUserSpecificVariablesCount;

	FillDisplayList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


static char BASED_CODE szRegFilter[] = "TXT Files (*.txt)|*.txt|All Files (*.*)|*.*||";

/****************************************************************************
     Function: OnAdd()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Add button.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010	   JCK			Added support for Delay
****************************************************************************/
void CUserRegSettings::OnAdd() 
{	
	CAddItem cAddItem;

	_UserSpecific* pUserSpecific = &m_pProcCfgCopy1.pUserSpecificVariables[m_pProcCfgCopy1.ulUserSpecificVariablesCount];
   if (cAddItem.DoModal() == IDOK)
      {
		strcpy(pUserSpecific->szAction , WRITE_REG);

		if ( 0 == strlen(cAddItem.m_szName) )
		{
			sprintf(cAddItem.m_szName, "PerReg%d",  m_pProcCfgCopy1.ulUserSpecificVariablesCount);
		}
		sprintf(pUserSpecific->szVariableName, "%c%s%c", '\"',cAddItem.m_szName, '\"');
		strcpy(pUserSpecific->szVariableName, cAddItem.m_szName);

		pUserSpecific->adVariableSize = cAddItem.m_ulSize;
		pUserSpecific->adVariableAddress = (uint32)cAddItem.m_ulAddress;
		pUserSpecific->ulVariableValue   = cAddItem.m_ulValue;
        m_pProcCfgCopy1.ulUserSpecificVariablesCount++; 
        FillDisplayList();
      }
}

/****************************************************************************
     Function: OnAddDelay()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is used to add delay.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010		  JCK		Added support for Delay
****************************************************************************/
void CUserRegSettings::OnAddDelay() 
{
	AddDelay cAddDelay;
	_UserSpecific* pUserSpecific = &m_pProcCfgCopy1.pUserSpecificVariables[m_pProcCfgCopy1.ulUserSpecificVariablesCount];

	if (IDOK == cAddDelay.DoModal())
	{
		CString cDelay = cAddDelay.m_Delay;
		unsigned long ulDelay;
		CHexUtils::GetValueFromHexString( 	cAddDelay.m_Delay, ulDelay );

		strcpy( pUserSpecific->szAction , DELAY );
		pUserSpecific->ulVariableValue = ulDelay;
		strcpy( pUserSpecific->szVariableName , DELAY );

		m_pProcCfgCopy1.ulUserSpecificVariablesCount++; 

		FillDisplayList();
	}

}

/****************************************************************************
     Function: OnAddfile()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Add from File button.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010		  JCK		  Added support for Delay
****************************************************************************/
void CUserRegSettings::OnAddfile() 
{
	// TODO: Add your control notification handler code here

	int      i;
	FILE     *hFile;
	char     szFileString[MAX_STRING_SIZE];
	char     szLocalFilename[_MAX_PATH];
	char     szErrorMessage[MAX_STRING_SIZE];
	BOOL     bComment;

	CString  cFileName;
	cFileName = SharedLIBGetProfileString("USERREG", "LAST_USED", NULL);

	CFileDialog RegisterFile(TRUE,
	".TXT",
	cFileName,
	OFN_HIDEREADONLY | OFN_EXPLORER,
	szRegFilter,
	NULL);

	if (RegisterFile.DoModal() == IDOK)
	{
		strcpy(szLocalFilename,(LPCTSTR)RegisterFile.GetPathName());
		(void)SharedLIBWriteProfileString("USERREG", "LAST_USED", szLocalFilename);

		if ((hFile = fopen(szLocalFilename, "rt")) != NULL)
		{
			while (!feof(hFile))
			{
				if (fgets(szFileString, MAX_STRING_SIZE, hFile) != NULL)
				{
					bComment = TRUE;

					for (i=0; i < strlen(szFileString); i++)
					{
						if ((szFileString[i] == 0xA) || (szFileString[i] == 0xD) || (szFileString[i] == '#'))
							break;
						if ((szFileString[i] != ' ') && (szFileString[i] != '\t'))
						{
							bComment = FALSE;
							break;
						}
					}
					
					if (!bComment)
					{
						_UserSpecific* pUserSpecific = &m_pProcCfgCopy1.pUserSpecificVariables[m_pProcCfgCopy1.ulUserSpecificVariablesCount];

						char* pszCommentStart = strchr(szFileString, '"');
						unsigned char i = 0;
						pszCommentStart = pszCommentStart + 1;
						while (pszCommentStart[i] != '"')
						{
							pUserSpecific->szVariableName[i] = pszCommentStart[i];
							
							i = i + 1;
						}						
						
						if (sscanf(&szFileString[i + 2], "%s %X %X %X",	pUserSpecific->szAction,																	
																	&pUserSpecific->adVariableSize,
																	&pUserSpecific->adVariableAddress,
																	&pUserSpecific->ulVariableValue) == 4)
						{   
							if (0 == strcmp(WRITE_REG , pUserSpecific->szAction))
							{
								if ((pUserSpecific->adVariableSize == 1) ||
									(pUserSpecific->adVariableSize == 2) ||
									(pUserSpecific->adVariableSize == 4))
								{
									char szVariableName[MAX_STRING_LENGTH];
									m_pProcCfgCopy1.ulUserSpecificVariablesCount++;
									
									for (i=0; i < m_pProcCfgCopy->ulUserSpecificVariablesCount; i++)
									{
										strcpy(m_pProcCfgCopy->pUserSpecificVariables[i].szAction, m_pProcCfgCopy1.pUserSpecificVariables[i].szAction);
										strcpy(szVariableName, &pUserSpecific->szVariableName[1]);
										
										unsigned long ulLength = strlen(szVariableName);
										
										if (szVariableName[ulLength - 1] == '"')
											szVariableName[ulLength - 1] = '\0';
										
										strcpy(pUserSpecific->szVariableName, szVariableName);
										
										strcpy(m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName, m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName);
												m_pProcCfgCopy->pUserSpecificVariables[i].adVariableSize = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize;
												m_pProcCfgCopy->pUserSpecificVariables[i].adVariableAddress = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress;
												m_pProcCfgCopy->pUserSpecificVariables[i].ulVariableValue = m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue;
									}
									//FillDisplayList();
								}
								else
								{
									char szErrorMessage[MAX_STRING_SIZE];
									sprintf(szErrorMessage, "Unable to parse:\n\n%s\nOnly sizes of 1, 2 or 4 are allowed", szFileString);   	   	   		  
								}
	
							} 
							else
							{
								if (sscanf(&szFileString[i + 2], "%s %X %X %X", pUserSpecific->szAction,
									&pUserSpecific->adVariableSize,
									&pUserSpecific->adVariableAddress,
									&pUserSpecific->ulVariableValue) == 4)
								{						
									m_pProcCfgCopy1.ulUserSpecificVariablesCount++;
									//FillDisplayList();
								}
							}													
						}
						else   	   	   	   
							sprintf(szErrorMessage, "Unable to parse:\n\n%s", szFileString);

					}
				}
			}
			fclose(hFile);
		}
		else         
			(void)AfxMessageBox("No such file or directory on disk.", MB_OK);

	}
	
	FillDisplayList();
}

/****************************************************************************
     Function: OnEdit()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Edit button.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010	   JCK			  Added support for Delay
****************************************************************************/
void CUserRegSettings::OnEdit() 
{
	int i;
	CListCtrl *pList;
	CAddItem cAddItem;
	AddDelay cAddDelay;

	pList = (CListCtrl *)GetDlgItem(IDC_USER_LIST);

	// Find the item that is currently selected....
	i = pList->GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);

	if (i != -1)
	{      

		if (0 == strcmp(WRITE_REG ,m_pProcCfgCopy1.pUserSpecificVariables[i].szAction))
		{
			strcpy(cAddItem.m_szName, m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName);
			cAddItem.m_ulAddress = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress;
			cAddItem.m_ulValue   = m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue;
			cAddItem.m_ulSize    = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize;

			cAddItem.m_bEditItem = TRUE;

			if (cAddItem.DoModal() == IDOK)
			{
				strcpy(m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName, cAddItem.m_szName);
				m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress = cAddItem.m_ulAddress;
				m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue = cAddItem.m_ulValue  ;
				m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize  = cAddItem.m_ulSize   ;

				for (i=0; i < m_pProcCfgCopy->ulUserSpecificVariablesCount; i++)
				{
					strcpy(m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName, m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName);
					m_pProcCfgCopy->pUserSpecificVariables[i].adVariableSize = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize;
					m_pProcCfgCopy->pUserSpecificVariables[i].adVariableAddress = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress;
					m_pProcCfgCopy->pUserSpecificVariables[i].ulVariableValue = m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue;
				}

			FillDisplayList();
			}
		}
		else
		{
			char szVariableVal[4];
			itoa(m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue, szVariableVal, 16);

			strcpy((char*)((LPCTSTR)cAddDelay.m_Delay), szVariableVal);
			
			if (IDOK == cAddDelay.DoModal())
			{
				_UserSpecific* pUserSpecific = &m_pProcCfgCopy1.pUserSpecificVariables[i];	

				CString cDelay = cAddDelay.m_Delay;
				unsigned long ulDelay;
				CHexUtils::GetValueFromHexString( 	cAddDelay.m_Delay, ulDelay );			
				pUserSpecific->ulVariableValue = ulDelay;

				FillDisplayList();
			}
		}

	}
	
}

/****************************************************************************
     Function: OnRemove()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Remove button.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010	      JCK		  Added support for Delay
****************************************************************************/

void CUserRegSettings::OnRemove() 
{
	// TODO: Add your control notification handler code here

	CListCtrl *pList;
	unsigned long i;
	unsigned long j;

	pList = (CListCtrl *)GetDlgItem(IDC_USER_LIST);

	// Find the item that is currently selected....
	j = pList->GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);

	if (j != (unsigned long)-1)
	{
		// Remove the item from the list...
		m_pProcCfgCopy1.ulUserSpecificVariablesCount--;
		m_pProcCfgCopy->ulUserSpecificVariablesCount--;

		for (i = j; i < m_pProcCfgCopy1.ulUserSpecificVariablesCount; i++)
		{
			strcpy(m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName, m_pProcCfgCopy1.pUserSpecificVariables[i+1].szVariableName);
			m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize = m_pProcCfgCopy1.pUserSpecificVariables[i+1].adVariableSize;
			m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress = m_pProcCfgCopy1.pUserSpecificVariables[i+1].adVariableAddress ;
			m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue = m_pProcCfgCopy1.pUserSpecificVariables[i+1].ulVariableValue;

			strcpy(m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName, m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName);
			m_pProcCfgCopy->pUserSpecificVariables[i].adVariableSize = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize;
			m_pProcCfgCopy->pUserSpecificVariables[i].adVariableAddress = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress;
			m_pProcCfgCopy->pUserSpecificVariables[i].ulVariableValue = m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue;
		}		

		FillDisplayList();
	}
}

/****************************************************************************
     Function: OnRemoveAll()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Remove All button.               
Date           Initials    Description
29-May-2008       JCK         Initial
****************************************************************************/

void CUserRegSettings::OnRemoveAll() 
{
	m_pProcCfgCopy1.ulUserSpecificVariablesCount = 0;
	m_pProcCfgCopy->ulUserSpecificVariablesCount = 0;

	FillDisplayList();	
}

/****************************************************************************
     Function: OnStorefile()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses Store to file button.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010	      JCK		  Added support for Delay
****************************************************************************/
void CUserRegSettings::OnStorefile() 
{
	FILE          *hFile;
	unsigned long  i;

	CString cFileName;
	cFileName = SharedLIBGetProfileString("USERREG", "LAST_USED", NULL);

	CFileDialog RegisterFile(FALSE,
		".TXT",
		cFileName,
		OFN_HIDEREADONLY | OFN_EXPLORER,
		szRegFilter,
		NULL);

	if (RegisterFile.DoModal() == IDOK)
	   {
		char szLocalFilename[_MAX_PATH];
		char szVariableName[MAX_STRING_LENGTH];

		strcpy(szLocalFilename,(LPCTSTR)RegisterFile.GetPathName());
		if((hFile = fopen(szLocalFilename, "w+t")) != NULL)
		{
			for (i=0; i < m_pProcCfgCopy1.ulUserSpecificVariablesCount; i++)
			{
				sprintf(szVariableName, "%c%s%c", '\"',m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName, '\"');
				fprintf(hFile, "%s %s 0x%8.8X 0x%8.8X 0x%8.8X\n", szVariableName,
					m_pProcCfgCopy1.pUserSpecificVariables[i].szAction,					
					m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize,
					m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress,
					m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue);
			}
			fclose(hFile);

			(void)SharedLIBWriteProfileString("USERREG", "LAST_USED", szLocalFilename);
		}
		else         
			(void)AfxMessageBox("File is write protected.", MB_OK);
		
	   }
	
}

/****************************************************************************
     Function: OnOK()
     Engineer: Jeenus Chalattu Kunnath
     Input   : void
     Output  : 
  Description: This function is called when user presses OK button.               
Date           Initials    Description
29-May-2008       JCK         Initial
07-May-2010	      JCK		  Added support for Delay
****************************************************************************/
void CUserRegSettings::OnOK() 
{
	unsigned long i;
	
	//for saving to a file
	FILE *tempFile;
	unsigned long j;
	CString dllPath;
	char actualPath[260];
	
	COpellaARMConfigDlg::GetARMRDIDriverPath(dllPath);
	const char* fulldllPath = (LPCTSTR)dllPath;
	strcpy(actualPath,fulldllPath);
	strcat(actualPath,"\\temp.txt");
	if((tempFile = fopen(actualPath, "w+t")) != NULL)
	{
		char szVariableName[MAX_STRING_LENGTH];
		for (j=0; j < m_pProcCfgCopy1.ulUserSpecificVariablesCount; j++)
		{
			sprintf(szVariableName, "%c%s%c", '\"',m_pProcCfgCopy1.pUserSpecificVariables[j].szVariableName, '\"');

			strcpy( m_pProcCfgCopy1.pUserSpecificVariables[j].szVariableName, szVariableName );
			fprintf(tempFile, "%s %s 0x%8.8X 0x%8.8X 0x%8.8X\n",	m_pProcCfgCopy1.pUserSpecificVariables[j].szVariableName,
																	m_pProcCfgCopy1.pUserSpecificVariables[j].szAction,																						
																	m_pProcCfgCopy1.pUserSpecificVariables[j].adVariableSize,
																	m_pProcCfgCopy1.pUserSpecificVariables[j].adVariableAddress,
																	m_pProcCfgCopy1.pUserSpecificVariables[j].ulVariableValue);
		}
		fclose(tempFile);
	}

	for (i=0; i < m_pProcCfgCopy1.ulUserSpecificVariablesCount; i++)
	{		
		strcpy(m_pProcCfgCopy->pUserSpecificVariables[i].szAction, m_pProcCfgCopy1.pUserSpecificVariables[i].szAction);
		strcpy(m_pProcCfgCopy->pUserSpecificVariables[i].szVariableName, m_pProcCfgCopy1.pUserSpecificVariables[i].szVariableName);
		m_pProcCfgCopy->pUserSpecificVariables[i].adVariableSize = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableSize;
		m_pProcCfgCopy->pUserSpecificVariables[i].adVariableAddress = m_pProcCfgCopy1.pUserSpecificVariables[i].adVariableAddress;
		m_pProcCfgCopy->pUserSpecificVariables[i].ulVariableValue = m_pProcCfgCopy1.pUserSpecificVariables[i].ulVariableValue;
	}
	m_pProcCfgCopy->ulUserSpecificVariablesCount = m_pProcCfgCopy1.ulUserSpecificVariablesCount;

	CDialog::OnOK();
}
