/****************************************************************************
     Module  : LoadSaveDlg.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CLoadSaveDlg class
Date           Initials    Description
03-Aug-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#ifndef _SAVE_STORE_DLG_H_
#define _SAVE_STORE_DLG_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "OpellaConfigPageBase.h"

/****************************************************************************
     Function: CLoadSaveDlg
     Engineer: Jiju George T
  Description: Handled the Load/Save page operations
Date           Initials    Description
03-Aug-2007       JG         Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/
class CLoadSaveDlg : public COpellaConfigPageBase
{
    //DECLARE_DYNCREATE(CLoadSaveDlg)

// Construction
public:
    void InitializeUIData ( COpellaConfig &OpellaConfig );
    CLoadSaveDlg(COpellaARMConfigDlg *pOpellaARMConfigDlg);
    ~CLoadSaveDlg();
    bool ApplyChanges( COpellaConfig &OpellaConfig );

// Dialog Data
    //{{AFX_DATA(CLoadSaveDlg)
    enum { IDD = IDD_LOADSTORE_DLG };
    CString m_szConfigFilename;
    //}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CLoadSaveDlg)
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CLoadSaveDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnSave();
    afx_msg void OnBrowseConfigLoadBtn();
    afx_msg void OnLoadBtn();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
   COpellaARMConfigDlg *m_pOpellaARMConfigDlg;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _SAVE_STORE_DLG_H_
