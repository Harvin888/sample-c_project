/****************************************************************************
       Module: DriverMgr.h
     Engineer: Jiju George T
  Description: This file contains the declaration of CDriverMgr class
Date           Initials    Description
24-Jul-2007      JG          Initial
****************************************************************************/

#ifndef _DRIVE_MGR_H_
#define _DRIVE_MGR_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/****************************************************************************
     Function: CDriverMgr
     Engineer: Jiju George T
  Description: This class acts as the link between RDI Driver and GUI
Date           Initials    Description
24-Jul-2007       JG         Initial
****************************************************************************/
class CDriverMgr
{
private:

    CDriverMgr();
	char m_szOpellaXDSerialNumber[MAX_SERIAL_NUMBER_LEN];
	unsigned long m_ulDebugBaseAddr;
	unsigned long m_ulMemApIndex;
	unsigned long m_ulDebugApIndex;
	unsigned long m_ulJtagApIndex;

public:
    static CDriverMgr& GetInstance();
    virtual ~CDriverMgr();
    void DetectConnectedOpellas( TyMultipleUSBDevices& tyMultipleUSBDevices );
    CString GetOpellaStatus(TyUSBDeviceInstance &tyUSBDeviceInstance);
    CString GetTargetStatus(TyUSBDeviceInstance &tyUSBDeviceInstance);
    CString GetRDIDriverVersion();
    CString GetFirmwareVersion();

	void SetOpellaXDSerialNumber( char* pszOpellaXDSerialNumber )
	{
		strcpy( m_szOpellaXDSerialNumber, pszOpellaXDSerialNumber );
	}
	void GetOpellaXDSerialNumber( char*& pszOpellaXDSerialNumber )
	{
		strcpy( pszOpellaXDSerialNumber, m_szOpellaXDSerialNumber );
	}

	void SetDebugBaseAddr( unsigned long ulDebugBaseAddr )
	{
		
	}
	void GetDebugBaseAddr( unsigned long*& pulDebugBaseAddr )
	{
		*pulDebugBaseAddr = m_ulDebugBaseAddr;
	}

	void SetCoresightParam( CString csDebugApIndex,
							CString csMemApIndex,
							CString csJtagApIndex,
							CString csDebugBaseAddr)
	{
		unsigned long ulDebugApIndex;
		unsigned long ulMemApIndex;
		unsigned long ulJtagApIndex;
		unsigned long ulDebugBaseAddr;
		bool bValidData = true;

		bValidData = CHexUtils::GetValueFromHexString( 	csDebugApIndex,
														ulDebugApIndex );
		if( bValidData )
		{
			m_ulDebugApIndex = ulDebugApIndex;
		}

		bValidData = CHexUtils::GetValueFromHexString( 	csMemApIndex,
			ulMemApIndex );
		if( bValidData )
		{
			m_ulMemApIndex = ulMemApIndex;
		}

		bValidData = CHexUtils::GetValueFromHexString( 	csJtagApIndex,
			ulJtagApIndex );
		if( bValidData )
		{
			m_ulJtagApIndex  = ulJtagApIndex;
		}

		bValidData = CHexUtils::GetValueFromHexString( 	csDebugBaseAddr,
			ulDebugBaseAddr );
		if( bValidData )
		{
			m_ulDebugBaseAddr = ulDebugBaseAddr;
		}		
	}

	void GetCoresightParam( unsigned long* pulDebugApIndex,
							unsigned long* pulMemApIndex,
							unsigned long* pulJtagApIndex,
							unsigned long* pulDebugBaseAddr)
	{
		*pulDebugApIndex = m_ulDebugApIndex;
		*pulMemApIndex = m_ulMemApIndex;
		*pulJtagApIndex  = m_ulJtagApIndex;
		*pulDebugBaseAddr = m_ulDebugBaseAddr;
	}

};

#endif // _DRIVE_MGR_H_
