/****************************************************************************
       Module: LoadSaveDlg.cpp
     Engineer: Jiju George T
  Description: This file contains the implementation of CLoadSaveDlg class
Date           Initials    Description
24-Jul-2007       JG          Initial
27-Feb-2008       SPC        Added Support for Load/Save config options.
****************************************************************************/

#include "stdafx.h"
#include "OpellaConfig.h"
#include "LoadSaveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LOAD_SAVE_FILE_FILTER "Opella-XD ARM Configuration File|*.cnf|"
#define CONFIG_FILE_EXTN "cnf"
static const short FILE_EXTN_SIZE=4;

/////////////////////////////////////////////////////////////////////////////
// CLoadSaveDlg property page

//IMPLEMENT_DYNCREATE(CLoadSaveDlg, CResizablePage)


/****************************************************************************
     Function: Constructor
     Engineer: Jiju George T
     Input   : void
     Output  : void 
  Description:
Date           Initials    Description
24-Jul-2007      JG           Initial
27-Feb-2008      SPC          Initializations are changed to another function.
****************************************************************************/

CLoadSaveDlg::CLoadSaveDlg(COpellaARMConfigDlg *pOpellaARMConfigDlg) : 
   COpellaConfigPageBase(CLoadSaveDlg::IDD),
   m_pOpellaARMConfigDlg(pOpellaARMConfigDlg)   
{
    //{{AFX_DATA_INIT(CLoadSaveDlg)
    m_szConfigFilename = _T("");
    //}}AFX_DATA_INIT

    m_psp.dwFlags &= (~PSP_HASHELP);
}
 
/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
     Input   : void
     Output  : void 
  Description:
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/

CLoadSaveDlg::~CLoadSaveDlg()
{
}
 
/****************************************************************************
     Function: DoDataExchange()
     Engineer: Jiju George T
     Input   : pDX  - Holds the exchange information
     Output  : void
  Description: Does the data exchange between controls and corresponding member variables
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/

void CLoadSaveDlg::DoDataExchange(CDataExchange* pDX)
{
    CResizablePage::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoadSaveDlg)
    DDX_Text(pDX, IDC_CONFIGFILEPATH_EDIT, m_szConfigFilename);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoadSaveDlg, COpellaConfigPageBase)
    //{{AFX_MSG_MAP(CLoadSaveDlg)
    ON_BN_CLICKED(IDC_SAVE, OnSave)
    ON_BN_CLICKED(IDC_BROWSECOFIGLOAD_BTN, OnBrowseConfigLoadBtn)
    ON_BN_CLICKED(IDC_LOAD_BTN, OnLoadBtn)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()
 
/****************************************************************************
     Function: OnInitDialog()
     Engineer: Jiju George T
     Input   : void
     Output  : BOOL - controls the focus on control
  Description: Called when this page is shown for first time.
               Does the initialization job.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/

BOOL CLoadSaveDlg::OnInitDialog()
{
    CResizablePage::OnInitDialog();

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}

/****************************************************************************
     Function: ApplyChanges()
     Engineer: Jiju George T
     Input   : OpellaConfig - Configuration object to save configurations
     Output  : bool -  Return false if some required information is not
               filled by user (In this case notification about
               the error should be given to user). Else return true.
  Description: This function is called by the parent when user presses Apply.OK button.
               All the user configuration in the page should be saved to the passed
               COpellaConfig object
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/

bool CLoadSaveDlg::ApplyChanges( COpellaConfig &OpellaConfig )
{
    // Exchange data from controls to members
    if( IsWindow( m_hWnd ))
    {
        UpdateData();
    }

    // Not saving anything to configuration file.

    return true;
}

/****************************************************************************
     Function: OnSave()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This function is the handler for Save button click event. 
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CLoadSaveDlg::OnSave() 
{
    bool bRet = true;
    CString szMsgTitle;
    szMsgTitle.LoadString(IDS_ARM_RDI_TITLE);
    CString szFilename = "";
    CFileDialog dlgSaveFile( FALSE, CONFIG_FILE_EXTN, NULL, OFN_OVERWRITEPROMPT,
                 LOAD_SAVE_FILE_FILTER, this);
    //Showing Save Dialog box
    if( IDOK == dlgSaveFile.DoModal() )
    {
        szFilename = dlgSaveFile.GetPathName();
        //Removing extension ".cnf" of file 
        if ( 0 == dlgSaveFile.GetFileExt().CompareNoCase(CONFIG_FILE_EXTN))
        {
            szFilename.Delete( szFilename.ReverseFind('.'), FILE_EXTN_SIZE );
        }

        bRet = m_pOpellaARMConfigDlg->SaveConfigurationToFile(szFilename);
        if (bRet)
        {
            MessageBox("Configuration successfully saved.", 
                szMsgTitle, MB_OK|MB_ICONINFORMATION );
        }
        else
        {
            MessageBox("Error in saving configuration.", 
                szMsgTitle, MB_OK|MB_ICONERROR );
        }
    }
}

/****************************************************************************
     Function: OnBrowseConfigLoadBtn()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This function is the handler for Browse button click event to 
               show open dialog box and choose a file. 
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CLoadSaveDlg::OnBrowseConfigLoadBtn() 
{
    CString szTmp;
    CFileDialog dlgFileOpen( TRUE, CONFIG_FILE_EXTN, NULL, OFN_HIDEREADONLY, 
                   LOAD_SAVE_FILE_FILTER, this);
    if (IDOK == dlgFileOpen.DoModal())
    {
        if ( 0 != dlgFileOpen.GetFileExt().CompareNoCase(CONFIG_FILE_EXTN))
        {
            CString Errmsg;
            CString szMsgTitle;
            szMsgTitle.LoadString(IDS_ARM_RDI_TITLE);
            Errmsg = dlgFileOpen.GetFileName() + " is not a valid configuration file. File extension must be .cnf";
            MessageBox(Errmsg, szMsgTitle, MB_OK|MB_ICONERROR);
            return;
        }
        m_szConfigFilename = dlgFileOpen.GetPathName();
        UpdateData(FALSE);
    }
}

/****************************************************************************
     Function: OnLoadBtn()
     Engineer: Suresh P.C
     Input   : None
     Output  : None
  Description: This function is the handler for Load button click event. 
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CLoadSaveDlg::OnLoadBtn() 
{
    CString szMsgTitle;
    szMsgTitle.LoadString(IDS_ARM_RDI_TITLE);
    UpdateData();
    CString szTempFilename = m_szConfigFilename;
    if (m_szConfigFilename.IsEmpty())
    {
        MessageBox("No configuration files selected. Please select a valid configuration file.",
            szMsgTitle, MB_OK);
        return;
    }

    //Checking the extension of the file
    int nDotIndex = szTempFilename.ReverseFind('.');
    if (-1 != nDotIndex) // Have some Extension
    {
        //Now checking it is same as CONFIG_FILE_EXTN
        if ( 0 == szTempFilename.Right(szTempFilename.GetLength() 
            - (nDotIndex + 1) ).CompareNoCase(CONFIG_FILE_EXTN) )
        {
            // Same as CONFIG_FILE_EXTN so we are removing it
            szTempFilename.Delete( nDotIndex, FILE_EXTN_SIZE );
        }
    }
    if (!m_pOpellaARMConfigDlg->LoadConfigurationFile(szTempFilename))
    {
        MessageBox("Unable to load configuration properly.", 
            szMsgTitle, MB_OK|MB_ICONERROR);
        return;
    }

    MessageBox("Configuration loaded successfully.",szMsgTitle, MB_OK);
}

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This function will initialize GUI data variables using the 
               configuration object as argument. This function is called by
               Constructor of the class and base class while loading configuration.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CLoadSaveDlg::InitializeUIData( COpellaConfig &OpellaConfig )
{
    //Calling base class Initializations. 
    COpellaConfigPageBase::InitializeUIData( OpellaConfig );
}

