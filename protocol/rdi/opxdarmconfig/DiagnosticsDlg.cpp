/****************************************************************************
       Module: DiagnosticsDlg.cpp
     Engineer: Jiju George T
  Description: This file contains the implementation of CDiagnosticsDlg class
Date           Initials    Description
01-Aug-2007       JG          Initial
27-Feb-2008       SPC         Added Support for Load/Save config options.
12-Aug-2010		  SJ		  Now uses singleton class instance to call RDI functions.
****************************************************************************/

#include "stdafx.h"
#include "OpellaConfig.h"
#include "TargetConnectionDlg.h"
#include "DiagnosticsDlg.h"
#include "DriverMgr.h"
#include "DeviceDlg.h"
#include "../opxdarmdll/midlayerarm.h"
#include "FamilyDatabase.h"
#include "CSingleton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define NO_OPELLA_SELECTED_MESSAGE _T( "No Opella-XD's detected.\r\nPlease connect one and retry.(Select one from Target Connection Dialog)\r\n\r\n" );

//Error codes
#define DIAG_ERROR_NO_ERROR					0x0000
#define DIAG_ERROR_USB_DRIVER				0x0001
#define DIAG_ERROR_INVALID_SERIAL_NUMBER	0x0003
#define DIAG_ERROR_TAPID1_FAILED			0x0004
#define DIAG_ERROR_TAPID2_FAILED			0x0005
#define DIAG_ERROR_TAPID_FAILED				0x0006
#define DIAG_ERROR_CPREG_FAILED				0x0007
#define DIAG_ERROR_TAP_LB_FAILED			0x0008
#define DIAG_ERROR_WRONG_CORE				0x0009
#define DIAG_ERROR_TPA_NOT_CONNECTED		0x000A
#define DIAG_ERROR_INVALID_CONFIG			0x000B
#define DIAG_ERROR_CORE_MISS_MATCH			0x000C
#define DIAG_ERROR_READ_ROM_FAILED			0x000D
#define DIAG_ERROR_ARM_NO_DEBUG_ENTRY_AP	0x000E
#define DIAG_ERROR_ARM_NOT_VALID_ROM_ADDR	0x000F
#define DIAG_ERROR_INVALID_CORESIGHT_CONFIG	0x0010
#define DIAG_ERROR_OPELLA_FAILURE           360

TyError ParseCorsightDevice();
/****************************************************************************
     Function: Constructor
     Input   : OpellaConfig - contains the configuration options
     Output  : void
     Engineer: Jiju George T
  Description: 
Date           Initials    Description
24-Jul-2007      JG           Initial
27-Feb-2008      SPC          Initializations are changed to another function.
****************************************************************************/
CDiagnosticsDlg::CDiagnosticsDlg( COpellaConfig &OpellaConfig,
                                  CTargetConnectionDlg &TargetConnectionPage ,
                                  CDeviceDlg &DevicePage) :
    COpellaConfigPageBase( CDiagnosticsDlg::IDD ),
    m_OpellaConfig( OpellaConfig ),
    m_TargetConnectionPage( TargetConnectionPage ),
    m_DevicePage( DevicePage )

{
    //{{AFX_DATA_INIT(CDiagnosticsDlg)
    m_bEnableRDILogging = FALSE;
    m_csOpellaStatus = _T("");
    m_csTargetStatus = _T("");
	//}}AFX_DATA_INIT

    // To Remove Help button from parent property sheet
    m_psp.dwFlags &= (~PSP_HASHELP);

    // Get Version Information
    //m_csRDIDriverVersion = CDriverMgr::GetInstance().GetRDIDriverVersion();

    InitializeUIData(m_OpellaConfig);
}


/****************************************************************************
     Function: Destructor
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: 
Date           Initials    Description
24-Jul-2007      JG           Initial
****************************************************************************/
CDiagnosticsDlg::~CDiagnosticsDlg()
{
}
/****************************************************************************
     Function: DoDataExchange()
     Engineer: Jiju George T
     Input   : pDX  - Holds the exchange information
     Output  : void
  Description: Does the data exchange between controls and corresponding member variables.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
void CDiagnosticsDlg::DoDataExchange(CDataExchange* pDX)
{
    CResizablePage::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CDiagnosticsDlg)
	DDX_Control(pDX, IDC_TARGET_STATUS, m_csSetCheckTarget);
	DDX_Control(pDX, IDC_OPELLA_STATUS, m_csSetCheckOpella);
    DDX_Check(pDX, IDC_ENABLE_RDI_LOGGING, m_bEnableRDILogging);
    DDX_Text(pDX, IDC_OPELLA_STATUS, m_csOpellaStatus);
    DDX_Text(pDX, IDC_TARGET_STATUS, m_csTargetStatus);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDiagnosticsDlg, COpellaConfigPageBase)
    //{{AFX_MSG_MAP(CDiagnosticsDlg)
    ON_BN_CLICKED(IDC_OPELLA_CHECK, OnOpellaCheck)
    ON_BN_CLICKED(IDC_TARGET_CHECK, OnTargetCheck)
    ON_BN_CLICKED(IDC_HARD_RESET, OnHardReset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/****************************************************************************
     Function: OnInitDialog()
     Engineer: Jiju George T
     Input   : void
     Output  : BOOL - controls the focus on control
  Description: Called when this page is shown for first time.
               Does the initialization job.
Date           Initials    Description
24-Jul-2007      JG           Initial
27-Feb-2008      SPC          UI Updates are doing in another function and 
                              it will call on OnShowWindow function.
****************************************************************************/
BOOL CDiagnosticsDlg::OnInitDialog()
{
    CResizablePage::OnInitDialog();

    return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
}



/****************************************************************************
     Function: OnOpellaCheck()
     Engineer: Suraj S
     Input   : void
     Output  : void
  Description: Called when user selects the button to check Opella's status
Date           Initials    Description
24-Jul-2007      SJ         Initial
12-Aug-2010		 SJ			Now uses singleton class instance to call RDI functions.
****************************************************************************/
void CDiagnosticsDlg::OnOpellaCheck()
{
    // Get detail of Opella selected in target connection page
    TyUSBDeviceInstance tyUSBDeviceInstance = { 0 };
    TyRDIDiagInfo tyRDIDiagInfo = {0};
    TyError ErrRet;
    CString csNewString;
	
	CSingleton* instance = CSingleton::getInstance();

    if( m_TargetConnectionPage.GetSelectedOpellaDetails( tyUSBDeviceInstance ))
    {
        UpdateData(TRUE);
        m_csOpellaStatus += "Checking...\r\n";
        UpdateData(FALSE);
        int nPos = m_csSetCheckOpella.GetLineCount();
        m_csSetCheckOpella.LineScroll( nPos );
        nPos = m_csSetCheckTarget.GetLineCount();
        m_csSetCheckTarget.LineScroll( nPos );
       // m_csSetCheckOpella.UpdateWindow();

        //m_csSetCheckOpella.GetScrollPos()
        //ErrRet = PerformRDIDiagnostics(tyUSBDeviceInstance.szOpellaXDSerialNumber, &tyRDIDiagInfo);
        ErrRet = instance->m_PerformRDIDiagnostics(tyUSBDeviceInstance.szOpellaXDSerialNumber, &tyRDIDiagInfo);

        if(ErrRet != 0)
        {
         CString csErrorMessage1;
         CString csErrorMessage2;
         csErrorMessage1.Format("RDI driver not responding, cannot continue." );
         csErrorMessage2.Format("Check Opella-XD is properly connected.\r\n");
         csNewString.Format("%s\r\n%s\r\n",csErrorMessage1,csErrorMessage2);
        }

        else
        {
        // Add Serial Number
        CString csSerialNumber;
        csSerialNumber.Format( "Opella-XD serial number: %s", tyRDIDiagInfo.pszSerialNumber );

        // Add Manufactured details
        CString csManufactured;
        csManufactured.Format( "Manufactured: %s, %s", tyRDIDiagInfo.pszManufDate, tyRDIDiagInfo.pszBoardRevision );

        // Add USB Driver details
        CString csUSBDriver;
        csUSBDriver.Format( "USB driver: %s", tyRDIDiagInfo.pszUSBDriverVersion );

        // Add Firmware details
        CString csFirmware;
        csFirmware.Format( "Firmware: %s, %s", tyRDIDiagInfo.pszFwVersion, tyRDIDiagInfo.pszFwDate );

        // Add Diskware details
        CString csDiskware;
        if(tyRDIDiagInfo.bDwRunning)
           csDiskware.Format( "Diskware: %s, %s", tyRDIDiagInfo.pszDwVersion, tyRDIDiagInfo.pszDwDate);
        else
           csDiskware.Format( "Diskware not downloaded");
           
        // Add FPGAware details
        CString csFPGAware;
        csFPGAware.Format( "FPGAware: %s, %s\r\n", tyRDIDiagInfo.pszFpgawareVersion, tyRDIDiagInfo.pszFpgawareDate);

        // Add TPA details
        CString csTPA;
        if(!strcmp(tyRDIDiagInfo.pszTpaName,""))
        csTPA.Format( "TPA not connected");
        else
        csTPA.Format( "Opella-XD Target Probe Assembly (TPA): %s, %s", tyRDIDiagInfo.pszTpaName, tyRDIDiagInfo.pszTpaRevision);

        // Multiline addition
        csNewString.Format("%s\r\n%s\r\n%s\r\n%s\r\n%s\r\n%s\r\n%s\r\n",csSerialNumber,csTPA,csManufactured,csUSBDriver,csFirmware,csDiskware,csFPGAware);
        }

    }
    else
    {
        csNewString = NO_OPELLA_SELECTED_MESSAGE;
    }

    m_csOpellaStatus += csNewString;
    UpdateData( FALSE );
    int nPos = m_csSetCheckOpella.GetLineCount();
    m_csSetCheckOpella.LineScroll( nPos );
    nPos = m_csSetCheckTarget.GetLineCount();
    m_csSetCheckTarget.LineScroll( nPos );
    //m_csSetCheckOpella.UpdateWindow();
}


/****************************************************************************
     Function: OnTargetCheck()
     Engineer: Suraj S
     Input   : void
     Output  : void
  Description: Called when user selects the button to check Target's status..
Date           Initials    Description
24-Jul-2007       SJ        Initial
09-Apr-2008       JCK       Corrected Diagnostics messages, error handling etc.
12-Aug-2010		  SJ		Now uses singleton class instance to call RDI functions.
****************************************************************************/
void CDiagnosticsDlg::OnTargetCheck()
{
   // Get detail of Opella selected in target connection page
   TyUSBDeviceInstance tyUSBDeviceInstance = { 0 };
   TyRDIOnTargetDiagInfo tyRDIOnTargetDiagInfo = {0};
   //TyCoresightFeature tyCoresightFeatureInfo;
   TyError ErrRet;
   char csSelectedDevice[20];
   CString csInternalName;

   CSingleton* instance = CSingleton::getInstance();

   strcpy ( csSelectedDevice ,m_DevicePage.GetSelectedCoreDetails() ); 
   //strcpy (tyRDIOnTargetDiagInfo.pszCoreType ,m_DevicePage.GetSelectedDeviceName() ); 
   CFamilyDatabase::GetInstance().GetInternalName( &csInternalName, csSelectedDevice );

   strcpy(tyRDIOnTargetDiagInfo.pszCoreType, LPCTSTR (csInternalName)); 
   //csInternalName.Format("%s", tyRDIOnTargetDiagInfo.pszCoreType);
 
   tyRDIOnTargetDiagInfo.ulDbgCoreNumber = m_DevicePage.GetSelectedCoreNumber();
   tyRDIOnTargetDiagInfo.ptyJtagScanChain = m_DevicePage.GetJtagScanDetails();
   tyRDIOnTargetDiagInfo.tyJTAGClockFreqMode = m_TargetConnectionPage.GetFrequencyMode();  


   tyRDIOnTargetDiagInfo.bCoresightCompliant = CFamilyDatabase::GetInstance().CheckCoresightCompliant(tyRDIOnTargetDiagInfo.pszCoreType); 
   if (tyRDIOnTargetDiagInfo.bCoresightCompliant)
   {
	   CDriverMgr::GetInstance().GetCoresightParam (&tyRDIOnTargetDiagInfo.ulDbgApIndex, 
													&tyRDIOnTargetDiagInfo.ulMemApIndex, 
													&tyRDIOnTargetDiagInfo.ulJtagApIndex, 
													&tyRDIOnTargetDiagInfo.ulCoresightBaseAddr );  
												    
   }

   /* Check that the device selected is TI complaint */
   bool bTiCore = CFamilyDatabase::GetInstance().CheckTICore( csSelectedDevice );
   
   tyRDIOnTargetDiagInfo.bTICore = bTiCore;
   if (TRUE == bTiCore)
   {
	   CString csSubPortNum;
	   unsigned long ulSubPortNum;
	   boolean  bValidData;
	   
	   CFamilyDatabase::GetInstance().GetSecondaryPortNum(&csSubPortNum, csSelectedDevice);
	   bValidData = CHexUtils::GetValueFromHexString( 	csSubPortNum,
		   ulSubPortNum );
	   
	   if( bValidData )
	   {
		   tyRDIOnTargetDiagInfo.ulTISubPortNum = ulSubPortNum;
	   }
	   
	   CString csDapPcSupport;
	   unsigned long ulDapPcSupport;		
	   CFamilyDatabase::GetInstance().GetDapPcSupport( &csDapPcSupport, csSelectedDevice);
	   bValidData = CHexUtils::GetValueFromHexString( 	csDapPcSupport,
		   ulDapPcSupport );
	   
	   if( bValidData )
	   {
		   tyRDIOnTargetDiagInfo.bDapPcSupport = ulDapPcSupport;
	   }
	}

   if(tyRDIOnTargetDiagInfo.tyJTAGClockFreqMode == Fixed)
      tyRDIOnTargetDiagInfo.ulFreqVal = m_TargetConnectionPage.GetFrequencyValue();

   CString csLoopBack;
   CString csTgtVolt;
   CString csIdcodeNumber;
   CString csTotCores;
   CString csIRLength;
   CString csCoID0; 
   CString csCoID1;
   CString csICsize;
   CString csDCsize;   
   CString csArch;
   CString csTAPLoopBack;
   CString csRegCheck;
   CString csCoreType;
   CString csNewString;
   CString csConfErr;

   if(tyRDIOnTargetDiagInfo.ulDbgCoreNumber > 9)
      {
      UpdateData(TRUE);
      csNewString.Format("No ARM cores selected\r\n. Please select atleast one ARM core\r\n\r\n");
      }
   
   else if( m_TargetConnectionPage.GetSelectedOpellaDetails( tyUSBDeviceInstance ))
      {
      UpdateData(TRUE);
      m_csTargetStatus += "Checking...\r\n";
      UpdateData(FALSE);
      int nPos = m_csSetCheckTarget.GetLineCount();
      m_csSetCheckTarget.LineScroll( nPos );
      nPos = m_csSetCheckOpella.GetLineCount();
      m_csSetCheckOpella.LineScroll( nPos );
      //m_csSetCheckTarget.UpdateWindow();	  

	  
	  //ErrRet = PerformRDIDiagnosticsOnTarget(tyUSBDeviceInstance.szOpellaXDSerialNumber, &tyRDIOnTargetDiagInfo);
	  ErrRet = instance->m_PerformRDIDiagnosticsOnTarget(tyUSBDeviceInstance.szOpellaXDSerialNumber, &tyRDIOnTargetDiagInfo);
	      

      if(ErrRet == DIAG_ERROR_INVALID_CONFIG)
         {
         char szError[5000];
         if((tyRDIOnTargetDiagInfo.ulTotCores > 0) && (tyRDIOnTargetDiagInfo.ptyJtagScanChain->ucNumJtagNodes != tyRDIOnTargetDiagInfo.ulTotCores))
            {
            sprintf(szError,"%d core(s) detected. This does not match with the selected number of cores.\r\n",tyRDIOnTargetDiagInfo.ulTotCores);
            }

         else
            {            
            strcpy(szError,"Failed to initialize target. Please reconfigure the Opella-XD RDI driver\r\nappropriately for your target\r\n");
            strcat(szError,"Try the following hints to fix the problem:\r\n");
            strcat(szError,"  * Check whether the multi-core configuration is correct.Ensure the IR length of each core are correct.\r\n");
            strcat(szError,"  * Check whether the correct device\\core is selected\r\n");
            strcat(szError,"  * Use a lower frequency or RTCK if the core supports it\r\n");
            }
         csConfErr.Format("%s\r\n",szError);
         }

	  else if (DIAG_ERROR_INVALID_CORESIGHT_CONFIG == ErrRet)
	  {
		  char szError[100];
		  strcpy(szError,"Invalid Coresight Debug Base Address\r\n");		
		  
		  csConfErr.Format("%s\r\n",szError);
	  }

      if(ErrRet == DIAG_ERROR_CORE_MISS_MATCH)
         {
         csNewString.Format("Selected ARM device is incorrect. Please select the correct one and retry.\r\n\r\n");
         }
      if(ErrRet == DIAG_ERROR_CPREG_FAILED || 
         ErrRet == DIAG_ERROR_TAP_LB_FAILED || 
         ErrRet == DIAG_ERROR_TPA_NOT_CONNECTED || 
         ErrRet == DIAG_ERROR_NO_ERROR ||
         ErrRet == DIAG_ERROR_INVALID_CONFIG ||
		 ErrRet == DIAG_ERROR_INVALID_CORESIGHT_CONFIG)
         {    
            if(tyRDIOnTargetDiagInfo.tyTpaConnected == TPA_NONE)
               csLoopBack.Format( "Opella-XD TPA not connected.\r\n");

            else if(tyRDIOnTargetDiagInfo.tyTpaConnected != TPA_NONE)
            {
               // Add TPA LoopBack
               if(tyRDIOnTargetDiagInfo.szTpaLBResult )
                  csLoopBack.Format( "Opella-XD TPA loopback test ok");
               else
                  csLoopBack.Format( "Opella-XD TPA loopback test failed");

               if (tyRDIOnTargetDiagInfo.szTgtVoltage == 0)
                  {     
                  // Add Target Volt
                  csTgtVolt.Format( "Target not detected. Ensure target is powered and properly connected.\r\n" ); 
                  }
               else
                  {
                  // Add Target Volt
                  csTgtVolt.Format( "Target voltage: %2.2fV", tyRDIOnTargetDiagInfo.szTgtVoltage );

                  // Add Idcode
                  csIdcodeNumber.Format( "Target TAP IDCODE: 0x%LX", tyRDIOnTargetDiagInfo.ulIDCode );

                  // Add Total Cores
                  csTotCores.Format( "%lx core(s) detected in target", tyRDIOnTargetDiagInfo.ulTotCores );

                  // Add IR length
                  csIRLength.Format( "Total Instruction Register(IR) length: %ld", tyRDIOnTargetDiagInfo.ulIRLen );
                  }
            }
         }

      if(((ErrRet == DIAG_ERROR_TAP_LB_FAILED || ErrRet == DIAG_ERROR_NO_ERROR))&&
         (tyRDIOnTargetDiagInfo.tyTpaConnected != TPA_NONE) && 
         (tyRDIOnTargetDiagInfo.szTgtVoltage != 0))
         {
         if (tyRDIOnTargetDiagInfo.ulCoreID==0) 
         csCoID0.Format( "No Coprocessor"  );
         else
         csCoID0.Format( "CP R0: 0x%08LX", tyRDIOnTargetDiagInfo.ulCoreID );

         // Add Core Co-processor Reg 1
         if (tyRDIOnTargetDiagInfo.ulCpReg1==0) 
         csCoID1.Format( "No Cache"  );
         else
         {
            csCoID1.Format( "CP R1: 0x%08LX", tyRDIOnTargetDiagInfo.ulCpReg1 );

            // Add Instruction Cache size
            csICsize.Format( "ICache: %s", tyRDIOnTargetDiagInfo.pszICache );

            // Add Data cache size
            csDCsize.Format( "DCache: %s", tyRDIOnTargetDiagInfo.pszDCache );
         }
         // Add Core Type Print
         if ( tyRDIOnTargetDiagInfo.szCoreMake == 1)
         csCoreType.Format( "( ARM %lx)", tyRDIOnTargetDiagInfo.ulPartno );
         else if (tyRDIOnTargetDiagInfo.szArm7 == 1) 
         csCoreType.Format( "( ARM 7)");

         // Add Architecture Version(ISA)
         csArch.Format( "Architecture: v%s", tyRDIOnTargetDiagInfo.pszArch);
         }
      
      if(ErrRet == DIAG_ERROR_USB_DRIVER || ErrRet == DIAG_ERROR_INVALID_SERIAL_NUMBER )
      {
         CString csErrorMessage1;
         CString csErrorMessage2;
         csErrorMessage1.Format("RDI driver not responding, cannot continue." );
         csErrorMessage2.Format("Check Opella-XD is properly connected.\r\n");
         csNewString.Format("%s\r\n%s\r\n",csErrorMessage1,csErrorMessage2);
      }

      else if(ErrRet == DIAG_ERROR_TPA_NOT_CONNECTED)
          csNewString.Format("%s\r\n",csLoopBack);

      else if(ErrRet == DIAG_ERROR_CPREG_FAILED)
      {     
                 
          if (tyRDIOnTargetDiagInfo.szTgtVoltage == 0)
            csNewString.Format("%s\r\n%s\r\n",csLoopBack,csTgtVolt);
         else  
            csNewString.Format("%s\r\n%s\r\n%s\r\n%s\r\n%s\r\n",csLoopBack,csIdcodeNumber,csTotCores,csIRLength,csTgtVolt);
      }

      else if(ErrRet == DIAG_ERROR_TAP_LB_FAILED)
      {         
      if (tyRDIOnTargetDiagInfo.szTgtVoltage == 0)
			csNewString.Format("%s\r\n%s\r\n",csLoopBack,csTgtVolt);
		else 
			csNewString.Format("%s\r\n%s %s\r\n%s\r\n%s\r\n%s\r\n",csLoopBack,csIdcodeNumber,csCoreType,csTotCores,csIRLength,csTgtVolt);
      }

      else if (ErrRet == DIAG_ERROR_INVALID_CONFIG)
         {
         if (tyRDIOnTargetDiagInfo.szTgtVoltage == 0)
            csNewString.Format("%s\r\n%s\r\n",csLoopBack,csTgtVolt);
         else
            csNewString.Format("%s\r\n%s\r\n\r\n%s\r\n",csLoopBack,csTgtVolt,csConfErr);
         }

	  else if (ErrRet == DIAG_ERROR_INVALID_CORESIGHT_CONFIG)
	  {
		  csNewString.Format("%s\r\n%s\r\n\r\n%s\r\n",csLoopBack,csTgtVolt,csConfErr);
      }

      else if(ErrRet == DIAG_ERROR_NO_ERROR)
      {        
         if (tyRDIOnTargetDiagInfo.szTgtVoltage != 0)
            {         
            // Add Target TAP LoopBack
            if(tyRDIOnTargetDiagInfo.uiTargetTAPNo != 0 )
               csTAPLoopBack.Format( "Target TAP loopback test ok. %d TAP(s) detected",tyRDIOnTargetDiagInfo.uiTargetTAPNo);
            else
               csTAPLoopBack.Format( "Target TAP loopback test failed");

            // Add Core Reg R/W
            if(tyRDIOnTargetDiagInfo.szRegCheck == 0 )
               csRegCheck.Format( "Core register read/write test ok\r\n");
            else if(tyRDIOnTargetDiagInfo.szRegCheck == 1 )
               csRegCheck.Format( "Core register read/write test failed\r\n"); 
            }
          if (tyRDIOnTargetDiagInfo.szTgtVoltage == 0)
            csNewString.Format("%s\r\n%s\r\n",csLoopBack,csTgtVolt);
          else if(tyRDIOnTargetDiagInfo.ulCoreID == 0 && tyRDIOnTargetDiagInfo.ulCpReg1 ==0 )
            csNewString.Format("%s\r\n%s\r\n%s %s\r\n%s\r\n%s\r\n%s\r\n%s\r\n",csLoopBack,csTAPLoopBack,csIdcodeNumber,csCoreType,csTotCores,csIRLength,csTgtVolt,csRegCheck);
          else if(tyRDIOnTargetDiagInfo.ulCoreID != 0 && tyRDIOnTargetDiagInfo.ulCpReg1 !=0 )
            csNewString.Format("%s\r\n%s\r\n%s %s\r\n%s %s\r\n%s %s\r\t %s\r\n%s\r\n%s\r\n%s\r\n%s\r\n",csLoopBack,csTAPLoopBack,csIdcodeNumber,csCoreType,csCoID0,csArch,csCoID1,csICsize,csDCsize,csTotCores,csIRLength,csTgtVolt,csRegCheck);
          else if(tyRDIOnTargetDiagInfo.ulCoreID != 0 && tyRDIOnTargetDiagInfo.ulCpReg1 ==0 ) 
            csNewString.Format("%s\r\n%s\r\n%s %s\r\n%s %s\r\n%s\r\n%s\r\n%s\r\n%s\r\n\n\n",csLoopBack,csTAPLoopBack,csIdcodeNumber,csCoreType,csCoID0,csArch,csTotCores,csIRLength,csTgtVolt,csRegCheck);
      }
   }
   else
      {
      UpdateData(TRUE);
      csNewString = NO_OPELLA_SELECTED_MESSAGE;
      }
     m_csTargetStatus += csNewString;
     UpdateData( FALSE );
     int nPos = m_csSetCheckTarget.GetLineCount();
     m_csSetCheckTarget.LineScroll( nPos );
     nPos = m_csSetCheckOpella.GetLineCount();
     m_csSetCheckOpella.LineScroll( nPos );

     
    /* int nLineCount = m_csSetCheckTarget.GetLineCount();
    //m_csSetCheckTarget.SetScrollPos( SB_VERT , nLineCount );
    //m_csSetCheckTarget.SetSel(0, -1);
    //m_csSetCheckTarget.ReplaceSel(m_csTargetStatus);
    // m_csSetCheckTarget.SetFocus();
     int iLastLineIndex = m_csSetCheckTarget.LineIndex(m_csSetCheckTarget.GetLineCount()-1);
     int iPos = iLastLineIndex + m_csSetCheckTarget.LineLength(iLastLineIndex);
     m_csSetCheckTarget.SetSel(iPos, iPos);
    // m_csSetCheckTarget.SetSel(nLineCount);*/


}


/****************************************************************************
     Function: ApplyChanges()
     Engineer: Jiju George T
     Input    : OpellaConfig - Configuration object to save configurations
     Output  : bool -  Return false if some required information is not
               filled by user (In this case notification about
               the error should be given to user). Else return true.
  Description: his function is called by the parent when user presses Apply.OK button.
               All the user configuration in the page should be saved to the passed
               COpellaConfig object.
Date           Initials    Description
24-Jul-2007       JG          Initial
****************************************************************************/
bool CDiagnosticsDlg::ApplyChanges( COpellaConfig &OpellaConfig )
{
    // Exchange data from controls to members
    if( IsWindow( m_hWnd ))
    {
        UpdateData();
    }

    // Save logging status
    OpellaConfig.EnableRDILogging( m_bEnableRDILogging ? TRUE : FALSE );

    return true;
}

/****************************************************************************
     Function: OnHardReset()
     Engineer: Suraj S
     Input    : void
     Output  : void
  Description: Called when user selects the button to do Hard Reset.
Date           Initials    Description
24-Jul-2007      SJ        Initial
12-Aug-2010		 SJ			Now uses singleton class instance to call RDI functions.
****************************************************************************/
void CDiagnosticsDlg::OnHardReset() 
{    
   TyUSBDeviceInstance tyUSBDeviceInstance = { 0 };
   TyError ErrRet;
   TyRDIOnTargetDiagInfo tyRDIOnTargetDiagInfo = {0};
   
   CString csNewString;

   CSingleton* instance = CSingleton::getInstance();

   strcpy (tyRDIOnTargetDiagInfo.pszCoreType ,m_DevicePage.GetSelectedCoreDetails() ); 
   tyRDIOnTargetDiagInfo.ulDbgCoreNumber = m_DevicePage.GetSelectedCoreNumber();
   tyRDIOnTargetDiagInfo.ptyJtagScanChain = m_DevicePage.GetJtagScanDetails();
   tyRDIOnTargetDiagInfo.tyJTAGClockFreqMode = m_TargetConnectionPage.GetFrequencyMode();   
   
   if(tyRDIOnTargetDiagInfo.tyJTAGClockFreqMode == Fixed)
      tyRDIOnTargetDiagInfo.ulFreqVal = m_TargetConnectionPage.GetFrequencyValue();

   if(tyRDIOnTargetDiagInfo.ulDbgCoreNumber > 9)
      {
      UpdateData(TRUE);
      csNewString.Format("No ARM cores selected\r\n. Please select atleast one ARM core\r\n\r\n");
      }
   
   else if( m_TargetConnectionPage.GetSelectedOpellaDetails( tyUSBDeviceInstance ))
      {
      UpdateData(TRUE);
      CString csResetStatus;
      //ErrRet =  PerformTargetHardReset( tyUSBDeviceInstance.szOpellaXDSerialNumber, &tyRDIOnTargetDiagInfo);
      ErrRet =  instance->m_PerformTargetHardReset( tyUSBDeviceInstance.szOpellaXDSerialNumber, &tyRDIOnTargetDiagInfo);
      if (tyRDIOnTargetDiagInfo.tyRDIHardResetDiagInfo.ucTgtAssertResetStatus == 1 && tyRDIOnTargetDiagInfo.tyRDIHardResetDiagInfo.ucTgtDeassertResetStatus == 0)
         csResetStatus.Format( "Target reset asserted\r\n");
      else
         csResetStatus.Format( "Target reset not asserted\r\n");

      csNewString.Format("%s\r\n",csResetStatus);
      }
   else      
      csNewString = NO_OPELLA_SELECTED_MESSAGE;   

   m_csTargetStatus += csNewString;
   
   UpdateData( FALSE );
   int nPos = m_csSetCheckTarget.GetLineCount();
   m_csSetCheckTarget.LineScroll( nPos );
   nPos = m_csSetCheckOpella.GetLineCount();
   m_csSetCheckOpella.LineScroll( nPos );

}

/****************************************************************************
     Function: OnHardResetAndDebug()
     Engineer: Jiju George T
     Input   : void
     Output  : void
  Description: Called when user selects the button to do Hard Reset and enter Debug mode.
  Date           Initials    Description
24-Jul-2007        JG           Initial
****************************************************************************/
/*
void CDiagnosticsDlg::OnHardResetAndDebug() 
{
    // TODO: Add your control notification handler code here
    
}
*/

/****************************************************************************
     Function: InitializeUIData()
     Engineer: Suresh P.C
     Input   : OpellaConfig - Configuration object to load configurations 
     Output  : None
  Description: This function will initialize GUI data variables using the 
               configuration object as argument. This function is called by
               Constructor of the class and base class while loading configuration.
Date           Initials    Description
27-Feb-2008       SPC         Initial
****************************************************************************/
void CDiagnosticsDlg::InitializeUIData( COpellaConfig &OpellaConfig )
{
    // Initialize the logging status
    m_bEnableRDILogging = OpellaConfig.IsRDILoggingEnabled();

    // Exchange data to controls if there is a window exists. 
    // It necessary because we are calling UpdateData() in ApplyChanges function. 
    if( IsWindow( m_hWnd ))
    {
        UpdateData( FALSE );
    }

    //Calling base class Initializations. 
    COpellaConfigPageBase::InitializeUIData( OpellaConfig );
}

TyError ParseCorsightDevice()
{
	return 0;
}
