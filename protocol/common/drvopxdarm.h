
/******************************************************************************
       Module: drvopxdarm.h
     Engineer: Suraj S
  Description: Header for Opella-XD driver layer for ARM
  Date           Initials    Description
  13-Jul-2007    SJ          Initial
******************************************************************************/

#ifndef _DRVOPXDARM_H_
#define _DRVOPXDARM_H_

// main header file for driver layer needs to be included
#include "drvopxd.h"

//TODO: Status bits in FPGA - need to be newly defined for ARM
#define TARGET_RESET 0x20

// Error codes used in drvlayer specific for ARM
// !!! should start at DRVOXP_ERROR_ARM_OFFSET and finish at (DRVOXP_ERROR_ARM_OFFSET + 0x0FFF) !!!


typedef enum {ARM7=1, ARM7S, ARM9, ARM11, CORTEXM, CORTEXA = 0x11, DAP, NOCORE=0} TyArmProcType;

 
/****************************************************************************
*** ARM Specific Functions                                               ***
****************************************************************************/
TyError DL_OPXD_Arm_ResetProc(TyDevHandle tyDevHandle, 
                              uint32_t ulCore, 
                              unsigned char bAssertReset, 
                              unsigned char *pbCurrStatus, 
                              unsigned char *pbPrevStatus);

TyError DL_OPXD_Arm_DebugRequest(TyDevHandle tyDevHandle, 
                                 uint32_t ulCore, 
                                 unsigned char ucDebugReq);

TyError DL_OPXD_Arm_DebugAcknowledge(TyDevHandle tyDevHandle, 
                                     unsigned char *pbDebugAck);

TyError DL_OPXD_Arm_SelectScanChain(TyDevHandle tyDevHandle, 
                                    uint32_t ulCore, 
                                    unsigned char ucScanChainNum, 
                                    unsigned char ucNumIntest, 
                                    TyArmProcType tyArmType);

TyError DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore, 
                                                TyArmProcType tyArmType);


TyError DL_OPXD_Arm_ReadICEBreaker(TyDevHandle tyDevHandle, 
                                   uint32_t ulCore, 
                                   uint32_t ulRegAddress, 
                                   uint32_t *pulData);

TyError DL_OPXD_Arm_WriteICEBreaker(TyDevHandle tyDevHandle, 
                                    uint32_t ulCore, 
                                    uint32_t ulRegAddress, 
                                    uint32_t *pulData);

TyError DL_OPXD_Arm_WriteByte(TyDevHandle tyDevHandle, 
                              uint32_t ulCore, 
                              TyArmProcType tyArmType,
                              uint32_t ulAddress, 
                              unsigned char *pulData);

TyError DL_OPXD_Arm_WriteWord(TyDevHandle tyDevHandle, 
                              uint32_t ulCore, 
                              TyArmProcType tyArmType,
                              uint32_t ulAddress, 
                              uint32_t *pulData);
TyError DL_OPXD_Arm_WriteWord_MemoryAP(TyDevHandle tyDevHandle, 
									   uint32_t ulCore, 
									   TyArmProcType tyArmType,
									   uint32_t ulAddress, 
									   uint32_t ulCount, 
									   uint32_t *pulData);

TyError DL_OPXD_Arm_ReadWord(TyDevHandle tyDevHandle, 
                             uint32_t ulCore, 
                             TyArmProcType tyArmType,
                             uint32_t ulAddress, 
                             uint32_t *pulData);

TyError DL_OPXD_Arm_ReadWord_MemoryAP(TyDevHandle tyDevHandle, 
									  uint32_t ulCore, 
									  TyArmProcType tyArmType,
									  uint32_t ulAddress, 
									  uint32_t ulCount, 
									  uint32_t *pulData);

TyError DL_OPXD_Arm_CheckForDataAbort(TyDevHandle tyDevHandle, 
                                      uint32_t ulCore, 
                                      TyArmProcType tyArmType, 
                                      unsigned char *pulData);

TyError DL_OPXD_Arm_WriteMultipleWords(TyDevHandle tyDevHandle, 
                                       uint32_t ulCore, 
                                       TyArmProcType tyArmType,
                                       uint32_t ulAddress, 
                                       uint32_t *pulData, 
                                       uint32_t ulLength);

TyError DL_OPXD_Arm_WriteMultipleBlocks(TyDevHandle tyDevHandle, 
                                        uint32_t ulCore, 
                                        TyArmProcType tyArmType,
                                        uint32_t ulAddress, 
                                        uint32_t *pulData, 
                                        uint32_t ulLength);

TyError DL_OPXD_Arm_Status_Proc(TyDevHandle tyDevHandle, 
                                uint32_t ulCore, 
                                TyArmProcType tyArmType, 
                                unsigned char ucGlobalScanChain, 
                                unsigned char ucGlobalExecStatus, 
                                unsigned char *pucStopProblem, 
                                unsigned char *pucExecuting, 
                                unsigned char *pucThumb, 
                                unsigned char *pucWatchPoint);

TyError DL_OPXD_Arm_Change_To_Arm_Mode(TyDevHandle tyDevHandle, 
                                       uint32_t ulCore, 
                                       TyArmProcType tyArmType, 
                                       unsigned char bWatchPoint, 
                                       unsigned char bUserHalt, 
                                       unsigned char *pucWordAligned);

TyError DL_OPXD_Arm_Perform_ARM_Nops(TyDevHandle tyDevHandle, 
                                     uint32_t ulCore, 
                                     TyArmProcType tyArmType, 
                                     uint32_t ulNoOfOps);

TyError DL_OPXD_Arm_ReadRegisters(TyDevHandle tyDevHandle, 
                                  uint32_t ulCore, 
                                  TyArmProcType tyArmType, 
                                  uint32_t *pulRegValues, 
                                  uint32_t ulRegArraySize);

TyError DL_OPXD_Arm_ReadMultipleBlocks(TyDevHandle tyDevHandle, 
                                       uint32_t ulCore, 
                                       TyArmProcType tyArmType,
                                       uint32_t ulStartAddress, 
                                       uint32_t *pulData, 
                                       uint32_t ulLength);

TyError DL_OPXD_Arm_ReadMultipleWords(TyDevHandle tyDevHandle, 
                                      uint32_t ulCore, 
                                      TyArmProcType tyArmType,
                                      uint32_t ulStartAddress, 
                                      uint32_t *pulData, 
                                      uint32_t ulLength);

TyError DL_OPXD_Arm_SetupExecuteProc(TyDevHandle tyDevHandle, 
                                     uint32_t ulCore, 
                                     TyArmProcType tyArmType,
                                     uint32_t *pulData);

TyError DL_OPXD_Arm_WriteRegisters(TyDevHandle tyDevHandle, 
                                   uint32_t ulCore, 
                                   TyArmProcType tyArmType,
                                   uint32_t *pulRegValues, 
                                   uint32_t ulRegArraySize);

TyError DL_OPXD_Arm_ExecuteProc(TyDevHandle tyDevHandle, 
                                uint32_t ulCore, 
                                TyArmProcType tyArmType,
                                unsigned char bThumbMode, 
                                uint32_t ulR0Value);

TyError DL_OPXD_Arm_SetSafeNonVectorAddress(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore, 
                                            TyArmProcType tyArmType,
                                            uint32_t ulSafeNonVectorAddress,
                                            unsigned char bIsEnabled);

TyError DL_OPXD_Arm_Change_To_Thumb_Mode(TyDevHandle tyDevHandle, 
                                         uint32_t ulCore, 
                                         TyArmProcType tyArmType, 
                                         uint32_t ulReg0, 
                                         uint32_t ulRegPC);

TyError DL_OPXD_ResetDap(TyDevHandle tyDevHandle);

TyError DL_OPXD_Arm_Restart(TyDevHandle tyDevHandle, 
                            uint32_t ulCore);

TyError DL_OPXD_Arm_InitTarget(TyDevHandle tyDevHandle,
							   uint32_t ulCore,
							   uint32_t ulTargetType);

TyError DL_OPXD_Arm_Write_ARM720CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC2_TTB,
                                             uint32_t ulC3_DAC,
                                             uint32_t ulC5_FSR,
                                             uint32_t ulC6_FAR,
                                             uint32_t ulC13_PID);

TyError DL_OPXD_Arm_Write_ARM740CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC2_TTB,
                                             uint32_t ulC3_DAC,
                                             uint32_t ulC5_FSR,
                                             uint32_t ulC6_FAR);

TyError DL_OPXD_Arm_Write_ARM920_922CP_Registers(TyDevHandle tyDevHandle, 
                                                 uint32_t ulCore,
                                                 unsigned char bRestoreInterpretedModeRegisters,
                                                 uint32_t ulC1_Control,
                                                 uint32_t ulC2_D_CacheTTB,
                                                 uint32_t ulC2_I_CacheTTB,
                                                 uint32_t ulC3_D_CacheDAC,
                                                 uint32_t ulC3_I_CacheDAC,
                                                 uint32_t ulC5_D_CacheFSR,
                                                 uint32_t ulC5_I_CacheFSR,
                                                 uint32_t ulC6_D_CacheFAR,
                                                 uint32_t ulC6_I_CacheFAR,
                                                 uint32_t ulC9_D_Cache_L_Down,
                                                 uint32_t ulC9_I_Cache_L_Down,
                                                 uint32_t ulC10_D_CacheTLB_L_Down,
                                                 uint32_t ulC10_I_CacheTLB_L_Down,
                                                 uint32_t ulC13_ProcessID);

TyError DL_OPXD_Arm_Write_ARM926CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC2_TTB,
                                             uint32_t ulC3_Domain_Access_Control,
                                             uint32_t ulC5_D_FSR,
                                             uint32_t ulC5_I_FSR,
                                             uint32_t ulC6_FAR,
                                             uint32_t ulC9_D_Lock_Down,
                                             uint32_t ulC9_I_Lock_Down,
                                             uint32_t ulC9_D_TCM,
                                             uint32_t ulC9_I_TCM,
                                             uint32_t ulC10_TLB_LockDown,
                                             uint32_t ulC13_PID);

TyError DL_OPXD_Arm_Write_ARM940_946CP_Registers(TyDevHandle tyDevHandle, 
                                                 uint32_t ulCore,
                                                 uint32_t ulC1_Control,
                                                 uint32_t ulC2_D_CacheConfiguration,
                                                 uint32_t ulC2_I_CacheConfiguration,
                                                 uint32_t ulC3_WriteBufferControl,
                                                 uint32_t ulC5_D_AccessPermission,
                                                 uint32_t ulC5_I_AccessPermission,
                                                 uint32_t ulC6_ProtectionRegion0,
                                                 uint32_t ulC6_ProtectionRegion1,
                                                 uint32_t ulC6_ProtectionRegion2,
                                                 uint32_t ulC6_ProtectionRegion3,
                                                 uint32_t ulC6_ProtectionRegion4,
                                                 uint32_t ulC6_ProtectionRegion5,
                                                 uint32_t ulC6_ProtectionRegion6,
                                                 uint32_t ulC6_ProtectionRegion7,
                                                 uint32_t ulC9_D_CacheTLB_L_Down,
                                                 uint32_t ulC9_I_CacheTLB_L_Down);

TyError DL_OPXD_Arm_Write_ARM966CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC15_BIST_Control,
                                             uint32_t ulC15_BIST_I_Address,
                                             uint32_t ulC15_BIST_I_General,
                                             uint32_t ulC15_BIST_D_Address,
                                             uint32_t ulC15_BIST_D_General);

TyError DL_OPXD_Arm_Read_ARM720CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC2_TTB,
                                            uint32_t *pulC3_DAC,
                                            uint32_t *pulC5_FSR,
                                            uint32_t *pulC6_FAR,
                                            uint32_t *pulC13_PID);

TyError DL_OPXD_Arm_Read_ARM740CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC2_TTB,
                                            uint32_t *pulC3_DAC,
                                            uint32_t *pulC5_FSR,
                                            uint32_t *pulC6_FAR);

TyError DL_OPXD_Arm_Read_ARM920_922CoProcessor(TyDevHandle tyDevHandle, 
                                               uint32_t ulCore,
                                               uint32_t ulCPRegAddr,    
                                               uint32_t *pulCPRegVal);

TyError DL_OPXD_Arm_Write_ARM920_922CoProcessor(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPRegAddr,    
                                                uint32_t ulCPRegVal);

TyError DL_OPXD_Arm_Read_ARM920_922CP_Registers(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t *pulC0_ID_Reg,
                                                uint32_t *pulC0_Cache_Type,
                                                uint32_t *pulC1_Control,
                                                uint32_t *pulC2_D_CacheTTB,
                                                uint32_t *pulC2_I_CacheTTB,
                                                uint32_t *pulC3_D_CacheDAC,
                                                uint32_t *pulC3_I_CacheDAC,
                                                uint32_t *pulC5_D_CacheFSR,
                                                uint32_t *pulC5_I_CacheFSR,
                                                uint32_t *pulC6_D_CacheFAR,
                                                uint32_t *pulC6_I_CacheFAR,
                                                uint32_t *pulC9_D_Cache_L_Down,
                                                uint32_t *pulC9_I_Cache_L_Down,
                                                uint32_t *pulC10_D_CacheTLB_L_Down,
                                                uint32_t *pulC10_I_CacheTLB_L_Down,
                                                uint32_t *pulC13_ProcessID);

TyError DL_OPXD_Arm_Read_ARM926CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC0_Cache_Type, 
                                            uint32_t *pulC0_TCM_Size, 
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC2_TTB,
                                            uint32_t *pulC3_Domain_Access_Control,
                                            uint32_t *pulC5_D_FSR,
                                            uint32_t *pulC5_I_FSR,
                                            uint32_t *pulC6_FAR,
                                            uint32_t *pulC9_D_Lock_Down,
                                            uint32_t *pulC9_I_Lock_Down,
                                            uint32_t *pulC9_D_TCM,
                                            uint32_t *pulC9_I_TCM,
                                            uint32_t *pulC10_TLB_LockDown,
                                            uint32_t *pulC13_PID);

TyError DL_OPXD_Arm_Read_ARM940_946CP_Registers(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t *pulC0_ID_Reg,    
                                                uint32_t *pulC0_Cache_Type, 
                                                uint32_t *pulC0_TCM_Size, 
                                                uint32_t *pulC1_Control,
                                                uint32_t *pulC2_D_CacheConfiguration,
                                                uint32_t *pulC2_I_CacheConfiguration,
                                                uint32_t *pulC3_WriteBufferControl,
                                                uint32_t *pulC5_D_AccessPermission,
                                                uint32_t *pulC5_I_AccessPermission,
                                                uint32_t *pulC6_ProtectionRegion0,
                                                uint32_t *pulC6_ProtectionRegion1,
                                                uint32_t *pulC6_ProtectionRegion2,
                                                uint32_t *pulC6_ProtectionRegion3,
                                                uint32_t *pulC6_ProtectionRegion4,
                                                uint32_t *pulC6_ProtectionRegion5,
                                                uint32_t *pulC6_ProtectionRegion6,
                                                uint32_t *pulC6_ProtectionRegion7,
                                                uint32_t *pulC9_D_CacheTLB_L_Down,
                                                uint32_t *pulC9_I_CacheTLB_L_Down);

TyError DL_OPXD_Arm_Read_ARM966CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC15_BIST_Control,
                                            uint32_t *pulC15_BIST_I_Address,
                                            uint32_t *pulC15_BIST_I_General,
                                            uint32_t *pulC15_BIST_D_Address,
                                            uint32_t *pulC15_BIST_D_General);

TyError DL_OPXD_Arm_Write_ARM968CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control);

TyError DL_OPXD_Arm_Read_ARM968CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control);

TyError DL_OPXD_Arm_Write_ARM940_946CoProcessor(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPRegAddr,    
                                                uint32_t ulCPRegVal);

TyError DL_OPXD_Arm_RunARM9TargetCacheFlush(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t ulStartOfAddressOfRoutine,
                                            uint32_t ulEndOfAddressOfRoutine);

TyError DL_OPXD_Arm_Write_ARM926CoProcessor(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t ulCPRegAddr,    
                                            uint32_t ulCPRegVal);

TyError DL_OPXD_Arm_InvalidateARM920_922ICache(TyDevHandle tyDevHandle, 
                                               uint32_t ulCore);

TyError DL_OPXD_Arm_Write_ARM720_740CoProcessor(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPInstruction,    
                                                uint32_t ulCPRegVal);

TyError DL_OPXD_Arm_CheckDCC (TyDevHandle tyDevHandle, 
                              uint32_t ulCore, 
                              TyArmProcType tyArmType, 
                              uint32_t *pulDCCReadData, 
                              unsigned char *pbDataToRead, 
                              uint32_t *pulDCCWriteData,
                              unsigned char *pbDataWritten);

TyError DL_OPXD_Arm_RegisterTest(TyDevHandle tyDevHandle, 
								 uint32_t ulCore, 
								 TyArmProcType tyArmType, 
								 unsigned char *bRegCheck);

TyError DL_OPXD_CortexA_RegisterTest(TyDevHandle tyDevHandle,
									 uint32_t ulCore, 
									 TyArmProcType tyArmType, 
									 unsigned char *bRegCheck);

//ARM 11 Function prototypes

TyError DL_OPXD_Arm_Enter_Debug_State(TyDevHandle tyDevHandle,
                                      uint32_t ulCore,
									  TyArmProcType TyProcType,
                                      uint32_t *pulData, 
                                      uint32_t ulArraySize);

TyError DL_OPXD_Arm_Leave_Debug_State(TyDevHandle tyDevHandle,
                                      uint32_t ulCore,
									  TyArmProcType tyArmType,
									  unsigned char uchThumb,
                                      uint32_t *pulData,
									  uint32_t ulArraySize);


TyError DL_OPXD_Arm_ReadDebug_Status_Control_Reg(TyDevHandle tyDevHandle,
                                                 uint32_t ulCore,
												 TyArmProcType TyProcType,
                                                 uint32_t *pulData);
TyError DL_OPXD_Arm_WriteVectorCacheReg(TyDevHandle tyDevHandle, 
										uint32_t ulCore,
										TyArmProcType TyProcType,
										uint32_t *pulData);

TyError DL_OPXD_Arm_ReadCPSR(TyDevHandle tyDevHandle, 
									  uint32_t ulCore, 
									  TyArmProcType tyArmType,
									  uint32_t *pulData);

TyError DL_OPXD_Arm_ReadCDCR(TyDevHandle tyDevHandle, 
									  uint32_t ulCore, 
									  TyArmProcType tyArmType,
									  uint32_t *pulData);



TyError DL_OPXD_Arm_Write_ARM1136CP_Registers(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPInstruction,    
                                                uint32_t* ulCPRegVal,
																uint32_t ulArraySize);

TyError DL_OPXD_Arm_Read_ARM1136CP_Registers(TyDevHandle tyDevHandle, 
															uint32_t ulCore,
															uint32_t *pulCP_Reg
															);
TyError DL_OPXD_Arm_WriteCP14DebugRegARM11(TyDevHandle tyDevHandle,
										   uint32_t ulCore,
										   TyArmProcType tyArmType,
										   uint32_t ulRegAddress,
										   uint32_t *pulData);

TyError DL_OPXD_Arm_Instn_Exec_Enable(TyDevHandle tyDevHandle,
												  uint32_t ulCore,
												  TyArmProcType tyArmType);

TyError DL_OPXD_CleanandDisable1136_Caches(TyDevHandle tyDevHandle,
										   uint32_t ulCore,
										   TyArmProcType tyArmType);

TyError DL_OPXD_Arm11_RegisterTest(TyDevHandle tyDevHandle, 
								   uint32_t ulCore, 
								   TyArmProcType tyArmType, 
								   unsigned char *bRegCheck);

TyError DL_OPXD_Cortex_RegisterTest(TyDevHandle tyDevHandle, 
								   uint32_t ulCore, 
								   TyArmProcType tyArmType, 
								   unsigned char *bRegCheck);


/* Cortex-M3 specific function prototypes */
TyError DL_OPXD_Arm_SetupHardBreakRes(
	TyDevHandle tyDevHandle, 
	uint32_t ulCore, 
    TyArmProcType tyArmType,
	uint32_t ulNumHardBreakPoints,
	uint32_t ulNumHardBreakData,
    uint32_t *pulData);

TyError DL_OPXD_Arm_SetupHardWatchRes(
	TyDevHandle tyDevHandle, 
	uint32_t ulCore, 
    TyArmProcType tyArmType,
	uint32_t ulNumHardWatchPoints,
	uint32_t ulNumHardWatchPointsData,
    uint32_t *pulData);

TyError DL_OPXD_ReadROMTable(TyDevHandle tyDevHandle, 
							 uint32_t ulCore,
							 uint32_t ulBaseAddr,
							 uint32_t ulApIndex,
							 uint32_t* pulCoresightComp);

TyError  DL_OPXD_ReadDebugReg(TyDevHandle tyDiagDevHandle, 
							  uint32_t ulCore,
							  uint32_t ulDebugRegAddr,
							  uint32_t ulMemApIndex,
							  uint32_t* pulProcType);

TyError  DL_OPXD_Config_Coresight(TyDevHandle tyDevHandle,
								  uint32_t ulCore,
								  uint32_t ulCoresightBaseAddr,
								  uint32_t ulDbgApIndex, 
								  uint32_t ulMemApIndex,
								  uint32_t ulJtagApIndex,
								  uint32_t ulUseMemApIndexMemAccess);

TyError DL_OPXD_Config_VectorCatch (TyDevHandle tyDevHandle, 
									uint32_t ulCore, 
									TyArmProcType tyArmType,
									uint32_t ulVectorCatchData);

TyError DL_OPXD_Arm_CortexACacheFlush (TyDevHandle tyDevHandle, 
									uint32_t ulCore, 
									uint32_t ulCacheCleanStartAddr,
									uint32_t ulStartAddr,
									uint32_t ulEndAddr);

TyError  DL_OPXD_InitIcepick( TyDevHandle tyDevHandle,
							  uint32_t ulCore,
							  uint32_t ulTISubPortNum );

//Typedefs
typedef struct _TyArm9Registors {
   uint32_t ulR0;
   uint32_t ulR1;
   uint32_t ulR2;
   uint32_t ulR3;
   uint32_t ulR4;
   uint32_t ulR5;
   uint32_t ulR6;
   uint32_t ulR7;
   uint32_t ulPC;
   uint32_t ulCPSR;
   uint32_t ulR8;
   uint32_t ulR9;
   uint32_t ulR10;
   uint32_t ulR11;
   uint32_t ulR12;
   uint32_t ulR13Usr;
   uint32_t ulR14Usr;
   //FIQ mode Registors
   uint32_t ulR8Fiq;
   uint32_t ulR9Fiq;
   uint32_t ulR10Fiq;
   uint32_t ulR11Fiq;
   uint32_t ulR12Fiq;
   uint32_t ulR13Fiq;
   uint32_t ulR14Fiq;
   uint32_t ulSPSRFiq;
   //IRQ mode Registors
   uint32_t ulR13Irq;
   uint32_t ulR14Irq;
   uint32_t ulSPSRIrq;
   //Supervisor mode Registors
   uint32_t ulR13Svc;
   uint32_t ulR14Svc;
   uint32_t ulSPSRSvc;
   //Abort mode Registors
   uint32_t ulR13Abt;
   uint32_t ulR14Abt;
   uint32_t ulSPSRAbt;
   
   //Undifined mode Registors
   uint32_t ulR13Und;
   uint32_t ulR14Und;
   uint32_t ulSPSRUnd;

} TyArm9Registors, *PTyArm9Registors;

//Typedefs
typedef struct _TyCortexARegistors {
	uint32_t ulR0;
	uint32_t ulR1;
	uint32_t ulR2;
	uint32_t ulR3;
	uint32_t ulR4;
	uint32_t ulR5;
	uint32_t ulR6;
	uint32_t ulR7;
	uint32_t ulPC;
	uint32_t ulCPSR;
	uint32_t ulR8;
	uint32_t ulR9;
	uint32_t ulR10;
	uint32_t ulR11;
	uint32_t ulR12;
	uint32_t ulR13Usr;
	uint32_t ulR14Usr;
	//FIQ mode registers
	uint32_t ulR8Fiq;
	uint32_t ulR9Fiq;
	uint32_t ulR10Fiq;
	uint32_t ulR11Fiq;
	uint32_t ulR12Fiq;
	uint32_t ulR13Fiq;
	uint32_t ulR14Fiq;
	uint32_t ulSPSRFiq;
	//IRQ mode registers
	uint32_t ulR13Irq;
	uint32_t ulR14Irq;
	uint32_t ulSPSRIrq;
	//Supervisor mode registers
	uint32_t ulR13Svc;
	uint32_t ulR14Svc;
	uint32_t ulSPSRSvc;
	//Abort mode registers
	uint32_t ulR13Abt;
	uint32_t ulR14Abt;
	uint32_t ulSPSRAbt;
	
	//Undefined mode registers
	uint32_t ulR13Und;
	uint32_t ulR14Und;
	uint32_t ulSPSRUnd;

	//Monitor mode registers
	uint32_t ulR13Mon;
	uint32_t ulR14Mon;
	uint32_t ulSPSRMon;
	
} TyCortexARegistors, *PTyCortexARegistors;

//Typedefs
typedef struct _TyCortexRegistors {
	uint32_t ulR0;
	uint32_t ulR1;
	uint32_t ulR2;
	uint32_t ulR3;
	uint32_t ulR4;
	uint32_t ulR5;
	uint32_t ulR6;
	uint32_t ulR7;	
	uint32_t ulR8;
	uint32_t ulR9;
	uint32_t ulR10;
	uint32_t ulR11;
	uint32_t ulR12;
	uint32_t ulCurrSp;
	uint32_t ulR14;
	uint32_t ulPC;
	uint32_t ulxPSR;
	uint32_t ulMSP;
	uint32_t ulPSP;
	uint32_t ulControl;	
} TyCortexRegistors, *PTyCortexRegistors;


// Function prototypes
/****************************************************************************
*** ARM Specific Functions                                                ***
****************************************************************************/
TyError DL_OPXD_Arm_RReg(TyDevHandle tyDevHandle, TyArm9Registors *ptyArm9Registors, char *pszParams);
TyError DL_OPXD_Arm_TEST(TyDevHandle tyDevHandle, char *pszParams);
TyError DL_OPXD_Arm_GetMultiCoreInfo(TyDevHandle tyDevHandle, 
                                   uint32_t *pulNoOfCores, 
                                   uint32_t *pulIRLengthArray);

/* Added for CortexM3 */
TyError DL_OPXD_Arm_StepProc(TyDevHandle tyDevHandle, 
                                uint32_t ulCore, 
                                TyArmProcType tyArmType);
#endif // _DRVOPXDARM_H_
