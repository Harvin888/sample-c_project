/******************************************************************************
       Module: drvopxdtarch.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD driver layer for ThreadArch(RedPine)
  Date           Initials    Description
  18-Sep-2007    VH          Initial
******************************************************************************/
#ifndef _DRVOPXDTARCH_H_
#define _DRVOPXDTARCH_H_

// main header file for driver layer needs to be included
#include "drvopxd.h"

// Error codes used in drvlayer specific for ThreadArch
// !!! should start at DRVOXP_ERROR_TARCH_OFFSET and finish at (DRVOXP_ERROR_TARCH_OFFSET + 0x0FFF) !!!

// Function prototypes
/****************************************************************************
*** ThreadArch Specific Functions                                         ***
****************************************************************************/
TyError DL_OPXD_ThreadArch_HoldCore(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bIndicateChange);
TyError DL_OPXD_ThreadArch_ReleaseCore(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bIndicateChange);
TyError DL_OPXD_ThreadArch_SingleStep(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bIndicateChange);
TyError DL_OPXD_ThreadArch_WriteLocation(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, unsigned short *pusData, unsigned char bHoldCoreBefore);
TyError DL_OPXD_ThreadArch_ReadLocation(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, unsigned short *pusData, unsigned char bHoldCoreBefore);
TyError DL_OPXD_ThreadArch_WriteBlock(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, uint32_t ulLength,
                                      unsigned short *pusData, unsigned char bHoldCoreBefore);
TyError DL_OPXD_ThreadArch_ReadBlock(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, uint32_t ulLength,
                                     unsigned short *pusData, unsigned char bHoldCoreBefore);

#endif // _DRVOPXDTARCH_H_

