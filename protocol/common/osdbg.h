/******************************************************************************
       Module: osdbg.h
     Engineer: Suresh P. C.
  Description: Header file for OS Debug support.

  Date           Initials    Description
  15-Jul-2009    SPC         Initial
******************************************************************************/

#ifndef _OSDBG_H_
#define _OSDBG_H_

#include "drvopxd.h"
#include "../mips/midlayer.h"

int32_t IsOSDbgEnabled(void);
int32_t IsOSMMUMappingNeeded(uint32_t ulAddrs);
int32_t SetOSMMUMapping(TyMLHandle *ptyMLHandle, uint32_t ulAddrs, int32_t iWriteAccess);
void UnsetOSMMUMapping(TyMLHandle *ptyMLHandle);

typedef int32_t (*TyInitfn)(TyMLHandle *ptyMLHandle, uint32_t ulPgDirAddrs, double dOSVer);
typedef int32_t (*TyCheckMapping)(uint32_t ulAddrs);
typedef int32_t (*TySetOSMapping)(TyMLHandle *ptyMLHandle, uint32_t ulAddrs, int32_t iWriteAccess);
typedef void (*TyUnsetOSMapping)(TyMLHandle *ptyMLHandle);
typedef int32_t (*TySetAppMMUMapping)(uint32_t ulAppCon, uint32_t ulPgDirAddrs);

typedef struct __OsLyrFuns
{
    TyInitfn IntOSfn;
    TyCheckMapping CheckMapping;
    TySetOSMapping SetMapping;
    TyUnsetOSMapping UnsetMapping;
    TySetAppMMUMapping SetAppMMU;
}TyOsLyrFuns;

typedef enum {
    Linux26
	} TyOS;

#endif //_OSDBG_H_
