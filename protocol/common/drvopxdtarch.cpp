/******************************************************************************
       Module: drvopxdtarch.cpp
     Engineer: Vitezslav Hola
  Description: Opella-XD driver layer for ThreadArch(RedPine)
  Date           Initials    Description
  18-Sep-2007    VH          Initial
******************************************************************************/
#include <stdio.h>   
#include <stdlib.h>
#include <string.h>

#include "libusb_dyn.h"
#include "drvopxdtarch.h"
#include "drvopxdcom.h"
#include "firmware/export/api_cmd.h"

#define MAX_TARCH_HALFWORDS_IN_BLOCK                  32768
#define TARCH_LONG_TIMEOUT                            25000          // timeout for block send

/****************************************************************************
*** External variables from drvopxd.cpp                                   ***
****************************************************************************/
extern TyDrvDeviceStruct ptyDrvDeviceTable[MAX_DEVICES_SUPPORTED];

/****************************************************************************
*** Local function prototypes                                             ***
****************************************************************************/

/****************************************************************************
*** ThreadArch Specific Functions                                         ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_ThreadArch_HoldCore
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               unsigned char bIndicateChange - indicate "target status" change on probe via LED
       Output: error code DRVOPXD_ERROR_xxx
  Description: hold ThreadArch core, core number refers to scanchain order (0 for 1st core, etc.)
               bIndicateChange flag is used to show target status change on probe LED if required
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_HoldCore(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bIndicateChange)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_HOLD_CORE;             // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bIndicateChange)
      pucBuffer[0x06] = 0x1;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   iPacketLen = TRANSMIT_LEN(8);                                                 // sending 8 bytes

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ThreadArch_ReleaseCore
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               unsigned char bIndicateChange - indicate "target status" change on probe via LED
       Output: error code DRVOPXD_ERROR_xxx
  Description: release ThreadArch core, core number refers to scanchain order (0 for 1st core, etc.)
               bIndicateChange flag is used to show target status change on probe LED if required
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_ReleaseCore(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bIndicateChange)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_RELEASE_CORE;          // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bIndicateChange)
      pucBuffer[0x06] = 0x1;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   iPacketLen = TRANSMIT_LEN(8);                                                 // sending 8 bytes

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ThreadArch_SingleStep
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               unsigned char bIndicateChange - indicate "target status" change on probe via LED
       Output: error code DRVOPXD_ERROR_xxx
  Description: do single step on ThreadArch core, core number refers to scanchain order (0 for 1st core, etc.)
               bIndicateChange flag is used to show target status change on probe LED if required
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_SingleStep(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bIndicateChange)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_SINGLE_STEP;           // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bIndicateChange)
      pucBuffer[0x06] = 0x1;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   iPacketLen = TRANSMIT_LEN(8);                                                 // sending 8 bytes

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ThreadArch_WriteLocation
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               uint32_t ulAddress - address to write
               unsigned short *pusData - pointer to data to write
               unsigned char bHoldCoreBefore - flag to hold core before writting
       Output: error code DRVOPXD_ERROR_xxx
  Description: write single halfword to given location
               flag bHoldCoreBefore allows to stop core before write access starts
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_WriteLocation(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, unsigned short *pusData, unsigned char bHoldCoreBefore)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_WRITE_LOCATION;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bHoldCoreBefore)
      pucBuffer[0x06] = 0x80;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                            // address
   *((unsigned short *)(pucBuffer + 0xC)) = *pusData;                            // set data
   iPacketLen = TRANSMIT_LEN(14);                                                // number of bytes to send

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ThreadArch_ReadLocation
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               uint32_t ulAddress - address to read
               unsigned short *pusData - pointer to read data
               unsigned char bHoldCoreBefore - flag to hold core before reading
       Output: error code DRVOPXD_ERROR_xxx
  Description: read single halfword from given location
               flag bHoldCoreBefore allows to stop core before read access starts
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_ReadLocation(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, unsigned short *pusData, unsigned char bHoldCoreBefore)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_READ_LOCATION;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bHoldCoreBefore)
      pucBuffer[0x06] = 0x80;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                            // address
   iPacketLen = TRANSMIT_LEN(12);                                                // number of bytes to send

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      *pusData = *((unsigned short *)(pucBuffer + 0x4));

   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ThreadArch_WriteBlock
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               uint32_t ulAddress - start address to write
               uint32_t ulLength - number of halfwords to write (up to 32768 halfwords)
               unsigned short *pusData - pointer to data to write
               unsigned char bHoldCoreBefore - flag to hold core before writting
       Output: error code DRVOPXD_ERROR_xxx
  Description: write multiple halfwords from given location
               flag bHoldCoreBefore allows to stop core before write access starts
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_WriteBlock(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, uint32_t ulLength,
                                      unsigned short *pusData, unsigned char bHoldCoreBefore)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   if (ulLength > MAX_TARCH_HALFWORDS_IN_BLOCK)
      return DRVOPXD_ERROR_TARGET_WRITE_MEMORY;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_WRITE_BLOCK;           // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bHoldCoreBefore)
      pucBuffer[0x06] = 0x80;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                            // start address
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of halfwords
   memcpy((void *)(pucBuffer + 0x10), (void *)pusData, ulLength * sizeof(uint16_t));              // 2 means size of halfword
   iPacketLen = TRANSMIT_LEN(16 + ((int32_t)ulLength)*2);                            // number of bytes to send

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, TARCH_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ThreadArch_ReadBlock
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 for 1st core or if MC is not used)
               uint32_t ulAddress - start address to read
               uint32_t ulLength - number of halfwords to read (up to 32768 halfwords)
               unsigned short *pusData - pointer to read data
               unsigned char bHoldCoreBefore - flag to hold core before reading
       Output: error code DRVOPXD_ERROR_xxx
  Description: read multiple halfwords from given location
               flag bHoldCoreBefore allows to stop core before read access starts
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_ThreadArch_ReadBlock(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulAddress, uint32_t ulLength,
                                     unsigned short *pusData, unsigned char bHoldCoreBefore)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   if (ulLength > MAX_TARCH_HALFWORDS_IN_BLOCK)
      return DRVOPXD_ERROR_TARGET_READ_MEMORY;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARCH_READ_BLOCK;           // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bHoldCoreBefore)
      pucBuffer[0x06] = 0x80;
   else
      pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                            // start address
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of halfwords
   iPacketLen = TRANSMIT_LEN(16);                                                // number of bytes to send

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, TARCH_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      memcpy((void *)pusData, (void *)(pucBuffer + 0x08), ulLength * sizeof(uint16_t));           // copy data from buffer into specified location

   // synchronization not used
   return(tyResult);
}

/****************************************************************************
*** Local functions                                                       ***
****************************************************************************/


