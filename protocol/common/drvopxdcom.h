/******************************************************************************
       Module: drvopxdcom.h
     Engineer: Vitezslav Hola
  Description: Header for driver layer Opella-XD interface (common header)
  Date           Initials    Description
  16-Jul-2007    VH          Initial
******************************************************************************/
#ifndef DRVOPXDCOM_H_
#define DRVOPXDCOM_H_

/****************************************************************************
*** Defines used in drvopxd.cpp and drvopxdyyy.cpp for specific targets   ***
****************************************************************************/

//*** Opella-XD firmware/diskware USB endpoints ***
//      --- EPA --> 
// PC   <-- EPB ---   Opella-XD
//      --- EPC -->
//      <-- EPD ---
// 
#define EPA                0x01                 // endpoint to send commands into Opella-XD
#define EPB                0x82                 // enpoint to send response to commands from Opella-XD
#define EPC                0x03                 // endpoint to send raw data (very fast transfers) into Opella-XD
#define EPD                0x84                 // endpoint to receive raw data (very fast transfers) from Opella-XD

//*** Macros and buffer size for packet sending/receiving ***
#define SMALL_BUFFER_LEN                        128       // small buffer
#define BUFFER_PAYLOAD                          (16*1024) //andre: restricted from 64K to 16K for LPC1837
#define BIG_BUFFER_LEN                          (SMALL_BUFFER_LEN + BUFFER_PAYLOAD)  // big buffer + space for header
#define TRANSMIT_LEN(val)                       ((val) | 0x1)                        // always odd length
//***

/****************************************************************************
*** Typedefs                                                              ***
****************************************************************************/

// Local type definitions
typedef struct _TyDrvDeviceStruct {
   unsigned char bOpenedDevice;                          // device used?
   unsigned char bDeviceShared;                          // is device handle shared
   char pszSerialNumber[MAX_SERIAL_NUMBER_LEN];          // device serial number 
   int32_t iDeviceInstances;                                 // device instances open
   usb_dev_handle *ptyUsbDevHandle;                      // handle to open USB driver
   TyDiskwareType tyDiskwareType;                        // type of diskware
   TyFpgawareType tyFpgawareType;                        // type of fpgaware
   int32_t iConfigInstances;                                 // how many threads configured device
   int32_t iDataTimeout;                                     // timeout for data
   uint32_t *pulTransferBuffer;                     // pointer to shared transfer buffer (sharing between threads is required)
}TyDrvDeviceStruct;

/****************************************************************************
*** Common function prototypes                                            ***
****************************************************************************/
int32_t DL_OPXD_ConvertDwErrToDrvlayerErr(uint32_t ulErrorCode);

#endif // DRVOPXDCOM_H_

