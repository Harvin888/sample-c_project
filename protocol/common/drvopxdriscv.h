/******************************************************************************
	   Module: drvopxdriscv.h
	 Engineer: Ashutosh Garg
  Description: Header for Opella-XD driver layer for RISCV
  Date           Initials    Description
  10-sep-2018      AG          Initial
******************************************************************************/
#pragma once
#ifndef _DRVOPXDRISCV_H_
#define _DRVOPXDRISCV_H_

#include "drvopxd.h"

#define MAX_RISCV_WORDS_ACCESS                          56250                // maximum words for one access

typedef enum { RISCV_JTAG_TPA_R1 } TyRiscvTargetType;

/***********************************************************************************
*** RISCV Specific functions*****
************************************************************************************/
TyError DL_OPXD_RISCV_SelectTarget(TyDevHandle tyDevHandle, TyRiscvTargetType tyTargetType);

TyError DL_OPXD_RISCV_Discovery(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t *pulDiscoveryData,uint32_t ulHartNo);
TyError DL_OPXD_RISCV_HaltHart(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId,uint32_t ulAbits);
TyError DL_OPXD_RISCV_RunHart(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulAbits);
TyError DL_OPXD_RISCV_SingleStep(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_ResetProc(TyDevHandle tyDevHandle, unsigned char bAssertReset, unsigned char bEnableResetSensing);
TyError DL_OPXD_RISCV_Read_Register(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_Write_Register(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_WriteBlock(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulSize,uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_ReadBlock(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_StatusProc(TyDevHandle tyDevHandle, uint32_t usTapId, uint32_t uiTapId, uint32_t ulHartId,unsigned char *pucGlobalStatus, unsigned char *pucStopProblem,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_WriteARegister(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddess,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_ReadARegister(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulRegValue , uint32_t ulRegAddress,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_WriteWord(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_ReadWord(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_WriteDPC(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulValue,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_Add_Trigger(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulTriggerAddr, uint32_t ulTriggerNum, uint32_t *ulTriggerOut,uint32_t utType,uint32_t ulArchSize, uint32_t ulAbits, uint32_t ulLength, uint32_t ulMatch, uint32_t ulResrequired);
TyError DL_OPXD_RISCV_Remove_Trigger(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId,uint32_t ulTriggerNum,uint32_t ulArchSize, uint32_t ulAbits, uint32_t ulResUsed);
TyError DL_OPXD_RISCV_Trigger_Info(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *pulTriggerInfo,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_RISCV_Mechanism(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulRegMec, uint32_t ulMemMec, uint32_t ulCsrSupp, uint32_t ulFloatingProc,uint32_t ulFence_i);
TyError DL_OPXD_RISCV_TargetReset(TyDevHandle tyDevHandle,uint32_t ulHartId);
TyError DL_OPXD_RISCV_WriteCSR(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulRegisterValue, uint32_t ulRegisternumber,uint32_t ulArchSize, uint32_t ulAbits);
TyError DL_OPXD_ConfigureVega(TyDevHandle tyDevHandle, uint32_t uiTapId, unsigned int uiInstrtoSelectADI, unsigned int uiCore, unsigned int uiIRLen);
TyError DL_OPXD_RISCV_Discover_Harts(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t *pulNumberOfHarts);
TyError DL_OPXD_ConfigureOpxddiag(TyDevHandle tyDevHandle, unsigned long *pulArray);
TyError DL_OPXD_OpxddiagRiscvInit(TyDevHandle tyDevHandle, uint32_t uiTapId, unsigned long* pulArray);

#endif