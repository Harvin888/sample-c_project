/******************************************************************************
	   Module: drvopxdarc.cpp
	 Engineer: Ashutosh Garg
  Description: Opella-XD driver layer for RISCV
  Date           Initials    Description
  10-Sep-2018    AG          Initial
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifdef __LINUX
#include <stdarg.h>
#endif

#include "libusb_dyn.h"
#include "../../protocol/rdi/opxdarmdll/common.h"
#include "../../firmware/export/api_cmd.h"
#include "../../firmware/export/api_err.h"

//#local defines
#define RISCV_SEND_TIMEOUT                     5000                    // timeout to send command into Opella-XD (5 seconds)
#define RISCV_RECEIVE_SHORT_TIMEOUT            5000                  // timeout to receive response from Opella-XD (5 seconds)


#include "drvopxdriscv.h"
#include "drvopxdcom.h"

extern TyDrvDeviceStruct ptyDrvDeviceTable[MAX_DEVICES_SUPPORTED];

/****************************************************************************
	 Function: DL_OPXD_ConfigureOpxddiag
	 Engineer: Chandrasekar B R
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned long *pulArray - value to set such as fence_supp, Direct_access_CSR, Double_precision,
			   Memory_mech, Register_Mech
	   Output: error code DRVOPXD_ERROR_xxx
  Description: calls the command code in diskware to set the riscv parameters for Opxddiag
Date           Initials    Description
05-Jun-2019    CBR         Initial
****************************************************************************/
TyError DL_OPXD_ConfigureOpxddiag(TyDevHandle tyDevHandle, unsigned long *pulArray)
{
	usb_dev_handle* ptyUsbDevHandle;
	unsigned char* pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char*)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t*)(pucBuffer + 0x0)) = CMD_CODE_RISCV_SET_OPXDDIAG_PARAMS;
	*((uint32_t*)(pucBuffer + 0x4)) = pulArray[0];  //Access method to CSR
	*((uint32_t*)(pucBuffer + 0x8)) = pulArray[1];  // Double precision
	*((uint32_t*)(pucBuffer + 0xc)) = pulArray[2];	//fence support
	*((uint32_t*)(pucBuffer + 0x10)) = pulArray[3];	//Memory Mechanism 

	iPacketLen = TRANSMIT_LEN(20);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char*)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char*)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t*)(pucBuffer + 0x00)));
	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_OpxddiagRiscvInit
	 Engineer: Chandrasekar B R
		Input: TyDevHandle tyDevHandle - handle to open device
			   uint32_t uiTapId - pass the tap number
			   unsigned long *pulArray - get the Abits, Idle count and version
	   Output: error code DRVOPXD_ERROR_xxx
  Description: calls the command code in diskware to set the riscv parameters for Opxddiag
Date           Initials    Description
10-Jun-2019    CBR         Initial
****************************************************************************/
TyError DL_OPXD_OpxddiagRiscvInit(TyDevHandle tyDevHandle, uint32_t uiTapId, unsigned long *pulArray)
{
	usb_dev_handle* ptyUsbDevHandle;
	unsigned char* pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char*)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t*)(pucBuffer + 0x0)) = CMD_CODE_RISCV_OPXDDIAG_RV_INIT;
	*((uint32_t*)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t*)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId


	iPacketLen = TRANSMIT_LEN(12);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char*)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char*)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t*)(pucBuffer + 0x00)));
		if (tyResult == DRVOPXD_ERROR_NO_ERROR)
		{
			memcpy((void*)pulArray, (void*)(pucBuffer + 0x04), 0xc);
		}
	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_SelectTarget
	 Engineer: Rejeesh S Babu
		Input: TyDevHandle tyDevHandle - handle to open device
			   TyRiscvTargetType tyTargetType - target type
	   Output: error code DRVOPXD_ERROR_xxx
  Description: select proper RISCV target type (currently only one type)
Date           Initials    Description
26-Feb-2019    RSB          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_SelectTarget(TyDevHandle tyDevHandle, TyRiscvTargetType tyTargetType)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	// create packet
	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_SET_TARGET_CONFIG;       // command code
	switch (tyTargetType)
	{
	case RISCV_JTAG_TPA_R1: // ARC target with JTAG TPA revision 1
		pucBuffer[0x04] = 0x04;
		break;
	default:
		pucBuffer[0x04] = 0x01;
		break;
	}
	// fill empty reserved values
	pucBuffer[0x05] = 0;    pucBuffer[0x06] = 0;    pucBuffer[0x07] = 0;
	*((uint32_t *)(pucBuffer + 0x08)) = 0;
	iPacketLen = TRANSMIT_LEN(12);
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	// synchronization not used
	return(tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_HaltHart
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Halt the selected ulHartId for usCore
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_HaltHart(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_HALT;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;

	iPacketLen = TRANSMIT_LEN(20);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
 	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_RunHart
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Run the hart i.e ulHartId for usCore
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_RunHart(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_RUN;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;

	iPacketLen = TRANSMIT_LEN(20);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_SingleStep
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Run step by step in DebugMode
Date           Initials    Description
10-Sep-2018     AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_SingleStep(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_SINGLE_STEP;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulArchSize;
	iPacketLen = TRANSMIT_LEN(24);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_WriteBlock
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulNumberOfWords - number of words (max 16384)
			   uint32_t ulAddress - starting memory address/register index
			   uint32_t *pulData - pointer to data
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write specified number of words into RISCV Memory
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_WriteBlock(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	if ((pulData == NULL))
	{
		return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_WRITE_MEMORY_BLOCK;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulAddress;
	*((uint32_t *)(pucBuffer + 0x18)) = ulSize;
	*((uint32_t *)(pucBuffer + 0x1c)) = ulCount;
	*((uint32_t *)(pucBuffer + 0x20)) = ulArchSize;
	memcpy((unsigned char *)(pucBuffer + 0x24), (unsigned char *)pulData, (uint64_t)ulSize * ulCount) ;

	iPacketLen = TRANSMIT_LEN(36 + ((int)(ulSize * ulCount)));
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_ReadBlock
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulNumberOfWords - number of words (max 16384)
			   uint32_t ulAddress - starting memory address/register index
			   uint32_t *pulData - pointer to data
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Read specified number of words from RISCV Memory
Date           Initials    Description
10-Sep-2018     AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_ReadBlock(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCounts, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	if ((pulData == NULL))
	{
		return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_READ_MEMORY_BLOCK;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((unsigned short *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulAddress;
	*((uint32_t *)(pucBuffer + 0x18)) = ulSize;
	*((uint32_t *)(pucBuffer + 0x1c)) = ulCounts;
	*((uint32_t *)(pucBuffer + 0x20)) = ulArchSize;
	iPacketLen = TRANSMIT_LEN(36);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	if (tyResult == DRVOPXD_ERROR_NO_ERROR)
	{
		memcpy((void *)pulData, (void *)(pucBuffer + 0x04), (uint64_t)ulSize * ulCounts);

	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_Read_Register
	 Engineer: Ashutosh Grag
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t *pulRegValues - Pointer to register values
               uint32_t ulRegArraySize - Size of the register array to read
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Reads the RISCV processor registers 
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Read_Register(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	
	if (uiTapId >= MAX_TAPS_ON_SCANCHAIN)
	{
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	}

	if (pulRegValues == NULL)
	{
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_READ_REGISTERS;
	* ((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)ulRegArraySize;
	*((uint32_t *)(pucBuffer + 0x18)) = (uint32_t)ulArchSize;
	iPacketLen = TRANSMIT_LEN(28);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		if (tyResult == DRVOPXD_ERROR_NO_ERROR)
		{
			//increase the reg value as per the arch
			switch (ulArchSize/32)
			{
			case 1:
				memcpy((void *)pulRegValues, (void *)(pucBuffer + 0x04), (ulRegArraySize) * sizeof(uint32_t));
				break;
			case 2:
				memcpy((void *)pulRegValues, (void *)(pucBuffer + 0x04), (ulRegArraySize * sizeof(uint16_t)) * sizeof(uint32_t));
				break;
			case 4:
				memcpy((void *)pulRegValues, (void *)(pucBuffer + 0x04), (ulRegArraySize * sizeof(uint32_t)) * sizeof(uint32_t));
				break;
			default:
				memcpy((void *)pulRegValues, (void *)(pucBuffer + 0x04), (ulRegArraySize) * sizeof(uint32_t));
				break;
			}
			
		}
	}

	return (tyResult);	
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_Write_Register
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t *pulRegValues - Pointer to register values
			   uint32_t ulRegArraySize - Size of the register array to read
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write the RISCV processor registers
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Write_Register(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	if (uiTapId >= MAX_TAPS_ON_SCANCHAIN)
	{
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	}

	if (pulRegValues == NULL)
	{
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_WRITE_REGISTERS;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)ulRegArraySize;
	memcpy((uint32_t *)(pucBuffer + 0x18), (uint32_t *)(pulRegValues), (ulRegArraySize * sizeof(uint32_t)));
	iPacketLen = TRANSMIT_LEN(24 + (int)ulRegArraySize*4);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_Discovery
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Defines the features that can be implemented by the target
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Discovery(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t *pulDiscoveryData,uint32_t ulHartNo)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_DISCOVERY;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = ulHartNo;//(unsigned short)usCore;

	iPacketLen = TRANSMIT_LEN(8);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		if (tyResult == DRVOPXD_ERROR_NO_ERROR)
		{
			memcpy((void *)pulDiscoveryData, (void *)(pucBuffer + 0x04), 10 * 4);
		}
	}

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_Discover_Harts
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   uint32_t *pulNumberOfHarts - number of harts detected
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Detects the number of harts on TAP
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Discover_Harts(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t *pulNumberOfHarts)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_DISCOVER_HARTS;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	iPacketLen = TRANSMIT_LEN(8);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		if (tyResult == DRVOPXD_ERROR_NO_ERROR)
		{
			memcpy((void *)pulNumberOfHarts, (void *)(pucBuffer + 0x04), 2*4);
		}
	}

	return (tyResult);
}


/****************************************************************************
	 Function: DL_OPXD_RISCV_StatusProc
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   unsigned char *pucGlobalStatus - 1 - halted, 0 - running  
			   unsigned char *pucStopProblem - if halted reason for halting
	   Output: error code DRVOPXD_ERROR_xxx
  Description: tell the status of the selected hart
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_StatusProc(TyDevHandle tyDevHandle, uint32_t usTapId, uint32_t uiTapId, uint32_t ulHartId, unsigned char *pucGlobalStatus, unsigned char *pucStopProblem,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	
	if (uiTapId >= MAX_TAPS_ON_SCANCHAIN)
	{
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_STATUS_PROC;
	*((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)usTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)ulArchSize;

	iPacketLen = TRANSMIT_LEN(24);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		*pucGlobalStatus = *((unsigned char *)(pucBuffer + 0x04));
		*pucStopProblem = *((unsigned char *)(pucBuffer + 0x05));
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_WriteARegister
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulRegValue - value to be written on address location
			   uint32_t ulRegAddress - address of the GPR
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write a specified register
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_WriteARegister(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	if (uiTapId >= MAX_TAPS_ON_SCANCHAIN)
	{
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_WRITE_REGISTER;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)ulRegAddress;
	*((uint32_t *)(pucBuffer + 0x18)) = (uint32_t)ulArchSize;
	memcpy((void *)(pucBuffer + 0x1C), (void *)ulRegValue, (ulArchSize / 32) * sizeof(uint32_t));
	iPacketLen = TRANSMIT_LEN(28 + (int)(ulArchSize / 32) * 4);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}

	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_ReadARegister
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulRegValue - value of the GPR read from the target
			   uint32_t ulRegAddress - address of the GPR
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write a specified register
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_ReadARegister(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_READ_REGISTER;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)ulRegAddress;
	*((uint32_t *)(pucBuffer + 0x18)) = (uint32_t)ulArchSize;
	iPacketLen = TRANSMIT_LEN(28);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
 	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		memcpy((void *)ulRegValue, (void *)(pucBuffer + 0x04), (ulArchSize / 32) * sizeof(uint32_t));
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}

	return (tyResult);
}


TyError DL_OPXD_RISCV_ResetProc(TyDevHandle tyDevHandle, unsigned char bAssertReset, unsigned char bEnableResetSensing)
{
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_WriteWord
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulAddress - address of the word
			   uint32_t *pulData - pointer to the value to be written 
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write a specified word in memory
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_WriteWord(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	if ((pulData == NULL))
	{
		return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
	}

	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_WRITE_MEMORY;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulAddress;
	*((uint32_t *)(pucBuffer + 0x18)) = *pulData;

	iPacketLen = TRANSMIT_LEN(28);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_ReadWord
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulAddress - address of the word
			   uint32_t *pulData - pointer for the value at the word
	   Output: error code DRVOPXD_ERROR_xxx
  Description: read a specified word in memory
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_ReadWord(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_READ_MEMORY;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulAddress;

	iPacketLen = TRANSMIT_LEN(18);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		*pulData = *((uint32_t *)(pucBuffer + 0x04));
	}
	return (tyResult);
}
/****************************************************************************
	 Function: DL_OPXD_RISCV_WriteDPC
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulValue - PC value
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write value to PC
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_WriteDPC(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulValue,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_WRITE_CSR;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = 32;
	*((uint32_t *)(pucBuffer + 0x18)) = ulArchSize;
	memcpy((void *)(pucBuffer + 0x1C), (void *)ulValue, (ulArchSize / 32) * sizeof(uint32_t));
	iPacketLen = TRANSMIT_LEN(28 + (int)(ulArchSize / 32) * 4);

	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_WriteCSR
	 Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usCoreId   : CoreId
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegisterValue : value of CSR to write
				  uint32_t ulRegisterNumber : CSR number
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
	   Output: error code DRVOPXD_ERROR_xxx
  Description: write value to CSR
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_WriteCSR(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *ulRegisterValue, uint32_t ulRegisternumber,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_WRITE_CSR;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulRegisternumber;
	*((uint32_t *)(pucBuffer + 0x18)) = ulArchSize;
	memcpy((void *)(pucBuffer + 0x1C), (void *)ulRegisterValue, (ulArchSize / 32) * sizeof(uint32_t));
	iPacketLen = TRANSMIT_LEN(28 + (int)(ulArchSize / 32) * 4);

	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_Add_Trigger
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulTriggerAddr - Address where trigger has to be added
			   uint32_t ulTriggerNum - Trigger Number to be Acessed
			   uint32_t *ulTriggerOut - value after adding the trigger
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Add Trigger(H/W Breakpoint) at a specific address
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Add_Trigger(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulTriggerAddr, uint32_t ulTriggerNum, uint32_t *ulTriggerOut,uint32_t ulType,uint32_t ulArchSize,uint32_t ulAbits, uint32_t ulLength, uint32_t ulMatch, uint32_t ulResrequired)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_ADD_TRIGGER;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulTriggerAddr;
	*((uint32_t *)(pucBuffer + 0x18)) = ulTriggerNum;
	*((uint32_t *)(pucBuffer + 0x1C)) = ulType;
	*((uint32_t *)(pucBuffer + 0x20)) = ulArchSize;
	*((uint32_t *)(pucBuffer + 0x24)) = ulLength;
	*((uint32_t *)(pucBuffer + 0x28)) = ulMatch;
	*((uint32_t *)(pucBuffer + 0x2C)) = ulResrequired;
	iPacketLen = TRANSMIT_LEN(48);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
 	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{	
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		*ulTriggerOut = *((uint32_t *)(pucBuffer + 0x04));
	}
	return (tyResult);
}
/****************************************************************************
	 Function: DL_OPXD_RISCV_Remove_Trigger
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulTriggerNum - Trigger Number to be Acessed
			   uint32_t ulArchSize    -Architecture Size
			   uint32_t ulAbits       -Address bit size  
			   uint32_t ulResUsed     -resources used by trigger
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Remove a specific  Trigger(H/W Breakpoint) 
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Remove_Trigger(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t ulTriggerNum,uint32_t ulArchSize,uint32_t ulAbits, uint32_t ulResUsed)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_REMOVE_TRIGGER;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulTriggerNum;
	*((uint32_t *)(pucBuffer + 0x18)) = ulArchSize;
	*((uint32_t *)(pucBuffer + 0x1C)) = ulResUsed;

	iPacketLen = TRANSMIT_LEN(32);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	return (tyResult); 
}
/****************************************************************************
	 Function: DL_OPXD_RISCV_Trigger_Info
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned short usCoreId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t pulTriggerInfo - return number of triggers available
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Number of triggers per Core for Discovery
Date           Initials    Description
10-Sep-2018      AG          Initial
****************************************************************************/


TyError DL_OPXD_RISCV_Trigger_Info(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulHartId, uint32_t *pulTriggerInfo,uint32_t ulArchSize,uint32_t ulAbits)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_TIRGGER_INFO;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)ulHartId;
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)ulAbits;
	*((uint32_t *)(pucBuffer + 0x14)) = ulArchSize;


	iPacketLen = TRANSMIT_LEN(24);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
 	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		if (tyResult == DRVOPXD_ERROR_NO_ERROR)
		{
			memcpy((void *)pulTriggerInfo, (void *)(pucBuffer + 0x04), 5 * 4);
		}
	}
	return (tyResult);
}
/****************************************************************************
	 Function: DL_OPXD_RISCV_Mechanism
	 Engineer: Ashutosh Garg
		Input: TyDevHandle tyDevHandle - handle to open device
			   uint32_t ulRegMec - Reg support for connected Target
			   uint32_t ulMemMec - Mem Support for connected Target
			   uint32_t ulCsrSupp - CSR support for connected Target
	   Output: error code DRVOPXD_ERROR_xxx
  Description: sytsem mechanism for connected target

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_Mechanism(TyDevHandle tyDevHandle, uint32_t uiTapId, uint32_t ulRegMec, uint32_t ulMemMec, uint32_t ulCsrSupp, uint32_t ulFloatingProc,uint32_t ulFence_i)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_MECHANISM;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x08)) = 0; //TODO: Replace with uiCoreId
	*((uint32_t *)(pucBuffer + 0x0C)) = ulRegMec;
	*((uint32_t *)(pucBuffer + 0x10)) = ulMemMec;
	*((uint32_t *)(pucBuffer + 0x14)) = ulCsrSupp;
	*((uint32_t *)(pucBuffer + 0x18)) = ulFloatingProc;
	*((uint32_t *)(pucBuffer + 0x1C)) = ulFence_i;
	iPacketLen = TRANSMIT_LEN(32);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_RISCV_TargetReset
	 Engineer: Harvinder Singh
		Input: TyDevHandle tyDevHandle - handle to open device
		       uint32_t ulHartId - hart id of hart to halt.
	   Output: error code DRVOPXD_ERROR_xxx
  Description: Reset RISCV Target.
Date           Initials    Description
06-June-2019     HS          Initial
****************************************************************************/
TyError DL_OPXD_RISCV_TargetReset(TyDevHandle tyDevHandle,uint32_t ulHartId)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_RESET_TARGET;
	*((uint32_t *)(pucBuffer + 0x4)) = ulHartId;
	iPacketLen = TRANSMIT_LEN(4);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	return (tyResult);
}

/****************************************************************************
	 Function: DL_OPXD_ConfigureVega
	 Engineer: 
		Input: TyDevHandle tyDevHandle - handle to open device
			   unsigned int uiInstrtoSelectADI - 
			   unsigned int uiCore - 
			   unsigned int uiIRLen - 
	   Output: error code DRVOPXD_ERROR_xxx
  Description: 

Date           Initials    Description
01-Nov-2018                 Initial
****************************************************************************/
TyError DL_OPXD_ConfigureVega(TyDevHandle tyDevHandle, uint32_t uiTapId, unsigned int uiInstrtoSelectADI, unsigned int uiCore, unsigned int uiIRLen)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int iPacketLen;

	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
	{
		return DRVOPXD_ERROR_INVALID_HANDLE;
	}
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_RISCV_SET_VEGA_PARAMS;
	*((uint32_t *)(pucBuffer + 0x04)) = uiTapId;
	*((uint32_t *)(pucBuffer + 0x06)) = (uint32_t)uiCore;
	*((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)uiIRLen;
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)uiInstrtoSelectADI;

	iPacketLen = TRANSMIT_LEN(16);
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, RISCV_SEND_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, RISCV_RECEIVE_SHORT_TIMEOUT) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	return (tyResult);
}