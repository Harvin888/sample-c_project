/******************************************************************************
       Module: drvopxd.cpp
     Engineer: Vitezslav Hola
  Description: Driver layer for Opella-XD interface
  Date           Initials    Description
  27-Sep-2006    VH          Initial
  20-Mar-2007    VH          Added support for PathFinder for MIPS
  23-Dec-2008    SPC         Added two more error return cases in MIPS
  09-MAr-2012    SPT         added cJATAG functions
******************************************************************************/
#include <stdio.h>   
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef __LINUX            // Win includes
#ifdef PFW				   // PathFinder headers
#include "/pathfndr/winmfc/stdafx.h"      
#include "/pathfndr/protocol/common/opella/drvlayer.h"
#include "/pathfndr/drivers/usb/opellaxd/usb.h"
#include "/pathfndr/global/headers/simple/global/simple.h"
#else                      // GDBServer and others
#include <windows.h>
#endif
#else                      // Linux includes
#include <unistd.h>
#endif

#include "libusb_dyn.h"
#include "../../utility/opxddiag/diagglob.h"
#include "../../firmware/export/api_cmd.h"
#include "../../firmware/export/api_def.h"
#include "../../firmware/export/api_err.h"
#include "drvopxd.h"
#include "drvopxdcom.h"
/****************************************************************************
*** Defines                                                               ***
****************************************************************************/

// Frequency restrictions
//#define MIN_FREQUENCY                           0.010          // 10kHz minimum frequency
//#define MAX_FREQUENCY                           166.000        // 166 MHz maximum frequency (depends on FPGA, PLL, etc.)

/****************************************************************************
*** Typedefs                                                              ***
****************************************************************************/

/****************************************************************************
*** Variables                                                             ***
****************************************************************************/
unsigned char bDriverInitialized = 0;
TyDrvDeviceStruct ptyDrvDeviceTable[MAX_DEVICES_SUPPORTED];

/****************************************************************************
*** Local function prototypes                                             ***
****************************************************************************/
static unsigned char DL_OPXD_ConvertDoubleToVoltage(double dValue);
static double DL_OPXD_ConvertVoltageToDouble(unsigned char ucValue);

/****************************************************************************
	 Function: ConvertVersionString
	 Engineer: Vitezslav Hola
		Input: uint32_t ulVersion - version
			   char *pszVersionString - buffer for version string
	   Output: none
  Description: convert version format to string
Date           Initials    Description
03-Oct-2006    VH          Initial
****************************************************************************/
static void ConvertVersionString(uint32_t ulVersion, char *pszVersionString)
{
	uint32_t uiVal1, uiVal2, uiVal3;

	assert(pszVersionString != NULL);
	// decode firmware version
	uiVal1 = (uint32_t)((ulVersion & 0xff000000) >> 24);
	uiVal2 = (uint32_t)((ulVersion & 0x00ff0000) >> 16);
	uiVal3 = (uint32_t)((ulVersion & 0x0000ff00) >> 8);
	if (ulVersion & 0x000000ff)
		sprintf(pszVersionString, "v%x.%x.%x-%c", uiVal1, uiVal2, uiVal3, (char)(ulVersion & 0x000000ff));
	else
		sprintf(pszVersionString, "v%x.%x.%x", uiVal1, uiVal2, uiVal3);
}
// Function implementation
/****************************************************************************
*** Device Management                                                     ***
****************************************************************************/

/****************************************************************************
	 Function: DL_OPXD_TerminateDW
	 Engineer: Rejeesh Shaji Babu
		Input: TyDevHandle tyDevHandle - device handle from DL_OpenDevice
			   TyDevType tyDevType - diskware type (ARC, RISC-V etc)
	   Output: error code DRVOPXD_ERROR_xxx
  Description: terminate running diskware
Date           Initials    Description
20-Jun-2019    RSB          Initial
****************************************************************************/
TyError DL_OPXD_TerminateDW(TyDevHandle tyDevHandle, TyDevType tyDevType)
{
	TyError tyResult;
	assert(tyDevHandle < MAX_DEVICES_SUPPORTED);
	tyResult = DRVOPXD_ERROR_NO_ERROR;
	tyDevHandle = tyDevHandle;

	DL_OPXD_LockDriver();

	unsigned char *pucBuffer;
	int32_t iPacketLen;
	usb_dev_handle *ptyUsbDevHandle;

	// terminate both diskware and fpgaware
	(void)DL_OPXD_LockDevice(tyDevHandle);
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	//added in case if system is not initialized (tyHandle == 0)
	if (pucBuffer == NULL)
		return tyResult;

	*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TERMINATE_DISKWARE;
	*((uint32_t *)(pucBuffer + 0x04)) = tyDevType;
	iPacketLen = TRANSMIT_LEN(8);

	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;

	if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;

	tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));

	DL_OPXD_UnlockDevice(tyDevHandle);
	ptyDrvDeviceTable[tyDevHandle].tyDiskwareType = DW_UNKNOWN;
	ptyDrvDeviceTable[tyDevHandle].iConfigInstances = 0;

	DL_OPXD_UnlockDriver();
	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_OpenDevice
     Engineer: Vitezslav Hola
        Input: const char *pszSerialNumber - string with device serial number
               unsigned short usDevicePID - device PID (product ID)
               unsigned char bShareDevice - share device with other threads (multiple DL_OpenDevice can be called)
               TyDevHandle *ptyDevHandle - pointer to device handle to return back
       Output: error code DRVOPXD_ERROR_xxx
  Description: open device with given serial number if it is available (connected to USB and not already used by other process)
               If successful, returns DRVOPXD_ERROR_NO_ERROR and valid handle in *ptyDevHandle.
               ! Do NOT use any DL_ function requiring handle without prior calling DL_OpenDevice !
Date           Initials    Description
03-Oct-2006    VH          Initial
19-Dec-2007    VH          Cleaned code for libusb initialization
****************************************************************************/
TyError DL_OPXD_OpenDevice(const char *pszSerialNumber, unsigned short usDevicePID, unsigned char bShareDevice, TyDevHandle *ptyDevHandle)
{
    int32_t iItem, iAvailableItem;
    usb_dev_handle *ptyUsbDevHandle;
    uint32_t *pulPtr;

    assert(pszSerialNumber != NULL);
    assert(ptyDevHandle != NULL);

    ptyUsbDevHandle = NULL;
    DL_OPXD_LockDriver();
    // set invalid handle, this handle causes assert in other function if used
    // this value is returned only when OpenDevice fails and therefore should not be ever used
    *ptyDevHandle = (TyDevHandle)MAX_DEVICES_SUPPORTED;
    if (!bDriverInitialized) {  // initialize libusb and local structures for the first time
        if (usb_init() != USB_SUCCESS)
            return DRVOPXD_ERROR_USB_DRIVER;
        memset((void *)ptyDrvDeviceTable, 0, sizeof(TyDrvDeviceStruct) * MAX_DEVICES_SUPPORTED);
        bDriverInitialized = 1;
    }
    // cannot use empty serial number string or too long serial number
    if (!strcmp(pszSerialNumber, "") ||
        strlen(pszSerialNumber) >= MAX_SERIAL_NUMBER_LEN)
        return(DRVOPXD_ERROR_INVALID_SERIAL_NUMBER);
    // first check if driver has already been opened
    for (iItem = 0; iItem < MAX_DEVICES_SUPPORTED; iItem++) {
        // check serial numbers
        if (!strcmp(ptyDrvDeviceTable[iItem].pszSerialNumber, pszSerialNumber)) {
            if (ptyDrvDeviceTable[iItem].bDeviceShared &&
                bShareDevice) {
                ptyDrvDeviceTable[iItem].iDeviceInstances++;    // increase number of instances
                *ptyDevHandle = (TyDevHandle)iItem;             // we found valid device, just return current handle
                DL_OPXD_UnlockDriver();                              // unlock driver
                return(DRVOPXD_ERROR_NO_ERROR);
            } else {
                DL_OPXD_UnlockDriver();
                return(DRVOPXD_ERROR_DEVICE_ALREADY_IN_USE);     // device is already in use
            }
        }
    }
    // let find available slot in table
    iAvailableItem = MAX_DEVICES_SUPPORTED;
    for (iItem = 0; iItem < MAX_DEVICES_SUPPORTED; iItem++) {
        if (!ptyDrvDeviceTable[iItem].bOpenedDevice) {
            iAvailableItem = iItem;
            break;
        }
    }
    if (iAvailableItem == MAX_DEVICES_SUPPORTED) {
        DL_OPXD_UnlockDriver();
        return(DRVOPXD_ERROR_TOO_MANY_DEVICES_OPENED);     // max number of devices open
    } else {     // ok, we got free slot, let try to find USB device and open it
        TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

        ptyDrvDeviceTable[iAvailableItem].ptyUsbDevHandle = NULL;

        // allocate memory for transfer buffer
        pulPtr = (uint32_t *)malloc(BIG_BUFFER_LEN);                 // > 64 KB
        if (pulPtr == NULL) {
            DL_OPXD_UnlockDriver();
            return DRVOPXD_ERROR_MEMORY_ALLOC;
        }

        ptyUsbDevHandle = usb_open(ASHLING_USB_VID, usDevicePID, pszSerialNumber);
        if (ptyUsbDevHandle != NULL) {
            ptyDrvDeviceTable[iAvailableItem].ptyUsbDevHandle = ptyUsbDevHandle;
            TyDevHandle tyDevHandle = (TyDevHandle)iAvailableItem;
            ptyDrvDeviceTable[iAvailableItem].bDeviceShared = bShareDevice;
            ptyDrvDeviceTable[iAvailableItem].bOpenedDevice = 1;
            ptyDrvDeviceTable[iAvailableItem].iDeviceInstances = 1;
            strcpy(ptyDrvDeviceTable[iAvailableItem].pszSerialNumber, pszSerialNumber);      // we checked strlen earlier
            ptyDrvDeviceTable[iAvailableItem].iConfigInstances = 0;
            ptyDrvDeviceTable[iAvailableItem].tyDiskwareType = DW_UNKNOWN;
            ptyDrvDeviceTable[iAvailableItem].tyFpgawareType = FPGA_UNKNOWN;
            ptyDrvDeviceTable[iAvailableItem].iDataTimeout = DRV_TIMEOUT;                    // set default timeout
            ptyDrvDeviceTable[iAvailableItem].pulTransferBuffer = pulPtr;
            *ptyDevHandle = (TyDevHandle)iAvailableItem;

            // now we need to flush any data from USB from particular device (from all pipes)
            (void)DL_OPXD_LockDevice(tyDevHandle);
            (void)usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pulPtr, BIG_BUFFER_LEN, 10);     // flush anything what is in buffer
            DL_OPXD_UnlockDevice(tyDevHandle);
        } else {
            free((void *)pulPtr);
            tyResult = DRVOPXD_ERROR_INVALID_SERIAL_NUMBER;
        }

        DL_OPXD_UnlockDriver();
        return(tyResult);
    }
}


/****************************************************************************
     Function: DL_OPXD_CloseDevice
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - device handle from DL_OpenDevice
       Output: error code DRVOPXD_ERROR_xxx
  Description: close device with given handle
Date           Initials    Description
03-Oct-2006    VH          Initial
****************************************************************************/
TyError DL_OPXD_CloseDevice(TyDevHandle tyDevHandle)
{
    if (tyDevHandle >= MAX_DEVICES_SUPPORTED || tyDevHandle < 0)
        return DRVOPXD_ERROR_INVALID_HANDLE;

    DL_OPXD_LockDriver();
    if (ptyDrvDeviceTable[tyDevHandle].bOpenedDevice) {
        ptyDrvDeviceTable[tyDevHandle].iDeviceInstances--;
        if (ptyDrvDeviceTable[tyDevHandle].iDeviceInstances == 0) {
            // close device entirely
            ptyDrvDeviceTable[tyDevHandle].bOpenedDevice = 0;
            ptyDrvDeviceTable[tyDevHandle].bDeviceShared = 0;
            ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
            strcpy(ptyDrvDeviceTable[tyDevHandle].pszSerialNumber, "");
            // call usb driver to close handle
            if (ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle != NULL) {
#ifdef __LINUX
                (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPA);
                (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPB);
                (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPC);
                (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPD);
#endif
                (void)usb_close(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle);
            }
            ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle = NULL;
            // free allocated transfer buffer
            if (ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer != NULL)
                free((void *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);

            ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer = NULL;
        }
    } else {
        DL_OPXD_UnlockDriver();
        return(DRVOPXD_ERROR_INVALID_HANDLE);
    }

    DL_OPXD_UnlockDriver();
    return(DRVOPXD_ERROR_NO_ERROR);
}

/****************************************************************************
     Function: DL_OPXD_UpgradeDevice
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - device handle from DL_OpenDevice
               TyDeviceUpgradeType tyUpgradeType - upgrade type (UPG_xxx)
               const char *pszFirmwareFilename - filename with firmware intended for upgrade
               unsigned char *pbReconnectNeeded - pointer to variable signalling if reopen of the device is needed
               int32_t *piFirmwareDiff - pointer to variable with firmware comparison
                                       0 - firmware version is up-to-date
                                       >0 - device has newer version than file
                                       <0 - device has older version than file
               uint32_t *pulFileFwVersion - pointer to variable for file FW version (can be NULL)
               uint32_t *pulDeviceFwVersion - pointer to variable for FW version in device before upgrade(can be NULL)
       Output: error code DRVOPXD_ERROR_xxx
  Description: upgrade firmware in device
               if UPG_NEWER_VERSION or UPG_DIFF_VERSION is used, upgrade will proceed
  NOTE! This function require following consideration.
        Device needs to be open using DL_OpenDevice, not sharing is essential. It is also recommended verify firmware upgrade once
        when starting program (PathFinder/GDBServer) and from single thread. Never call this function from multiple threads.
        If upgrade is needed, this function writes new firmware into Opella-XD Flash and reset device (disconnects from USB). This means
        this function release usb interface for this device (invalid at that time).
        If *pbReconnectNeeded != 0, never use device handle again, it becomes invalid when device is restarted.
        In program that call this function, you should wait sufficient time (about 8s) for device to be restarted and reconnected.
        Then call DL_OpenDevice again. In next step, you should verify firmware update has happened (call this function with UPG_CHECK_xxx)
        and check *piFirmwareDiff is 0. This means firmware upgrade has been successful.
Date           Initials    Description
24-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_UpgradeDevice(TyDevHandle tyDevHandle, TyDeviceUpgradeType tyUpgradeType, const char *pszFirmwareFilename, const char *pszFlashUtilFilename,
                              unsigned char *pbReconnectNeeded, int32_t *piFirmwareDiff, uint32_t *pulFileFwVersion, uint32_t *pulDeviceFwVersion, const char *pszSerialNumber, PfPrintFunc pfPrintFunc)
{
   TyDeviceInfo tyDeviceInfo;

   assert(tyDevHandle < MAX_DEVICES_SUPPORTED);
   assert(pszFirmwareFilename != NULL);
   assert(pbReconnectNeeded != NULL);
   assert(piFirmwareDiff != NULL);

   if (DL_OPXD_GetDeviceInfo(tyDevHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
      return DRVOPXD_ERROR_USB_DRIVER;

   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
   {
      return DL_OPXD_UpgradeDevice_R2(tyDevHandle, 
                                      tyUpgradeType, 
                                      pszFirmwareFilename, 
                                      pbReconnectNeeded,
                                      piFirmwareDiff,
                                      pulFileFwVersion,
                                      pulDeviceFwVersion);
   }
   else
   {
      return DL_OPXD_UpgradeDevice_R3(tyDevHandle, 
                                      tyUpgradeType, 
                                      pszFirmwareFilename,
									  pszFlashUtilFilename,
                                      pbReconnectNeeded,
                                      piFirmwareDiff,
                                      pulFileFwVersion,
                                      pulDeviceFwVersion,
									  pszSerialNumber,
									  pfPrintFunc);
   }
}

/****************************************************************************
     Function: DL_OPXD_UpgradeDevice_R2
     Engineer: Vitezslav Hola
        Input: same as above
       Output: error code DRVOPXD_ERROR_xxx
  Description: upgrade firmware in device of Opella-XD R2

Date           Initials    Description
24-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_UpgradeDevice_R2(TyDevHandle         tyDevHandle,
                                 TyDeviceUpgradeType tyUpgradeType,
                                 const char         *pszFirmwareFilename, 
                                 unsigned char      *pbReconnectNeeded,
                                 int32_t                *piFirmwareDiff,
                                 uint32_t      *pulFileFwVersion,
                                 uint32_t      *pulDeviceFwVersion)
{
   int32_t iPacketLen, iBytesRead, iOffset;
   unsigned char *pucBuffer;
   uint32_t pulFileHeader[8];
   uint32_t ulDeviceVersion, ulFileVersion, ulChecksum;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   usb_dev_handle *ptyUsbDevHandle;

   FILE *pFile;

   assert(tyDevHandle < MAX_DEVICES_SUPPORTED);
   assert(pszFirmwareFilename != NULL);
   assert(pbReconnectNeeded != NULL);
   assert(piFirmwareDiff != NULL);

   *pbReconnectNeeded = 0;
   *piFirmwareDiff = 0;

   // get firmware version from file (just read header)
   pFile = fopen(pszFirmwareFilename, "rb");
   if (pFile == NULL)
      return DRVOPXD_ERROR_INVALID_FILENAME;

   if (fread((unsigned char *)pulFileHeader, sizeof(uint32_t), 8, pFile) != 8)
      {
      fclose(pFile);
      return DRVOPXD_ERROR_INVALID_FIRMWARE;
      }
   fclose(pFile);             // close file for now

   // analyze header (check id code, size, update flag)
   if ((pulFileHeader[0] != 0xC0DE1234) || (pulFileHeader[1] != 0xFFFFFFFF) || (pulFileHeader[3] > 0x41000))
       return DRVOPXD_ERROR_INVALID_FIRMWARE;
   ulFileVersion = pulFileHeader[5];
   if (pulFileFwVersion != NULL)
      *pulFileFwVersion = ulFileVersion;

   // get firmware version from device
   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)(ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_GET_FIRMWARE_INFO;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   ulDeviceVersion = *((uint32_t *)(pucBuffer + 0x04));
   if (pulDeviceFwVersion != NULL)
      *pulDeviceFwVersion = ulDeviceVersion;

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return(tyResult);
      }

   // compare version numbers
   if (ulDeviceVersion > ulFileVersion)
      *piFirmwareDiff = 1;                      // device has newer firmware version than in file
   else if (ulDeviceVersion < ulFileVersion)
      *piFirmwareDiff = -1;                     // device has older firmware version than in file
   else
      *piFirmwareDiff = 0;                      // device has same firmware version than in file

   
   // return if upgrade is not needed or just checking version differences
   if ((tyUpgradeType == UPG_CHECK_ONLY) ||
       ((tyUpgradeType == UPG_NEWER_VERSION) && (*piFirmwareDiff >= 0)) ||
       ((tyUpgradeType == UPG_DIFF_VERSION) && (*piFirmwareDiff == 0))
       )
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return(tyResult);
      }

   // otherwise, go ahead for device upgrade
   // reopen file with firmware, keep device locked
   pFile = fopen(pszFirmwareFilename, "rb");
   if (pFile == NULL)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return DRVOPXD_ERROR_INVALID_FILENAME;
      }

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_START_UPGRADE_FIRMWARE;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      fclose(pFile);
      DL_OPXD_UnlockDevice(tyDevHandle);
      return tyResult;
      }

   // first calculate valid checksum and replace it in file header
   // sum of all words in firmware (with valid upgrade flag and valid checksum) must be 0
   ulChecksum = 0;
   (void)fseek(pFile, 0, SEEK_SET);
   do
      {
      int32_t iCnt;
      iBytesRead = (int32_t)fread((char *)(pucBuffer), 1, BUFFER_PAYLOAD, pFile);
      for (iCnt=0; iCnt < iBytesRead; iCnt+=4)
         {
         ulChecksum += *((uint32_t *)(pucBuffer + iCnt));
         }
      }
   while(iBytesRead && (tyResult == DRVOPXD_ERROR_NO_ERROR));
   ulChecksum += (1 + 0x55555555);           // replace blank upgrade flag with valid one
   ulChecksum = (0xFFFFFFFF - ulChecksum) + 1;     // calculate complement

   (void)fseek(pFile, 0, SEEK_SET);          // start again
   iOffset = 0;
   do
      {
      iBytesRead = (int32_t)fread((char *)(pucBuffer + 12), 1, BUFFER_PAYLOAD, pFile);
      if (iBytesRead > 0)
         {
         // set header
         *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_UPGRADE_FIRMWARE;
         *((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)iOffset;
         *((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)iBytesRead;
         if (iOffset == 0)
            *((uint32_t *)(pucBuffer + 20)) = ulChecksum;             // replace valid checksum in header

         iOffset += iBytesRead;
         iPacketLen = TRANSMIT_LEN(12+iBytesRead);

         if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
         // receive packet
         if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
             (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
         }
      }
   while(iBytesRead && (tyResult == DRVOPXD_ERROR_NO_ERROR));

   fclose(pFile);

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
   {
	   DL_OPXD_UnlockDevice(tyDevHandle);
	   return tyResult;
   }

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_FINISH_UPGRADE_FIRMWARE;
   iPacketLen = TRANSMIT_LEN(4);

   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return tyResult;
      }

   // now restart device (will require reconnection)
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_RESET_DEVICE;
   iPacketLen = TRANSMIT_LEN(4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // no response expected
   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return tyResult;
      }

   // reconnecting will be needed
   *pbReconnectNeeded = 1;
   DL_OPXD_UnlockDevice(tyDevHandle);

   DL_OPXD_LockDriver();
   // close device entirely
   ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
   ptyDrvDeviceTable[tyDevHandle].bOpenedDevice = 0;
   ptyDrvDeviceTable[tyDevHandle].bDeviceShared = 0;
   ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
   strcpy(ptyDrvDeviceTable[tyDevHandle].pszSerialNumber, "");
   // call usb driver to close handle
   if (ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle != NULL)
      {
#ifdef __LINUX
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPA);
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPB);
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPC);
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPD);
#endif
      (void)usb_close(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle);
      }
   ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle = NULL;
   // free allocated transfer buffer
   if (ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer != NULL)
      free((void *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);
   ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer = NULL;

   DL_OPXD_UnlockDriver();

   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_UpgradeDevice_R3
     Engineer: Andre Schmiel
        Input: same as above
       Output: error code DRVOPXD_ERROR_xxx
  Description: upgrade firmware in device of Opella-XD R3

Date           Initials    Description
24-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_UpgradeDevice_R3(TyDevHandle         tyDevHandle,
                                 TyDeviceUpgradeType tyUpgradeType,
                                 const char         *pszFirmwareFilename, 
								 const char         *pszFlashUtilFilename,
                                 unsigned char      *pbReconnectNeeded,
                                 int32_t            *piFirmwareDiff,
                                 uint32_t      *pulFileFwVersion,
                                 uint32_t      *pulDeviceFwVersion,
								 const char         *pszSerialNumber,
								 PfPrintFunc		 pfPrintFunc)
{
   int32_t            iPacketLen, iBytesRead, iOffset, iCnt;
   unsigned char *pucBuffer;
   //uint32_t  pulFileHeader[0x60000/sizeof(uint32_t)];
   uint32_t* pulFileHeader;
   pulFileHeader = (uint32_t*)malloc((0x60000 / sizeof(uint32_t)) * sizeof(uint32_t));
   if (pulFileHeader == NULL)
	   return DRVOPXD_ERROR_MEMORY_ALLOC;
   uint32_t  ulDeviceVersion, ulFileVersion, ulChecksum, ulFwSize;
   TyError        tyResult = DRVOPXD_ERROR_NO_ERROR;
   char          pszDevVersion[64];
   char          pszFileVersion[64];

   usb_dev_handle *ptyUsbDevHandle;

   FILE *pFile;

   *pbReconnectNeeded = 0;
   *piFirmwareDiff = 0;

   // get firmware version from file (just read header)
   pFile = fopen(pszFirmwareFilename, "rb");
   if (pFile == NULL)
   {
	   free(pulFileHeader);
	   return DRVOPXD_ERROR_INVALID_FILENAME;
   }

   if (fread((unsigned char *)pulFileHeader, sizeof(uint32_t), (0x60000/sizeof(uint32_t)), pFile) != (0x60000/sizeof(uint32_t)))
   {
      fclose(pFile);
	  free(pulFileHeader);
      return DRVOPXD_ERROR_INVALID_FILENAME;
   }

   // analyze header (check id code, size, update flag)
   if ((pulFileHeader[0x5f800 / sizeof(uint32_t)] != 0xC0DE1234)
	   || (pulFileHeader[0x5f804 / sizeof(uint32_t)] != 0xFFFFFFFF)
	   || ((pulFileHeader[0x5f80c / sizeof(uint32_t)] != 0x60000) && (pulFileHeader[0x5f80c / sizeof(uint32_t)] != 0x80000)))// 0x60000 check is for backword compatibility
   {
	   free(pulFileHeader);
	   return DRVOPXD_ERROR_INVALID_FILENAME;
   }

   ulFileVersion = pulFileHeader[0x5f814/sizeof(uint32_t)];
   free(pulFileHeader);
   if (pulFileFwVersion != NULL)
   {
	   *pulFileFwVersion = ulFileVersion;
   }

   // get firmware version from device
   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)(ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_GET_FIRMWARE_INFO;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   ulDeviceVersion = *((uint32_t *)(pucBuffer + 0x04));
   if (pulDeviceFwVersion != NULL)
      *pulDeviceFwVersion = ulDeviceVersion;

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return(tyResult);
      }

   // compare version numbers
   if (ulDeviceVersion > ulFileVersion)
      *piFirmwareDiff = 1;                      // device has newer firmware version than in file
   else if (ulDeviceVersion < ulFileVersion)
      *piFirmwareDiff = -1;                     // device has older firmware version than in file
   else
      *piFirmwareDiff = 0;                      // device has same firmware version than in file
   
   // return if upgrade is not needed or just checking version differences
   if ((tyUpgradeType == UPG_CHECK_ONLY) ||
       ((tyUpgradeType == UPG_NEWER_VERSION) && (*piFirmwareDiff >= 0)) ||
       ((tyUpgradeType == UPG_DIFF_VERSION) && (*piFirmwareDiff == 0))
       )
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return(tyResult);
      }

   // otherwise, go ahead for device upgrade
   if (pfPrintFunc != NULL) {
	   ConvertVersionString(ulDeviceVersion, pszDevVersion);
	   ConvertVersionString(ulFileVersion, pszFileVersion);
	   fprintf(stdout, "Updating Opella-XD firmware from %s to %s.\n", pszDevVersion, pszFileVersion);
	   fprintf(stdout, "\tDownloading firmware update utility to Opella-XD.\n");
	   fflush(stdout);
   }
   //download flash utility to ext RAM
   pFile = fopen(pszFlashUtilFilename, "rb");
   if (pFile == NULL)
   {
	   DL_OPXD_UnlockDevice(tyDevHandle);
	   return DRVOPXD_ERROR_INVALID_FILENAME;
   }
   if (pFile != NULL)
   {
	   int32_t iBytesRead, iDataLen;
	   uint32_t ulLocation;

	   iDataLen = BIG_BUFFER_LEN - SMALL_BUFFER_LEN;
	   ulLocation = 0x0;          // start with offset 0
	   (void)DL_OPXD_LockDevice(tyDevHandle);
	   do
	   {
		   // read data to buffer
		   iBytesRead = (int32_t)fread((unsigned char *)(&pucBuffer[0x0c]), 1, (uint32_t)iDataLen, pFile);
		   if (iBytesRead > 0)
		   {  // send data to device
			   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_LOAD_MEMORY;
			   *((uint32_t *)(pucBuffer + 0x04)) = 0xE0000000 | (ulLocation & 0x0FFFFFFF);            // loading to ext RAM
			   *((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)iBytesRead;
			   iPacketLen = TRANSMIT_LEN(0x0c + iBytesRead);
			   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
				   tyResult = DRVOPXD_ERROR_USB_DRIVER;
			   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) ||
				   (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
				   tyResult = DRVOPXD_ERROR_USB_DRIVER;
			   ulLocation += (uint32_t)iBytesRead;
		   }
	   } while ((iBytesRead > 0) && (tyResult != DRVOPXD_ERROR_USB_DRIVER));
   }
   DL_OPXD_UnlockDevice(tyDevHandle);

   if (pFile != NULL)
	   fclose(pFile);
   // Now pass the control to flash utility
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
   {
	   (void)DL_OPXD_LockDevice(tyDevHandle);
	   // execute flash utility
	   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_EXECUTE_DISKWARE;
	   *((uint32_t *)(pucBuffer + 0x04)) = FLASH_UTIL;
	   iPacketLen = TRANSMIT_LEN(5);
	   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;

	   //reset host side as well
	   DL_OPXD_UnlockDevice(tyDevHandle);

	   DL_OPXD_LockDriver();
	   // close device entirely
	   ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
	   ptyDrvDeviceTable[tyDevHandle].bOpenedDevice = 0;
	   ptyDrvDeviceTable[tyDevHandle].bDeviceShared = 0;
	   ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
	   strcpy(ptyDrvDeviceTable[tyDevHandle].pszSerialNumber, "");
	   // call usb driver to close handle
	   if (ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle != NULL)
	   {
#ifdef __LINUX
		   (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPA);
		   (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPB);
		   (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPC);
		   (void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPD);
#endif
		   (void)usb_close(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle);
	   }
	   ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle = NULL;
	   // free allocated transfer buffer
	   if (ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer != NULL)
		   free((void *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);
	   ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer = NULL;

	   DL_OPXD_UnlockDriver();
   }
   else
   {
	   return(tyResult);
   }
	unsigned int uiCnt;
	// wait for 3 seconds to reconnect probe
	for (uiCnt = 0; uiCnt < 3; uiCnt++)
#ifndef __LINUX
            Sleep((DWORD)1000);
#else
            usleep(1000);
#endif		
	bDriverInitialized = 0;
	//Open new connection to Opella-XD
	tyResult = DL_OPXD_OpenDevice(pszSerialNumber, OPELLAXD_USB_PID, false, &tyDevHandle);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
		return tyResult;

	(void)DL_OPXD_LockDevice(tyDevHandle);
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)(ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);

   // reopen firmware file with firmware, keep device locked
   pFile = fopen(pszFirmwareFilename, "rb");
   if (pFile == NULL)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return DRVOPXD_ERROR_INVALID_FILENAME;
      }

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_START_UPGRADE_FIRMWARE;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      fclose(pFile);
      DL_OPXD_UnlockDevice(tyDevHandle);
      return tyResult;
      }

   // first calculate valid checksum and replace it in file header
   // sum of all words in firmware (with valid upgrade flag and valid checksum) must be 0
   ulChecksum = 0;
   ulFwSize   = 0;
   (void)fseek(pFile, 0, SEEK_SET);
   do
   {
      iBytesRead = (int32_t)fread((char *)(pucBuffer), 1, BUFFER_PAYLOAD, pFile);

      for (iCnt=0; iCnt < iBytesRead; iCnt+=4)
         ulChecksum += *((uint32_t *)(pucBuffer + iCnt));

      //the maufacturing section and flash programming code is located > 0x60000!!!
      //don't upgrade this area
      ulFwSize += (uint32_t)iBytesRead;

      if (ulFwSize == 0x60000)
         iBytesRead = 0;
   }
   while(iBytesRead && (tyResult == DRVOPXD_ERROR_NO_ERROR));

   ulChecksum += (1 + 0x55555555);           // replace blank upgrade flag with valid one
   ulChecksum = (0xFFFFFFFF - ulChecksum) + 1;     // calculate complement

   (void)fseek(pFile, 0, SEEK_SET);          // start again
   ulFwSize = 0;
   iOffset  = 0;
   if (pfPrintFunc != NULL) {
	   pfPrintFunc("\tUpdating Opella-XD firmware");	
   }
   do
   {
      iBytesRead = (int32_t)fread((char *)(pucBuffer + 12), 1, BUFFER_PAYLOAD, pFile);
      if (iBytesRead > 0)
      {
         // set header
         *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_UPGRADE_FIRMWARE;
         *((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)iOffset;
         *((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)iBytesRead;

         iOffset += iBytesRead;
         iPacketLen = TRANSMIT_LEN(12+iBytesRead);

         if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
         // receive packet
         if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
             (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
		 fprintf(stdout, ".");
		 fflush(stdout);
      }

      ulFwSize += (uint32_t)iBytesRead;
	  //skip maufacturing sector
	  if (ulFwSize == 0x60000)
	  {		  
		  (void)fseek(pFile, 0x70000, SEEK_SET); // set to sector 14
		  iOffset += 0x10000; // sector 13 size,64K
		  ulFwSize += 0x10000; // sector 13 size,64K
	  }
	  if (ulFwSize == 0x80000)
		  iBytesRead = 0;
   }
   while(iBytesRead && (tyResult == DRVOPXD_ERROR_NO_ERROR));

   fclose(pFile);
   fprintf(stdout, "\n");
   fflush(stdout);

   if (tyResult == DRVOPXD_ERROR_USB_DRIVER)
      return DRVOPXD_ERROR_USB_DRIVER;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_FINISH_UPGRADE_FIRMWARE;
   *((uint32_t *)(pucBuffer + 0x04)) = ulFwSize;
   *((uint32_t *)(pucBuffer + 0x08)) = ulChecksum;

   iPacketLen = TRANSMIT_LEN(0x0c);

   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   if (tyResult != DRVOPXD_ERROR_NO_ERROR)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      return tyResult;
      }

   // reconnecting will be needed
   *pbReconnectNeeded = 1;

   DL_OPXD_UnlockDevice(tyDevHandle);

   DL_OPXD_LockDriver();
   // close device entirely
   ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
   ptyDrvDeviceTable[tyDevHandle].bOpenedDevice = 0;
   ptyDrvDeviceTable[tyDevHandle].bDeviceShared = 0;
   ptyDrvDeviceTable[tyDevHandle].iDeviceInstances = 0;
   strcpy(ptyDrvDeviceTable[tyDevHandle].pszSerialNumber, "");
   // call usb driver to close handle
   if (ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle != NULL)
      {
#ifdef __LINUX
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPA);
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPB);
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPC);
		(void)usb_resetep(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle, EPD);
#endif
      (void)usb_close(ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle);
      }
   ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle = NULL;
   // free allocated transfer buffer
   if (ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer != NULL)
      free((void *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);
   ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer = NULL;

   DL_OPXD_UnlockDriver();

   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_GetConnectedDevices
     Engineer: Vitezslav Hola
        Input: char ppszDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN] - buffer for serial numbers
               unsigned short *pusNumberOfDevices - pointer to variable for number of connected devices
               unsigned short usPID - Product ID of device
       Output: none
  Description: obtain list of connected Ashling devices with specified PID
               If more than MAX_DEVICES_SUPPORTED devices connected, only MAX_DEVICES_SUPPORTED will be reported
Date           Initials    Description
27-Sep-2006    VH          Initial
19-Dec-2007    VH          Cleaned libusb initialization
****************************************************************************/
void DL_OPXD_GetConnectedDevices(char ppszDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN], unsigned short *pusNumberOfDevices, unsigned short usPID)
{
   int32_t iCnt;
   assert(ppszDeviceNumbers != NULL);
   assert(pusNumberOfDevices != NULL);

   *pusNumberOfDevices = 0;
   if (!bDriverInitialized)
      {  // initialize libusb and local structures
       if (usb_init() != USB_SUCCESS) {
           return;
       }
      memset((void *)ptyDrvDeviceTable, 0, sizeof(TyDrvDeviceStruct)*MAX_DEVICES_SUPPORTED);
      bDriverInitialized = 1;
      }

   // clear list
   for (iCnt=0; iCnt < MAX_DEVICES_SUPPORTED; iCnt++)
      strcpy(ppszDeviceNumbers[iCnt], "");

   *pusNumberOfDevices = usb_get_dev_list(ASHLING_USB_VID, usPID, ppszDeviceNumbers);
}

/****************************************************************************
	 Function: DL_OPXD_ExecuteDW
	 Engineer: Rejeesh Shaji Babu
		Input: TyDevHandle tyDevHandle - handle to open device
			   TyDiskwareType tyDwType - type of diskware - ARC, RISCV etc...
	   Output: error code DRVOPXD_ERROR_xxx
  Description: This function call lets diskware know this instance is using diskware
Date           Initials    Description
20-Jun-2019    RSB         Initial
****************************************************************************/
TyError DL_OPXD_ExecuteDW(TyDevHandle tyDevHandle, TyDevType tyDevType)
{
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	usb_dev_handle *ptyUsbDevHandle;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

	assert(tyDevHandle < MAX_DEVICES_SUPPORTED);

	(void)DL_OPXD_LockDevice(tyDevHandle);
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)(ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);

	*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_EXECUTE_DISKWARE;
	*((uint32_t *)(pucBuffer + 0x04)) = tyDevType;

	iPacketLen = TRANSMIT_LEN(8);

	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;

	// receive packet
	if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));

	DL_OPXD_UnlockDevice(tyDevHandle);

	return tyResult;
}

/****************************************************************************
	 Function: DL_OPXD_GetDiskwareStatus
	 Engineer: Rejeesh Shaji Babu
		Input: TyDevHandle tyDevHandle - handle to open device
			   TyDeviceInfo *ptyDeviceInfo - pointer to device info structure
			   TyDiskwareType tyDwType - type of current diskware - ARC, RISCV etc...
	   Output: error code DRVOPXD_ERROR_xxx
  Description: get status of diskware - If any diskware is running in target or not
Date           Initials    Description
18-Jun-2019    RSB         Initial
****************************************************************************/
TyError DL_OPXD_GetDiskwareStatus(TyDevHandle tyDevHandle, TyDeviceInfo *ptyDeviceInfo, TyDevType tyDevType)
{
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	usb_dev_handle *ptyUsbDevHandle;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

	assert(tyDevHandle < MAX_DEVICES_SUPPORTED);
	assert(ptyDeviceInfo != NULL);

	(void)DL_OPXD_LockDevice(tyDevHandle);
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)(ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);

	*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_DISKWARE_STATUS;
	*((uint32_t *)(pucBuffer + 0x04)) = tyDevType;

	iPacketLen = TRANSMIT_LEN(8);

	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;

	// receive packet
	if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));

	ptyDeviceInfo->uiActiveDiskwares = *((uint32_t *)(pucBuffer + 0x04));

	if (pucBuffer[0x08] == 0x01)
		ptyDeviceInfo->bOtherDiskwareRunning = 1;
	else
		ptyDeviceInfo->bOtherDiskwareRunning = 0;

	DL_OPXD_UnlockDevice(tyDevHandle);

	return tyResult;
}
/****************************************************************************
     Function: DL_OPXD_GetDeviceInfo
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyDeviceInfo *ptyDeviceInfo - pointer to device info structure
			   TyDiskwareType tyDwType - type of diskware - ARC, RISCV etc...
       Output: error code DRVOPXD_ERROR_xxx
  Description: get information about firmware, diskware and fpgaware in device
Date           Initials    Description
03-Oct-2006    VH          Initial
23-Nov-2007    VH          Added TPA serial number support and FPGA information
12-Jun-2019    RSB         Added TyDiskwareType (diskware details are not fetched if TyDiskwareType is UNKNOWN)
****************************************************************************/
TyError DL_OPXD_GetDeviceInfo(TyDevHandle tyDevHandle, TyDeviceInfo *ptyDeviceInfo, TyDiskwareType tyDwType)
{
   unsigned char *pucBuffer;
   int32_t iPacketLen, iRetSize;
   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult =DRVOPXD_ERROR_NO_ERROR;

   assert(tyDevHandle < MAX_DEVICES_SUPPORTED);
   assert(ptyDeviceInfo != NULL);
   memset((void *)(ptyDeviceInfo), 0, sizeof(TyDeviceInfo));

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)(ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer);
   // get Opella-XD serial number from table in the driver
   strcpy(ptyDeviceInfo->pszSerialNumber, ptyDrvDeviceTable[tyDevHandle].pszSerialNumber);
   // get device info
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_GET_INFO;
   iPacketLen = TRANSMIT_LEN(4);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   iRetSize = usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT);

   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((unsigned long *)(pucBuffer + 0x00)));
   if (tyResult != ERR_NO_ERROR)
	   return tyResult;

   // get Opella-XD info
   ptyDeviceInfo->ulBoardRevision = *((uint32_t *)(pucBuffer + 0x04));
   ptyDeviceInfo->ulManufacturingDate = *((uint32_t *)(pucBuffer + 0x08));

   // TPA information
   if (pucBuffer[0x18] == 0)
      ptyDeviceInfo->tyTpaConnected = TPA_NONE;
   else
      {
      switch (*((unsigned short *)(pucBuffer + 0x1A)))
         {
         case 0x0000:   // TPA with invalid signiture
            ptyDeviceInfo->tyTpaConnected = TPA_INVALID;
            break;
         case 0x0100:
            ptyDeviceInfo->tyTpaConnected = TPA_MIPS14;
            break;
         case 0x0200:
            ptyDeviceInfo->tyTpaConnected = TPA_ARM20;
            break;
         case 0x0300:
            ptyDeviceInfo->tyTpaConnected = TPA_ARC20;
            break;
         case 0xFF00:
            ptyDeviceInfo->tyTpaConnected = TPA_DIAG;
            break;
         default:
            ptyDeviceInfo->tyTpaConnected = TPA_UNKNOWN;
            break;
         }
      if (ptyDeviceInfo->tyTpaConnected != TPA_INVALID)
         {
         memcpy((void *)ptyDeviceInfo->pszTpaName, (void *)&(pucBuffer[0x1C]), 16);
         ptyDeviceInfo->pszTpaName[15] = 0;
         }
      }
   ptyDeviceInfo->dCurrentVtref  = DL_OPXD_ConvertVoltageToDouble(pucBuffer[0x2C]);
   if (ptyDeviceInfo->dCurrentVtref < 0.3)
      ptyDeviceInfo->dCurrentVtref = 0.0;
   ptyDeviceInfo->dCurrentVtpa   = DL_OPXD_ConvertVoltageToDouble(pucBuffer[0x2D]);
   ptyDeviceInfo->dMinVtpa       = DL_OPXD_ConvertVoltageToDouble(pucBuffer[0x2E]);
   ptyDeviceInfo->dMaxVtpa       = DL_OPXD_ConvertVoltageToDouble(pucBuffer[0x2F]);
   if (pucBuffer[0x30])
      ptyDeviceInfo->bTrackingVtpaMode = 1;
   else
      ptyDeviceInfo->bTrackingVtpaMode = 0;
   if (pucBuffer[0x31])
      ptyDeviceInfo->bTpaDriversEnabled = 1;
   else
      ptyDeviceInfo->bTpaDriversEnabled = 0;
   if (pucBuffer[0x32])
      ptyDeviceInfo->bTargetConnected = 1;
   else
      ptyDeviceInfo->bTargetConnected = 0;
   ptyDeviceInfo->ulTpaRevision = *((uint32_t *)(pucBuffer + 0x34));
   // get TPA serial number (if sufficient number of bytes received) and TPA is valid
   if ((iRetSize >= 72) && (ptyDeviceInfo->tyTpaConnected != TPA_INVALID) && (ptyDeviceInfo->tyTpaConnected != TPA_NONE))
      {
      memcpy((void *)ptyDeviceInfo->pszTpaSerialNumber, (void *)&(pucBuffer[0x38]), 16);
      ptyDeviceInfo->pszTpaSerialNumber[15] = 0;
      }
   else
      ptyDeviceInfo->pszTpaSerialNumber[0] = 0;                               // empty string

   // get firmware info
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_GET_FIRMWARE_INFO;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   ptyDeviceInfo->ulFirmwareVersion = *((uint32_t *)(pucBuffer + 0x04));
   ptyDeviceInfo->ulFirmwareDate    = *((uint32_t *)(pucBuffer + 0x08));

   // This piece of code can be removed when CMD_CODE_GET_RISCV_DISKWARE_INFO, CMD_CODE_GET_ARC_DISKWARE_INFO cmds are implemented in R2 fw.
   if (ptyDeviceInfo->ulBoardRevision < 0x03000000) //R2 Opella-XD
   {
	   *((uint32_t*)(pucBuffer + 0x00)) = CMD_CODE_GET_ARC_DISKWARE_INFO; //equivalent to old CMD_CODE_GET_DISKWARE_INFO
   }
   else
   {
	   // get diskware and FPGAware info
	   switch (tyDwType)
	   {
	   case DW_UNKNOWN:
		   // unknown means the call is done before firmware update, so no need to get diskware/FPGAware info
		   tyResult = DRVOPXD_ERROR_NO_ERROR;
		   DL_OPXD_UnlockDevice(tyDevHandle);
		   return(tyResult);
		   break;
	   case DW_RISCV:
		   *((uint32_t*)(pucBuffer + 0x00)) = CMD_CODE_GET_RISCV_DISKWARE_INFO;
		   break;
	   case DW_ARC:
		   //TODO: make it ARC specific
		   *((uint32_t*)(pucBuffer + 0x00)) = CMD_CODE_GET_ARC_DISKWARE_INFO;
	   default:
		   break;
	   }
   }

   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   iRetSize = usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT);
   if ((iRetSize <= 0) || (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   ptyDeviceInfo->ulDiskwareVersion = *((uint32_t *)(pucBuffer + 0x04));
   ptyDeviceInfo->ulDiskwareDate    = *((uint32_t *)(pucBuffer + 0x08));
   ptyDeviceInfo->ulDiskwareType    = *((uint32_t *)(pucBuffer + 0x10));
   if (iRetSize >= 36)
      {
      ptyDeviceInfo->ulFpgawareVersion = *((uint32_t *)(pucBuffer + 0x18));
      ptyDeviceInfo->ulFpgawareDate    = *((uint32_t *)(pucBuffer + 0x1C));
      ptyDeviceInfo->ulFpgawareType    = *((uint32_t *)(pucBuffer + 0x20));
      ptyDeviceInfo->bFpgawareConfigured = (unsigned char)((ptyDeviceInfo->ulFpgawareType != 0) ? 1 : 0);
      }
  
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ConfigureDevice
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - device handle from DL_OpenDevice
               TyDiskwareType tyDwType - type of diskware
               TyFpgawareType tyFpgaType - type of fpgaware
               const char *pszDiskwareFilename - diskware filename, if name is "", no diskware is loaded and executed
               const char *pszFpgawareFilename - fpgaware filename, if name is "", FPGA is not configured
       Output: error code DRVOPXD_ERROR_xxx
  Description: configure device with given diskware and fpgaware
Date           Initials    Description
03-Oct-2006    VH          Initial
****************************************************************************/
TyError DL_OPXD_ConfigureDevice(TyDevHandle tyDevHandle, TyDiskwareType tyDwType, TyFpgawareType tyFpgaType,
                                const char *pszDiskwareFilename, const char *pszFpgawareFilename)
{
   FILE           *pFile;
   unsigned char   bFileLoaded;
   unsigned char  *pucBuffer;
   int32_t         iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   TyError         tyResult = DRVOPXD_ERROR_NO_ERROR;
   TyDeviceInfo    tyDeviceInfo;

   assert(pszDiskwareFilename != NULL);
   assert(pszFpgawareFilename != NULL);
   assert(tyDevHandle < MAX_DEVICES_SUPPORTED);

   if (DL_OPXD_GetDeviceInfo(tyDevHandle, &tyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
	   return DRVOPXD_ERROR_USB_DRIVER;

   // we need to use both locks
   DL_OPXD_LockDriver();
   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // check if configuration is necessary
   if (ptyDrvDeviceTable[tyDevHandle].iConfigInstances > 0)
      {
      if ((ptyDrvDeviceTable[tyDevHandle].tyDiskwareType == tyDwType) && 
          (ptyDrvDeviceTable[tyDevHandle].tyFpgawareType == tyFpgaType))
         {  // already configured with same types
         ptyDrvDeviceTable[tyDevHandle].iConfigInstances++;
         tyResult = DRVOPXD_ERROR_NO_ERROR;
         }
      else
         tyResult = DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE;

      DL_OPXD_UnlockDevice(tyDevHandle);
      DL_OPXD_UnlockDriver();
      return(tyResult);
      }

   // first configure FPGA if requested
   if (strcmp(pszFpgawareFilename,""))
      {
      // load FPGA configuration data to external memory
      pFile = fopen(pszFpgawareFilename, "rb");
      bFileLoaded = 0;
      if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pFile != NULL))
         {
         int32_t iBytesRead, iDataLen;
         uint32_t ulLocation;

         iDataLen = BIG_BUFFER_LEN - SMALL_BUFFER_LEN;
         ulLocation = 0x0;          // start with offset 0
         do
            {
            // read data to buffer
            iBytesRead = (int32_t)fread((unsigned char *)(&pucBuffer[0x0c]), 1, (uint32_t)iDataLen, pFile);
            if (iBytesRead > 0)
               {  // send data to device
               *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_LOAD_MEMORY;
               *((uint32_t *)(pucBuffer + 0x04)) = 0xE0000000 | (ulLocation & 0x0FFFFFFF);            // loading to external memory
               *((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)iBytesRead;
               iPacketLen = TRANSMIT_LEN(0x0c + iBytesRead);
               if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
                  tyResult = DRVOPXD_ERROR_USB_DRIVER;
               if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
                   (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
                  tyResult = DRVOPXD_ERROR_USB_DRIVER;
               ulLocation += (uint32_t)iBytesRead;
               }
            }while ((iBytesRead > 0) && (tyResult != DRVOPXD_ERROR_USB_DRIVER));

         if (tyResult == DRVOPXD_ERROR_NO_ERROR)
            bFileLoaded = 1;
         }

      if (pFile != NULL)
         fclose(pFile);

      if (bFileLoaded && (tyResult == DRVOPXD_ERROR_NO_ERROR))
         {
         // configure FPGA
         *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_CONFIGURE_FPGA;
         iPacketLen = TRANSMIT_LEN(4);
         if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
         if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0))
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
         else if (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR)
            tyResult = DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE;
         }
      else
         {
         if (tyResult == DRVOPXD_ERROR_NO_ERROR)
            tyResult = DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME;
         }
      }

   // now load and execute diskware
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (strcmp(pszDiskwareFilename,"")))
      {
      // load diskware to external memory
      pFile = fopen(pszDiskwareFilename, "rb");
      bFileLoaded = 0;
      if (pFile != NULL)
         {
         int32_t iBytesRead, iDataLen;
         uint32_t ulLocation;

         iDataLen = BIG_BUFFER_LEN - SMALL_BUFFER_LEN;
         ulLocation = 0x0;          // start with offset 0
         do
            {
            // read data to buffer
            iBytesRead = (int32_t)fread((unsigned char *)(&pucBuffer[0x0c]), 1, (uint32_t)iDataLen, pFile);
            if (iBytesRead > 0)
               {  // send data to device
               *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_LOAD_MEMORY;

			   if (tyDeviceInfo.ulBoardRevision < 0x03000000)
			   {
				   //R2 still dont have multiple diskware suppport, so use same location for all diskwares
				   *((uint32_t*)(pucBuffer + 0x04)) = 0xE0000000 | (ulLocation & 0x0FFFFFFF);            // loading to other diskware location
			   }
			   else
			   {
				   //TODO:Remove 0xC0000000, 0xD0000000, 0xE0000000 hard codings
				   switch (tyDwType)
				   {
				   case DW_RISCV:
					   *((uint32_t*)(pucBuffer + 0x04)) = 0xD0000000 | (ulLocation & 0x0FFFFFFF);            // loading to RISCV diskware location
					   break;
				   case DW_ARC:
					   *((uint32_t*)(pucBuffer + 0x04)) = 0xC0000000 | (ulLocation & 0x0FFFFFFF);            // loading to ARC diskware location
					   break;
				   default:
					   *((uint32_t*)(pucBuffer + 0x04)) = 0xE0000000 | (ulLocation & 0x0FFFFFFF);            // loading to other diskware location
					   break;
				   }
			   }

               *((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)iBytesRead;
               iPacketLen = TRANSMIT_LEN(0x0c + iBytesRead);
               if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
                  tyResult = DRVOPXD_ERROR_USB_DRIVER;
               if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
                   (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
                  tyResult = DRVOPXD_ERROR_USB_DRIVER;
               ulLocation += (uint32_t)iBytesRead;
               }
            }while ((iBytesRead > 0) && (tyResult != DRVOPXD_ERROR_USB_DRIVER));

         if (tyResult == DRVOPXD_ERROR_NO_ERROR)
            bFileLoaded = 1;
         }

      if (pFile != NULL)
         fclose(pFile);

      if (bFileLoaded && (tyResult == DRVOPXD_ERROR_NO_ERROR))
         {
         // execute diskware
         *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_EXECUTE_DISKWARE;
		 switch (tyDwType)
		 {
		 case DW_RISCV:
			 *((uint32_t *)(pucBuffer + 0x04)) = RISCV;
			 break;
		 case DW_ARC:
			 *((uint32_t *)(pucBuffer + 0x04)) = ARC;
			 break;
		 default:
			 //TODO: See if error can be improved
			 tyResult = DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE;
			 DL_OPXD_UnlockDevice(tyDevHandle);
			 DL_OPXD_UnlockDriver();
			 return(tyResult);
		 }
         iPacketLen = TRANSMIT_LEN(5);
         if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
            tyResult = DRVOPXD_ERROR_USB_DRIVER;

         if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0))
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
         else if (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR)
            tyResult = DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE;
         }
      else
         {
         if (tyResult == DRVOPXD_ERROR_NO_ERROR)
            tyResult = DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME;
         }
      }

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      ptyDrvDeviceTable[tyDevHandle].iConfigInstances = 1;
      ptyDrvDeviceTable[tyDevHandle].tyDiskwareType = tyDwType;
      ptyDrvDeviceTable[tyDevHandle].tyFpgawareType = tyFpgaType;
      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   DL_OPXD_UnlockDriver();
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_UnconfigureDevice
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - device handle from DL_OpenDevice
       Output: error code DRVOPXD_ERROR_xxx
  Description: unconfigure device (terminate diskware and unconfigure FPGA)
Date           Initials    Description
03-Oct-2006    VH          Initial
17_June-2008   NCH		   Remove unconfiguration due to an issue with
						   FPGA unconfiguration
18-June-2019   RSB		   Removed diskware termination as well 
                           (same diskware could be in use by some other instance of gdb server)
****************************************************************************/
TyError DL_OPXD_UnconfigureDevice(TyDevHandle tyDevHandle)
{
   TyError tyResult;

   assert(tyDevHandle < MAX_DEVICES_SUPPORTED);

   tyResult = DRVOPXD_ERROR_NO_ERROR;
   tyDevHandle = tyDevHandle;

   DL_OPXD_LockDriver();
   ptyDrvDeviceTable[tyDevHandle].iConfigInstances--;
   if (ptyDrvDeviceTable[tyDevHandle].iConfigInstances <= 0)
      {
      DL_OPXD_UnlockDevice(tyDevHandle);
      ptyDrvDeviceTable[tyDevHandle].tyDiskwareType = DW_UNKNOWN;
      ptyDrvDeviceTable[tyDevHandle].iConfigInstances = 0;
      }

   DL_OPXD_UnlockDriver();
   return(tyResult);
}

/****************************************************************************
*** Synchronization support                                               ***
****************************************************************************/
int32_t DL_OPXD_LockDevice(TyDevHandle tyDevHandle) 
{ 
   tyDevHandle = tyDevHandle;
   return(0);
}

void DL_OPXD_UnlockDevice(TyDevHandle tyDevHandle)
{
   tyDevHandle = tyDevHandle;
}
void DL_OPXD_LockDriver(void)
{

}
void DL_OPXD_UnlockDriver(void)
{

}




/****************************************************************************
*** Basic Functions                                                      ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_DeviceMemoryAccess
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulMemoryAddress - memory address in Opella-XD memory space
               unsigned char ucDataSize - data size (1 - byte access, 2 - halfword access, 4 - word access)
               uint32_t ulWrittenValue - value to write to memory location (not used for read access)
               uint32_t *pulReadValue  - value read from memory location (can be NULL for write access)
               unsigned char bWriteAccess   - access type (0 - read access, >0 - write access)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Do specified access to memory location on Opella-XD board
               NOTE: No checking of consequences of such an access, use VERY CAREFULLY !!!
Date           Initials    Description
10-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_DeviceMemoryAccess(TyDevHandle tyDevHandle, uint32_t ulMemoryAddress, unsigned char ucDataSize, 
                                   uint32_t ulWrittenValue, uint32_t *pulReadValue, unsigned char bWriteAccess)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (pulReadValue != NULL)
      *pulReadValue = 0;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_MEMORY_ACCESS;
   *((uint32_t *)(pucBuffer + 0x04)) = ulMemoryAddress;
   *((uint32_t *)(pucBuffer + 0x08)) = ulWrittenValue;
   if (bWriteAccess)
      pucBuffer[0xC]  = 0x01;
   else
      pucBuffer[0xC]  = 0x00;

   switch(ucDataSize)
      {
      case 0x01 :    // byte access
         pucBuffer[0xD] = 0x01;
         break;
      case 0x02 :    // half-word access
         pucBuffer[0xD] = 0x02;
         break;
      default :      // word access
         pucBuffer[0xD] = 0x04;
         break;
      }
   iPacketLen = TRANSMIT_LEN(14);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      if (pulReadValue != NULL)
         *pulReadValue = *((uint32_t *)(pucBuffer + 0x04));
      }
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_SetVtpaVoltage
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               float fVoltage - voltage to set [V]
               unsigned char bTrackingTarget - 0x1 for sense target reference voltage and set same as Vtpa (0x0 for fixed Vtpa) 
               unsigned char bEnableDrivers - enable output drivers to the target (if supported on TPA)
       Output: error code DRVOPXD_ERROR_xxx
  Description: set Vtpa voltage and mode
               2 modes are supported:
               fixed voltage (bTrackingTarget==0) - set target voltage to fVoltage regardless of target reference voltage
               tracking mode (bTrackingTarget==1) - check Vtref (target reference voltage) and set Vtpa on same level when Vtref is stable
               Note, Opella-XD can set voltage in range 0V to 5V (0x00==0V; 0x100==6.6V) however it is not recommend to use voltage less than 0.6V.
               In addition, standard voltage levels are prefered as following:         
               0.0 , 0.9 , 1.2 , 1.5 , 1.8 , 2.2 , 2.5 , 2.7 , 3.0 , 3.2 , 3.3 , 3.6 , 4.9 and 5.0
Date           Initials    Description
08-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_SetVtpaVoltage(TyDevHandle tyDevHandle, double dVoltage, unsigned char bTrackingTarget, unsigned char bEnableDrivers)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED || tyDevHandle < 0)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   // restrict voltage to range <0.0;5.0>
   if (dVoltage > 5.0)
      dVoltage = 5.0;
   if (dVoltage < 0.0)
      dVoltage = 0.0;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   if (pucBuffer == NULL)
	   return DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SET_VTPA;
   pucBuffer[0x04] = DL_OPXD_ConvertDoubleToVoltage(dVoltage);

   if (bTrackingTarget)
      pucBuffer[0x05] = 0x01;
   else
      pucBuffer[0x05] = 0x00;

   if (bEnableDrivers)
      pucBuffer[0x06] = 0x01;
   else
      pucBuffer[0x06] = 0x00;

   iPacketLen = TRANSMIT_LEN(7);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_VoltageSense
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyVoltageSenseType tyChannel - channel to read (VS_VTREF, VS_USB or VS_VTPA)
               float *pfVoltage - pointer to value for voltage [V]
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read voltage from Opella-XD on specified channel (Vtref, Vtpa or USB)
Date           Initials    Description
10-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_VoltageSense(TyDevHandle tyDevHandle, TyVoltageSenseType tyChannel, double *pdVoltage)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   unsigned short usVoltage;
   
   assert(pdVoltage != NULL);

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   *pdVoltage = 0.0;
   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_GET_VOLTAGE;
   switch(tyChannel)
      {
      case VS_VTREF : // Vtref voltage
         pucBuffer[4] = 0x00;
         break;

      case VS_USB : // USB voltage
         pucBuffer[4] = 0x01;
         break;

      case VS_VTPA : // Vtpa
      default : 
         pucBuffer[4] = 0x02;
         break;
      }

   iPacketLen = TRANSMIT_LEN(5);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      usVoltage = *((unsigned short *)(pucBuffer + 0x04));        // get result voltage (10-bit from 6.6V range)
      if (usVoltage & 0x0002)
         *pdVoltage = DL_OPXD_ConvertVoltageToDouble((unsigned char)((usVoltage >> 2)+1));
      else
         *pdVoltage = DL_OPXD_ConvertVoltageToDouble((unsigned char)(usVoltage >> 2));
      if (*pdVoltage < 0.1)                                       // restrict sensitivity around 0
         *pdVoltage = 0.0;
      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_MeasurePllFrequency
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               double dExpectedFrequency - expected frequency of PLL (used to choose best measuring method) [Mhz]
               double *pdMeasuredFrequency - pointer to variable for measured frequency [MHz]
       Output: error code DRVOPXD_ERROR_xxx
  Description: measure PLL frequency using FPGA design (clock counters)
               requires expected frequency value in order to use best measurement              
Date           Initials    Description
11-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_MeasurePllFrequency(TyDevHandle tyDevHandle, double dExpectedFrequency, double *pdMeasuredFrequency)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
   unsigned short pusDivRatio[8];
   unsigned char ucRatio, ucSamples;
   unsigned short usTicks;
   double dPeriod, dTemp;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   assert(pdMeasuredFrequency != NULL);

   // select best counter settings to measure expected frequency
   // using 16-bit counter to count PLL clock, need to get closest time period (counter value and divider ratio)
   // expecting about 60000 pulses during that time
   dPeriod = (60000.0 / (dExpectedFrequency * 1000000.0)) * 10.0;     // [s] period to measure 60000 pll counts, FPGA measures PLL freq/10
   // divider ratios for OKI timer
   pusDivRatio[0] = 1;      
   pusDivRatio[1] = 2;      
   pusDivRatio[2] = 4;      
   pusDivRatio[3] = 8;
   pusDivRatio[4] = 16;     
   pusDivRatio[5] = 64;     
   pusDivRatio[6] = 128;    
   pusDivRatio[7] = 256;

   ucRatio = 0;
   while (ucRatio < 7)
      {
      dTemp = (65535.0 / 7500000.0) * pusDivRatio[ucRatio];       // calculate maximum period with given clock (7.5MHz timer clock, 16-bit counter)
      if (dPeriod < dTemp)
         break;                                                   // found ratio
      ucRatio++;
      }

   // now calculate number of counts required for period with given ratio
   dTemp = (dPeriod * 7500000.0) / pusDivRatio[ucRatio];
   usTicks = (unsigned short)(dTemp + 0.5);                       // round number of ticks

   // decide number of samples
   if (dExpectedFrequency < 10.0)
      ucSamples = 1;
   else if (dExpectedFrequency < 100.0)
      ucSamples = 3;
   else
      ucSamples = 4;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_MEASURE_PLL;
   *((unsigned short *)(pucBuffer + 0x4)) = usTicks;
   *((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)ucRatio;          // set div ratio (index)
   pucBuffer[8] = ucSamples;

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   *pdMeasuredFrequency = 0.0;
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      unsigned char ucIndex;
      double dCounts = 0.0;

      // get average count
      for (ucIndex=0; ucIndex < ucSamples; ucIndex++)
         dCounts += (double)(*((uint32_t *)(pucBuffer + 0x04 + ucIndex * sizeof(uint32_t))));
      dCounts /= ucSamples;

      // get measured frequency
      *pdMeasuredFrequency = ((dCounts / usTicks) / pusDivRatio[ucRatio]) * 7500000.0;
      *pdMeasuredFrequency /= 1000000.0;                    // convert to MHz
      *pdMeasuredFrequency *= 10.0;                         // FPGA measures PLL/10
      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_WriteTpaSigniture
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyTpaParameters *ptyTpaParams - pointer to structure with TPA parameters
       Output: error code DRVOPXD_ERROR_xxx
  Description: writes TPA signiture into connected TPA
               Notes:
Date           Initials    Description
09-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_WriteTpaSigniture(TyDevHandle tyDevHandle, TyTpaParameters *ptyTpaParams)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyTpaEeprom tyTpaEeprom;
   uint32_t ulChecksum, ulIndex;
   uint32_t *pulPtr;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   assert(ptyTpaParams != NULL);
   assert(sizeof(TyTpaEeprom) <= 0x100);

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   // first convert signiture into EEPROM image
   memset((void *)&tyTpaEeprom, 0, sizeof(TyTpaEeprom));          // clear structure (also add terminating 0s to strings)
   tyTpaEeprom.ulTpaId = 0x79A1D79A;                              // TPA ID code
   strncpy(tyTpaEeprom.pucTpaSerialNumber, ptyTpaParams->pucTpaSerialNumber, 15);         // copy serial number (max 15 chars)
   tyTpaEeprom.ulManufDate = ptyTpaParams->ulManufacturingDate;
   tyTpaEeprom.ulTpaVersion = ptyTpaParams->ulTpaRevision;
   switch (ptyTpaParams->tyTpaType)
      {
      case TPA_MIPS14:                                            // *** TPA-MIPS14 ***
         // general info
         tyTpaEeprom.usTpaType = 0x0100;                          // MIPS14 type
         tyTpaEeprom.usFpgaId  = 0x0100;                          // requires MIPS FPGA
         strncpy(tyTpaEeprom.pucTpaName, "TPAOP-MIPS14", 15);     // define name as TPAOP-MIPS14
         // FPGA access info
         tyTpaEeprom.ulTpaOutputDriverMask = 0x00000001;          // use FSIO0 to control DRI_EN
         // voltage info
         tyTpaEeprom.ucDefaultVtpa = DL_OPXD_ConvertDoubleToVoltage(3.3);  // default voltage 3.3V
         tyTpaEeprom.ucMaxVtpa = DL_OPXD_ConvertDoubleToVoltage(3.3);      // maximum voltage 3.3V
         tyTpaEeprom.ucMinVtpa = DL_OPXD_ConvertDoubleToVoltage(1.2);      // minimum voltage 1.2V
         // frequency info
         tyTpaEeprom.ulMinFrequency = 0;                          // 0 MHz min frequency
         tyTpaEeprom.ulMaxFrequency = 100000000;                  // 100 MHz max frequency
         break;
      case TPA_ARM20:                                             // *** TPA-JTAG20 (ARM20) ***
         // general info
         tyTpaEeprom.usTpaType = 0x0200;                          // ARM20 type
         tyTpaEeprom.usFpgaId  = 0x0200;                          // requires MIPS FPGA
         strncpy(tyTpaEeprom.pucTpaName, "TPAOP-ARM20", 15);      // define name as TPAOP-ARM20
         // FPGA access info
         tyTpaEeprom.ulTpaOutputDriverMask = 0x00000041;          // use FSIO0 and FSIO6 to control DRI_EN and EN_TRST
         // voltage info
         tyTpaEeprom.ucDefaultVtpa = DL_OPXD_ConvertDoubleToVoltage(3.3);  // default voltage 3.3V
         tyTpaEeprom.ucMaxVtpa = DL_OPXD_ConvertDoubleToVoltage(3.3);      // maximum voltage 3.3V
         tyTpaEeprom.ucMinVtpa = DL_OPXD_ConvertDoubleToVoltage(1.2);      // minimum voltage 1.2V
         // frequency info
         tyTpaEeprom.ulMinFrequency = 0;                          // 0 MHz min frequency
         tyTpaEeprom.ulMaxFrequency = 100000000;                  // 100 MHz max frequency
         break;
      case TPA_ARC20:                                             // *** TPA-JTAG20 (ARC20) ***
         // general info
         tyTpaEeprom.usTpaType = 0x0300;                          // ARC20 type
         tyTpaEeprom.usFpgaId  = 0x0300;                          // requires MIPS FPGA
         strncpy(tyTpaEeprom.pucTpaName, "TPAOP-ARC20", 15);      // define name as TPAOP-ARC20
         // FPGA access info
         tyTpaEeprom.ulTpaOutputDriverMask = 0x00000041;          // use FSIO0 and FSIO6 to control DRI_EN and EN_TRST
         // voltage info
         tyTpaEeprom.ucDefaultVtpa = DL_OPXD_ConvertDoubleToVoltage(3.3);  // default voltage 3.3V
         tyTpaEeprom.ucMaxVtpa = DL_OPXD_ConvertDoubleToVoltage(3.3);      // maximum voltage 3.3V
         tyTpaEeprom.ucMinVtpa = DL_OPXD_ConvertDoubleToVoltage(1.2);      // minimum voltage 1.2V
         // frequency info
         tyTpaEeprom.ulMinFrequency = 0;                          // 0 MHz min frequency
         tyTpaEeprom.ulMaxFrequency = 100000000;                  // 100 MHz max frequency
         break;
      case TPA_DIAG:                                              // *** Opella-XD Production Test Board (DIAG) ***
         // general info
         tyTpaEeprom.usTpaType = 0xFF00;                          // DIAG type
         tyTpaEeprom.usFpgaId  = 0xFF00;                          // requires DIAG FPGA
         strncpy(tyTpaEeprom.pucTpaName, "DIAGNOSTICS", 15);      // define name as DIAGNOSTICS
         // FPGA access info
         tyTpaEeprom.ulTpaOutputDriverMask = 0x00000000;          // no driver buffers available
         // voltage info
         tyTpaEeprom.ucDefaultVtpa = DL_OPXD_ConvertDoubleToVoltage(2.0);  // default voltage 2.0V
         tyTpaEeprom.ucMaxVtpa = DL_OPXD_ConvertDoubleToVoltage(5.0);      // maximum voltage 5.0V
         tyTpaEeprom.ucMinVtpa = DL_OPXD_ConvertDoubleToVoltage(1.2);      // minimum voltage 1.2V
         // frequency info
         tyTpaEeprom.ulMinFrequency = 0;                          // 0 MHz min frequency
         tyTpaEeprom.ulMaxFrequency = 200000000;                  // 200 MHz max frequency
         break;
      case TPA_UNKNOWN:
      case TPA_NONE:
      case TPA_INVALID:
      default:
         return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
      }

   // calculate checksum
   ulChecksum = 0;      pulPtr = (uint32_t *)&tyTpaEeprom;
   for (ulIndex=0; ulIndex < (sizeof(TyTpaEeprom)/sizeof(uint32_t)); ulIndex++)
      {
      ulChecksum += *pulPtr;
      pulPtr++;
      }
   tyTpaEeprom.ulChecksum = 0xFFFFFFFF - ulChecksum + 1;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_PROGRAM_TPA_EEPROM;
   *((unsigned short *)(pucBuffer + 0x4)) = 0;
   *((unsigned short *)(pucBuffer + 0x6)) = 0x100;                      // 256 bytes to program
   memcpy((void *)(pucBuffer + 0x08), (void *)&tyTpaEeprom, sizeof(TyTpaEeprom));
   iPacketLen = TRANSMIT_LEN(8 + sizeof(TyTpaEeprom));

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // now read back TPA EEPROM
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_READ_TPA_EEPROM;
   *((unsigned short *)(pucBuffer + 0x4)) = 0;
   *((unsigned short *)(pucBuffer + 0x6)) = 0x100;                      // 256 bytes to read
   iPacketLen = TRANSMIT_LEN(8);
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {     // compare TPA EEPROM images
      if (memcmp((void *)(pucBuffer + 0x04), (void *)&tyTpaEeprom, sizeof(TyTpaEeprom)))
         tyResult = DRVOPXD_ERROR_TPA_PROGRAM_ERROR;
      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ProcessCP15Register
     Engineer: Roshan T R
        Input: TyDevHandle tyDevHandle - handle to open device
		       uint32_t uiFamily - Processor Family (ARM7, 9 or 11)
		       uint32_t uiCore - Core type
		       uint32_t uiOperation - Operation READ/WRITE
               uint32_t ulOpcode - Opcode of MCR/ MRC instruction
			   uint32_t *puiRegValue - Pointer to value read/write from/to register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read the specified coprocessor register 
Date           Initials    Description
07-Dec-2009    RTR         Initial
****************************************************************************/
TyError DL_OPXD_ProcessCP15Register(TyDevHandle tyDevHandle, uint32_t uiCore,
							   uint32_t uiArmType,uint32_t uiArmFamily, uint32_t uiOperation,
							   uint32_t ulOpcode, uint32_t *pulRegValue)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	// create packet
	if(uiOperation)
	{
		*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_READ_CP_REGISTER;
	}
	else
	{
		*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_WRITE_CP_REGISTER;
	}
	*((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)(uiCore);
	*((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)(uiArmType);
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)(uiArmFamily);
	*((uint32_t *)(pucBuffer + 0x10)) = ulOpcode;
	if(!uiOperation)
	{
		*((uint32_t *)(pucBuffer + 0x14)) = *pulRegValue;
		iPacketLen = TRANSMIT_LEN(24);
	}
	else
	{
		iPacketLen = TRANSMIT_LEN(20);
	}

	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	if (tyResult == DRVOPXD_ERROR_NO_ERROR && uiOperation)
	{
		if (pulRegValue != NULL)
		{
			*pulRegValue = (uint32_t)(*((uint32_t *)(pucBuffer + 0x04)));
		}
	}
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ReadCPRegister
     Engineer: Roshan T R
        Input: TyDevHandle tyDevHandle - handle to open device
		       uint32_t uiFamily - Processor Family (ARM7, 9 or 11)
		       uint32_t uiCore - Core type
		       uint32_t uiCPNo - The coprocessor to be read
               uint32_t uiRegIndex - The register index
			   uint32_t *puiRegValue - Value read from register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read the specified coprocessor register 
Date           Initials    Description
07-Dec-2009    RTR         Initial
****************************************************************************/
TyError DL_OPXD_ReadCPRegister(TyDevHandle tyDevHandle, uint32_t uiCore,
							   uint32_t uiArmType,uint32_t uiArmFamily, uint32_t uiCpNum,
							   uint32_t uiRegIndex, uint32_t *puiRegValue)
{
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	// create packet
	*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_READ_CP_REGISTER;
	*((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)(uiCore);
	*((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)(uiArmType);
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)(uiArmFamily);
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)(uiCpNum);
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)(uiRegIndex);
	
	iPacketLen = TRANSMIT_LEN(24);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	if (tyResult == DRVOPXD_ERROR_NO_ERROR)
	{
		if (puiRegValue != NULL)
		{
			*puiRegValue = (uint32_t)(*((uint32_t *)(pucBuffer + 0x04)));
		}
	}
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_WriteCPRegister
     Engineer: Roshan T R
        Input: TyDevHandle tyDevHandle - handle to open device
		       uint32_t uiFamily - Processor Family (ARM7, 9 or 11)
		       uint32_t uiCore - Core type
		       uint32_t uiCPNo - The coprocessor to be read
               uint32_t uiRegIndex - The register index
			   uint32_t uiRegValue - Value to be write to the register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read the specified coprocessor register 
Date           Initials    Description
07-Dec-2009    RTR         Initial
****************************************************************************/
TyError DL_OPXD_WriteCPRegister(TyDevHandle tyDevHandle, uint32_t uiCore,
							   uint32_t uiArmType, uint32_t uiArmFamily,
							   uint32_t uiCpNum, uint32_t uiRegIndex, 
							   uint32_t uiRegValue)
{

	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	// create packet
	*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_WRITE_CP_REGISTER;
	*((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)(uiCore);
	*((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)(uiArmType);
	*((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)(uiArmFamily);
	*((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)(uiCpNum);
	*((uint32_t *)(pucBuffer + 0x14)) = (uint32_t)(uiRegIndex);
	*((uint32_t *)(pucBuffer + 0x18)) = (uint32_t)(uiRegValue);
	
	iPacketLen = TRANSMIT_LEN(28);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}

/****************************************************************************
*** Basic JTAG Functions                                                  ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_JtagSetFrequency
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               double dFrequency - frequency value [MHz]
               double *pdPllFrequency - pointer to value for frequency set in PLL itself (can be NULL)
               double *pdFpgaDivideRatio - pointer to value for FPGA frequency divide ratio (can be NULL)
       Output: error code DRVOPXD_ERROR_xxx
  Description: set JTAG clock frequency (JTAG engine frequency), returns error code and frequency 
               value for PLL itself and FPGA divide ratio
Date           Initials    Description
23-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagSetFrequency(TyDevHandle tyDevHandle, double dFrequency, double *pdPllFrequency, double *pdFpgaDivideRatio)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED || tyDevHandle < 0)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   if (pucBuffer == NULL)
	   return DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SET_JTAG_FREQUENCY;
   *((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)(dFrequency * 1000000);              // frequency value in Hz
   pucBuffer[0x08] = 0x01;                                                                      // using selectable frequency from PLL
   pucBuffer[0x09] = 0x00;                                                                      // disabling adaptive clock
   pucBuffer[0x0A] = 0x00;    pucBuffer[0x0B] = 0x00;                                           // reserved
   *((unsigned short *)(pucBuffer + 0x0C)) = 0x0;                                               // timeout in ms
   pucBuffer[0x0E] = 0x00;                                                                      // not using reset occured to stop adaptive clock
   pucBuffer[0x0F] = 0x00;                                                                      // reserved
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      if (pdPllFrequency != NULL)
         {
         *pdPllFrequency = (double)(*((uint32_t *)(pucBuffer + 0x08)));
         *pdPllFrequency /= 1000000;                                                               // convert to MHz
         }
      if (pdFpgaDivideRatio != NULL)
         *pdFpgaDivideRatio = (double)(*((uint32_t *)(pucBuffer + 0x0C)));
      if (*((uint32_t *)(pucBuffer + 0x0C)) >= 1000)
         ptyDrvDeviceTable[tyDevHandle].iDataTimeout = DRV_TIMEOUT * 10;                           // set longer timeout for frequency < 10 kHz
      else
         ptyDrvDeviceTable[tyDevHandle].iDataTimeout = DRV_TIMEOUT;
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagSetAdaptiveClock
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulTimeout - timeout for adaptive clock when RTCK is not present [ms]
               double dAlternativeFrequency - frequency to be used as alternative [MHz]
       Output: error code DRVOPXD_ERROR_xxx
  Description: set adaptive JTAG clock with timeout in case that RTCK stops toggling and
               alternative frequency (used for timing of nTRST, etc.)
Date           Initials    Description
03-Oct-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagSetAdaptiveClock(TyDevHandle tyDevHandle, uint32_t ulTimeout, double dAlternativeFrequency)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulTimeout > MAX_ADAPTIVE_CLOCK_TIMEOUT)
      return DRVOPXD_ERROR_INVALID_FREQUENCY;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SET_JTAG_FREQUENCY;
   *((uint32_t *)(pucBuffer + 0x04)) = (uint32_t)(dAlternativeFrequency * 1000000);   // frequency value in Hz
   pucBuffer[0x08] = 0x01;                                                                      // using selectable frequency from PLL
   pucBuffer[0x09] = 0x01;                                                                      // enabling adaptive clock
   pucBuffer[0x0A] = 0x00;    pucBuffer[0x0B] = 0x00;                                           // reserved
   *((unsigned short *)(pucBuffer + 0x0C)) = (unsigned short)(ulTimeout & 0xFFFF);              // timeout in ms
   pucBuffer[0x0E] = 0x00;                                                                      // not using reset occurred to stop adaptive clock
   pucBuffer[0x0F] = 0x00;                                                                      // reserved
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      if (*((uint32_t *)(pucBuffer + 0x0C)) >= 1000)
         ptyDrvDeviceTable[tyDevHandle].iDataTimeout = DRV_TIMEOUT * 10 + (int32_t)ulTimeout;       // set longer timeout for frequency < 10 kHz
      else
         ptyDrvDeviceTable[tyDevHandle].iDataTimeout = DRV_TIMEOUT + (int32_t)ulTimeout;  
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagScanIR
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1), 0 if no MC was selected 
               uint32_t ulIRLength - length of IR register, valid range is <1;8160>
               unsigend long *pulDataIn - pointer to data to be shifted into scanchain, if NULL, all 1s are used
               unsigend long *pulDataOut - pointer to data to be shifted from scanchain, if NULL, output data are ignored
       Output: error code DRVOPXD_ERROR_xxx
  Description: shifts specific data into IR register of selected core on scanchain (other cores in bypass mode) and get output data
Date           Initials    Description
29-May-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagScanIR(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulIRLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   uint32_t ulDataWords;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if ((ulIRLength == 0) || (ulIRLength > MAX_SCANCHAIN_LENGTH))  // check scanchain length
      return DRVOPXD_ERROR_JTAG_LENGTH_ERROR;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)                          // check core number
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SCAN_IR;                 // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;          // core number
   if (pulDataOut == NULL)                                                    // set whether output from scanchain is ignored
      pucBuffer[0x06] = 0x0;
   else
      pucBuffer[0x06] = 0x1;
   // ignore 5 bytes (for future use)
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = 0x0;
   pucBuffer[0x09] = 0x0;
   pucBuffer[0x0A] = 0x0;
   pucBuffer[0x0B] = 0x0;
   // set scan parameters
   *((uint32_t *)(pucBuffer + 0x0C)) = ulIRLength;
   if (ulIRLength % 32)
      ulDataWords = (ulIRLength / 32) + 1;
   else
      ulDataWords = (ulIRLength / 32);
   if (pulDataIn == NULL)
      memset((void *)(pucBuffer + 0x10), 0xff, ulDataWords * sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x10), (void *)pulDataIn, ulDataWords * sizeof(uint32_t));
   iPacketLen = TRANSMIT_LEN(16 + (int32_t)(ulDataWords*sizeof(uint32_t)));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get data shifted from scanchain if requested
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pulDataOut != NULL))
      memcpy((void *)pulDataOut, (void *)(pucBuffer + 0x04), ulDataWords * sizeof(uint32_t));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_JtagScanDR
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1), 0 when MC is not used 
               uint32_t ulDRLength - length of DR register, valid range is <1;8160>
               unsigend long *pulDataIn - pointer to data to be shifted into scanchain, if NULL, 0s are used
               unsigend long *pulDataOut - pointer to data to be shifted from scanchain, if NULL, output data are ignored
       Output: error code DRVOPXD_ERROR_xxx
  Description: shifts specific data into DR register of selected core on scanchain (other cores in bypass mode) and get output data
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagScanDR(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulDRLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   uint32_t ulDataWords;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if ((ulDRLength == 0) || (ulDRLength > MAX_SCANCHAIN_LENGTH))  // check scanchain length
      return DRVOPXD_ERROR_JTAG_LENGTH_ERROR;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)                          // check core number
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SCAN_DR;                 // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;          // core number
   if (pulDataOut == NULL)                                                    // set whether output from scanchain is ignored
      pucBuffer[0x06] = 0x0;
   else
      pucBuffer[0x06] = 0x1;
   // ignore 5 bytes (for future use)
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = 0x0;
   pucBuffer[0x09] = 0x0;
   pucBuffer[0x0A] = 0x0;
   pucBuffer[0x0B] = 0x0;
   // set scan parameters
   *((uint32_t *)(pucBuffer + 0x0C)) = ulDRLength;
   if (ulDRLength % 32)
      ulDataWords = (ulDRLength / 32) + 1;
   else
      ulDataWords = (ulDRLength / 32);
   if (pulDataIn == NULL)
      memset((void *)(pucBuffer + 0x10), 0x00, ulDataWords * sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x10), (void *)pulDataIn, ulDataWords * sizeof(uint32_t));
   
   iPacketLen = TRANSMIT_LEN(16 + (int32_t)(ulDataWords*sizeof(uint32_t)));
   
	// send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get data shifted from scanchain if requested
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pulDataOut != NULL))
      memcpy((void *)pulDataOut, (void *)(pucBuffer + 0x04), ulDataWords * sizeof(uint32_t));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagScanIRandDR
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1), 0 if no MC was selected 
               uint32_t ulIRLength - length of DR register, if 0, ulDefaultDRLength from MC config structure is used
               unsigend long *pulDataInForIR - pointer to data to be shifted into scanchain during IR scan
               uint32_t ulDRLength - length of DR register, if 0, ulDefaultDRLength from MC config structure is used
               unsigend long *pulDataInForDR - pointer to data to be shifted into scanchain during DR scan, if NULL, 0s are used
               unsigend long *pulDataOutFromDR - pointer to data to be shifted from scanchain during DR scan, if NULL, output data are ignored
       Output: error code DRVOPXD_ERROR_xxx
  Description: shifts specific data into DR register of selected core on scanchain (other cores in bypass mode) and get output data
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagScanIRandDR(TyDevHandle tyDevHandle, 
								uint32_t ulCore, 
								uint32_t ulIRLength, 
								uint32_t *pulDataInForIR, 
                                uint32_t ulDRLength, 
								uint32_t *pulDataInForDR, 
								uint32_t *pulDataOutFromDR)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   uint32_t ulDataWordsIR, ulDataWordsDR;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if ((ulIRLength == 0) || (ulIRLength > MAX_SCANCHAIN_LENGTH) || 
       (ulDRLength == 0) || (ulDRLength > MAX_SCANCHAIN_LENGTH))  // check scanchain length
      return DRVOPXD_ERROR_JTAG_LENGTH_ERROR;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)                          // check core number
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SCAN_IR_DR;              // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;          // core number
   if (pulDataOutFromDR == NULL)                                              // set whether output from scanchain is ignored
      pucBuffer[0x06] = 0x0;
   else
      pucBuffer[0x06] = 0x1;
   // ignore 9 bytes (for future use)
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = 0x0;
   pucBuffer[0x09] = 0x0;
   pucBuffer[0x0A] = 0x0;
   pucBuffer[0x0B] = 0x0;
   pucBuffer[0x0C] = 0x0;
   pucBuffer[0x0D] = 0x0;
   pucBuffer[0x0E] = 0x0;
   pucBuffer[0x0F] = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = ulIRLength;
   *((uint32_t *)(pucBuffer + 0x14)) = ulDRLength;

   if (ulIRLength % 32)
      ulDataWordsIR = (ulIRLength / 32) + 1;
   else
      ulDataWordsIR = (ulIRLength / 32);

   if (ulDRLength % 32)
      ulDataWordsDR = (ulDRLength / 32) + 1;
   else
      ulDataWordsDR = (ulDRLength / 32);

   // add data for IR
   if (pulDataInForIR == NULL)
      memset((void *)(pucBuffer + 0x18), 0xff, ulDataWordsIR * sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x18), (void *)pulDataInForIR, ulDataWordsIR * sizeof(uint32_t));
   // add data for DR
   if (pulDataInForDR == NULL)
      memset((void *)(pucBuffer + 0x18 + (ulDataWordsIR * sizeof(uint32_t))), 0x00, ulDataWordsDR * sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x18 + (ulDataWordsIR * sizeof(uint32_t))), (void *)pulDataInForDR, ulDataWordsDR * sizeof(uint32_t));

   iPacketLen = TRANSMIT_LEN(24 + (int32_t)(ulDataWordsIR*sizeof(uint32_t)) + (int32_t)(ulDataWordsDR*sizeof(uint32_t)));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get data shifted from scanchain if requested
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pulDataOutFromDR != NULL))
      memcpy((void *)pulDataOutFromDR, (void *)(pucBuffer + 0x04), ulDataWordsDR * sizeof(uint32_t));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagConfigMulticore
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulNumberOfTAPs - number of TAPs on scanchain, 0 means no multicore support 
               TyTAPConfig ptyTAPConfig[] - array of core config data (NULL if no MC support)
       Output: error code DRVOPXD_ERROR_xxx
  Description: configures data structures for multicore support, possibly disables entirely MC support
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagConfigMulticore(TyDevHandle tyDevHandle, uint32_t ulNumberOfTAPs, TyTAPConfig ptyTAPConfig[])
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   uint32_t ulCnt, ulOffset, ulBit, ulBitPos, ulTotalLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   // check valid parameters
   if ((ulNumberOfTAPs > MAX_TAPS_ON_SCANCHAIN) || ((ulNumberOfTAPs > 0) && (ptyTAPConfig == NULL)))
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   ulTotalLen = 0;
   for (ulCnt=0; ulCnt < ulNumberOfTAPs; ulCnt++)
      {
      if ((ptyTAPConfig[ulCnt].usDefaultIRLength > MAX_SCANCHAIN_LENGTH) || (ptyTAPConfig[ulCnt].pulBypassIRPattern == NULL))
         return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
      ulTotalLen += ptyTAPConfig[ulCnt].usDefaultIRLength;
      }
   if (ulTotalLen > MAX_MULTICORE_SCANCHAIN_LENGTH)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_SET_MULTICORE;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulNumberOfTAPs;  // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0x0000;
   ulOffset = 0x08;
   // copy each IR length to packet
   for (ulCnt=0; ulCnt < ulNumberOfTAPs; ulCnt++)
      {
      *((unsigned short *)(pucBuffer + ulOffset)) = ptyTAPConfig[ulCnt].usDefaultIRLength;
      ulOffset += 2;
      }
   // do same for default DR length
   for (ulCnt=0; ulCnt < ulNumberOfTAPs; ulCnt++)
      {
      *((unsigned short *)(pucBuffer + ulOffset)) = ptyTAPConfig[ulCnt].usDefaultDRLength;
      ulOffset += 2;
      }
   // ulOffset is alligned to word
   if (ulTotalLen % 32)
      *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)((ulTotalLen / 32) + 1);
   else
      *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)(ulTotalLen / 32);
   *((unsigned short *)(pucBuffer + ulOffset + 2)) = 0x0000;
   ulOffset += 4;
   // now create whole IRBypass pattern
   ulBitPos = 0;
   for (ulCnt=ulNumberOfTAPs; ulCnt > 0; ulCnt--)
      {
      for (ulBit=0; ulBit < ptyTAPConfig[ulCnt-1].usDefaultIRLength; ulBit++)        // process every bit
         {
         uint32_t ulCurrentBit;
         uint32_t ulMask = (0x00000001 << ulBitPos);
         ulCurrentBit = ((ptyTAPConfig[ulCnt-1].pulBypassIRPattern[ulBit/32]) >> (ulBit % 32)) & 0x1;         // get current bit from pattern
         if (ulCurrentBit)
            *((uint32_t *)(pucBuffer + ulOffset)) |= ulMask;
         else
            *((uint32_t *)(pucBuffer + ulOffset)) &= ~ulMask;
         ulBitPos++;
         if (!(ulBitPos % 32))
            {
            ulBitPos = 0;
            ulOffset += 4;          // go to next word
            }
         }
      }
   if (ulBitPos % 32)
      ulOffset += 4;                // ensure all words are written
   iPacketLen = TRANSMIT_LEN((int32_t)ulOffset);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagSetTMS
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyTmsSequence *ptyTmsForScanIR - pointer to structure with TMS sequence for ScanIR
               TyTmsSequence *ptyTmsForScanDR - pointer to structure with TMS sequence for ScanDR
               TyTmsSequence *ptyTmsForScanIRandDR_IR - pointer to structure with TMS sequence for ScanIRandDR (IR phase)
               TyTmsSequence *ptyTmsForScanIRandDR_DR - pointer to structure with TMS sequence for ScanIRandDR (DR phase)
       Output: error code DRVOPXD_ERROR_xxx
  Description: This function configures TMS sequences for ScanIR, ScanDR and ScanIRandDR functions.
               If any pointer is NULL, default sequences are used (residing in Run-Test/Idle after each scan).
Date           Initials    Description
29-May-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagSetTMS(TyDevHandle tyDevHandle, TyTmsSequence *ptyTmsForScanIR, TyTmsSequence *ptyTmsForScanDR,
                           TyTmsSequence *ptyTmsForScanIRandDR_IR, TyTmsSequence *ptyTmsForScanIRandDR_DR)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_SET_TMS;    // command code
   // set TMS sequence for ScanIR
   if (ptyTmsForScanIR == NULL)
      {
      *((unsigned char*)(pucBuffer + 0x04))		= 5;		// 5 bits before IR scan
	  *((uint16_t*)(pucBuffer + 0x05))			= 0x06;
	  *((unsigned char*)(pucBuffer + 0x07))		= 4;		// 4 bits after IR scan
	  *((uint16_t*)(pucBuffer + 0x08))			= 0x03;
      }
   else
      {
	   *((unsigned char*)(pucBuffer + 0x04)) = ptyTmsForScanIR->ucPreTmsCount;
	   *((uint16_t*)(pucBuffer + 0x05)) = ptyTmsForScanIR->usPreTmsBits;
	   *((unsigned char*)(pucBuffer + 0x07)) = ptyTmsForScanIR->ucPostTmsCount;
	   *((uint16_t*)(pucBuffer + 0x08)) = ptyTmsForScanIR->usPostTmsBits;
      }
   // set TMS sequence for ScanDR
   if (ptyTmsForScanDR == NULL)
      {
	   *((unsigned char*)(pucBuffer + 0x0A)) = 4;		// 5 bits before DR scan
	   *((uint16_t*)(pucBuffer + 0x0B))		 = 0x02;
	   *((unsigned char*)(pucBuffer + 0x0D)) = 4;		// 4 bits after DR scan
	   *((uint16_t*)(pucBuffer + 0x0E))		 = 0x03;
      }
   else
      {
	   *((unsigned char*)(pucBuffer + 0x0A)) = ptyTmsForScanDR->ucPreTmsCount;
	   *((uint16_t*)(pucBuffer + 0x0B))		 = ptyTmsForScanDR->usPreTmsBits;
	   *((unsigned char*)(pucBuffer + 0x0D)) = ptyTmsForScanDR->ucPostTmsCount;
	   *((uint16_t*)(pucBuffer + 0x0E))		 = ptyTmsForScanDR->usPostTmsBits;
      }
   // set TMS sequence for ScanIRandDR (IR phase)
   if (ptyTmsForScanIRandDR_IR == NULL)
      {     
	   *((unsigned char*)(pucBuffer + 0x10)) = 5;		// 5 bits before IR scan
	   *((uint16_t*)(pucBuffer + 0x11))		 = 0x06;
	   *((unsigned char*)(pucBuffer + 0x13)) = 4;		// 4 bits after IR scan
	   *((uint16_t*)(pucBuffer + 0x14))		 = 0x03;
      }
   else
      {
	   *((unsigned char*)(pucBuffer + 0x10)) = ptyTmsForScanIRandDR_IR->ucPreTmsCount;
	   *((uint16_t*)(pucBuffer + 0x11))		 = ptyTmsForScanIRandDR_IR->usPreTmsBits;
	   *((unsigned char*)(pucBuffer + 0x13)) = ptyTmsForScanIRandDR_IR->ucPostTmsCount;
	   *((uint16_t*)(pucBuffer + 0x14))		 = ptyTmsForScanIRandDR_IR->usPostTmsBits;
      }
   // set TMS sequence for ScanIRandDR (DR phase)
   if (ptyTmsForScanIRandDR_DR == NULL)
      {     
	   *((unsigned char*)(pucBuffer + 0x16)) = 4;		// 5 bits before DR scan
	   *((uint16_t*)(pucBuffer + 0x17))		 = 0x02;
	   *((unsigned char*)(pucBuffer + 0x19)) = 4;		// 4 bits after DR scan
	   *((uint16_t*)(pucBuffer + 0x1A))		 = 0x03;
      }
   else
      {
	   *((unsigned char*)(pucBuffer + 0x16)) = ptyTmsForScanIRandDR_DR->ucPreTmsCount;
	   *((uint16_t*)(pucBuffer + 0x17))		 = ptyTmsForScanIRandDR_DR->usPreTmsBits;
	   *((unsigned char*)(pucBuffer + 0x19)) = ptyTmsForScanIRandDR_DR->ucPostTmsCount;
	   *((uint16_t*)(pucBuffer + 0x1A))		 = ptyTmsForScanIRandDR_DR->usPostTmsBits;
      }
   iPacketLen = TRANSMIT_LEN(28);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagResetTAP
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               bool bUseTrst -  TRUE - reset TAPs using TRST signal 
                                FALSE - TMS high during sufficient clock cycles
       Output: error code DRVOPXD_ERROR_xxx
  Description: reset JTAG TAPs on scanchain
Date           Initials    Description
18-Jan-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagResetTAP(TyDevHandle tyDevHandle, unsigned char bUseTrst)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;             // command code
   if (bUseTrst)
      pucBuffer[4] = 0x01;
   else
      pucBuffer[4] = 0x00;
   iPacketLen = TRANSMIT_LEN(5);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_IndicateTargetStatus
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number
               unsigned char bTargetIsRunning - >0 if target is running
       Output: error code DRVOPXD_ERROR_xxx
  Description: indicate target status change by Opella-XD LED
Date           Initials    Description
16-Apr-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_IndicateTargetStatus(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bTargetIsRunning)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   assert(ulCore < MAX_TAPS_ON_SCANCHAIN);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_TARGET_STATUS_CHANGED;          // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                 // core number
   if (bTargetIsRunning)
      pucBuffer[0x6] = 0x01;
   else
      pucBuffer[0x6] = 0x00;
   pucBuffer[0x7] = 0x00;
   iPacketLen = TRANSMIT_LEN(8);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagPulses
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulNumberOfPulses - number of pulses to generate (max. 65536)
               uint32_t ulPulsePeriodUs - pulse period in us (between 1 and 1000)
               uint32_t *pulTDIPattern - pattern for TDI
               uint32_t *pulTMSPattern - pattern for TMS
               uint32_t *pulTDOPattern - captured pattern for TDO
       Output: error code DRVOPXD_ERROR_xxx
  Description: Generate specified number of JTAG pulses using pulse period and TDI and TMS patterns.
               Values of TDO signal are captured as well if required.
Date           Initials    Description
13-Dec-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagPulses(TyDevHandle tyDevHandle, uint32_t ulNumberOfPulses, uint32_t ulPulsePeriodUs,
	uint32_t *pulTDIPattern, uint32_t *pulTMSPattern, uint32_t *pulTDOPattern)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   uint32_t ulPatternSize;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_JTAG_PULSES;                    // command code
   *((uint32_t *)(pucBuffer + 0x4)) = ulNumberOfPulses;                        // number of pulses
   *((uint32_t *)(pucBuffer + 0x8)) = ulPulsePeriodUs;                         // period of pulses in microseconds
   *((uint32_t *)(pucBuffer + 0xC)) = 0x0;                                     // reserved
   // fill patterns for TDI and TMS
   ulPatternSize = ulNumberOfPulses / 32;
   if (ulNumberOfPulses % 32)
      ulPatternSize++;
   if (pulTDIPattern == NULL)
      memset((void *)(pucBuffer + 0x10), 0x00, ulPatternSize * sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x10), (void *)pulTDIPattern, ulPatternSize * sizeof(uint32_t));
   if (pulTMSPattern == NULL)
      memset((void *)(pucBuffer + 0x10 + ulPatternSize*sizeof(uint32_t)), 0x00, ulPatternSize*sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x10 + ulPatternSize*sizeof(uint32_t)), (void *)pulTMSPattern, ulPatternSize*sizeof(uint32_t));
   iPacketLen = TRANSMIT_LEN(16 + 2*((int32_t)(ulPatternSize*sizeof(uint32_t))));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get result
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pulTDOPattern != NULL))
      memcpy((void *)pulTDOPattern, (void *)(pucBuffer + 0x4), ulPatternSize * sizeof(uint32_t));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagGetPins
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char *pucTDIPin - status of TDI pin
               unsigned char *pucTDOPin - status of TDO pin
               unsigned char *pucTMSPin - status of TMS pin
               unsigned char *pucTCKPin - status of TCK pin
       Output: error code DRVOPXD_ERROR_xxx
  Description: Get current status of JTAG pins.
Date           Initials    Description
13-Dec-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagGetPins(TyDevHandle tyDevHandle, unsigned char *pucTDIPin, unsigned char *pucTDOPin, unsigned char *pucTMSPin, unsigned char *pucTCKPin)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_JTAG_PINS;                   // command code
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get result
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      if (pucTDIPin != NULL)
         *pucTDIPin = (unsigned char)((pucBuffer[0x04]) ? 0x1 : 0x0);
      if (pucTDOPin != NULL)
         *pucTDOPin = (unsigned char)((pucBuffer[0x05]) ? 0x1 : 0x0);
      if (pucTMSPin != NULL)
         *pucTMSPin = (unsigned char)((pucBuffer[0x06]) ? 0x1 : 0x0);
      if (pucTCKPin != NULL)
         *pucTCKPin = (unsigned char)((pucBuffer[0x07]) ? 0x1 : 0x0);
      }
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_JtagScanMultiple
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number
               uint32_t ulNumberOfScans - number of scans
               TyMultipleScanParams *ptyScanParams - array of parameters for each scan
               uint32_t **ppulDataIn - array of pointers to data to be shifted into scanchain
               uint32_t **ppulDataOut - array of pointers to data shifted from scanchain
       Output: error code DRVOPXD_ERROR_xxx
  Description: Multiple access to JTAG registers using optimized way (minimising traffic on USB and 
               increasing performance).
Date           Initials    Description
08-Jan-2008    VH          Initial
****************************************************************************/
TyError DL_OPXD_JtagScanMultiple(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulNumberOfScans, 
                                 TyMultipleScanParams *ptyScanParams, uint32_t **ppulDataIn, uint32_t **ppulDataOut)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   uint32_t ulIndex, ulOffset;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)                          // check core number
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   if (ulNumberOfScans == 0)
      return ERR_NO_ERROR;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_SCAN_MULTIPLE;           // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x06)) = (unsigned short)ulNumberOfScans; // number of scans
   // fill parameters for each scan
   ulOffset = 0x08;
   for (ulIndex=0; ulIndex < ulNumberOfScans; ulIndex++)
      {
      unsigned short usLocWords = (unsigned short)(ptyScanParams[ulIndex].usLength / 32);
      if (ptyScanParams[ulIndex].usLength % 32)
         usLocWords++;
      ptyScanParams[ulIndex].usInReserved = usLocWords;
      ptyScanParams[ulIndex].usOutReserved = usLocWords;
      *((unsigned short *)(pucBuffer + (ulOffset + 0x00))) = ptyScanParams[ulIndex].usLength;   // number of bits to scan
      pucBuffer[ulOffset + 0x02] = (unsigned char)(ptyScanParams[ulIndex].bScanIR ? 0x01 : 0x0);                 // scan type
      pucBuffer[ulOffset + 0x03] = 0x0;                                                         // reserved
      *((unsigned short *)(pucBuffer + ((uint64_t)ulOffset + 0x04))) = (unsigned short)(ptyScanParams[ulIndex].usPreTmsCount & 0x000F);
      *((unsigned short *)(pucBuffer + ((uint64_t)ulOffset + 0x06))) = (unsigned short)(ptyScanParams[ulIndex].usPreTmsBits & 0x0FFF);
      *((unsigned short *)(pucBuffer + ((uint64_t)ulOffset + 0x08))) = (unsigned short)(ptyScanParams[ulIndex].usPostTmsCount & 0x000F);
      *((unsigned short *)(pucBuffer + ((uint64_t)ulOffset + 0x0A))) = (unsigned short)(ptyScanParams[ulIndex].usPostTmsBits & 0x0FFF);
      *((unsigned short *)(pucBuffer + ((uint64_t)ulOffset + 0x0C))) = ptyScanParams[ulIndex].usInReserved;
      *((unsigned short *)(pucBuffer + ((uint64_t)ulOffset + 0x0E))) = ptyScanParams[ulIndex].usOutReserved;
      ulOffset += 0x10;
      }
   // now copy data for all scans
   for (ulIndex=0; ulIndex < ulNumberOfScans; ulIndex++)
      {
      uint32_t ulLocWords = (uint32_t)ptyScanParams[ulIndex].usInReserved;
      if (ppulDataIn[ulIndex] == NULL)
         {
         if (ptyScanParams[ulIndex].bScanIR)
            memset((void *)(pucBuffer + ulOffset), 0xff, ulLocWords * sizeof(uint32_t));
         else
            memset((void *)(pucBuffer + ulOffset), 0x00, ulLocWords * sizeof(uint32_t));
         }
      else
         memcpy((void *)(pucBuffer + ulOffset), (void *)ppulDataIn[ulIndex], ulLocWords * sizeof(uint32_t));
      ulOffset += (ulLocWords * sizeof(uint32_t));
      }
   iPacketLen = TRANSMIT_LEN((int32_t)ulOffset);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get input data
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      ulOffset = 0x04;
      for (ulIndex=0; ulIndex < ulNumberOfScans; ulIndex++)
         {
         uint32_t ulLocWords = (uint32_t)ptyScanParams[ulIndex].usOutReserved;
         if (ppulDataOut[ulIndex] != NULL)
            memcpy((void *)ppulDataOut[ulIndex], (void *)(pucBuffer + ulOffset), ulLocWords * sizeof(uint32_t));
         ulOffset += (ulLocWords * sizeof(uint32_t));
         }
      }
   // synchronization not used
   return (tyResult);
}

/****************************************************************************
*** Diagnostic Support                                                    ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_LedDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char bDiagnosticMode - > 0 to enable diagnostic mode, 0 to disable it
               uint32_t ulLedDiagnosticValue - bitfield with LEDs to turn on/off
                                                    bits  0 to 15 - value for port E
                                                    bits 16 to 31 - value for port F
                                                    if bit==1, LED is off
       Output: error code DRVOPXD_ERROR_xxx
  Description: send LED diagnostic command to Opella (turn on/off selected LEDs)
Date           Initials    Description
11-Dec-2006    VH          Initial
****************************************************************************/
TyError DL_OPXD_LedDiagnosticTest(TyDevHandle tyDevHandle, unsigned char bDiagnosticMode, uint32_t ulLedDiagnosticValue)
{
   unsigned char *pucBuffer;
   unsigned short usPortE, usPortF;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   usPortE =  (unsigned short)(ulLedDiagnosticValue & 0x0000FFFF);
   usPortF = (unsigned short)((ulLedDiagnosticValue & 0xFFFF0000) >> 16);

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_LED;
   *((unsigned short *)(&pucBuffer[4])) = usPortE;
   *((unsigned short *)(&pucBuffer[6])) = usPortF;
   if (bDiagnosticMode)
      *((unsigned char  *)(&pucBuffer[8])) = 1;
   else
      *((unsigned char  *)(&pucBuffer[8])) = 0;
   iPacketLen = TRANSMIT_LEN(9);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
  
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_CpuDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
       Output: error code DRVOPXD_ERROR_xxx
  Description: send CPU diagnostic test command
Date           Initials    Description
09-Oct-2006    VH          Initial
****************************************************************************/
TyError DL_OPXD_CpuDiagnosticTest(TyDevHandle tyDevHandle)
{
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_CPU;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else 
      {     // receive packet and check error code
      if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         switch (*((uint32_t *)(pucBuffer + 0x00)))
            {
            case ERR_NO_ERROR:
               break;
            case ERR_TEST_CPU:
               tyResult = DRVOPXD_ERROR_TEST_CPU;
               break;
            default:
               tyResult = DRVOPXD_ERROR_USB_DRIVER;
               break;
            }
         }
      }
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_FpgaDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
       Output: error code DRVOPXD_ERROR_xxx
  Description: send FPGA diagnostic test command
Date           Initials    Description
03-Aug-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_FpgaDiagnosticTest(TyDevHandle tyDevHandle)
{
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_FPGA;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else 
      {     // receive packet and check error code
      if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_PllDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
       Output: error code DRVOPXD_ERROR_xxx
  Description: send PLL diagnostic test command
Date           Initials    Description
03-Aug-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_PllDiagnosticTest(TyDevHandle tyDevHandle)
{
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_PLL;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else 
      {     // receive packet and check error code
      if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ExtramDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t *pulMemSize - detected memory size
       Output: error code DRVOPXD_ERROR_xxx
  Description: send external RAM diagnostic test command
Date           Initials    Description
09-Oct-2006    VH          Initial
****************************************************************************/
TyError DL_OPXD_ExtramDiagnosticTest(TyDevHandle tyDevHandle, uint32_t *pulMemSize)
{
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   uint32_t ulMemSize = 0;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle; // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_EXTRAM;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else 
      {     // receive packet and check error code
      if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         ulMemSize = *((uint32_t *)(pucBuffer + 0x08));
         }
      }
   if (pulMemSize != NULL)
      *pulMemSize = ulMemSize;

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_TestPerformance
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char bDownload - 0 for upload speed, 1 for download speed
       Output: error code DRVOPXD_ERROR_xxx
  Description: measure download/upload performance using whole transfer buffer
Date           Initials    Description
06-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_TestPerformance(TyDevHandle tyDevHandle, unsigned char bDownload)
{
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   if (bDownload)
      {
      *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_PERFORMANCE_DOWNLOAD;
      *((uint32_t *)(pucBuffer + 0x04)) = BUFFER_PAYLOAD;
      pucBuffer[0x08] = 1;             // use FPGA access
      iPacketLen = TRANSMIT_LEN(9);    // optimized transfer, send only header
      }
   else
      {
      *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_PERFORMANCE_UPLOAD;
      *((uint32_t *)(pucBuffer + 0x04)) = BUFFER_PAYLOAD;
      pucBuffer[0x08] = 1;             // use FPGA access
      iPacketLen = TRANSMIT_LEN(9);
      }
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   if (bDownload)
      {
      iPacketLen = TRANSMIT_LEN(BUFFER_PAYLOAD);
      if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      }
   else
      {
      if (usb_bulk_read(ptyUsbDevHandle, EPD, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      }

   // receive packet
   if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_TpaLoopDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char *pbResult - result of test, 1 for passed, 0 for failure
       Output: error code DRVOPXD_ERROR_xxx
  Description: runs TPA loopback test
Date           Initials    Description
12-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_TpaLoopDiagnosticTest(TyDevHandle tyDevHandle, unsigned char *pbResult)
{
   unsigned char  *pucBuffer;
   int32_t             iPacketLen;
   double          dMeasuredVoltage;
   usb_dev_handle *ptyUsbDevHandle;
   TyError         tyResult = DRVOPXD_ERROR_NO_ERROR;
   TyError         tyRes;

   assert(pbResult != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //check whether target is powered
   tyRes = DL_OPXD_VoltageSense(tyDevHandle, VS_VTPA, &dMeasuredVoltage);
   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
   {
      if (dMeasuredVoltage < 0.4) // target is powered-down
      {
         tyRes = DL_OPXD_SetVtpaVoltage(tyDevHandle, 1.2, FALSE, FALSE); // set fixed voltage
         if (tyRes == DRVOPXD_ERROR_NO_ERROR)
#ifndef __LINUX
            Sleep(1000);
#else
            usleep(1000);
#endif
         if (tyRes == DRVOPXD_ERROR_NO_ERROR)
            tyRes = DL_OPXD_VoltageSense(tyDevHandle, VS_VTPA, &dMeasuredVoltage);
         if (tyRes == DRVOPXD_ERROR_NO_ERROR)
         {
            // check measured voltage (use 10% tolerance from actual value)
            if ((dMeasuredVoltage < 1.1) || (dMeasuredVoltage > 1.3))
            {
               printf(" ERROR adjusting target voltage\n");
               printf("\n");
               tyResult = DRVOPXD_ERROR_USB_DRIVER;
            }
         }

         if (tyRes != DRVOPXD_ERROR_NO_ERROR)
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
      }
   }

   if (tyRes == DRVOPXD_ERROR_NO_ERROR)
   {
      // create packet
      *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_TPA_LOOP;
      iPacketLen = TRANSMIT_LEN(4);
      // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      // receive packet
      if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         if (*((uint32_t *)(pucBuffer + 0x00)) == ERR_NO_ERROR)
            *pbResult = 1;
         else
            *pbResult = 0;
         }
   }

   //always enable tracking voltage
   tyRes = DL_OPXD_SetVtpaVoltage(tyDevHandle, 0.0, TRUE, FALSE);
   if (tyRes != DRVOPXD_ERROR_NO_ERROR)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_TpaInterfaceDiagnosticTest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
       Output: error code DRVOPXD_ERROR_xxx
  Description: runs TPA interface extensive test
               this test requires Opella-XD production test board connencted
               to Opella-XD
Date           Initials    Description
07-Aug-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_TpaInterfaceDiagnosticTest(TyDevHandle tyDevHandle)
{
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   (void)DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;                // get usb handle
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_TEST_TPA_INTERFACE;
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // receive packet
   if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else 
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ConvertDwErrToDrvlayerErr
     Engineer: Vitezslav Hola
        Input: uint32_t ulErrorCode - error code from diskware (api_err.h)
       Output: int32_t - error code DRVOPXD_ERROR_xxx
  Description: convert diskware error code into drvlayer error code
Date           Initials    Description
20-Feb-2007    VH          Initial
23-Dec-2008    SPC         Added two more error return cases
****************************************************************************/
int32_t DL_OPXD_ConvertDwErrToDrvlayerErr(uint32_t ulErrorCode)
{
   int32_t tyResult = DRVOPXD_ERROR_USB_DRIVER;

   // no error is most common case, so we need to eliminate it as fast as possible
   if (ulErrorCode == ERR_NO_ERROR)
      return DRVOPXD_ERROR_NO_ERROR;
   // any error happen, now we have more time to test it

   // check if diagnostic error occurred
   if ((ulErrorCode >= ERR_TEST_ERROR_OFFSET) && (ulErrorCode <= ERR_TEST_ERROR_LAST))
      {
      // eliminate bus errors
      if ((ulErrorCode >= ERR_TEST_FPGA_ADDRESS(0)) && 
          (ulErrorCode <= ERR_TEST_FPGA_ADDRESS(19)))
         tyResult = DRVOPXD_ERROR_TEST_FPGA_ADDRESS((int32_t)ulErrorCode - ERR_TEST_FPGA_ADDRESS(0));
      else if ((ulErrorCode >= ERR_TEST_FPGA_DATA(0)) && 
               (ulErrorCode <= ERR_TEST_FPGA_DATA(15)))
         tyResult = DRVOPXD_ERROR_TEST_FPGA_DATA((int32_t)ulErrorCode - ERR_TEST_FPGA_DATA(0));
      else if ((ulErrorCode >= ERR_TEST_FPGA_XBS(0)) && 
               (ulErrorCode <= ERR_TEST_FPGA_XBS(1)))
         tyResult = DRVOPXD_ERROR_TEST_FPGA_XBS((int32_t)ulErrorCode - ERR_TEST_FPGA_XBS(0));
      else if ((ulErrorCode >= ERR_TEST_EXTRAM_DATA(0)) && 
               (ulErrorCode <= ERR_TEST_EXTRAM_DATA(15)))
         tyResult = DRVOPXD_ERROR_TEST_EXTRAM_DATA((int32_t)ulErrorCode - ERR_TEST_EXTRAM_DATA(0));
      else if ((ulErrorCode >= ERR_TEST_EXTRAM_ADDRESS(0)) && 
               (ulErrorCode <= ERR_TEST_EXTRAM_ADDRESS(22)))
         tyResult = DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS((int32_t)ulErrorCode - ERR_TEST_EXTRAM_ADDRESS(0));
      else if ((ulErrorCode >= ERR_TEST_TPAINT_DIO_P(0)) && 
               (ulErrorCode <= ERR_TEST_TPAINT_DIO_P(9)))
         tyResult = DRVOPXD_ERROR_TEST_TPAINT_DIO_P((int32_t)ulErrorCode - ERR_TEST_TPAINT_DIO_P(0));
      else if ((ulErrorCode >= ERR_TEST_TPAINT_DIO_N(0)) && 
               (ulErrorCode <= ERR_TEST_TPAINT_DIO_N(9)))
         tyResult = DRVOPXD_ERROR_TEST_TPAINT_DIO_N((int32_t)ulErrorCode - ERR_TEST_TPAINT_DIO_N(0));
      else if ((ulErrorCode >= ERR_TEST_TPAINT_FSIO(0)) && 
               (ulErrorCode <= ERR_TEST_TPAINT_FSIO(9)))
         tyResult = DRVOPXD_ERROR_TEST_TPAINT_FSIO((int32_t)ulErrorCode - ERR_TEST_TPAINT_FSIO(0));
      else if ((ulErrorCode >= ERR_TEST_TPAINT_CSIO(0)) && 
               (ulErrorCode <= ERR_TEST_TPAINT_CSIO(1)))
         tyResult = DRVOPXD_ERROR_TEST_TPAINT_CSIO((int32_t)ulErrorCode - ERR_TEST_TPAINT_CSIO(0));
      else
         {  // deal with rest of cases
         switch (ulErrorCode)
            {
            case  ERR_TEST_FPGA_ID :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_ID;
               break;
            case  ERR_TEST_FPGA_XSYSCLK :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_XSYSCLK;
               break;
            case  ERR_TEST_FPGA_INIT :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_INIT;
               break;
            case  ERR_TEST_FPGA_IRQ :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_IRQ;
               break;
            case  ERR_TEST_FPGA_DMADREQ :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_DMADREQ;
               break;
            case  ERR_TEST_FPGA_DMADREQCLR :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_DMADREQCLR;
               break;
            case  ERR_TEST_FPGA_DMATCOUT :
               tyResult = DRVOPXD_ERROR_TEST_FPGA_DMATCOUT;
               break;

            case  ERR_TEST_CPU :
               tyResult = DRVOPXD_ERROR_TEST_CPU;
               break;

            case ERR_TEST_EXTRAM_RANDOM:
               tyResult = DRVOPXD_ERROR_TEST_EXTRAM_RANDOM;
               break;

            case ERR_TEST_TPAINT_LOOP:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_LOOP;
               break;
            case ERR_TEST_TPAINT_ABSENT:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_ABSENT;
               break;
            case ERR_TEST_TPAINT_VTPA:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_VTPA;
               break;
            case ERR_TEST_TPAINT_VTPA_2:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_VTPA_2;
               break;
            case ERR_TEST_TPAINT_VTPA_3V3:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_VTPA_3V3;
               break;
            case ERR_TEST_TPAINT_VTPA_2V5:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_VTPA_2V5;
               break;
            case ERR_TEST_TPAINT_LED:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_LED;
               break;
            case ERR_TEST_TPAINT_VTREF:
               tyResult = DRVOPXD_ERROR_TEST_TPAINT_VTREF;
               break;

            case ERR_TEST_PLL_REFERENCE:
               tyResult = DRVOPXD_ERROR_TEST_PLL_REFERENCE;
               break;
            case ERR_TEST_PLL_JTAGCLK1:
               tyResult = DRVOPXD_ERROR_TEST_PLL_JTAGCLK1;
               break;
            case ERR_TEST_PLL_JTAGCLK2:
               tyResult = DRVOPXD_ERROR_TEST_PLL_JTAGCLK2;
               break;

            default:
               tyResult = DRVOPXD_ERROR_TEST_UNDEFINED;
               break;
            }
         }
      return tyResult;           // return error code
      }

   // check if MIPS specific error occured
   if ((ulErrorCode >= ERR_MIPS_ERROR_OFFSET) && 
       (ulErrorCode <= ERR_MIPS_ERROR_LAST))
      {
      switch (ulErrorCode)
         {
         case ERR_MIPS_EXEDW2_NORMAL_TRAP: //TODO: return proper error for TRAP
            tyResult = DRVOPXD_ERROR_NO_ERROR;
            break;
         // MIPS runtime errors
         case ERR_MIPS_EXEDW2_RESET_OCCURRED:
            tyResult = DRVOPXD_ERROR_TGT_RESET_OCCURRED;
            break;
         case ERR_MIPS_EXEDW2_LOCKUP:
         case ERR_MIPS_EXEDW2_OUTOFBOUNDS:
         case ERR_MIPS_EXEDW2_INVALID_ADDRESS:
         case ERR_MIPS_EXEDW2_NOT_IN_DEBUG_MODE:
         case ERR_MIPS_EXEDW2_LOW_PWR:
         case ERR_MIPS_EXEDW2_HALT_DETECTED:
         case ERR_MIPS_EXEDW2_PROBE_DISABLED:
         //case ERR_MIPS_EXEDW2_NORMAL_TRAP:
         case ERR_MIPS_EXEDW2_BYTE_READ:
         case ERR_MIPS_EXEDW2_HW_READ:
         case ERR_MIPS_EXEDW2_TRIPPLE_READ:
         case ERR_MIPS_EXEDW2_BYTE_WRITE:
         case ERR_MIPS_EXEDW2_HW_WRITE:
         case ERR_MIPS_EXEDW2_TRIPLE_WRITE:
            tyResult = DRVOPXD_ERROR_EXECUTE_DW2;
            break;
         case ERR_MIPS_EXEDW2_DEBUG_EXCEPTION:
            tyResult = DRVOPXD_ERROR_DW2_EXCEPTION;
            break;
         // memory access
         case ERR_MIPS_READ_WORD_STD:
         case ERR_MIPS_READ_WORD_DMA:
            tyResult = DRVOPXD_ERROR_TARGET_READ_MEMORY;
            break;
         case ERR_MIPS_WRITE_WORD_DMA:
            tyResult = DRVOPXD_ERROR_TARGET_WRITE_MEMORY;
            break;
         default:    // other errors
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
            break;
         }
      return tyResult;
      }

  
   // check if ARM specific error occurred
   if ((ulErrorCode >= ERR_ARM_ERROR_OFFSET) && 
       (ulErrorCode <= ERR_ARM_ERROR_LAST))
      {
	   switch (ulErrorCode)
	   {
	   case ERR_ARM_NO_DEBUG_ENTRY_AP:
		   tyResult = DRVOPXD_ERROR_ARM_NO_DEBUG_ENTRY_AP; 
	   	break;
	   case ERR_ARM_NOT_VALID_ROM_ADDR:
			tyResult = DRVOPXD_ERROR_ARM_NOT_VALID_ROM_ADDR;
			break;
	   case ERR_ARM_MAXIMUM_COUNT_REACHED:
		   tyResult = DRVOPXD_ERROR_ARM_DATA_ABORT;
		   break;
	   default:
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
		   break;
	   }
      
      return tyResult;
      }

   // check if specific error occured
   if ((ulErrorCode >= ERR_ARC_ERROR_OFFSET) && 
       (ulErrorCode <= ERR_ARC_ERROR_LAST))
      {
      switch (ulErrorCode)
         {
         case ERR_ARC_TARGET_RESET_OCCURED:
            tyResult = DRVOPXD_ERROR_ARC_TARGET_RESET_OCCURED;
            break;
         case ERR_ARC_ACCESS_FAILED:
         case ERR_ARC_ACCESS_STALLED:
            tyResult = DRVOPXD_ERROR_ARC_DEBUG_ACCESS;
            break;
         case ERR_ARC_ACCESS_PDOWN:
            tyResult = DRVOPXD_ERROR_ARC_DEBUG_PDOWN;
            break;
         case ERR_ARC_ACCESS_POFF:
            tyResult = DRVOPXD_ERROR_ARC_DEBUG_POFF;
            break;
         default:    // other errors
            tyResult = DRVOPXD_ERROR_USB_DRIVER;
            break;
         }
      return tyResult;
      }

   // check if ThreadArch specific error occured
   if ((ulErrorCode >= ERR_TARCH_ERROR_OFFSET) && 
       (ulErrorCode <= ERR_TARCH_ERROR_LAST))
      {
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
      return tyResult;
      }
   
   //riscv errors 
   if ((ulErrorCode >= ERR_RISCV_ERROR_OFFSET) && (ulErrorCode <= ERR_RISCV_ERROR_LAST))
   {
	   switch (ulErrorCode)
	   {
	   case ERR_RISCV_ACCESS_POFF:
		   tyResult = DRVOPXD_ERROR_RISCV_DEBUG_POFF;
		   break;
	   case ERR_RISCV_ABSTRACT_REG_WRITE_BUSY:
		   tyResult = DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUSY;
		   break;
	   case ERR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED:
		   tyResult = DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED;
		   break;
	   case ERR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION:
		   tyResult = DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION;
		   break;
	   case ERR_RISCV_ABSTRACT_REG_WRITE_HART_STATE:
		   tyResult = DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_HART_STATE;
		   break;
	   case ERR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR:
		   tyResult = DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR;
		   break;
	   case ERR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON:
		   tyResult = DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON;
		   break;
	   case ERR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED:
		   tyResult = DRVOPXD_ERROR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED;
		   break;
	   case ERR_RISCV_SYSTEM_BUS_ADDRESS_FAILED:
		   tyResult = DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ADDRESS_FAILED;
		   break;
	   case ERR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED:
		   tyResult = DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED;
		   break;
	   case ERR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED:
		   tyResult = DRVOPXD_ERROR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED;
		   break;
	   case ERR_RISCV_SYSTEM_BUS_OTHER_REASON:
		   tyResult = DRVOPXD_ERROR_RISCV_SYSTEM_BUS_OTHER_REASON;
		   break;
	   case ERR_RISCV_SYSTEM_BUS_MASTER_BUSY:
		   tyResult = DRVOPXD_ERROR_RISCV_SYSTEM_BUS_MASTER_BUSY;
		   break;
	   case ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD:
		   tyResult = DRVOPXD_ERROR_RISCV_MEM_INVALID_LENGTH;
		   break;
	   case ERR_RISCV_ERROR_HALT_TIMEOUT:
		   tyResult = DRVOPXD_ERROR_RISCV_HALT_TIMEOUT;
		   break;
	   case ERR_RISCV_ERROR_RESUME_TIMEOUT:
		   tyResult = DRVOPXD_ERROR_RISCV_RESUME_TIMEOUT;
		   break;
	   case ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE:
		   tyResult = DRVOPXD_ERROR_RISCV_UNSUPPORT_ARCH_TYPE;
		   break;
	   case ERR_RISCV_ERROR_UNSUPPORT_MEMORY_ACCESS_SIZE:
		   tyResult = DRVOPXD_ERROR_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE;
		   break;
	   case ERR_RISCV_ERROR_UNSUPPORT_ABSTRACT_ACCESS_OPERATION:
		   tyResult = DRVOPXD_ERROR_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;
		   break;
	   case ERR_RISCV_ERROR_TRIGGER_INFO_LOOP_TIMEOUT:
		   tyResult = DRVOPXD_ERROR_RISCV_TRIGGER_INFO_LOOP_TIMEOUT;
		   break;
	   case ERR_RISCV_ERROR_DISALE_ABSTRACT_COMMAND:
		   tyResult = DRVOPXD_ERROR_RISCV_DISALE_ABSTRACT_COMMAND;
		   break;
	   case ERR_RISCV_ERROR_NEXT_DEBUG_MODULE_TIMEOUT:
		   tyResult = DRVOPXD_ERROR_RISCV_NEXT_DEBUG_MODULE_TIMEOUT;
		   break;
	   default:
		   break;
	   }
	   return tyResult;
   }

   // deal with rest of errors (general errors)
   switch (ulErrorCode)
      {
      case ERR_ADAPTIVE_CLOCK_TIMEOUT:
         tyResult = DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT;
         break;
      case ERR_I2C_ERROR:
         tyResult = DRVOPXD_ERROR_DEVICE_I2C_ERROR;
         break;
	  case ERR_UNKNOWN_CMD:
		  tyResult = DRVOPXD_ERROR_UNKNOWN_CMD;
		  break;
	  case ERR_DISKWARE_RUNNING:
		  tyResult = DRVOPXD_ERROR_DW_RUNNING;
      default:    // other errors
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
         break;
      }

   return (tyResult);
}

/****************************************************************************
*** Local functions                                                       ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_ConvertDoubleToVoltage
     Engineer: Vitezslav Hola
        Input: double dValue - voltage value as double
       Output: unsigned char - voltage value as unsigned char
  Description: convert voltage value for Opella-XD D/A converter, uses 50 mV resolution
Date           Initials    Description
09-Mar-2007    VH          Initial
****************************************************************************/
static unsigned char DL_OPXD_ConvertDoubleToVoltage(double dValue)
{
   uint32_t ulValue;
   double dStep = (6.6 / 256);
   double dOffset = (6.6 / 512);

   if (dValue <= 0.0)
      return 0x00;
   // round to nearest 50 mV resolution
   ulValue = (uint32_t)(dValue * 20 + 0.5);
   dValue = ((double)ulValue) / 20;
   // convert value
   dValue = (dValue + dOffset) / dStep;

   if (dValue >= 255)
      return 0xFF;
   else
      return ((unsigned char)dValue);
}

/****************************************************************************
     Function: DL_OPXD_ConvertDoubleToVoltage
     Engineer: Vitezslav Hola
        Input: unsigned char ucValue - voltage value as unsigned char
       Output: double - voltage value as double
  Description: convert voltage value for Opella-XD D/A converter, uses 50 mV resolution
Date           Initials    Description
09-Mar-2007    VH          Initial
****************************************************************************/
static double DL_OPXD_ConvertVoltageToDouble(unsigned char ucValue)
{
   uint32_t ulValue;
   double dStep = (6.6 / 256);
   double dOffset = (6.6 / 512);
   double dValue = (double)ucValue;

   // convert value
   dValue = (dValue * dStep) - dOffset;
   // round to nearest 50 mV resolution
   ulValue = (uint32_t)(dValue * 20 + 0.5);
   dValue = ((double)ulValue) / 20;

   if (dValue < 0.0)
      return 0.0;
   else if (dValue >=6.6)
      return 6.6;
   else
      return dValue;
}

//added cJTAG functions
/****************************************************************************
     Function: DL_OPXD_cJtagEscape
     Engineer: Shameerudheen P T
        Input: TyDevHandle tyDevHandle - handle to open device
       Output: error code DRVOPXD_ERROR_xxx
  Description: Issue different Escape sequences (reset, selection, deselectio and custom)
Date           Initials    Description
09-Mar-2012    SPT          Initial
****************************************************************************/
TyError DL_OPXD_cJtagEscape(TyDevHandle tyDevHandle, unsigned char ucEscapeSequence)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_CJTAG_ESCAPE;             // command code
   pucBuffer[4] = ucEscapeSequence;
   
   iPacketLen = TRANSMIT_LEN(5);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_cJtagTwoPartCommand
     Engineer: Shameerudheen P T
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucOpcodeCount - Opcode count
               unsigned char ucOperandCount - Operand count
       Output: error code DRVOPXD_ERROR_xxx
  Description: Issue a Two Part command to a cJTAG TAP.7 Controller
Date           Initials    Description
09-Mar-2012    SPT          Initial
****************************************************************************/
TyError DL_OPXD_cJtagTwoPartCommand(TyDevHandle tyDevHandle, unsigned char ucOpcodeCount, unsigned char ucOperandCount)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_CJTAG_TWO_PART_CMD;             // command code
   pucBuffer[4] = ucOpcodeCount;
   pucBuffer[5] = 0x0;
   pucBuffer[6] = ucOperandCount;
   pucBuffer[7] = 0x0;
   
   iPacketLen = TRANSMIT_LEN(8);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_cJtagThreePartCommand
     Engineer: Shameerudheen P T
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucOpcodeCount - Opcode count
               unsigned char ucOperandCount - Operand count
       Output: error code DRVOPXD_ERROR_xxx
  Description: Issue a Three Part command to a cJTAG TAP.7 Controller
Date           Initials    Description
09-Mar-2012    SPT          Initial
****************************************************************************/
TyError DL_OPXD_cJtagThreePartCommand(TyDevHandle tyDevHandle, unsigned char ucOpcodeCount, unsigned char ucOperandCount,
                                      uint32_t ulCore, uint32_t ulDRLength, uint32_t *pulDataToScan, 
                                      uint32_t *pulDataFromScan)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   uint32_t ulDataWords;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if ((ulDRLength == 0) || (ulDRLength > MAX_SCANCHAIN_LENGTH))  // check scanchain length
      return DRVOPXD_ERROR_JTAG_LENGTH_ERROR;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)                          // check core number
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_CJTAG_THREE_PART_CMD;             // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;                  // core number
   pucBuffer[0x06] = ucOpcodeCount;
   pucBuffer[0x07] = 0;
   pucBuffer[0x08] = ucOperandCount;
   pucBuffer[0x09] = 0;
   if (pulDataFromScan == NULL) 
      pucBuffer[0x0A] = 0;
   else
      pucBuffer[0x0A] = 1;
   pucBuffer[0x0B] = 0;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulDRLength;

   if (ulDRLength % 32)
      ulDataWords = (ulDRLength / 32) + 1;
   else
      ulDataWords = (ulDRLength / 32);
   if (pulDataToScan == NULL)
      memset((void *)(pucBuffer + 0x10), 0x00, ulDataWords * sizeof(uint32_t));
   else
      memcpy((void *)(pucBuffer + 0x10), (void *)pulDataToScan, ulDataWords * sizeof(uint32_t));
   
   iPacketLen = TRANSMIT_LEN(16 + (int32_t)(ulDataWords * sizeof(uint32_t)));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // get data shifted from scanchain if requested
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pulDataFromScan != NULL))
      memcpy((void *)pulDataFromScan, (void *)(pucBuffer + 0x04), ulDataWords * sizeof(uint32_t));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_cJtagInitTap7Controller
     Engineer: Shameerudheen P T
        Input: TyDevHandle tyDevHandle - handle to open device
       Output: error code DRVOPXD_ERROR_xxx
  Description: Initialize TAP.7 Controller
Date           Initials    Description
09-Mar-2012    SPT          Initial
****************************************************************************/
TyError DL_OPXD_cJtagInitTap7Controller(TyDevHandle tyDevHandle)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_CJTAG_INIT_TAP7_CONTROLLER;             // command code
   
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_cJtagChangeScanFormat
     Engineer: Shameerudheen P T
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucScanMode - Scan mode
               unsigned char ucReadyCount - Ready Count
               unsigned char ucDelayCount - Delay Count
       Output: error code DRVOPXD_ERROR_xxx
  Description: Change Scan format
Date           Initials    Description
09-Mar-2012    SPT          Initial
****************************************************************************/
TyError DL_OPXD_cJtagChangeScanFormat(TyDevHandle tyDevHandle, unsigned char ucScanMode,
                                      unsigned char ucReadyCount, unsigned char ucDelayCount)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_CJTAG_CHANGE_SCAN_FORMAT;             // command code
   pucBuffer[4] = ucScanMode;
   pucBuffer[5] = 0x0;
   pucBuffer[6] = ucReadyCount;
   pucBuffer[7] = 0x0;
   pucBuffer[8] = ucDelayCount;
   pucBuffer[9] = 0x0;
   
   iPacketLen = TRANSMIT_LEN(10);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_cJtagSetFPGAtocJTAGMode
     Engineer: Shameerudheen P T
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char uccJTAGMode - FPGA cJTAG mode 1 = cJTAG, 0 = JTAG
       Output: error code DRVOPXD_ERROR_xxx
  Description: Change FPGA cJTAG mode
Date           Initials    Description
01-Jun-2012    SPT          Initial
****************************************************************************/
TyError DL_OPXD_cJtagSetFPGAtocJTAGMode(TyDevHandle tyDevHandle, unsigned char uccJtagMode)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_CJTAG_SET_FPGA_CJTAG_MODE;             // command code
   pucBuffer[4] = uccJtagMode;
   pucBuffer[5] = 0x0;

   iPacketLen = TRANSMIT_LEN(6);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}
