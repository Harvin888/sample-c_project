/******************************************************************************
       Module: drvopxdarm.cpp
     Engineer: Suraj S
  Description: Opella-XD driver layer for ARM
  Date           Initials    Description
  13-Jul-2007    SJ          Initial
  24-Mar-2008	 SJ		     Added 968 core support
  02-Apr-2008    JCK         Modified for performance improvement
  15-Apr-2008    SH          Done code clean up
  02-Nov-2009	 JCK		 Cortex-M3 Support added
******************************************************************************/
#include <stdio.h>   
#include <stdlib.h>
#include <string.h>

#ifndef __LINUX            // Win includes
#include <windows.h>
#else                      // Linux includes
#include <stdarg.h>
#endif

#include "../../protocol/rdi/opxdarmdll/common.h"
#include "../../firmware/export/api_cmd.h"
#include "../../firmware/export/api_err.h"
#include "libusb_dyn.h"
#include "drvopxdarm.h"
#include "drvopxdcom.h"

/****************************************************************************
*** External variables from drvopxd.cpp                                   ***
****************************************************************************/
extern TyDrvDeviceStruct ptyDrvDeviceTable[MAX_DEVICES_SUPPORTED];
extern TyTargetConfig tyTargetConfig;
/****************************************************************************
*** Local function prototypes                                             ***
****************************************************************************/


typedef enum {
   INFO_MESSAGE      = 0,
   ERROR_MESSAGE     = 1,
   HELP_MESSAGE      = 2,
   DEBUG_MESSAGE     = 3,
   LOG_MESSAGE       = 4
} TyMessageType;

void PrintMessage1(TyMessageType tyMessageType, const char * pszToFormat, ...);
#ifdef __LINUX
#define strcmpi			strcasecmp
#endif

/****************************************************************************
*** ARM Specific Functions                                               ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_Arm_ResetProc
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               unsigned char bAssertReset - 1 to assert reset, 0 to deassert. To get 
			   status, this should be 0
               unsigned char *pbCurrStatus - pointer to store current reset status (can be NULL)
			   unsigned char *pbPrevStatus - pointer to store previous reset status (can be NULL)
       Output: error code DRVOPXD_ERROR_xxx
  Description: control reset of all processors on current scanchain (ulCore is ignored)
Date           Initials    Description
13-Jul-2007       SJ        Initial
****************************************************************************/

TyError DL_OPXD_Arm_ResetProc(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bAssertReset, unsigned char *pbCurrStatus, unsigned char *pbPrevStatus)
{
   DLOG("Entering: DL_OPXD_Arm_ResetProc()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_RESET_PROC;             // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;             // core number
   if (bAssertReset)
      pucBuffer[0x6] = 0x1;
   else
      pucBuffer[0x6] = 0x0;

   iPacketLen = TRANSMIT_LEN(7);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if(tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      if(pbCurrStatus != NULL) 
         *pbCurrStatus = pucBuffer[0x04];
      if(pbPrevStatus != NULL)
         *pbPrevStatus = pucBuffer[0x05];
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);

}


/****************************************************************************
     Function: DL_OPXD_Arm_DebugRequest
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               unsigned char ucDebugReq - 1 to assert debug request, 0 to deassert.
       Output: error code DRVOPXD_ERROR_xxx
  Description: control debug request of all processors on current scanchain (ulCore is ignored)
Date           Initials    Description
13-Jul-2007       SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_DebugRequest(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char ucDebugReq)
{
   DLOG("Entering: DL_OPXD_Arm_DebugRequest()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;           // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (ucDebugReq)
      pucBuffer[0x6] = 0x1;
   else
      pucBuffer[0x6] = 0x0;

   iPacketLen = TRANSMIT_LEN(7);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_InitTarget
     Engineer: Roshan T R
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulTargetType - Target type.
       Output: error code DRVOPXD_ERROR_xxx
  Description: control debug request of all processors on current scanchain (ulCore is ignored)
Date           Initials    Description
13-Mar-2010    RTR         Initial
****************************************************************************/
TyError DL_OPXD_Arm_InitTarget(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulTargetType)
{
   DLOG("Entering: DL_OPXD_Arm_InitTarget()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_INIT_TARGET;             // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   *((uint32_t *)(pucBuffer + 0x6)) = (unsigned short)ulTargetType;         // Target type

   iPacketLen = TRANSMIT_LEN(10);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_DebugAcknowledge
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char *pbDebugAck - pointer for debug acknowledge status (can be NULL)
       Output: error code DRVOPXD_ERROR_xxx
  Description: get status of DBGACK pin of ARM processor
Date           Initials    Description
20-Nov-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arm_DebugAcknowledge(TyDevHandle tyDevHandle, unsigned char *pbDebugAck)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_ACKNOWLEDGE;       // command code
   iPacketLen = TRANSMIT_LEN(4);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // get DBGACK status
   if (pbDebugAck != NULL)
      {
      if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pucBuffer[0x04] > 0))
         *pbDebugAck = 1;
      else
         *pbDebugAck = 0;
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_SelectScanChain
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               unsigned char ucScanChainNum - the scan chain to be selected
               unsigned char ucNumIntest - Number of INTEST instructions
               TyArmProcType tyArmType - Processor Type - 0  ARM7, 1 ARM9
       Output: error code DRVOPXD_ERROR_xxx
  Description: Selects the given scan chain
Date           Initials    Description
13-Jul-2007       SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_SelectScanChain(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                    unsigned char ucScanChainNum, unsigned char ucNumIntest, TyArmProcType tyArmType)
{
   DLOG("Entering: DL_OPXD_Arm_SelectScanChain()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   pucBuffer[0x6] = ucScanChainNum;           // scan chain number 
   pucBuffer[0x7] = ucNumIntest;              // number of INTEST instructions 
   pucBuffer[0x8] = (unsigned char)tyArmType; // Processor type - ARM7 or ARM9 

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadICEBreaker
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulRegAddress - address of the ICEBreaker register to read 
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads a register from the ICE Breaker module
Date           Initials    Description
13-Jul-2007       SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadICEBreaker(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                   uint32_t ulRegAddress, uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_ReadICEBreaker()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_ICE_BREAKER;      // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;             // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                  // reserved
   *((uint32_t *)(pucBuffer + 0x8))  = ulRegAddress;                       // register address to read

   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      *pulData = *((uint32_t *)(pucBuffer + 0x04));                         // get word from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_WriteICEBreaker
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulRegAddress - address of the ICEBreaker register to be written 
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to a register in the ICE Breaker module
Date           Initials    Description
13-Jul-2007       SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteICEBreaker(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                   uint32_t ulRegAddress, uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_WriteICEBreaker()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_WRITE_ICE_BREAKER;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;               // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                    // reserved
   *((uint32_t *)(pucBuffer + 0x8))  = ulRegAddress;                         // register address to write
   *((uint32_t *)(pucBuffer + 0xC))  = *pulData;                             // value to be written

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
Function: DL_OPXD_Arm_WriteVectorCacheReg
Engineer: Rejeesh S Babu
Input: TyDevHandle tyDevHandle - handle to open device
uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
uint32_t *pulData - pointer to data
Output: error code DRVOPXD_ERROR_xxx
Description: Writes to VCR register in the Scan Chain 7.
Date           Initials    Description
21-Aug-2008    RS          Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteVectorCacheReg(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,uint32_t *pulData)
{
	DLOG("Entering: DL_OPXD_Arm_WriteVectorCacheReg()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	if (pulData == NULL)
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))	  = CMD_CODE_ARM_WRITE_VCR;                // command code
	*((unsigned short *)(pucBuffer + 0x4))	  = (unsigned short)ulCore;               // core number
	*((unsigned short *)(pucBuffer + 0x6))	  = 0;                                    // reserved
	*((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;
	*((uint32_t *)(pucBuffer + 0x9))	  = *pulData;                            // value to be written
	
	iPacketLen = TRANSMIT_LEN(13);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}

/****************************************************************************
Function: DL_OPXD_Arm_WriteCP14DebugRegARM11
Engineer: Roshan T R
Input: TyDevHandle tyDevHandle - handle to open device
uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
uint32_t *pulData - pointer to data
Output: error code DRVOPXD_ERROR_xxx
Description: Writes to CP14 debug register with supplied reg address
Date           Initials    Description
22-Aug-2009    RTR          Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteCP14DebugRegARM11(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType, uint32_t ulRegAddress, uint32_t *pulData)
{
	DLOG("Entering: DL_OPXD_Arm_WriteCP14DebugRegARM11()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	if (pulData == NULL)
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))	  = CMD_CODE_ARM_WRITE_CP14_REG;                // command code
	*((unsigned short *)(pucBuffer + 0x4))	  = (unsigned short)ulCore;               // core number
	*((unsigned short *)(pucBuffer + 0x6))	  = 0;                                    // reserved
	*((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;
	*((unsigned char *)(pucBuffer + 0x9))   = 0;
	*((unsigned char *)(pucBuffer + 0xA))   = 0;
	*((unsigned char *)(pucBuffer + 0xB))   = 0;
	*((uint32_t *)(pucBuffer + 0xC))	  = ulRegAddress;                            // value to be written
	*((uint32_t *)(pucBuffer + 0x10))	  = *pulData;                            // value to be written


	iPacketLen = TRANSMIT_LEN(20);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_WriteByte
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be written 
               unsigned char *pucData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes a single byte to specified target memory
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteByte(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                              uint32_t ulAddress, unsigned char *pucData)
{
   DLOG("Entering: DL_OPXD_Arm_WriteByte()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pucData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_BYTE;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      //processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                     // address to write
   *((unsigned char *)(pucBuffer + 0x10))  = *pucData;                      // value to be written

   iPacketLen = TRANSMIT_LEN(17);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_WriteWord
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be written 
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes a single word to specified target memory
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteWord(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                              uint32_t ulAddress, uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_WriteWord()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_WORD;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                     // address to write
   *((uint32_t *)(pucBuffer + 0x10))  = *pulData;                      // value to be written

   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_WriteWord_MemoryAP
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be written 
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes a single word to specified target memory
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteWord_MemoryAP(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                              uint32_t ulAddress, uint32_t ulCount, uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_WriteWord_MemoryAP()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_WRITE_MEM_AHB_AP;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                     // address to write
   *((uint32_t *)(pucBuffer + 0x10))  = ulCount;                      // value to be written
   *((uint32_t *)(pucBuffer + 0x14))  = 4;                     // address to write
   *((uint32_t *)(pucBuffer + 0x18))  = *pulData;

   iPacketLen = TRANSMIT_LEN(28);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadWord_MemoryAP
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be written 
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes a single word to specified target memory
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadWord_MemoryAP(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                              uint32_t ulAddress, uint32_t ulCount, uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_ReadWord_MemoryAP()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_READ_MEM_AHB_AP;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                     // address to write
   *((uint32_t *)(pucBuffer + 0x10))  = ulCount;               
   *((uint32_t *)(pucBuffer + 0x14))  = 4;                        

   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
   {
	   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy((void *)pulData, (void *)(pucBuffer + 0x04), (ulCount * sizeof(uint32_t)));
   }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}
/****************************************************************************
     Function: DL_OPXD_Arm_ReadWord
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be read 
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads a single word from specified target memory
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadWord(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                              uint32_t ulAddress, uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_ReadWord()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_WORD;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;     // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                            // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                    // address to read

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      *pulData = *((uint32_t *)(pucBuffer + 0x04));                   // get word from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_CheckForDataAbort
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               unsigned char *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: Checks wheter a data abort has occured or not
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_CheckForDataAbort(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                      TyArmProcType tyArmType, unsigned char *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_CheckForDataAbort()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_CHECK_FOR_DATA_ABORT; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      *pulData = *((unsigned char *)(pucBuffer + 0x04));       // get data from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_WriteMultipleWords
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be written 
               uint32_t *pulData - pointer to data
               uint32_t ulLength - Number of words to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes multiple words to specified target memory onwards
Date           Initials    Description
13-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteMultipleWords(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                                       uint32_t ulAddress, uint32_t *pulData, uint32_t ulLength)
{
   DLOG("Entering: DL_OPXD_Arm_WriteMultipleWords()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   if(ulLength > (BUFFER_PAYLOAD/4))
      return DRVOPXD_ERROR_INVALID_PACKET_SIZE;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_MULTIPLE_WORDS;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                     // address to write
   *((uint32_t *)(pucBuffer + 0x10))  = ulLength;                      // no. of words to write
   //copy the data to be written into transmit buffer
   memcpy(((uint32_t *)(pucBuffer + 0x14)), (uint32_t *)(pulData), (ulLength * sizeof(uint32_t)));

   iPacketLen = TRANSMIT_LEN(20 + ((int32_t)ulLength * 4));

   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_WriteMultipleBlocks
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulAddress - Target address to be written 
               uint32_t *pulData - pointer to data
               uint32_t ulLength - Number of blocks to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes multiple blocks to specified target memory onwards
Date           Initials    Description
13-Jul-2007      SJ        Initial
02-Apr-2008      JCK       Modified for performance improvement
****************************************************************************/
TyError DL_OPXD_Arm_WriteMultipleBlocks(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                                       uint32_t ulAddress, uint32_t *pulData, uint32_t ulLength)
{
   DLOG("Entering: DL_OPXD_Arm_WriteMultipleBlocks()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   if(ulLength > (BUFFER_PAYLOAD/BLOCKSIZE))
      return DRVOPXD_ERROR_INVALID_PACKET_SIZE;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;       // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulAddress;                     // address to write
   *((uint32_t *)(pucBuffer + 0x10))  = ulLength;                      // no. of blocks to write

	if((tyArmType==ARM11)	 || 
	   (tyArmType==CORTEXM) || 
	   ((tyArmType==CORTEXA)))
	{
	   //copy the data to be written into transmit buffer
	   memcpy(((uint32_t *)(pucBuffer + 0x14)), (uint32_t *)(pulData), ((uint64_t)ulLength * BLOCKSIZE));	
	   iPacketLen = TRANSMIT_LEN(20 + (ulLength*BLOCKSIZE));
		// send packet
	   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		  tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		  tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else
		  tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	else
	{	
	   iPacketLen = TRANSMIT_LEN(5 * 4);
	   // send packet
	   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		  tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   iPacketLen = ((int32_t) ulLength * BLOCKSIZE);
	   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)pulData, iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
		  tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		  tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else
		  tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

	   // synchronization not used, assuming single thread uses this function

	}

   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Status_Proc
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               unsigned char ucGlobalScanChain - Global Scan chain number
               unsigned char ucGlobalExecStatus - Global Execution status
               unsigned char *pucStopProblem - ARM Stop problem
               unsigned char *pucExecuting - Processor executing or not
               unsigned char *pucThumb - Processor stopped in Thumb mode or not
               unsigned char *pucWatchPoint - Is the cause of stop a watch point?
       Output: error code DRVOPXD_ERROR_xxx
  Description: Gets the execution status of the processor
Date           Initials    Description
16-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Status_Proc(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                TyArmProcType tyArmType, unsigned char ucGlobalScanChain, unsigned char ucGlobalExecStatus, 
                                unsigned char *pucStopProblem, unsigned char *pucExecuting, unsigned char *pucThumb, unsigned char *pucWatchPoint)
{
   DLOG("Entering: DL_OPXD_Arm_Status_Proc()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if ((pucStopProblem == NULL)||(pucExecuting == NULL)
      ||(pucThumb == NULL)||(pucWatchPoint == NULL))
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_STATUS_PROC; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;   // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                        // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType; // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = ucGlobalScanChain;        // Global scan chain
   *((unsigned char *)(pucBuffer + 0xA))   = ucGlobalExecStatus;       // Global execution status

   iPacketLen = TRANSMIT_LEN(11);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
	  *pucStopProblem = *((unsigned char *)(pucBuffer + 0x04));       // get data from buffer+
      *pucExecuting   = *((unsigned char *)(pucBuffer + 0x05));       // get data from buffer
      *pucThumb       = *((unsigned char *)(pucBuffer + 0x06));       // get data from buffer
      *pucWatchPoint  = *((unsigned char *)(pucBuffer + 0x07));       // get data from buffer
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Change_To_Arm_Mode
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               unsigned char bWatchPoint -
               unsigned char bUserHalt -
               unsigned char *pucWordAligned - Whether the core stopped on a 
               non word aligned address or not
       Output: error code DRVOPXD_ERROR_xxx
  Description: Change the processor mode to ARM
Date           Initials    Description
16-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Change_To_Arm_Mode(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType, 
                                       unsigned char bWatchPoint, unsigned char bUserHalt, unsigned char *pucWordAligned)
{
   DLOG("Entering: DL_OPXD_Arm_Change_To_Arm_Mode()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pucWordAligned == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_CHANGE_TO_ARM_MODE; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                               // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;        // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = bWatchPoint;                     // Watch point
   *((unsigned char *)(pucBuffer + 0xA))   = bUserHalt;                       // User Halt

   iPacketLen = TRANSMIT_LEN(11);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      *pucWordAligned = *((unsigned char *)(pucBuffer + 0x04));       // get data from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Perform_ARM_Nops
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulNoOfOps - Number of NOPs to perform
       Output: error code DRVOPXD_ERROR_xxx
  Description: Perform the specified number of NOP's
Date           Initials    Description
16-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Perform_ARM_Nops(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                     TyArmProcType tyArmType, uint32_t ulNoOfOps)
{
   DLOG("Entering: DL_OPXD_Arm_Perform_ARM_Nops()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_PERFORM_ARM_NOPS; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                             // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = (uint32_t)ulNoOfOps;      // Number of NOPs

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_CleanandDisable1136_Caches
     Engineer: Roshan T R
	  Input:    TyDevHandle tyDevHandle - handle to open device
	            uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)   
               TyProcType - Processor type(ARM11,ARM9,ARM7 etc)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Disable and clean caches
Date           Initials    Description
29-Sep-2009    RTR          Initial
****************************************************************************/
TyError DL_OPXD_CleanandDisable1136_Caches(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType)
{
   DLOG("Entering: DL_OPXD_CleanandDisable1136_Caches()\n");
	
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
	
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
	
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;


	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CLEAN_ARM11_CACHES;    // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;          // core number
	*((unsigned short *)(pucBuffer + 0x6)) = 0;                               // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;

   iPacketLen = TRANSMIT_LEN(9);
	
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
	{
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Instn_Exec_Enable
     Engineer: Roshan T R
	  Input:    TyDevHandle tyDevHandle - handle to open device
	            uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)   
               TyProcType - Processor type(ARM11,ARM9,ARM7 etc)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Enable Arm instruction execution after a halt.
Date           Initials    Description
10-Sep-2009    RTR          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Instn_Exec_Enable(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType)
{
   DLOG("Entering: DL_OPXD_Arm_Instn_Exec_Enable()\n");
	
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
	
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
	
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;


	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_INST_EXE_ENABLE;    // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;          // core number
	*((unsigned short *)(pucBuffer + 0x6)) = 0;                               // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;

   iPacketLen = TRANSMIT_LEN(9);
	
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
	{
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadDebug_Status_Control_Reg
     Engineer: Venkitakrishnan K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)   
               uint32_t *pulData - Read value.            
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read DSCR of ARM 11
Date           Initials    Description
21-jul-2008    VK          Initial

****************************************************************************/
TyError DL_OPXD_Arm_ReadDebug_Status_Control_Reg(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_ReadDebug_Status_Control_Reg()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_DSCR;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                               // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;
                           

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
          *pulData = *((uint32_t *)(pucBuffer + 0x04));                  // get DSCR from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadCPSR
     Engineer: Roshan T R
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)   
               uint32_t *pulData - Read value.            
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read CPSR of ARM 11
Date           Initials    Description
21-Aug-2008    RTR          Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadCPSR(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_ReadCPSR()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_READ_CPSR;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                               // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;
                           

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
          *pulData = *((uint32_t *)(pucBuffer + 0x04));                  // get DSCR from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Arm_ReadCDCR
     Engineer: Roshan T R
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)   
               uint32_t *pulData - Read value.            
       Output: error code DRVOPXD_ERROR_xxx
  Description: Read CDCR of ARM 11
Date           Initials    Description
21-Aug-2008    RTR          Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadCDCR(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_ReadCDCR()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_READ_CDCR;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                               // reserved
   *((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;
   *((unsigned char *)(pucBuffer + 0x9))  = (unsigned char)tyTargetConfig.tyProcType;                    

   iPacketLen = TRANSMIT_LEN(10);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
          *pulData = *((uint32_t *)(pucBuffer + 0x04));                  // get DSCR from buffer
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Enter_Debug_State
     Engineer: Rejeesh S Babu
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)  
               uint32_t *pulData -Data from Diskware
               uint32_t ulArraySize - Size of data                     
       Output: error code DRVOPXD_ERROR_xxx
  Description: To change mode to Debug mode for ARM11 and Cortex core.
Date           Initials    Description
31-Jul-2008    RS          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Enter_Debug_State(TyDevHandle tyDevHandle, uint32_t ulCore,TyArmProcType tyArmType,uint32_t *pulData, uint32_t ulArraySize)
{
   DLOG("Entering: DL_OPXD_Arm_Enter_Debug_State()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;               // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                    // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;
   *((unsigned char *)(pucBuffer + 0x9))   = (unsigned char)tyTargetConfig.tyProcType;
   
   memcpy(((unsigned char *)(pucBuffer + 0xC)), (const void *)pulData, ulArraySize);

   iPacketLen = TRANSMIT_LEN(12 + ulArraySize);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
          {
		  if (CORTEXA == tyArmType)
              {
			  *pulData = *(uint32_t*)(pucBuffer + 0x04);
              }
		  else 
              {
			  memcpy((void *)pulData, (void *)(pucBuffer + 0x04),ulArraySize);
              }
          }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
Function: DL_OPXD_Arm_Leave_Debug_State
Engineer: Rejeesh S Babu
Input: TyDevHandle tyDevHandle - handle to open device
uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)  
uint32_t *pulData -Data to Diskware(wDTR,R0,rDTR)
uint32_t ulArraySize - Size of data        
Output: error code DRVOPXD_ERROR_xxx
Description: Leave Debug state for ARM 11 core only.
Date           Initials    Description
31-Jul-2008    RS          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Leave_Debug_State(TyDevHandle tyDevHandle,
                                      uint32_t ulCore,
									  TyArmProcType tyArmType,
									  unsigned char uchThumb,
                                      uint32_t *pulData,
									  uint32_t ulArraySize)
{
	DLOG("Entering: DL_OPXD_Arm_Leave_Debug_State()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	if (pulData == NULL)
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;               // core number
	*((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)0;                                    // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;             // set if THUMB mode
	*((unsigned char *)(pucBuffer + 0x9))  = (unsigned char)uchThumb;              // set if THUMB mode
	*((unsigned char *)(pucBuffer + 0xA))  = (unsigned char)tyTargetConfig.tyProcType;
	*((unsigned char *)(pucBuffer + 0xB))  = 0x0;
	memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(pulData), (ulArraySize));//wDTR,R0,rDTR
	iPacketLen = TRANSMIT_LEN(14 + (int32_t)ulArraySize); 
	
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	//free(pucBuffer);
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadRegisters
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType -  Processor type - 0  ARM7, 1 ARM9
               uint32_t *pulRegValues - Pointer to register values
               uint32_t ulRegArraySize - Size of the register array to read
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads the ARM processor registers in all modes
Date           Initials    Description
17-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadRegisters(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                  TyArmProcType tyArmType, uint32_t *pulRegValues, uint32_t ulRegArraySize)
{
   DLOG("Entering: DL_OPXD_Arm_ReadRegisters()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulRegValues == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_REGISTERS; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;      // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                           // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;    // processor type

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy((void *)pulRegValues, (void *)(pucBuffer + 0x04), ulRegArraySize);
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadMultipleBlocks
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulStartAddress - start address to read blocks
               uint32_t *pulData - pointer to read data
               uint32_t ulLength - number of blocks to read
       Output: error code DRVOPXD_ERROR_xxx
  Description: read multiple blocks of data from target memory
Date           Initials    Description
18-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadMultipleBlocks(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                                       uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength)
{
   DLOG("Entering: DL_OPXD_Arm_ReadMultipleBlocks()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   if(ulLength > (BUFFER_PAYLOAD/BLOCKSIZE))
      return DRVOPXD_ERROR_INVALID_PACKET_SIZE;


   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_MULTIPLE_BLOCKS;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                   // reserved
   *((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;            // processor type
   *((unsigned char *)(pucBuffer + 0x9))  = 0;                                   // reserved
   *((unsigned short *)(pucBuffer + 0xA)) = 0;                                   // reserved
   *((uint32_t *)(pucBuffer + 0xC))  = ulStartAddress;                      // address to read
   *((uint32_t *)(pucBuffer + 0x10)) = ulLength;                            // number of blocks
   
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      memcpy((void *)pulData, (void *)(pucBuffer + 0x04), ((uint64_t)ulLength * BLOCKSIZE));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ReadMultipleWords
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulStartAddress - start address to read words
               uint32_t *pulData - pointer to read data
               uint32_t ulLength - number of words to read
       Output: error code DRVOPXD_ERROR_xxx
  Description: read multiple words from target memory
Date           Initials    Description
18-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ReadMultipleWords(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                                      uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength)
{
   DLOG("Entering: DL_OPXD_Arm_ReadMultipleWords()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   if(ulLength > (BUFFER_PAYLOAD/BLOCKSIZE))
      return DRVOPXD_ERROR_INVALID_PACKET_SIZE;


   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_MULTIPLE_WORDS;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;             // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                  // reserved
   *((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;           // processor type
   *((unsigned char *)(pucBuffer + 0x9))  = 0;                                  // reserved
   *((unsigned short *)(pucBuffer + 0xA)) = 0;                                  // reserved
   *((uint32_t *)(pucBuffer + 0xC))  = ulStartAddress;                     // address to read
   *((uint32_t *)(pucBuffer + 0x10)) = ulLength;                           // number of words
   
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy((void *)pulData, (void *)(pucBuffer + 0x04), (ulLength * sizeof(uint32_t)));
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_SetupExecuteProc
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t *pulData - pointer to WP reister values
       Output: error code DRVOPXD_ERROR_xxx
  Description: Setup the ICE Breaker Watch point registers
Date           Initials    Description
19-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_SetupExecuteProc(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType,
                                     uint32_t *pulData)
{
   DLOG("Entering: DL_OPXD_Arm_SetupExecuteProc()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulData == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_SETUP_EXECUTE_PROC;    // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;             // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                  // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;           // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                                  // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                                  // reserved
   
	if(tyArmType != ARM11)
	{
		//copy the data to be written into transmit buffer
		memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(pulData), (MAX_WP_REG * 4));

		iPacketLen = TRANSMIT_LEN(12 + (MAX_WP_REG * 4));
	}
	else
	{
		//copy the data to be written into transmit buffer
		memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(pulData), (MAX_BP_REG_ARM11 * 4));
		
		iPacketLen = TRANSMIT_LEN(12 + (MAX_BP_REG_ARM11 * 4));
	}

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_SetupHardBreakRes
     Engineer: Jeenus C.K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type
			   uint32_t ulNumHardBreakPoints - Number of hardware breakpoint resources
               uint32_t *pulData - pointer to HB point reister values
       Output: error code DRVOPXD_ERROR_xxx
  Description: In order to setup the Flash patch comparator register
Date           Initials    Description
03-Nov-2009    JCK        Initial
****************************************************************************/
TyError DL_OPXD_Arm_SetupHardBreakRes(
	TyDevHandle tyDevHandle, 
	uint32_t ulCore, 
    TyArmProcType tyArmType,
	uint32_t ulNumHardBreakPoints,
	uint32_t ulNumHardBreakData,
    uint32_t *pulData)
{
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	if (pulData == NULL)
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	// create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_SETUP_HARD_BREAK;         // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;                // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                     // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;              // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                                     // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = (unsigned short)ulNumHardBreakPoints;  // reserved

   //copy the data to be written into transmit buffer
	memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(pulData), ulNumHardBreakData);

	iPacketLen = TRANSMIT_LEN(12 + ulNumHardBreakData);

	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}

	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_SetupHardWatchRes
     Engineer: Jeenus C.K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type
			   uint32_t ulNumHardWatchPoints - Number of watch point resources
               uint32_t *pulData - pointer to Watch point reister values
       Output: error code DRVOPXD_ERROR_xxx
  Description: In order to setup the DWT comparator, mask and function registers
Date           Initials    Description
03-Nov-2009      JCK        Initial
****************************************************************************/
TyError DL_OPXD_Arm_SetupHardWatchRes(
	TyDevHandle tyDevHandle, 
	uint32_t ulCore, 
    TyArmProcType tyArmType,
	uint32_t ulNumHardWatchPoints,
	uint32_t ulNumHardWatchPointsData,
    uint32_t *pulData)

{
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	if (pulData == NULL)
		return DRVOPXD_ERROR_MEMORY_ALLOC;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	// create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_SETUP_WATCH_POINT;        // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;                // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                     // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;              // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                                     // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = (unsigned short)ulNumHardWatchPoints;  // reserved

   //copy the data to be written into transmit buffer
	memcpy(((uint32_t *)(pucBuffer + 0x10)), (uint32_t *)(pulData), (ulNumHardWatchPointsData));

	iPacketLen = TRANSMIT_LEN(12 + (ulNumHardWatchPointsData));

	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}

	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Config_VectorCatch
     Engineer: Jeenus C.K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type
			   uint32_t ulVectorCatchData - Vector catch data.
       Output: error code DRVOPXD_ERROR_xxx
  Description: In order to setup the vector catch.
Date           Initials    Description
21-Apr-2010    JCK        Initial
****************************************************************************/
TyError DL_OPXD_Config_VectorCatch (TyDevHandle tyDevHandle, 
									uint32_t ulCore, 
									TyArmProcType tyArmType,
									uint32_t ulVectorCatchData)
{
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	// create packet
	*((uint32_t  *)(pucBuffer + 0x0))  = CMD_CODE_CONFIG_VECTORCATCH;        // command code
	*((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;                // core number
	*((unsigned short *)(pucBuffer + 0x6))  = 0;                                     // reserved
	*((unsigned char  *)(pucBuffer + 0x8))  = (unsigned char)tyArmType;              // processor type
	*((unsigned char  *)(pucBuffer + 0x9))  = 0;                                     // reserved
	*((unsigned short *)(pucBuffer + 0xA))  = 0;                                     // reserved
    *((uint32_t  *)(pucBuffer + 0xC))  = (uint32_t)ulVectorCatchData;  // Vector catch data

	iPacketLen = TRANSMIT_LEN(0x10);	
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	
	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_CortexACacheFlush
     Engineer: Jeenus C.K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCacheCleanStartAddr - Cache clean start address
			   uint32_t ulStartAddr - Start address of memory
               uint32_t ulEndAddr - End address of memory.
       Output: error code DRVOPXD_ERROR_xxx
  Description: In order to invalidate and clean the cortex A8 cache.
Date           Initials    Description
14-May-2010    JCK        Initial
****************************************************************************/
TyError DL_OPXD_Arm_CortexACacheFlush ( TyDevHandle tyDevHandle, 
									    uint32_t ulCore,
									    uint32_t ulCacheCleanStartAddr,
									    uint32_t ulStartAddr,
										uint32_t ulEndAddr)

{
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t  *)(pucBuffer + 0x0))  = CMD_CODE_CORTEXA_CLEAN_CACHE; // command code
	*((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number
	*((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
	*((uint32_t  *)(pucBuffer + 0x8))  = ulCacheCleanStartAddr;		// ulCacheCleanStartAddr
	*((uint32_t  *)(pucBuffer + 0xC))  = ulStartAddr;                  // ulStartAddr
	*((uint32_t *)(pucBuffer + 0x10))  = ulEndAddr;                    // ulEndAddr

	iPacketLen = TRANSMIT_LEN(0x14);	
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	
	return(tyResult);


}

/****************************************************************************
     Function: DL_OPXD_InitIcepick
     Engineer: Jeenus C.K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCacheCleanStartAddr - Cache clean start address
			   uint32_t ulStartAddr - Start address of memory
               uint32_t ulEndAddr - End address of memory.
       Output: error code DRVOPXD_ERROR_xxx
  Description: In order to invalidate and clean the cortex A8 cache.
Date           Initials    Description
14-May-2010    JCK        Initial
****************************************************************************/
TyError  DL_OPXD_InitIcepick( TyDevHandle tyDevHandle,
							  uint32_t ulCore,
							  uint32_t ulTISubPortNum )
{
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t  *)(pucBuffer + 0x0))  = CMD_CODE_INIT_ICEPICK; // command code
	*((uint32_t  *)(pucBuffer + 0x4))  = ulCore;		// ulTISubPortNum
	*((uint32_t  *)(pucBuffer + 0x8))  = ulTISubPortNum;		// ulTISubPortNum

	iPacketLen = TRANSMIT_LEN(0xC);	
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	{
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	}
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	}
	
	return(tyResult);
}
/****************************************************************************
     Function: DL_OPXD_Arm_WriteRegisters
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t *pulRegValues - Pointer to register values 
               uint32_t ulRegArraySize - Size of the register array to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes multiple words to specified target memory onwards
Date           Initials    Description
19-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_WriteRegisters(TyDevHandle tyDevHandle, 
                                   uint32_t ulCore, 
                                   TyArmProcType tyArmType,
                                   uint32_t *pulRegValues, 
                                   uint32_t ulRegArraySize)
{
   DLOG("Entering: DL_OPXD_Arm_WriteRegisters()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   if (pulRegValues == NULL)
      return DRVOPXD_ERROR_MEMORY_ALLOC;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_REGISTERS;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;        // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;      // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0xa))   = 0;                             // reserved
   *((unsigned char *)(pucBuffer + 0xb))   = 0;                             // reserved
   memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(pulRegValues), (ulRegArraySize));
   iPacketLen = TRANSMIT_LEN(12 + (int32_t)ulRegArraySize); //160

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ExecuteProc
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               unsigned char bThumbMode - 1 -> THUMB mode, 0 -> ARM mode
               uint32_t ulR0Value - Register R0 value
       Output: error code DRVOPXD_ERROR_xxx
  Description: Setup the proccessor for execution
Date           Initials    Description
20-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ExecuteProc(TyDevHandle tyDevHandle, 
                                uint32_t ulCore, 
                                TyArmProcType tyArmType,
                                unsigned char bThumbMode, 
                                uint32_t ulR0Value)
{
   DLOG("Entering: DL_OPXD_Arm_ExecuteProc()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_EXECUTE_PROC;    // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;     // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                            // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                            // reserved
   *((unsigned char *)(pucBuffer + 0xC))   = bThumbMode;                   // thumb or arm mode
   *((unsigned char *)(pucBuffer + 0xD))   = 0;                            // reserved
   *((unsigned short *)(pucBuffer + 0xE))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0x10))  = ulR0Value;                    // R0 value

   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_SetSafeNonVectorAddress
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulSafeNonVectorAddress - Safe non vector address value
       Output: error code DRVOPXD_ERROR_xxx
  Description: Sets the safe non vector address
Date           Initials    Description
20-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_SetSafeNonVectorAddress(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore, 
                                            TyArmProcType tyArmType,
                                            uint32_t ulSafeNonVectorAddress,
                                            boolean bIsEnabled)
{
   DLOG("Entering: DL_OPXD_Arm_SetSafeNonVectorAddress()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_SET_SAFE_NON_VECTOR_ADDRESS;   // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;     // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = bIsEnabled;                   // to enable or not
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulSafeNonVectorAddress;       // safe non vector address

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Change_To_Thumb_Mode
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t ulReg0 - Register 0 value
               uint32_t ulRegPC - Register PC value
       Output: error code DRVOPXD_ERROR_xxx
  Description: Change the processor mode to THUMB
Date           Initials    Description
23-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Change_To_Thumb_Mode(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType, 
                                         uint32_t ulReg0, uint32_t ulRegPC)
{
   DLOG("Entering: DL_OPXD_Arm_Change_To_Thumb_Mode()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_CHANGE_TO_THUMB_MODE; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;            // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                 // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;                         // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                                  // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                                 // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulReg0;                            // Register0 value
   *((uint32_t *)(pucBuffer + 0x10))  = ulRegPC;                           // RegisterPC value

   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_ResetDap
     Engineer: Jeenus Chalattu Kunnath 
        Input: TyDevHandle tyDevHandle - handle to open device               
       Output: error code DRVOPXD_ERROR_xxx
  Description: reset JTAG Dap on scanchain
Date           Initials    Description
11-Dec-2009    JCK          Initial
****************************************************************************/
TyError DL_OPXD_ResetDap(TyDevHandle tyDevHandle)
{
	DLOG("Entering: DL_OPXD_ResetDap()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;	
	
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_RESET_DAP; // command code	
	
	iPacketLen = TRANSMIT_LEN(4);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Restart
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Sets TAP state machine to RESTART
Date           Initials    Description
23-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Restart(TyDevHandle tyDevHandle, uint32_t ulCore)
{
   DLOG("Entering: DL_OPXD_Arm_Restart()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_RESTART;      // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;    // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM720CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
               uint32_t ulC2_TTB - C2 Translation Table Base Register
               uint32_t ulC3_DAC - C3 Domain Access Control Register
               uint32_t ulC5_FSR - C5 Fault Status Register
               uint32_t ulC6_FAR - C6 Fault Address Register
               uint32_t ulC13_PID - C13 Process ID Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM720 Co Processor registers
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM720CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC2_TTB,
                                             uint32_t ulC3_DAC,
                                             uint32_t ulC5_FSR,
                                             uint32_t ulC6_FAR,
                                             uint32_t ulC13_PID)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM720CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM720CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulC1_Control;                     // C1 Control Register value
   *((uint32_t *)(pucBuffer + 0xC))   = ulC2_TTB;                         // C2 Translation Table Base Register value
   *((uint32_t *)(pucBuffer + 0x10))  = ulC3_DAC;                         // C3 Domain Access Control Register value
   *((uint32_t *)(pucBuffer + 0x14))  = ulC5_FSR;                         // C5 Fault Status Register value
   *((uint32_t *)(pucBuffer + 0x18))  = ulC6_FAR;                         // C6 Fault Address Register value
   *((uint32_t *)(pucBuffer + 0x1C))  = ulC13_PID;                        // C13 Process ID Register value

   iPacketLen = TRANSMIT_LEN(32);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM740CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
               uint32_t ulC2_TTB - C2 Translation Table Base Register
               uint32_t ulC3_DAC - C3 Domain Access Control Register
               uint32_t ulC5_FSR - C5 Fault Status Register
               uint32_t ulC6_FAR - C6 Fault Address Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM740 Co Processor registers
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM740CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC2_TTB,
                                             uint32_t ulC3_DAC,
                                             uint32_t ulC5_FSR,
                                             uint32_t ulC6_FAR)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM740CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM740CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulC1_Control;                     // C1 Control Register value
   *((uint32_t *)(pucBuffer + 0xC))   = ulC2_TTB;                         // C2 Translation Table Base Register value
   *((uint32_t *)(pucBuffer + 0x10))  = ulC3_DAC;                         // C3 Domain Access Control Register value
   *((uint32_t *)(pucBuffer + 0x14))  = ulC5_FSR;                         // C5 Fault Status Register value
   *((uint32_t *)(pucBuffer + 0x18))  = ulC6_FAR;                         // C6 Fault Address Register value

   iPacketLen = TRANSMIT_LEN(28);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM920_922CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
               uint32_t ulC2_D_CacheTTB - C2 D Cache Translation Table Base Register
               uint32_t ulC2_I_CacheTTB - C2 I Cache Translation Table Base Register
               uint32_t ulC3_D_CacheDAC - C3 D Cache Domain Access Control Register
               uint32_t ulC3_I_CacheDAC - C3 I Cache Domain Access Control Register
               uint32_t ulC5_D_CacheFSR - C5 D Cache Fault Status Register
               uint32_t ulC5_I_CacheFSR - C5 I Cache Fault Status Register
               uint32_t ulC6_D_CacheFAR - C6 D Cache Fault Address Register
               uint32_t ulC6_I_CacheFAR - C6 I Cache Fault Address Register
               uint32_t ulC9_D_Cache_L_Down - C9 D Cache Lock Down Register
               uint32_t ulC9_I_Cache_L_Down - C9 I Cache Lock Down Register
               uint32_t ulC10_D_CacheTLB_L_Down - C10 D Cache TLB Lock Down Register
               uint32_t ulC10_I_CacheTLB_L_Down - C10 I Cache TLB Lock Down Register
               uint32_t ulC13_ProcessID - C13 Process ID Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM920/922 Co Processor registers
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM920_922CP_Registers(TyDevHandle tyDevHandle, 
                                                 uint32_t ulCore,
                                                 unsigned char       bRestoreInterpretedModeRegisters,
                                                 uint32_t ulC1_Control,
                                                 uint32_t ulC2_D_CacheTTB,
                                                 uint32_t ulC2_I_CacheTTB,
                                                 uint32_t ulC3_D_CacheDAC,
                                                 uint32_t ulC3_I_CacheDAC,
                                                 uint32_t ulC5_D_CacheFSR,
                                                 uint32_t ulC5_I_CacheFSR,
                                                 uint32_t ulC6_D_CacheFAR,
                                                 uint32_t ulC6_I_CacheFAR,
                                                 uint32_t ulC9_D_Cache_L_Down,
                                                 uint32_t ulC9_I_Cache_L_Down,
                                                 uint32_t ulC10_D_CacheTLB_L_Down,
                                                 uint32_t ulC10_I_CacheTLB_L_Down,
                                                 uint32_t ulC13_ProcessID)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM920_922CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM920_922CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;               // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                    // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)bRestoreInterpretedModeRegisters;                     
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                        // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                        // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulC1_Control;             // C1 Control Register value
   *((uint32_t *)(pucBuffer + 0x10))  = ulC2_D_CacheTTB;          // C2 D Cache Translation Table Base Register value
   *((uint32_t *)(pucBuffer + 0x14))  = ulC2_I_CacheTTB;          // C2 I Cache Translation Table Base Register value
   *((uint32_t *)(pucBuffer + 0x18))  = ulC3_D_CacheDAC;          // C3 D Cache Domain Access Control Register value
   *((uint32_t *)(pucBuffer + 0x1C))  = ulC3_I_CacheDAC;          // C3 I Cache Domain Access Control Register value
   *((uint32_t *)(pucBuffer + 0x20))  = ulC5_D_CacheFSR;          // C5 D Cache Fault Status Register value
   *((uint32_t *)(pucBuffer + 0x24))  = ulC5_I_CacheFSR;          // C5 I Cache Fault Status Register value
   *((uint32_t *)(pucBuffer + 0x28))  = ulC6_D_CacheFAR;          // C6 D Cache Fault Address Register value
   *((uint32_t *)(pucBuffer + 0x2C))  = ulC6_I_CacheFAR;          // C6 I Cache Fault Address Register value
   *((uint32_t *)(pucBuffer + 0x30))  = ulC9_D_Cache_L_Down;      // C9 D Cache Lock Down Register value               
   *((uint32_t *)(pucBuffer + 0x34))  = ulC9_I_Cache_L_Down;      // C9 I Cache Lock Down Register value               
   *((uint32_t *)(pucBuffer + 0x38))  = ulC10_D_CacheTLB_L_Down;  // C10 D Cache TLB Lock Down Register value              
   *((uint32_t *)(pucBuffer + 0x3C))  = ulC10_I_CacheTLB_L_Down;  // C10 I Cache TLB Lock Down Register value             
   *((uint32_t *)(pucBuffer + 0x40))  = ulC13_ProcessID;          // C13 Process ID Register value               

   iPacketLen = TRANSMIT_LEN(68);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM926CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
               uint32_t ulC2_TTB     - C2 Translation Table Base Register
               uint32_t ulC3_Domain_Access_Control - C3 Domain Access Control Register
               uint32_t ulC5_D_FSR - C5 D Cache Fault Status Register
               uint32_t ulC5_I_FSR - C5 I Cache Fault Status Register
               uint32_t ulC6_FAR - C6 Fault Address Register
               uint32_t ulC9_D_Lock_Down - C9 D Cache Lock Down Register
               uint32_t ulC9_I_Lock_Down - C9 I Cache Lock Down Register
               uint32_t ulC9_D_TCM - C9 D Cache TCM Register
               uint32_t ulC9_I_TCM - C9 I Cache TCM Register
               uint32_t ulC10_TLB_LockDown - C10 TLB Lock Down Register
               uint32_t ulC13_ProcessID - C13 Process ID Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM926 Co Processor registers
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM926CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC2_TTB,
                                             uint32_t ulC3_Domain_Access_Control,
                                             uint32_t ulC5_D_FSR,
                                             uint32_t ulC5_I_FSR,
                                             uint32_t ulC6_FAR,
                                             uint32_t ulC9_D_Lock_Down,
                                             uint32_t ulC9_I_Lock_Down,
                                             uint32_t ulC9_D_TCM,
                                             uint32_t ulC9_I_TCM,
                                             uint32_t ulC10_TLB_LockDown,
                                             uint32_t ulC13_PID)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM926CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM926CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulC1_Control;                 // C1 Control Register value
   *((uint32_t *)(pucBuffer + 0xC))   = ulC2_TTB;                     // C2 Translation Table Base Register value
   *((uint32_t *)(pucBuffer + 0x10))  = ulC3_Domain_Access_Control;   // C3 Domain Access Control Register value
   *((uint32_t *)(pucBuffer + 0x14))  = ulC5_D_FSR;                   // C5 D Cache Fault Status Register value
   *((uint32_t *)(pucBuffer + 0x18))  = ulC5_I_FSR;                   // C5 I Cache Fault Status Register value
   *((uint32_t *)(pucBuffer + 0x1C))  = ulC6_FAR;                     // C6 Fault Address Register value
   *((uint32_t *)(pucBuffer + 0x20))  = ulC9_D_Lock_Down;             // C9 D Cache Lock Down Register value
   *((uint32_t *)(pucBuffer + 0x24))  = ulC9_I_Lock_Down;             // C9 I Cache Lock Down Register value
   *((uint32_t *)(pucBuffer + 0x28))  = ulC9_D_TCM;                   // C9 D Cache TCM Register value
   *((uint32_t *)(pucBuffer + 0x2C))  = ulC9_I_TCM;                   // C9 I Cache TCM Register value
   *((uint32_t *)(pucBuffer + 0x30))  = ulC10_TLB_LockDown;           // C10 TLB Lock Down Register value               
   *((uint32_t *)(pucBuffer + 0x34))  = ulC13_PID;                    // C13 Process ID Register value               

   iPacketLen = TRANSMIT_LEN(56);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM940_946CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
               uint32_t ulC2_D_CacheConfiguration - C2 D Cache Configuration Register
               uint32_t ulC2_I_CacheConfiguration - C2 I Cache Configuration Register
               uint32_t ulC3_WriteBufferControl - C3 Write Buffer Control Register
               uint32_t ulC5_D_AccessPermission - C5 D Cache Access Permission Register
               uint32_t ulC5_I_AccessPermission - C5 I Cache Access Permission Register
               uint32_t ulC6_ProtectionRegion0 - C6 Protection Region0 Register
               uint32_t ulC6_ProtectionRegion1 - C6 Protection Region1 Register
               uint32_t ulC6_ProtectionRegion2 - C6 Protection Region2 Register
               uint32_t ulC6_ProtectionRegion3 - C6 Protection Region3 Register
               uint32_t ulC6_ProtectionRegion4 - C6 Protection Region4 Register
               uint32_t ulC6_ProtectionRegion5 - C6 Protection Region5 Register
               uint32_t ulC6_ProtectionRegion6 - C6 Protection Region6 Register
               uint32_t ulC6_ProtectionRegion7 - C6 Protection Region7 Register
               uint32_t ulC9_D_CacheTLB_L_Down - C9 D Cache TLB Lock Down Register
               uint32_t ulC9_I_CacheTLB_L_Down - C9 I Cache TLB Lock Down Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM940/946 Co Processor registers
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM940_946CP_Registers(TyDevHandle tyDevHandle, 
                                                 uint32_t ulCore,
                                                 uint32_t ulC1_Control,
                                                 uint32_t ulC2_D_CacheConfiguration,
                                                 uint32_t ulC2_I_CacheConfiguration,
                                                 uint32_t ulC3_WriteBufferControl,
                                                 uint32_t ulC5_D_AccessPermission,
                                                 uint32_t ulC5_I_AccessPermission,
                                                 uint32_t ulC6_ProtectionRegion0,
                                                 uint32_t ulC6_ProtectionRegion1,
                                                 uint32_t ulC6_ProtectionRegion2,
                                                 uint32_t ulC6_ProtectionRegion3,
                                                 uint32_t ulC6_ProtectionRegion4,
                                                 uint32_t ulC6_ProtectionRegion5,
                                                 uint32_t ulC6_ProtectionRegion6,
                                                 uint32_t ulC6_ProtectionRegion7,
                                                 uint32_t ulC9_D_CacheTLB_L_Down,
                                                 uint32_t ulC9_I_CacheTLB_L_Down)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM940_946CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM940_946CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulC1_Control;                 // C1 Control Register value
   *((uint32_t *)(pucBuffer + 0xC))   = ulC2_D_CacheConfiguration;    // C2 D Cache Configuration Register value
   *((uint32_t *)(pucBuffer + 0x10))  = ulC2_I_CacheConfiguration;    // C2 I Cache Configuration Register value
   *((uint32_t *)(pucBuffer + 0x14))  = ulC3_WriteBufferControl;      // C3 Write Buffer Control Register value
   *((uint32_t *)(pucBuffer + 0x18))  = ulC5_D_AccessPermission;      // C5 D Cache Access Permission Register value
   *((uint32_t *)(pucBuffer + 0x1C))  = ulC5_I_AccessPermission;      // C5 I Cache Access Permission Register value
   *((uint32_t *)(pucBuffer + 0x20))  = ulC6_ProtectionRegion0;       // C6 Protection Region0 Register value
   *((uint32_t *)(pucBuffer + 0x24))  = ulC6_ProtectionRegion1;       // C6 Protection Region1 Register value
   *((uint32_t *)(pucBuffer + 0x28))  = ulC6_ProtectionRegion2;       // C6 Protection Region2 Register value
   *((uint32_t *)(pucBuffer + 0x2C))  = ulC6_ProtectionRegion3;       // C6 Protection Region3 Register value
   *((uint32_t *)(pucBuffer + 0x30))  = ulC6_ProtectionRegion4;       // C6 Protection Region4 Register value
   *((uint32_t *)(pucBuffer + 0x34))  = ulC6_ProtectionRegion5;       // C6 Protection Region5 Register value     
   *((uint32_t *)(pucBuffer + 0x38))  = ulC6_ProtectionRegion6;       // C6 Protection Region6 Register value     
   *((uint32_t *)(pucBuffer + 0x3C))  = ulC6_ProtectionRegion7;       // C6 Protection Region7 Register value     
   *((uint32_t *)(pucBuffer + 0x40))  = ulC9_D_CacheTLB_L_Down;       // C9 D Cache TLB Lock Down Register value     
   *((uint32_t *)(pucBuffer + 0x44))  = ulC9_I_CacheTLB_L_Down;       // C9 D Cache TLB Lock Down Register value     

   iPacketLen = TRANSMIT_LEN(72);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM966CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
               uint32_t ulC15_BIST_Control - C15 BIST Control Register
               uint32_t ulC15_BIST_I_Address - C15 BIST I Cache Address Register
               uint32_t ulC15_BIST_I_General - C15 BIST I Cache General Register
               uint32_t ulC15_BIST_D_Address - C15 BIST D Cache Address Register
               uint32_t ulC15_BIST_D_General - C15 BIST D Cache General Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM966 Co Processor registers
Date           Initials    Description
25-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM966CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control,
                                             uint32_t ulC15_BIST_Control,
                                             uint32_t ulC15_BIST_I_Address,
                                             uint32_t ulC15_BIST_I_General,
                                             uint32_t ulC15_BIST_D_Address,
                                             uint32_t ulC15_BIST_D_General)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM966CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM966CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulC1_Control;                 // C1 Control Register value
   *((uint32_t *)(pucBuffer + 0xC))   = ulC15_BIST_Control;           // C15 BIST Control Register value
   *((uint32_t *)(pucBuffer + 0x10))  = ulC15_BIST_I_Address;         // C15 BIST I Cache Address Register value
   *((uint32_t *)(pucBuffer + 0x14))  = ulC15_BIST_I_General;         // C15 BIST I Cache General Register value
   *((uint32_t *)(pucBuffer + 0x18))  = ulC15_BIST_D_Address;         // C15 BIST D Cache Address Register value
   *((uint32_t *)(pucBuffer + 0x1C))  = ulC15_BIST_D_General;         // C15 BIST D Cache General Register value

   iPacketLen = TRANSMIT_LEN(32);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM968CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulC1_Control - C1 Control Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM968 Co Processor registers
Date           Initials    Description
24-Mar-2008      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM968CP_Registers(TyDevHandle tyDevHandle, 
                                             uint32_t ulCore,
                                             uint32_t ulC1_Control)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM968CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM968CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulC1_Control;                 // C1 Control Register value

   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM720CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register value
               uint32_t *pulC1_Control - C1 Control Register value
               uint32_t *pulC2_TTB - C2 Translation Table Base Register value
               uint32_t *pulC3_DAC - C3 Domain Access Control Register value
               uint32_t *pulC5_FSR - C5 Fault Status Register value
               uint32_t *pulC6_FAR - C6 Fault Address Register value
               uint32_t *pulC13_PID - C13 Process ID Register value
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM720 Co Processor registers
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM720CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC2_TTB,
                                            uint32_t *pulC3_DAC,
                                            uint32_t *pulC5_FSR,
                                            uint32_t *pulC6_FAR,
                                            uint32_t *pulC13_PID)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM720CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM720CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)  = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC1_Control) = *((uint32_t *)(pucBuffer + 0x08));
         *((uint32_t *)pulC2_TTB)     = *((uint32_t *)(pucBuffer + 0x0C));
         *((uint32_t *)pulC3_DAC)     = *((uint32_t *)(pucBuffer + 0x10));
         *((uint32_t *)pulC5_FSR)     = *((uint32_t *)(pucBuffer + 0x14));
         *((uint32_t *)pulC6_FAR)     = *((uint32_t *)(pucBuffer + 0x18));
         *((uint32_t *)pulC13_PID)    = *((uint32_t *)(pucBuffer + 0x1C));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM740CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register value
               uint32_t *pulC1_Control - C1 Control Register value
               uint32_t *pulC2_TTB - C2 Translation Table Base Register value
               uint32_t *pulC3_DAC - C3 Domain Access Control Register value
               uint32_t *pulC5_FSR - C5 Fault Status Register value
               uint32_t *pulC6_FAR - C6 Fault Address Register value
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM740 Co Processor registers
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM740CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC2_TTB,
                                            uint32_t *pulC3_DAC,
                                            uint32_t *pulC5_FSR,
                                            uint32_t *pulC6_FAR)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM740CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM740CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)  = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC1_Control) = *((uint32_t *)(pucBuffer + 0x08));
         *((uint32_t *)pulC2_TTB)     = *((uint32_t *)(pucBuffer + 0x0C));
         *((uint32_t *)pulC3_DAC)     = *((uint32_t *)(pucBuffer + 0x10));
         *((uint32_t *)pulC5_FSR)     = *((uint32_t *)(pucBuffer + 0x14));
         *((uint32_t *)pulC6_FAR)     = *((uint32_t *)(pucBuffer + 0x18));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM920_922CoProcessor
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulRegAddr - CoProcessor Register to be read
               uint32_t *pulRegVal - pointer to CoProcessor Register value read
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads from ARM920/922 Co Processor
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM920_922CoProcessor(TyDevHandle tyDevHandle, 
                                               uint32_t ulCore,
                                               uint32_t ulCPRegAddr,    
                                               uint32_t *pulCPRegVal)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM920_922CoProcessor()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM920_922CP;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                               // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)ulCPRegAddr;      // register address

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         *((uint32_t *)pulCPRegVal)  = *((uint32_t *)(pucBuffer + 0x04));
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM920_922CoProcessor
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCPRegAddr - CoProcessor Register to be written
               uint32_t ulCPRegVal - Register value to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM920/922 Co Processor
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM920_922CoProcessor(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPRegAddr,    
                                                uint32_t ulCPRegVal)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM920_922CoProcessor()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM920_922CP; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                               // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)ulCPRegAddr;      // register address
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                               // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                               // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulCPRegVal;                      // register value

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM920_922CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register 
               uint32_t *pulC0_Cache_Type - C0 Cache Type
               uint32_t *pulC1_Control - C1 Control Register 
               uint32_t *pulC2_D_CacheTTB - C2 D Cache Translation Table Base Register
               uint32_t *pulC2_I_CacheTTB - C2 I Cache Translation Table Base Register
               uint32_t *pulC3_D_CacheDAC - C3 D Cache Domain Access Control Register
               uint32_t *pulC3_I_CacheDAC - C3 I Cache Domain Access Control Register
               uint32_t *pulC5_D_CacheFSR - C5 D Cache Fault Status Register
               uint32_t *pulC5_I_CacheFSR - C5 I Cache Fault Status Register
               uint32_t *pulC6_D_CacheFAR - C6 D Cache Fault Address Register
               uint32_t *pulC6_I_CacheFAR - C6 I Cache Fault Address Register
               uint32_t *pulC9_D_Cache_L_Down - C9 D Cache Lock Down Register
               uint32_t *pulC9_I_Cache_L_Down - C9 I Cache Lock Down Register
               uint32_t *pulC10_D_CacheTLB_L_Down - C10 D Cache TLB Lock Down Register
               uint32_t *pulC10_I_CacheTLB_L_Down - C10 I Cache TLB Lock Down Register
               uint32_t *pulC13_ProcessID - C13 Process ID Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM920/922 Co Processor registers
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM920_922CP_Registers(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t *pulC0_ID_Reg,
                                                uint32_t *pulC0_Cache_Type,
                                                uint32_t *pulC1_Control,
                                                uint32_t *pulC2_D_CacheTTB,
                                                uint32_t *pulC2_I_CacheTTB,
                                                uint32_t *pulC3_D_CacheDAC,
                                                uint32_t *pulC3_I_CacheDAC,
                                                uint32_t *pulC5_D_CacheFSR,
                                                uint32_t *pulC5_I_CacheFSR,
                                                uint32_t *pulC6_D_CacheFAR,
                                                uint32_t *pulC6_I_CacheFAR,
                                                uint32_t *pulC9_D_Cache_L_Down,
                                                uint32_t *pulC9_I_Cache_L_Down,
                                                uint32_t *pulC10_D_CacheTLB_L_Down,
                                                uint32_t *pulC10_I_CacheTLB_L_Down,
                                                uint32_t *pulC13_ProcessID)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM920_922CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM920_922CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;              // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)             = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC0_Cache_Type)         = *((uint32_t *)(pucBuffer + 0x08));
         *((uint32_t *)pulC1_Control)            = *((uint32_t *)(pucBuffer + 0x0C));
         *((uint32_t *)pulC2_D_CacheTTB)         = *((uint32_t *)(pucBuffer + 0x10));
         *((uint32_t *)pulC2_I_CacheTTB)         = *((uint32_t *)(pucBuffer + 0x14));
         *((uint32_t *)pulC3_D_CacheDAC)         = *((uint32_t *)(pucBuffer + 0x18));
         *((uint32_t *)pulC3_I_CacheDAC)         = *((uint32_t *)(pucBuffer + 0x1C));
         *((uint32_t *)pulC5_D_CacheFSR)         = *((uint32_t *)(pucBuffer + 0x20));
         *((uint32_t *)pulC5_I_CacheFSR)         = *((uint32_t *)(pucBuffer + 0x24));
         *((uint32_t *)pulC6_D_CacheFAR)         = *((uint32_t *)(pucBuffer + 0x28));
         *((uint32_t *)pulC6_I_CacheFAR)         = *((uint32_t *)(pucBuffer + 0x2C));
         *((uint32_t *)pulC9_D_Cache_L_Down)     = *((uint32_t *)(pucBuffer + 0x30));
         *((uint32_t *)pulC9_I_Cache_L_Down)     = *((uint32_t *)(pucBuffer + 0x34));
         *((uint32_t *)pulC10_D_CacheTLB_L_Down) = *((uint32_t *)(pucBuffer + 0x38));
         *((uint32_t *)pulC10_I_CacheTLB_L_Down) = *((uint32_t *)(pucBuffer + 0x3C));
         *((uint32_t *)pulC13_ProcessID)         = *((uint32_t *)(pucBuffer + 0x40));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM926CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register 
               uint32_t *pulC0_Cache_Type - C0 Cache Type
               uint32_t *pulC0_TCM_Size - C0 TCM Size 
               uint32_t *pulC1_Control - C1 Control Register 
               uint32_t *pulC2_TTB - C2 Translation Table Base Register
               uint32_t *pulC3_Domain_Access_Control - C3 Domain Access Control Register
               uint32_t *pulC5_D_FSR - C5 D Cache Fault Status Register
               uint32_t *pulC5_I_FSR - C5 I Cache Fault Status Register
               uint32_t *pulC6_FAR - C6 Fault Address Register
               uint32_t *pulC9_D_Lock_Down - C9 D Cache Lock Down Register
               uint32_t *pulC9_I_Lock_Down - C9 I Cache Lock Down Register
               uint32_t *pulC9_D_TCM - C9 D Cache TCM Register
               uint32_t *pulC9_I_TCM - C9 I Cache TCM Register
               uint32_t *pulC10_TLB_LockDown - C10 TLB Lock Down Register
               uint32_t *pulC13_PID - C13 Process ID Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM926 Co Processor registers
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM926CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC0_Cache_Type, 
                                            uint32_t *pulC0_TCM_Size, 
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC2_TTB,
                                            uint32_t *pulC3_Domain_Access_Control,
                                            uint32_t *pulC5_D_FSR,
                                            uint32_t *pulC5_I_FSR,
                                            uint32_t *pulC6_FAR,
                                            uint32_t *pulC9_D_Lock_Down,
                                            uint32_t *pulC9_I_Lock_Down,
                                            uint32_t *pulC9_D_TCM,
                                            uint32_t *pulC9_I_TCM,
                                            uint32_t *pulC10_TLB_LockDown,
                                            uint32_t *pulC13_PID)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM926CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM926CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)                = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC0_Cache_Type)            = *((uint32_t *)(pucBuffer + 0x08));
         *((uint32_t *)pulC0_TCM_Size)              = *((uint32_t *)(pucBuffer + 0x0C));
         *((uint32_t *)pulC1_Control)               = *((uint32_t *)(pucBuffer + 0x10));
         *((uint32_t *)pulC2_TTB)                   = *((uint32_t *)(pucBuffer + 0x14));
         *((uint32_t *)pulC3_Domain_Access_Control) = *((uint32_t *)(pucBuffer + 0x18));
         *((uint32_t *)pulC5_D_FSR)                 = *((uint32_t *)(pucBuffer + 0x1C));
         *((uint32_t *)pulC5_I_FSR)                 = *((uint32_t *)(pucBuffer + 0x20));
         *((uint32_t *)pulC6_FAR)                   = *((uint32_t *)(pucBuffer + 0x24));
         *((uint32_t *)pulC9_D_Lock_Down)           = *((uint32_t *)(pucBuffer + 0x28));
         *((uint32_t *)pulC9_I_Lock_Down)           = *((uint32_t *)(pucBuffer + 0x2C));
         *((uint32_t *)pulC9_D_TCM)                 = *((uint32_t *)(pucBuffer + 0x30));
         *((uint32_t *)pulC9_I_TCM)                 = *((uint32_t *)(pucBuffer + 0x34));
         *((uint32_t *)pulC10_TLB_LockDown)         = *((uint32_t *)(pucBuffer + 0x38));
         *((uint32_t *)pulC13_PID)                  = *((uint32_t *)(pucBuffer + 0x3C));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM940_946CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register 
               uint32_t *pulC0_Cache_Type - C0 Cache Type
               uint32_t *pulC0_TCM_Size - C0 TCM Size 
               uint32_t *pulC1_Control - C1 Control Register 
               uint32_t *pulC2_D_CacheConfiguration - C2 D Cache Configuration Register
               uint32_t *pulC2_I_CacheConfiguration - C2 I Cache Configuration Register
               uint32_t *pulC3_WriteBufferControl - C3 Write Buffer Control Register
               uint32_t *pulC5_D_AccessPermission - C5 D Cache Access Permission Register
               uint32_t *pulC5_I_AccessPermission - C5 I Cache Access Permission Register
               uint32_t *pulC6_ProtectionRegion0 - C6 Protection Region0 Register
               uint32_t *pulC6_ProtectionRegion1 - C6 Protection Region1 Register
               uint32_t *pulC6_ProtectionRegion2 - C6 Protection Region2 Register
               uint32_t *pulC6_ProtectionRegion3 - C6 Protection Region3 Register
               uint32_t *pulC6_ProtectionRegion4 - C6 Protection Region4 Register
               uint32_t *pulC6_ProtectionRegion5 - C6 Protection Region5 Register
               uint32_t *pulC6_ProtectionRegion6 - C6 Protection Region6 Register
               uint32_t *pulC6_ProtectionRegion7 - C6 Protection Region7 Register
               uint32_t *pulC9_D_CacheTLB_L_Down - C9 D Cache TLB Lock Down Register
               uint32_t *pulC9_I_CacheTLB_L_Down - C9 I Cache TLB Lock Down Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM940/946 Co Processor registers
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM940_946CP_Registers(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t *pulC0_ID_Reg,    
                                                uint32_t *pulC0_Cache_Type, 
                                                uint32_t *pulC0_TCM_Size, 
                                                uint32_t *pulC1_Control,
                                                uint32_t *pulC2_D_CacheConfiguration,
                                                uint32_t *pulC2_I_CacheConfiguration,
                                                uint32_t *pulC3_WriteBufferControl,
                                                uint32_t *pulC5_D_AccessPermission,
                                                uint32_t *pulC5_I_AccessPermission,
                                                uint32_t *pulC6_ProtectionRegion0,
                                                uint32_t *pulC6_ProtectionRegion1,
                                                uint32_t *pulC6_ProtectionRegion2,
                                                uint32_t *pulC6_ProtectionRegion3,
                                                uint32_t *pulC6_ProtectionRegion4,
                                                uint32_t *pulC6_ProtectionRegion5,
                                                uint32_t *pulC6_ProtectionRegion6,
                                                uint32_t *pulC6_ProtectionRegion7,
                                                uint32_t *pulC9_D_CacheTLB_L_Down,
                                                uint32_t *pulC9_I_CacheTLB_L_Down)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM940_946CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM940_946CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;              // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)                = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC0_Cache_Type)            = *((uint32_t *)(pucBuffer + 0x08));
         *((uint32_t *)pulC0_TCM_Size)              = *((uint32_t *)(pucBuffer + 0x0C));
         *((uint32_t *)pulC1_Control)               = *((uint32_t *)(pucBuffer + 0x10));
         *((uint32_t *)pulC2_D_CacheConfiguration)  = *((uint32_t *)(pucBuffer + 0x14));
         *((uint32_t *)pulC2_I_CacheConfiguration)  = *((uint32_t *)(pucBuffer + 0x18));
         *((uint32_t *)pulC3_WriteBufferControl)    = *((uint32_t *)(pucBuffer + 0x1C));
         *((uint32_t *)pulC5_D_AccessPermission)    = *((uint32_t *)(pucBuffer + 0x20));
         *((uint32_t *)pulC5_I_AccessPermission)    = *((uint32_t *)(pucBuffer + 0x24));
         *((uint32_t *)pulC6_ProtectionRegion0)     = *((uint32_t *)(pucBuffer + 0x28));
         *((uint32_t *)pulC6_ProtectionRegion1)     = *((uint32_t *)(pucBuffer + 0x2C));
         *((uint32_t *)pulC6_ProtectionRegion2)     = *((uint32_t *)(pucBuffer + 0x30));
         *((uint32_t *)pulC6_ProtectionRegion3)     = *((uint32_t *)(pucBuffer + 0x34));
         *((uint32_t *)pulC6_ProtectionRegion4)     = *((uint32_t *)(pucBuffer + 0x38));
         *((uint32_t *)pulC6_ProtectionRegion5)     = *((uint32_t *)(pucBuffer + 0x3C));
         *((uint32_t *)pulC6_ProtectionRegion6)     = *((uint32_t *)(pucBuffer + 0x40));
         *((uint32_t *)pulC6_ProtectionRegion7)     = *((uint32_t *)(pucBuffer + 0x44));
         *((uint32_t *)pulC9_D_CacheTLB_L_Down)     = *((uint32_t *)(pucBuffer + 0x48));
         *((uint32_t *)pulC9_I_CacheTLB_L_Down)     = *((uint32_t *)(pucBuffer + 0x4C));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM966CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register 
               uint32_t *pulC1_Control - C1 Control Register 
               uint32_t *pulC15_BIST_Control - C15 BIST Control Register
               uint32_t *pulC15_BIST_I_Address - C15 BIST I Address Register
               uint32_t *pulC15_BIST_I_General - C15 BIST I General Register
               uint32_t *pulC15_BIST_D_Address - C15 BIST D Address Register
               uint32_t *pulC15_BIST_D_General - C15 BIST D General Register
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM966 Co Processor registers
Date           Initials    Description
26-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM966CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control,
                                            uint32_t *pulC15_BIST_Control,
                                            uint32_t *pulC15_BIST_I_Address,
                                            uint32_t *pulC15_BIST_I_General,
                                            uint32_t *pulC15_BIST_D_Address,
                                            uint32_t *pulC15_BIST_D_General)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM966CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM966CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)           = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC1_Control)          = *((uint32_t *)(pucBuffer + 0x08));
         *((uint32_t *)pulC15_BIST_Control)    = *((uint32_t *)(pucBuffer + 0x0C));
         *((uint32_t *)pulC15_BIST_I_Address)  = *((uint32_t *)(pucBuffer + 0x10));
         *((uint32_t *)pulC15_BIST_I_General)  = *((uint32_t *)(pucBuffer + 0x14));
         *((uint32_t *)pulC15_BIST_D_Address)  = *((uint32_t *)(pucBuffer + 0x18));
         *((uint32_t *)pulC15_BIST_D_General)  = *((uint32_t *)(pucBuffer + 0x1C));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM968CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register 
               uint32_t *pulC1_Control - C1 Control Register 
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM968 Co Processor registers
Date           Initials    Description
24-Mar-2008      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM968CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pulC0_ID_Reg,    
                                            uint32_t *pulC1_Control)
{
   DLOG("Entering: DL_OPXD_Arm_Read_ARM968CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM968CP_REG;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         *((uint32_t *)pulC0_ID_Reg)           = *((uint32_t *)(pucBuffer + 0x04));
         *((uint32_t *)pulC1_Control)          = *((uint32_t *)(pucBuffer + 0x08));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM940_946CoProcessor
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCPRegAddr - CoProcessor Register to be written
               uint32_t ulCPRegVal - Register value to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM940/946 Co Processor
Date           Initials    Description
01-Aug-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM940_946CoProcessor(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPRegAddr,    
                                                uint32_t ulCPRegVal)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM940_946CoProcessor()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM940_946CP;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;           // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)ulCPRegAddr;       // register address
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                                // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                                // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulCPRegVal;                       // register value

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM926CoProcessor
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCPRegAddr - CoProcessor Register to be written
               uint32_t ulCPRegVal - Register value to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM926 Co Processor
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM926CoProcessor(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t ulCPRegAddr,    
                                            uint32_t ulCPRegVal)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM926CoProcessor()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM926CP;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)ulCPRegAddr;   // register address
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                            // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                            // reserved
   *((uint32_t *)(pucBuffer + 0xC))   = ulCPRegVal;                   // register value

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM720_740CoProcessor
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCPInstruction - CoProcessor Instruction which 
               specifies which register to be written
               uint32_t ulCPRegVal - Register value to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM720/740 Co Processor
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM720_740CoProcessor(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPInstruction,    
                                                uint32_t ulCPRegVal)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM720_740CoProcessor()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM720_740CP; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                               // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulCPInstruction;                 // register address
   *((uint32_t *)(pucBuffer + 0xC))   = ulCPRegVal;                      // register value

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Write_ARM1136CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulCPInstruction - CoProcessor Instruction which 
               specifies which register to be written
               uint32_t ulCPRegVal - Register value to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes to ARM1136 Co Processor
Date           Initials    Description
10-Oct-2009    RTR         Initial
****************************************************************************/
TyError DL_OPXD_Arm_Write_ARM1136CP_Registers(TyDevHandle tyDevHandle, 
                                                uint32_t ulCore,
                                                uint32_t ulCPInstruction,    
                                                uint32_t* ulCPRegVal,
																uint32_t ulArraySize)
{
   DLOG("Entering: DL_OPXD_Arm_Write_ARM1136CP_Registers()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_WRITE_ARM11_CP; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                               // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulCPInstruction;                 // register address
    
	memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(ulCPRegVal), (ulArraySize));

	iPacketLen = TRANSMIT_LEN(16 + (int32_t)ulArraySize);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_Read_ARM1136CP_Registers
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulC0_ID_Reg - C0 ID Register 
               uint32_t *pulC0_Cache - C0 Cache type Register 
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads ARM968 Co Processor registers
Date           Initials    Description
24-Mar-2008      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_Read_ARM1136CP_Registers(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t *pul_CPReg)  

{
	DLOG("Entering: DL_OPXD_Arm_Read_ARM1136CP_Registers()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_ARM1136CP_REG;  // command code
	*((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
	
	iPacketLen = TRANSMIT_LEN(6);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
	{
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		if (tyResult == DRVOPXD_ERROR_NO_ERROR)
		{
			*((uint32_t *)pul_CPReg++)          = *((uint32_t *)(pucBuffer + 0x04));
			*((uint32_t *)pul_CPReg++)          = *((uint32_t *)(pucBuffer + 0x08));
			*((uint32_t *)pul_CPReg++)          = *((uint32_t *)(pucBuffer + 0x0C));
		}
	}
	
	// synchronization not used, assuming single thread uses this function
	return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_RunARM9TargetCacheFlush
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartOfAddressOfRoutine - Start Address of Cache flush routine
               uint32_t ulEndOfAddressOfRoutine - End Address of Cache flush routine
       Output: error code DRVOPXD_ERROR_xxx
  Description: Runs the Cache flush routines on ARM9 Cores
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_RunARM9TargetCacheFlush(TyDevHandle tyDevHandle, 
                                            uint32_t ulCore,
                                            uint32_t ulStartOfAddressOfRoutine,
                                            uint32_t ulEndOfAddressOfRoutine)
{
   DLOG("Entering: DL_OPXD_Arm_RunARM9TargetCacheFlush()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_ARM9_RUN_CACHE_CLEAN; // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;            // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                 // reserved
   *((uint32_t *)(pucBuffer + 0x8))   = ulEndOfAddressOfRoutine;           // End Address of Cache flush routine
   *((uint32_t *)(pucBuffer + 0xC))   = ulStartOfAddressOfRoutine;         // Start Address of Cache flush routine

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_InvalidateARM920_922ICache
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Invalidates ARM 920/922 Instruction Cache
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_InvalidateARM920_922ICache(TyDevHandle tyDevHandle, 
                                               uint32_t ulCore)
{
   DLOG("Entering: DL_OPXD_Arm_InvalidateARM920_922ICache()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_ARM920_922CP_INVALIDATE_ICACHE;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number

   iPacketLen = TRANSMIT_LEN(6);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_CheckDCC
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               uint32_t *pulDCCReadData - DCC Data Read pointer
               unsigned char       *pbDataToRead - Is there any Data to read
               uint32_t *pulDCCWriteData - DCC Data Write pointer
               unsigned char       *pbDataWritten - Is there any data to write
       Output: error code DRVOPXD_ERROR_xxx
  Description: Reads / Write data to the Debug Comms Channel.
Date           Initials    Description
15-Aug-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_CheckDCC (TyDevHandle tyDevHandle, 
                              uint32_t ulCore, 
                              TyArmProcType tyArmType, 
                              uint32_t *pulDCCReadData, 
                              unsigned char       *pbDataToRead, 
                              uint32_t *pulDCCWriteData,
                              unsigned char       *pbDataWritten)
{
   DLOG("Entering: DL_OPXD_Arm_CheckDCC()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
   unsigned char bDataToWriteToCore; 

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   bDataToWriteToCore = (pulDCCWriteData != NULL);

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_READ_DCC_CHANNEL;  // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;         // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                              // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;       // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = bDataToWriteToCore;             // if there is any data to write
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                              // reserved

   if(bDataToWriteToCore)
     *((uint32_t *)(pucBuffer + 0xC))   = *pulDCCWriteData;  // The DCC data to write
   else
     *((uint32_t *)(pucBuffer + 0xC))   = 0;  // nothing to write

   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         {
         if (pbDataWritten)
            *((unsigned char *)pbDataWritten)  = *((unsigned char *)(pucBuffer + 0x04));
         *((unsigned char *)pbDataToRead)   = *((unsigned char *)(pucBuffer + 0x05));
         *((uint32_t *)pulDCCReadData) = *((uint32_t *)(pucBuffer + 0x08));
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Core Type - ARM9 or ARM7
       Output: error code DRVOPXD_ERROR_xxx
  Description: Changes the JTAG TAP state from TestLogicReset to RunTestIdle
Date           Initials    Description
13-Aug-2007       SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType)
{
   DLOG("Entering: DL_OPXD_Arm_ChangeTAPState_FromTLRToRTI()\n");

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;   // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                        // reserved
   *((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)tyArmType; // core type

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);

}


/****************************************************************************
     Function: DL_OPXD_ReadROMTable
     Engineer: Jeenus C.K.
        Input: TyDevHandle tyDevHandle  - handle to open device
               uint32_t ulCore	    - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulBaseAddr - Base address of the ROM table
			   uint32_t ulApIndex  - Access port Index.
			   uint32_t* pulCoresightComp - Inorder to return the 
												 ROM table entries.
				
       Output: error code DRVOPXD_ERROR_xxx
  Description: To read the rom table entries and IDR register, Configuration 
			   register and Base address register values.
Date           Initials    Description
30-Apr-2010       JCK        Initial
****************************************************************************/
TyError DL_OPXD_ReadROMTable(TyDevHandle tyDevHandle, 
							 uint32_t ulCore,
							 uint32_t ulBaseAddr,
							 uint32_t ulApIndex,
							 uint32_t* pulCoresightComp)
{
	DLOG("Entering: DL_OPXD_ReadROMTable()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_ROM_TABLE;   // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;   // core number
	*((unsigned short *)(pucBuffer + 0x6)) = 0;                        // reserved
	*((uint32_t *)(pucBuffer + 0x8))  = ulBaseAddr;                        
	*((uint32_t *)(pucBuffer + 0xC))  = ulApIndex;  
	
	iPacketLen = TRANSMIT_LEN(16);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

	uint32_t ulNumEntries = *((uint32_t *)(pucBuffer + 0x04));	
	uint32_t ulNumValidData; 

#if 0
	ptyCoresightComp->ulNumEntries = ulNumEntries;
	ptyCoresightComp->ulIdrRegValue = *((uint32_t *)(pucBuffer + 0x08));
	ptyCoresightComp->ulDbgBaseRegValue = *((uint32_t *)(pucBuffer + 0x0C));
	ptyCoresightComp->ulConfiRegValue = *((uint32_t *)(pucBuffer + 0x10));

	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{
		return(tyResult);
	}

	TyRomTableEntry* ptyRomTableEntry = ptyCoresightComp->tyRomTableEntry;
	uint32_t ulEntryIndex;
	uint32_t* pulRomTableData = (uint32_t *)(pucBuffer + 0x014); 

	for (ulEntryIndex = 0; ulEntryIndex < ulNumEntries; ulEntryIndex++)
	{
		ptyRomTableEntry->ulEntryPresent = *(pulRomTableData);
		pulRomTableData = pulRomTableData + 1;
		ptyRomTableEntry->ulCompBaseAddr = *pulRomTableData;
		pulRomTableData = pulRomTableData + 1;
		ptyRomTableEntry->ulCompClass    = *pulRomTableData;
		pulRomTableData = pulRomTableData + 1;
		ptyRomTableEntry->ulCompType     = *pulRomTableData;
		pulRomTableData = pulRomTableData + 1;
		ptyRomTableEntry = ptyRomTableEntry + 1;
	}
#endif


	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{
		if ( (DRVOPXD_ERROR_ARM_NO_DEBUG_ENTRY_AP  == tyResult ) ||
			 (DRVOPXD_ERROR_ARM_NOT_VALID_ROM_ADDR == tyResult))
		{
			/* In this case Number of data, IDR register value and configuration register value  
			needs to be send to the Upper layer. */
			ulNumValidData = sizeof(uint32_t) * 4;
			memcpy((void*) pulCoresightComp, (void*)(pucBuffer + 0x04), ulNumValidData);
		}
		
	} 
	else
	{
		/* In this case Number of data, IDR register value and configuration register value needs 
		   to be send to the Upper layer. Also ROM table entries also needs to be send. */
		ulNumValidData = (sizeof(uint32_t) * 4) + (ulNumEntries * sizeof(uint32_t) * 4);
		memcpy((void*) pulCoresightComp, (void*)(pucBuffer + 0x04), ulNumValidData);
	}

	// synchronization not used, assuming single thread uses this function
	return(tyResult);

}


/****************************************************************************
     Function: DL_OPXD_ReadDebugReg
     Engineer: Jeenus C.K.
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Core Type
			   uint32_t ulMemApIndex - Memory access port index.
			   uint32_t ulDebugRegAddr - Address of the Debug Register.
			   uint32_t* pulProcType - To return the Cpuid Value
       Output: error code DRVOPXD_ERROR_xxx
  Description: To read the Debug register of the Coresight component.
Date           Initials    Description
30-Apr-2010       JCK        Initial
****************************************************************************/
TyError  DL_OPXD_ReadDebugReg(TyDevHandle tyDiagDevHandle, 
				  uint32_t ulCore,
				  uint32_t ulDebugRegAddr,
				  uint32_t ulMemApIndex,
				  uint32_t* pulProcType)
{
	DLOG("Entering: DL_OPXD_ReadDebugReg()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	if (tyDiagDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;


	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDiagDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDiagDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_DEBUG_REG;   // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;   // core number
	*((unsigned short *)(pucBuffer + 0x6)) = 0;                        // reserved
	*((uint32_t *) (pucBuffer + 0x8)) = ulMemApIndex;
	*((uint32_t *) (pucBuffer + 0xC)) = ulDebugRegAddr;  
	
	iPacketLen = TRANSMIT_LEN(16);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDiagDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{
		return(tyResult);
	}


	*pulProcType = *(uint32_t *)(pucBuffer + 0x04);

	return (tyResult);

}

/****************************************************************************
     Function: DL_OPXD_Config_Coresight
     Engineer: Jeenus C.K.
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType ulCoresightBaseAddr - Coresight base address.
			   uint32_t ulMemApIndex - Debug access port index.
			   uint32_t ulMemApIndex - Memory access port index.
			   uint32_t ulJtagApIndex - Jtag access port index.			   
       Output: error code DRVOPXD_ERROR_xxx
  Description: Inorder to configure the Coresight components..
Date           Initials    Description
30-Apr-2010       JCK        Initial
****************************************************************************/
TyError  DL_OPXD_Config_Coresight(TyDevHandle tyDevHandle,
								  uint32_t ulCore,
								  uint32_t ulCoresightBaseAddr,
								  uint32_t ulDbgApIndex, 
								  uint32_t ulMemApIndex,
								  uint32_t ulJtagApIndex,
								  uint32_t ulUseMemApIndexMemAccess)
{
	DLOG("Entering: DL_OPXD_Config_Coresight()\n");
	
	usb_dev_handle *ptyUsbDevHandle;
	unsigned char *pucBuffer;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	int32_t iPacketLen;
	
	NOREF(ulJtagApIndex);
	 
	if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
		return DRVOPXD_ERROR_INVALID_HANDLE;
	
	if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
		return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	
	// synchronization not used, assuming single thread uses this function
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer  + 0x00)) = CMD_CODE_CONFIG_CORESIGHT;   // command code
	*((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;   // core number
	*((unsigned short *)(pucBuffer + 0x08)) = CORTEXA;                        // reserved
	*((uint32_t  *)(pucBuffer + 0x0C)) = ulDbgApIndex;
	*((uint32_t  *)(pucBuffer + 0x10)) = ulMemApIndex;
	*((uint32_t  *)(pucBuffer + 0x14)) = ulCoresightBaseAddr;
	*((uint32_t  *)(pucBuffer + 0x18)) = ulUseMemApIndexMemAccess;
	
	iPacketLen = TRANSMIT_LEN(28);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	
	// synchronization not used, assuming single thread uses this function
	return(tyResult);

}

/****************************************************************************
     Function: DL_OPXD_Arm_RegisterTest
     Engineer: Suraj S
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore  
               TyArmProcType tyArmType
       Output: error code DRVOPXD_ERROR_xxx
  Description: read ARM core registers
Date           Initials    Description
18-Jan-2008     SJ         Initial
03-Apr-2010	    JCK		   Added support for Cortex-A8.
****************************************************************************/
TyError DL_OPXD_Arm_RegisterTest(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType, unsigned char *bRegCheck)
{

   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyArm9Registors tyArm9Registors;
   uint32_t ulTemp = 0;
   uint32_t ulCount = 0;
   boolean bThumbMode  = FALSE;
   boolean bWatchPoint = FALSE;


   //Read Registers

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

#if 0
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;   
   pucBuffer[0x4] = 0x0;                         
   iPacketLen = TRANSMIT_LEN(5);
   
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
   
   
   
   //Bringin TAP to RTI State
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;  
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore ;                         
   iPacketLen = TRANSMIT_LEN(6);
   
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
   
#endif

   switch (tyArmType)
   {
   case ARM7:
   case ARM7S:
   case ARM9:
   case ARM11:
	   //Select Scan Chain
	   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
	   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore ;
	   pucBuffer[0x06] = 0x02;
	   pucBuffer[0x07] = 0x01;
	   pucBuffer[0x08] = (unsigned char)tyArmType;
	   iPacketLen = TRANSMIT_LEN(9);
	   
	   // send packet
	   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else
		   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
		   return  tyResult;
	   
	   //Write ICE Breaker control register debug request
	   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_ICE_BREAKER;     
	   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore ;
	   pucBuffer[0x06] = 0;
	   pucBuffer[0x07] = 0;
	   *((uint32_t *)(pucBuffer + 0x8))= 0x00;
	   *((uint32_t *)(pucBuffer + 0xC))= 0x02;
	   iPacketLen = TRANSMIT_LEN(16);
	   
	   // send packet
	   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else
		   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
		   return  tyResult;
	   
	   do
	   {
		   //ARM STATUS PROC
		   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;
		   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore ;
		   pucBuffer[0x06] = 0;
		   pucBuffer[0x07] = 0;
		   pucBuffer[0x08] = (unsigned char)tyArmType;
		   pucBuffer[0x09] = 0x1;
		   pucBuffer[0x0a] = 0x1;
		   iPacketLen = TRANSMIT_LEN(11);
		   
		   // send packet
		   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
			   tyResult = DRVOPXD_ERROR_USB_DRIVER;
		   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
			   tyResult = DRVOPXD_ERROR_USB_DRIVER;
		   else
			   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
		   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
			   return  tyResult;
		   
		   ulCount++;
		   if (ulCount == 500)
			   return DRVOPXD_ERROR_TARGET_BROCKEN;
		   
      }while (pucBuffer[0x05]  != 0x0);

	   /* If Thumb mode */
	   bThumbMode  = pucBuffer[0x06];
	   bWatchPoint = pucBuffer[0x07];

	   if (bThumbMode)
	   {
		   // create packet
		   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_CHANGE_TO_ARM_MODE; // command code
		   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;          // core number
		   *((unsigned short *)(pucBuffer + 0x6))  = 0;                               // reserved
		   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;        // processor type
		   *((unsigned char *)(pucBuffer + 0x9))   = bWatchPoint;                     // Watch point
		   *((unsigned char *)(pucBuffer + 0xA))   = 0;                       // User Halt
		   
		   iPacketLen = TRANSMIT_LEN(11);
		   
		   // send packet
		   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
			   tyResult = DRVOPXD_ERROR_USB_DRIVER;
		   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
			   tyResult = DRVOPXD_ERROR_USB_DRIVER;
		   else
		   {
			   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));			   
		   }
	   }
	   
      
   	break;

   case CORTEXA:   
	   /* do nothing */
	   break;
   default:
	   return DRVOPXD_ERROR_CONFIGURATION;
	   //break;
   }
   

   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS; 
   *((uint32_t *)(pucBuffer + 0x4)) = ulCore;
   *((uint32_t *)(pucBuffer + 0x8)) = tyArmType;  

   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
  // return(tyResult);


   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));


   //PrintMessage1(INFO_MESSAGE,"Writing registers");
   //Writing Registers

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
   *((uint32_t *)(pucBuffer + 0x04)) = ulCore;                         // halfword - usCore number (0 if MC support disabled)
   *((uint32_t *)(pucBuffer + 0x08)) = tyArmType;

   *((uint32_t *)(pucBuffer + 0x7C)) = ulTemp; tyArm9Registors.ulR0 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x80)) = ulTemp; tyArm9Registors.ulR1 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x84)) = ulTemp; tyArm9Registors.ulR2 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x88)) = ulTemp; tyArm9Registors.ulR3 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x8C)) = ulTemp; tyArm9Registors.ulR4 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x90)) = ulTemp; tyArm9Registors.ulR5 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x94)) = ulTemp; tyArm9Registors.ulR6 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x98)) = ulTemp; tyArm9Registors.ulR7 = ulTemp++;

   *((uint32_t *)(pucBuffer + 0x5C)) = ulTemp; tyArm9Registors.ulR8 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x60)) = ulTemp; tyArm9Registors.ulR9 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x64)) = ulTemp; tyArm9Registors.ulR10 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x68)) = ulTemp; tyArm9Registors.ulR11 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x6C)) = ulTemp; tyArm9Registors.ulR12 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x70)) = ulTemp; tyArm9Registors.ulR13Usr = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x74)) = ulTemp; tyArm9Registors.ulR14Usr = ulTemp++;

   *((uint32_t *)(pucBuffer + 0x9C)) = tyArm9Registors.ulPC;

   *((uint32_t *)(pucBuffer + 0x78)) = tyArm9Registors.ulCPSR;

   *((uint32_t *)(pucBuffer + 0x10)) = ulTemp; tyArm9Registors.ulR8Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x14)) = ulTemp; tyArm9Registors.ulR9Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x18)) = ulTemp; tyArm9Registors.ulR10Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x1C)) = ulTemp; tyArm9Registors.ulR11Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x20)) = ulTemp; tyArm9Registors.ulR12Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x24)) = ulTemp; tyArm9Registors.ulR13Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x28)) = ulTemp; tyArm9Registors.ulR14Fiq= ulTemp++;                                                                 
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm9Registors.ulSPSRFiq ;

   *((uint32_t *)(pucBuffer + 0x30)) = ulTemp; tyArm9Registors.ulR13Irq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x34)) = ulTemp; tyArm9Registors.ulR14Irq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm9Registors.ulSPSRIrq;

   *((uint32_t *)(pucBuffer + 0x3C)) = ulTemp;  tyArm9Registors.ulR13Svc = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x40)) = ulTemp;  tyArm9Registors.ulR14Svc = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm9Registors.ulSPSRSvc;

   *((uint32_t *)(pucBuffer + 0x48)) = ulTemp; tyArm9Registors.ulR13Abt = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x4C)) = ulTemp; tyArm9Registors.ulR14Abt = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm9Registors.ulSPSRAbt;

   *((uint32_t *)(pucBuffer + 0x54)) = ulTemp; tyArm9Registors.ulR13Und = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x58)) = ulTemp; tyArm9Registors.ulR14Und = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm9Registors.ulSPSRUnd;

   iPacketLen = TRANSMIT_LEN(160);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   //return(tyResult);

   //Reading again 
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                         // halfword - usCore number (0 if MC support disabled)
   pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = (unsigned char)tyArmType; //ucProcessor

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   //return(tyResult);

   if (tyArm9Registors.ulR0 != (*(uint32_t *)(pucBuffer + 0x04)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR1 != *((uint32_t *)(pucBuffer + 0x08)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR2 != *((uint32_t *)(pucBuffer + 0x0C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR3 != *((uint32_t *)(pucBuffer + 0x10)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR4 != *((uint32_t *)(pucBuffer + 0x14)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR5 != *((uint32_t *)(pucBuffer + 0x18)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR6 != *((uint32_t *)(pucBuffer + 0x1C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR7 != *((uint32_t *)(pucBuffer + 0x20)))	   *bRegCheck = 1;
   //PC No need to compare
   //CPSR Also not Comparing
   if (tyArm9Registors.ulR8 != *((uint32_t *)(pucBuffer + 0x2C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR9 != *((uint32_t *)(pucBuffer + 0x30)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR10 != *((uint32_t *)(pucBuffer + 0x34)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR11 != *((uint32_t *)(pucBuffer + 0x38)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR12 != *((uint32_t *)(pucBuffer + 0x3C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR13Usr != *((uint32_t *)(pucBuffer + 0x40)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR14Usr != *((uint32_t *)(pucBuffer + 0x44)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR8Fiq != *((uint32_t *)(pucBuffer + 0x48)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR9Fiq != *((uint32_t *)(pucBuffer + 0x4C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR10Fiq != *((uint32_t *)(pucBuffer + 0x50)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR11Fiq != *((uint32_t *)(pucBuffer + 0x54)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR12Fiq != *((uint32_t *)(pucBuffer + 0x58)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR13Fiq != *((uint32_t *)(pucBuffer + 0x5C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR14Fiq != *((uint32_t *)(pucBuffer + 0x60)))	   *bRegCheck = 1;
   //SPSRFIQ not comparing
   if (tyArm9Registors.ulR13Irq != *((uint32_t *)(pucBuffer + 0x68)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR14Irq != *((uint32_t *)(pucBuffer + 0x6C)))	   *bRegCheck = 1;
   //SPSRIrq not comparing
   if (tyArm9Registors.ulR13Svc != *((uint32_t *)(pucBuffer + 0x74)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR14Svc != *((uint32_t *)(pucBuffer + 0x78)))	   *bRegCheck = 1;
   //SPSR Svc not comparing
   if (tyArm9Registors.ulR13Abt != *((uint32_t *)(pucBuffer + 0x80)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR14Abt != *((uint32_t *)(pucBuffer + 0x84)))	   *bRegCheck = 1;
   //SPSR ABT not comparing
   if (tyArm9Registors.ulR13Und != *((uint32_t *)(pucBuffer + 0x8C)))	   *bRegCheck = 1;
   if (tyArm9Registors.ulR14Und != *((uint32_t *)(pucBuffer + 0x90)))	   *bRegCheck = 1;
      //SPSR UND not comparing
   
   if (bThumbMode)
   {
	   uint32_t ulRegPC = tyArm9Registors.ulPC | 1;                            
	   uint32_t ulReg0  = tyArm9Registors.ulR0;

	   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_CHANGE_TO_THUMB_MODE; // command code
	   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;            // core number
	   *((unsigned short *)(pucBuffer + 0x6))  = 0;                                 // reserved
	   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;                         // processor type
	   *((unsigned char *)(pucBuffer + 0x9))   = 0;                                  // reserved
	   *((unsigned short *)(pucBuffer + 0xA))  = 0;                                 // reserved
	   *((uint32_t *)(pucBuffer + 0xC))   = ulReg0;                            // Register0 value
	   *((uint32_t *)(pucBuffer + 0x10))  = ulRegPC;                           // RegisterPC value
	   
	   iPacketLen = TRANSMIT_LEN(20);
	   
	   // send packet
	   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		   tyResult = DRVOPXD_ERROR_USB_DRIVER;
	   else
		   tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   }
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_CortexA_RegisterTest
     Engineer: Jeenus C K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore  
               TyArmProcType tyArmType
			   unsigned char *bRegCheck - Status of reg test
       Output: error code DRVOPXD_ERROR_xxx
  Description: read ARM core registers
Date           Initials    Description
03-Apr-2010	    JCK		   Initial
****************************************************************************/
TyError DL_OPXD_CortexA_RegisterTest(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType, unsigned char *bRegCheck)
{

   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyCortexARegistors tyCortexARegistors;
   uint32_t ulTemp = 0;

   //Read Registers

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
 

   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS; 
   *((uint32_t *)(pucBuffer + 0x4)) = ulCore;
   *((uint32_t *)(pucBuffer + 0x8)) = tyArmType;  

   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
  // return(tyResult);


   tyCortexARegistors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyCortexARegistors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyCortexARegistors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyCortexARegistors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyCortexARegistors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyCortexARegistors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyCortexARegistors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyCortexARegistors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyCortexARegistors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyCortexARegistors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyCortexARegistors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyCortexARegistors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyCortexARegistors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyCortexARegistors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyCortexARegistors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyCortexARegistors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyCortexARegistors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyCortexARegistors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyCortexARegistors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyCortexARegistors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyCortexARegistors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyCortexARegistors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyCortexARegistors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyCortexARegistors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyCortexARegistors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyCortexARegistors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyCortexARegistors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyCortexARegistors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyCortexARegistors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyCortexARegistors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyCortexARegistors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyCortexARegistors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyCortexARegistors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyCortexARegistors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyCortexARegistors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyCortexARegistors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyCortexARegistors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));


   //PrintMessage1(INFO_MESSAGE,"Writing registers");
   //Writing Registers

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   uint32_t i = 0;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
   *((uint32_t *)(pucBuffer + 0x04)) = ulCore;                         // halfword - usCore number (0 if MC support disabled)
   *((uint32_t *)(pucBuffer + 0x08)) = tyArmType;

   i = 0x0C;

   *((uint32_t *)(pucBuffer + i)) = tyCortexARegistors.ulSPSRFiq ;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR8Fiq = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR9Fiq = ulTemp++;
   i +=4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR10Fiq = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR11Fiq = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR12Fiq = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Fiq = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Fiq= ulTemp++;
   i += 4;

   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulSPSRIrq = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Irq = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Irq= ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulSPSRSvc = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Svc = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Svc= ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulSPSRAbt = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Abt = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Abt= ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulSPSRUnd = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Und = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Und= ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulSPSRMon = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Mon = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Mon = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR8 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR9 = ulTemp++;
   i +=4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR10 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR11 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR12 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR13Usr = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR14Usr = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = tyCortexARegistors.ulCPSR; 
   i += 4;


   /**********/
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR0 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR1 = ulTemp++;
   i +=4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR2 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR3 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR4 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR5 = ulTemp++;
   i += 4;
   
   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR6 = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = ulTemp; 
   tyCortexARegistors.ulR7 = ulTemp++;
   i += 4;

   *((uint32_t *)(pucBuffer + i)) = tyCortexARegistors.ulPC;

   iPacketLen = TRANSMIT_LEN(172);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   //return(tyResult);

   //Reading again 
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                         // halfword - usCore number (0 if MC support disabled)
   pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = (unsigned char)tyArmType; //ucProcessor

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   //return(tyResult);

   if (tyCortexARegistors.ulR0 != (*(uint32_t *)(pucBuffer + 0x04)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR1 != *((uint32_t *)(pucBuffer + 0x08)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR2 != *((uint32_t *)(pucBuffer + 0x0C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR3 != *((uint32_t *)(pucBuffer + 0x10)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR4 != *((uint32_t *)(pucBuffer + 0x14)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR5 != *((uint32_t *)(pucBuffer + 0x18)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR6 != *((uint32_t *)(pucBuffer + 0x1C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR7 != *((uint32_t *)(pucBuffer + 0x20)))	   *bRegCheck = 1;
   //PC No need to compare
   //CPSR Also not Comparing
   if (tyCortexARegistors.ulR8 != *((uint32_t *)(pucBuffer + 0x2C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR9 != *((uint32_t *)(pucBuffer + 0x30)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR10 != *((uint32_t *)(pucBuffer + 0x34)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR11 != *((uint32_t *)(pucBuffer + 0x38)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR12 != *((uint32_t *)(pucBuffer + 0x3C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR13Usr != *((uint32_t *)(pucBuffer + 0x40)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR14Usr != *((uint32_t *)(pucBuffer + 0x44)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR8Fiq != *((uint32_t *)(pucBuffer + 0x48)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR9Fiq != *((uint32_t *)(pucBuffer + 0x4C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR10Fiq != *((uint32_t *)(pucBuffer + 0x50)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR11Fiq != *((uint32_t *)(pucBuffer + 0x54)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR12Fiq != *((uint32_t *)(pucBuffer + 0x58)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR13Fiq != *((uint32_t *)(pucBuffer + 0x5C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR14Fiq != *((uint32_t *)(pucBuffer + 0x60)))	   *bRegCheck = 1;
   //SPSRFIQ not comparing
   if (tyCortexARegistors.ulR13Irq != *((uint32_t *)(pucBuffer + 0x68)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR14Irq != *((uint32_t *)(pucBuffer + 0x6C)))	   *bRegCheck = 1;
   //SPSRIrq not comparing
   if (tyCortexARegistors.ulR13Svc != *((uint32_t *)(pucBuffer + 0x74)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR14Svc != *((uint32_t *)(pucBuffer + 0x78)))	   *bRegCheck = 1;
   //SPSR Svc not comparing
   if (tyCortexARegistors.ulR13Abt != *((uint32_t *)(pucBuffer + 0x80)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR14Abt != *((uint32_t *)(pucBuffer + 0x84)))	   *bRegCheck = 1;
   //SPSR ABT not comparing
   if (tyCortexARegistors.ulR13Und != *((uint32_t *)(pucBuffer + 0x8C)))	   *bRegCheck = 1;
   if (tyCortexARegistors.ulR14Und != *((uint32_t *)(pucBuffer + 0x90)))	   *bRegCheck = 1;
      //SPSR UND not comparing

   /* Monitor mode is compared*/
   
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm11_RegisterTest
     Engineer: Amerdas D K
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore  
               TyArmProcType tyArmType
       Output: error code DRVOPXD_ERROR_xxx
  Description: read ARM11 core Registors
Date           Initials    Description
19-Oct-2009     ADK         Initial
****************************************************************************/
TyError DL_OPXD_Arm11_RegisterTest(TyDevHandle tyDevHandle, uint32_t ulCore, TyArmProcType tyArmType, unsigned char *bRegCheck)
{

   usb_dev_handle *ptyUsbDevHandle;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   unsigned char *pucBuffer;
   int32_t iPacketLen;
   TyArm9Registors tyArm11Registors;
   uint32_t ulTemp = 0;
   
   //Read Registers
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS; 
   *((uint32_t *)(pucBuffer + 0x4)) = ulCore;
   *((uint32_t *)(pucBuffer + 0x8)) = tyArmType;  

   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
  // return(tyResult);


   tyArm11Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm11Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm11Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm11Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm11Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm11Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm11Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm11Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm11Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm11Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm11Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm11Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm11Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm11Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm11Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm11Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm11Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm11Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm11Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm11Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm11Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm11Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm11Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm11Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm11Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm11Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm11Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm11Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm11Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm11Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm11Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm11Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm11Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm11Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm11Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm11Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm11Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));


   //PrintMessage1(INFO_MESSAGE,"Writing Resgistors");
   //Writing Registers

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
   *((uint32_t *)(pucBuffer + 0x04)) = ulCore;                         // halfword - usCore number (0 if MC support disabled)
   *((uint32_t *)(pucBuffer + 0x08)) = tyArmType;

   *((uint32_t *)(pucBuffer + 0x7C)) = ulTemp; tyArm11Registors.ulR0 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x80)) = ulTemp; tyArm11Registors.ulR1 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x84)) = ulTemp; tyArm11Registors.ulR2 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x88)) = ulTemp; tyArm11Registors.ulR3 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x8C)) = ulTemp; tyArm11Registors.ulR4 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x90)) = ulTemp; tyArm11Registors.ulR5 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x94)) = ulTemp; tyArm11Registors.ulR6 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x98)) = ulTemp; tyArm11Registors.ulR7 = ulTemp++;

   *((uint32_t *)(pucBuffer + 0x5C)) = ulTemp; tyArm11Registors.ulR8 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x60)) = ulTemp; tyArm11Registors.ulR9 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x64)) = ulTemp; tyArm11Registors.ulR10 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x68)) = ulTemp; tyArm11Registors.ulR11 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x6C)) = ulTemp; tyArm11Registors.ulR12 = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x70)) = ulTemp; tyArm11Registors.ulR13Usr = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x74)) = ulTemp; tyArm11Registors.ulR14Usr = ulTemp++;

   tyArm11Registors.ulPC = tyArm11Registors.ulPC - 0x08;

   *((uint32_t *)(pucBuffer + 0x9C)) = tyArm11Registors.ulPC;

   //*((uint32_t *)(pucBuffer + 0x78)) = tyArm11Registors.ulCPSR;

   *((uint32_t *)(pucBuffer + 0x10)) = ulTemp; tyArm11Registors.ulR8Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x14)) = ulTemp; tyArm11Registors.ulR9Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x18)) = ulTemp; tyArm11Registors.ulR10Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x1C)) = ulTemp; tyArm11Registors.ulR11Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x20)) = ulTemp; tyArm11Registors.ulR12Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x24)) = ulTemp; tyArm11Registors.ulR13Fiq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x28)) = ulTemp; tyArm11Registors.ulR14Fiq= ulTemp++;                                                                 
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm11Registors.ulSPSRFiq ;

   *((uint32_t *)(pucBuffer + 0x30)) = ulTemp; tyArm11Registors.ulR13Irq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x34)) = ulTemp; tyArm11Registors.ulR14Irq = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm11Registors.ulSPSRIrq;

   *((uint32_t *)(pucBuffer + 0x3C)) = ulTemp;  tyArm11Registors.ulR13Svc = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x40)) = ulTemp;  tyArm11Registors.ulR14Svc = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm11Registors.ulSPSRSvc;

   *((uint32_t *)(pucBuffer + 0x48)) = ulTemp; tyArm11Registors.ulR13Abt = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x4C)) = ulTemp; tyArm11Registors.ulR14Abt = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm11Registors.ulSPSRAbt;

   *((uint32_t *)(pucBuffer + 0x54)) = ulTemp; tyArm11Registors.ulR13Und = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x58)) = ulTemp; tyArm11Registors.ulR14Und = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm11Registors.ulSPSRUnd;

   iPacketLen = TRANSMIT_LEN(160);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   //return(tyResult);

   //Reading again 
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //zero all memory
   for(int32_t i= 0 ;i<160;i++)
      *((uint32_t *)(pucBuffer + i)) = 0;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                         // halfword - usCore number (0 if MC support disabled)
   pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = (unsigned char)tyArmType; //ucProcessor

   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   //return(tyResult);

   if (tyArm11Registors.ulR0 != (*(uint32_t *)(pucBuffer + 0x04)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR1 != *((uint32_t *)(pucBuffer + 0x08)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR2 != *((uint32_t *)(pucBuffer + 0x0C)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR3 != *((uint32_t *)(pucBuffer + 0x10)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR4 != *((uint32_t *)(pucBuffer + 0x14)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR5 != *((uint32_t *)(pucBuffer + 0x18)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR6 != *((uint32_t *)(pucBuffer + 0x1C)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR7 != *((uint32_t *)(pucBuffer + 0x20)))	   *bRegCheck = 1;
   //PC No need to compare
   //CPSR Also not Comaparing
   if (tyArm11Registors.ulR8 != *((uint32_t *)(pucBuffer + 0x2C)))		   *bRegCheck = 1;
   if (tyArm11Registors.ulR9 != *((uint32_t *)(pucBuffer + 0x30)))		   *bRegCheck = 1;
   if (tyArm11Registors.ulR10 != *((uint32_t *)(pucBuffer + 0x34)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR11 != *((uint32_t *)(pucBuffer + 0x38)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR12 != *((uint32_t *)(pucBuffer + 0x3C)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR13Usr != *((uint32_t *)(pucBuffer + 0x40)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR14Usr != *((uint32_t *)(pucBuffer + 0x44)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR8Fiq != *((uint32_t *)(pucBuffer + 0x48)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR9Fiq != *((uint32_t *)(pucBuffer + 0x4C)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR10Fiq != *((uint32_t *)(pucBuffer + 0x50)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR11Fiq != *((uint32_t *)(pucBuffer + 0x54)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR12Fiq != *((uint32_t *)(pucBuffer + 0x58)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR13Fiq != *((uint32_t *)(pucBuffer + 0x5C)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR14Fiq != *((uint32_t *)(pucBuffer + 0x60)))	   *bRegCheck = 1;
   //SPSRFIQ not camparing
   if (tyArm11Registors.ulR13Irq != *((uint32_t *)(pucBuffer + 0x68)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR14Irq != *((uint32_t *)(pucBuffer + 0x6C)))	   *bRegCheck = 1;
   //SPSRIrq not comparing
   if (tyArm11Registors.ulR13Svc != *((uint32_t *)(pucBuffer + 0x74)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR14Svc != *((uint32_t *)(pucBuffer + 0x78)))	   *bRegCheck = 1;
   //SPSR Svc not comparing
   if (tyArm11Registors.ulR13Abt != *((uint32_t *)(pucBuffer + 0x80)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR14Abt != *((uint32_t *)(pucBuffer + 0x84)))	   *bRegCheck = 1;
   //SPSR ABT not comapring
   if (tyArm11Registors.ulR13Und != *((uint32_t *)(pucBuffer + 0x8C)))	   *bRegCheck = 1;
   if (tyArm11Registors.ulR14Und != *((uint32_t *)(pucBuffer + 0x90)))	   *bRegCheck = 1;
      //SPSR UND not comparing
   
   return tyResult;
}

TyError DL_OPXD_Cortex_RegisterTest(TyDevHandle tyDevHandle, 
									uint32_t ulCore, 
									TyArmProcType tyArmType, 
								   unsigned char *bRegCheck)
{
	usb_dev_handle *ptyUsbDevHandle;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
	unsigned char *pucBuffer;
	int32_t iPacketLen;
	TyCortexRegistors tyCortexM3Registors;
	uint32_t ulTemp = 0;
	
	//Read Registers
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	
	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS; 
	*((uint32_t *)(pucBuffer + 0x4)) = ulCore;
	*((uint32_t *)(pucBuffer + 0x8)) = tyArmType;  
	
	iPacketLen = TRANSMIT_LEN(9);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
	// return(tyResult);	

	tyCortexM3Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
	tyCortexM3Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
	tyCortexM3Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
	tyCortexM3Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
	tyCortexM3Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
	tyCortexM3Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
	tyCortexM3Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
	tyCortexM3Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
	tyCortexM3Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x24));
	tyCortexM3Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x28));
	tyCortexM3Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x2C));
	tyCortexM3Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x30));
	tyCortexM3Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x34));
	tyCortexM3Registors.ulCurrSp  = *((uint32_t *)(pucBuffer + 0x38));
	tyCortexM3Registors.ulR14	  = *((uint32_t *)(pucBuffer + 0x3C));
	tyCortexM3Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x40));
	tyCortexM3Registors.ulxPSR    = *((uint32_t *)(pucBuffer + 0x44));	
	tyCortexM3Registors.ulMSP	  = *((uint32_t *)(pucBuffer + 0x48));
	tyCortexM3Registors.ulPSP	  = *((uint32_t *)(pucBuffer + 0x4C));
	tyCortexM3Registors.ulControl = *((uint32_t *)(pucBuffer + 0x50));
	
	
	
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
	*((uint32_t *)(pucBuffer + 0x04)) = ulCore;                         // halfword - usCore number (0 if MC support disabled)
	*((uint32_t *)(pucBuffer + 0x08)) = tyArmType;
	
	*((uint32_t *)(pucBuffer + 0x0C)) = ulTemp; tyCortexM3Registors.ulR0 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x10)) = ulTemp; tyCortexM3Registors.ulR1 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x14)) = ulTemp; tyCortexM3Registors.ulR2 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x18)) = ulTemp; tyCortexM3Registors.ulR3 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x1C)) = ulTemp; tyCortexM3Registors.ulR4 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x20)) = ulTemp; tyCortexM3Registors.ulR5 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x24)) = ulTemp; tyCortexM3Registors.ulR6 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x28)) = ulTemp; tyCortexM3Registors.ulR7 = ulTemp++;	
	*((uint32_t *)(pucBuffer + 0x2C)) = ulTemp; tyCortexM3Registors.ulR8 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x30)) = ulTemp; tyCortexM3Registors.ulR9 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x34)) = ulTemp; tyCortexM3Registors.ulR10 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x38)) = ulTemp; tyCortexM3Registors.ulR11 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x3C)) = ulTemp; tyCortexM3Registors.ulR12 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x40)) = tyCortexM3Registors.ulCurrSp; 
	*((uint32_t *)(pucBuffer + 0x44)) = ulTemp; tyCortexM3Registors.ulR14 = ulTemp++;
	*((uint32_t *)(pucBuffer + 0x48)) = tyCortexM3Registors.ulPC;	
	*((uint32_t *)(pucBuffer + 0x4C)) = tyCortexM3Registors.ulxPSR;	
	*((uint32_t *)(pucBuffer + 0x50)) = tyCortexM3Registors.ulCurrSp; tyCortexM3Registors.ulMSP = tyCortexM3Registors.ulCurrSp;
	*((uint32_t *)(pucBuffer + 0x54)) = tyCortexM3Registors.ulCurrSp; tyCortexM3Registors.ulPSP = tyCortexM3Registors.ulCurrSp;
	*((uint32_t *)(pucBuffer + 0x58)) = tyCortexM3Registors.ulControl;
	
	
	iPacketLen = TRANSMIT_LEN(92);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
	//return(tyResult);
	
	//Reading again 
	ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	
	//zero all memory
	for(int32_t i= 0 ;i<160;i++)
		*((uint32_t *)(pucBuffer + i)) = 0;
	
	// create packet
	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_REGISTERS;                   // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                         // halfword - usCore number (0 if MC support disabled)
	pucBuffer[0x06] = 0x0;
	pucBuffer[0x07] = 0x0;
	pucBuffer[0x08] = (unsigned char)tyArmType; //ucProcessor
	
	iPacketLen = TRANSMIT_LEN(9);
	
	// send packet
	if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
		tyResult = DRVOPXD_ERROR_USB_DRIVER;
	else
		tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	
	// synchronization not used, assuming single thread uses this function
	//return(tyResult);
	
	if (tyCortexM3Registors.ulR0 != (*(uint32_t *)(pucBuffer + 0x04)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR1 != *((uint32_t *)(pucBuffer + 0x08)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR2 != *((uint32_t *)(pucBuffer + 0x0C)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR3 != *((uint32_t *)(pucBuffer + 0x10)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR4 != *((uint32_t *)(pucBuffer + 0x14)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR5 != *((uint32_t *)(pucBuffer + 0x18)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR6 != *((uint32_t *)(pucBuffer + 0x1C)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR7 != *((uint32_t *)(pucBuffer + 0x20)))	   *bRegCheck = 1;
	
	if (tyCortexM3Registors.ulR8 != *((uint32_t *)(pucBuffer + 0x24)))		   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR9 != *((uint32_t *)(pucBuffer + 0x28)))		   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR10 != *((uint32_t *)(pucBuffer + 0x2C)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR11 != *((uint32_t *)(pucBuffer + 0x30)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR12 != *((uint32_t *)(pucBuffer + 0x34)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulCurrSp != *((uint32_t *)(pucBuffer + 0x38)))	   *bRegCheck = 1;
	if (tyCortexM3Registors.ulR14 != *((uint32_t *)(pucBuffer + 0x3C)))	   *bRegCheck = 1;
	
	
	return tyResult;
}
/****************************************************************************
*** Local functions                                                       ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_ConvertArmDwErrToDrvlayerErr
     Engineer: Suraj S
        Input: uint32_t ulErrorCode - error code from diskware (api_err.h)
       Output: error code DRVOPXD_ERROR_xxx
  Description: convert ARM diskware error code into drvlayer error code
Date           Initials    Description
13-Jul-2007    SJ          Initial
****************************************************************************/
static TyError DL_OPXD_ConvertArmDwErrToDrvlayerErr(uint32_t ulErrorCode)
{
   TyError tyResult = DRVOPXD_ERROR_USB_DRIVER;

   switch (ulErrorCode)
      {
      case ERR_NO_ERROR:   // no error
         tyResult = DRVOPXD_ERROR_NO_ERROR;
         break;

      case ERR_I2C_ERROR:
         tyResult = DRVOPXD_ERROR_DEVICE_I2C_ERROR;
         break;

      case ERR_ADAPTIVE_CLOCK_TIMEOUT:
         tyResult = DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT;
         break;

      default:    // other errors
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
         break;
      }
   return (tyResult);
}


/****************************************************************************
***          ARM Specific Functions  - For testing with OPXDDIAG          ***
****************************************************************************/
/****************************************************************************
     Function: SendAndReceiveUSB
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - usb device handle
               TyDevHandle tyDevHandle - handle to open device
               unsigned char *pucBuffer - Pointer to USB buffer
               uint32_t TransmitDataLength - Length of Transmission bytes
       Output: error code DRVOPXD_ERROR_xxx
 Descr4iption: Transmit and receive data from through USB
Date           Initials    Description
16-Jul-2007    SPT          Initial
****************************************************************************/
TyError SendAndReceiveUSB(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle,
                          unsigned char *pucBuffer,uint32_t TransmitDataLength)
{
   int32_t iPacketLen;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   iPacketLen = TRANSMIT_LEN((int32_t)TransmitDataLength);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      {
      printf("USB Transmit Error\n");
      return DRVOPXD_ERROR_USB_DRIVER;
      } 
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      {
      printf("USB Receive Error\n");
      return DRVOPXD_ERROR_USB_DRIVER;
      } 
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult != ERR_NO_ERROR )
      printf("Command execution Error\n");
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_Test_Register
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - pointer usb device handle
               TyDevHandle tyDevHandle - handle to open device
               unsigned char ucProcessor - Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore - nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: read ARM core Registors
Date           Initials    Description
05-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Register(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle, 
                                  unsigned char ucProcessor, unsigned short usCore)
{

   TyError tyResult;
   unsigned char *pucBuffer;
   TyArm9Registors tyArm9Registors;
   static uint32_t ulTemp = 0;

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS; 
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x8)) = ucProcessor;  
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

   //Writing Registers

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor;

   *((uint32_t *)(pucBuffer + 0x7C)) = ulTemp++; tyArm9Registors.ulR0 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x80)) = ulTemp++; tyArm9Registors.ulR1 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x84)) = ulTemp++; tyArm9Registors.ulR2 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x88)) = ulTemp++; tyArm9Registors.ulR3 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x8C)) = ulTemp++; tyArm9Registors.ulR4 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x90)) = ulTemp++; tyArm9Registors.ulR5 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x94)) = ulTemp++; tyArm9Registors.ulR6 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x98)) = ulTemp++; tyArm9Registors.ulR7 = ulTemp - 1;

   *((uint32_t *)(pucBuffer + 0x5C)) = ulTemp++; tyArm9Registors.ulR8 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x60)) = ulTemp++; tyArm9Registors.ulR9 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x64)) = ulTemp++; tyArm9Registors.ulR10 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x68)) = ulTemp++; tyArm9Registors.ulR11 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x6C)) = ulTemp++; tyArm9Registors.ulR12 = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x70)) = ulTemp++; tyArm9Registors.ulR13Usr = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x74)) = ulTemp++; tyArm9Registors.ulR14Usr = ulTemp - 1;

   *((uint32_t *)(pucBuffer + 0x9C)) = tyArm9Registors.ulPC;

   *((uint32_t *)(pucBuffer + 0x78)) = tyArm9Registors.ulCPSR;

   *((uint32_t *)(pucBuffer + 0x10)) = ulTemp++; tyArm9Registors.ulR8Fiq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x14)) = ulTemp++; tyArm9Registors.ulR9Fiq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x18)) = ulTemp++; tyArm9Registors.ulR10Fiq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x1C)) = ulTemp++; tyArm9Registors.ulR11Fiq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x20)) = ulTemp++; tyArm9Registors.ulR12Fiq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x24)) = ulTemp++; tyArm9Registors.ulR13Fiq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x28)) = ulTemp++; tyArm9Registors.ulR14Fiq= ulTemp - 1;                                                                 
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm9Registors.ulSPSRFiq ;

   *((uint32_t *)(pucBuffer + 0x30)) = ulTemp++; tyArm9Registors.ulR13Irq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x34)) = ulTemp++; tyArm9Registors.ulR14Irq = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm9Registors.ulSPSRIrq;

   *((uint32_t *)(pucBuffer + 0x3C)) = ulTemp++;  tyArm9Registors.ulR13Svc = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x40)) = ulTemp++;  tyArm9Registors.ulR14Svc = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm9Registors.ulSPSRSvc;

   *((uint32_t *)(pucBuffer + 0x48)) = ulTemp++; tyArm9Registors.ulR13Abt = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x4C)) = ulTemp++; tyArm9Registors.ulR14Abt = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm9Registors.ulSPSRAbt;

   *((uint32_t *)(pucBuffer + 0x54)) = ulTemp++; tyArm9Registors.ulR13Und = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x58)) = ulTemp++; tyArm9Registors.ulR14Und = ulTemp - 1;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm9Registors.ulSPSRUnd;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (40 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Reading again 
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = ucProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (9));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   if (tyArm9Registors.ulR0 != (*(uint32_t *)(pucBuffer + 0x04)))
      {
      PrintMessage1(ERROR_MESSAGE,"R0 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR1 != *((uint32_t *)(pucBuffer + 0x08)))
      {
      PrintMessage1(ERROR_MESSAGE,"R1 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR2 != *((uint32_t *)(pucBuffer + 0x0C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R2 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR3 != *((uint32_t *)(pucBuffer + 0x10)))
      {
      PrintMessage1(ERROR_MESSAGE,"R3 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR4 != *((uint32_t *)(pucBuffer + 0x14)))
      {
      PrintMessage1(ERROR_MESSAGE,"R4 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR5 != *((uint32_t *)(pucBuffer + 0x18)))
      {
      PrintMessage1(ERROR_MESSAGE,"R5 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR6 != *((uint32_t *)(pucBuffer + 0x1C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R6 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR7 != *((uint32_t *)(pucBuffer + 0x20)))
      {
      PrintMessage1(ERROR_MESSAGE,"R7 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   //PC No need to compare
   //CPSR Also not Comaparing
   if (tyArm9Registors.ulR8 != *((uint32_t *)(pucBuffer + 0x2C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R8 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR9 != *((uint32_t *)(pucBuffer + 0x30)))
      {
      PrintMessage1(ERROR_MESSAGE,"R9 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR10 != *((uint32_t *)(pucBuffer + 0x34)))
      {
      PrintMessage1(ERROR_MESSAGE,"R10 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR11 != *((uint32_t *)(pucBuffer + 0x38)))
      {
      PrintMessage1(ERROR_MESSAGE,"R11 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR12 != *((uint32_t *)(pucBuffer + 0x3C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R12 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR13Usr != *((uint32_t *)(pucBuffer + 0x40)))
      {
      PrintMessage1(ERROR_MESSAGE,"R13 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR14Usr != *((uint32_t *)(pucBuffer + 0x44)))
      {
      PrintMessage1(ERROR_MESSAGE,"R14 Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR8Fiq != *((uint32_t *)(pucBuffer + 0x48)))
      {
      PrintMessage1(ERROR_MESSAGE,"R8 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR9Fiq != *((uint32_t *)(pucBuffer + 0x4C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R9 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR10Fiq != *((uint32_t *)(pucBuffer + 0x50)))
      {
      PrintMessage1(ERROR_MESSAGE,"R10 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR11Fiq != *((uint32_t *)(pucBuffer + 0x54)))
      {
      PrintMessage1(ERROR_MESSAGE,"R11 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR12Fiq != *((uint32_t *)(pucBuffer + 0x58)))
      {
      PrintMessage1(ERROR_MESSAGE,"R12 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR13Fiq != *((uint32_t *)(pucBuffer + 0x5C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R13 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR14Fiq != *((uint32_t *)(pucBuffer + 0x60)))
      {
      PrintMessage1(ERROR_MESSAGE,"R14 FIQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   //SPSRFIQ not camparing
   if (tyArm9Registors.ulR13Irq != *((uint32_t *)(pucBuffer + 0x68)))
      {
      PrintMessage1(ERROR_MESSAGE,"R13 IRQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR14Irq != *((uint32_t *)(pucBuffer + 0x6C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R14 IRQ Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   //SPSRIrq not comparing
   if (tyArm9Registors.ulR13Svc != *((uint32_t *)(pucBuffer + 0x74)))
      {
      PrintMessage1(ERROR_MESSAGE,"R13 SVC Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR14Svc != *((uint32_t *)(pucBuffer + 0x78)))
      {
      PrintMessage1(ERROR_MESSAGE,"R14 SVC Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   //SPSR Svc not comparing
   if (tyArm9Registors.ulR13Abt != *((uint32_t *)(pucBuffer + 0x80)))
      {
      PrintMessage1(ERROR_MESSAGE,"R13 ABT Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   if (tyArm9Registors.ulR14Abt != *((uint32_t *)(pucBuffer + 0x84)))
      {
      PrintMessage1(ERROR_MESSAGE,"R114 ABT Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
   //SPSR ABT not comapring
   if (tyArm9Registors.ulR13Und != *((uint32_t *)(pucBuffer + 0x8C)))
      {
      PrintMessage1(ERROR_MESSAGE,"R13 UND Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      };
   if (tyArm9Registors.ulR14Und != *((uint32_t *)(pucBuffer + 0x90)))
      {
      PrintMessage1(ERROR_MESSAGE,"R14 UND Register missmatch");
      PrintMessage1(ERROR_MESSAGE,"Read and Write Register Test Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;
      }
      //SPSR UND not comparing
   PrintMessage1(INFO_MESSAGE,"Register Read and Write Test Passed OK...\n");
   return tyResult;
}
/****************************************************************************
     Function: DL_OPXD_Arm_Test_Initialise
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - pointer usb device handle
               TyDevHandle tyDevHandle - handle to open device
               unsigned char ucProcessor - Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore - nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Initailise TAP controller Bring Processor to Debug mode
Date           Initials    Description
06-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Initialise(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle, 
                                    unsigned char ucProcessor, unsigned short usCore)
{
   TyError tyResult;
   unsigned char *pucBuffer;
   uint32_t ulCount = 0;


#ifdef IMX515
   uint32_t pulBypassIRPattern = 0xFFFFFFFF;
   uint32_t ulCnt, ulOffset, ulBit, ulBitPos, ulTotalLen, ulNumberOfCores;
   int32_t iPacketLen;
#endif

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   /*---------------For IMX515-------------------*/
#ifdef IMX515
   TyTAPConfig ptyCoreConfig[3] = {
                                    {
                                     0x0005,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                    {
                                     0x0004,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                    {
                                     0x0004,
                                     0x0001,
                                     &pulBypassIRPattern
                                    }                                  
                                   };


   ulNumberOfCores = 3;
   ulTotalLen = 0;
   for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
      {
      if ((ptyCoreConfig[ulCnt].usDefaultIRLength > MAX_SCANCHAIN_LENGTH) || (ptyCoreConfig[ulCnt].pulBypassIRPattern == NULL))
         return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
      ulTotalLen += ptyCoreConfig[ulCnt].usDefaultIRLength;
      }
   if (ulTotalLen > MAX_MULTICORE_SCANCHAIN_LENGTH)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_SET_MULTICORE;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulNumberOfCores;  // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0x0000;
   ulOffset = 0x08;
   // copy each IR length to packet
   for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
      {
      *((unsigned short *)(pucBuffer + ulOffset)) = ptyCoreConfig[ulCnt].usDefaultIRLength;
      ulOffset += 2;
      }
   // do same for default DR length
   for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
      {
      *((unsigned short *)(pucBuffer + ulOffset)) = ptyCoreConfig[ulCnt].usDefaultDRLength;
      ulOffset += 2;
      }
   // ulOffset is alligned to word
   if (ulTotalLen % 32)
      *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)((ulTotalLen / 32) + 1);
   else
      *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)(ulTotalLen / 32);
   *((unsigned short *)(pucBuffer + ulOffset + 2)) = 0x0000;
   ulOffset += 4;
   // now create whole IRBypass pattern
   ulBitPos = 0;
   for (ulCnt=ulNumberOfCores; ulCnt > 0; ulCnt--)
      {
      for (ulBit=0; ulBit < ptyCoreConfig[ulCnt-1].usDefaultIRLength; ulBit++)        // process every bit
         {
         uint32_t ulCurrentBit;
         uint32_t ulMask = (0x00000001 << ulBitPos);
         ulCurrentBit = ((ptyCoreConfig[ulCnt-1].pulBypassIRPattern[ulBit/32]) >> (ulBit % 32)) & 0x1;         // get current bit from pattern
         if (ulCurrentBit)
            *((uint32_t *)(pucBuffer + ulOffset)) |= ulMask;
         else
            *((uint32_t *)(pucBuffer + ulOffset)) &= ~ulMask;
         ulBitPos++;
         if (!(ulBitPos % 32))
            {
            ulBitPos = 0;
            ulOffset += 4;          // go to next word
            }
         }
      }
   if (ulBitPos % 32)
      ulOffset += 4;                // ensure all words are written
   iPacketLen = TRANSMIT_LEN((int32_t)ulOffset);

   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer,iPacketLen);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;
   pucBuffer[0x4] = 0x0;   
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 5);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   //Bringin TAP to RTI State
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8)) = ARM11;
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 7);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
#else

//Bringin TAP to RTI State
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 6);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;



   //Set Safe Non Vector Address
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SET_SAFE_NON_VECTOR_ADDRESS;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x08] = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0xC)) = 0x10000;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 16);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x06] = 0x02;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

//Write ICE Breaker controll register debug requist
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_ICE_BREAKER;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x06] = 0;
   pucBuffer[0x07] = 0;
   *((uint32_t *)(pucBuffer + 0x8))= 0x00;
   *((uint32_t *)(pucBuffer + 0xC))= 0x02;
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 16);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {
//ARM STATUS PROC
      // create packet
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;     // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore number (0 if MC support disabled)
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = ucProcessor ; //ucPROCESSOR
      pucBuffer[0x09] = 0x1; //Scan chain
      pucBuffer[0x0a] = 0x1; //Execution status
      // send packet
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;

      ulCount++;
      if (ulCount == 500)
         {
         PrintMessage1(ERROR_MESSAGE,"Bring Processor to Debug mode Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
         }
      }while (pucBuffer[0x05]  != 0x0);


   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x06] = 0x01;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
#endif   
   return tyResult;

}

/****************************************************************************
     Function: DL_OPXD_Arm_Test_Initialise
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - pointer usb device handle
               TyDevHandle tyDevHandle - handle to open device
               uint32_t ulAddress - Memory address to test
               unsigned char ucProcessor - Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore - nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Memory Test
Date           Initials    Description
06-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Memory(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle,
                                uint32_t ulAddress, unsigned char ucProcessor, 
                                unsigned short usCore)
{
   TyError tyResult;
   unsigned char *pucBuffer;
   unsigned char ucI;
   uint32_t *pulTempPointer;
   int32_t iPacketLen;
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;


   //Single Word Read and write
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_WORD;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 0xA5A55A5A; 
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_WORD;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (4 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
   if (*((uint32_t *)(pucBuffer + 0x04)) != 0xA5A55A5A)
      {
      PrintMessage1(ERROR_MESSAGE,"Single Word Memory Read and Write Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;         
      }

   //Multiple word Memory Read Write
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_MULTIPLE_WORDS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 10;
   pulTempPointer = ((uint32_t *)(pucBuffer + 0x14));
   for (ucI = 0; ucI < 10; ucI++)
      {
      *pulTempPointer = ucI;
      pulTempPointer++;
      }
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_MULTIPLE_WORDS;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress;
   *((uint32_t *)(pucBuffer + 0x10)) = 10;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
   pulTempPointer = ((uint32_t *)(pucBuffer + 0x04));
   for (ucI = 0; ucI < 10 ; ucI++)
      {
      if (*pulTempPointer != ucI)
         {
         PrintMessage1(ERROR_MESSAGE,"Multiple Word Memory Read and Write Failed!!");
         return DRVOPXD_ERROR_DATA_MISSMATCH;         
         }
      pulTempPointer++;
      }

   //Multiple Blocks Memory Read Write
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 2;
   pulTempPointer = ((uint32_t *)(pucBuffer + 0x14));
   for (ucI = 0; ucI < (2 * 14); ucI++)
      {
      *pulTempPointer = ucI;
      pulTempPointer++;
      }

   iPacketLen = TRANSMIT_LEN(5 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   iPacketLen = TRANSMIT_LEN(2 * 14 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)(pucBuffer + 0x14), iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      return DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult != ERR_NO_ERROR )
      {
      printf("Command execution Error\n");
      return  tyResult;
      }      

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_MULTIPLE_BLOCKS;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress;
   *((uint32_t *)(pucBuffer + 0x10)) = 2;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
   pulTempPointer = ((uint32_t *)(pucBuffer + 0x04));
   for (ucI = 0; ucI < ( 2 * 14) ; ucI++)
      {
      if (*pulTempPointer != ucI)
         {
         PrintMessage1(ERROR_MESSAGE,"Multiple Block Memory Read and Write Failed!!");
         return DRVOPXD_ERROR_DATA_MISSMATCH;         
         }
      pulTempPointer++;
      }

   //Byte Read and write
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_BYTE;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 0xA5; 
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_BYTE;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 1; 
   *((uint32_t *)(pucBuffer + 0x10)) = 0x12; 
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_WORD;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ; 
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (4 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;
   if (*((unsigned short *)(pucBuffer + 0x04)) != 0x12A5)
      {
      PrintMessage1(ERROR_MESSAGE,"Byte Memory Read and Write Failed!!");
      return DRVOPXD_ERROR_DATA_MISSMATCH;         
      }

   PrintMessage1(INFO_MESSAGE,"Memory Read and Write Test Passed OK...\n");


   return  tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_Test_Execute_SB
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - USB device handle
               TyDevHandle tyDevHandle - handle to open device
               uint32_t ulAddress - OnChipMemory address
               unsigned char ucProcessor Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Tests Execution /Software break point
Date           Initials    Description
06-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Execute_SB(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle,
                                    uint32_t ulAddress, unsigned char ucProcessor, 
                                    unsigned short usCore)
{
   TyError tyResult;
   unsigned char *pucBuffer;
   uint32_t ulCount = 0; // Counter for loop checking of dubug status register
   int32_t iPacketLen;
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   TyArm9Registors tyArm9Registors;

   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

//loading program   
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;  // command code
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 1;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xE1A06007; //MOV R0, R7
   *((uint32_t *)(pucBuffer + 0x18)) = 0xE1A04005; //MOV R4, R5
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xEEEEEEEE; //Software Breack Point
   *((uint32_t *)(pucBuffer + 0x20)) = 0xE1A06007; //MOV R6, R7
   *((uint32_t *)(pucBuffer + 0x24)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x28)) = 0xE1A02003; //MOV R2, R3
   *((uint32_t *)(pucBuffer + 0x2C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x30)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x34)) = 0xE1A00000; //NOP;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x40)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x44)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x48)) = 0xE1A00000; //NOP


   iPacketLen = TRANSMIT_LEN(5 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   iPacketLen = TRANSMIT_LEN(1 * 14 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)(pucBuffer + 0x14), iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      return DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xEEEEEEEE;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x104;
   *((uint32_t *)(pucBuffer + 0x20)) = 0xF7;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_CHECK_FOR_DATA_ABORT;
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x8)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm9Registors.ulSPSRFiq ;
   *((uint32_t *)(pucBuffer + 0x10)) = tyArm9Registors.ulR8Fiq;
   *((uint32_t *)(pucBuffer + 0x14)) = tyArm9Registors.ulR9Fiq;
   *((uint32_t *)(pucBuffer + 0x18)) = tyArm9Registors.ulR10Fiq;
   *((uint32_t *)(pucBuffer + 0x1C)) = tyArm9Registors.ulR11Fiq;
   *((uint32_t *)(pucBuffer + 0x20)) = tyArm9Registors.ulR12Fiq;
   *((uint32_t *)(pucBuffer + 0x24)) = tyArm9Registors.ulR13Fiq;
   *((uint32_t *)(pucBuffer + 0x28)) = tyArm9Registors.ulR14Fiq;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm9Registors.ulSPSRIrq;
   *((uint32_t *)(pucBuffer + 0x30)) = tyArm9Registors.ulR13Irq;
   *((uint32_t *)(pucBuffer + 0x34)) = tyArm9Registors.ulR14Irq;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm9Registors.ulSPSRSvc;
   *((uint32_t *)(pucBuffer + 0x3C)) = tyArm9Registors.ulR13Svc;
   *((uint32_t *)(pucBuffer + 0x40)) = tyArm9Registors.ulR14Svc;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm9Registors.ulSPSRAbt;
   *((uint32_t *)(pucBuffer + 0x48)) = tyArm9Registors.ulR13Abt;
   *((uint32_t *)(pucBuffer + 0x4C)) = tyArm9Registors.ulR14Abt;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm9Registors.ulSPSRUnd;
   *((uint32_t *)(pucBuffer + 0x54)) = tyArm9Registors.ulR13Und;
   *((uint32_t *)(pucBuffer + 0x58)) = tyArm9Registors.ulR14Und;
   *((uint32_t *)(pucBuffer + 0x5C)) = tyArm9Registors.ulR8;
   *((uint32_t *)(pucBuffer + 0x60)) = tyArm9Registors.ulR9;
   *((uint32_t *)(pucBuffer + 0x64)) = tyArm9Registors.ulR10;
   *((uint32_t *)(pucBuffer + 0x68)) = tyArm9Registors.ulR11;
   *((uint32_t *)(pucBuffer + 0x6C)) = tyArm9Registors.ulR12;
   *((uint32_t *)(pucBuffer + 0x70)) = tyArm9Registors.ulR13Usr;
   *((uint32_t *)(pucBuffer + 0x74)) = tyArm9Registors.ulR14Usr;
   *((uint32_t *)(pucBuffer + 0x78)) = tyArm9Registors.ulCPSR;
   *((uint32_t *)(pucBuffer + 0x7C)) = tyArm9Registors.ulR0;
   *((uint32_t *)(pucBuffer + 0x80)) = tyArm9Registors.ulR1;
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2; //tyArm9Registors.ulR2;
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3; //tyArm9Registors.ulR3;
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4; //tyArm9Registors.ulR4;
   *((uint32_t *)(pucBuffer + 0x90)) = 0x5; //tyArm9Registors.ulR5;
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6; // tyArm9Registors.ulR6;
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7; //tyArm9Registors.ulR7;
   *((uint32_t *)(pucBuffer + 0x9C)) = (ulAddress + 4); //tyArm9Registors.ulPC;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (160));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_EXECUTE_PROC;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));

   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x02;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {
//ARM STATUS PROC
      // create packet
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;     // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore1 number (0 if MC support disabled)
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = ucProcessor ; //ucPROCESSOR
      pucBuffer[0x09] = 0x1; //Scan chain
      pucBuffer[0x0a] = 0x1; //Execution status
      // send packet
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      ulCount++;
      if (ulCount == 500)
      {
         PrintMessage1(ERROR_MESSAGE,"Processor not return to debug mode.");
         PrintMessage1(ERROR_MESSAGE,"Execute / Software Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
      }while (pucBuffer[0x05]  != 0x0);

   //reset Watch point registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 0x8;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x01;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

//Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 8);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

   DL_OPXD_UnlockDevice(tyDevHandle);
   if (tyArm9Registors.ulR4 != 0x5 || tyArm9Registors.ulR5 != 0x5 || 
       tyArm9Registors.ulR6 != 0x6 || tyArm9Registors.ulR7 != 0x7 )
      {
         PrintMessage1(ERROR_MESSAGE,"Execution Error");
         PrintMessage1(ERROR_MESSAGE,"Execute / Software Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
   PrintMessage1(INFO_MESSAGE, "Execute / Software Break Point Passed OK...\n");
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_Test_Execute_SB_7DI
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - USB device handle
               TyDevHandle tyDevHandle - handle to open device
               uint32_t ulAddress - OnChipMemory address
               unsigned char ucProcessor Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Tests Execution /Software break point //Special for 7DI core
Date           Initials    Description
06-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Execute_SB_7DI(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle,
                                        uint32_t ulAddress, unsigned char ucProcessor, 
                                        unsigned short usCore)
{
   TyError tyResult;
   unsigned char *pucBuffer;
   uint32_t ulCount = 0; // Counter for loop checking of dubug status register
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   TyArm9Registors tyArm9Registors;
   int32_t iPacketLen;

   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

//loading program   
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;  // command code
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 1;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xE1A06007; //MOV R0, R7
   *((uint32_t *)(pucBuffer + 0x18)) = 0xE1A04005; //MOV R4, R5
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xEEEEEEEE; //Software Breack Point
   *((uint32_t *)(pucBuffer + 0x20)) = 0xE1A06007; //MOV R6, R7
   *((uint32_t *)(pucBuffer + 0x24)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x28)) = 0xE1A02003; //MOV R2, R3
   *((uint32_t *)(pucBuffer + 0x2C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x30)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x34)) = 0xE1A00000; //NOP;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x40)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x44)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x48)) = 0xE1A00000; //NOP


   iPacketLen = TRANSMIT_LEN(5 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   iPacketLen = TRANSMIT_LEN(1 * 14 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)(pucBuffer + 0x14), iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      return DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xEEEEEEEE;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x80; //0x086; for ARM7DI //0x104;
   *((uint32_t *)(pucBuffer + 0x20)) = 0xFB; //0xF9;  for ARM7DI //0xF7;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_CHECK_FOR_DATA_ABORT;
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x8)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm9Registors.ulSPSRFiq ;
   *((uint32_t *)(pucBuffer + 0x10)) = tyArm9Registors.ulR8Fiq;
   *((uint32_t *)(pucBuffer + 0x14)) = tyArm9Registors.ulR9Fiq;
   *((uint32_t *)(pucBuffer + 0x18)) = tyArm9Registors.ulR10Fiq;
   *((uint32_t *)(pucBuffer + 0x1C)) = tyArm9Registors.ulR11Fiq;
   *((uint32_t *)(pucBuffer + 0x20)) = tyArm9Registors.ulR12Fiq;
   *((uint32_t *)(pucBuffer + 0x24)) = tyArm9Registors.ulR13Fiq;
   *((uint32_t *)(pucBuffer + 0x28)) = tyArm9Registors.ulR14Fiq;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm9Registors.ulSPSRIrq;
   *((uint32_t *)(pucBuffer + 0x30)) = tyArm9Registors.ulR13Irq;
   *((uint32_t *)(pucBuffer + 0x34)) = tyArm9Registors.ulR14Irq;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm9Registors.ulSPSRSvc;
   *((uint32_t *)(pucBuffer + 0x3C)) = tyArm9Registors.ulR13Svc;
   *((uint32_t *)(pucBuffer + 0x40)) = tyArm9Registors.ulR14Svc;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm9Registors.ulSPSRAbt;
   *((uint32_t *)(pucBuffer + 0x48)) = tyArm9Registors.ulR13Abt;
   *((uint32_t *)(pucBuffer + 0x4C)) = tyArm9Registors.ulR14Abt;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm9Registors.ulSPSRUnd;
   *((uint32_t *)(pucBuffer + 0x54)) = tyArm9Registors.ulR13Und;
   *((uint32_t *)(pucBuffer + 0x58)) = tyArm9Registors.ulR14Und;
   *((uint32_t *)(pucBuffer + 0x5C)) = tyArm9Registors.ulR8;
   *((uint32_t *)(pucBuffer + 0x60)) = tyArm9Registors.ulR9;
   *((uint32_t *)(pucBuffer + 0x64)) = tyArm9Registors.ulR10;
   *((uint32_t *)(pucBuffer + 0x68)) = tyArm9Registors.ulR11;
   *((uint32_t *)(pucBuffer + 0x6C)) = tyArm9Registors.ulR12;
   *((uint32_t *)(pucBuffer + 0x70)) = tyArm9Registors.ulR13Usr;
   *((uint32_t *)(pucBuffer + 0x74)) = tyArm9Registors.ulR14Usr;
   *((uint32_t *)(pucBuffer + 0x78)) = tyArm9Registors.ulCPSR;
   *((uint32_t *)(pucBuffer + 0x7C)) = tyArm9Registors.ulR0;
   *((uint32_t *)(pucBuffer + 0x80)) = tyArm9Registors.ulR1;
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2; //tyArm9Registors.ulR2;
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3; //tyArm9Registors.ulR3;
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4; //tyArm9Registors.ulR4;
   *((uint32_t *)(pucBuffer + 0x90)) = 0x5; //tyArm9Registors.ulR5;
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6; // tyArm9Registors.ulR6;
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7; //tyArm9Registors.ulR7;
   *((uint32_t *)(pucBuffer + 0x9C)) = (ulAddress + 4); //tyArm9Registors.ulPC;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (160));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_EXECUTE_PROC;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));

   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x02;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {
//ARM STATUS PROC
      // create packet
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;     // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore1 number (0 if MC support disabled)
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = ucProcessor ; //ucPROCESSOR
      pucBuffer[0x09] = 0x1; //Scan chain
      pucBuffer[0x0a] = 0x1; //Execution status
      // send packet
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      ulCount++;
      if (ulCount == 500)
      {
         PrintMessage1(ERROR_MESSAGE,"Processor not return to debug mode.");
         PrintMessage1(ERROR_MESSAGE,"Execute / Software Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
      }while (pucBuffer[0x05]  != 0x0);

   //reset Watch point registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 0x8;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x01;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

//Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 8);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

   DL_OPXD_UnlockDevice(tyDevHandle);
   if (tyArm9Registors.ulR4 != 0x5 || tyArm9Registors.ulR5 != 0x5 || 
       tyArm9Registors.ulR6 != 0x6 || tyArm9Registors.ulR7 != 0x7 )
      {
         PrintMessage1(ERROR_MESSAGE,"Execution Error");
         PrintMessage1(ERROR_MESSAGE,"Execute / Software Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
   PrintMessage1(INFO_MESSAGE, "Execute / Software Break Point Passed OK...\n");
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_Test_Execute_HB
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - USB device handle
               TyDevHandle tyDevHandle - handle to open device
               uint32_t ulAddress - OnChipMemory address
               unsigned char ucProcessor Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Tests Execution and Hardware break point
Date           Initials    Description
06-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Execute_HB(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle,
                                    uint32_t ulAddress, unsigned char ucProcessor,
                                    unsigned short usCore)
{
   TyError tyResult;
   unsigned char *pucBuffer;
   uint32_t ulCount = 0; // Counter for loop checking of dubug status register
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   TyArm9Registors tyArm9Registors;
   int32_t iPacketLen;

   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

//loading program   
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;  // command code
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 1;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xE1A06007; //MOV R0, R7
   *((uint32_t *)(pucBuffer + 0x18)) = 0xE1A04005; //MOV R4, R5 //this is the executing instruction
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xE1A06007; //MOV R6, R7
   *((uint32_t *)(pucBuffer + 0x20)) = 0xE1A06007; //MOV R6, R7
   *((uint32_t *)(pucBuffer + 0x24)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x28)) = 0xE1A02003; //MOV R2, R3
   *((uint32_t *)(pucBuffer + 0x2C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x30)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x34)) = 0xE1A00000; //NOP;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x40)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x44)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x48)) = 0xE1A00000; //NOP


   iPacketLen = TRANSMIT_LEN(5 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   iPacketLen = TRANSMIT_LEN(1 * 14 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)(pucBuffer + 0x14), iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      return DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 0x8;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x18)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x103;
   *((uint32_t *)(pucBuffer + 0x20)) = 0xF7;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_CHECK_FOR_DATA_ABORT;
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x8)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm9Registors.ulSPSRFiq ;
   *((uint32_t *)(pucBuffer + 0x10)) = tyArm9Registors.ulR8Fiq;
   *((uint32_t *)(pucBuffer + 0x14)) = tyArm9Registors.ulR9Fiq;
   *((uint32_t *)(pucBuffer + 0x18)) = tyArm9Registors.ulR10Fiq;
   *((uint32_t *)(pucBuffer + 0x1C)) = tyArm9Registors.ulR11Fiq;
   *((uint32_t *)(pucBuffer + 0x20)) = tyArm9Registors.ulR12Fiq;
   *((uint32_t *)(pucBuffer + 0x24)) = tyArm9Registors.ulR13Fiq;
   *((uint32_t *)(pucBuffer + 0x28)) = tyArm9Registors.ulR14Fiq;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm9Registors.ulSPSRIrq;
   *((uint32_t *)(pucBuffer + 0x30)) = tyArm9Registors.ulR13Irq;
   *((uint32_t *)(pucBuffer + 0x34)) = tyArm9Registors.ulR14Irq;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm9Registors.ulSPSRSvc;
   *((uint32_t *)(pucBuffer + 0x3C)) = tyArm9Registors.ulR13Svc;
   *((uint32_t *)(pucBuffer + 0x40)) = tyArm9Registors.ulR14Svc;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm9Registors.ulSPSRAbt;
   *((uint32_t *)(pucBuffer + 0x48)) = tyArm9Registors.ulR13Abt;
   *((uint32_t *)(pucBuffer + 0x4C)) = tyArm9Registors.ulR14Abt;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm9Registors.ulSPSRUnd;
   *((uint32_t *)(pucBuffer + 0x54)) = tyArm9Registors.ulR13Und;
   *((uint32_t *)(pucBuffer + 0x58)) = tyArm9Registors.ulR14Und;
   *((uint32_t *)(pucBuffer + 0x5C)) = tyArm9Registors.ulR8;
   *((uint32_t *)(pucBuffer + 0x60)) = tyArm9Registors.ulR9;
   *((uint32_t *)(pucBuffer + 0x64)) = tyArm9Registors.ulR10;
   *((uint32_t *)(pucBuffer + 0x68)) = tyArm9Registors.ulR11;
   *((uint32_t *)(pucBuffer + 0x6C)) = tyArm9Registors.ulR12;
   *((uint32_t *)(pucBuffer + 0x70)) = tyArm9Registors.ulR13Usr;
   *((uint32_t *)(pucBuffer + 0x74)) = tyArm9Registors.ulR14Usr;
   *((uint32_t *)(pucBuffer + 0x78)) = tyArm9Registors.ulCPSR;
   *((uint32_t *)(pucBuffer + 0x7C)) = tyArm9Registors.ulR0;
   *((uint32_t *)(pucBuffer + 0x80)) = tyArm9Registors.ulR1;
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2; //tyArm9Registors.ulR2;
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3; //tyArm9Registors.ulR3;
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4; //tyArm9Registors.ulR4;
   *((uint32_t *)(pucBuffer + 0x90)) = 0x5; //tyArm9Registors.ulR5;
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6; // tyArm9Registors.ulR6;
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7; //tyArm9Registors.ulR7;
   *((uint32_t *)(pucBuffer + 0x9C)) = (ulAddress + 4); //tyArm9Registors.ulPC;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (160));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_EXECUTE_PROC;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));

   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x02;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {

//ARM STATUS PROC
      // create packet
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;     // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore1 number (0 if MC support disabled)
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = ucProcessor ; //ucPROCESSOR
      pucBuffer[0x09] = 0x1; //Scan chain
      pucBuffer[0x0a] = 0x1; //Execution status
      // send packet
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      ulCount++;
      if (ulCount == 500)
      {
         PrintMessage1(ERROR_MESSAGE,"Processor not return to debug mode.");
         PrintMessage1(ERROR_MESSAGE,"Hardware Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
      }while (pucBuffer[0x05]  != 0x0);

   //reset Watch point registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 0x8;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x01;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 8);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

   DL_OPXD_UnlockDevice(tyDevHandle);
   if (tyArm9Registors.ulR4 != 0x5 || tyArm9Registors.ulR5 != 0x5 || 
       tyArm9Registors.ulR6 != 0x6 || tyArm9Registors.ulR7 != 0x7 )
      {
         PrintMessage1(ERROR_MESSAGE,"Execution Error");
         PrintMessage1(ERROR_MESSAGE,"Hardware Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
   PrintMessage1(INFO_MESSAGE, "Hardware Break Point Passed OK...\n");
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_Test_Execute_HB_7DI
     Engineer: Shameerudheen PT.
        Input: usb_dev_handle *ptyUsbDevHandle - USB device handle
               TyDevHandle tyDevHandle - handle to open device
               uint32_t ulAddress - OnChipMemory address
               unsigned char ucProcessor Processor ARM7(1), ARM7S(2), ARM9(3)
               unsigned char ucCore nomulticore(0), firstcore(0), second core(1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: Tests Execution and Hardware break point, special for 7DI core
Date           Initials    Description
06-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Execute_HB_7DI(usb_dev_handle *ptyUsbDevHandle, TyDevHandle tyDevHandle,
                                        uint32_t ulAddress, unsigned char ucProcessor, 
                                        unsigned short usCore)
{
   TyError tyResult;
   unsigned char *pucBuffer;
   uint32_t ulCount = 0; // Counter for loop checking of dubug status register
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   TyArm9Registors tyArm9Registors;
   int32_t iPacketLen;

   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

//loading program   
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;  // command code
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) = 1;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xE1A06007; //MOV R0, R7
   *((uint32_t *)(pucBuffer + 0x18)) = 0xE1A04005; //MOV R4, R5 //this is the executing instruction
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xE1A06007; //MOV R6, R7
   *((uint32_t *)(pucBuffer + 0x20)) = 0xE1A06007; //MOV R6, R7
   *((uint32_t *)(pucBuffer + 0x24)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x28)) = 0xE1A02003; //MOV R2, R3
   *((uint32_t *)(pucBuffer + 0x2C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x30)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x34)) = 0xE1A00000; //NOP;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x40)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x44)) = 0xE1A00000; //NOP
   *((uint32_t *)(pucBuffer + 0x48)) = 0xE1A00000; //NOP


   iPacketLen = TRANSMIT_LEN(5 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   iPacketLen = TRANSMIT_LEN(1 * 14 * 4);
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)(pucBuffer + 0x14), iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      return DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      return DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 0x8;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x18)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x80;
   *((uint32_t *)(pucBuffer + 0x20)) = 0xFB;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_CHECK_FOR_DATA_ABORT;
   *((uint32_t *)(pucBuffer + 0x4)) = usCore;
   *((uint32_t *)(pucBuffer + 0x8)) = ucProcessor ;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = tyArm9Registors.ulSPSRFiq ;
   *((uint32_t *)(pucBuffer + 0x10)) = tyArm9Registors.ulR8Fiq;
   *((uint32_t *)(pucBuffer + 0x14)) = tyArm9Registors.ulR9Fiq;
   *((uint32_t *)(pucBuffer + 0x18)) = tyArm9Registors.ulR10Fiq;
   *((uint32_t *)(pucBuffer + 0x1C)) = tyArm9Registors.ulR11Fiq;
   *((uint32_t *)(pucBuffer + 0x20)) = tyArm9Registors.ulR12Fiq;
   *((uint32_t *)(pucBuffer + 0x24)) = tyArm9Registors.ulR13Fiq;
   *((uint32_t *)(pucBuffer + 0x28)) = tyArm9Registors.ulR14Fiq;
   *((uint32_t *)(pucBuffer + 0x2C)) = tyArm9Registors.ulSPSRIrq;
   *((uint32_t *)(pucBuffer + 0x30)) = tyArm9Registors.ulR13Irq;
   *((uint32_t *)(pucBuffer + 0x34)) = tyArm9Registors.ulR14Irq;
   *((uint32_t *)(pucBuffer + 0x38)) = tyArm9Registors.ulSPSRSvc;
   *((uint32_t *)(pucBuffer + 0x3C)) = tyArm9Registors.ulR13Svc;
   *((uint32_t *)(pucBuffer + 0x40)) = tyArm9Registors.ulR14Svc;
   *((uint32_t *)(pucBuffer + 0x44)) = tyArm9Registors.ulSPSRAbt;
   *((uint32_t *)(pucBuffer + 0x48)) = tyArm9Registors.ulR13Abt;
   *((uint32_t *)(pucBuffer + 0x4C)) = tyArm9Registors.ulR14Abt;
   *((uint32_t *)(pucBuffer + 0x50)) = tyArm9Registors.ulSPSRUnd;
   *((uint32_t *)(pucBuffer + 0x54)) = tyArm9Registors.ulR13Und;
   *((uint32_t *)(pucBuffer + 0x58)) = tyArm9Registors.ulR14Und;
   *((uint32_t *)(pucBuffer + 0x5C)) = tyArm9Registors.ulR8;
   *((uint32_t *)(pucBuffer + 0x60)) = tyArm9Registors.ulR9;
   *((uint32_t *)(pucBuffer + 0x64)) = tyArm9Registors.ulR10;
   *((uint32_t *)(pucBuffer + 0x68)) = tyArm9Registors.ulR11;
   *((uint32_t *)(pucBuffer + 0x6C)) = tyArm9Registors.ulR12;
   *((uint32_t *)(pucBuffer + 0x70)) = tyArm9Registors.ulR13Usr;
   *((uint32_t *)(pucBuffer + 0x74)) = tyArm9Registors.ulR14Usr;
   *((uint32_t *)(pucBuffer + 0x78)) = tyArm9Registors.ulCPSR;
   *((uint32_t *)(pucBuffer + 0x7C)) = tyArm9Registors.ulR0;
   *((uint32_t *)(pucBuffer + 0x80)) = tyArm9Registors.ulR1;
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2; //tyArm9Registors.ulR2;
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3; //tyArm9Registors.ulR3;
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4; //tyArm9Registors.ulR4;
   *((uint32_t *)(pucBuffer + 0x90)) = 0x5; //tyArm9Registors.ulR5;
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6; // tyArm9Registors.ulR6;
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7; //tyArm9Registors.ulR7;
   *((uint32_t *)(pucBuffer + 0x9C)) = (ulAddress + 4); //tyArm9Registors.ulPC;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (160));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_EXECUTE_PROC;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));

   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x02;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {
//ARM STATUS PROC
      // create packet
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;     // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore1 number (0 if MC support disabled)
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = ucProcessor ; //ucPROCESSOR
      pucBuffer[0x09] = 0x1; //Scan chain
      pucBuffer[0x0a] = 0x1; //Execution status
      // send packet
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      ulCount++;
      if (ulCount == 500)
      {
         PrintMessage1(ERROR_MESSAGE,"Processor not return to debug mode.");
         PrintMessage1(ERROR_MESSAGE,"Hardware Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
      }while (pucBuffer[0x05]  != 0x0);

   //reset Watch point registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;  // command code
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0x0C)) = ulAddress + 0x8;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x01;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore;
   *((uint32_t *)(pucBuffer + 0x08)) = ucProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 8);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   tyArm9Registors.ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   tyArm9Registors.ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   tyArm9Registors.ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   tyArm9Registors.ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   tyArm9Registors.ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   tyArm9Registors.ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   tyArm9Registors.ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   tyArm9Registors.ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   tyArm9Registors.ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   tyArm9Registors.ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   tyArm9Registors.ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   tyArm9Registors.ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   tyArm9Registors.ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   tyArm9Registors.ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   tyArm9Registors.ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   tyArm9Registors.ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   tyArm9Registors.ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   tyArm9Registors.ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   tyArm9Registors.ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   tyArm9Registors.ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   tyArm9Registors.ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   tyArm9Registors.ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   tyArm9Registors.ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   tyArm9Registors.ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   tyArm9Registors.ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   tyArm9Registors.ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   tyArm9Registors.ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   tyArm9Registors.ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   tyArm9Registors.ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   tyArm9Registors.ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   tyArm9Registors.ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   tyArm9Registors.ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   tyArm9Registors.ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   tyArm9Registors.ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   tyArm9Registors.ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   tyArm9Registors.ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   tyArm9Registors.ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));

   DL_OPXD_UnlockDevice(tyDevHandle);
   if (tyArm9Registors.ulR4 != 0x5 || tyArm9Registors.ulR5 != 0x5 || 
       tyArm9Registors.ulR6 != 0x6 || tyArm9Registors.ulR7 != 0x7 )
      {
         PrintMessage1(ERROR_MESSAGE,"Execution Error");
         PrintMessage1(ERROR_MESSAGE,"Hardware Break Point Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
      }
   PrintMessage1(INFO_MESSAGE, "Hardware Break Point Passed OK...\n");
   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_Arm7TDMI
     Engineer: Shameerudheen PT.
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test ARM7TDMI Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
05-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Arm7TDMI(TyDevHandle tyDevHandle, unsigned char ucTestOnchipMem, 
                                  uint32_t ulOnChipMemory, unsigned short usCore)
{
   const unsigned char ucProcessor = 1; //1 for ARM7 2 ARM7S 3 ARM9

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   tyResult = DL_OPXD_Arm_Test_Initialise(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //REG_READ_WRITE
   tyResult = DL_OPXD_Arm_Test_Register(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //ON_CHIP_MEMORY
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Memory(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_SB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_SB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_HB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_HB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Bring PROCESSOR into normal mode
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                   
   pucBuffer[0x06] = 0x00;
   pucBuffer[0x07] = 0x0;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (8));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   DL_OPXD_UnlockDevice(tyDevHandle);

   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_Arm7DI
     Engineer: Shameerudheen PT.
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test ARM7DI Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
05-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Arm7DI(TyDevHandle tyDevHandle, unsigned char ucTestOnchipMem, 
                                uint32_t ulOnChipMemory, unsigned short usCore)
{
   const unsigned char ucProcessor = 1;
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   tyResult = DL_OPXD_Arm_Test_Initialise(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //REG_READ_WRITE
   tyResult = DL_OPXD_Arm_Test_Register(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //ON_CHIP_MEMORY
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Memory(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_SB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_SB_7DI(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_HB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_HB_7DI(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Bring Processor into normal mode
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                   
   pucBuffer[0x06] = 0x00;
   pucBuffer[0x07] = 0x0;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (8));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   DL_OPXD_UnlockDevice(tyDevHandle);

   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_Arm7TDMIS
     Engineer: Shameerudheen PT.
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucTestOnchipMem - 1 Test onchip memory, 0 don't test
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test ARM7TMIS Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
07-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_Test_Arm7TDMIS(TyDevHandle tyDevHandle, unsigned char ucTestOnchipMem, 
                                   uint32_t ulOnChipMemory, unsigned short usCore)
{
   const unsigned char ucProcessor = 2; //1 fro ARM7 2 ARM7S 3 ARM9

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   tyResult = DL_OPXD_Arm_Test_Initialise(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //REG_READ_WRITE
   tyResult = DL_OPXD_Arm_Test_Register(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //ON_CHIP_MEMORY
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Memory(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_SB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_SB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_HB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_HB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Bring Processor into normal mode
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST; 
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                    
   pucBuffer[0x06] = 0x00;
   pucBuffer[0x07] = 0x0;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (8));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   DL_OPXD_UnlockDevice(tyDevHandle);

   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_Arm946ES
     Engineer: Shameerudheen PT.
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucTestOnchipMem - 1 Test onchip memory, 0 don't test
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test ARM946ES Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
07-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_TEST_Arm946ES(TyDevHandle tyDevHandle, unsigned char ucTestOnchipMem, 
                                  uint32_t ulOnChipMemory, unsigned short usCore)
{
   const unsigned char ucProcessor = 3; //1 fro ARM7 2 ARM7S 3 ARM9


   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   tyResult = DL_OPXD_Arm_Test_Initialise(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //REG_READ_WRITE
   tyResult = DL_OPXD_Arm_Test_Register(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   //ON_CHIP_MEMORY
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Memory(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_SB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_SB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_HB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_HB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, ucProcessor, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }


   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Bring PROCESSOR into normal mode
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x00;
   pucBuffer[0x07] = 0x0;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (8));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   DL_OPXD_UnlockDevice(tyDevHandle);

   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_CortexA8
     Engineer: Jeenus Chalattu Kunnath.
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucTestOnchipMem - 1 Test onchip memory, 0 don't test
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test Cortex M3 Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
24-SEP-2009    JCK          Initial
****************************************************************************/
TyError DL_OPXD_Arm_TEST_CortexA8(
    TyDevHandle tyDevHandle, 
    unsigned char ucTestOnchipMem, 
    uint32_t ulOnChipMemory, 
    unsigned short usCore
    )
{
    
    int32_t iPacketLen;
    const unsigned char ucProcessor = 3; //1 fro ARM7 2 ARM7S 3 ARM9

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;   
   TyTmsSequence ptyTmsForScanIR, ptyTmsForScanDR, ptyTmsForScanIRandDR_IR, ptyTmsForScanIRandDR_DR;
   uint32_t pulBypassIRPattern = 0xFFFFFFFF;
   uint32_t ulCnt, ulOffset, ulBit, ulBitPos, ulTotalLen, ulNumberOfCores;
   uint32_t ulDataArray[3];
   uint32_t ulDatatoScan;
   uint32_t ulDataFromScan;
   
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   NOREF(ulOnChipMemory);
   NOREF(ucTestOnchipMem);

   tyResult = DL_OPXD_LockDevice(tyDevHandle);

/*------------------For OMAP3530--------------------*/
   
/*--------------------------------------------------------------------------------------------------------*/
											// Reset DAP
/*--------------------------------------------------------------------------------------------------------*/
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_RESET_DAP; // command code

   iPacketLen = TRANSMIT_LEN(4);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											//Reseting TAP
/*--------------------------------------------------------------------------------------------------------*/
    
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;
	pucBuffer[0x4] = 0x0;

    iPacketLen = TRANSMIT_LEN(5);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

/*--------------------------------------------------------------------------------------------------------*/
											//Bringing TLR to RTI
/*--------------------------------------------------------------------------------------------------------*/
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                     // reserved
   *((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)0x11;   // core type CORTEXA8 = 0x11

    iPacketLen = TRANSMIT_LEN(9);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

/*--------------------------------------------------------------------------------------------------------*/
											//Reseting TAP
/*--------------------------------------------------------------------------------------------------------*/
    
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;
	pucBuffer[0x4] = 0x0;

    iPacketLen = TRANSMIT_LEN(5);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											/* ML_Config_ICEPICK */
/*--------------------------------------------------------------------------------------------------------*/
 
	ulDatatoScan = 0x7;
	tyResult = DL_OPXD_JtagScanIR (tyDevHandle,0,0x6, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	ulDatatoScan = 0x89;
	tyResult = DL_OPXD_JtagScanDR (tyDevHandle,0,8, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}
	tyResult = DL_OPXD_JtagScanDR (tyDevHandle,0,8, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	ulDatatoScan = 0x2;
	tyResult = DL_OPXD_JtagScanIR (tyDevHandle,0,0x6, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}
	
	ulDatatoScan = 0xA0002108 | (0x3 << 24);
	tyResult = DL_OPXD_JtagScanDR (tyDevHandle,0,32, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	ulDatatoScan = 0x20000000 | (0x3 << 24);
	tyResult = DL_OPXD_JtagScanDR (tyDevHandle,0,32, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	ulDatatoScan = 0x20000000 | (0x3 << 24);
	tyResult = DL_OPXD_JtagScanDR (tyDevHandle,0,32, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	ulDatatoScan = 0xFFFFFFFF;
	tyResult = DL_OPXD_JtagScanIR (tyDevHandle,0,32, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	ulDatatoScan = 0x04E;
	tyResult = DL_OPXD_JtagScanIR (tyDevHandle,0,10, &ulDatatoScan, &ulDataFromScan);
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
	{	
		return tyResult;					
	}

	{
		uint32_t ulTemp[2]; 

		ulDatatoScan = 0;
		tyResult = DL_OPXD_JtagScanDR (tyDevHandle,0,64, &ulDatatoScan, ulTemp);
		if (tyResult != DRVOPXD_ERROR_NO_ERROR)
		{	
			return tyResult;					
		}	
	}
/*--------------------------------------------------------------------------------------------------------*/
											/* Setting TMS */
/*--------------------------------------------------------------------------------------------------------*/

   ptyTmsForScanIR.usPostTmsBits  = 0xD;
   ptyTmsForScanIR.ucPostTmsCount = 0x5;
   ptyTmsForScanIR.usPreTmsBits   = 0x3;
   ptyTmsForScanIR.ucPreTmsCount  = 0x4;

   ptyTmsForScanIRandDR_IR = ptyTmsForScanIR;

   ptyTmsForScanDR.usPostTmsBits  = 0xD;
   ptyTmsForScanDR.ucPostTmsCount = 0x5;
   ptyTmsForScanDR.usPreTmsBits   = 0x1;
   ptyTmsForScanDR.ucPreTmsCount  = 0x3;

   ptyTmsForScanIRandDR_DR = ptyTmsForScanDR;

   // Set TMS
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))		 = CMD_CODE_SCAN_SET_TMS;    // command code
   *((unsigned char*)(pucBuffer + 0x04)) = ptyTmsForScanIR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x05))		 = ptyTmsForScanIR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x07)) = ptyTmsForScanIR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x08))		 = ptyTmsForScanIR.usPostTmsBits;

   *((unsigned char*)(pucBuffer + 0x0A)) = ptyTmsForScanDR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x0B))		 = ptyTmsForScanDR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x0D)) = ptyTmsForScanDR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x0E))		 = ptyTmsForScanDR.usPostTmsBits;

   *((unsigned char*)(pucBuffer + 0x10)) = ptyTmsForScanIRandDR_IR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x11))		 = ptyTmsForScanIRandDR_IR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x13)) = ptyTmsForScanIRandDR_IR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x14))		 = ptyTmsForScanIRandDR_IR.usPostTmsBits;

   *((unsigned char*)(pucBuffer + 0x16)) = ptyTmsForScanIRandDR_DR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x17))		 = ptyTmsForScanIRandDR_DR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x19)) = ptyTmsForScanIRandDR_DR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x1A))		 = ptyTmsForScanIRandDR_DR.usPostTmsBits;

    iPacketLen = TRANSMIT_LEN(28);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


/*--------------------------------------------------------------------------------------------------------*/
											 // Multicore configuration 
/*--------------------------------------------------------------------------------------------------------*/
	  ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	  pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	  TyTAPConfig ptyCoreConfig[2] = {
									   {
										0x0006,
										0x0001,
										&pulBypassIRPattern
									   },
									   {
										0x0004,
										0x0001,
										&pulBypassIRPattern
									   }
									  };


	  ulNumberOfCores = 2;
	  ulTotalLen = 0;
	  for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
		 {
		 if ((ptyCoreConfig[ulCnt].usDefaultIRLength > MAX_SCANCHAIN_LENGTH) || (ptyCoreConfig[ulCnt].pulBypassIRPattern == NULL))
			return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
		 ulTotalLen += ptyCoreConfig[ulCnt].usDefaultIRLength;
		 }
	  if (ulTotalLen > MAX_MULTICORE_SCANCHAIN_LENGTH)
		 return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
	  // synchronization not used, assuming single thread uses this function
	  ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
	  pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
	  // create packet


	  *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_SET_MULTICORE;       // command code
	  *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulNumberOfCores;  			// core number
	  *((unsigned short *)(pucBuffer + 0x6)) = 0x0000;
	  ulOffset = 0x08;
	  // copy each IR length to packet
	  for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
		 {
		 *((unsigned short *)(pucBuffer + ulOffset)) = ptyCoreConfig[ulCnt].usDefaultIRLength;
		 ulOffset += 2;
		 }
	  // do same for default DR length
	  for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
		 {
		 *((unsigned short *)(pucBuffer + ulOffset)) = ptyCoreConfig[ulCnt].usDefaultDRLength;
		 ulOffset += 2;
		 }
	  // ulOffset is alligned to word
	  if (ulTotalLen % 32)
		 *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)((ulTotalLen / 32) + 1);
	  else
		 *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)(ulTotalLen / 32);
	  *((unsigned short *)(pucBuffer + ulOffset + 2)) = 0x0000;
	  ulOffset += 4;
	  // now create whole IRBypass pattern
	  ulBitPos = 0;
	  for (ulCnt=ulNumberOfCores; ulCnt > 0; ulCnt--)
		 {
		 for (ulBit=0; ulBit < ptyCoreConfig[ulCnt-1].usDefaultIRLength; ulBit++)        // process every bit
			{
			uint32_t ulCurrentBit;
			uint32_t ulMask = (0x00000001 << ulBitPos);
			ulCurrentBit = ((ptyCoreConfig[ulCnt-1].pulBypassIRPattern[ulBit/32]) >> (ulBit % 32)) & 0x1;         // get current bit from pattern
			if (ulCurrentBit)
			   *((uint32_t *)(pucBuffer + ulOffset)) |= ulMask;
			else
			   *((uint32_t *)(pucBuffer + ulOffset)) &= ~ulMask;
			ulBitPos++;
			if (!(ulBitPos % 32))
			   {
			   ulBitPos = 0;
			   ulOffset += 4;          // go to next word
			   }
			}
		 }
	  if (ulBitPos % 32)
		 ulOffset += 4;                // ensure all words are written
	  iPacketLen = TRANSMIT_LEN((int32_t)ulOffset);

	  // send packet
	  tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer,iPacketLen);
	  if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
		 return  tyResult;

/*--------------------------------------------------------------------------------------------------------*/
											/* Coresight configuration */
/*--------------------------------------------------------------------------------------------------------*/
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer  + 0x00))  = CMD_CODE_CONFIG_CORESIGHT;   // command code
	*((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)0x1;   // core number
	*((unsigned short *)(pucBuffer + 0x08)) = 0x11;                        // reserved
	*((uint32_t  *)(pucBuffer + 0x0C)) = 0x1;
	*((uint32_t  *)(pucBuffer + 0x10)) = 0x0;
	*((uint32_t  *)(pucBuffer + 0x14)) = 0xd4011000;
	*((uint32_t  *)(pucBuffer + 0x18)) = 0x0;

    // send packet
    iPacketLen = TRANSMIT_LEN(28);

    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											/* // Debug request */
/*--------------------------------------------------------------------------------------------------------*/
   
    // Set TMS
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

    // create packet
    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;        // command code
    *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;              // core number
    pucBuffer[0x6] = 0x0;

    iPacketLen = TRANSMIT_LEN(7);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											 // Reset proc using 1
/*--------------------------------------------------------------------------------------------------------*/

    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

    // create packet
    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_RESET_PROC;          // command code
    *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;             // core number
    pucBuffer[0x6] = 0x1;

    iPacketLen = TRANSMIT_LEN(7);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											 // Reset proc using 1
/*--------------------------------------------------------------------------------------------------------*/
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;    // create packet

    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_RESET_PROC;          // command code
    *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;             // core number
    pucBuffer[0x6] = 0x0;

    iPacketLen = TRANSMIT_LEN(7);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											 // Enter debug state
/*--------------------------------------------------------------------------------------------------------*/
    ulDataArray[0] = TRUE;
    ulDataArray[1] = 0x5401d030;
    ulDataArray[2] = (0x1 << 13);

   // Set TMS
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;               // core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                    // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)0x11;
   *((unsigned char *)(pucBuffer + 0x9))   = (unsigned char)0x16;
   
   memcpy(((unsigned char *)(pucBuffer + 0xC)), (const void *)ulDataArray, 12);

   iPacketLen = TRANSMIT_LEN(22);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											 // Leave debug state
/*--------------------------------------------------------------------------------------------------------*/
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   uint32_t ulArray[5] = {0x0};

	// create packet
	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       	// command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)1;               			// core number
	*((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)0;                         // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)0x11;;             			
	*((unsigned char *)(pucBuffer + 0x9))  = (unsigned char)0;              			
	*((unsigned char *)(pucBuffer + 0xA))  = (unsigned char)0x16;
	*((unsigned char *)(pucBuffer + 0xB))  = 0x0;

	memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(ulArray), sizeof(ulArray));//wDTR,R0,rDTR

	iPacketLen = TRANSMIT_LEN(14 + sizeof(ulArray)); 

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	  tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	  tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
	  {
	  tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	  }


/*------------------For OMAP3530--------------------*/

   tyResult = DL_OPXD_Arm_Test_Initialise(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CONFIG_CORESIGHT;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((uint32_t *)(pucBuffer + 0x8))  = 5;
   *((uint32_t *)(pucBuffer + 0xC))  = 1;
   *((uint32_t *)(pucBuffer + 0x10))  = 0;
   *((uint32_t *)(pucBuffer + 0x14))  = 0x60008000;
   // send packet
   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

#if 0
   TyCoresightComp tyCoresightComp;
   DL_OPXD_ReadROMTable(tyDevHandle, usCore, 0x60000000,  0x1, &tyCoresightComp);
#endif

   /*****************Debug Mode*****************************************/
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

#if 0

      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
      *((uint32_t *)(pucBuffer + 0xC))  = 0x97805830;
      // send packet
      iPacketLen = TRANSMIT_LEN(16);

      // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         }


      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;

      iPacketLen = TRANSMIT_LEN(12);

     // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
   /* Read PC,Write Back,Comand prompt ok test */


   /*****************Debug Mode*****************************************/
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_REG;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xDEADBEEF;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_REG;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((unsigned char *)(pucBuffer + 0xC))  = 0x0;
   // send packet
   iPacketLen = TRANSMIT_LEN(0x10);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   uint32_t c;
   uint32_t ulCPSR;
   uint32_t mode[8] = {
                            0x10,
                            0x11,
                            0x12,
                            0x13,
                            0x17,
                            0x1B,
                            0x1F,
                            0x16
                           };

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_CPSR;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   ulCPSR = *((uint32_t *)(pucBuffer + 0x04));


   for(c = 0;c < 8; c++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_CPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = ((ulCPSR & 0xFFFFFFE0) | mode[c]);
           // send packet
           iPacketLen = TRANSMIT_LEN(16);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
    
    
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_CPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }

           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_CPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = ((ulCPSR & 0xFFFFFFE0) | mode[c]);
           // send packet
           iPacketLen = TRANSMIT_LEN(16);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }


           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_SPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x20000000;
   // send packet
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_BYTE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((unsigned char *)(pucBuffer + 0x10)) = 0xAB;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_BYTE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_BYTE_MULTIPLE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((unsigned char *)(pucBuffer + 0x10)) = 0xF;

   *((uint32_t *)(pucBuffer + 0x14)) = 0x1;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x2;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x3;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x4;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x5;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x6;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x7;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x8;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x9;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xA;
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xB;
   *((uint32_t *)(pucBuffer + 0x40)) = 0xC;
   *((uint32_t *)(pucBuffer + 0x44)) = 0xD;
   *((uint32_t *)(pucBuffer + 0x48)) = 0xE;
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xF;

   // send packet
   iPacketLen = TRANSMIT_LEN(76);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_BYTE_MULTIPLE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xABCD1234;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }



   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SET_DCR_ACCESSS;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x00100000;    /* 0x00000000,0x00100000,0x00200000 */
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_REG_STALL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xDEADBEEF;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_REG_STALL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((unsigned char *)(pucBuffer + 0xC))  = 0x0;
   // send packet
   iPacketLen = TRANSMIT_LEN(0x10);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CO_PROC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;

   *((uint32_t *)(pucBuffer + 0xC))  = 0xF;
   *((unsigned char *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x0;

   // send packet
   iPacketLen = TRANSMIT_LEN(32);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CO_PROC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;

   *((uint32_t *)(pucBuffer + 0xC))  = 0xF;
   *((unsigned char *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x1;

   // send packet
   iPacketLen = TRANSMIT_LEN(32);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   /* BREAK POINT */
   uint32_t z;
   uint32_t r = 1;
   uint32_t A[12] = {
                            0xE3A010AA,          /* 0x1FFFE000 = MOV R1,#0xAA */
                            0xE3A020BB,          /* 0x1FFFE004 = MOV R2,#0xBB */
                            0xE3A030CC,          /* 0x1FFFE008 = MOV R3,#0xCC */
                            0xE3A040DD,          /* 0x1FFFE00C = MOV R4,#0xDD */
                            0xE3A050EE,          /* 0x1FFFE010 = MOV R5,#0xEE */
                            0xE3A060FF,          /* 0x1FFFE014 = MOV R6,#0xFF */
                            0xE3A010A0,          /* 0x1FFFE018 = MOV R1,#0xA0 */
                            0xE3A020B0,          /* 0x1FFFE01C = MOV R2,#0xB0 */
                            0xE3A030C0,          /* 0x1FFFE020 = MOV R3,#0xC0 */
                            0xE3A040D0,          /* 0x1FFFE024 = MOV R4,#0xD0 */
                            0xE3A050E0,          /* 0x1FFFE028 = MOV R5,#0xE0 */
                            0xE3A060F0           /* 0x1FFFE02C = MOV R6,#0xF0 */
                         };


   for(z = 0;z < 12;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           *((uint32_t *)(pucBuffer + 0x10)) = A[z];
           // send packet
           iPacketLen = TRANSMIT_LEN(20);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
/*
   for(z = 0;z < 6;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           // send packet
           iPacketLen = TRANSMIT_LEN(16);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
*/
   for(z = 0;z < 12;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SET_BREAK;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = ((0x1FFFE000 + (z*4)) + 4);
        
           // send packet
           iPacketLen = TRANSMIT_LEN(17);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
           // send packet
           iPacketLen = TRANSMIT_LEN(16);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
        
        
            *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
            *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
            *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
        
        	iPacketLen = TRANSMIT_LEN(12);
        
           // send packet
            if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
            else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
            else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
        
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_INST_EXE_ENAB;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
        
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
           
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_REG;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((unsigned char *)(pucBuffer + 0xC))  = r;
           // send packet
           iPacketLen = TRANSMIT_LEN(0x10);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }

           r++;
           if(r>6) r = 1;
       } 


#if 0
                            0xE3A010AA,          /* 0x1FFFE000 = MOV R1,#0xAA       */
                            0xE3A020BB,          /* 0x1FFFE004 = MOV R2,#0xBB       */
                            0xE3A030CC,          /* 0x1FFFE008 = MOV R3,#0xCC       */
                            0xE3A040DD,          /* 0x1FFFE00C = MOV R4,#0xDD       */
                            0xE3A050EE,          /* 0x1FFFE010 = MOV R5,#0xEE       */
                            0xE3A060FF,          /* 0x1FFFE014 = MOV R6,#0xFF       */
                            0xE3A010A0,          /* 0x1FFFE018 = MOV R1,#0xA0       */
                            0xE3A020B0,          /* 0x1FFFE01C = MOV R2,#0xB0       */
                            0xE3A030C0,          /* 0x1FFFE020 = MOV R3,#0xC0       */
                            0xE3A040D0,          /* 0x1FFFE024 = MOV R4,#0xD0       */
                            0xE3A050E0,          /* 0x1FFFE028 = MOV R5,#0xE0       */
                            0xE3A060F0,          /* 0x1FFFE02C = MOV R6,#0xF0       */
                            0xE59F700C,          /* 0x1FFFE030 = LDR R7,=0x1FFFE004 */
                            0xE5D71000,          /* 0x1FFFE034 = LDRB R1,[R7]       */
                            0xE3A010AA,          /* 0x1FFFE038 = MOV R1,#0xAA       */
                            0xE3A020BB,          /* 0x1FFFE03C = MOV R2,#0xBB       */
                            0xE3A030CC,          /* 0x1FFFE040 = MOV R3,#0xCC       */
                            0xE3A040DD,          /* 0x1FFFE044 = MOV R4,#0xDD       */
                            0xE3A050EE,          /* 0x1FFFE048 = MOV R5,#0xEE       */
                            0xE3A060FF,          /* 0x1FFFE04C = MOV R6,#0xFF       */
                            0x1FFFE004

#endif

   for(z = 0;z < 21;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           *((uint32_t *)(pucBuffer + 0x10)) = A[z];
           // send packet
           iPacketLen = TRANSMIT_LEN(20);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
/*
   for(z = 0;z < 6;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           // send packet
           iPacketLen = TRANSMIT_LEN(16);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
*/

// MULTIPLE MEMORY READ/WRITE
   uint32_t z;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD_MUL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((unsigned char *)(pucBuffer + 0x10)) = 15;

   *((uint32_t *)(pucBuffer + 0x14)) = 0x11111111;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x22222222;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x33333333;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x44444444;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x55555555;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x66666666;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x77777777;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x88888888;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x99999999;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xAAAAAAAA;
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xBBBBBBBB;
   *((uint32_t *)(pucBuffer + 0x40)) = 0xCCCCCCCC;
   *((uint32_t *)(pucBuffer + 0x44)) = 0xDDDDDDDD;
   *((uint32_t *)(pucBuffer + 0x48)) = 0xEEEEEEEE;
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xFFFFFFFF;

   // send packet
   iPacketLen = TRANSMIT_LEN(80);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

#if 0
   for(z = 0;z < 15;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           // send packet
           iPacketLen = TRANSMIT_LEN(16);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }

#endif


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD_MUL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned short *)(pucBuffer + 0x6)) = 0x0; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((unsigned char *)(pucBuffer + 0x9))  = 0x0;
   *((unsigned char *)(pucBuffer + 0xA))  = 0x0;
   *((unsigned char *)(pucBuffer + 0xB))  = 0x0;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((uint32_t *)(pucBuffer + 0x10)) = 16;
   // send packet
   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
#endif

   /* WATCH POINT */
#if 0
   uint32_t A[13] = {
                            0xE3A010AA,        /* 0x1FFFE000 MOV R1,#0xAA           */       
                            0xE3A020BB,        /* 0x1FFFE004 MOV R2,#0xBB           */       
                            0xE3A030CC,        /* 0x1FFFE008 MOV R3,#0xCC           */
                            0xE3A040DD,        /* 0x1FFFE00C MOV R4,#0xDD           */       
                            0xE3A050EE,        /* 0x1FFFE010 MOV R5,#0xEE           */       
                            0xE3A060FF,        /* 0x1FFFE014 MOV R6,#0xFF           */       
                            0xE59F7010,        /* 0x1FFFE018 LDR R7,=0x1FFFE004     */ 
                            0xE5D71000,        /* 0x1FFFE01C LDRB R1,[R7]           */       
                            0xE3A02000,        /* 0x1FFFE020 MOV R2,#0x00           */ 
                            0xE5C72000,        /* 0x1FFFE024 STRB R2,[R7]           */    
                            0xE3A010AA,        /* 0x1FFFE028 MOV R1,#0xAA           */       
                            0xE3A020BB,        /* 0x1FFFE02C MOV R2,#0xBB           */
                            0x1FFFE004
                         };

    uint32_t A[13] = {
                        0xE3A010AA,        /* 0x1FFFE000 MOV R1,#0xAA           */       
                        0xE3A020BB,        /* 0x1FFFE004 MOV R2,#0xBB           */       
                        0xE3A030CC,        /* 0x1FFFE008 MOV R3,#0xCC           */
                        0xE3A040DD,        /* 0x1FFFE00C MOV R4,#0xDD           */       
                        0xE3A050EE,        /* 0x1FFFE010 MOV R5,#0xEE           */       
                        0xE3A060FF,        /* 0x1FFFE014 MOV R6,#0xFF           */       
                        0xE59F7010,        /* 0x1FFFE018 LDR R7,=0x1FFFE004     */ 
                        0xE5C72000,        /* 0x1FFFE01C STRB R2,[R7]           */       
                        0xE3A02000,        /* 0x1FFFE020 MOV R2,#0x00           */ 
                        0xE5D71000,        /* 0x1FFFE024 LDRB R1,[R7]           */    
                        0xE3A010AA,        /* 0x1FFFE028 MOV R1,#0xAA           */       
                        0xE3A020BB,        /* 0x1FFFE02C MOV R2,#0xBB           */
                        0x1FFFE004
                     };

     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD_MUL;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
     *((unsigned char *)(pucBuffer + 0x10)) = 15;

     *((uint32_t *)(pucBuffer + 0x14)) = A[0];
     *((uint32_t *)(pucBuffer + 0x18)) = A[1];
     *((uint32_t *)(pucBuffer + 0x1C)) = A[2];
     *((uint32_t *)(pucBuffer + 0x20)) = A[3];
     *((uint32_t *)(pucBuffer + 0x24)) = A[4];
     *((uint32_t *)(pucBuffer + 0x28)) = A[5];
     *((uint32_t *)(pucBuffer + 0x2C)) = A[6];
     *((uint32_t *)(pucBuffer + 0x30)) = A[7];
     *((uint32_t *)(pucBuffer + 0x34)) = A[8];
     *((uint32_t *)(pucBuffer + 0x38)) = A[9];
     *((uint32_t *)(pucBuffer + 0x3C)) = A[10];
     *((uint32_t *)(pucBuffer + 0x40)) = A[11];
     *((uint32_t *)(pucBuffer + 0x44)) = A[12];

     // send packet
     iPacketLen = TRANSMIT_LEN(72);

     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }


     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SET_WATCH;                    // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE004;
    
     // send packet
     iPacketLen = TRANSMIT_LEN(17);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    
    
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
     // send packet
     iPacketLen = TRANSMIT_LEN(16);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }


     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_DSCR;                   // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);

     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

    
    
      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
    
      iPacketLen = TRANSMIT_LEN(12);
    
     // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    
    
      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;         // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
      // send packet
      iPacketLen = TRANSMIT_LEN(9);
    
      // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         }
    
    /*
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_INST_EXE_ENAB;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    */
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_DSCR;                   // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

    
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD_MUL;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned short *)(pucBuffer + 0x6)) = 0x0; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((unsigned char *)(pucBuffer + 0x9))  = 0x0;
     *((unsigned char *)(pucBuffer + 0xA))  = 0x0;
     *((unsigned char *)(pucBuffer + 0xB))  = 0x0;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
     *((uint32_t *)(pucBuffer + 0x10)) = 14;
     // send packet
     iPacketLen = TRANSMIT_LEN(24);

     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

    uint32_t z;

    for(z=0;z<8;z++)
    {
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CHANGE_MODE;                   // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((unsigned char *)(pucBuffer + 0xC))  = z;
     // send packet
     iPacketLen = TRANSMIT_LEN(17);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    }


    uint32_t z;
    
    for(z=0;z<8;z++)
    {
        *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CHANGE_MODE;                   // command code
        *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
        *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
        *((unsigned char *)(pucBuffer + 0xC))  = z;
        // send packet
        iPacketLen = TRANSMIT_LEN(17);
        
        // send packet
        if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else
         {
         tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         }
        
    
    
        *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_SPSR;         // command code
        *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
        *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
        // send packet
        iPacketLen = TRANSMIT_LEN(13);
        
        // send packet
        if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
          tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
          tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else
          {
          tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
          }
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_CPSR;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(13);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
#endif



   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;


   *((uint32_t *)(pucBuffer + 0x0C)) = 0x12345678; // SPSR       FIQ
   *((uint32_t *)(pucBuffer + 0x10)) = 0xA0000000; // R8         FIQ
   *((uint32_t *)(pucBuffer + 0x14)) = 0xA1111111; // R9         FIQ 
   *((uint32_t *)(pucBuffer + 0x18)) = 0xA2222222; // R10        FIQ
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xA3333333; // R11        FIQ
   *((uint32_t *)(pucBuffer + 0x20)) = 0xA4444444; // R12        FIQ
   *((uint32_t *)(pucBuffer + 0x24)) = 0xA5555555; // R13        FIQ
   *((uint32_t *)(pucBuffer + 0x28)) = 0xA6666666; // R14        FIQ

   *((uint32_t *)(pucBuffer + 0x2C)) = 0x12345678; // SPSR       IRQ
   *((uint32_t *)(pucBuffer + 0x30)) = 0xB1111111; // R13        IRQ
   *((uint32_t *)(pucBuffer + 0x34)) = 0xB2222222; // R14        IRQ

   *((uint32_t *)(pucBuffer + 0x38)) = 0x12345678; // SPSR       SUPERVISOR
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xC1111111; // R13        SUPERVISOR
   *((uint32_t *)(pucBuffer + 0x40)) = 0xC2222222; // R14        SUPERVISOR

   *((uint32_t *)(pucBuffer + 0x44)) = 0x12345678; // SPSR       ABORT
   *((uint32_t *)(pucBuffer + 0x48)) = 0xD1111111; // R13        ABORT
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xD2222222; // R14        ABORT

   *((uint32_t *)(pucBuffer + 0x50)) = 0x12345678; // SPSR       UNDEF
   *((uint32_t *)(pucBuffer + 0x54)) = 0xE1111111; // R13        UNDEF
   *((uint32_t *)(pucBuffer + 0x58)) = 0xE2222222; // R14        UNDEF

   *((uint32_t *)(pucBuffer + 0x5C)) = 0xF8888888; // R8         USER
   *((uint32_t *)(pucBuffer + 0x60)) = 0xF9999999; // R9         USER
   *((uint32_t *)(pucBuffer + 0x64)) = 0xFAAAAAAA; // R10        USER
   *((uint32_t *)(pucBuffer + 0x68)) = 0xFBBBBBBB; // R11        USER
   *((uint32_t *)(pucBuffer + 0x6C)) = 0xFCCCCCCC; // R12        USER
   *((uint32_t *)(pucBuffer + 0x70)) = 0xFDDDDDDD; // R13        USER
   *((uint32_t *)(pucBuffer + 0x74)) = 0xFEEEEEEE; // R14        USER

   *((uint32_t *)(pucBuffer + 0x78)) = 0x000001D3; // CPSR

   *((uint32_t *)(pucBuffer + 0x7C)) = 0x0000AAAA; // R0
   *((uint32_t *)(pucBuffer + 0x80)) = 0x1111AAAA; // R1
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2222AAAA; // R2
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3333AAAA; // R3
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4444AAAA; // R4

   *((uint32_t *)(pucBuffer + 0x90)) = 0x5555AAAA; // R5
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6666AAAA; // R6
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7777AAAA; // R7

   *((uint32_t *)(pucBuffer + 0x9C)) = 0x1FFFE000; // PC                                            

   // send packet
   iPacketLen = TRANSMIT_LEN(160);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }



   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   /*****************Restart Mode*****************************************/
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x2;               // core number
	*((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)0;                                    // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)0x11;             // set if THUMB mode
	*((unsigned char *)(pucBuffer + 0x9))  = (unsigned char)0x0;              // set if THUMB mode
	*((unsigned char *)(pucBuffer + 0xA))  = (unsigned char)0x0;
	*((unsigned char *)(pucBuffer + 0xB))  = 0x0;
	iPacketLen = TRANSMIT_LEN(14);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   /*****************Restart Mode*****************************************/

   DL_OPXD_UnlockDevice(tyDevHandle);

   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_CortexA9
     Engineer: Jeenus Chalattu Kunnath.
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucTestOnchipMem - 1 Test onchip memory, 0 don't test
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test Cortex M3 Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
24-SEP-2009    JCK          Initial
****************************************************************************/
TyError DL_OPXD_Arm_TEST_CortexA9(
    TyDevHandle tyDevHandle, 
    unsigned char ucTestOnchipMem, 
    uint32_t ulOnChipMemory, 
    unsigned short usCore
    )
{
    
    int32_t iPacketLen;
    const unsigned char ucProcessor = 3; //1 fro ARM7 2 ARM7S 3 ARM9
    TyTmsSequence ptyTmsForScanIR, ptyTmsForScanDR, ptyTmsForScanIRandDR_IR, ptyTmsForScanIRandDR_DR;
    uint32_t pulBypassIRPattern = 0xFFFFFFFF;
    uint32_t ulCnt, ulOffset, ulBit, ulBitPos, ulTotalLen, ulNumberOfCores;
    uint32_t ulDataArray[3];
 

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;   
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   NOREF(ulOnChipMemory);
   NOREF(ucTestOnchipMem);

   tyResult = DL_OPXD_LockDevice(tyDevHandle);

/*--------------------------------------------For TRIDENT-------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------*/
											 // Reset DAP
/*--------------------------------------------------------------------------------------------------------*/
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_RESET_DAP; // command code

   iPacketLen = TRANSMIT_LEN(4);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
											 // TMS setting
/*--------------------------------------------------------------------------------------------------------*/

   ptyTmsForScanIR.usPostTmsBits  = 0xD;
   ptyTmsForScanIR.ucPostTmsCount = 0x5;
   ptyTmsForScanIR.usPreTmsBits   = 0x3;
   ptyTmsForScanIR.ucPreTmsCount  = 0x4;

   ptyTmsForScanIRandDR_IR = ptyTmsForScanIR;

   ptyTmsForScanDR.usPostTmsBits  = 0xD;
   ptyTmsForScanDR.ucPostTmsCount = 0x5;
   ptyTmsForScanDR.usPreTmsBits   = 0x1;
   ptyTmsForScanDR.ucPreTmsCount  = 0x3;

   ptyTmsForScanIRandDR_DR = ptyTmsForScanDR;

   // Set TMS
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))		 = CMD_CODE_SCAN_SET_TMS;    // command code
   *((unsigned char*)(pucBuffer + 0x04)) = ptyTmsForScanIR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x05))		 = ptyTmsForScanIR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x07)) = ptyTmsForScanIR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x08))		 = ptyTmsForScanIR.usPostTmsBits;

   *((unsigned char*)(pucBuffer + 0x0A)) = ptyTmsForScanDR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x0B)) = ptyTmsForScanDR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x0D)) = ptyTmsForScanDR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x0E)) = ptyTmsForScanDR.usPostTmsBits;

   *((unsigned char*)(pucBuffer + 0x10)) = ptyTmsForScanIRandDR_IR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x11)) = ptyTmsForScanIRandDR_IR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x13)) = ptyTmsForScanIRandDR_IR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x14)) = ptyTmsForScanIRandDR_IR.usPostTmsBits;

   *((unsigned char*)(pucBuffer + 0x16)) = ptyTmsForScanIRandDR_DR.ucPreTmsCount;
   *((uint16_t*)(pucBuffer + 0x17)) = ptyTmsForScanIRandDR_DR.usPreTmsBits;
   *((unsigned char*)(pucBuffer + 0x19)) = ptyTmsForScanIRandDR_DR.ucPostTmsCount;
   *((uint16_t*)(pucBuffer + 0x1A)) = ptyTmsForScanIRandDR_DR.usPostTmsBits;

    iPacketLen = TRANSMIT_LEN(28);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

/*--------------------------------------------------------------------------------------------------------*/
										  // Multicore configuration 
/*--------------------------------------------------------------------------------------------------------*/
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //default multicore configuration
   TyTAPConfig ptyCoreConfig[2] = {
                                    {
                                     0x0006,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                    {
                                     0x0004,
                                     0x0001,
                                     &pulBypassIRPattern
                                    }
                                  };

   ulNumberOfCores = 2;


#ifdef TRIDENT
   TyTAPConfig ptyCoreConfig[4] = {
                                    {
                                     0x0007,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                    {
                                     0x0007,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                    {
                                     0x0005,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                                                        {
                                     0x0004,
                                     0x0001,
                                     &pulBypassIRPattern
                                    } 
                                   };


   ulNumberOfCores = 4;
#endif


#ifdef PANDA
   TyTAPConfig ptyCoreConfig[2] = {
                                    {
                                     0x0006,
                                     0x0001,
                                     &pulBypassIRPattern
                                    },
                                    {
                                     0x0004,
                                     0x0001,
                                     &pulBypassIRPattern
                                    }
                                  };


   ulNumberOfCores = 2;
#endif

   ulTotalLen = 0;
   for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
      {
      if ((ptyCoreConfig[ulCnt].usDefaultIRLength > MAX_SCANCHAIN_LENGTH) || (ptyCoreConfig[ulCnt].pulBypassIRPattern == NULL))
         return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
      ulTotalLen += ptyCoreConfig[ulCnt].usDefaultIRLength;
      }
   if (ulTotalLen > MAX_MULTICORE_SCANCHAIN_LENGTH)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_SET_MULTICORE;       // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulNumberOfCores;  			// core number
   *((unsigned short *)(pucBuffer + 0x6)) = 0x0000;
   ulOffset = 0x08;
   // copy each IR length to packet
   for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
      {
      *((unsigned short *)(pucBuffer + ulOffset)) = ptyCoreConfig[ulCnt].usDefaultIRLength;
      ulOffset += 2;
      }
   // do same for default DR length
   for (ulCnt=0; ulCnt < ulNumberOfCores; ulCnt++)
      {
      *((unsigned short *)(pucBuffer + ulOffset)) = ptyCoreConfig[ulCnt].usDefaultDRLength;
      ulOffset += 2;
      }
   // ulOffset is alligned to word
   if (ulTotalLen % 32)
      *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)((ulTotalLen / 32) + 1);
   else
      *((unsigned short *)(pucBuffer + ulOffset)) = (unsigned short)(ulTotalLen / 32);
   *((unsigned short *)(pucBuffer + ulOffset + 2)) = 0x0000;
   ulOffset += 4;
   // now create whole IRBypass pattern
   ulBitPos = 0;
   for (ulCnt=ulNumberOfCores; ulCnt > 0; ulCnt--)
      {
      for (ulBit=0; ulBit < ptyCoreConfig[ulCnt-1].usDefaultIRLength; ulBit++)        // process every bit
         {
         uint32_t ulCurrentBit;
         uint32_t ulMask = (0x00000001 << ulBitPos);
         ulCurrentBit = ((ptyCoreConfig[ulCnt-1].pulBypassIRPattern[ulBit/32]) >> (ulBit % 32)) & 0x1;         // get current bit from pattern
         if (ulCurrentBit)
            *((uint32_t *)(pucBuffer + ulOffset)) |= ulMask;
         else
            *((uint32_t *)(pucBuffer + ulOffset)) &= ~ulMask;
         ulBitPos++;
         if (!(ulBitPos % 32))
            {
            ulBitPos = 0;
            ulOffset += 4;          // go to next word
            }
         }
      }
   if (ulBitPos % 32)
      ulOffset += 4;                // ensure all words are written
   iPacketLen = TRANSMIT_LEN((int32_t)ulOffset);

   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer,iPacketLen);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

/*--------------------------------------------------------------------------------------------------------*/
											 //Reseting TAP
/*--------------------------------------------------------------------------------------------------------*/

    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;
	pucBuffer[0x4] = 0x0;

    iPacketLen = TRANSMIT_LEN(5);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
										  //Bringin TAP to RTI State
/*--------------------------------------------------------------------------------------------------------*/
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;   // command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif

   *((unsigned short *)(pucBuffer + 0x6)) = 0;                     // reserved
   *((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)0x11;   // core type CORTEXA8 = 0x11

    iPacketLen = TRANSMIT_LEN(9);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
										  //Config core sight
/*--------------------------------------------------------------------------------------------------------*/

    // Set TMS
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

	*((uint32_t *)(pucBuffer  + 0x00))  = CMD_CODE_CONFIG_CORESIGHT;   // command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif
	*((unsigned short *)(pucBuffer + 0x08)) = 0x11;                        // reserved
	*((uint32_t  *)(pucBuffer + 0x0C)) = 0x1;
	*((uint32_t  *)(pucBuffer + 0x10)) = 0x0;
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x14)) = 0xE0670000;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x14)) = 0xD4140000;   // core number
#endif
	*((uint32_t  *)(pucBuffer + 0x18)) = 0x1;

    // send packet
    iPacketLen = TRANSMIT_LEN(28);

    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

/*--------------------------------------------------------------------------------------------------------*/
											 // Debug request
/*--------------------------------------------------------------------------------------------------------*/
    // Set TMS
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

    // create packet
    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;        // command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif
    pucBuffer[0x6] = 0x0;

    iPacketLen = TRANSMIT_LEN(7);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

/*--------------------------------------------------------------------------------------------------------*/
	// Reset proc using 1
/*--------------------------------------------------------------------------------------------------------*/
    
    // Set TMS
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

    // create packet
    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_RESET_PROC;          // command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif
    pucBuffer[0x6] = 0x1;

    iPacketLen = TRANSMIT_LEN(7);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
/*--------------------------------------------------------------------------------------------------------*/
	   // Reset proc using 0
/*--------------------------------------------------------------------------------------------------------*/

   // Set TMS
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;    // create packet

    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_RESET_PROC;          // command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif
    pucBuffer[0x6] = 0x0;

    iPacketLen = TRANSMIT_LEN(7);

    // send packet
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

/*--------------------------------------------------------------------------------------------------------*/
	// Enter debug state
/*--------------------------------------------------------------------------------------------------------*/
	// Enter debug state
	ulDataArray[0] = TRUE;
	ulDataArray[1] = 0x5401d030;
	ulDataArray[2] = (0x1 << 13);

	ulDataArray[0] = 0x0;
	ulDataArray[1] = 0x0;
	ulDataArray[2] = 0x0;

   // Set TMS
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;       // command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif
   *((unsigned short *)(pucBuffer + 0x6)) = 0;                                    // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)0x11;
   *((unsigned char *)(pucBuffer + 0x9))   = (unsigned char)0x16;

   memcpy(((unsigned char *)(pucBuffer + 0xC)), (const void *)ulDataArray, 0xC);

   iPacketLen = TRANSMIT_LEN(25);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	  tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	  tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
	  {
	  tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	  }
/*--------------------------------------------------------------------------------------------------------*/
	// Enter debug state
/*--------------------------------------------------------------------------------------------------------*/

   uint32_t ulArray[5] = {0x0};

	// create packet
	*((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       	// command code
#ifdef TRIDENT
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x3;   // core number
#endif
#ifdef PANDA
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x1;   // core number
#endif
	*((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)0;                                    // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)0x11;;             			// set if THUMB mode
	*((unsigned char *)(pucBuffer + 0x9))  = (unsigned char)1;              			// set if THUMB mode
	*((unsigned char *)(pucBuffer + 0xA))  = (unsigned char)0x16;
	*((unsigned char *)(pucBuffer + 0xB))  = 0x0;

	memcpy(((uint32_t *)(pucBuffer + 0xC)), (uint32_t *)(ulArray), sizeof(ulArray));//wDTR,R0,rDTR

	iPacketLen = TRANSMIT_LEN(14 + sizeof(ulArray)); 

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
	  tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
	  tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
	  {
	  tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
	  }


#if 0
   //Set Safe Non Vector Address
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SET_SAFE_NON_VECTOR_ADDRESS;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x08] = ucProcessor ;
   *((uint32_t *)(pucBuffer + 0xC)) = 0x10000;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 16);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x06] = 0x02;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

//Write ICE Breaker controll register debug requist
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_ICE_BREAKER;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x06] = 0;
   pucBuffer[0x07] = 0;
   *((uint32_t *)(pucBuffer + 0x8))= 0x00;
   *((uint32_t *)(pucBuffer + 0xC))= 0x02;
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 16);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {
//ARM STATUS PROC
      // create packet
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;     // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore;                           // halfword - usCore number (0 if MC support disabled)
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = ucProcessor ; //ucPROCESSOR
      pucBuffer[0x09] = 0x1; //Scan chain
      pucBuffer[0x0a] = 0x1; //Execution status
      // send packet
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;

      ulCount++;
      if (ulCount == 500)
         {
         PrintMessage1(ERROR_MESSAGE,"Bring Processor to Debug mode Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
         }
      }while (pucBuffer[0x05]  != 0x0);


   //Select Scan Chain
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;     // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;                         // halfword - usCore1 number (0 if MC support disabled)
   pucBuffer[0x06] = 0x01;//scan chain number
   pucBuffer[0x07] = 0x01;//number of intest
   pucBuffer[0x08] = ucProcessor ;//arm ucPROCESSOR
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   return tyResult;

#endif
/*--------------------------------------------------------------------------------------------------------*/

/*------------------For OMAP3530--------------------*/

   tyResult = DL_OPXD_Arm_Test_Initialise(ptyUsbDevHandle, tyDevHandle, ucProcessor, usCore);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CONFIG_CORESIGHT;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((uint32_t *)(pucBuffer + 0x8))  = 5;
   *((uint32_t *)(pucBuffer + 0xC))  = 1;
   *((uint32_t *)(pucBuffer + 0x10))  = 0;
   *((uint32_t *)(pucBuffer + 0x14))  = 0x60008000;
   // send packet
   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

#if 0
   TyCoresightComp tyCoresightComp;
   DL_OPXD_ReadROMTable(tyDevHandle, usCore, 0x60000000,  0x1, &tyCoresightComp);
#endif

   /*****************Debug Mode*****************************************/
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

#if 0

      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
      *((uint32_t *)(pucBuffer + 0xC))  = 0x97805830;
      // send packet
      iPacketLen = TRANSMIT_LEN(16);

      // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         }


      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;

      iPacketLen = TRANSMIT_LEN(12);

     // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
   /* Read PC,Write Back,Comand prompt ok test */


   /*****************Debug Mode*****************************************/
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_REG;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xDEADBEEF;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_REG;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((unsigned char *)(pucBuffer + 0xC))  = 0x0;
   // send packet
   iPacketLen = TRANSMIT_LEN(0x10);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   uint32_t c;
   uint32_t ulCPSR;
   uint32_t mode[8] = {
                            0x10,
                            0x11,
                            0x12,
                            0x13,
                            0x17,
                            0x1B,
                            0x1F,
                            0x16
                           };

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_CPSR;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   ulCPSR = *((uint32_t *)(pucBuffer + 0x04));


   for(c = 0;c < 8; c++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_CPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = ((ulCPSR & 0xFFFFFFE0) | mode[c]);
           // send packet
           iPacketLen = TRANSMIT_LEN(16);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
    
    
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_CPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }

           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_CPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = ((ulCPSR & 0xFFFFFFE0) | mode[c]);
           // send packet
           iPacketLen = TRANSMIT_LEN(16);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }


           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_SPSR;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x20000000;
   // send packet
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_BYTE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((unsigned char *)(pucBuffer + 0x10)) = 0xAB;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_BYTE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_BYTE_MULTIPLE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((unsigned char *)(pucBuffer + 0x10)) = 0xF;

   *((uint32_t *)(pucBuffer + 0x14)) = 0x1;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x2;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x3;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x4;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x5;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x6;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x7;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x8;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x9;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xA;
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xB;
   *((uint32_t *)(pucBuffer + 0x40)) = 0xC;
   *((uint32_t *)(pucBuffer + 0x44)) = 0xD;
   *((uint32_t *)(pucBuffer + 0x48)) = 0xE;
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xF;

   // send packet
   iPacketLen = TRANSMIT_LEN(76);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_BYTE_MULTIPLE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xABCD1234;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }



   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SET_DCR_ACCESSS;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x00100000;    /* 0x00000000,0x00100000,0x00200000 */
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_REG_STALL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xDEADBEEF;
   // send packet
   iPacketLen = TRANSMIT_LEN(20);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_REG_STALL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((unsigned char *)(pucBuffer + 0xC))  = 0x0;
   // send packet
   iPacketLen = TRANSMIT_LEN(0x10);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CO_PROC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;

   *((uint32_t *)(pucBuffer + 0xC))  = 0xF;
   *((unsigned char *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x0;

   // send packet
   iPacketLen = TRANSMIT_LEN(32);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CO_PROC;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;

   *((uint32_t *)(pucBuffer + 0xC))  = 0xF;
   *((unsigned char *)(pucBuffer + 0x10)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x14)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x1;

   // send packet
   iPacketLen = TRANSMIT_LEN(32);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   /* BREAK POINT */
   uint32_t z;
   uint32_t r = 1;
   uint32_t A[12] = {
                            0xE3A010AA,          /* 0x1FFFE000 = MOV R1,#0xAA */
                            0xE3A020BB,          /* 0x1FFFE004 = MOV R2,#0xBB */
                            0xE3A030CC,          /* 0x1FFFE008 = MOV R3,#0xCC */
                            0xE3A040DD,          /* 0x1FFFE00C = MOV R4,#0xDD */
                            0xE3A050EE,          /* 0x1FFFE010 = MOV R5,#0xEE */
                            0xE3A060FF,          /* 0x1FFFE014 = MOV R6,#0xFF */
                            0xE3A010A0,          /* 0x1FFFE018 = MOV R1,#0xA0 */
                            0xE3A020B0,          /* 0x1FFFE01C = MOV R2,#0xB0 */
                            0xE3A030C0,          /* 0x1FFFE020 = MOV R3,#0xC0 */
                            0xE3A040D0,          /* 0x1FFFE024 = MOV R4,#0xD0 */
                            0xE3A050E0,          /* 0x1FFFE028 = MOV R5,#0xE0 */
                            0xE3A060F0           /* 0x1FFFE02C = MOV R6,#0xF0 */
                         };


   for(z = 0;z < 12;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           *((uint32_t *)(pucBuffer + 0x10)) = A[z];
           // send packet
           iPacketLen = TRANSMIT_LEN(20);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
/*
   for(z = 0;z < 6;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           // send packet
           iPacketLen = TRANSMIT_LEN(16);
    
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
*/
   for(z = 0;z < 12;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SET_BREAK;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = ((0x1FFFE000 + (z*4)) + 4);
        
           // send packet
           iPacketLen = TRANSMIT_LEN(17);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
           // send packet
           iPacketLen = TRANSMIT_LEN(16);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
        
        
            *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
            *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
            *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
        
        	iPacketLen = TRANSMIT_LEN(12);
        
           // send packet
            if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
            else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
            else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
        
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_INST_EXE_ENAB;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
        
        
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           // send packet
           iPacketLen = TRANSMIT_LEN(9);
        
           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
           
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_REG;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((unsigned char *)(pucBuffer + 0xC))  = r;
           // send packet
           iPacketLen = TRANSMIT_LEN(0x10);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }

           r++;
           if(r>6) r = 1;
       } 


#if 0
                            0xE3A010AA,          /* 0x1FFFE000 = MOV R1,#0xAA       */
                            0xE3A020BB,          /* 0x1FFFE004 = MOV R2,#0xBB       */
                            0xE3A030CC,          /* 0x1FFFE008 = MOV R3,#0xCC       */
                            0xE3A040DD,          /* 0x1FFFE00C = MOV R4,#0xDD       */
                            0xE3A050EE,          /* 0x1FFFE010 = MOV R5,#0xEE       */
                            0xE3A060FF,          /* 0x1FFFE014 = MOV R6,#0xFF       */
                            0xE3A010A0,          /* 0x1FFFE018 = MOV R1,#0xA0       */
                            0xE3A020B0,          /* 0x1FFFE01C = MOV R2,#0xB0       */
                            0xE3A030C0,          /* 0x1FFFE020 = MOV R3,#0xC0       */
                            0xE3A040D0,          /* 0x1FFFE024 = MOV R4,#0xD0       */
                            0xE3A050E0,          /* 0x1FFFE028 = MOV R5,#0xE0       */
                            0xE3A060F0,          /* 0x1FFFE02C = MOV R6,#0xF0       */
                            0xE59F700C,          /* 0x1FFFE030 = LDR R7,=0x1FFFE004 */
                            0xE5D71000,          /* 0x1FFFE034 = LDRB R1,[R7]       */
                            0xE3A010AA,          /* 0x1FFFE038 = MOV R1,#0xAA       */
                            0xE3A020BB,          /* 0x1FFFE03C = MOV R2,#0xBB       */
                            0xE3A030CC,          /* 0x1FFFE040 = MOV R3,#0xCC       */
                            0xE3A040DD,          /* 0x1FFFE044 = MOV R4,#0xDD       */
                            0xE3A050EE,          /* 0x1FFFE048 = MOV R5,#0xEE       */
                            0xE3A060FF,          /* 0x1FFFE04C = MOV R6,#0xFF       */
                            0x1FFFE004

#endif

   for(z = 0;z < 21;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           *((uint32_t *)(pucBuffer + 0x10)) = A[z];
           // send packet
           iPacketLen = TRANSMIT_LEN(20);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
/*
   for(z = 0;z < 6;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           // send packet
           iPacketLen = TRANSMIT_LEN(16);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }
*/

// MULTIPLE MEMORY READ/WRITE
   uint32_t z;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD_MUL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((unsigned char *)(pucBuffer + 0x10)) = 15;

   *((uint32_t *)(pucBuffer + 0x14)) = 0x11111111;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x22222222;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x33333333;
   *((uint32_t *)(pucBuffer + 0x20)) = 0x44444444;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x55555555;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x66666666;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x77777777;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x88888888;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x99999999;
   *((uint32_t *)(pucBuffer + 0x38)) = 0xAAAAAAAA;
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xBBBBBBBB;
   *((uint32_t *)(pucBuffer + 0x40)) = 0xCCCCCCCC;
   *((uint32_t *)(pucBuffer + 0x44)) = 0xDDDDDDDD;
   *((uint32_t *)(pucBuffer + 0x48)) = 0xEEEEEEEE;
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xFFFFFFFF;

   // send packet
   iPacketLen = TRANSMIT_LEN(80);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

#if 0
   for(z = 0;z < 15;z++)
       {
           *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD;         // command code
           *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
           *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
           *((uint32_t *)(pucBuffer + 0xC))  = (0x1FFFE000 + (z*4));
           // send packet
           iPacketLen = TRANSMIT_LEN(16);

           // send packet
           if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
              tyResult = DRVOPXD_ERROR_USB_DRIVER;
           else
              {
              tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
              }
       }

#endif


   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD_MUL;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned short *)(pucBuffer + 0x6)) = 0x0; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   *((unsigned char *)(pucBuffer + 0x9))  = 0x0;
   *((unsigned char *)(pucBuffer + 0xA))  = 0x0;
   *((unsigned char *)(pucBuffer + 0xB))  = 0x0;
   *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
   *((uint32_t *)(pucBuffer + 0x10)) = 16;
   // send packet
   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
#endif

   /* WATCH POINT */
#if 0
   uint32_t A[13] = {
                            0xE3A010AA,        /* 0x1FFFE000 MOV R1,#0xAA           */       
                            0xE3A020BB,        /* 0x1FFFE004 MOV R2,#0xBB           */       
                            0xE3A030CC,        /* 0x1FFFE008 MOV R3,#0xCC           */
                            0xE3A040DD,        /* 0x1FFFE00C MOV R4,#0xDD           */       
                            0xE3A050EE,        /* 0x1FFFE010 MOV R5,#0xEE           */       
                            0xE3A060FF,        /* 0x1FFFE014 MOV R6,#0xFF           */       
                            0xE59F7010,        /* 0x1FFFE018 LDR R7,=0x1FFFE004     */ 
                            0xE5D71000,        /* 0x1FFFE01C LDRB R1,[R7]           */       
                            0xE3A02000,        /* 0x1FFFE020 MOV R2,#0x00           */ 
                            0xE5C72000,        /* 0x1FFFE024 STRB R2,[R7]           */    
                            0xE3A010AA,        /* 0x1FFFE028 MOV R1,#0xAA           */       
                            0xE3A020BB,        /* 0x1FFFE02C MOV R2,#0xBB           */
                            0x1FFFE004
                         };

    uint32_t A[13] = {
                        0xE3A010AA,        /* 0x1FFFE000 MOV R1,#0xAA           */       
                        0xE3A020BB,        /* 0x1FFFE004 MOV R2,#0xBB           */       
                        0xE3A030CC,        /* 0x1FFFE008 MOV R3,#0xCC           */
                        0xE3A040DD,        /* 0x1FFFE00C MOV R4,#0xDD           */       
                        0xE3A050EE,        /* 0x1FFFE010 MOV R5,#0xEE           */       
                        0xE3A060FF,        /* 0x1FFFE014 MOV R6,#0xFF           */       
                        0xE59F7010,        /* 0x1FFFE018 LDR R7,=0x1FFFE004     */ 
                        0xE5C72000,        /* 0x1FFFE01C STRB R2,[R7]           */       
                        0xE3A02000,        /* 0x1FFFE020 MOV R2,#0x00           */ 
                        0xE5D71000,        /* 0x1FFFE024 LDRB R1,[R7]           */    
                        0xE3A010AA,        /* 0x1FFFE028 MOV R1,#0xAA           */       
                        0xE3A020BB,        /* 0x1FFFE02C MOV R2,#0xBB           */
                        0x1FFFE004
                     };

     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_MEM_WORD_MUL;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
     *((unsigned char *)(pucBuffer + 0x10)) = 15;

     *((uint32_t *)(pucBuffer + 0x14)) = A[0];
     *((uint32_t *)(pucBuffer + 0x18)) = A[1];
     *((uint32_t *)(pucBuffer + 0x1C)) = A[2];
     *((uint32_t *)(pucBuffer + 0x20)) = A[3];
     *((uint32_t *)(pucBuffer + 0x24)) = A[4];
     *((uint32_t *)(pucBuffer + 0x28)) = A[5];
     *((uint32_t *)(pucBuffer + 0x2C)) = A[6];
     *((uint32_t *)(pucBuffer + 0x30)) = A[7];
     *((uint32_t *)(pucBuffer + 0x34)) = A[8];
     *((uint32_t *)(pucBuffer + 0x38)) = A[9];
     *((uint32_t *)(pucBuffer + 0x3C)) = A[10];
     *((uint32_t *)(pucBuffer + 0x40)) = A[11];
     *((uint32_t *)(pucBuffer + 0x44)) = A[12];

     // send packet
     iPacketLen = TRANSMIT_LEN(72);

     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }


     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SET_WATCH;                    // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE004;
    
     // send packet
     iPacketLen = TRANSMIT_LEN(17);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    
    
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_PC;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
     // send packet
     iPacketLen = TRANSMIT_LEN(16);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }


     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_DSCR;                   // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);

     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

    
    
      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
    
      iPacketLen = TRANSMIT_LEN(12);
    
     // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    
    
      *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;         // command code
      *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
      *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
      // send packet
      iPacketLen = TRANSMIT_LEN(9);
    
      // send packet
      if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
      else
         {
         tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         }
    
    /*
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_INST_EXE_ENAB;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    */
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_DSCR;                   // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_PC;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     // send packet
     iPacketLen = TRANSMIT_LEN(9);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

    
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_MEM_WORD_MUL;         // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned short *)(pucBuffer + 0x6)) = 0x0; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((unsigned char *)(pucBuffer + 0x9))  = 0x0;
     *((unsigned char *)(pucBuffer + 0xA))  = 0x0;
     *((unsigned char *)(pucBuffer + 0xB))  = 0x0;
     *((uint32_t *)(pucBuffer + 0xC))  = 0x1FFFE000;
     *((uint32_t *)(pucBuffer + 0x10)) = 14;
     // send packet
     iPacketLen = TRANSMIT_LEN(24);

     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }

    uint32_t z;

    for(z=0;z<8;z++)
    {
     *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CHANGE_MODE;                   // command code
     *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
     *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
     *((unsigned char *)(pucBuffer + 0xC))  = z;
     // send packet
     iPacketLen = TRANSMIT_LEN(17);
    
     // send packet
     if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
     else
        {
        tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
        }
    }


    uint32_t z;
    
    for(z=0;z<8;z++)
    {
        *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CHANGE_MODE;                   // command code
        *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
        *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
        *((unsigned char *)(pucBuffer + 0xC))  = z;
        // send packet
        iPacketLen = TRANSMIT_LEN(17);
        
        // send packet
        if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
         tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else
         {
         tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
         }
        
    
    
        *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_READ_CORTEXA8_SPSR;         // command code
        *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
        *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
        // send packet
        iPacketLen = TRANSMIT_LEN(13);
        
        // send packet
        if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
          tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
          tyResult = DRVOPXD_ERROR_USB_DRIVER;
        else
          {
          tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
          }
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_WRITE_CORTEXA8_CPSR;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(13);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
#endif



   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;


   *((uint32_t *)(pucBuffer + 0x0C)) = 0x12345678; // SPSR       FIQ
   *((uint32_t *)(pucBuffer + 0x10)) = 0xA0000000; // R8         FIQ
   *((uint32_t *)(pucBuffer + 0x14)) = 0xA1111111; // R9         FIQ 
   *((uint32_t *)(pucBuffer + 0x18)) = 0xA2222222; // R10        FIQ
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xA3333333; // R11        FIQ
   *((uint32_t *)(pucBuffer + 0x20)) = 0xA4444444; // R12        FIQ
   *((uint32_t *)(pucBuffer + 0x24)) = 0xA5555555; // R13        FIQ
   *((uint32_t *)(pucBuffer + 0x28)) = 0xA6666666; // R14        FIQ

   *((uint32_t *)(pucBuffer + 0x2C)) = 0x12345678; // SPSR       IRQ
   *((uint32_t *)(pucBuffer + 0x30)) = 0xB1111111; // R13        IRQ
   *((uint32_t *)(pucBuffer + 0x34)) = 0xB2222222; // R14        IRQ

   *((uint32_t *)(pucBuffer + 0x38)) = 0x12345678; // SPSR       SUPERVISOR
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xC1111111; // R13        SUPERVISOR
   *((uint32_t *)(pucBuffer + 0x40)) = 0xC2222222; // R14        SUPERVISOR

   *((uint32_t *)(pucBuffer + 0x44)) = 0x12345678; // SPSR       ABORT
   *((uint32_t *)(pucBuffer + 0x48)) = 0xD1111111; // R13        ABORT
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xD2222222; // R14        ABORT

   *((uint32_t *)(pucBuffer + 0x50)) = 0x12345678; // SPSR       UNDEF
   *((uint32_t *)(pucBuffer + 0x54)) = 0xE1111111; // R13        UNDEF
   *((uint32_t *)(pucBuffer + 0x58)) = 0xE2222222; // R14        UNDEF

   *((uint32_t *)(pucBuffer + 0x5C)) = 0xF8888888; // R8         USER
   *((uint32_t *)(pucBuffer + 0x60)) = 0xF9999999; // R9         USER
   *((uint32_t *)(pucBuffer + 0x64)) = 0xFAAAAAAA; // R10        USER
   *((uint32_t *)(pucBuffer + 0x68)) = 0xFBBBBBBB; // R11        USER
   *((uint32_t *)(pucBuffer + 0x6C)) = 0xFCCCCCCC; // R12        USER
   *((uint32_t *)(pucBuffer + 0x70)) = 0xFDDDDDDD; // R13        USER
   *((uint32_t *)(pucBuffer + 0x74)) = 0xFEEEEEEE; // R14        USER

   *((uint32_t *)(pucBuffer + 0x78)) = 0x000001D3; // CPSR

   *((uint32_t *)(pucBuffer + 0x7C)) = 0x0000AAAA; // R0
   *((uint32_t *)(pucBuffer + 0x80)) = 0x1111AAAA; // R1
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2222AAAA; // R2
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3333AAAA; // R3
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4444AAAA; // R4

   *((uint32_t *)(pucBuffer + 0x90)) = 0x5555AAAA; // R5
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6666AAAA; // R6
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7777AAAA; // R7

   *((uint32_t *)(pucBuffer + 0x9C)) = 0x1FFFE000; // PC                                            

   // send packet
   iPacketLen = TRANSMIT_LEN(160);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }



   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 0x11;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }


   /*****************Restart Mode*****************************************/
   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;       // command code
	*((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)0x2;               // core number
	*((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)0;                                    // reserved
	*((unsigned char *)(pucBuffer + 0x8))  = (unsigned char)0x11;             // set if THUMB mode
	*((unsigned char *)(pucBuffer + 0x9))  = (unsigned char)0x0;              // set if THUMB mode
	*((unsigned char *)(pucBuffer + 0xA))  = (unsigned char)0x0;
	*((unsigned char *)(pucBuffer + 0xB))  = 0x0;
	iPacketLen = TRANSMIT_LEN(14);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   /*****************Restart Mode*****************************************/

   DL_OPXD_UnlockDevice(tyDevHandle);

   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST_CortexM3
     Engineer: Jeenus Chalattu Kunnath.
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char ucTestOnchipMem - 1 Test onchip memory, 0 don't test
               uint32_t ulOnChipMemory On-Chip Memory address
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test Cortex M3 Core Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
24-SEP-2009    JCK          Initial
****************************************************************************/
TyError DL_OPXD_Arm_TEST_CortexM3(
    TyDevHandle tyDevHandle, 
    unsigned char ucTestOnchipMem, 
    uint32_t ulOnChipMemory, 
    unsigned short usCore
    )
{
    int32_t iPacketLen;
   //const unsigned char ucProcessor = 3; //1 fro ARM7 2 ARM7S 3 ARM9

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;   
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_CONFIG_CORESIGHT;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 5;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_ENTER_DEBUG_STATE;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((unsigned char *)(pucBuffer + 0x8))  = 5;
   // send packet
   iPacketLen = TRANSMIT_LEN(9);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
      
    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM11_LEAVE_DEBUG_STATE;                   // command code
    *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
    *((unsigned short *)(pucBuffer + 0x6)) = (unsigned short)0; 
    *((uint32_t *)(pucBuffer + 0x8)) =  5;
    *((uint32_t *)(pucBuffer + 0xC)) =  0;
    *((uint32_t *)(pucBuffer + 0x10)) = 0;
    *((uint32_t *)(pucBuffer + 0x14)) = 0;
    *((uint32_t *)(pucBuffer + 0x18)) = 0;
  
   // send packet
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

// READ REGISTERS
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((uint32_t *)(pucBuffer + 0x8)) =  5;

   iPacketLen = TRANSMIT_LEN(12);
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);
   printf("\n R0 : %d R1 %d ", pucBuffer[4], pucBuffer[8]);
/*  if (tyResult != iPacketLen)
     tyResult = DRVOPXD_ERROR_USB_DRIVER;
  else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
     tyResult = DRVOPXD_ERROR_USB_DRIVER;
  else
     {
     tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
     }
*/

   /* Write a Sample program to Memory */

   *((uint32_t *)(pucBuffer + 0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;                   // command code
   *((unsigned short *)(pucBuffer + 4)) = usCore; 
   *((uint32_t *)(pucBuffer + 8)) =  CORTEXM;

   *((uint32_t *)(pucBuffer + 12)) =  0x20000000;
   *((uint32_t *)(pucBuffer + 16)) = 1;
   *((uint32_t *)(pucBuffer + 20)) = 0x11111111;//0xF04F0001;
   *((uint32_t *)(pucBuffer + 24)) = 0x22222222;//0xF04F0102;
   *((uint32_t *)(pucBuffer + 28)) = 0x33333333;//0xF04F0203;
   *((uint32_t *)(pucBuffer + 32)) = 0x44444444;//0xF04F0304;
   *((uint32_t *)(pucBuffer + 36)) = 0x55555555;//0xF04F0405;
   *((uint32_t *)(pucBuffer + 40)) = 0x66666666;//0xF04F0506;
   *((uint32_t *)(pucBuffer + 44)) = 0x77777777;//0xF04F0607;
   *((uint32_t *)(pucBuffer + 48)) = 0x88888888;//0xF04F0708;
   *((uint32_t *)(pucBuffer + 52)) = 0x99999999;//0xF04F0001;
   *((uint32_t *)(pucBuffer + 56)) = 0xAAAAAAAA;//0xF04F0102;
   *((uint32_t *)(pucBuffer + 60)) = 0xBBBBBBBB;//0xF04F0203;
   *((uint32_t *)(pucBuffer + 64)) = 0xCCCCCCCC;//0xF04F0304;
   *((uint32_t *)(pucBuffer + 68)) = 0xDDDDDDDD;//0xF04F0405;
   *((uint32_t *)(pucBuffer + 72)) = 0xEEEEEEEE;//0xF04F0506;
   iPacketLen = TRANSMIT_LEN(76);

   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);
   if(tyResult == DRVOPXD_ERROR_NO_ERROR)
       {
       printf("Sample Pgm Downloaded");
       }

// READ AND VERIFY DOWNLOAD
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_MULTIPLE_WORDS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((uint32_t *)(pucBuffer + 0x8)) =  CORTEXM;
   *((uint32_t *)(pucBuffer + 12)) =  0x20000000;
   *((uint32_t *)(pucBuffer + 16)) =  14;

   iPacketLen = TRANSMIT_LEN(20);
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);

   *((uint32_t *)(pucBuffer + 0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;                   // command code
   *((unsigned short *)(pucBuffer + 4)) = usCore; 
   *((uint32_t *)(pucBuffer + 8)) =  CORTEXM;

   *((uint32_t *)(pucBuffer + 12)) =  0x20000000;
   *((uint32_t *)(pucBuffer + 16)) = 1;
   *((uint32_t *)(pucBuffer + 20)) = 0x12345671;//0xF04F0001;
   *((uint32_t *)(pucBuffer + 24)) = 0x22345672;//0xF04F0102;
   *((uint32_t *)(pucBuffer + 28)) = 0x32345673;//0xF04F0203;
   *((uint32_t *)(pucBuffer + 32)) = 0x42345674;//0xF04F0304;
   *((uint32_t *)(pucBuffer + 36)) = 0x52345675;//0xF04F0405;
   *((uint32_t *)(pucBuffer + 40)) = 0x62345676;//0xF04F0506;
   *((uint32_t *)(pucBuffer + 44)) = 0x72345677;//0xF04F0607;
   *((uint32_t *)(pucBuffer + 48)) = 0x82345678;//0xF04F0708;
   *((uint32_t *)(pucBuffer + 52)) = 0x92345679;//0xF04F0001;
   *((uint32_t *)(pucBuffer + 56)) = 0xA234567A;//0xF04F0102;
   *((uint32_t *)(pucBuffer + 60)) = 0xB234567B;//0xF04F0203;
   *((uint32_t *)(pucBuffer + 64)) = 0xC234567C;//0xF04F0304;
   *((uint32_t *)(pucBuffer + 68)) = 0xD234567D;//0xF04F0405;
   *((uint32_t *)(pucBuffer + 72)) = 0xE234567E;//0xF04F0506;
   iPacketLen = TRANSMIT_LEN(76);

   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);
   if(tyResult == DRVOPXD_ERROR_NO_ERROR)
       {
       printf("Sample Pgm Downloaded");
       }

// READ AND VERIFY DOWNLOAD
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_MULTIPLE_WORDS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((uint32_t *)(pucBuffer + 0x8)) =  CORTEXM;
   *((uint32_t *)(pucBuffer + 12)) =  0x20000000;
   *((uint32_t *)(pucBuffer + 16)) =  14;

   iPacketLen = TRANSMIT_LEN(20);
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);

// WRITE REGISTERS
   *((uint32_t *)(pucBuffer + 0)) = CMD_CODE_ARM_WRITE_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 4)) = usCore; 
   *((uint32_t *)(pucBuffer + 8)) =  CORTEXM;

   *((uint32_t *)(pucBuffer + 12)) =  10;
   *((uint32_t *)(pucBuffer + 16)) = 11;
   *((uint32_t *)(pucBuffer + 20)) = 12;
   *((uint32_t *)(pucBuffer + 24)) = 13;
   *((uint32_t *)(pucBuffer + 28)) = 14;
   *((uint32_t *)(pucBuffer + 32)) = 15;
   *((uint32_t *)(pucBuffer + 36)) = 16;
   *((uint32_t *)(pucBuffer + 40)) = 17;
   *((uint32_t *)(pucBuffer + 44)) = 18;
   *((uint32_t *)(pucBuffer + 48)) = 19;
   *((uint32_t *)(pucBuffer + 52)) = 20;
   *((uint32_t *)(pucBuffer + 56)) = 21;
   *((uint32_t *)(pucBuffer + 60)) = 22; //R12
   *((uint32_t *)(pucBuffer + 64)) = 0x800; //Current SP
   *((uint32_t *)(pucBuffer + 68)) = 0xC00; //LR
   *((uint32_t *)(pucBuffer + 72)) = 0x20000000; //PC
   *((uint32_t *)(pucBuffer + 76)) = 0x900;//xPSR
   *((uint32_t *)(pucBuffer + 80)) = 0xA01; //MSP
   *((uint32_t *)(pucBuffer + 84)) = 0xB00; //PSP                                                    
   *((uint32_t *)(pucBuffer + 92)) = 0xD00; //PSP 

   iPacketLen = TRANSMIT_LEN(96);
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);
   if(tyResult == DRVOPXD_ERROR_NO_ERROR)
       printf("Wrote Successfully");

// READ AGAIN
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS;                   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
   *((uint32_t *)(pucBuffer + 0x8)) =  CORTEXM;

   iPacketLen = TRANSMIT_LEN(12);
   // send packet
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);

   /* SINGLE STEP */
   for(int32_t i=0; i <=4; i++)
       {
       *((uint32_t *)(pucBuffer + 0x0))  = CMD_CODE_SINGLE_STEP;         // command code
       *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
       *((unsigned char *)(pucBuffer + 0x8))  = 5;
       // send packet
       iPacketLen = TRANSMIT_LEN(9);
    
       // send packet
       if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
          tyResult = DRVOPXD_ERROR_USB_DRIVER;
       else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
          tyResult = DRVOPXD_ERROR_USB_DRIVER;
       else
          {
          tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
          }
    // To WATCH PC
       *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS;                   // command code
       *((unsigned short *)(pucBuffer + 0x4)) = usCore; 
       *((uint32_t *)(pucBuffer + 0x8)) =  CORTEXM;
    
       iPacketLen = TRANSMIT_LEN(12);
       // send packet
       tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, iPacketLen);
       }
/* SINGLE STEP*/

   
   // ON_CHIP_MEMORY
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Memory(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, CORTEXM, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_SB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_SB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, CORTEXM, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }

   //EXECUTE_HB
   if (ucTestOnchipMem)
      {
      tyResult = DL_OPXD_Arm_Test_Execute_HB(ptyUsbDevHandle, tyDevHandle, ulOnChipMemory, CORTEXM, usCore);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      }


   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Bring PROCESSOR into normal mode
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore;
   pucBuffer[0x06] = 0x00;
   pucBuffer[0x07] = 0x0;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (8));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   DL_OPXD_UnlockDevice(tyDevHandle);

   return tyResult;
}

/****************************************************************************
     Function: DL_OPXD_Arm_TEST
     Engineer: Shameerudheen PT.
        Input: TyDevHandle tyDevHandle - handle to open device
               char *pszParams - Command line Parameters
       Output: error code DRVOPXD_ERROR_xxx
  Descr4iption: Test Arm cores Register Rd/wr, On-chip Memory Rd/Wr, 
                Execute/Software Break Point, Hardware Break Point
Date           Initials    Description
05-OCT-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_TEST(TyDevHandle tyDevHandle, char *pszParams)
{
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   uint32_t uiOnChipMemory;
   uint32_t ulCoreno;
   char pcCore[10] = "\0";
   char pcOnChipMemory[10] = "\0";
   char pucCoreNo[10];
   unsigned char ucTestOnchipMem = 1;
   uint32_t uiCoreNo = 0;
   unsigned short usCoreNo = 0;

   if (sscanf(pszParams, "%s ", pcCore) < 1)
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;

   if (!strcmpi(pcCore,"HELP"))
      {
      PrintMessage1(INFO_MESSAGE, "armtest <armcore> <core no> <on-chip memory> Test ARM Cores.");
      PrintMessage1(INFO_MESSAGE, "                            Testing Register RD/WR, On-Chip Memory RD/WR,");
      PrintMessage1(INFO_MESSAGE, "                            Execute/Software Break Point, Hardware Break Point");
      PrintMessage1(INFO_MESSAGE, "                            <armcore> ARM7DI, ARM7TDMI, ARM7TDMI-S, ARM740T,");
      PrintMessage1(INFO_MESSAGE, "                            ARM920T, ARM922T, ARM940T, ARM926EJ-S, ARM946E-S,");
      PrintMessage1(INFO_MESSAGE, "                            ARM966E-S, ARM968EJ-S");
      PrintMessage1(INFO_MESSAGE, "                            <core no> 0 for 1st, 1 for 2nd core in the chain.");
      PrintMessage1(INFO_MESSAGE, "                            <on-chip memory> Location of onchip memory in HEX");
      PrintMessage1(INFO_MESSAGE, "                            if no On-Chip Memory NIL");
      return ERR_NO_ERROR;
      }
      

   if (sscanf(pszParams, "%s %s %s", pcCore, pucCoreNo, pcOnChipMemory) < 3)
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;

   if (!strcmpi(pcOnChipMemory,"NIL"))
      {
         ucTestOnchipMem = 0;
         uiOnChipMemory = 0;
         if (sscanf(pszParams, "%s %d", pcCore, &uiCoreNo) < 2)
            return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
      }
   else
      {
         ucTestOnchipMem = 1;
         if (sscanf(pszParams, "%s %d %x", pcCore, &uiCoreNo, &uiOnChipMemory) < 3)
            return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
         ucTestOnchipMem = 1;
      }
   usCoreNo = (unsigned short)uiCoreNo;
   PrintMessage1(INFO_MESSAGE,"\nCore          : %s\nOn-Chip Memory: 0x%08X\n", pcCore, uiOnChipMemory);
   if (!strcmpi(pcCore,"ARM7DI"))
      ulCoreno = 0x7001;
   else if (!strcmpi(pcCore,"ARM7TDMI"))
      ulCoreno = 0x7002;
   else if (!strcmpi(pcCore,"ARM7TDMI-S"))
      ulCoreno = 0x7003;
   else if (!strcmpi(pcCore,"ARM740T"))
      ulCoreno = 0x7400;
   else if (!strcmpi(pcCore,"ARM920T"))
      ulCoreno = 0x9200;
   else if (!strcmpi(pcCore,"ARM922T"))
      ulCoreno = 0x9220;
   else if (!strcmpi(pcCore,"ARM940T"))
      ulCoreno = 0x9400;
   else if (!strcmpi(pcCore,"ARM926EJ-S"))
      ulCoreno = 0x9260;
   else if (!strcmpi(pcCore,"ARM946E-S"))
      ulCoreno = 0x9460;
   else if (!strcmpi(pcCore,"ARM966E-S"))
      ulCoreno = 0x9660;
   else if (!strcmpi(pcCore,"ARM968E-S"))
      ulCoreno = 0x9680;
   else if (!strcmpi(pcCore,"CortexM3"))
      ulCoreno = 0xC300;
   else if (!strcmpi(pcCore,"CortexA8"))
      ulCoreno = 0xCA80;
   else if (!strcmpi(pcCore,"CortexA9"))
      ulCoreno = 0xCA90;
   else
      ulCoreno = 0;

   switch (ulCoreno)
      {
      case 0x7001: //ARM7DI
         return DL_OPXD_Arm_Test_Arm7DI(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x7002: //ARM7TDMI
         //printf("ARM7TMDI core\n");
         return DL_OPXD_Arm_Test_Arm7TDMI(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x7003: //ARM7TDMI-S
         return DL_OPXD_Arm_Test_Arm7TDMIS(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x7400: //ARM740T
         //if you need any special settings for ARM740T write a new function
         return DL_OPXD_Arm_Test_Arm7TDMI(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9200: //ARM920T
         //if you need any special settings for ARM920T write a new function
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9220: //ARM922T
         //if you need any special settings for ARM922T write a new function
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9400: //ARM940T
         //if you need any special settings for ARM940T write a new function
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9260: //ARM926EJ-S
         //if you need any special settings for ARM926EJ-S write a new function
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9460: //ARM946E-S
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9660: //ARM966E-S
         //if you need any special settings for ARM966E-S write a new function
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0x9680: //ARM968E-S
         //if you need any special settings for ARM968E-S write a new function
         return DL_OPXD_Arm_TEST_Arm946ES(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0xC300:
          return DL_OPXD_Arm_TEST_CortexM3(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0xCA80:
          return DL_OPXD_Arm_TEST_CortexA8(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      case 0xCA90:
          return DL_OPXD_Arm_TEST_CortexA9(tyDevHandle, ucTestOnchipMem, uiOnChipMemory, usCoreNo);
      default:
         printf("\nNo Support for this %s core\n\n", pcCore);
         tyResult = DRVOPXD_ERROR_NOT_A_VALID_CORE;
      }
   return(tyResult);
}
/****************************************************************************
     Function: PrintMessage1
     Engineer: Vitezslav Hola
        Input: TyMessageType tyMessageType - type of message
               char * pszToFormat - format of message with variable number of parameters (as printf)
       Output: none
  Description: prints out messages to console, debug window, etc...
Date           Initials    Description
19-Jan-2007    VH          Initial
10-May-2012    SPT         Corrected for building in linux
*****************************************************************************/
void PrintMessage1(TyMessageType tyMessageType, const char * pszToFormat, ...)
{
   char     szBuffer[4096];
   char     szDisplay[4096];
   int32_t uiResult;
   va_list  marker; // = NULL;

   va_start(marker, pszToFormat);
   uiResult = vsprintf(szBuffer, pszToFormat, marker); 
   if (uiResult >= 0)
      va_end  (marker);

   switch (tyMessageType)
      {
      case HELP_MESSAGE:
         strcpy(szDisplay, szBuffer);
         break;
      case INFO_MESSAGE:
         sprintf(szDisplay, "%s\n", szBuffer);
         break;
      case ERROR_MESSAGE:
         sprintf(szDisplay, "Error: %s\n", szBuffer);
         break;
      case DEBUG_MESSAGE:
         sprintf(szDisplay, "Debug: %s\n", szBuffer);
         break;
      case LOG_MESSAGE:
//         LogMessage(szBuffer, "");
         return;
      default:
         return;
      }

   printf("%s",szDisplay);
}

/****************************************************************************
     Function: DL_OPXD_Arm_GetMultiCoreInfo
     Engineer: Shameerudheen P T.
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t *pulNoOfCores - Return Number cores in the current scan Chain
               uint32_t *pulIRLengthArray - Return IR Length Array
       Output: error code DRVOPXD_ERROR_xxx
  Description: Writes multiple words to specified target memory onwards
Date           Initials    Description
10-Oct-2007      SPT        Initial
03-Jan-2008      SPT        Did correction
****************************************************************************/
TyError DL_OPXD_Arm_GetMultiCoreInfo(TyDevHandle tyDevHandle, 
                                   uint32_t *pulNoOfCores, 
                                   uint32_t *pulIRLengthArray)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   uint32_t *pulTempPointer;
   uint32_t ulI;

   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Reseting TAP
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;
   pucBuffer[0x4] = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 5);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Bringin TAP to RTI State
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;
   *((unsigned short *)(pucBuffer + 0x4)) = 0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 6);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Getting multicore information
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_ARM_GET_MULTICORE_INFO;
   iPacketLen = TRANSMIT_LEN(4);
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      //tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if (tyResult == ERR_NO_ERROR)
      {
      *pulNoOfCores = *((uint32_t *)(pucBuffer + 0x04));
      pulTempPointer = ((uint32_t *)(pucBuffer + 0x08));
      if ((*pulNoOfCores) > 16)
          return ERR_ARM_MULTICORE_CALC_ERROR;

      if((*pulNoOfCores) != 0)
         {
         for (ulI = 0; ulI < (*pulNoOfCores); ulI++)
            {
             pulIRLengthArray[ulI] = *pulTempPointer; 
             pulTempPointer++;
            }
         }
      }
   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Arm_RReg
     Engineer: Shameerudheen PT.
        Input: TyDevHandle tyDevHandle - handle to open device
               TyArm9Registors *ptyArm9Registors - Pointer to Register Array
               char *pszParams - Command line parameters
       Output: error code DRVOPXD_ERROR_xxx
 Descr4iption: Don't care about this function this is just for immediate testing
               will vary every time a new test requirement came. It is an internal
               function  will not appear in the help command
Date           Initials    Description
16-Jul-2007    SPT          Initial
****************************************************************************/
TyError DL_OPXD_Arm_RReg(TyDevHandle tyDevHandle, TyArm9Registors *ptyArm9Registors, char *pszParams)
{  //uncomment as per test you want
   //enable one of these defines for
   #define REG_WRITE //Register testing
   //#define MEMWRITE //Memory testing
   //#define EXECUTE //Execution testing

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   //int32_t iPacketLen;
   #ifdef REG_WRITE
      static uint32_t ulTemp = 0;
   #endif
   #ifdef EXECUTE
      int32_t i;
   #endif
   #ifdef MEMWRITE
      int32_t i;
      static uint32_t ulTemp = 0;
   #endif
   uint32_t uiAddress, uiData;
   unsigned short usCore;
   uint32_t uiCore;
   uint32_t uiProcessor;
   uint32_t ulCount = 0;
   char pcHelp[10] = "\0";

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
       return DRVOPXD_ERROR_INVALID_HANDLE;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   if (sscanf(pszParams, "%s ", pcHelp) < 1)
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;

   if(!strcmp(pcHelp,"HELP"))
      {
      printf("armrreg <core no.> <processor> <Address> <Data>\nThis is a test command, so it`s usage may vary\n"); 
      return ERR_NO_ERROR;
      }

   if (sscanf(pszParams, "%x %x %x %x", &uiCore, &uiProcessor, &uiAddress, &uiData) < 3)
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
   usCore = (unsigned short) uiCore;

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_SCAN_RESET_TAP;   
   pucBuffer[0x4] = 0x0;                         
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 5);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;



   //Bringin TAP to RTI State
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI;  
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;                         
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 6);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Set Safe Non Vector Address
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SET_SAFE_NON_VECTOR_ADDRESS;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
   pucBuffer[0x08] = (unsigned char)uiProcessor; 
   *((uint32_t *)(pucBuffer + 0xC)) = 0x10000;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 16);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Select Scan Chain
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
   pucBuffer[0x06] = 0x02;
   pucBuffer[0x07] = 0x01;
   pucBuffer[0x08] = (unsigned char)uiProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Write ICE Breaker controll register debug requist
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_ICE_BREAKER;     
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
   pucBuffer[0x06] = 0;
   pucBuffer[0x07] = 0;
   *((uint32_t *)(pucBuffer + 0x8))= 0x00;
   *((uint32_t *)(pucBuffer + 0xC))= 0x02;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 16);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   do
      {
      //ARM STATUS PROC
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;
      *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = (unsigned char)uiProcessor;
      pucBuffer[0x09] = 0x1;
      pucBuffer[0x0a] = 0x1;
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      
      ulCount++;
      if (ulCount == 500)
         {
         printf("executing = 0x%x thumb = 0x%x watch point = 0x%x\n",pucBuffer[0x05],pucBuffer[0x06],pucBuffer[0x07]);
         printf("ARM7 Stop Problem = 0x%x \n",pucBuffer[0x04]);
         PrintMessage1(ERROR_MESSAGE,"Bring uiProcessor to Debug mode Failed!!");
         return DRVOPXD_ERROR_TARGET_BROCKEN;
         }
      }while (pucBuffer[0x05]  != 0x0);


   //Select Scan Chain
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_SELECT_SCAN_CHAIN;
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
   pucBuffer[0x06] = 0x01;
   pucBuffer[0x07] = 0x01;
   pucBuffer[0x08] = (unsigned char)uiProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 9);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;



#ifdef REG_WRITE
   //Read Registers
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x4)) = usCore ;                    
   *((uint32_t *)(pucBuffer + 0x8)) = uiProcessor;  

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   ptyArm9Registors->ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   ptyArm9Registors->ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   ptyArm9Registors->ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   ptyArm9Registors->ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   ptyArm9Registors->ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   ptyArm9Registors->ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   ptyArm9Registors->ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   ptyArm9Registors->ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   ptyArm9Registors->ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   ptyArm9Registors->ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   ptyArm9Registors->ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   ptyArm9Registors->ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   ptyArm9Registors->ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   ptyArm9Registors->ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   ptyArm9Registors->ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   ptyArm9Registors->ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   ptyArm9Registors->ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   ptyArm9Registors->ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   ptyArm9Registors->ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   ptyArm9Registors->ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   ptyArm9Registors->ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   ptyArm9Registors->ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   ptyArm9Registors->ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   ptyArm9Registors->ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   ptyArm9Registors->ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   ptyArm9Registors->ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   ptyArm9Registors->ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   ptyArm9Registors->ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   ptyArm9Registors->ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   ptyArm9Registors->ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   ptyArm9Registors->ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   ptyArm9Registors->ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   ptyArm9Registors->ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   ptyArm9Registors->ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   ptyArm9Registors->ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   ptyArm9Registors->ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   ptyArm9Registors->ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));



   printf("R0      = 0x%08x\nR1      = 0x%08x\nR2      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR0,(uint32_t) ptyArm9Registors->ulR1,(uint32_t) ptyArm9Registors->ulR2);
   printf("R3      = 0x%08x\nR4      = 0x%08x\nR5      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR3,(uint32_t) ptyArm9Registors->ulR4,(uint32_t) ptyArm9Registors->ulR5);
   printf("R6      = 0x%08x\nR7      = 0x%08x\nR8      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR6,(uint32_t) ptyArm9Registors->ulR7,(uint32_t) ptyArm9Registors->ulR8);
   printf("R9      = 0x%08x\nR10     = 0x%08x\nR11     = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR9,(uint32_t) ptyArm9Registors->ulR10,(uint32_t) ptyArm9Registors->ulR11);
   printf("R12     = 0x%08x\nR13Usr  = 0x%08x\nR14Usr  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR12,(uint32_t) ptyArm9Registors->ulR13Usr,(uint32_t) ptyArm9Registors->ulR14Usr);
   printf("PC      = 0x%08x\nCPSR    = 0x%08x\n",(uint32_t) ptyArm9Registors->ulPC,(uint32_t) ptyArm9Registors->ulCPSR);
   printf("R8Fiq   = 0x%08x\nR9Fiq   = 0x%08x\nR10Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR8Fiq,(uint32_t) ptyArm9Registors->ulR9Fiq,(uint32_t) ptyArm9Registors->ulR10Fiq);
   printf("R11Fiq  = 0x%08x\nR12Fiq  = 0x%08x\nR13Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR11Fiq,(uint32_t) ptyArm9Registors->ulR12Fiq,(uint32_t) ptyArm9Registors->ulR13Fiq);
   printf("R14Fiq  = 0x%08x\nSPSRFiq = 0x%08x\nR13Irq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Fiq,(uint32_t) ptyArm9Registors->ulSPSRFiq,(uint32_t) ptyArm9Registors->ulR13Irq);
   printf("R14Irq  = 0x%08x\nSPSRIrq = 0x%08x\nR13Svc  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Irq,(uint32_t) ptyArm9Registors->ulSPSRIrq,(uint32_t) ptyArm9Registors->ulR13Svc);
   printf("R14Svc  = 0x%08x\nSPSRSvc = 0x%08x\nR13Abt  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Svc,(uint32_t) ptyArm9Registors->ulSPSRSvc,(uint32_t) ptyArm9Registors->ulR13Abt);
   printf("R14Abt  = 0x%08x\nSPSRAbt = 0x%08x\nR13und  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Abt,(uint32_t) ptyArm9Registors->ulSPSRAbt,(uint32_t) ptyArm9Registors->ulR13Und);
   printf("R14Und  = 0x%08x\nSPSRUnd = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Und,(uint32_t) ptyArm9Registors->ulSPSRUnd);
   printf("\n\n\n");

   //Writing Registers
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;                     
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;

   *((uint32_t *)(pucBuffer + 0x7C)) = ulTemp++; //ptyArm9Registors->ulR0;
   *((uint32_t *)(pucBuffer + 0x80)) = ulTemp++; //0x12345678; //ptyArm9Registors->ulR1;
   *((uint32_t *)(pucBuffer + 0x84)) = ulTemp++; //0x12345678; //ptyArm9Registors->ulR2;
   *((uint32_t *)(pucBuffer + 0x88)) = ulTemp++; //0xA5A51023; //ptyArm9Registors->ulR3;
   *((uint32_t *)(pucBuffer + 0x8C)) = ulTemp++; //0x12345678; //ptyArm9Registors->ulR4;
   *((uint32_t *)(pucBuffer + 0x90)) = ulTemp++; //ptyArm9Registors->ulR5;
   *((uint32_t *)(pucBuffer + 0x94)) = ulTemp++; //ptyArm9Registors->ulR6;
   *((uint32_t *)(pucBuffer + 0x98)) = ulTemp++; //ptyArm9Registors->ulR7;

   *((uint32_t *)(pucBuffer + 0x5C)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR8;
   *((uint32_t *)(pucBuffer + 0x60)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR9;
   *((uint32_t *)(pucBuffer + 0x64)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR10;
   *((uint32_t *)(pucBuffer + 0x68)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR11;
   *((uint32_t *)(pucBuffer + 0x6C)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR12;
   *((uint32_t *)(pucBuffer + 0x70)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR13Usr;
   *((uint32_t *)(pucBuffer + 0x74)) = 0xB0000000 + ulTemp++; //ptyArm9Registors->ulR14Usr;

   *((uint32_t *)(pucBuffer + 0x9C)) = ptyArm9Registors->ulPC;

   *((uint32_t *)(pucBuffer + 0x78)) = (ptyArm9Registors->ulCPSR & 0xFFFFFF00) | 0xD1;

   *((uint32_t *)(pucBuffer + 0x10)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR8Fiq;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR9Fiq;
   *((uint32_t *)(pucBuffer + 0x18)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR10Fiq;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR11Fiq;
   *((uint32_t *)(pucBuffer + 0x20)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR12Fiq;
   *((uint32_t *)(pucBuffer + 0x24)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR13Fiq;
   *((uint32_t *)(pucBuffer + 0x28)) = 0xF0000000 + ulTemp++; //ptyArm9Registors->ulR14Fiq;                                                                 
   *((uint32_t *)(pucBuffer + 0x0C)) = ptyArm9Registors->ulSPSRFiq ;

   *((uint32_t *)(pucBuffer + 0x30)) = 0x10000000 + ulTemp++; //ptyArm9Registors->ulR13Irq;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x10000000 + ulTemp++;  //ptyArm9Registors->ulR14Irq;
   *((uint32_t *)(pucBuffer + 0x2C)) = ptyArm9Registors->ulSPSRIrq;

   *((uint32_t *)(pucBuffer + 0x3C)) = 0xC0000000 + ulTemp++;  //ptyArm9Registors->ulR13Svc;
   *((uint32_t *)(pucBuffer + 0x40)) = 0xC0000000 + ulTemp++;  //ptyArm9Registors->ulR14Svc;
   *((uint32_t *)(pucBuffer + 0x38)) = ptyArm9Registors->ulSPSRSvc;

   *((uint32_t *)(pucBuffer + 0x48)) = 0xA0000000 + ulTemp++; //ptyArm9Registors->ulR13Abt;
   *((uint32_t *)(pucBuffer + 0x4C)) = 0xA0000000 + ulTemp++; //ptyArm9Registors->ulR14Abt;
   *((uint32_t *)(pucBuffer + 0x44)) = ptyArm9Registors->ulSPSRAbt;

   *((uint32_t *)(pucBuffer + 0x54)) = 0xD0000000 + ulTemp++; //ptyArm9Registors->ulR13Und;
   *((uint32_t *)(pucBuffer + 0x58)) = 0xD0000000 + ulTemp++; //ptyArm9Registors->ulR14Und;
   *((uint32_t *)(pucBuffer + 0x50)) = ptyArm9Registors->ulSPSRUnd;


   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (40 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   //Reading again 
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS;  
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;                     
   pucBuffer[0x06] = 0x0;
   pucBuffer[0x07] = 0x0;
   pucBuffer[0x08] = (unsigned char)uiProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (9));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   ptyArm9Registors->ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   ptyArm9Registors->ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   ptyArm9Registors->ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   ptyArm9Registors->ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   ptyArm9Registors->ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   ptyArm9Registors->ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   ptyArm9Registors->ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   ptyArm9Registors->ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   ptyArm9Registors->ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   ptyArm9Registors->ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   ptyArm9Registors->ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   ptyArm9Registors->ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   ptyArm9Registors->ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   ptyArm9Registors->ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   ptyArm9Registors->ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   ptyArm9Registors->ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   ptyArm9Registors->ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   ptyArm9Registors->ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   ptyArm9Registors->ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   ptyArm9Registors->ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   ptyArm9Registors->ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   ptyArm9Registors->ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   ptyArm9Registors->ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   ptyArm9Registors->ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   ptyArm9Registors->ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   ptyArm9Registors->ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   ptyArm9Registors->ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   ptyArm9Registors->ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   ptyArm9Registors->ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   ptyArm9Registors->ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   ptyArm9Registors->ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   ptyArm9Registors->ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   ptyArm9Registors->ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   ptyArm9Registors->ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   ptyArm9Registors->ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   ptyArm9Registors->ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   ptyArm9Registors->ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));
      

   printf("R0      = 0x%08x\nR1      = 0x%08x\nR2      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR0,(uint32_t) ptyArm9Registors->ulR1,(uint32_t) ptyArm9Registors->ulR2);
   printf("R3      = 0x%08x\nR4      = 0x%08x\nR5      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR3,(uint32_t) ptyArm9Registors->ulR4,(uint32_t) ptyArm9Registors->ulR5);
   printf("R6      = 0x%08x\nR7      = 0x%08x\nR8      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR6,(uint32_t) ptyArm9Registors->ulR7,(uint32_t) ptyArm9Registors->ulR8);
   printf("R9      = 0x%08x\nR10     = 0x%08x\nR11     = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR9,(uint32_t) ptyArm9Registors->ulR10,(uint32_t) ptyArm9Registors->ulR11);
   printf("R12     = 0x%08x\nR13Usr  = 0x%08x\nR14Usr  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR12,(uint32_t) ptyArm9Registors->ulR13Usr,(uint32_t) ptyArm9Registors->ulR14Usr);
   printf("PC      = 0x%08x\nCPSR    = 0x%08x\n",(uint32_t) ptyArm9Registors->ulPC,(uint32_t) ptyArm9Registors->ulCPSR);
   printf("R8Fiq   = 0x%08x\nR9Fiq   = 0x%08x\nR10Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR8Fiq,(uint32_t) ptyArm9Registors->ulR9Fiq,(uint32_t) ptyArm9Registors->ulR10Fiq);
   printf("R11Fiq  = 0x%08x\nR12Fiq  = 0x%08x\nR13Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR11Fiq,(uint32_t) ptyArm9Registors->ulR12Fiq,(uint32_t) ptyArm9Registors->ulR13Fiq);
   printf("R14Fiq  = 0x%08x\nSPSRFiq = 0x%08x\nR13Irq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Fiq,(uint32_t) ptyArm9Registors->ulSPSRFiq,(uint32_t) ptyArm9Registors->ulR13Irq);
   printf("R14Irq  = 0x%08x\nSPSRIrq = 0x%08x\nR13Svc  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Irq,(uint32_t) ptyArm9Registors->ulSPSRIrq,(uint32_t) ptyArm9Registors->ulR13Svc);
   printf("R14Svc  = 0x%08x\nSPSRSvc = 0x%08x\nR13Abt  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Svc,(uint32_t) ptyArm9Registors->ulSPSRSvc,(uint32_t) ptyArm9Registors->ulR13Abt);
   printf("R14Abt  = 0x%08x\nSPSRAbt = 0x%08x\nR13und  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Abt,(uint32_t) ptyArm9Registors->ulSPSRAbt,(uint32_t) ptyArm9Registors->ulR13Und);
   printf("R14Und  = 0x%08x\nSPSRUnd = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Und,(uint32_t) ptyArm9Registors->ulSPSRUnd);

   DL_OPXD_UnlockDevice(tyDevHandle);

#endif //REG_WRITE

#ifdef MEMWRITE

   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_REGISTERS; 
   *((uint32_t *)(pucBuffer + 0x4)) = usCore ;
   *((uint32_t *)(pucBuffer + 0x8)) = uiProcessor;  
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   *((uint32_t *)(pucBuffer + 0x0C)) = uiAddress;
   *((uint32_t *)(pucBuffer + 0x10)) = 2;
   *((uint32_t *)(pucBuffer + 0x14)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x18)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x1C)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x20)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x24)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x28)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x2C)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x30)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x34)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x38)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x3C)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x40)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x44)) = ulTemp++; 
   *((uint32_t *)(pucBuffer + 0x48)) = ulTemp++; 

   *((uint32_t *)(pucBuffer + 0x4C)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x50)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x54)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x58)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x5C)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x60)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x64)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x68)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x6C)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x70)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x74)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x78)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x7C)) = ulTemp++;
   *((uint32_t *)(pucBuffer + 0x80)) = ulTemp++;


   iPacketLen = TRANSMIT_LEN((5*4));
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      {
      return DRVOPXD_ERROR_USB_DRIVER;
      }
   iPacketLen = 28*4;
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)(pucBuffer + 0x14), iPacketLen, DRV_TIMEOUT) != iPacketLen)
      {
      return DRVOPXD_ERROR_USB_DRIVER;
      } 
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      {
      printf("USB Receive Error\n");
      return DRVOPXD_ERROR_USB_DRIVER;
      } 
   else
      tyResult = DL_OPXD_ConvertArmDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));


   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_MULTIPLE_BLOCKS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor; 
   *((uint32_t *)(pucBuffer + 0x0C)) = uiAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) =  2;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   for ( i = 0 ; i < (28); i++)
      {
      printf("read data%3d = 0x%08x \n",(i+1),*((uint32_t *)(pucBuffer + ((i * 4) + 4))));
      }
#endif //MEMWRITE

#ifdef EXECUTE
   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

//Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;                    
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   ptyArm9Registors->ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   ptyArm9Registors->ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   ptyArm9Registors->ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   ptyArm9Registors->ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   ptyArm9Registors->ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   ptyArm9Registors->ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   ptyArm9Registors->ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   ptyArm9Registors->ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   ptyArm9Registors->ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   ptyArm9Registors->ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   ptyArm9Registors->ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   ptyArm9Registors->ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   ptyArm9Registors->ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   ptyArm9Registors->ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   ptyArm9Registors->ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   ptyArm9Registors->ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   ptyArm9Registors->ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   ptyArm9Registors->ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   ptyArm9Registors->ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   ptyArm9Registors->ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   ptyArm9Registors->ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   ptyArm9Registors->ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   ptyArm9Registors->ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   ptyArm9Registors->ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   ptyArm9Registors->ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   ptyArm9Registors->ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   ptyArm9Registors->ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   ptyArm9Registors->ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   ptyArm9Registors->ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   ptyArm9Registors->ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   ptyArm9Registors->ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   ptyArm9Registors->ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   ptyArm9Registors->ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   ptyArm9Registors->ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   ptyArm9Registors->ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   ptyArm9Registors->ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   ptyArm9Registors->ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));
   
   printf("R0      = 0x%08x\nR1      = 0x%08x\nR2      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR0,(uint32_t) ptyArm9Registors->ulR1,(uint32_t) ptyArm9Registors->ulR2);
   printf("R3      = 0x%08x\nR4      = 0x%08x\nR5      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR3,(uint32_t) ptyArm9Registors->ulR4,(uint32_t) ptyArm9Registors->ulR5);
   printf("R6      = 0x%08x\nR7      = 0x%08x\nR8      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR6,(uint32_t) ptyArm9Registors->ulR7,(uint32_t) ptyArm9Registors->ulR8);
   printf("R9      = 0x%08x\nR10     = 0x%08x\nR11     = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR9,(uint32_t) ptyArm9Registors->ulR10,(uint32_t) ptyArm9Registors->ulR11);
   printf("R12     = 0x%08x\nR13Usr  = 0x%08x\nR14Usr  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR12,(uint32_t) ptyArm9Registors->ulR13Usr,(uint32_t) ptyArm9Registors->ulR14Usr);
   printf("PC      = 0x%08x\nCPSR    = 0x%08x\n",(uint32_t) ptyArm9Registors->ulPC,(uint32_t) ptyArm9Registors->ulCPSR);
   printf("R8Fiq   = 0x%08x\nR9Fiq   = 0x%08x\nR10Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR8Fiq,(uint32_t) ptyArm9Registors->ulR9Fiq,(uint32_t) ptyArm9Registors->ulR10Fiq);
   printf("R11Fiq  = 0x%08x\nR12Fiq  = 0x%08x\nR13Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR11Fiq,(uint32_t) ptyArm9Registors->ulR12Fiq,(uint32_t) ptyArm9Registors->ulR13Fiq);
   printf("R14Fiq  = 0x%08x\nSPSRFiq = 0x%08x\nR13Irq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Fiq,(uint32_t) ptyArm9Registors->ulSPSRFiq,(uint32_t) ptyArm9Registors->ulR13Irq);
   printf("R14Irq  = 0x%08x\nSPSRIrq = 0x%08x\nR13Svc  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Irq,(uint32_t) ptyArm9Registors->ulSPSRIrq,(uint32_t) ptyArm9Registors->ulR13Svc);
   printf("R14Svc  = 0x%08x\nSPSRSvc = 0x%08x\nR13Abt  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Svc,(uint32_t) ptyArm9Registors->ulSPSRSvc,(uint32_t) ptyArm9Registors->ulR13Abt);
   printf("R14Abt  = 0x%08x\nSPSRAbt = 0x%08x\nR13und  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Abt,(uint32_t) ptyArm9Registors->ulSPSRAbt,(uint32_t) ptyArm9Registors->ulR13Und);
   printf("R14Und  = 0x%08x\nSPSRUnd = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Und,(uint32_t) ptyArm9Registors->ulSPSRUnd);
   printf("\n\n\n");

   //loading program   
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS;  
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
   pucBuffer[6] = 0x0;
   pucBuffer[7] = 0x0;
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   *((uint32_t *)(pucBuffer + 0x0C)) = (uiAddress); 
   *((uint32_t *)(pucBuffer + 0x10)) = 1;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xE1A06007; 
   *((uint32_t *)(pucBuffer + 0x18)) = 0xE1A04005; 
   *((uint32_t *)(pucBuffer + 0x1C)) = 0xEEEEEEEE; 
   *((uint32_t *)(pucBuffer + 0x20)) = 0xE1A04005; 
   *((uint32_t *)(pucBuffer + 0x24)) = 0xE1A00000; 
   *((uint32_t *)(pucBuffer + 0x28)) = 0xE1A02003;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0xE1A00000;
   *((uint32_t *)(pucBuffer + 0x30)) = 0xE1A00000;
   *((uint32_t *)(pucBuffer + 0x34)) = 0xE1A00000; 
   *((uint32_t *)(pucBuffer + 0x38)) = 0xE1A00000; 
   *((uint32_t *)(pucBuffer + 0x3C)) = 0xE1A00000; 
   *((uint32_t *)(pucBuffer + 0x40)) = 0xE1A00000; 
   *((uint32_t *)(pucBuffer + 0x44)) = 0xE1A00000; 
   *((uint32_t *)(pucBuffer + 0x48)) = 0xEEEEEEEE; 


   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (19 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_READ_MULTIPLE_WORDS; 
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
   pucBuffer[6] = 0x0;
   pucBuffer[7] = 0x0;
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor; 
   *((uint32_t *)(pucBuffer + 0x0C)) = uiAddress; 
   *((uint32_t *)(pucBuffer + 0x10)) =  14;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   for (i = 0 ; i < (14); i++)
      {
      printf("read data%3d = 0x%08x \n",(i+1),*((uint32_t *)(pucBuffer + ((i * 4) + 4))));
      }

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_SETUP_EXECUTE_PROC;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0xFFFFFFFF;
   *((uint32_t *)(pucBuffer + 0x14)) = 0xEEEEEEEE;
   *((uint32_t *)(pucBuffer + 0x18)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x1C)) = 0x104;
   *((uint32_t *)(pucBuffer + 0x20)) = 0xF7;
   *((uint32_t *)(pucBuffer + 0x24)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x28)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x2C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x30)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x34)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x38)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (15 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_CHECK_FOR_DATA_ABORT;
   *((uint32_t *)(pucBuffer + 0x4)) = usCore ;
   *((uint32_t *)(pucBuffer + 0x8)) = uiProcessor; //Arm uiProcessor
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (3 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;



   //Writing Registers
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_WRITE_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;                     
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   *((uint32_t *)(pucBuffer + 0x0C)) = ptyArm9Registors->ulSPSRFiq ;
   *((uint32_t *)(pucBuffer + 0x10)) = ptyArm9Registors->ulR8Fiq;
   *((uint32_t *)(pucBuffer + 0x14)) = ptyArm9Registors->ulR9Fiq;
   *((uint32_t *)(pucBuffer + 0x18)) = ptyArm9Registors->ulR10Fiq;
   *((uint32_t *)(pucBuffer + 0x1C)) = ptyArm9Registors->ulR11Fiq;
   *((uint32_t *)(pucBuffer + 0x20)) = ptyArm9Registors->ulR12Fiq;
   *((uint32_t *)(pucBuffer + 0x24)) = ptyArm9Registors->ulR13Fiq;
   *((uint32_t *)(pucBuffer + 0x28)) = ptyArm9Registors->ulR14Fiq;
   *((uint32_t *)(pucBuffer + 0x2C)) = ptyArm9Registors->ulSPSRIrq;
   *((uint32_t *)(pucBuffer + 0x30)) = ptyArm9Registors->ulR13Irq;
   *((uint32_t *)(pucBuffer + 0x34)) = ptyArm9Registors->ulR14Irq;
   *((uint32_t *)(pucBuffer + 0x38)) = ptyArm9Registors->ulSPSRSvc;
   *((uint32_t *)(pucBuffer + 0x3C)) = ptyArm9Registors->ulR13Svc;
   *((uint32_t *)(pucBuffer + 0x40)) = ptyArm9Registors->ulR14Svc;
   *((uint32_t *)(pucBuffer + 0x44)) = ptyArm9Registors->ulSPSRAbt;
   *((uint32_t *)(pucBuffer + 0x48)) = ptyArm9Registors->ulR13Abt;
   *((uint32_t *)(pucBuffer + 0x4C)) = ptyArm9Registors->ulR14Abt;
   *((uint32_t *)(pucBuffer + 0x50)) = ptyArm9Registors->ulSPSRUnd;
   *((uint32_t *)(pucBuffer + 0x54)) = ptyArm9Registors->ulR13Und;
   *((uint32_t *)(pucBuffer + 0x58)) = ptyArm9Registors->ulR14Und;
   *((uint32_t *)(pucBuffer + 0x5C)) = ptyArm9Registors->ulR8;
   *((uint32_t *)(pucBuffer + 0x60)) = ptyArm9Registors->ulR9;
   *((uint32_t *)(pucBuffer + 0x64)) = ptyArm9Registors->ulR10;
   *((uint32_t *)(pucBuffer + 0x68)) = ptyArm9Registors->ulR11;
   *((uint32_t *)(pucBuffer + 0x6C)) = ptyArm9Registors->ulR12;
   *((uint32_t *)(pucBuffer + 0x70)) = ptyArm9Registors->ulR13Usr;
   *((uint32_t *)(pucBuffer + 0x74)) = ptyArm9Registors->ulR14Usr;
   *((uint32_t *)(pucBuffer + 0x78)) = ptyArm9Registors->ulCPSR;
   *((uint32_t *)(pucBuffer + 0x7C)) = ptyArm9Registors->ulR0;
   *((uint32_t *)(pucBuffer + 0x80)) = ptyArm9Registors->ulR1;
   *((uint32_t *)(pucBuffer + 0x84)) = 0x2; //ptyArm9Registors->ulR2;
   *((uint32_t *)(pucBuffer + 0x88)) = 0x3; //ptyArm9Registors->ulR3;
   *((uint32_t *)(pucBuffer + 0x8C)) = 0x4; //ptyArm9Registors->ulR4;
   *((uint32_t *)(pucBuffer + 0x90)) = 0x5; //ptyArm9Registors->ulR5;
   *((uint32_t *)(pucBuffer + 0x94)) = 0x6; // ptyArm9Registors->ulR6;
   *((uint32_t *)(pucBuffer + 0x98)) = 0x7; //ptyArm9Registors->ulR7;
   *((uint32_t *)(pucBuffer + 0x9C)) = (uiAddress + 4); //ptyArm9Registors->ulPC;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (160));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_EXECUTE_PROC;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   *((uint32_t *)(pucBuffer + 0x0C)) = 0x0;
   *((uint32_t *)(pucBuffer + 0x10)) = 0x0;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (5 * 4));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;


   do
      {
      //ARM STATUS PROC
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_STATUS_PROC;
      *((unsigned short *)(pucBuffer + 0x4)) = usCore ;
      pucBuffer[0x06] = 0;
      pucBuffer[0x07] = 0;
      pucBuffer[0x08] = uiProcessor;
      pucBuffer[0x09] = 0x1;
      pucBuffer[0x0a] = 0x1;
      tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 11);
      if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
         return  tyResult;
      printf("executing = 0x%x thumb = 0x%x watch point = 0x%x\n",pucBuffer[0x05],pucBuffer[0x06],pucBuffer[0x07]);
      printf("ARM7 Stop Problem = 0x%x \n",pucBuffer[0x04]);
      }while (pucBuffer[0x05]  != 0x0);


   //Read Registers
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARM_READ_REGISTERS;
   *((uint32_t *)(pucBuffer + 0x04)) = usCore ;                    
   *((uint32_t *)(pucBuffer + 0x08)) = uiProcessor;
   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, 8);
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   ptyArm9Registors->ulR0      = *((uint32_t *)(pucBuffer + 0x04));
   ptyArm9Registors->ulR1      = *((uint32_t *)(pucBuffer + 0x08));
   ptyArm9Registors->ulR2      = *((uint32_t *)(pucBuffer + 0x0C));
   ptyArm9Registors->ulR3      = *((uint32_t *)(pucBuffer + 0x10));
   ptyArm9Registors->ulR4      = *((uint32_t *)(pucBuffer + 0x14));
   ptyArm9Registors->ulR5      = *((uint32_t *)(pucBuffer + 0x18));
   ptyArm9Registors->ulR6      = *((uint32_t *)(pucBuffer + 0x1C));
   ptyArm9Registors->ulR7      = *((uint32_t *)(pucBuffer + 0x20));
   ptyArm9Registors->ulPC      = *((uint32_t *)(pucBuffer + 0x24));
   ptyArm9Registors->ulCPSR    = *((uint32_t *)(pucBuffer + 0x28));
   ptyArm9Registors->ulR8      = *((uint32_t *)(pucBuffer + 0x2C));
   ptyArm9Registors->ulR9      = *((uint32_t *)(pucBuffer + 0x30));
   ptyArm9Registors->ulR10     = *((uint32_t *)(pucBuffer + 0x34));
   ptyArm9Registors->ulR11     = *((uint32_t *)(pucBuffer + 0x38));
   ptyArm9Registors->ulR12     = *((uint32_t *)(pucBuffer + 0x3C));
   ptyArm9Registors->ulR13Usr  = *((uint32_t *)(pucBuffer + 0x40));
   ptyArm9Registors->ulR14Usr  = *((uint32_t *)(pucBuffer + 0x44));
   ptyArm9Registors->ulR8Fiq   = *((uint32_t *)(pucBuffer + 0x48));
   ptyArm9Registors->ulR9Fiq   = *((uint32_t *)(pucBuffer + 0x4C));
   ptyArm9Registors->ulR10Fiq  = *((uint32_t *)(pucBuffer + 0x50));
   ptyArm9Registors->ulR11Fiq  = *((uint32_t *)(pucBuffer + 0x54));
   ptyArm9Registors->ulR12Fiq  = *((uint32_t *)(pucBuffer + 0x58));
   ptyArm9Registors->ulR13Fiq  = *((uint32_t *)(pucBuffer + 0x5C));
   ptyArm9Registors->ulR14Fiq  = *((uint32_t *)(pucBuffer + 0x60));
   ptyArm9Registors->ulSPSRFiq = *((uint32_t *)(pucBuffer + 0x64));
   ptyArm9Registors->ulR13Irq  = *((uint32_t *)(pucBuffer + 0x68));
   ptyArm9Registors->ulR14Irq  = *((uint32_t *)(pucBuffer + 0x6C));
   ptyArm9Registors->ulSPSRIrq = *((uint32_t *)(pucBuffer + 0x70));
   ptyArm9Registors->ulR13Svc  = *((uint32_t *)(pucBuffer + 0x74));
   ptyArm9Registors->ulR14Svc  = *((uint32_t *)(pucBuffer + 0x78));
   ptyArm9Registors->ulSPSRSvc = *((uint32_t *)(pucBuffer + 0x7C));
   ptyArm9Registors->ulR13Abt  = *((uint32_t *)(pucBuffer + 0x80));
   ptyArm9Registors->ulR14Abt  = *((uint32_t *)(pucBuffer + 0x84));
   ptyArm9Registors->ulSPSRAbt = *((uint32_t *)(pucBuffer + 0x88));
   ptyArm9Registors->ulR13Und  = *((uint32_t *)(pucBuffer + 0x8C));
   ptyArm9Registors->ulR14Und  = *((uint32_t *)(pucBuffer + 0x90));
   ptyArm9Registors->ulSPSRUnd = *((uint32_t *)(pucBuffer + 0x94));



   printf("R0      = 0x%08x\nR1      = 0x%08x\nR2      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR0,(uint32_t) ptyArm9Registors->ulR1,(uint32_t) ptyArm9Registors->ulR2);
   printf("R3      = 0x%08x\nR4      = 0x%08x\nR5      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR3,(uint32_t) ptyArm9Registors->ulR4,(uint32_t) ptyArm9Registors->ulR5);
   printf("R6      = 0x%08x\nR7      = 0x%08x\nR8      = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR6,(uint32_t) ptyArm9Registors->ulR7,(uint32_t) ptyArm9Registors->ulR8);
   printf("R9      = 0x%08x\nR10     = 0x%08x\nR11     = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR9,(uint32_t) ptyArm9Registors->ulR10,(uint32_t) ptyArm9Registors->ulR11);
   printf("R12     = 0x%08x\nR13Usr  = 0x%08x\nR14Usr  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR12,(uint32_t) ptyArm9Registors->ulR13Usr,(uint32_t) ptyArm9Registors->ulR14Usr);
   printf("PC      = 0x%08x\nCPSR    = 0x%08x\n",(uint32_t) ptyArm9Registors->ulPC,(uint32_t) ptyArm9Registors->ulCPSR);
   printf("R8Fiq   = 0x%08x\nR9Fiq   = 0x%08x\nR10Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR8Fiq,(uint32_t) ptyArm9Registors->ulR9Fiq,(uint32_t) ptyArm9Registors->ulR10Fiq);
   printf("R11Fiq  = 0x%08x\nR12Fiq  = 0x%08x\nR13Fiq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR11Fiq,(uint32_t) ptyArm9Registors->ulR12Fiq,(uint32_t) ptyArm9Registors->ulR13Fiq);
   printf("R14Fiq  = 0x%08x\nSPSRFiq = 0x%08x\nR13Irq  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Fiq,(uint32_t) ptyArm9Registors->ulSPSRFiq,(uint32_t) ptyArm9Registors->ulR13Irq);
   printf("R14Irq  = 0x%08x\nSPSRIrq = 0x%08x\nR13Svc  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Irq,(uint32_t) ptyArm9Registors->ulSPSRIrq,(uint32_t) ptyArm9Registors->ulR13Svc);
   printf("R14Svc  = 0x%08x\nSPSRSvc = 0x%08x\nR13Abt  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Svc,(uint32_t) ptyArm9Registors->ulSPSRSvc,(uint32_t) ptyArm9Registors->ulR13Abt);
   printf("R14Abt  = 0x%08x\nSPSRAbt = 0x%08x\nR13und  = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Abt,(uint32_t) ptyArm9Registors->ulSPSRAbt,(uint32_t) ptyArm9Registors->ulR13Und);
   printf("R14Und  = 0x%08x\nSPSRUnd = 0x%08x\n",(uint32_t) ptyArm9Registors->ulR14Und,(uint32_t) ptyArm9Registors->ulSPSRUnd);
   printf("\n\n\n");



   DL_OPXD_UnlockDevice(tyDevHandle);

#endif //EXECUTE

   tyResult = DL_OPXD_LockDevice(tyDevHandle);
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   //Bring uiProcessor into normal mode
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARM_DEBUG_REQUEST;  
   *((unsigned short *)(pucBuffer + 0x4)) = usCore ;                    
   pucBuffer[0x06] = 0x00;
   pucBuffer[0x07] = 0x0;

   tyResult = SendAndReceiveUSB(ptyUsbDevHandle, tyDevHandle, pucBuffer, (8));
   if ( tyResult != DRVOPXD_ERROR_NO_ERROR)
      return  tyResult;

   DL_OPXD_UnlockDevice(tyDevHandle);

   return(tyResult);
}
/****************************************************************************
     Function: DL_OPXD_Arm_StepProc
     Engineer: Deepa V.A.
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyArmProcType tyArmType - Processor type - 0  ARM7, 1 ARM9
               unsigned char bThumbMode - 1 -> THUMB mode, 0 -> ARM mode
               uint32_t ulR0Value - Register R0 value
       Output: error code DRVOPXD_ERROR_xxx
  Description: Setup the proccessor for execution
Date           Initials    Description
20-Jul-2007      SJ        Initial
****************************************************************************/
TyError DL_OPXD_Arm_StepProc(TyDevHandle tyDevHandle, 
                                uint32_t ulCore, 
                                TyArmProcType tyArmType)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   DLOG("Entering: DL_OPXD_Arm_ExecuteProc()\n");

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;

   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0))   = CMD_CODE_SINGLE_STEP;    // command code
   *((unsigned short *)(pucBuffer + 0x4))  = (unsigned short)ulCore;       // core number
   *((unsigned short *)(pucBuffer + 0x6))  = 0;                            // reserved
   *((unsigned char *)(pucBuffer + 0x8))   = (unsigned char)tyArmType;     // processor type
   *((unsigned char *)(pucBuffer + 0x9))   = 0;                            // reserved
   *((unsigned short *)(pucBuffer + 0xA))  = 0;                            // reserved

   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}
