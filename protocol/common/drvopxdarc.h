/******************************************************************************
       Module: drvopxdarc.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD driver layer for ARC
  Date           Initials    Description
  26-Sep-2007    VH          Initial
******************************************************************************/
#ifndef _DRVOPXDARC_H_
#define _DRVOPXDARC_H_

// main header file for driver layer needs to be included
#include "drvopxd.h"

#define MAX_ARC_BLAST_PAYLOAD                         (64*1024*8)          // maximum number of bits during blast
#define MAX_ARC_WORDS_ACCESS                          16384                // maximum words for one access

// Error codes used in drvlayer specific for ARC
// !!! should start at DRVOXP_ERROR_ARC_OFFSET and finish at (DRVOXP_ERROR_ARC_OFFSET + 0x0FFF) !!!

#define ARC_ACCESS_TYPE_MASK                          0x000F               // masking access type in read/write functions
#define ARC_ACCESS_TYPE_MEMORY                        0x0000               // access to memory
#define ARC_ACCESS_TYPE_COREREG                       0x0001               // access to core register
#define ARC_ACCESS_TYPE_AUXREG                        0x0002               // access to aux register
#define ARC_ACCESS_TYPE_MADI                          0x0003               // access to MADI

#define ARC_ACCESS_CORE_MASK                          0x00F0               // masking core type in read/write functions
#define ARC_ACCESS_CORE_ARCTANGETA4                   0x0000               // core is ARCtanget-A4
#define ARC_ACCESS_CORE_ARCTANGETA5                   0x0010               // core is ARCtanget-A5
#define ARC_ACCESS_CORE_ARC600                        0x0020               // core is ARC600
#define ARC_ACCESS_CORE_ARC700                        0x0030               // core is ARC700
#define ARC_ACCESS_CORE_ARCV2EM                       0x0040               // core is ARCv2EM

#define ARC_ACCESS_INVSKIP_MASK                       0x0100               // masking skipping icache invalidation
#define ARC_ACCESS_INVSKIP_INVALIDATE                 0x0000               // invalidate icache after memory write
#define ARC_ACCESS_INVSKIP_SKIP                       0x0100               // skip icache invalidation after memory write

#define ARC_ACCESS_AUTOINC_MASK                       0x0200               // masking auto-increment address option
#define ARC_ACCESS_AUTOINC_MANUAL                     0x0000               // address auto-increment is not supported by the core (diskware should do it)
#define ARC_ACCESS_AUTOINC_AUTO                       0x0200               // address auto-increment is supported by the core

#define ARC_ACCESS_OPTIMIZE_MASK                      0x0400               // masking JTAG optimized access option
#define ARC_ACCESS_OPTIMIZE_DISABLE                   0x0000               // do not use optimized JTAG access without status checking
#define ARC_ACCESS_OPTIMIZE_ENABLE                    0x0400               // use optimized JTAG access without status checking

#define ARC_ACCESS_ENDIAN_MASK                        0x8000               // masking endianess
#define ARC_ACCESS_ENDIAN_LITTLE                      0x0000               // target is LE
#define ARC_ACCESS_ENDIAN_BIG                         0x8000               // target is BE

// ARC target types (supported by Opella-XD)
typedef enum { ARCT_ARCANGEL, ARCT_NORMAL, ARCT_RSTDETECT, ARCT_CJTAG_TPA_R1, ARCT_JTAG_TPA_R1 } TyARCTargetType;

// ARCangel mode pins structure
typedef struct _TyARCangelMode {
   unsigned char bSS0;                                   // value/status of SS0 pin
   unsigned char bSS1;                                   // value/status of SS1 pin
   unsigned char bCNT;                                   // value/status of CNT pin
   unsigned char bOP;                                    // status of OP pin (only as input value)
} TyARCangelMode;

// ARCangel extended command structure
typedef struct _TyARCangelExtcmd {
   unsigned char ucCommandCode;                          // extended command code
   uint32_t pulParameter[2];                        // parameter (16 bits or 64 bits)
   unsigned short usResponse;                            // response from target
} TyARCangelExtcmd;

// Function prototypes
/****************************************************************************
*** ARC Specific Functions                                                ***
****************************************************************************/
// ARC configuration functions
TyError DL_OPXD_Arc_SelectTarget(TyDevHandle tyDevHandle, TyARCTargetType tyTargetType);
TyError DL_OPXD_Arc_ResetProc(TyDevHandle tyDevHandle, unsigned char bAssertReset, unsigned char bEnableResetSensing);
TyError DL_OPXD_Arc_DebugRequest(TyDevHandle tyDevHandle, unsigned char bDebugRequest, unsigned char *pbDebugAcknowledge);
// ARCangel specific functions
TyError DL_OPXD_Arc_AABlast(TyDevHandle tyDevHandle, uint32_t ulNumberOfBits, unsigned char *pucData);
TyError DL_OPXD_Arc_AAMode(TyDevHandle tyDevHandle, TyARCangelMode *ptyOutputValue, TyARCangelMode *ptyInputValue, unsigned char bEnableBlasting);
TyError DL_OPXD_Arc_AAData(TyDevHandle tyDevHandle, unsigned char bControlEnable, unsigned char ucValue);
TyError DL_OPXD_Arc_AAExtendedCommand(TyDevHandle tyDevHandle, TyARCangelExtcmd *ptyExtCommand);
// ARC read/write functions
TyError DL_OPXD_Arc_WriteBlock(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned short usAccessType, 
                               uint32_t ulAddress, uint32_t ulNumberOfWords, uint32_t *pulData);
TyError DL_OPXD_Arc_ReadBlock(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned short usAccessType, 
                               uint32_t ulAddress, uint32_t ulNumberOfWords, uint32_t *pulData);

#endif // _DRVOPXDARC_H_

