#ifndef __LIBUSB_DYN_H__
#define __LIBUSB_DYN_H__

#include "types.h"

#define MAX_SERIAL_NUMBER_LEN       16                      // maximum serial number string length (including terminating 0)

#define USB_SUCCESS 0
#define USB_ERROR -1

typedef struct libusb_device_handle usb_dev_handle;

int usb_init(void);
void usb_exit(void);

int usb_get_dev_list(uint16_t vid, uint16_t pid, char devicelist[][MAX_SERIAL_NUMBER_LEN]);

usb_dev_handle *usb_open(uint16_t vid, uint16_t pid, const char *slno);
void usb_close(usb_dev_handle *device);

int usb_bulk_write(usb_dev_handle *dev, int ep, char *bytes, int size,
	int timeout);
int usb_bulk_read(usb_dev_handle *dev, int ep, char *bytes, int size,
	int timeout);

void usb_resetep(usb_dev_handle *device, int32_t ep);

#endif /* __LIBUSB_DYN_H__ */
