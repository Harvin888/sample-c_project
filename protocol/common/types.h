/******************************************************************************
       Module: types.h
     Engineer: Jeenus Chalattu Kunnath
  Description: typdef stdint.h
  Date           Initials    Description
  19-Sep-2018    JCK          Initial
******************************************************************************/

#ifdef _MSC_VER

	typedef signed __int8     int8_t;
	typedef signed __int16    int16_t;
	typedef signed __int32    int32_t;
	typedef unsigned __int8   uint8_t;
	typedef unsigned __int16  uint16_t;
	typedef unsigned __int32  uint32_t;
	typedef signed __int64       int64_t;
	typedef unsigned __int64     uint64_t;

#else
	#include <inttypes.h>
#endif