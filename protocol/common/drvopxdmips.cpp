/******************************************************************************
       Module: drvopxdmips.cpp
     Engineer: Vitezslav Hola
  Description: Opella-XD driver layer for MIPS
  Date           Initials    Description
  23-Aug-2007    VH          Initial
  23-Dec-2008    SPC         Returning errors in DL_OPXD_Mips_ExecuteDW2
******************************************************************************/
#include <stdio.h>   
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifndef __LINUX            // Win includes
#ifdef PFW				   // PathFinder headers
#include "/pathfndr/drivers/usb/opellaxd/usb.h"
#include "/pathfndr/global/headers/simple/global/simple.h"
#include "/pathfndr/protocol/common/opella/drvlayer.h"
#endif
#endif

#include "libusb_dyn.h"
#include "drvopxdmips.h"
#include "drvopxdcom.h"
#include "firmware/export/api_cmd.h"
#include "firmware/export/api_def.h"
#include "firmware/export/api_err.h"
#include "../../utility/debug/debug.h"
#include "../mips/trace.h"
// local defines

/****************************************************************************
*** External variables from drvopxd.cpp                                   ***
****************************************************************************/
extern TyDrvDeviceStruct ptyDrvDeviceTable[MAX_DEVICES_SUPPORTED];

/****************************************************************************
*** Local function prototypes                                             ***
****************************************************************************/

/****************************************************************************
*** MIPS Specific Functions                                               ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_Mips_WriteDiskwareStruct
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number
               uint32_t tyStructureType - structure type
               void *pvData - pointer to data
               uint32_t ulSize - number of bytes to send
       Output: error code DRVOPXD_ERROR_xxx
  Description: write diskware configuration structure into Opella-XD
Date           Initials    Description
21-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteDiskwareStruct(TyDevHandle tyDevHandle, 
										 uint32_t ulCore, 
										 uint32_t tyStructureType, 
                                         void *pvData, 
										 uint32_t ulSize)
{ 
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert(pvData != NULL);
   assert(ulSize < BUFFER_PAYLOAD);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // check is size is 0
   if (!ulSize)
      return tyResult;           

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet for DW scan config structure
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_DW_STRUCT;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   switch ((TyDiskwareStructure)tyStructureType)
      {
      case DWS_SCAN:
         *((unsigned short *)(pucBuffer + 0x6)) = 0x0001;
         break;
      case DWS_MIPS_CORE:
         *((unsigned short *)(pucBuffer + 0x6)) = 0x0002;
         break;
      case DWS_MIPS_PIPELINE:
         *((unsigned short *)(pucBuffer + 0x6)) = 0x0003;
         break;
      case DWS_LAST:
      default:
         *((unsigned short *)(pucBuffer + 0x6)) = 0x0000;
         break;
      }
   memcpy((void *)(pucBuffer + 0x8), pvData, ulSize);                            // copy structure into buffer
   iPacketLen = TRANSMIT_LEN(8 + (int32_t)ulSize);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   // synchronization not used, assuming single thread uses this function
   return tyResult;
}


/****************************************************************************
     Function: DL_OPXD_Mips_ExecuteDW2
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to device
               uint32_t ulCore - core number (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t *pulApplication - pointer to DW2 application instructions
               uint32_t *pulDataFromProc - storage for data read from processor by DW2 application
               uint32_t *pulDataToProc - data to be written to processor by DW2 application
               unsigned short usNumberOfInstructions - number of instructions in DW2 application
               unsigned char bBigEndian - 1 for Big Endian, 0 for Little Endian
       Output: error code DRVOPXD_ERROR_xxx
  Description: execute DW2 application on MIPS target
Date           Initials    Description
20-Feb-2007    VH          Initial
23-Dec-2008    SPC         Returning errors more properly.
****************************************************************************/
TyError DL_OPXD_Mips_ExecuteDW2(TyDevHandle tyDevHandle, 
                                uint32_t ulCore, 
                                uint32_t *pulApplication, 
                                uint32_t *pulDataFromProc, 
                                uint32_t *pulDataToProc, 
                                unsigned short usNumberOfInstructions, 
                                unsigned char bBigEndian)
{
    usb_dev_handle *ptyUsbDevHandle;
    unsigned char *pucBuffer;
    TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
    int32_t iPacketLen;

    assert(pulApplication != NULL);
    assert(pulDataFromProc != NULL);
    assert(pulDataToProc != NULL);
    assert(usNumberOfInstructions < MAX_MIPS_DW2_APP_SIZE);
    if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
        return DRVOPXD_ERROR_INVALID_HANDLE;
    if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
        return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

    // synchronization not used, assuming single thread uses this function
    ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
    pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

    // create packet
    *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_EXECUTE_DW2;            // command code
    *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
    *((unsigned short *)(pucBuffer + 0x6)) = usNumberOfInstructions;              // number of instructions
    if (bBigEndian)
        pucBuffer[8] = 0x1;
    else
        pucBuffer[8] = 0x0;
    // copy data for processor
    memcpy((void *)(pucBuffer + 0x0C), (void *)pulDataToProc, MAX_MIPS_DW2_DATA_SIZE);
    // copy DW2 application
    memcpy((void *)(pucBuffer + 0x0C + MAX_MIPS_DW2_DATA_SIZE), (void *)pulApplication, MAX_MIPS_DW2_APP_SIZE);
    iPacketLen = TRANSMIT_LEN(12 + MAX_MIPS_DW2_DATA_SIZE + MAX_MIPS_DW2_APP_SIZE);
    if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0))
        tyResult = DRVOPXD_ERROR_USB_DRIVER;
    else if ((*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
        tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

    memcpy((void *)pulDataFromProc, (void *)(pucBuffer + 0x04), MAX_MIPS_DW2_DATA_SIZE);
    // synchronization not used, assuming single thread uses this function
    return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_ExecuteDW2App
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to device
               uint32_t ulCore - core number (0 to MAX_TAPS_ON_SCANCHAIN-1)
               TyDW2AppParams *ptyDW2App - DW2 application instruction and data
       Output: error code DRVOPXD_ERROR_xxx
  Description: execute DW2 application on MIPS target, optimized implementation
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
TyError DL_OPXD_Mips_ExecuteDW2App(TyDevHandle tyDevHandle, 
								   uint32_t ulCore, 
								   TyDW2AppParams *ptyDW2App)
{

   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   uint32_t uiOffset, uiCurrentSize;
   int32_t iPacketLen;
   // check parameters, using asserts is preferable because of performance in release version
   assert(ptyDW2App != NULL);
   assert(ptyDW2App->pvAppInst != NULL);
   assert((ptyDW2App->uiInstSize + ptyDW2App->puiDataToProcSize[0] + ptyDW2App->puiDataToProcSize[1]) <= BUFFER_PAYLOAD);
   assert((ptyDW2App->puiDataFromProcSize[0] + ptyDW2App->puiDataFromProcSize[1]) <= BUFFER_PAYLOAD);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_MIPS_EXECUTE_DW2_APP;       // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;             // core number
   pucBuffer[0x06] = (ptyDW2App->bBigEndian)? 1 : 0;                             // endianess
   pucBuffer[0x07] = (ptyDW2App->bSwapDataValues) ? 1 : 0;                       // swap data values in DW
   *((uint32_t *)(pucBuffer + 0x08)) = (uint32_t)ptyDW2App->uiInstSize;   // number of bytes in DW2 application
   // fill number of bytes in data to and from DW2 application
   *((uint32_t *)(pucBuffer + 0x0C)) = (uint32_t)(ptyDW2App->puiDataToProcSize[0] + ptyDW2App->puiDataToProcSize[1]);
   *((uint32_t *)(pucBuffer + 0x10)) = (uint32_t)(ptyDW2App->puiDataFromProcSize[0] + ptyDW2App->puiDataFromProcSize[1]);
   uiOffset = 0x14;
   // copy DW2 app instructions (allign size to words)
   memcpy((void *)(pucBuffer + uiOffset), ptyDW2App->pvAppInst, ptyDW2App->uiInstSize);
   uiCurrentSize = ptyDW2App->uiInstSize;
   if (uiCurrentSize % 4)
      uiCurrentSize += (4 - (uiCurrentSize % 4));                                // allign to words
   uiOffset += uiCurrentSize;
   // copy DW2 app data to processor (1st and 2nd part)
   if (ptyDW2App->ppvAppDataToProc[0] != NULL)
      {
      memcpy((void *)(pucBuffer + uiOffset), ptyDW2App->ppvAppDataToProc[0], ptyDW2App->puiDataToProcSize[0]);
      uiOffset += ptyDW2App->puiDataToProcSize[0];
      }
   if (ptyDW2App->ppvAppDataToProc[1] != NULL)
      {
      memcpy((void *)(pucBuffer + uiOffset), ptyDW2App->ppvAppDataToProc[1], ptyDW2App->puiDataToProcSize[1]);
      uiOffset += ptyDW2App->puiDataToProcSize[1];
      }
   iPacketLen = TRANSMIT_LEN((int32_t)uiOffset);
   // send packet to diskware
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if ((usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0) || 
       (*((uint32_t *)(pucBuffer + 0x00)) != ERR_NO_ERROR))
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      // operation was successful, we can obtain data
      uiOffset = 0x04;
      if (ptyDW2App->ppvAppDataFromProc[0] != NULL)
         {
         memcpy((void *)ptyDW2App->ppvAppDataFromProc[0], (void *)(pucBuffer + uiOffset), ptyDW2App->puiDataFromProcSize[0]);
         uiOffset += ptyDW2App->puiDataFromProcSize[0];
         }
      if (ptyDW2App->ppvAppDataFromProc[1] != NULL)
         memcpy((void *)ptyDW2App->ppvAppDataFromProc[1], (void *)(pucBuffer + uiOffset), ptyDW2App->puiDataFromProcSize[1]);
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}
#endif
/****************************************************************************
     Function: DL_OPXD_Mips_ReadWord
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - address to read word
               uint32_t *pulData - pointer to data
               unsigned char bBigEndian - 1 when reading data as BE, 0 otherwise
               unsigned char bDmaAccess - use DMA access when reading word
       Output: error code DRVOPXD_ERROR_xxx
  Description: read word from MIPS memory (DMA access is optional)
Date           Initials    Description
20-Feb-2007    VH          Initial
17-May-2012    SPC         Added Read/Write memory Fast/Safe methods
****************************************************************************/
TyError DL_OPXD_Mips_ReadWord(TyDevHandle tyDevHandle, uint32_t ulCore, 
                              uint32_t ulStartAddress, uint32_t* pulData, 
                              unsigned char bBigEndian, unsigned char bDmaAccess,unsigned char bFastMemoryRW)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   ulStartAddress &= 0xFFFFFFFC;                                                 // ensure address allignment

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   if (bFastMemoryRW)
      {
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_SINGLE_WORD_FAST;    // command code
      }
   else
      {
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_SINGLE_WORD_SAFE;    // command code
      }
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number

   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;

   if (bDmaAccess)
      pucBuffer[7] = 0x1;
   else
      pucBuffer[7] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      *pulData = *((uint32_t *)(pucBuffer + 0x04));       // get word from buffer
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_SyncCache
     Engineer: Suresh P.C
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulAddress - Sync cache address
       Output: error code DRVOPXD_ERROR_xxx
  Description: write word into MIPS memory (DMA access is optional)
Date           Initials    Description
11-Aug-2011    SPC         Initial
****************************************************************************/
TyError DL_OPXD_SyncCache(TyDevHandle tyDevHandle, uint32_t ulCore, 
                               uint32_t ulAddress)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if ( (ulAddress >= 0xA0000000) && (ulAddress < 0xC0000000) )
      {
         return DRVOPXD_ERROR_NO_ERROR;
      }

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function

   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_SYNC_CACHE;      // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number

   pucBuffer[6] = 0x0;

   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = (uint32_t)0;           //Just making last word as 0
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}
/****************************************************************************
     Function: DL_OPXD_Mips_WriteWord
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - address to write word
               uint32_t *pulData - pointer to data
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
               unsigned char bDmaAccess - use DMA access when writing word
       Output: error code DRVOPXD_ERROR_xxx
  Description: write word into MIPS memory (DMA access is optional)
Date           Initials    Description
20-Feb-2007    VH          Initial
17-May-2012    SPC         Added Read/Write memory Fast/Safe methods
****************************************************************************/
TyError DL_OPXD_Mips_WriteWord(TyDevHandle tyDevHandle, uint32_t ulCore, 
                               uint32_t ulStartAddress, uint32_t* pulData, 
                               unsigned char bBigEndian, unsigned char bDmaAccess,unsigned char bFastMemoryRW)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   ulStartAddress &= 0xFFFFFFFC;                                                 // ensure address allignment

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   if (bFastMemoryRW)
      {
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_SINGLE_WORD_FAST;      // command code
      }
   else
      {
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_SINGLE_WORD_SAFE;      // command code
      }

   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number

   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;

   if (bDmaAccess)
      pucBuffer[7] = 0x1;
   else
      pucBuffer[7] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = *pulData;
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_ReadMultipleWordsStd
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to read words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to read
               unsigned char bBigEndian - 1 when reading data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: read multiple words from memory using MIPS core
Date           Initials    Description
20-Feb-2007    VH          Initial
17-May-2012    SPC         Added Read/Write memory Fast/Safe methods
****************************************************************************/
TyError DL_OPXD_Mips_ReadMultipleWords(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                       uint32_t ulStartAddress, uint32_t* pulData, 
                                       uint32_t ulLength, unsigned char bBigEndian,
                                       unsigned char bFastMemoryRW)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert(ulLength <= (BUFFER_PAYLOAD/4));
   assert(pulData != NULL);
   assert((ulStartAddress & 0x00000003) == 0);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // check core number
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   if ( bFastMemoryRW )
      {
      /* Fast methord is not used here because it found not working on all boards. E.g. Malta-4KEc
         download and verify */
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_MULTIPLE_WORDS_SAFE;   // command code
      }
   else
      {
      *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_MULTIPLE_WORDS_SAFE;   // command code
      }
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of words
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if ((tyResult == DRVOPXD_ERROR_NO_ERROR)||(tyResult == DRVOPXD_ERROR_DW2_EXCEPTION))
         {
         memcpy((void *)pulData, (void *)(pucBuffer + 0x04), ulLength * sizeof(uint32_t));
         tyResult = DRVOPXD_ERROR_NO_ERROR;
         }
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_WriteWordPairsSafe
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of word pairs to write
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write multiple word pairs into MIPS memory using safe method
Date           Initials    Description
20-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteWordPairsSafe(TyDevHandle tyDevHandle, uint32_t ulCore,
                                        uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(ulLength <= (BUFFER_PAYLOAD/8));            // check number of word pairs!
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_WORD_PAIR_SAFE;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of words
   memcpy((void *)(pucBuffer + 0x10), (void *)pulData, (uint64_t)ulLength * 8);            // 8 means size of pair of words in bytes
   iPacketLen = TRANSMIT_LEN(16 + ((int32_t)ulLength)*8);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_WriteWordPairsFast
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of word pairs to write
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write multiple word pairs into MIPS memory using fast method
Date           Initials    Description
20-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteWordPairsFast(TyDevHandle tyDevHandle, uint32_t ulCore,
                                        uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(ulLength <= (BUFFER_PAYLOAD/8));            // check number of word pairs!
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_WORD_PAIR_FAST;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of words
   memcpy((void *)(pucBuffer + 0x10), (void *)pulData, (uint64_t)ulLength * 8);            // 8 means size of pair of words in bytes
   iPacketLen = TRANSMIT_LEN(16 + ((int32_t)ulLength)*8);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_ReadWordDma
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - address to read word
               uint32_t *pulData - pointer to data
               unsigned char bBigEndian - 1 when reading data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: read word from memory using DMA core
Date           Initials    Description
15-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_ReadWordDma(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                  uint32_t ulStartAddress, uint32_t *pulData, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert(pulData != NULL);
   assert((ulStartAddress & 0x00000003) == 0);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_SINGLE_WORD_DMA;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   iPacketLen = TRANSMIT_LEN(12);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      *pulData = *((uint32_t *)(pucBuffer + 0x04));       // get word from buffer
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_WriteWordDma
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - address to write word
               uint32_t *pulData - pointer to data
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write word into memory using DMA core
Date           Initials    Description
15-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteWordDma(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                   uint32_t ulStartAddress, uint32_t *pulData, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_SINGLE_WORD_DMA;  // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = *pulData;
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_ReadMultipleWordsDma
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to read words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to read
               unsigned char bBigEndian - 1 when reading data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: read multiple words from memory using DMA core
Date           Initials    Description
15-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_ReadMultipleWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                          uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(pulData != NULL);
   assert(ulLength <= (BUFFER_PAYLOAD/4));
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // ensure address allignment to words
   ulStartAddress &= 0xFFFFFFFC;
   // check core number
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_MULTIPLE_WORD_DMA; // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of words
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy((void *)pulData, (void *)(pucBuffer + 0x04), ulLength * sizeof(uint32_t));
      }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_WriteMultipleWordsDma
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to write
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write multiple words into memory using DMA core
Date           Initials    Description
15-Feb-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteMultipleWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                           uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(ulLength <= (BUFFER_PAYLOAD/4));
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_MULTIPLE_WORD_DMA; // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of words
//   memcpy((void *)(pucBuffer + 0x10), (void *)pulData, ulLength * 4);
//   iPacketLen = TRANSMIT_LEN(16 + ulLength*4);
   iPacketLen = TRANSMIT_LEN(16);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   iPacketLen = (int32_t)ulLength*4;
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)pulData, iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_ReadWordsFastDownload
     Engineer: Suresh P.C
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to read words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to read
               uint32_t ulWriteMemAddress - address of read mem routine
               uint32_t ulLocationOfWriteDataAddress - location of read data address
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write words into memory using EJTAG FAST DATA methord
Date           Initials    Description
26-Jun-2012    SPC          Initial
****************************************************************************/
TyError DL_OPXD_Mips_ReadWordsFastDownload(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                            uint32_t ulStartAddress, uint32_t *pulData, 
                                            uint32_t ulLength, uint32_t ulReadMemRoutineAddress, 
                                            unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_WORDS_FAST_DOWNLOAD;    // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                 // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x08)) = ulStartAddress;                      // address to read
   *((uint32_t *)(pucBuffer + 0x0C)) = ulLength;                            // number of words
   *((uint32_t *)(pucBuffer + 0x10)) = ulReadMemRoutineAddress;             // address of read routine in memory
   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy((void *)pulData, (void *)(pucBuffer + 0x04), ulLength * sizeof(uint32_t));

      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_WriteWordsFastDownload
     Engineer: Suresh P.C
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to write
               uint32_t ulWriteMemAddress - address of write mem routine
               uint32_t ulLocationOfWriteDataAddress - location of write data address
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write words into memory using EJTAG FAST DATA methord
Date           Initials    Description
18-Jun-2012    SPC          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteWordsFastDownload(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                            uint32_t ulStartAddress, uint32_t *pulData, 
                                            uint32_t ulLength, uint32_t ulWriteMemRoutineAddress, 
                                            unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(pulData != NULL);
   if ( tyDevHandle >= MAX_DEVICES_SUPPORTED )
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if ( ulCore >= MAX_TAPS_ON_SCANCHAIN )
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   if ( !ulLength )
      return(tyResult);

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_WORDS_FAST_DOWNLOAD;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                 // core number
   if ( bBigEndian )
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x08)) = ulStartAddress;                      // address to read
   *((uint32_t *)(pucBuffer + 0x0C)) = ulLength;                            // number of words
   *((uint32_t *)(pucBuffer + 0x10)) = ulWriteMemRoutineAddress;            // address of write routine in memory
   iPacketLen = TRANSMIT_LEN(20);

   // send packet 
   if ( usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen )
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   // now send data itself
   iPacketLen = (int32_t)ulLength*4;
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)pulData, iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;

   // wait for response
   else if ( usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0 )
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_WriteWordsReallyFast
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to write
               uint32_t ulWriteMemAddress - address of write mem routine
               uint32_t ulLocationOfWriteDataAddress - location of write data address
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write words into memory using write function in cache routine (fastest way)
Date           Initials    Description
27-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_WriteWordsReallyFast(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, 
                                          uint32_t *pulData, uint32_t ulLength, 
                                          uint32_t ulWriteMemAddress, uint32_t ulLocationOfWriteDataAddress, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(ulLength <= (BUFFER_PAYLOAD/4));
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   if (!ulLength)
      return(tyResult);

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_WORDS_REALLY_FAST;   // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                 // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x08)) = ulStartAddress;                      // address to read
   *((uint32_t *)(pucBuffer + 0x0C)) = ulLength;                            // number of words
   *((uint32_t *)(pucBuffer + 0x10)) = ulWriteMemAddress;                   // address of write routine in memory
   *((uint32_t *)(pucBuffer + 0x14)) = ulLocationOfWriteDataAddress;        // location of write data address
   iPacketLen = TRANSMIT_LEN(24);                                                // send only header, data will be sent later

   // send packet (header)
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // now send data itself
   iPacketLen = (int32_t)ulLength*4;
   if (usb_bulk_write(ptyUsbDevHandle, EPC, (char *)pulData, iPacketLen, DRV_LONG_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   // wait for response
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_ReadWordsReallyFast
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               uint32_t *pulData - pointer to data
               uint32_t ulLength - number of words to write
               uint32_t ulReadMemAddress - address of read mem routine
               uint32_t ulLocationOfReadDataAddress - location of read data address
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: read words from memory using read function in cache routine (fastest way)
Date           Initials    Description
27-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_ReadWordsReallyFast(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                         uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength,
                                         uint32_t ulReadMemAddress, uint32_t ulLocationOfReadDataAddress, unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress & 0x00000003) == 0);
   assert(ulLength <= (BUFFER_PAYLOAD/4));
   assert(pulData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_WORDS_REALLY_FAST;    // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;                 // core number
   if (bBigEndian)
      pucBuffer[6] = 0x1;
   else
      pucBuffer[6] = 0x0;
   *((uint32_t *)(pucBuffer + 0x08)) = ulStartAddress;                      // address to read
   *((uint32_t *)(pucBuffer + 0x0C)) = ulLength;                            // number of words
   *((uint32_t *)(pucBuffer + 0x10)) = ulReadMemAddress;                    // address of read routine in memory
   *((uint32_t *)(pucBuffer + 0x14)) = ulLocationOfReadDataAddress;         // location of read data address
   iPacketLen = TRANSMIT_LEN(24);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy((void *)pulData, (void *)(pucBuffer + 0x04), ulLength * sizeof(uint32_t));

      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}


/****************************************************************************
     Function: DL_OPXD_Mips_ResetProc
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               unsigned char bAssertReset - 1 to assert reset, 0 to deassert
               unsigned char *pbResetAsserted - pointer to store reset status (can be NULL)
       Output: error code DRVOPXD_ERROR_xxx
  Description: control reset of all processors on current scanchain (ulCore is ignored)
Date           Initials    Description
16-Mar-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_ResetProc(TyDevHandle tyDevHandle, 
							   uint32_t ulCore, 
							   unsigned char bAssertReset, 
							   unsigned char *pbResetAsserted)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_RESET_PROC;             // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bAssertReset)
      pucBuffer[0x6] = 0x1;
   else
      pucBuffer[0x6] = 0x0;

   iPacketLen = TRANSMIT_LEN(7);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pbResetAsserted != NULL))
      {
      *pbResetAsserted = pucBuffer[0x04];
      }

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_DebugInterrupt
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               unsigned char bAssertDint - 1 to assert DINT, 0 to deassert
       Output: error code DRVOPXD_ERROR_xxx
  Description: control debug interrupt (DINT) signal for all processors on scanchain
Date           Initials    Description
23-Aug-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_DebugInterrupt(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bAssertDint)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;

   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;

   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_DEBUG_INTERRUPT;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   if (bAssertDint)
      pucBuffer[0x6] = 0x1;
   else
      pucBuffer[0x6] = 0x0;
   iPacketLen = TRANSMIT_LEN(7);

   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));

   DL_OPXD_UnlockDevice(tyDevHandle);
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_WriteWordsDma
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               void *pvData - pointer to data
               uint32_t ulLength - number of words to write
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: write words into memory using DMA core
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#if 0000 // OPTIMIZED_DW
TyError DL_OPXD_Mips_WriteWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, void *pvData, uint32_t ulLength, 
                                   unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   assert((ulStartAddress % 4) == 0);
   assert(ulLength <= (BUFFER_PAYLOAD/4));
   assert(pvData != NULL);
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_WRITE_WORDS_DMA;        // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   pucBuffer[0x06] = (bBigEndian) ? 0x1 : 0x0;                                   // endianess
   pucBuffer[0x07] = 0x0;                                                        // reserved
   *((uint32_t *)(pucBuffer + 0x08)) = ulStartAddress;                      // address to read
   *((uint32_t *)(pucBuffer + 0x0C)) = ulLength;                            // number of words
   memcpy((void *)(pucBuffer + 0x10), pvData, ulLength * 4);
   iPacketLen = TRANSMIT_LEN((int32_t)(16 + ulLength*4));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ptyDrvDeviceTable[tyDevHandle].iDataTimeout) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronisation not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Mips_ReadWordsDma
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core selector (0 to MAX_TAPS_ON_SCANCHAIN-1)
               uint32_t ulStartAddress - start address to write words
               void *pvData - pointer to data
               uint32_t ulLength - number of words to write
               unsigned char bBigEndian - 1 when writing data as BE, 0 otherwise
       Output: error code DRVOPXD_ERROR_xxx
  Description: read words from memory using DMA core
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_ReadWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, void *pvData, uint32_t ulLength, 
                                  unsigned char bBigEndian)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;
   // check parameters
   assert((ulStartAddress % 4) == 0);
   assert(pvData != NULL);
   assert(ulLength <= (BUFFER_PAYLOAD/4));
   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ulCore >= MAX_TAPS_ON_SCANCHAIN)
      return DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_MIPS_READ_WORDS_DMA;         // command code
   *((unsigned short *)(pucBuffer + 0x4)) = (unsigned short)ulCore;              // core number
   pucBuffer[0x06] = (bBigEndian) ? 0x1 : 0x0;                                   // endianess
   pucBuffer[0x07] = 0x0;                                                        // reserved 
   *((uint32_t *)(pucBuffer + 0x8)) = ulStartAddress;                       // address to read
   *((uint32_t *)(pucBuffer + 0xC)) = ulLength;                             // number of words
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, DRV_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, DRV_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      if (tyResult == DRVOPXD_ERROR_NO_ERROR)
         memcpy(pvData, (void *)(pucBuffer + 0x04), ulLength * 4);
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}
#endif
/****************************************************************************
     Function: DL_OPXD_Mips_InitialiseDebugger
     Engineer: Nikolay Chokoev
        Input: TyDevHandle tyDevHandle - handle to open device
               double dFrequency : frequency in MHz
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_InitialiseDebugger (TyDevHandle tyDevHandle,
										 double dFrequency)
{
   TyError ErrRet;

   ErrRet = DL_OPXD_JtagSetFrequency(tyDevHandle, dFrequency, NULL, NULL);
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      {
	  ErrRet = DL_OPXD_JtagSetTMS(tyDevHandle, NULL, NULL, NULL, NULL);   // set default TMS sequence
      }
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      {
	  ErrRet = DL_OPXD_JtagConfigMulticore(tyDevHandle, 0, NULL);         // set no MC support
      }
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      {
	  ErrRet = DL_OPXD_JtagResetTAP(tyDevHandle, 1);                      // reset TAPs using TRST signal
      }
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      {
	  ErrRet = DL_OPXD_JtagResetTAP(tyDevHandle, 0);                      // in addition, ensure TAPs are in initial state
      }

   return ErrRet;
}


/****************************************************************************
     Function: DL_OPXD_Mips_InitialiseDebugger
     Engineer: Nikolay Chokoev
        Input: TyDevHandle tyDevHandle - handle to open device
               double dFrequency : frequency in MHz
       Output: Error code
  Description: 
Date           Initials    Description
10-Jul-2008    NCH          Initial
****************************************************************************/
TyError DL_OPXD_Mips_Initialise_Debugger_Ex(TyDevHandle tyDevHandle,
                                            int32_t bSpecifiedProcessor,
                                            double dJtagFreqInMHz)
{
   TyError ErrRet = DRVOPXD_ERROR_NO_ERROR;

   // if not using diag tools, just call other version
   if(bSpecifiedProcessor)
      return DL_OPXD_Mips_InitialiseDebugger(tyDevHandle, dJtagFreqInMHz);

#ifdef JTAG_LOG_ENABLE
//   DL_InitialiseJTAGLogging();
#endif

   // Opella-XD implementation
   // set JTAG frequency
   ErrRet = DL_OPXD_JtagSetFrequency(tyDevHandle, dJtagFreqInMHz, NULL, NULL);
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      ErrRet = DL_OPXD_JtagSetTMS(tyDevHandle, NULL, NULL, NULL, NULL);   // set default TMS sequence
   if (ErrRet == DRVOPXD_ERROR_NO_ERROR)
      ErrRet = DL_OPXD_JtagConfigMulticore(tyDevHandle, 0, NULL);         // set no MC support
   // do not reset TAPs now
//todo   ErrRet = DL_ConvertOpellaXDError(ErrRet);
   return ErrRet;
  
}



