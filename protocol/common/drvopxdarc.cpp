/******************************************************************************
       Module: drvopxdarc.cpp
     Engineer: Vitezslav Hola
  Description: Opella-XD driver layer for ARC
  Date           Initials    Description
  26-Sep-2007    VH          Initial
******************************************************************************/
#include <stdio.h>   
#include <stdlib.h>
#include <string.h>

#include "libusb_dyn.h"
#include "drvopxdarc.h"
#include "drvopxdcom.h"
#include "firmware/export/api_cmd.h"

/****************************************************************************
*** Local defines                                                         ***
****************************************************************************/
#define ARC_SEND_TIMEOUT                     2000                    // timeout to send command into Opella-XD (2 seconds)
#define ARC_RECEIVE_SHORT_TIMEOUT            2000                    // timeout to receive response from Opella-XD (2 seconds)
#define ARC_RECEIVE_LONG_TIMEOUT             20000                   // timeout to receive response from Opella-XD (20 seconds)
#define ARC_RECEIVE_VERY_LONG_TIMEOUT        25000                   // timeout to receive response from Opella-XD (25 seconds)                                                                     

/****************************************************************************
*** External variables from drvopxd.cpp                                   ***
****************************************************************************/
extern TyDrvDeviceStruct ptyDrvDeviceTable[MAX_DEVICES_SUPPORTED];

/****************************************************************************
*** Local function prototypes                                             ***
****************************************************************************/


/****************************************************************************
*** ARC Specific Functions                                                ***
****************************************************************************/

/****************************************************************************
     Function: DL_OPXD_Arc_SelectTarget
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyARCTargetType tyTargetType - target type ARCT_xxx
       Output: error code DRVOPXD_ERROR_xxx
  Description: select proper ARC target type 
               (ARCANGEL, normal ARC with/without reset, Siano, etc.)
Date           Initials    Description
26-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_SelectTarget(TyDevHandle tyDevHandle, TyARCTargetType tyTargetType)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARC_SET_TARGET_CONFIG;       // command code
   switch (tyTargetType)
      {
      case ARCT_ARCANGEL:  // ARCangel with AD-ARC-D15 adaptor board
         pucBuffer[0x04] = 0x00;
         break;
      case ARCT_RSTDETECT: // ARC target with reset detection (i.e. Siano)
         pucBuffer[0x04] = 0x02;
         break;
      case ARCT_CJTAG_TPA_R1: // ARC target with CJTAG TPA revision 1
         pucBuffer[0x04] = 0x03;
         break;
      case ARCT_JTAG_TPA_R1: // ARC target with JTAG TPA revision 1
         pucBuffer[0x04] = 0x04;
         break;
      case ARCT_NORMAL:    // ARC target with 20-way connector
      default:             
         pucBuffer[0x04] = 0x01;
         break;
      }
   // fill empty reserved values
   pucBuffer[0x05] = 0;    pucBuffer[0x06] = 0;    pucBuffer[0x07] = 0;
   *((uint32_t *)(pucBuffer + 0x08)) = 0;
   iPacketLen = TRANSMIT_LEN(12);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_SHORT_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_ResetProc
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char bAssertReset - 1 to assert reset, 0 to deassert
               unsigned char bEnableResetSensing - 1 to enable target reset sensing, 0 to disable
       Output: error code DRVOPXD_ERROR_xxx
  Description: assert/deassert target reset and controls target sensing
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_ResetProc(TyDevHandle tyDevHandle, unsigned char bAssertReset, unsigned char bEnableResetSensing)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARC_TGT_RESET;               // command code
   pucBuffer[0x04] = (bAssertReset)? 0x1 : 0x0;
   pucBuffer[0x05] = (bEnableResetSensing)? 0x1 : 0x0;
   iPacketLen = TRANSMIT_LEN(6);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_SHORT_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // ignore incomming bytes
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_DebugRequest
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char bDebugRequest - value for debug request signal
               unsigned char *pbDebugAcknowledge - status of debug acknowledge signal (can be NULL)
       Output: error code DRVOPXD_ERROR_xxx
  Description: get status of debug acknowledge and controls debug request
Date           Initials    Description
21-Nov-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_DebugRequest(TyDevHandle tyDevHandle, unsigned char bDebugRequest, unsigned char *pbDebugAcknowledge)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARC_DEBUG_RQ_ACK;                  // command code
   pucBuffer[0x04] = (bDebugRequest)? 0x1 : 0x0;
   iPacketLen = TRANSMIT_LEN(5);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_SHORT_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // process incoming data
   if (pbDebugAcknowledge)
      {
      if ((tyResult == DRVOPXD_ERROR_NO_ERROR) && (pucBuffer[0x4] > 0))
         *pbDebugAcknowledge = 1;
      else
         *pbDebugAcknowledge = 0;
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_AABlast
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulNumberOfBits - number of bits to blast
               unsigned char *pucData - pointer to data to blast
       Output: error code DRVOPXD_ERROR_xxx
  Description: blast specified number of bits into ARCangel FPGA
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_AABlast(TyDevHandle tyDevHandle, uint32_t ulNumberOfBits, unsigned char *pucData)
{
   uint32_t ulBytesInPayload;
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if ((pucData == NULL) || (ulNumberOfBits > MAX_ARC_BLAST_PAYLOAD))
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARC_AA_BLAST;                   // command code
   *((uint32_t *)(pucBuffer + 0x4)) = ulNumberOfBits;                          // number of bits to blast
   *((uint32_t *)(pucBuffer + 0x8)) = 0;                                       // reserved
   ulBytesInPayload = ulNumberOfBits / 8;
   if (ulNumberOfBits % 8)
      ulBytesInPayload++;
   memcpy((void *)(pucBuffer + 0x0C), (void *)pucData, ulBytesInPayload);           // copy all bytes
   iPacketLen = TRANSMIT_LEN(12 + (int32_t)ulBytesInPayload);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_AAMode
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyARCangelMode *ptyOutputValue - values for AA mode pins (if NULL, output is not updated)
               TyARCangelMode *ptyInputValue - status of AA mode pins (if NULL, ignored)
               unsigned char bEnableBlasting - enable/disable blasting pins (D0 and D1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: set/check ARCangel mode pins (SS0, SS1, CNT, OP, etc.)
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_AAMode(TyDevHandle tyDevHandle, TyARCangelMode *ptyOutputValue, TyARCangelMode *ptyInputValue, unsigned char bEnableBlasting)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARC_AA_MODE;                   // command code
   if (ptyOutputValue != NULL)
      {  // update output
      pucBuffer[0x04] = 0x01;                                                       // update output pins (SS0, SS1 and CNT)
      pucBuffer[0x05] = (ptyOutputValue->bSS0) ? 0x1 : 0x0;
      pucBuffer[0x06] = (ptyOutputValue->bSS1) ? 0x1 : 0x0;
      pucBuffer[0x07] = (ptyOutputValue->bCNT) ? 0x1 : 0x0;
      }
   else
      {  // do not update outputs, just set 0's
      pucBuffer[0x04] = 0x00;                                                       // do not update output pins
      pucBuffer[0x05] = 0x00;       pucBuffer[0x06] = 0x00;       pucBuffer[0x07] = 0x00;
      }
   pucBuffer[0x08] = (bEnableBlasting) ? 0x1 : 0x0;
   pucBuffer[0x09] = 0x0;                                                           // no manual control over data signals
   pucBuffer[0x0A] = 0x0;
   iPacketLen = TRANSMIT_LEN(11);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_SHORT_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // process response
   if ((tyResult == DRVOPXD_ERROR_NO_ERROR) &&
       (ptyInputValue != NULL))
      {  // check status of mode pins
      ptyInputValue->bOP  = (pucBuffer[0x04]) ? 0x1 : 0x0;
      ptyInputValue->bSS0 = (pucBuffer[0x05]) ? 0x1 : 0x0;
      ptyInputValue->bSS1 = (pucBuffer[0x06]) ? 0x1 : 0x0;
      ptyInputValue->bCNT = (pucBuffer[0x07]) ? 0x1 : 0x0;
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_AAData
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               unsigned char bControlEnable - enable/disable control over D0 and D1 signals
               unsigned char ucValue - value for data signals (D0 is bit 0, D1 is bit 1)
       Output: error code DRVOPXD_ERROR_xxx
  Description: control over ARCangel data signals D0 and D1 (used for diagnostics)
Date           Initials    Description
23-Nov-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_AAData(TyDevHandle tyDevHandle, unsigned char bControlEnable, unsigned char ucValue)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARC_AA_MODE; // command code
   pucBuffer[0x04] = 0x00;                                        // do not update output pins
   pucBuffer[0x05] = 0x00;       pucBuffer[0x06] = 0x00;       pucBuffer[0x07] = 0x00;
   pucBuffer[0x08] = 0x0;                                         // no blasting
   pucBuffer[0x09] = (bControlEnable) ? 0x1 : 0x0;                // manual control over data pins
   pucBuffer[0x0A] = ucValue;
   iPacketLen = TRANSMIT_LEN(11);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_SHORT_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_AAExtendedCommand
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               TyARCangelExtcmd *ptyExtCommand - extended command structure
       Output: error code DRVOPXD_ERROR_xxx
  Description: send extended command to ARCangel4 target and get response
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_AAExtendedCommand(TyDevHandle tyDevHandle, TyARCangelExtcmd *ptyExtCommand)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if (ptyExtCommand == NULL)
      return(tyResult);             // just return, no command to send
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x00)) = CMD_CODE_ARC_AA_EXTCMD;                 // command code
   pucBuffer[0x04] = ptyExtCommand->ucCommandCode;                                  // AA4 ext command code
   pucBuffer[0x05] = 0x0;     pucBuffer[0x06] = 0x0;     pucBuffer[0x07] = 0x0;     // reserved
   *((uint32_t *)(pucBuffer + 0x08)) = ptyExtCommand->pulParameter[0];         // AA4 ext command parameters
   *((uint32_t *)(pucBuffer + 0x0C)) = ptyExtCommand->pulParameter[1];         // AA4 ext command parameters
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_SHORT_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   // process response
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      ptyExtCommand->usResponse = *((unsigned short *)(pucBuffer + 0x04));
   else
      ptyExtCommand->usResponse = 0x0;
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_WriteBlock
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 if MC not used)
               unsigned short usAccessType - type of access (ARC_ACCESS_xxx)
               uint32_t ulAddress - starting memory address/register index
               uint32_t ulNumberOfWords - number of words (max 16384)
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: write specified number of words into ARC (memory/register)
Date           Initials    Description
27-Sep-2007    VH          Initial
****************************************************************************/
TyError DL_OPXD_Arc_WriteBlock(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned short usAccessType, 
                               uint32_t ulAddress, uint32_t ulNumberOfWords, uint32_t *pulData)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if ((pulData == NULL) || (ulNumberOfWords > MAX_ARC_WORDS_ACCESS))
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARC_WRITE_NORMAL;            // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;             // core number
   *((unsigned short *)(pucBuffer + 0x06)) = usAccessType;                       // access type parameters
   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                            // start address/offset
   *((uint32_t *)(pucBuffer + 0xC)) = ulNumberOfWords;                      // number of words to write
   memcpy((void *)(pucBuffer + 0x10), (void *)pulData, ulNumberOfWords * sizeof(uint32_t));         // copy all words
   iPacketLen = TRANSMIT_LEN(16 + ((int32_t)ulNumberOfWords*4));
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_VERY_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
   
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
   {
      uint32_t ulWordsWritten = *((uint32_t *)(pucBuffer + 0x04));

      if (ulWordsWritten != ulNumberOfWords)
         tyResult = DRVOPXD_ERROR_TARGET_WRITE_MEMORY;                           // error when writing to target memory
   }

   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}

/****************************************************************************
     Function: DL_OPXD_Arc_ReadBlock
     Engineer: Vitezslav Hola
        Input: TyDevHandle tyDevHandle - handle to open device
               uint32_t ulCore - core number (0 if MC not used)
               unsigned short usAccessType - type of access (ARC_ACCESS_xxx)
               uint32_t ulAddress - starting memory address/register index
               uint32_t ulNumberOfWords - number of words (max 16384)
               uint32_t *pulData - pointer to data
       Output: error code DRVOPXD_ERROR_xxx
  Description: read specified number of words from ARC (memory/register)
Date           Initials    Description
27-Sep-2007    VH          Initial 
09-Jan-2013    SPT         Fixed Memory read error during debug power down
****************************************************************************/
TyError DL_OPXD_Arc_ReadBlock(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned short usAccessType, 
                               uint32_t ulAddress, uint32_t ulNumberOfWords, uint32_t *pulData)
{
   usb_dev_handle *ptyUsbDevHandle;
   unsigned char *pucBuffer;
   TyError tyResult = DRVOPXD_ERROR_NO_ERROR;
   int32_t iPacketLen;

   if (tyDevHandle >= MAX_DEVICES_SUPPORTED)
      return DRVOPXD_ERROR_INVALID_HANDLE;
   if ((pulData == NULL) || (ulNumberOfWords > MAX_ARC_WORDS_ACCESS))
      return DRVOPXD_ERROR_INVALID_DATA_FORMAT;
   // synchronization not used, assuming single thread uses this function
   ptyUsbDevHandle = ptyDrvDeviceTable[tyDevHandle].ptyUsbDevHandle;
   pucBuffer = (unsigned char *)ptyDrvDeviceTable[tyDevHandle].pulTransferBuffer;
   // create packet
   *((uint32_t *)(pucBuffer + 0x0)) = CMD_CODE_ARC_READ_NORMAL;             // command code
   *((unsigned short *)(pucBuffer + 0x04)) = (unsigned short)ulCore;             // core number
   *((unsigned short *)(pucBuffer + 0x06)) = usAccessType;                       // access type parameters
   *((uint32_t *)(pucBuffer + 0x8)) = ulAddress;                            // start address/offset
   *((uint32_t *)(pucBuffer + 0xC)) = ulNumberOfWords;                      // number of words to write
   iPacketLen = TRANSMIT_LEN(16);
   // send packet
   if (usb_bulk_write(ptyUsbDevHandle, EPA, (char *)pucBuffer, iPacketLen, ARC_SEND_TIMEOUT) != iPacketLen)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else if (usb_bulk_read(ptyUsbDevHandle, EPB, (char *)pucBuffer, BIG_BUFFER_LEN, ARC_RECEIVE_VERY_LONG_TIMEOUT) <= 0)
      tyResult = DRVOPXD_ERROR_USB_DRIVER;
   else
      {
      tyResult = DL_OPXD_ConvertDwErrToDrvlayerErr(*((uint32_t *)(pucBuffer + 0x00)));
      }
   // some registers can be read during powerdown state
   if (tyResult == DRVOPXD_ERROR_NO_ERROR)
      {
      uint32_t ulWordsRead = *((uint32_t *)(pucBuffer + 0x04));
      if (ulWordsRead != ulNumberOfWords)
         tyResult = DRVOPXD_ERROR_TARGET_READ_MEMORY;                            // error when writing to target memory
      else
         {
         memcpy((void *)pulData, (void *)(pucBuffer + 0x08), ulNumberOfWords * sizeof(uint32_t)); // copy all words
         tyResult = DRVOPXD_ERROR_NO_ERROR;
         }
      }
   // synchronization not used, assuming single thread uses this function
   return(tyResult);
}


/****************************************************************************
*** Local functions                                                       ***
****************************************************************************/


