/******************************************************************************
       Module: osdbg.cpp
     Engineer: Suresh P. C.
  Description: Implementation file for OS Debug support.

  Date           Initials    Description
  15-Jul-2009    SPC         Initial
******************************************************************************/

#include "osdbg.h"
#include "../mips/MmuLinux26.h"

//We have to create OS Layer function mapping's
//This is the OS Layer functions for Linux 2.6
TyOsLyrFuns Linux26MMUfns = {
   InitLinux26MMU,   //Init function
   IsLinux26MMUMappingNeeded, //Function to check mapping is needed or not
   SetLinux26MMUMapping, //Set MMU mapping if needed
   UnSetLinux26MMUMapping, //Unset the MMU mapping
   InitLinux26AppMMU
};

extern TyMLHandle *ptyMLHandle;
static int32_t bOsDbgEnabled = 0;
static TyOsLyrFuns CurrentOS;

/****************************************************************************
     Function: AshInitOSMMU
     Engineer: Suresh P.C
        Input: TyOS os - OS selection
               uint32_t *ulDataToMMUHandler - Data to MMU Handler
       Output: 0 :  Success ; 1 : otherwise
  Description: Initializes the OS MMU handler.
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
extern "C" int32_t AshInitOSMMU(TyOS os, uint32_t *ulDataToMMUHandler, double dOSVer)
{
   //Call MMU init function in specific layer.
   switch ( os )
      {
      case Linux26:
         CurrentOS = Linux26MMUfns;
         break;
      default:
         //printf("Currently do not support \n");
         return 1;
      }
   if ( 0 != CurrentOS.IntOSfn(ptyMLHandle,*ulDataToMMUHandler, dOSVer) )
      return 1;

   bOsDbgEnabled = 1;
   return 0;
}

/****************************************************************************
     Function: InitOSAppMMU
     Engineer: Suresh P.C
        Input: uint32_t ulAppCon - Application context
               uint32_t ulPgDirAddrs - Address to App PGD
       Output: 0 : Success ; Error code: otherwise
  Description: Initializes the current OS Application MMU mapping
Date           Initials    Description
20-Oct-2009    SPC         Initial
****************************************************************************/
extern "C" int32_t AshInitOSAppMMU(uint32_t ulAppCon, uint32_t ulPgDirAddrs)
{
   if ( bOsDbgEnabled )
      return CurrentOS.SetAppMMU(ulAppCon, ulPgDirAddrs);
   return 1;
}

/****************************************************************************
     Function: IsOSDbgEnabled
     Engineer: Suresh P.C
        Input: none
       Output: 1 :  OS Debug enabled ; 1 : OS Debug not enabled
  Description: Returns the OS debug is enabled or not
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
int32_t IsOSDbgEnabled()
{
   return bOsDbgEnabled;
}

/****************************************************************************
     Function: IsOSMMUMappingNeeded
     Engineer: Suresh P.C
        Input: uint32_t ulAddrs - Memory address that requires OS translation
       Output: Return code from OS awareness layer
  Description: Checks if MMU translation needed or not
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
int32_t IsOSMMUMappingNeeded(uint32_t ulAddrs)
{
   int32_t nRet = 0;
   if ( IsOSDbgEnabled() )
      {
      //Check the address is need a translation from each OS layer
      nRet = CurrentOS.CheckMapping(ulAddrs);
      }
   return nRet;
}

/****************************************************************************
     Function: SetOSMMUMapping
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyDevHandle - handle to connection
               uint32_t ulAddrs - address that requires OS translation
       Output: Return code from OS awareness layer
  Description: Sets the OS MMU mapping by calling the OS layer.
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
int32_t SetOSMMUMapping(TyMLHandle *ptyDevHandle, uint32_t ulAddrs, int32_t iWriteAccess)
{
   //Set MMU Mapping from each layer.
   return CurrentOS.SetMapping(ptyDevHandle, ulAddrs, iWriteAccess);
}

/****************************************************************************
     Function: UnsetOSMMUMapping
     Engineer: Suresh P.C
        Input: TyMLHandle *ptyDevHandle - handle to connection
       Output: none
  Description: Unsets the OS MMU mapping by calling the OS layer.
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
void UnsetOSMMUMapping(TyMLHandle *ptyDevHandle)
{
   if ( bOsDbgEnabled )
      CurrentOS.UnsetMapping(ptyDevHandle);
}

/****************************************************************************
     Function: AshDisableOSDebugging
     Engineer: Suresh P.C
        Input: none
       Output: none
  Description: Disables the OS awareness debugging.
Date           Initials    Description
17-Jul-2009    SPC         Initial
****************************************************************************/
void AshDisableOSDebugging()
{
   bOsDbgEnabled = 0;
}


