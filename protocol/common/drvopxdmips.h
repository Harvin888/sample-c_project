/******************************************************************************
       Module: drvopxdmips.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD driver layer for MIPS
  Date           Initials    Description
  23-Aug-2007    VH          Initial
******************************************************************************/
#ifndef _DRVOPXDMIPS_H_
#define _DRVOPXDMIPS_H_

// main header file for driver layer needs to be included
#include "drvopxd.h"

// Error codes used in drvlayer specific for MIPS
// !!! should start at DRVOXP_ERROR_MIPS_OFFSET and finish at (DRVOXP_ERROR_MIPS_OFFSET + 0x0FFF) !!!

// structure types
// DW2 application parameter structure
// this structure is used for defining DW2 instructions and data to/from processor with minimum overhead
// data to/from processor can be assembled from two separate buffers allowing optimum memory accesses (no temporaty buffers needed)
typedef struct _TyDW2AppParams {
   void *pvAppInst;                                               // pointer to instructions
   uint32_t uiInstSize;                                       // number of bytes for instructions
   void *ppvAppDataToProc[2];                                     // 2 pointers for data to processor
   uint32_t puiDataToProcSize[2];                             // size of data to processor
   void *ppvAppDataFromProc[2];                                   // 2 pointers for data from processor
   uint32_t puiDataFromProcSize[2];                           // size of data from processor
   unsigned char bBigEndian;                                      // is target BE or LE (to handle EJTAG data register)
   unsigned char bSwapDataValues;                                 // swap bytes in words/halfwords in data to/from processor
} TyDW2AppParams;

// Function prototypes
/****************************************************************************
*** MIPS Specific Functions                                               ***
****************************************************************************/
// MIPS diskware basic functions
TyError DL_OPXD_Mips_WriteDiskwareStruct(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t tyStructureType, 
                                         void *pvData, uint32_t ulSize);
TyError DL_OPXD_Mips_ExecuteDW2(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t *pulApplication, uint32_t *pulDataFromProc, 
                                uint32_t *pulDataToProc, unsigned short usNumberOfInstructions, unsigned char bBigEndian);

// MIPS basic memory functions
TyError DL_OPXD_Mips_ReadWord(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, 
                              unsigned char bBigEndian, unsigned char bDmaAccess,unsigned char bFastMemoryRW);
TyError DL_OPXD_SyncCache(TyDevHandle tyDevHandle, uint32_t ulCore, 
                               uint32_t ulAddress);
TyError DL_OPXD_Mips_WriteWord(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, 
                               unsigned char bBigEndian, unsigned char bDmaAccess,unsigned char bFastMemoryRW);
TyError DL_OPXD_Mips_ReadMultipleWords(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t* pulData, 
                                          uint32_t ulLength, unsigned char bBigEndian,unsigned char bFastMemoryRW);
TyError DL_OPXD_Mips_WriteWordPairsSafe(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, 
                                        uint32_t ulLength, unsigned char bBigEndian);
TyError DL_OPXD_Mips_WriteWordPairsFast(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, 
                                        uint32_t ulLength, unsigned char bBigEndian);

TyError DL_OPXD_Mips_WriteWordsFastDownload(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                            uint32_t ulStartAddress, uint32_t *pulData, 
                                            uint32_t ulLength, uint32_t ulWriteMemRoutineAddress, 
                                            unsigned char bBigEndian);
TyError DL_OPXD_Mips_ReadWordsFastDownload(TyDevHandle tyDevHandle, uint32_t ulCore, 
                                            uint32_t ulStartAddress, uint32_t *pulData, 
                                            uint32_t ulLength, uint32_t ulReadMemRoutineAddress, 
                                            unsigned char bBigEndian);
// MIPS DMA core functions
TyError DL_OPXD_Mips_ReadWordDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, unsigned char bBigEndian);
TyError DL_OPXD_Mips_WriteWordDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, unsigned char bBigEndian);
TyError DL_OPXD_Mips_ReadMultipleWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, 
                                          unsigned char bBigEndian);
TyError DL_OPXD_Mips_WriteMultipleWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, 
                                           unsigned char bBigEndian);
TyError DL_OPXD_Mips_ResetProc(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bAssertReset, unsigned char *pbResetAsserted);
TyError DL_OPXD_Mips_DebugInterrupt(TyDevHandle tyDevHandle, uint32_t ulCore, unsigned char bAssertDint);

// MIPS fast memory functions (using cache routine)
TyError DL_OPXD_Mips_WriteWordsReallyFast(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, 
                                          uint32_t ulWriteMemAddress, uint32_t ulLocationOfWriteDataAddress, unsigned char bBigEndian);
TyError DL_OPXD_Mips_ReadWordsReallyFast(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, uint32_t *pulData, uint32_t ulLength, 
                                         uint32_t ulReadMemAddress, uint32_t ulLocationOfReadDataAddress, unsigned char bBigEndian);

// new functions
TyError DL_OPXD_Mips_ExecuteDW2App(TyDevHandle tyDevHandle, uint32_t ulCore, TyDW2AppParams *ptyDW2App);
TyError DL_OPXD_Mips_WriteWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, void *pvData, uint32_t ulLength, 
                                   unsigned char bBigEndian);
TyError DL_OPXD_Mips_ReadWordsDma(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulStartAddress, void *pvData, uint32_t ulLength, 
                                  unsigned char bBigEndian);
TyError DL_OPXD_Mips_InitialiseDebugger (TyDevHandle tyDevHandle,
										 double dFrequency);
TyError DL_OPXD_Mips_Initialise_Debugger_Ex(TyDevHandle tyDevHandle,
                                            int32_t bSpecifiedProcessor,
                                            double dJtagFreqInMHz);

#endif // _DRVOPXDMIPS_H_

