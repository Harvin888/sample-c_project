/******************************************************************************
       Module: drvopxd.h
     Engineer: Vitezslav Hola
  Description: Header for driver layer Opella-XD interface
  Date           Initials    Description
  27-Sep-2006    VH          Initial
  20-Mar-2007    VH          Added support for PathFinder
  23-Dec-2008    SPC         Added one more error return case for MIPS
  23-Apr-2010    JCK         Added more error returns for ARM
  09-Mar-2012    SPT         added cJTAG functions
******************************************************************************/
#ifndef _DRVOPXD_H_
#define _DRVOPXD_H_

#include "types.h"
#include "../../firmware/export/api_def.h"

// Device defines
#define MAX_DEVICES_SUPPORTED       16                      // maximum devices connected to single PC

#define MAX_SERIAL_NUMBER_LEN       16                      // maximum serial number string length (including terminating 0)
#define MAX_STRING_LENGTH_PROC_NAME 260

// USB driver defines
#define ASHLING_USB_VID             0x0B6B                  // Ashling Microsystems Ltd USB VID
#define OPELLAXD_USB_PID            0x0010                  // Opella-XD USB PID

#define DRV_TIMEOUT                 5000                    // default timeout 5s
#define DRV_LONG_TIMEOUT            (4*60*1000)             // long timeout (4 min) for functions at very low frequency
#define MAX_ADAPTIVE_CLOCK_TIMEOUT  20000                   // maximum adaptive clock timeout in ms

// Error codes used in drvlayer (TyError type)
#define DRVOPXD_ERROR_NO_ERROR                        0x0000
#define DRVOPXD_ERROR_USB_DRIVER                      0x0001
#define DRVOPXD_ERROR_NO_USB_DEVICE_CONNECTED         0x0002
#define DRVOPXD_ERROR_INVALID_SERIAL_NUMBER           0x0003
#define DRVOPXD_ERROR_DEVICE_ALREADY_IN_USE           0x0004
#define DRVOPXD_ERROR_TOO_MANY_DEVICES_OPENED         0x0005
#define DRVOPXD_ERROR_INVALID_HANDLE                  0x0006
#define DRVOPXD_ERROR_CANNOT_CONFIGURE_DEVICE         0x0007
#define DRVOPXD_ERROR_VTPA_VOLTAGE                    0x0008
#define DRVOPXD_ERROR_INVALID_FILENAME                0x0009
#define DRVOPXD_ERROR_MEMORY_ALLOC                    0x000A         // problems with allocation of memory
#define DRVOPXD_ERROR_INVALID_FIRMWARE                0x000B
#define DRVOPXD_ERROR_INVALID_DISKWARE_FILENAME       0x000C
#define DRVOPXD_ERROR_INVALID_FPGAWARE_FILENAME       0x000D
#define DRVOPXD_ERROR_DEVICE_I2C_ERROR                0x000E
#define DRVOPXD_ERROR_TPA_PROGRAM_ERROR               0x000F
#define DRVOPXD_ERROR_INVALID_TPA                     0x0010
#define DRVOPXD_ERROR_INVALID_FREQUENCY               0x0011         // invalid frequency requested
#define DRVOPXD_ERROR_INVALID_DATA_FORMAT             0x0012         // invalid data format
#define DRVOPXD_ERROR_DEVICE_POST_FAILED              0x0013         // POST failed in device
#define DRVOPXD_ERROR_FAILED_TO_WRITE_REGISTRY_KEY    0x0014
#define DRVOPXD_ERROR_INVALID_PACKET_SIZE             0x0015
#define DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT          0x0020         // adaptive clock timeout
#define DRVOPXD_ERROR_NOT_A_VALID_CORE                0x0021         // not a valid core number
#define DRVOPXD_ERROR_DATA_MISSMATCH                  0x0022         // write and read compare data missmatch
#define DRVOPXD_ERROR_TARGET_BROCKEN                  0x0023
#define DRVOPXD_ERROR_CONFIGURATION                   0x0024
#define DRVOPXD_ERROR_TIMEOUT                         0x0025         // halt timeout error
#define DRVOPXD_ERROR_UNKNOWN_CMD                     0x0026		 // command which is not implemented in firmware/diskware

// target memory function errors
#define DRVOPXD_ERROR_TARGET_READ_MEMORY              0x0400
#define DRVOPXD_ERROR_TARGET_WRITE_MEMORY             0x0401
// target runtime errors
#define DRVOPXD_ERROR_EXECUTE_DW2                     0x0480
#define DRVOPXD_ERROR_TGT_RESET_OCCURRED              0x0481
#define DRVOPXD_ERROR_DW2_EXCEPTION                   0x0482
#define DRVOPXD_ERROR_DW_RUNNING                      0x0483

#define DRVOPXD_ERROR_JTAG_LENGTH_ERROR               0x1000         // incorrect length of scan chain
#define DRVOPXD_ERROR_JTAG_INVALID_CORE_CONFIG        0x1001         // invalid configuration of (multi)core

// diagnostic error codes (from 0x2000 to 0x2FFF)
#define DRVOPXD_ERROR_TEST_FPGA_ID                    0x2001         // cannot read FPGA ID
#define DRVOPXD_ERROR_TEST_FPGA_XSYSCLK               0x2002         // problem with XSYSCLK
#define DRVOPXD_ERROR_TEST_FPGA_XBS(x)                (0x2003 + (x)) // problem with XBS(x) (x from 0 to 1)
#define DRVOPXD_ERROR_TEST_FPGA_INIT                  0x2005         // problem with FPGA_INIT
#define DRVOPXD_ERROR_TEST_FPGA_IRQ                   0x2006         // problem with FPGA_IRQ
#define DRVOPXD_ERROR_TEST_FPGA_ADDRESS(x)            (0x2010 + (x)) // problem with FPGA address line x (x from 0 to 19)
#define DRVOPXD_ERROR_TEST_FPGA_DATA(x)               (0x2030 + (x)) // problem with FPGA data line x (x from 0 to 15)
#define DRVOPXD_ERROR_TEST_FPGA_DMADREQ               0x2040         // problem with FPGA DMA DREQ signal
#define DRVOPXD_ERROR_TEST_FPGA_DMADREQCLR            0x2041         // problem with FPGA DMA DREQCLR signal
#define DRVOPXD_ERROR_TEST_FPGA_DMATCOUT              0x2042         // problem with FPGA DMA TCOUT signal
#define DRVOPXD_ERROR_TEST_CPU                        0x2080         // failed CPU test
#define DRVOPXD_ERROR_TEST_EXTRAM_DATA(x)             (0x2100 + (x)) // problem with external RAM data line x (x from 0 to 15)
#define DRVOPXD_ERROR_TEST_EXTRAM_ADDRESS(x)          (0x2110 + (x)) // problem with external RAM address line x (x from 0 to 21)
#define DRVOPXD_ERROR_TEST_EXTRAM_RANDOM              0x2130         // problem with external RAM data line x (x from 0 to 15)

#define DRVOPXD_ERROR_TEST_TPAINT_DIO_P(x)            (0x2200 + (x)) // problem with TPA interface DIO_P pin (x from 0 to 9)
#define DRVOPXD_ERROR_TEST_TPAINT_DIO_N(x)            (0x220A + (x)) // problem with TPA interface DIO_N pin (x from 0 to 9)
#define DRVOPXD_ERROR_TEST_TPAINT_FSIO(x)             (0x2214 + (x)) // problem with TPA interface FSIO pin (x from 0 to 9)
#define DRVOPXD_ERROR_TEST_TPAINT_CSIO(x)             (0x221E + (x)) // problem with TPA interface CSIO pin (x from 0 to 1)
#define DRVOPXD_ERROR_TEST_TPAINT_LOOP                0x2220         // problem with TPA interface loop pin
#define DRVOPXD_ERROR_TEST_TPAINT_ABSENT              0x2221         // problem with TPA interface absent pin
#define DRVOPXD_ERROR_TEST_TPAINT_VTPA                0x2222         // problem with TPA interface Vtpa pin
#define DRVOPXD_ERROR_TEST_TPAINT_VTPA_2              0x2223         // problem with TPA interface Vtpa2 pin
#define DRVOPXD_ERROR_TEST_TPAINT_VTPA_3V3            0x2224         // problem with TPA interface Vtpa33 pin
#define DRVOPXD_ERROR_TEST_TPAINT_VTPA_2V5            0x2225         // problem with TPA interface Vtpa25 pin
#define DRVOPXD_ERROR_TEST_TPAINT_LED                 0x2226         // problem with TPA interface LED pin
#define DRVOPXD_ERROR_TEST_TPAINT_VTREF               0x2227         // problem with TPA interface Vtref pin

#define DRVOPXD_ERROR_TEST_PLL_REFERENCE              0x2300         // invalid frequency reference on Opella-XD board
#define DRVOPXD_ERROR_TEST_PLL_JTAGCLK1               0x2301         // problem with JTAGCLK1 signal
#define DRVOPXD_ERROR_TEST_PLL_JTAGCLK2               0x2302         // problem with JTAGCLK2 signal

#define DRVOPXD_ERROR_TEST_UNDEFINED                  0x2FFF         // undefine test error

// target specific error code offsets
#define DRVOPXD_ERROR_MIPS_OFFSET                     0x4000         // MIPS specific error codes
#define DRVOPXD_ERROR_MIPS_INVALID_DW2                (DRVOPXD_ERROR_MIPS_OFFSET + 1)

#define DRVOPXD_ERROR_ARM_OFFSET                      0x5000         // ARM specific error codes
#define DRVOPXD_ERROR_ARM_NO_DEBUG_ENTRY_AP           0x5001
#define DRVOPXD_ERROR_ARM_NOT_VALID_ROM_ADDR          0x5002
#define DRVOPXD_ERROR_ARM_DATA_ABORT                  0x5003

#define DRVOPXD_ERROR_ARC_OFFSET                      0x6000         // ARC specific error codes
#define DRVOPXD_ERROR_ARC_TARGET_RESET_OCCURED        (DRVOPXD_ERROR_ARC_OFFSET + 1)
#define DRVOPXD_ERROR_ARC_DEBUG_ACCESS                (DRVOPXD_ERROR_ARC_OFFSET + 2)
#define DRVOPXD_ERROR_ARC_DEBUG_PDOWN                 (DRVOPXD_ERROR_ARC_OFFSET + 3)
#define DRVOPXD_ERROR_ARC_DEBUG_POFF                  (DRVOPXD_ERROR_ARC_OFFSET + 4)

#define DRVOPXD_ERROR_RISCV_OFFSET                                      0x7000         // RISCV specific error codes
#define DRVOPXD_ERROR_RISCV_TARGET_RESET_OCCURED                        (DRVOPXD_ERROR_RISCV_OFFSET + 0x01)
#define DRVOPXD_ERROR_RISCV_DEBUG_ACCESS                                (DRVOPXD_ERROR_RISCV_OFFSET + 0x02)
#define DRVOPXD_ERROR_RISCV_DEBUG_PDOWN                                 (DRVOPXD_ERROR_RISCV_OFFSET + 0x03)
#define DRVOPXD_ERROR_RISCV_DEBUG_POFF                                  (DRVOPXD_ERROR_RISCV_OFFSET + 0x04)
#define DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUSY                     (DRVOPXD_ERROR_RISCV_OFFSET + 0x05)   //abstarct reg cmd error busy
#define DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED            (DRVOPXD_ERROR_RISCV_OFFSET + 0x06)   //abstarct reg cmd error not supported
#define DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION                (DRVOPXD_ERROR_RISCV_OFFSET + 0x07)   //abstarct reg cmd error exception
#define DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_HART_STATE               (DRVOPXD_ERROR_RISCV_OFFSET + 0x08)   //abstarct reg cmd error hart state
#define DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR                (DRVOPXD_ERROR_RISCV_OFFSET + 0x09)   //abstarct reg cmd bus error
#define DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON             (DRVOPXD_ERROR_RISCV_OFFSET + 0x0A)   //abstarct reg cmd other errors
#define DRVOPXD_ERROR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED                   (DRVOPXD_ERROR_RISCV_OFFSET + 0x0B)   //system bus sberror timeout
#define DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ADDRESS_FAILED                   (DRVOPXD_ERROR_RISCV_OFFSET + 0x0C)   //system bus sberror incorrect address
#define DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED                 (DRVOPXD_ERROR_RISCV_OFFSET + 0x0D)   //system bus sberror alignment error
#define DRVOPXD_ERROR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED          (DRVOPXD_ERROR_RISCV_OFFSET + 0x0E)   //system bus sberror unsupported size access
#define DRVOPXD_ERROR_RISCV_SYSTEM_BUS_OTHER_REASON                     (DRVOPXD_ERROR_RISCV_OFFSET + 0x0F)   //system bus sberror due to other reasons
#define DRVOPXD_ERROR_RISCV_SYSTEM_BUS_MASTER_BUSY                      (DRVOPXD_ERROR_RISCV_OFFSET + 0x10)   //system bus error due system bus masterbusy
#define DRVOPXD_ERROR_RISCV_MEM_INVALID_LENGTH                          (DRVOPXD_ERROR_RISCV_OFFSET + 0x11)
#define DRVOPXD_ERROR_RISCV_HALT_TIMEOUT                                (DRVOPXD_ERROR_RISCV_OFFSET + 0x12)   //unable to halt
#define DRVOPXD_ERROR_RISCV_RESUME_TIMEOUT                              (DRVOPXD_ERROR_RISCV_OFFSET + 0x13)   //unable to resume
#define DRVOPXD_ERROR_RISCV_UNSUPPORT_ARCH_TYPE                         (DRVOPXD_ERROR_RISCV_OFFSET + 0x14)   //Unknown architecture type to diskware
#define DRVOPXD_ERROR_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE                (DRVOPXD_ERROR_RISCV_OFFSET + 0x15)   //Unsupported memory size to diskware
#define DRVOPXD_ERROR_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION         (DRVOPXD_ERROR_RISCV_OFFSET + 0x16)   //Unsupported abstract access operation mentioned to diskware
#define DRVOPXD_ERROR_RISCV_TRIGGER_INFO_LOOP_TIMEOUT                   (DRVOPXD_ERROR_RISCV_OFFSET + 0x17)   //Loop break after 50 iterations
#define DRVOPXD_ERROR_RISCV_DISALE_ABSTRACT_COMMAND                     (DRVOPXD_ERROR_RISCV_OFFSET + 0x18)   //Not execute abstract command if resume halt or havereset is enable
#define DRVOPXD_ERROR_RISCV_NEXT_DEBUG_MODULE_TIMEOUT                   (DRVOPXD_ERROR_RISCV_OFFSET + 0x19)   //Failed and loop break after 100 iterations

#define DRVOPXD_ERROR_TARCH_OFFSET                    0x7000         // ThreadArch specific error codes

// common defines - boolean logic
#ifndef TRUE
#define TRUE         1
#endif

#ifndef FALSE
#define FALSE        0
#endif

// diskware types
typedef enum {DW_UNKNOWN, DW_NONE, DW_MIPS, DW_ARM, DW_ARC, DW_DIAG, DW_TARCH, DW_RISCV} TyDiskwareType;
// fpgaware types
typedef enum {FPGA_UNKNOWN, FPGA_NONE, FPGA_MIPS, FPGA_ARM, FPGA_ARC, FPGA_DIAG, FPGA_RISCV} TyFpgawareType;
// TPA types
typedef enum {TPA_UNKNOWN, TPA_NONE, TPA_INVALID, TPA_MIPS14, TPA_ARM20, TPA_ARC20, TPA_DIAG ,TPA_RISCV} TyTpaType;

// diskware structure types
typedef enum
{
   DWS_SCAN,
   DWS_MIPS_CORE,
   DWS_MIPS_PIPELINE,
   DWS_LAST
} TyDiskwareStructure;

// voltage sense channel
typedef enum {VS_VTREF, VS_USB, VS_VTPA} TyVoltageSenseType;

// error codes
#ifndef PFW
//typedef int    TyError;
typedef long TyError;
#endif

// device handle
typedef int32_t    TyDevHandle;

// device upgrade
typedef enum {UPG_CHECK_ONLY, UPG_NEWER_VERSION, UPG_DIFF_VERSION, UPG_FORCE_VERSION} TyDeviceUpgradeType;
typedef enum {UPG_ERASE_STAGE, UPG_PROGRAM_STAGE, UPG_RESET_STAGE, UPG_RECONNECT_STAGE, UPG_DONE} TyDeviceUpgradeStage;
typedef int32_t (*PfDeviceUpgradeCallback)(TyDeviceUpgradeStage, unsigned char);

typedef struct _TyDeviceInfo {
   // general device information
   char pszSerialNumber[MAX_SERIAL_NUMBER_LEN];
   uint32_t ulBoardRevision;
   uint32_t ulManufacturingDate;
   // firmware information
   uint32_t ulFirmwareVersion;
   uint32_t ulFirmwareDate;
   // diskware information
   uint32_t ulDiskwareType;
   uint32_t ulDiskwareVersion;
   uint32_t ulDiskwareDate;
   uint32_t uiActiveDiskwares;
   bool bOtherDiskwareRunning;
   // FPGA information
   uint32_t ulFpgawareType;
   uint32_t ulFpgawareVersion;
   uint32_t ulFpgawareDate;
   unsigned char bFpgawareConfigured;
   // TPA information
   TyTpaType tyTpaConnected;
   char pszTpaName[16];
   double dMinVtpa;
   double dMaxVtpa;
   double dCurrentVtpa;
   double dCurrentVtref;
   unsigned char bTrackingVtpaMode;
   unsigned char bTpaDriversEnabled;
   unsigned char bTargetConnected;
   uint32_t ulTpaRevision;
   char pszTpaSerialNumber[16];
} TyDeviceInfo;

// TMS sequence structure
typedef struct _TyTmsSequence {
   unsigned char ucPreTmsCount;                          // number of bits in TMS before scan
   unsigned short usPreTmsBits;                           // bits in TMS sequence before scan
   unsigned char ucPostTmsCount;                         // number of bits in TMS after scan
   unsigned short usPostTmsBits;                          // bits in TMS sequence after scan
} TyTmsSequence;

// multicore config structure
typedef struct _TyTAPConfig {
   unsigned short usDefaultIRLength;
   unsigned short usDefaultDRLength;
   uint32_t *pulBypassIRPattern;
} TyTAPConfig;

// multiple scan parameter structure
typedef struct _TyMultipleScanParams {
   unsigned short usOutReserved;                         // reserved, do not remove
   unsigned short usInReserved;                          // reserved, do not remove
   unsigned short usLength;                              // number of bits to scan
   unsigned short usPreTmsBits;                          // bits in TMS sequence before scan
   unsigned short usPostTmsBits;                         // bits in TMS sequence after scan
   unsigned short usPreTmsCount;                         // number of bits in TMS before scan
   unsigned short usPostTmsCount;                        // number of bits in TMS after scan
   unsigned char bScanIR;                                // scan into IR or DR register
} TyMultipleScanParams;

// TPA structure
typedef struct _TyTpaParameters {
   TyTpaType tyTpaType;                                  // TPA type
   char pucTpaSerialNumber[16];                          // TPA serial number
   uint32_t ulTpaRevision;                          // TPA revision
   uint32_t ulManufacturingDate;                    // manufacturing date
} TyTpaParameters;

// Function prototypes
typedef void(*PfPrintFunc)(const char *);                              // callback for general print messages
/****************************************************************************
*** Device Management                                                     ***
****************************************************************************/
// Following functions provides basic device management
TyError DL_OPXD_TerminateDW(TyDevHandle tyDevHandle, TyDevType tyDevType);
TyError DL_OPXD_OpenDevice(const char *pszSerialNumber,
                           unsigned short usDevicePID,
                           unsigned char bShareDevice,
                           TyDevHandle *ptyDevHandle);
TyError DL_OPXD_CloseDevice(TyDevHandle tyDevHandle);
TyError DL_OPXD_UpgradeDevice(TyDevHandle tyDevHandle,
                              TyDeviceUpgradeType tyUpgradeType,
                              const char *pszFirmwareFilename,
                              const char *pszFlashUtilFilename,
                              unsigned char *pbReconnectNeeded,
                              int32_t *piFirmwareDiff,
                              uint32_t *pulFileFwVersion,
                              uint32_t *pulDeviceFwVersion,
                              const char *pszSerialNumber,
                              PfPrintFunc pfPrintFunc);
TyError DL_OPXD_UpgradeDevice_R2(TyDevHandle         tyDevHandle,
                                 TyDeviceUpgradeType tyUpgradeType,
                                 const char         *pszFirmwareFilename,
                                 unsigned char      *pbReconnectNeeded,
                                 int32_t                *piFirmwareDiff,
                                 uint32_t      *pulFileFwVersion,
                                 uint32_t      *pulDeviceFwVersion);
TyError DL_OPXD_UpgradeDevice_R3(TyDevHandle         tyDevHandle,
                                 TyDeviceUpgradeType tyUpgradeType,
                                 const char         *pszFirmwareFilename,
                                 const char         *pszFlashUtilFilename,
                                 unsigned char      *pbReconnectNeeded,
                                 int32_t            *piFirmwareDiff,
                                 uint32_t      *pulFileFwVersion,
                                 uint32_t      *pulDeviceFwVersion,
                                 const char         *pszSerialNumber,
                                 PfPrintFunc         pfPrintFunc);
TyError DL_OPXD_GetDeviceInfo(TyDevHandle tyDevHandle,
                              TyDeviceInfo *ptyDeviceInfo,
							  TyDiskwareType tyDwType);

TyError DL_OPXD_ExecuteDW(TyDevHandle tyDevHandle, TyDevType tyDevType);

TyError DL_OPXD_GetDiskwareStatus(TyDevHandle tyDevHandle,
								  TyDeviceInfo *ptyDeviceInfo,
								  TyDevType tyDwType);

void DL_OPXD_GetConnectedDevices(char ppszDeviceNumbers[MAX_DEVICES_SUPPORTED][MAX_SERIAL_NUMBER_LEN],
                                 unsigned short *pusNumberOfDevices,
                                 unsigned short usPID);
TyError DL_OPXD_ConfigureDevice(TyDevHandle tyDevHandle,
                                TyDiskwareType tyDwType,
                                TyFpgawareType tyFpgaType,
                                const char *pszDiskwareFilename,
                                const char *pszFpgawareFilename);
TyError DL_OPXD_UnconfigureDevice(TyDevHandle tyDevHandle);

/****************************************************************************
*** Synchronization Support                                               ***
****************************************************************************/
int32_t DL_OPXD_LockDevice(TyDevHandle tyDevHandle);
void DL_OPXD_UnlockDevice(TyDevHandle tyDevHandle);
void DL_OPXD_LockDriver(void);
void DL_OPXD_UnlockDriver(void);

/****************************************************************************
*** Basic Functions                                                      ***
****************************************************************************/
TyError DL_OPXD_DeviceMemoryAccess(TyDevHandle tyDevHandle,
                                   uint32_t ulMemoryAddress,
                                   unsigned char ucDataSize,
                                   uint32_t ulWrittenValue,
                                   uint32_t *pulReadValue,
                                   unsigned char bWriteAccess);
TyError DL_OPXD_SetVtpaVoltage(TyDevHandle tyDevHandle,
                               double dVoltage,
                               unsigned char bTrackingTarget,
                               unsigned char bEnableDrivers);
TyError DL_OPXD_VoltageSense(TyDevHandle tyDevHandle,
                             TyVoltageSenseType tyChannel,
                             double *pdVoltage);
TyError DL_OPXD_MeasurePllFrequency(TyDevHandle tyDevHandle,
                                    double dExpectedFrequency,
                                    double *pdMeasuredFrequency);
TyError DL_OPXD_WriteTpaSigniture(TyDevHandle tyDevHandle,
                                  TyTpaParameters *ptyTpaParams);
TyError DL_OPXD_ProcessCP15Register(TyDevHandle tyDevHandle, int32_t uiCore,
                                    uint32_t uiArmType, uint32_t uiArmFamily,
                                    uint32_t uiOperation, uint32_t ulOpcode, uint32_t *pulRegValue);
TyError DL_OPXD_ReadCPRegister(TyDevHandle tyDevHandle, uint32_t uiCore,
                               uint32_t uiArmType,uint32_t uiArmFamily, uint32_t uiCpNum,
                               uint32_t uiRegIndex, uint32_t *puiRegValue);
TyError DL_OPXD_WriteCPRegister(TyDevHandle tyDevHandle, uint32_t uiCore,
                                uint32_t uiArmType, uint32_t uiArmFamily,
                                uint32_t uiCpNum, uint32_t uiRegIndex,
                               uint32_t uiRegValue);

/****************************************************************************
*** General JTAG Functions                                                ***
****************************************************************************/
TyError DL_OPXD_JtagSetFrequency(TyDevHandle tyDevHandle,
                                 double dFrequency,
                                 double *pdPllFrequency,
                                 double *pdFpgaDivideRatio);
TyError DL_OPXD_JtagSetAdaptiveClock(TyDevHandle tyDevHandle,
                                     uint32_t ulTimeout,
                                     double dAlternativeFrequency);
TyError DL_OPXD_JtagConfigMulticore(TyDevHandle tyDevHandle,
                                    uint32_t ulNumberOfCores,
                                    TyTAPConfig pTyTAPConfig[]);
TyError DL_OPXD_JtagSetTMS(TyDevHandle tyDevHandle,
                           TyTmsSequence *ptyTmsForScanIR,
                           TyTmsSequence *ptyTmsForScanDR,
                           TyTmsSequence *ptyTmsForScanIRandDR_IR,
                           TyTmsSequence *ptyTmsForScanIRandDR_DR);
TyError DL_OPXD_JtagScanIR(TyDevHandle tyDevHandle,
                           uint32_t ulCore,
                           uint32_t ulIRLength,
                           uint32_t *pulDataIn,
                           uint32_t *pulDataOut);
TyError DL_OPXD_JtagScanDR(TyDevHandle tyDevHandle,
                           uint32_t ulCore,
                           uint32_t ulDRLength,
                           uint32_t *pulDataIn,
                           uint32_t *pulDataOut);
TyError DL_OPXD_JtagScanIRandDR(TyDevHandle tyDevHandle,
                                uint32_t ulCore,
                                uint32_t ulIRLength,
                                uint32_t *pulDataInForIR,
                                uint32_t ulDRLength,
                                uint32_t *pulDataInForDR,
                                uint32_t *pulDataOutFromDR);
TyError DL_OPXD_JtagResetTAP(TyDevHandle tyDevHandle,
                             unsigned char bUseTrst);
TyError DL_OPXD_IndicateTargetStatus(TyDevHandle tyDevHandle,
                                     uint32_t ulCore,
                                     unsigned char bTargetIsRunning);
TyError DL_OPXD_JtagPulses(TyDevHandle tyDevHandle,
                           uint32_t ulNumberOfPulses,
                           uint32_t ulPulsePeriodUs,
                           uint32_t *pulTDIPattern,
                           uint32_t *pulTMSPattern,
                           uint32_t *pulTDOPattern);
TyError DL_OPXD_JtagGetPins(TyDevHandle tyDevHandle,
                            unsigned char *pucTDIPin,
                            unsigned char *pucTDOPin,
                            unsigned char *pucTMSPin,
                            unsigned char *pucTCKPin);
TyError DL_OPXD_JtagScanMultiple(TyDevHandle tyDevHandle, uint32_t ulCore, uint32_t ulNumberOfScans,
                                 TyMultipleScanParams *ptyScanParams, uint32_t **ppulDataIn, uint32_t **ppulDataOut);

//cJTAG Functions
TyError DL_OPXD_cJtagEscape(TyDevHandle tyDevHandle, unsigned char ucEscapeSequence);
TyError DL_OPXD_cJtagTwoPartCommand(TyDevHandle tyDevHandle, unsigned char ucOpcodeCount, unsigned char ucOperandCount);
TyError DL_OPXD_cJtagThreePartCommand(TyDevHandle tyDevHandle, unsigned char ucOpcodeCount, unsigned char ucOperandCount,
                                      uint32_t ulCore, uint32_t ulDRLength, uint32_t *pulDataToScan,
                                      uint32_t *pulDataFromScan);
TyError DL_OPXD_cJtagInitTap7Controller(TyDevHandle tyDevHandle);
TyError DL_OPXD_cJtagChangeScanFormat(TyDevHandle tyDevHandle, unsigned char ucScanMode,
                                      unsigned char ucReadyCount, unsigned char ucDelayCount);
TyError DL_OPXD_cJtagSetFPGAtocJTAGMode(TyDevHandle tyDevHandle, unsigned char uccJtagMode);

/****************************************************************************
*** Target specific functions are placed in separate header files         ***
****************************************************************************/

/****************************************************************************
*** Diagnostic Support                                                    ***
****************************************************************************/
TyError DL_OPXD_LedDiagnosticTest(TyDevHandle tyDevHandle,
                                  unsigned char bDiagnosticMode,
                                  uint32_t ulLedDiagnosticValue);
TyError DL_OPXD_CpuDiagnosticTest(TyDevHandle tyDevHandle);
TyError DL_OPXD_FpgaDiagnosticTest(TyDevHandle tyDevHandle);
TyError DL_OPXD_PllDiagnosticTest(TyDevHandle tyDevHandle);
TyError DL_OPXD_ExtramDiagnosticTest(TyDevHandle tyDevHandle, uint32_t *pulMemSize);
TyError DL_OPXD_TestPerformance(TyDevHandle tyDevHandle,
                                unsigned char bDownload);
TyError DL_OPXD_TpaLoopDiagnosticTest(TyDevHandle tyDevHandle,
                                      unsigned char *pbResult);
TyError DL_OPXD_TpaInterfaceDiagnosticTest(TyDevHandle tyDevHandle);

#endif // _DRVOPXD_H_

