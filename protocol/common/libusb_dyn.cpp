/****************************************************************************
       Module: libusb_dyn.cpp
     Engineer: Vitezslav Hola
  Description: Dynamic implementation of Libusb funcitons

 * LIBUSB-WIN32, Generic Windows USB Library
 * Copyright (c) 2002-2005 Stephan Meyer <ste_meyer@web.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Date           Initials    Description
08-Oct-2007    VH          Initial
29-May-2019    SPC         Rewrite to use libusb-1.0
****************************************************************************/
#ifdef __LINUX
#include <libusb-1.0/libusb.h>
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
// Linux build change...
#include "protocol/common/types.h"

// DLL name
#define LIBUSB_DLL_NAME "libusb-1.0.so"
#define ENOFILE 2
#define  LoadLibrary(lib)                 dlopen(lib, RTLD_NOW)
#define  GetProcAddress(handle, proc)     dlsym(handle, proc)
#define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#else
#include <windows.h>
#include <cstdio>

//Disable some warnings generated in libusb.h
#pragma warning( push )
#pragma warning( disable : 4200 )
#include "drivers/usb/include/libusb-1.0/libusb.h"
#pragma warning( pop )

// DLL name
#define LIBUSB_DLL_NAME "libusb-1.0.dll"
#define ENOFILE 2
#endif

#include "libusb_dyn.h"

static libusb_context* ctx = NULL;

// function prototypes
typedef int(LIBUSB_CALL*libusb_init_t)(libusb_context **ctx);
typedef void(LIBUSB_CALL *libusb_exit_t)(libusb_context *ctx);

typedef ssize_t(LIBUSB_CALL *libusb_get_device_list_t)(libusb_context *ctx, libusb_device ***list);
typedef void(LIBUSB_CALL *libusb_free_device_list_t)(libusb_device **list, int unref_devices);

typedef int(LIBUSB_CALL *libusb_open_t)(libusb_device *dev, libusb_device_handle **dev_handle);
typedef void(LIBUSB_CALL *libusb_close_t)(libusb_device_handle *dev_handle);

typedef int(LIBUSB_CALL *libusb_set_configuration_t)(libusb_device_handle *dev_handle, int configuration);
typedef int(LIBUSB_CALL *libusb_claim_interface_t)(libusb_device_handle *dev_handle, int interface_number);
typedef int(LIBUSB_CALL *libusb_release_interface_t)(libusb_device_handle *dev_handle, int interface_number);

typedef int(LIBUSB_CALL *libusb_clear_halt_t)(libusb_device_handle *dev_handle, unsigned char endpoint);
typedef int(LIBUSB_CALL *libusb_reset_device_t)(libusb_device_handle *dev_handle);

typedef int(LIBUSB_CALL *libusb_get_device_descriptor_t)(libusb_device *dev,
    struct libusb_device_descriptor *desc);

typedef int(LIBUSB_CALL *libusb_get_string_descriptor_ascii_t)(libusb_device_handle *dev_handle,
    uint8_t desc_index, unsigned char *data, int length);

typedef int(LIBUSB_CALL *libusb_bulk_transfer_t)(libusb_device_handle *dev_handle,
    unsigned char endpoint, unsigned char *data, int length,
    int *actual_length, unsigned int timeout);

typedef const char * (LIBUSB_CALL *libusb_error_name_t)(int errcode);

// local pointers to functions
static libusb_init_t _libusb_init = NULL;
static libusb_exit_t _libusb_exit = NULL;
static libusb_get_device_list_t _libusb_get_device_list = NULL;
static libusb_free_device_list_t _libusb_free_device_list = NULL;
static libusb_get_device_descriptor_t _libusb_get_device_descriptor = NULL;
static libusb_get_string_descriptor_ascii_t _libusb_get_string_descriptor_ascii = NULL;
static libusb_open_t _libusb_open = NULL;
static libusb_close_t _libusb_close = NULL;
static libusb_set_configuration_t _libusb_set_configuration = NULL;
static libusb_claim_interface_t _libusb_claim_interface = NULL;
static libusb_release_interface_t _libusb_release_interface = NULL;
static libusb_clear_halt_t _libusb_clear_halt = NULL;
static libusb_reset_device_t _libusb_reset_device = NULL;
static libusb_error_name_t _libusb_error_name = NULL;
static libusb_bulk_transfer_t _libusb_bulk_transfer = NULL;

/****************************************************************************
     Function: usb_init
     Engineer: Suresh P.C
        Input: none
       Output: USB_SUCCESS or USB_ERROR
  Description: Initialize the libusb library
Date           Initials    Description
29-May-2019    SPC         Initial
****************************************************************************/
int usb_init(void)
{
    HINSTANCE libusb_dll = LoadLibrary(LIBUSB_DLL_NAME);
    int r;

    if (!libusb_dll) {
#ifdef __LINUX
        printf("Error : %s\n", dlerror());
#endif
        fprintf(stdout, "LIBUSB: Failed to load %s. Please install Opella-XD driver\n", LIBUSB_DLL_NAME);
        return USB_ERROR;
    }

    _libusb_init = (libusb_init_t)GetProcAddress(libusb_dll, "libusb_init");
    _libusb_exit = (libusb_exit_t)GetProcAddress(libusb_dll, "libusb_exit");
    _libusb_get_device_list = (libusb_get_device_list_t)GetProcAddress(libusb_dll, "libusb_get_device_list");
    _libusb_free_device_list = (libusb_free_device_list_t)GetProcAddress(libusb_dll, "libusb_free_device_list");
    _libusb_get_device_descriptor = (libusb_get_device_descriptor_t)GetProcAddress(libusb_dll, "libusb_get_device_descriptor");
    _libusb_get_string_descriptor_ascii = (libusb_get_string_descriptor_ascii_t)GetProcAddress(libusb_dll, "libusb_get_string_descriptor_ascii");

    _libusb_open = (libusb_open_t)GetProcAddress(libusb_dll, "libusb_open");
    _libusb_close = (libusb_close_t)GetProcAddress(libusb_dll, "libusb_close");

    _libusb_set_configuration = (libusb_set_configuration_t)GetProcAddress(libusb_dll, "libusb_set_configuration");
    _libusb_claim_interface = (libusb_claim_interface_t)GetProcAddress(libusb_dll, "libusb_claim_interface");
    _libusb_release_interface = (libusb_release_interface_t)GetProcAddress(libusb_dll, "libusb_release_interface");

    _libusb_clear_halt = (libusb_clear_halt_t)GetProcAddress(libusb_dll, "libusb_clear_halt");
    _libusb_reset_device = (libusb_reset_device_t)GetProcAddress(libusb_dll, "libusb_reset_device");
    _libusb_error_name = (libusb_error_name_t)GetProcAddress(libusb_dll, "libusb_error_name");

    _libusb_bulk_transfer = (libusb_bulk_transfer_t)GetProcAddress(libusb_dll, "libusb_bulk_transfer");

    if (_libusb_init) {
        r = _libusb_init(&ctx);
        if (r != LIBUSB_SUCCESS) {
            fprintf(stdout, "LIBUSB: libusb_init failed. Errorcode: %d\n", r);
            return r;
        }
    } else {
        fprintf(stdout, "LIBUSB: Failed to load %s. Please install Opella-XD driver\n", LIBUSB_DLL_NAME);
        return USB_ERROR;
    }

    return USB_SUCCESS;
}

/****************************************************************************
     Function: usb_exit
     Engineer: Suresh P.C
        Input: None
       Output: None
  Description: Exit the libusb driver
Date           Initials    Description
29-May-2019    SPC         Initial
****************************************************************************/
void usb_exit(void)
{
    //Device release
    _libusb_exit(ctx);
}

/****************************************************************************
     Function: usb_get_dev_list
     Engineer: Suresh P.C
        Input: vid: Vendor ID of the device
               pid: Product ID of the devie
       Output: devicelist : Device list
      Returns: Number of devices found
  Description: Return the list of connected devices with vid and pid
Date           Initials    Description
31-May-2019    SPC         Initial
****************************************************************************/
int usb_get_dev_list(uint16_t vid, uint16_t pid, char devicelist[][MAX_SERIAL_NUMBER_LEN])
{
    libusb_device** usb_devs;
    libusb_device* dev;
    ssize_t cnt;
    int num_devs = 0;

    int i = 0;
    int status;
    int ret;
    libusb_device_handle* device = NULL;

    unsigned char pcDevSerialNumber[MAX_SERIAL_NUMBER_LEN];

    cnt = _libusb_get_device_list(ctx, &usb_devs);
    if (cnt < 0) {
        _libusb_exit(ctx);
        fprintf(stdout, "LIBUSB: Failed to get device list\n");
        return 0;
    }

    while ((dev = usb_devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        ret = _libusb_get_device_descriptor(dev, &desc);
        if (ret < 0) {
            fprintf(stdout, "LIBUSB: Failed to get device descriptor\n");
            return 0;
        }

        if ((desc.idVendor == vid) && (desc.idProduct == pid)) {
            status = _libusb_open(dev, &device);
            if (status < 0) {
                fprintf(stdout, "libusb_open() failed: %s\n", _libusb_error_name(status));
                return 0;
            }

            //Get Device serial number
            if (desc.iSerialNumber) {
                //Read the serial number
                ret = _libusb_get_string_descriptor_ascii(device, desc.iSerialNumber, pcDevSerialNumber, MAX_SERIAL_NUMBER_LEN);
                if (ret <= 0) {
                    fprintf(stdout, "LIBUSB: libusb_get_string_descriptor_ascii failed\n");
                    return 0;
                }

                pcDevSerialNumber[ret] = '\0';
                strcpy(devicelist[num_devs++], (char*)pcDevSerialNumber);
            }
            _libusb_release_interface(device, 0);
            _libusb_close(device);
        }
    }

    _libusb_free_device_list(usb_devs, 1);
    return num_devs;
}

/****************************************************************************
     Function: usb_open
     Engineer: Suresh P.C
        Input: vid: Vendor ID of the device
               pid: Product ID of the devie
               slno: Serial number of device
       Output: None
      Returns: Device handle
  Description: Open the usb device with vid, pid and slno
Date           Initials    Description
31-May-2019    SPC         Initial
****************************************************************************/
usb_dev_handle *usb_open(uint16_t vid, uint16_t pid, const char *slno)
{
    libusb_device** usb_devs;
    libusb_device* dev;
    ssize_t cnt;

    int i = 0;
    int status;
    int ret;
    libusb_device_handle* device = NULL;

    unsigned char pcDevSerialNumber[MAX_SERIAL_NUMBER_LEN];

    cnt = _libusb_get_device_list(ctx, &usb_devs);
    if (cnt < 0) {
        _libusb_exit(ctx);
        fprintf(stdout, "LIBUSB: Failed to get device list\n");
        return NULL;
    }

    while ((dev = usb_devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        ret = _libusb_get_device_descriptor(dev, &desc);
        if (ret < 0) {
            fprintf(stdout, "LIBUSB: Failed to get device descriptor\n");
            return NULL;
        }

        if ((desc.idVendor == vid) && (desc.idProduct == pid)) {
            status = _libusb_open(dev, &device);
            if (status < 0) {
                fprintf(stdout, "libusb_open() failed: %s\n", _libusb_error_name(status));
                return NULL;
            }

            //Get Device serial number
            if (desc.iSerialNumber) {
                //Read the serial number
                ret = _libusb_get_string_descriptor_ascii(device, desc.iSerialNumber, pcDevSerialNumber, sizeof(pcDevSerialNumber));
                if (ret <= 0) {
                    fprintf(stdout, "LIBUSB: libusb_get_string_descriptor_ascii failed\n");
                    return NULL;
                }

                pcDevSerialNumber[ret] = '\0';
                if (strcmp((char*)pcDevSerialNumber, slno) == 0) {
                    //Select Opella-XD Device Configuration
                    _libusb_set_configuration(device, 1);
                    ret = _libusb_claim_interface(device, 0);
                    if (ret != 0) {
                        fprintf(stdout, "LIBUSB: libusb_claim_interface failed\n");
                        return NULL;
                    }
                    break;
                } else {
                    _libusb_release_interface(device, 0);
                    _libusb_close(device);
                    device = NULL;
                }
            }
        }
    }

    _libusb_free_device_list(usb_devs, 1);
    return device;
}

/****************************************************************************
     Function: usb_close
     Engineer: Suresh P.C
        Input: Device handle
       Output: None
  Description: Close the device handle
Date           Initials    Description
29-May-2019    SPC         Initial
****************************************************************************/
void usb_close(usb_dev_handle *device)
{
    //Device release
    _libusb_release_interface(device, 0);
#ifdef __LINUX
    _libusb_reset_device(device);
#endif
    _libusb_close(device);
}

/****************************************************************************
     Function: usb_resetep
     Engineer: Suresh P.C
        Input: device: Device handle
               ep: USB end point
       Output: None
  Description: Reset the EP
Date           Initials    Description
29-May-2019    SPC         Initial
****************************************************************************/
void usb_resetep(usb_dev_handle *device, int32_t ep)
{
    _libusb_clear_halt(device, ep);
}

/****************************************************************************
     Function: usb_bulk_write
     Engineer: Suresh P.C
        Input: dev: USB Device handle
               ep: USB End point handle
               bytes: Data to transfer
               size: Size of data to transfer
               timeout: Transfer timeout
       Output: None
      Returns: Actual byte transferred via USB
  Description: Write data to USB endpoint
Date           Initials    Description
29-May-2019    SPC          Initial
****************************************************************************/
int32_t usb_bulk_write(usb_dev_handle *dev, int32_t ep, char *bytes, int32_t size, int32_t timeout)
{
    int ret, actual_length;

    ret = _libusb_bulk_transfer(dev, ep & 0xff, (unsigned char*)bytes, size, &actual_length, timeout);
    if (ret < 0)
        return USB_ERROR;

    return actual_length;
}

/****************************************************************************
     Function: usb_bulk_read
     Engineer: Suresh P.C
        Input: dev: USB Device handle
               ep: USB End point handle
               bytes: Data to transfer
               size: Size of data to transfer
               timeout: Transfer timeout
       Output: None
      Returns: Actual byte transferred via USB.
               USB_ERROR(-1) in case of error
  Description: Read data from USB endpoint
Date           Initials    Description
29-May-2019    SPC         Initial
****************************************************************************/
int32_t usb_bulk_read(usb_dev_handle *dev, int32_t ep, char *bytes, int32_t size, int32_t timeout)
{
    int ret, actual_length;

    ret = _libusb_bulk_transfer(dev, ep & 0xff, (unsigned char*)bytes, size, &actual_length, timeout);
    if (ret < 0)
        return USB_ERROR;

    return actual_length;
}
