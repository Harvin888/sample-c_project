/****************************************************************************
       Module: interface.h
     Engineer:
  Description: Header with Opella-XD RISCV MetaWare debugger driver
Date           Initials    Description
08-Oct-2007             Initial
****************************************************************************/
#ifndef INTERFACE_H_
#define INTERFACE_H_

#include "ml_riscv.h"

// MetaWare debugger driver version strings
#ifdef __LINUX
// Linux
#define INTERFACE_NAME_AND_VERSION  "Ashling Opella-XD RISCV Driver, 27-November-2018 v1.0"
#define INTERFACE_NAME              "Ashling Opella-XD RISCV Driver"
#define INTERFACE_VERSION           "1-October-2018 v1.0"
#else
// WinXP/2000/Vista
#define INTERFACE_NAME_AND_VERSION  "Ashling Opella-XD RISCV Driver, 17-May-2019 v2.0"
#define INTERFACE_NAME              "Ashling Opella-XD RISCV Driver"
#define INTERFACE_VERSION           "17-May-2019 v2.0"

#endif

//AUX PM Registers
#define REG_PM_STATUS   0x450

// MetaWare RISCV Processor Interface defines
// return values
#define ASH_POFF        4
#define ASH_PDOWN       2
#define ASH_TRUE        1
#define ASH_FALSE       0

#define ERR_RISCV_NO_ERROR     0
#define ERR_RISCV_ERROR        1

//riscv abstract register error codes
#define ASH_RISCV_ABSTRACT_REG_WRITE_BUSY                       0x5  //abstarct reg cmd error busy
#define ASH_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED              0x6  //abstarct reg cmd error not supported
#define ASH_RISCV_ABSTRACT_REG_WRITE_EXCEPTION                  0x7  //abstarct reg cmd error exception
#define ASH_RISCV_ABSTRACT_REG_WRITE_HART_STATE                 0x8  //abstarct reg cmd error hart state
#define ASH_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR                  0x9  //abstarct reg cmd bus error
#define ASH_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR                0xA  //abstarct reg cmd other errors
#define ASH_RISCV_SYSTEM_BUS_TIMEOUT_FAILED                     0xB  //system bus sberror timeout
#define ASH_RISCV_SYSTEM_BUS_ADDRESS_FAILED                     0xC  //system bus sberror incorrect address
#define ASH_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED                   0xD  //system bus sberror alignment error
#define ASH_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED            0xE  //system bus sberror unsupported size access
#define ASH_RISCV_SYSTEM_BUS_OTHER_REASON                       0xF  //system bus sberror due to other reasons
#define ASH_RISCV_SYSTEM_BUS_MASTER_BUSY                        0x10 //system bus master busy
#define ASH_RISCV_MEM_INVALID_LENGTH                            0x11 //system bus master busy
#define ASH_RISCV_HALT_TIMEOUT                                  0x12 //unable to halt
#define ASH_RISCV_RESUME_TIMEOUT                                0x13 //unable to resume
#define ASH_RISCV_UNSUPPORT_ARCH_TYPE                           0x14 //Unknown architecture type to diskware
#define ASH_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE                  0x15 //Unsupported memory size to diskware
#define ASH_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION           0x16 //Unsupported abstract access operation mentioned to diskware
#define ASH_RISCV_TRIGGER_INFO_LOOP_TIMEOUT                     0x17 //Loop break after 50 iterations
#define ASH_RISCV_DISALE_ABSTRACT_COMMAND                       0x18 //Not execute abstract command if resume halt or havereset is enable
#define ASH_RISCV_NEXT_DEBUG_MODULE_TIMEOUT                     0x19 //Failed and loop break after 100 iterations
#define ASH_RISCV_INVALID_TRIGGER_OPERATION                     0x1A //Unsupported trigger operation.
//
#ifdef __LINUX
// Linux
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
typedef int BOOL;
#define TRUE 1
#define FALSE 0
#define _snprintf snprintf
//extern void usleep(int);
void Sleep(unsigned int  mseconds);
#define ASH_DLL_EXPORT
#else
// WinXP/2000/Vista
#define ASH_DLL_EXPORT __declspec(dllexport)
#endif

// --- Debugger Interface Function table ---
typedef struct _TyFunctionTable {
    int(*DummyFunction)(struct TyRISCVInstance*);
    int(*version)(struct TyRISCVInstance*);
    const char *   (*id)(struct TyRISCVInstance*);
    void(*destroy)(struct TyRISCVInstance*);
    const char *   (*additional_possibilities)(struct TyRISCVInstance*);
    void*          (*additional_information)(struct TyRISCVInstance*, unsigned);
    int(*prepare_for_new_program)(struct TyRISCVInstance*, int);
    int(*process_property)(struct TyRISCVInstance*, const char *, const char *);
    int(*is_simulator)(struct TyRISCVInstance*);
    int(*step)(struct TyRISCVInstance*,unsigned short, uint32_t,uint32_t, uint32_t);
    int(*Run)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t);
    int(*Halt)(struct TyRISCVInstance*, unsigned short, uint32_t,uint32_t);
    int(*StatusProc)(struct TyRISCVInstance*, unsigned short, unsigned short, uint32_t, unsigned char *, unsigned char *,uint32_t,uint32_t);
    int(*Discovery)(struct TyRISCVInstance*, unsigned short, uint32_t *,uint32_t);
    int(*Discover_Harts)(struct TyRISCVInstance*, unsigned short, uint32_t *);
    int(*riscv_mechanism)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t,uint32_t,uint32_t);
    int(*TriggerInfo)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t *,uint32_t, uint32_t);
    int(*read_memory)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t , uint32_t, uint32_t, unsigned char *,uint32_t, uint32_t);
    int(*write_memory)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t, unsigned char *,uint32_t, uint32_t);
    int(*read_reg)(struct TyRISCVInstance*,unsigned short, uint32_t, uint32_t *,uint32_t,uint32_t, uint32_t );
    int(*write_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t,uint32_t, uint32_t );
    int(*write_a_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t,uint32_t, uint32_t);
    int(*read_a_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t,uint32_t, uint32_t);
    int(*writeword)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t *,uint32_t, uint32_t );
    int(*readword)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t *,uint32_t, uint32_t );
    int(*writedpc)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t*,uint32_t, uint32_t);
    int(*writecsr)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t*, uint32_t, uint32_t, uint32_t);
    int(*addtrigger)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t *, uint32_t, uint32_t, uint32_t, uint32_t,uint32_t, uint32_t);
    int(*removetrigger)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t,uint32_t, uint32_t,uint32_t);
    unsigned(*memory_size)(struct TyRISCVInstance*);
    int(*set_memory_size)(struct TyRISCVInstance*, unsigned);
    int(*set_reg_watchpoint)(struct TyRISCVInstance*, int, int);
    int(*remove_reg_watchpoint)(struct TyRISCVInstance*, int, int);
    int(*set_mem_watchpoint)(struct TyRISCVInstance*, uint32_t, int);
    int(*remove_mem_watchpoint)(struct TyRISCVInstance*, uint32_t, int);
    int(*stopped_at_watchpoint)(struct TyRISCVInstance*);
    int(*stopped_at_exception)(struct TyRISCVInstance*);
    int(*set_breakpoint)(struct TyRISCVInstance*, unsigned, void*);
    int(*remove_breakpoint)(struct TyRISCVInstance*, unsigned, void*);
    int(*retrieve_breakpoint_code)(struct TyRISCVInstance*, unsigned, char *, unsigned, void *);
    int(*breakpoint_cookie_len)(struct TyRISCVInstance*);
    int(*at_breakpoint)(struct TyRISCVInstance*);
    int(*define_displays)(struct TyRISCVInstance*, struct Register_display *);
    int(*fill_memory)(struct TyRISCVInstance*, uint32_t, void *, uint32_t, uint32_t, int);
    int(*instruction_trace_count)(struct TyRISCVInstance*);
    void(*get_instruction_traces)(struct TyRISCVInstance*, uint32_t *);
    void(*receive_callback)(struct TyRISCVInstance*, RISCV_callback*);
    int(*supports_feature)(struct TyRISCVInstance*);
    uint32_t(*data_exchange)(struct TyRISCVInstance*, uint32_t, uint32_t, uint32_t, void *, uint32_t, void *);
    int(*in_same_process_as_debugger)(struct TyRISCVInstance*);
    uint32_t(*max_data_exchange_transfer)(struct TyRISCVInstance*);
    int(*read_banked_reg)(struct TyRISCVInstance*, int, int, uint32_t *);
    int(*write_banked_reg)(struct TyRISCVInstance*, int, int, uint32_t *);
    int(*configure_vega)(struct TyRISCVInstance*, unsigned short, unsigned int, unsigned int uiCore, unsigned int uiIRLen);
	int(*target_reset)(struct TyRISCVInstance*,uint32_t ulHartId);
} TyFunctionTable;

// --- Debugger Interface Identifier structure ---
typedef struct TyRISCVInstance
{
    TyFunctionTable *ptyFunctionTable;                     // pointer to interface function table
    uint32_t ulMemorySize;                                 // memory size
    unsigned int  uiPMVersion;                             // PU BCR
    unsigned char bTraceMessagesEnabled;                   // flag enabling trace messages
    unsigned char bUseGUI;                                 // flag indicating using GUI (dialog boxes, etc.)
    unsigned char bStartupInfoShown;                       // flag indicating if starting message has been shown
    unsigned short usTapNumber;                            //indicating the tap number
    unsigned int  uiCoreNumber;                            // which core the interface belongs to
    uint32_t ulHartId;                                     //indicating the hartId
    char          pszIniFileLocation[260];
    RISCV_callback *pcbCallbackFunction;
} TyRISCVInstanceStruct;

char *pszIniFileName;

// defines for start_state parameter
#define RISCV_START_STATE_FROM_CURRENT               0x00        // staying in current state (Select-IR-Scan or Select-DR-Scan state)
#define RISCV_START_STATE_FROM_RTI                   0x01        // move from Run-Test/Idle into Select-IR-Scan or Select-DR-Scan state before scan
#define RISCV_START_STATE_FROM_DR                    0x02        // move from Select-DR-Scan into Select-IR-Scan or Select-DR-Scan state before scan
#define RISCV_START_STATE_FROM_EXIT1_IR_RTI          0x03          // move from Exit1-IR state to shift-DR state via RTI
#define RISCV_START_STATE_FROM_EXIT1_DR_RTI          0x04        // move from Exit1-DR state to shift-IR state via RTI
// defines for end_state parameter
#define RISCV_END_STATE_TO_RTI                       0x00        // move to Run-Test/Idle state after scan
#define RISCV_END_STATE_TO_DR                        0x01        // move to Select-DR-Scan state after scan
#define RISCV_END_STATE_TO_IR                        0x02        // move to Select-IR-Scan state after scan
#define RISCV_END_STATE_TO_RTI_DR                    0x03        // move to Run-Test/Idle state and continue to Select-DR-Scan state after scan
#define RISCV_END_STATE_TO_RTI_IR                    0x04        // move to Run-Test/Idle state and continue to Select-IR-Scan state after scan
#define RISCV_END_STATE_TO_RTI_SKIPPAUSE             0x05        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states after scan
#define RISCV_END_STATE_TO_DR_SKIPPAUSE              0x06        // move to Select-DR-Scan state skipping PAUSE and EXIT2 states after scan
#define RISCV_END_STATE_TO_IR_SKIPPAUSE              0x07        // move to Select-IR-Scan state skipping PAUSE and EXIT2 states after scan
#define RISCV_END_STATE_TO_RTI_DR_SKIPPAUSE          0x08        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states and continue to Select-DR-Scan state after scan
#define RISCV_END_STATE_TO_RTI_IR_SKIPPAUSE          0x09        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states and continue to Select-IR-Scan state after scan
#define RISCV_END_STATE_TO_EXIT1_IR                  0x0A        // move to Exit1-IR state after scan
#define RISCV_END_STATE_TO_EXIT1_DR                  0x0B        // move to Exit1-DR state after scan
TyRISCVTargetParams *ptyMLHandle = NULL;                             // pointer to structure used in MidLayer

#endif   // INTERFACE_H_
