#ifndef __LINUX
// Win specific defines
    #include "windows.h"
    #include "stdio.h"
	 #include "protocol\common\types.h"
#include "..\..\utility\debug\debug.h"

//#define _MSC_VER 0 //We buil it with GCC
#define ASH_RISCV_FOR_WINDOWS_GCC 1
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "riscvint.h"
#else
#include <dlfcn.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
	 #include "protocol/common/types.h"
#define ASH_RISCV_FOR_WINDOWS_GCC 1                              // required when building driver for MetaWare for Windows !!! with GCC compiler
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1                     // this is required by arcint.h
#include "arcint.h"
#include "../../utility/debug/debug.h"
#define  LoadLibrary(lib)                 dlopen(lib, RTLD_NOW)
#define  GetProcAddress(handle, proc)     dlsym(handle, proc)
#define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#endif // !__LINUX

// must be included before other headers
#include "interface.h"
#include "ml_riscv.h"      
#include "riscvsetup.h"    

#define JTAG_SYSTEM_MODE      0x7

#ifdef __LINUX
// __LINUX
void __attribute__((constructor)) my_load(void);
void __attribute__((destructor))  my_unload(void);
#else
// Windows specific
extern "C" BOOL APIENTRY DllMain(HINSTANCE hInstDll, DWORD dwReason, LPVOID lpReserved);
#endif

extern "C" ASH_DLL_EXPORT struct TyRISCVInstance * get_RISCV_interface(void);
extern "C" ASH_DLL_EXPORT struct TyRISCVInstance * get_RISCV_interface_ex(PFRISCVSetupFunction pfExternalRISCVSetup);
extern "C" ASH_DLL_EXPORT void ASH_ListConnectedProbes(char ppszInstanceNumber[][16], unsigned short usMaxInstances,
	unsigned short *pusConnectedInstances);
extern "C" ASH_DLL_EXPORT const char *ASH_GetInterfaceID(void);
extern "C" ASH_DLL_EXPORT void ASH_SetSetupItem(struct TyRISCVInstance *ptyPtr, const char *pszSection, const char *pszItem, const char *pszValue);
extern "C" ASH_DLL_EXPORT void ASH_GetSetupItem(struct TyRISCVInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue);
extern "C" ASH_DLL_EXPORT void ASH_GetLastErrorMessage(struct TyRISCVInstance *p, char *pszMessage, unsigned int uiSize);
extern "C" ASH_DLL_EXPORT int ASH_TestScanchain(struct TyRISCVInstance *p);

extern "C" ASH_DLL_EXPORT int ASH_AutoDetectScanChain(struct TyRISCVInstance *p);
extern "C" ASH_DLL_EXPORT int ASH_DetectScanchain(struct TyRISCVInstance *p, unsigned int uiMaxCores, unsigned int *puiNumberOfCores,
	uint32_t *pulIRWidth, unsigned char *pbRISCVCores);
extern "C" ASH_DLL_EXPORT void ASH_UpdateScanchain(struct TyRISCVInstance *p);
extern "C" ASH_DLL_EXPORT void ASH_SetGeneralMessagePrint(PfPrintFunc pfFunc);
extern "C" ASH_DLL_EXPORT int ASH_GDBScanIR(struct TyRISCVInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
extern "C" ASH_DLL_EXPORT int ASH_GDBScanDR(struct TyRISCVInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
extern "C" ASH_DLL_EXPORT int ASH_ScanIR(struct TyRISCVInstance *p, int iLength, uint32_t *pulDataIn, uint32_t *pulDataOut,
	unsigned char ucStartState, unsigned char ucEndState);
extern "C" ASH_DLL_EXPORT int ASH_ScanDR(struct TyRISCVInstance *p, int iLength, uint32_t *pulDataIn, uint32_t *pulDataOut,
	unsigned char ucStartState, unsigned char ucEndState);
extern "C" ASH_DLL_EXPORT int ASH_TMSReset(struct TyRISCVInstance *p);
extern "C" ASH_DLL_EXPORT int ASH_TRSTReset(struct TyRISCVInstance *p);

// interface function prototypes
extern "C" int Halt(struct TyRISCVInstance *p,unsigned short usTapId, uint32_t ulHartId,uint32_t ulAbits);
extern "C" int StatusProc(struct TyRISCVInstance *p, unsigned short usTapId, unsigned short usCoreId, uint32_t ulHartId, unsigned char *pucGlobalStatus, unsigned char *pucStopProblem,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int Run(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAbits);
extern "C" int Discovery(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t *pulDiscoveryData,uint32_t ulHartNo);
extern "C" int Read_Registers(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int Write_Registers(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int Write_A_Reg(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int Read_A_Reg(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int WriteWord(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int ReadWord(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int WriteDPC(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulValue,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int WriteMem(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int ReadMem(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int Step(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int AddTrigger(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerAddr, uint32_t ulTriggerNum, uint32_t *ulTriggerOut,uint32_t ulType,uint32_t ulArchSize, uint32_t ulAbits, uint32_t ulLength,uint32_t ulMatch,uint32_t ulResrequired);
extern "C" int RemoveTrigger(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerNum,uint32_t ulArchSize, uint32_t ulAbits,uint32_t ulResrequired);
extern "C" int TriggerInfo(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulTriggerInfo,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int process_property(struct TyRISCVInstance *p, const char *property, const char *value);
extern "C" int WriteCsr(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegisterValue, uint32_t ulRegisterNumber,uint32_t ulArchSize, uint32_t ulAbits);
extern "C" int Riscv_Mechanism(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulRegMec, uint32_t ulMemMec, uint32_t ulCsrSupp,uint32_t ulFloatingProc,uint32_t ulFence_i);
extern "C" int TargetReset(struct TyRISCVInstance *p, uint32_t ulHartId);
extern "C" int ConfigureVega(struct TyRISCVInstance *p, unsigned short usTapId, unsigned int uiInstrtoSelectADI, unsigned int uiCore, unsigned int uiIRLen);
extern "C" int Discover_Harts(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t *pulNumberofHarts);
extern "C" void receive_callback(struct TyRISCVInstance *p, RISCV_callback *callback);

//local variables
static PfPrintFunc pfGeneralPrintFunc = NULL;
static int iInterfaces = 0;

//local function prototype
void StoreINIFile(struct TyRISCVInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename);
int  LoadINIFile(struct TyRISCVInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename);
void GENERAL_MESSAGE_PRINT(const char *pszMessage);
static void SetCurrentTap(struct TyRISCVInstance *p);
void TRACE_PRINT(struct TyRISCVInstance *p, const char * strToFormat, ...);
void ERROR_PRINT(struct TyRISCVInstance *p, const char * strToFormat, ...);
static char pszTempMessage[255];
void ShowProbeInformation(struct TyRISCVInstance *p, unsigned char bToDebugger);
static void ConvertVersionDate(uint32_t ulValue, unsigned char bVersionFormat, char *pszBuffer, unsigned int uiSize);
void MESSAGE_PRINT(struct TyRISCVInstance *p, const char * strToFormat, ...);
void GENERAL_PRINTF(const char * strToFormat, ...);
static void SetupPrintMessage(struct TyRISCVInstance *p, const char *pszMessage, int iSetupPrint);
int ShowRISCVSetupWindowFromDll(struct TyRISCVInstance *p);
int ConvertMLErrorToGDBErrors(struct TyRISCVInstance *p,int mlError);
//void TRACE_PRINT(struct TyArcInstance *p, const char * strToFormat, ...);

#ifndef __LINUX
/****************************************************************************
Function: DllMain
Engineer: Ashutosh Garg
Input: HINSTANCE hInstDll - DLL handle
DWORD dwReason - reason whay this function was called
LPVOID lpReserved - reserved
Output: BOOL - TRUE if everythink is ok
Description: DLL main entry point, called when DLL is loaded/unloaded
Date           Initials    Description
08-Oct-2007       AG       Initial
****************************************************************************/
extern "C" BOOL APIENTRY DllMain(HINSTANCE hInstDll, DWORD dwReason, LPVOID lpReserved)
{
	lpReserved = lpReserved;         // make lint happy
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
	{  // loading DLL, this is time to get current path
		char pszLocDllPath[_MAX_PATH];
		// get DLL path name (without opxdarc.dll)
		if (!GetModuleFileName(hInstDll,pszLocDllPath,_MAX_PATH))
			ML_SetDllPath("");
		else
			ML_SetDllPath(pszLocDllPath);
		pfGeneralPrintFunc = NULL;
	}
	break;
	case DLL_PROCESS_DETACH:
		pfGeneralPrintFunc = NULL;
		if (ptyMLHandle != NULL)
		{  // close probe connection
			(void)ML_CloseConnection(ptyMLHandle);
			//FREE(ptyMLHandle);
		}
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	default:
		break;
	}
	return TRUE;
}
#else
/****************************************************************************
Function: Sleep
Engineer: Nikolay Chokoev
Input: mseconds
Output: none
Description:
Date           Initials    Description
18-Dec-2007    NCH         Initial
****************************************************************************/
void Sleep(unsigned int  mseconds)
{
	// Windows Sleep uses miliseconds
	// linux usleep uses microsecond
	usleep(mseconds * 1000);
}

// This function will be called when the library is loaded and before dlopen() returns
void my_load(void)
{
#define SIZE 260
	FILE *f;
	char line[SIZE];
	char* pszPath;


	f = fopen("/proc/self/maps", "r");

	while (!feof(f))
	{
		if (fgets(line, SIZE, f) == NULL)
			break;

		// Sanity check. 
		if (strstr(line, "opxdarc.so") == NULL)
			continue;

		pszPath = strchr(line, '/');

		ML_SetDllPath(pszPath);
	}

	fclose(f);

	// Add initialization code�
}

// This function will be called when the library is unloaded and before dlclose() returns
void my_unload(void)
{
	// Add clean-up code�
}
#endif
/****************************************************************************
Function: ShowRISCVSetupWindowFromDll
Engineer:
Input: struct TyRISCVInstance *p - pointer to instance structure
Output: int - TRUE if new setting is valid, FALSE to cancel
Description: show RISCV setup dialog window in GUI, called from external DLL
Date           Initials    Description
15-Oct-2007    VH          Initial
26-Feb-2008    NCH
****************************************************************************/
int ShowRISCVSetupWindowFromDll(struct TyRISCVInstance *p)
{
	char                pszDllPath[_MAX_PATH];
	int                 iRetValue = FALSE;

	TyRISCVSetupInterface tyRISCVSetupInterface;
	PFRISCVSetupFunction  pfRISCVSetupWindow = NULL;
	HINSTANCE           hARCGuiDll = NULL;

	ML_GetDllPath(pszDllPath, _MAX_PATH);
	if (!strcmp(pszDllPath, ""))
		strcpy(pszDllPath, RISCVGUI_DLL);
	else
	{
#ifdef __LINUX
		strcat(pszDllPath, "/");
#else
		strcat(pszDllPath, "\\");
#endif
		strcat(pszDllPath, RISCVGUI_DLL);
	}
	p = p;
	hARCGuiDll = LoadLibrary(pszDllPath);
	if (hARCGuiDll == NULL)
		return FALSE;                             // cannot load DLL
	// get pointer to function
	pfRISCVSetupWindow = (PFRISCVSetupFunction)GetProcAddress(hARCGuiDll, RISCVGUISETUPDLG_NAME);
	// call function
	if (pfRISCVSetupWindow != NULL)
	{
		if (pfRISCVSetupWindow((void*)p, &tyRISCVSetupInterface))
			iRetValue = TRUE;
	}
	(void)FreeLibrary(hARCGuiDll);
	return iRetValue;
}

/****************************************************************************
Function: CallExternalRISCVSetup
Engineer: 
Input: struct TyRISCVInstance *p - pointer to instance structure
PFRISCVSetupFunction pfExternalRISCVSetup - external callback function
Output: int - TRUE if new setting is valid, FALSE to cancel
Description: call external RISCV setup callback function
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/
int CallExternalRISCVSetup(struct TyRISCVInstance *p, PFRISCVSetupFunction pfExternalRISCVSetup)
{
	TyRISCVSetupInterface tyRISCVSetupInterface;
	int iRetValue = FALSE;

	p = p;
	// check pointer to external callback
	if (pfExternalRISCVSetup == NULL)
		return FALSE;
	// prepare structure with local pointers
	tyRISCVSetupInterface.pfGetID = ASH_GetInterfaceID;
	tyRISCVSetupInterface.pfListConnectedProbes = ASH_ListConnectedProbes;
	tyRISCVSetupInterface.pfGetSetupItem = (PFAshGetSetupItem)ASH_GetSetupItem;
	tyRISCVSetupInterface.pfSetSetupItem = (PFAshSetSetupItem)ASH_SetSetupItem;
	
	// call callback with prepared structure
	if (pfExternalRISCVSetup((void*)p, &tyRISCVSetupInterface))
		iRetValue = TRUE;
	return iRetValue;
}
/****************************************************************************
Function: get_RISCV_interface
Engineer: 
Input: *******************
Output: * As per RISCVINT.H *
Description: *******************
Initial function called by MetaWare debugger.
Must return prepared structure with debugger interface functions.
This function MUST be exported from DLL so debugger can call it.
Date           Initials    Description
10-Oct-2007    VH          Initial
04-Dec-2007    VH          Modified to use extended function
****************************************************************************/
extern "C" ASH_DLL_EXPORT struct TyRISCVInstance * get_RISCV_interface(void)
{
	// just call externded function with NULL parameter
	return get_RISCV_interface_ex(NULL);
}


/****************************************************************************
Function: get_RISCV_interface_ex
Engineer: 
Input: NULL
Output: struct TyArcInstance * - pointer to allocated debug structure
Description: Initial function called by MetaWare debugger.
Must return prepared structure with debugger interface functions.
This function MUST be exported from DLL so debugger can call it.
Date           Initials    Description

****************************************************************************/
extern "C" ASH_DLL_EXPORT struct TyRISCVInstance * get_RISCV_interface_ex(PFRISCVSetupFunction pfExternalRISCVSetup)
{
	int iRes;
	int iError = 0;
	TyRISCVTargetParams *ptyLocMLHandle = NULL;
	TyFunctionTable *ptyLocFunctionTable = NULL;
	struct TyRISCVInstance *ptyLocRISCVInstance = NULL;
	char ppszLocInstanceNumber[16][16];
	unsigned short usConnectedOpellaXD;
	unsigned char bConnectionOk = 0;
	//flag if target is not connected to opella-Xd or target is not powered on
	//unsigned char bTargetPowered = 0;

	//ensure no device handle is assigned
	//ptyMLHandle = NULL;

	// first allocate memory for all dynamic structures and check if available
	ptyLocRISCVInstance = (struct TyRISCVInstance*)malloc(sizeof(struct TyRISCVInstance));
	ptyLocFunctionTable = (TyFunctionTable *)malloc(sizeof(TyFunctionTable));
	if ((ptyLocRISCVInstance == NULL) || (ptyLocFunctionTable == NULL))
	{  // we can do anything but return with error
		if (ptyLocRISCVInstance != NULL)
			FREE(ptyLocRISCVInstance);
		if (ptyLocFunctionTable != NULL)
			FREE(ptyLocFunctionTable);
		return NULL;
	}
	// clear all allocated structures just for sure
	(void)memset(ptyLocRISCVInstance, 0, sizeof(struct TyRISCVInstance));
	(void)memset(ptyLocFunctionTable, 0, sizeof(TyFunctionTable));

	// allocation of all structures was successful so link them together
	ptyLocRISCVInstance->ptyFunctionTable = ptyLocFunctionTable;
	// now fill pointers to function table

	ptyLocRISCVInstance->ptyFunctionTable->Halt = Halt;
	ptyLocRISCVInstance->ptyFunctionTable->StatusProc = StatusProc;
	ptyLocRISCVInstance->ptyFunctionTable->Run = Run;
	ptyLocRISCVInstance->ptyFunctionTable->Discovery = Discovery;
	ptyLocRISCVInstance->ptyFunctionTable->read_reg = Read_Registers;
	ptyLocRISCVInstance->ptyFunctionTable->write_reg = Write_Registers;
	ptyLocRISCVInstance->ptyFunctionTable->write_a_reg = Write_A_Reg;
	ptyLocRISCVInstance->ptyFunctionTable->read_a_reg = Read_A_Reg;
	ptyLocRISCVInstance->ptyFunctionTable->writeword = WriteWord;
	ptyLocRISCVInstance->ptyFunctionTable->readword = ReadWord;
	ptyLocRISCVInstance->ptyFunctionTable->writedpc = WriteDPC;
	ptyLocRISCVInstance->ptyFunctionTable->read_memory = ReadMem;
	ptyLocRISCVInstance->ptyFunctionTable->write_memory = WriteMem;
	ptyLocRISCVInstance->ptyFunctionTable->step = Step;
	ptyLocRISCVInstance->ptyFunctionTable->addtrigger = AddTrigger;
	ptyLocRISCVInstance->ptyFunctionTable->removetrigger = RemoveTrigger;
	ptyLocRISCVInstance->ptyFunctionTable->process_property = process_property;
	ptyLocRISCVInstance->ptyFunctionTable->TriggerInfo = TriggerInfo;
	ptyLocRISCVInstance->ptyFunctionTable->configure_vega = ConfigureVega;
	ptyLocRISCVInstance->ptyFunctionTable->receive_callback = receive_callback;
	ptyLocRISCVInstance->ptyFunctionTable->riscv_mechanism = Riscv_Mechanism;
	ptyLocRISCVInstance->ptyFunctionTable->target_reset = TargetReset;
	ptyLocRISCVInstance->ptyFunctionTable->writecsr = WriteCsr;
	ptyLocRISCVInstance->ptyFunctionTable->Discover_Harts = Discover_Harts;
	
	ptyLocRISCVInstance->uiPMVersion = 0;
	// default memory size as max
	ptyLocRISCVInstance->ulMemorySize = 0xFFFFFFFF;
	ptyLocRISCVInstance->bTraceMessagesEnabled = FALSE;
	ptyLocRISCVInstance->usTapNumber = 0;
	ptyLocRISCVInstance->uiCoreNumber = 0;
	ptyLocRISCVInstance->ulHartId = 0;
	ptyLocRISCVInstance->pcbCallbackFunction = NULL;


	// GUI used only when not using external setup callback
	
	if (pfExternalRISCVSetup == NULL)
		ptyLocRISCVInstance->bUseGUI = TRUE;
	else
		ptyLocRISCVInstance->bUseGUI = FALSE;

	ptyLocRISCVInstance->bStartupInfoShown = FALSE;          //starting message has been shown or not setting it to false

	// assuming this interface belongs to core 1.
	ptyLocRISCVInstance->uiCoreNumber = 1;
	// let use simple option, take first connected Opella-XD in the list and try to use it
	bConnectionOk = 0;
	usConnectedOpellaXD = 0;

	//assuming the Target is connected to opella-XD
	//bTargetPowered = 1;

	// check if it is the first interface to be opened
	if (iInterfaces == 0)
	{
		// Prepare pointer to ML handle for target params
		ptyLocMLHandle = (TyRISCVTargetParams *)malloc(sizeof(TyRISCVTargetParams));
		if (ptyLocMLHandle == NULL)
		{// we can do anything but return with error
			FREE(ptyLocRISCVInstance);
			FREE(ptyLocFunctionTable);
			return NULL;
		}
		(void)memset(ptyLocMLHandle, 0, sizeof(TyRISCVTargetParams));
		ptyMLHandle = ptyLocMLHandle;

		// get connected Opella-XD and pick first
		ML_GetListOfConnectedProbes(ppszLocInstanceNumber, 16, &usConnectedOpellaXD);   //char array[16][16] and unsigned short int.
		if (usConnectedOpellaXD) {
			strncpy(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0], 15);
			ptyMLHandle->pszProbeInstanceNumber[15] = 0;
		} else {
			FREE(ptyLocRISCVInstance);
			FREE(ptyLocFunctionTable);
            FREE(ptyLocMLHandle);
            ptyMLHandle = NULL;
			return NULL;
		}

		// ini file location
		if (pszIniFileName == NULL)
		{
			memset(ptyLocRISCVInstance->pszIniFileLocation, 0, 256);
			ML_GetDllPath(ptyLocRISCVInstance->pszIniFileLocation, _MAX_PATH);
			strncat(ptyLocRISCVInstance->pszIniFileLocation, "/", _MAX_PATH - strlen(ptyLocRISCVInstance->pszIniFileLocation));
			strncat(ptyLocRISCVInstance->pszIniFileLocation, RISCV_INI_DEFAULT_NAME, _MAX_PATH - strlen(ptyLocRISCVInstance->pszIniFileLocation));
		}
		else
			strcpy(ptyLocRISCVInstance->pszIniFileLocation, pszIniFileName);

		// now it is time to initialize probe settings
		// note we do not bother with default values since it will be set in ML_OpenConnection
		// we need only specify Opella-XD instance number and connection flag should be 0
		ptyMLHandle->bConnectionInitialized = 0;
		// select ARC target type
		ptyMLHandle->tyRISCVTargetType = ARC_TGT_JTAG_TPA_R1;        // starting with ARC normal TPA rev. 0
		ptyMLHandle->bAdaptiveClockEnabled = FALSE;                 // no adaptive clock
		ptyMLHandle->bDoNotIssueTAPReset = FALSE;                     // TAP reset
		// define muticore structure
		ptyMLHandle->ulNumberOfTapsOnScanchain = 1;     // 1 TAP on scanchain
		ptyMLHandle->tyScanchainTapParams[0].bRISCVTap = TRUE;  // it is RISCV core
		ptyMLHandle->tyScanchainTapParams[0].ulIRLength = 5;   // IR length
		ptyMLHandle->tyScanchainTapParams[0].ulBypassInst = 0x0000001f;	   //bypass instruction
		ptyMLHandle->ulHartId = 1;

		// now we need to get ARC setup, using either external GUI DLL 
		// or external setup callback. INI file is used in both cases.
		(void)LoadINIFile(ptyLocRISCVInstance, RISCV_INI_ENV_VARIABLE, RISCV_INI_DEFAULT_NAME);

		// setup only from the first interface
		if (pfExternalRISCVSetup != NULL)
		{
			if (!CallExternalRISCVSetup(ptyLocRISCVInstance, pfExternalRISCVSetup))
			{ // ARC setup selection via external callback
				FREE(ptyLocRISCVInstance);
				FREE(ptyLocFunctionTable);
				FREE(ptyLocMLHandle);
                ptyMLHandle = NULL;
				return NULL;
			}
		}
		else
		{
			unsigned char ucIndex;

			for (ucIndex = 0; ucIndex < 16; ucIndex++)
			{
				if (strncmp(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[ucIndex], 15) == 0)
					break; //we have a match 
			}

			if ((ucIndex >= 16)
				&& (usConnectedOpellaXD == 1))
			{
				sprintf(pszTempMessage,
					"OPXDARC.INI configuration file uses Opella-XD serial number:%s which is not connected. Continuing with Opella-XD serial number:%s.\n",
					ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0]);
				GENERAL_MESSAGE_PRINT(pszTempMessage);
				SetupPrintMessage(NULL, pszTempMessage, 0); //Save message
				strncpy(ptyMLHandle->pszProbeInstanceNumber, ppszLocInstanceNumber[0], 15);
				ptyMLHandle->pszProbeInstanceNumber[15] = 0;
			}

			if (ptyLocRISCVInstance->bUseGUI)
			{ // call GUI (supported only for Windows when not using external callback)
				if (!ShowRISCVSetupWindowFromDll(ptyLocRISCVInstance))
				{ // ARC setup selection has been canceled, so cancel whole initialization
					FREE(ptyLocRISCVInstance);
					FREE(ptyLocFunctionTable);
					FREE(ptyLocMLHandle);
                    ptyMLHandle = NULL;
					return NULL;
				}
			}
		}

		// try to store current configuration into file for next session
		StoreINIFile(ptyLocRISCVInstance, RISCV_INI_ENV_VARIABLE, RISCV_INI_DEFAULT_NAME);
		// first check probe firmware
		// now there must be valid Opella-XD serial number, so try opening connection
		iRes = ML_UpgradeFirmware(ptyMLHandle, GENERAL_MESSAGE_PRINT);
		if ((iRes == ERR_ML_RISCV_NO_ERROR) ||
			(iRes == ERR_ML_RISCV_ALREADY_INITIALIZED))
		{
			/* Added to solve the reconnect issue in some machine if Auto-Scan button is not clicked.*/ 
			for (int i = 0; i < 10; i++)
			{
				iRes = ML_OpenConnection(ptyMLHandle);
				if ((iRes == ERR_ML_RISCV_NO_ERROR) ||
					(iRes == ERR_ML_RISCV_ALREADY_INITIALIZED))
					bConnectionOk = 1;
				//adde if the target is not connected to opella-XD or target is not powered on
				else if (iRes == ERR_ML_RISCV_DEBUG_POFF)
				{
					//bTargetPowered = 0;
					GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));
					break;
				}
				else
				{
					GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));
				}

				if (bConnectionOk == 1)
				{
					break;
				}
			}
		}
		else
			GENERAL_MESSAGE_PRINT(ML_GetErrorMessage(iRes));

		// check if connection is ok
		if (!bConnectionOk)
		{  // there is no probe or error occured when openning connection
			FREE(ptyLocRISCVInstance);
			FREE(ptyLocFunctionTable);
			FREE(ptyLocMLHandle);
            ptyMLHandle = NULL;
			return NULL;
		}
	}

	//    if (iInterfaces+1 > (int)ptyMLHandle->ulNumberOfCoresOnScanchain)
	//       {  // somebody try to open more interfaces than the cores in a chain
	//       free(ptyLocArcInstance);
	//       free(ptyLocFunctionTable);
	//       // we are going to close the only interface
	//       if((ptyLocMLHandle!=NULL) && (iInterfaces < 1))
	//          free(ptyLocMLHandle);
	//       return NULL;
	//       }

	// Interface successfuly created. Increase interface number
	iInterfaces++;
	//if (!ptyMLHandle->bDoNotIssueTAPReset)
	{
		iError = ML_ResetTAP(ptyMLHandle, 0);
		if (iError != ERR_ML_RISCV_NO_ERROR)
			return ASH_FALSE;
	}
	return ptyLocRISCVInstance;
}

/****************************************************************************
Function: StoreINIFile
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to instance structure
const char *pszEnvVarName - name of optional environment variable with INI path
const char *pszDefaultIniFilename - default name of INI file
Output: none
Description: store ARC setup configuration into INI file
Date           Initials    Description
17-Oct-2007    VH          Initial
09-Sep-2009    RSB         TAP reset selection stored to ini file
****************************************************************************/
void StoreINIFile(struct TyRISCVInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename)
{
	int   iTapIndex;
	char  pszValue[100];
	char *pszEnvVarValue;
	FILE *pIniFile;

	if ((p == NULL) || (pszEnvVarName == NULL) || (pszDefaultIniFilename == NULL))
		return;

	// check if environment variable exists and possibly use it as INI filename, try to open INI file
	pszEnvVarValue = getenv(pszEnvVarName);
	if (pszEnvVarValue != NULL)
		strcpy(p->pszIniFileLocation, pszEnvVarValue);

	pIniFile = fopen(p->pszIniFileLocation, "wt");

	if (pIniFile == NULL)
		return;

	// writing target configuration section
	fprintf(pIniFile, "[%s]\n", RISCV_SETUP_SECT_TARGETCONFIG);                                 // section name
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, pszValue);
	if (strcmp(pszValue, ""))
		fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_TARGETOPTION, pszValue);                   // store target selection
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_ADAPTIVECLOCK, pszValue);
	if (strcmp(pszValue, ""))
		fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_ADAPTIVECLOCK, pszValue);                  // store adaptive clock selection
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TAPRESET, pszValue);
	if (strcmp(pszValue, ""))
		fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_TAPRESET, pszValue);                  // store TAP reset selection

	// writing misc options
	fprintf(pIniFile, "[%s]\n", RISCV_SETUP_SECT_MISCOPTIONS);                                  // section name
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_MISCOPTIONS, RISCV_SETUP_ITEM_SHOWCONFIG, pszValue);
	if (strcmp(pszValue, ""))
		fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_SHOWCONFIG, pszValue);                     // using GUI option
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_MISCOPTIONS, RISCV_SETUP_ITEM_PROBEINSTANCE, pszValue);
	if (strcmp(pszValue, ""))
		fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_PROBEINSTANCE, pszValue);                  // probe instance
	// writing multicore section
	fprintf(pIniFile, "[%s]\n", RISCV_SETUP_SECT_MULTICORE);                                    // section name
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_MULTICORE, RISCV_SETUP_ITEM_NUMBEROFTAPS, pszValue);
	if (strcmp(pszValue, ""))
		fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_NUMBEROFTAPS, pszValue);                  // using GUI option
	// write information about each core
	for (iTapIndex = 0; iTapIndex < (int)ptyMLHandle->ulNumberOfTapsOnScanchain; iTapIndex++)
	{
		char pszSect[20];
		sprintf(pszSect, "%s%d", RISCV_SETUP_SECT_DEVICE, iTapIndex + 1);
		fprintf(pIniFile, "[%s]\n", pszSect);
		ASH_GetSetupItem(p, pszSect, RISCV_SETUP_ITEM_RISCVCORE, pszValue);
		if (strcmp(pszValue, ""))
			fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_RISCVCORE, pszValue);                     // is it ARC core ?
		ASH_GetSetupItem(p, pszSect, RISCV_SETUP_ITEM_IRWIDTH, pszValue);
		if (strcmp(pszValue, ""))
			fprintf(pIniFile, "%s=%s\n", RISCV_SETUP_ITEM_IRWIDTH, pszValue);                     // IR length
	}

	fclose(pIniFile);                         // close INI file
}

/****************************************************************************
Function: LoadINIFile
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p           - pointer to instance structure
const char *pszEnvVarName         - name of optional environment variable with INI path
const char *pszDefaultIniFilename - default name of INI file
const char *pszIniFileLocation    - location of INI file
Output: none
Description: load ARC setup configuration from INI file
Date           Initials    Description
17-Oct-2007    VH          Initial
18-Jan-2017    AS          Added INI file location
****************************************************************************/
int LoadINIFile(struct TyRISCVInstance *p, const char *pszEnvVarName, const char *pszDefaultIniFilename)
{
	FILE *pIniFile;
	char *pszEnvVarValue = NULL;
	char  pszCurrentSection[32];
	char  pszIniLine[_MAX_PATH];

	if ((p == NULL) || (pszDefaultIniFilename == NULL))
		return -1;

	// check if environment variable exists and possibly use it as INI filename, try to open INI file
	if (pszEnvVarName != NULL)
		pszEnvVarValue = getenv(pszEnvVarName);

	if (pszEnvVarValue != NULL)
		strcpy(p->pszIniFileLocation, pszEnvVarValue);

	pIniFile = fopen(p->pszIniFileLocation, "rt");// we should use default name in same directory as DLL

	if (pIniFile == NULL)
		return -1;

	strcpy(pszCurrentSection, "");

	// read INI file line by line
	while (fgets(pszIniLine, _MAX_PATH, pIniFile) != NULL)
	{  // got line so try to parse it, first we need to remove any '\r' and '\n' characters (just strap them)
		char *pszLabelEnd = NULL;

		pszLabelEnd = strchr(pszIniLine, '\n');
		if (pszLabelEnd != NULL)
			*pszLabelEnd = 0;
		pszLabelEnd = strchr(pszIniLine, '\r');
		if (pszLabelEnd != NULL)
			*pszLabelEnd = 0;
		if ((pszIniLine[0] == '[') && (strchr(pszIniLine, ']') != NULL))
		{  // got section label
			pszLabelEnd = strchr(pszIniLine, ']');
			if (pszLabelEnd != NULL)
				*pszLabelEnd = 0;                                           // cut end of label
			strncpy(pszCurrentSection, &(pszIniLine[1]), 32);                // copy label name into local 
			pszCurrentSection[31] = 0;
		}
		else if (strchr(pszIniLine, '=') != NULL)
		{  // got variable assignment
			pszLabelEnd = strchr(pszIniLine, '=');
			if (pszLabelEnd != NULL)
			{
				char *pszValue;
				pszValue = pszLabelEnd + 1;                                    // value after '=' character to the end of line
				*pszLabelEnd = 0;                                              // cut '=' from string
				ASH_SetSetupItem(p, pszCurrentSection, pszIniLine, pszValue);   // leave parsing to AshSetSetupItem, just call it
			}
		}
	}

	fclose(pIniFile);

	return 1;
}

/****************************************************************************
Function: GENERAL_MESSAGE_PRINT
Engineer: Vitezslav Hola
Input: const char *pszMessage - message to print
Output: None
Description: general message print
Date           Initials    Description
31-Oct-2007    VH          Initial
****************************************************************************/
void GENERAL_MESSAGE_PRINT(const char *pszMessage)
{
	// call general print function
	if (pfGeneralPrintFunc == NULL){
		fprintf(stdout, pszMessage);
		fflush(stdout);
	} else {
		pfGeneralPrintFunc(pszMessage);
	}
}

/****************************************************************************
Function: ASH_ListConnectedProbes
Engineer: Vitezslav Hola
Input: char ppszInstanceNumber[][16] - buffer for list of connected instances
unsigned short usMaxInstances - maximum number of instances in list
unsigned short *pusConnectedInstances - current number of connected instances
Output: none
Description: Get list with serial numbers of connected instances of Opella-XD probe.
Caller should provide buffer for list and specify maximum number of instance for the list.
This function detects number of connected probes and fills list with their serial numbers.
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_ListConnectedProbes(char ppszInstanceNumber[][16], unsigned short usMaxInstances,
	unsigned short *pusConnectedInstances)
{
	// check connected instances of probe (Opella-XD)
	ML_GetListOfConnectedProbes(ppszInstanceNumber, usMaxInstances, pusConnectedInstances);
}

/****************************************************************************
Function: ASH_GetInterfaceID
Engineer: Vitezslav Hola
Input: None
Output: const char * - string with interface name and version
Description: return string with interface name and version number
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT const char *ASH_GetInterfaceID(void)
{
	return INTERFACE_NAME_AND_VERSION;
}

/****************************************************************************
Function: ASH_SetSetupItem
Engineer: Vitezslav Hola
Input: struct TyArcInstance *ptyPtr - pointer to ARC instance structure
const char *pszSection - string with section name
const char *pszItem - string with item name
const char *pszValue - string with value
Output: none
Description: set ARC setup item specified by section and item name to
particular value in ARC structure (pointer passed as parameter)
Date           Initials    Description
15-Oct-2007    VH          Initial
09-Sep-2009    RSB         TAP reset selection added
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_SetSetupItem(struct TyRISCVInstance *ptyPtr, const char *pszSection, const char *pszItem, const char *pszValue)
{
	if (pszSection == NULL)
		return;

	// check for valid parameters
	if (strcmp(pszSection, RISCV_SETUP_SECT_INIFILE))
	{
		if ((ptyPtr == NULL) || (ptyMLHandle == NULL) ||
			(pszItem == NULL) || (pszValue == NULL))
			return;
	}

	// parse section name
	if (!strcmp(pszSection, RISCV_SETUP_SECT_INIFILE))
		pszIniFileName = (char*)pszValue;
	else if (!strcmp(pszSection, RISCV_SETUP_SECT_TARGETCONFIG))
	{  // this is target configuration section
		if (!strcmp(pszItem, RISCV_SETUP_ITEM_TARGETOPTION))
		{     // check for target options, value must be string with 1 character
			if (strlen(pszValue) == 1)
			{
				
				ptyMLHandle->tyRISCVTargetType = ARC_TGT_JTAG_TPA_R1;
					
			}
		}
		else if (!strcmp(pszItem, RISCV_SETUP_ITEM_ADAPTIVECLOCK))
		{
			if (!strcmp(pszValue, RISCV_SETUP_ITEM_TRUE))
				ptyMLHandle->bAdaptiveClockEnabled = 1;
			else if (!strcmp(pszValue, RISCV_SETUP_ITEM_FALSE))
				ptyMLHandle->bAdaptiveClockEnabled = 0;
		}
		else if (!strcmp(pszItem, RISCV_SETUP_ITEM_TAPRESET))
		{
			if (!strcmp(pszValue, RISCV_SETUP_ITEM_TRUE))
				ptyMLHandle->bDoNotIssueTAPReset = 0;
			else if (!strcmp(pszValue, RISCV_SETUP_ITEM_FALSE))
				ptyMLHandle->bDoNotIssueTAPReset = 1;
			// else do nothing (invalid value)
		}

	}
	else if (!strcmp(pszSection, RISCV_SETUP_SECT_MISCOPTIONS))
	{  // misc section
		if (!strcmp(pszItem, RISCV_SETUP_ITEM_SHOWCONFIG))   // displaying GUI
		{
			if (!strcmp(pszValue, RISCV_SETUP_ITEM_TRUE))
				ptyPtr->bUseGUI = 1;
			else if (!strcmp(pszValue, RISCV_SETUP_ITEM_FALSE))
				ptyPtr->bUseGUI = 0;
		}
		else if (!strcmp(pszItem, RISCV_SETUP_ITEM_PROBEINSTANCE))    // which Opella-XD instance is selected
		{
			strncpy(ptyMLHandle->pszProbeInstanceNumber, pszValue, 16);
			ptyMLHandle->pszProbeInstanceNumber[15] = 0;
		}
	}
	else if (!strcmp(pszSection, RISCV_SETUP_SECT_MULTICORE))
	{  // multicore section
		if (!strcmp(pszItem, RISCV_SETUP_ITEM_NUMBEROFTAPS))
		{
			int iNumber = 0;
			//            if ((sscanf(pszValue, "%d", &iNumber) == 1) && (iNumber >= 1) && (iNumber <= MAX_ARC_CORES))
			//changed with introduction of auto detect scan chain
			if ((sscanf(pszValue, "%d", &iNumber) == 1) && (iNumber <= MAX_RISCV_TAPS))
			{
				ptyMLHandle->ulNumberOfTapsOnScanchain = (uint32_t)iNumber;
			}
		}
	}
	else if (strstr(pszSection, RISCV_SETUP_SECT_DEVICE) == pszSection)
	{  // section about particular core
		int iDeviceNum = 0;
		char *pszNum = NULL;
		pszNum = (char *)&(pszSection[strlen(RISCV_SETUP_SECT_DEVICE)]);      // get pointer to number in string
		if ((sscanf(pszNum, "%d", &iDeviceNum) == 1) && (iDeviceNum >= 1) &&
			(iDeviceNum <= RISCV_SETUP_MAX_SCANCHAIN_CORES))
		{
			iDeviceNum--;
			if (!strcmp(pszItem, RISCV_SETUP_ITEM_RISCVCORE))
			{  // is it ARC core or not
				if (!strcmp(pszValue, RISCV_SETUP_ITEM_TRUE))
					ptyMLHandle->tyScanchainTapParams[iDeviceNum].bRISCVTap = 1;
				else if (!strcmp(pszValue, RISCV_SETUP_ITEM_FALSE))
					ptyMLHandle->tyScanchainTapParams[iDeviceNum].bRISCVTap = 0;
			}
			else if (!strcmp(pszItem, RISCV_SETUP_ITEM_IRWIDTH))
			{
				int iWidth = 0;
				if ((sscanf(pszValue, "%d", &iWidth) == 1) &&
					(iWidth >= 1) &&
					(iWidth <=RISCV_SETUP_MAX_CORE_IR_LENGTH))
					ptyMLHandle->tyScanchainTapParams[iDeviceNum].ulIRLength = (uint32_t)iWidth;
			}
			else if (!strcmp(pszItem, RISCV_SETUP_ITEM_BYPASSINST))
			{
				int BypassIns = 0;
				if ((sscanf(pszValue, "%d", &BypassIns) == 1) &&
					(BypassIns >= 1) &&
					(BypassIns <= RISCV_SETUP_MAX_BYPASS_IN_LENGTH))
					ptyMLHandle->tyScanchainTapParams[iDeviceNum].ulBypassInst = (uint32_t)BypassIns;
			}
		}
	}
}

/****************************************************************************
Function: ASH_GetSetupItem
Engineer: Vitezslav Hola
Input: struct TyArcInstance *ptyPtr - pointer to ARC instance structure
const char *pszSection - string with section name
const char *pszItem - string with item name
const char *pszValue - string with value
Output: none
Description: get ARC setup item specified by section and item name
from ARC structure (pointer passed as parameter)
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_GetSetupItem(struct TyRISCVInstance *ptyPtr, const char *pszSection, const char *pszItem, char *pszValue)
{
	// check for valid parameters
	if (pszValue == NULL)
		return;
	// first copy empty string
	strcpy(pszValue, "");
	if ((ptyPtr == NULL) || (ptyMLHandle == NULL) || (pszSection == NULL) || (pszItem == NULL))
		return;

	// parse section name
	if (!strcmp(pszSection, RISCV_SETUP_SECT_INIFILE))
		strcpy(pszValue, ptyPtr->pszIniFileLocation);
	else if (!strcmp(pszSection, RISCV_SETUP_SECT_TARGETCONFIG))
	{  // this is target configuration section
		if (!strcmp(pszItem, RISCV_SETUP_ITEM_TARGETOPTION))
		{
			                    // so it must be normal ARC target
				strcpy(pszValue, "1");
				
		}
		else if (!strcmp(pszItem, RISCV_SETUP_ITEM_ADAPTIVECLOCK))
		{
			if (ptyMLHandle->bAdaptiveClockEnabled)                // using adaptive clock or not
				strcpy(pszValue, RISCV_SETUP_ITEM_TRUE);
			else
				strcpy(pszValue, RISCV_SETUP_ITEM_FALSE);
		}
		else if (!strcmp(pszItem, RISCV_SETUP_ITEM_TAPRESET))
		{
			if (ptyMLHandle->bDoNotIssueTAPReset)                // using adaptive clock or not
				strcpy(pszValue, RISCV_SETUP_ITEM_TRUE);
			else
				strcpy(pszValue, RISCV_SETUP_ITEM_FALSE);
		}
	}
	else if (!strcmp(pszSection, RISCV_SETUP_SECT_MISCOPTIONS))
	{  // misc section
		if (!strcmp(pszItem, RISCV_SETUP_ITEM_SHOWCONFIG))
		{
			if (ptyPtr->bUseGUI)                                          // showing configuration dialog or not
				strcpy(pszValue, RISCV_SETUP_ITEM_TRUE);
			else
				strcpy(pszValue, RISCV_SETUP_ITEM_FALSE);
		}
		else if (!strcmp(pszItem, RISCV_SETUP_ITEM_PROBEINSTANCE))          // which Opella-XD instance is selected
			strcpy(pszValue, ptyMLHandle->pszProbeInstanceNumber);
	}
	else if (!strcmp(pszSection, RISCV_SETUP_SECT_MULTICORE))
	{  // multicore section
		if (!strcmp(pszItem, RISCV_SETUP_ITEM_NUMBEROFTAPS))
			sprintf(pszValue, "%d", (int)ptyMLHandle->ulNumberOfTapsOnScanchain);
	}
	else if (strstr(pszSection, RISCV_SETUP_SECT_DEVICE) == pszSection)
	{  // section about particular core
		int iDeviceNum = 0;
		char *pszNum = NULL;
		pszNum = (char *)&(pszSection[strlen(RISCV_SETUP_SECT_DEVICE)]);      // get pointer to number in string
		if ((sscanf(pszNum, "%d", &iDeviceNum) == 1) && (iDeviceNum >= 1) && (iDeviceNum <= RISCV_SETUP_MAX_SCANCHAIN_CORES))
		{
			iDeviceNum--;
			if (!strcmp(pszItem, RISCV_SETUP_ITEM_RISCVCORE))
			{
				if (ptyMLHandle->tyScanchainTapParams[iDeviceNum].bRISCVTap)    // is it ARC core or not
					strcpy(pszValue, RISCV_SETUP_ITEM_TRUE);
				else
					strcpy(pszValue, RISCV_SETUP_ITEM_FALSE);
			}
			else if (!strcmp(pszItem, RISCV_SETUP_ITEM_IRWIDTH))
				sprintf(pszValue, "%d", (int)ptyMLHandle->tyScanchainTapParams[iDeviceNum].ulIRLength);
			else if (!strcmp(pszItem, RISCV_SETUP_ITEM_BYPASSINST))
				sprintf(pszValue, "%d", (int)ptyMLHandle->tyScanchainTapParams[iDeviceNum].ulBypassInst);
		}
	}
}

/****************************************************************************
Function: ASH_GetLastErrorMessage
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *pszMessage - pointer to buffer with message
int iSize - number of bytes in buffer
Output: none
Description: get text description of last error
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_GetLastErrorMessage(struct TyRISCVInstance *p, char *pszMessage, unsigned int uiSize)
{
	// validate parameters
	if ((p == NULL) || (pszMessage == NULL) || (uiSize == 0))
		return;
	else
	{
		char *pszLocMsg = ML_GetErrorMessage(ptyMLHandle->iLastErrorCode);            // get message
		strncpy(pszMessage, pszLocMsg, uiSize);
		pszMessage[uiSize - 1] = 0;                                                        // terminate string (just for sure)
	}
}

/****************************************************************************
Function: ASH_TestScanchain
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC structure
Output: int - 1 if test passes, 0 if fails
Description: test scanchain loopback (TDI to TDO)
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_TestScanchain(struct TyRISCVInstance *p)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;

	iRet = ML_TestScanchain(ptyMLHandle, 1);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

extern "C" int Halt(struct TyRISCVInstance *p,unsigned short usTapId, uint32_t ulHartId, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Halting the hart [%lu] on TAP [%hu]", ulHartId, usTapId);
	
	iRet = ML_HaltHart(ptyMLHandle, usTapId, ulHartId, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: Run
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulAbits     : Size of address in DMI 
		Output:	  Error code
		Description: 
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Run(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Run the hart [%lu] on TAP [%hu]",ulHartId, usTapId);

	iRet = ML_RunHart(ptyMLHandle, usTapId, ulHartId, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: StatusProc
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId	: TapId
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  unsigned char *pucGlobalStatus : system status if 1 [halted]/ 0 [Running]
				  unsigned char *pucStopProblem  : cause of system Halt
												   1 - Ebreak
												   2 - Trigger 
												   3 - Debugger Requested
												   4 - step
				  uint32_t ulArchSize  : System Architecture
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Reporting the target Status
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int StatusProc(struct TyRISCVInstance *p, unsigned short usTapId, unsigned short usCoreId,
	uint32_t ulHartId, unsigned char *pucGlobalStatus, unsigned char *pucStopProblem,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Checking the status of hart [%lu] on TAP [%hu]", ulHartId, usTapId);

	iRet = ML_StatusProc(ptyMLHandle, usTapId, usCoreId, ulHartId, pucGlobalStatus, pucStopProblem, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: Discovery
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  uint32_t *pulDiscoveryData : pointer for returning discoverd info
				  uint32_t ulHartId    : HartId
		Output:	  Error code
		Description: Discovery of required parameters for the further processing on target[structure TyDiscoveryData]
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Discovery(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t *pulDiscoveryData,uint32_t ulHartNo)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Discovery of hart [%lu] info ", ulHartNo);

	iRet = ML_Discovery(ptyMLHandle, usTapId, pulDiscoveryData, ulHartNo);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;

}

/****************************************************************************
		Function: Discover_Harts
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  uint32_t *pulNumberofHarts : pointer for number of harts detected on Traget
		Output:	  Error code
		Description: Discovery of number of harts
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Discover_Harts(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t *pulNumberofHarts)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Discovery of harts");

	iRet = ML_Discover_Harts(ptyMLHandle, usTapId, pulNumberofHarts);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: Read_Registers
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *pulRegValues : pointer for returing the GPRS Values
				  uint32_t ulRegArraySize : number of GPRS regisers
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI 
		Output:	  Error code
		Description: read the GPRS values 
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Read_Registers(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Reading the Gprs Values on hart [%lu] on TAP [%hu]",ulHartId, usTapId);

	iRet = ML_Read_Registers(ptyMLHandle, usTapId, ulHartId, pulRegValues, ulRegArraySize, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, "\n");
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: Write_Registers
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *pulRegValues : pointer for GPRS Values to be written
				  uint32_t ulRegArraySize : number of GPRS regisers
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Write the GPRS values
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Write_Registers(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Writing the Gprs Values on hart [%lu] on TAP [%hu]", ulHartId, usTapId);

	iRet = ML_Write_Registers(ptyMLHandle, usTapId, ulHartId, pulRegValues, ulRegArraySize, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, "\n");
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: Write_A_Reg
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegValue : register value 
				  uint32_t ulRegAdress : register number
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Write a specified register
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Write_A_Reg(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAdress,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Writing a register 0X0[%lu] on hart [%lu] on TAP [%hu]", ulRegAdress, ulHartId, usTapId);

	iRet = ML_Write_A_Register(ptyMLHandle, usTapId, ulHartId, ulRegValue, ulRegAdress, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (Register Number: 0x%04x)\n", ulRegAdress);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: Read_A_Reg
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegValue : register value
				  uint32_t ulRegAdress : register number
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Read a specified register
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Read_A_Reg(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAdress,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Reading a register 0X0[%lu] on hart [%lu] on TAP [%hu]", ulRegAdress, ulHartId, usTapId);

	iRet = ML_Read_A_Register(ptyMLHandle, usTapId, ulHartId, ulRegValue, ulRegAdress, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (Register Number: 0x%04x)\n", ulRegAdress);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: WriteWord
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulAddress   : Address of the memory
				  uint32_t *pulData    : Memory Data Word 
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Wrie Memory Word
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int WriteWord(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "writing the memory word on address[%X] on hart [%lu] on TAP [%hu]", ulAddress, ulHartId, usTapId);

	iRet = ML_WriteWord(ptyMLHandle, usTapId, ulHartId, ulAddress, pulData, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (0x%08x)\n", ulAddress);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}
/****************************************************************************
		Function: ReadWord
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulAddress   : Address of the memory
				  uint32_t *pulData    : Memory Data Word
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Read Memory Word
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int ReadWord(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Reading the memory word on address[%X] on hart [%lu] on TAP [%hu]", ulAddress, ulHartId, usTapId);

	iRet = ML_ReadWord(ptyMLHandle, usTapId, ulHartId, ulAddress, pulData, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (0x%08x)\n", ulAddress);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: WriteDPC
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulValue	: PC value 
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Write PC value
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int WriteDPC(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulValue,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "write pc value for hart [%lu] on TAP [%hu]",ulHartId, usTapId);

	iRet = ML_Write_DPC(ptyMLHandle, usTapId, ulHartId, ulValue, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (pc)\n");
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: WriteCsr
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegisterValue : value of CSR to write
				  uint32_t ulRegisterNumber : CSR number
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Write CSR value
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int WriteCsr(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegisterValue, uint32_t ulRegisterNumber,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "write CSR value for hart [%lu] on TAP [%hu]", ulHartId, usTapId);

	iRet = ML_Write_CSR(ptyMLHandle, usTapId, ulHartId, ulRegisterValue, ulRegisterNumber, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (Register Number: 0x%04x)\n", ulRegisterNumber);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: WriteMem
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulSize		: 1 - byte
											  2 - half Word
											  3 - Word
				  uint32_t ulCount		: number of ulSize to be written
				  uint32_t ulAddress	: memory address
				  unsigned char *pulData	: memory data 
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Write Memory byte/halfword/word
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int WriteMem(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "writing the memory on address[%X] on hart [%lu] on TAP [%hu]", ulAddress, ulHartId, usTapId);

	iRet = ML_WriteBlock(ptyMLHandle, usTapId, ulHartId, ulSize, ulCount, ulAddress, pulData, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (0x%08x)\n", ulAddress);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: ReadMem
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulSize		: 1 - byte
											  2 - half Word
											  3 - Word
				  uint32_t ulCount		: number of ulSize to be written
				  uint32_t ulAddress	: memory address
				  unsigned char *pulData	: memory data
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Read Memory byte/halfword/word
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int ReadMem(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	int iGDBRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Read the memory on address[%X] on hart [%lu] on TAP [%hu]", ulAddress, ulHartId, usTapId);

	iRet = ML_ReadBlock(ptyMLHandle, usTapId, ulHartId, ulSize, ulCount, ulAddress, pulData, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
	{
		iGDBRet = ConvertMLErrorToGDBErrors(p, iRet);
		fprintf(stdout, " (0x%08x)\n", ulAddress);
		fflush(stdout);
		return iGDBRet;
	}
	return iRet;
}

/****************************************************************************
		Function: Step
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: stepping 
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Step(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Stepping on hart [%lu] on TAP [%hu]",ulHartId, usTapId);

	iRet = ML_SingleStep(ptyMLHandle, usTapId, ulHartId, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: AddTrigger
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulTriggerAddr : Trigger Address
				  uint32_t ulTriggerNum : Trigger number
				  uint32_t *ulTriggerOut : Trigger response
												0 - added
												2 - already exist
												3 - does not exist 
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Add the trigger on given address
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int AddTrigger(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerAddr, uint32_t ulTriggerNum, uint32_t *ulTriggerOut,uint32_t ulType,uint32_t ulArchSize, uint32_t ulAbits, uint32_t ulLength, uint32_t ulMatch, uint32_t ulResrequired)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Adding Trigger number[%lu] on hart [%lu] on TAP [%hu] at address [%X]", ulTriggerNum,ulHartId, usTapId, ulTriggerAddr);

	iRet = ML_AddTrigger(ptyMLHandle, usTapId, ulHartId, ulTriggerAddr, ulTriggerNum, ulTriggerOut, ulType, ulArchSize, ulAbits,ulLength,ulMatch,ulResrequired);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: RemoveTrigger
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulTriggerNum : Trigger number
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Remove the trigger by trigger number
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int RemoveTrigger(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerNum,uint32_t ulArchSize, uint32_t ulAbits,uint32_t ulResUsed)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Removing Trigger number[%lu] on hart [%lu] on TAP [%hu] ", ulTriggerNum, ulHartId, usTapId);

	iRet = ML_RemoveTrigger(ptyMLHandle, usTapId, ulHartId, ulTriggerNum, ulArchSize, ulAbits, ulResUsed);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: TriggerInfo
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *pulTriggerInfo : pointer for number of triggers
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  Error code
		Description: Detect the number of triggers on target
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int TriggerInfo(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulTriggerInfo,uint32_t ulArchSize, uint32_t ulAbits)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Detecting the number of triggers on hart [%lu] on TAP[%hu] ",ulHartId, usTapId);

	iRet = ML_TriggerInfo(ptyMLHandle, usTapId, ulHartId, pulTriggerInfo, ulArchSize, ulAbits);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: Riscv_Mechanism
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  uint32_t ulRegMec : if !=0 program Buffer
										   else Abstract Register Access
				  uint32_t ulMemMec : 32 - 32 bit system bus
										   64 - 64 bit system bus	
										   2 - program Buffer
										   3 - Abstract Access Memory
				  uint32_t ulCsrSupp : 0 if present/1 if not
				  uint32_t ulFloatingProc : 
				  uint32_t ulFence_i : 1 if present/0 if not
		Output:	  Error code
		Description: initializing the Register and memory access parameters
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
extern "C" int Riscv_Mechanism(struct TyRISCVInstance *p, unsigned short usTapId, uint32_t ulRegMec, uint32_t ulMemMec, uint32_t ulCsrSupp,uint32_t ulFloatingProc,uint32_t ulFence_i)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Setting up Riscv Mechanism");

	iRet = ML_Riscv_Mechanism(ptyMLHandle, usTapId, ulRegMec, ulMemMec, ulCsrSupp, ulFloatingProc, ulFence_i);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}

/****************************************************************************
		Function: TargetReset
		Engineer: HS
		Input:	  struct TyRISCVInstance *p : debugger structure
				  uint32_t ulHartId - hart id of hart to halt. 
		Output:	  Error code
		Description:Reset the RISCV Target.
		Date           Initials    Description
		06-June-2019	HS	           initial
****************************************************************************/
extern "C" int TargetReset(struct TyRISCVInstance *p,uint32_t ulHartId)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Resetting RISC-V target");
	
	iRet = ML_Riscv_TargetReset(ptyMLHandle,ulHartId);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);

	return iRet;
}

/****************************************************************************
		Function: ConfigureVega
		Engineer: 
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned int uiInstrtoSelectADI : 
				  unsigned int uiCore : 
				  unsigned int uiIRLen : 
		Output:	  Error code
		Description: 
		Date           Initials    Description
		10-Dec-2018		            Initial
****************************************************************************/
extern "C" int ConfigureVega(struct TyRISCVInstance *p, unsigned short usTapId, unsigned int uiInstrtoSelectADI, unsigned int uiCore, unsigned int uiIRLen)
{
	int iRet = ERR_RISCV_ERROR;
	if (p == NULL)
		return iRet;
	TRACE_PRINT(p, "Configurig vega board");

	iRet = ML_ConfigureVega(ptyMLHandle, usTapId, uiInstrtoSelectADI, uiCore, uiIRLen);
	if (iRet != ERR_RISCV_NO_ERROR)
		return ConvertMLErrorToGDBErrors(p, iRet);
	return iRet;
}
/****************************************************************************
Function: SetCurrentTap
Engineer: Nikolay Chokoev
Input: struct TyArcInstance *p - debugger structure
Output: none
Description: local function to change the current core
Date           Initials    Description
27-Feb-2008    NCH         Initial
****************************************************************************/
static void SetCurrentTap(struct TyRISCVInstance *p)
{
	(void)ML_SetCurrentTap(ptyMLHandle, (uint32_t)p->usTapNumber);
}

/****************************************************************************
Function: process_property
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Processing special requests from MetaWare debugger using
properties.
Date           Initials    Description
16-Jul-2007    VH          Initial
27-Feb-2008    NCH         Add Multicore
09-Sep-2009    RSB         TAP reset done after Target reset.
09-May-2016    ST          Added BSCI_SystemModeReg property
****************************************************************************/
extern "C" int process_property(struct TyRISCVInstance *p, const char *property, const char *value)
{
	int iRet = ERR_RISCV_ERROR;
	// check property string, check its value and do the action
	if (property != NULL && value != NULL)
	{
		// "jtag_frequency" property
		if (!strcmp(property, "jtag_frequency"))
		{
			double dNewFrequency = 0.0;
			// check if requesting adaptive clock
			if (!ML_ConvertFrequencySelection(value, &dNewFrequency))
			{
				ERROR_PRINT(p, MSG_ML_INVALID_FREQUENCY);
				return iRet;
			}
			else
			{
				iRet = ML_SetJtagFrequency(ptyMLHandle, dNewFrequency);         // set new frequency value
				if (iRet != ERR_ML_RISCV_NO_ERROR)
					ERROR_PRINT(p, ML_GetErrorMessage(iRet));
				return iRet;
			}
		}
		if (!strcmp(property, "probe_info"))
		{     // show information about probe (version, etc.)
			MESSAGE_PRINT(p, "*** %s\n", INTERFACE_NAME_AND_VERSION);               // first show DLL info
			ShowProbeInformation(p, TRUE);
			return ERR_RISCV_NO_ERROR;
		}
		if (!strcmp(property, "reset_target"))
		{
			ML_ResetTAP(ptyMLHandle,1);
			return ERR_RISCV_NO_ERROR;
		}
		// "tapnum" property
		if (!strcmp(property, "tapnum"))
		{     // set tap number for debug session
			unsigned int uiValue = 0;
			if (sscanf(value, "%d", &uiValue) != 1)
				return ERR_RISCV_ERROR;
			p->usTapNumber = uiValue;
			SetCurrentTap(p);
			return ERR_RISCV_NO_ERROR;
		}
	}
	return iRet;
}

/****************************************************************************
Function: ShowProbeInformation
Engineer: Vitezslav Hola
Input: struct TyRISCVInstance *p - pointer to RISCV instance structure
unsigned char bToDebugger - TRUE to print into debugger instance, FALSE to general messages
Output: None
Description: show information about probe (serial number, diskware and firmware version, etc.)
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
void ShowProbeInformation(struct TyRISCVInstance *p, unsigned char bToDebugger)
{
	if ((p == NULL) || (ptyMLHandle == NULL))
		return;
	else
	{
		char pszLocVersion[20];
		char pszLocDate[20];
		// show information about serial number
		if (bToDebugger)
			MESSAGE_PRINT(p, "Opella-XD serial number : %s\n", ptyMLHandle->pszProbeInstanceNumber);
		else
			GENERAL_PRINTF("Opella-XD serial number : %s\n", ptyMLHandle->pszProbeInstanceNumber);
		// show information about firmware
		ConvertVersionDate(ptyMLHandle->tyProbeFirmwareInfo.ulDate, FALSE, pszLocDate, 20);
		ConvertVersionDate(ptyMLHandle->tyProbeFirmwareInfo.ulVersion, TRUE, pszLocVersion, 20);
		if (bToDebugger)
			MESSAGE_PRINT(p, "Opella-XD firmware info : %s, %s\n", pszLocVersion, pszLocDate);
		else
			GENERAL_PRINTF("Opella-XD firmware info : %s, %s\n", pszLocVersion, pszLocDate);
		// show information about diskware
		ConvertVersionDate(ptyMLHandle->tyProbeDiskwareInfo.ulDate, FALSE, pszLocDate, 20);
		ConvertVersionDate(ptyMLHandle->tyProbeDiskwareInfo.ulVersion, TRUE, pszLocVersion, 20);
		if (bToDebugger)
			MESSAGE_PRINT(p, "Opella-XD diskware info : %s, %s\n", pszLocVersion, pszLocDate);
		else
			GENERAL_PRINTF("Opella-XD diskware info : %s, %s\n", pszLocVersion, pszLocDate);
		// show information about FPGAware
		ConvertVersionDate(ptyMLHandle->tyProbeFPGAwareInfo.ulDate, FALSE, pszLocDate, 20);
		ConvertVersionDate(ptyMLHandle->tyProbeFPGAwareInfo.ulVersion, TRUE, pszLocVersion, 20);
		if (bToDebugger)
			MESSAGE_PRINT(p, "Opella-XD FPGAware info : %s, %s\n", pszLocVersion, pszLocDate);
		else
			GENERAL_PRINTF("Opella-XD FPGAware info : %s, %s\n", pszLocVersion, pszLocDate);
	}
}

/****************************************************************************
Function: ConvertVersionDate
Engineer: Vitezslav Hola
Input: uint32_t ulValue - value with version or date
unsigned char bVersionFormat - TRUE to convert version and FALSE to convert date
char *pszBuffer - buffer for converted string
unsigned int uiSize - maximum number of bytes in buffer
Output: none
Description: remove action point
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
static void ConvertVersionDate(uint32_t ulValue, unsigned char bVersionFormat, char *pszBuffer, unsigned int uiSize)
{
	if ((pszBuffer == NULL) || (uiSize == 0))
		return;
	pszBuffer[0] = 0;             // empty string
	if (bVersionFormat)
	{  // converting version into string
		uint32_t ulVer1 = (ulValue & 0xFF000000) >> 24;
		uint32_t ulVer2 = (ulValue & 0x00FF0000) >> 16;
		uint32_t ulVer3 = (ulValue & 0x0000FF00) >> 8;
		uint32_t ulVer4 = (ulValue & 0x000000FF);
		// is it major release
		if (((ulVer4 >= 'A') && (ulVer4 <= 'Z')) || ((ulVer4 >= 'a') && (ulVer4 <= 'z')))
			(void)_snprintf(pszBuffer, uiSize, "v%x.%x.%x-%c", (unsigned int)ulVer1, (unsigned int)ulVer2, (unsigned int)ulVer3, (unsigned char)ulVer4);
		else
			(void)_snprintf(pszBuffer, uiSize, "v%x.%x.%x", (unsigned int)ulVer1, (unsigned int)ulVer2, (unsigned int)ulVer3);
	}
	else
	{  // converting date into string (i.e. 02-Nov-2007)
		char pszLocMonth[16];
		uint32_t ulDate, ulMonth, ulYear;
		ulDate = (ulValue & 0xFF000000) >> 24;
		ulMonth = (ulValue & 0x00FF0000) >> 16;
		ulYear = (ulValue & 0x0000FFFF);
		switch (ulMonth)
		{
		case 0x01:  strcpy(pszLocMonth, "Jan");   break;
		case 0x02:  strcpy(pszLocMonth, "Feb");   break;
		case 0x03:  strcpy(pszLocMonth, "Mar");   break;
		case 0x04:  strcpy(pszLocMonth, "Apr");   break;
		case 0x05:  strcpy(pszLocMonth, "May");   break;
		case 0x06:  strcpy(pszLocMonth, "Jun");   break;
		case 0x07:  strcpy(pszLocMonth, "Jul");   break;
		case 0x08:  strcpy(pszLocMonth, "Aug");   break;
		case 0x09:  strcpy(pszLocMonth, "Sep");   break;
		case 0x10:  strcpy(pszLocMonth, "Oct");   break;
		case 0x11:  strcpy(pszLocMonth, "Nov");   break;
		case 0x12:
		default:    strcpy(pszLocMonth, "Dec");   break;
		}
		(void)_snprintf(pszBuffer, uiSize, "%02x-%s-%04x", (unsigned int)ulDate, pszLocMonth, (unsigned int)ulYear);
	}
	pszBuffer[uiSize - 1] = 0;                         // ensure string is always terminated correctly
}


/****************************************************************************
Function: MESSAGE_PRINT
Engineer: Vitezslav Hola
Input: struct TyRISCVInstance *p - pointer to RISCV instance structure
char *strToFormat - string format + parameters (like printf)
Output: None
Description: The function is called when DLL wants to print normal message
to debugger window
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void MESSAGE_PRINT(struct TyRISCVInstance *p, const char * strToFormat, ...)
{
	if ((p != NULL) && (p->pcbCallbackFunction != NULL))
	{
		char buffer[0x100];
		va_list ArgList;

		va_start(ArgList, strToFormat);
		(void)vsprintf(buffer, strToFormat, ArgList);
		va_end(ArgList);
		(void)p->pcbCallbackFunction->printf(buffer);
	}
}

/****************************************************************************
Function: GENERAL_PRINTF
Engineer: Vitezslav Hola
Input: char *strToFormat - string format + parameters (like printf)
Output: None
Description: Function to print general messages like printf
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
void GENERAL_PRINTF(const char * strToFormat, ...)
{
	char buffer[0x100];
	va_list ArgList;

	va_start(ArgList, strToFormat);
	(void)vsprintf(buffer, strToFormat, ArgList);
	va_end(ArgList);
	GENERAL_MESSAGE_PRINT(buffer);
}

/****************************************************************************
Function: receive_callback
Engineer: Vitezslav Hola
Input: *******************
Output: * As per ARCINT.H *
Description: *******************
Date           Initials    Description
10-Oct-2007    VH          Initial
****************************************************************************/
extern "C" void receive_callback(struct TyRISCVInstance *p, RISCV_callback *callback)
{
	//TRACE_PRINT(p, "receive_callback\n");
	p->pcbCallbackFunction = callback;
	SetupPrintMessage(p, pszTempMessage, 1); //Print if there is a message waiting
}

/****************************************************************************
Function: SetupPrintMessage
Engineer: Nikolay Chokoev
Input: TyArcInstance *p - ARC structure.
Can be NULL if only store the message
pszMessage - Message to print
iSetupPrint - 0 - Save message; 1 - Print the message
Output: none
Description: Save message for later print. The size of the string should be
no more than _MAX_PATH.
Date           Initials    Description
22-Aug-2008    NCH         Initial
****************************************************************************/
static void SetupPrintMessage(struct TyRISCVInstance *p,
	const char *pszMessage,
	int iSetupPrint)
{
	static int bIsMessage = 0;
	static char pszSavedMessage[_MAX_PATH];
	if (iSetupPrint == 0)
	{
		sprintf(pszSavedMessage, "%s", pszMessage);
		bIsMessage = 1; //there is a message
	}

	else if ((p->pcbCallbackFunction != NULL) && (bIsMessage == 1))
	{
		(void)p->pcbCallbackFunction->printf(pszSavedMessage);
		bIsMessage = 0; // there is no a message to display anymore
	}
}

/****************************************************************************
Function: ASH_UpdateScanchain
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC structure
Output: none
Description: update scanchain settings in Opella-XD
Date           Initials    Description
05-Nov-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_UpdateScanchain(struct TyRISCVInstance *p)
{
	if (p == NULL)
		return;
	(void)ML_UpdateScanchain(ptyMLHandle);
}

/****************************************************************************
Function: ASH_SetGeneralMessagePrint
Engineer: Vitezslav Hola
Input: PfPrintfFunc pfFunc - pointer to printf-like function
Output: none
Description: set pointer for general message print function
if pfFunc is NULL, default printf will be used
Date           Initials    Description
31-Oct-2007    VH          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT void ASH_SetGeneralMessagePrint(PfPrintFunc pfFunc)
{
	pfGeneralPrintFunc = pfFunc;
}

/****************************************************************************
Function: ASH_AutoDetectScanChain
Engineer: Andre Schmiel
Input: struct TyArcInstance *p - pointer to ARC structure
Output: int - 1
Description: detect scanchain structure and decide about number of cores on scanchain and IR length

Date           Initials    Description
10-Apr-2017    AS          Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_AutoDetectScanChain(struct TyRISCVInstance *p)
{
	unsigned int  uiNumberOfTaps, uiIndex;
	uint32_t pulIRWidth[MAX_RISCV_TAPS];
	unsigned char pbARCCores[MAX_RISCV_TAPS];
	unsigned char ucConnectionOpened = ptyMLHandle->bConnectionInitialized;
	char          pszValue[5];
	char          pszLocCore[20];
	double        dNewFrequency = 0.0;
	double        dOldFrequency = ptyMLHandle->dJtagFrequency;

	if (ucConnectionOpened == 0)
	{
		if (ML_OpenConnection(ptyMLHandle) != ERR_ML_RISCV_NO_ERROR)
			return ASH_FALSE;
	}

	//we need to select a low frequency (BSCI works with 300kHz)
	(void)ML_ConvertFrequencySelection("100kHz", &dNewFrequency);

	if (ML_SetJtagFrequency(ptyMLHandle, dNewFrequency) != ERR_ML_RISCV_NO_ERROR) // set new frequency value
	{
		if (ucConnectionOpened == 0)
			(void)ML_CloseConnection(ptyMLHandle);

		return ERR_RISCV_ERROR;
	}

	//get current target option 
	ASH_GetSetupItem(p, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, pszValue);
	ptyMLHandle->tyRISCVTargetType = (TyRISCVTargetType)strtoul(pszValue, NULL, 16);

	if (ASH_DetectScanchain(p, MAX_RISCV_TAPS, &uiNumberOfTaps, pulIRWidth, pbARCCores) != ASH_TRUE)
	{
		ptyMLHandle->ulNumberOfTapsOnScanchain = 0;
		ASH_SetSetupItem(p, RISCV_SETUP_SECT_MULTICORE, RISCV_SETUP_ITEM_NUMBEROFTAPS, "0");

		if (ucConnectionOpened == 0)
			(void)ML_CloseConnection(ptyMLHandle);
		else
			(void)ML_SetJtagFrequency(ptyMLHandle, dOldFrequency);

		return ERR_RISCV_ERROR;
	}
	else
	{
		sprintf(pszValue, "%d", uiNumberOfTaps);
		ASH_SetSetupItem(p, RISCV_SETUP_SECT_MULTICORE, RISCV_SETUP_ITEM_NUMBEROFTAPS, pszValue);

		for (uiIndex = 0; uiIndex < uiNumberOfTaps; uiIndex++)
		{
			sprintf(pszLocCore, "%s%d", RISCV_SETUP_SECT_DEVICE, uiIndex + 1);

			if (pbARCCores[uiIndex])
				ASH_SetSetupItem(p, pszLocCore, RISCV_SETUP_ITEM_RISCVCORE, RISCV_SETUP_ITEM_TRUE);
			else
				ASH_SetSetupItem(p, pszLocCore, RISCV_SETUP_ITEM_RISCVCORE, RISCV_SETUP_ITEM_FALSE);

			sprintf(pszValue, "%ld", pulIRWidth[uiIndex]);
			ASH_SetSetupItem(p, pszLocCore, RISCV_SETUP_ITEM_IRWIDTH, pszValue);
		}

		//store modified target option 
		sprintf(pszValue, "%d", ptyMLHandle->tyRISCVTargetType);
		ASH_SetSetupItem(p, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, pszValue);
	}

	//switch back to old frequency settings
	if (ML_SetJtagFrequency(ptyMLHandle, dOldFrequency) != ERR_ML_RISCV_NO_ERROR) // set old frequency value
		return ERR_RISCV_ERROR;

	if (ucConnectionOpened == 0)
		(void)ML_CloseConnection(ptyMLHandle);

	return ERR_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ASH_DetectScanchain
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC structure
unsigned int uiMaxTaps - maximum number of cores supported
unsigned int *puiNumberOfTaps - pointer to number of detected cores
uint32_t *pulIRLength - array of IR length for each core, array must have uiMaxTaps items
unsigned char *pbARCcores - array of boolean indicating ARC core, array must have uiMaxTaps items
Output: int - 1 if detection was successful, 0 if detection failed
Description: detect scanchain structure and decide about number of cores on scanchain and IR length

Date           Initials    Description
05-Nov-2007    VH          Initial
11-Jan-2017    AS          Added update of Scanchain parameters
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_DetectScanchain(struct TyRISCVInstance *p, unsigned int uiMaxTaps, unsigned int *puiNumberOfTaps,
	uint32_t *pulIRWidth, unsigned char *pbRISCVCores)
{
	uint32_t pulLocIRWidth[MAX_RISCV_TAPS];
	unsigned char pbLocRISCVCores[MAX_RISCV_TAPS];
	unsigned int  uiLocNumberOfTaps = 0;

	if ((p == NULL) || (uiMaxTaps == 0))
		return ERR_RISCV_ERROR;

	// detect scanchain structure
	if (ML_DetectScanchain(ptyMLHandle, MAX_RISCV_TAPS, &uiLocNumberOfTaps, pulLocIRWidth, pbLocRISCVCores) != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;

	// set information about scanchain structure
	if (uiLocNumberOfTaps > uiMaxTaps)
		return ERR_RISCV_ERROR;
	else
	{
		unsigned int uiIndex;

		if (uiLocNumberOfTaps == 0)
			return ERR_RISCV_ERROR;

		*puiNumberOfTaps = uiLocNumberOfTaps;
		ptyMLHandle->ulNumberOfTapsOnScanchain = uiLocNumberOfTaps;

		if (pulIRWidth != NULL)
		{
			for (uiIndex = 0; uiIndex < uiLocNumberOfTaps; uiIndex++)
			{
				pulIRWidth[uiIndex] = pulLocIRWidth[uiIndex];
				ptyMLHandle->tyScanchainTapParams[uiIndex].ulIRLength = pulLocIRWidth[uiIndex];
			}
		}

		if (pbRISCVCores != NULL)
		{
			for (uiIndex = 0; uiIndex < uiLocNumberOfTaps; uiIndex++)
			{
				pbRISCVCores[uiIndex] = pbLocRISCVCores[uiIndex];
				ptyMLHandle->tyScanchainTapParams[uiIndex].bRISCVTap = pbLocRISCVCores[uiIndex];
			}
		}
	}

	return ERR_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ASH_GDBScanIR
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - ARC structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int - ERR_RISCV_NO_ERROR if success, otherwise ERR_RISCV_ERROR
Description: scan specified number of bits into JTAG IR register
Date           Initials    Description
04-Dec-2007    VH          Initial
03-Sept-2008   NCH         Renamed to ASH_GDBScanIR
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_GDBScanIR(struct TyRISCVInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
	if ((p == NULL) || (ptyMLHandle == NULL))
		return ERR_RISCV_ERROR;
	if (ulLength == 0)
		return ERR_RISCV_NO_ERROR;
	if (ML_ScanIR(ptyMLHandle, ulLength, pulDataIn, pulDataOut) != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;
	return ERR_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ASH_GDBScanDR
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - ARC structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int - ERR_RISCV_NO_ERROR if success, otherwise ERR_RISCV_ERROR
Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
04-Dec-2007    VH          Initial
03-Sept-2008   NCH         Renamed to ASH_GDBScanDR
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_GDBScanDR(struct TyRISCVInstance *p, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
	if ((p == NULL) || (ptyMLHandle == NULL))
		return ERR_RISCV_ERROR;
	if (ulLength == 0)
		return ERR_RISCV_NO_ERROR;
	if (ML_ScanDR(ptyMLHandle, ulLength, pulDataIn, pulDataOut) != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;
	return ERR_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ASH_ScanIR
Engineer: Nikolay Chokoev
Input: struct TyRISCVInstance *p - RISCV structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int - ERR_RISCV_NO_ERROR if success, otherwise ERR_RISCV_ERROR
Description: scan specified number of bits into JTAG IR register
Date           Initials    Description
03-Sept-2008   NCH         Initial
16-Jan -2009    RS         Modified to add new JTAG states.
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_ScanIR(struct TyRISCVInstance *p, int iLength, uint32_t *pulDataIn, uint32_t *pulDataOut,
	unsigned char ucStartState, unsigned char ucEndState)
{
	int iError;
	//   MESSAGE_PRINT(p, "TAP Reset Done.\n");
	if ((p == NULL) || (ptyMLHandle == NULL))
		return ERR_RISCV_ERROR;
	if (iLength == 0)
		return ERR_RISCV_NO_ERROR;
	switch (ucStartState)
	{
	case RISCV_START_STATE_FROM_CURRENT:
		//assuming we are in Select-IR and have to go to Shift-IR
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 2;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0;
		break;
	case RISCV_START_STATE_FROM_RTI:
		//assuming we are in Idle and have to go to Shift-IR
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 5;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x6;
		break;
	case RISCV_START_STATE_FROM_DR:
		//assuming we are in Select-DR and have to go to Shift-IR
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 3;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x1;
		break;
	case RISCV_START_STATE_FROM_EXIT1_DR_RTI:
		//assuming we are in Exit1-DR and have to go to Shift-IR via RTI
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 8;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x36;
		break;
	default:
		//wrong parameter
		return ASH_FALSE;
	}

	switch (ucEndState)
	{
	case RISCV_END_STATE_TO_RTI:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0xD;
		break;
	case RISCV_END_STATE_TO_DR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1D;
		break;
	case RISCV_END_STATE_TO_IR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x3D;
		break;
	case RISCV_END_STATE_TO_RTI_DR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x2D;
		break;
	case RISCV_END_STATE_TO_RTI_IR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 7;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x6D;
		break;
	case RISCV_END_STATE_TO_RTI_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x03;
		break;
	case RISCV_END_STATE_TO_DR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x07;
		break;
	case RISCV_END_STATE_TO_IR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0F;
		break;
	case RISCV_END_STATE_TO_RTI_DR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0B;
		break;
	case RISCV_END_STATE_TO_RTI_IR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1B;
		break;
	case RISCV_END_STATE_TO_EXIT1_IR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 1;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1;
		break;
	default:
		//wrong parameter
		return ASH_FALSE;
	}

	ptyMLHandle->tyMultipleScanParams.bScanIR = 1;
	ptyMLHandle->tyMultipleScanParams.usLength = (unsigned short)iLength;
	ptyMLHandle->tyMultipleScanParams.usInReserved = 0;
	ptyMLHandle->tyMultipleScanParams.usOutReserved = 0;

	if (pulDataIn != NULL)
		dbgprint("IR:*pulDataIn:%08lX\n", (uint32_t)*pulDataIn);
	else
		dbgprint("IR:*pulDataIn:<null>\n");
	iError = ML_ScanMultiple(ptyMLHandle, &ptyMLHandle->tyMultipleScanParams,
		pulDataIn, pulDataOut);
	if (pulDataOut != NULL)
		dbgprint("IR:*pulDataOut:%08lX\n", (uint32_t)*pulDataOut);
	else
		dbgprint("IR:*pulDataOut:<null>\n");
	dbgprint("IR:iError:%08lX\n", iError);

	if (iError != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;
	return ERR_RISCV_NO_ERROR;
}


/****************************************************************************
Function: ASH_ScanDR
Engineer: Nikolay Chokoev
Input: struct TyRISCVInstance *p - RISCV structure
uint32_t ulLength - number of bits to shift
uint32_t *pulDataIn - data into scanchain
uint32_t *pulDataOut - data from scanchain
Output: int - ERR_RISCV_NO_ERROR if success, otherwise ERR_RISCV_ERROR
Description: scan specified number of bits into JTAG DR register
Date           Initials    Description
03-Sept-2008   NCH         Initial
16-Jan -2009   RS          Modified to add new JTAG states
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_ScanDR(struct TyRISCVInstance *p, int iLength, uint32_t *pulDataIn, uint32_t *pulDataOut,
	unsigned char ucStartState, unsigned char ucEndState)
{
	int iError;
	if ((p == NULL) || (ptyMLHandle == NULL))
		return ERR_RISCV_ERROR;
	if (iLength == 0)
		return ERR_RISCV_NO_ERROR;

	switch (ucStartState)
	{
	case RISCV_START_STATE_FROM_CURRENT:
		//assuming we are in Select-DR and have to go to Shift-DR
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 2;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0;
		break;
	case RISCV_START_STATE_FROM_RTI:
		//assuming we are in Idle and have to go to Shift-DR
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 4;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x2;
		break;
	case RISCV_START_STATE_FROM_EXIT1_IR_RTI:
		//assuming we are in Exit1-IR and have to go to Shift-DR via RTI
		ptyMLHandle->tyMultipleScanParams.usPreTmsCount = 7;
		ptyMLHandle->tyMultipleScanParams.usPreTmsBits = 0x16;
		break;

	case RISCV_START_STATE_FROM_DR:
		//wrong state - see documentation
	default:
		//wrong parameter
		return ASH_FALSE;
	}

	switch (ucEndState)
	{
	case RISCV_END_STATE_TO_RTI:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0xD;
		break;
	case RISCV_END_STATE_TO_DR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1D;
		break;
	case RISCV_END_STATE_TO_IR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x3D;
		break;
	case RISCV_END_STATE_TO_RTI_DR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 6;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x2D;
		break;
	case RISCV_END_STATE_TO_RTI_IR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 7;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x6D;
		break;
	case RISCV_END_STATE_TO_RTI_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x03;
		break;
	case RISCV_END_STATE_TO_DR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 3;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x07;
		break;
	case RISCV_END_STATE_TO_IR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0F;
		break;
	case RISCV_END_STATE_TO_RTI_DR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 4;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x0B;
		break;
	case RISCV_END_STATE_TO_RTI_IR_SKIPPAUSE:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 5;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1B;
		break;
	case RISCV_END_STATE_TO_EXIT1_DR:
		ptyMLHandle->tyMultipleScanParams.usPostTmsCount = 1;
		ptyMLHandle->tyMultipleScanParams.usPostTmsBits = 0x1;
		break;
	default:
		//wrong parameter
		return ASH_FALSE;
	}

	ptyMLHandle->tyMultipleScanParams.bScanIR = 0;
	ptyMLHandle->tyMultipleScanParams.usLength = (unsigned short)iLength;
	ptyMLHandle->tyMultipleScanParams.usInReserved = 0;
	ptyMLHandle->tyMultipleScanParams.usOutReserved = 0;

	if (pulDataIn != NULL)
		dbgprint("DR:*pulDataIn:%08lX\n", (uint32_t)*pulDataIn);
	else
		dbgprint("DR:*pulDataIn:<null>\n");
	iError = ML_ScanMultiple(ptyMLHandle, &ptyMLHandle->tyMultipleScanParams,
		pulDataIn, pulDataOut);
	if (pulDataOut != NULL)
		dbgprint("DR:*pulDataOut:%08lX\n", (uint32_t)*pulDataOut);
	else
		dbgprint("DR:*pulDataOut:<null>\n");
	if (iError != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;
	return ERR_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ASH_TMSReset
Engineer: Nikolay Chokoev
Input: struct TyRISCVInstance *p - RISCV structure
Output: int - ERR_RISCV_NO_ERROR if success, otherwise ERR_RISCV_ERROR
Description: Reset JTAG state machine and go to RTI state.
Date           Initials    Description
03-Sept-2008   NCH         Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_TMSReset(struct TyRISCVInstance *p)
{
	int iError;
	//   MESSAGE_PRINT(p, "TAP Reset Done.\n");
	if (p == NULL)
		return ASH_FALSE;
	iError = ML_ResetTAP(ptyMLHandle, 0);
	if (iError != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;
	return ERR_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ASH_TRSTReset
Engineer: Rejeesh S Babu
Input: struct TyRISCVInstance *p - RISCV structure
Output: int - ERR_RISCV_NO_ERROR if success, otherwise ERR_RISCV_ERROR
Description: Reset JTAG state machine using TRST pin.
Date           Initials   		 Description
19-Jan-2009    	  RS  	           Initial
****************************************************************************/
extern "C" ASH_DLL_EXPORT int ASH_TRSTReset(struct TyRISCVInstance *p)
{
	int iError;
	//   MESSAGE_PRINT(p, "TAP Reset Done.\n");
	if (p == NULL)
		return ASH_FALSE;
	iError = ML_ResetTAP(ptyMLHandle, 1);
	if (iError != ERR_ML_RISCV_NO_ERROR)
		return ERR_RISCV_ERROR;
	return ERR_RISCV_NO_ERROR;
}


/****************************************************************************
Function: TRACE_PRINT
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *strToFormat - string format + parameters (like printf)
Output: None
Description: The function is called when DLL wants to print trace
message to debugger window
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void TRACE_PRINT(struct TyRISCVInstance *p, const char * strToFormat, ...)
{
	if ((p != NULL) && (p->bTraceMessagesEnabled != FALSE) && (p->pcbCallbackFunction != NULL))
	{
		char *pszLocPtr;
		char buffer[0x120];
		va_list ArgList;

		strcpy(buffer, "OPELLA-XD RISCV TRACE: ");
		pszLocPtr = &(buffer[strlen(buffer)]);
		va_start(ArgList, strToFormat);
		(void)vsprintf(pszLocPtr, strToFormat, ArgList);
		va_end(ArgList);
		(void)p->pcbCallbackFunction->printf(buffer);
	}

#ifdef DEBUG
	char *pszLocPtr;
	char buffer[0x120];
	va_list ArgList;
	FILE * fp;

	strcpy(buffer, "OPELLA-XD RISCV TRACE: ");
	pszLocPtr = &(buffer[strlen(buffer)]);
	va_start(ArgList, strToFormat);
	(void)vsprintf(pszLocPtr, strToFormat, ArgList);
	va_end(ArgList);

	fp = fopen("riscv_debug.txt", "a");
	if (fp != NULL)
	{
		fprintf(fp, buffer);

		fclose(fp);
	}
#endif

}


/****************************************************************************
Function: ERROR_PRINT
Engineer: Vitezslav Hola
Input: struct TyArcInstance *p - pointer to ARC instance structure
char *strToFormat - string format + parameters (like printf)
Output: None
Description: The function is called when DLL wants to print error message
(either to GUI or debugger window)
Date           Initials    Description
16-Jul-2007    VH          Initial
****************************************************************************/
void ERROR_PRINT(struct TyRISCVInstance *p, const char * strToFormat, ...)
{
	if (p != NULL)
	{
		if ((p->pcbCallbackFunction != NULL))
		{
			char *pszLocPtr;
			char buffer[0x120];
			va_list ArgList;
			// printing to debugger window
			strcpy(buffer, "OPELLA-XD RISCV ERROR: ");
			pszLocPtr = &(buffer[strlen(buffer)]);
			va_start(ArgList, strToFormat);
			(void)vsprintf(pszLocPtr, strToFormat, ArgList);
			va_end(ArgList);
			(void)p->pcbCallbackFunction->printf(buffer);
		}
	}
}

/****************************************************************************
		Function: ConvertMLErrorToGDBErrors
		Engineer: Ashutosh Garg
		Input:	  int mlError(Middle layer error)
		Output:	  Error code
		Description: return error to server
		Date           Initials    Description
		1-APR-2019		  AG        Initial
****************************************************************************/
int ConvertMLErrorToGDBErrors(struct TyRISCVInstance *p,int mlError)
{
	int iRet = ERR_RISCV_ERROR;
	switch (mlError)
	{
	case ERR_ML_RISCV_NO_ERROR:
		iRet = ERR_RISCV_NO_ERROR;
		break;
	case ERR_ML_RISCV_DEBUG_PDOWN:
		iRet = ASH_PDOWN;
		break;
	case ERR_ML_RISCV_DEBUG_POFF:
		iRet = ASH_POFF;
		break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUSY:
		iRet = ASH_RISCV_ABSTRACT_REG_WRITE_BUSY;
		break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED:
		iRet = ASH_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED;
		break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION:
		iRet = ASH_RISCV_ABSTRACT_REG_WRITE_EXCEPTION;
		break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE:
		iRet = ASH_RISCV_ABSTRACT_REG_WRITE_HART_STATE;
		break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR:
		iRet = ASH_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR;
		break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR:
		iRet = ASH_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR;
		break;
	case ERR_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED:
		iRet = ASH_RISCV_SYSTEM_BUS_TIMEOUT_FAILED;
		break;
	case ERR_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED:
		iRet = ASH_RISCV_SYSTEM_BUS_ADDRESS_FAILED;
		break;
	case ERR_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED:
		iRet = ASH_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED;
		break;
	case ERR_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED:
		iRet = ASH_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED;
		break;
	case ERR_ML_RISCV_SYSTEM_BUS_OTHER_REASON:
		iRet = ASH_RISCV_SYSTEM_BUS_OTHER_REASON;
		break;
	case ERR_ML_RISCV_SYSTEM_BUS_MASTER_BUSY:
		iRet = ASH_RISCV_SYSTEM_BUS_MASTER_BUSY;
		break;
	case ERR_ML_RISCV_MEM_INVALID_LENGTH:
		iRet = ASH_RISCV_MEM_INVALID_LENGTH;
		break;
	case ERR_ML_RISCV_HALT_TIMEOUT:
		iRet = ASH_RISCV_HALT_TIMEOUT;
		break;
	case ERR_ML_RISCV_RESUME_TIMEOUT:
		iRet = ASH_RISCV_RESUME_TIMEOUT;
		break;
	case ERR_ML_RISCV_UNSUPPORT_ARCH_TYPE:
		iRet = ASH_RISCV_UNSUPPORT_ARCH_TYPE;
		break;
	case ERR_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE:
		iRet = ASH_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE;
		break;
	case ERR_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION:
		iRet = ASH_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;
		break;
	case ERR_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT:
		iRet = ASH_RISCV_TRIGGER_INFO_LOOP_TIMEOUT;
		break;
	case ERR_ML_RISCV_DISALE_ABSTRACT_COMMAND:
		iRet = ASH_RISCV_DISALE_ABSTRACT_COMMAND;
		break;
	case ERR_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT:
		iRet = ASH_RISCV_NEXT_DEBUG_MODULE_TIMEOUT;
		break;
	default:
		break;
	}

	//TODO: Add above cases in ML_GetErrorMessage function
	ERROR_PRINT(p, ML_GetErrorMessage(mlError));

	return iRet;
}