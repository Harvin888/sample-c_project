/****************************************************************************
	   Module: RISCVsetup.h
	 Engineer: 
  Description: Headers for ARC setup interface.
Date           Initials    Description
15-Oct-2007    AG          Initial

****************************************************************************/
#ifndef RISCV_SETUP_H_
#define RISCV_SETUP_H_

// NOTE: This DLL is included from several projects because it is used as interface.
//       Therefore be careful if you are changing something in this header.

// External DLLs or libraries used by the driver
#ifdef __LINUX
// Linux
#define RISCVGUI_DLL                           "riscvgui.so"
#else
// WinXP/2000/Vista
#define RISCVGUI_DLL                           "riscvgui.dll"
#endif

#define RISCVGUISETUPDLG_NAME                  "AshShowRISCVSetupWindow"

// default INI filename (setup for ARC)
#define RISCV_INI_DEFAULT_NAME                 "opxdriscv.ini"
#define GDB_RISCV_INI_DEFAULT_NAME					"gdbopxdriscv.ini"
#define RISCV_INI_ENV_VARIABLE                 "OPXDRISCV_INI_FILE"

//-------------------------------------------------------------------
// This section defines names for configuration file (item and section names)
//
#define RISCV_SETUP_SECT_INIFILE               "InitFileLocation"
#define RISCV_SETUP_SECT_TARGETCONFIG          "TargetConfiguration"
#define RISCV_SETUP_SECT_MISCOPTIONS           "MiscOptions"
#define RISCV_SETUP_SECT_MULTICORE             "MultiCoreDebug"
#define RISCV_SETUP_SECT_DEVICE                "Device"
#define RISCV_SETUP_SECT_DEVICE1               "Device1"
#define RISCV_SETUP_SECT_DEVICE2               "Device2"                        // this may go further
// items in target configuration section
#define RISCV_SETUP_ITEM_ADAPTIVECLOCK         "AdaptiveClock"
#define RISCV_SETUP_ITEM_TARGETOPTION          "TargetOptions"
#define RISCV_SETUP_ITEM_TAPRESET			   "Tap Reset"
// items in misc options
#define RISCV_SETUP_ITEM_SHOWCONFIG            "ShowConfiguration"
#define RISCV_SETUP_ITEM_PROBEINSTANCE         "ProbeInstance"
// items in multicore section
#define RISCV_SETUP_ITEM_NUMBEROFTAPS         "NumberOfTaps"
// items in deviceX sections
#define RISCV_SETUP_ITEM_RISCVCORE             "RISCV"
#define RISCV_SETUP_ITEM_IRWIDTH               "IRWidth"
#define RISCV_SETUP_ITEM_BYPASSINST            "Bypass"
// item values (bool type)
#define RISCV_SETUP_ITEM_TRUE                  "1"
#define RISCV_SETUP_ITEM_FALSE                 "0"

// restiction to certain values
#define RISCV_SETUP_MAX_DEVICES                16                               // maximum Opella-XD devices
#define RISCV_SETUP_MAX_SCANCHAIN_CORES        256                              // maximum cores on scanchain
#define RISCV_SETUP_MAX_CORE_IR_LENGTH         32                               // maximum length for IR register for each core
#define RISCV_SETUP_MAX_BYPASS_IN_LENGTH       32								// maximum length for bypass instruction

//-------------------------------------------------------------------

// type definition
typedef const char *(*PFAshGetID)(void);
typedef void(*PFAshListConnectedProbes)(char ppszInstanceNumber[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances);
typedef void(*PFAshSetSetupItem)(void *pvPtr, const char *pszSection, const char *pszItem, const char *pszValue);
typedef void(*PFAshGetSetupItem)(void *pvPtr, const char *pszSection, const char *pszItem, char *pszValue);
typedef void(*PFAshAutoDetectScanChain)(void *pvPtr);

typedef struct _TyRISCVSetupInterface {
	// NOTE: when new functions are being added, ALWAYS add them to the end 
	PFAshGetID               pfGetID;					// pointer to func to obtain DLL ID
	PFAshListConnectedProbes pfListConnectedProbes; // pointer to func to get list of probes
	PFAshGetSetupItem        pfGetSetupItem;        // pointer to func to get setup item
	PFAshSetSetupItem        pfSetSetupItem;        // pointer to func to set setup item
	PFAshAutoDetectScanChain pfAutoDetectScanChain;	// pointer to func to auto detected scan chain cores
   // add new functions here if needed
} TyRISCVSetupInterface;

typedef int(*PFRISCVSetupFunction)(void *pvPtr, TyRISCVSetupInterface *ptyRISCVSetupInt);

#endif   // ML_ARC_H_

