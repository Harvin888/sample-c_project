#pragma once
#ifndef ML_RISCV_ERR_H_
#define ML_RISCV_ERR_H_

// error messages
#define MSG_ML_NO_ERROR                                     ""
#define MSG_ML_INVALID_DISKWARE_FILENAME                    "Cannot find Opella-XD RISCV diskware file\n"
#define MSG_ML_INVALID_FPGAWARE_FILENAME                    "Cannot find Opella-XD RISCV FPGAware file\n"
#define MSG_ML_INVALID_FIRMWARE_FILENAME                    "Cannot find Opella-XD firmware file\n"
#define MSG_ML_INVALID_TPA_CONNECTED                        "Missing or invalid type of TPA connected to Opella-XD\n"
#define MSG_ML_USB_DRIVER_ERROR                             "USB driver error occurred\n"
#define MSG_ML_INVALID_DRIVER_HANDLE                        "Invalid driver handle used\n"
#define MSG_ML_PROBE_NOT_AVAILABLE                          "Selected Opella-XD is not available for debug session\n"
#define MSG_ML_INVALID_TPA                                  "Missing or invalid TPA type connected to Opella-XD\n"
#define MSG_ML_PROBE_CONFIG_ERROR                           "Cannot configure Opella-XD with RISCV diskware and FPGAware\n"
#define MSG_ML_TARGET_NOT_SUPPORTED                         "Requested feature is not supported for current target\n"
#define MSG_ML_INVALID_FREQUENCY                            "Invalid frequency format used\n"
#define MSG_ML_AA_BLASTING_FAILED                           "Cannot blast FPGA into RISCVangel\n"
#define MSG_ML_AA_INVALID_EXTCMD_RESPONSE                   "Unexpected response from RISCVangel\n"
#define MSG_ML_AA_INVALID_PLL_FREQUENCY                     "Invalid PLL frequency requested for RISCVangel\n"
#define MSG_ML_RTCK_TIMEOUT_EXPIRED                         "Cannot detect RTCK signal from target within specified timeout\n"
#define MSG_ML_DEBUG_ACCESS_FAILED                          "Access to RISCV debug interface has failed\n"
#define MSG_ML_SCANCHAIN_TEST_FAILED                        "Scanchain loopback test has failed\n"
#define MSG_ML_INVALID_PROPERTY_FORMAT                      "Invalid property format\n"
#define MSG_ML_UNKNOWN_ERROR                                "Unknown error has occurred\n"
#define MSG_ML_TARGET_RESET_DETECTED                        "\nA target initiated reset has been detected\n"
#define MSG_ML_TARGET_POWERED_DOWN                          "\nTarget is Powered-down\n"
#define MSG_ML_TARGET_POWERED_OFF                           "\nTarget not detected. Ensure the Opella-XD is connected to the target and that the target is powered on\n"
#define MSG_ML_RISCV_ABSTRACT_REG_WRITE_BUSY                "Internal error. Abstract command execution failed: An abstract command is currently being executed\n"
#define MSG_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED       "Internal error. Abstract command execution failed: unsupported operation\n"
#define MSG_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION           "Internal error. Abstract command execution failed: exception occurred"
#define MSG_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE          "Internal error. Abstract command execution failed: hart is either in invalid state(running/halted) or unavailable\n"
#define MSG_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR           "Internal error. Abstract command execution failed: bus error (e.g. alignment, access size, or timeout) occurred"
#define MSG_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR         "Internal error. Abstract command execution failed: other reasons\n"
#define MSG_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED              "Internal error. Memory access via system bus failed: timeout occurred"
#define MSG_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED              "Internal error. Memory access via system bus failed: bad address accessed"
#define MSG_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED            "Internal error. Memory access via system bus failed: alignment error occurred"
#define MSG_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED     "Internal error. Memory access via system bus failed: access of unsupported size requested"
#define MSG_ML_RISCV_SYSTEM_BUS_OTHER_REASON                "Internal error. Memory access via system bus failed: other reasons"
#define MSG_ML_RISCV_SYSTEM_BUS_MASTER_BUSY                 "Internal error. Memory access via system bus failed: system bus master is busy even after waiting 2 seconds"
#define MSG_ML_RISCV_MEM_INVALID_LENGTH                     "Internal error. Abstract command/System bus execution failed: Invalid size\n"
//TODO: Below error messages need review
#define MSG_ML_RISCV_HALT_TIMEOUT                           "Internal error. Could not halt the target: timeout occurred\n"
#define MSG_ML_RISCV_RESUME_TIMEOUT                         "Internal error. Could not resume the target: timeout occurred\n"
#define MSG_ML_RISCV_UNSUPPORT_ARCH_TYPE                    "Internal error. Unknown architecture\n"
#define MSG_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE           "Internal error. Unsupported memory access size request\n"
#define MSG_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION    "Internal error. Unsupported abstract access operation\n"
#define MSG_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT              "Internal error. Could not fetch trigger details: timeout occurred\n"
#define MSG_ML_RISCV_DISALE_ABSTRACT_COMMAND                "Internal error. Could not execute abstract command as resume, halt or havereset is enabled\n"
#define MSG_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT              "Internal error. Error identifying next debug module\n"
#define MSG_ML_RISCV_INVALID_TRIGGER_OPERATION              "Internal error. Unsupported trigger operation\n"
#define MSG_ML_RISCV_UNKNOWN_CMD                            "Internal error. Unsupported USB command\n"

#endif