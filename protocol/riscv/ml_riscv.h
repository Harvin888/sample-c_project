#pragma once
#ifndef ML_RISCV_H_
#define ML_RISCV_H_

#ifdef __LINUX
#include "../../protocol/common/drvopxd.h"
#include "../../protocol/riscv/ml_riscv_err.h"
#else
#include "..\..\protocol\common\drvopxd.h"
#include "..\..\protocol\riscv\ml_riscv_err.h"
#endif

#ifdef __LINUX
// Linux
typedef uint32_t       DWORD;
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#endif //__LINUX

#define FREE(ptr)    free(ptr);\
    ptr = NULL;

// error codes for RISCV midlayer
#define ERR_ML_RISCV_NO_ERROR                                 0           // no error occured
#define ERR_ML_RISCV_INVALID_HANDLE                           1           // invalid handle with parameters
#define ERR_ML_RISCV_ALREADY_INITIALIZED                      2           // connection already initialized
#define ERR_ML_RISCV_TARGET_NOT_SUPPORTED                     3           // functionality not supported for selected target
#define ERR_ML_RISCV_USB_DRIVER_ERROR                         4           // driver internal error
#define ERR_ML_RISCV_PROBE_NOT_AVAILABLE                      5           // cannot open connection with probe
#define ERR_ML_RISCV_PROBE_INVALID_TPA                        6           // no or invalid TPA used
#define ERR_ML_RISCV_PROBE_CONFIG_ERROR                       7           // cannot configure probe
#define ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED                     10          // RTCK timeout expired
#define ERR_ML_RISCV_INVALID_FREQUENCY                        50          // invalid frequency selection
#define ERR_ML_RISCV_ANGEL_BLASTING_FAILED                    100         // blasting to RISCVangel failed
#define ERR_ML_RISCV_ANGEL_INVALID_EXTCMD_RESPONSE            101         // invalid response to extended command
#define ERR_ML_RISCV_ANGEL_INVALID_PLL_FREQUENCY              102         // invalid PLL frequency
#define ERR_ML_RISCV_TEST_SCANCHAIN_FAILED                    200         // failing test scanchain
#define ERR_ML_RISCV_DETECT_SCANCHAIN_FAILED                  201         // detection of scanchain structure failed
#define ERR_ML_RISCV_INVALID_SCANCHAIN_STRUCTURE              202         // invalid scanchain structure
#define ERR_ML_RISCV_TARGET_RESET_DETECTED                    300         // target reset occured
#define ERR_ML_RISCV_DEBUG_ACCESS                             400         // access to RISCV debug failed
#define ERR_ML_RISCV_DEBUG_PDOWN                              401         // access to RISCV debug failed due to powerdown
#define ERR_ML_RISCV_DEBUG_POFF                               402         // access to RISCV debug failed due to power-off
#define ERR_ML_UNKNOWN_CMD                                    403		  // command which is not implemented in firmware/diskware

//riscv specific errors
#define ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUSY                500   //abstarct reg cmd error busy
#define ERR_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED       501   //abstarct reg cmd error not supported
#define ERR_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION           502   //abstarct reg cmd error exception
#define ERR_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE          503   //abstarct reg cmd error hart state
#define ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR           504   //abstarct reg cmd bus error
#define ERR_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR         505   //abstarct reg cmd other errors
#define ERR_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED              506   //system bus sberror timeout
#define ERR_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED              507   //system bus sberror incorrect address
#define ERR_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED            508   //system bus sberror alignment error
#define ERR_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED     509   //system bus sberror unsupported size access
#define ERR_ML_RISCV_SYSTEM_BUS_OTHER_REASON                510   //system bus sberror due to other reasons
#define ERR_ML_RISCV_SYSTEM_BUS_MASTER_BUSY                 511   //system bus master busy error
#define ERR_ML_RISCV_MEM_INVALID_LENGTH                     512
#define ERR_ML_RISCV_HALT_TIMEOUT                           513   //unable to halt
#define ERR_ML_RISCV_RESUME_TIMEOUT                         514   //unable to resume
#define ERR_ML_RISCV_UNSUPPORT_ARCH_TYPE                    515   //Unknown architecture type to diskware
#define ERR_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE           516   //Unsupported memory size to diskware
#define ERR_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION    517   //Unsupported abstract access operation mentioned to diskware
#define ERR_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT              518   //Loop break after 50 iterations
#define ERR_ML_RISCV_DISALE_ABSTRACT_COMMAND                519   //Not execute abstract command if resume halt or havereset is enable
#define ERR_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT              520   //Failed and loop break after 100 iterations
#define ERR_ML_RISCV_INVALID_TRIGGER_OPERATION              521   //Unsupported trigger operation

// filenames for RISCV diskware and FPGAware
#define OPXD_DISKWARE_RISCV_FILENAME                          "opxddriscv.bin"
#define OPXD_DISKWARE_RISCV_LPC1837_FILENAME                  "opxddriscv_r3.bin"
#define OPXD_FPGAWARE_RISCV_FILENAME                          "opxdfriscv.bin"
#define OPXD_FPGAWARE_RISCV_TPA_R1_FILENAME                   "opxdfriscv_tpa_r1.bin"
#define OPXD_FPGAWARE_RISCV_LPC1837_FILENAME                  "opxdfriscv_nxplpc.bin"
#define OPXD_FIRMWARE_FILENAME                                "opxdfw.bin"
#define OPXD_FIRMWARE_FILENAME_LPC1837                        "opxdfw_nxplpc.bin"
#define OPXD_FLASHUTIL_FILENAME_LPC1837                       "opxdflsh_nxplpc.bin"

// maximum memory block size
#define MAX_RISCV_READ_MEMORY                                 0x4000      // in words (total 64 kB)
#define MAX_RISCV_WRITE_MEMORY                                0x4000      // in words (total 64 kB)

#define MAX_RISCV_TAPS                                       256
#define MAX_ON_TARGET_RESET_ACTIONS                           16


// sleep function prototype
typedef void(*TySleepFunc) (unsigned miliseconds);                     // sleep function prototype
typedef void(*TyProgressBarFunc) (void);                               // callback prototype for progress bar (blasting, etc.)
typedef void(*PfPrintFunc)(const char *);                              // callback for general print messages

// target types
typedef enum { ARC_TGT_JTAG_TPA_R1 } TyRISCVTargetType;

// structures used in RISCV midlayer
typedef struct _TyRISCVTapParams {
    uint32_t ulScanchainTap;                                      // TAP index on scanchain
    uint32_t ulTapVersion;                                        // TAP version
    unsigned short usAccessType;                                  // encoded access parameters
} TyRISCVTapParams;

typedef struct _TyScanchainTapParams {
    // TAP information
    unsigned char bRISCVTap;                                      // flag if this core is RISCV or not
    uint32_t ulIRLength;                                          // IR length
	uint32_t ulBypassInst;                                        //BypassInstruction
} TyScanchainTapParams;

// firmware / diskware version
typedef struct _TyProbeVersion {
    uint32_t ulVersion;                                           // version
    uint32_t ulDate;                                              // date
} TyProbeVersion;

typedef struct _TyRISCVTargetParams {
    unsigned char bConnectionInitialized;
    // target settings
    TyRISCVTargetType tyRISCVTargetType;                           // RISCV target type
    double dJtagFrequency;                                         // JTAG frequency in MHz
    unsigned char bAdaptiveClockEnabled;                           // adaptive clock flag
    unsigned char bDoNotIssueTAPReset;
    uint32_t ulAdaptiveClockTimeout;                               // adaptive clock timeout
    uint32_t ulDelayAfterReset;                                    // delay after reset in ms
    uint32_t ulResetDuration;                                      // reset duration in ms
    // core(s)/TAP(s) settings
    uint32_t ulCurrentRISCVTap;                                    // current TAP (starting from 1)
    uint32_t ulCurrentRISCVCore;                                   // current RISCV core (starting from 0)
    uint32_t ulHartId;                                             // current RISCV Hart
    uint32_t ulNumberOfRISCVTaps;                                  // number of RISCV TAPs on scanchain
    uint32_t ulNumberOfTapsOnScanchain;                            // number of TAPs on scanchain
    TyRISCVTapParams tyRISCVTapParams[MAX_RISCV_TAPS];             // array with information about RISCV TAPs
    TyScanchainTapParams tyScanchainTapParams[MAX_RISCV_TAPS];     // array with information about TAPs on scanchain
    // common parameters for all RISCV cores
    unsigned char bCycleStep;                                            // cycle step flag
    // function pointers
    TySleepFunc pfSleepFunc;                                             // pointer to sleep function
    TyProgressBarFunc pfProgressBarFunc;                                 // pointer to progress bar function
    // probe information
    char pszProbeInstanceNumber[16];                                     // probe instance serial number
    int iCurrentHandle;                                                  // current probe handle
    TyProbeVersion tyProbeDiskwareInfo;                                  // probe diskware info
    TyProbeVersion tyProbeFirmwareInfo;                                  // probe firmware info
    TyProbeVersion tyProbeFPGAwareInfo;                                  // probe FPGAware info
    // other information
    int iLastErrorCode;                                                  // last error code (if any error happened)
    TyMultipleScanParams tyMultipleScanParams;
} TyRISCVTargetParams, *PTyRISCVTargetParams;


// function prototypes in RISCV midlayer
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances);
int ML_UpgradeFirmware(TyRISCVTargetParams *ptyRISCVParams, PfPrintFunc pfPrintFunc);
void ML_RISCVParamsInit(TyRISCVTargetParams* ptyRISCVParams, TyDevHandle tyDevHandle, TyDeviceInfo* ptyDeviceInfo, bool bMCconfig);
int ML_ConfigureOpellaXD(TyDevHandle tyDevHandle, TyDeviceInfo* ptyDeviceInfo, bool bConfigureFpga);
int ML_OpenConnection(TyRISCVTargetParams *ptyRISCVParams);
int ML_CloseConnection(TyRISCVTargetParams *ptyRISCVParams);
int ML_TestScanchain(TyRISCVTargetParams *ptyRISCVParams, unsigned char bVerifyNumberOfCores);
int ML_DetectScanchain(TyRISCVTargetParams *ptyRISCVParams, unsigned int uiMaxCores, unsigned int *puiNumberOfCores,
    uint32_t *pulIRWidth, unsigned char *pbRISCVCores);
int ML_UpdateScanchain(TyRISCVTargetParams *ptyRISCVParams);

// function supporting target reset
int ML_SetResetProperty(TyRISCVTargetParams *ptyRISCVParams, unsigned char bSetResetDelay, unsigned int *piValue);
int ML_ResetTarget(TyRISCVTargetParams *ptyRISCVParams);
// function setting frequency
int ML_SetJtagFrequency(TyRISCVTargetParams *ptyRISCVParams, double dFrequency);
unsigned char ML_UsingAdaptiveJtagClock(TyRISCVTargetParams *ptyRISCVParams);
int ML_ToggleAdaptiveJtagClock(TyRISCVTargetParams *ptyRISCVParams, unsigned char bEnableAdaptiveClock);
// read/write functions
int ML_HaltHart(TyRISCVTargetParams *ptyRISCVParams,unsigned short usTapId,uint32_t ulHartId,uint32_t ulAbits);
int ML_StatusProc(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, unsigned short usCoreId, uint32_t ulHartId,unsigned char *pucGlobalStatus, unsigned char *pucStopProblem,uint32_t ulArchSize, uint32_t ulAbits);
int ML_RunHart(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAbits);
int ML_Discovery(TyRISCVTargetParams *ptyRISCVParams, uint32_t uiTapId, uint32_t * pulDiscoveryData,uint32_t ulHartNo);
int ML_Read_Registers(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits);
int ML_Write_Registers(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize, uint32_t ulAbits);
int ML_Write_A_Register(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize, uint32_t ulAbits);
int ML_Read_A_Register(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize, uint32_t ulAbits);
int ML_WriteWord(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits);
int ML_ReadWord(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize, uint32_t ulAbits);
int ML_Write_DPC(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulValue,uint32_t ulArchSize, uint32_t ulAbits);
int ML_ReadBlock(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits);
int ML_WriteBlock(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize, uint32_t ulAbits);
int ML_SingleStep(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId,uint32_t ulArchSize, uint32_t ulAbits);
int ML_AddTrigger(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerAddr, uint32_t ulTriggerNum, uint32_t *ulTriggerOut,uint32_t ulType,uint32_t ulArchSize, uint32_t ulAbits, uint32_t ulLength, uint32_t ulMatch, uint32_t ulResrequired);
int ML_RemoveTrigger(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerNum,uint32_t ulArchSize, uint32_t ulAbits, uint32_t ulResUsed);
int ML_TriggerInfo(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulTriggerInfo,uint32_t ulArchSize, uint32_t ulAbits);
int ML_Riscv_Mechanism(TyRISCVTargetParams * ptyRISCVParams, unsigned short usTapId, uint32_t ulRegMec, uint32_t ulMemMec, uint32_t ulCsrSupp,uint32_t ulFloatingProc,uint32_t ulFence_i);
int ML_Riscv_TargetReset(TyRISCVTargetParams *ptyRISCVParams,uint32_t ulHartId);
int ML_Write_CSR(TyRISCVTargetParams * ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegisterValue, uint32_t ulRegisterNumber,uint32_t ulArchSize, uint32_t ulAbits);
int ML_Discover_Harts(TyRISCVTargetParams *ptyRISCVParams, uint32_t uiTapId, uint32_t *pulNumberOfHarts);
int ML_ConvertDrvlayerErrorToMLError(TyError drvLayerError);

// function setting target parameters
int ML_InvalidatingICache(TyRISCVTargetParams *ptyRISCVParams, unsigned char bDisableICacheInvalidation);
int ML_SetTargetEndian(TyRISCVTargetParams *ptyRISCVParams, unsigned char bBigEndianTarget);
unsigned char ML_IsCpuBE(TyRISCVTargetParams *ptyRISCVParams);
int ML_SetCurrentTap(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulCpuNum);
int ML_SetCycleStep(TyRISCVTargetParams *ptyRISCVParams, unsigned char bCycleStep);
unsigned char ML_GetCycleStep(TyRISCVTargetParams *ptyRISCVParams);
int ML_SetAdaptiveClockTimeout(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulTimeout);
int ML_SetMemoryAccessOptimization(TyRISCVTargetParams *ptyRISCVParams, unsigned char bEnableOptimizedAccess);
int ML_SetAddressAutoincrement(TyRISCVTargetParams *ptyRISCVParams, unsigned char bEnableAddressAutoincrement);
void ML_SetCpuVersionNumber(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulVersionNumber);
int ML_IndicateTargetStatusChange(TyRISCVTargetParams *ptyRISCVParams);
int ML_IntializeCJTAG(TyRISCVTargetParams *ptyRISCVParams);
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency);

// additional functions for JTAG console
int ML_ScanIR(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
int ML_ScanDR(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut);
int ML_ResetTAP(TyRISCVTargetParams *ptyRISCVParams, int bResetType);
int ML_ScanMultiple(TyRISCVTargetParams *ptyRISCVParams, TyMultipleScanParams *ptyMultipleScanParams,
    uint32_t *pulDataIn, uint32_t *pulDataOut);


void ML_SetDllPath(const char *pszDllPath);
void ML_GetDllPath(char *pszDllPath, unsigned int uiSize);
char* ML_GetErrorMessage(int iReturnCode);
int ML_ResetTAP(TyRISCVTargetParams *ptyRISCVParams, int bResetType);
void ML_Sleep(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulMiliseconds);
int ML_ConfigureVega(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, unsigned int uiInstrtoSelectADI, unsigned int uiCore, unsigned int uiIRLen);


#endif  //
