#ifndef RISCV_INT_H
#define RISCV_INT_H
/*======================================================================
Note: this header standard is copied from ARC Metware.
======================================================================*/

// (If you are using this in C, and your C compiler is so old as to not
// support // comments, then just surround these comments with the
// standard C comment convention)
enum {
	RISCVINT_BASE_VERSION = 1,
	RISCVINT_BREAK_WATCHPOINT_VERSION = 2,
	RISCVINT_SPECIFIC_DISPLAY_VERSION = 3,
	RISCVINT_FILL_MEM_VERSION = 4,
	// Trace adds support for getting the last N PC values
	// as in an ICE trace buffer, which MetaWare's simulator maintains
	// anyway.
	RISCVINT_TRACE_VERSION = 5
};

struct Register_display;

#define RISCV_FEATURE_fill_memory 1
#define RISCV_FEATURE_instruction_trace 2
#define RISCV_FEATURE_data_exchange 4
#define RISCV_FEATURE_banked_reg 8

// For new watchpoint interface:
#define RISCV_HARDWARE_BREAKPOINT 3
#define RISCV_WATCHPOINT_READ 0	// WP on read
#define RISCV_WATCHPOINT_WRITE 1	// WP on write
#define RISCV_WATCHPOINT_ACCESS 2	// WP on access
				// (This agrees with simext.h)
#define cntxt_cacheless 4	// set if the request is cacheless

// The first 1024 data exchange opcodes are system-defined.
// This opcode is implemented by any transport facility and tells the
// maximum amount allowed to be transferred by the data exchange facility.
#define DATA_XCHG_FIRST_USER_OPCODE 1024
#define DATA_XCHG_UNRECOGNIZED_OPCODE 0xffffffff

// Warning: there seem to have been some versions of this interface
// with RISCV_REG_TYPE as uint32_t.  The official type is uint32_t.
// If you implement a C++ version of this DLL and use Microsoft C++,
// it *will not* tell you that you have the wrong type in your definition
// of, say, read_banked_reg, and you won't override the base class 
// function; your defined function will be ignored silently.
// If you have source code that uses a different type for the register
// you can change this define, or, preferably, change your code to 
// use RISCV_REG_TYPE everywhere.
// Similarly, RISCV_ADDR_TYPE is the type of an address in the RISCV's memory
// space.
// There are no unsigned or signed longs in the interface.  The reason for
// this is so that it works properly when compiled on a 64-bit machine.
#define RISCV_REG_TYPE uint32_t
#if __x86_64
#define RISCV_ADDR_TYPE unsigned
#define RISCV_REG_VALUE_TYPE unsigned
#define RISCV_AMOUNT_TYPE unsigned
#else
// So that previous 32-bit interfaces won't break (due to type changes)
// the 32-bit interface remains as it was.
#define RISCV_ADDR_TYPE uint32_t
#define RISCV_REG_VALUE_TYPE uint32_t
#define RISCV_AMOUNT_TYPE uint32_t
#endif

#define RISCV_functions(func,fbdy,OBJ0,OBJ) \
    func(uint32_t, version, (OBJ0) )				\
    func(const char *, id, (OBJ0) )			\
    fbdy(void, destroy, (OBJ0), return; )		\
    fbdy(const char *, additional_possibilities, (OBJ0), return 0;)\
    fbdy(void*, additional_information, (OBJ unsigned), return 0;) 	\
    func(uint32_t, prepare_for_new_program, (OBJ uint32_t will_download))		\
    func(uint32_t, process_property, (OBJ const char *key, const char *value)) \
							\
    fbdy(uint32_t, is_simulator, (OBJ0), return 0; )		\
    fbdy(uint32_t, step, (OBJ0), return 0; ) 		\
    fbdy(uint32_t, run,  (OBJ0), return 0; ) 		\
    func(uint32_t, read_memory, (OBJ uint32_t adr, void *, \
		uint32_t amount, uint32_t context) )	\
    func(uint32_t, write_memory, (OBJ uint32_t adr, void *, \
		uint32_t amount, uint32_t context) ) \
    func(uint32_t, read_reg, (OBJ uint32_t r, uint32_t *value)) 	\
    func(uint32_t, write_reg, (OBJ uint32_t r, uint32_t value)) 	\
    fbdy(unsigned, memory_size, (OBJ0), return 0;)		\
    fbdy(uint32_t, set_memory_size, (OBJ unsigned S), return 0;) 	\
    /* Version 1 interface ends here. */		\
    /* Version 2 interface includes watchpoint fcns. */ \
    fbdy(uint32_t, set_reg_watchpoint, (OBJ uint32_t r, uint32_t length), return 0; )    \
    fbdy(uint32_t, remove_reg_watchpoint, (OBJ uint32_t r, uint32_t length), return 0; ) \
    fbdy(uint32_t, set_mem_watchpoint, (OBJ uint32_t addr, uint32_t length), return 0; ) \
    fbdy(uint32_t, remove_mem_watchpoint, (OBJ uint32_t addr, uint32_t length), return 0; ) \
    fbdy(uint32_t, stopped_at_watchpoint, (OBJ0), return 0; )\
    fbdy(uint32_t, stopped_at_exception, (OBJ0), return 0;)  \
    fbdy(uint32_t, set_breakpoint, (OBJ unsigned address, void*cookie), return 0;)\
    fbdy(uint32_t, remove_breakpoint, 			\
		(OBJ unsigned address, void*cookie), return 0;)\
    fbdy(uint32_t, retrieve_breakpoint_code, (OBJ unsigned address, 	\
		char *dest, unsigned len, void *cookie_buf), return 0;)\
    fbdy(uint32_t, breakpoint_cookie_len, (OBJ0), return 0;)	\
    fbdy(uint32_t, at_breakpoint, (OBJ0), return 0;)		\
    /* Version 2 interface ends here. */  		\
    fbdy(uint32_t, define_displays, (OBJ struct Register_display *rd),return 0;) \
    /* Version 3 interface ends here. */  		\
    fbdy(uint32_t, fill_memory, (OBJ uint32_t adr, void *buf, \
	uint32_t amount, unsigned repeat_count, uint32_t context), \
	return 0;)	\
    /* Version 4 interface ends here. */  		\
    fbdy(uint32_t, instruction_trace_count, (OBJ0), return 0;)	\
    fbdy(void, get_instruction_traces, (OBJ unsigned *traces), return;) \
    fbdy(void, receive_callback, (OBJ RISCV_callback*), return;)	\
    fbdy(uint32_t, supports_feature, (OBJ0), return 0;)	\
    fbdy(unsigned, data_exchange,			\
    	(OBJ unsigned what_to_do, unsigned caller_endian, \
    	unsigned send_amount, void *send_buf,		\
        unsigned max_receive_amount, void *receive_buf), return 0;)	\
    fbdy(uint32_t, in_same_process_as_debugger, (OBJ0), return 1; )	\
    fbdy(unsigned, max_data_exchange_transfer, (OBJ0), return 0xfffffffe; )	\
    fbdy(uint32_t, read_banked_reg, \
    	(OBJ uint32_t bank, uint32_t r, uint32_t *value), return 0;) \
    fbdy(uint32_t, write_banked_reg, \
    	(OBJ uint32_t bank, uint32_t r, uint32_t *value), return 0;) \
    /* Replacement watchpoint functions. */				\
    fbdy(uint32_t, set_mem_watchpoint2, (OBJ RISCV_ADDR_TYPE addr, uint32_t length, unsigned options, void **cookie), return 0; ) \
    /* Version 5 interface ends here. */                \



// Registers 0..63 are core; 64.. are aux.
enum {
	reg_LP_COUNT = 60,
	AUX_BASE = 64,
	reg_STATUS = AUX_BASE, reg_SEMAPHORE, reg_LP_START, reg_LP_END,
	reg_IDENTITY, reg_DEBUG,
	// RISCVompact registers only are designated with reg_AC.
	reg_AC_PC,		                                  // RISCVompact 32-bit PC register.
	reg_ADCR = AUX_BASE + 0x7,                             // Actionpoint Data Comparison Register (only RISCVtangent-A4 and RISCVtangent-A5)
	reg_APCR,   	                                      // Actionpoint Program Counter Register (only RISCVtangent-A4 and RISCVtangent-A5)
	reg_ACR, 		                                  // Actionpoint Control Register (only RISCVtangent-A4 and RISCVtangent-A5)
	reg_AC_STATUS32,	                                  // RISCVompact status register
	reg_AC_STATUS32_L1,	                              // RISCVompact status register for uint32_t level 1
	reg_AC_STATUS32_L2,	                              // RISCVompact status register for uint32_t level 2
	reg_ICACHE = AUX_BASE + 0x10,                          // Invalidate cache
	reg_MADI_build = AUX_BASE + 0x73,                     // MADI Build Configuration Register
	reg_AP_build = AUX_BASE + 0x76,                        // Actionpoint Build register
	reg_PMU_build = AUX_BASE + 0xF7,                       // PMU Build register
	reg_AP_AMV0 = AUX_BASE + 0x220,                        // Actionpoint 0 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM0 = AUX_BASE + 0x221,                        // Actionpoint 0 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC0 = AUX_BASE + 0x222,                         // Actionpoint 0 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV1 = AUX_BASE + 0x223,                        // Actionpoint 1 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM1 = AUX_BASE + 0x224,                        // Actionpoint 1 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC1 = AUX_BASE + 0x225,                         // Actionpoint 1 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV2 = AUX_BASE + 0x226,                        // Actionpoint 2 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM2 = AUX_BASE + 0x227,                        // Actionpoint 2 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC2 = AUX_BASE + 0x228,                         // Actionpoint 2 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV3 = AUX_BASE + 0x229,                        // Actionpoint 3 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM3 = AUX_BASE + 0x22A,                        // Actionpoint 3 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC3 = AUX_BASE + 0x22B,                         // Actionpoint 3 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV4 = AUX_BASE + 0x22C,                        // Actionpoint 4 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM4 = AUX_BASE + 0x22D,                        // Actionpoint 4 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC4 = AUX_BASE + 0x22E,                         // Actionpoint 4 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV5 = AUX_BASE + 0x22F,                        // Actionpoint 5 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM5 = AUX_BASE + 0x230,                        // Actionpoint 5 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC5 = AUX_BASE + 0x231,                         // Actionpoint 5 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV6 = AUX_BASE + 0x232,                        // Actionpoint 6 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM6 = AUX_BASE + 0x233,                        // Actionpoint 6 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC6 = AUX_BASE + 0x234,                         // Actionpoint 6 Control register (only RISCV600 and RISCV700)
	reg_AP_AMV7 = AUX_BASE + 0x235,                        // Actionpoint 7 Match Value register (only RISCV600 and RISCV700)
	reg_AP_AMM7 = AUX_BASE + 0x236,                        // Actionpoint 7 Match Mask register (only RISCV600 and RISCV700)
	reg_AP_AC7 = AUX_BASE + 0x237,                         // Actionpoint 7 Control register (only RISCV600 and RISCV700)

	// For read_banked_reg and write_banked_reg:
	reg_bank_core = 0,
	reg_bank_aux = 1,
	reg_bank_madi = 2
};

// MADI registers.  To read/write these, you must use read/write_banked_regs.
enum {
	reg_MCR = 0,  	// MADI control register.
	reg_MER = 1,	// These are unused in RISCV's code.
	reg_MID = 2,
	reg_MAE = 9
};


#if OEM_USE_OF_DEBUGGER_HEADER_FILES
#include "crint.h"
#else
#include "../../common/crint.h"
#endif
// This invokes the macro in crint.h to create either C or C++ declarations
// for the RISCV interface.
typedef struct RISCV_callback RISCV_callback;
create_interface_allow_bodies(RISCV)

typedef enum {
	MAP_INVALID = 0, 	// There was no entry in any valid map, and there
			// exists one or more maps.
			MAP_VALID = 1,	// This is a valid entry from a  map.
			MAP_THERE_IS_NO_MAP = 2,	// There are NO maps defined so we have
					// computed default values (typically: r/w,
					// 4-wide, noverify).
} Map_code;

struct Memory_info {
	// See Map_code above for interpreting the value.
	// Ensure a single byte for mapentry, despite different enum defaults
	// for various compilers.
	unsigned char /*Map_code*/ mapentry; // is this memory address in our map?
	// None of these fields have anything reliable if mapentry == 0.
	char width;		// typically 2 or 4.  1?
	char readonly;	// Is the memory read-only? (must use HW bkpts)
	char verify;	// request to verify writes.
};

#include <stdarg.h>

#define RISCV_endian_functions(func,OBJ0,OBJ)		\
    func(uint32_t, version, (OBJ0) )				\
    /* Get the endian of the computer executing this function.  \
       Bit 0 is 1 iff the endian is "little". */	\
    func(uint32_t, get_host_endian, (OBJ0))			\
    /* If get_host_endian() != "endian", 			\
       these two functions swap a 32-bit or 16-bit value. */	\
    func(unsigned, convert_long, (OBJ uint32_t endian, unsigned value)) \
    func(unsigned, convert_short, (OBJ uint32_t endian, unsigned value)) \
    /* Version 1 interface ends here. */		\

create_interface(RISCV_endian)

struct Debugger_access;

#define RISCV_callback_functions(func,OBJ0,OBJ) \
    func(uint32_t, version, (OBJ0) )				\
    /* This is the printf in the client.  Use this 	\
       if you want to avoid having a different printf 	\
       in your DLL implementation of the interface.	\
     */							\
    func(uint32_t, printf, (OBJ const char *format, ...)) 	\
    /* If you need to know on a memory read/write, you can ask \
       what the memory map says about the memory at 	\
       the given address.  If mapentry is 0, there is 	\
       information available.				\
     */							\
    func(void, meminfo, (OBJ unsigned addr, struct Memory_info *m))	\
    /* Version 1 interface ends here. */		\
    func(uint32_t, vprintf, (OBJ const char *format, va_list ap)) 	\
    /* Return whether the client has a "verbose" switch on. */ \
    func(uint32_t, get_verbose, (OBJ0))			\
    /* Version 2 interface ends here. */		\
    func(void, sleep, (OBJ unsigned milliseconds))	\
    func(uint32_t, in_same_process_as_debugger, (OBJ0))	\
    /* Get endian conversion object. */			\
    func(struct RISCV_endian*, get_endian, (OBJ0))	\
    /* Private for debugger use only. */		\
    func(void *, get_tcp_factory, (OBJ0))		\
    func(void *, get_process, (OBJ0))			\
    /* Version 3 interface ends here. */		\
    func(struct Debugger_access *, get_debugger_access, (OBJ0))\
    /* Version 4 interface ends here. */		\

create_interface(RISCV_callback)

// This function returns an RISCV object.  For a DLL, you must export
// a function by this name.
// On V.4, how can we have multiple DLLs each with the same initialization
// function?  Probably can't; must name the functions differently.
extern RISCV *get_RISCV_LE_simulator(),
*get_RISCV_BE_simulator(),
*get_RISCV_hardware();

#if __HIGHC__ && __cplusplus
class System; class Delayed_memory;
// The debugger obtains this additional information if
// additional_possibilities() has the string MetaWare in it.
struct sim_disRISCV {
	virtual char *disRISCV(unsigned addr, uint32_t *memory) = 0;
};
struct RISCV_simext;
struct Addr_counter;
struct Trace_history;
struct Additional_metaware_simulator_communication {
	virtual void set_disRISCV(sim_disRISCV *dp) = 0;
	virtual void set_system(System*) = 0;
	virtual void set_simulator_extensions(RISCV_simext **) = 0;
	virtual void set_memory(Delayed_memory *) = 0;
	virtual void new_simulator_extension_found(RISCV_simext *) = 0;
	virtual void each_addr_counter() -> (Addr_counter*) = 0;
	virtual void set_early_hostlink(uint32_t) = 0;
	virtual Trace_history *get_trace_history() = 0;
};
struct Additional_metaware_hardware_communication {
	virtual void set_system(System*) = 0;
};
#endif

#if TEST
/////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------//
// 			Appendix Sample Implementation.		       //
// 	       A sample construction of a dummy DLL in MS C++.         //
//---------------------------------------------------------------------//
/////////////////////////////////////////////////////////////////////////
// Dummy implementation of the simulator that implements a single
// hardware breakpoint.
// This is loadable as a DLL.
// #include "riscvint.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
enum { halt_bit = (1 << 25) };
#define _24_bits 0xffffff
#if __HIGHC__
#define override _Override
#else
#define override 	// No protection in MSC.
#endif

static char trace = 0;

struct My_RISCV : RISCV {
	My_RISCV() {
#if __HIGHC__ // Much more readable big number:
		memsize = 1_000_000;
#else
		memsize = 1000000;
#endif
		mem = (char*)malloc(memsize);
		bpreg = 0;
		status = debug = 0;
		stepcnt = 0;
	}
	unsigned status, debug;
	unsigned stepcnt;
	uint32_t memsize;
	char *mem;
	// Breakpoint register.
	uint32_t bpreg;
	override uint32_t MS_CDECL version() { return RISCVINT_TRACE_VERSION + 1; }
	override const char  *MS_CDECL id() { return "dummy implementation"; }
	override void MS_CDECL destroy() {
		// printf("destroy was called.\n");
		delete this;
	}
	override uint32_t MS_CDECL is_simulator() { return 1; }
	override uint32_t MS_CDECL step() {
		if (trace || stepcnt % 500 == 0)
			printf("[%d]step. PC %x\n", stepcnt, status);
		stepcnt++;
		// If at BP, set halted and return.
		if (((status << 2) & _24_bits) == bpreg) {
			status |= halt_bit;
			return 1;
		}
		status += 1;
		// status |= halt_bit;	// We get done immediately, so say we're halted.
		//status &= ~halt_bit;
		trace && printf("after step, status %x\n", status);
		// printf("trying sleep in callback\n");
		// callback->sleep(1000);
		return 1;
	}
	override uint32_t MS_CDECL run() { return 0; }	// Simulator can't run.
	override uint32_t MS_CDECL read_memory(uint32_t adr, void *buf,
		uint32_t amount, uint32_t context) {
		if (adr + amount > memsize) return 0;
		memcpy(buf, mem + adr, amount);
		return amount;
	}
	override uint32_t MS_CDECL write_memory(uint32_t adr, void *buf,
		uint32_t amount, uint32_t context) {
		// NOTE!!!
		// You must invalidate any caches after a memory write.  To invalidate the
		// icache on a standard RISCV, write 0 to aux register 16.
		if (adr + amount > memsize) return 0;
		memcpy(mem + adr, buf, amount);
		return amount;
	}
	override uint32_t MS_CDECL read_reg(uint32_t r, uint32_t *value) {
		if (r == reg_STATUS) {
			trace && printf("read status; it's %x\n", status);
			*value = status;
		}
		else if (r == reg_IDENTITY) *value = 6;
		else if (r == reg_DEBUG) *value = debug;
		else *value = 0x100 + r;
		return 1;
	}
	override uint32_t MS_CDECL write_reg(uint32_t r, uint32_t value) {
		if (r == reg_STATUS) {
			if ((status & halt_bit) == 0 && (value & halt_bit)) {
				// If running, just turn on halt bit.
				trace && printf("Just set the halt bit; before %x...\n", status);
				status |= halt_bit;
			}
			else status = value;
			trace && printf("write status with %x new value %x\n", value, status);
		}
		else if (r == reg_DEBUG) {
			trace && printf("writing %x to debug reg\n");
			if (value & 1) {
				// Single-step the processor.  Write nothing.
				// Whether it's instruction (value_IS) or cycle-step doesn't
				// matter for this simulator.
				return step();
			}
			debug = value;
			if (value & 2) {
				trace && printf("halt the processor!\n");
				status |= halt_bit;	// Halt the processor.
			}
		}
		return 1;
	}
	override unsigned MS_CDECL memory_size() { return memsize; }
	override uint32_t MS_CDECL set_memory_size(unsigned S) {
		free(mem);
		mem = (char*)malloc(memsize = S);
		return 1;
	}
	override const char * MS_CDECL additional_possibilities() {
		// Request that additional_information be called with parameter "which"
		// == 1 so we can supply addr counters.
		// return "adrcount";  // Not implemented yet.
		return 0;
	}

	override void *MS_CDECL additional_information(unsigned which) {
		switch (which) {
		case 1: {
			// Supply a zero-terminated array of addr counters.
			return 0;	// Not implemented yet.
		}
		}
		return 0;
	}
	override uint32_t MS_CDECL prepare_for_new_program(uint32_t will_download) {
		printf("I am preparing!\n"); return 1;
	}
	override uint32_t MS_CDECL process_property(const char *key, const char *value) {
		if (strcmp(key, "blast") == 0) {
			// You should implement blast here.  value is the file name.
			printf("I'm having a blast! -- filename %s\n", value);
			return 1;
		}
		return 0;	// No properties recognized.
	}
	// Breakpoints.  We provide one breakpoint register.
	override uint32_t MS_CDECL retrieve_breakpoint_code(unsigned address,
		char *dest, unsigned len, void *cookie_buf) {
		// We don't overwrite the code, so no need to do anything here.  
		return 1;
	}

	override uint32_t MS_CDECL at_breakpoint() {
		unsigned pcval = (status << 2)&_24_bits;
		return pcval == bpreg;
	}
	override uint32_t MS_CDECL set_breakpoint(unsigned addr, void *cookie) {
		if (bpreg == 0) { bpreg = addr; return 1; }
		else return 0;	// Couldn't set: only 1 bp avail.
	}
	override uint32_t MS_CDECL remove_breakpoint(unsigned addr, void *cookie) {
		if (addr == bpreg) { bpreg = 0; return 1; }
		else return 0;	// No BP here.
	}
	override uint32_t MS_CDECL breakpoint_cookie_len() { return 0; }
	override void MS_CDECL receive_callback(RISCV_callback*cb) {
		callback = cb;
	}
	override unsigned MS_CDECL data_exchange(
		unsigned what_to_do, unsigned caller_endians,
		unsigned send_amount, void *send_buf,
		unsigned max_receive_amount, void *receive_buf) {
		printf("in data exchange.\n");
		if (what_to_do == 1) {
			unsigned amt = max_receive_amount;
			if (send_amount < amt) amt = send_amount;
			memcpy(receive_buf, send_buf, amt);
			return amt;
		}
		else return 999;
	}
	RISCV_callback *callback;
};

extern
// _MSC_VER is microsoft's compiler's define.
#if _MSNT || _MSC_VER	// Microsoft NT.
"C" __declspec(dllexport)
#endif
RISCV *get_RISCV_interface() {
	RISCV *p = new My_RISCV();
	// Sleep for a long time.
	//extern "C" void _Dstdcall Sleep(long);
	//Sleep(5000);
	return p;
}
#endif
/*
// This can be built with MetaWare's High C and the command:
// 	hc RISCVint.c -mslink -Hdll
// Or with Microsoft C's command:
// 	cl /LD RISCVint.c
// But you should be able to use any W95 C compiler to do this.
// Be sure that the __declspec(...) above gets enabled.  Currently
// it's enabled for just High C on W95/NT and MSC; it has to remain
// off for UNIX, of course.
// On UNIX, use the command:
//	cc RISCVint.c -G -o RISCVint.so
*/
#endif
