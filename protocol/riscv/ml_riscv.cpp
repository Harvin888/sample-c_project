#include <stdio.h>
#include "ml_riscv.h"
#include "../../protocol/common/drvopxd.h"
#include "../../protocol/common/drvopxdriscv.h"
#include "../../firmware/export/api_def.h"

//andre: needed for maximum buffer size of 16KB
#ifndef __LINUX
#include "../common/libusb_dyn.h"
#else
#include "usb.h"
#endif
#include "../../protocol/common/drvopxdcom.h"

#ifndef __LINUX
//Win specific defines
#include "windows.h"
#include "..\..\utility\debug\debug.h"
#else
// Linux specific defines
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "../../utility/debug/debug.h"
#endif
// defines for status proc
#define IS_RUNNING                                          0
#define IS_HALTED											1
// default values
#define DEFAULT_RISCV_DELAY_AFTER_RESET			            100               // 100 ms default delay after reset
#define DEFAULT_RISCV_RESET_DURATION						50                // 50 ms default reset pulse
#define MAX_RISCV_RESET_VALUE                               5000              // maximum 5s for delay and duration

#define DEFAULT_RISCV_RTCK_TIMEOUT                          50                // default timeout is 50 ms
#define MIN_RISCV_RTCK_TIMEOUT                              5                 // minimum timeout is 5 ms
#define MAX_RISCV_RTCK_TIMEOUT                              20000             // maximum timeout is 20 s

#define DEFAULT_ACCESS_TYPE                                 0x0400

// Opella-XD RISCV frequency limits and default values
#define MAX_RISCV_JTAG_AND_BLAST_FREQUENCY                    100.0             // maximum frequency is 100 MHz
#define MIN_RISCV_JTAG_AND_BLAST_FREQUENCY                    0.001             // minimum frequency is 1 kHz
#define DEFAULT_RISCV_JTAG_FREQUENCY                          30.0               // 1 MHz as default JTAG clock (safer)

// test scanchain
#define RISCV_TSTSC_BITS                                      8192               // number of bits for testing scanchain (MUST be div by 32) -> max 256 cores
#define RISCV_TSTSC_WORDS                                     (RISCV_TSTSC_BITS / 32) 

// local variables
#define DLLPATH_LENGTH                                      (_MAX_PATH)
char pszGlobalDllPath[DLLPATH_LENGTH];                                        // contains DLL path
// buffer for blasting

// local functions
static int CompareArrayOfWords(uint32_t *pulArray1, uint32_t *pulArray2, uint32_t ulNumberOfBits);
static void ShiftArrayOfWordsRight(uint32_t *pulArray, uint32_t ulWords);

/****************************************************************************
	 Function: StringToLwr
	 Engineer: 
		Input: char *pszString - string to convert
	   Output: none
  Description: Convert string to lower case.
Date           Initials    Description
07-Dec-2007    AG        Initial
*****************************************************************************/
void StringToLwr(char *pszString)
{
	while (*pszString)
	{
		if ((*pszString >= 'A') && (*pszString <= 'Z'))
			*pszString += 'a' - 'A';
		pszString++;
	}
}

/****************************************************************************
	 Function: ML_GetListOfConnectedProbes
	 Engineer: 
		Input: char ppszInstanceNumbers[][16] - pointer to list for instance numbers

	   Output: none
  Description: get list of connected Opella-XD probe (list of serial numbers)
Date           Initials    Description
16-Jul-2006    AG          Initial
****************************************************************************/
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances)
{
	unsigned short usIndex;
	unsigned short usNumberOfDevices = 0;
	char ppszLocalInstanceNumber[MAX_DEVICES_SUPPORTED][16];

	//check connected Opella-XDs
	DL_OPXD_GetConnectedDevices(ppszLocalInstanceNumber,&usNumberOfDevices,OPELLAXD_USB_PID);

	//copy list of devices up to maximum number
	if (usNumberOfDevices > usMaxInstances)
		usNumberOfDevices = usMaxInstances;

	for (usIndex = 0; usIndex < usNumberOfDevices; usIndex++)
	{
		strcpy(ppszInstanceNumbers[usIndex], ppszLocalInstanceNumber[usIndex]);

		if (pusConnectedInstances != NULL)
			*pusConnectedInstances = usNumberOfDevices;
	}
}

/****************************************************************************
	 Function: ML_RISCVParamsInit
	 Engineer: Rejeesh Shaji Babu
		Input: TyRISCVTargetParams* ptyRISCVParams - structure to initialise
			   TyDevHandle tyDevHandle - handle to open device
			   TyDeviceInfo* ptyDeviceInfo - pointer to device info structure
			   bool bMCconfig - flag to decide whether Opella-XD MC configuration needs to be done
	   Output: none
  Description: Initialize RISCVParams struct
Date           Initials    Description
20-Jun-2019    RSB          Rejeesh
****************************************************************************/
void ML_RISCVParamsInit(TyRISCVTargetParams* ptyRISCVParams, TyDevHandle tyDevHandle, TyDeviceInfo* ptyDeviceInfo, bool bMCconfig)
{
	uint32_t ulIndex;

	// obtain information about device
	if (DL_OPXD_GetDeviceInfo(tyDevHandle, ptyDeviceInfo, DW_RISCV) == DRVOPXD_ERROR_NO_ERROR)
	{  // fill information to local structure
		ptyRISCVParams->tyProbeFirmwareInfo.ulVersion = ptyDeviceInfo->ulFirmwareVersion;
		ptyRISCVParams->tyProbeFirmwareInfo.ulDate = ptyDeviceInfo->ulFirmwareDate;
		ptyRISCVParams->tyProbeDiskwareInfo.ulVersion = ptyDeviceInfo->ulDiskwareVersion;
		ptyRISCVParams->tyProbeDiskwareInfo.ulDate = ptyDeviceInfo->ulDiskwareDate;
		ptyRISCVParams->tyProbeFPGAwareInfo.ulVersion = ptyDeviceInfo->ulFpgawareVersion;
		ptyRISCVParams->tyProbeFPGAwareInfo.ulDate = ptyDeviceInfo->ulFpgawareDate;
	}
	else
	{
		ptyRISCVParams->tyProbeFirmwareInfo.ulVersion = 0x0;
		ptyRISCVParams->tyProbeFirmwareInfo.ulDate = 0x0;
		ptyRISCVParams->tyProbeDiskwareInfo.ulVersion = 0x0;
		ptyRISCVParams->tyProbeDiskwareInfo.ulDate = 0x0;
		ptyRISCVParams->tyProbeFPGAwareInfo.ulVersion = 0x0;
		ptyRISCVParams->tyProbeFPGAwareInfo.ulDate = 0x0;
	}

	//prepare multicore settings
	ptyRISCVParams->ulCurrentRISCVTap = 0;
	ptyRISCVParams->ulCurrentRISCVCore = 0;
	ptyRISCVParams->ulNumberOfRISCVTaps = 0;
	ptyRISCVParams->ulHartId = 0;

	for (ulIndex = 0; ulIndex < ptyRISCVParams->ulNumberOfTapsOnScanchain; ulIndex++)
	{
		if (ptyRISCVParams->tyScanchainTapParams[ulIndex].bRISCVTap)
		{
			ptyRISCVParams->tyRISCVTapParams[ptyRISCVParams->ulCurrentRISCVTap].ulScanchainTap = ulIndex;        // link RISCV core with particular core on scanchain
			ptyRISCVParams->tyRISCVTapParams[ptyRISCVParams->ulCurrentRISCVTap].ulTapVersion = 0x0;
			ptyRISCVParams->tyRISCVTapParams[ptyRISCVParams->ulCurrentRISCVTap].usAccessType = DEFAULT_ACCESS_TYPE;
			ptyRISCVParams->ulCurrentRISCVTap++;
		}
	}

	ptyRISCVParams->ulNumberOfRISCVTaps = ptyRISCVParams->ulCurrentRISCVTap;
	ptyRISCVParams->ulCurrentRISCVTap = 0;                                              // starting with first RISCV core

	if (!ptyRISCVParams->ulNumberOfRISCVTaps)
	{  // we could not find RISCV core, in that case, use first core
		ptyRISCVParams->tyRISCVTapParams[0].ulScanchainTap = 0;
		ptyRISCVParams->tyRISCVTapParams[0].ulTapVersion = 0x0;
		ptyRISCVParams->tyRISCVTapParams[0].usAccessType = DEFAULT_ACCESS_TYPE;
		ptyRISCVParams->ulNumberOfRISCVTaps = 1;
	}

	if (true == bMCconfig)
	{
		if (ptyRISCVParams->ulNumberOfTapsOnScanchain > 1)
		{  // more than 2 cores, we need to configure probe
			TyTAPConfig ptyScanchainConfig[MAX_RISCV_TAPS];

			// create structure with core information
			for (ulIndex = 0; ulIndex < ptyRISCVParams->ulNumberOfTapsOnScanchain; ulIndex++)
			{
				ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyRISCVParams->tyScanchainTapParams[ulIndex].ulIRLength);
				ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;               // does not matter
				ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(ptyRISCVParams->tyScanchainTapParams[ulIndex].ulBypassInst);    // bypass code
			}
			(void)DL_OPXD_JtagConfigMulticore(tyDevHandle, ptyRISCVParams->ulNumberOfTapsOnScanchain, ptyScanchainConfig);
		}
		else
			(void)DL_OPXD_JtagConfigMulticore(tyDevHandle, 0, NULL);                 // do not set MC support, we have just 1 core
	}

	// initialize rest of items in structure
	ptyRISCVParams->bCycleStep = FALSE;
	ptyRISCVParams->ulDelayAfterReset = DEFAULT_RISCV_DELAY_AFTER_RESET;
	ptyRISCVParams->ulResetDuration = DEFAULT_RISCV_RESET_DURATION;
	// we came up here so initialization has been successful
	ptyRISCVParams->bConnectionInitialized = 1;
	ptyRISCVParams->iCurrentHandle = (int)tyDevHandle;
	// initialize last error
	ptyRISCVParams->iLastErrorCode = ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_ConfigureOpellaXD
	 Engineer: Rejeesh Shaji Babu
		Input: TyDevHandle tyDevHandle - handle to open device
			   TyDeviceInfo* ptyDeviceInfo - pointer to device info structure
			   bool bConfigureFpga - flag to decide whether to configure FPGA or not
			                         Do not confgiure FPGA if another diskware is already running
	   Output: status - err/success
  Description: Configure Opella-XD (load & configure FPGA, load and execute diskware)
Date           Initials    Description
20-Jun-2019    RSB          Rejeesh
****************************************************************************/
int ML_ConfigureOpellaXD(TyDevHandle tyDevHandle, TyDeviceInfo* ptyDeviceInfo, bool bConfigureFpga)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	unsigned int uiTPARev;

	ptyDeviceInfo->tyTpaConnected = TPA_NONE;
	if ((DL_OPXD_UnconfigureDevice(tyDevHandle) != DRVOPXD_ERROR_NO_ERROR) ||
		(DL_OPXD_GetDeviceInfo(tyDevHandle, ptyDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR))
	{
		(void)DL_OPXD_CloseDevice(tyDevHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}
	if (ptyDeviceInfo->tyTpaConnected != TPA_ARC20)
	{
		(void)DL_OPXD_CloseDevice(tyDevHandle);
		return ERR_ML_RISCV_PROBE_INVALID_TPA;
	}

	if (!strcmp(pszGlobalDllPath, "")) //empty path
	{
		uiTPARev = (unsigned int)(((ptyDeviceInfo->ulTpaRevision) & 0xFF000000) >> 24);
		switch (uiTPARev)
		{
		case 1: //R1 TPA
			if (true == bConfigureFpga)
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000) //R2 Opella-XD				
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_FILENAME, OPXD_FPGAWARE_RISCV_TPA_R1_FILENAME);
				else //R3 Opella-XD
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_LPC1837_FILENAME, OPXD_FPGAWARE_RISCV_LPC1837_FILENAME);
			}
			else
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000) //R2 Opella-XD				
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_FILENAME, "");
				else //R3 Opella-XD
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_LPC1837_FILENAME, "");
			}
			break;
		case 0: // Old TPA ?
		default:
			if (true == bConfigureFpga)
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_FILENAME, OPXD_FPGAWARE_RISCV_FILENAME);
				else
					return ERR_ML_RISCV_USB_DRIVER_ERROR;
			}
			else
			{
				if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
					tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, OPXD_DISKWARE_RISCV_FILENAME, "");
				else
					return ERR_ML_RISCV_USB_DRIVER_ERROR;
			}
		}
	}
	else //path not empty
	{
		char pszLocDiskwareName[_MAX_PATH];
		char pszLocFpgawareName[_MAX_PATH];
		strcpy(pszLocDiskwareName, pszGlobalDllPath);
#ifdef __LINUX
		strcat(pszLocDiskwareName, "/");
#else
		strcat(pszLocDiskwareName, "\\");
#endif
		if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
			strcat(pszLocDiskwareName, OPXD_DISKWARE_RISCV_FILENAME);
		else
			strcat(pszLocDiskwareName, OPXD_DISKWARE_RISCV_LPC1837_FILENAME);

		// create full fpgaware filename
		strcpy(pszLocFpgawareName, pszGlobalDllPath);
#ifdef __LINUX
		strcat(pszLocFpgawareName, "/");
#else
		strcat(pszLocFpgawareName, "\\");
#endif
		uiTPARev = (unsigned int)(((ptyDeviceInfo->ulTpaRevision) & 0xFF000000) >> 24);
		switch (uiTPARev)
		{
		case 1:
			if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
				strcat(pszLocFpgawareName, OPXD_FPGAWARE_RISCV_TPA_R1_FILENAME);
			else
				strcat(pszLocFpgawareName, OPXD_FPGAWARE_RISCV_LPC1837_FILENAME);
			break;
		case 0:
		default:
			if (ptyDeviceInfo->ulBoardRevision < 0x03000000)
				strcat(pszLocFpgawareName, OPXD_FPGAWARE_RISCV_FILENAME);
			else
				return ERR_ML_RISCV_USB_DRIVER_ERROR;
			break;
		}
		if (true == bConfigureFpga) 
		{
			tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, pszLocDiskwareName, pszLocFpgawareName);
		}
		else
		{
			tyRes = DL_OPXD_ConfigureDevice(tyDevHandle, DW_RISCV, FPGA_RISCV, pszLocDiskwareName, "");
		}		
	}
	
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyDevHandle);
		return ERR_ML_RISCV_PROBE_CONFIG_ERROR;
	}
	return tyRes;
}

/****************************************************************************
	 Function:			ML_OpenConnection
	 Engineer:
	 Input:				char ppszInstanceNumbers[][16] - pointer to list for instance numbers

	 Output:			none
     Description:		open the connection
	 Date          Initials    Description
	 16-Jul-2006                Initial
****************************************************************************/
int ML_OpenConnection(TyRISCVTargetParams *ptyRISCVParams)
{
	TyDeviceInfo tyLocDeviceInfo;
	TyDevHandle tyLocHandle = MAX_DEVICES_SUPPORTED;
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;

	//check parameters
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	if (ptyRISCVParams->bConnectionInitialized)
		return ERR_ML_RISCV_ALREADY_INITIALIZED;

	//lets try to open device with serial number from structure to get a handler
	if (DL_OPXD_OpenDevice(ptyRISCVParams->pszProbeInstanceNumber, OPELLAXD_USB_PID, false, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_PROBE_NOT_AVAILABLE;

	//Opella-XD R2 FW/DW still dont support CMD_CODE_DISKWARE_STATUS command, so dont use that for R2
	if (DL_OPXD_GetDeviceInfo(tyLocHandle, &tyLocDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyLocHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}

	if (tyLocDeviceInfo.ulBoardRevision < 0x03000000)
	{
		//R2
		tyLocDeviceInfo.uiActiveDiskwares = 0;
		tyLocDeviceInfo.bOtherDiskwareRunning = 0;
		(void)DL_OPXD_TerminateDW(tyLocHandle, RISCV);
	}
	else
	{
		//Check if diskware is already running (could be that its loaded by another instance of GDB server)
		if (DL_OPXD_GetDiskwareStatus(tyLocHandle, &tyLocDeviceInfo, RISCV) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
		}
	}

	if (tyLocDeviceInfo.uiActiveDiskwares > 0)
	{
		//RISC-V diskware is already running.
		fprintf(stdout, "RISC-V diskware (%d) is already running in Opella-XD, hence skipping probe initialization.\n", tyLocDeviceInfo.uiActiveDiskwares);
		fflush(stdout);

		//So just do local struct inits
		ML_RISCVParamsInit(ptyRISCVParams, tyLocHandle, &tyLocDeviceInfo, false);

		// Call CMD_CODE_EXECUTE_DISKWARE so that Opella-XD firmware is aware that diskware is being used.
		if (DL_OPXD_ExecuteDW(tyLocHandle, RISCV) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
		}
		return ERR_ML_RISCV_NO_ERROR;
	}
	else if (tyLocDeviceInfo.bOtherDiskwareRunning)
	{
		//Some other diskware is running.
		fprintf(stdout, "\nSome other diskware is running in Opella-XD, hence skipping probe initialization.\n");
		fflush(stdout);

		//Load and execute RISCV diskware, no FPGA config, JTAG freq setting, VTPA setting etc
		if (ML_ConfigureOpellaXD(tyLocHandle, &tyLocDeviceInfo, false) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
		}

		// Just do local struct inits
		// NOTE: Since another diskware is running, MC config is not done. 
		// This is assuming that other diskware should have done MC config and that remains same for this also. e.g.: controllerx soc
		ML_RISCVParamsInit(ptyRISCVParams, tyLocHandle, &tyLocDeviceInfo, false);

		return ERR_ML_RISCV_NO_ERROR;
	}
	else
	{
		// No diskware running in target, so do all stuff like FPGA config, diskware load, JTAG freq, VTPA set etc...
		if (ML_ConfigureOpellaXD(tyLocHandle, &tyLocDeviceInfo, true) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
		}

		ML_RISCVParamsInit(ptyRISCVParams, tyLocHandle, &tyLocDeviceInfo, true);

		// ok, probe is configured, with correct TPA, etc. so let prepare for debug session with default settings
		// set target type, TMS sequence, default JTAG frequency, no multicore, fixed voltage of 3.3V
		ptyRISCVParams->dJtagFrequency = DEFAULT_RISCV_JTAG_FREQUENCY;
		ptyRISCVParams->ulAdaptiveClockTimeout = DEFAULT_RISCV_RTCK_TIMEOUT;

		if (!ptyRISCVParams->bAdaptiveClockEnabled)
			tyRes = DL_OPXD_JtagSetFrequency(tyLocHandle, ptyRISCVParams->dJtagFrequency, NULL, NULL);              // set default fixed frequency
		else                                                                                                     // else set adaptive frequency
			tyRes = DL_OPXD_JtagSetAdaptiveClock(tyLocHandle, ptyRISCVParams->ulAdaptiveClockTimeout, ptyRISCVParams->dJtagFrequency);

		if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_TerminateDW(tyLocHandle, RISCV);
			(void)DL_OPXD_UnconfigureDevice(tyLocHandle);
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_PROBE_CONFIG_ERROR;
		}

		if (DL_OPXD_JtagSetTMS(tyLocHandle, NULL, NULL, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_TerminateDW(tyLocHandle, RISCV);
			(void)DL_OPXD_UnconfigureDevice(tyLocHandle);
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_PROBE_CONFIG_ERROR;
		}

		// depending on target type
		tyRes = DRVOPXD_ERROR_NO_ERROR;
		switch (ptyRISCVParams->tyRISCVTargetType)
		{
		case ARC_TGT_JTAG_TPA_R1:
			tyRes = DL_OPXD_RISCV_SelectTarget(tyLocHandle, RISCV_JTAG_TPA_R1);
			if (tyRes != DRVOPXD_ERROR_NO_ERROR)
			{
				(void)DL_OPXD_TerminateDW(tyLocHandle, RISCV);
				(void)DL_OPXD_UnconfigureDevice(tyLocHandle);
				(void)DL_OPXD_CloseDevice(tyLocHandle);
				//added if the target is not connected or not powered on
				if (tyRes == DRVOPXD_ERROR_RISCV_DEBUG_POFF)
					return ERR_ML_RISCV_DEBUG_POFF;
				else
					return ERR_ML_RISCV_PROBE_CONFIG_ERROR;
			}
			break;
		default:
			break;
		}

		tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 1, 0);
		if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_TerminateDW(tyLocHandle, RISCV);
			(void)DL_OPXD_UnconfigureDevice(tyLocHandle);
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_PROBE_CONFIG_ERROR;
		}

		// wait to stabilize voltage
		ML_Sleep(ptyRISCVParams, 400); // 400 ms
		// enable drivers

		tyRes = DL_OPXD_SetVtpaVoltage(tyLocHandle, 3.3, 1, 1);
		if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_TerminateDW(tyLocHandle, RISCV);
			(void)DL_OPXD_UnconfigureDevice(tyLocHandle);
			(void)DL_OPXD_CloseDevice(tyLocHandle);
			return ERR_ML_RISCV_PROBE_CONFIG_ERROR;
		}

		ML_Sleep(ptyRISCVParams, 500);
	}
	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_UpgradeFirmware
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   PfPrintFunc pfPrintFunc - print message function
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: open connection and verify firmware version
			   automatically upgrade if necessary
Date           Initials    Description
31-Oct-2007    AG          Initial
****************************************************************************/
int ML_UpgradeFirmware(TyRISCVTargetParams *ptyRISCVParams, PfPrintFunc pfPrintFunc)
{
	char          pszLocFirmwareName[_MAX_PATH];
	char          pszLocFlashUtilName[_MAX_PATH];
	int           iFirmwareDiff = 0;
	unsigned char bReconnectNeeded = 0;
	TyError tyResult = DRVOPXD_ERROR_NO_ERROR;

	TyDeviceInfo tyDeviceInfo;

	TyDevHandle tyLocHandle = MAX_DEVICES_SUPPORTED;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	if (ptyRISCVParams->bConnectionInitialized)
		return ERR_ML_RISCV_ALREADY_INITIALIZED;

	// let try to open device with serial number from structure to get a handle
	if (DL_OPXD_OpenDevice(ptyRISCVParams->pszProbeInstanceNumber, OPELLAXD_USB_PID, false, &tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_PROBE_NOT_AVAILABLE;

	// got handle so do configuration steps
	if (DL_OPXD_UnconfigureDevice(tyLocHandle) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyLocHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}

	// get firmware full path
	tyResult = DL_OPXD_GetDeviceInfo(tyLocHandle, &tyDeviceInfo, DW_UNKNOWN);
	switch (tyResult)
	{
	case DRVOPXD_ERROR_NO_ERROR:
		tyResult = ERR_ML_RISCV_NO_ERROR;
		break;
	case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
		tyResult = ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED;
		break;
	case DRVOPXD_ERROR_RISCV_DEBUG_ACCESS:
		tyResult = ERR_ML_RISCV_DEBUG_ACCESS;
		break;
	case DRVOPXD_ERROR_RISCV_DEBUG_PDOWN:
		tyResult = ERR_ML_RISCV_DEBUG_PDOWN;
		break;
	case DRVOPXD_ERROR_RISCV_DEBUG_POFF:
		tyResult = ERR_ML_RISCV_DEBUG_POFF;
		break;
	case DRVOPXD_ERROR_RISCV_TARGET_RESET_OCCURED:
		tyResult = ERR_ML_RISCV_TARGET_RESET_DETECTED;
		break;
	default:
		tyResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
		break;
	}
		//return DRVOPXD_ERROR_USB_DRIVER;
	if (tyResult != DRVOPXD_ERROR_NO_ERROR)
		return tyResult;

	if (!strcmp(pszGlobalDllPath, ""))
	{
		if (tyDeviceInfo.ulBoardRevision < 0x03000000)
			strcpy(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);
		else
		{
			strcpy(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME_LPC1837);
			strcpy(pszLocFlashUtilName, OPXD_FLASHUTIL_FILENAME_LPC1837);
		}
	}
	else
	{     // there is stored some path so append \ and filenames
		strcpy(pszLocFirmwareName, pszGlobalDllPath);
		strcpy(pszLocFlashUtilName, pszGlobalDllPath);

#ifdef __LINUX
		strcat(pszLocFirmwareName, "/");
		strcat(pszLocFlashUtilName, "/");
#else
		strcat(pszLocFirmwareName, "\\");
		strcat(pszLocFlashUtilName, "\\");
#endif

		if (tyDeviceInfo.ulBoardRevision < 0x03000000)
			strcat(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME);
		else 
		{
			strcat(pszLocFirmwareName, OPXD_FIRMWARE_FILENAME_LPC1837);
			strcat(pszLocFlashUtilName, OPXD_FLASHUTIL_FILENAME_LPC1837);
		}
	}
	// check firmware version if we need to upgrade
	if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_CHECK_ONLY, pszLocFirmwareName, pszLocFlashUtilName, &bReconnectNeeded, &iFirmwareDiff, NULL, NULL, ptyRISCVParams->pszProbeInstanceNumber, pfPrintFunc) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyLocHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}
	if (iFirmwareDiff == 0)
	{
		// great, probe has same firmware as host pc, so close handle and return
		(void)DL_OPXD_CloseDevice(tyLocHandle);
		return ERR_ML_RISCV_NO_ERROR;
	}
	// we need to upgrade firmware
	bReconnectNeeded = 0;
	if (tyDeviceInfo.ulBoardRevision < 0x03000000)
		printf("Updating Opella-XD firmware...\n");
	if (DL_OPXD_UpgradeDevice(tyLocHandle, UPG_DIFF_VERSION, pszLocFirmwareName, pszLocFlashUtilName, &bReconnectNeeded, &iFirmwareDiff, NULL, NULL, ptyRISCVParams->pszProbeInstanceNumber, pfPrintFunc) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(tyLocHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}
	if (bReconnectNeeded)
	{
		if (pfPrintFunc != NULL)
			pfPrintFunc("\tResetting and reconnecting to Opella-XD.\n");
		unsigned int uiCnt;
		// wait for 7 seconds to reconnect probe
		for (uiCnt = 0; uiCnt < 7; uiCnt++)
			ML_Sleep(ptyRISCVParams, 1000);                                        // 100 ms
	}
	else
		(void)DL_OPXD_CloseDevice(tyLocHandle);                                 // close handle

	if (pfPrintFunc != NULL)
		pfPrintFunc("Opella-XD firmware has been updated.\n");
	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_CloseConnection
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: close connection to debugger
			   Must be called as last of ML_ functions.
Date           Initials    Description
16-Jul-2006    AG          Initial
****************************************************************************/
int ML_CloseConnection(TyRISCVTargetParams *ptyRISCVParams)
{
	int iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	// if device is already closed, just return
	if (!ptyRISCVParams->bConnectionInitialized || (ptyRISCVParams->iCurrentHandle >= MAX_DEVICES_SUPPORTED))
		return ERR_ML_RISCV_NO_ERROR;

	iResult = DL_OPXD_TerminateDW(ptyRISCVParams->iCurrentHandle, RISCV);
	if (iResult != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(iResult);

	(void)DL_OPXD_CloseDevice(ptyRISCVParams->iCurrentHandle);
	ptyRISCVParams->bConnectionInitialized = 0;                            // connection is closed
	ptyRISCVParams->iCurrentHandle = MAX_DEVICES_SUPPORTED;                // handle to usb is now invalid

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_TestScanchain
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned char bVerifyNumberOfTAPs - verify number of TAPs with MC settings
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: run scanchain loopback test
Date           Initials    Description
22-Oct-2007    AG          Initial
****************************************************************************/
int ML_TestScanchain(TyRISCVTargetParams *ptyRISCVParams, unsigned char bVerifyNumberOfTAPs)
{
	uint32_t ulIndex;
	unsigned char bPatternMatch = 0;
	int iResult = ERR_ML_RISCV_NO_ERROR;
	// check parameters
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	// testing scanchain is based on putting all cores into bypass mode and shifting different patterns via DR
	// all output patterns should correspond input patterns shifted by number of cores on scanchain (apply also if there is no core)
	if (!ptyRISCVParams->bDoNotIssueTAPReset)
	{
		// 1st step - reset TAPs using TMS sequence
		if (DL_OPXD_JtagResetTAP(ptyRISCVParams->iCurrentHandle, 0) != DRVOPXD_ERROR_NO_ERROR)
			iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
	}
	// 2nd step - disable MC
	(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, 0, NULL);
	// 3rd step - prepare patterns
	if (iResult == ERR_ML_RISCV_NO_ERROR)
	{
		TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
		uint32_t pulDRPatternOut1[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternOut2[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternOut3[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternOut4[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternIn1[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternIn2[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternIn3[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternIn4[RISCV_TSTSC_WORDS];
		// prepare test patterns
		for (ulIndex = 0; ulIndex < RISCV_TSTSC_WORDS; ulIndex++)
		{
			pulDRPatternOut1[ulIndex] = 0xDB6DB6DB;   pulDRPatternOut2[ulIndex] = 0x24924924;
			pulDRPatternIn1[ulIndex] = 0x0;           pulDRPatternIn2[ulIndex] = 0x0;
			pulDRPatternOut3[ulIndex] = 0xAAAAAAAA;   pulDRPatternOut4[ulIndex] = 0x55555555;
			pulDRPatternIn3[ulIndex] = 0x0;           pulDRPatternIn4[ulIndex] = 0x0;
		}
		// do 4 scans (place all cores into bypass, shift 1st pattern into DR, shift 2nd pattern into DR and go back to bypass
		tyRes = DL_OPXD_JtagScanIR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, NULL, NULL);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, pulDRPatternOut1, pulDRPatternIn1);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, pulDRPatternOut2, pulDRPatternIn2);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, pulDRPatternOut3, pulDRPatternIn3);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, pulDRPatternOut4, pulDRPatternIn4);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanIR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, NULL, NULL);
		switch (tyRes)
		{
		case DRVOPXD_ERROR_NO_ERROR:        // no error, just go ahead
			break;
		case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
			iResult = ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED;
			break;
		default:                            // any error in driver
			iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
			break;
		}

		if (iResult == ERR_ML_RISCV_NO_ERROR)
		{  // verify results
			uint32_t ulNumberOfBits = RISCV_TSTSC_BITS;
			// check if pattern matches (test up to supported number of cores)
			for (ulIndex = 0; ulIndex < (MAX_RISCV_TAPS + 1); ulIndex++)
			{
				if (!CompareArrayOfWords(pulDRPatternOut1, pulDRPatternIn1, ulNumberOfBits) &&
					!CompareArrayOfWords(pulDRPatternOut2, pulDRPatternIn2, ulNumberOfBits) &&
					!CompareArrayOfWords(pulDRPatternOut3, pulDRPatternIn3, ulNumberOfBits) &&
					!CompareArrayOfWords(pulDRPatternOut4, pulDRPatternIn4, ulNumberOfBits)
					)
					bPatternMatch = 1; // found pattern match
				else
				{
					bPatternMatch = 0;
				}

				if (bVerifyNumberOfTAPs && (ulIndex == ptyRISCVParams->ulNumberOfTapsOnScanchain))
					break;

				ShiftArrayOfWordsRight(pulDRPatternIn1, RISCV_TSTSC_WORDS);
				ShiftArrayOfWordsRight(pulDRPatternIn2, RISCV_TSTSC_WORDS);
				ShiftArrayOfWordsRight(pulDRPatternIn3, RISCV_TSTSC_WORDS);
				ShiftArrayOfWordsRight(pulDRPatternIn4, RISCV_TSTSC_WORDS);
				ulNumberOfBits--;
			}
		}
	}
	// 5th step - restore MC settings
	if (ptyRISCVParams->ulNumberOfTapsOnScanchain > 1)
	{  // more than 2 cores, we need to configure probe
		TyTAPConfig ptyScanchainConfig[MAX_RISCV_TAPS];
		uint32_t pulBypass[MAX_RISCV_TAPS];
		// create structure with core information
		for (ulIndex = 0; ulIndex < ptyRISCVParams->ulNumberOfTapsOnScanchain; ulIndex++)
		{
			pulBypass[ulIndex] = 0xFFFFFFFF;                                  // all 1's as bypass code
			ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyRISCVParams->tyScanchainTapParams[ulIndex].ulIRLength);
			ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;               // does not matter
			ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(pulBypass[ulIndex]);    // bypass code
		}
		(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulNumberOfTapsOnScanchain, ptyScanchainConfig);
	}
	else
		(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, 0, NULL);

	if (!ptyRISCVParams->bDoNotIssueTAPReset)
	{
		// 6th step - reset TAPs using TMS sequence (always)
		(void)DL_OPXD_JtagResetTAP(ptyRISCVParams->iCurrentHandle, 0);

	}
	if ((iResult == ERR_ML_RISCV_NO_ERROR) && !bPatternMatch)
		iResult = ERR_ML_RISCV_TEST_SCANCHAIN_FAILED;

	return iResult;
}


/****************************************************************************
	 Function: ML_DetectScanchain
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned int uiMaxCores - maximum number of cores supported
			   unsigned int *puiNumberOfCores - number of cores detected
			   uint32_t *pulIRWidth - pointer to array with IR length for each core
			   unsigned char *pbRISCVCCores - pointer to array of boolean if particular core is RISCV
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: Detect scanchain structure and decide about number of cores on scanchain.
Date           Initials    Description
06-Oct-2007    VH          Initial
****************************************************************************/
int ML_DetectScanchain(TyRISCVTargetParams *ptyRISCVParams, unsigned int uiMaxCores, unsigned int *puiNumberOfCores,
	uint32_t *pulIRWidth, unsigned char *pbRISCVCores)
{
	int           iResult = ERR_ML_RISCV_NO_ERROR;
	TyRISCVTargetType tyLocTargetOtion;
	TyRISCVTargetType tyLocRISCVTargetType;
	TyDeviceInfo  tyDeviceInfo;

	if ((ptyRISCVParams == NULL) || (puiNumberOfCores == NULL) || (pulIRWidth == NULL) || (pbRISCVCores == NULL) || (uiMaxCores == 0))
		return ERR_ML_RISCV_INVALID_HANDLE;

	//take a copy of current target option
	tyLocTargetOtion = ptyRISCVParams->tyRISCVTargetType;

	//the auto detect scan chain only works with the correct TPA
	//detect the currently connected TPA and adjust the target option accordingly
	if (DL_OPXD_GetDeviceInfo(ptyRISCVParams->iCurrentHandle, &tyDeviceInfo, DW_RISCV) != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_DETECT_SCANCHAIN_FAILED;

			ptyRISCVParams->tyRISCVTargetType = ARC_TGT_JTAG_TPA_R1;
			tyLocRISCVTargetType = ARC_TGT_JTAG_TPA_R1;

	if (!ptyRISCVParams->bDoNotIssueTAPReset)
	{
		// 1st step - reset TAPs using TMS sequence
		if (DL_OPXD_JtagResetTAP(ptyRISCVParams->iCurrentHandle, 0) != DRVOPXD_ERROR_NO_ERROR)
			iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
	}

	// 2nd step - disable MC
	(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, 0, NULL);

	// 3rd step - shift patterns via IR and DR to determine number of cores and scanchain structure
	*puiNumberOfCores = 0;
	if (iResult == ERR_ML_RISCV_NO_ERROR)
	{
		uint32_t ulIndex;
		TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
		uint32_t pulDRPatternOut1[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternOut2[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternIn1[RISCV_TSTSC_WORDS];
		uint32_t pulDRPatternIn2[RISCV_TSTSC_WORDS];
		uint32_t pulIRPatternIn[RISCV_TSTSC_WORDS];

		// prepare test patterns
		for (ulIndex = 0; ulIndex < RISCV_TSTSC_WORDS; ulIndex++)
		{
			pulDRPatternOut1[ulIndex] = 0xAAAAAAAA; pulDRPatternOut2[ulIndex] = 0x55555555;
			pulDRPatternIn1[ulIndex] = 0x0;        pulDRPatternIn2[ulIndex] = 0x0;
			pulIRPatternIn[ulIndex] = 0xFFFFFFFF;
		}

		// place cores into bypass and shift 2 test patterns via DR and finally get pattern from IR
		tyRes = DL_OPXD_JtagScanIR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, NULL, NULL);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, pulDRPatternOut1, pulDRPatternIn1);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, pulDRPatternOut2, pulDRPatternIn2);
		if (tyRes == DRVOPXD_ERROR_NO_ERROR)
			tyRes = DL_OPXD_JtagScanIR(ptyRISCVParams->iCurrentHandle, 0, RISCV_TSTSC_BITS, NULL, pulIRPatternIn);

		switch (tyRes)
		{
		case DRVOPXD_ERROR_NO_ERROR:        // no error, just go ahead
			break;
		case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
			iResult = ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED;
			break;
		default:                            // any error in driver
			iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
			break;
		}

		// determine number of cores from DR shifts
		if (iResult == ERR_ML_RISCV_NO_ERROR)
		{  // verify results
			uint32_t ulNumberOfBits = RISCV_TSTSC_BITS;

			// check if pattern matches (test up to supported number of cores)
			for (ulIndex = 0; ulIndex < (uiMaxCores); ulIndex++)
			{
				if (!CompareArrayOfWords(pulDRPatternOut1, pulDRPatternIn1, ulNumberOfBits) &&
					!CompareArrayOfWords(pulDRPatternOut2, pulDRPatternIn2, ulNumberOfBits))
				{  // found pattern match
					break;
				}

				ShiftArrayOfWordsRight(pulDRPatternIn1, RISCV_TSTSC_WORDS);
				ShiftArrayOfWordsRight(pulDRPatternIn2, RISCV_TSTSC_WORDS);
				ulNumberOfBits--;
				(*puiNumberOfCores)++;
			}

			if (!ulNumberOfBits)
				iResult = ERR_ML_RISCV_DETECT_SCANCHAIN_FAILED;
		}

		// analyze patterns
		if (iResult == ERR_ML_RISCV_NO_ERROR)
		{
			//to know the exact IR register length we would need to read the
			//ID register of each device, i.e. we assume they all are RISCV processors
			if ((*puiNumberOfCores == 0)
				|| (*puiNumberOfCores >= uiMaxCores))
				iResult = ERR_ML_RISCV_DETECT_SCANCHAIN_FAILED;
			else
			{
				for (ulIndex = 0; ulIndex < *puiNumberOfCores; ulIndex++)
				{
					pulIRWidth[ulIndex] = 4;
					pbRISCVCores[ulIndex] = 1;
				}
			}
		}
	}

	// 4th step - restore MC settings
	if (ptyRISCVParams->ulNumberOfTapsOnScanchain > 1)
	{  // more than 2 cores, we need to configure probe
		TyTAPConfig ptyScanchainConfig[MAX_RISCV_TAPS];
		uint32_t pulBypass[MAX_RISCV_TAPS];
		uint32_t ulIndex;

		// create structure with core information
		for (ulIndex = 0; ulIndex < ptyRISCVParams->ulNumberOfTapsOnScanchain; ulIndex++)
		{
			pulBypass[ulIndex] = 0xFFFFFFFF;                                  // all 1's as bypass code
			ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyRISCVParams->tyScanchainTapParams[ulIndex].ulIRLength);
			ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;               // does not matter
			ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(pulBypass[ulIndex]);    // bypass code
		}

		(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulNumberOfTapsOnScanchain, ptyScanchainConfig);
	}
	else
		(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, 0, NULL);

	
	if (!ptyRISCVParams->bDoNotIssueTAPReset)
	{
		// 5th step - reset TAPs using TMS sequence (always)
		(void)DL_OPXD_JtagResetTAP(ptyRISCVParams->iCurrentHandle, 0);
	}

	return iResult;
}

/****************************************************************************
	 Function: ML_UpdateScanchain
	 Engineer: Vitezslav Hola
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: Update scanchain settings (multicore) with new values
Date           Initials    Description
06-Oct-2007    VH          Initial
****************************************************************************/
int ML_UpdateScanchain(TyRISCVTargetParams *ptyRISCVParams)
{
	uint32_t ulIndex;
	// check handle
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	// update multicore settings based on number of cores and tyRISCVCoreParams
	ptyRISCVParams->ulCurrentRISCVTap = 0;
	ptyRISCVParams->ulNumberOfRISCVTaps = 0;
	for (ulIndex = 0; ulIndex < ptyRISCVParams->ulNumberOfTapsOnScanchain; ulIndex++)
	{
		if (ptyRISCVParams->tyScanchainTapParams[ulIndex].bRISCVTap)
		{  // found next RISCV core
			ptyRISCVParams->tyRISCVTapParams[ptyRISCVParams->ulCurrentRISCVTap].ulScanchainTap = ulIndex;        // link RISCV core with particular core on scanchain
			ptyRISCVParams->tyRISCVTapParams[0].ulTapVersion = 0x0;
			ptyRISCVParams->tyRISCVTapParams[0].usAccessType = DEFAULT_ACCESS_TYPE;
			ptyRISCVParams->ulCurrentRISCVTap++;
		}
	}

	ptyRISCVParams->ulNumberOfRISCVTaps = ptyRISCVParams->ulCurrentRISCVTap;
	ptyRISCVParams->ulCurrentRISCVTap = 0;                          // starting with first RISCV core

	if (!ptyRISCVParams->ulNumberOfRISCVTaps)
	{  // we could not find RISCV core, in that case, use first core
		ptyRISCVParams->tyRISCVTapParams[0].ulScanchainTap = 0;
		ptyRISCVParams->tyRISCVTapParams[0].ulTapVersion = 0x0;
		ptyRISCVParams->tyRISCVTapParams[0].usAccessType = DEFAULT_ACCESS_TYPE;
		ptyRISCVParams->ulNumberOfRISCVTaps = 1;
	}
	// update multicore config in Opella-XD
	if (ptyRISCVParams->ulNumberOfTapsOnScanchain > 1)
	{  // more than 2 cores, we need to configure probe
		TyTAPConfig ptyScanchainConfig[MAX_RISCV_TAPS];
		//uint32_t pulBypass[MAX_RISCV_TAPS];
		// create structure with core information
		for (ulIndex = 0; ulIndex < ptyRISCVParams->ulNumberOfTapsOnScanchain; ulIndex++)
		{
			//pulBypass[ulIndex] = 0xFFFFFFFF;                                              // all 1's as bypass code
			ptyScanchainConfig[ulIndex].usDefaultIRLength = (unsigned short)(ptyRISCVParams->tyScanchainTapParams[ulIndex].ulIRLength);
			ptyScanchainConfig[ulIndex].usDefaultDRLength = 32;                           // does not matter
			ptyScanchainConfig[ulIndex].pulBypassIRPattern = &(ptyRISCVParams->tyScanchainTapParams[ulIndex].ulBypassInst);       // bypass code
		}
		(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulNumberOfTapsOnScanchain, ptyScanchainConfig);
	}
	else
		(void)DL_OPXD_JtagConfigMulticore(ptyRISCVParams->iCurrentHandle, 0, NULL);        // do not set MC support, we have just 1 core

	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_SetJtagFrequency
	 Engineer: Vitezslav Hola
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   double dFrequency - frequency in MHz to set
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: Set fixed frequency from 10kHz to 100MHz or adaptive JTAG clock.
Date           Initials    Description
08-Oct-2007    VH          Initial
****************************************************************************/
int ML_SetJtagFrequency(TyRISCVTargetParams *ptyRISCVParams, double dFrequency)
{
	int iResult = ERR_ML_RISCV_NO_ERROR;
	TyDeviceInfo tyLocDeviceInfo;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	if ((dFrequency > MAX_RISCV_JTAG_AND_BLAST_FREQUENCY) || (dFrequency < MIN_RISCV_JTAG_AND_BLAST_FREQUENCY))
		return ERR_ML_RISCV_INVALID_FREQUENCY;

	//Opella-XD R2 FW/DW still dont support CMD_CODE_DISKWARE_STATUS command, so dont use that for R2
	if (DL_OPXD_GetDeviceInfo(ptyRISCVParams->iCurrentHandle, &tyLocDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(ptyRISCVParams->iCurrentHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}

	if (tyLocDeviceInfo.ulBoardRevision < 0x03000000)
	{
		tyLocDeviceInfo.uiActiveDiskwares = 0;
		tyLocDeviceInfo.bOtherDiskwareRunning = 0;
	}
	else
	{
		//Check if diskware is already running (could be that its loaded by another instance of GDB server)
		if (DL_OPXD_GetDiskwareStatus(ptyRISCVParams->iCurrentHandle, &tyLocDeviceInfo, RISCV) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(ptyRISCVParams->iCurrentHandle);
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
		}
	}

	if ((tyLocDeviceInfo.uiActiveDiskwares > 1) || tyLocDeviceInfo.bOtherDiskwareRunning)
	{
		//Some diskware is running, so dont change JTAG freq now!
		return ERR_ML_RISCV_NO_ERROR;
	}

	// frequency is correct range so we can store frequency value and try to set clock in Opella-XD probe
	if (ptyRISCVParams->bAdaptiveClockEnabled)
	{  // adaptive clock is being used, so use frequency as alternative
		if (DL_OPXD_JtagSetAdaptiveClock(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulAdaptiveClockTimeout, dFrequency) != DRVOPXD_ERROR_NO_ERROR)
			iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
		else
			ptyRISCVParams->dJtagFrequency = dFrequency;               // update frequency in local variable
	}
	else
	{
		if (DL_OPXD_JtagSetFrequency(ptyRISCVParams->iCurrentHandle, dFrequency, NULL, NULL) != DRVOPXD_ERROR_NO_ERROR)
			iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
		else
			ptyRISCVParams->dJtagFrequency = dFrequency;               // update frequency in local variable
	}
	return iResult;
}

/****************************************************************************
	 Function: ML_UsingAdaptiveJtagClock
	 Engineer: Vitezslav Hola
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: unsigned char - TRUE if adaptive clock is being used, FALSE otherwise
  Description: is adaptive clock currently being used
Date           Initials    Description
16-Oct-2007    VH          Initial
****************************************************************************/
unsigned char ML_UsingAdaptiveJtagClock(TyRISCVTargetParams *ptyRISCVParams)
{
	if ((ptyRISCVParams != NULL) && (ptyRISCVParams->bAdaptiveClockEnabled))
		return TRUE;
	return FALSE;
}

/****************************************************************************
	 Function: ML_ToggleAdaptiveJtagClock
	 Engineer: Vitezslav Hola
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: unsigned char - TRUE if adaptive clock is being used, FALSE otherwise
  Description: is adaptive clock currently being used
Date           Initials    Description
06-Nov-2007    VH          Initial
****************************************************************************/
int ML_ToggleAdaptiveJtagClock(TyRISCVTargetParams *ptyRISCVParams, unsigned char bEnableAdaptiveClock)
{
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	// check if this feature is supported for current target
	/*if ((ptyRISCVParams->tyRISCVTargetType != RISCV_TGT_NORMAL) && (ptyRISCVParams->tyRISCVTargetType != RISCV_TGT_RSTDETECT))
		return ERR_ML_RISCV_TARGET_NOT_SUPPORTED;*/
	// check if we need to do some action
	if ((ptyRISCVParams->bAdaptiveClockEnabled && bEnableAdaptiveClock) ||
		(!ptyRISCVParams->bAdaptiveClockEnabled && !bEnableAdaptiveClock))
		return ERR_ML_RISCV_NO_ERROR;                              // nothing to do
	 // we need to change 
	if (bEnableAdaptiveClock)
	{  // switch to adaptive clock
		(void)DL_OPXD_JtagSetAdaptiveClock(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulAdaptiveClockTimeout, ptyRISCVParams->dJtagFrequency);
	}
	else
	{  // switch to fixed clock
		(void)DL_OPXD_JtagSetFrequency(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->dJtagFrequency, NULL, NULL);
	}
	ptyRISCVParams->bAdaptiveClockEnabled = bEnableAdaptiveClock;
	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_SetResetProperty
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned char bSetResetDelay - setting reset delay(!=0x0) or reset duration (0x0)
			   unsigned int *piValue - pointer to value to set (NULL for default)
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: set target reset parameters (reset duration and delay after reset)
			   nRST       _____               ___________
							   |             |
							   |_____________|
							   <- duration -> <- delay ->(debug communication)

Date           Initials    Description
16-Jul-2006    AG          Initial
****************************************************************************/
int ML_SetResetProperty(TyRISCVTargetParams *ptyRISCVParams, unsigned char bSetResetDelay, unsigned int *piValue)
{
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	if (bSetResetDelay)
	{
		// updating delay after reset (in ms)
		if ((piValue != NULL) && (*piValue <= MAX_RISCV_RESET_VALUE))
			ptyRISCVParams->ulDelayAfterReset = (unsigned int)(*piValue);
		else
			ptyRISCVParams->ulDelayAfterReset = DEFAULT_RISCV_DELAY_AFTER_RESET;
	}
	else
	{
		// updating reset duration (in ms), duration cannot be 0 (at least 1 ms)
		if ((piValue != NULL) && (*piValue <= MAX_RISCV_RESET_VALUE) && (*piValue > 0))
			ptyRISCVParams->ulResetDuration = (uint32_t)(*piValue);
		else
			ptyRISCVParams->ulResetDuration = DEFAULT_RISCV_RESET_DURATION;
	}
	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
Function: ML_InvalidatingICache
Engineer :
Input : TyRISCVTargetParams * ptyRISCVParams - RISCV target parameters / settings
unsigned char bDisableICacheInvalidation - enable skipping icache invalidation
Output : int - error code ERR_ML_RISCV_xxx
Description : enable / disable skipping of icache invalidation after writting to memory
Date           Initials    Description
16 - Jul - 2006    AG         Initial
****************************************************************************/
int ML_InvalidatingICache(TyRISCVTargetParams *ptyRISCVParams, unsigned char bDisableICacheInvalidation)
{
	return 0;
}


int ML_SetTargetEndian(TyRISCVTargetParams *ptyRISCVParams, unsigned char bBigEndianTarget)
{
	return 0;

}


unsigned char ML_IsCpuBE(TyRISCVTargetParams *ptyRISCVParams)
{
	return 0;
}


/****************************************************************************
	 Function: ML_SetCurrentTap
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t ulTapNum - set current core number
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: set TAP number for debug session
Date           Initials    Description
16-Jul-2006    AG         Initial
****************************************************************************/
int ML_SetCurrentTap(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulTapNum)
{
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = ulTapNum;

	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_SetCycleStep
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned char bCycleStep - using cycle step
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: set if cycle step is used
Date           Initials    Description
16-Jul-2007    AG          Initial
****************************************************************************/
int ML_SetCycleStep(TyRISCVTargetParams *ptyRISCVParams, unsigned char bCycleStep)
{
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	// modify item in current structure and update settings for all cores
	ptyRISCVParams->bCycleStep = bCycleStep;
	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_GetCycleStep
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: unsigned char - cycle step status
  Description: set if cycle step is used
Date           Initials    Description
11-Oct-2007    AG          Initial
****************************************************************************/
unsigned char ML_GetCycleStep(TyRISCVTargetParams *ptyRISCVParams)
{
	return (ptyRISCVParams->bCycleStep);
}

int ML_SetAdaptiveClockTimeout(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulTimeout)
{
	return 0;

}

int ML_SetMemoryAccessOptimization(TyRISCVTargetParams *ptyRISCVParams, unsigned char bEnableOptimizedAccess)
{
	return 0;

}


int ML_SetAddressAutoincrement(TyRISCVTargetParams *ptyRISCVParams, unsigned char bEnableAddressAutoincrement)
{
	return 0;

}

void ML_SetCpuVersionNumber(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulVersionNumber)
{
	

}

/****************************************************************************
	 Function: ML_IndicateTargetStatusChange
	 Engineer:
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: send probe request to display target status change
Date           Initials    Description
12-Oct-2006    AG          Initial
****************************************************************************/
int ML_IndicateTargetStatusChange(TyRISCVTargetParams *ptyRISCVParams)
{
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	if (DL_OPXD_IndicateTargetStatus(ptyRISCVParams->iCurrentHandle, 0, 1) != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	return ERR_ML_RISCV_NO_ERROR;
}
/****************************************************************************
	 Function: ML_ConvertFrequencySelection
	 Engineer:
		Input: 
	   Output: int - error code ERR_ML_RISCV_xxx
  Description: 
Date           Initials    Description
16-Jul-2006    AG          Initial
****************************************************************************/
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency)
{
	double dFrequency = 0.0;
	int iValue = 0;
	char pszLocSelection[20];

	if ((pszSelection == NULL) || (strlen(pszSelection) >= 20))
		return FALSE;
	else
	{  // convert string to lower case in local buffer
		unsigned int uiIndex = 0;
		while (uiIndex < strlen(pszSelection))
		{
			if ((pszSelection[uiIndex] >= 'A') && (pszSelection[uiIndex] <= 'Z'))
				pszLocSelection[uiIndex] = pszSelection[uiIndex] + ('a' - 'A');
			else
				pszLocSelection[uiIndex] = pszSelection[uiIndex];
			uiIndex++;
		}
		pszLocSelection[uiIndex] = 0;        // terminate string
	}
	if (pdFrequency != NULL)
		*pdFrequency = 0.0;

	// first check frequency selection as index (backward compatibility with OpellaUSB)
	if (strlen(pszLocSelection) == 1)
	{  // selection made by index so check value
		switch (pszLocSelection[0])
		{
		case '0':               // 24 MHz
			dFrequency = 24.0;
			break;
		case '1':               // 12 MHz
			dFrequency = 12.0;
			break;
		case '2':               //  8 MHz
			dFrequency = 8.0;
			break;
		case '3':               //  6 MHz
			dFrequency = 6.0;
			break;
		case '4':               //  4 MHz
			dFrequency = 4.0;
			break;
		case '5':               //  3 MHz
			dFrequency = 3.0;
			break;
		case '6':               //  2 MHz
			dFrequency = 2.0;
			break;
		case '7':               //  1 MHz
			dFrequency = 1.0;
			break;
		default:
			return FALSE;        // error in selection
		}
	}
	else if ((strstr(pszLocSelection, "khz") != NULL) && (sscanf(pszLocSelection, "%dkhz", &iValue) == 1))
	{  // value specified in kHz
		dFrequency = (double)iValue;                    // frequency in kHz
		dFrequency /= 1000.0;                           // convert to MHz
	}
	else if ((strstr(pszLocSelection, "mhz") != NULL) && (sscanf(pszLocSelection, "%dmhz", &iValue) == 1))
	{  // value in MHz
		dFrequency = (double)iValue;                    // frequency in MHz, no need to further conversion
	}
	else
		return FALSE;

	// otherwise frequency has been correctly parsed
	if (pdFrequency != NULL)
		*pdFrequency = dFrequency;

	return TRUE;
}

/****************************************************************************
	 Function: ML_Sleep
	 Engineer: 
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t ulMiliseconds - number of miliseconds to sleep
	   Output: none
  Description: sleep version in MidLayer, calling debugger implementation if provided
Date           Initials    Description
09-Oct-2007    AG          Initial
****************************************************************************/
void ML_Sleep(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulMiliseconds)
{
	if ((ptyRISCVParams == NULL) || (ptyRISCVParams->pfSleepFunc == NULL))
	{
		Sleep((DWORD)ulMiliseconds);  // we must use own implementation
	}
	else
		ptyRISCVParams->pfSleepFunc((unsigned)ulMiliseconds);
}

/****************************************************************************
	 Function: ML_ResetTarget
	 Engineer: Vitezslav Hola
		Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
	   Output: int - error code ERR_ML_ARC_xxx
  Description: reset ARC target if supported
			   this function assert nRST pin for certain number of ms and also waits
			   after deasserting (delays can be set by ML_SetResetProperty function)
Date           Initials    Description
16-Jul-2006    VH          Initial
****************************************************************************/
int ML_ResetTarget(TyRISCVTargetParams *ptyRISCVParams)
{
	int iResult = ERR_ML_RISCV_NO_ERROR;
	// check valid pointer and if reset is supported for current target
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	if (ptyRISCVParams->tyRISCVTargetType != ARC_TGT_JTAG_TPA_R1)
		return ERR_ML_RISCV_TARGET_NOT_SUPPORTED;
	// let assert reset pin and wait certain time
	if (DL_OPXD_RISCV_ResetProc(ptyRISCVParams->iCurrentHandle, 1, 0) != DRVOPXD_ERROR_NO_ERROR)
		iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
	ML_Sleep(ptyRISCVParams, ptyRISCVParams->ulResetDuration);
	// now we can deassert reset and wait certain time
	if (DL_OPXD_RISCV_ResetProc(ptyRISCVParams->iCurrentHandle, 0, 0) != DRVOPXD_ERROR_NO_ERROR)
		iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
	ML_Sleep(ptyRISCVParams, ptyRISCVParams->ulDelayAfterReset);
	// we done, so return result
	return iResult;
}

/****************************************************************************
	 Function: ML_HaltHart
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore 
	   Output: int - error code ERR_ML_ARC_xxx
  Description: Halt the RISCV target
			   
Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_HaltHart(TyRISCVTargetParams *ptyRISCVParams,unsigned short usTapId,uint32_t ulHartId,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_HaltHart(ptyRISCVParams->iCurrentHandle,ptyRISCVParams->ulCurrentRISCVTap,ptyRISCVParams->ulHartId,ulAbits);
	if(tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_RunHart
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
	   Output: int - error code ERR_ML_ARC_xxx
  Description: Resume the RISCV target

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_RunHart(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_RunHart(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}
/****************************************************************************
	 Function: ML_AddTrigger
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulTriggerAddr - Address to add Hardware breakpoint
			   uint32_t ulTriggerNum - Number of HardWare Breakpoint
			   uint32_t *ulTriggerOut - return sucess or fail
	   Output: int - error code ERR_ML_ARC_xxx
  Description: insert the hardware breakpoint

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_AddTrigger(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerAddr, uint32_t ulTriggerNum, uint32_t *ulTriggerOut,uint32_t ulType,uint32_t ulArchSize,uint32_t ulAbits, uint32_t ulLength, uint32_t ulMatch, uint32_t ulResrequired)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_Add_Trigger(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId,ulTriggerAddr,ulTriggerNum,ulTriggerOut, ulType,ulArchSize,ulAbits,ulLength,ulMatch,ulResrequired);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_RemoveTrigger
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulTriggerNum - Trigger number to be removed
				uint32_t ulArchSize   -Architecture Size
			   uint32_t ulAbits       -Address bit size
			   uint32_t ulResUsed     -resources used by trigger
	   Output: int - error code ERR_ML_ARC_xxx
  Description: Remove the hardware breakpoint

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_RemoveTrigger(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulTriggerNum,uint32_t ulArchSize,uint32_t ulAbits,uint32_t ulResUsed)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_Remove_Trigger(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulTriggerNum,ulArchSize,ulAbits, ulResUsed);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_TriggerInfo
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t *pulTriggerInfo - Info number of triggers(H/w breakpoints ) present
	   Output: int - error code ERR_ML_ARC_xxx
  Description: number of H/W breakpoints available

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_TriggerInfo(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulTriggerInfo,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_Trigger_Info(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, pulTriggerInfo,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_SingleStep
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
	   Output: int - error code ERR_ML_ARC_xxx
  Description: stepover in program

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_SingleStep(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_SingleStep(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_Riscv_Mechanism
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t ulRegMec - Reg support for connected Target
			   uint32_t ulMemMec - Mem Support for connected Target
			   uint32_t ulCsrSupp - CSR support for connected Target
	   Output: int - error code ERR_ML_ARC_xxx
  Description: sytsem mechanism for connected target

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_Riscv_Mechanism(TyRISCVTargetParams *ptyRISCVParams, unsigned short uiTapId, uint32_t ulRegMec, uint32_t ulMemMec, uint32_t ulCsrSupp, uint32_t ulFloatingProc,uint32_t ulFence_i)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	
	ptyRISCVParams->ulCurrentRISCVTap = uiTapId;

	tyRes = DL_OPXD_RISCV_Mechanism(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ulRegMec, ulMemMec, ulCsrSupp, ulFloatingProc, ulFence_i);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_Riscv_TargetReset
	 Engineer: Harvinder Singh
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
		       uint32_t ulHartId - hart id of hart to halt.
	   Output: int - error code ERR_ML_ARC_xxx
  Description: Reset RISCV target.
Date           Initials    Description
06-June-2019     HS          Initial
****************************************************************************/
int ML_Riscv_TargetReset(TyRISCVTargetParams *ptyRISCVParams,uint32_t ulHartId)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	tyRes = DL_OPXD_RISCV_TargetReset(ptyRISCVParams->iCurrentHandle,ulHartId);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_Discovery
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t * pulDiscoveryData - Information Dscovered from Target
	   Output: int - error code ERR_ML_ARC_xxx
  Description: basic information about the target being debug

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_Discovery(TyRISCVTargetParams *ptyRISCVParams, uint32_t uiTapId, uint32_t * pulDiscoveryData,uint32_t ulHartNo)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = uiTapId;

	tyRes = DL_OPXD_RISCV_Discovery(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, pulDiscoveryData,ulHartNo);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_Discover_Harts
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t * pulNumberOfHarts - number of Harts in target
	   Output: int - error code ERR_ML_ARC_xxx
  Description: number of harts in target

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_Discover_Harts(TyRISCVTargetParams *ptyRISCVParams, uint32_t uiTapId, uint32_t *pulNumberOfHarts)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = uiTapId;
	tyRes = DL_OPXD_RISCV_Discover_Harts(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, pulNumberOfHarts);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_StatusProc
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   unsigned char *pucGlobalStatus - status 1: Halted 0:Running
				unsigned char *pucStopProblem - if halted reason for halting
	   Output: int - error code ERR_ML_ARC_xxx
  Description: status regarding target halted or running with reason if halted

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_StatusProc(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, unsigned short usCoreId, uint32_t ulHartId, unsigned char *pucGlobalStatus, unsigned char *pucStopProblem,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulCurrentRISCVCore = usCoreId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_StatusProc(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulCurrentRISCVCore, ptyRISCVParams->ulHartId, pucGlobalStatus, pucStopProblem,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}
/****************************************************************************
	 Function: ML_StatusProc
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   unsigned char *pucGlobalStatus - status 1: Halted 0:Running
				unsigned char *pucStopProblem - if halted reason for halting
	   Output: int - error code ERR_ML_ARC_xxx
  Description: status regarding target halted or running with reason if halted

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_ReadBlock(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	uint32_t ulWordsLeft = 0, ulChunkCount = 0, ulIndex = 0;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;

	ulWordsLeft = ulCount;
	do
	{
		ulChunkCount = (ulWordsLeft*ulSize > BUFFER_PAYLOAD) ? (BUFFER_PAYLOAD / ulSize) : (ulWordsLeft);

		tyRes = DL_OPXD_RISCV_ReadBlock(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, 
			ulSize, ulChunkCount, ulAddress + ulIndex, (pulData + ulIndex),ulArchSize,ulAbits);
		ulWordsLeft = ulWordsLeft - ulChunkCount;
		ulIndex = ulIndex + (ulChunkCount * ulSize);
	} while ((ulWordsLeft > 0)
		&& (tyRes == DRVOPXD_ERROR_NO_ERROR));

	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_WriteBlock
	 Engineer: Ashutosh Garg
		Input: TyArcTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   unsigned short usTapId - core selector (0 to MAX_TAPS_ON_SCANCHAIN - 1)
			   uint32_t ulHartId - hart selector for usCore
			   uint32_t ulSize		: 1 - byte
											  2 - half Word
											  3 - Word
			   uint32_t ulCount	: number of ulSize to be written
			   uint32_t ulAddress	: memory address
			   unsigned char *pulData	: memory data 
			   uint32_t ulArchSize : System Architechure
			   uint32_t ulAbits    : Size of address in DMI
	   Output: int - error code ERR_ML_ARC_xxx
  Description: Write Memory byte/halfword/word

Date           Initials    Description
01-Nov-2018     AG          Initial
****************************************************************************/
int ML_WriteBlock(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId,
	uint32_t ulSize, uint32_t ulCount, uint32_t ulAddress, unsigned char *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;
	uint32_t ulWordsLeft = 0, ulChunkCount = 0, ulIndex = 0;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	/* Divide the pay load to Opella-XD in to chunks of 16KB (Buffer_payload) */
	ulWordsLeft = ulCount;
	do 
	{
		ulChunkCount = (ulWordsLeft*ulSize > BUFFER_PAYLOAD)? (BUFFER_PAYLOAD / ulSize):(ulWordsLeft);

		tyRes = DL_OPXD_RISCV_WriteBlock(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap,
			ptyRISCVParams->ulHartId, ulSize, ulChunkCount, ulAddress + ulIndex, (pulData + ulIndex),ulArchSize,ulAbits);

		ulWordsLeft = ulWordsLeft - ulChunkCount;
		ulIndex = ulIndex + (ulChunkCount * ulSize);
	} while ((ulWordsLeft > 0)
		&& (tyRes == DRVOPXD_ERROR_NO_ERROR));

	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}


/****************************************************************************
		Function: ML_Read_Registers
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *pulRegValues : pointer for returing the GPRS Values
				  uint32_t ulRegArraySize : number of GPRS regisers
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: read the GPRS values
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_Read_Registers(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_Read_Register(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId,pulRegValues,ulRegArraySize,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_Write_Registers
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *pulRegValues : pointer for GPRS Values to be written
				  uint32_t ulRegArraySize : number of GPRS regisers
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: Write the GPRS values
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_Write_Registers(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *pulRegValues, uint32_t ulRegArraySize,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_Write_Register(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, pulRegValues, ulRegArraySize,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_Write_A_Register
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegValue : register value
				  uint32_t ulRegAdress : register number
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: Write a specified register
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_Write_A_Register(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_WriteARegister(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulRegValue, ulRegAddress,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_Read_A_Register
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegValue : register value
				  uint32_t ulRegAdress : register number
				  uint32_t ulArchSize : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: Read a specified register
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_Read_A_Register(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegValue, uint32_t ulRegAddress,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_ReadARegister(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulRegValue, ulRegAddress,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_WriteWord
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulAddress   : Address of the memory
				  uint32_t *pulData    : Memory Data Word
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: Wrie Memory Word
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_WriteWord(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_WriteWord(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulAddress, pulData,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_ReadWord
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t ulAddress   : Address of the memory
				  uint32_t *pulData    : Memory Data Word
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: Read Memory Word
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_ReadWord(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t ulAddress, uint32_t *pulData,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_ReadWord(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulAddress, pulData,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_Write_DPC
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulValue	: PC value
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	   int - error code ERR_ML_ARC_xxx
		Description: Write PC value
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_Write_DPC(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulValue,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_WriteDPC(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulValue,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
		Function: ML_Write_CSR
		Engineer: Ashutosh Garg
		Input:	  struct TyRISCVInstance *p : debugger structure
				  unsigned short usTapId   : TAP Id
				  uint32_t ulHartId    : HartId
				  uint32_t *ulRegisterValue : value of CSR to write
				  uint32_t ulRegisterNumber : CSR number
				  uint32_t ulArchSize  : System Architechure
				  uint32_t ulAbits     : Size of address in DMI
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: Write CSR value
		Date           Initials    Description
		10-Dec-2018		  AG        Initial
****************************************************************************/
int ML_Write_CSR(TyRISCVTargetParams * ptyRISCVParams, unsigned short usTapId, uint32_t ulHartId, uint32_t *ulRegisterValue, uint32_t ulRegisterNumber,uint32_t ulArchSize,uint32_t ulAbits)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int     iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;

	ptyRISCVParams->ulCurrentRISCVTap = usTapId;
	ptyRISCVParams->ulHartId = ulHartId;
	tyRes = DL_OPXD_RISCV_WriteCSR(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, ptyRISCVParams->ulHartId, ulRegisterValue, ulRegisterNumber,ulArchSize,ulAbits);
	if (tyRes != DRVOPXD_ERROR_NO_ERROR)
		iResult = ML_ConvertDrvlayerErrorToMLError(tyRes);

	ptyRISCVParams->iLastErrorCode = iResult;
	return (ptyRISCVParams->iLastErrorCode);
}

/****************************************************************************
	 Function: ML_SetDllPath
	 Engineer: Vitezslav Hola
		Input: const char *pszDllPath - DLL full path
	   Output: none
  Description: extract path to DLL folder and store into global variable
			   (to be used with ML_CreateFullFilename and others)
Date           Initials    Description
09-Oct-2007    VH          Initial
****************************************************************************/
void ML_SetDllPath(const char *pszDllPath)
{
	char *pszPtr;

	pszGlobalDllPath[0] = 0;
	// copy string to buffer and convert to lower case
	strncpy(pszGlobalDllPath, pszDllPath, DLLPATH_LENGTH - 1);
	pszGlobalDllPath[DLLPATH_LENGTH - 1] = 0;

#ifdef __LINUX
	// try to find .so extension
	pszPtr = strstr(pszGlobalDllPath, ".so");
	if (pszPtr != NULL)
	{     // found .so so go backwards to nearest '/' (or to start of string)
		*pszPtr = 0;
		while ((*pszPtr != '/') && (pszPtr > pszGlobalDllPath))
			pszPtr--;
		*pszPtr = 0;   // cut string (remove dll name+extension and \ character
	}
#else
	StringToLwr(pszGlobalDllPath);
	// try to find .dll extension
	pszPtr = strstr(pszGlobalDllPath, ".dll");
	if (pszPtr != NULL)
	{     // found .dll so go backwards to nearest '\\' (or to start of string)
		*pszPtr = 0;
		while ((*pszPtr != '\\') && (pszPtr > pszGlobalDllPath))
			pszPtr--;
		*pszPtr = 0;   // cut string (remove dll name+extension and \ character
	}
#endif
	else
		pszGlobalDllPath[0] = 0;   // wrong format, so use empty string
}

/****************************************************************************
	 Function: ML_GetDllPath
	 Engineer: Vitezslav Hola
		Input: char *pszDllPath - full path to DLL
			   unsigned int uiSize - maximum number of bytes in pszDllPath
	   Output: none
  Description: copy full DLL path into provided string
Date           Initials    Description
15-Oct-2007    VH          Initial
****************************************************************************/
void ML_GetDllPath(char *pszDllPath, unsigned int uiSize)
{
	if (uiSize)
	{
		strncpy(pszDllPath, pszGlobalDllPath, uiSize);
		pszDllPath[uiSize - 1] = 0;
	}
}

/****************************************************************************
	 Function: ML_GetErrorMessage
	 Engineer: Vitezslav Hola
		Input: int iReturnCode - error code ML_xxx
	   Output: char * - pointer to string with message
  Description: get error message
Date           Initials    Description
05-Nov-2007    VH          Initial
****************************************************************************/
char* ML_GetErrorMessage(int iReturnCode)
{
	char *pszLocMsg;
	static char cTempMsg[50];

	switch (iReturnCode)
	{
	case ERR_ML_RISCV_NO_ERROR:                       // no error
		pszLocMsg = (char *)MSG_ML_NO_ERROR;                 break;
	case ERR_ML_RISCV_INVALID_HANDLE:                 // invalid driver handle
		pszLocMsg = (char *)MSG_ML_INVALID_DRIVER_HANDLE;    break;
	case ERR_ML_RISCV_TARGET_NOT_SUPPORTED:           // target does not support selected feature
		pszLocMsg = (char *)MSG_ML_TARGET_NOT_SUPPORTED;     break;
	case ERR_ML_RISCV_USB_DRIVER_ERROR:               // USB driver error (Opella-XD did not respond or USB error)
		pszLocMsg = (char *)MSG_ML_USB_DRIVER_ERROR;         break;
	case ERR_ML_RISCV_PROBE_NOT_AVAILABLE:            // Opella-XD is not available for debug session
		pszLocMsg = (char *)MSG_ML_PROBE_NOT_AVAILABLE;      break;
	case ERR_ML_RISCV_PROBE_INVALID_TPA:              // missing or invalid TPA
		pszLocMsg = (char *)MSG_ML_INVALID_TPA;              break;
	case ERR_ML_RISCV_PROBE_CONFIG_ERROR:             // probe configuration error (missing diskware or FPGAware)
		pszLocMsg = (char *)MSG_ML_PROBE_CONFIG_ERROR;       break;
	case ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED:           // RTCK timeout expired (problem with RTCK)
		pszLocMsg = (char *)MSG_ML_RTCK_TIMEOUT_EXPIRED;     break;
	case ERR_ML_RISCV_DEBUG_ACCESS:                   // access to ARC JTAG debug register failed
		pszLocMsg = (char *)MSG_ML_DEBUG_ACCESS_FAILED;      break;
	case ERR_ML_RISCV_INVALID_FREQUENCY:              // invalid frequency format
		pszLocMsg = (char *)MSG_ML_INVALID_FREQUENCY;        break;
	case ERR_ML_RISCV_ANGEL_BLASTING_FAILED:          // blasting FPGA into ARCangel failed
		pszLocMsg = (char *)MSG_ML_AA_BLASTING_FAILED;       break;
	case ERR_ML_RISCV_ANGEL_INVALID_EXTCMD_RESPONSE:  // invalid response to certain extended commands (setting clocks on ARCangel4)
		pszLocMsg = (char *)MSG_ML_AA_INVALID_EXTCMD_RESPONSE;  break;
	case ERR_ML_RISCV_ANGEL_INVALID_PLL_FREQUENCY:    // cannot set PLL frequency on ARCangel4 (requested frequency is not supported)
		pszLocMsg = (char *)MSG_ML_AA_INVALID_PLL_FREQUENCY; break;
	case ERR_ML_RISCV_TEST_SCANCHAIN_FAILED:          // failing scanchain loopback test
		pszLocMsg = (char *)MSG_ML_SCANCHAIN_TEST_FAILED;    break;
	case ERR_ML_RISCV_TARGET_RESET_DETECTED:          // target reset detected
		pszLocMsg = (char *)MSG_ML_TARGET_RESET_DETECTED;    break;
	case ERR_ML_RISCV_DEBUG_PDOWN:                    // target is powered-down
		pszLocMsg = (char *)MSG_ML_TARGET_POWERED_DOWN;      break;
	case ERR_ML_RISCV_DEBUG_POFF:                    // target is powered-off
		pszLocMsg = (char *)MSG_ML_TARGET_POWERED_OFF;      break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUSY:
		pszLocMsg = (char *)MSG_ML_RISCV_ABSTRACT_REG_WRITE_BUSY;      break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED:
		pszLocMsg = (char *)MSG_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED;      break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION:
		pszLocMsg = (char *)MSG_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION;      break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE:
		pszLocMsg = (char *)MSG_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE;      break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR:
		pszLocMsg = (char *)MSG_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR;      break;
	case ERR_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR:
		pszLocMsg = (char *)MSG_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR;      break;
	case ERR_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED:
		pszLocMsg = (char *)MSG_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED;      break;
	case ERR_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED:
		pszLocMsg = (char *)MSG_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED;      break;
	case ERR_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED:
		pszLocMsg = (char *)MSG_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED;      break;
	case ERR_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED:
		pszLocMsg = (char *)MSG_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED;      break;
	case ERR_ML_RISCV_SYSTEM_BUS_OTHER_REASON:
		pszLocMsg = (char *)MSG_ML_RISCV_SYSTEM_BUS_OTHER_REASON;      break;
	case ERR_ML_RISCV_SYSTEM_BUS_MASTER_BUSY:
		pszLocMsg = (char *)MSG_ML_RISCV_SYSTEM_BUS_MASTER_BUSY;      break;
	case ERR_ML_RISCV_MEM_INVALID_LENGTH:
		pszLocMsg = (char *)MSG_ML_RISCV_MEM_INVALID_LENGTH;      break;
	case ERR_ML_RISCV_HALT_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_HALT_TIMEOUT;      break;
	case ERR_ML_RISCV_RESUME_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_RESUME_TIMEOUT;      break;
	case ERR_ML_RISCV_UNSUPPORT_ARCH_TYPE:
		pszLocMsg = (char*)MSG_ML_RISCV_UNSUPPORT_ARCH_TYPE;      break;
	case ERR_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE:
		pszLocMsg = (char*)MSG_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE;      break;
	case ERR_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION:
		pszLocMsg = (char*)MSG_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;      break;
	case ERR_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT;      break;
	case ERR_ML_RISCV_DISALE_ABSTRACT_COMMAND:
		pszLocMsg = (char*)MSG_ML_RISCV_DISALE_ABSTRACT_COMMAND;      break;
	case ERR_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT:
		pszLocMsg = (char*)MSG_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT;      break;
	case ERR_ML_UNKNOWN_CMD:
		pszLocMsg = (char*)MSG_ML_RISCV_UNKNOWN_CMD;      break;
	default:                                        // unspecified error
		sprintf(cTempMsg, "%s (Error no.: %d)", (char *)MSG_ML_UNKNOWN_ERROR, iReturnCode);
		pszLocMsg = cTempMsg;
		break;
	}
	return pszLocMsg;
}

/****************************************************************************
	 Function: CompareArrayOfWords
	 Engineer: Vitezslav Hola
		Input: uint32_t *pulArray1 - pointer to 1st array of words
			   uint32_t *pulArray2 - pointer to 2nd array of words
			   uint32_t ulNumberOfBits - number of bits to compare (least significant)
	   Output: int - 0 if arrays are same, otherwise value different than 0
  Description: compare two array of words taking only certain number of bits for comparison
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
static int CompareArrayOfWords(uint32_t *pulArray1, uint32_t *pulArray2, uint32_t ulNumberOfBits)
{
	if ((pulArray1 == NULL) || (pulArray2 == NULL) || (ulNumberOfBits == 0))
		return 0;

	ulNumberOfBits -= 32;

	// compare whole words
	while (ulNumberOfBits >= 32)
	{
		if (*pulArray1 != *pulArray2)
			return 1;
		pulArray1++;
		pulArray2++;
		ulNumberOfBits -= 32;
	}
	// compare parts of last word with mask
	if (ulNumberOfBits)
	{
		uint32_t ulMask = 0xFFFFFFFF >> (32 - ulNumberOfBits);
		if ((*pulArray1 & ulMask) != (*pulArray2 & ulMask))
			return 1;
	}
	// otherwise both arrays are same
	return 0;
}

/****************************************************************************
	 Function: ShiftArrayOfWordsRight
	 Engineer: Vitezslav Hola
		Input: uint32_t *pulArray - pointer to array of words
			   uint32_t ulWords - number of words in array
	   Output: none
  Description: shift array of words by 1 bit right
Date           Initials    Description
22-Oct-2007    VH          Initial
****************************************************************************/
static void ShiftArrayOfWordsRight(uint32_t *pulArray, uint32_t ulWords)
{
	uint32_t ulIndex;
	uint32_t ulTransfer = 0;

	if ((!ulWords) || (pulArray == NULL))
		return;

	for (ulIndex = ulWords; ulIndex > 0; ulIndex--)
	{
		uint32_t ulTemp = (pulArray[ulIndex - 1] & 0x1);   // get LSB
		pulArray[ulIndex - 1] >>= 1;
		pulArray[ulIndex - 1] |= ulTransfer;
		ulTransfer = ulTemp << 31;    // carry bit for next word
	}
}

/****************************************************************************
	 Function: ML_ScanIR
	 Engineer: Vitezslav Hola
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t ulLength - number of bits to scan
			   uint32_t *pulDataIn - data to scanchain
			   uint32_t *pulDataOut - data from scanchain
	   Output: int - result
  Description: scan JTAG IR register for current core
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/
int ML_ScanIR(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	// call function to scan IR, parameters are checked inside function
	tyRes = DL_OPXD_JtagScanIR(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->tyRISCVTapParams[ptyRISCVParams->ulCurrentRISCVTap].ulScanchainTap,
		ulLength, pulDataIn, pulDataOut);
	// check what caused function to fail
	switch (tyRes)
	{
	case DRVOPXD_ERROR_NO_ERROR:
		iResult = ERR_ML_RISCV_NO_ERROR;
		break;
	case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
		iResult = ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED;
		break;
	default:
		iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
		break;
	}
	return iResult;
}

/****************************************************************************
	 Function: ML_ScanDR
	 Engineer: Vitezslav Hola
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
			   uint32_t ulLength - number of bits to scan
			   uint32_t *pulDataIn - data to scanchain
			   uint32_t *pulDataOut - data from scanchain
	   Output: int - result
  Description: scan JTAG DR register for current core
Date           Initials    Description
04-Dec-2007    VH          Initial
****************************************************************************/
int ML_ScanDR(TyRISCVTargetParams *ptyRISCVParams, uint32_t ulLength, uint32_t *pulDataIn, uint32_t *pulDataOut)
{
	TyError tyRes = DRVOPXD_ERROR_NO_ERROR;
	int iResult = ERR_ML_RISCV_NO_ERROR;

	if (ptyRISCVParams == NULL)
		return ERR_ML_RISCV_INVALID_HANDLE;
	// call function to scan DR, parameters are checked inside function
	tyRes = DL_OPXD_JtagScanDR(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->tyRISCVTapParams[ptyRISCVParams->ulCurrentRISCVTap].ulScanchainTap,
		ulLength, pulDataIn, pulDataOut);
	// check what caused function to fail
	switch (tyRes)
	{
	case DRVOPXD_ERROR_NO_ERROR:
		iResult = ERR_ML_RISCV_NO_ERROR;
		break;
	case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
		iResult = ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED;
		break;
	default:
		iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
		break;
	}
	return iResult;
}

/****************************************************************************
	 Function: ML_ScanMultiple
	 Engineer: Nikolay Chokoev
		Input: TyRISCVTargetParams *ptyRISCVParams - RISCV target parameters/settings
	   Output: int - result
  Description:
Date           Initials    Description
03-Sept-2008   NCH         Initial
****************************************************************************/
int ML_ScanMultiple(TyRISCVTargetParams *ptyRISCVParams, TyMultipleScanParams *ptyMultipleScanParams,
	uint32_t *pulDataIn, uint32_t *pulDataOut)
{
	int iRes = DRVOPXD_ERROR_NO_ERROR;
	uint32_t *ppulDataIn[1];
	uint32_t *ppulDataOut[1];

	ppulDataOut[0] = (uint32_t *)pulDataOut;
	ppulDataIn[0] = (uint32_t *)pulDataIn;

	dbgprint("SM:ulCurrentRISCVTap:%08lX\n", ptyRISCVParams->ulCurrentRISCVTap);
	if (ppulDataIn[0] != NULL)
		dbgprint("SM:*ppulDataIn[0]:%08lX\n", (uint32_t)*ppulDataIn[0]);
	else
		dbgprint("SM:*ppulDataIn[0]:<null>\n");
	iRes = DL_OPXD_JtagScanMultiple(ptyRISCVParams->iCurrentHandle, ptyRISCVParams->ulCurrentRISCVTap, 1,
		ptyMultipleScanParams, ppulDataIn, ppulDataOut);
	dbgprint("SM:iRes:%08lX\n", iRes);
	if (ppulDataOut[0] != NULL)
		dbgprint("SM:*ppulDataOut[0]:%08lX\n", (uint32_t)*ppulDataOut[0]);
	else
		dbgprint("SM:*ppulDataOut[0]:<null>\n");
	if (iRes != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_ResetTAP
	 Engineer: Nikolay Chokoev
		Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
	   Output: int - result
  Description: Reset the TAP statemachine
Date           Initials    Description
03-Sept-2008   NCH         Initial
16-Jan  -2009    RS           Added Reset using TRST pin.
****************************************************************************/
int ML_ResetTAP(TyRISCVTargetParams *ptyRISCVParams, int bResetType)
{
	int iRes = DRVOPXD_ERROR_NO_ERROR;
	double dFrequency;
	uint32_t ulCurrentPulsePeriod;
	uint32_t ulTDI;
	uint32_t ulTMS;
	uint32_t ulTDO;
	TyDeviceInfo tyLocDeviceInfo;

	//Opella-XD R2 FW/DW still dont support CMD_CODE_DISKWARE_STATUS command, so dont use that for R2
	if (DL_OPXD_GetDeviceInfo(ptyRISCVParams->iCurrentHandle, &tyLocDeviceInfo, DW_UNKNOWN) != DRVOPXD_ERROR_NO_ERROR)
	{
		(void)DL_OPXD_CloseDevice(ptyRISCVParams->iCurrentHandle);
		return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}

	if (tyLocDeviceInfo.ulBoardRevision < 0x03000000)
	{
		tyLocDeviceInfo.uiActiveDiskwares = 0;
		tyLocDeviceInfo.bOtherDiskwareRunning = 0;
	}
	else
	{
		//Check if diskware is already running (could be that its loaded by another instance of GDB server)
		if (DL_OPXD_GetDiskwareStatus(ptyRISCVParams->iCurrentHandle, &tyLocDeviceInfo, RISCV) != DRVOPXD_ERROR_NO_ERROR)
		{
			(void)DL_OPXD_CloseDevice(ptyRISCVParams->iCurrentHandle);
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
		}
	}

	if ((tyLocDeviceInfo.uiActiveDiskwares > 1) || tyLocDeviceInfo.bOtherDiskwareRunning)
	{
		//Some diskware is running, so dont change JTAG freq now!
		return ERR_ML_RISCV_NO_ERROR;
	}

	dFrequency = ptyRISCVParams->dJtagFrequency;

	if (dFrequency <= 0.001)
		ulCurrentPulsePeriod = 1000;
	else if (dFrequency >= 1.0)
		ulCurrentPulsePeriod = 1;
	else
	{
		double dPulsePeriod = dFrequency;   //frequency in MHz
		dPulsePeriod = 1.0 / dPulsePeriod;  // get pulse period in microseconds
		ulCurrentPulsePeriod = (uint32_t)(dPulsePeriod + 0.5);
		// check if pulse period is in valid range
		if (ulCurrentPulsePeriod < 1)
			ulCurrentPulsePeriod = 1;
		else if (ulCurrentPulsePeriod > 1000)
			ulCurrentPulsePeriod = 1000;
	}

	if (bResetType)
	{
		//reset the TAP using TRST pin.
		iRes = DL_OPXD_JtagResetTAP(ptyRISCVParams->iCurrentHandle, 1);
		if (iRes != DRVOPXD_ERROR_NO_ERROR)
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}
	else
	{
		//reset the TAP with TMS sequence
		iRes = DL_OPXD_JtagResetTAP(ptyRISCVParams->iCurrentHandle, 0);
		if (iRes != DRVOPXD_ERROR_NO_ERROR)
			return ERR_ML_RISCV_USB_DRIVER_ERROR;
	}
	//now have to go to RTI
	ulTDI = 0; ulTMS = 0;
	iRes = DL_OPXD_JtagPulses(ptyRISCVParams->iCurrentHandle, 1, ulCurrentPulsePeriod,
		&ulTDI, &ulTMS, &ulTDO);
	if (iRes != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_USB_DRIVER_ERROR;

	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
	 Function: ML_ConfigureVega
	 Engineer: 
		Input: TyArcTargetParams *ptyArcParams - ARC target parameters/settings
			   unsigned int uiInstrtoSelectADI - 
			   unsigned int uiCore - CoreId
			   unsigned int uiIRLen - IRLength
	   Output: int - result
  Description: 
Date           Initials    Description
03-Sept-2008   NCH         Initial
               
****************************************************************************/
int ML_ConfigureVega(TyRISCVTargetParams *ptyRISCVParams, unsigned short usTapId, unsigned int uiInstrtoSelectADI, unsigned int uiCore, unsigned int uiIRLen)
{
	int iRes = DRVOPXD_ERROR_NO_ERROR;
	
	iRes = DL_OPXD_ConfigureVega(ptyRISCVParams->iCurrentHandle, usTapId, uiInstrtoSelectADI, uiCore, uiIRLen);
	if (iRes != DRVOPXD_ERROR_NO_ERROR)
		return ERR_ML_RISCV_USB_DRIVER_ERROR;

	return ERR_ML_RISCV_NO_ERROR;
}

/****************************************************************************
		Function: ML_ConvertDrvlayerErrorToMLError
		Engineer: Ashutosh Garg
		Input:	  TyError - DrvLayerError		 
		Output:	  int - error code ERR_ML_ARC_xxx
		Description: convert drv llaer error to middle layer errors.
		Date           Initials    Description
		1-APR-2019		  AG        Initial
****************************************************************************/
int ML_ConvertDrvlayerErrorToMLError(TyError drvLayerError)
{
	int iResult = ERR_ML_RISCV_NO_ERROR;
	switch(drvLayerError)
	{
	case DRVOPXD_ERROR_NO_ERROR:
		iResult = ERR_ML_RISCV_NO_ERROR;
		break;
	case DRVOPXD_ERROR_UNKNOWN_CMD:
		iResult = ERR_ML_UNKNOWN_CMD;
		break;
	case DRVOPXD_ERROR_ADAPTIVE_CLOCK_TIMEOUT:
		iResult = ERR_ML_RISCV_RTCK_TIMEOUT_EXPIRED;
		break;
	case DRVOPXD_ERROR_RISCV_DEBUG_ACCESS:
		iResult = ERR_ML_RISCV_DEBUG_ACCESS;
		break;
	case DRVOPXD_ERROR_RISCV_DEBUG_PDOWN:
		iResult = ERR_ML_RISCV_DEBUG_PDOWN;
		break;
	case DRVOPXD_ERROR_RISCV_DEBUG_POFF:
		iResult = ERR_ML_RISCV_DEBUG_POFF;
		break;
	case DRVOPXD_ERROR_RISCV_TARGET_RESET_OCCURED:
		iResult = ERR_ML_RISCV_TARGET_RESET_DETECTED;
		break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUSY:
		iResult = ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUSY;
		break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED:
		iResult = ERR_ML_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED;
		break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION:
		iResult = ERR_ML_RISCV_ABSTRACT_REG_WRITE_EXCEPTION;
		break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_HART_STATE:
		iResult = ERR_ML_RISCV_ABSTRACT_REG_WRITE_HART_STATE;
		break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR:
		iResult = ERR_ML_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR;
		break;
	case DRVOPXD_ERROR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON:
		iResult = ERR_ML_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR;
		break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED:
		iResult = ERR_ML_RISCV_SYSTEM_BUS_TIMEOUT_FAILED;
		break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ADDRESS_FAILED:
		iResult = ERR_ML_RISCV_SYSTEM_BUS_ADDRESS_FAILED;
		break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED:
		iResult = ERR_ML_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED;
		break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED:
		iResult = ERR_ML_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED;
		break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_OTHER_REASON:
		iResult = ERR_ML_RISCV_SYSTEM_BUS_OTHER_REASON;
		break;
	case DRVOPXD_ERROR_RISCV_SYSTEM_BUS_MASTER_BUSY:
		iResult = ERR_ML_RISCV_SYSTEM_BUS_MASTER_BUSY;
		break;
	case DRVOPXD_ERROR_RISCV_MEM_INVALID_LENGTH:
		iResult = ERR_ML_RISCV_MEM_INVALID_LENGTH;
		break;
	case DRVOPXD_ERROR_RISCV_HALT_TIMEOUT:
		iResult = ERR_ML_RISCV_HALT_TIMEOUT;
		break;
	case DRVOPXD_ERROR_RISCV_RESUME_TIMEOUT:
		iResult = ERR_ML_RISCV_RESUME_TIMEOUT;
		break;
	case DRVOPXD_ERROR_RISCV_UNSUPPORT_ARCH_TYPE:
		iResult = ERR_ML_RISCV_UNSUPPORT_ARCH_TYPE;
		break;
	case DRVOPXD_ERROR_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE:
		iResult = ERR_ML_RISCV_UNSUPPORT_MEMORY_ACCESS_SIZE;
		break;
	case DRVOPXD_ERROR_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION:
		iResult = ERR_ML_RISCV_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;
		break;
	case DRVOPXD_ERROR_RISCV_TRIGGER_INFO_LOOP_TIMEOUT:
		iResult = ERR_ML_RISCV_TRIGGER_INFO_LOOP_TIMEOUT;
		break;
	case DRVOPXD_ERROR_RISCV_DISALE_ABSTRACT_COMMAND:
		iResult = ERR_ML_RISCV_DISALE_ABSTRACT_COMMAND;
		break;
	case DRVOPXD_ERROR_RISCV_NEXT_DEBUG_MODULE_TIMEOUT:
		iResult = ERR_ML_RISCV_NEXT_DEBUG_MODULE_TIMEOUT;
		break;
	default:
		iResult = ERR_ML_RISCV_USB_DRIVER_ERROR;
		break;
	}
	return iResult;
}