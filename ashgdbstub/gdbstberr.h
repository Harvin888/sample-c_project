/*****************************************************************************
      Module: gdbstberr.h
    Engineer: Dileep Kumar K 
 Description: Ashling Ash GDBServer Stub error handling number
 Date		Initials	Description
 04-Oct-2010	DK		Initial
*****************************************************************************/
#ifndef ASHGDBSTBERR_H_
#define ASHGDBSTBERR_H_

#define ASHGDBSTB_NO_ERROR  0x0
#define ASHGDBSTB_OPT_ERROR 0x2001
/*Macro to report command line option repetition*/
#define ASHGDBSSTB_EMSG_CLINE_OPT_RPTED     "Command line option %s repeated."
/*Macro to report invalid command line option*/
#define ASHGDBSTB_EMSG_CLINE_INV_OPTION     "Invalid command line option %s."
/*Macro to report unavialable of QEMU Server*/
#define ASHGDBSTB_EMSG_CLNT_NO_LISTN_SOCKET "Can not connect to QEMU server,"\
/*Macro to report unavailability of AshGDBStub server*/
#define ASHGDBSTB_EMSG_SRV_NO_LISTN_SOCKET  "Can not Open listner socket for "\
	                                    "AshGdbStb Server.Error %d occurred."
/*Macro to report error in binding the AshGdbStub server */
#define ASHGDBSTB_EMSG_SRV_ERR_BIND_ADDR    "Could not bind to socket."
/*Macro to report error message related to accept system call*/
#define ASHGDBSTB_EMSG_SRV_ERR_ACPT         "Unable to accept connection from"\
	                                    "GDB server."
/*Macro to report thread_create error*/
#define ASHGDBSTB_EMSG_THREAD_CREATE        "Unable to create thread for"\
                                                                  "read write."
/*Macro to report error for connect system call*/
#define ASHGDBSTB_EMASG_CLNT_CONCT_FAIL     "Connection failed while connecting"\
	                                    " to QEMU server.Error %d occurred."

#endif
