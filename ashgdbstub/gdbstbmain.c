/*****************************************************************************
 Module		:gdbstbmain.c
 Engineer	:Rejeesh S Babu
 Description:Ashling GDB-GDBServer Stub, main
 Date			Initials	Description
 15-Nov-2010		RSB			Initial
 *****************************************************************************/
#include <stdio.h>
#include "gdbstb.h"
#include "gdbstbopt.h"
#include "gdbstberr.h"

TyServerInfo tySI; //Global structure with server info
static pthread_t iGdbThread, iQemuThread;
/*****************************************************************************
 Function	 : main
 Engineer	 : Rejeesh S Babu
 Input		 : int argc        : Number of program arguments
 char *argv[]: Array of argument (strings) from command line
 Output		 : int             : Program return Value
 Descriptiom : Accept connection rom GDB,Connects to QEMU server,create separate threads for client and server
 Date            Initials        Description
 15-Nov-2010     RSB              Initial
 *****************************************************************************/
int main(int argc, char *argv[]) {
	int *iStatus = 0;
	//Process command line arguments
	if (ProcessCmdLineArgs(argc, argv)) {
		printf("%s\n", tySI.szError);
		return -1;
	}
	//Wait for GDB client connection
	if (WaitForGDBConnection()) {
		printf("%s\n", tySI.szError);
		return -1;
	}
	//GDB connection OK,now Connect to QEMU GDBServer
	if (ConnectToQemuServer()) {
		printf("%s\n", tySI.szError);
		return -1;
	}
	//Create thread for recieving data from GDB and sending to QEMU server
	if ((pthread_create(&iGdbThread, NULL, TransferGDBtoQEMU, NULL)) != 0) {
		printf(ASHGDBSTB_EMSG_THREAD_CREATE);
		return -1;
	}
	//Create thread for recieving data from QEMU and sending to GDB
	if ((pthread_create(&iQemuThread, NULL, TransferQEMUtoGDB, NULL)) != 0) {
		printf(ASHGDBSTB_EMSG_THREAD_CREATE);
		return -1;
	}

	pthread_join(iGdbThread, (void **) &iStatus);
	pthread_join(iQemuThread, (void **) &iStatus);

	return 0;
}
