/****************************************************************************
      Module: gdbstbglob.h
    Engineer: Dileep Kumar K
 Description: Ashling GDB server stub, global header file
 Date		Initials	Description
 04-Oct-2010	DK		Initials
****************************************************************************/ 

#ifndef ASHGDBSTB_H_
#define ASHGDBSTB_H_
//#define NULL ((void *)0)
#define NOREF(var)	(var = var)	//No reference used in function
#define _MAX_PATH_	260

#if defined(MIPS)
	#define ASHGDBSRVSTUB_PROG_NAME		"ash-gdbserver-stub"
	#define ASHGDBSRVSTUB_PROC_FAMILY	_MIPS
	#define ASHGDBSRVSTUB_ARCH_NAME		"MIPS"
	#define ASHGDBSRVSTUB_PROG_TITLE	"Ashling GDB server Stub for"\
                                                "ASHGDBSRVSTUB_ARCH_NAME" \
                                                ("ASHGDBSRVSTUB_PROG_NAME").
	#define ASHGDBSRVSTUB_PROG_VERSION	"v1.0.0, 04-Oct-2010,"\
	                                        "   (c)Ashling Microsystems Ltd 2010."
#endif

#endif
