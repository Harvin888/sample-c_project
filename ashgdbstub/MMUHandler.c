/*****************************************************************************
 Module		: MMUHandler.c
 Engineer	: Rejeesh S Babu
 Description: Handles all MMU related actions
 Date		Initials	Description
 15-Nov-2010	RSB		Initial
 *****************************************************************************/
#define APP_SEC_END_ADRR 0x80000000
#define ASID_MASK 0x000000FF
#define MIPS_TLB_MAX 128

#include <string.h>
#include "MMUHandler.h"
#include "gdbstb.h"

static TyTLBQemuEntry tlbEntryBackup;
static unsigned int guiGlobalPgd = 0;
static unsigned int guiAppPgd = 0;
static unsigned int guiAppContext = 0;
static int giWriteAccess = 0;
static int giBigEndian = 0;
static unsigned char *gpucGDBReply;
static unsigned int gitlbentries[MIPS_TLB_MAX];
static unsigned int guitlbcnt = 0;

/*****************************************************************************
 Function	: IsMappingRequired
 Engineer	: Rejeesh S Babu
 Input		: long lVirtualAddr        : Virtual Address
 Output 	: 	1 if mapping required
				0 if mapping not required
 Description: Checks if mapping is required for lVirtualAddr
 Date            Initials        Description
 15-Nov-2010		  RSB			  Initial
 *****************************************************************************/
int MMU_IsMappingRequired(unsigned int uiVirtualAddr) {
	unsigned int msb_bits = 0;
	/*
	 MIPS Memory mapping's are
	 useg  0x0000.0000 0x7FFF.FFFF  Mapped   Cached
	 kseg0 0x8000.0000 0x9FFF.FFFF  Unmapped Cached
	 kseg1 0xA000.0000 0xBFFF.FFFF  Unmapped Uncached
	 kseg2 0xC000.0000 0xDFFF.FFFF  Mapped   Cached
	 kseg3 0xE000.0000 0xFFFF.FFFF  Mapped   Cached
	 */
	//We are checking the first 2 bits to binary 10
	//If there is a result, we are return that mapping needed.

	msb_bits = uiVirtualAddr >> 30;
	return (msb_bits ^ 2) ? 1 : 0;
}

/*****************************************************************************
 Function	: MMU_HandleOsMMUMapping
 Engineer	: Rejeesh S Babu
 Input  	: 	long lVirtualAddr        : Virtual Address
				int *piRestoretlb		 :Store this pointer with 1 fi TLB is to be restored 
 Output 	: err return:1 if error,0 otherwise
 Description: Handle MMU mapping of OS
 Date            Initials        Description
 15-Nov-2010		RSB			  Initial
 *****************************************************************************/
int MMU_HandleOsMMUMapping(unsigned int uiVirtualAddr, int *piRestoretlb) {
	int tlbIndex = -1;
	unsigned int uiPgdIndex;
	unsigned int uiPgdAddr;
	unsigned int uiPteBaseAddr;
	unsigned int uiPteAddr;
	unsigned int uiPfn1;
	unsigned int uiPfn2;
	unsigned int uiPgd;
	unsigned short usPteIndex;

	TyTLBQemuEntry tytlbEntry;
	TyEntryLo tyEntryLo0;
	TyEntryLo tyEntryLo1;

	memset(&tytlbEntry, 0, sizeof(tytlbEntry));

	//tlbEntry.EntryHi.Bits.VPN2 = ulVirtualAddr >> (PAGE_SHIFT + 1);
	tytlbEntry.VPN = uiVirtualAddr;

	//First we checking for a TLB entry exist or not
	if (0 <= (tlbIndex = MMU_SearchTLB(tytlbEntry.VPN))) {
		int itlbEntryModified = 0;
		//TLB entry exists for the address. we have to make its cache property to
		//write-through. Otherwise it wont update the memory.
		if (giWriteAccess) {
			(void) MMU_ReadTLBEntry(tlbIndex, &tytlbEntry);
			if ((tytlbEntry.PFN[1] != 0) && (tytlbEntry.D1 != 1)) {
				tytlbEntry.D1 = 1; //Making dirty bit to 1
				itlbEntryModified = 1;
			}
			if ((tytlbEntry.PFN[0] != 0) && (tytlbEntry.D0 != 1)) {
				tytlbEntry.D0 = 1; //Making dirty bit to 1
				itlbEntryModified = 1;
			}
			if (tytlbEntry.C1 == 3) //Cacheable, noncoherent, write-back, write allocate
			{
				tytlbEntry.C1 = 1; //Cacheable, noncoherent, write-through, write allocate
				itlbEntryModified = 1;
			}
			if (tytlbEntry.C0 == 3) //Cacheable, noncoherent, write-back, write allocate
			{
				tytlbEntry.C0 = 1; //Cacheable, noncoherent, write-through, write allocate
				itlbEntryModified = 1;
			}
		}

		if (itlbEntryModified) {
			(void) MMU_WriteTLBEntry(tlbIndex, &tytlbEntry);
		}
		return 0;
	}
	// Not found a TLB entry. So we have to create one.

	if (uiVirtualAddr < APP_SEC_END_ADRR) {
		uiPgd = guiAppPgd;
		tytlbEntry.ASID = guiAppContext & ASID_MASK;
	} else {
		uiPgd = guiGlobalPgd;
	}

	if (uiPgd == 0)
	{
		printf("Global Page Dir:%d\n",uiPgd);
		return 1;
	}
	// finding pgd index
	uiPgdIndex = pgd_index(uiVirtualAddr);
	uiPgdAddr = (uiPgd + (uiPgdIndex * 4));

	//Getting pte address
	(void) MMU_ReadWord(uiPgdAddr, &uiPteBaseAddr);
	// If target is little endian, swap the bytes
	if (!giBigEndian)
		uiPteBaseAddr = Opt_SwapEndianWord(uiPteBaseAddr);

	if (uiPteBaseAddr == 0)
		return 1;

	//finding pte index
	usPteIndex = (unsigned short) __pte_offset(uiVirtualAddr) & 0xFFFE;
	uiPteAddr = (uiPteBaseAddr + (usPteIndex * 4));

	//Getting pfn1
	(void) MMU_ReadWord(uiPteAddr, &uiPfn1);
	// If target is little endian, swap the bytes
	if (!giBigEndian)
		uiPfn1 = Opt_SwapEndianWord(uiPfn1);

	usPteIndex++;
	uiPteAddr = (uiPteBaseAddr + (usPteIndex * 4));

	//Getting pfn2
	(void) MMU_ReadWord(uiPteAddr, &uiPfn2);
	// If target is little endian, swap the bytes
	if (!giBigEndian)
		uiPfn2 = Opt_SwapEndianWord(uiPfn2);

	if ((uiPfn1 == 0) && (uiPfn2 == 0))
		return 1;

	//Backing up the first entry of TLB
	*piRestoretlb = 1;
	(void) MMU_ReadTLBEntry(0x0, &tlbEntryBackup);

	//Create new TLB entry
	tytlbEntry.PageMask = 0;

	tyEntryLo0.Data = uiPfn1 >> 6; //Got from __update_tlb fn in Linux
	tyEntryLo1.Data = uiPfn2 >> 6; //Got from __update_tlb fn in Linux

	tytlbEntry.PFN[0] = tyEntryLo0.Bits.PFN << 12;
	tytlbEntry.PFN[1] = tyEntryLo1.Bits.PFN << 12;

	tytlbEntry.G = tyEntryLo0.Bits.G;
	tytlbEntry.V0 = tyEntryLo0.Bits.V;
	tytlbEntry.V1 = tyEntryLo1.Bits.V;
	tytlbEntry.D0 = tyEntryLo0.Bits.D;
	tytlbEntry.D1 = tyEntryLo1.Bits.D;
	tytlbEntry.C0 = tyEntryLo0.Bits.C;
	tytlbEntry.C1 = tyEntryLo1.Bits.C;

	if (giWriteAccess) {
		if ((tytlbEntry.PFN[1] != 0) && (tytlbEntry.D1 != 1)) {
			tytlbEntry.D1 = 1; //Making dirty bit to 1
		}
		if ((tytlbEntry.PFN[0] != 0) && (tytlbEntry.D0 != 1)) {
			tytlbEntry.D0 = 1; //Making dirty bit to 1
		}
	}
	if (tytlbEntry.C1 == 3) //Cacheable, noncoherent, write-back, write allocate
	{
		tytlbEntry.C1 = 1; //Cacheable, noncoherent, write-through, write allocate
	}
	if (tytlbEntry.C0 == 3) //Cacheable, noncoherent, write-back, write allocate
	{
		tytlbEntry.C0 = 1; //Cacheable, noncoherent, write-through, write allocate
	}
	return (MMU_WriteTLBEntry(0x0, &tytlbEntry));
}

/*****************************************************************************
 Function	: RestoreTLB
 Engineer	: Rejeesh S Babu
 Input		: void
 Output 	:int error_code :1 if error
 Description: Restore TLB Entry
 Date            Initials        Description
 15-Nov-2010		  RSB			  Initial
 *****************************************************************************/
int MMU_RestoreTLBEntry(void) 
{
	return MMU_WriteTLBEntry(0x0, &tlbEntryBackup);
}
/*****************************************************************************
 Function	: ReadTLBEntry
 Engineer	: Rejeesh S Babu
 Input		: 	unsigned long uiIndex - Index ofTLB
				TyTLBQemuEntry *ptyTLBEntry - pointer to store TLB entries
 Output 		:int - error code
 Description	: read MIPS processor TLB entry
 Date            Initials        Description
 07-Dec-2010		 RSB		  Initial
 *****************************************************************************/
int MMU_ReadTLBEntry(unsigned int uiIndex, TyTLBQemuEntry *ptyTLBEntry) {
	int i = 1, j = 0, len = 0;
	long idx = -1;
	char pcOutPacket[100] = { 0 };
	char szRd[100] = "readtlb ";
	char szInd[100];
	char *sztemp;
	char *sztoken;
	unsigned char szmem_buf[100], szRply[100];

	sprintf(szInd, "0x%x", uiIndex);
	char *pcCmd = strcat(szRd, szInd);
#ifdef __DEBUG
	printf("Internally generated command:%s\n", pcCmd);
#endif
	sprintf(pcOutPacket + 1, "qRcmd,");//info registers");
	for (i = 0, j = 7; i < strlen(pcCmd); i++, j += 2) {
		sprintf(pcOutPacket + j, "%x", (char) pcCmd[i]);
	}

	SendDatatoQEMU(pcOutPacket, strlen(pcOutPacket), 2);
	WaitForBufUpdtn();

	if (gpucGDBReply[0] == '+')
		i = 2;
	else
		i = 1;

	for (j = 0; i < strlen(gpucGDBReply); i++, j++) {
		if (gpucGDBReply[i] == '#')
			break;
		szRply[j] = gpucGDBReply[i];

	}
	szRply[j] = '\0';
	GetString(szRply + 1, szmem_buf, NULL);
#ifdef __DEBUG
	printf("TLB read:%s\n",szmem_buf);
#endif
	//String Format:tlb->VPN,tlb->PageMask,tlb->ASID,tlb->G,tlb->C0,tlb->C1,tlb->V0,tlb->V1,tlb->D0,tlb->D1,tlb->PFN[0],tlb->PFN[1]
	sztoken = strtok(szmem_buf, " ");
	ptyTLBEntry->VPN = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->PageMask = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->ASID = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->G = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->C0 = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->C1 = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->V0 = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->V1 = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->D0 = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->D1 = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->PFN[0] = strtoul(sztoken, NULL, 16);

	sztoken = strtok(NULL, " ");
	ptyTLBEntry->PFN[1] = strtoul(sztoken, NULL, 16);

	return 0;

}
/*****************************************************************************
 Function	: MMU_WriteTLBEntry
 Engineer 	: Rejeesh S Babu
 Input		: 	unsigned long uiIndex - Index of TLB
				TyTLBEntry *ptyTLBEntry - pointer to TLB entries
 Output 	:int - error code
 Description: Write MIPS processor TLB entry
 Date            Initials        Description
 08-Dec-2010		RSB			  Initial
 *****************************************************************************/
int MMU_WriteTLBEntry(unsigned int uiIndex, TyTLBQemuEntry *ptyTLBEntry) {
	//assert(ptyTLBEntry != NULL);
	char szCmdStrng[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	char szOutPkt[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	char szReply[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	unsigned char szmem_buf[100], szRply[100];
	int icmdlen = 0, len = 0, i, j;

	len = sprintf(szCmdStrng, "writetlb ");
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "%x ", uiIndex);
	if (len == -1) {
		return len;
	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->VPN);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->PageMask);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->ASID);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->G);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->C0);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->C1);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->V0);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->V1);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->D0);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->D1);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x ", ptyTLBEntry->PFN[0]);
	if (len == -1) {
		return len;

	}
	icmdlen += len;
	len = sprintf(szCmdStrng + icmdlen, "0x%x", ptyTLBEntry->PFN[1]);
	if (len == -1) {
		return len;

	}
	szCmdStrng[icmdlen + len] = '\0';
#ifdef __DEBUG
	printf("Internally generated command:%s\n", szCmdStrng);
#endif
	sprintf(szOutPkt + 1, "qRcmd,");
	for (i = 0, j = 7; i < strlen(szCmdStrng); i++, j += 2) {
		sprintf(szOutPkt + j, "%x", (char) szCmdStrng[i]);
	}

	SendDatatoQEMU(szOutPkt, strlen(szOutPkt), 1);
	WaitForBufUpdtn();
	return 0;

}
/*****************************************************************************
 Function: ReadWord
 Engineer: Rejeesh S Babu
 Input	: 	unsigned int ulAddr - address to read
			unsigned int *puiData - pointer to store read data
 Output :int - error code 0if NO_ERROR
 Description: read a word
 Date            Initials        Description
 08-Dec-2010		  RSB			  Initial
 *****************************************************************************/
int MMU_ReadWord(unsigned int uiAddr, unsigned int *puiData) {
	int i = 0, j = 0;
	unsigned char szOutPkt[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	unsigned char szRdWrd[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	unsigned char szmem_buf[ASHGDBSRV_SOCKET_BUFFER_SIZE];

	sprintf(szOutPkt + 1, "m%x,4", (unsigned int) uiAddr);
#ifdef __DEBUG
	printf("Internally generated command:%s\n", szOutPkt);
#endif
	SendDatatoQEMU(szOutPkt, strlen(szOutPkt), 1);
	WaitForBufUpdtn();

	if (gpucGDBReply[0] == '+')
		i = 2;
	else
		i = 1;
	for (j = 0; i < strlen(gpucGDBReply); i++, j++) {
		if (gpucGDBReply[i] == '#')
			break;
		szRdWrd[j] = gpucGDBReply[i];

	}
	szRdWrd[j] = '\0';
	*puiData = strtoul(szRdWrd, NULL, 16);
	return 0;
}
/*****************************************************************************
 Function	:MMU_EnableOSMMU
 Engineer	:Rejeesh S Babu
 Input		:unsigned int uiPgDirAddrs - Address of swapper_pg_dir
 Output 		:int - error code : 0 if no error
 Description	:Set the addr of swapper_pg_dir
 Date            Initials        Description
 08-Dec-2010		  RSB			  Initial
 *****************************************************************************/
int MMU_EnableOSMMU(unsigned int uiPgDirAddrs) {
	guiGlobalPgd = uiPgDirAddrs;
	return 0;
}
/*****************************************************************************
 Function	:MMU_SetOSAppMMU
 Engineer	:Rejeesh S Babu
 Input		:unsigned int AppContext - Address of AppContext
			 unsigned int AppPgd - Address of AppPgd
 Output 	:int - error code : 0 if no error
 Description:Set the addr of AppPgd and AppContext
 Date            Initials        Description
 07-Jan-2010		RSB			  Initial
 *****************************************************************************/
int MMU_SetOSAppMMU(unsigned int AppContext, unsigned int AppPgd) {
	guiAppPgd = AppPgd;
	guiAppContext = AppContext;
	return 0;
}
/****************************************************************************
 Function	: MMU_UpdateTLB
 Engineer	: Rejeesh S Babu
 Input		: void
 Output		: -1 if entry not found,TLB index otherwise
 Description	: Update all TLB entries
 Date           Initials    Description
 21-Jan-2011    RSB          Initial
 ****************************************************************************/
int MMU_UpdateTLBEntries()
{
	int i = 1, j = 0, len = 0;
	long idx = -1;
	char pcOutPacket[100] = { 0 };
	char szUpdtCmd[11] = "updatetlb";
	char szInd[100];
	char *sztemp;
	char *sztoken;
	unsigned char *szmem_buf;
	unsigned char *szRply;

	#ifdef __DEBUG
	printf("Internally generated command:%s\n", szUpdtCmd);
	#endif
	sprintf(pcOutPacket + 1, "qRcmd,");
	for (i = 0, j = 7; i < strlen(szUpdtCmd); i++, j += 2) {
		sprintf(pcOutPacket + j, "%x", (char) szUpdtCmd[i]);
	}

	SendDatatoQEMU(pcOutPacket, strlen(pcOutPacket), 2);
	WaitForBufUpdtn();

	if (gpucGDBReply[0] == '+')
		i = 2;
	else
		i = 1;

	szRply	  = (unsigned char*)malloc(strlen(gpucGDBReply));
	szmem_buf = (unsigned char*)malloc(strlen(gpucGDBReply));

	for (j = 0; i < strlen(gpucGDBReply); i++, j++) {
		if (gpucGDBReply[i] == '#')
			break;
		szRply[j] = gpucGDBReply[i];

	}
	szRply[j] = '\0';
	GetString(szRply + 1, szmem_buf, NULL);

	i=0;
	sztoken = strtok(szmem_buf, " ");
	while(sztoken != NULL)
	{
		gitlbentries[i++] = strtoul(sztoken, NULL, 16);
		sztoken = strtok(NULL, " ");
	}
	guitlbcnt = i-1;
	return 0;
}
/****************************************************************************
 Function	: MMU_SearchTLB
 Engineer	: Rejeesh S Babu
 Input		: unsigned int ulVPN - VPN for searching
 Output		: -1 if entry not found,TLB index otherwise
 Description	: Search for a TLB entry
 Date           Initials    Description
 10-DEC-2010    RSB          Initial
 ****************************************************************************/
int MMU_SearchTLB(unsigned int uiVPN) {

	int i = 0;
	int idx = -1;
	unsigned int uivpn_tag = uiVPN >> (PAGE_SHIFT + 1);
	for (i=0;i<guitlbcnt;i++)
	{
		if( ( gitlbentries[i] >> (PAGE_SHIFT + 1)) == uivpn_tag)
		{
			idx = i;
			//printf("\nVPN match at index:%d\n",i);
			break;
		}
	}
	return idx;

/*	int i = 1, j = 0, len = 0;
	int idx = -1;
	char pcOutPacket[100] = { 0 };
	char szSrch[100] = "srchtlb ";
	char szVpn[100];
	char *pclong;
	unsigned char szmem_buf[100] = { 0 };
	char szRply[100] = { 0 };

	sprintf(szVpn, "0x%x", uiVPN);
	char *pcCmd = strcat(szSrch, szVpn);
#ifdef __DEBUG
	printf("Internally generated command:%s\n", pcCmd);
#endif

	sprintf(pcOutPacket + 1, "qRcmd,");
	for (i = 0, j = 7; i < strlen(pcCmd); i++, j += 2) {
		sprintf(pcOutPacket + j, "%x", (char) pcCmd[i]);
	}

	SendDatatoQEMU(pcOutPacket, strlen(pcOutPacket), 2);
	WaitForBufUpdtn();
	//To handle the case where GDB sends '+' with the pkt
	if (gpucGDBReply[0] == '+') {
		i = 2;
	} else {
		i = 1;
	}
	for (j = 0; i < strlen(gpucGDBReply); i++, j++) {
		if (gpucGDBReply[i] == '#')
			break;
		szRply[j] = gpucGDBReply[i];

	}
	//szRply[i-1] = '\0';
	//'O' always precedes data...we ignore it
	len = strlen(szRply + 1);
	hextomem(szmem_buf, szRply + 1, len);
	if (!strncmp(szmem_buf, "-1", 2))
		return idx;
	idx = strtoul(szmem_buf, &pclong, 10);
#ifdef __DEBUG
	printf("TLB index:%d\n", idx);
#endif
	return idx;*/
}
/****************************************************************************
 Function	: MMU_StoreGDBReply
 Engineer	: Rejeesh S Babu
 Input		: unsigned char *pucGDBRply - GDB reply
 Output		: void
 Description	: Store the GDB reply in gpucGDBReply for internal use
 Date           Initials    Description
 13-DEC-2010    RSB          Initial
 ****************************************************************************/
void MMU_StoreGDBReply(unsigned char *pucGDBRply, int iRcvPkt) {
	//assert(pucGDBRply != NULL);
	gpucGDBReply = (unsigned char*) malloc(iRcvPkt);
	memcpy(gpucGDBReply, pucGDBRply, iRcvPkt);
}
/****************************************************************************
 Function	:MMU_WriteAccess
 Engineer	:Rejeesh S Babu
 Input		:int iWriteAccess - TRUE if mem access command is  write access
 Output		:void
 Description	:Stores whether the command is a memory access command or not
 Date           Initials    Description
 13-DEC-2010    RSB          Initial
 ****************************************************************************/
void MMU_WriteAccess(int iWriteAccess) {
	giWriteAccess = iWriteAccess;
}
/*****************************************************************************
 Function: MMU_SetEndian
 Engineer: Rejeesh S Babu
 Input: int ibBigEndian :TRUE if big endian ,FALSE if little
 Output :void
 Description: Set endianness of the target
 Date            Initials        Description
 20-Dec-2010		  RSB			  Initial
 *****************************************************************************/
void MMU_SetEndian(int ibBigEndian) {
	giBigEndian = ibBigEndian;
}
/*
 * From qEMU source code
 */
static void hextomem(unsigned char *mem, const char *buf, int len) {
	int i;

	for (i = 0; i < len; i++) {
		mem[i] = (fromhexqemu(buf[0]) << 4) | fromhexqemu(buf[1]);
		buf += 2;
	}
	mem[i] = '\0';
}
/*
 * From qemu source code
 */
inline int fromhexqemu(int v) {
	if (v >= '0' && v <= '9')
		return v - '0';
	else if (v >= 'A' && v <= 'F')
		return v - 'A' + 10;
	else if (v >= 'a' && v <= 'f')
		return v - 'a' + 10;
	else
		return 0;
}
