/*****************************************************************************
 Module		: gdbstbsrv.c
 Engineer	: Rejeesh S Babu
 Description: Ashling GDB server stub file.
 	 	 	  Handles packets from and to GDB Client
 Date		Initials	Description
 15-Nov-2010	RSB		Initial
 *****************************************************************************/
#include <stdio.h>
#include "gdbstb.h"
#include "MMUHandler.h"
#include "gdbstbopt.h"

SOCKET GDBListenSocket;// = INVALID_SOCKET;
SOCKET GDBClientSocket;// = INVALID_SOCKET;

TySockAddrIn tyAshGdbstbAddr;
extern TyServerInfo tySI; //Global structure with server info

static unsigned int gbOsdbg = 0;
#define ERROR_SRV 1
#define NO_ERROR_SRV 0
/*****************************************************************************
 Function	:OpenGDBListnerSocket
 Engineer	:Rejeesh S Babu
 Input		:void
 Output		:Error code: 0: NO_ERROR
 	 	 	 	 	 	 -1: ERROR
 Description: Creates server and waits for connection
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
static int OpenGDBListnerSocket(void) 
{
	int iError;
	int val;
#ifndef __LINUX
	//Initializing Winsock
	static WSADATA wsData;
	if (WSAStartup(MAKEWORD(2, 2), &wsData) != 0) {
		printf("Error:WSASTART\n");
		return -1;
	}
#endif
	// Create a socket for the server to listen for client connection(GDB)
	if ((GDBListenSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		iError = GetSocketError();
		printf(ASHGDBSTB_EMSG_SRV_NO_LISTN_SOCKET, iError);
		return -1;
	}
	//Bind the socket
	tyAshGdbstbAddr.sin_family = AF_INET;
	tyAshGdbstbAddr.sin_port = htons(tySI.usGdbSrvPort);
	tyAshGdbstbAddr.sin_addr.s_addr = INADDR_ANY;
	if ((val = bind(GDBListenSocket, (TySockAddr *) &tyAshGdbstbAddr,
			sizeof(TySockAddrIn))) < 0) {
		printf(ASHGDBSTB_EMSG_SRV_ERR_BIND_ADDR);
		return -1;
	}
	if (listen(GDBListenSocket, 1) < 0) {
		//TODO :Change Error message
		printf(ASHGDBSTB_EMSG_SRV_ERR_BIND_ADDR);
		return -1;
	}
	printf("waiting for connection from GDB client");
	return 0;
}
/*****************************************************************************
 Function	:AcceptGDBConnection
 Engineer	:Rejeesh S Babu
 Input		:void
 Output		:Error code: 0: NO_ERROR
 	 	 	 	 	 	-1: ERROR
 Description:Accept connection from GDB client
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
static int AcceptGDBConnection(void) 
{
	int iSockAddrLen = 0;
	iSockAddrLen = sizeof(TySockAddrIn);
	GDBClientSocket = accept(GDBListenSocket, (TySockAddr *) &tyAshGdbstbAddr,
			&iSockAddrLen);
	if (GDBClientSocket < 0) {
		printf(ASHGDBSTB_EMSG_SRV_ERR_ACPT);
		printf("\n");
		return -1;
	}
	return 0;
}
/*****************************************************************************
 Function	: TransferGDBtoQEMU
 Engineer	: Rejeesh S Babu
 Input		: Param
 Output		: NULL pointer
 Description: Thread for receiving data from GDB,do MMU handling if reqd. and then send to QEMU
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
void *TransferGDBtoQEMU(void *param) 
{
	char szInPacket[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	char szCommand[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	char szReply[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	int recvpkt_size = 0;
	int iError_ret = 0;
	int iResult;
	TyPacketType tyPacketType = 0;
	while (1) {
		//Recieve Data from GDB
		recvpkt_size = recv(GDBClientSocket, szInPacket, sizeof(szInPacket), 0);
		szInPacket[recvpkt_size] = '\0';
		#ifdef __DEBUG
		printf("Pkt Rcvd frm GDB :%s ", szInPacket);
		#endif
		if (recvpkt_size == 0) {
			printf("GDB connection closed\n");
			break;
		}
		tyPacketType = Opt_AnalysePkt(szInPacket, szCommand);

		switch (tyPacketType) {
		case OTHERS :
				if (SendDatatoQEMU(szInPacket, recvpkt_size, 0)) 
					{
					printf("GDB connection closed\n");
					return NULL;
					}
				break;
		case HALT	:
			iError_ret = HandlehaltCmd();
			if (SendDatatoQEMU(szInPacket, recvpkt_size, 0))
				{
					printf("GDB connection closed\n");
					return NULL;
				}
			break;
		case MONITOR:
			iError_ret = HandleMonitorCmd(szCommand, szInPacket, recvpkt_size);
			break;
		case MEMACCESS:
			iError_ret = HandleMemAccessCmd(szCommand, szInPacket, recvpkt_size);
			break;
		default:
			if (SendDatatoQEMU(szInPacket, recvpkt_size, 0)) {
				printf("GDB connection closed\n");
				return NULL;
			}
			break;
		}
		if (iError_ret) {
			//Break from loop
			break;
		}
		memset(szInPacket, 0, sizeof(szInPacket));
	}
	return NULL;
}
/*****************************************************************************
 Function	: WaitForGDBConnection
 Engineer	: Rejeesh S Babu
 Input		: void
 Output		: Error code: 0: NO_ERROR
 	 	 	 	 	 	 -1: ERROR
 Description: Creates server socket and wait for connection from GDB
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
int WaitForGDBConnection(void) {
	if (OpenGDBListnerSocket()) {
		return -1;
	}
	if (AcceptGDBConnection()) {
		return -1;
	}
	return 0;
}
/*****************************************************************************
 Function	: SendDatatoGDB
 Engineer	: Rejeesh S Babu
 Input		: char *szInPacket : Packet data to send to GDB
 	 	 	  int recvpkt_size : Size of the packet to send
 	 	 	  bPreparePkt	   : TRUE if $,# and checksum needs to be added to pkt
 Output		: Error code: 0: NO_ERROR
 	 	 	 	 	 	  1: ERROR
 Description: Send data to GDB from QEMU
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
int SendDatatoGDB(char *szInPacket, int recvpkt_size, int bPreparePkt) {
	int iResult, iStrLen, iIndex;
	unsigned char ucCheckSum = 0;
	if (bPreparePkt) {
		//Add checksum and $ to the packet
		szInPacket[0] = '$'; //lint !e527
		iStrLen = (int) strlen(szInPacket);
		// calculate checksum...
		for (iIndex = 1; iIndex < iStrLen; iIndex++) {
			ucCheckSum += (unsigned char) szInPacket[iIndex];
			//szInPacket[iIndex] = (char)ToHex(szInPacket[iIndex]);
		}

		// and the next four bytes are reserved for us...
		szInPacket[iIndex++] = '#';
		szInPacket[iIndex++] = (char) ToHex(ucCheckSum >> 4);
		szInPacket[iIndex++] = (char) ToHex(ucCheckSum);
		szInPacket[iIndex] = '\0';
		recvpkt_size = iIndex;
	}
	iResult = send(GDBClientSocket, szInPacket, recvpkt_size, 0);
#ifdef __DEBUG
	printf("Pkt snd to GDB :%s\n", szInPacket);
#endif
	if (iResult != strlen(szInPacket)) {
		/*printf("send failed: %d\n", WSAGetLastError());
		 closesocket(GDBClientSocket);
		 WSACleanup();*/
		return 1;
	}
	return 0;
}
/*****************************************************************************
 Function	: HandleMemAccess
 Engineer	: Rejeesh S Babu
 Input		: char *szCommand : Command alone
 	 	 	  char *szInPacket: Entire GDB packet
 	 	 	  int recvpkt_size: Size of received packet
 Output		: Error code: 0: NO_ERROR
 	 	 	 	 	 	  1: ERROR
 Description: Handle memory access commands from GDB
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
int HandleMemAccessCmd(char *szCommand, char* szInPacket, int recvpkt_size) {
	unsigned int uiVirtualAddr = 0;
	unsigned short usTlbCnt = 0;
	int *piRestoretlb = (int*) malloc(sizeof(int));
	*piRestoretlb = 0;
	int ipkt = 0;
	char szpkt[100];

	//Check if OS debugging enabled,else do nothing
	if (!gbOsdbg) {
		//Nothing to do,send Data to QEMU directly.
		if (SendDatatoQEMU(szInPacket, recvpkt_size, 0)) {
			printf("GDB connection closed\n");
			return ERROR_SRV;
		}
		return NO_ERROR_SRV;
	}

	// Check if MMU mapping is required
	uiVirtualAddr = Opt_DecodeVirtualAddr(szCommand);
	if (!MMU_IsMappingRequired(uiVirtualAddr)) {
		//Possibly this is unmapped area,so post the comment,no mapping required.
		if (SendDatatoQEMU(szInPacket, recvpkt_size, 0)) {
			printf("GDB connection closed\n");
			return ERROR_SRV;
		}
		return NO_ERROR_SRV;
	}
#ifdef __DEBUG
	printf("\n\n=========== MMU HANDLER START ===============\n");
#endif
	//MMU Mapping:Virtual-> Physical
	if (MMU_HandleOsMMUMapping(uiVirtualAddr, piRestoretlb)) {
#ifdef __DEBUG
		printf("MMU HANDLER FAILED!!!\n");
#endif
		//MMU Handling failed.Need to do something
	}
#ifdef __DEBUG
	printf("\n\n===========  MMU HANDLER END ===============\n");
#endif
	//Send Data to QEMU
	if (SendDatatoQEMU(szInPacket, recvpkt_size, 0)) {
		printf("GDB connection closed\n");
		return ERROR_SRV;
	}
	if (*piRestoretlb) {
		//QEMU will send a pkt to GDB now,
		//so we should receive pkt from gdb and send to QEMU here
		ipkt = recv(GDBClientSocket, szpkt, sizeof(szpkt), 0);
		szpkt[ipkt] = '\0';
#ifdef __DEBUG
		printf("Pkt Rcvd frm GDB :%s ", szpkt);
#endif
		if (ipkt == 0) {
			printf("GDB connection closed\n");
			return ERROR_SRV;
		}
		//We expected '+' from GDB,still check if GDB has sent an additional pkt along with '+'
		if (szpkt[1] == '$') {
			//We are not in a state to handle this pkt now,hence send a negative ACK '-' so that GDB will send it again later
			(void) SendDatatoGDB("-", 1, 0);
			//QEMU is waiting for an ACK,so send it
			SendDatatoQEMU("+", 1, 0);

		} else {
			//GDB has sent only '+',so its OK to send the pkt directly to QEMU
			SendDatatoQEMU(szpkt, ipkt, 0);
		}
#ifdef __DEBUG
		printf("\n\n=========== MMU HANDLER TLB Restore START ===============\n");
#endif
		(void) MMU_RestoreTLBEntry();
#ifdef __DEBUG
		printf("\n\n=========== MMU HANDLER TLB Restore END   ===============\n");
#endif
	}
	return NO_ERROR_SRV;
}
/*****************************************************************************
 Function	: HandleMonitorCmd
 Engineer	: Rejeesh S Babu
 Input		: char *szCommand : Command alone(qRcmd,XXXXX)
 Output		: Error code: 0: NO_ERROR
 1: ERROR
 Description: Handle Monitor Commands
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
int HandleMonitorCmd(char* szCommand, char *szInPacket, int irecvpkt_size) {

	int iErr_ret = 0, i = 0, iResult = 0;
	char pucreply[100] = { 0 };
	unsigned char pucErrbuf[100];
	char cMonCommandAscii[100];
	char *pucbuffer;
	char *pucTempbuf;
	if (!GetString(szCommand + 6, cMonCommandAscii, pucErrbuf)) {
		if (SendDatatoGDB(pucErrbuf, strlen(pucErrbuf), 1)) {
			printf("QEMU connection closed\n");
			iErr_ret = ERROR_SRV;
		}
	}
	pucTempbuf = (char*) malloc(strlen(cMonCommandAscii));
	memcpy(pucTempbuf, cMonCommandAscii, strlen(cMonCommandAscii));
	pucbuffer = strtok(pucTempbuf, " ");
	if (!(strcasecmp(pucbuffer, "osmmu"))) {
		//Send ack
		iResult = send(GDBClientSocket, "+", 1, 0);
#ifdef __DEBUG
		printf("\nAck '+' send to GDB for monitor command %s\n", pucbuffer);
#endif
		//TODO
		/*if (iResult != strlen(szInPacket))
		 {
		 printf("send failed: %d\n", WSAGetLastError());
		 closesocket(GDBClientSocket);
		 WSACleanup();
		 return -1;
		 }*/
		if (!Opt_HandleOSDbgCmd(cMonCommandAscii, pucreply + 1))
			gbOsdbg = 1;
		//Send response to GDB
		if (SendDatatoGDB(pucreply, sizeof(pucreply), 1)) {
			printf("QEMU connection closed\n");
			iErr_ret = ERROR_SRV;
		}
	} else if (!(strcasecmp(pucbuffer, "endian"))) {
		//Send ack
		iResult = send(GDBClientSocket, "+", 1, 0);
#ifdef __DEBUG
		printf("\nAck '+' send to GDB\n");
#endif
		//TODO
		/*if (iResult == SOCKET_ERROR)
		 {
		 printf("send failed: %d\n", WSAGetLastError());
		 closesocket(GDBClientSocket);
		 WSACleanup();
		 return -1;
		 }*/
		(void) Opt_HandleEndianCmd(cMonCommandAscii, pucreply + 1);
		//Send response to GDB
		if (SendDatatoGDB(pucreply, sizeof(pucreply), 1)) {
			printf("QEMU connection closed\n");
			iErr_ret = ERROR_SRV;
		}
	} else if (!(strcasecmp(pucbuffer, "osappmmu"))) {
		//Send ACK for the monitor command
		iResult = send(GDBClientSocket, "+", 1, 0);
#ifdef __DEBUG
		printf("\nAck '+' send to GDB for monitor command %s\n", pucbuffer);
#endif
		//TODO
		/*if (iResult == SOCKET_ERROR)
		 {
		 printf("send failed: %d\n", WSAGetLastError());
		 closesocket(GDBClientSocket);
		 WSACleanup();
		 return -1;
		 }*/
		(void) Opt_HandleOSAppCmd(cMonCommandAscii, pucreply + 1);
		//Send response to GDB
		if (SendDatatoGDB(pucreply, sizeof(pucreply), 1)) {
			printf("QEMU connection closed\n");
			iErr_ret = ERROR_SRV;
		}
	} else {
		//This monitor command is meant for QEMU gdbserver,hence send it to QEMU .
		if (SendDatatoQEMU(szInPacket, irecvpkt_size, 0)) {
			printf("GDB connection closed\n");
			iErr_ret = ERROR_SRV;
		}
	}
	return iErr_ret;
}
/*****************************************************************************
 Function	: HandleHaltCmd
 Engineer	: Rejeesh S Babu
 Input		: null
 Output		: Error code: 0: NO_ERROR
 	 	 	 	 	 	  1: ERROR
 Description: Update TLB entries whenever the target is halt
 Date         Initials       Description
21-Jan-2011  RSB            Initial
 *****************************************************************************/
int HandlehaltCmd()
{
	//Post monitor tlbupdate
	MMU_UpdateTLBEntries();
	return 0;
}
/*****************************************************************************
 Function	: GetString
 Engineer	: Rejeesh S Babu
 Input		: 	char *pcInput 			: Hex data(data after qRcmd,)
 char *cMonCommandAscii	: Pointer to store the String format of command
 unsigned char *pcOutput	: Pointer to store the output string
 Output		: Error code: 0: NO_ERROR
 1: ERROR
 Description: Handle Monitor Commands
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
//TODO : CUstomize this function for general use(remove pcOutut)
int GetString(char* pcInput, char *pcStringAscii, unsigned char *pcOutput) {
	unsigned int uiStrLen = strlen(pcInput);
	char szBuffer[100];
	// process command
	for (uiStrLen = 0; uiStrLen < strlen(pcInput); uiStrLen += 2) {
		int iTemp = FromHex(pcInput[uiStrLen]);
		if (iTemp == -1) {
			sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
			strcat(szBuffer, "\n");
			AddStringToOutAsciiz(szBuffer, pcOutput);
			return 0;
		}
		pcStringAscii[uiStrLen / 2] = (char) (iTemp << 4); //lint !e701
		iTemp = FromHex(pcInput[uiStrLen + 1]);
		if (iTemp == -1) {
			sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
			strcat(szBuffer, "\n");
			AddStringToOutAsciiz(szBuffer, pcOutput);
			return 0;
		}
		pcStringAscii[uiStrLen / 2] |= (char) iTemp;
	}

	pcStringAscii[uiStrLen / 2] = '\0'; // terminate command string
	return 1;
}
