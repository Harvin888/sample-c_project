/****************************************************************************8
	 Module: ashgdbstbopt.c
   Engineer: Dileep Kumar K
Description: Ashling GDBServer Stub, command line options
Date	 	Initials	Descriptio
04-Oct-2010	DK		Initial
*****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "gdbstbglob.h"
#include "gdbstb.h"
#include "gdbstbopt.h"
#include "gdbstberr.h"
#include "MMUHandler.h"


#define MAX_ULONG_STRING_SIZE       0x20
#define GDBSERV_PACKET_BUFFER_SIZE  0x10000
extern TyServerInfo tySI;

//command line options (Must be used as global variable
TyOption tyaOption[]=
{
	{
		"qemu-port",
		"<port>",
		"Connect to QEMU with port number <port>",
		"for remote connection to QEMU GDBserver.",
		"Default is port 1234",
		"",
		OptionQemuPort,
		0
	},
	{
		"gdb-server-port",
		"<port>",
		"Listen on <port> for remote connection",
		"from GDB. Default is port 2418",
		"",
		"",
		OptionGDBServerPort,
		0
	}
};


#define MAX_OPTIONS ((int)(sizeof(tyaOption) / sizeof(TyOption)));


int iAshDBStubOptionsNumber = MAX_OPTIONS;

/*****************************************************************************
	 Function: OptionQemuport
	 Engineer: Dileep Kumar K
		Input: unsigned int *puiArg- pointer to index of next arg
			   unsigned int uiArgCount-Count of args in list
			   char **ppszArgs-List of args
	   OutPut: Error code
Date		Initials	Description
04-Oct-2010	DK		Initial
*****************************************************************************/
static int OptionQemuPort(unsigned int *puiArg, unsigned int uiArgCount, 
						  char **ppszArgs)
{
	tySI.bQemuPort = 1;
	return GetPortNumber(puiArg, uiArgCount, ppszArgs);
}

/*****************************************************************************
	 Function: OptionGDBServerPort
	 Engineer: Dileep Kumar K
		Input: unsigned int *puiArg- pointer to index of next arg
			   unsigned int uiArgCount-Count of args in list
			   char **ppszArgs-List of args
	   OutPut: Error code
Date		Initials	Description
04-Oct-2010	DK		Initial
*****************************************************************************/
static int OptionGDBServerPort(unsigned int *puiArg, unsigned int uiArgCount, 
							   char **ppszArgs)
{
	tySI.bGdbSrvPort = 1;
	return GetPortNumber(puiArg, uiArgCount, ppszArgs);
}

/*****************************************************************************
	Function: StrToUlong
	Engineer: Dileep Kumar K
	   Input: char *pszValue	: string value
			  int *pbValid	: Storage for flag if value is valid
	  Output: unsigned long	: Value represented by the string
 Description: Converts string to long .Supportdecimal and hex
			  whne string starts with 0X.
 Date		Initials	Description
 04-Oct-2010	DK		Initial
*****************************************************************************/
unsigned long StrToUlong(char *pszValue, int *pbValid)
{
	unsigned long ulValue;
	char *pszStop = (char *)NULL;
	//Don't allow octal, so skip leading 'o's unless its 0x...
	while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] !='X')
		   && (pszValue[1] != '\0'))
		pszValue++;
	//strtoul supports decimal nnnn and hex 0xnnnn...
	ulValue = strtoul(pszValue, &pszStop, 0);
	//Only valid if all characters were processed ok...
	if ((pszStop) && (pszStop[0] == '\0'))
	{
		if (pbValid)
		{
			*pbValid = 1;
		}
		return ulValue;
	}
	else
	{
		if (pbValid)
		{
			*pbValid = 0;
		}
		return 0;
	}
}

/*****************************************************************************
	Function: OptionGDBServerPort
	Engineer: Dileep Kumar K
	   Input: unsigned int *puiArg	: Pointer to index of next Arg
			  unsigned int uiArgCount	: Count of Arg List
			  char **ppszArgs		: List of Args
	  Output: int	: Error code.
 Description: Gather QEMU GDB server port number
 Date		Initials	Description
 04-Oct-2010	DK		Initial
*****************************************************************************/
static int GetPortNumber(unsigned int *puiArg, unsigned int uiArgCount,
						 char **ppszArgs)
{
	unsigned long ulPort = 0;
	int bValid = 0;
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing Port number after %s Option." , 
				ppszArgs[*puiArg]);
		return ASHGDBSTB_OPT_ERROR;
	}
	else
	{
		//TO Be handled
	}
	(*puiArg)++;
	ulPort = StrToUlong(ppszArgs[*puiArg] , &bValid);
	if (!bValid || (ulPort > 0xFFFF))
	{
		sprintf(tySI.szError, "Invalid Port number %s", ppszArgs[*puiArg]);
		return ASHGDBSTB_OPT_ERROR; 
	}
	else
	{
		//TO do hanlding if any later
	}
	if (!strcmp(((ppszArgs[*puiArg - 1]) + 2), "qemu-port"))
	{
		tySI.usQemuPort = (unsigned short)ulPort;
	}
	else if (!strcmp(((ppszArgs[*puiArg - 1]) + 2), "gdb-server-port"))
	{
		tySI.usGdbSrvPort = (unsigned short)ulPort;
	}
	else
	{
		//To be  handled for future modifications
	}  
	return 0;
}


/*****************************************************************************
	Function: ProcessCmdLineArgs
	Engineer: Dileep Kumar K
	   Input: int argc	: Number of Arguments
			  char *argv[]	: Array of Strings with Arguments
	  Output: int		:Error Code
 Description:	Process all command line arguments
 Date		Initials	Description
 04-Oct-2010	DK		Initial
*****************************************************************************/
int ProcessCmdLineArgs(int argc, char *argv[])
{
	int iError;
	if (argc <= 1)
	{
		iError = 0;
		tySI.bHelp = 1;
		return iError;
	}
	else
	{
		iError = ProcessArgList((unsigned int)(argc - 1), (argv + 1));
		if (iError)
		{
			return iError;
		}
	}       
	return 0;
}


/*****************************************************************************
   Function: ProcessArgList
   Enginerr: Dileep Kumar K
	  Input: unsigned int uiArgCount: Count of args in the list
			 char **ppszargs        : List of args
	 Output: int                    :Error Code
Description: Process a list of arguments
Date            Initials        Description
04-Oct-2010     DK              Initial
*****************************************************************************/
static int ProcessArgList(unsigned int uiArgCount, char **ppszArgs)
{
	int iError, iOption, bValidOption;
	unsigned int uiArg;
	char *pszArg;

	//For each user Arg Specified...
	for (uiArg = 0; uiArg < uiArgCount; uiArg++)
	{
		bValidOption = 0;
		pszArg = ppszArgs[uiArg];
		if ((pszArg[0] == '-') && (pszArg[1] == '-'))
		{ //Compare Arg with the valid options table...
			for (iOption = 0; iOption < iAshDBStubOptionsNumber; iOption++)
			{
				if (strcmp((pszArg + 2), tyaOption[iOption].pszOption) == 0)
				{
					if (tyaOption[iOption].bOptionUsed)
					{
						sprintf(tySI.szError, ASHGDBSSTB_EMSG_CLINE_OPT_RPTED, 
								pszArg);
						return ASHGDBSTB_OPT_ERROR;
					}
					tyaOption[iOption].bOptionUsed = 1;
					if (tyaOption[iOption].pfOptionFunc)
					{
						iError = tyaOption[iOption].pfOptionFunc(&uiArg,uiArgCount,
																 ppszArgs);
						if (iError != ASHGDBSTB_NO_ERROR)
						{
							return iError;
						}
					}
					bValidOption = 1;
					break;
				}
			}
		}
		if (!bValidOption)
		{
			sprintf(tySI.szError, ASHGDBSTB_EMSG_CLINE_INV_OPTION, pszArg);
			return ASHGDBSTB_OPT_ERROR;
		}
	}
	return ASHGDBSTB_NO_ERROR;
}
/*****************************************************************************
   Function: IsHaltCommand
   Enginerr: Rejeesh S Babu
	  Input: char *szCommand: Packet from GDB
	 Output: int                    :1 if HALT command, 0 otherwise
Description: check if packet is a HALT command
Date            Initials        Description
15-Nov-2010     RSB              Initial
*****************************************************************************/
int IsHaltCommand(char *szCommand)
{
	return 0;
	//TO_DO
}
/*****************************************************************************
   Function: OptDecodePacket
   Enginerr: Rejeesh S Babu
	  Input: char *szInPacket: Packet from GDB
		  	 char *szCommand:Parsed command
	 Output: int 1 if error,0 if no error
Description: Parse packet to get command alone(removes checksum)
Date            Initials        Description
15-Nov-2010     RSB              Initial
*****************************************************************************/
int Opt_DecodePacket(char *szInPacket,char *szCommand)
{
	int iPacketIdentifier = 0;
	int iChksumFound,iBuflen=0,i,j = 0;
    //Remove $
	for(i=0;i<strlen(szInPacket);i++)
	{
		if('$'== szInPacket[i])
		{
			iPacketIdentifier=1;
			break;
		}
	}
	if(!iPacketIdentifier)
	{
		//Do nothing
		return 0;
	}
	//Capture data upto '#'
	for(j=i+1;j<strlen(szInPacket);j++)
	{
		if('#' == szInPacket[j])
		{
			iChksumFound = 1;
			break;
		}
		szCommand[iBuflen++] = szInPacket[j];
	}
	if(!iChksumFound)
	{
		//Do nothing
		return 0;
	}
	szCommand[iBuflen] = '\0';
	return 0;

}
/*****************************************************************************
   Function: Opt_AnalysePktTpe
   Enginerr: Rejeesh S Babu
	  Input: char *szCommand:Parsed command
	 Output: int 1 if error,0 if no error
Description: Parse packet to get command alone(removes checksum)
Date            Initials        Description
15-Nov-2010     RSB              Initial
*****************************************************************************/
TyPacketType Opt_AnalysePkt(char *szInPacket,char *szCommand)
{
	int iPacketIdentified = 0;
	int iChksumFound,iBuflen=0,i,j = 0;
	TyPacketType tyPktTpe = OTHERS;
	while(*szInPacket != '\0')
	{
		if(*szInPacket == '$')
		{
			szInPacket++;
			iPacketIdentified = 1;
		}
		if(iPacketIdentified)
		{
			//Memory read/write command
			if(*szInPacket == 'm' || *szInPacket == 'M' || *szInPacket == 'X' || *szInPacket == 'z' || *szInPacket == 'Z')
			{
				tyPktTpe = MEMACCESS;
				if(*szInPacket == 'm')
				{
					MMU_WriteAccess(0);
				}
				else
				{
					MMU_WriteAccess(1);
				}
				while(*szInPacket != '\0')
				{
                     if(*szInPacket == '#')
                         break;
                    *szCommand++ = *szInPacket++;
				}
				*szCommand = '\0';
				break;
			}else if(*szInPacket == 'g')
			{
				tyPktTpe = HALT;
				break;
			}
			else
			{
				char *szbuffer;
				char *szTempPkt = (char *)malloc(strlen(szInPacket));
				memcpy(szTempPkt,szInPacket,strlen(szInPacket));
				szbuffer = strtok(szTempPkt,",");

				if(!strcasecmp(szbuffer,"qRcmd"))
				{
					tyPktTpe = MONITOR;
					while(*szInPacket != '#')
					{
						*szCommand++ = *szInPacket++;
					}
					*szCommand = '\0';
					break;
				}
				else
				{
					tyPktTpe = OTHERS;
					break;
				}
			}
		}
		else
		{
			szInPacket++;
		}

	}
	return tyPktTpe;

}
/*****************************************************************************
   Function: OptDecodeVirtualAddr
   Enginerr: Rejeesh S Babu
	  Input: char *szCommand: Packet from GDB
	 Output: long                    :returns virtual address 
Description: check if packet contains memory access commands
Date            Initials        Description
15-Nov-2010     RSB              Initial
*****************************************************************************/
unsigned int Opt_DecodeVirtualAddr(char *szCommand)
{
   int iIndex;
   char *szTempAddr;
   char szAddress[MAX_ULONG_STRING_SIZE];
   //assert(szCommand != NULL);
   switch(szCommand[0])
	{
   case 'M':
   case 'm':
   case 'X':
	      szTempAddr =&szCommand[1];
		   for (iIndex=0; iIndex < MAX_ULONG_STRING_SIZE; iIndex++)
			  {
			  szAddress[iIndex] = *szTempAddr++;
		
			  if (szAddress[iIndex] == ',')
				 {
				 szAddress[iIndex] = '\0';
				 break;
				 }
			  }
		   szAddress[MAX_ULONG_STRING_SIZE-1] = '\0';
		   return(StringToUlong(szAddress));
   case 'Z':
   case 'z':
	   //skip Z0,
	   szTempAddr =&szCommand[3];
	  for (iIndex = 0; iIndex < MAX_ULONG_STRING_SIZE; iIndex++)
      {
		  if (szTempAddr[iIndex] == ',')
			 {
			  //skip TYPE
			  szAddress[iIndex] = '\0';
			 break;
			 }
		  szAddress[iIndex] = szTempAddr[iIndex];
      }
	  //TODO: check if strtoul will work here
   return(StringToUlong(szAddress));
	default:
		break;
	}
   return -1;
}
/****************************************************************************
     Function: StringToUlong
     Engineer: Rejeesh S  Babu
        Input: char *pszString          : input string
       Output: long value
  Description: Converts a string to an unsigned long
Date           Initials    Description
06-Dec-2010    RSB          Initial
*****************************************************************************/
unsigned long StringToUlong(char *pszString)
{
   //assert(pszString != NULL);
   unsigned long ulValue = 0;
   while (*pszString != '\0')
      {
      ulValue <<= 4;
      ulValue |= FromHex(*pszString++) & 0xF;
      }
   return ulValue;
}
/****************************************************************************
     Function: FromHex
     Engineer: Rejeesh S  Babu
        Input: iChar : hex character
       Output: int   : value represented by the hex char
  Description: Convert character to hex value.
Date           Initials    Description
06-Dec-2010   RSB          Initial
*****************************************************************************/
int FromHex(int iChar)
{
   if (iChar >= '0' && iChar <= '9')
      return iChar - '0';
   else if (iChar >= 'A' && iChar <= 'F')
      return iChar - 'A' + 10;
   else if (iChar >= 'a' && iChar <= 'f')
      return iChar - 'a' + 10;
   else
      return -1;
}
/****************************************************************************
     Function: ML_SwapEndianWord
     Engineer: Vitezslav Hola
        Input: unsigned long ulWord - word value
       Output: unsigned long - word value with swapped bytes
  Description: Swap bytes in words to convert between LE and BE.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
unsigned long Opt_SwapEndianWord(unsigned long ulWord)
{
   unsigned long ulLocValue = (ulWord >> 24) | (ulWord << 24);
   ulLocValue |= ((ulWord >> 8) & 0x0000FF00);
   ulLocValue |= ((ulWord << 8) & 0x00FF0000);
   return ulLocValue;
}
/****************************************************************************
     Function: Opt_HandleOSDbgCmd
     Engineer: Rejeesh S Babu
        Input: char *pcCommand - command string
               char *pcReply -  monitor output command string
       Output: int - 0 Osdbg Enabled successfully
  Description: handle "osmmu" monitor command
Date           Initials    Description
09-DEC-2010     RS          Initial
*****************************************************************************/
int Opt_HandleOSDbgCmd(char *pcCommand,char *pcReply)
{
   char szBuffer[1000];
   char cOSMMUAddress[1000]= "0";
   char cTempReply[1000];
   int iTokenIndex=0;
   char *pszToken=0;
   char *pcEnd = NULL;
   int iRetVal=0;
   unsigned long ulOSMMU;
  // assert(pcCommand);
   //parse the command line arguments
   pszToken = strtok(pcCommand," ");
   //To skip "osmmu"
   pszToken = strtok( 0, " " );
   while(pszToken != 0)
      {
      //get type of OS 
      if(0 == iTokenIndex)
         {
         if( strcasecmp(pszToken, "Linux26"))
		 {
            sprintf(cTempReply, "Unsupported <OS name> %s\n" ,pszToken);
            iRetVal = 1;
            goto ErrorExit;
         }
		 else
		 {
			 //Do nothing
		 }
	  }
      //Get OS MMU Address
      else if( 1 == iTokenIndex)
         strcpy(cOSMMUAddress, pszToken);
      else
         {
         iTokenIndex++;
         break;
         }
      iTokenIndex++;
      pszToken = strtok( 0, " " );
      }
   if(0 == iTokenIndex)
      {
      sprintf(cTempReply,"Missing arguments <OS Name> <addr>.\n");
      iRetVal = 1;
      goto ErrorExit;

      }
   if(1 == iTokenIndex)
      {
      sprintf(cTempReply,"Missing argument <addr>.\n");
      iRetVal = 1;
      goto ErrorExit;
      }
   if(2 < iTokenIndex)
      {
      sprintf(cTempReply,"Invalid argument after <addr>.\n");
      iRetVal = 1;
      goto ErrorExit;
      }
   //This converts string to unsigned long
   ulOSMMU=strtoul(cOSMMUAddress,&pcEnd ,16);
   if(ulOSMMU == 0)
      {
      //PrintMessage(ERROR_MESSAGE, "strtoul function failed\n" );
	  sprintf(cTempReply,"Invalid argument after <addr>.\n");
      iRetVal = 1;
      goto ErrorExit;
      }
   iRetVal=MMU_EnableOSMMU(ulOSMMU);
   if( iRetVal==0)
      {
      sprintf(cTempReply,"OK.\n");
      }
   else
      {
      sprintf(cTempReply,"Unable to set OSMMU.\n");
      goto ErrorExit;
      }

      ErrorExit:
	   AddStringToOutAsciiz(cTempReply, pcReply);
	   return iRetVal;
}
/****************************************************************************
Function	: Opt_HandleOSAppCmd
Engineer	: Rejeesh S Babu
Input		: char *pcCommand 	- command string
       	   	  char *pcReply 	- monitor output command string
Output		: void
Description	: handle "osappmmu" monitor command
Date           Initials    Description
07-JAN-2010     RS          Initial
*****************************************************************************/
void Opt_HandleOSAppCmd(char *pcInput, char *pcReply)
{
	   int iTokenIndex = 0;
	   int iRetVal = 0;
	   unsigned int ulAppPgd = 0;
	   unsigned int ulAppContext = 0;
	   char *pszToken;
	   char *pcEnd = NULL;
	   char szBuffer[100];
	   char szOSAppContext[50];
	   char szAppMMUAddress[50];

	   pszToken=strtok(pcInput," ");
	   //To skip monitor command
	   pszToken=strtok(0," ");
	   while(pszToken != 0)
	      {
	      //Get OS APP context Address
	      if( 0 == iTokenIndex)
	         strcpy(szOSAppContext, pszToken);
	      else if( 1 == iTokenIndex)
	         strcpy(szAppMMUAddress, pszToken);
	      else
	         {
	         iTokenIndex++;
	         break;
	         }
	      iTokenIndex++;
	      pszToken = strtok( 0, " " );
	      }
	   if(0 == iTokenIndex)
	      {
	      sprintf(szBuffer,"Missing arguments <OS Name> <context> <addr>.\n");
	      goto ErrorExit;
	      }
	   if(1 == iTokenIndex)
	      {
	      sprintf(szBuffer,"Missing arguments <context> <addr>.\n");
	      goto ErrorExit;
	      }
	   if(2 < iTokenIndex)
	      {
	      sprintf(szBuffer,"Invalid argument after <addr>.\n");
	      goto ErrorExit;
	      }
	   //This converts string to unsigned long
	   ulAppPgd=strtoul(szAppMMUAddress,&pcEnd ,16);
	   ulAppContext=strtoul(szOSAppContext,&pcEnd ,16);

	   iRetVal=MMU_SetOSAppMMU(ulAppContext, ulAppPgd);
	   if( iRetVal==0)
	      {
	      sprintf(szBuffer,"OK.\n");
	      }
	   else
	      {
	      sprintf(szBuffer,"Unable to set OS App MMU.\n");
	      }
	   ErrorExit:
	   AddStringToOutAsciiz(szBuffer, pcReply);
}
/****************************************************************************
     Function: Opt_HandleEndianCmd
     Engineer: Rejeesh S Babu
        Input: char *pcCommand - command string
               char *pcReply -  monitor output command string
       Output: int - 0 Endianness set successfully
  Description: Handle Endianness monitor comand
Date           Initials    Description
20-DEC-2010     RS          Initial
*****************************************************************************/
int Opt_HandleEndianCmd(char *pcCommand,char* pcReply)
{
	int iErr_ret = 0;
	char cTempReply[100];
	char *pcTemp = strtok(pcCommand," ");
	pcTemp = strtok(NULL," ");
	if(!strcasecmp(pcTemp,"little"))
	{
	  MMU_SetEndian(0);
	  sprintf(cTempReply,"OK.\n");
      AddStringToOutAsciiz(cTempReply, pcReply);
	  iErr_ret = 0;
	}
	else if(!strcasecmp(pcTemp,"big"))
	{
		MMU_SetEndian(1);
	  sprintf(cTempReply,"OK.\n");
      AddStringToOutAsciiz(cTempReply, pcReply);
	  iErr_ret = 0;
	}
	else
	{
	  sprintf(cTempReply,"Unable to set Endianness\n");
      AddStringToOutAsciiz(cTempReply, pcReply);
	  iErr_ret = 1;
	}
	return iErr_ret ;

}
/****************************************************************************
     Function: AddStringToOutAsciiz
     Engineer: Nikolay Chokoev
        Input: *szBuffer  : input buffer
               *pcOutput  : output bufffer
       Output: none
  Description: Convert input string to ASCIIz and paste to the end of the
               output string. I assumpts that the output string is null 
               terminated string.
Date           Initials    Description
11-Feb-2008    NCH         Initial
10-Nov-2009    SVL         Fixed a bug - 'Remote Packet too long'
15-Dec-2009	   RS		   Output packet size increased to 0x10000
****************************************************************************/
void AddStringToOutAsciiz(const char *szBuffer, char *pcOutput)
{
   char *pcTempOut;
   unsigned int uiSize=0;
   unsigned int uiOutSize;

   pcTempOut=pcOutput;
   uiOutSize=strlen(pcOutput);
   pcTempOut+=uiOutSize;
   while ((szBuffer[uiSize] != '\0') && 
          (uiOutSize < GDBSERV_PACKET_BUFFER_SIZE-1) &&
          /* NOTE: The macro '_MAX_PATH'(260) was replaced with 
          'GDBSERV_PACKET_BUFFER_SIZE'(0x10000) for fixing the bug 
          'Remote Packet too long' in the case of 'monitor getpregs'. Even though
          the GDBSERVER Packet can hold 65536(0x10000) bytes, this loop will
          terminate when 'uiSize' reaches '_MAX_PATH'. */
          /* (uiSize < _MAX_PATH-1) */
          (uiSize < GDBSERV_PACKET_BUFFER_SIZE-1))
      {
      *(pcTempOut)++ = (char)ToHex(((szBuffer[uiSize]) & 0xF0)>>4); 
      *(pcTempOut)++ = (char)ToHex((szBuffer[uiSize]) & 0x0F); 
      uiSize++;
      uiOutSize++;
      }
   *(pcTempOut) = '\0';
}
/****************************************************************************
     Function: ToHex
     Engineer: Vitezslav Hola
        Input: iNibble : 4 bit value
       Output: int     : hex character representing the iNibble
  Description: Convert value to hex
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int ToHex(int iNibble)
{
   iNibble &= 0xF;
   if (iNibble < 10)
      return iNibble + '0';
   else
      return iNibble + 'a' - 10;
}
