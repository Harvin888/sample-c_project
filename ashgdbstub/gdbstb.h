#ifndef GDBSTUB
#define GDBSTUB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#ifndef __LINUX
#include <conio.h>
#ifndef strcasecmp
#define strcasecmp   _stricmp
#endif
#include <assert.h>
#include <windows.h>
#include <winsock2.h>
#include <conio.h>
#include <winerror.h>
typedef int socklen_t;
#define O_NONBLOCK FIONBIO
#define fcntl(fd, b, c) ioctlsocket(fd, c, &arg)
#define GetSocketError() WSAGetLastError()
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#define GetSocketError() errno
typedef int SOCKET;
#endif

#define _MAX_PATH_	260

#include "gdbstbty.h"
#include "gdbstberr.h"

void 	*TransferQEMUtoGDB	(void *param);
void 	*TransferGDBtoQEMU	(void *param);
int  	WaitForGDBConnection(void);
int  	ConnectToQemuServer	(void);
int  	SendDatatoQEMU		(char *szInPacket, int recvpkt_size,int bDontSendtoGDB);
int  	SendDatatoGDB 		(char *szInPacket, int recvpkt_size,int bPreparePkt);
int 	HandleMemAccessCmd	(char *szCommand,char* szInPacket,int recvpkt_size);
int 	HandleMonitorCmd	(char* szCommand,char *szInPacket,int irecvpkt_size);
void 	MsSleep				(int uiMiliseconds);
inline int fromhexqemu		(int v);
static void hextomem		(unsigned char *mem, const char *buf, int len);
#define ASHGDBSRV_SOCKET_BUFFER_SIZE   0x1000
#define MESSAGE_ERR_MONCMD_RECOGNITION_FAILED     "GDB command recognition failure."

typedef	enum
{	
	_MIPS	            = 0x00,			//MIPS Processor
	INVALID_PROC_FAMILY = 0x01
}TyProcessorFamilty;
typedef enum{
	MEMACCESS,MONITOR,OTHERS,HALT
}TyPacketType;
typedef struct _TyServerInfo
{
	int bHelp;
	int bVersion;
	int bQemuPort;
	int bGdbSrvPort;
	int QemuClientFd;
	int AshGdbStubSrvFd;
	unsigned short usQemuPort;
	unsigned short usGdbSrvPort;
	char szError[_MAX_PATH_];
}TyServerInfo;

typedef struct sockaddr TySockAddr;
typedef struct sockaddr_in TySockAddrIn;

#endif
