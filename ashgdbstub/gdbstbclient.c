#include "gdbstb.h"
#ifndef __LINUX
#include <windows.h>
#pragma comment(lib, "wininet.lib")
#endif

extern TyServerInfo tySI; //Global structure with server info
SOCKET QEMUServerSocket;// = INVALID_SOCKET;
static int gbDntSndtoGdb = 0;
/*****************************************************************************
 Function: ConnectToQemuServer
 Engineer: Dileep Kumar K
 Input: void
 Output: int        :Error Code
 Description: Connect ash gdbserver stub to QEMU Gdbserver
 Date         Initials         Description
 05-Oct-2010  DK               Initial
 *****************************************************************************/
int ConnectToQemuServer(void) {
	int iError;
#ifndef __LINUX
	//Initializing Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		printf("Error at WSAStartup()\n");
		return iResult;
	}
#endif
	//Create a SOCKET for connecting to QEMU gdbserver
	if ((QEMUServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		//QEMUServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		//if (QEMUServerSocket == INVALID_SOCKET) {
		/*printf("Error at socket(): %d\n", WSAGetLastError());
		 WSACleanup();*/
		return -1;
	}

	// The sockaddr_in structure specifies the address family,
	// IP address, and port of the server to be connected to.
	TySockAddrIn clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr("127.0.0.1");
	clientService.sin_port = htons(tySI.usQemuPort);

	// Connect to QEMU gdbserver.
	if (connect(QEMUServerSocket, (TySockAddr*) &clientService,
			sizeof(clientService)) < 0) {
		printf("Failed to connect.\n");
		//WSACleanup();
		return -1;
	}

	printf("\nConnected to server.\n");
	//WSACleanup();
	return 0;
}

/*****************************************************************************
 Function: TransferQEMUtoGDB
 Engineer: Dileep Kumar K
 Input: Param
 Output: NULL pointer
 Description: Thread waiting for read from QEMU and write to GDB
 Date         Initials       Description
 08-Oct-2010  DK             Initial
 *****************************************************************************/
void *TransferQEMUtoGDB(void *param) {
	char szRcvPckt[ASHGDBSRV_SOCKET_BUFFER_SIZE];
	int recvpkt_size = 0;
	int iokrecd = 0;
	while (1) {
		//Receive data from QEMU
		#ifdef __DEBUG
		printf("Waiting for receiving 1st reply from QEMU\n ");
		#endif
		recvpkt_size = recv(QEMUServerSocket, szRcvPckt, sizeof(szRcvPckt), 0);
		szRcvPckt[recvpkt_size] = '\0';
		#ifdef __DEBUG
		printf("Pkt Rcvd frm QEMU:%s ", szRcvPckt);
		#endif
		if (recvpkt_size == 0) {
			printf("QEMU connection closed\n");
			break;
		}
		if (!gbDntSndtoGdb) {
			//Send data to GDB
			if (SendDatatoGDB(szRcvPckt, recvpkt_size, 0)) {
				printf("QEMU connection closed\n");
				break;
			}
		} else {
			//received data was ACK only,so accept the next data
			if ((szRcvPckt[0] == '+') && (szRcvPckt[1] != '$'))
			{
				#ifdef __DEBUG
				printf("Waiting again as QEMU send '+' only \n ");
				#endif
				recvpkt_size = recv(QEMUServerSocket, szRcvPckt,
						sizeof(szRcvPckt), 0);
				#ifdef __DEBUG
				printf("\nPkt Rcvd from QEMU:%s ", szRcvPckt);
				#endif
				send(QEMUServerSocket, "+", 1, 0);
				#ifdef __DEBUG
				printf("ACK + send to qemu\n");
				#endif
			}else
			{
				//pkts of type +$XXX...#XX
				send(QEMUServerSocket, "+", 1, 0);
				#ifdef __DEBUG
				printf("ACK + send to qemu\n");
				#endif
			}
			//Keep data in a buffer,required internally
			szRcvPckt[recvpkt_size] = '\0';
			MMU_StoreGDBReply(szRcvPckt, recvpkt_size);
			#ifdef __DEBUG
			printf("Pkt internally treated :%s\n", szRcvPckt);
			#endif
			//gbDntSndtoGdb = 2 indicates that previously send packet was
			// an internal monitor command(readtlb or srchtlb)
			if (2 == gbDntSndtoGdb) {

				//Most of the time qemu send 2 pkt together;like $O31360d0a#41$OK#9a.This is handled in above else case.
				//If this is not the case($OK#XX send separately),we need to handle it
				//So we count total no: of occ. of '$'to to see if OK was already received
				char *ch = strchr(szRcvPckt, '$');
				iokrecd = 0;
				while (ch != NULL) {
					ch = strchr(ch + 1, '$');
					if (ch != NULL) {
						iokrecd = 1;
						break;
					}
				}
				if(!iokrecd)
				{
					#ifdef __DEBUG
					printf("Waiting again as we know QEMU will send 'OK' now \n ");
					#endif
					recvpkt_size = recv(QEMUServerSocket, szRcvPckt,sizeof(szRcvPckt), 0);
					szRcvPckt[recvpkt_size] = '\0';
					#ifdef __DEBUG
					printf("Internally blocked pkt:%s\n", szRcvPckt);
					#endif
					send(QEMUServerSocket, "+", 1, 0);
					#ifdef __DEBUG
					printf("ACK + send to qemu\n");
					#endif
				}

			}
			gbDntSndtoGdb = 0;
		}
		memset(szRcvPckt, 0, sizeof(szRcvPckt));
	}
	return NULL;
}
/*****************************************************************************
 Function	: SendDatatoQEMU
 Engineer	: Rejeesh S Babu
 Input		: 	char *szInPacket 	: 	Packet to send to QEMU
				int size 			: 	Size of Packet to send
				int bDontSendtoGDB	: 	0:Send the Response to GDB
										1:Send the response to buffer
										2:Pkt send was a monitor command,so additional handling reqd.
 Output		: Error code: 	0: NO_ERROR
							-1: ERROR
 Description: Send data to QEMU from GDB
 Date         Initials       Description
 15-Nov-2010  RSB            Initial
 *****************************************************************************/
int SendDatatoQEMU(char *szInPacket, int size, int bDntSndtoGDB) {
	int iResult = 0, iStrLen = 0, iIndex = 1;
	unsigned char ucCheckSum = 0;
	gbDntSndtoGdb = bDntSndtoGDB;
	if (gbDntSndtoGdb) {
		//Add checksum and $ to the packet
		szInPacket[0] = '$'; //lint !e527
		iStrLen = (int) strlen(szInPacket);
		// calculate checksum...
		for (iIndex = 1; iIndex < iStrLen; iIndex++) {
			ucCheckSum += (unsigned char) szInPacket[iIndex];
		}

		// and the next four bytes are reserved for us...
		szInPacket[iIndex++] = '#';
		szInPacket[iIndex++] = (char) ToHex(ucCheckSum >> 4);
		szInPacket[iIndex++] = (char) ToHex(ucCheckSum);
		szInPacket[iIndex] = '\0';
		size = strlen(szInPacket);
#ifdef __DEBUG
		printf("Internally generated Pkt :%s\n", szInPacket);
#endif
	}
	iResult = send(QEMUServerSocket, szInPacket, size, 0);
#ifdef __DEBUG
	printf("Pkt snd to QEMU :%s\n", szInPacket);
#endif
	if (iResult != strlen(szInPacket)) {
		/*printf("send failed: %d\n", WSAGetLastError());
		 closesocket(QEMUServerSocket);
		 WSACleanup();*/
		return -1;
	}
	return 0;

}
/****************************************************************************
 Function	: WaitForBufUpdtn
 Engineer	: Rejeesh S babu
 Input		: void
 Output		: none
 Description: Wait for recieving data from QEMU
 Date           Initials    Description
 15-Nov-2010  	  RSB         Initial
 *****************************************************************************/
void WaitForBufUpdtn(void) {
	while (gbDntSndtoGdb) {
#ifdef __DEBUG
		printf("Waiting for recieving data from QEMU\n");
#endif
		MsSleep(1);
	}
}
/****************************************************************************
 Function	: MsSleep
 Engineer	:  Rejeesh S babu
 Input		: unsigned int uiMiliseconds : time to wait
 Output		: none
 Description: Implementation of sleep function to wait given number of miliseconds.
 Date           Initials    Description
 15-Nov-2010  	  RSB         Initial
 *****************************************************************************/
void MsSleep(int uiMiliseconds) {
#ifndef __LINUX
	Sleep(uiMiliseconds);
#else
	usleep(uiMiliseconds * 1000);
#endif
}
