#ifndef _MMUHANDLER
#define _MMUHANDLER

#define CONFIG_PAGE_SIZE_4KB

#ifdef CONFIG_PAGE_SIZE_4KB
#define PAGE_SHIFT	12
#endif

#ifdef CONFIG_PAGE_SIZE_8KB
#define PAGE_SHIFT	13
#endif

#ifdef CONFIG_PAGE_SIZE_16KB
#define PAGE_SHIFT	14
#endif

#ifdef CONFIG_PAGE_SIZE_64KB
#define PAGE_SHIFT	16
#endif

#define PAGE_SIZE	(1UL << PAGE_SHIFT)

#define PGD_T_LOG2 2 //logic is (__builtin_ffs(sizeof(pgd_t)) - 1)
#define PTE_T_LOG2 2 //logic is (__builtin_ffs(sizeof(pte_t)) - 1) 
                     //sizeof(pte_t) is 4. __builtin_ffs = (1's Index position from LSB) + 1

//#define __PGD_ORDER	(32 - 3 * PAGE_SHIFT + PGD_T_LOG2 + PTE_T_LOG2)
//#define PGD_ORDER	(__PGD_ORDER >= 0 ? __PGD_ORDER : 0)

//#define PTE_ORDER	0


#define PTRS_PER_PGD	((PAGE_SIZE /*<< PGD_ORDER*/) / 4 /*sizeof(pgd_t)*/ )
#define PTRS_PER_PTE	((PAGE_SIZE /*<< PTE_ORDER*/) / 4 /*sizeof(pte_t)*/ )

#define PGDIR_SHIFT	(2 * PAGE_SHIFT /*+ PTE_ORDER*/ - PTE_T_LOG2)
#define PGDIR_SIZE	(1UL << PGDIR_SHIFT)

#define pgd_index(address)	(((address) >> PGDIR_SHIFT) & (PTRS_PER_PGD-1))  //0x3FF
/* Find an entry in the third-level page table.. */
#define __pte_offset(address)						\
  	(((address) >> PAGE_SHIFT) & (PTRS_PER_PTE - 1))
#define MMUSizeBitMask              0x7E000000
#define MMUSizeBitShift             25

typedef struct TyTLBQemuEntry {
    unsigned int VPN;
    unsigned int PageMask;
    unsigned char ASID;
    unsigned int G:1;
    unsigned int C0:3;
    unsigned int C1:3;
    unsigned int V0:1;
    unsigned int V1:1;
    unsigned int D0:1;
    unsigned int D1:1;
    unsigned int PFN[2];
}TyTLBQemuEntry;
/* EntryLo Register format */
typedef union __TyEntryLo
{
   unsigned int Data;
   struct
   {
      unsigned int G    :1;
      unsigned int V    :1;
      unsigned int D    :1;
      unsigned int C    :3;
      unsigned int PFN  :20;
      unsigned int Zero :4;
      unsigned int RI   :2;
   }Bits;
} TyEntryLo;


int		MMU_IsMappingRequired(unsigned int lVirtualAddr);
int 	MMU_HandleOsMMUMapping(unsigned int lVirtualAddr,int *piRestoretlb);
int 	MMU_WriteTLBEntry(unsigned int ulIndex,TyTLBQemuEntry *ptyTLBEntry);
int 	MMU_ReadTLBEntry(unsigned int ulIndex, TyTLBQemuEntry *ptyTLBEntry);
int 	MMU_RestoreTLBEntry(void);
int 	MMU_ReadWord(unsigned int ulPgdAddr, unsigned int *pulPteBaseAddr);
void 	MMU_WriteAccess(int iWriteAccess );
int 	MMU_EnableOSMMU(unsigned int ulPgDirAddrs);
int 	MMU_SetOSAppMMU(unsigned int AppContext, unsigned int AppPgd);
void 	MMU_SetEndian(int ibBigEndian);

#endif
