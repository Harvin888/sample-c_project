/****************************************************************************
      Module: ashgdbstbopt.h
    Engineer: Dileep Kumar K
 Description: Ashling GDB Server Stub, command line options header
 Date		Initials	Description
 04-Oct-2010	DK		Initial
 ***************************************************************************/

#ifndef ASHGDBSTBOPT_H_
#define ASHGDBSTBOPT_H_

typedef int (TyOptionFunc) (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);

typedef struct _TyOption
{
	const char *pszOption;
	const char *pszHelpArg;
	const char *pszHelpLine1;
	const char *pszHelpLine2;
	const char *pszHelpLine3;
	const char *pszHelpLine4;
	TyOptionFunc *pfOptionFunc;
	int bOptionUsed;
}TyOption;

static int  OptionHelp(unsigned int *puiArg, unsigned int uiArgCount, 
					   char **ppszArgs);
static int OptionVersion(unsigned int *puiArg, unsigned int uiArgCount, 
						 char **ppszArgs);
static int OptionQemuPort(unsigned int *puiArg, unsigned int uiArgCount,
						  char **ppszArgs);
static int OptionGDBServerPort(unsigned int *puiArg, unsigned int uiArgCount,
							   char **ppszArgs);
static int GetPortNumber(unsigned int *puiArg, unsigned int uiArgCount,
						 char **ppszArgs);
static int ProcessArgList(unsigned int uiArgCount, char **ppszArgs);
unsigned long StrToUlong(char *pszValue, int *pbValid);
void AddStringToOutAsciiz(const char *szBuffer, char *pcOutput);
int ToHex(int iNibble);
int Opt_HandleOSDbgCmd(char *pcCommand,char *pcReply);
int ProcessCmdLineArgs(int arc, char *argv[]);
int IsMemoryAccessCommand(char *szInPacket);
int IsHaltCommand(char *szInPacket);
unsigned long OptDecodeVirtualAddr(char *szInPacket);
unsigned long StringToUlong(char *pszString);
unsigned long Opt_SwapEndianWord(unsigned long ulWord);
TyPacketType Opt_AnalysePkt(char *szInPacket,char *szCommand);
static void hextomem(unsigned char *mem, const char *buf, int len);
#endif
