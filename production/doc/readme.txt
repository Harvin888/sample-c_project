Release notes for the Ashling Opella-XD Production Test Software
v1.0.1, 23-Jul-2012 (c) Ashling Microsystems Limited.

To install the software copy all files into destination directory 
and run "INSTALL.BAT"

Features of this release include:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. Opella-XD firmware v1.0.0
2. OPXDDIAG utility v1.0.3-A
3. Opella-XD Production Test Board FPGA v0.1.1
4. TPAOP-MIPS14 and ADOP-EJTAG20 support for production
5. TPAOP-ARM20 and ADOP-RPINE10 support for production
6. TPAOP-ARC20 and ADOP-ARC15 support for production
7. Added option for continuous test
8. Show TPA serial number
9. TPAOP-ARC20 R1 support added (Maximum test TCK is 70MHz)


