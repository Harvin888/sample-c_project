@echo off
:: *************************************************************************
::        Module: opellaxd.bat
::      Engineer: Vitezslav Hola
::   Description: Production script file for Opella-XD mainboard
::      Comments: 
:: Date           Initials    Description
:: 18-Aug-2007    VH          Initial
:: 23-Aug-2007    VH          Added TPAOP-MIPS14 production
:: 23-Nov-2007    VH          Added TPAOP-ARC20, TPAOP-ARM20 and adaptor boards
:: 23-Jul-2012    SPT         Added TPAOP-ARC20 R1 test support
::                            (mostly changes in opxddiag and PTB FPGAware)
:: 26-May-2016    AS          Added support for Opella-XD R3
::                            (the EEPROM is located on the board and needs to
::                             be programmed before the Tests)
:: *************************************************************************

:startBatch
cls
:: save path
set TempPath=%path%
set TempRootDir=%CD%
set MYSLEEP=%CD%\sleep.bat
set OPXDCONTTEST=0
set OPXDCONT_ITERATION=0
set OPXDCONT_ERROR=0
:: default settings

echo !!! IMPORTENT !!! !!! IMPORTENT !!! !!! IMPORTENT !!!
echo.
echo If you are testing an Opella-XD R3 board then the EEPROM needs to
echo be programmed as diag. (use test option 3)
echo.
echo !!! IMPORTENT !!! !!! IMPORTENT !!! !!! IMPORTENT !!!
pause
cls

:: ================================================
:: == Main menu ===================================
:: ================================================
:selectMMOption
cd %TempRootDir%
echo.
echo    Opella-XD Production Test Procedure
echo    v1.0.3, 26-May-2016 (c) Ashling Microsystems Ltd.
echo.
echo    Main menu
echo    ----------------------------------------------------
echo      1      Opella-XD Initial Flash Programming
echo      2      Opella-XD Production Diagnostic Tests
echo      3      TPA for Opella-XD Initial Programming
echo      4      TPA for Opella-XD Production Diagnostic Tests
echo      5      TPA Adaptor Board Production Diagnostic Tests
echo.
echo      8      Update Opella-XD Firmware Version
if %OPXDCONTTEST% == 0  echo      9      Select Continuous Test (currently using single)
if NOT %OPXDCONTTEST% == 0  echo      9      Select Single Test (currently using continuous)
echo    ----------------------------------------------------
echo      0      Quit
echo.
set /p MMOption=" Please select an option from main menu :> "

:: verify MMOption
if .%MMOption%. == ..  goto badMMOption
if /i %MMOption% == 0  goto exit
if /i %MMOption% == 1  goto doFlashProg
if /i %MMOption% == 2  goto doProdTests
if /i %MMOption% == 3  goto doTpaProg
if /i %MMOption% == 4  goto doTpaTests
if /i %MMOption% == 5  goto doTpaAdTests
if /i %MMOption% == 8  goto doFWUpdate
if /i %MMOption% == 9  goto SelContSingle
goto badMMOption

:badMMOption
echo Invalid option selected: %MMOption%
goto selectMMOption

:SelContSingle
if %OPXDCONTTEST% == 0 goto SelContSingle2
set OPXDCONTTEST=0
cls
goto selectMMOption
:SelContSingle2
set OPXDCONTTEST=1
cls
goto selectMMOption

:: ================================================
:: == Opella-XD Initial Flash Programming =========
:: ================================================
:doFlashProg
cls
echo.
echo Opella-XD Initial Flash Programming
echo Step 1 - Generating Flash Image
echo.
echo Please type Opella-XD serial number corresponding with the label.
set /p OpXDSerNum="Opella-XD serial number :> "
echo.
echo Please enter Opella-XD revision number corresponding with the label.
set /p OpXDRevision="Opella-XD revision, Enter for R3-M0-E0 :> "
echo.
cd bin
:: Remove previous image
del /Q opxdmanf.bin
:: Use Opella-XD diagnostic utility to generate Flash image
if .%OpXDRevision%.== .. goto useLatestRevision
call opxddiag.exe --genbin %OpXDSerNum% %OpXDRevision%
cd %TempRootDir%
goto doFlashProg1
:useLatestRevision
:: Using latest board revision number, edit following line when updated
call opxddiag.exe --genbin %OpXDSerNum% R3-M0-E0
IF NOT %ERRORLEVEL%==0 goto progFailed

if .%OpXDRevision%.==R3-M0-E0 goto programMessage

cd %TempRootDir%
goto doFlashProg1

:doFlashProg1
call %MYSLEEP% 10
pause

:doFlashProg2
cls
echo.
echo Opella-XD Initial Flash Programming
echo Step 2 - Preparation for Flash Programming
echo.
echo Please follow next steps carefully.
echo 1) Both Opella-XD and Production Test Board are not powered now.
echo 2) Connect Opella-XD with Production Test Board via 24-way cable.
echo    Cable connects J2 connector on production test board with 
echo    J1 connector on Opella-XD mainboard.
echo 3) Select BOOT (right) position of BOOT switch (SW9) on the 
echo    Opella-XD production test board.
echo 4) Connect USB cable from PC into Opella-XD mainboard (J2)
echo 5) PC should detect Opella-XD unit as "Oki FBP696203" under
echo    Control Panel-Device Manager-USB devices.
echo    Windows may prompt for a driver when installing for the first
echo    time. Driver is located in "OKI" subdirectory.
echo.
call %MYSLEEP% 10
pause
goto doFlashProg3

:doFlashProg3
cls
echo.
echo Opella-XD Initial Flash Programming
echo Step 3 - Programming Opella-XD Flash
echo.
echo Please follow next steps carefully.
echo 1) The Flash programming application will start automatically.
echo 2) Click the "Flash Update" button.
echo 3) Programming may take several seconds. Ensure no error occured 
echo    i.e. "File Transfer OK"
echo 4) If an error occurs then pres the RESET button on Opella-XD
echo    Production Test Board and try programming again.
echo 5) When programming is complete close the application.
echo.
call %MYSLEEP% 10
cd oki
call FBP696203.exe
cd %TempRootDir%
call %MYSLEEP% 10
pause
goto doFlashProg4

:doFlashProg4
cls
echo.
echo Opella-XD Initial Flash Programming
echo Step 4 - Finishing Flash Programming
echo.
echo Please follow next steps carefully.
echo 1) Select the NORMAL (left) position of BOOT switch (SW9) on
echo    the Opella-XD Production Test Board.
echo 2) Press the RESET button on Opella-XD Production Test Board 
echo    for about 1 second.
echo 3) PC should detect Opella-XD unit as "Opella-XD" under
echo    Control Panel-Device Manager-Ashling USB Devices.
echo    Windows may prompt for a driver when installing for 
echo    the first time.
echo    Driver is located in "DRIVER" subdirectory or you can 
echo    allow WIndows to install it automatically.
echo 4) Verify PC detected the unit correctly.
echo 5) Verify Power LED is GREEN on Opella-XD unit
echo    indicating POST (Power On Self Test) has passed and flash
echo    programming is finished. If Power LED is RED, POST has 
echo    failed indicating hardware problems.
echo 6) Disconnect Opella-XD from PC by removing USB cable.
echo 7) Disconnect Opella-XD from Production Test Board.
echo.
echo When all checks passed, Opella-XD Flash is now fully 
echo programmed.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:progFailed
:: ================================================
:: == Flash programming failed ====================
:: ================================================
cd %TempRootDir%
echo.
echo Cannot generate Flash image.
echo You entered invalid format of serial number or revision,
echo or some files are missing.
echo Please try to generate Flash image again.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:: ================================================
:: ================================================

:: ================================================
:: == Opella-XD Production Diagnostic Tests =======
:: ================================================
:doProdTests
cls
echo.
echo Opella-XD Production Diagnostic Tests
echo Step 1 - Preparing for Diagnostic Tests
echo.
echo Please follow next steps carefully.
echo.
echo 1) Both Opella-XD and Production Test Board are not powered now.
echo 2) Connect Opella-XD to Production Test Board via TPA interface.
echo    Cable connects J4 connector on Production Test Board with 
echo    J3 connector on Opella-XD mainboard.
echo 3) Power on Production Test Board by connecting its power supply 
echo    to J6 connector. 
echo 4) Connect Opella-XD to PC via USB cable.
echo 5) Verify PC detected the unit correctly.
echo 6) Verify Power LED is GREEN on Opella-XD unit indicating POST
echo    (Power On Self Test) has passed and flash programming is 
echo    finished. If power LED is RED, POST has failed indicating
echo    hardware problems.
echo.
call %MYSLEEP% 10
pause
goto doProdTests2

:doProdTests2
set OPXDCONT_ITERATION=0
set OPXDCONT_ERROR=0
REM check if single or continuous tests was selected
if %OPXDCONTTEST% == 0 goto doProdTests2Single
REM this is continous test (infinite loop)
:doProdTests2Cont
cd bin
call opxddiag.exe --list
IF NOT %ERRORLEVEL%==0 set /a OPXDCONT_ERROR=%OPXDCONT_ERROR%+1 >nul
cd %TempRootDir%
call %MYSLEEP% 10
cd bin
call opxddiag.exe --diag 1 --production
IF NOT %ERRORLEVEL%==0 set /a OPXDCONT_ERROR=%OPXDCONT_ERROR%+1 >nul
cd %TempRootDir%
REM increase number of iterations and show current status
set /a OPXDCONT_ITERATION=%OPXDCONT_ITERATION%+1 >nul
echo    ----------------------------------------------------
echo    Opella-XD Continuous Production Test Status
echo.
echo    Iteration : %OPXDCONT_ITERATION%
echo    Error(s)  : %OPXDCONT_ERROR%
echo    ----------------------------------------------------
echo    Terminate batch file with CTRL+C to stop the test.
echo.
call %MYSLEEP% 10
IF NOT %OPXDCONT_ERROR%==0 goto diagContFailed
goto doProdTests2Cont

:doProdTests2Single
cls
echo.
echo Opella-XD Production Diagnostic Tests
echo Step 2 - Showing Opella-XD information
echo.
echo Please check that the information shown below corresponds
echo with the unit under test (serial number, revision, etc.)
echo.
cd bin
call opxddiag.exe --list
IF NOT %ERRORLEVEL%==0 goto diagFailed
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto doProdTests3Single

:doProdTests3Single
cls
echo.
echo Opella-XD Production Diagnostic Tests
echo Step 3 - Running Diagnostic Tests
echo.
echo Please check diagnostic tests results below.
echo.
cd bin
call opxddiag.exe --diag 1 --production
IF NOT %ERRORLEVEL%==0 goto diagFailed
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto doProdTests4

:doProdTests4
cls
echo.
echo Opella-XD Production Diagnostic Tests
echo Step 4 - Finishing Diagnostic Tests
echo.
echo Please follow next steps carefully.
echo 1) Disconnect USB cable from Opella-XD unit.
echo 2) Power off Production Test Board by removing power supply.
echo 3) Disconnect Opella-XD unit from Production Test Board.
echo.
echo When all checks passed, Opella-XD has been now fully tested.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:diagFailed
:: ================================================
:: == Diagnostic error ============================
:: ================================================
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto startBatch

:: ================================================
:: ================================================

:: ================================================
:: == TPA for Opella-XD Initial Programming =======
:: ================================================
:doTpaProg
cls
echo.
echo TPA for Opella-XD Initial Programming
echo Step 1 - Programming signature into TPA
echo.
echo Please select TPA type you are going to program
echo      1      TPA for MIPS       TPAOP-MIPS14
echo      2      TPA for ARM        TPAOP-ARM20
echo      3      TPA for ARC        TPAOP-ARC20
echo.
echo      9      TPA on Opella-XD Production Test Board
echo.
set /p TPAOption=" Please select an option from menu :> "

:: verify TPAOption
if .%TPAOption%. == ..  goto badTPAOption
if /i %TPAOption% == 1  goto doTpaProg1
if /i %TPAOption% == 2  goto doTpaProg1
if /i %TPAOption% == 3  goto doTpaProg1
if /i %TPAOption% == 9  goto doTpaProg1
goto badTPAOption

:badTPAOption
echo.
echo You selected invalid option, please try again.
pause
goto doTpaProg

:doTpaProg1
echo Please type TPA serial number corresponding with the label.
set /p TPASerNum="TPA serial number :> "
echo.
echo Please enter TPA revision number corresponding with the label.
set /p TPARevision="TPA revision :> "
echo.
echo Please connect TPA you want to program to Opella-XD.
echo Then connect both to PC via USB cable.
echo.
call %MYSLEEP% 10
pause
echo.
cd bin
:: Use Opella-XD diagnostic utility to program TPA signature
if %TPAOption% == 1 goto doTpaProgMIPS14
if %TPAOption% == 2 goto doTpaProgARM20
if %TPAOption% == 3 goto doTpaProgARC20
::otherwise programming signature into Opella-XD Production Test Board
call opxddiag.exe --sign-tpa DIAG %TPASerNum% %TPARevision%
IF NOT %ERRORLEVEL%==0 goto progTpaFailed
cd %TempRootDir%
goto doTpaProg2

:doTpaProgMIPS14
call opxddiag.exe --sign-tpa MIPS14 %TPASerNum% %TPARevision%
IF NOT %ERRORLEVEL%==0 goto progTpaFailed
cd %TempRootDir%
goto doTpaProg2

:doTpaProgARM20
opxddiag.exe --sign-tpa ARM20 %TPASerNum% %TPARevision%
IF NOT %ERRORLEVEL%==0 goto progTpaFailed
cd %TempRootDir%
goto doTpaProg2

:doTpaProgARC20
call opxddiag.exe --sign-tpa ARC20 %TPASerNum% %TPARevision%
IF NOT %ERRORLEVEL%==0 goto progTpaFailed
cd %TempRootDir%
goto doTpaProg2

:doTpaProg2
echo.
echo Please check LED on TPA which should be on 
echo signalling new TPA has been recognized properly.
echo.
echo Also check that all information shown below corresponds
echo with the TPA under test (type, serial number, revision, etc.)
echo.
cd bin
call opxddiag.exe --list
IF NOT %ERRORLEVEL%==0 goto progTpaFailed
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto doTpaProg3

:doTpaProg3
cls
echo.
echo TPA for Opella-XD Initial Programming
echo Step 2 - Finishing TPA signature programming
echo.
echo Please follow next steps carefully.
echo 1) Disconnect USB cable from Opella-XD unit.
echo 2) Disconnect TPA from Opella-XD.
echo.
echo When all checks passed, TPA signature has been 
echo programmed successfully.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:progTpaFailed
:: ================================================
:: == TPA initial programming failed ==============
:: ================================================
cd %TempRootDir%
echo.
echo Cannot program TPA signature.
echo You may have entered invalid format of serial number 
echo or revision, or there is hardware failure.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:: ================================================
:: ================================================

:: ===================================================
:: == TPA for Opella-XD Production Diagnostic Tests ==
:: ===================================================
:doTpaTests
cls
echo.
echo TPA for Opella-XD Production Diagnostic Tests
echo Step 1 - Preparing for TPA Diagnostic Tests
echo.
echo Please follow next steps carefully.
echo 1) Both Opella-XD and Production Test Board are not powered now.
echo 2) Select 3.3V for TPA I/F on Production Test Board by setting
echo    SW3 to position 4.
echo 3) Connect tested TPA to Opella-XD unit.
echo 4) Connect tested TPA to proper TPA/IF connector on Production
echo    Test Board as shown in following list.
echo    a) Use MIPS14 when testing TPAOP-MIPS14 TPA.
echo    b) Use ARM20 when testing TPAOP-ARM20 TPA.
echo    c) Use ARC20 when testing TPAOP-ARC20 TPA.
echo 5) Connect Opella-XD to PC via USB cable
echo 6) Power on Production Test Board by connecting it power supply
echo    to J6 connector.
echo 7) Wait until "FPGA DONE" LED is on.
echo.
call %MYSLEEP% 10
pause
goto doTpaTests2

:doTpaTests2
set OPXDCONT_ITERATION=0
set OPXDCONT_ERROR=0
REM check if single or continuous tests was selected
if %OPXDCONTTEST% == 0 goto doTpaTests2Single
REM this is continous test (infinite loop)
:doTpaTests2Cont
cd bin
call opxddiag.exe --list
IF NOT %ERRORLEVEL%==0 set /a OPXDCONT_ERROR=%OPXDCONT_ERROR%+1 >nul
cd %TempRootDir%
call %MYSLEEP% 10
cd bin
call opxddiag.exe --diag 10
IF NOT %ERRORLEVEL%==0 set /a OPXDCONT_ERROR=%OPXDCONT_ERROR%+1 >nul
cd %TempRootDir%
call %MYSLEEP% 10
cd bin
call opxddiag.exe --tpa-production --diag 10
IF NOT %ERRORLEVEL%==0 set /a OPXDCONT_ERROR=%OPXDCONT_ERROR%+1 >nul
cd %TempRootDir%
REM increase number of iterations and show current status
set /a OPXDCONT_ITERATION=%OPXDCONT_ITERATION%+1 >nul
echo    ----------------------------------------------------
echo    TPA for Opella-XD Continuous Production Test Status
echo.
echo    Iteration : %OPXDCONT_ITERATION%
echo    Error(s)  : %OPXDCONT_ERROR%
echo    ----------------------------------------------------
echo    Terminate batch file with CTRL+C to stop the test.
echo.
call %MYSLEEP% 10
IF NOT %OPXDCONT_ERROR%==0 goto diagContFailed
goto doTpaTests2Cont

:doTpaTests2Single
cls
echo.
echo TPA for Opella-XD Production Diagnostic Tests
echo Step 2 - Running TPA Diagnostic Tests
echo.
echo Please check diagnostic tests results below.
echo.
cd bin
call opxddiag.exe --list
IF NOT %ERRORLEVEL%==0 goto diagTpaFailed
echo.
call opxddiag.exe --diag 10
IF NOT %ERRORLEVEL%==0 goto diagTpaFailed
echo.
call opxddiag.exe --tpa-production --diag 10
IF NOT %ERRORLEVEL%==0 goto diagTpaFailed
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto doTpaTests3

:doTpaTests3
cls
echo.
echo TPA for Opella-XD Production Diagnostic Tests
echo Step 3 - Finishing TPA Diagnostic Tests
echo.
echo Please follow next steps carefully.
echo 1) Power off Production Test Board by removing power supply.
echo 2) Disconnect USB cable from Opella-XD unit.
echo 3) Disconnect TPA from Production Test Board.
echo 4) Disconnect TPA from Opella-XD.
echo.
echo When all checks passed, TPA is now fully tested.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:diagTpaFailed
:: ================================================
:: == Diagnostic TPA error ========================
:: ================================================
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto startBatch

:: ================================================
:: ================================================

:: ===================================================
:: == TPA Adaptor Board Production Diagnostic Tests ==
:: ===================================================
:doTpaAdTests
cls
echo.
echo TPA Adaptor Board Production Diagnostic Tests
echo Step 1 - Preparing for TPA Adaptor Diagnostic Tests
echo.
echo Please follow next steps carefully.
echo 1) Both Opella-XD and Production Test Board are not powered now.
echo 2) Select 3.3V for TPA I/F on Production Test Board by setting
echo    SW3 to position 4.
echo 3) Connect TPA Adaptor Board to reference TPA.
echo    a) Use TPAOP-MIPS14 when testing ADOP-EJTAG20 adaptor.
echo    b) Use TPAOP-ARM20 when testing ADOP-RPINE10 adaptor.
echo    c) Use TPAOP-ARC20 when testing ADOP-ARC15 adaptor.
echo 4) Connect TPA Adaptor Board to proper TPA/IF connector on 
echo    Production Test Board.
echo    a) Use MIPS20 connector when testing ADOP-EJTAG20 adaptor
echo    b) Use ARM20 connector with reduction when testing ADOP-RPINE10 adaptor.
echo    c) Use ARC15 connector when testing ADOP-ARC15 adaptor
echo 5) Connect Opella-XD to PC via USB cable.
echo 6) Power on Production Test Board by connecting its power 
echo    supply to J6 connector.
echo 7) Wait until "FPGA DONE" LED is on.
echo.
call %MYSLEEP% 10
pause
goto doTpaAdTests2

:doTpaAdTests2
set OPXDCONT_ITERATION=0
set OPXDCONT_ERROR=0
REM check if single or continuous tests was selected
if %OPXDCONTTEST% == 0 goto doTpaAdTests2Single
REM this is continous test (infinite loop)
:doTpaAdTests2Cont
cd bin
call opxddiag.exe --tpa-production --tpa-adaptor --diag 10
IF NOT %ERRORLEVEL%==0 set /a OPXDCONT_ERROR=%OPXDCONT_ERROR%+1 >nul
cd %TempRootDir%
REM increase number of iterations and show current status
set /a OPXDCONT_ITERATION=%OPXDCONT_ITERATION%+1 >nul
echo    ----------------------------------------------------
echo    TPA for Opella-XD Continuous Production Test Status
echo.
echo    Iteration : %OPXDCONT_ITERATION%
echo    Error(s)  : %OPXDCONT_ERROR%
echo    ----------------------------------------------------
echo    Terminate batch file with CTRL+C to stop the test.
echo.
call %MYSLEEP% 10
IF NOT %OPXDCONT_ERROR%==0 goto diagContFailed
goto doTpaAdTests2Cont

:doTpaAdTests2Single
cls
echo.
echo TPA Adaptor Board Production Diagnostic Tests
echo Step 2 - Running TPA Diagnostic Tests
echo.
echo Please check diagnostic tests results below.
echo.
cd bin
call opxddiag.exe --tpa-production --tpa-adaptor --diag 10
IF NOT %ERRORLEVEL%==0 goto diagTpaAdFailed
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto doTpaAdTests3

:doTpaAdTests3
cls
echo.
echo TPA Adaptor Board Production Diagnostic Tests
echo Step 3 - Finishing TPA Diagnostic Tests
echo.
echo Please follow next steps carefully.
echo 1) Power off Production Test Board by removing power supply.
echo 2) Disconnect USB cable from Opella-XD unit.
echo 3) Disconnect TPA Adaptor Board from Production Test Board.
echo 4) Disconnect TPA Adaptor Board from TPA.
echo.
echo When all checks passed, TPA Adaptor Board is now fully
echo tested.
echo.
call %MYSLEEP% 10
pause
goto startBatch

:doFWUpdate
:: ================================================
:: == Opella-XD firmware update ===================
:: ================================================
cls
echo.
echo Opella-XD Firmware Version Update
echo.
echo Please follow next steps carefully.
echo 1) Opella-XD is not powered now and no TPA is connected to it.
echo 2) Connect Opella-XD to PC via USB cable.
echo.
call %MYSLEEP% 10
pause
cd bin
call opxddiag.exe --update-firmware
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto startBatch

:diagTpaAdFailed
:: ================================================
:: == Diagnostic TPA adaptor error ================
:: ================================================
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto startBatch

:diagContFailed
:: ================================================
:: == Continuous Test Failed ======================
:: ================================================
cd %TempRootDir%
echo.
call %MYSLEEP% 10
pause
goto exit

:programMessage
:: ================================================
:: == The R3 baord flash is programmed via  =======
:: == LPCXpresso. =================================
:: ================================================
echo Please use the LPCXpresso software to program the Flash!
goto exit

:: ================================================
:: ================================================

:: ================================================
:: == Cleanup and exit ============================
:: ================================================
:exit

:: clear variables


