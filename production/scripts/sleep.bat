@echo off
:: *************************************************************************
::        Module: sleep.bat
::      Engineer: Vitezslav Hola
::   Description: This script implements sleep command just in case command
::                prompt does not support it
::      Comments: 
::    Parameters: %1 - number of second to wait from 0 to ...
:: Date           Initials    Description
:: 23-Aug-2007    VH          Initial
:: *************************************************************************
set /a swait=%1+1
ping -n %swait% 127.0.0.1 > NUL 2>&1

