@echo off
:: *************************************************************************
::        Module: install.bat
::      Engineer: Vitezslav Hola
::   Description: Install script for Opella-XD Production Software
:: 
::      Comments: 
:: 
:: Date           Initials    Description
:: 18-Aug-2007    VH          Initial
:: *************************************************************************

:startBatch
set TempRootDir=%CD%
:: == Installing registry entries ==
echo Note this script must run from Opella-XD production software
echo production software folder.
echo.
echo Checking install files ...
if exist %CD%\oki\oki.reg goto filesChecked
echo Cannot find installation files.
echo Please check if install script is running from production 
echo software folder.
goto exit

:filesChecked
echo Adding new registry entries ...
regedit /S %CD%\oki\oki.reg
if %ERRORLEVEL%==0 goto installDone
echo Cannot install registry entries.
echo Please try to install %CD%\oki\oki.reg manually.
goto exit

:installDone
echo Installation has finished successfully
goto exit

:: == Exit ==
:exit


