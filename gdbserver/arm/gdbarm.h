/****************************************************************************
       Module: gdbarm.h
     Engineer: Venkitakrishnan K/Rejeesh S Babu
  Description: Ashling GDB Server, ARM target interface header file
Date           Initials    Description
31-MAR-2008    VK/RS         initial
****************************************************************************/
#ifndef __GDBARM_H__
#define __GDBARM_H__

#include "../../protocol/rdi/rdi/rdi.h"

#define MAX_SERIAL_NUMBER_LEN       16                      // maximum serial number string length (including terminating 0)
#define WARM_BOOT                0x1
#define COMMS_RESET              0x2
#define BIG_ENDIAN_MASK          0x4
#define LITTLE_ENDIAN_MASK       0x0
#define DONT_CARE_ENDIAN_MASK    0x8
#define DONT_HALT_TARGET	     0x10

/* These are the "kind" of BP's given by GDB */
#define THUMB_BP    2
#define THUMB2_BP   3
#define ARM_BP      4

extern unsigned int gbCortexM3;

typedef enum {TPA_UNKNOWN, TPA_NONE, TPA_INVALID, TPA_MIPS14, TPA_ARM20, TPA_ARC20, TPA_DIAG} TyTpaType;

typedef unsigned short  uint16;
typedef signed short    sint16;
typedef unsigned int    uint;
typedef signed int      sint;

typedef unsigned long  uint32;
typedef long           sint32;
typedef float          float16;
typedef float          float32; 
typedef char *         string ;
typedef unsigned char  ubyte ;
typedef char           sbyte ;
typedef unsigned char  uchar ;
typedef char           schar ;
typedef long           slong;
typedef unsigned long  ulong;
typedef ubyte          uint8 ;
typedef sbyte          sint8 ;
typedef uint8          bool8 ; 
typedef uint16         bool16 ;
typedef uint32         bool32 ;
typedef sint32         TyError;

typedef struct
   {
   char   * pszRegName;    // register name
   unsigned long    ulAddrIndex;
   } TyARMReg;
typedef enum {
    START_REG=0x00 ,
   //ARM
   REG_ARM_R0              = 0x0, //Core Registers
   REG_ARM_R1              = 0x1,
   REG_ARM_R2              = 0x2,
   REG_ARM_R3              = 0x3,
   REG_ARM_R4              = 0x4,
   REG_ARM_R5              = 0x5,
   REG_ARM_R6              = 0x6,
   REG_ARM_R7              = 0x7,
   REG_ARM_R8              = 0x8,
   REG_ARM_R9              = 0x9,
   REG_ARM_R10             = 0xa,
   REG_ARM_R11             = 0xb,
   REG_ARM_R12             = 0xc,
   REG_ARM_R13             = 0xd,
   REG_ARM_R14             = 0xe,
   REG_ARM_R15             = 0xf,
   REG_ARM_FPS             = 0x18,
   REG_ARM_CPSR            = 0x19,
    //Extra registesr set to support JTAG Console.
   REG_ARM_R8_FIQ          = 0x11,
   REG_ARM_R9_FIQ          = 0x12,
   REG_ARM_R10_FIQ         = 0x13,
   REG_ARM_R11_FIQ         = 0x14,
   REG_ARM_R12_FIQ         = 0x15,
   REG_ARM_R13_FIQ         = 0x16,
   REG_ARM_R14_FIQ         = 0x17,
   REG_ARM_SPSR_FIQ        = 0x10,

   REG_ARM_R13_SVC         = 0x26,
   REG_ARM_R14_SVC         = 0x1a,
   REG_ARM_SPSR_SVC        = 0x1b,
   REG_ARM_R13_ABT         = 0x1c,
   REG_ARM_R14_ABT         = 0x1d,
   REG_ARM_SPSR_ABT        = 0x1e,
   REG_ARM_R13_IRQ         = 0x1f,
   REG_ARM_R14_IRQ         = 0x20,
   REG_ARM_SPSR_IRQ        = 0x21,
   REG_ARM_R13_UND         = 0x22,
   REG_ARM_R14_UND         = 0x23,
   REG_ARM_SPSR_UND        = 0x24,
   //changed from 0x10 t0 0x19 as gdb protocol sends this value.

    /* Added for CortexM3 Support */
   REG_CORTEXM3_xPSR        = 0x30,
   REG_CORTEXM3_APSR        = 0x31,
   REG_CORTEXM3_IPSR        = 0x32,
   REG_CORTEXM3_EPSR        = 0x33,
   //REG_CORTEXM3_IAPSR_UND   = 0x34,
   //REG_CORTEXM3_EAPSR_UND   = 0x35,
   //REG_CORTEXM3_IEPSR_UND   = 0x36,
   REG_CORTEXM3_PRIMASK     = 0x34,
   REG_CORTEXM3_FAULTMASK   = 0x35,
   REG_CORTEXM3_BASEPRI     = 0x36,
   //REG_CORTEXM3_BASEPRI_MAX = 0x37,
   REG_CORTEXM3_CONTROL     = 0x37,
   REG_CORTEXM3_MSP         = 0x38,
   REG_CORTEXM3_PSP         = 0x39
   } TXRXReg;

#define TOP_26BIT_MASK           0xFFFFFF00
#define ARM_SUPERVISOR_MODE_MASK 0x13
#define INTERRUPTS_DISABLE  		0xC0

typedef int (*TyAshScanIR)(unsigned long ulTargetIRLength,unsigned long *pulDataIn,unsigned long *pulDataOut);
typedef int (*TyAshScanDR)( unsigned long ulTargetIRLength,unsigned long *pulDataIn,unsigned long *pulDataOut);
typedef int (*TyAshScanIRScanDR)(unsigned long *pulLength,unsigned long *pulDataIn,unsigned long *pulDataOut);
typedef int (*TySetJTAGFrequency)(unsigned long ulJtagFreqKhz,int bJtagRtck);
typedef int (*TyResetTAP)(void);
typedef int (*TyPrepareforJTAGConsole)(void);
typedef int (*TyProcessCP15Register) (unsigned int uiOperation, unsigned int uiCRn, unsigned int uiCRm,
                                      unsigned int uiOp1, unsigned int uiOp2,unsigned long *pulData);
typedef int (*TyReadCPRegister)(unsigned int uiCPNo, unsigned int uiRegIndex, unsigned long *pulData);
typedef int (*TyWriteCPRegister)(unsigned int uiCPNo, unsigned int uiRegIndex, unsigned long ulRegValue);
  
struct TyDeviceInfo{
   // general device information
   char pszSerialNumber[MAX_SERIAL_NUMBER_LEN];
   unsigned long ulBoardRevision;
   unsigned long ulManufacturingDate;
   // firmware information
   unsigned long ulFirmwareVersion;
   unsigned long ulFirmwareDate;
   // diskware information
   unsigned long ulDiskwareType;
   unsigned long ulDiskwareVersion;
   unsigned long ulDiskwareDate;
   unsigned char uiActiveDiskwares;
   // FPGA information
   unsigned long ulFpgawareType;
   unsigned long ulFpgawareVersion;
   unsigned long ulFpgawareDate;
   unsigned char bFpgawareConfigured;
   // TPA information
   TyTpaType tyTpaConnected;
   char pszTpaName[16];
   double dMinVtpa;
   double dMaxVtpa;
   double dCurrentVtpa;
   double dCurrentVtref;
   unsigned char bTrackingVtpaMode;
   unsigned char bTpaDriversEnabled;
   unsigned char bTargetConnected;
   unsigned long ulTpaRevision;
   char pszTpaSerialNumber[16];
} ;

//ARM Messages
#define GDB_MSG_TARGET_DISCONNECT       "Disconnecting target ..."
#define GDB_MSG_READ_REGISTERS          "Read all registers."
#define GDB_MSG_READ_A_REGISTER         "Read register %s value=0x%08lX."
#define GDB_MSG_WRITE_REGISTERS         "Write all registers."
#define GDB_MSG_WRITE_A_REGISTER        "Write register %s value=0x%08lX."
#define GDB_MSG_READ_PC_REGISTER        "Read PC value=0x%08lX."
#define GDB_MSG_WRITE_PC_REGISTER       "Write PC value=0x%08lX."
#define GDB_MSG_READ_MEMORY             "Read Memory at 0x%08lX for length 0x%08lX."
#define GDB_MSG_WRITE_MEMORY            "Write Memory at 0x%08lX for length 0x%08lX."
#define GDB_MSG_NONEED_DELETE_BP        "Ignoring delete breakpoint request as there is no %s breakpoint set at address 0x%08lX."
#define GDB_MSG_DELETE_BP               "Delete %s breakpoint at 0x%08lX for length %ld (Core %d)."
#define GDB_MSG_SET_BP                  "Set %s breakpoint at 0x%08lX for length %ld (Core %d)."
#define GDB_MSG_CONTINUE                "Continue."
#define GDB_MSG_EXECUTE                 "Execute."
#define GDB_MSG_EXECUTION_TERMINATED    "Execution of %s terminated normally."
#define GDB_MSG_KEY_HIT_DETECTED        "Key press detected, halting execution of %s."
#define GDB_MSG_STEP                    "Step."
#define GDB_MSG_HWRESET                 "Hwreset."
#define GDB_MSG_DETACH                  "Detach."


int LOW_ReadAnyMemory(unsigned long ulAddress,
                   unsigned long ulCount,
                   unsigned char *pucData,
                   RDI_AccessType eRDIAccessType);
#endif// __GDBARM_H__
