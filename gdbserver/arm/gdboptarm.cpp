/****************************************************************************
       Module: gdboptarm.c
     Engineer: Venkitakrishnan K/Rejeesh S Babu
  Description: Ashling GDB Server, ARM specific command line options
Date           Initials    Description
23-Mar-2008    VK/RS       initial
09-Apr-2008    VK          Displaying Help support options
23-Sep-2009    SVL         Added support for 'PFXD mode'
29-Apr-2010    SJ          Added Support for Coresight devices
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdboptarm.h"
#include "../../protocol/rdi/rdi/rdi.h"
#include "../../protocol/rdi/opxdarmconfigmgr/OpellaConfig.h"
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbopt.h"

extern unsigned long gulPC;
extern TyServerInfo tySI;


// --------------------------
// local function prototypes
// --------------------------
static int OptionHelp               (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionVersion            (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionJtagConsoleMode    (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionEnableRDILogging   (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDebugStdout        (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDebugFile          (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionCommandFile        (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionGdbPort            (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionProbeInstance      (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionJtagFrequency      (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDevice             (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionScan               (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionProgramEntryPoint  (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionTapNumber          (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionTargetReset        (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionEndian             (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionnTRST              (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionCacheFlushRoutine  (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDbrq               (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionSafenonVector      (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionTargetVoltage      (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionPFXDMode          (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDevRegFile         (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionUserRegFile        (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionConnection         (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionProbe              (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionTotalCores         (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionCoresightBaseAddress(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDebugAPIndex       (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionMemoryAPIndex      (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionJtagAPIndex        (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static void HelpJtagFrequency(void);
static void HelpDevice              (void);
static void HelpReset               (void);
static void HelpEndian              (void);
static void HelpTargetVoltage       (void);
static char *  TranslateName(char * pszDst, char * pszSrc, unsigned int uiSize);
extern void CFG_ScanChainNode(TyXMLNode *ptyThisNode);
#define MIN_JTAGFreq                1
#define MAX_JTAGFreq                100000

/* ---------------------------
Command Line Options table
-----------------------------*/

TyOption tyaOptions[] =
{
   {
   "help",
   "",
   "Display usage information.",
   "",
   "",
   "",
   OptionHelp,
   NULL,
   FALSE
   },

   {
   "version",
   "",
   "Display program version.",
   "",
   "",
   "",
   OptionVersion,
   NULL,
   FALSE
   },

   {
   "debug-stdout",
   "",
   "Display debug messages to standard output.",
   "",
   "",
   "",
   OptionDebugStdout,
   NULL,
   FALSE
   },
	{
		"EnableRDILogging",
		"",
		"Enable RDI logging.",
		"",
		"",
		"",
		OptionEnableRDILogging,
		NULL,
		FALSE
	},
   {
   "debug-file",
   " <file>",
   "Log debug messages to <file>.",
   "",
   "",
   "",
   OptionDebugFile,
   NULL,
   FALSE
   },

   {
   "command-file",
   " <file>",
   "Read command line options from <file>.",
   "",
   "",
   "",
   OptionCommandFile,
   NULL,
   FALSE
   },

   {
   "gdb-port",
   " <port>",
   "Listen on <port> for remote connections",
   "from GDB. Default is port 2331.",
   "",
   "",
   OptionGdbPort,
   NULL,
   FALSE
   },

   {
   "instance",
   " <number>",
   "Select Opella-XD from multiple Opella-XD(s)",
   "connected.",
   "<number> is Opella-XD serial number.",
   "",
   OptionProbeInstance,
   NULL,
   FALSE
   },

   {
   "jtag-frequency",
   " <freq>",
   "Specifies JTAG frequency.",
   "",
   "",
   "",
   OptionJtagFrequency,
   HelpJtagFrequency,
   FALSE
   },
   {
   "device",
   " <dev>",
   "Specifies target device. Device information",
   "is extracted from FAMLYARM.XML.",
   "<dev> is:",
   "",
   OptionDevice,
   HelpDevice,
   FALSE
   },

   {
   "scan-file",
   " <file>",
   "Specifies target scanchain.",
   "The scanchain information is extracted ",
   "from XML <file>",
   "",
   OptionScan,
   NULL,
   FALSE
   },

   {
   "program-entry-point",
   " <addr>",
   "Set the program entry point to <addr>.",
   "",
   "",
   "",
   OptionProgramEntryPoint,
   NULL,
   FALSE
   },

   {
   "tap-number",
   " <number>",
   "Specifies JTAG Test Access Point(TAP) number.",
   "",
   "",
   "",
   OptionTapNumber,
   NULL,
   FALSE
   },

   {
   "jtag-console-mode",
   "",
   "Open JTAG low-level console.",
   "",
   "",
   "",
   OptionJtagConsoleMode,
   NULL,
   FALSE
   },

   {
   "target-reset",
   " <0|1|2|3|4|5>",
   "Specifies target reset option.",
   "",
   "",
   "",
   OptionTargetReset,
   HelpReset,
   FALSE
   },

   {
   "endian",
   " <endianess>",
   "Specifies endian mode.",
   "",
   "",
   "",
   OptionEndian,
   HelpEndian,
   FALSE
   },

   {
   "use-ntrst",
   " <0|1>",
   "Enable nTRST:",
   "  0 nTRST disabled. ",
   "  1 nTRST enabled (default).",
   "",
   OptionnTRST,
   NULL,
   FALSE
   },

   {
   "set-dbrq",
   " <0|1>",
   "Set DBRQ:",
   "  0 Low (default).",
   "  1 High.",
   "",
   OptionDbrq,
   NULL,
   FALSE
   },

   {
   "cache-flush-routine",
   " <addr>",
   "Enable debugging of cached code and set the",
   "cache invalidation routine to <addr>.",
   "Requires 0x200 bytes at <addr>.",
   "",
   OptionCacheFlushRoutine,
   NULL,
   FALSE
   },

   {
   "safe-non-vector",
   " <addr>",
   "Set safe non-vector <addr>.",
   "",
   "",
   "",
   OptionSafenonVector,
   NULL,
   FALSE
   },  
    {
   "target-voltage",
   " <voltage>",
   "Select target voltage.",
   "",
   "",
   "",
   OptionTargetVoltage,
   HelpTargetVoltage,
   FALSE
   },
   /*PFXD mode*/
   {
   "pfxd-mode",
   "",
   "Start in PFXD mode which defers connecting to",
   "target until monitor connect command is received",
   "",
   "",
   OptionPFXDMode,
   NULL,
   FALSE
   },   
   {
   "device-reg-file",
   "<file>",
   "Parse a device register file to get the peripheral register information.",
   "This functionality is not currently supported",
   "",
   "",
   OptionDevRegFile,
   NULL,
   FALSE
   },   
   {
   "reg-settings-file",
   "<file>",
   "Read and apply register settings from <file>.",
   "This functionality is not currently supported",
   "",
   "",
   OptionUserRegFile,
   NULL,
   FALSE
   },   
   {
   "connection",
   "<usb|ethernet>",
   "Specify the connection interface which is used to connect Vitra and host.",
   "This functionality is not currently supported",
   "",
   "",
   OptionConnection,
   NULL,
   FALSE
   },   
   {
   "probe",
   "<opellaxd|vitra>",
   "Specify the debug probe being used.",
   "This functionality is not currently supported",
   "",
   "",
   OptionProbe,
   NULL,
   FALSE
   },   
   {
   "total-cores",
   "<num>",
   "Specify the total number of cores in the target.",
   "This functionality is not currently supported",
   "",
   "",
   OptionTotalCores,
   NULL,
   FALSE
   },

   {
   "coresight-base-address",
   " <addr>",
   "Set Coresight Base Address <addr>.",
   "Valid only for Coresight compliant devices",
   "",
   "",
   OptionCoresightBaseAddress,
   NULL,
   FALSE
   },

   {
   "debug-ap-index",
   " <index>",
   "Set Debug AP Index <index>.",
   "Valid only for Coresight compliant devices",
   "",
   "",
   OptionDebugAPIndex,
   NULL,
   FALSE
   },

   {
   "memory-ap-index",
   " <index>",
   "Set Memory AP Index <index>.",
   "Valid only for Coresight compliant devices",
   "",
   "",
   OptionMemoryAPIndex,
   NULL,
   FALSE
   },

   {
		"jtag-ap-index",
		" <index>",
		"Set Jtag AP Index <index>.",
		"Valid only for Coresight compliant devices",
		"",
		"",
		OptionJtagAPIndex,
		NULL,
		FALSE
   }
};

#define MAX_OPTIONS ((int)(sizeof(tyaOptions) / sizeof(TyOption)))
int iGdb_options_number = MAX_OPTIONS;

// ----------------
// local variables
// ----------------
enum TargetResetMode eTargetResetMode;
enum Endian eEndian;
enum SemiHostingSupport eSemiHostingSupport;
enum JTAGClockFreqMode eJTAGClockFreqMode;
// ----------------
// local functions 
// ----------------
/****************************************************************************
     Function: OptionHelp
     Engineer: Venkitakrishnan K
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option help.
Date           Initials    Description
23-Mar-2008    VK         initial
*****************************************************************************/
static int OptionHelp(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bHelp = 1;
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionEndian
     Engineer: Rejeesh S Babu
        Input: *piArg    : pointer to index of next arg
               uiArgCount: count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for option Endian.
Date           Initials    Description
23-Mar-2008    RS         initial
*****************************************************************************/
static int OptionEndian(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{ 
    assert(puiArg      !=NULL);
    assert(ppszArgs    !=NULL);

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_ENDIANESS_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   
   if (strcasecmp(ppszArgs[*puiArg], "little") == 0)
      tySI.tyProcConfig.Processor._ARM.tyEndianess = Little_Endian;
   else if (strcasecmp(ppszArgs[*puiArg], "big") == 0)
      tySI.tyProcConfig.Processor._ARM.tyEndianess = Big_Endian;
   else
      {
      sprintf(tySI.szError, GDB_EMSG_ENDIANESS_INVALID, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionTargetReset
     Engineer: Venkitakrishnan K
        Input: *piArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Specifies Target Reset Mode
Date           Initials    Description
31-Mar-2008     VK         Initial
*****************************************************************************/
static int OptionTargetReset(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulReset  = 0;
   int bResetDelay  = 0;
   int bValid       = FALSE;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_PORT_NO_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulReset = StrToUlong(ppszArgs[*puiArg], &bValid);
   if (!bValid || (ulReset > 0x5))
      {
      sprintf(tySI.szError, GDB_EMSG_RST_OPTN_INVALID, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   if(ulReset==0)
      tySI.tyProcConfig.Processor._ARM.tyTargetReset =NoHard_Reset;
   if(ulReset==1)
   {
       tySI.tyProcConfig.Processor._ARM.tyTargetReset =HardResetAnd_Delay;
       tySI.ulEnableTargetReset=1;
   }
      
   if(ulReset==2)
   {
       tySI.ulEnableTargetReset=1;
       tySI.tyProcConfig.Processor._ARM.tyTargetReset =HardResetAnd_HaltAtRV;
   }
      
   if(ulReset==3)
   {
       tySI.ulEnableTargetReset=1;
       tySI.tyProcConfig.Processor._ARM.tyTargetReset =Hot_Plug;
   }

   if((ulReset==4))
   {

       tySI.ulEnableTargetReset=1;
       tySI.tyProcConfig.Processor._ARM.tyTargetReset =Core_Only;
   }
   if((ulReset==5))
   {

       tySI.ulEnableTargetReset=1;
       tySI.tyProcConfig.Processor._ARM.tyTargetReset =Core_And_Peripherals;
   }
   if(ulReset==1)
       {
           (*puiArg)++;
    
       bResetDelay =(int)StrToUlong(ppszArgs[*puiArg], &bValid);
       if (!bValid || (bResetDelay > 0x10))
          {
          sprintf(tySI.szError,GDB_EMSG_RDELAY_OPTN_INVALID, ppszArgs[*puiArg]);
          return GDBSERV_OPT_ERROR;
          }
       }
   tySI.bTargetResetDelay=bResetDelay;

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionnTRST
     Engineer: Venkitakrishnan K
        Input: *piArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option nTRST
Date           Initials    Description
23-Mar-2008    VK         initial
15-Apr-2008    VK         Added Options(ON/OFF) 
*****************************************************************************/
static int OptionnTRST(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
    assert(puiArg      !=NULL);
    assert(ppszArgs    !=NULL);
   unsigned long ulnTRst;
    int bValid       = FALSE;
   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_ARG_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulnTRst = StrToUlong(ppszArgs[*puiArg], &bValid);
   if((!bValid) ||(ulnTRst > 1))
      {
      sprintf(tySI.szError, GDB_EMSG_NTRST_OPTN_INVALID);
      return GDBSERV_OPT_ERROR;
      }
   else
      if(ulnTRst == 1)
         tySI.bnTRst = TRUE;
      else
         tySI.bnTRst = FALSE;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionDbrq
     Engineer: Rejeesh S Babu
        Input: *piArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option Dbrq
Date           Initials    Description
23-Mar-2008    RS         initial
*****************************************************************************/
static int OptionDbrq(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulDbrq;
   int  bValid         = FALSE;
    if ((*puiArg)+1 >= uiArgCount)
       {
       sprintf(tySI.szError, GDB_EMSG_ARG_MISSING, ppszArgs[*puiArg]);
       return GDBSERV_OPT_ERROR;
       }
    (*puiArg)++;

 ulDbrq = StrToUlong(ppszArgs[*puiArg], &bValid);
   if((!bValid) ||(ulDbrq > 1))
      {
      sprintf(tySI.szError, GDB_EMSG_DBRQ_OPTN_INVALID);
      return GDBSERV_OPT_ERROR;
      }
   else
      if(ulDbrq == 0)
         tySI.bDbrq = TRUE;
      else
         tySI.bDbrq = FALSE;
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionCacheFlushRoutine
     Engineer: Rejeesh S Babu
        Input: *piArg    : pointer to index of next arg
               iArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option CacheFlushRoutine
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionCacheFlushRoutine(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulFlushRoutine = 0;
   int  bValid         = FALSE;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_CACHE_ADDR_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulFlushRoutine = StrToUlong(ppszArgs[*puiArg], &bValid);

   if ((!bValid) ||(ulFlushRoutine>0xFFFFFFFF))
      {
      sprintf(tySI.szError, GDB_EMSG_CACHE_ADDR_INVALID, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }

   tySI.tyProcConfig.Processor._ARM.ulCacheCleanAddress  = ulFlushRoutine;

   PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_CACHE_ADDR, ulFlushRoutine);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionSafenonVector
     Engineer: Venkitakrishnan K
        Input: *piArg    : pointer to index of next arg
               iArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option SafenonVector
Date           Initials    Description
23-Mar-2008    VK         initial
*****************************************************************************/
static int OptionSafenonVector(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulsafenonvector = 0;
   int  bValid         = FALSE;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_SAFE_NON_VECTOR_ADDR_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulsafenonvector = StrToUlong(ppszArgs[*puiArg], &bValid);

   if ((!bValid) ||(ulsafenonvector>0xFFFFFFFF))
      {
      sprintf(tySI.szError, GDB_EMSG_SAFE_NON_VECTOR_ADDR_INVALD, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }

   tySI.tyProcConfig.Processor._ARM.ulSafeNonVectorAddress  = ulsafenonvector;
   tySI.bEnableSafenonVectorAddress=TRUE;
   PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_SAFE_NON_VECTOR_ADDR, ulsafenonvector);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionTargetVoltage
     Engineer: Venkitakrishnan K
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option TargetVoltage
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionTargetVoltage(unsigned int *puiArg, 
                                   unsigned int uiArgCount, 
                                   char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   float   fFixedvoltage=0.0; 
   char    szVoltage[64];

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_TARGET_VOLTAGE_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   strncpy(szVoltage, ppszArgs[*puiArg], sizeof(szVoltage)-1);
   szVoltage[sizeof(szVoltage)-1] = '\0';

   // test for TRACKING 
   if ((strcasecmp(szVoltage, "TRACK") == 0) ||
       (strcasecmp(szVoltage, "track") == 0))  
      {

      tySI.bVoltageTracking = TRUE;
      
      }
   else
      {
      tySI.bVoltageTracking = FALSE;         
      if (strcasecmp(szVoltage, "1.2") == 0)
         fFixedvoltage =1.2;
      if (strcasecmp(szVoltage, "1.8") == 0)
         fFixedvoltage =1.8;
      if (strcasecmp(szVoltage, "2.5") == 0)
         fFixedvoltage =2.5;
      if (strcasecmp(szVoltage, "3.3") == 0)
         fFixedvoltage =3.3;
      else
      {
         sprintf(tySI.szError, GDB_EMSG_TARGET_VOLTAGE_INVALID, ppszArgs[*puiArg]);
         return GDBSERV_OPT_ERROR;
         }
      }

   tySI.fVoltage=fFixedvoltage;

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionVersion
     Engineer: Venkitakrishnan K
        Input: *piArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option Version
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionVersion(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bVersion = 1;
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionJtagConsoleMode
     Engineer: Venkitakrishnan K
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option jtag console mode.
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionJtagConsoleMode(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bJtagConsoleMode = 1;
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
	  Function: OptionEnableRDILogging
	  Engineer: Roshan T R
		  Input: unsigned int *puiArg - pointer to index of next arg
					unsigned int uiArgCount - count of args in the list
					char **ppszArgs - list of args
		 Output: int - error code
  Description: Action for option Enable RDI Logging.
Date           Initials    Description
01-Sep-2010    RTR         initial
*****************************************************************************/
static int OptionEnableRDILogging(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
	assert(puiArg      !=NULL);
	assert(ppszArgs    !=NULL);
	NOREF(uiArgCount);
	NOREF(puiArg);
	NOREF(ppszArgs);
	tySI.bEnableRDILogging = 1;
	return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionDebugStdout
     Engineer: Venkitakrishnan K
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option debug output.
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionDebugStdout(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bDebugStdout = 1;
   tySI.bDebugOutput = 1;
   PrintMessage(INFO_MESSAGE, GDB_MSG_DEBUG_MSG_To_STDOUT);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionDebugFile
     Engineer: Venkitakrishnan K
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for debug file.
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionDebugFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_FILENAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(tySI.szDebugFile, ppszArgs[*puiArg], sizeof(tySI.szDebugFile)-1);
   tySI.szDebugFile[sizeof(tySI.szDebugFile)-1] = '\0';
   tySI.pDebugFile = fopen(tySI.szDebugFile, "w");
   if (tySI.pDebugFile == NULL)
      {
      sprintf(tySI.szError, GDB_EMSG_DEBUG_FILE_ERROR, tySI.szDebugFile);
      return GDBSERV_OPT_ERROR;
      }
   tySI.bDebugOutput = 1;
   // put title and version in log file
   LogTitleAndVersion();
   PrintMessage(INFO_MESSAGE, GDB_MSG_LOGGING_DEBUG_MSG, tySI.szDebugFile);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionCommandFile
     Engineer: Rejeesh S Babu
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option command file.
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionCommandFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   int iError;
   if (((*puiArg) + 1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_FILENAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(tySI.szCommandFile, ppszArgs[*puiArg], sizeof(tySI.szCommandFile)-1);
   tySI.szCommandFile[sizeof(tySI.szCommandFile)-1] = '\0';
   PrintMessage(INFO_MESSAGE, GDB_MSG_READING_CMD_LINE_OPTN, tySI.szCommandFile);
   iError = ProcessCmdFileArgs(tySI.szCommandFile, 0);
   return iError;
}
/****************************************************************************
     Function: OptionGdbPort
     Engineer: Rejeesh S Babu
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option GDB port.
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionGdbPort(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulPort = 0;
   int  bValid = 0;
   if (((*puiArg) + 1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_PORT_NUMBER_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   ulPort = StrToUlong(ppszArgs[*puiArg], &bValid);
   if (!bValid || (ulPort > 0xFFFF))
      {
      sprintf(tySI.szError, GDB_EMSG_PORT_NUMBER_INVALID, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   tySI.usGdbPort = (unsigned short)ulPort;
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionProbeInstance
     Engineer: Venkitakrishnan K
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Selecting probe instance number if necessary
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionProbeInstance(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
    if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_OPELLAXD_SERIAL_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   memset((void *)tySI.szProbeInstance, 0, PROBE_STRING_LEN * sizeof(char));
   if (strlen(ppszArgs[*puiArg]) < PROBE_STRING_LEN)
      {  // string length is less than buffer size
      strcpy(tySI.szProbeInstance, ppszArgs[*puiArg]);
      tySI.szProbeInstance[PROBE_STRING_LEN-1] = '\0';
      }
   else
      {
      sprintf(tySI.szError, GDB_EMSG_OPELLAXD_SERIAL_INVALID );
      return GDBSERV_OPT_ERROR;
      }
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionJtagFrequency
     Engineer: Venkitakrishnan K
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionJtagFrequency(unsigned int *puiArg, 
                                   unsigned int uiArgCount, 
                                   char **ppszArgs)
{
   assert(puiArg      !=NULL);  
   assert(ppszArgs    !=NULL);
   unsigned long    ulMultiplier = 1000;
   unsigned long    ulFrequency  = 0;
   int     bValid       = FALSE;
   char    *pszUnit;
   int      iLen;
   char     szFrequency[64];
   
   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_JTFREQ_MISSING , ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   strncpy(szFrequency, ppszArgs[*puiArg], sizeof(szFrequency)-1);
   szFrequency[sizeof(szFrequency)-1] = '\0';

   // test for RTCK 
   if ((strcasecmp(szFrequency, "rtck") == 0) ||
       (strcasecmp(szFrequency, "RTCK") == 0))  
      {

      tySI.bJtagRtck = TRUE;
      
      }
   else
      {
      tySI.bJtagRtck = FALSE;
      // test for kHz or MHz (default) at end of number...
      if ((iLen = (int)strlen(szFrequency)) > 3)
         {
         pszUnit = szFrequency + iLen - 3;
         if (strcasecmp(pszUnit, "khz") == 0)
            {
            pszUnit[0]   = '\0';
            ulMultiplier = 1;
            }
         else if (strcasecmp(pszUnit, "mhz") == 0)
            {
            pszUnit[0]   = '\0';
            ulMultiplier = 1000;
            }
         }
   
      ulFrequency = StrToUlong(szFrequency, &bValid);
   
      if (!bValid)
         {
         if(!(strncmp(ppszArgs[*puiArg],"-",1)))
            sprintf(tySI.szError, GDB_EMSG_JTFREQ_MISSING, ppszArgs[--*puiArg]);
         else
            sprintf(tySI.szError, GDB_EMSG_JTFREQ_INVALID,ppszArgs[*puiArg]);
         return GDBSERV_OPT_ERROR;
         }
   
      tySI.ulJtagFreqKhz = ulFrequency * ulMultiplier;

      if((tySI.ulJtagFreqKhz<MIN_JTAGFreq) || (tySI.ulJtagFreqKhz>MAX_JTAGFreq))
         {
             sprintf(tySI.szError, GDB_EMSG_JTFREQ_INVALID,ppszArgs[*puiArg]); 
             return GDBSERV_OPT_ERROR;

         }
      }
   if(tySI.bJtagRtck)
      {
         eJTAGClockFreqMode=Adaptive;
      }
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionDevice
     Engineer: Rejeesh S Babu
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option device.
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionDevice(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   TyXMLNode *ptyNode;
   char *pszDevice;
   char szBuffer[_MAX_PATH];
   char szBufferArg[_MAX_PATH];
   unsigned long ulIndex = 0;
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   tySI.ulProcessor = 0;
   tySI.ptyNodeProcessor = NULL;
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_DEVICE_NAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   (void)TranslateName(szBufferArg, ppszArgs[*puiArg], sizeof(szBufferArg));

   // check for a matching device...
   ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Processor");
   while (ptyNode != NULL)
      {
      if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
         {
         ulIndex++;
         (void)TranslateName(szBuffer, pszDevice, sizeof(szBuffer));
         if (strcasecmp(szBufferArg ,szBuffer) == 0)
            {
            tySI.ulProcessor = ulIndex;
            tySI.ptyNodeProcessor = ptyNode;
            break;
            }
         }
      ptyNode = XML_GetNextNode(ptyNode);
      }
   if (tySI.ulProcessor == 0)
      {
      if(!(strncmp(ppszArgs[*puiArg],"-",1)))
         PrintMessage(ERROR_MESSAGE,GDB_EMSG_DEVICE_NAME_MISSING, ppszArgs[--*puiArg]);
      else
         PrintMessage(ERROR_MESSAGE,GDB_EMSG_DEVICE_NAME_INVALID, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionScan
     Engineer: Rejeesh S Babu
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option scan.
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionScan(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   TyXMLNode *ptyNode;
   TyXMLFile tyXMLSCFile;
   int iError;
   char szFileName[_MAX_PATH];
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_FILENAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(szFileName, ppszArgs[*puiArg], sizeof(szFileName)-1);
   szFileName[sizeof(szFileName)-1] = '\0';
   iError = XML_ReadFile(szFileName, &tyXMLSCFile);
   if (iError != 0)
      return iError;
   ptyNode = XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "TargetDefinition");
   if (ptyNode == NULL)
      {
      sprintf(tySI.szError, GDB_EMSG_SCANCHAIN_FILE_INVALID, tySI.tyXMLFile.szName);
      return GDBSERV_CFG_ERROR;
      }
   CFG_ScanChainNode(ptyNode);
   XML_Free(&tyXMLSCFile);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionProgramEntryPoint
     Engineer: Venkitakrishnan K
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option entry point.
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static int OptionProgramEntryPoint(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   unsigned long ulEntryPoint = 0;
   int  bValid = 0;
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_ENTRYPOINT_ADDR_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   ulEntryPoint = StrToUlong(ppszArgs[*puiArg], &bValid);
   if (!bValid)
      {
      sprintf(tySI.szError, GDB_EMSG_ENTRYPOINT_ADDR_INVALID, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   tySI.tyProcConfig.ulProgEntryPoint  = ulEntryPoint;
   gulPC=tySI.tyProcConfig.ulProgEntryPoint;
   tySI.tyProcConfig.bUseEntryPoint    = TRUE;
   PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_ENTRYPOINT_ADDR, ulEntryPoint);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionTapNumber
     Engineer: Rejeesh S Babu
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Action for Option TapNumber
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
static int OptionTapNumber(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   unsigned long ulTap  = 0;
   char szTAPNumbers[MAX_TAP_NUM*2];
   char szCurTAPNumber[MAX_TAP_NUM*2];
   int bValid = 0;
   unsigned int uiTapCnt = 0;
   unsigned int uiInd = 0;
   int iCnt;
   int t=0;
   int tc=0;
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_TAP_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   

   strncpy(szTAPNumbers, ppszArgs[*puiArg], (MAX_TAP_NUM*2));
   //get TAP numbers from command line argument and 
   //store them as a strings array
   for (iCnt=0; iCnt <(MAX_TAP_NUM*2); iCnt++)
      {
      if ((szTAPNumbers[iCnt] != ',') &&
          (szTAPNumbers[iCnt] != '\0')) //get the last TAP
         {
         szCurTAPNumber[uiInd++] = szTAPNumbers[iCnt]; 
         }
      else
         {
         szCurTAPNumber[uiInd] = '\0';
         ulTap = StrToUlong(szCurTAPNumber, &bValid);
         if (!bValid || 
             (ulTap > MAX_TAP_NUM) ||
             (uiTapCnt > (MAX_TAP_NUM-1)))
            {
            sprintf(tySI.szError, GDB_EMSG_TAP_INVALID, szCurTAPNumber);

            return GDBSERV_OPT_ERROR;
            }
      
         tySI.ulTAP[uiTapCnt++] = ulTap;

         if(szTAPNumbers[iCnt] == '\0')   
            break;   //this was the last TAP in arguments
         uiInd = 0;
         }
      }
   tc=(int)uiTapCnt-1;

   PrintMessage(HELP_MESSAGE, GDB_MSG_TAP_NUM_CHOSEN);
   for( t=0;t<=tc;t++)
      PrintMessage(HELP_MESSAGE, "%d ",(unsigned int)tySI.ulTAP[t]);
   PrintMessage(HELP_MESSAGE, "\n",NULL);  
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionPFXDMode
     Engineer: Sandeep V L
        Input: None
       Output: int : GDBSERV_NO_ERROR, always
  Description: Checks whether the GDB Server is started in PFXD mode
Date           Initials    Description
23-Sep-2009    SVL         Initial
****************************************************************************/
static int OptionPFXDMode(unsigned int *puiArg, unsigned int uiArgCount, 
                           char **ppszArgs)
{
   NOREF(puiArg);
   NOREF(uiArgCount);
   NOREF(ppszArgs);

   tySI.bPfxdMode = TRUE; 
   tySI.bIsConnected = FALSE;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionDevRegFile
     Engineer: Sandeep V L
        Input: None
       Output: None
  Description: Device Register file specifies the information for peripheral 
               registers in a particular target board.
Date           Initials    Description
21-Oct-2009    SVL         Initial
****************************************************************************/
static int OptionDevRegFile(unsigned int *puiArg, unsigned int uiArgCount, 
                           char **ppszArgs)
{
   NOREF(puiArg);
   NOREF(uiArgCount);
   NOREF(ppszArgs);

   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_FILENAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   printf("Device register file : %s\n", ppszArgs[*puiArg]);
   tySI.bDevRegFile = true;
   /* copy the device register file name */
   strcpy(tySI.szDevRegFile, ppszArgs[*puiArg]);

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: ParseUserRegFile
     Engineer: Sandeep V L
        Input: None
       Output: None
  Description: Parse user specified register file.
Date           Initials    Description
21-Oct-2009    SVL         Initial
****************************************************************************/
int ParseUserRegFile()
{
   FILE    *pFile = NULL;
   int      iLineNumber = 0;
   int      iScannedItems;
   unsigned long    ulArg1;
   unsigned long    ulArg2;
   unsigned long    ulArg3;
   char     szVariableName[_MAX_PATH];
   char     szLine[_MAX_PATH];

   // open in text mode so that CR+LF is converted to LF
   pFile = fopen(tySI.szRegisterFile, "rt");
   if (pFile == NULL)
      {
      sprintf(tySI.szError, "Could not open register settings file '%s'.", tySI.szRegisterFile);
      return GDBSERV_OPT_ERROR;
      }

   PrintMessage(INFO_MESSAGE, "Processing register settings file '%s'...", tySI.szRegisterFile);

   while (!feof(pFile))
      {
      if (fgets(szLine, sizeof(szLine), pFile) == NULL)
         break;

      iLineNumber++;

      szVariableName[0] = '\0';
      ulArg1 = ulArg2 = ulArg3 = 0;

      iScannedItems = sscanf(szLine, " %s %lx %lx %lx ", szVariableName,
                                                      &ulArg1,
                                                      &ulArg2,
                                                      &ulArg3);
      // if an empty line, continue with next line...
      if (iScannedItems <= 0)
         continue;

      // if a comment, continue with next line...
      if ((szVariableName[0] == '\0') ||
          (szVariableName[0] == '#' ) ||
          (szVariableName[0] == ';' ) ||
          ((szVariableName[0] == '/') && (szVariableName[1] == '/')))
         continue;

      if (iScannedItems == 2)
         {
         // Format: Name Value.
         // E.g.  : CMEM0 0xF

         strcpy(tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].szVariableName, szVariableName);
         tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].ulVariableSize    = 0;
         tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].ulVariableAddress = 0;
         tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].ulVariableValue   = ulArg1;
         tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount++;

         //PrintMessage(INFO_MESSAGE, "Writing 0x%lX to %s.",
         //               ulArg1, szVariableName);
         }
      else if (iScannedItems == 4)
         {
         // Format: Name Size Addr Value.
         // E.g.  : R0 0x00000004 0xb800380c 0x18000000

         if ((ulArg1 != 1) && (ulArg1 != 2) && (ulArg1 != 4))
            {
            sprintf(tySI.szError, "Invalid register size at line %d of register settings\nfile '%s', only sizes 1, 2 or 4 allowed:\n%s", iLineNumber, tySI.szRegisterFile, szLine);
            return GDBSERV_OPT_ERROR;
            }

         strcpy(tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].szVariableName, szVariableName);
         tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].ulVariableSize    = ulArg1;
         tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].ulVariableAddress = ulArg2;
         tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount].ulVariableValue   = ulArg3;
         tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount++;

         //PrintMessage(INFO_MESSAGE, "Writing 0x%lX to %s at 0x%08lX of %d bytes.",
         //               ulArg3, szVariableName, ulArg2, ulArg1);
         }
      else
         {
         sprintf(tySI.szError, "Invalid format at line %d of register settings file '%s':\n%s", iLineNumber, tySI.szRegisterFile, szLine);
         return GDBSERV_OPT_ERROR;
         }
      }
   if(pFile != NULL)
      (void)fclose(pFile);

   return 0;
}

/****************************************************************************
     Function: OptionUserRegFile
     Engineer: Sandeep V L
        Input: None
       Output: None
  Description: Parse user specified register file.
Date           Initials    Description
21-Oct-2009    SVL         Initial
****************************************************************************/
static int OptionUserRegFile(unsigned int *puiArg, unsigned int uiArgCount, 
                           char **ppszArgs)
{
   NOREF(puiArg);
   NOREF(uiArgCount);
   NOREF(ppszArgs);

    if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_FILENAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   strncpy(tySI.szRegisterFile, ppszArgs[*puiArg], sizeof(tySI.szRegisterFile)-1);
   tySI.szRegisterFile[sizeof(tySI.szRegisterFile)-1] = '\0';

   /* Parse user specified register file */
   (void)ParseUserRegFile();

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionConnection
     Engineer: Sandeep V L
        Input: None
       Output: None
  Description: Specifies the connection interface, either USB or Ethernet.
Date           Initials    Description
21-Oct-2009    SVL         Initial
****************************************************************************/
static int OptionConnection(unsigned int *puiArg, unsigned int uiArgCount, 
                           char **ppszArgs)
{
   NOREF(puiArg);
   NOREF(uiArgCount);
   NOREF(ppszArgs);

    if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_CONN_INTERFACE_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   /* TODO:  */

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionProbe
     Engineer: Sandeep V L
        Input: None
       Output: None
  Description: Specifies the debug probe being used.
Date           Initials    Description
21-Oct-2009    SVL         Initial
****************************************************************************/
static int OptionProbe(unsigned int *puiArg, unsigned int uiArgCount, 
                           char **ppszArgs)
{
   NOREF(puiArg);
   NOREF(uiArgCount);
   NOREF(ppszArgs);

   if (((*puiArg)+1) >= uiArgCount)
   {
      sprintf(tySI.szError, GDB_EMSG_PROBE_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
   }
   (*puiArg)++;
   /* TODO:  */

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionTotalCores
     Engineer: Sandeep V L
        Input: None
       Output: None
  Description: Specifies the total number of cores in a target.
Date           Initials    Description
21-Oct-2009    SVL         Initial
****************************************************************************/
static int OptionTotalCores(unsigned int *puiArg, unsigned int uiArgCount, 
                           char **ppszArgs)
{
   NOREF(puiArg);
   NOREF(uiArgCount);
   NOREF(ppszArgs);

   if (((*puiArg)+1) >= uiArgCount)
   {
      sprintf(tySI.szError, GDB_EMSG_TOTCORES_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
   }
   (*puiArg)++;
   /* TODO: */

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionCoresightBaseAddress
     Engineer: Suraj S
        Input: *piArg    : pointer to index of next arg
               iArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Get the Coresight Base Address for coresight compliant devices
Date           Initials    Description
29-Apr-2010    SJ         initial
*****************************************************************************/
static int OptionCoresightBaseAddress(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulCoresightBaseAddr = 0;
   int  bValid         = FALSE;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_CORESIGHT_BASE_ADDR_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulCoresightBaseAddr = StrToUlong(ppszArgs[*puiArg], &bValid);

   if ((!bValid) ||(ulCoresightBaseAddr>0xFFFFFFFF))
      {
      sprintf(tySI.szError, GDB_EMSG_CORESIGHT_BASE_ADDR_INVALD, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }

   tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulCoresightBaseAddr  = ulCoresightBaseAddr;
   PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_CORESIGHT_BASE_ADDR, ulCoresightBaseAddr);
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionDebugAPIndex
     Engineer: Suraj S
        Input: *piArg    : pointer to index of next arg
               iArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Get the Debug AP Index for coresight compliant devices
Date           Initials    Description
29-Apr-2010    SJ         initial
*****************************************************************************/
static int OptionDebugAPIndex(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulDebugAPIndex = 0;
   int  bValid         = FALSE;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_DEBUG_AP_INDEX_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulDebugAPIndex = StrToUlong(ppszArgs[*puiArg], &bValid);

   if ((!bValid) ||(ulDebugAPIndex>0xFF))
      {
      sprintf(tySI.szError, GDB_EMSG_DEBUG_AP_INDEX_INVALD, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }

   tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulDbgApIndex  = ulDebugAPIndex;
   PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_DEBUG_AP_INDEX, ulDebugAPIndex);
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: OptionMemoryAPIndex
     Engineer: Suraj S
        Input: *piArg    : pointer to index of next arg
               iArgCount : count of args in the list
               **ppszArgs: list of args
       Output: int - error code
  Description: Get the Memory AP Index for coresight compliant devices
Date           Initials    Description
29-Apr-2010    SJ         initial
*****************************************************************************/
static int OptionMemoryAPIndex(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg      !=NULL);
   assert(ppszArgs    !=NULL);
   unsigned long ulMemAPIndex = 0;
   int  bValid         = FALSE;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_MEM_AP_INDEX_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   ulMemAPIndex = StrToUlong(ppszArgs[*puiArg], &bValid);

   if ((!bValid) ||(ulMemAPIndex>0xFF))
      {
      sprintf(tySI.szError, GDB_EMSG_MEM_AP_INDEX_INVALD, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }

   tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulMemApIndex  = ulMemAPIndex;
   PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_MEM_AP_INDEX, ulMemAPIndex);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
	  Function: OptionJtagAPIndex
	  Engineer: Suraj S
		  Input: *piArg    : pointer to index of next arg
					iArgCount : count of args in the list
					**ppszArgs: list of args
		 Output: int - error code
  Description: Get the Jtag AP Index for coresight compliant devices
Date           Initials    Description
26-Mar-2011    ADK        initial
*****************************************************************************/
static int OptionJtagAPIndex(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
	assert(puiArg      !=NULL);
	assert(ppszArgs    !=NULL);
	unsigned long ulJtgAPIndex = 0;
	int  bValid         = FALSE;

	if ((*puiArg)+1 >= uiArgCount)
		{
		sprintf(tySI.szError, GDB_EMSG_JTAG_AP_INDEX_MISSING, ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
		}
	(*puiArg)++;

	ulJtgAPIndex = StrToUlong(ppszArgs[*puiArg], &bValid);

	if ((!bValid) ||(ulJtgAPIndex>0xFF))
		{
		sprintf(tySI.szError, GDB_EMSG_JTAG_AP_INDEX_INVALD, ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
		}

	tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulJtagApIndex  = ulJtgAPIndex;
	PrintMessage(INFO_MESSAGE, GDB_MSG_SETTING_JTAG_AP_INDEX, ulJtgAPIndex);
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: TranslateName
     Engineer: Rejeesh S Babu
        Input: char *pszDst - storage for translated name
               char *pszSrc - name to translate
               unsigned int uiSize - size of destination storage
       Output: char * - copy of pszDst
  Description: Translate device names to be command line option compatible
Date           Initials    Description
23-Mar-2008      RS         initial 
*****************************************************************************/
static char *TranslateName(char *pszDst, char *pszSrc, unsigned int uiSize)
{
   char *pszTemp;
   assert(pszDst != NULL);
   assert(pszSrc != NULL);
   assert(uiSize > 0);
   strncpy(pszDst, pszSrc, uiSize-1);
   pszDst[uiSize-1] = '\0';
   StringToLwr(pszDst);
   // first convert anything unusual to '-'
   pszTemp = pszDst;
   while (*pszTemp)
      {
      if (((*pszTemp < 'a') || (*pszTemp > 'z'))  && ((*pszTemp < '0') || (*pszTemp > '9')))
         *pszTemp = '-';
      pszTemp++;
      }
   // remove any leading '-'
   while (*pszDst == '-')
      strcpy(pszDst, pszDst+1);
   // remove any trailing '-'
   pszTemp = pszDst + strlen(pszDst) -1;
   while ((pszTemp > pszDst) && (*pszTemp == '-'))
      *pszTemp-- = '\0';
   // remove any duplicate '-'
   pszTemp = pszDst;
   while (*pszTemp)
      {
      if ((pszTemp[0] == '-') && (pszTemp[1] == '-'))
         strcpy(pszTemp, pszTemp+1);
      else
         pszTemp++;
      }
   // remove leading 'philips-'
   if (strncmp(pszDst, "philips-", 8) == 0)
      strcpy(pszDst, pszDst+8);
   // remove '-core' if present
   if ((pszTemp = strstr(pszDst, "-core")) != NULL)
      strcpy(pszTemp, pszTemp+5);
   // change '-pin' to 'pin'  (e.g. -14-pin => -14pin)
   if ((pszTemp = strstr(pszDst, "-pin")) != NULL)
      strcpy(pszTemp, pszTemp+1);
   return pszDst;
}
/****************************************************************************
     Function: HelpDevice
     Engineer: Rejeesh S Babu
        Input: none
       Output: none
  Description: Additional help for device option
Date           Initials    Description
23-Mar-2008      RS         initial 
*****************************************************************************/
static void HelpDevice(void)
{
   TyXMLNode *ptyNode;
   char *pszDevice;
   char szBuffer[_MAX_PATH];
   char *szName;
   ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Processor");
   while (ptyNode != NULL)
      {
      if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
         {
         szName=TranslateName(szBuffer,pszDevice, sizeof(szBuffer));
         #ifndef __LINUX
         (void)strupr(szName);
         #endif
         PrintMessage(HELP_MESSAGE, "%*s  %s\n", GDBSERV_HELP_COL_WIDTH, " ",szName );
         }
      ptyNode = XML_GetNextNode(ptyNode);
      }
}
/****************************************************************************
     Function: HelpReset
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for reset option
Date           Initials    Description
9-Apr-2008      VK        initial 
*****************************************************************************/
static void HelpReset(void)
{
  
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","0 No hard reset (default).");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","1 Hard reset and delay n seconds.");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","2 Hard reset and halt at reset vector.");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","3 Hot plug (connect to running target).");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","4 Core only( Supported only for ARMv7M and above)");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","5 Core and peripherals (Supported only for ARMv7M and above)");
}
/****************************************************************************
     Function: HelpEndianess
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for Endianess option
Date           Initials    Description
9-Apr-2008      VK        initial 
*****************************************************************************/
static void HelpEndian (void)
{
  
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ","<endianess> is: ");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","Little (default).");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","Big.");
}
/****************************************************************************
     Function: HelpTargetVoltage
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for fixed target option
Date           Initials    Description
9-Apr-2008      VK        initial 
*****************************************************************************/
static void HelpTargetVoltage(void)
{
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ","<voltage> is:");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","1.2");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","1.8");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","2.5");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","3.3");
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH+2, " ","track (default)");
}
/****************************************************************************
     Function: HelpJtagFrequency
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for frequency selection
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static void HelpJtagFrequency(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {
      PrintMessage(HELP_MESSAGE, "%*s<freq> for Opella-XD can be in range\n", GDBSERV_HELP_COL_WIDTH, " ");
      PrintMessage(HELP_MESSAGE, "%*sfrom 1kHz to 100MHz (default 1MHz) or RTCK.\n", GDBSERV_HELP_COL_WIDTH, " ");
      }
}



