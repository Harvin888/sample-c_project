/****************************************************************************
       Module: gdbxmlcfgscarm.c
     Engineer: Rejeesh S Babu
  Description: Ashling GDB Server, contains needed resources to parse 
               scan chain structure from XML file.
Date           Initials    Description
31-Mar-2008      RS         initial
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"


//extern TyProcFamGrp CFG_FamilyGroup(char * pszName);
extern TyServerInfo tySI;


/****************************************************************************
     Function: CFG_ProcessorNode
     Engineer: Rejeesh S Babu
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
31-Mar-2008    RS          initial
31-Dec-2009    SVL         Added null check for 'IsARMCore'
*****************************************************************************/
void CFG_ScanChainNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode      *ptyNode;
   unsigned long  ulIndex=0;
   char *pszState = NULL;
   // Process child nodes
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Core")) != NULL)
      {
      do
         {   
         ulIndex = XML_GetDec(XML_GetAttrib(ptyNode,"Number"),0);

         if (ulIndex > MAX_CORES_IN_CHAIN)
            break;

         pszState = XML_GetAttrib(ptyNode,"IsARMCore");
         /* If IsARMCore information is not provided in the scan-file, 
         it will be treated as 'Not an ARM core' by default */
         if(pszState != NULL)
             {
             if (strcasecmp(pszState, "true") == 0)
                {
                tySI.tyProcConfig.Processor._ARM.tyScanChainDef[ulIndex].bIsARMCore = 1; //lint !e661
                }
             else
                {
                tySI.tyProcConfig.Processor._ARM.tyScanChainDef[ulIndex].bIsARMCore = 0; //lint !e661
                }
             }
         else
             {
             tySI.tyProcConfig.Processor._ARM.tyScanChainDef[ulIndex].bIsARMCore = 0; //lint !e661
             }


         tySI.tyProcConfig.Processor._ARM.tyScanChainDef[ulIndex].ulIRlength =    
            XML_GetChildElementDec(ptyNode,"IRlength",0);   //lint !e661

         tySI.tyProcConfig.Processor._ARM.tyScanChainDef[ulIndex].ulBypassInst =  
            XML_GetChildElementHex(ptyNode,"BypassInst",0); //lint !e661

         }while((ptyNode=XML_GetNextNode(ptyNode)) != NULL);
      }
   tySI.tyProcConfig.Processor._ARM.uiNumberofCores=ulIndex;
   
}
   

