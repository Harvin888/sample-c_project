/***************************************************************************
       Module: gdboptarm.h
     Engineer: Rejeesh S Babu/Venkitakrishnan K
  Description: 
Date           Initials    Description    
31-Mar-2008    RS/VK        Initial
09-Apr-2008    VK           Moved Function Declarations for pfHelpFunc
29-Apr-2010    SJ           Support for Coresight devices
****************************************************************************/
#if !defined(GDBOPTARM)

#define GDBOPTARM 



//typedef unsigned short  uint16;
//typedef signed short    sint16;
typedef unsigned int    uint;
typedef signed int      sint;




typedef unsigned long  uint32;
typedef long           sint32;
typedef float          float16;
typedef float          float32; 
typedef char *         string ;
typedef unsigned char  ubyte ;
typedef char           sbyte ;
typedef unsigned char  uchar ;
typedef char           schar ;
typedef long           slong;
typedef unsigned long  ulong;
typedef ubyte          uint8 ;
typedef sbyte          sint8 ;
typedef uint8          bool8 ; 
//typedef uint16         bool16 ;
typedef uint32         bool32 ;




// TyError is a signed 32 bit interger no matter who asks
typedef sint32         TyError;



//typedef uint16    segtyp;                  
typedef uint8     dttype;
//typedef uint16    word;


#ifndef  __cplusplus
      typedef int     bool;
      #endif
      #ifndef _WINDEF_
      typedef int     BOOL;
      #endif
      #ifdef __LINUX
      typedef int     BOOL;
      #endif

#define GDB_MSG_TAP_NUM_CHOSEN                  "TAP number(s) chosen:"
#define GDB_MSG_SETTING_CACHE_ADDR              "Setting cache flush routine to 0x%08lX."
#define GDB_MSG_SETTING_SAFE_NON_VECTOR_ADDR    "Setting safe non-vector address to 0x%08lX."
#define GDB_MSG_DEBUG_MSG_To_STDOUT             "Displaying debug messages to standard output."
#define GDB_MSG_READING_CMD_LINE_OPTN           "Reading command line options from file %s."
#define GDB_MSG_LOGGING_DEBUG_MSG               "Logging debug messages to file '%s'."
#define GDB_MSG_SETTING_ENTRYPOINT_ADDR         "Setting program entry point (PC) to 0x%08lX."
#define GDB_MSG_SETTING_CORESIGHT_BASE_ADDR     "Setting Coresight Base Address to 0x%08lX."
#define GDB_MSG_SETTING_DEBUG_AP_INDEX          "Setting Debug AP Index to 0x%08lX."
#define GDB_MSG_SETTING_MEM_AP_INDEX            "Setting Memory AP Index to 0x%08lX."
#define GDB_MSG_SETTING_JTAG_AP_INDEX           "Setting Jtag AP Index to 0x%08lX."
#define GDB_MSG_SETTING_MEM_AP_MEM_ACC          "Setting Memory AP Mem Access to 0x%08lX."
#endif // GLOBAL_SIMPLE_H__19052004__INCLUDED_

