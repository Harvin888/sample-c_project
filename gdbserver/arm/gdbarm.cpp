/****************************************************************************
       Module: gdbarm.cpp
     Engineer: Rejeesh S Babu/Venkitakrishnan K
  Description: Ashling GDB Server, ARM target interface
Date           Initials    Description
31-MAR-2008    RS/VK         initial
23-Sep-2009    SVL         Added support for 'PFXD mode'
05-Oct-09      SVL         Added support for deleting individual watchpoint
16-Oct-2009    SVL         Added support for vector catch
19-Oct-2009    SVL         Support for forcefully setting breakpoint types
29-Apr-2010    SJ          Added Support for Coresight devices, 
                           Corrections in Function headers
20-May-2010    SJ          Modified Wathcpoint and Breakpoint handling for ARM.
                           Introduced Linked List for storing BP/WP details.
                           Added proper comments in functions.
****************************************************************************/
#if _WINCONS
    #include <windows.h>
    #include <conio.h>
    #include <windef.h>
#elif __LINUX
    #include <sys/ioctl.h>
    #include <termios.h>
    #include <dlfcn.h>
    #include <unistd.h>
#endif
#include <stdio.h>
#if _WINCONS
    #include <stdlib.h>
#endif
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbopt.h"
#include "gdbserver/gdbserv.h"
#include "gdbarm.h"
#include "gdbserver/moncommand/gdbmonitor.h"
#include "gdboptarm.h"
#include "../../protocol/rdi/rdi/rdi150.h"
#include "../../protocol/rdi/opxdarmconfigmgr/OpellaConfig.h"
#include "../../protocol/rdi/rdi/rdi_err.h"
#include "../../protocol/rdi/rdi/rdi_info.h"
#include "../../protocol/rdi/rdi/rdi_hif.h"


#if _WINCONS
    #include "../../protocol/rdi/rdi/winrdi.h"
#elif __LINUX
    #include "../../protocol/rdi/rdi/sordi.h"
#endif
#include "../../protocol/rdi/rdi/rdi_prop.h"

#ifdef _WINCONS
    #define  OPELLAXD_DLL_NAME                "opxdrdi.dll"
#elif __LINUX
    #define  OPELLAXD_DLL_NAME                "./libopxdrdi.so"
    #define  LoadLibrary(lib)                 dlopen(lib, RTLD_NOW)
    #define  GetProcAddress(handle, proc)     dlsym(handle, proc)
    #define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#endif

//REGISTER MASKS
#define     R0_MASK      0x1
#define     R1_MASK      0x2
#define     R2_MASK      0x4
#define     R3_MASK      0x8
#define     R4_MASK      0x10
#define     R5_MASK      0x20
#define     R6_MASK      0x40
#define     R7_MASK      0x80
#define     R8_MASK      0x100
#define     R9_MASK      0x200
#define     R10_MASK     0x400
#define     R11_MASK     0x800
#define     R12_MASK     0x1000
#define     R13_MASK     0x2000
#define     R14_MASK     0x4000
#define     R15_MASK     0x10000  
#define     R16_MASK     0x20000
#define     R17_MASK     0x40000

/* Added for CortexM3 */
#define     xPSR_MASK           0x00020000
#define     APSR_MASK           0x00040000
#define     IPSR_MASK           0x00080000
#define     EPSR_MASK           0x00100000
#define     PRIMASK             0x01000000
#define     FAULTMASK           0x02000000
#define     BASEPRI_MASK        0x04000000
//#define     BASEPRI_MAX_MASK    0x8000000
#define     CONTROL_MASK        0x10000000
#define     MSP_MASK            0x20000000
#define     PSP_MASK            0x40000000

//processor modes
#define  USER         0x10
#define  FIQ          0x11
#define  IRQ          0x12
#define  SUPERVISOR   0x13
#define  ABORT        0x17
#define  UNDEF        0x1b
#define  CURRENT      0xFF

TyARMReg ptyARMReg[] = {
    {(char*)"R0",           (int)0x00},
    {(char*)"R1",           (int)0x01},
    {(char*)"R2",           (int)0x02},
    {(char*)"R3",           (int)0x03},
    {(char*)"R4",           (int)0x04},
    {(char*)"R5",           (int)0x05},
    {(char*)"R6",           (int)0x06},
    {(char*)"R7",           (int)0x07},
    {(char*)"R8",           (int)0x08},
    {(char*)"R9",           (int)0x09},
    {(char*)"R10",          (int)0x0A},
    {(char*)"R11",          (int)0x0B},
    {(char*)"R12",          (int)0x0C},
    {(char*)"SP",           (int)0x0D},
    {(char*)"LR",           (int)0x0E},
    {(char*)"PC",           (int)0x0F},
    {(char*)"FPS",          (int)0x18},
    {(char*)"CPSR",         (int)0x19},

    {(char*)"R13_SVC",      (int)0x26},
    {(char*)"R14_SVC",      (int)0x1A},
    {(char*)"SPSR_SVC",     (int)0x1B},
    {(char*)"R13_ABT",      (int)0x1C},
    {(char*)"R14_ABT",      (int)0x1D},
    {(char*)"SPSR_ABT",     (int)0x1E}, 
    {(char*)"R13_IRQ",      (int)0x1F},
    {(char*)"R14_IRQ",      (int)0x20},
    {(char*)"SPSR_IRQ",     (int)0x21},
    {(char*)"R13_UND",      (int)0x22},
    {(char*)"R14_UND",      (int)0x23},
    {(char*)"SPSR_UND",     (int)0x24},
    {(char*)"R8_FIQ",       (int)0x11},
    {(char*)"R9_FIQ",       (int)0x12},
    {(char*)"R10_FIQ",      (int)0x13},
    {(char*)"R11_FIQ",      (int)0x14},
    {(char*)"R12_FIQ",      (int)0x15},
    {(char*)"R13_FIQ",      (int)0x16},
    {(char*)"R14_FIQ",      (int)0x17},
    {(char*)"SPSR_fIQ",     (int)0x10}
};

TyARMReg ptyCortexM3Reg[] = {
    {(char*)"R0",           (int)0x00},
    {(char*)"R1",           (int)0x01},
    {(char*)"R2",           (int)0x02},
    {(char*)"R3",           (int)0x03},
    {(char*)"R4",           (int)0x04},
    {(char*)"R5",           (int)0x05},
    {(char*)"R6",           (int)0x06},
    {(char*)"R7",           (int)0x07},
    {(char*)"R8",           (int)0x08},
    {(char*)"R9",           (int)0x09},
    {(char*)"R10",          (int)0x0A},
    {(char*)"R11",          (int)0x0B},
    {(char*)"R12",          (int)0x0C},
    {(char*)"SP",           (int)0x0D},
    {(char*)"LR",           (int)0x0E},
    {(char*)"PC",           (int)0x0F},
    {(char*)"xPSR",          (int)0x30},
    {(char*)"APSR",         (int)0x31},
    {(char*)"IPSR",         (int)0x32},
    {(char*)"EPSR",         (int)0x33},

    {(char*)"PRIMASK",      (int)0x34},
    {(char*)"FAULTMASK",    (int)0x35},
    {(char*)"BASEPRI",      (int)0x36},
    {(char*)"CONTROL",      (int)0x37},
    {(char*)"MSP",          (int)0x38},
    {(char*)"PSP",          (int)0x39}
};

#define  MAX_ARM_REG          ((sizeof(ptyARMReg))/(sizeof(TyARMReg)))
#define  MAX_CORTEXM3_REG     ((sizeof(ptyCortexM3Reg))/(sizeof(TyARMReg)))

typedef struct _TyBreakpointElement
    {
    unsigned long ulBpType;
    unsigned long Address;
    unsigned long ulIdentifier;
    unsigned long ulDataType;
    unsigned long ulDataValue;
    } TyBreakpointElement;

struct _TyBreakpointElementInfo
    {
    TyBreakpointElement tyBreakpointElement;
    struct _TyBreakpointElementInfo *next;
    };

typedef struct _TyBreakpointElementInfo TyBreakpointElementInfo;

/* Pointer to the list of breakpoint element list */
TyBreakpointElementInfo *ptyBreakpointElementList = NULL;

extern int RemoteDataPresent(int iConnectionIndex);
extern unsigned long             ulGSemiState;
extern TyServerInfo              tySI;    
extern JTAGClockFreqMode         eJTAGClockFreqMode;
extern TyStopSignal              tyGlobalStopSignal;
extern unsigned long             gulPC;
extern char                      filename[_MAX_PATH];

unsigned long                             ulGlobalCoreNo                            =0;
unsigned long                             ulGlobalCoreNoTemp                        =0;
unsigned int                              uiErrStore                                =0;            
unsigned int                              uiGExError                                =TRUE;                                           
unsigned int                              uiGClearHandle                            =0;
unsigned long                             ulNoOfWPs                                 =0;  
char                                      szGOutPacket[GDBSERV_PACKET_BUFFER_SIZE];
static char                               szTargetDllName[_MAX_PATH]                = {0};

static  ARMword EndianessType            = LITTLE_ENDIAN_MASK;
static RDI_AgentHandle                   tyGlobalAgent;
static RDI_ModuleDesc                    tyGlobalArmModule[5];
static RDI150_ProcVec                    *pRDIAgentVectorTable;
static const RDI150_ProcVec              *pRDIModuleVectorTable;

/* For semi-hosting support, made global for PFXD mode */
static struct RDI_HostosInterface        tyGlobalhostif;

TyDeviceInfo                              tyDeviceInfo;
static HINSTANCE                          hGlobalInstRdiDll                         = NULL;
TySetJTAGFrequency                        pfSetJTAGFreq;
TyAshScanIR                               pfAshScanIR;
TyAshScanDR                               pfAshScanDR;      
TyAshScanIRScanDR                         pfAshScanIRScanDR;
TyResetTAP                                pfResetTAP;
TyPrepareforJTAGConsole                   pfPrepareforJTAGConsole;
TyProcessCP15Register                     pfProcessCP15Register;
TyReadCPRegister                          pfReadCPRegister;
TyWriteCPRegister                         pfWriteCPRegister;
/* For Cortex-M3 Support */
unsigned int gbCortexM3 = FALSE;

// local function prototypes

TyError         OpenProcessorModule      (void);
static void     GetDatabaseFilename      (char *pszPathName);
char *          LocRDI_Hif_GetS          (RDI_Hif_HostosArg *arg, char *buffer, int len);
int             LocRDI_Hif_ReadC         (RDI_Hif_HostosArg *arg);
void            LocRDI_Hif_WriteC        (RDI_Hif_HostosArg *arg, int c);
int             LocRDI_Hif_Write         (RDI_Hif_HostosArg *arg,char const *buffer,int len);
void            LOW_HaltProcessorManually(void *punused);
static bool     GetModeAndMaskForReg     (unsigned long *ulRegister, unsigned long *pMode,unsigned long *pRegMask);
static void     SetupErrorMessage        (TyError tyError);
char*           GetRegName               (unsigned long ulGdbRegNumber);
static TyError  ARMFreeLibrary           (void);
static char*    GetBpTypeName            (unsigned long ulBpType);
static int      AddBpInfoToList          (TyBreakpointElement *tyBreakpointElement);
static void     RemoveAllBPInfoFromList  (void);
static int      RemoveBPInfoFromList     (unsigned long ulIdentifier);
static int      IsAddressAlreadyInBPList (unsigned long ulAddress);
static int      GetBPHandleFromList      (unsigned long ulAddress, unsigned long *pulIdentifier);
static int      GetWPHandleFromList      (unsigned long ulAddress, unsigned long ulData, unsigned int uiWPType, unsigned long *pulIdentifier);
static int      GetWPHandlesFromList     (unsigned long *pulIdentifier, unsigned long *pulWPCnt);
void            ConvertVersionString     (unsigned long ulVersion, char *pszVersionString);
static int SetupRegisterSettings(void);
/* Loads the RDI library, when started in PFXD mode */
static int RDI_LoadLibrary(void);
/* Connects the debug controller to debug target through 'monitor connect', 
when started in PFXD mode */
static int RDI_ConnectPFXD(void);

/****************************************************************************
    Function    : OpenProcessorModule
    Engineer    : Rejeesh S Babu
    Input       : void  
    Output      : TyError
    description : Warm Boot
Date           Initials    Description
20-Apr-08        RS          Initial
****************************************************************************/
TyError OpenProcessorModule(void)
{
    TyError ErrRet;
    unsigned int type = WARM_BOOT|EndianessType;
    ErrRet = pRDIModuleVectorTable->open(tyGlobalArmModule[ulGlobalCoreNo].handle, type, NULL,NULL, NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        }
    return NO_ERROR;

}

/****************************************************************************
    Function    : LocRDI_Hif_ReadC
    Engineer    : Rejeesh S Babu
    Input       : *arg:Semihost Handle   
                  *buffer:pointer to store the data.
                   len   :number of bytes to read.
    Output      : data from console
    description : Read data from console
Date           Initials    Description
20-Apr-08        RS          Initial
****************************************************************************/
char * LocRDI_Hif_GetS(RDI_Hif_HostosArg *arg, char *buffer, int len)
{
    NOREF(arg);
    (void)fgets(buffer, len, stdin);
    return buffer;
}

/****************************************************************************
    Function    : LocRDI_Hif_WriteC
    Engineer    : Rejeesh S Babu
    Input       : int c:Charecter to display   
                  *arg:Semihost Handle
    Output      : Void
    description : Display a single character to standard debug controller output
Date           Initials    Description
20-Apr-08        RS          Initial
****************************************************************************/
void LocRDI_Hif_WriteC(RDI_Hif_HostosArg *arg, int c)
{
    NOREF(arg);
    putchar(c);
}

/****************************************************************************
    Function    : LocRDI_Hif_ReadC
    Engineer    : Rejeesh S Babu
    Input       : *arg:Semihost Handle   
    Output      : data from console
    description : Read a char from console
Date           Initials    Description
20-Apr-08        RS          Initial
****************************************************************************/
int LocRDI_Hif_ReadC(RDI_Hif_HostosArg *arg)
{
    char a;   
    NOREF(arg);
    a=(char)getchar();
    return a;
}

/****************************************************************************
    Function    : LocRDI_Hif_Write
    Engineer    : Rejeesh S Babu
    Input       : *arg   :  Semihost Handle
                  *buffer:  pointer to write data
                  len    :  number of bytes to write
    Output      : NO_ERROR
    Description : Write string to Console
Date           Initials    Description
20-Apr-08        RS          Initial
****************************************************************************/
int LocRDI_Hif_Write(RDI_Hif_HostosArg *arg, char const *buffer, int len)
{
    NOREF(arg);
    NOREF(len);
    fputs(buffer, stdout);
    return NO_ERROR;
}

/****************************************************************************
    Function    : HaltProcessorManually
    Engineer    : Rejeesh S Babu
    Input       : *punused: void pointer from RDI
    Output      : NULL
    description : Halt the processor incase of no response from RDI
Date           Initials    Description
20-Apr-08        RS          Initial
****************************************************************************/
void LOW_HaltProcessorManually(void *punused)
{

    TyError      ErrRet;
    NOREF(punused);
    if(KeyHit())
        {  // user hit some key, he may want to quit GDB server ...
        PrintMessage(INFO_MESSAGE, "Halting processor.");
        ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDISignal_Stop,(unsigned long *)&ulGlobalCoreNo,NULL);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            }
        tyGlobalStopSignal=TARGET_SIGINT;
#ifdef __LINUX
        usleep(100* 1000);
#endif
#ifdef _WINCONS
        Sleep(100);
#endif         
        }
    if(RemoteDataPresent(0))//0 as argument as there is no Multicore
        {
        // user hit some key, he may want to quit GDB server ...
        PrintMessage(INFO_MESSAGE, "Halting processor.");
        ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDISignal_Stop,(unsigned long *)&ulGlobalCoreNo,NULL);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            }
        tyGlobalStopSignal=TARGET_SIGINT;
#ifdef __LINUX
        usleep(100* 1000);
#endif
#ifdef _WINCONS
        Sleep(100);
#endif
        }

}

/****************************************************************************
     Function: GetModeAndMaskForReg
     Engineer: Venkitakrishnan K
        Input: *ulRegister:Pointer to register no:
               *pMode     :Pointer to store Register Mode
               *pRegMask  :Pointer to store Regster Mask
       Output: bool       :TRUE:found register
                           FALSE:No register found
  Description: Returns register mode and a mask for the register no:
Date           Initials    Description
31-Mar-08       VK          Initial
10-Nov-09       DVA         Added CortexM3 registers.
29-Mar-10       SVL         Bugfix for reading registers depending on the 
                            current processor mode
*****************************************************************************/
static bool GetModeAndMaskForReg(unsigned long *ulRegister, unsigned long *pMode,unsigned long *pRegMask)
{
    assert(ulRegister !=NULL);
    assert(pMode      !=NULL);
    assert(pRegMask   !=NULL);
    unsigned long pulRegisterMasks[] = {R0_MASK,
        R1_MASK,
        R2_MASK,
        R3_MASK,
        R4_MASK,
        R5_MASK,
        R6_MASK,
        R7_MASK,
        R8_MASK,
        R9_MASK,
        R10_MASK,
        R11_MASK,
        R12_MASK,
        R13_MASK,
        R14_MASK,
        R15_MASK};

    if(*ulRegister <= (unsigned long) REG_ARM_R15)
        {
        /* For correct register values R8-R14 in all modes */
        *pMode    = CURRENT;
        *pRegMask = pulRegisterMasks[*ulRegister];
        }
    else
        {
        switch(*ulRegister)
            {
            case  REG_ARM_CPSR : 
                *pMode=USER;
                *pRegMask=R16_MASK;
                break;
            case  REG_ARM_FPS :
                *pMode=0;
                *pRegMask=0;
                break;
            case  REG_ARM_R8_FIQ  :
                *pMode=FIQ;
                *pRegMask=R8_MASK;
                break;
            case  REG_ARM_R9_FIQ  : 
                *pMode=FIQ;
                *pRegMask=R9_MASK;
                break;
            case  REG_ARM_R10_FIQ :  
                *pMode=FIQ;
                *pRegMask=R10_MASK;
                break;
            case  REG_ARM_R11_FIQ :
                *pMode=FIQ;
                *pRegMask=R11_MASK;
                break;
            case  REG_ARM_R12_FIQ :    
                *pMode=FIQ;
                *pRegMask=R12_MASK;
                break;
            case  REG_ARM_R13_FIQ :
                *pMode=FIQ;
                *pRegMask=R13_MASK;
                break;
            case  REG_ARM_R14_FIQ :  
                *pMode=FIQ;
                *pRegMask=R14_MASK;
                break;
            case  REG_ARM_SPSR_FIQ :
                *pMode=FIQ;
                *pRegMask=R17_MASK;
                break;
            case  REG_ARM_R13_SVC :  
                *pMode=SUPERVISOR;
                *pRegMask=R13_MASK;
                break;
            case  REG_ARM_R14_SVC :
                *pMode=SUPERVISOR;
                *pRegMask=R14_MASK;
                break;
            case  REG_ARM_SPSR_SVC :  
                *pMode=SUPERVISOR;
                *pRegMask=R17_MASK;
                break;
            case  REG_ARM_R13_ABT :
                *pMode=ABORT;
                *pRegMask=R13_MASK;
                break;
            case  REG_ARM_R14_ABT :  
                *pMode=ABORT;
                *pRegMask=R14_MASK;
                break;
            case  REG_ARM_SPSR_ABT :
                *pMode=ABORT;
                *pRegMask=R17_MASK;
                break;
            case  REG_ARM_R13_IRQ :  
                *pMode=IRQ;
                *pRegMask=R13_MASK;
                break;
            case  REG_ARM_R14_IRQ :
                *pMode=IRQ;
                *pRegMask=R14_MASK;
                break;
            case  REG_ARM_SPSR_IRQ :  
                *pMode=IRQ;
                *pRegMask=R17_MASK;
                break;
            case  REG_ARM_R13_UND :
                *pMode=UNDEF;
                *pRegMask=R13_MASK;
                break;
            case  REG_ARM_R14_UND :  
                *pMode=UNDEF;
                *pRegMask=R14_MASK;
                break;
            case  REG_ARM_SPSR_UND :
                *pMode=UNDEF;
                *pRegMask=R17_MASK;
                break;
            case REG_CORTEXM3_xPSR :
                *pMode=CURRENT;
                *pRegMask=xPSR_MASK;
                break; 
            case REG_CORTEXM3_APSR :
                *pMode      = CURRENT;
                *pRegMask   = APSR_MASK;
                break;
            case REG_CORTEXM3_IPSR :
                *pMode      = CURRENT;
                *pRegMask   = IPSR_MASK;
                break;
            case REG_CORTEXM3_EPSR :
                *pMode      = CURRENT;
                *pRegMask   = EPSR_MASK;
                break;
            case REG_CORTEXM3_PRIMASK :
                *pMode      = CURRENT;
                *pRegMask   = PRIMASK;
                break;
            case REG_CORTEXM3_FAULTMASK :
                *pMode      = CURRENT;
                *pRegMask   = FAULTMASK;
                break;
            case REG_CORTEXM3_BASEPRI :
                *pMode      = CURRENT;
                *pRegMask   = BASEPRI_MASK;
                break;
                /*case REG_CORTEXM3_BASEPRI_MAX :
                    *pMode      = CURRENT;
                    *pRegMask   = BASEPRI_MAX_MASK;
                    break; */
            case REG_CORTEXM3_CONTROL :
                *pMode      = CURRENT;
                *pRegMask   = CONTROL_MASK;
                break;
            case REG_CORTEXM3_MSP :
                *pMode      = CURRENT;
                *pRegMask   = MSP_MASK;
                break;
            case REG_CORTEXM3_PSP :
                *pMode      = CURRENT;
                *pRegMask   = PSP_MASK;
                break;
            default :          
                return FALSE;
            }
        }

    return TRUE;

}  

/****************************************************************************
     Function: AddBpInfoToList
     Engineer: Suraj S
        Input: TyBreakpointElement tyBreakpointElement - Breakpoint element structure
               which contains info on the new BP/WP set
       Output: NO_ERROR, On success
               ERROR, On Failure
  Description: Add the details of the curent BP/WP to breakpoint element list  
Date           Initials    Description
20-May-2010      SJ        Initial
*****************************************************************************/
static int AddBpInfoToList(TyBreakpointElement *ptyBreakpointElement)
{
    TyBreakpointElementInfo *pNewNode = NULL;
    TyBreakpointElementInfo *pTempNode = NULL;

    pNewNode = (TyBreakpointElementInfo *)malloc(sizeof(TyBreakpointElementInfo));

    if(!pNewNode)
        {
        /* Memory allocation failure */
        PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
        return IDERR_ADD_BP_NO_MEMORY;
        }

    /* fill the node with BP/WP info */
    pNewNode->tyBreakpointElement = *ptyBreakpointElement;
    pNewNode->next = NULL;

    /* for the first node */
    if(ptyBreakpointElementList == NULL)
        {
        ptyBreakpointElementList = pNewNode;
        }

    else
        {
        /* New node is added to list as in a stack */
        pTempNode = ptyBreakpointElementList;
        while(pTempNode->next != NULL)
            {
            pTempNode = pTempNode->next;
            }
        pTempNode->next = pNewNode;
        }
    return NO_ERROR;
}

/****************************************************************************
     Function: GetBPHandleFromList
     Engineer: Suraj S
        Input: unsigned long ulAddress - Address of the required BP
               unsigned long *pulIdentifier - pointer to store the handle 
               of Breakpoint
       Output: NO_ERROR, On success
               ERROR, On Failure
  Description: Get the handle of the required BP from the breakpoint 
               element list  
Date           Initials    Description
20-May-2010      SJ        Initial
*****************************************************************************/
static int GetBPHandleFromList(unsigned long ulAddress, unsigned long *pulIdentifier)
{
    TyBreakpointElementInfo *pCurrNode = ptyBreakpointElementList;

    while(NULL != pCurrNode)
        {
        /*  Check for an address match. If the address matches, this is the 
            required BP handle. Check the Datatype also to ensure this address 
            is not that of a WP, since WP has a non-zero Datatype  */
        if((pCurrNode->tyBreakpointElement.Address == ulAddress) && 
           (pCurrNode->tyBreakpointElement.ulDataType == 0))
            {
            *pulIdentifier = pCurrNode->tyBreakpointElement.ulIdentifier;
            return NO_ERROR;
            }
        pCurrNode = pCurrNode->next;
        }
    return IDERR_REMOVE_BP_NOT_FOUND;
}

/****************************************************************************
     Function: GetWPHandleFromList
     Engineer: Suraj S
        Input: unsigned long ulAddress - Address of the required WP
               unsigned long ulData - Data of the required WP
               unsigned int uiWPType - Watcpoint type
               unsigned long *pulIdentifier - pointer to store the handle 
               of Watchpoint
       Output: NO_ERROR, On success
               ERROR, On Failure
  Description: Get the handle of the required WP from the breakpoint 
               element list  
Date           Initials    Description
21-May-2010      SJ        Initial
*****************************************************************************/
static int GetWPHandleFromList(unsigned long ulAddress, unsigned long ulData, unsigned int uiWPType, unsigned long *pulIdentifier)
{
    TyBreakpointElementInfo *pCurrNode = ptyBreakpointElementList;

    while(NULL != pCurrNode)
        {
        /*  Check for an address match if the WP type is WP_ADDR_TYPE. If the address matches, this is the 
            required WP handle. Check the Datatype also to ensure this handle 
            is not that of a BP, since BP has a Datatype of value 0 */
        if(WP_ADDR_TYPE == uiWPType)
            {
            if((pCurrNode->tyBreakpointElement.Address == ulAddress) && 
               (pCurrNode->tyBreakpointElement.ulDataType != 0))
                {
                *pulIdentifier = pCurrNode->tyBreakpointElement.ulIdentifier;
                return NO_ERROR;
                }
            pCurrNode = pCurrNode->next;
            }

        /*  Check for an data match if the WP type is WP_DATA_TYPE. If the data matches, this is the 
            required WP handle. Check the Datatype also to ensure this handle 
            is not that of a BP, since BP has a Datatype of value 0 */
        else if(WP_DATA_TYPE == uiWPType)
            {
            if((pCurrNode->tyBreakpointElement.ulDataValue == ulData) && 
               (pCurrNode->tyBreakpointElement.ulDataType != 0))
                {
                *pulIdentifier = pCurrNode->tyBreakpointElement.ulIdentifier;
                return NO_ERROR;
                }
            pCurrNode = pCurrNode->next;
            }

        /*  Check for both address match and data match if the WP type is WP_ADDR_DATA_TYPE. If a match occurs, this is the 
            required WP handle. Check the Datatype also to ensure this handle 
            is not that of a BP, since BP has a Datatype of value 0 */
        else if(WP_ADDR_DATA_TYPE == uiWPType)
            {
            if((pCurrNode->tyBreakpointElement.Address == ulAddress) &&
               (pCurrNode->tyBreakpointElement.ulDataValue == ulData) && 
               (pCurrNode->tyBreakpointElement.ulDataType != 0))
                {
                *pulIdentifier = pCurrNode->tyBreakpointElement.ulIdentifier;
                return NO_ERROR;
                }
            pCurrNode = pCurrNode->next;
            }
        }
    return IDERR_REMOVE_BP_NOT_FOUND;
}

/****************************************************************************
     Function: GetWPHandlesFromList
     Engineer: Suraj S
        Input: unsigned long *pulIdentifier - pointer to store the handles 
               of Watchpoint
               unsigned long ulWPCnt - Get the number of WP's currently set
       Output: NO_ERROR, On success
               ERROR, On Failure
  Description: Get the handle of all WP from the breakpoint element list
               and the count                 
Date           Initials    Description
25-May-2010      SJ        Initial
*****************************************************************************/
static int GetWPHandlesFromList(unsigned long *pulIdentifier, unsigned long *pulWPCnt)
{
    unsigned long ulWPCount = 0;
    TyBreakpointElementInfo *pCurrNode = ptyBreakpointElementList;

    while(NULL != pCurrNode)
        {
        /*  Check for all non-zero Datatypes in the BP list and 
        get all the handles of WP's currently set */
        if(pCurrNode->tyBreakpointElement.ulDataType != 0)
            {
            *pulIdentifier++ = pCurrNode->tyBreakpointElement.ulIdentifier;
            ulWPCount++;
            }
        pCurrNode = pCurrNode->next;
        }
    if(0 == ulWPCount)
        return IDERR_REMOVE_BP_NOT_FOUND;
    else
        {
        *pulWPCnt = ulWPCount;
        return NO_ERROR;
        }
}

/****************************************************************************
     Function: RemoveBPInfoFromList
     Engineer: Suraj S
        Input: unsigned long ulIdentifier - Handle of the BP/WP to be removed 
               from the list
       Output: NO_ERROR, On success
               ERROR, On Failure
  Description: Remove the details of given BP/WP from breakpoint element list  
Date           Initials    Description
20-May-2010      SJ        Initial
*****************************************************************************/
static int RemoveBPInfoFromList(unsigned long ulIdentifier)
{ 
    TyBreakpointElementInfo *pCurrNode = NULL;
    TyBreakpointElementInfo *pTempNode = NULL;

    pCurrNode = ptyBreakpointElementList;

    /* If the BP/WP list is empty return ERROR */
    if(NULL == pCurrNode)
        return IDERR_REMOVE_BP_NOT_FOUND;

    /*  Traverse through the BP/WP List    */
    while(NULL != pCurrNode)
        {
        /*  Check if the identifier matches */
        if(pCurrNode->tyBreakpointElement.ulIdentifier == ulIdentifier)
            {
            /*  Check if the first node matches */
            if(pCurrNode == ptyBreakpointElementList)
                {
                ptyBreakpointElementList = pCurrNode->next;                
                }
            else
                {
                if(NULL == pTempNode)
                    {
                    /* Do nothing. Just to handle compilation error */
                    }
                else 
                    {
                    pTempNode->next = pCurrNode->next;
                    }
                
                }
            /* Free the current node if a match occurs  */
            free(pCurrNode); 
            return  NO_ERROR;
            }
        else 
            {
            /* Point to the next element in the list    */
            pTempNode = pCurrNode;
            pCurrNode = pCurrNode->next;
            }
        }
    return IDERR_REMOVE_BP_NOT_FOUND;
}


/****************************************************************************
     Function: RemoveAllBPInfoFromList
     Engineer: Suraj S
        Input: void
       Output: NO_ERROR, On success
               ERROR, On Failure
  Description: Remove the details of all BP/WP's from breakpoint element list  
Date           Initials    Description
21-May-2010      SJ        Initial
*****************************************************************************/
static void RemoveAllBPInfoFromList(void)
{
    TyBreakpointElementInfo *pCurrNode = NULL;

    pCurrNode = ptyBreakpointElementList;

    /* free the BP element list */
    if(NULL != pCurrNode)
        free(pCurrNode);
}

/****************************************************************************
     Function: IsAddressAlreadyInBPList
     Engineer: Suraj S
        Input: unsigned long ulAddress - Breakpoint/Wathpoint Address
       Output: TRUE if a match is found
               -1, On Failure
  Description: Check if an address is already in the list for a given BP/WP 
Date           Initials    Description
20-May-2010     SJ         Initial
*****************************************************************************/
static int IsAddressAlreadyInBPList(unsigned long ulAddress)
{
    TyBreakpointElementInfo *pTempNode = NULL;

    /* Start from the first node */
    pTempNode = ptyBreakpointElementList;

    while (pTempNode != NULL) 
    {
        if(pTempNode->tyBreakpointElement.Address == ulAddress)
        {
            /* return the pointer to this node and the breakpoint type */
            return TRUE;
        }
        pTempNode = pTempNode->next;
    }
    return FALSE;
}

/****************************************************************************
     Function: SetupErrorMessage
     Engineer: Rejeesh S Babu
     Input   : tyError: Error Value
    Output   : none
  Description: Display Error Message
Date           Initials    Description
31-Mar-08        RS          Initial
*****************************************************************************/
static void SetupErrorMessage(TyError tyError)
{
    switch(tyError)
        {
        case IDERR_LOAD_DLL_FAILED:
            PrintMessage(ERROR_MESSAGE,  "Failed to load target library %s. File not found or is corrupt.", szTargetDllName);
            break;
        case IDERR_DLL_NOT_LOADED:
            PrintMessage(ERROR_MESSAGE,  "Internal server error, target library %s not loaded.", szTargetDllName);
            break;
        case IDERR_ADD_BP_NO_MEMORY:
            PrintMessage(ERROR_MESSAGE,  "Insufficient memory while setting a breakpoint.");
            break;
        case IDERR_ADD_BP_INTERNAL1:
            PrintMessage(ERROR_MESSAGE,  "Internal error 1 occurred while setting a breakpoint.");
            break;
        case IDERR_ADD_BP_DUPLICATE:
            PrintMessage(ERROR_MESSAGE,  "Duplicate breakpoint found at address while setting a breakpoint.");
            break;
        case IDERR_REMOVE_BP_INTERNAL1:
            PrintMessage(ERROR_MESSAGE,  "Internal error 1 occurred while deleting a breakpoint.");
            break;
        case IDERR_REMOVE_BP_INTERNAL2:
            PrintMessage(ERROR_MESSAGE,  "Internal error 2 occurred while deleting a breakpoint.");
            break;
        case IDERR_REMOVE_BP_NOT_FOUND:
            PrintMessage(ERROR_MESSAGE,  "Breakpoint not found, unable to delete a breakpoint.");
            break;
        case IDERR_ASH_SRCREGISTER:
            PrintMessage(LOG_MESSAGE,    "Read attempted from an unsupported register number.");
            break;
        case IDERR_ASH_DSTREGISTER:
            PrintMessage(LOG_MESSAGE,    "Write to an unsupported register number ignored.");
            break;
        case RDIError_BreakpointReached:
        case RDIError_UndefinedInstruction:
        case RDIError_TargetStopped:
        case RDIError_UserInterrupt:
        case RDIError_PrefetchAbort:
        case RDIError_DataAbort:
        case RDIError_AddressException:
        case RDIError_FIQ:
        case RDIError_WatchpointAccessed:
        case RDIError_BranchThrough0:
        case RDIError_SoftwareInterrupt:
        case RDIError_IRQ:
        case RDIError_TriggerOcuured:
        case RDIError_Executing:
            // These are actually 'cause of break' messages...       
            uiGExError=NO_ERROR;         
            break;
        case RDIError_Reset:
            PrintMessage(ERROR_MESSAGE, RDI_ERROR_RESET);
            break;
        case RDIError_TargetRunning:
        case RDIError_NotAllowedWhilstExecuting:
            PrintMessage(ERROR_MESSAGE, RDI_ERROR_TARGET_RUNNING);
            break;
        case RDIError_UnableToInitialise:
        case RDIError_Comms:
        case RDIError_TargetBroken:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_TARGET_BROKEN);
            break;

        case RDIError_LoopBack_Opella:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_LOOPBACK_OPELLA_XD);
            break;

        case RDIError_BadPointType:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_BADPOINT_TYPE);
            break;
        case RDIError_IncorrectProcType:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_INCORRECT_PROC_TYPE);
            break;
        case RDIError_CantSetPoint:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_CANT_SET_POINT);
            break;
        case RDIError_NoSuchPoint:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_NO_SUCH_POINT);
            break;
        case RDIError_PointInUse:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_POINT_USE);
            break;
        case RDIError_Dw1FileFailed:
        case RDIError_Diskware_File_Not_Found:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_DW1_FAILED);
            break;
        case RDIError_Fpga1FileFailed:
        case RDIError_FPGA_File_Not_Found:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_FPGA_FAILED);
            break;
        case RDIError_NoToolConfig:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_NO_TOOL_CONF);
            break;
        case RDIError_UnknownDiskware:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_UNKNOWN_DW_ERROR);
            break;
        case RDIError_NoRTCKSupport:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_NO_RTCK_SUPPORT);
            break;
        case RDIError_NonEditablePoint:
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_ROM_AREA);
            break;
        case  RDIError_SoftInitialiseError :
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_CONFIG_ERROR);
            break;
        case  RDIError_TPANotConnected :
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_TPA_ERROR);
            break;
        case  RDIError_DeviceAlreadyInUse :
            PrintMessage(ERROR_MESSAGE,RDI_ERROR_DEVICE_IN_USE);
            break;
	   default:
		  {
            PrintMessage(ERROR_MESSAGE, RDI_ERROR_UNKNOWN_ERROR);
            break;
		  }
        }
}
/****************************************************************************
     Function: LOW_SetJtagFreq
     Engineer: Venkitakrishnan K
        Input: void
       Output: Error Code
  Description: Set JTAG Frequency
Date           Initials    Description
31-Mar-08        VK         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_SetJtagFreq(void)
{

    if(pfSetJTAGFreq(tySI.ulJtagFreqKhz,tySI.bJtagRtck))
        return GDBSERV_NO_ERROR;
    else
        return GDBSERV_ERROR;
}

/****************************************************************************
     Function: ARMFreeLibrary
     Engineer: Rejeesh S Babu
        Input: none
       Output: TyError - No Error
  Description: Free ARM library
Date           Initials    Description
31-Mar-08         RS          Initial
****************************************************************************/
static TyError ARMFreeLibrary(void)
{

    // Remove any BP details...
    RemoveAllBPInfoFromList();

    // if library not loaded, get out.
    if(hGlobalInstRdiDll == NULL)
        return NO_ERROR;

    (void)FreeLibrary(hGlobalInstRdiDll);

    hGlobalInstRdiDll     = NULL;
    pfAshScanIR           = NULL;
    pfAshScanDR           = NULL;
    pfResetTAP            = NULL;
    pfSetJTAGFreq         = NULL;

    return ARMFreeLibrary();
}

/****************************************************************************
     Function: GetRegName
     Engineer: Venkitakrishnan K
        Input: ulGdbRegNumber:Register No:
       Output: char*         :Register Name.
  Description: returns reg name for GDB reg number
Date           Initials    Description
31-Mar-08         VK          Initial
****************************************************************************/
char  * GetRegName(unsigned long ulGdbRegNumber)
{
    static char szRegName[16];
    unsigned long ulTRegIndex=0;
    TyARMReg*        pTempReg;
    unsigned long    ulMaxNumReg;

    if(gbCortexM3)
        {
        ulMaxNumReg  = MAX_CORTEXM3_REG;
        pTempReg     = ptyCortexM3Reg;
        }
    else
        {
        ulMaxNumReg  = MAX_ARM_REG;
        pTempReg     = ptyARMReg;
        }
    for(ulTRegIndex=0;ulTRegIndex<ulMaxNumReg;ulTRegIndex++)
        {
        if(pTempReg[ulTRegIndex].ulAddrIndex == ulGdbRegNumber)
            return pTempReg[ulTRegIndex].pszRegName;
        }

    sprintf(szRegName, "0x%lX", ulGdbRegNumber);
    return szRegName;
}

/****************************************************************************
     Function: GetRegAddr
     Engineer: Venkitakrishnan K
        Input: *pRegName:pointer to Register Name
               *iRegAddr:pointer to store Register Address
       Output: TRUE if the register is found, FALSE if not
  Description: returns ARM register address by register name
Date           Initials    Description
31-Mar-08        VK          Initial
****************************************************************************/
int GetRegAddr(char *pRegName, int *iRegAddr)
{
    assert(pRegName    !=NULL);
    assert(iRegAddr    !=NULL);
    int iIndex;
    TyARMReg*        pTempReg;
    unsigned long    ulMaxNumReg;

    if(gbCortexM3)
        {
        ulMaxNumReg  = MAX_CORTEXM3_REG;
        pTempReg     = ptyCortexM3Reg;
        }
    else
        {
        ulMaxNumReg  = MAX_ARM_REG;
        pTempReg     = ptyARMReg;
        }
    for(iIndex=0; iIndex < (int)ulMaxNumReg; iIndex++)
        {
        if(strcasecmp(pTempReg[iIndex].pszRegName, pRegName) == 0)
            {
            *iRegAddr = (int)pTempReg[iIndex].ulAddrIndex;
            return TRUE;
            }
        }
    *iRegAddr = 0xFFFF;    
    return FALSE;
}

/****************************************************************************
     Function: GetBpTypeName
     Engineer: Venkitakrishan K
        Input: ulBpType:Breakpoint Type
       Output: char *  :Breakpoint Type Name 
  Description: return BP type name
Date           Initials    Description
31-Mar-08        VK         Initial
****************************************************************************/
static char  * GetBpTypeName(unsigned long ulBpType)
{
    static char szBpName[16];

    switch(ulBpType)
        {
        case BPTYPE_SOFTWARE:
            return(char*)"SW";
        case BPTYPE_HARDWARE:
            return(char*)"HW";
        case BPTYPE_WRITE:
        case BPTYPE_READ:
        case BPTYPE_ACCESS:
        default:
            sprintf(szBpName, "0x%lX", ulBpType);
            return szBpName;
        }
}

/****************************************************************************
     Function: GetDatabaseFilename
     Engineer: Rejeesh S Babu
        Input: char *pszPathName:Pointer to store path for creating Config. file.
       Output: Void
  Description: Filename and path for creating Config. file.
Date           Initials    Description
31-Mar-08       RS         Initial
11-Mar-10       SVL/VK     Added checking for '\' as separator
****************************************************************************/
static void GetDatabaseFilename(char *pszPathName)
{
    assert(pszPathName    !=NULL);
    char szDatabaseFilename[_MAX_PATH] = {0};
    strcpy(szDatabaseFilename,filename);
    // Strip off the executable name....
    while(strlen(szDatabaseFilename) != 0x0)
        {
        if((szDatabaseFilename[strlen(szDatabaseFilename) - 1] == '/')
           ||(szDatabaseFilename[strlen(szDatabaseFilename) - 1] == '\\'))
            break;
        szDatabaseFilename[strlen(szDatabaseFilename) - 1] = '\0';
        }
    strcat(szDatabaseFilename, "OpellaXD");
    // Now return it....
    strcpy(pszPathName,szDatabaseFilename);
}

/*==============================================
 global LOW_functions from here...
 ==============================================*/
/****************************************************************************
     Function: LOW_Connect
     Engineer: Rejeesh S Babu
        Input: none
       Output: int : FALSE if error
  Description: Connect to target
Date           Initials    Description
31-Mar-2008      RS          Initial
29-Apr-2010      SJ          Added Cortex-A series support
04-Jun-2010      SJ          Added support for TI devices
****************************************************************************/
int LOW_Connect(void)
{
#if _WINCONS
    static WinRDI_funcGetRDIProcVec           GetRDIProcVec;
    static WinRDI_funcGetVersion              GetDLLRDIVersion; 
    static WinRDI_funcSetVersion              SetDLLRDIVersion;
    static WinRDI_funcRegister_Yield_Callback Register_Yield_Callback;    
#elif __LINUX
    static SoRDI_funcGetRDIProcVec           GetRDIProcVec;
    static SoRDI_funcGetVersion              GetDLLRDIVersion; 
    static SoRDI_funcSetVersion              SetDLLRDIVersion;
    static SoRDI_funcRegister_Yield_Callback Register_Yield_Callback;    
#endif
    TyError                                 ErrRet;
    char                                    szdebuggername[16]                ="GDBServerForARM";
    char                                    szDatabaseFilename[_MAX_PATH];
    unsigned int                            type                              = 0;
    unsigned int                            uiRDIVersion                      = 0;
    unsigned long                           nProcs                            = 0;
    unsigned long                           ulAddress                         = 0;
    unsigned int                            uiCount;
    unsigned long                           ulTemp                            = 0;



    hGlobalInstRdiDll                       = LoadLibrary(OPELLAXD_DLL_NAME);
    if(hGlobalInstRdiDll == NULL)
        {
#ifdef __LINUX
        PrintMessage(DEBUG_MESSAGE, "DLL Error: %s", dlerror());
#endif
        PrintMessage(ERROR_MESSAGE,  GDB_EMSG_LOAD_LIB_FAILED, szTargetDllName);
        return GDBSERV_ERROR;

        }

#if _WINCONS
    GetDLLRDIVersion          =  (WinRDI_funcGetVersion)GetProcAddress(hGlobalInstRdiDll,WinRDI_strGetVersion);
    SetDLLRDIVersion          =  (WinRDI_funcSetVersion)GetProcAddress(hGlobalInstRdiDll,WinRDI_strSetVersion);
    GetRDIProcVec             =  (WinRDI_funcGetRDIProcVec)GetProcAddress(hGlobalInstRdiDll,WinRDI_strGetRDIProcVec);
    Register_Yield_Callback   =  (WinRDI_funcRegister_Yield_Callback)GetProcAddress(hGlobalInstRdiDll,WinRDI_strRegister_Yield_Callback);
#elif __LINUX
    GetDLLRDIVersion          =  (SoRDI_funcGetVersion)GetProcAddress(hGlobalInstRdiDll,SoRDI_strGetVersion);
    SetDLLRDIVersion          =  (SoRDI_funcSetVersion)GetProcAddress(hGlobalInstRdiDll,SoRDI_strSetVersion);
    GetRDIProcVec             =  (SoRDI_funcGetRDIProcVec)GetProcAddress(hGlobalInstRdiDll,SoRDI_strGetRDIProcVec);
    Register_Yield_Callback   =  (SoRDI_funcRegister_Yield_Callback)GetProcAddress(hGlobalInstRdiDll,SoRDI_strRegister_Yield_Callback);
#endif
    pfAshScanIR               = (TyAshScanIR)              GetProcAddress(hGlobalInstRdiDll, "ASH_ScanIR");
    pfAshScanDR               = (TyAshScanDR)              GetProcAddress(hGlobalInstRdiDll, "ASH_ScanDR");
    pfAshScanIRScanDR         = (TyAshScanIRScanDR)        GetProcAddress(hGlobalInstRdiDll, "ASH_ScanIRScanDR");
    pfSetJTAGFreq             = (TySetJTAGFrequency)       GetProcAddress(hGlobalInstRdiDll, "ASHSetJTAGFrequency");
    pfResetTAP                = (TyResetTAP)               GetProcAddress(hGlobalInstRdiDll, "ASH_ResetTAPController");
    pfPrepareforJTAGConsole   = (TyPrepareforJTAGConsole)  GetProcAddress(hGlobalInstRdiDll, "ASH_PrepareforJTAGConsole");

    pfProcessCP15Register = (TyProcessCP15Register) GetProcAddress(hGlobalInstRdiDll, "ASH_ProcessCP15Register");
    pfReadCPRegister = (TyReadCPRegister) GetProcAddress(hGlobalInstRdiDll, "ASH_ReadCPRegister");
    pfWriteCPRegister = (TyWriteCPRegister) GetProcAddress(hGlobalInstRdiDll, "ASH_WriteCPRegister");
    // get a pointer to the exported ProcVector entry points
    pRDIAgentVectorTable      = GetRDIProcVec();

    COpellaConfig copellaConfig;

    // Debugger name
    copellaConfig.SetDebuggerName(szdebuggername);

    //EndianessType 
    copellaConfig.SetEndianness((Endian)tySI.tyProcConfig.Processor._ARM.tyEndianess);     

    //Reset configuration
    copellaConfig.SetTargetResetMode((TargetResetMode)tySI.tyProcConfig.Processor._ARM.tyTargetReset);
    if((TargetResetMode)tySI.tyProcConfig.Processor._ARM.tyTargetReset == HardResetAndDelay)
        copellaConfig.SetHardResetDelay(tySI.bTargetResetDelay);

    //Cache Clean Address    
    copellaConfig.SetCacheCleanStartAddress( tySI.tyProcConfig.Processor._ARM.ulCacheCleanAddress );

    // Safe Non-vector Address
    copellaConfig.EnableSafeNonVectorAddress((bool)tySI.bEnableSafenonVectorAddress);   
    if(tySI.bEnableSafenonVectorAddress)
        copellaConfig.SetSafeNonVectorAddress( tySI.tyProcConfig.Processor._ARM.ulSafeNonVectorAddress );

    // Compiler Type   
    copellaConfig.SetSemiHostingSupport((SemiHostingSupport)tySI.tyProcConfig.Processor._ARM.tySemihostSupport);

    // Use nTRST
    copellaConfig.UsenTRST((int)tySI.bnTRst);

    // Use DBGRQ   
    copellaConfig.SetDBGRQLow((int)tySI.bDbrq );

    // JTAG clock speed rate index basically a "divide-by" value)
    copellaConfig.SetJTAGClockFreqMode(eJTAGClockFreqMode );
    if(!eJTAGClockFreqMode)
        copellaConfig.SetFixedJTAGFrequencyInKHz((int)tySI.ulJtagFreqKhz );

    //Target Voltage
    copellaConfig.SetHotPlugVoltage(tySI.fVoltage);

    //Processor Name
    copellaConfig.SetProcessorName( tySI.tyProcConfig.szProcName );

    //Internal Name
    copellaConfig.SetSelectedDevice( tySI.tyProcConfig.szInternalName );
    if(strcmp(tySI.tyProcConfig.szInternalName, "Cortex-M3")==0)
        {
        gbCortexM3 = TRUE;
        }

    //Handle TI devices here
    if(tySI.tyProcConfig.Processor._ARM.bTiDevice)
    {
        copellaConfig.SetTICore( tySI.tyProcConfig.Processor._ARM.bTiDevice );
        copellaConfig.SetTISubPortNumber( tySI.tyProcConfig.Processor._ARM.uiTiSubPortNum);
        copellaConfig.SetDapPcSupport( tySI.tyProcConfig.Processor._ARM.bDapPcControl);
    }

    copellaConfig.SetSelectedCore(tySI.ulTAP[0]-1); //TO_DO for Multicore
    copellaConfig.SetOpellaUSBSerialNumber(tySI.szProbeInstance);
    copellaConfig.SetNumberOfCores((int)tySI.tyProcConfig.Processor._ARM.uiNumberofCores);
    for(uiCount=0; uiCount <tySI.tyProcConfig.Processor._ARM.uiNumberofCores; uiCount++)
        {
        copellaConfig.SetIRLength((int)uiCount,(unsigned char)tySI.tyProcConfig.Processor._ARM.tyScanChainDef[uiCount+1].ulIRlength);//uiCount+1 used bcoz count starts from 1 in XML file.
        copellaConfig.SetCoreType((int)uiCount,(unsigned char)tySI.tyProcConfig.Processor._ARM.tyScanChainDef[uiCount+1].bIsARMCore );
        }

    copellaConfig.SetShiftIRPreTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftIRPreTCKs);
    copellaConfig.SetShiftIRPostTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftIRPostTCKs);
    copellaConfig.SetShiftDRPreTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftDRPreTCKs);
    copellaConfig.SetShiftDRPostTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftDRPostTCKs);

    //Get the coresight configuration parameters, if the ARM device is coresight compliant
    if(TRUE == tySI.tyProcConfig.Processor._ARM.bCoresightCompliant)
        {
        copellaConfig.SetCoresightCompliant(tySI.tyProcConfig.Processor._ARM.bCoresightCompliant);
        copellaConfig.SetDebugBaseAddress(tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulCoresightBaseAddr);
        copellaConfig.SetDebugApIndex(tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulDbgApIndex);
        copellaConfig.SetMemoryApIndex(tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulMemApIndex);
		copellaConfig.SetUseMemApForMemAccess(tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulUseMemApMemAccess);
		}

    if (TRUE == tySI.tyProcConfig.Processor._ARM.bCacheInvalidationNeeded)
        {
        copellaConfig.SetCacheInvalidationParam(tySI.tyProcConfig.Processor._ARM.bCacheInvalidationNeeded);
        }

    if (tySI.bEnableRDILogging)
		{
		copellaConfig.EnableRDILogging(TRUE);
		}
	else
		{
		copellaConfig.EnableRDILogging(FALSE);
		}
    // Get the database filename...
    GetDatabaseFilename(szDatabaseFilename);

    // Write the Toolconf database to the file...
    (void)copellaConfig.Save( szDatabaseFilename );

    // Check the RDI version 
    uiRDIVersion=(unsigned int)GetDLLRDIVersion();

    // If the DLL returns version number 150 then we must try to set version 151
    // and check that the version has changed to RDI 151. 
    if(uiRDIVersion!=151)
        {
        SetDLLRDIVersion(151);
        uiRDIVersion=(unsigned int)GetDLLRDIVersion();         
        }

    // Register the GDBServer callback
    Register_Yield_Callback((WinRDI_YieldProc *)LOW_HaltProcessorManually,NULL);

    //For Semi-hosting  
    tyGlobalhostif.write     = LocRDI_Hif_Write;
    tyGlobalhostif.readc     = LocRDI_Hif_ReadC;
    tyGlobalhostif.gets      = LocRDI_Hif_GetS;
    tyGlobalhostif.writec    = LocRDI_Hif_WriteC;

    // set the type of boot required and Initialisation info
    // We want to do a cold boot if:
    //  - the user has selected "Issue a Hard Reset on PathFinder configuration" at initialisation

    if(tySI.ulEnableTargetReset)
        {
        // we are initialising and we want the target to be reset*/
        tyGlobalAgent = NULL;
        type= COMMS_RESET|EndianessType; 
        }
    else
        {
        //warm boot, proc already initialised earlier, or do not want target reset
        type= WARM_BOOT|EndianessType;
        }
    ErrRet = pRDIAgentVectorTable->info(NULL,RDIInfo_DisableGUI,NULL,NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    //Step 1.  Identify the Debug Agent     
    ErrRet = pRDIAgentVectorTable->openagent(&tyGlobalAgent,type,NULL,&tyGlobalhostif,NULL);
    if(ErrRet != NO_ERROR)
        {
        if((tySI.bJtagConsoleMode == 1) && (ErrRet == RDIError_TargetBroken))
            return GDBSERV_NO_ERROR;
        else
            {
            (void)pRDIAgentVectorTable->closeagent(tyGlobalAgent);
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }
    //Step 2. Get the number of processors
    nProcs = 0; //initialise to zero
    ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&nProcs,NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    // Step 3. Get details on the processor, Module handle etc.
    //ulProcessorNumber=1 chosen for matching with RDI implementation;
    for(ulGlobalCoreNo=1;ulGlobalCoreNo<=nProcs;ulGlobalCoreNo++)
        {
        ulGlobalCoreNoTemp=ulGlobalCoreNo;
        ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&ulGlobalCoreNoTemp,(unsigned long *)&tyGlobalArmModule[ulGlobalCoreNo-1]);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }


    pRDIModuleVectorTable = pRDIAgentVectorTable;

    for(ulGlobalCoreNo=0;ulGlobalCoreNo<nProcs;ulGlobalCoreNo++)
        {
        ErrRet = pRDIModuleVectorTable->open(tyGlobalArmModule[ulGlobalCoreNo].handle, type, NULL,&tyGlobalhostif, NULL);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        // ConfigureSemiHosting is always called in order to set
        // the necessary attribute in Midlayer.
        // Configure SemiHosting Options
        if(Low_ConfigureSemihost(tySI.tyProcConfig.Processor._ARM.uiTopOfMem,tySI.tyProcConfig.Processor._ARM.uiVectorTrap,tySI.tyProcConfig.Processor._ARM.uidisableSemihosting))
            return GDBSERV_ERROR;

        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIVector_Catch,&ulTemp,NULL);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }

//MANDATORY INFO CALLS
        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_Points,&ulTemp,NULL);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }

        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_Step,&ulTemp,NULL);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }

        // CACHE CLEAN ADDRESS
        // we want to set the cache clean address for some cores

        if(!strcmp(tySI.tyProcConfig.szInternalName,"ARM946E-S")||
           !strcmp(tySI.tyProcConfig.szInternalName,"ARM922T")||
           !strcmp(tySI.tyProcConfig.szInternalName,"ARM920T")||
           !strcmp(tySI.tyProcConfig.szInternalName,"ARM940T"))
            {
            // get the address
            ulAddress = tySI.tyProcConfig.Processor._ARM.ulCacheCleanAddress;
            // set the address
            ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_SetARM9RestartCodeAddress,&ulAddress,NULL);
            if(ErrRet != NO_ERROR)
                {
                SetupErrorMessage(ErrRet);
                return GDBSERV_ERROR;
                }
            }

        //Does Processor support Register cache selection
        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_CP15CacheSelection,NULL,NULL);
        if(ErrRet==RDIError_UnimplementedMessage)
            {
            ErrRet = RDIError_NoError;
            }

        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }

        //Does Processor support Register memory region selection
        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_CP15CurrentMemoryArea,NULL,NULL);
        if(ErrRet==RDIError_UnimplementedMessage)
            {
            ErrRet = RDIError_NoError;
            }
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }
    return GDBSERV_NO_ERROR;

}

/****************************************************************************
     Function: LOW_Disconnect
     Engineer: Venkitakrishnan K
        Input: none
       Output: Error Code
  Description: Disconnect from target
Date           Initials    Description
31-Mar-08        VK          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_Disconnect(void)
{
    TyError ErrRet;

    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, GDB_MSG_TARGET_DISCONNECT);

    if((ErrRet = ARMFreeLibrary()) != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IsProcessorExecuting
     Engineer: Rejeesh S Babu
        Input: *pbExecuting : pointer to flag for result
       Output: int : FALSE if error
  Description: Checks if processor is executing
Date           Initials    Description
31-Mar-08         RS          Initial
****************************************************************************/
int LOW_IsProcessorExecuting(volatile int *pbExecuting,
                             int iCurrentCoreIndex)
{
    assert(pbExecuting    !=NULL);
    int arg1;
    int arg2;
    TyError ErrRet;
    *pbExecuting=1;
    tySI.tyProcConfig.Processor._ARM.bRunningFlag[iCurrentCoreIndex] = TRUE;
    ErrRet =pRDIAgentVectorTable->info(tyGlobalArmModule[iCurrentCoreIndex].handle,RDIInfo_StatusProc,(ARMword *)&arg1,(ARMword *)&arg2);
    if(ErrRet!= NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    *pbExecuting=arg1;
    if(*pbExecuting==FALSE)
        {
        tySI.tyProcConfig.Processor._ARM.bRunningFlag[iCurrentCoreIndex] = FALSE;
        return 1;
        }
    else
        return 0;
}

/****************************************************************************
     Function: LOW_GetCauseOfBreak
     Engineer: Rejeesh S Babu
        Input: *ptyStopSignal : storage for cause of break
       Output: Error Code
  Description: Gets cause of last break
Date           Initials    Description
31-Mar-08          RS          Initial
o5-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_GetCauseOfBreak(TyStopSignal *ptyStopSignal)
{
    assert(ptyStopSignal    !=NULL);
    int arg1;
    int arg2;
    TyError ErrRet;
    ErrRet =pRDIAgentVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_StatusProc,(ARMword *)&arg1,(ARMword *)&arg2);
    if(ErrRet!= NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }
    NOREF(arg1);
    switch(arg2)
        {
        case  0x11:
            *ptyStopSignal=TARGET_SIGTERM;
            break;
        case 0x0a:
            *ptyStopSignal=TARGET_SIGINT;
            break;
        default:
            *ptyStopSignal=TARGET_SIGTRAP;
            break;
        }
    return GDBSERV_NO_ERROR;

}

/****************************************************************************
     Function: LOW_SetActiveCoreIndex
     Engineer: Rejeesh S Babu
        Input: iIndex : index to the Arm core
       Output: void
  Description: detect scanchain
Date           Initials    Description
04-Dec-2007    RS          Initial
****************************************************************************/
void LOW_SetActiveCoreIndex(int iIndex)
{
    ulGlobalCoreNo=(unsigned long)iIndex;
}

/****************************************************************************
     Function: LOW_ReadRegisters
     Engineer: Venkitakrishnan K
        Input: *pulRegisters : storage for registers
       Output: int : FALSE if error
  Description: Reads registers
Date           Initials    Description
31-Mar-08          VK          Initial
****************************************************************************/
int LOW_ReadRegisters(unsigned long *pulRegisters)
{
    assert(pulRegisters    !=NULL);
    unsigned int uiCount;
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, GDB_MSG_READ_REGISTERS );

    for(uiCount=0; uiCount < GDBSERV_GEN_PURPOSE_REG; uiCount++)//Read upto pc only.
        {
        if(LOW_ReadRegister(uiCount, &pulRegisters[uiCount]) == FALSE)
            return FALSE;
        }
    for(;uiCount<41;uiCount++)
        {
        pulRegisters[uiCount]=0;
        }

    //CPSR Mask is same as xPSR for Cortex-M3
    if(LOW_ReadRegister((int)REG_ARM_CPSR, &pulRegisters[uiCount])==FALSE)
        return FALSE;

    return TRUE;
}

/****************************************************************************
     Function: LOW_ReadRegister
     Engineer: Venkitakrishnan K
        Input: ulRegisterNumber  : register to read
               *pulRegisterValue : storage for one register
       Output: int : FALSE if error
  Description: Reads one register
Date           Initials    Description
31-Mar-08         VK          Initial
****************************************************************************/
int LOW_ReadRegister(unsigned long   ulRegisterNumber,
                     unsigned long  *pulRegisterValue)
{
    assert(pulRegisterValue    !=NULL);
    TyError          ErrRet=NO_ERROR;
    unsigned long    ulReg;
    unsigned long    ulMode=0;
    unsigned long    ulRegMask=0;
    if(tyGlobalArmModule[0].handle==NULL)//0 as no multicore support now
        {
        PrintMessage(ERROR_MESSAGE,RDI_ERROR_TARGET_BROKEN);
        return FALSE;
        }

    if(!GetModeAndMaskForReg(&ulRegisterNumber,&ulMode,&ulRegMask))
        return FALSE;
    ulReg = ulRegisterNumber;


    if(ulReg == 0xFFFFFFFF)
        {
        *pulRegisterValue = BAD_HEX_VALUE;
        }
    else
        {
        ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule[ulGlobalCoreNo].handle,ulMode,ulRegMask, pulRegisterValue);

        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return FALSE;
            }
        }
    PrintMessage(DEBUG_MESSAGE, 
                 GDB_MSG_READ_A_REGISTER, 
                 GetRegName(ulRegisterNumber),*pulRegisterValue);

    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     GDB_MSG_READ_A_REGISTER, 
                     GetRegName(ulRegisterNumber), 
                     *pulRegisterValue);

    return TRUE;
}

/****************************************************************************
     Function: LOW_WriteRegisters
     Engineer: Venkitakrishnan K
        Input: *pulRegisters : registers values to write
       Output: int : FALSE if error
  Description: Writes registers
Date           Initials    Description
31-Mar-08          VK          Initial
****************************************************************************/
int LOW_WriteRegisters(unsigned long *pulRegisters)
{
    assert(pulRegisters    !=NULL);
    unsigned long uiCount;
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, GDB_MSG_WRITE_REGISTERS);

    for(uiCount=0; uiCount < GDBSERV_GEN_PURPOSE_REG; uiCount++)
        {
        if(LOW_WriteRegister(uiCount, pulRegisters[uiCount]) == FALSE)
            return FALSE;
        }
    //CPSR Mask is same as xPSR for Cortex-M3
    if(LOW_WriteRegister((int)REG_ARM_CPSR, pulRegisters[uiCount]))
        return 1;
    else
        return 0;
}

/****************************************************************************
     Function: LOW_WriteRegister
     Engineer: Venkitakrishnan K
        Input: ulRegisterNumber  : register to write
               ulRegisterValue   : register value
       Output: int : FALSE if error
  Description: writes one register
Date           Initials    Description
31-Mar-08          VK          Initial
****************************************************************************/
int LOW_WriteRegister(unsigned long ulRegisterNumber,
                      unsigned long ulRegisterValue)
{
    unsigned long    ulReg;
    unsigned long    ulOldRegisterValue;
    unsigned long    ulMode=0;
    unsigned long    ulRegMask=0;
    if(tyGlobalArmModule[0].handle==NULL)//0 as no multicore support now
        {
        PrintMessage(ERROR_MESSAGE,RDI_ERROR_TARGET_BROKEN);
        return FALSE;
        }
    TyError ErrRet=NO_ERROR;

    if(LOW_ReadRegister(ulRegisterNumber , &ulOldRegisterValue) == FALSE)
        return FALSE;

    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     GDB_MSG_WRITE_A_REGISTER, 
                     GetRegName(ulRegisterNumber), 
                     ulRegisterValue);

    ulReg = ulRegisterNumber;

    if(ulReg == 0xFFFFFFFF)
        {
        SetupErrorMessage(IDERR_ASH_DSTREGISTER);
        return TRUE;
        }

    if(!GetModeAndMaskForReg(&ulRegisterNumber,&ulMode,&ulRegMask))
        return FALSE;

    //Write to the register
    ErrRet = pRDIModuleVectorTable->CPUwrite(tyGlobalArmModule[ulGlobalCoreNo].handle,ulMode,ulRegMask,&ulRegisterValue);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    PrintMessage(DEBUG_MESSAGE,GDB_MSG_WRITE_A_REGISTER,GetRegName(ulRegisterNumber),ulRegisterValue);
    return TRUE;
}

/****************************************************************************
     Function: LOW_ReadPC
     Engineer: Venkitakrishnan K
        Input: *pulRegisterValue : storage for PC
       Output: int : FALSE if error
  Description: Reads PC register
Date           Initials    Description
31-Mar-08         VK          Initial
****************************************************************************/
int LOW_ReadPC(unsigned long  *pulRegisterValue)
{
    assert(pulRegisterValue    !=NULL);
    TyError ErrRet=NO_ERROR;
    unsigned long ulMode=(unsigned long)REG_ARM_R15;
    unsigned long ulRegMask=(unsigned long)R15_MASK;

    ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule[ulGlobalCoreNo].handle,ulMode,ulRegMask,pulRegisterValue);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    if(tySI.bDebugOutput)
        PrintMessage(DEBUG_MESSAGE, 
                     GDB_MSG_READ_PC_REGISTER, 
                     *pulRegisterValue);

    return TRUE;
}

/****************************************************************************
     Function: LOW_WritePC
     Engineer:Venkitakrishnan K
        Input: ulRegisterValue   : new PC value
       Output: Error Code
  Description: writes PC register
Date           Initials    Description
31-Mar-08         VK          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_WritePC(unsigned long ulRegisterValue)
{
    TyError ErrRet=NO_ERROR;
    unsigned long ulMode=(unsigned long)REG_ARM_R15;
    unsigned long ulRegMask=(unsigned long)R15_MASK;

    ErrRet = pRDIModuleVectorTable->CPUwrite(tyGlobalArmModule[ulGlobalCoreNo].handle,ulMode,ulRegMask,&ulRegisterValue);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     GDB_MSG_WRITE_PC_REGISTER, 
                     ulRegisterValue);
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadMemory
     Engineer: Venkitakrishnan K
        Input: ulAddress  : address to read from
               ulCount    : number of bytes to read
               *pucData   : storage for data
       Output: int : FALSE if error
  Description: reads memory
Date            Initials    Description
31-Mar-08         VK          Initial
****************************************************************************/
int LOW_ReadMemory(unsigned long ulAddress,
                   unsigned long ulCount,
                   unsigned char *pucData)
{
    assert(pucData    !=NULL);
    unsigned int uicount;
    TyError ErrRet=NO_ERROR;
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE,GDB_MSG_READ_MEMORY,ulAddress,ulCount);
    uicount=ulCount;
    if(tyGlobalArmModule[0].handle==NULL)//0 as no multicore support now
        {
        PrintMessage(ERROR_MESSAGE,RDI_ERROR_TARGET_BROKEN);
        return GDBSERV_ERROR;
        }
    ErrRet = pRDIModuleVectorTable->read(tyGlobalArmModule[ulGlobalCoreNo].handle,ulAddress , 
                                         (void *) pucData, &uicount, 
                                         (RDI_AccessType) RDIAccess_Code);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        if(uiGExError)
            return GDBSERV_ERROR;
        }

    return GDBSERV_NO_ERROR;
}  

/****************************************************************************
     Function: LOW_ReadMemory
     Engineer: Deepa
        Input: ulAddress  : address to read from
               ulCount    : number of bytes to read
               *pucData   : storage for data
               eRDIAccessType: type of data access
       Output: int : FALSE if error
  Description: reads memory
Date            Initials    Description
31-Mar-08        DVA         Initial
****************************************************************************/
int LOW_ReadAnyMemory(unsigned long ulAddress,
                      unsigned long ulCount,
                      unsigned char *pucData,
                      RDI_AccessType eRDIAccessType)
{
    assert(pucData    !=NULL);
    unsigned int uicount;
    TyError ErrRet=NO_ERROR;
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE,GDB_MSG_READ_MEMORY,ulAddress,ulCount);
    uicount=ulCount;
    if(tyGlobalArmModule[0].handle==NULL)//0 as no multicore support now
        {
        PrintMessage(ERROR_MESSAGE,RDI_ERROR_TARGET_BROKEN);
        return GDBSERV_ERROR;
        }
    ErrRet = pRDIModuleVectorTable->read(tyGlobalArmModule[ulGlobalCoreNo].handle,ulAddress , 
                                         (void *) pucData, &uicount, 
                                         eRDIAccessType);//(RDI_AccessType) RDIAccess_Code);

    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        if(uiGExError)
            return GDBSERV_ERROR;
        }

    return GDBSERV_NO_ERROR;
}  
/****************************************************************************
     Function: LOW_IsBPSet
     Engineer: Venkitakrishnan K
        Input: ulPCValue         :PC Value
               iCurrentCoreIndex :Current Core No:
       Output: int : FALSE if error
  Description: Checks whether a BP is set at the PC value.
Date           Initials    Description
31-Mar-08        VK          Initial
21-May-2010      SJ          Modified for using the Breakpoint element list
****************************************************************************/
int LOW_IsBPSet(unsigned long ulPCValue,
                int iCurrentCoreIndex)
{
    TyBreakpointElementInfo *pTempNode = NULL;

    NOREF(iCurrentCoreIndex);

    /* Start from the first node */
    pTempNode = ptyBreakpointElementList;

    while (pTempNode != NULL) 
    {
        if((pTempNode->tyBreakpointElement.Address == ulPCValue) &&
            (pTempNode->tyBreakpointElement.ulDataType == 0))
        {
            /* return TRUE if a BP is set at this address */
            return TRUE;
        }
        pTempNode = pTempNode->next;
    }
    return -1;

}
/****************************************************************************
     Function: LOW_GetRemovedBP
     Engineer:Venkitakrishnan K
        Input: ulPCValue         :PC Value
               iCurrentCoreIndex :Current Core No:
       Output: int : FALSE if error
  Description: Gets the removed BP.
Date           Initials    Description
31-Mar-08        VK          Initial
21-May-2010      SJ          Modified for using the Breakpoint element list
****************************************************************************/
int LOW_GetRemovedBP(unsigned long ulPCValue,
                     int iCurrentCoreIndex)
{    
    TyError ErrRet=NO_ERROR;
    unsigned long ulBpType=0;
    bool arg2;
    unsigned long uiPointType=RDIPoint_EQ;
    RDI_PointHandle BpPointHandle;

    TyBreakpointElementInfo *pTempNode = NULL;

    NOREF(iCurrentCoreIndex);

    /* Start from the first node */
    pTempNode = ptyBreakpointElementList;

    while (pTempNode != NULL) 
    {
        if((pTempNode->tyBreakpointElement.Address == ulPCValue) &&
            (pTempNode->tyBreakpointElement.ulDataType == 0))
        {
            /*  Get the BP type corresponding to this address    */
            ulBpType = pTempNode->tyBreakpointElement.ulBpType;
        }
        pTempNode = pTempNode->next;
    }

    if(BPTYPE_HARDWARE == ulBpType)
        {
        arg2 = TRUE;
        ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIProperty_SetAsNumeric,0,(ARMword *)&arg2);
        if(ErrRet != NO_ERROR)
            {

            SetupErrorMessage(ErrRet);
            return FALSE;
            }
        }

    if((ErrRet = pRDIModuleVectorTable->setbreak(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                                 ulPCValue,
                                                 uiPointType, 
                                                 0,
                                                 0, 
                                                 &BpPointHandle))!= NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    if(ulBpType==1)
        {
        arg2 = FALSE;
        ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIProperty_SetAsNumeric,0,(ARMword *)&arg2);
        if(ErrRet != NO_ERROR)
            {

            SetupErrorMessage(ErrRet);
            return FALSE;
            }
        }
    return FALSE;
}

/****************************************************************************
     Function: LOW_WriteMemory
     Engineer: Venkitakrishnan K
        Input: ulAddress  : address to write to
               ulCount    : number of bytes to write
               *pucData   : pointer to data to write
       Output: Erroro code
  Description: writes memory
Date           Initials    Description
31-Mar-08        VK          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_WriteMemory(unsigned long ulAddress,
                    unsigned long ulCount,
                    unsigned char *pucData)
{
    assert(pucData    !=NULL);
    unsigned int uicount;
    TyError ErrRet=NO_ERROR;

    if(tySI.bDebugOutput)
        {
        if(tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         GDB_MSG_WRITE_MEMORY, 
                         ulAddress, ulCount);
        }
    uicount=ulCount;
    if(tyGlobalArmModule[0].handle==NULL)//0 as no multicore support now
        {
        PrintMessage(ERROR_MESSAGE,RDI_ERROR_TARGET_BROKEN);
        return GDBSERV_ERROR;
        }
    ErrRet = pRDIModuleVectorTable->write(tyGlobalArmModule[ulGlobalCoreNo].handle, 
                                          (void const *) pucData ,
                                          ulAddress ,&uicount ,
                                          (RDI_AccessType)RDIAccess_Code);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SupportedBreakpoint
     Engineer: Venkitakrishnan K
        Input: ulBpType  : breakpoint type
       Output: int : TRUE if BP type supported, else false
  Description: indicate which BP types are supported
Date           Initials    Description
31-Mar-08        VK          Initial
****************************************************************************/
int LOW_SupportedBreakpoint(unsigned long ulBpType)
{
    switch(ulBpType)
        {
        case BPTYPE_SOFTWARE:
            return TRUE;
        case BPTYPE_HARDWARE:
            return TRUE;
        case BPTYPE_WRITE:
            return FALSE;
        case BPTYPE_READ:
            return FALSE;
        case BPTYPE_ACCESS:
            return FALSE;
        default:
            return FALSE;
        }
}

/****************************************************************************
     Function: LOW_DeleteBreakpoint
     Engineer: Venkitakrishnan K
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
       Output: Error Code
  Description: delete a breakpoint
Date           Initials    Description
31-Mar-08        VK          Initial
05-Aug-2008    NCH           Error Code
20-May-2010     SJ           Modified to improve BP and WP handles support.
                             Added proper commenting. 
****************************************************************************/
int LOW_DeleteBreakpoint(unsigned long ulBpType,
                         unsigned long ulAddress,
                         unsigned long ulLength,
                         int iCurrentCoreIndex)
{
    TyError        ErrRet;
    //int            iBpIndex;
    //unsigned int uiIndex;
    unsigned long ulBpHandle;

    /*   Check whether the BP type is supported  */
    if(!LOW_SupportedBreakpoint(ulBpType))
        {
        assert(0);
        }

    /*   Check for the "kind" of BP's supported    */
    if((ulLength != ARM_BP) && (ulLength != THUMB_BP) && (ulLength != THUMB2_BP))
        {
        assert(0);
        }

    /*  Get the BP handle corresponding to the BP address   */
    if(NO_ERROR != GetBPHandleFromList(ulAddress, &ulBpHandle))
        {
        PrintMessage(LOG_MESSAGE, 
                     GDB_MSG_NONEED_DELETE_BP, 
                     GetBpTypeName(ulBpType),ulAddress);
        }
    else
        {
        //if(ulGSemiState)
        //   iBpIndex++;
        ErrRet = pRDIModuleVectorTable->clearbreak(tyGlobalArmModule[ulGlobalCoreNo].handle, ulBpHandle);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }

        if((ErrRet = RemoveBPInfoFromList(ulBpHandle)) != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        if(tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         GDB_MSG_DELETE_BP, 
                         GetBpTypeName(ulBpType), ulAddress, ulLength, iCurrentCoreIndex);

        }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SetBreakpoint
     Engineer: Venkitakrishnan K
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
               iCurrentCoreIndex : core index
       Output: Error Code
  Description: set a breakpoint
Date           Initials    Description
31-Mar-08        VK          Initial
05-Aug-2008     NCH          Error Code
19-Oct-2009     SVL          Support for forcefully setting breakpoint types
20-May-2010     SJ           Modified to improve BP and WP handles support.
                             Added proper commenting. 
****************************************************************************/
int LOW_SetBreakpoint(unsigned long ulBpType,
                      unsigned long ulAddress,
                      unsigned long ulLength,
                      int  iCurrentCoreIndex)
{
    TyError          ErrRet;
    TyBreakpointElement tyBreakpointElement;
    TyBpTypeInfo     *pBpNode = NULL;  
    int              iBpType = 0;
    unsigned int     uiPointType = 0;
    RDI_PointHandle  BpPointHandle;
    bool             arg2;

    if(IsAddressAlreadyInBPList(ulAddress))
        {
        /*    There is breakpoint at this address already     */
        SetupErrorMessage(IDERR_ADD_BP_DUPLICATE);
        return GDBSERV_NO_ERROR;
        }

     /*   Check whether the BP type is supported  */
    if(!LOW_SupportedBreakpoint(ulBpType))
        {
        assert(0);
        }
    /*   Check for the "kind" of BP's supported    */
    if((ulLength != ARM_BP) && (ulLength != THUMB_BP) && (ulLength != THUMB2_BP))
        {
        assert(0);
        }
    /*   For ARMv7 architectures, the point type should be "RDIPoint_16Bit" */
    if(strcmp(tySI.tyProcConfig.szInternalName,"Cortex-M3") == 0)
        {
        uiPointType = RDIPoint_16Bit;
        }
    else
        {
        /* Get the breakpoint type from BpType array */
        iBpType = GetBpType(ulAddress, &pBpNode);
        if(iBpType == -1)
            {
            iBpType = (int)BPTYPE_AUTO;
            }
        switch(iBpType)
            {
            case BPTYPE_AUTO:
                if(ulLength == 2)
                    {
                    uiPointType = RDIPoint_16Bit;
                    }
                else
                    {
                    uiPointType = RDIPoint_EQ;
                    }
                break;
            case BPTYPE_ARM:
                uiPointType = RDIPoint_EQ;
                break;
            case BPTYPE_THUMB:
                uiPointType = RDIPoint_16Bit;
                break;
            default:
                break;
            }
        }

    /*  Inform RDI if the BP Type is a HW BP. After this info call, RDI will insert only HW BP. This is done evry time when a 
        HW BP is inserted    */
    if(BPTYPE_HARDWARE == ulBpType)
        {
        arg2 = TRUE;
        ErrRet = pRDIAgentVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIProperty_SetAsNumeric,0,(ARMword *)&arg2);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }
    /*   Set the BP at the given address    */
    ErrRet = pRDIModuleVectorTable->setbreak(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                             ulAddress,
                                             uiPointType, 
                                             0,
                                             0, 
                                             &BpPointHandle);

    /*   Handle the error case   */
    if(ErrRet)
        {
        /* If a HW BP was given for insertion, inform RDI that further BP's are not HW BP's */
        if(BPTYPE_HARDWARE == ulBpType)
            {
            arg2 = FALSE;
            (void)pRDIAgentVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIProperty_SetAsNumeric,0,(ARMword *)&arg2);
            }
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     GDB_MSG_SET_BP, 
                     GetBpTypeName(ulBpType), 
                     ulAddress,ulLength,
                     iCurrentCoreIndex);

    /* If a HW BP was given for insertion, inform RDI that further BP's are not HW BP's */
    if(BPTYPE_HARDWARE == ulBpType)
        {
        arg2 = FALSE;
        ErrRet = pRDIAgentVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIProperty_SetAsNumeric,0,(ARMword *)&arg2);
        if(ErrRet != NO_ERROR)
            {

            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }

    /* increment the number of hardware resources used */
    ulNoOfWPs++;

    /* Add the current BP information to the list    */
    tyBreakpointElement.Address      =   ulAddress;
    tyBreakpointElement.ulBpType     =   ulBpType;
    tyBreakpointElement.ulDataType   =   0;
    tyBreakpointElement.ulDataValue  =   0;
    tyBreakpointElement.ulIdentifier =   BpPointHandle;
    if((ErrRet = AddBpInfoToList(&tyBreakpointElement)) != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Continue
     Engineer:Rejeesh S Babu
        Input: *ptyStopSignal:storage for cause of break
               iCurrentCore :Current Core No:
       Output: int : FALSE if error
  Description: start execution of target
Date           Initials    Description
31-Mar-08        RS          Initial
****************************************************************************/
int LOW_Continue(TyStopSignal *ptyStopSignal,
                 int iCurrentCore)
{
    assert(ptyStopSignal    !=NULL);
    TyError        ErrRet = TRUE;
    unsigned long ulAddress;

    ptyStopSignal=ptyStopSignal;
    long ulBPTemp;
    bool bBPFound=FALSE;
    RDI_PointHandle   PointHandle;

    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, GDB_MSG_CONTINUE);

    if(!LOW_ReadPC(&ulAddress))
        return FALSE;

    if((ulBPTemp=LOW_IsBPSet(ulAddress,iCurrentCore))!=-1)
        {

        bBPFound=TRUE;
        if(ulGSemiState)
            ulBPTemp++;
        ulBPTemp=ulBPTemp;
        /* NOTE: This was commented for 'move to line' in Eclipse */
        //ErrRet = pRDIModuleVectorTable->clearbreak(tyGlobalArmModule[ulGlobalCoreNo].handle,(unsigned long)ulBPTemp);         
        /*if(ErrRet != NO_ERROR)
           {
        
            SetupErrorMessage(ErrRet);
            return FALSE;
           }  */
        }
    ErrRet = pRDIAgentVectorTable->execute(tyGlobalAgent,&tyGlobalArmModule[ulGlobalCoreNo].handle,FALSE,&PointHandle);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        tySI.tyProcConfig.Processor._ARM.bRunningFlag[iCurrentCore]=0;
        if(uiGExError)
            return 0;
        }
    NOREF(bBPFound);
    /* NOTE: This was commented for 'move to line' in Eclipse */
    /*if(bBPFound)
    {
        if ((LOW_GetRemovedBP (ulAddress,
                              iCurrentCore))!=NO_ERROR)
           return FALSE;
 
    }*/
    if(!LOW_ReadPC(&ulAddress))
        return FALSE;

    if(LOW_GetCauseOfBreak(ptyStopSignal) != GDBSERV_NO_ERROR)
        return FALSE;
    return TRUE;

}
/****************************************************************************
     Function: LOW_Exexute
     Engineer: Rejeesh S Babu
        Input: * pszDownloadFileName:Name of Download File
       Output: int : FALSE if error
  Description: start execution of target for download without listening to server
Date           Initials    Description
31-Mar-08        RS          Initial
****************************************************************************/
int LOW_Execute(char* pszDownloadFileName)
{
    assert(pszDownloadFileName    !=NULL);
    int          bExecuting = FALSE;
    pszDownloadFileName=pszDownloadFileName;

    PrintMessage(DEBUG_MESSAGE, GDB_MSG_EXECUTE);

    // Now we must wait until the processor has stopped executing or the user hits any key
    for(;;)
        {
        if(!LOW_IsProcessorExecuting(&bExecuting, 0))
            return FALSE;

        if(!bExecuting)
            {
            PrintMessage(DEBUG_MESSAGE,
                         GDB_MSG_EXECUTION_TERMINATED, 
                         pszDownloadFileName);
            break;
            }

        if(KeyHit())
            {
            // while executing we treat any key press from the user as a halt...
            PrintMessage(INFO_MESSAGE,
                         GDB_MSG_KEY_HIT_DETECTED, 
                         pszDownloadFileName);
            if(LOW_Halt(0) == FALSE)
                return FALSE;
            break;
            }
#ifdef __LINUX
        usleep(100* 1000);
#endif
#ifdef _WINCONS
        Sleep(100);
#endif
        }

    return TRUE;

}
/****************************************************************************
     Function: LOW_Step
     Engineer: Venkitakrishnan K
        Input: *ptyStopSignal : storage for cause of break
       Output: int : FALSE if error
  Description: Single step
Date           Initials    Description
31-Mar-08        VK          Initial
****************************************************************************/
int LOW_Step(TyStopSignal *ptyStopSignal)
{
    assert(ptyStopSignal    !=NULL);
    TyError        ErrRet;
    RDI_PointHandle   PointHandle;
    unsigned long ulAddress;
    int iCurrentCore=0;
    NOREF(ptyStopSignal);
    if(!LOW_ReadPC(&ulAddress))
        return FALSE;
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE,GDB_MSG_STEP );

    ErrRet = pRDIAgentVectorTable->step(tyGlobalAgent,&tyGlobalArmModule[ulGlobalCoreNo].handle,TRUE,1,&PointHandle);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);        
        tySI.tyProcConfig.Processor._ARM.bRunningFlag[iCurrentCore]=0;
        if(uiGExError)
            return 0;
        }

    if(!LOW_ReadPC(&ulAddress))
        return FALSE;

    if(LOW_GetCauseOfBreak(ptyStopSignal) != GDBSERV_NO_ERROR)
        return FALSE;

    return TRUE;
}
/****************************************************************************
     Function: LOW_Halt
     Engineer: Rejeesh S Babu
        Input: iCurrentCoreIndex : core index
       Output: int : FALSE if error
  Description: Halt execution of target
Date           Initials    Description
31-Mar-08         RS          Initial
****************************************************************************/
int LOW_Halt(int iCurrentCoreIndex)
{

    TyError        ErrRet;
    ErrRet =pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_TargetHalt,NULL,NULL);
    if(ErrRet != NO_ERROR)
        {

        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    //Update the running flag
    tySI.tyProcConfig.Processor._ARM.bRunningFlag[iCurrentCoreIndex] = FALSE;

    return TRUE;
}
/****************************************************************************
     Function: LOW_CloseDevice
     Engineer:Rejeesh S Babu
        Input: void
       Output: void
  Description: Close Opella-XD
Date           Initials    Description
16-Jun-08        RS          Initial
****************************************************************************/
int LOW_CloseDevice(int iIndex)
{
    TyError              ErrRet;
    if(tyGlobalArmModule[iIndex].handle!=NULL)
        {
        //close the debug module  
        ErrRet = pRDIModuleVectorTable->close(tyGlobalArmModule[iIndex].handle);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return FALSE;
            }
        }

    if(tyGlobalAgent)
        {
        //close the debug module  
        ErrRet = pRDIAgentVectorTable->closeagent(tyGlobalAgent);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return FALSE;
            }
        }
    tyGlobalArmModule[iIndex].handle=NULL;
    tyGlobalAgent = NULL;

    return TRUE;

}
/****************************************************************************
     Function: LOW_ResetTarget
     Engineer:Rejeesh S Babu
        Input: none
       Output: Error Code
  Description: Resets target
Date           Initials    Description
31-Mar-08        RS          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_ResetTarget(void)
{
#if 0
   TyError              ErrRet;
    // set the global variable to report that we are recovering from a hardware reset
    tyGlobalStopSignal = (TyStopSignal)RDIError_Reset;

    // OK, let's close the existing handles we have....
    if(tyGlobalArmModule[0].handle!=NULL)//tyGlobalArmModule[0] has to be changed for Multicore.
        {
        //close the debug module  
        ErrRet = pRDIModuleVectorTable->close(tyGlobalArmModule[ulGlobalCoreNo].handle);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }

    if(tyGlobalAgent)
        {
        //close the debug module  
        ErrRet = pRDIAgentVectorTable->closeagent(tyGlobalAgent);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }
    tyGlobalArmModule[0].handle=NULL;
    tyGlobalAgent = NULL;

	//Free Library
	if(hGlobalInstRdiDll)
		{
		printf("Freeing RDI Library\n");
	 	(void)FreeLibrary(hGlobalInstRdiDll);
		}

	hGlobalInstRdiDll     = NULL;

    ErrRet = LOW_Connect();
    if(ErrRet != NO_ERROR)
        {
        return GDBSERV_ERROR;
        }
#endif
    //tySI.tyProcConfig.Processor._ARM.bRunningFlag[ulGlobalCoreNo] = FALSE;

    return GDBSERV_NO_ERROR;         
}
/****************************************************************************
     Function: LOW_SoftReset
     Engineer:Rejeesh S Babu
        Input: none
       Output: none
  Description: Issue Soft Reset
Date           Initials    Description
28-May-08        RS          Initial
****************************************************************************/
void LOW_SoftReset()
{
    TyError       ErrRet;
    unsigned long ulCPSR    = 0;
    unsigned long ulMode=(unsigned long)REG_ARM_CPSR;
    unsigned long ulRegMask=(unsigned long)R16_MASK;

    ErrRet = pRDIModuleVectorTable->CPUread(tyGlobalArmModule[ulGlobalCoreNo].handle,ulMode,ulRegMask,&ulCPSR);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        //return 0;
        }
    // blank the last eight bits of the CPSR.
    ulCPSR = ulCPSR & TOP_26BIT_MASK;
    // Write the pattern 0x13 into the cpsr to set the mode to 13 (T bit is zeroed above)
    ulCPSR = ulCPSR | ARM_SUPERVISOR_MODE_MASK|INTERRUPTS_DISABLE;

    ErrRet = pRDIModuleVectorTable->CPUwrite(tyGlobalArmModule[ulGlobalCoreNo].handle,ulMode,ulRegMask,&ulCPSR);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        //return 0;
        }
    // Set PC 
    (void)LOW_WritePC(gulPC);
}
/****************************************************************************
     Function: LOW_GetDWFWVersions
     Engineer: Rejeesh S Babu
        Input: Pointers to store d/w and f/w information.
       Output: Error Code
  Description: Get diskware and  firmware version information
Date           Initials    Description
31-Mar-08       RS          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_GetDWFWVersions(char * pszDiskWare1,
                        char * pszFirmWare,
                        char * pszFpgaWare1,
                        char * pszFpgaWare2,
                        char * pszFpgaWare3,
                        char * pszFpgaWare4)
{
    assert(pszDiskWare1    !=NULL);
    assert(pszFirmWare     !=NULL);
    assert(pszFpgaWare1    !=NULL);
    assert(pszFpgaWare2    !=NULL);
    assert(pszFpgaWare3    !=NULL);
    assert(pszFpgaWare4    !=NULL);
    pszDiskWare1[0] = '\0';
    pszFirmWare [0] = '\0';
    pszFpgaWare1[0] = '\0';
    pszFpgaWare2[0] = '\0';
    pszFpgaWare3[0] = '\0';
    pszFpgaWare4[0] = '\0';
    TyError                                 ErrRet;

    ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Device,&tyDeviceInfo.ulDiskwareVersion,&tyDeviceInfo.ulFirmwareVersion);
    if(ErrRet!=NO_ERROR)
        return GDBSERV_ERROR;
    ConvertVersionString(tyDeviceInfo.ulDiskwareVersion,pszDiskWare1);
    ConvertVersionString(tyDeviceInfo.ulFirmwareVersion,pszFirmWare);
    return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: LOW_IsBigEndian
     Engineer:Rejeesh S Babu
        Input: iCoreIndex:Current Core No:
       Output: Processor Mode
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
31-Mar-08       RS          Initial
****************************************************************************/
int LOW_IsBigEndian(int iCoreIndex)
{
    return tySI.tyProcConfig.Processor._ARM.bProcIsBigEndian[iCoreIndex];
}
/****************************************************************************
     Function: LOW_ConnectJcon
     Engineer: Venkitakrishnan K
        Input: int bInitialJtagAccess : enable using initial jtag access (MIPS specific)
       Output: int : FALSE if error
  Description: Connect to target (use for JTAG console)
Date           Initials    Description
31-Mar-08       VK          Initial
****************************************************************************/
int LOW_ConnectJcon(int bInitialJtagAccess)
{
    int iRet = GDBSERV_NO_ERROR;
    bInitialJtagAccess=bInitialJtagAccess;
//   return LOW_Connect();

    hGlobalInstRdiDll = LoadLibrary(OPELLAXD_DLL_NAME);

    if(hGlobalInstRdiDll == NULL)
        {
#ifdef __LINUX
        PrintMessage(DEBUG_MESSAGE, "DLL Error: %s", dlerror());
#endif
        PrintMessage(ERROR_MESSAGE,  GDB_EMSG_LOAD_LIB_FAILED, szTargetDllName);
        return GDBSERV_ERROR;
        }

    pfAshScanIR               = (TyAshScanIR)              GetProcAddress(hGlobalInstRdiDll, "ASH_ScanIR");
    pfAshScanDR               = (TyAshScanDR)              GetProcAddress(hGlobalInstRdiDll, "ASH_ScanDR");
    pfAshScanIRScanDR         = (TyAshScanIRScanDR)        GetProcAddress(hGlobalInstRdiDll, "ASH_ScanIRScanDR");
    pfSetJTAGFreq             = (TySetJTAGFrequency)       GetProcAddress(hGlobalInstRdiDll, "ASHSetJTAGFrequency");
    pfResetTAP                = (TyResetTAP)               GetProcAddress(hGlobalInstRdiDll, "ASH_ResetTAPController");
    //pfMulticoreConfig         = (TyMulticoreConfig)        GetProcAddress(hGlobalInstRdiDll, "ASH_MulticoreConfig");
    pfPrepareforJTAGConsole   = (TyPrepareforJTAGConsole)  GetProcAddress(hGlobalInstRdiDll, "ASH_PrepareforJTAGConsole");

    // call a function in DLL
    if(pfPrepareforJTAGConsole != NULL)
        {
        iRet = pfPrepareforJTAGConsole();
        if(iRet!= GDBSERV_NO_ERROR)
            return GDBSERV_ERROR;
        else
            return GDBSERV_NO_ERROR;
        }
    else
        return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_DisconnectJcon
     Engineer:Venkitakrishnan K
        Input: none
       Output: int : FALSE if error
  Description: Disconnect from target (use for JTAG console)
Date           Initials    Description
31-Mar-08       VK          Initial
****************************************************************************/
int LOW_DisconnectJcon(void)
{
    return LOW_Disconnect();
}
/****************************************************************************
     Function: LOW_ScanIR
     Engineer: Venkitakrishnan K
        Input: unsigned long * pulDataOut : array of unsigned long to be shifted into IR
               unsigned long * pulDataIn  : array of unsigned long for data to be shifted from IR
               unsigned long ulDataLength : number of bits shifted into/from IR
       Output: int : Error Code
  Description: shift specified data into IR and read data shifted out
Date           Initials    Description
31-Mar-08        VK          Initial
05-Aug-2008    NCH         Error code
****************************************************************************/
int LOW_ScanIR(unsigned long * pulDataIn, 
               unsigned long * pulDataOut, 
               unsigned long ulDataLength)
{
    assert(pulDataOut != NULL);
    assert(pulDataIn  != NULL);

    // call a function in DLL
    if(pfAshScanIR != NULL)
        {
        (void)pfAshScanIR(ulDataLength, pulDataIn, pulDataOut);
        return GDBSERV_NO_ERROR;
        }
    else
        return GDBSERV_ERROR;
}
/****************************************************************************
     Function: LOW_ScanDR
     Engineer: Venkitakrishnan K
        Input: unsigned long * pulDataOut : array of unsigned long to be shifted into DR
               unsigned long * pulDataIn  : array of unsigned long for data to be shifted from DR
               unsigned long ulDataLength : number of bits shifted into/from DR
       Output: int : Error code
  Description: shift specified data into DR and read data shifted out
Date           Initials    Description
31-Mar-08         VK          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_ScanDR(unsigned long * pulDataIn, 
               unsigned long * pulDataOut, 
               unsigned long ulDataLength)
{
    assert(pulDataOut != NULL);
    assert(pulDataIn  != NULL);

    // call a function in DLL
    if(pfAshScanDR != NULL)
        {
        (void)pfAshScanDR(ulDataLength, pulDataIn, pulDataOut);
        return GDBSERV_NO_ERROR;
        }
    else
        return GDBSERV_ERROR;
}
/****************************************************************************
     Function: LOW_ScanIRScanDR
     Engineer: Venkitakrishnan K
        Input: *pulDataIn:Inpuut Data.
               ulTap     :Total number of taps
       Output: int : FALSE if error
  Description: shift specified data into DR and read data shifted out
Date           Initials    Description
31-Mar-08        VK          Initial
****************************************************************************/
int LOW_ScanIRScanDR(unsigned long * pulDataIn, 
                     unsigned long * ulLength,
                     unsigned long * pulDataOut)
{
    assert(pulDataIn    !=NULL);
    assert(pulDataOut   !=NULL);
    // call a function in DLL
    if(pfAshScanIRScanDR != NULL)
        {
        if(pfAshScanIRScanDR(ulLength,pulDataIn,pulDataOut)==TRUE)
            {
            return TRUE;
            }
        return FALSE;
        }
    else
        return FALSE;
}
/****************************************************************************
     Function: LOW_ResetTAP
     Engineer: Rejeesh S Babu
        Input: none
       Output: int : FALSE if error
  Description: Reset Jtag TAP
Date           Initials    Description
31-Mar-08        RS          Initial
****************************************************************************/
int LOW_ResetTAP(void)
{
    int iRet;

    if(pfResetTAP != NULL)
        {
        iRet = pfResetTAP();
        if(iRet!= GDBSERV_NO_ERROR)
            return GDBSERV_ERROR;
        else
            return GDBSERV_NO_ERROR;
        }
    else
        return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_Detach
     Engineer: Venkitakrishnan K
        Input: iCurrentCoreIndex:Current Core Number
       Output: Error Code
  Description: Detatch  Target.
Date           Initials    Description
31-Mar-08         VK          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int LOW_Detach(int iCurrentCoreIndex)
{
    int bExecuting = FALSE;

    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, GDB_MSG_DETACH);

    if(!LOW_IsProcessorExecuting(&bExecuting, iCurrentCoreIndex))
        return GDBSERV_ERROR;
    else
        return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: Low_ConfigureSemihost
     Engineer: Rejeesh S Babu
        Input: ultop       :Top OF Memory
               ulvector    :vector Trap Address
               ulSemiState :Semihosting State(ON/OFF)
        Output: int : FALSE if error
  Description: Configure processor for semi-hosting.
Date           Initials    Description
31-Mar-08        RS          Initial
****************************************************************************/
int Low_ConfigureSemihost(unsigned long ultop,unsigned long ulvector,unsigned long ulSemiState)
{
    TyError ErrRet =NO_ERROR;
    uint32    uiSemiHostDummy=0;
    uint32    uiReadSemiState;
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDISemiHosting_SetVector,
                                         &ulvector,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }
    // Can ARM SWI be set
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDIInfo_SemiHostingSetARMSWI,
                                         &uiSemiHostDummy,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }
    // Set the SWI value
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDISemiHosting_SetARMSWI,
                                         (unsigned long *)&tySI.tyProcConfig.Processor._ARM.uiarmSWI,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }

    // Can Thumb SWI be set
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDIInfo_SemiHostingSetThumbSWI,
                                         &uiSemiHostDummy,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }

    //Set the SWI value
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDISemiHosting_SetThumbSWI,
                                         (unsigned long *)&tySI.tyProcConfig.Processor._ARM.uiarmThumb,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }

    // Can Top Of Memory be set
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDIInfo_SetTopMem,
                                         &uiSemiHostDummy,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }

    //Set the Top Of Memory value
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDIInfo_SetTopMem,
                                         &ultop,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }
    // set the semi hosting state, enable,disable, via DCC
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDISemiHosting_SetState,
                                         &ulSemiState,
                                         NULL);

    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }


    // Check that semi-hosting was successfully set
    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDISemiHosting_GetState,
                                         &uiReadSemiState,
                                         NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return ErrRet;
        }

    return NO_ERROR;


}

/****************************************************************************
     Function: LOW_DeleteWatchpoints
     Engineer: Suraj S
        Input: none
        Output: Error Code
  Description: Get the handles of all WP's currently set, delete them amd remove
               them from the BP list
Date           Initials    Description
26-May-2010      SJ          Initial
****************************************************************************/
int LOW_DeleteWatchpoints(void)
{  
    TyError ErrRet = 0;
    unsigned long pulWPHandle[10];
    unsigned long ulWpIndex=0;
    unsigned long ulWPCnt=0;

    /*  Get the handles of all WP's currently set and the count */
    if((ErrRet = GetWPHandlesFromList(pulWPHandle, &ulWPCnt)) != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }
    
    if(ulWPCnt > 0)
        {
        for(ulWpIndex=0; ulWpIndex < ulWPCnt; ulWpIndex++)
            {
            /*  Delete each WP's    */
            ErrRet = pRDIModuleVectorTable->clearwatch(tyGlobalArmModule[ulGlobalCoreNo].handle,pulWPHandle[ulWpIndex]);
            if(ErrRet != NO_ERROR)
                {
                SetupErrorMessage(ErrRet);
                return GDBSERV_ERROR;
                }
            else
                {
                /*  Remove the WP's from the list*/
                if((ErrRet = RemoveBPInfoFromList(pulWPHandle[ulWpIndex])) != NO_ERROR)
                    {
                    SetupErrorMessage(ErrRet);
                    return GDBSERV_ERROR;
                    }
                }
            }
        }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_DeleteWatchpoint
     Engineer: Venkitakrishnan K
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
       Output: Error Code
  Description: delete a breakpoint
Date           Initials    Description
31-Mar-08        VK          Initial
05-Aug-2008    NCH           Error Code
20-May-2010     SJ           Modified to improve BP and WP handles support.
                             Added proper commenting. 
****************************************************************************/
int LOW_DeleteWatchpoint(unsigned long ulAddress,
                         unsigned long ulData,
                         unsigned long ulWpType,
                         int iCurrentCoreIndex)
{
    TyError        ErrRet;
    unsigned long ulWpHandle;

    NOREF(iCurrentCoreIndex);

    /*  Get the BP handle corresponding to the BP address   */
    if(NO_ERROR != GetWPHandleFromList(ulAddress, ulData, ulWpType, &ulWpHandle))
        {
        PrintMessage(LOG_MESSAGE, 
                     GDB_MSG_NONEED_DELETE_BP, 
                     GetBpTypeName(ulWpType),ulAddress);
        return GDBSERV_ERROR;
        }
    else
        {
        ErrRet = pRDIModuleVectorTable->clearwatch(tyGlobalArmModule[ulGlobalCoreNo].handle, ulWpHandle);
        if(ErrRet != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }

        if((ErrRet = RemoveBPInfoFromList(ulWpHandle)) != NO_ERROR)
            {
            SetupErrorMessage(ErrRet);
            return GDBSERV_ERROR;
            }
        }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SetWatchpoint
     Engineer: Venkitakrishnan K
        Input: ulAddress : watchpoint address
               ulData    : watchpoint data
               DataType  : Data type
               uiBPType  : These are Ashling specific watchpoint type
       Output: int : FALSE if error
  Description: set a watchpoint
Date           Initials    Description
31-Mar-08        VK          Initial
21-May-2010      SJ          Modified to use the breakpoint element list which 
                             keeeps the details of all BP's and WP's
****************************************************************************/
int LOW_SetWatchpoint(unsigned long        ulAddress, 
                      unsigned long        uldata,
                      unsigned long        DataType,
                      unsigned int         uiBPType)
{
    TyError ErrRet;
    unsigned long ulDataType;
    unsigned int    uiPointType = 0;
    RDI_PointHandle BpPointHandle;
    TyBreakpointElement tyBreakpointElement;

    ulDataType =(unsigned int) DataType;

    switch(uiBPType)
        {
        case WP_ADDR_TYPE:        
            uiPointType = RDIPoint_EQ;
            break;
        case WP_ADDR_DATA_TYPE:
            uiPointType = RDIDataAndAddressPoint;
            break;
        case WP_DATA_TYPE:
            uiPointType = RDIDataPoint;
            break;
        default:
            break;
        }

    ErrRet=pRDIModuleVectorTable->setwatch  (tyGlobalArmModule[ulGlobalCoreNo].handle,
                                             ulAddress,
                                             uiPointType,
                                             ulDataType,
                                             uldata,  
                                             0, 
                                             &BpPointHandle);

    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    ulNoOfWPs++;    

    /* Add the current BP information to the list    */
    tyBreakpointElement.Address      =   ulAddress;
    tyBreakpointElement.ulBpType     =   uiBPType;
    tyBreakpointElement.ulDataType   =   ulDataType;
    tyBreakpointElement.ulDataValue  =   uldata;
    tyBreakpointElement.ulIdentifier =   BpPointHandle;
    if((ErrRet = AddBpInfoToList(&tyBreakpointElement)) != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    return TRUE;

} 

/****************************************************************************
     Function: LOW_ProcessCPReg
     Engineer: Roshan T R
        Input: unsigned int uiOperation 	: Read /Write
               unsigned int uiCRn           : CRn in coproc instruction
               unsigned int uiCRm           : CRm in coproc instruction
               unsigned int uiOp1           : Operand1 in coproc instruction
               unsigned int uiOp2           : Operand2 in coproc instruction
               pulData      : Pointer to the data read/write from/to coprocessor reg
       Output: int : FALSE if error
  Description: Performs a READ/WRITE operation on a CP15 register
Date           Initials    Description
01-Dec-09      RTR         Initial
****************************************************************************/
int LOW_ProcessCP15Reg(unsigned int uiOperation,unsigned int uiCRn,
                       unsigned int uiCRm,unsigned int uiOp1,
                       unsigned int uiOp2,unsigned long *pulData)
{
    TyError ErrRet;

    ErrRet = pfProcessCP15Register(uiOperation, uiCRn,
                                   uiCRm, uiOp1, uiOp2, pulData);

    if(ErrRet == FALSE)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    return TRUE;
}

/****************************************************************************
     Function: LOW_ReadCPReg
     Engineer: Roshan T R
        Input: ulCPNo 		: Coprocessor Number
               ulRegIndex   : The register to be read
               *pulData     : Pointer to the data read from coprocessor reg
       Output: int : FALSE if error
  Description: Reads from a specific CP15 register
Date           Initials    Description
01-Dec-09      RTR         Initial
****************************************************************************/
int LOW_ReadCPReg(unsigned long ulCPNo, unsigned long ulRegIndex, unsigned long *pulData)
{
    TyError ErrRet;

    ErrRet = pfReadCPRegister(ulCPNo, ulRegIndex, pulData);

    if(ErrRet == FALSE)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    return TRUE;
}

/****************************************************************************
     Function: LOW_WriteCPReg
     Engineer: Roshan T R
        Input: ulCPNo 		: Coprocessor Number
               ulRegIndex   : The register to be read
               pulData      : data to write to coprocessor reg
       Output: int : FALSE if error
  Description: Writes to a specific CP15 register
Date           Initials    Description
01-Dec-09      RTR         Initial
****************************************************************************/
int LOW_WriteCPReg(unsigned long ulCPNo, unsigned long ulRegIndex, unsigned long ulData)
{
    TyError ErrRet;

    ErrRet = pfWriteCPRegister(ulCPNo, ulRegIndex, ulData);

    if(ErrRet == FALSE)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    return TRUE;
}
/****************************************************************************
     Function: LOW_AddCoreRegDefArrayEntries
     Engineer: Nikolay Chokoev
        Input: none
       Output: int - error code
  Description: Add register definitions for core registers
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_AddCoreRegDefArrayEntries(void)
{
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IndexCommonRegDefArrayEntries
     Engineer: Nikolay Chokoev
        Input: none
       Output: int - error code
  Description: Index common registers in reg definition array for fast access
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_IndexCommonRegDefArrayEntries(void)
{
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_StartPFXDMode
     Engineer: Nikolay Chokoev
        Input: None
       Output: int : FALSE if error
  Description: Starts in PFXD mode. Loads RDI library functions.
Date           Initials    Description
05-Aug-2008    NCH         Initial
23-Sep-2009    SVL         Added the support for 'PFXD mode'
****************************************************************************/
int LOW_StartPFXDMode(void)
{   
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Loading RDI libraries...");

    /* Load the library and get the exported functions */
    if(RDI_LoadLibrary() != GDBSERV_NO_ERROR)
        {
        (void)ARMFreeLibrary();
        return GDBSERV_ERROR;
        }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: RDI_LoadLibrary
     Engineer: Sandeep V L
        Input: None
       Output: int : GDBSERV_ERROR if error
                     GDBSERV_NO_ERROR, on success
  Description: Loads RDI library functions, when started in PFXD mode.
               In PFXD mode, during startup, only library is loaded and debug
               agent is opened. Debug target is not connected.
Date           Initials    Description
23-Sep-2009    SVL         Initial
****************************************************************************/
static int RDI_LoadLibrary(void)
{
#if _WINCONS
    static WinRDI_funcGetRDIProcVec           GetRDIProcVec;
    static WinRDI_funcGetVersion              GetDLLRDIVersion; 
    static WinRDI_funcSetVersion              SetDLLRDIVersion;
    static WinRDI_funcRegister_Yield_Callback Register_Yield_Callback; 
#elif __LINUX
    static SoRDI_funcGetRDIProcVec           GetRDIProcVec;
    static SoRDI_funcGetVersion              GetDLLRDIVersion; 
    static SoRDI_funcSetVersion              SetDLLRDIVersion;
    static SoRDI_funcRegister_Yield_Callback Register_Yield_Callback; 
#endif
    TyError                                 ErrRet;
    char                                    szdebuggername[16]                ="GDBServerForARM";
    char                                    szDatabaseFilename[_MAX_PATH];
    unsigned int                            type                              = 0;
    unsigned int                            uiRDIVersion                      = 0;
    unsigned long                           nProcs                            = 0;
    unsigned long                           ulAddress                         = 0;
    unsigned int                            uiCount;
    unsigned long                           ulTemp                            =0;

    NOREF(tyGlobalhostif);
    NOREF(type);
    NOREF(ulAddress);
    NOREF(ulTemp);
    NOREF(nProcs);

    hGlobalInstRdiDll                       = LoadLibrary(OPELLAXD_DLL_NAME);
    if(hGlobalInstRdiDll == NULL)
        {
#ifdef __LINUX
        PrintMessage(DEBUG_MESSAGE, "DLL Error: %s", dlerror());
#endif
        PrintMessage(ERROR_MESSAGE,  GDB_EMSG_LOAD_LIB_FAILED, szTargetDllName);
        return GDBSERV_ERROR;

        }

#if _WINCONS
    GetDLLRDIVersion          =  (WinRDI_funcGetVersion)GetProcAddress(hGlobalInstRdiDll,WinRDI_strGetVersion);
    SetDLLRDIVersion          =  (WinRDI_funcSetVersion)GetProcAddress(hGlobalInstRdiDll,WinRDI_strSetVersion);
    GetRDIProcVec             =  (WinRDI_funcGetRDIProcVec)GetProcAddress(hGlobalInstRdiDll,WinRDI_strGetRDIProcVec);
    Register_Yield_Callback   =  (WinRDI_funcRegister_Yield_Callback)GetProcAddress(hGlobalInstRdiDll,WinRDI_strRegister_Yield_Callback);
#elif __LINUX
    GetDLLRDIVersion          =  (SoRDI_funcGetVersion)GetProcAddress(hGlobalInstRdiDll,SoRDI_strGetVersion);
    SetDLLRDIVersion          =  (SoRDI_funcSetVersion)GetProcAddress(hGlobalInstRdiDll,SoRDI_strSetVersion);
    GetRDIProcVec             =  (SoRDI_funcGetRDIProcVec)GetProcAddress(hGlobalInstRdiDll,SoRDI_strGetRDIProcVec);
    Register_Yield_Callback   =  (SoRDI_funcRegister_Yield_Callback)GetProcAddress(hGlobalInstRdiDll,SoRDI_strRegister_Yield_Callback);
#endif
    pfAshScanIR               = (TyAshScanIR)              GetProcAddress(hGlobalInstRdiDll, "ASH_ScanIR");
    pfAshScanDR               = (TyAshScanDR)              GetProcAddress(hGlobalInstRdiDll, "ASH_ScanDR");
    pfAshScanIRScanDR         = (TyAshScanIRScanDR)        GetProcAddress(hGlobalInstRdiDll, "ASH_ScanIRScanDR");
    pfSetJTAGFreq             = (TySetJTAGFrequency)       GetProcAddress(hGlobalInstRdiDll, "ASHSetJTAGFrequency");

    // get a pointer to the exported ProcVector entry points
    pRDIAgentVectorTable      = GetRDIProcVec();

    COpellaConfig copellaConfig;

    // Debugger name
    copellaConfig.SetDebuggerName(szdebuggername);

    //EndianessType 
    copellaConfig.SetEndianness((Endian)tySI.tyProcConfig.Processor._ARM.tyEndianess);     

    //Reset configuration
    copellaConfig.SetTargetResetMode((TargetResetMode)tySI.tyProcConfig.Processor._ARM.tyTargetReset);
    if((TargetResetMode)tySI.tyProcConfig.Processor._ARM.tyTargetReset==HardResetAndDelay)
        copellaConfig.SetHardResetDelay(tySI.bTargetResetDelay);

    //Cache Clean Address    
    copellaConfig.SetCacheCleanStartAddress( tySI.tyProcConfig.Processor._ARM.ulCacheCleanAddress );

    // Safe Non-vector Address
    copellaConfig.EnableSafeNonVectorAddress((bool)tySI.bEnableSafenonVectorAddress);   
    if(tySI.bEnableSafenonVectorAddress)
        copellaConfig.SetSafeNonVectorAddress( tySI.tyProcConfig.Processor._ARM.ulSafeNonVectorAddress );

    // Compiler Type   
    copellaConfig.SetSemiHostingSupport((SemiHostingSupport)tySI.tyProcConfig.Processor._ARM.tySemihostSupport);

    // Use nTRST
    copellaConfig.UsenTRST((int)tySI.bnTRst);

    // Use DBGRQ   
    copellaConfig.SetDBGRQLow((int)tySI.bDbrq );

    // JTAG clock speed rate index basically a "divide-by" value)
    copellaConfig.SetJTAGClockFreqMode(eJTAGClockFreqMode );
    if(!eJTAGClockFreqMode)
        copellaConfig.SetFixedJTAGFrequencyInKHz((int)tySI.ulJtagFreqKhz );

    //Target Voltage
    copellaConfig.SetHotPlugVoltage(tySI.fVoltage);

    //Processor Name
    copellaConfig.SetProcessorName( tySI.tyProcConfig.szProcName );

    //Internal Name
    copellaConfig.SetSelectedDevice( tySI.tyProcConfig.szInternalName );

    copellaConfig.SetSelectedCore(tySI.ulTAP[0]-1); //TO_DO for Multicore
    copellaConfig.SetOpellaUSBSerialNumber(tySI.szProbeInstance);
    copellaConfig.SetNumberOfCores((int)tySI.tyProcConfig.Processor._ARM.uiNumberofCores);
    for(uiCount=0; uiCount <tySI.tyProcConfig.Processor._ARM.uiNumberofCores; uiCount++)
        {
        copellaConfig.SetIRLength((int)uiCount,(unsigned char)tySI.tyProcConfig.Processor._ARM.tyScanChainDef[uiCount+1].ulIRlength);//uiCount+1 used bcoz count starts from 1 in XML file.
        copellaConfig.SetCoreType((int)uiCount,(unsigned char)tySI.tyProcConfig.Processor._ARM.tyScanChainDef[uiCount+1].bIsARMCore );
        }

    copellaConfig.SetShiftIRPreTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftIRPreTCKs);
    copellaConfig.SetShiftIRPostTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftIRPostTCKs);
    copellaConfig.SetShiftDRPreTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftDRPreTCKs);
    copellaConfig.SetShiftDRPostTCKs(tySI.tyProcConfig.Processor._ARM.ucShiftDRPostTCKs);

    // Get the database filename...
    GetDatabaseFilename(szDatabaseFilename);

    // Write the Toolconf database to the file...
    (void)copellaConfig.Save( szDatabaseFilename );

    // Check the RDI version 
    uiRDIVersion=(unsigned int)GetDLLRDIVersion();

    // If the DLL returns version number 150 then we must try to set version 151
    // and check that the version has changed to RDI 151. 
    if(uiRDIVersion!=151)
        {
        SetDLLRDIVersion(151);
        uiRDIVersion=(unsigned int)GetDLLRDIVersion();         
        }

    // Register the GDBServer callback
    Register_Yield_Callback((WinRDI_YieldProc *)LOW_HaltProcessorManually,NULL);

    //For Semi-hosting  
    tyGlobalhostif.write     = LocRDI_Hif_Write;
    tyGlobalhostif.readc     = LocRDI_Hif_ReadC;
    tyGlobalhostif.gets      = LocRDI_Hif_GetS;
    tyGlobalhostif.writec    = LocRDI_Hif_WriteC;

    // set the type of boot required and Initialisation info
    // We want to do a cold boot if:
    //  - the user has selected "Issue a Hard Reset on PathFinder configuration" at initialisation

    if(tySI.ulEnableTargetReset)
        {
        // we are initialising and we want the target to be reset*/
        tyGlobalAgent = NULL;
        type= COMMS_RESET|EndianessType; 
        }
    else
        {
        //warm boot, proc already initialised earlier, or do not want target reset
        type= WARM_BOOT|EndianessType;
        }
    ErrRet = pRDIAgentVectorTable->info(NULL,RDIInfo_DisableGUI,NULL,NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_PFXDConnect
     Engineer: Sandeep V L
        Input: None
       Output: int : GDBSERV_ERROR if error
                     GDBSERV_NO_ERROR, on success
  Description: To connect to the debug target, when started in PFXD mode
Date           Initials    Description
23-Sep-2009    SVL         Initial
****************************************************************************/
int LOW_PFXDConnect(void)
{
    int ErrRet;

    if(tySI.bDebugOutput)
        {
        PrintMessage(LOG_MESSAGE, "Connecting to target...");
        }
    /* Connect the debug target */
    if((ErrRet = RDI_ConnectPFXD()) != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: RDI_ConnectPFXD
     Engineer: Sandeep V L
        Input: None
       Output: int : GDBSERV_ERROR if error
                     GDBSERV_NO_ERROR, on success
  Description: Opens the debug target and establishes the connection between 
               debug target and debug controller
Date           Initials    Description
23-Sep-2009    SVL         Initial
****************************************************************************/
static int RDI_ConnectPFXD(void)
{
    TyError                                 ErrRet;   
    unsigned int                            type                              = 0;
    unsigned long                           nProcs                            = 0;
    unsigned long                           ulAddress                         = 0;   
    unsigned long                           ulTemp                            =0;

    //Step 1.  Identify the Debug Agent     
    ErrRet = pRDIAgentVectorTable->openagent(&tyGlobalAgent,type,NULL,&tyGlobalhostif,NULL);
    if(ErrRet != NO_ERROR)
        {
        if((tySI.bJtagConsoleMode == 1) && (ErrRet == RDIError_TargetBroken))
            return GDBSERV_NO_ERROR;
        else
            {
            (void)pRDIAgentVectorTable->closeagent(tyGlobalAgent);         
            return ErrRet;
            }
        }

    //Step 2. Get the number of processors
    nProcs = 0; //initialise to zero
    ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&nProcs,NULL);
    if(ErrRet != NO_ERROR)
        {
        return ErrRet;
        }

    // Step 3. Get details on the processor, Module handle etc.
    //ulProcessorNumber=1 chosen for matching with RDI implementation;
    for(ulGlobalCoreNo=1;ulGlobalCoreNo<=nProcs;ulGlobalCoreNo++)
        {
        ulGlobalCoreNoTemp=ulGlobalCoreNo;
        ErrRet = pRDIAgentVectorTable->info(tyGlobalAgent,RDIInfo_Modules,&ulGlobalCoreNoTemp,(unsigned long *)&tyGlobalArmModule[ulGlobalCoreNo-1]);
        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }
        }

    pRDIModuleVectorTable = pRDIAgentVectorTable;

    for(ulGlobalCoreNo=0;ulGlobalCoreNo<nProcs;ulGlobalCoreNo++)
        {
        ErrRet = pRDIModuleVectorTable->open(tyGlobalArmModule[ulGlobalCoreNo].handle, type, NULL,&tyGlobalhostif, NULL);
        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }
        // ConfigureSemiHosting is always called in order to set
        // the necessary attribute in Midlayer.
        // Configure SemiHosting Options
        if(Low_ConfigureSemihost(tySI.tyProcConfig.Processor._ARM.uiTopOfMem,tySI.tyProcConfig.Processor._ARM.uiVectorTrap,tySI.tyProcConfig.Processor._ARM.uidisableSemihosting))
            return GDBSERV_ERROR;

        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIVector_Catch,&ulTemp,NULL);
        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }

//MANDATORY INFO CALLS
        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_Points,&ulTemp,NULL);
        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }

        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_Step,&ulTemp,NULL);
        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }

        // CACHE CLEAN ADDRESS
        // we want to set the cache clean address for some cores

        if(!strcmp(tySI.tyProcConfig.szInternalName,"ARM946E-S")||
           !strcmp(tySI.tyProcConfig.szInternalName,"ARM922T")||
           !strcmp(tySI.tyProcConfig.szInternalName,"ARM920T")||
           !strcmp(tySI.tyProcConfig.szInternalName,"ARM940T"))
            {
            // get the address
            ulAddress = tySI.tyProcConfig.Processor._ARM.ulCacheCleanAddress;
            // set the address
            ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_SetARM9RestartCodeAddress,&ulAddress,NULL);
            if(ErrRet != NO_ERROR)
                {
                return ErrRet;
                }
            }

        //Does Processor support Register cache selection
        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_CP15CacheSelection,NULL,NULL);
        if(ErrRet==RDIError_UnimplementedMessage)
            {
            ErrRet = RDIError_NoError;
            }

        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }

        //Does Processor support Register memory region selection
        ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,RDIInfo_CP15CurrentMemoryArea,NULL,NULL);
        if(ErrRet==RDIError_UnimplementedMessage)
            {
            ErrRet = RDIError_NoError;
            }
        if(ErrRet != NO_ERROR)
            {
            return ErrRet;
            }
        }
    /* setup the resgisters using register settings file */
    (void)SetupRegisterSettings();
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_CfgVectorCatch
     Engineer: Sandeep V L
        Input: unsigned long *pulVectorMask - Bit mask of the vectors to watch               
       Output: TRUE, on success
               GDBSERV_ERROR, on error
  Description: Configure vector catch settings  
Date           Initials    Description
16-Oct-2009     SVL        Initial
*****************************************************************************/
int LOW_CfgVectorCatch(unsigned long *pulVectorMask)
{
    TyError ErrRet;   
    assert(pulVectorMask != NULL);

    ErrRet = pRDIModuleVectorTable->info(tyGlobalArmModule[ulGlobalCoreNo].handle,
                                         RDIVector_Catch,pulVectorMask,NULL);
    if(ErrRet != NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return FALSE;
        }
    return TRUE;
}

/****************************************************************************
     Function: ReadFullRegisterByRegDef
     Engineer: Sandeep V L
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               unsigned long *pulRegValue - storage for register value               
       Output: GDBSERV_NO_ERROR, on success
               GDBSERV_ERROR, on error
  Description: Read the register from the emulator   
Date           Initials    Description
3-Nov-2009     SVL        Initial
*****************************************************************************/
int ReadFullRegisterByRegDef(TyRegDef *ptyRegDef, unsigned long *pulRegValue)
{
    unsigned char *pucData = NULL;
    char pszString[_MAX_PATH] = {'\0'};
    int iIdx = 0;
    unsigned long ulByteVal = 0;

    pucData = (unsigned char*)malloc(sizeof(unsigned char) * ptyRegDef->ubByteSize);
    if(!pucData)
        {
        PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
        return GDBSERV_ERROR;
        }

    if(ptyRegDef->ubMemType == MEM_CODE)
        {
        if(GDBSERV_NO_ERROR != LOW_ReadMemory(ptyRegDef->ulAddress, ptyRegDef->ubByteSize, pucData))
            {
            PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_READ_MEM);
            free(pucData);
            return GDBSERV_ERROR;
            }
        }

    for(iIdx = 0; iIdx < ptyRegDef->ubByteSize; iIdx++)
        {
        UCharToString(pucData[iIdx], pszString);  
        StringToUlong(pszString, &ulByteVal);
        *pulRegValue |= ulByteVal << (iIdx*8);
        }

    free(pucData);
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               unsigned long *pulRegValue - storage for register value
       Output: int - error code
  Description: Read a register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef, 
                             unsigned long *pulRegValue)
{
    int iError;
    unsigned long ulValue = 0;
    assert(ptyRegDef != NULL);
    assert(pulRegValue != NULL);
    if((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulValue)) != GDBSERV_NO_ERROR)
        {
        if(tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Read register %s ...", 
                         GetRegDefDescription(ptyRegDef));
        SetupErrorMessage(iError);
        return GDBSERV_ERROR;
        }
    printf("Register value of %s : %ld\n", ptyRegDef->pszRegName, ulValue);
    *pulRegValue = ExtractRegBits(ulValue, ptyRegDef);
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Read register %s value=0x%08lX.", 
                     GetRegDefDescription(ptyRegDef), *pulRegValue);
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: WriteFullRegisterByRegDef
     Engineer: Sandeep V L
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               unsigned long *pulRegValue - register value               
       Output: GDBSERV_NO_ERROR, on success
               GDBSERV_ERROR, on error
  Description: Write full register    
Date           Initials    Description
3-Nov-2009     SVL        Initial
*****************************************************************************/
int WriteFullRegisterByRegDef(TyRegDef *ptyRegDef, unsigned long *pulRegValue)
{
    char pszString[10] = {'\0'}, temp[3] = {'\0'};
    unsigned char *pucData = NULL;
    int i = 0, j = 0;

    /* pucData allocated */
    pucData = (unsigned char*)malloc(sizeof(unsigned char) * 10);
    if(!pucData)
        {
        PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
        return GDBSERV_ERROR;
        }

    if(ptyRegDef->ubMemType == MEM_CODE)
        {
        UlongToString(*pulRegValue, pszString);        
        for(i = (int)strlen(pszString)-2, j = 0; i >= 0; i-=2, j++)
            {
            strncpy(temp, (pszString + i), 2);
            temp[2] = '\0';
            StringToUChar(temp, &pucData[j]);            
            }
        if(GDBSERV_NO_ERROR != LOW_WriteMemory(ptyRegDef->ulAddress, 
                                               ptyRegDef->ubByteSize, pucData))
            {
            PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_WRITE_MEM);
            free(pucData);
            return GDBSERV_ERROR;
            }
        }

    free(pucData);
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WriteRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               unsigned long ulRegValue - register value to write
       Output: int - error code
  Description: Write a register using a register definition
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef,
                              unsigned long ulRegValue)
{
    int iError;
    unsigned long ulOldValue;
    assert(ptyRegDef != NULL);
    if(tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Write register %s value=0x%08lX.", 
                     GetRegDefDescription(ptyRegDef), ulRegValue);
    if(ptyRegDef->ubBitSize < 32)
        {
        if((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulOldValue)) != GDBSERV_NO_ERROR)
            {
            SetupErrorMessage(iError);
            return GDBSERV_ERROR;
            }
        ulRegValue = MergeRegBits(ulOldValue, ulRegValue, ptyRegDef);
        }
    if((iError = WriteFullRegisterByRegDef(ptyRegDef, &ulRegValue)) != GDBSERV_NO_ERROR)
        {
        SetupErrorMessage(iError);
        return GDBSERV_ERROR;
        }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: GetRegDefForUserSpecificReg
     Engineer: Nikolay Chokoev
        Input: adAddr
               ulData
       Output: int
  Description: Convert a user specific reg struct to a TyRegDef struct
Date           Initials    Description
03-Feb-2006    PJS         Initial
****************************************************************************/
static int GetRegDefForUserSpecificReg(TyRegDef         *ptyRegDef,
                                       TyUserSpecific    *ptyUserSpecReg)
{
    TyRegDef    *ptyTempRegDef;

    if(ptyUserSpecReg->ulVariableSize == 0)
        {
        // reference to a register in the .REG file
        if((ptyTempRegDef = GetRegDefForName(ptyUserSpecReg->szVariableName)) == NULL)
            {
            // TO_DO: when to display message?
            PrintMessage(ERROR_MESSAGE, "Unknown register '%s'.", ptyUserSpecReg->szVariableName);
            return IDERR_ASH_DSTREGISTER;
            }

        *ptyRegDef = *ptyTempRegDef;
        }
    else
        {
        // this is a memory mapped register defined in reg-settings-file
        ptyRegDef->pszRegName    = ptyUserSpecReg->szVariableName;
        ptyRegDef->ulAddress     = ptyUserSpecReg->ulVariableAddress;
        ptyRegDef->ubMemType     = MEM_CODE;
        if(ptyUserSpecReg->ulVariableSize > 4)
            ptyRegDef->ubByteSize = 4;
        else
            ptyRegDef->ubByteSize = (unsigned char)ptyUserSpecReg->ulVariableSize;
        ptyRegDef->ubBitOffset   = 0;
        ptyRegDef->ubBitSize     = (unsigned char)(ptyRegDef->ubByteSize * 8);
        ptyRegDef->iFullRegIndex = -1;
        }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: SetupRegisterSettings
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Update all register settings after a reset.
Date           Initials    Description
03-Feb-2006    PJS         Initial
22-Mar-2006    PJS         use register definitions (.REG file support)
09-May-2008    SPC         Not setting PC, if --program-entry-point not used.
****************************************************************************/
static int SetupRegisterSettings(void)
{
    int  ErrRet;
    unsigned long    i;
    TyRegDef       tyRegDef;

    for(i=0; i < tySI.tyProcConfig.Processor._ARM.ulUserSpecificVariablesCount; i++)
        {
        if((ErrRet = GetRegDefForUserSpecificReg(&tyRegDef,
                                                 &(tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[i]))) != GDBSERV_NO_ERROR)
            return ErrRet;

        if(GDBSERV_NO_ERROR != LOW_WriteRegisterByRegDef(&tyRegDef, tySI.tyProcConfig.Processor._ARM.pUserSpecificVariables[i].ulVariableValue))
            break;
        }

    return GDBSERV_NO_ERROR;
}

