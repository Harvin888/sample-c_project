/****************************************************************************
       Module: gdbxmlcfgarm.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, contains needed resources to parse 
               family specific XML file parameters for ARM.
Date           Initials    Description
08-Feb-2008    VH          Initial
21-Oct-2009    SVL         Added Device and User register file support
31-Dec-2009    SVL         Modified for PathFinder-XD's Peripheral Register 
                           View. RegisterDefintionType will also be stored 
                           in the data structures.
29-Apr-2010    SJ          Added Support for Coresight devices
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdberr.h"

#ifdef __LINUX
#else
    #define strncasecmp _strnicmp
#endif

// external functions
extern TyServerInfo tySI;

// local functions
static void CFG_DebuggerControlNode(TyXMLNode *ptyThisNode);
static void CFG_ARCEmulatorNode(TyXMLNode *ptyThisNode);

int CFG_AddGroupDefArrayEntry(char           *pszGroupName,
                              int             iStartRegIndex,
                              int             iEndRegIndex, 
                              int             iRegDefType);
static int CFG_ValidRegDefLine(char          *pszLine,
                               int           iLineNumber,
                               int           *pbValidReg,
                               char          *pszRegName,
                               unsigned long *pulAddress,
                               unsigned char *pubMemType,
                               unsigned char *pubByteSize,
                               unsigned char *pubBitOffset,
                               unsigned char *pubBitSize,
                               int           *piFullRegIndex,
                               int           *piType);

/****************************************************************************
     Function: CFG_DebuggerControlNode
     Engineer: Nikolay Chokoev
        Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
       Output: none
  Description: Get debugger control node
Date           Initials    Description
12-Nov-2007    NCH         Initial
*****************************************************************************/
static void CFG_DebuggerControlNode(TyXMLNode *ptyThisNode)
{
    tySI.tyProcConfig.ulMaxJtagFrequency = XML_GetChildElementDec(ptyThisNode, "MaxJtagFrequencyKhz", 0);
    tySI.tyProcConfig.ulMaxJtagFrequency *= FREQ_KHZ(1);                 // convert to Hz
}

/****************************************************************************
     Function: CFG_ARCEmulatorNode
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
       Output: none
  Description: Get ARC emulator node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void CFG_ARCEmulatorNode(TyXMLNode *ptyThisNode)
{
    assert(ptyThisNode != NULL);
    tySI.ulTargetVoltagemV = TGTVOLTAGE_MATCH;                           // set default target voltage to match target
    if((XML_GetChildNode(ptyThisNode, "TargetVoltage")) != NULL)
        {  // check target voltage type
        char *pszSupply = XML_GetChildAttrib(ptyThisNode, "TargetVoltage", "SupplyType");
        if(pszSupply != NULL)
            {
            if(!strcasecmp(pszSupply, TGTVOLTAGE_MATCH_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_MATCH;                  // match target voltage
            else if(!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_3V3_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(3300);         // fixed voltage 3.3V
            else if(!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_3V0_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(3000);         // fixed voltage 3.0V
            else if(!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_2V5_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(2500);         // fixed voltage 2.5V
            else if(!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_1V8_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(1800);         // fixed voltage 1.8V
            else if(!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_1V5_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(1500);         // fixed voltage 1.5V
            else if(!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_1V2_NAME))
                tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(1200);         // fixed voltage 1.2V
            }
        }
}

/****************************************************************************
     Function: CFG_ProcessorNode
     Engineer: Nikolay Chokoev
        Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
       Output: none
  Description: Get processor node
Date           Initials    Description
12-Nov-2007    NCH         Initial
29-Apr-2010    SJ          Get the "CoresightCompliant" parameter for ARM
04-Jun-2010    SJ          Add support for TI devices
*****************************************************************************/
void CFG_ProcessorNode(TyXMLNode *ptyThisNode)
{
    TyXMLNode *ptyNode, *ptyCSCfgNode = NULL;
    char *pszName;

    assert(ptyThisNode != NULL);
    // process child nodes
    if((ptyNode = XML_GetChildNode(ptyThisNode, "DebuggerControl")) != NULL)
        CFG_DebuggerControlNode(ptyNode);
    if((ptyNode = XML_GetChildNode(ptyThisNode, "ARCEmulator")) != NULL)
        CFG_ARCEmulatorNode(ptyNode);
    if((pszName = XML_GetAttrib(ptyThisNode, "Device")) != NULL)
        {  // processor name
        strncpy(tySI.tyProcConfig.szProcName, pszName, sizeof(tySI.tyProcConfig.szProcName) -1);
        tySI.tyProcConfig.szProcName[sizeof(tySI.tyProcConfig.szProcName) -1] = '\0';
        }
    if((pszName = XML_GetChildElement(ptyThisNode, "InternalName")) != NULL)
        {  // internal Name (for identification purposes)
        strncpy(tySI.tyProcConfig.szInternalName, pszName, sizeof(tySI.tyProcConfig.szInternalName) -1);
        tySI.tyProcConfig.szInternalName[sizeof(tySI.tyProcConfig.szInternalName) -1] = '\0';
        }
#ifdef ARM
    if((pszName = XML_GetChildElement(ptyThisNode, "CoresightCompliant")) != NULL)
        {
        if(0 == strcmp(pszName,"1"))
            tySI.tyProcConfig.Processor._ARM.bCoresightCompliant = TRUE;
        else
            tySI.tyProcConfig.Processor._ARM.bCoresightCompliant = FALSE;
        }

    if ((ptyCSCfgNode = XML_GetChildNode(ptyThisNode, "CoresightConfig")) != NULL)
        {
        if ((pszName = XML_GetChildElement(ptyCSCfgNode, "UseMemApForMemAccess")) != NULL)
            {
            if (0 == strcmp(pszName,"1"))
                tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulUseMemApMemAccess = TRUE;
            else
                tySI.tyProcConfig.Processor._ARM.tyCoresightConfigInfo.ulUseMemApMemAccess = FALSE;
            }
        }
    if((pszName = XML_GetChildElement(ptyThisNode, "CacheInvalidationNeeded")) != NULL)
        {
        if(0 == strcmp(pszName,"1"))
            tySI.tyProcConfig.Processor._ARM.bCacheInvalidationNeeded = TRUE;
        else
            tySI.tyProcConfig.Processor._ARM.bCacheInvalidationNeeded = FALSE;
        }

    /*  Handles TI devices   */
    if((pszName = XML_GetChildElement(ptyThisNode, "TiDevice")) != NULL)
        {
        if(0 == strcmp(pszName,"1"))
            {
            tySI.tyProcConfig.Processor._ARM.bTiDevice = TRUE;
            if((pszName = XML_GetChildElement(ptyThisNode, "SecondaryTapNum")) != NULL)
                {
                 tySI.tyProcConfig.Processor._ARM.uiTiSubPortNum = (unsigned int)atoi(pszName);
                }

            if((pszName = XML_GetChildElement(ptyThisNode, "DAPPCControl")) != NULL)
                {
                if(0 == strcmp(pszName,"1"))
                    tySI.tyProcConfig.Processor._ARM.bDapPcControl = TRUE;
                else
                    tySI.tyProcConfig.Processor._ARM.bDapPcControl = FALSE;
                }
            }
        else
            tySI.tyProcConfig.Processor._ARM.bTiDevice = FALSE;
        }

#endif
}

/****************************************************************************
     Function: CFG_ProcessRegFile
     Engineer: Nikolay Chokoev
        Input: none
       Output: int - error code
  Description: Process the register file
Date           Initials    Description
03-Jul-2008	   NCH         Initial
21-Oct-2009    SVL         Added for Device register file support in ARM
                           NOTE: This can be removed from this file if the 
                           same function from gdbxmlcfgmips is moved to gdbxml
31-Dec-2009    SVL         Modified for PathFinder-XD's Peripheral Register 
                           View. RegisterDefintionType will also be stored 
                           in the data structures.
*****************************************************************************/
int CFG_ProcessRegFile(void)
{
#ifdef ARM
    int         ErrRet        = NO_ERROR;
    FILE            *pFile        = NULL;
    int             iFullRegIndex = -1;
    int             iLineNumber   = 0;
    unsigned long           ulAddress     = 0;
    unsigned char           ubMemType     = 0;
    unsigned char           ubByteSize    = 4;
    unsigned char           ubBitOffset   = 0;
    unsigned char           ubBitSize     = 0;
    int            bValidReg     = FALSE;
    char            szLine[_MAX_PATH];
    char            szRegName[_MAX_PATH];
    int             iType = (int)REGDEFTYPE_NONE;
    char            szPrevGroupName[_MAX_PATH];
    int             iPrevGrpStartRegIndex = -1;
    int             iPrevGrpEndRegIndex = -1;

    if(tySI.szDevRegFile[0] == '\0')
        {
        sprintf(tySI.szError, "Device specific <RegisterFile..> file not found.");
        return GDBSERV_CFG_ERROR;
        }

    // open in text mode so that CR+LF is converted to LF
    pFile = fopen(tySI.szDevRegFile, "rt");
    if(pFile == NULL)
        {
        sprintf(tySI.szError, "Cannot find device specific register definition file '%s'.", tySI.szDevRegFile);
        return GDBSERV_CFG_ERROR;
        }

    // Have a default group name just in case some registers are defined wihtout having any
    // preceeding group 
    strcpy (szPrevGroupName,"Default");

    while(!feof(pFile))
        {
        if(fgets(szLine, sizeof(szLine), pFile) == NULL)
            break;

        iLineNumber++;

        if((ErrRet = CFG_ValidRegDefLine(szLine,
                                         iLineNumber,
                                         &bValidReg,
                                         szRegName,
                                         &ulAddress,
                                         &ubMemType,
                                         &ubByteSize,
                                         &ubBitOffset,
                                         &ubBitSize,
                                         &iFullRegIndex,
                                         &iType)) != NO_ERROR)
            return ErrRet;

        if(bValidReg)
            {
            if((int)REGDEFTYPE_GROUP == iType)
                {
                if(-1 != iPrevGrpStartRegIndex && -1 != iPrevGrpEndRegIndex)
                    {
                    if((ErrRet = CFG_AddGroupDefArrayEntry(szPrevGroupName,
                                                           iPrevGrpStartRegIndex,
                                                           iPrevGrpEndRegIndex, 
                                                           iType)) != NO_ERROR)
                        return ErrRet;
                    }

                iPrevGrpStartRegIndex = -1;
                iPrevGrpEndRegIndex = -1;
                strcpy(szPrevGroupName, szRegName);

                }
            else
                {
                if((ErrRet = CFG_AddRegDefArrayEntry(szRegName,
                                                     ulAddress,
                                                     (TyMemTypes)ubMemType,
                                                     ubByteSize,
                                                     ubBitOffset,
                                                     ubBitSize,
                                                     iFullRegIndex, 
                                                     iType)) != NO_ERROR)
                    return ErrRet;

                if(iFullRegIndex < 0)
                    {
                    // have just added a full register, remember its index...
                    iFullRegIndex = (int)tySI.uiRegDefArrayEntryCount -1;
                    }

                iPrevGrpEndRegIndex = (int)tySI.uiRegDefArrayEntryCount -1;
                if(-1 == iPrevGrpStartRegIndex)
                    {
                    iPrevGrpStartRegIndex = iPrevGrpEndRegIndex;
                    }
                }
            }
        }

    // Add last group
    if(-1 != iPrevGrpStartRegIndex && -1 != iPrevGrpEndRegIndex)
        {
        if((ErrRet = CFG_AddGroupDefArrayEntry(szPrevGroupName,
                                               iPrevGrpStartRegIndex,
                                               iPrevGrpEndRegIndex, 
                                               iType)) != NO_ERROR)
            return ErrRet;
        }

    if(pFile != NULL)
        (void)fclose(pFile);
#endif

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: CFG_ValidRegDefLine
     Engineer: Patrick Shiels
        Input: *pszLine        : one line from register definition file
               iLineNumber     : line number of this line
               *pbValidReg     : storage for flag if valid reg/bit definition
               *pszRegName     : storage for register/bit name
               *pulAddress     : storage for address
               *pubMemType     : storage for memory type: reg, physical, etc
               *pubByteSize    : storage for size of containing reg in bytes (1, 2 or 4)
               *pubBitOffset   : storage for offset in bits (0..31)
               *pubBitSize     : storage for size in bits (1..32)
               *piFullRegIndex : storage for index to containing reg
       Output: int
  Description: Test if register/bit definition is valid and return details
Date           Initials    Description
25-Mar-2006    PJS         Initial
29-Apr-2008    JG          Added support to read names having space enclosed in ""
                           Also added support for returning type
21-Oct-2009    SVL         Added for Device and User register file support in ARM
                           NOTE: This can be removed from this file if the 
                           same function from gdbxmlcfgmips is moved to gdbxml
*****************************************************************************/
static int CFG_ValidRegDefLine(char          *pszLine,
                               int           iLineNumber,
                               int           *pbValidReg,
                               char          *pszRegName,
                               unsigned long *pulAddress,
                               unsigned char *pubMemType,
                               unsigned char *pubByteSize,
                               unsigned char *pubBitOffset,
                               unsigned char *pubBitSize,
                               int           *piFullRegIndex,
                               int           *piType)
{
    unsigned long          ulAddress, ulSize, ulOffset;
    TyMemTypes   tyMemType;
    TyRegDef      *ptyFullRegDef;
    int            iValidArgs;
    int            iIndex;
    char          *psz;
    int           bValidNum = FALSE;
    char           szLine   [_MAX_PATH];
    char           szRegName[_MAX_PATH];
    char           szType   [_MAX_PATH];
    char           szMemory [_MAX_PATH];
    char           szAddress[_MAX_PATH];
    char           szSize   [_MAX_PATH];
    char           szOffset [_MAX_PATH];

    *pbValidReg = FALSE;

    if(pszLine == NULL)
        return NO_ERROR;

    strncpy(szLine, pszLine, sizeof(szLine)-1);
    szLine[sizeof(szLine)-1] = '\0';

    szRegName[0] = '\0';
    szType   [0] = '\0';
    szMemory [0] = '\0';
    szAddress[0] = '\0';
    szSize   [0] = '\0';
    szOffset [0] = '\0';

    // parse the hard way, wish to preserve case of register name...

    // find first non space character ...
    for(psz=szLine; (psz[0] != '\0') && IsSpace(psz[0]); psz++)
        {
        // keep lint happy with a block!
        }
    if(psz[0] == '\0')
        return NO_ERROR;

    // definition must must start with predef
//   if (_strnicmp(psz, "predef", 6) != 0)
    if(strncasecmp(psz, "predef", 6) != 0)
        return NO_ERROR;
    psz+=6;

    // find next non space character (start of register name) ...
    for(; (psz[0] != '\0') && IsSpace(psz[0]); psz++)
        {
        // keep lint happy with a block!
        }
    // Handle names with space too
    //if ((psz[0] == '\0') || (psz[0] == '"'))
    if(psz[0] == '\0')
        return NO_ERROR;

    // collect register name preserving case...
    iIndex = 0;
    // If the name starts with a " then treat all chars till the ending " as name
    if(psz[0] == '"')
        {
        // Skip first "
        psz++;
        for(; (psz[0] != '\0') && psz[0] != '"'; psz++)
            {
            if(iIndex < (int)(sizeof(szRegName)-1))
                szRegName[iIndex++] = psz[0];
            }
        // Skip ending "
        if(psz[0] == '"')
            {
            psz++;
            }
        }
    else
        {
        for(; (psz[0] != '\0') && !IsSpace(psz[0]); psz++)
            {
            if(iIndex < (int)(sizeof(szRegName)-1))
                szRegName[iIndex++] = psz[0];
            }
        }
    szRegName[iIndex] = '\0';


    // now convert to lowercase and let sscanf parse the rest...  
    StringToLwr(psz);

    iValidArgs = sscanf(psz, " type = %s mem = %s addr = %s size = %s offset = %s ",
                        szType, szMemory, szAddress, szSize, szOffset);

    if(iValidArgs < 1)
        return NO_ERROR;

    // from here on, for register or bit type, enforce strict format...
    if(strcasecmp(szType, "register") == 0)
        {
        if(iValidArgs != 4)
            goto BadRegFileLine;
        *piType = (int)REGDEFTYPE_REGSITER;
        }
    else if(strcasecmp(szType, "bit") == 0)
        {
        if(iValidArgs != 5)
            goto BadRegFileLine;
        *piType = (int)REGDEFTYPE_BIT;
        }
    else if(strcasecmp(szType, "group") == 0)
        {
        if(iValidArgs != 1)
            goto BadRegFileLine;
        *piType = (int)REGDEFTYPE_GROUP;
        }
    else
        {
        return NO_ERROR;
        }

    if( *piType != (int)REGDEFTYPE_GROUP )
        {
        if((tyMemType = StringToMemType(szMemory)) == MEM_INVALID)
            goto BadRegFileLine;

        ulAddress = ExStrToUlong(szAddress, &bValidNum);
        if(!bValidNum)
            goto BadRegFileLine;

        ulSize    = ExStrToUlong(szSize,    &bValidNum);
        if(!bValidNum)
            goto BadRegFileLine;

        if(*piType == (int)REGDEFTYPE_BIT)
            {
            // reject if there is no containing full reg...
            if((ptyFullRegDef = GetRegDefForIndex(*piFullRegIndex)) == NULL)
                goto BadRegFileLine;
//printf("17\n");
//printf("Index:%08lX\n",*piFullRegIndex);
//printf("Address:%08lX\n",(unsigned long)&tySI.ptyRegDefArray[*piFullRegIndex]);
//printf("Address:%08lX\n",(unsigned long)ptyFullRegDef);
//printf("%08lX\t%08lX\n",ulAddress,ptyFullRegDef->ulAddress);
            // reject if bitfield address is not equal to containing full reg address...
            if(ulAddress != ptyFullRegDef->ulAddress)
                goto BadRegFileLine;

            // reject if bitfield size is larger than containing full reg...
            if((ulSize == 0) || (ulSize > (unsigned long)ptyFullRegDef->ubBitSize))
                goto BadRegFileLine;

            ulOffset = ExStrToUlong(szOffset,  &bValidNum);
            if(!bValidNum || ((ulOffset + ulSize) > (unsigned long)ptyFullRegDef->ubBitSize))
                goto BadRegFileLine;

            // *pubByteSize and *piFullRegIndex remain unchanged, i.e. the size in
            // bytes and array index of the register that contains this bit field.
            *pubBitSize   = (unsigned char)ulSize;
            *pubBitOffset = (unsigned char)ulOffset;
            }
        else     // register
            {
            if((ulSize != 1) && (ulSize != 2) && (ulSize != 4))
                goto BadRegFileLine;

            *pubByteSize    = (unsigned char)ulSize;
            *pubBitSize     = (unsigned char)(ulSize *8);
            *pubBitOffset   = 0;
            *piFullRegIndex = -1;
            }

        *pulAddress = ulAddress;
        *pubMemType = (unsigned char)tyMemType;
        }

    strcpy(pszRegName, szRegName);
    *pbValidReg = TRUE;
    return NO_ERROR;

    BadRegFileLine:
//printf("22\n");
    sprintf(tySI.szError, "Invalid register definition at line %d in device specific register definition file '%s'.", iLineNumber, tySI.tyProcConfig.szSymFile);
    return GDBSERV_CFG_ERROR;
}

/****************************************************************************
     Function: CFG_AddGroupDefArrayEntry
     Engineer: Jiju George
        Input: *pszGroupName   : group name
               iStartRegIndex  : index of first register in this group
               iEndRegIndex    : index of first last in this group
       Output: int
  Description: Add a group definition to the array
Date           Initials    Description
26-Apr-2008    JG          Initial
21-Oct-2009    SVL         Added for Device and User register file support in ARM
                           NOTE: This can be removed from this file if the 
                           same function from gdbxmlcfgmips is moved to gdbxml
*****************************************************************************/
int CFG_AddGroupDefArrayEntry(char           *pszGroupName,
                              int             iStartRegIndex,
                              int             iEndRegIndex,
                              int             iRegDefType
                             )
{
    unsigned int uiGroupNameLen;
    TyGroupDef   *ptyNewGroupDefArray;

    if(tySI.uiGroupDefArrayEntryCount >= tySI.uiGroupDefArrayAllocCount)
        {
        // reallocate array adding a chunk of entries...
        ptyNewGroupDefArray = (TyGroupDef*)realloc(tySI.ptyGroupDefArray, ((tySI.uiGroupDefArrayAllocCount+GDBSERV_REGDEF_ALLOC_CHUNK) * sizeof(TyGroupDef)));
        if(ptyNewGroupDefArray == NULL)
            {
            sprintf(tySI.szError, "Insufficient memory while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
            return GDBSERV_CFG_ERROR;
            }

        // clear new chunk to 0...
        memset(&ptyNewGroupDefArray[tySI.uiGroupDefArrayAllocCount], 0, (GDBSERV_REGDEF_ALLOC_CHUNK * sizeof(TyGroupDef)));

        tySI.ptyGroupDefArray = ptyNewGroupDefArray;
        tySI.uiGroupDefArrayAllocCount += GDBSERV_REGDEF_ALLOC_CHUNK;
        }

    // sanity check, this should not happen...
    if(tySI.uiGroupDefArrayEntryCount >= tySI.uiGroupDefArrayAllocCount)
        {
        sprintf(tySI.szError, "Internal error 1 while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
        return GDBSERV_CFG_ERROR;
        }

    uiGroupNameLen = (unsigned int)strlen(pszGroupName);

    // copy the new register name...
    strncpy(tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].pszGroupName, pszGroupName, uiGroupNameLen);
    tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].pszGroupName[uiGroupNameLen] = '\0';

    tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].iStartRegIndex  = iStartRegIndex;
    tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].iEndRegIndex    = iEndRegIndex;
    tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].iRegDefType    = iRegDefType;

    tySI.uiGroupDefArrayEntryCount++;

    return NO_ERROR;
}

