/****************************************************************************
       Module: gdbxml.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, XML parser header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBXML_H_
#define GDBXML_H_

// Linux build change...
#include "protocol/common/types.h"

#define XML_SPACES_PER_TAB    3
#define XML_TAB               0x09
#define XML_LF                0x0A
#define XML_CR                0x0D

typedef enum
{
   XMLTokenUndefined    = 0,
   XMLTokenLess         = 1,
   XMLTokenGreater      = 2,
   XMLTokenEqual        = 3,
   XMLTokenFSlash       = 4,
   XMLTokenBSlash       = 5,
   XMLTokenMinus        = 6,
   XMLTokenQuestion     = 7,
   XMLTokenExclaim      = 8,
   XMLTokenIdentifier   = 9,
   XMLTokenQuotedString = 10,
   XMLTokenElementText  = 11
} TyXMLToken;

typedef enum
{
   XMLBlockUndefined    = 0,
   XMLBlockInfo         = 1,
   XMLBlockStart        = 2,
   XMLBlockEnd          = 3,
   XMLBlockStartEnd     = 4,
   XMLBlockElementText  = 5,
   XMLBlockDTD          = 6    //Document Type Declaration
} TyXMLBlock;

typedef struct _TyXMLNode
{
   char              *pszNodeName;
   char              *pszElementText;
   TyXMLBlock         tyBlock;
   int32_t                iLine;
   int32_t                iIndent;
   int32_t                iAllocCount;
   int32_t                iAttribCount;
   char             **ppszAttrib;
   char             **ppszValue;
   struct _TyXMLNode  *pParent;
   struct _TyXMLNode  *pChild;
   struct _TyXMLNode  *pNext;
} TyXMLNode;

typedef struct _TyXMLFile
{
   char              szError[_MAX_PATH*2];
   char              szName[_MAX_PATH];
   FILE             *pFile;
   int32_t               iNextChar;
   int32_t               iLine;
   int32_t               iIndent;
   TyXMLNode        *ptyNodeTree;
} TyXMLFile;

// global function prototypes
void XML_Print(TyXMLNode *ptyNode);
void XML_Free(TyXMLFile *ptyFile);
int32_t XML_ReadFile(const char * pszName, TyXMLFile *ptyFile);
TyXMLNode *XML_GetBaseNode(TyXMLFile *ptyFile);
TyXMLNode *XML_GetNode(TyXMLNode *ptyNode, const char *pszNodeName);
TyXMLNode *XML_GetChildNode(TyXMLNode *ptyNode, const char *pszNodeName);
TyXMLNode *XML_GetNextNode(TyXMLNode *ptyNode);
char *XML_GetAttrib(TyXMLNode *ptyNode, const char *pszAttrib);
char *XML_GetElement(TyXMLNode *ptyNode);
char *XML_GetChildAttrib(TyXMLNode *ptyNode, const char *pszNodeName, const char *pszAttrib);
char *XML_GetChildElement(TyXMLNode *ptyNode, const char *pszNodeName);
int32_t XML_GetState(TyXMLNode *ptyNode);
int32_t XML_GetChildState(TyXMLNode *ptyNode, const char *pszNodeName);
int32_t XML_GetChildNodeCount(TyXMLNode *ptyNode, char *pszNodeName);
uint32_t XML_GetElementHex(TyXMLNode *ptyNode, uint32_t ulDefault);
uint32_t XML_GetChildElementHex(TyXMLNode *ptyNode, const char *pszNodeName, uint32_t ulDefault);
uint32_t XML_GetAttribHex(TyXMLNode *ptyNode, char *pszAttrib, uint32_t ulDefault);
uint32_t XML_GetChildAttribHex(TyXMLNode *ptyNode, char *pszNodeName, char *pszAttrib, uint32_t ulDefault);
uint32_t XML_GetElementDec(TyXMLNode *ptyNode, uint32_t ulDefault);
uint32_t XML_GetChildElementDec(TyXMLNode *ptyNode, const char *pszNodeName, uint32_t ulDefault);
uint32_t XML_GetHex(char * pszHex, uint32_t ulDefault);
uint32_t XML_GetDec(char * pszDec, uint32_t ulDefault);
void XML_PrintToFile(TyXMLNode *ptyNode, FILE * pfile);

#endif // GDBXML_H_

