/****************************************************************************
Module: gdbutil.c
Engineer: Vitezslav Hola
Description: Ashling GDB Server, utility functions
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS/VK       Modified for ARM support
17-Oct-2009    SVL         Added functions for parsing and validating arguments
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#ifndef __LINUX
#include <windows.h>
#include <conio.h>
#else
// Linux specific includes
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#endif
#include "gdbglob.h" 
#include "gdberr.h"
#include "gdbxml.h"
#include "gdbty.h"
#include "gdbutil.h"
#include "gdblow.h"
#ifndef __LINUX

#ifdef RISCV
#include "riscv\gdbriscv.h"
#else
#include "arc\gdbarc.h"
#endif 
#else
#ifdef RISCV
#include "gdbriscv.h"
#else
#include "gdbarc.h"
#endif // RISCV

#endif

#define MAX_ULONG_STRING_SIZE       0x80
//#define ARC_REG_PC               (0x46)
#define DEBUG_REG               (0x45)
#define AMV0                        (0x260)

#define AP_DEBUG_ASR_SHIFT                            3
#define AP_DEBUG_ASR_MASK                             (0x00000FF << AP_DEBUG_ASR_SHIFT)

// external variables
extern TyServerInfo tySI;
//extern int32_t LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t *pulRegValue);
//extern int32_t LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t ulRegValue);

// local variables
static int32_t bGlobalDebugModeOn = 0;

/****************************************************************************
Function: MsSleep
Engineer: Vitezslav Hola
Input: uint32_t uiMiliseconds : time to wait
Output: none
Description: Implementation of sleep function to wait given number of miliseconds.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void MsSleep(uint32_t uiMiliseconds)
{
#ifndef __LINUX
	Sleep(uiMiliseconds);
#else
	usleep(uiMiliseconds * 1000);
#endif
}

/****************************************************************************
Function: KeyHit
Engineer: Vitezslav Hola
Input: none
Output: int32_t : return value
Description: Check if any key has been hit.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t KeyHit(void)
{
#ifndef __LINUX
	return _kbhit();
#else
	static const int32_t STDIN = 0;
	static bool initialized = false;
	int32_t bytesWaiting;
	if (! initialized)
	{
		// Use termios to turn off line buffering
		termios term;
		tcgetattr(STDIN, &term);
		term.c_lflag &= ~ICANON;
		tcsetattr(STDIN, TCSANOW, &term);
		setbuf(stdin, NULL);
		initialized = true;
	}
	ioctl(STDIN, FIONREAD, &bytesWaiting);
	return bytesWaiting;
#endif
}

/****************************************************************************
Function: GetchWithoutEcho
Engineer: Vitezslav Hola
Input: none
Output: int32_t : read character
Description: return character from input withou echoing it.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t GetchWithoutEcho(void)
{
#ifndef __LINUX
	return _getch();
#else
	struct termios oldt, newt;
	int32_t ch;
	tcgetattr( STDIN_FILENO, &oldt );
	newt = oldt;
	newt.c_lflag &= ~( ICANON | ECHO );
	tcsetattr( STDIN_FILENO, TCSANOW, &newt );
	ch = getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
	return ch;
#endif
}

/****************************************************************************
Function: StringToUpr
Engineer: Vitezslav Hola
Input: char *pszString : string to convert
Output: none
Description: Convert all characters in string to upper case.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringToUpr(char *pszString)
{
	assert(pszString != NULL);
	while (*pszString)
	{
		if ((*pszString >= 'a') && (*pszString <='z'))
			*pszString -= 'a'-'A';
		pszString++;
	}
}

/****************************************************************************
Function: StringToLwr
Engineer: Vitezslav Hola
Input: char *pszString : string to convert
Output: none
Description: Convert all characters in string to lower case.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringToLwr(char *pszString)
{
	assert(pszString != NULL);
	while (*pszString)
	{
		if ((*pszString >= 'A') && (*pszString <='Z'))
			*pszString += 'a'-'A';
		pszString++;
	}
}

/****************************************************************************
Function: SwapUlongBytes
Engineer: Vitezslav Hola
Input: uint32_t ulData : data to swap
Output: uint32_t        : swapped data
Description: Swap bytes in word.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t SwapUlongBytes(uint32_t ulData)
{
	uint32_t ulSwapped = 0;
	ulSwapped += ((ulData << 24) & 0xFF000000);
	ulSwapped += ((ulData <<  8) & 0x00FF0000);
	ulSwapped += ((ulData >>  8) & 0x0000FF00);
	ulSwapped += ((ulData >> 24) & 0x000000FF);
	return ulSwapped;
}

/****************************************************************************
Function: SwapUshortBytes
Engineer: Vitezslav Hola
Input: unsigned short usData : data to swap
Output: unsigned short        : swapped data
Description: Swap bytes in halfword.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
unsigned short SwapUshortBytes(unsigned short usData)
{
	unsigned short usSwapped = 0;
	usSwapped += ((usData <<  8) & 0xFF00);
	usSwapped += ((usData >>  8) & 0x00FF);
	return usSwapped;
}

/****************************************************************************
Function: ToHex
Engineer: Vitezslav Hola
Input: iNibble : 4 bit value
Output: int32_t     : hex character representing the iNibble
Description: Convert value to hex
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t ToHex(int32_t iNibble)
{
	iNibble &= 0xF;
	if (iNibble < 10)
		return iNibble + '0';
	else
		return iNibble + 'a' - 10;
}

/****************************************************************************
Function: FromHex
Engineer: Vitezslav Hola
Input: iChar : hex character
Output: int32_t   : value represented by the hex char
Description: Convert character to hex value.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t FromHex(int32_t iChar)
{
	if (iChar >= '0' && iChar <= '9')
		return iChar - '0';
	else if (iChar >= 'A' && iChar <= 'F')
		return iChar - 'A' + 10;
	else if (iChar >= 'a' && iChar <= 'f')
		return iChar - 'a' + 10;
	else
		return -1;
}

/****************************************************************************
Function: IsDec
Engineer: Sandeep V L
Input: iChar : hex character
Output: int32_t   : -1, On Error (chosen to be in match with FromHex)
: TRUE, On Success
Description: Checks whether the given character is a decimal 
Date           Initials    Description
17-Oct-2009    SVL          Initial
*****************************************************************************/

int32_t IsDec(int32_t iChar)
{
	if ((iChar < '0') || (iChar > '9'))
	{
		return -1;
	}
	return TRUE;
}

/****************************************************************************
Function: ExStrToUlong
Engineer: Vitezslav Hola
Input: char *pszValue : string value
int32_t *pbValid   : storage for flag if value is valid
Output: uint32_t  : value represented by the string
Description: Converts string to int32_t supports decimal, decimal ending in 't' 
and hex when the string value begins with 0x.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t ExStrToUlong(char *pszValue, int32_t *pbValid)
{
	int32_t iLen = (int32_t)strlen(pszValue);
	assert(pszValue != NULL);
	if (iLen > 0)
	{
		if ((pszValue[iLen-1] == 't') || (pszValue[iLen-1] == 'T'))
			pszValue[iLen-1] = '\0';
	}
	return StrToUlong(pszValue, pbValid);
}

/****************************************************************************
Function: StrToUlong
Engineer: Vitezslav Hola
Input: char *pszValue    : string value
int32_t *pbValid      : storage for flag if value is valid
Output: uint32_t     : value represented by the string
Description: Converts string to int32_t supports decimal and hex 
when the string value begins with 0x.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t StrToUlong(char *pszValue, int32_t *pbValid)
{
	uint32_t ulValue;
	char *pszStop = NULL;
	assert(pszValue != NULL);
	// dont allow octal, so skip leading '0's unless its 0x...
	while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] != 'X') && (pszValue[1] != '\0'))
		pszValue++;
	// strtoul supports decimal nnnn and hex 0xnnnn...
	ulValue = strtoul(pszValue, &pszStop, 0);
	// only valid if all characters were processed ok...
	if ((pszStop) && (pszStop[0] == '\0'))
	{
		if (pbValid)
			*pbValid = 1;
		return ulValue;
	}
	else
	{
		if (pbValid)
			*pbValid = 0;
		return 0;
	}
}

/****************************************************************************
Function: IsSpace
Engineer: Vitezslav Hola
Input: int32_t iChar : character to test
Output: int32_t : 1 if character is space, 0 otherwise
Description: Check if character is space.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t IsSpace(int32_t iChar)
{
	return((iChar == ' ') || ((iChar >= '\t') && (iChar <= 13)));
}

/****************************************************************************
Function: IsHexChar
Engineer: Vitezslav Hola
Input: int32_t iChar        : character to test
Output: int32_t : 1 if character is hex, 0 otherwise
Description: Check if character is hex.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t IsHexChar(int32_t iChar)
{
	return(((iChar >= '0') && (iChar <= '9')) || ((iChar >= 'a') && (iChar <= 'z')) || ((iChar >= 'A') && (iChar <= 'Z')));
}

/****************************************************************************
Function: UlongToString
Engineer: Vitezslav Hola
Input: uint32_t ulValue : value to convert
char *pszString       : output string
Output: none
Description: Converts an uint32_t to a string
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void UlongToString(uint32_t ulValue, char *pszString)
{
	assert(pszString != NULL);
	sprintf(pszString, "%8.8x", ulValue);
}

/****************************************************************************
Function: StringToUlong
Engineer: Vitezslav Hola
Input: char *pszString          : input string
uint32_t *pulValue  : pointer to value
Output: none
Description: Converts a string to an uint32_t
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringToUlong(char *pszString, uint32_t *pulValue)
{
	assert(pszString != NULL);
	assert(pulValue != NULL);
	*pulValue = 0;
	while (*pszString != '\0')
	{
		*pulValue <<= 4;
		*pulValue |= FromHex(*pszString++) & 0xF;
	}
}

/****************************************************************************
Function: UCharToString
Engineer: Vitezslav Hola
Input: unsigned char ucValue : value to convert
char *pszString       : converted string
Output: 
Description: Converts an unsigned char to a string
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void UCharToString(unsigned char ucValue, char *pszString)
{
	assert(pszString != NULL);
	sprintf(pszString, "%2.2x", ucValue);
}

/****************************************************************************
Function: StringToUChar
Engineer: Vitezslav Hola
Input: char *pszString          : input string
unsigned char *pucValue  : pointer to converted value
Output: none
Description: Converts a string to an unsigned char
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringToUChar(char *pszString, unsigned char *pucValue)
{
	assert(pszString != NULL);
	assert(pucValue != NULL);
	*pucValue = 0;
	while (*pszString != '\0')
	{
		*pucValue <<= 4;
		*pucValue |= FromHex(*pszString++) & 0xF;
	}
}

/****************************************************************************
Function: RegistersToStrings
Engineer: Vitezslav Hola
Input: uint32_t *pulRegisters : pointer to register
char *pszOutput             : output string
uint32_t ulCount       : count
Output: none
Description: Converts register values into an output string
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void RegistersToStrings(uint32_t *pulRegisters, char *pszOutput, uint32_t ulCount)
{
	uint32_t ulIndex;
	int32_t bBigEndian = LOW_IsBigEndian(0);
	assert(pulRegisters != NULL);
	assert(pszOutput != NULL);
	for (ulIndex=0; ulIndex < ulCount; ulIndex++)
	{
		if (!bBigEndian)
			pulRegisters[ulIndex] = SwapUlongBytes(pulRegisters[ulIndex]);// if processor is LE, we must swap value for GDB!!!
		UlongToString(pulRegisters[ulIndex], &pszOutput[ulIndex * 8]);
	}
}

/****************************************************************************
Function: StringsToRegisters
Engineer: Vitezslav Hola
Input: char *pszInput              : input string
uint32_t *pulRegisters : pointer to register
uint32_t ulCount       : count
Output: none
Description: Converts an input string into register values
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringsToRegisters(char *pszInput, uint32_t *pulRegisters, uint32_t ulCount)
{
	uint32_t ulIndex;
	char szRegisterValue[MAX_ULONG_STRING_SIZE];
	int32_t bBigEndian = LOW_IsBigEndian(0);
	assert(pszInput != NULL);
	assert(pulRegisters != NULL);
	// TO_DO: For the write register function, it appears that GDB sends
	// down the data in the following format
	//
	//  AAAAAAAABBBBBBBBCCCCCCCCDDDDDDDD....
	// where A's are the 8 ascii values for the first    register,
	// "  B's  "   "  "   "      "    "   "  second      "    ,
	// "  C's  "   "  "   "      "    "   "  third       "    ,
	// etc, etc.
	// Therefore the following piece of code extracts the ascii values needed
	// for (the current register it's working on , appends a '\0' character
	// and passes this onto the StringToUlong function.
	// It would be nicer if GDB sent down the data for each register with
	// a comma in between, but our's is not to reason why....
	for (ulIndex=0; ulIndex < ulCount; ulIndex++)
	{
		memcpy(szRegisterValue, &pszInput[ulIndex * 8], 8);
		szRegisterValue[8] = '\0';
		StringToUlong(szRegisterValue, &pulRegisters[ulIndex]);
		if (!bBigEndian)
		{ // if processor is LE, must swap register values from GDB!!!
			pulRegisters[ulIndex] = SwapUlongBytes(pulRegisters[ulIndex]);
		}
	}
}

/****************************************************************************
Function: StringsToRegisterAndValue
Engineer: Vitezslav Hola
Input: char *pszInput                   : input string
uint32_t *pulRegisterNumber : pointer to register number
uint32_t *pulRegisterValue  : pointer to register value
Output: none
Description: Converts an input string into register number and register value
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringsToRegisterAndValue(char *pszInput, 
							   uint32_t *pulRegisterNumber, 
							   uint32_t *pulRegisterValue)
{
	uint32_t ulIndex;
	char szRegisterNumber[MAX_ULONG_STRING_SIZE];
	char szRegisterValue[MAX_ULONG_STRING_SIZE];
	int32_t bBigEndian = LOW_IsBigEndian(0);
	assert(pszInput != NULL);
	assert(pulRegisterNumber != NULL);
	assert(pulRegisterValue != NULL);

#ifdef RISCV
	char szPtemp[9];
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();

	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szRegisterNumber[ulIndex] = *pszInput++;
		if (szRegisterNumber[ulIndex] == '=')
		{
			szRegisterNumber[ulIndex] = '\0';
			break;
		}
		if (szRegisterNumber[ulIndex] == ':')
		{
			szRegisterNumber[ulIndex] = '\0';
			while ((*pszInput != '\0') && (*pszInput != '='))
				pszInput++;
			if (*pszInput == '=')
				pszInput++;
			break;
		}
	}
	szRegisterNumber[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szRegisterValue[ulIndex] = *pszInput++;
		if (szRegisterValue[ulIndex] == '\0')
			break;
	}
	szRegisterValue[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szRegisterNumber, pulRegisterNumber);
	for (ulIndex = 0; ulIndex < (tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32); ulIndex++)
	{
		strncpy(szPtemp, szRegisterValue, 8);
		szPtemp[8] = '\0';
		StringToUlong(szPtemp, &pulRegisterValue[ulIndex]);
		memmove(szRegisterValue, szRegisterValue + 8, 8);
		if (!bBigEndian)
		{ // if processor is LE, must swap register values from GDB !!!
			pulRegisterValue[ulIndex] = SwapUlongBytes(pulRegisterValue[ulIndex]);
		}
		//pulRegisterValue++;
	}
#else
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szRegisterNumber[ulIndex] = *pszInput++;
		if (szRegisterNumber[ulIndex] == '=')
		{
			szRegisterNumber[ulIndex] = '\0';
			break;
		}
		if (szRegisterNumber[ulIndex] == ':')
		{
			szRegisterNumber[ulIndex] = '\0';
			while ((*pszInput != '\0') && (*pszInput != '='))
				pszInput++;
			if (*pszInput == '=')
				pszInput++;
			break;
		}
	}
	szRegisterNumber[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szRegisterValue[ulIndex] = *pszInput++;
		if (szRegisterValue[ulIndex] == '\0')
			break;
	}
	szRegisterValue[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szRegisterNumber, pulRegisterNumber);
	StringToUlong(szRegisterValue,  pulRegisterValue);
	if (!bBigEndian)
	{ // if processor is LE, must swap register values from GDB !!!
		*pulRegisterValue = SwapUlongBytes(*pulRegisterValue);
	}
#endif
}

/****************************************************************************
Function: StringsToAddressAndCount
Engineer: Vitezslav Hola
Input: char *pszInput            : input string
uint32_t *pulAddress : pointer to address
uint32_t *pulCount   : pointer to count
Output: none
Description: Converts an input string into an address and count value.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringsToAddressAndCount(char *pszInput, uint32_t *pulAddress, uint32_t *pulCount)
{
	uint32_t ulIndex;
	char szAddress[MAX_ULONG_STRING_SIZE];
	char szCount[MAX_ULONG_STRING_SIZE];
	assert(pszInput != NULL);
	assert(pulAddress != NULL);
	assert(pulCount != NULL);
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szAddress[ulIndex] = *pszInput++;
		if (szAddress[ulIndex] == ',')
		{
			szAddress[ulIndex] = '\0';
			break;
		}
	}
	szAddress[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szCount[ulIndex] = *pszInput++;
		if (szCount[ulIndex] == '\0')
			break;
	}
	szCount[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szAddress, pulAddress);
	StringToUlong(szCount, pulCount);
}

/****************************************************************************
Function: MemoryValuesToStrings
Engineer: Vitezslav Hola
Input: unsigned char *pucData : pointer to data
char *pszOutput        : output string
uint32_t ulCount  : count
Output: none
Description: Converts memory values into strings
value
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void MemoryValuesToStrings(unsigned char *pucData, char *pszOutput, uint32_t ulCount)
{
	uint32_t ulIndex;
	assert(pucData != NULL);
	assert(pszOutput != NULL);
	// Following code assumes that unsigned charToString will always add 2 ascii digits for each unsigned char value. 
	// Also assumes that it will a '\0' after the ascii values. This '\0' will get overwrote each time, except for the last unsigned char value.
	for (ulIndex = 0; ulIndex < ulCount; ulIndex++)
		UCharToString(*pucData++, &pszOutput[ulIndex*2]);
}

/****************************************************************************
Function: StringsToAddressCountAndData
Engineer: Vitezslav Hola
Input: char *pszInput            : input string
uint32_t *pulAddress : pointer to address
uint32_t *pulCount   : pointer to count
unsigned char *pucData    : pointer to data
uint32_t ulDataSize  : data size
Output: none
Description: Converts an address and count value into strings value
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringsToAddressCountAndData(char *pszInput, uint32_t *pulAddress, uint32_t *pulCount, unsigned char *pucData, uint32_t ulDataSize)
{
	uint32_t ulIndex;
	char szAddress[MAX_ULONG_STRING_SIZE];
	char szCount[MAX_ULONG_STRING_SIZE];
	char szData[MAX_ULONG_STRING_SIZE];
	assert(pszInput != NULL);
	assert(pulAddress != NULL);
	assert(pulCount != NULL);
	assert(pucData != NULL);
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szAddress[ulIndex] = *pszInput++;

		if (szAddress[ulIndex] == ',')
		{
			szAddress[ulIndex] = '\0';
			break;
		}
	}
	szAddress[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szCount[ulIndex] = *pszInput++;
		if (szCount[ulIndex] == ':')
		{
			szCount[ulIndex] = '\0';
			break;
		}
	}
	szCount[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szAddress, pulAddress);
	StringToUlong(szCount, pulCount);
	// truncate to buffer size
	if (*pulCount > ulDataSize)
		*pulCount = ulDataSize;
	for (ulIndex=0; ulIndex < *pulCount; ulIndex++)
	{
		szData[0] = *pszInput++;
		szData[1] = *pszInput++;
		szData[2] = '\0';
		StringToUChar(szData, &pucData[ulIndex]);
	}
}

/****************************************************************************
Function: StringsToBpTypeAddressAndLength
Engineer: Vitezslav Hola
Input: char *pszInput            : input string
uint32_t *pulBpType  : pointer to breakpoint type
uint32_t *pulAddress : pointer to address
uint32_t *pulLength  : pointer to length
Output: none
Description: Converts strings into a BP type, address and length value.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void StringsToBpTypeAddressAndLength(char *pszInput, uint32_t *pulBpType, uint32_t *pulAddress, uint32_t *pulLength)
{
	uint32_t ulIndex;
	char szBpType[MAX_ULONG_STRING_SIZE];
	char szAddress[MAX_ULONG_STRING_SIZE];
	char szLength[MAX_ULONG_STRING_SIZE];
	assert(pszInput != NULL);
	assert(pulBpType != NULL);
	assert(pulAddress != NULL);
	assert(pulLength != NULL);
	for (ulIndex = 0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szBpType[ulIndex] = '\0';
			break;
		}
		szBpType[ulIndex] = *pszInput++;
		if (szBpType[ulIndex] == ',')
		{
			szBpType[ulIndex] = '\0';
			break;
		}
	}
	szBpType[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szAddress[ulIndex] = '\0';
			break;
		}
		szAddress[ulIndex] = *pszInput++;
		if (szAddress[ulIndex] == ',')
		{
			szAddress[ulIndex] = '\0';
			break;
		}
	}
	szAddress[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szLength[ulIndex] = '\0';
			break;
		}
		szLength[ulIndex] = *pszInput++;
	}
	szLength[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szBpType, pulBpType);
	StringToUlong(szAddress, pulAddress);
	StringToUlong(szLength, pulLength);
}

/****************************************************************************
Function: StopSignalToString
Engineer: Vitezslav Hola
Input: TyStopSignal tyStopSignal : stop signal
char *pszOutput           : pointer to output string
Output: none
Description: Converts a stop signal into a string
Date           Initials    Description
08-Feb-2008    VH          Initial
08-Nov-2011    SPC         Defined new packet for MIPS processors
16-Mar-2016    ST          Defined packet for ARC processors
*****************************************************************************/
void StopSignalToString(TyStopSignal tyStopSignal, char *pszOutput)
{
	
	assert(pszOutput != NULL);

#ifdef ARM
	*pszOutput = 'S';
	UCharToString((unsigned char)tyStopSignal, &pszOutput[1]);
#elif ARC 
	uint32_t debug_reg;
	if (Is_StoppedAtWatchpoint() == 1)
	{
		uint32_t ASR;
		uint32_t apindex = 0;
		uint32_t amvreg;
		char temp[256];

		(void)LOW_ReadRegister(DEBUG_REG, &debug_reg);

		ASR = (debug_reg & AP_DEBUG_ASR_MASK) >> AP_DEBUG_ASR_SHIFT;

		//printf("DEBUG_REG = %x ASR = %x\n", DEBUG_REG, ASR);
		while (ASR != 0)
		{
			if (ASR & 0x1)
			{
				break;
			}
			else
			{
				apindex++;
				ASR = ASR >> 1;
			}
		} 

		(void)LOW_ReadRegister(AMV0 + apindex * 3, &amvreg);

		//printf("amvreg = %x\n", amvreg);
		*pszOutput = 'T';
		UCharToString((unsigned char)tyStopSignal, &pszOutput[1]);

		sprintf(temp, "watch:%x;", amvreg);
		strcat(pszOutput, temp);

		//printf("%s\n", pszOutput);
	}
	else
	{
		*pszOutput = 'S';
		UCharToString((unsigned char)tyStopSignal, &pszOutput[1]);
	}
#elif RISCV
	char th[5];
	uint32_t ulThreadId;
	ulThreadId = LOW_CurrentHart();
	ulThreadId = ulThreadId + 1;
	*pszOutput = 'T';
	/*if (tyStopSignal == TARGET_SIGINT)
		tyStopSignal = TARGET_SIGTRAP;*/
	UCharToString((unsigned char)tyStopSignal, &pszOutput[1]);
	if (ulThreadId / 10 == 0)
	{
		strcat(pszOutput, "thread:0");
	}
	else
	{
		strcat(pszOutput, "thread:");
	}
	sprintf(th,"%lu;",ulThreadId);
	strcat(pszOutput, th);
#else
	*pszOutput = 'T';
	if (tyStopSignal == TARGET_SIGINT)
		tyStopSignal = TARGET_SIGTRAP;
	UCharToString((unsigned char)tyStopSignal, &pszOutput[1]);
	strcat(pszOutput,"thread:01;");
#endif
}

/****************************************************************************
Function: CheckNumberIsPresent
Engineer: Vitezslav Hola
Input: char *pszInput           : input string
uint32_t *pulNumber : pointer to number
Output: int32_t : 1 if present, 0 otherwise
Description: Checks if an number is present in the input string
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t CheckNumberIsPresent(char *pszInput, uint32_t *pulNumber)
{
	uint32_t ulIndex;
	char szNumber[MAX_ULONG_STRING_SIZE];
	assert(pulNumber != NULL);
	if (pszInput[0] == '\0')
		return 0;
	for (ulIndex = 0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szNumber[ulIndex] = *pszInput++;
		if (szNumber[ulIndex] == '\0')
			break;
		if (!IsHexChar(szNumber[ulIndex]))
		{
			szNumber[ulIndex] = '\0';
			break;
		}
	}
	szNumber[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szNumber, pulNumber);
	return 1;
}

/****************************************************************************
Function: LogMessage
Engineer: Vitezslav Hola
Input: char *pszSendRecv : send or receive string
char *pszData     : data to log
Output: none
Description: Logging message.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void LogMessage(const char *pszSendRecv, const char *pszData)
{
	if (tySI.bDebugStdout)
		printf("%s %s\n", pszSendRecv, pszData);
	if (tySI.pDebugFile != NULL)
	{
		fprintf(tySI.pDebugFile, "%s %s\n", pszSendRecv, pszData);
		fflush(tySI.pDebugFile);
	}
}

/****************************************************************************
Function: PrintMessage
Engineer: Vitezslav Hola
Input: TyMessageType tyMessageType : type of message (debug, log, etc.)
char * pszToFormat          : format of message (like printf) + parameters
Output: none
Description: Prints out messages to console, debug window, etc...
Date           Initials    Description
08-Feb-2008    VH          Initial
30-Jul-2008    NCH         Last error added
10-Nov-2009    SVL         Fixed a bug - 'Remote Packet too int32_t'
10-May-2012    SPT         
*****************************************************************************/
void PrintMessage(TyMessageType tyMessageType, const char * pszToFormat, ...)
{
	/* NOTE: The macro '_MAX_PATH'(260) was replaced with 
	'GDBSERV_PACKET_BUFFER_SIZE'(0x10000) for fixing the bug 
	'Remote Packet too long' in the case of 'monitor getpregs'. Even though
	the GDBSERVER Packet can hold 65536(0x10000) bytes, this loop will
	terminate when 'uiSize' reaches '_MAX_PATH'. */
	//char szBuffer[_MAX_PATH*8];
	char *szBuffer = (char*)malloc(GDBSERV_PACKET_BUFFER_SIZE*8);
	//char szDisplay[_MAX_PATH*8];
	char *szDisplay = (char*)malloc(GDBSERV_PACKET_BUFFER_SIZE*8);
	va_list  marker; // = NULL;

	if((szBuffer == NULL) ||
		(szDisplay == NULL))
	{
		return;
	}

#ifndef DEBUG
	if (tyMessageType == DEBUG_MESSAGE)
	{
		free(szBuffer);
		free(szDisplay);
		return;
	}
#endif

	va_start(marker, pszToFormat);
	(void)vsprintf(szBuffer, pszToFormat, marker); 
	va_end(marker);
	switch (tyMessageType)
	{
	case HELP_MESSAGE:
		strcpy(szDisplay, szBuffer);
		break;
	case INFO_MESSAGE:
		sprintf(szDisplay, "%s\n", szBuffer);
		strcpy(tySI.szLastError,szDisplay);
		break;
	case ERROR_MESSAGE:
		sprintf(szDisplay, "Error: %s\n", szBuffer);
		strcpy(tySI.szLastError,szDisplay);
		break;
	case DEBUG_MESSAGE:
		sprintf(szDisplay, "Debug: %s\n", szBuffer);
		break;
	case LOG_MESSAGE:
		LogMessage(szBuffer, "");
		free(szBuffer);
		free(szDisplay);
		return;
	default:
		free(szBuffer);
		free(szDisplay);
		return;
	}
	fprintf(stdout, "%s", szDisplay);
	fflush(stdout);

	if (tySI.pDebugFile != NULL)
	{
		fprintf(tySI.pDebugFile, "%s", szDisplay);
		fflush(tySI.pDebugFile);
	}

	free(szBuffer);
	free(szDisplay);
}

/****************************************************************************
Function: LogTitleAndVersion
Engineer: Vitezslav Hola
Input: none
Output: none
Description: Log title and version into debug file.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void LogTitleAndVersion(void)
{
	if (tySI.pDebugFile != NULL)
	{
		fprintf(tySI.pDebugFile, "%s\n", GDBSERV_PROG_TITLE);
		fprintf(tySI.pDebugFile, "%s\n", GDBSERV_PROG_VERSION);
		fflush(tySI.pDebugFile);
	}
}

/****************************************************************************
Function: ToggleDebugModeFlag
Engineer: Vitezslav Hola
Input: none
Output: none
Description: Toggle debug mode flag.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void ToggleDebugModeFlag(void)
{
	bGlobalDebugModeOn = !bGlobalDebugModeOn;
}

/****************************************************************************
Function: StringToMemType
Engineer: Vitezslav Hola
Input: char *pszMemType : pointer to memory type string
Output: TyMemoryTypes    : type of memory
Description: get memory type from string as supported in .REG file mem = xxx entries.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
TyMemTypes StringToMemType(char *pszMemType)
{
	if (strcasecmp(pszMemType, "reg") == 0)
		return MEM_REG;
	else if (strcasecmp(pszMemType, "code") == 0)
		return MEM_CODE;
	else if (strcasecmp(pszMemType, "xdata") == 0)
		return MEM_XDATA;
	else if (strcasecmp(pszMemType, "sfr") == 0)
		return MEM_SFR;
	else if (strcasecmp(pszMemType, "esfr") == 0)
		return MEM_ESFR;
	else if (strcasecmp(pszMemType, "ram") == 0)
		return MEM_RAM;
	else if (strcasecmp(pszMemType, "phy") == 0)
		return MEM_PHYSICL;
	else
		return MEM_INVALID;
}

/****************************************************************************
Function: ExtractRegBits
Engineer: Vitezslav Hola
Input: uint32_t ulRegValue : value from which to extract bits
TyRegDef *ptyRegDef      : register definition 
Output: uint32_t : extracted bits
Description: extract bits from a reg value
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t ExtractRegBits(uint32_t ulRegValue, TyRegDef *ptyRegDef)
{
	assert(ptyRegDef != NULL);
	if (ptyRegDef->ubBitSize >= 32)
		return ulRegValue;
	if (ptyRegDef->ubBitOffset > 0)
		ulRegValue >>= ptyRegDef->ubBitOffset;
	ulRegValue &= ((1<<ptyRegDef->ubBitSize) -1);
	return ulRegValue;
}

/****************************************************************************
Function: MergeRegBits
Engineer: Vitezslav Hola
Input: uint32_t ulRegValue : value into which to merge bits
uint32_t ulBitValue : the bit value to merge
TyRegDef *ptyRegDef      : register definition 
Output: uint32_t : merged bits
Description: merge bits into a reg value
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t MergeRegBits(uint32_t ulRegValue, uint32_t ulBitValue, TyRegDef *ptyRegDef)
{
	uint32_t ulBitMask;
	assert(ptyRegDef != NULL);
	if (ptyRegDef->ubBitSize >= 32)
		return ulBitValue;
	ulBitMask = (1<<ptyRegDef->ubBitSize) -1;
	if (ptyRegDef->ubBitOffset > 0)
	{
		ulBitMask  <<= ptyRegDef->ubBitOffset;
		ulBitValue <<= ptyRegDef->ubBitOffset;
	}
	ulBitValue &= ulBitMask;      // zero all but the new bits to be merged
	ulRegValue &= (~ulBitMask);   // zero the bits corresponding to this register
	ulRegValue |= ulBitValue;     // or in the new bits
	return ulRegValue;
}

/****************************************************************************
Function: GetRegDefForName
Engineer: Vitezslav Hola
Input: char *pszRegName      : register name
Output: TyRegDef * : pointer to register (NULL if invalid name)
Description: Get register definition for a given name
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
TyRegDef *GetRegDefForName(const char *pszRegName)
{
	uint32_t uiIndex;
	for (uiIndex=0; uiIndex < tySI.uiRegDefArrayEntryCount; uiIndex++)
	{
		if (strcasecmp(tySI.ptyRegDefArray[uiIndex].pszRegName, pszRegName) == 0)
			return &(tySI.ptyRegDefArray[uiIndex]);
	}
	return NULL;
}

/****************************************************************************
Function: GetRegDefForIndex
Engineer: Vitezslav Hola
Input: int32_t iIndex - index in register definition array
Output: TyRegDef * - pointer to register (NULL if invalid index)
Description: Get register definition for a given index
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
TyRegDef *GetRegDefForIndex(int32_t iIndex)
{
	if ((iIndex < 0) || ((uint32_t)iIndex >= tySI.uiRegDefArrayEntryCount))
		return NULL;
	return &(tySI.ptyRegDefArray[iIndex]);
}
#if 0000
/****************************************************************************
Function: ReadRegisterByName
Engineer: Vitezslav Hola
Input: char *pszRegName            : register name
uint32_t *pulRegValue  : storage for register value
Output: int32_t - 1 for successful read, 0 otherwise
Description: Reads a register by name
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int32_t ReadRegisterByName(char *pszRegName, uint32_t *pulRegValue)
{
	TyRegDef *ptyRegDef;
	if ((ptyRegDef = GetRegDefForName(pszRegName)) == NULL)
	{
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE, "Read register %s ...", pszRegName);
		PrintMessage(ERROR_MESSAGE, "Unknown register '%s'.", pszRegName);
		return 0;
	}
	return LOW_ReadRegisterByRegDef(ptyRegDef, pulRegValue);
}

/****************************************************************************
Function: WriteRegisterByName
Engineer: Vitezslav Hola
Input: char *pszRegName          : register name
uint32_t ulRegValue  : register value to write
Output: int32_t : 1 for successful write, 0 otherwise
Description: Writes a register by name
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int32_t WriteRegisterByName(char *pszRegName, uint32_t ulRegValue)
{
	TyRegDef *ptyRegDef;
	if ((ptyRegDef = GetRegDefForName(pszRegName)) == NULL)
	{
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE, "Write register %s value=0x%08X.", pszRegName, ulRegValue);
		PrintMessage(ERROR_MESSAGE, "Unknown register '%s'.", pszRegName);
		return 0;
	}
	return LOW_WriteRegisterByRegDef(ptyRegDef, ulRegValue);
}
#endif
/****************************************************************************
Function: GetRegDefDescription
Engineer: Vitezslav Hola
Input: TyRegDef *ptyRegDef : pointer to register definition
Output: char * : description string
Description: Return a string description of the register
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
const char *GetRegDefDescription(TyRegDef *ptyRegDef)
{
	static char szDescription[_MAX_PATH];
	char szRegName[_MAX_PATH];
	TyRegDef *ptyFullRegDef;
	if (ptyRegDef == NULL)
		return "Internal error getting register description.";
	if ((ptyFullRegDef = GetRegDefForIndex(ptyRegDef->iFullRegIndex)) != NULL)
		sprintf(szRegName, "%s.%s", ptyFullRegDef->pszRegName, ptyRegDef->pszRegName);
	else
		strcpy(szRegName, ptyRegDef->pszRegName);
	if ((TyMemTypes)ptyRegDef->ubMemType == MEM_REG)
	{
		if (ptyRegDef->iFullRegIndex < 0)
			strcpy(szDescription, szRegName);
		else if (ptyRegDef->ubBitSize <= 1)
			sprintf(szDescription, "%s (bit %d)", szRegName, ptyRegDef->ubBitOffset);
		else
			sprintf(szDescription, "%s (bits %d..%d)", szRegName, ptyRegDef->ubBitOffset + ptyRegDef->ubBitSize -1, ptyRegDef->ubBitOffset);
	}
	else
	{
		if (ptyRegDef->iFullRegIndex < 0)
			sprintf(szDescription, "%s at 0x%08X (length %d)", szRegName, ptyRegDef->ulAddress, ptyRegDef->ubByteSize);
		else if (ptyRegDef->ubBitSize <= 1)
			sprintf(szDescription, "%s at 0x%08X (bit %d)", szRegName, ptyRegDef->ulAddress, ptyRegDef->ubBitOffset);
		else
			sprintf(szDescription, "%s at 0x%08X (bits %d..%d)", szRegName, ptyRegDef->ulAddress, ptyRegDef->ubBitOffset + ptyRegDef->ubBitSize -1, ptyRegDef->ubBitOffset);
	}
	return szDescription;
}

/****************************************************************************
Function: AddStringToOutAsciiz
Engineer: Nikolay Chokoev
Input: *szBuffer  : input buffer
*pcOutput  : output bufffer
Output: none
Description: Convert input string to ASCIIz and paste to the end of the
output string. I assumpts that the output string is null 
terminated string.
Date           Initials    Description
11-Feb-2008    NCH         Initial
10-Nov-2009    SVL         Fixed a bug - 'Remote Packet too int32_t'
15-Dec-2009	   RS		   Output packet size increased to 0x10000
****************************************************************************/
void AddStringToOutAsciiz(const char *szBuffer, char *pcOutput)
{
	char *pcTempOut;
	uint32_t uiSize=0;
	uint32_t uiOutSize;

	pcTempOut=pcOutput;
	uiOutSize= (uint32_t)strlen(pcOutput);
	pcTempOut+=uiOutSize;
	while ((szBuffer[uiSize] != '\0') && 
		(uiOutSize < GDBSERV_PACKET_BUFFER_SIZE-1) &&
		/* NOTE: The macro '_MAX_PATH'(260) was replaced with 
		'GDBSERV_PACKET_BUFFER_SIZE'(0x10000) for fixing the bug 
		'Remote Packet too int32_t' in the case of 'monitor getpregs'. Even though
		the GDBSERVER Packet can hold 65536(0x10000) bytes, this loop will
		terminate when 'uiSize' reaches '_MAX_PATH'. */
		/* (uiSize < _MAX_PATH-1) */
		(uiSize < GDBSERV_PACKET_BUFFER_SIZE-1))
	{
		*(pcTempOut)++ = (char)ToHex(((szBuffer[uiSize]) & 0xF0)>>4); 
		*(pcTempOut)++ = (char)ToHex((szBuffer[uiSize]) & 0x0F); 
		uiSize++;
		uiOutSize++;
	}
	*(pcTempOut) = '\0';
}

#ifdef ARM
/****************************************************************************
Function: StringsToWpModeSizeAddressData
Engineer: Venkitakrishnan K
Input: char *pszInput               : input string
char *pszWpMode              : pointer to Watchpoint Mode(Write/Read/Dont Care)
char *pszSize                : pointer to watchpoint size(Word/Half/Byte)
uint32_t *pulAddress    : pointer to address
uint32_t *pulData       : pointer to data
Output: None
Description: Converts string to WP mode,size,Address and data
value
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
void StringsToWpModeSizeAddressandData  (char *pszInput,
										 char *pszWpMode,
										 char *pszSize,
										 uint32_t *pulAddress,
										 uint32_t *pulData)
{
	uint32_t ulIndex;
	uint32_t ulCount;
	uint32_t ulValidHex=2;
	char szWpMode[100];
	char szSize[100];
	char szAddress[MAX_ULONG_STRING_SIZE];
	char szData[MAX_ULONG_STRING_SIZE];
	ulCount=0;
	tySI.bInvalid=0;

	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szWpMode[ulCount] = '\0';
			break;
		}
		if (*pszInput == ' ')
			pszInput++;
		else
		{
			szWpMode[ulCount++] = *pszInput++;
			szWpMode[ulCount] = '\0';
			break;
		}     
	}   
	szWpMode[MAX_ULONG_STRING_SIZE-1] = '\0';
	*pszWpMode++=*szWpMode;
	*pszWpMode='\0';
	ulCount=0;
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szSize[ulCount] = '\0';
			break;
		}
		if (*pszInput == ' ')
			pszInput++;
		else
		{
			szSize[ulCount++] = *pszInput++;
			szSize[ulCount] = '\0';
			break;
		}    
	}
	szSize[MAX_ULONG_STRING_SIZE-1] = '\0';
	*pszSize++=*szSize;
	*pszSize='\0';

	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == ' ')
			pszInput++;
		else
			if (*pszInput == '\0')
			{
				tySI.bInvalid=1;
				return;
			}
			else
				break;
	}

	ulCount=0;
	ulValidHex=2;
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szAddress[ulCount] = '\0';
			break;
		}
		if (*pszInput == ' ')
		{
			szAddress[ulCount] = '\0';
			break;
		}
		else
		{
			szAddress[ulCount++] = *pszInput;
			pszInput++;
		}
	}
	szAddress[MAX_ULONG_STRING_SIZE-1] = '\0';
	if ((strncmp((const char*)szAddress,"0X",2))&&(strncmp((const char*)szAddress,"0x",2)))
	{
		ulCount+=2;
		ulValidHex=0;
	}
	if (ulCount>10)
	{
		tySI.bInvalid=1;
		return;
	}
	if (!ulValidHex)
		ulCount-=2;
	for (ulIndex=ulValidHex;ulIndex<ulCount; ulIndex++)
		if (FromHex(szAddress[ulIndex]) == -1)
		{
			tySI.bInvalid=1;
			return;
		}
		for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
		{
			if (*pszInput == ' ')
				pszInput++;
			else
				if (*pszInput == '\0')
				{
					tySI.bInvalid=1;
					return;
				}
				else
					break;
		}
		ulCount=0;
		ulValidHex=2;
		for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
		{
			if (*pszInput == '\0')
			{
				szData[ulCount] = '\0';
				break;
			}
			if (*pszInput == ' ')
			{
				tySI.bInvalid=1;
				return;
			}
			else
			{
				szData[ulCount++] = *pszInput;
				pszInput++;
			}
		}
		szData[MAX_ULONG_STRING_SIZE-1] = '\0';
		if ((strncmp((const char*)szData,"0X",2))&&(strncmp((const char*)szData,"0x",2)))
		{
			ulCount+=2;
			ulValidHex=0;
		}
		if (ulCount>10)
		{
			tySI.bInvalid=1;
			return;
		}
		if (!ulValidHex)
			ulCount-=2;
		for (ulIndex=ulValidHex;ulIndex<ulCount; ulIndex++)
			if (FromHex(szData[ulIndex]) == -1)
			{
				tySI.bInvalid=1;
				return;
			}
			*pulAddress=strtoul(szAddress,NULL,16);
			*pulData=strtoul(szData,NULL,16);
}
/****************************************************************************
Function: StringsToWpModeSizeAndAddressorData
Engineer: Venkitakrishnan K
Input: char *pszInput               : input string
char *pszWpMode              : pointer to Watchpoint Mode(Write/Read/Dont Care)
char *pszSize                : pointer to watchpoint size(Word/Half/Byte)
uint32_t *pulValue      : pointer to address/data
Output: None
Description: Converts string to WP mode,size,address or Data
value
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
void StringsToWpModeSizeAndAddressorData  (char          *pszInput,
										   char          *pszWpMode,
										   char          *pszSize,
										   uint32_t *pulvalue)
{
	uint32_t ulIndex;
	uint32_t ulCount;
	uint32_t ulValidHex=2;
	char szWpMode[100];
	char szSize[100];
	char szvalue[MAX_ULONG_STRING_SIZE];
	ulCount=0;
	tySI.bInvalid=0;

	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szWpMode[ulCount] = '\0';
			break;
		}
		if (*pszInput == ' ')
			pszInput++;
		else
		{
			szWpMode[ulCount++] = *pszInput++;
			szWpMode[ulCount] = '\0';
			break;
		}     
	}   
	szWpMode[MAX_ULONG_STRING_SIZE-1] = '\0';
	*pszWpMode++=*szWpMode;
	*pszWpMode='\0';
	ulCount=0;
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szSize[ulCount] = '\0';
			break;
		}
		if (*pszInput == ' ')
			pszInput++;
		else
		{
			szSize[ulCount++] = *pszInput++;
			szSize[ulCount] = '\0';
			break;
		}    
	}
	szSize[MAX_ULONG_STRING_SIZE-1] = '\0';
	*pszSize++=*szSize;
	*pszSize='\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == ' ')
			pszInput++;
		else
			if (*pszInput == '\0')
			{
				tySI.bInvalid=1;
				return;
			}
			else
				break;
	}
	ulCount=0;
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szvalue[ulCount] = '\0';
			break;
		}
		if (*pszInput == ' ')
		{
			tySI.bInvalid=1;
			return;
		}
		else
		{
			szvalue[ulCount++] = *pszInput;
			pszInput++;
		}
	}
	szvalue[MAX_ULONG_STRING_SIZE-1] = '\0';
	if ((strncmp((const char*)szvalue,"0X",2))&&(strncmp((const char*)szvalue,"0x",2)))
	{
		ulCount+=2;
		ulValidHex=0;
	}
	if (ulCount>10)
	{
		tySI.bInvalid=1;
		return;
	}
	if (!ulValidHex)
		ulCount-=2;
	for (ulIndex=ulValidHex;ulIndex<ulCount; ulIndex++)
		if (FromHex(szvalue[ulIndex]) == -1)
		{
			tySI.bInvalid=1;
			return;
		}

		*pulvalue=strtoul(szvalue,NULL,16);
}
/****************************************************************************
Function: StringsToTopOfMemandVectorTrapAddr
Engineer: Rejeesh S Babu
Input:  char *pszInput               : input string
uint32_t *pulTop        :Top of memory
uint32_t *pulvector     :vector trap address
Output: None
Description: Converts string to TOM and Vector trap Address 
value
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/
void StringsToTopOfMemandVectorTrapAddr (char          *pszInput,
										 uint32_t *pulTop,
										 uint32_t *pulvector)
{
	uint32_t ulIndex;
	uint32_t ulCount;
	uint32_t ulValidHex=2;
	char szTop[100];
	char szVector[100];
	tySI.bInvalid=0;


	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == ' ')
			pszInput++;
		else
			if (*pszInput == '\0')
			{
				tySI.bInvalid=1;
				return;
			}
			else
				break;
	}
	ulCount=0;
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		if (*pszInput == '\0')
		{
			szTop[ulCount] = '\0';
			strcpy(szVector,"0x8");
			break;
		}
		if (*pszInput==' ')
		{
			szTop[ulCount] = '\0';
			break;
		}
		szTop[ulCount++] = *pszInput++;
	}
	szTop[MAX_ULONG_STRING_SIZE-1] = '\0';
	if ((strncmp((const char*)szTop,"0X",2))&&(strncmp((const char*)szTop,"0x",2)))
	{
		ulCount+=2;
		ulValidHex=0;
	}
	if (ulCount>10)
	{
		tySI.bInvalid=1;
		return;
	}
	if (!ulValidHex)
		ulCount-=2;
	for (ulIndex=ulValidHex;ulIndex<ulCount; ulIndex++)
		if (FromHex(szTop[ulIndex]) == -1)
		{
			tySI.bInvalid=1;
			return;
		}
		ulCount=0;
		ulValidHex=2;
		if (*pszInput != '\0')
		{
			for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
			{
				if (*pszInput == ' ')
					pszInput++;
				else
					if (*pszInput == '\0')
					{
						tySI.bInvalid=1;
						return;
					}
					else
						break;
			}
			for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
			{
				if (*pszInput == '\0')
				{
					szVector[ulCount] = '\0';
					break;
				}
				if (*pszInput==' ')
				{
					szVector[ulCount] = '\0';
					tySI.bInvalid=1;
					return;
				}
				szVector[ulCount++] = *pszInput++;    
			}
		}
		szVector[MAX_ULONG_STRING_SIZE-1] = '\0';
		if ((strncmp((const char*)szVector,"0X",2))&&(strncmp((const char*)szVector,"0x",2)))
		{
			ulCount+=2;
			ulValidHex=0;
		}
		if (ulCount>10)
		{
			tySI.bInvalid=1;
			return;
		}
		if (!ulValidHex)
			ulCount-=2;
		for (ulIndex=ulValidHex;ulIndex<ulCount; ulIndex++)
			if (FromHex(szVector[ulIndex]) == -1)
			{
				tySI.bInvalid=1;
				return;
			}
			*pulTop=strtoul(szTop,NULL,16);
			*pulvector=strtoul(szVector,NULL,16);
}
/****************************************************************************
Function: StringsToProcessCP15Params
Engineer: Roshan T R
Input:  char *pszInput               : input string
uint32_t *puiCRn   : CRn in coprocessor read/Write
uint32_t *puiCRm   : CRm in coprocessor read/Write
uint32_t *puiOp1   : Operand1 in coprocessor read/Write
uint32_t *puiOp2   : Operand2 in coprocessor read/Write
uint32_t *pulData : Pointer to the data to be write to CP15 reg
Output: None
Description: Converts input string to Coprocessor read/write  function parameters
Date           Initials    Description
01-Dec-2009    RTR         initial
*****************************************************************************/
int32_t StringsToProcessCP15Params (char *pszInput, uint32_t *puiCRn,
								uint32_t *puiCRm, uint32_t *puiOp1,
								uint32_t *puiOp2, uint32_t *pulData)
{
	char szParams[6][25];//To parse the command line arguments - 5 params
	int32_t i = 0, j = 0;

	do
	{
		do
		{
			szParams[i][j++] = *pszInput++;
		}
		while (*pszInput != ' ' && *pszInput != '\0');
		szParams[i][j] = '\0';
		if (i > 4)
			return ERR_INVALID_ARG;
		if (j > 20)
			return ERR_UNSUPPORTED_REG;
		j = 0; i++;
	}
	while (*pszInput != '\0');


	*puiOp1 = (uint32_t)atoi(szParams[0]);
	if (MAX_OP1 < *puiOp1)
		return ERR_INVALID_OP1;
	*puiCRn = (uint32_t)atoi(szParams[1]);
	if (MAX_CRN < *puiCRn)
		return ERR_INVALID_CRN;
	*puiCRm = (uint32_t)atoi(szParams[2]);
	if (MAX_CRM < *puiCRm)
		return ERR_INVALID_CRM;
	*puiOp2 = (uint32_t)atoi(szParams[3]);
	if (MAX_OP2 < *puiOp2)
		return ERR_INVALID_OP2;
	if (szParams[4][1] == '0' && (szParams[4][2] == 'x'|| szParams[4][2] == 'X'))
	{
		*pulData = (uint32_t)(strtol(szParams[4],NULL,16));
	}
	else
	{
		*pulData = (uint32_t)atoi(szParams[4]);
	}

	return 0;
}
/****************************************************************************
Function: StringsToReadCPRegParams
Engineer: Roshan T R
Input:  char *pszInput               : input string
uint32_t *pulCPNumber   : Coprocessor to be read 
uint32_t *pulRegIndex   : Register index of coprocessor
Output: None
Description: Converts input string to ReadCpReg function parameters
value
Date           Initials    Description
01-Dec-2009    RTR         initial
*****************************************************************************/
int32_t StringsToReadCPRegParams (char *pszInput, uint32_t *pulCPNumber,
							  uint32_t *pulRegIndex)
{
	char szParams[4][100];//To parse the command line arguments - 4 params
	int32_t i = 0, j = 0;

	do
	{
		do
		{
			szParams[i][j++] = *pszInput++;
		}
		while (*pszInput != ' ' && *pszInput != '\0');
		szParams[i][j] = '\0';
		if (i > 1)
			return ERR_INVALID_ARG;
		if (j > 3)
			return ERR_UNSUPPORTED_REG;
		j = 0; i++;
	}
	while (*pszInput != '\0');

	*pulCPNumber = (uint32_t)atoi(szParams[0]);
	if (15 != *pulCPNumber)
		return ERR_UNSUPPORTED_CORPOC;
	*pulRegIndex = (uint32_t)atoi(szParams[1]);
	if (200 < *pulRegIndex)
		return ERR_UNSUPPORTED_REG;

	return 0;
}

/****************************************************************************
Function: StringsToWriteCPRegParams
Engineer: Roshan T R
Input:  char *pszInput               : input string
uint32_t *pulCPNumber   : Coprocessor to be read 
uint32_t *pulRegIndex   : Register index of coprocessor
uint32_t *pulData       : Data to be write
Output: None
Description: Converts input string to ReadCpReg function parameters
value
Date           Initials    Description
01-Dec-2009    RTR         initial
*****************************************************************************/
int32_t StringsToWriteCPRegParams (char *pszInput, uint32_t *pulCPNumber,
							   uint32_t *pulRegIndex, uint32_t *pulData)
{
	char szParams[4][100];//To parse the command line arguments - 4 params
	int32_t i = 0, j = 0;

	do
	{
		do
		{
			szParams[i][j++] = *pszInput++;
		}
		while (*pszInput != ' ' && *pszInput != '\0');
		szParams[i][j] = '\0';
		if (i > 2)
			return ERR_INVALID_ARG;
		if (j > 3)
			return ERR_UNSUPPORTED_REG;
		j = 0; i++;
	}
	while (*pszInput != '\0');

	*pulCPNumber = (uint32_t)atoi(szParams[0]);
	if (15 != *pulCPNumber)
		return ERR_UNSUPPORTED_CORPOC;
	*pulRegIndex = (uint32_t)atoi(szParams[1]);
	if (200 < *pulRegIndex)
		return ERR_UNSUPPORTED_REG;
	*pulData = (uint32_t)atoi(szParams[2]);

	return 0;
}
/****************************************************************************
Function: ConvertVersionString
Engineer: Rejeesh S Babu
Input: uint32_t ulVersion     :Input version
char *pszVersionString     :pointer to version in string format
Output: None
Description: Converts version to strings 
value
Date           Initials    Description
23-Mar-2008      RS         initial
*****************************************************************************/

void ConvertVersionString(uint32_t ulVersion, char *pszVersionString)
{
	uint32_t uiVal1, uiVal2, uiVal3;

	//assert(pszVersionString != NULL);
	// decode firmware version
	uiVal1 = (uint32_t)((ulVersion & 0xff000000) >> 24);
	uiVal2 = (uint32_t)((ulVersion & 0x00ff0000) >> 16);
	uiVal3 = (uint32_t)((ulVersion & 0x0000ff00) >> 8);
	if (ulVersion & 0x000000ff)
		sprintf(pszVersionString, "v%x.%x.%x-%c", uiVal1, uiVal2, uiVal3, (char)(ulVersion & 0x000000ff));
	else
		sprintf(pszVersionString, "v%x.%x.%x", uiVal1, uiVal2, uiVal3);

}
#endif

/****************************************************************************
Function: GetArgs
Engineer: Sandeep V L
Input: char *pcInput - command param string with white char at the beggining
char *ppszArgList - Argument list
Output: No of arguments
Description: Deletes Watchpoint. 
Date           Initials    Description
15-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t GetArgs(char *pcInput, char ppszArgList[][_MAX_PATH])
{
	int32_t iIndex = 0, iArgsCnt = 0, iChIdx = 0;
	char pszBuffer[_MAX_PATH] = {'\0'}, pszTempBuf[_MAX_PATH] = {'\0'};    

	strcpy(pszBuffer, pcInput);

	for (; pszBuffer[iIndex] != '\0';)
	{
		/* skip the blank spaces */
		for (;((pszBuffer[iIndex] == ' ') || (pszBuffer[iIndex] == '\t')); iIndex++)
		{
			/* Just for Lint */
		}    
		if (pszBuffer[iIndex] == '\0')
		{
			return iArgsCnt;
		}

		while ((pszBuffer[iIndex] != ' ')             
			&& (pszBuffer[iIndex] != '\t')
			&& (pszBuffer[iIndex] != '\n')
			&& (pszBuffer[iIndex] != '\0'))
		{
			pszTempBuf[iChIdx] = pszBuffer[iIndex];
			iIndex++;
			iChIdx++;
		}
		pszTempBuf[iChIdx] = '\0';
		strcpy(ppszArgList[iArgsCnt], pszTempBuf);
		iArgsCnt++;
		/* reset the index value for next argument */
		iChIdx = 0;
		memset(pszTempBuf, 0, _MAX_PATH);
	}

	if (pszBuffer[iIndex] != '\0')
	{
		/* indicates invalid argument */
		iArgsCnt = -1;
		return iArgsCnt;
	}

	return iArgsCnt;
}

/****************************************************************************
Function: ValidateStr
Engineer: Sandeep V L
Input: char* pszStr             : String to be validated
BASE base                : Hex or Decimal
uint32_t uiExpLen    : Expected length of the argument
Output: int32_t   : FALSE, On Error
: TRUE, On Success
Description: to validate a string whether it is valid
Date           Initials    Description
17-Oct-2009    SVL          Initial
*****************************************************************************/
int32_t ValidateStr(char *pszStr, TyBase tyBase, uint32_t uiExpLen)
{
	char *pszBuffer = NULL;

	if (pszStr == NULL)
		return FALSE;

	uint32_t uiStrLen = (uint32_t)strlen(pszStr), uiIdx = 0, uiStartIdx = 0;

    if (uiStrLen < 2) return FALSE;

	/* pointer to base function to be called */
	int32_t(*pfBaseFunc)(int32_t) = 0;

	pszBuffer = (char *)malloc(sizeof(char) * ((uint64_t)uiStrLen + 1));
	if (!pszBuffer)
	{
		PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
		return FALSE;
	}
	strcpy(pszBuffer, pszStr);

	/* Check whether the string contains a 0X prefix */    
	if ((strncmp(pszBuffer, "0X", 2) == 0)
		||(strncmp(pszBuffer, "0x", 2) == 0))
	{
		uiStartIdx = 2;
	}

	switch (tyBase)
	{
	case HEX:
		pfBaseFunc = FromHex;
		break;
	case DEC:
		pfBaseFunc = IsDec;
		break;
	case BINARY:      
	case OCTAL:
	default: 
		/* do nothing */
		break;
	}
	/* Check whether each character in the string is a valid hex */
	for (uiIdx = uiStartIdx; uiIdx < uiStrLen; uiIdx++)
	{
		if (pfBaseFunc)
		{
			if ((pfBaseFunc(pszBuffer[uiIdx])) < 0)
			{
				free(pszBuffer);
				pszBuffer = NULL;
				return FALSE;
			}
		}
	}

	/* check for the length of the input string */
	if (uiStrLen > uiExpLen)
	{
		free(pszBuffer);
		pszBuffer = NULL;
		return FALSE;
	}

	free(pszBuffer);
	pszBuffer = NULL;
	return TRUE;
}


/****************************************************************************
Function: StringsToAddressCountAndBinaryData
Engineer: Shameerudheen P T
Input: char *pszInput            : input string
uint32_t *pulAddress : pointer to address
uint32_t *pulCount   : pointer to count
unsigned char *pucData    : pointer to data
uint32_t ulDataSize  : data size
Output: none
Description: Converts address and count value from String to int32_t
Binary Datat value extracted from input string
Date           Initials    Description
09-Nov-2012    SPT          Initial
*****************************************************************************/
void StringsToAddressCountAndBinaryData(char *pszInput, uint32_t *pulAddress, uint32_t *pulCount, unsigned char *pucData, uint32_t ulDataSize)
{
	uint32_t ulIndex;
	char szAddress[MAX_ULONG_STRING_SIZE];
	char szCount[MAX_ULONG_STRING_SIZE];
	assert(pszInput != NULL);
	assert(pulAddress != NULL);
	assert(pulCount != NULL);
	assert(pucData != NULL);
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szAddress[ulIndex] = *pszInput++;

		if (szAddress[ulIndex] == ',')
		{
			szAddress[ulIndex] = '\0';
			break;
		}
	}
	szAddress[MAX_ULONG_STRING_SIZE-1] = '\0';
	for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
	{
		szCount[ulIndex] = *pszInput++;
		if (szCount[ulIndex] == ':')
		{
			szCount[ulIndex] = '\0';
			break;
		}
	}
	szCount[MAX_ULONG_STRING_SIZE-1] = '\0';
	StringToUlong(szAddress, pulAddress);
	StringToUlong(szCount, pulCount);
	// truncate to buffer size
	if (*pulCount > ulDataSize)
		*pulCount = ulDataSize;
	for (ulIndex=0; ulIndex < *pulCount; ulIndex++)
	{
		if (((*pszInput) == 0x7d))
		{
			pszInput++;
			pucData[ulIndex] = (unsigned char)((*pszInput) ^ 0x20) ;
			pszInput++;
		}
		else
		{
			pucData[ulIndex] = (unsigned char) *pszInput;
			pszInput++;
		}
	}
}
