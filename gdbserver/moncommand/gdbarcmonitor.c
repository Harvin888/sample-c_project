/****************************************************************************
       Module: gdbmonitor.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, GDB Monitor Command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/moncommand/gdbmonitor.h"
#include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
#include "gdbserver/moncommand/ashload/gdbashload.h"
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"

// external variables
extern TyServerInfo tySI;

// local functions
static int32_t GetMonCommand(char* pcInput, char *pcOutput);
static int32_t DoHWReset(char *pcInput, char *pcOutput);

typedef int32_t (*TyMonCommandFunction)(char *pcInput, char *pcOutput);

typedef struct _TyMonCommandEntry 
{
   char *monCommand;
   char *pszHelpArg;
   char *pszHelpLine1;
   char *pszHelpLine2;
   char *pszHelpLine3;
   char *pszHelpLine4;
   TyMonCommandFunction tyMonCommandFunction;
} TyMonCommandEntry;

// local variables
static TyMonCommandEntry monitorCommandTable[]  =
{
   {
      "ashload",
      " <file> <--verify>",
      "Load ELF format <file>.",
      "<file> should be given an absolute path.",
      "--verify will readback/verify data",
      "downloaded (default off).",
      Ashload
   },
   {
      "hwreset",
      "",
      "Reset target (hardware reset via RST* pin).",
      "",
      "",
      "",
      DoHWReset
   }
};
#define MAX_MON_COMMAND_ENTRIES ((int32_t)(sizeof(monitorCommandTable) / sizeof(TyMonCommandEntry)))

/****************************************************************************
     Function: ShowGDBMonitorCommandsHelp
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: Show GDB monitor command help       
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void ShowGDBMonitorCommandsHelp(void)
{
   int32_t iPadding, iStrLen, iIndex;
   char szBuffer[_MAX_PATH*2];

   PrintMessage(HELP_MESSAGE, "\n\nSupported GDB MONITOR commands:\n\n");
   //PrintMessage(HELP_MESSAGE, "Commands to be used from GDB, after connecting to Ashling GDB ");
   //PrintMessage(HELP_MESSAGE, "Server.\n\n");

   for (iIndex=0; iIndex<MAX_MON_COMMAND_ENTRIES; iIndex++)
      {
      sprintf(szBuffer, "%s%s%s","monitor ",monitorCommandTable[iIndex].monCommand, monitorCommandTable[iIndex].pszHelpArg);
      PrintMessage(HELP_MESSAGE, "%s", szBuffer);
      if ((iStrLen = (int32_t)strlen(szBuffer)) < GDBSERV_MON_HELP_COL_WIDTH-1) 
         iPadding = GDBSERV_MON_HELP_COL_WIDTH - iStrLen;
      else
         iPadding = 1;
      if (monitorCommandTable[iIndex].pszHelpLine1[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", iPadding, " ", monitorCommandTable[iIndex].pszHelpLine1);
      if (monitorCommandTable[iIndex].pszHelpLine2[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine2);
      if (monitorCommandTable[iIndex].pszHelpLine3[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine3);
      if (monitorCommandTable[iIndex].pszHelpLine4[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine4);
      }  
}

/****************************************************************************
     Function: ParseMonitorCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int32_t - 1 to send response, 0 - ignore
  Description: select particular monitor command handler and invoke it
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
int32_t ParseMonitorCommand(char *pcInput, char *pcOutput)
{
   int32_t iIndex;
   pcOutput[0] = '\0';
   // get monitor command
   if (!GetMonCommand(pcInput, pcOutput))
      return 0;
   // parse command
   for (iIndex = 0; iIndex < MAX_MON_COMMAND_ENTRIES; iIndex++)
      {
      if (!strncmp(monitorCommandTable[iIndex].monCommand, tySI.pszMonitorCommand, strlen(monitorCommandTable[iIndex].monCommand)))
         return monitorCommandTable[iIndex].tyMonCommandFunction(tySI.pszMonitorCommand + strlen(monitorCommandTable[iIndex].monCommand), pcOutput); 
      }
   return 0;
}

/****************************************************************************
     Function: GetMonCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput  : hexadecimal string representing monitor command
               char *pcOutput : hexadecimal string to be returned to the GDB
       Output: int32_t - 1 if command is ok, 0 otherwise
  Description: Convert monitor command from hexadecimal to ascii string.
               String will be stored into tySI.pszMonitorCommand
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use 'util' function to prepare the outstring
*****************************************************************************/
static int32_t GetMonCommand(char* pcInput, char *pcOutput)
{
   uint32_t uiStrLen = strlen(pcInput);
   char szBuffer[_MAX_PATH*2];
   // check if string is really hex
   if (uiStrLen % 2)
      {  // not correct string, print message to user
      sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
      PrintMessage(ERROR_MESSAGE, szBuffer);
      strcat(szBuffer, "\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   // check if string is not too int32_t
   if (uiStrLen >= (2 * _MAX_PATH))
      {
      sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
      PrintMessage(ERROR_MESSAGE, szBuffer);
      strcat(szBuffer, "\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   // process command
   for (uiStrLen = 0; uiStrLen < strlen(pcInput) ; uiStrLen+=2)
      {
      int32_t iTemp = FromHex(pcInput[uiStrLen]);
      if (iTemp == -1)
         {
         sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
         PrintMessage(ERROR_MESSAGE, szBuffer);
         strcat(szBuffer, "\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return 0;
         }
      tySI.pszMonitorCommand[uiStrLen / 2] = (char)(iTemp << 4);  //lint !e701
      iTemp = FromHex(pcInput[uiStrLen + 1]);
      if (iTemp == -1)
         {
         sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
         PrintMessage(ERROR_MESSAGE, szBuffer);
         strcat(szBuffer, "\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return 0;
         }
      tySI.pszMonitorCommand[uiStrLen/2] |= (char)iTemp;
      }
   if ((uiStrLen / 2) < _MAX_PATH)
      tySI.pszMonitorCommand[uiStrLen / 2] = '\0'; // terminate command string
   //sprintf(AshloadProcData.szErrorBuffer,);
   PrintMessage(LOG_MESSAGE, "Encoded command '%s'.\n", tySI.pszMonitorCommand);
   return 1;
}

/****************************************************************************
     Function: DoHWReset
     Engineer: Vitezslav Hola
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Do target hardware reset.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Added response message
*****************************************************************************/
static int32_t DoHWReset(char *pcInput, char *pcOutput)
{
   char  szBuffer[_MAX_PATH];

   NOREF(pcInput);
   // do hardware reset
#ifdef ARC
   (void)LOW_ResetTarget();
#elif defined(ARM)
#error Not implemented yet.
#elif defined(MIPS)
   //#error Not implemented yet.
   //todo
#endif
   // TO_DO: error handling
   sprintf(szBuffer,"Hardware reset asserted\n");
   PrintMessage(INFO_MESSAGE, "%s", szBuffer);
   AddStringToOutAsciiz(szBuffer, pcOutput);

//   strcat(pcOutput, "OK");
   return 1;
}
