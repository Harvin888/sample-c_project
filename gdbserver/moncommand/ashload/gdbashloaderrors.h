/****************************************************************************
       Module: gdbashloaderrors.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ashload errors, ashload messages
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBASHLOADERRORS_H_
#define GDBASHLOADERRORS_H_

// error codes
#define ERR_UNEXPECTED_END_OF_FILE                                100
#define ERR_F_NULL_FILE                                           101
#define ERR_F_SEEK                                                102
#define ERR_NOT_AN_ELF_FILE                                       103
#define ERR_NOT_ABSOLUTE_REC_E                                    104
#define ERR_IE_PROCDWRF00                                         105
#define ERR_INSUFFICENT_DATA_BUFFER_MEMORY                        106
#define ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY               109
#define ERR_MEMORY_READ_FAILURE                                   111
#define ERR_MEMORY_VERIFY_FAILURE                                 112
#define ERR_MEMORY_WRITE_FAILURE                                  113
#define ERR_INVALID_NUMBER_OF_PARMS                               114
#define ERR_FILE_PATH_TOO_LONG                                    115
#define ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND                      116
#define ERR_COMMAND_TOO_LONG                                      117
#define ERR_COMMAND_SWITCH_TOO_LONG                               118
#define ERR_COMMAND_SWITCH_USED_TWICE                             119
#define ERR_UNKNOWN_SWITCH                                        120
#define ERR_DUMMY_ALREADY_SHOWN                                   121
#define ERR_NOT_ASHLOAD_COMMAND                                   122

#define ERR_IE_PROCDWRF07                                         123
#define ERR_IE_PROCDWRF08                                         124
#define ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY               125
#define ERR_IE_READDWRF03                                         126
#define ERR_IE_READDWRF04                                         127
#define ERR_MEM_MALLOC_C                                          128
#define ERR_IE_READDWRF02                                         129
#define ERR_IE_READDWRF01                                         130
#define ERR_IE_READDWRF07                                         131
#define ERR_INVALID_FILE_NAME                                     132
#define ERR_COMMAND_ADDR_TOO_LONG                                 133

// error messages
#define MESSAGE_ERR_UNEXPECTED_END_OF_FILE                        "Unexpected end of file."
#define MESSAGE_ERR_F_NULL_FILE                                   "Could not find/open file '%s'."
#define MESSAGE_ERR_F_SEEK                                        "Seek error accessing file."
#define MESSAGE_ERR_NOT_AN_ELF_FILE                               "Invalid ELF file (magic bytes)."
#define MESSAGE_ERR_NOT_ABSOLUTE_REC_E                            "Invalid ELF file."
#define MESSAGE_ERR_IE_PROCDWRF00                                 "Invalid ELF file (program header)."
#define MESSAGE_ERR_INSUFFICENT_DATA_BUFFER_MEMORY                "Memory failure (data)."
#define MESSAGE_ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY       "Memory failure (program header)."
#define MESSAGE_ERR_MEMORY_READ_FAILURE                           "Reading target memory."
#define MESSAGE_ERR_MEMORY_VERIFY_FAILURE                         "Verification failed at 0x%02X. Expected:0x%02X, read:0x%02X."
#define MESSAGE_ERR_MEMORY_WRITE_FAILURE                          "Writing target memory."
#define MESSAGE_ERR_INVALID_NUMBER_OF_PARMS                       "Invalid parameters."
#define MESSAGE_ERR_FILE_PATH_TOO_LONG                            "Invalid parameters."
#define MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND              "No command arguments."
#define MESSAGE_ERR_COMMAND_TOO_LONG                              "Invalid parameters."
#define MESSAGE_ERR_COMMAND_SWITCH_TOO_LONG                       "Invalid parameters."
#define MESSAGE_ERR_COMMAND_SWITCH_USED_TWICE                     "Invalid parameters."
#define MESSAGE_ERR_UNKNOWN_SWITCH                                "Invalid parameters."
#define MESSAGE_ERR_DUMMY_ALREADY_SHOWN                           "Internal Error 01."
#define MESSAGE_ERR_NOT_ASHLOAD_COMMAND                           "GDB command recognition failure."

#define MESSAGE_ERR_IE_PROCDWRF07                                 "Invalid ELF file (string section index)."
#define MESSAGE_ERR_IE_PROCDWRF08                                 "Invalid ELF file (section header table size)."
#define MESSAGE_ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY       "Memory failure (data)."
#define MESSAGE_ERR_IE_READDWRF03                                 "Internal Error 03."
#define MESSAGE_ERR_IE_READDWRF04                                 "Invalid ELF file (string spans section end) table)."
#define MESSAGE_ERR_MEM_MALLOC_C                                  "Internal Error 04."
#define MESSAGE_ERR_IE_READDWRF02                                 "Internal Error 05."
#define MESSAGE_ERR_IE_READDWRF01                                 "Internal Error 06."
#define MESSAGE_ERR_IE_READDWRF07                                 "Internal Error 07."
#define MESSAGE_ERR_INVALID_FILE_NAME                             "Invalid file name."
#define MESSAGE_ERR_UNKNOWN_ERROR_OCURRED                         "Internal Error 02."
#define MESSAGE_ERR_MONCMD_RECOGNITION_FAILED                     "GDB command recognition failure."

#define MESSAGE_INFO_DOWNLOAD_IN_PROGRESS                         "\nDownloading %s"
#define MESSAGE_INFO_VERIFICATION_IN_PROGRESS                     "Verifying %s"
#define MESSAGE_INFO_FILE_DOWNLOADED                              "\nFile downloaded. Start address 0x%08X, load size %lu"
#define MESSAGE_INFO_FILE_VERIFIED                                "\nVerified ok."

#define MESSAGE_WARING_FILE_SZ_GRATER_MEM_SZ                      "Warning: Invalid p_filesz/p_memsz in Program Header Table entry in input file.\n"
#define MESSAGE_WARING_NO_PROGRAM_HEADER                          "  Warning: No Program Header Table in input file.\n"
#define MESSAGE_WARNING_NO_SECTION_HEADER_TABLE                   "Warning: No Section Header Table in input file."
#define MESSAGE_WARNING_NO_SECTION_NAME_STRING_TABLE              "  Warning: No Section Name String Table in input file."

#define GDB_CLIENT_ERROR_MESSAGE_PREFIX                           "Error: "
#define HEADER_OF_SECTION_HEADER_TABLE_ENTRIES                    "\nno# \tname\t\t\tSize\t\tOffset\t\tAlign"
#define SECTION_HEADER_TABLE_ENTRY                                "%-3lu\t%-20s\t0x%08X\t0x%08X\t0x%08X\t"

#endif   // GDBASHLOADERRORS_H_

