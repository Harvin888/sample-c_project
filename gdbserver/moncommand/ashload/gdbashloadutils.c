/****************************************************************************
       Module: ashloadutils.c
     Engineer: Vitezslav Hola
  Description: utils for ashload command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
// Linux build change...
#include "protocol/common/types.h"
#include "gdbserver/gdbglob.h"
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
#include "gdbserver/moncommand/ashload/gdbashloadutils.h"

/*
typedef struct _TyInitSectionInfo
{
   TySection Section;
   char *pszName;
   char *pszDescription;
   uint32_t uMaxBufferSize;
} TyInitSectionInfo;
*/

typedef struct _TySectionInfo
    {
    char *pszName;                      // pointer to section name, e.g. ".debug_info"
    char *pszDescription;               // pointer to extra description for dbug_msg
    uint32_t uSectionAddr;         // load address if loadable
    uint32_t uSectionBegin;        // offset within ELF/DWARF file to section begin
    uint32_t uSectionSize;         // size of this section in bytes
    uint32_t uReadOffset;          // current read point within section: 0..uSectionSize-1
    unsigned char *pBuffer;             // pointer to input buffer for this section
    uint32_t uMaxBufferSize;       // max buffer size for alloc
    uint32_t uBufferSize;          // actual buffer size allocated: 0..uMaxBufferSize
    uint32_t uBufferedStartOffset; // Offset within section from which data is buffered: 0..uSectionSize-1
    uint32_t uBufferedMaxOffset;   // Offset to end of buffered data +1 (i.e. to the first byte after the buffer)
    } TySectionInfo;

TySectionInfo     SectionInfo[MaxSectionNumber];
//TyInitSectionInfo InitSectionInfo[MaxSectionNumber] =
//{
//   // Section             pszName            pszDescription    uMaxBufferSize
//   // -------             -------            --------------    --------------
//   {SectionNames,         "",                "<Section Names>",  0x1000}
//}; 

// local functions
static int32_t FileSeek(FILE *pFile, uint32_t ulOffset, signed short siOrigin);

/****************************************************************************
     Function: ReadBytes
     Engineer: Vitezslav Hola
        Input: FILE *pFile - file from which bytes are read
               uint32_t ulFilePosition - position to read from if bSetFilePosition==1
               unsigned char bSetFilePosition - if 1 read form uFilePosition else use the current seek position
               void *pBuffer - pointer to storage for the bytes
               uint32_t ulNumBytes - count of bytes to read
       Output: int32_t - error code
  Description: Reads uNumBytes bytes from pFile at either uFilePosition or at the current seek position.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t ReadBytes(FILE *pFile, uint32_t ulFilePosition, unsigned char bSetFilePosition, void *pBuffer, uint32_t ulNumBytes)
{
    int32_t iError;
    signed short sTemp;
    uint32_t ulLoop;
    unsigned char *pubBuffer = (unsigned char *)pBuffer;

    if (bSetFilePosition)
        {  // position file pointer
        iError = FileSeek(pFile, ulFilePosition, SEEK_SET);
        if (iError != 0)
            return iError;
        }
    for (ulLoop=0; ulLoop < ulNumBytes; ulLoop++)
        {
        sTemp = (signed short)fgetc(pFile);
        if (sTemp == EOF)
            return ERR_UNEXPECTED_END_OF_FILE;
        pubBuffer[ulLoop] = (unsigned char)sTemp;
        }
    return 0;
}

/****************************************************************************
     Function: FileSeek
     Engineer: Vitezslav Hola
        Input: FILE *pFile - file for operation
               uint32_t ulOffset - offset from origin
               signed short siOrigin - origin (0 start of file, 1 current position, 2 EOF)
       Output: int32_t - error code
  Description: Replace for fseek with special return code if operation is not successful.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static int32_t FileSeek(FILE *pFile, uint32_t ulOffset, signed short siOrigin)
{
    if (pFile == NULL)
        return ERR_F_NULL_FILE;
    else
        {
        if (fseek(pFile, (int32_t)ulOffset, siOrigin) != 0)
            return ERR_F_SEEK;
        }
    return 0;
}

/****************************************************************************
     Function: AllocSectionBuffer
     Engineer: Vitezslav Hola
        Input: TySection tySection - section whose pBuffer is to be allocated
               uint32_t ulSectionAddr - load address if loadable
               uint32_t ulSectionBegin - offset within ELF/DWARF file to section begin
               uint32_t ulSectionSize - size of this section in bytes
       Output: int32_t - error code
  Description: Set the Section's address, begin and size then allocate the Section's pBuffer if necessary
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t AllocSectionBuffer(TySection tySection, uint32_t ulSectionAddr, uint32_t ulSectionBegin, uint32_t ulSectionSize)
{
    assert(tySection == SectionNames );
    if (SectionInfo[tySection].pBuffer != NULL)
        return ERR_IE_READDWRF03;
    SectionInfo[tySection].uReadOffset          = 0;
    SectionInfo[tySection].uBufferedStartOffset = 0;
    SectionInfo[tySection].uBufferedMaxOffset   = 0;
    SectionInfo[tySection].uSectionAddr         = ulSectionAddr;
    SectionInfo[tySection].uSectionBegin        = ulSectionBegin;
    SectionInfo[tySection].uSectionSize         = ulSectionSize;
    if (SectionInfo[tySection].uSectionSize < SectionInfo[tySection].uMaxBufferSize)
        SectionInfo[tySection].uBufferSize = SectionInfo[tySection].uSectionSize;
    else
        SectionInfo[tySection].uBufferSize = SectionInfo[tySection].uMaxBufferSize;
    if (SectionInfo[tySection].uBufferSize > 0)
        {
        //SectionInfo[tySection].pBuffer = (unsigned char *)amalloc(SectionInfo[tySection].uBufferSize, IE_READDWRF04, &ErrRet);
        SectionInfo[tySection].pBuffer = (unsigned char *)malloc(SectionInfo[tySection].uBufferSize);
        if (SectionInfo[tySection].pBuffer == NULL)
            {
            SectionInfo[tySection].uBufferSize = 0;
            return ERR_MEM_MALLOC_C;
            }
        }
    return 0;
}

/****************************************************************************
     Function: FreeSectionBuffer
     Engineer: Vitezslav Hola
        Input: TySection tySection - section whose pBuffer is to be freed
       Output: none
  Description: free this Section's pBuffer
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
void FreeSectionBuffer(TySection tySection)
{
    assert(tySection == SectionNames);
    if ((SectionInfo[tySection].uBufferSize > 0) && (SectionInfo[tySection].pBuffer != NULL))
        {
        free((char **)&SectionInfo[tySection].pBuffer);
        SectionInfo[tySection].uBufferSize = 0;
        }
}

/****************************************************************************
     Function: AshloadUtilsClean
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: free allocated memory during section processing
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
void AshloadUtilsClean(void)
{
    FreeSectionBuffer(SectionNames);
}

/****************************************************************************
     Function: ReadSectionString
     Engineer: Vitezslav Hola
        Input: FILE *pFile - file from which bytes are read
               TySection tySection - section in this file to read from
               uint32_t ulReadOffset - offset within section to read from if bSetReadOffset==1
               unsigned char bSetReadOffset - if 1 read bytes from uReadOffset within section else use the current pSectionInfo->uReadOffset.
               char *pszString - pointer to storage for the string
               uint32_t ulMaxStringSize - size of buffer for string
       Output: int32_t - error code
  Description: Reads a zero terminated string from section Section in pFile at either the offset given by uReadOffset or at this sections
               current pSectionInfo->uReadOffset.
               The string (truncated if necessary) is returned zero terminated in pszString.
               Buffering is used where possible to reduce the number of seeks caused by successive short reads to different sections.
               Note that pSectionInfo->uBufferedMaxOffset is the offset to the last buffered byte+1 (i.e. to the first byte after the buffer).
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t ReadSectionString(FILE *pFile, TySection tySection, uint32_t ulReadOffset, unsigned char bSetReadOffset, char *pszString, uint32_t ulMaxStringSize)
{
    int32_t iError;
    uint32_t ulFilePosition, ulBytesRead, ulCopyLength;
    uint32_t ulStringLength = 0;
    unsigned char bStringBeginInBuffer = 0;
    unsigned char bStringEndInBuffer = 0;
    TySectionInfo *pSectionInfo = &SectionInfo[tySection];

    NOREF(pSectionInfo->pszName);
    NOREF(pSectionInfo->pszDescription);
    if (bSetReadOffset)
        pSectionInfo->uReadOffset = ulReadOffset;
    pszString[0] = '\0';
    if (ulMaxStringSize == 0)
        return ERR_IE_READDWRF02;
    if (pSectionInfo->uReadOffset >= pSectionInfo->uSectionSize)
        return ERR_IE_READDWRF04;
    ulFilePosition = pSectionInfo->uSectionBegin + pSectionInfo->uReadOffset;
    // buffered read
    if (pSectionInfo->pBuffer != NULL)
        {  // check if the string is within the current section buffer
        StringWithinSectionBuffer(tySection, &bStringBeginInBuffer, &bStringEndInBuffer, &ulStringLength);
        if (!bStringBeginInBuffer)
            { // pSectionInfo->uReadOffset is not in the current section buffer refill the section buffer and try again
            iError = RefillSectionBuffer(pFile, tySection);
            if (iError != 0)
                return iError;
            StringWithinSectionBuffer(tySection, &bStringBeginInBuffer, &bStringEndInBuffer, &ulStringLength);
            }
        }
    if (bStringEndInBuffer)
        {  // the string with EOS is fully contained in the current section buffer 
        // starting at pSectionInfo->uReadOffset, copy the string (excluding EOS) to the callers buffer
        if (ulStringLength < (ulMaxStringSize-1))
            ulCopyLength = ulStringLength;
        else
            ulCopyLength = (ulMaxStringSize-1);
        memcpy(pszString, pSectionInfo->pBuffer + (pSectionInfo->uReadOffset - pSectionInfo->uBufferedStartOffset), ulCopyLength);
        pszString[ulCopyLength] = '\0';                    // terminate the copied string
        pSectionInfo->uReadOffset += (ulStringLength+1);   // update the current read point
        }
    else
        {  // either non-buffered read OR a buffered read where the string is not fully contained in the section buffer
        iError = ReadString(pFile, ulFilePosition, 1, pszString, ulMaxStringSize, &ulBytesRead);
        if (iError != 0)
            return iError;
        pSectionInfo->uReadOffset += ulBytesRead;          // update the current read point
        }
    return 0;
}

/****************************************************************************
     Function: ReadString
     Engineer: Vitezslav Hola
        Input: FILE *pFile - file from which bytes are read
               uint32_t ulFilePosition - position to read from if bSetFilePosition == 1
               unsigned char bSetFilePosition - if 1 read form uFilePosition else use the current seek position
               char *pszString - pointer to storage for the string
               uint32_t ulMaxStringSize - size of buffer for string
               uint32_t *pulBytesRead - storage for count of bytes read from the file including the zero byte.
       Output: int32_t - error code
  Description: Reads a zero terminated string from pFile at either uFilePosition
               or at the current seek position.
               The string (truncated if necessary) is returned zero terminated
               in pszString. if puBytesRead != NULL *puBytesRead is set to the
               total number of bytes read from the file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t ReadString(FILE *pFile, uint32_t ulFilePosition, unsigned char bSetFilePosition, char *pszString, uint32_t ulMaxStringSize, uint32_t *pulBytesRead)
{
    int32_t iError;
    uint32_t ulLoop = 0;
    signed short sTemp;

    if (ulMaxStringSize == 0)
        return ERR_IE_READDWRF01;
    if (bSetFilePosition)
        {  // position file pointer
        iError = FileSeek(pFile, ulFilePosition, SEEK_SET);
        if (iError != 0)
            return iError;
        }
    do
        {
        sTemp = (signed short)fgetc(pFile);
        if (sTemp == EOF)
            return ERR_UNEXPECTED_END_OF_FILE;
        if (ulLoop < ulMaxStringSize)
            pszString[ulLoop] = (char)sTemp;
        ulLoop++;
        }
    while (sTemp != 0);
    // make sure the string is zero terminated
    pszString[ulMaxStringSize - 1] = '\0';
    if (pulBytesRead != NULL)
        *pulBytesRead = ulLoop;
    return 0;
}

/****************************************************************************
     Function: RefillSectionBuffer
     Engineer: Vitezslav Hola
        Input: FILE * pFile - file from which bytes are read
               TySection Section - the section in this file to refill from
       Output: int32_t - error code
  Description: Refill the section buffer to include the byte at offset pSectionInfo->uReadOffset.
               Note that pSectionInfo->uBufferedMaxOffset is the offset to the
               last buffered byte+1 (i.e. to the first byte after the buffer).
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t RefillSectionBuffer(FILE * pFile, TySection Section)
{
    TySectionInfo *pSectionInfo = &SectionInfo[Section];
    if (pSectionInfo->uSectionSize < pSectionInfo->uBufferSize)
        return ERR_IE_READDWRF07;    // should not happen, see AllocSectionBuffer

    if ((pSectionInfo->uSectionSize - pSectionInfo->uBufferSize) < pSectionInfo->uReadOffset)
        pSectionInfo->uBufferedStartOffset = (pSectionInfo->uSectionSize - pSectionInfo->uBufferSize);
    else
        pSectionInfo->uBufferedStartOffset = pSectionInfo->uReadOffset;
    pSectionInfo->uBufferedMaxOffset   = pSectionInfo->uBufferedStartOffset + pSectionInfo->uBufferSize;
    return ReadBytes(pFile, pSectionInfo->uSectionBegin + pSectionInfo->uBufferedStartOffset, 1, pSectionInfo->pBuffer, pSectionInfo->uBufferSize);
}

/****************************************************************************
     Function: StringWithinSectionBuffer
     Engineer: Vitezslav Hola
        Input: TySection Section - the section whose buffer we search
               unsigned char *pbStringBeginInBuffer -  storage for flag if string begins (pSectionInfo->uReadOffset) within current section buffer
               unsigned char *pbStringEndInBuffer - storage for flag if the string ends (EOS) within current section buffer
               uint32_t *pulStringLength - storage for string length (excluding EOS) if string begins and ends within current section buffer
       Output: none
  Description: Determine if the string starting at pSectionInfo->uReadOffset is within the current pSectionInfo->pBuffer. 
               Note that pSectionInfo->uBufferedMaxOffset is the offset to the last buffered byte+1 (i.e. to the first byte after the buffer).
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
void StringWithinSectionBuffer(TySection Section, unsigned char *pbStringBeginInBuffer, unsigned char *pbStringEndInBuffer, uint32_t *pulStringLength)
{
    uint32_t ulBytesToSearch;
    unsigned char *pubStart;
    unsigned char *pubEnd;
    TySectionInfo *pSectionInfo = &SectionInfo[Section];

    *pbStringBeginInBuffer = *pbStringEndInBuffer = 0;
    if ((pSectionInfo->pBuffer != NULL) && 
        (pSectionInfo->uReadOffset >= pSectionInfo->uBufferedStartOffset) && (pSectionInfo->uReadOffset <  pSectionInfo->uBufferedMaxOffset))
        {
        // the string begins within the current section buffer
        *pbStringBeginInBuffer = 1;
        // check if it also ends within the current section buffer
        ulBytesToSearch = pSectionInfo->uBufferedMaxOffset - pSectionInfo->uReadOffset;
        pubStart = pSectionInfo->pBuffer + (pSectionInfo->uReadOffset - pSectionInfo->uBufferedStartOffset);
        if ((pubEnd = (unsigned char*)memchr(pubStart, '\0', ulBytesToSearch)) != NULL)
            {
            *pbStringEndInBuffer = 1;
            *pulStringLength = (uint32_t)(pubEnd - pubStart);
            }
        }
}

