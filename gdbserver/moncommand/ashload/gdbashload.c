/****************************************************************************
       Module: gdbmonitor.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, GDB Monitor Command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdberr.h"
#ifdef ARM
   #include "gdbserver/arm/gdbarm.h"
   #include "../../../protocol/rdi/rdi/rdi150.h"
#endif
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
#include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
#include "gdbserver/moncommand/ashload/gdbashload.h"
#include "gdbserver/moncommand/ashload/gdbashloadutils.h"

//#define MAX_SYMBOL_SIZE 63
// Constant for holding length of longest section name accepted
#define MAX_IDENTIFIER_NAME_SIZE  63 

// ELF header table modes
#define ASHLOAD_DOWNLOAD_ELF                100
#define ASHLOAD_VERIFY_ELF_DOWNLOAD         200
#define FLASH_DOWNLOAD_ELF                  300
#define FLASH_VERIFY_ELF_DOWNLOAD           400
// The maximum number of data bytes that a SymFinder should write to either 
// an ASDOF(0x54) or OMF(0x06) Absolute Data Content Record. The figure is
// rounded down to the next lower multiple of 16 for performance reasons.
#define MAX_DATA_CONTENT_WRITE_LENGTH        0xFFE0
#ifdef ARM
   #define MAX_DATA_CONTENT_READ_LENGTH         0xFFE0
#else
   #define MAX_DATA_CONTENT_READ_LENGTH         0x10
#endif

// Boundary of longest boundary switch accepted, to save memory - if any switch
// that is match at the moment is longer it mean that it is invalid switch therefore
// call error message
#define MAX_SWITCH_LENGTH 10 
#ifdef ARM
uint32_t gulPC;
uint32_t ulGlobalPrgLoadAddr;
uint32 uSize;
#endif
// local functions
static void AshloadInit(char *pcGDBOutput);
static void ShowProgress(uint32_t ulRemain, uint32_t ulFullSize);
static void SetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut);
static int32_t ProcessDwarfFile(void);
static int32_t ParseCommand(char *pcInput, char *pcOutput);
static void SwapHalfword(unsigned short *pusValue);
static void SwapWord(uint32_t *pulValue);

//Exported functions
int32_t VerifyFileFormat(FILE *pFile);
int32_t ProcessELFHeader(void);
int32_t ProcessELFSectionHeaderTable(int32_t iMode);
unsigned char CheckSpace(int32_t iChar);
int32_t ProcessELFProgramHeaderTable(int32_t Mode);
#ifdef ARM
static int32_t AddToRangeArray(TyRangeArray *ptyRangeArray, uint32 uStartAddr, uint32 uEndAddr);
static int32_t InsertRange(TyRangeArray *ptyRangeArray, uint32 uIndex, uint32 uStartAddr, uint32 uEndAddr);
static int32_t WriteDataContentXref(uint32 uVirtAddr, uint32* uLoadAddr, uint32* uSize, ubyte* pubData);
extern int32_t OpenProcessorModule(void);
#endif
//Exportedvariable
ElfProcessData tyELFProcData;
// local variables
static AshloadProcessData tyAshloadProcData;
//Imported variables
//Process data for elftosection command
#ifdef FLASH_SUPPORT
extern FileProcessData tyProcInFileData;
#endif //FLASH_SUPPORT
extern TyServerInfo tySI;
// external functions (required implementation in LOW)
extern int32_t LOW_WriteMemory(uint32_t ulAddress, uint32_t ulCount, unsigned char *pucData);
extern int32_t LOW_ReadMemory(uint32_t ulAddress, uint32_t ulCount, unsigned char *pucData);

#ifdef RISCV
extern int32_t LOW_WritePC(uint32_t *ulRegisterValue);
#else
extern int32_t LOW_WritePC(uint32_t ulRegisterValue);
#endif
extern int32_t LOW_IsBigEndian(int32_t iCoreIndex);
#ifdef ARM
extern int32_t LOW_WriteRegister(uint32_t ulRegisterNumber,
                             uint32_t ulRegisterValue);
#endif

#ifdef FLASH_SUPPORT
//Imported functions
extern unsigned char *GetCurSectionWrBufAddress(void);
extern uint32_t WriteSectionBuffer(uint32_t ulPhyAddr,
                                        uint32_t ulVirtualAddr,
                                        unsigned char *pulDestAddr,
                                        uint32_t ulSize,
                                        unsigned char *pulSrcAddr);
#endif //FLASH_SUPPORT
/****************************************************************************
     Function: Ashload
     Engineer: Vitezslav Hola
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Fast ELF download
Date           Initials    Description
02-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use 'util' function to prepare the outstring
*****************************************************************************/
int32_t Ashload(char *pcInput, char *pcOutput)
{
   int32_t iError = 0;
#ifdef ARM
   uint32_t ulVectorData[2];
#endif
   // Check whether already connected
   if(!tySI.bIsConnected)
      {
//todo      WriteReplyToGDB("Not connected to target\n");
      return FALSE;
      }

   AshloadInit(pcOutput);
   iError = ParseCommand(pcInput, pcOutput);
   if(iError != 0)
      {
      SetupErrorMessage(iError, tyAshloadProcData.szErrorBuffer, pcOutput);
      return 0;
      }
   iError = ProcessDwarfFile();
   if(iError != 0)
      {
      SetupErrorMessage(iError, tyAshloadProcData.szErrorBuffer, pcOutput);
      return 0;
      }
#ifdef ARM    
   sprintf(tyAshloadProcData.szErrorBuffer, "File downloaded. Start address 0x%08X, load size %u.\n",
           (uint32_t)ulGlobalPrgLoadAddr, uSize);
#else
   sprintf(tyAshloadProcData.szErrorBuffer, "File downloaded. Start address 0x%08X, load size %u.\n",
           (uint32_t)tyELFProcData.ElfHeader.e_entry, tyAshloadProcData.ulTotalLoad);
#endif
   AddStringToOutAsciiz(tyAshloadProcData.szErrorBuffer, pcOutput);
   if(tyAshloadProcData.verify_download)
      AddStringToOutAsciiz("Verified ok.\n", pcOutput);
   if(tyELFProcData.ElfHeader.e_entry > 0)
      {
#ifdef ARM
      gulPC=tyELFProcData.ElfHeader.e_entry;
#endif

#ifdef RISCV
	  if (GDBSERV_NO_ERROR != LOW_WritePC(&(tyELFProcData.ElfHeader.e_entry)))    //assume error message is sufficient , lets test!!
	  {
		  sprintf(tyAshloadProcData.szErrorBuffer, "Failed to set pc at: 0x%02X. \n", (uint32_t)tyELFProcData.ElfHeader.e_entry);
		  PrintMessage(INFO_MESSAGE, tyAshloadProcData.szErrorBuffer);
		  AddStringToOutAsciiz(tyAshloadProcData.szErrorBuffer, pcOutput);
		  return 0;
	  }
#else
      if(GDBSERV_NO_ERROR != LOW_WritePC(tyELFProcData.ElfHeader.e_entry))    //assume error message is sufficient , lets test!!
         {
         sprintf(tyAshloadProcData.szErrorBuffer, "Failed to set pc at: 0x%02X. \n", (uint32_t)tyELFProcData.ElfHeader.e_entry);
         PrintMessage(INFO_MESSAGE, tyAshloadProcData.szErrorBuffer);
         AddStringToOutAsciiz(tyAshloadProcData.szErrorBuffer, pcOutput);
         return 0; 
         }
#endif
#ifdef  ARM
/* For Cortex3, thae stack adress will be obtained from the 0th loacation of the program 
load area. So we will read this loacation and update this in the 'sp' register */
      if(gbCortexM3)
         {
         if(GDBSERV_NO_ERROR != LOW_ReadAnyMemory(ulGlobalPrgLoadAddr, 8, (unsigned char*)ulVectorData, RDIAccess_Data32))    //assume error message is sufficient , lets test!!
            {
            sprintf(tyAshloadProcData.szErrorBuffer, "Failed to read sp value from 0x%02X. \n", (uint32_t)ulGlobalPrgLoadAddr);
            PrintMessage(INFO_MESSAGE, tyAshloadProcData.szErrorBuffer);
            AddStringToOutAsciiz(tyAshloadProcData.szErrorBuffer, pcOutput);
            return 0; 
            }
         //Write sp for Cortex processor
         if(TRUE != LOW_WriteRegister((int32_t)REG_ARM_R13, ulVectorData[0]))    //assume error message is sufficient , lets test!!
            {
            sprintf(tyAshloadProcData.szErrorBuffer, "Failed to set sp value 0x%02X. \n", (uint32_t)ulVectorData[0]);
            PrintMessage(INFO_MESSAGE, tyAshloadProcData.szErrorBuffer);
            AddStringToOutAsciiz(tyAshloadProcData.szErrorBuffer, pcOutput);
            return 0; 
            }

         if(GDBSERV_NO_ERROR != LOW_WritePC(ulVectorData[1]))    //assume error message is sufficient , lets test!!
            {
            sprintf(tyAshloadProcData.szErrorBuffer, "Failed to set pc at: 0x%02X. \n", (uint32_t)ulVectorData[1]);
            PrintMessage(INFO_MESSAGE, tyAshloadProcData.szErrorBuffer);
            AddStringToOutAsciiz(tyAshloadProcData.szErrorBuffer, pcOutput);
            return 0; 
            }
         }
#endif
      }
#ifdef ARM
   (void)OpenProcessorModule();
#endif
   return 1;
}

/****************************************************************************
     Function: AshloadInit
     Engineer: Vitezslav Hola
        Input: char *pcGDBOutput - output string  
       Output: none
  Description: Clear and initialize data before ashload command execution.
Date           Initials    Description
02-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void AshloadInit(char *pcGDBOutput)
{
   tyAshloadProcData.verify_download = 0;
   tyAshloadProcData.pcGDBReplyOutput = pcGDBOutput;
   if(tySI.bIsConnected)
      tyELFProcData.bBigEndian = LOW_IsBigEndian(0);
}

/**************************************************************************** 
     Function: ProcessDwarfFile
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Process DWARF file
Date           Initials    Description
02-Feb-2008    VH          Initial & cleanup
*****************************************************************************/ 
static int32_t ProcessDwarfFile(void)
{
   int32_t iError = 0;
   // open file
   tyELFProcData.pInFile = fopen(tyAshloadProcData.szFileName, "rb");
   if(tyELFProcData.pInFile == NULL)
      return ERR_F_NULL_FILE;
   // check file format
   iError = VerifyFileFormat(tyELFProcData.pInFile);
   // process ELF header
   if(!iError)
      iError = ProcessELFHeader();
   // process ELF section header table
   if(!iError)
      iError = ProcessELFSectionHeaderTable(ASHLOAD_DOWNLOAD_ELF);
   // read ELF header
   if(!iError)
      iError = ProcessELFProgramHeaderTable(ASHLOAD_DOWNLOAD_ELF);
   // process ELF header table if necessary
   if(!iError && tyAshloadProcData.verify_download)
      iError = ProcessELFProgramHeaderTable(ASHLOAD_VERIFY_ELF_DOWNLOAD);
   // close file and return error code
   fclose(tyELFProcData.pInFile);
   return iError;
}

/****************************************************************************
     Function: VerifyFileFormat
     Engineer: Vitezslav Hola
        Input: FILE *pFile - pointer to file
       Output: int32_t - error code
  Description: Veryfie if given file is realy ELF
Date           Initials    Description
02-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t VerifyFileFormat(FILE *pFile)
{
   int32_t iError = 0;
   unsigned char pucData[4];
   assert(pFile != NULL);
   // read first 4 bytes of the file
   iError = ReadBytes(pFile, 0, 1, (void *)pucData, 4);
   if(iError)
      return iError;
   // check ELF header
   if((pucData[EI_MAG0] == 0x7f) && (pucData[EI_MAG1] == 'E') && (pucData[EI_MAG2] == 'L') && (pucData[EI_MAG3] == 'F'))
      return 0;
   return ERR_NOT_AN_ELF_FILE;
}

/****************************************************************************
     Function: ProcessELFHeader
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Process the ELF file's header.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t ProcessELFHeader(void)
{
   int32_t iError = 0;
   PrintMessage(LOG_MESSAGE, "\n====== Processing ELF Header.\n");
   // read the ELF header (at the beginning of the ELF file)
   iError = ReadBytes(tyELFProcData.pInFile, 0, 1, (void *)&(tyELFProcData.ElfHeader), sizeof(tyELFProcData.ElfHeader));
   if(iError)
      return iError;
   // handle endianess
   if(tyELFProcData.bBigEndian)
      {
      SwapHalfword(&tyELFProcData.ElfHeader.e_type);
      SwapHalfword(&tyELFProcData.ElfHeader.e_machine);
      SwapWord(&tyELFProcData.ElfHeader.e_version);
      SwapWord(&tyELFProcData.ElfHeader.e_entry);
      SwapWord(&tyELFProcData.ElfHeader.e_phoff);
      SwapWord(&tyELFProcData.ElfHeader.e_shoff);
      SwapWord(&tyELFProcData.ElfHeader.e_flags);
      SwapHalfword(&tyELFProcData.ElfHeader.e_ehsize);
      SwapHalfword(&tyELFProcData.ElfHeader.e_phentsize);
      SwapHalfword(&tyELFProcData.ElfHeader.e_phnum);
      SwapHalfword(&tyELFProcData.ElfHeader.e_shentsize);
      SwapHalfword(&tyELFProcData.ElfHeader.e_shnum);
      SwapHalfword(&tyELFProcData.ElfHeader.e_shstrndx);
      }
   PrintMessage(LOG_MESSAGE," ELF Header");
   PrintMessage(LOG_MESSAGE," ----------");
   PrintMessage(LOG_MESSAGE,"  e_ident[EI_MAG1]    = 0x%02X '%c'", tyELFProcData.ElfHeader.e_ident[EI_MAG1], tyELFProcData.ElfHeader.e_ident[EI_MAG1]); 
   PrintMessage(LOG_MESSAGE,"  e_ident[EI_MAG2]    = 0x%02X '%c'", tyELFProcData.ElfHeader.e_ident[EI_MAG2], tyELFProcData.ElfHeader.e_ident[EI_MAG2]); 
   PrintMessage(LOG_MESSAGE,"  e_ident[EI_MAG3]    = 0x%02X '%c'", tyELFProcData.ElfHeader.e_ident[EI_MAG3], tyELFProcData.ElfHeader.e_ident[EI_MAG3]); 
   PrintMessage(LOG_MESSAGE,"  e_ident[EI_CLASS]   = 0x%02X", tyELFProcData.ElfHeader.e_ident[EI_CLASS]); 
   PrintMessage(LOG_MESSAGE,"  e_ident[EI_DATA]    = 0x%02X", tyELFProcData.ElfHeader.e_ident[EI_DATA]); 
   PrintMessage(LOG_MESSAGE,"  e_ident[EI_VERSION] = 0x%02X", tyELFProcData.ElfHeader.e_ident[EI_VERSION]); 
   PrintMessage(LOG_MESSAGE,"  e_type      = 0x%04X", tyELFProcData.ElfHeader.e_type); 
   PrintMessage(LOG_MESSAGE,"  e_machine   = 0x%04X", tyELFProcData.ElfHeader.e_machine); 
   PrintMessage(LOG_MESSAGE,"  e_version   = 0x%08X", tyELFProcData.ElfHeader.e_version); 
   PrintMessage(LOG_MESSAGE,"  e_entry     = 0x%08X", tyELFProcData.ElfHeader.e_entry); 
   PrintMessage(LOG_MESSAGE,"  e_phoff     = 0x%08X", tyELFProcData.ElfHeader.e_phoff); 
   PrintMessage(LOG_MESSAGE,"  e_shoff     = 0x%08X", tyELFProcData.ElfHeader.e_shoff); 
   PrintMessage(LOG_MESSAGE,"  e_flags     = 0x%08X", tyELFProcData.ElfHeader.e_flags); 
   PrintMessage(LOG_MESSAGE,"  e_ehsize    = 0x%04X", tyELFProcData.ElfHeader.e_ehsize); 
   PrintMessage(LOG_MESSAGE,"  e_phentsize = 0x%04X", tyELFProcData.ElfHeader.e_phentsize); 
   PrintMessage(LOG_MESSAGE,"  e_phnum     = 0x%04X", tyELFProcData.ElfHeader.e_phnum); 
   PrintMessage(LOG_MESSAGE,"  e_shentsize = 0x%04X", tyELFProcData.ElfHeader.e_shentsize); 
   PrintMessage(LOG_MESSAGE,"  e_shnum     = 0x%04X", tyELFProcData.ElfHeader.e_shnum); 
   PrintMessage(LOG_MESSAGE,"  e_shstrndx  = 0x%04X\n", tyELFProcData.ElfHeader.e_shstrndx); 
   // accept absolute (executable) files only
   if(tyELFProcData.ElfHeader.e_type != ET_EXEC)
      return ERR_NOT_ABSOLUTE_REC_E;
   return iError;
} 

/****************************************************************************
     Function: ProcessELFProgramHeaderTable
     Engineer: Vitezslav Hola
        Input: int32_t iMode - mode
       Output: int32_t - error code
  Description: Process the ELF file's Data via the Program Header Table.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
18-Dec-2009    DVA/JCK     Modified to allow for limitation of which can 
                           only take a max size of < 64K
****************************************************************************/
int32_t ProcessELFProgramHeaderTable(int32_t iMode)
{
   int32_t iError = 0;
   uint32_t ulProgramHeaderTableSize, ulEntry, ulWriteSize, ulDataAddress, ulRemainingBlockSize, ulMaxDataTransfer;
   uint32_t ulDataBufferSize = 0;
   Elf32_Phdr *pProgramHeaderTable = NULL;
   unsigned char *pDataBuffer = NULL;
   unsigned char *pTempDataBuffer = NULL;
//MODIFY FOR FLASH START
#ifdef FLASH_SUPPORT
   unsigned char *pBuffAddress = NULL; //For writing sections to buffer
#endif //FLASH_SUPPORT
//MODIFY FOR FLASH END
   uint32_t ulLoadSize = 0;
   //unsigned char szDataBuffer2[MAX_DATA_CONTENT_WRITE_LENGTH];
   unsigned char *szDataBuffer2;
   szDataBuffer2 = (unsigned char *)malloc(MAX_DATA_CONTENT_WRITE_LENGTH * sizeof(unsigned char));
   if (szDataBuffer2 == NULL)
   {
	   iError = ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY;
	   goto Exit;
   }
   uint32_t uiIndex;

   assert((iMode == ASHLOAD_VERIFY_ELF_DOWNLOAD) || (iMode == ASHLOAD_DOWNLOAD_ELF) ||
          (iMode == FLASH_DOWNLOAD_ELF) || (iMode == FLASH_VERIFY_ELF_DOWNLOAD));
   if(iMode == ASHLOAD_DOWNLOAD_ELF)
      {
      PrintMessage(INFO_MESSAGE, MESSAGE_INFO_DOWNLOAD_IN_PROGRESS, tyAshloadProcData.szFileName);
      }
   else if(iMode == FLASH_DOWNLOAD_ELF)
      {
#ifdef FLASH_SUPPORT
      PrintMessage(INFO_MESSAGE, MESSAGE_INFO_DOWNLOAD_IN_PROGRESS, tyProcInFileData.szFileName);
#endif //FLASH_SUPPORT
      }
   else if(iMode == ASHLOAD_VERIFY_ELF_DOWNLOAD)
      {
      PrintMessage(INFO_MESSAGE, MESSAGE_INFO_VERIFICATION_IN_PROGRESS, tyAshloadProcData.szFileName);
      }
   else if(iMode == FLASH_VERIFY_ELF_DOWNLOAD)
      {
#ifdef FLASH_SUPPORT
      PrintMessage(INFO_MESSAGE, MESSAGE_INFO_VERIFICATION_IN_PROGRESS, tyProcInFileData.szFileName);
#endif //FLASH_SUPPORT
      }
   //Message here
   PrintMessage(LOG_MESSAGE,"====== Processing Program Header Table.\n");
   if((tyELFProcData.ElfHeader.e_phoff == 0) || (tyELFProcData.ElfHeader.e_phnum == 0))
      {  // no program header table
      PrintMessage(INFO_MESSAGE,MESSAGE_WARING_NO_PROGRAM_HEADER);
      return 0;
      }
   if(tyELFProcData.ElfHeader.e_phentsize < sizeof(Elf32_Phdr))
      {  // should not happen 
      iError = ERR_IE_PROCDWRF00;
      goto Exit;
      }
   // Program Header Table Size is the number of entries * entry size
   ulProgramHeaderTableSize = (uint32_t)(tyELFProcData.ElfHeader.e_phnum * tyELFProcData.ElfHeader.e_phentsize);
   if (ulProgramHeaderTableSize < sizeof(Elf32_Phdr) * 2 ) return 0;

   pProgramHeaderTable = (Elf32_Phdr *)malloc(ulProgramHeaderTableSize);
   if(pProgramHeaderTable == NULL)
      {
      iError = ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY;
      goto Exit;
      }
   // read the complete Program Header Table
   iError = ReadBytes(tyELFProcData.pInFile, tyELFProcData.ElfHeader.e_phoff, 1, pProgramHeaderTable, ulProgramHeaderTableSize);
   if(iError)
      goto Exit;
   if(tyELFProcData.bBigEndian)
      {  // invert values for each entry in the Program Header Table
      for(ulEntry=0; ulEntry < tyELFProcData.ElfHeader.e_phnum; ulEntry++)
         {
         SwapWord(&pProgramHeaderTable[ulEntry].p_type);
         SwapWord(&pProgramHeaderTable[ulEntry].p_offset);
         SwapWord(&pProgramHeaderTable[ulEntry].p_vaddr);
         SwapWord(&pProgramHeaderTable[ulEntry].p_paddr);
         SwapWord(&pProgramHeaderTable[ulEntry].p_filesz);
         SwapWord(&pProgramHeaderTable[ulEntry].p_memsz);
         SwapWord(&pProgramHeaderTable[ulEntry].p_flags);
         SwapWord(&pProgramHeaderTable[ulEntry].p_align);
         }
      }
   // process each entry in the Program Header Table
   for(ulEntry=0; ulEntry<tyELFProcData.ElfHeader.e_phnum; ulEntry++)
      {
      sprintf(tyAshloadProcData.szErrorBuffer, " ---- PHDR Entry %d ----", (int32_t)ulEntry);
      if((iMode == ASHLOAD_VERIFY_ELF_DOWNLOAD)||(iMode == ASHLOAD_DOWNLOAD_ELF))
         {
         PrintMessage(LOG_MESSAGE, tyAshloadProcData.szErrorBuffer);
         }
      PrintMessage(LOG_MESSAGE, " p_type   = 0x%08lX", pProgramHeaderTable[ulEntry].p_type);
      PrintMessage(LOG_MESSAGE,  " p_offset = 0x%08lX", pProgramHeaderTable[ulEntry].p_offset);
      PrintMessage(LOG_MESSAGE, " p_vaddr  = 0x%08lX", pProgramHeaderTable[ulEntry].p_vaddr);
      PrintMessage(LOG_MESSAGE, " p_paddr  = 0x%08lX", pProgramHeaderTable[ulEntry].p_paddr);
      PrintMessage(LOG_MESSAGE, " p_filesz = 0x%08lX", pProgramHeaderTable[ulEntry].p_filesz);
      PrintMessage(LOG_MESSAGE, " p_memsz  = 0x%08lX", pProgramHeaderTable[ulEntry].p_memsz);
      PrintMessage(LOG_MESSAGE, " p_flags  = 0x%08lX", pProgramHeaderTable[ulEntry].p_flags);
      PrintMessage(LOG_MESSAGE, " p_align  = 0x%08lX", pProgramHeaderTable[ulEntry].p_align);
      if((pProgramHeaderTable[ulEntry].p_type == PT_LOAD) && (pProgramHeaderTable[ulEntry].p_filesz != 0) && (pProgramHeaderTable[ulEntry].p_memsz != 0))
         {
         if(pProgramHeaderTable[ulEntry].p_filesz > pProgramHeaderTable[ulEntry].p_memsz)
            {
            PrintMessage(INFO_MESSAGE, MESSAGE_WARING_FILE_SZ_GRATER_MEM_SZ);
            continue;
            }
         else
            {
            uint32_t ulIncreaseOffset = 0;
            ulLoadSize += pProgramHeaderTable[ulEntry].p_filesz;
            if(pProgramHeaderTable[ulEntry].p_offset == 0)
               {  // this Segment includes the ELF file header which is always at 0, skip over the ELF file header
               ulIncreaseOffset = sizeof(tyELFProcData.ElfHeader);
               }
            if((pProgramHeaderTable[ulEntry].p_offset + ulIncreaseOffset) == tyELFProcData.ElfHeader.e_phoff)
               {  // this Segment includes the program header table, skip over the program header table
               ulIncreaseOffset += ulProgramHeaderTableSize;
               }
            if(ulIncreaseOffset != 0)
               {
               if((pProgramHeaderTable[ulEntry].p_filesz <= ulIncreaseOffset) || (pProgramHeaderTable[ulEntry].p_memsz  <= ulIncreaseOffset))
                  continue;                                             // nothing left after skipping headers, ignore the segment
               pProgramHeaderTable[ulEntry].p_offset += ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_vaddr  += ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_paddr  += ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_filesz -= ulIncreaseOffset;
               pProgramHeaderTable[ulEntry].p_memsz  -= ulIncreaseOffset;
               }
            if(pDataBuffer == NULL)
               {  // allocate buffer space for this segments data
               pDataBuffer = (unsigned char *)malloc(pProgramHeaderTable[ulEntry].p_filesz);
               if(iError || (pDataBuffer == NULL))
                  {
                  iError = ERR_INSUFFICENT_DATA_BUFFER_MEMORY;
                  goto Exit;
                  }
               ulDataBufferSize = pProgramHeaderTable[ulEntry].p_filesz;
               }
            else if(ulDataBufferSize < pProgramHeaderTable[ulEntry].p_filesz)
               {
               // must increase the existing buffer size
				//added a extra variable pTempDataBuffer,in case if realloc return null then not to assign it to pDataBuffer which is parameter to realloc
				pTempDataBuffer = (unsigned char *)realloc((char*)pDataBuffer, pProgramHeaderTable[ulEntry].p_filesz);
               if((pTempDataBuffer == NULL))
                  {
                  iError = ERR_INSUFFICENT_DATA_BUFFER_MEMORY;
                  goto Exit;
                  }
			   pDataBuffer = pTempDataBuffer;
				ulDataBufferSize = pProgramHeaderTable[ulEntry].p_filesz;
               }
            }
         // read all data bytes
         iError = ReadBytes(tyELFProcData.pInFile, pProgramHeaderTable[ulEntry].p_offset, 1, pDataBuffer, pProgramHeaderTable[ulEntry].p_filesz);
         if(iError != 0)
            goto Exit;
         ulRemainingBlockSize = pProgramHeaderTable[ulEntry].p_filesz;
#ifdef ARM
         ulDataAddress = pProgramHeaderTable[ulEntry].p_paddr;
         uSize = pProgramHeaderTable[ulEntry].p_filesz;

         /* Added to solve the limitation of size less than < 64K */
         (void)WriteDataContentXref(pProgramHeaderTable[ulEntry].p_vaddr, &ulDataAddress, &uSize, &pDataBuffer[pProgramHeaderTable[ulEntry].p_filesz - ulRemainingBlockSize]);
         ulRemainingBlockSize = uSize;
         /* This is for CortexM3 SP initialization. */
#else
         ulDataAddress = pProgramHeaderTable[ulEntry].p_vaddr;
#endif
         ulMaxDataTransfer =  ((iMode == ASHLOAD_DOWNLOAD_ELF) ||(iMode == FLASH_DOWNLOAD_ELF)) ? (MAX_DATA_CONTENT_WRITE_LENGTH):(MAX_DATA_CONTENT_READ_LENGTH);
         while(ulRemainingBlockSize != 0)
            {
            if(ulRemainingBlockSize < ulMaxDataTransfer)
               ulWriteSize = ulRemainingBlockSize;
            else
               ulWriteSize = ulMaxDataTransfer;
            ShowProgress(ulRemainingBlockSize, pProgramHeaderTable[ulEntry].p_filesz);
            //MODIFY_FOR_FLASH_START
            //Write the sections to a buffer
            if(iMode == FLASH_DOWNLOAD_ELF)
               {
#ifdef FLASH_SUPPORT
               pBuffAddress = GetCurSectionWrBufAddress();
               if(NULL == pBuffAddress)
                  {
                  iError = ERR_MEMORY_WRITE_FAILURE;
                  goto Exit;
                  }
               //Writing to section buffer instead of to RDI if it is being invoked as part of FlashElfToSections
               if(GDBSERV_NO_ERROR != WriteSectionBuffer(pProgramHeaderTable[ulEntry].p_paddr,
                                                         pProgramHeaderTable[ulEntry].p_vaddr,
                                                         pBuffAddress, ulWriteSize,
                                                         &pDataBuffer[pProgramHeaderTable[ulEntry].p_filesz - ulRemainingBlockSize]))
                  {
                  iError = ERR_MEMORY_WRITE_FAILURE;
                  goto Exit;
                  }
#endif      //FLASH_SUPPORT
               }
            else if(iMode == ASHLOAD_DOWNLOAD_ELF)
               {

               //If this is invoked as part of Ashload command
               if(GDBSERV_NO_ERROR != LOW_WriteMemory(ulDataAddress, ulWriteSize,&pDataBuffer[pProgramHeaderTable[ulEntry].p_filesz - ulRemainingBlockSize] ))
                  {
                  iError = ERR_MEMORY_WRITE_FAILURE;
                  goto Exit;
                  }
               }
            else if(iMode == ASHLOAD_VERIFY_ELF_DOWNLOAD)
               {
				//Workaround for MIPS32-74 K Error reading memory(last but one word always zero)
				//Need to look into this issue in detail..
#ifdef ARC 
               if(LOW_ReadMemory(ulDataAddress, ulWriteSize+0x10,szDataBuffer2) != TRUE)
                  {
                  iError = ERR_MEMORY_READ_FAILURE;
                  goto Exit;
                  }
#else
               if(GDBSERV_NO_ERROR != LOW_ReadMemory(ulDataAddress, ulWriteSize+0x10,szDataBuffer2))
                  {
                  iError = ERR_MEMORY_READ_FAILURE;
                  goto Exit;
                  }
#endif
               //Workaround for MIPS32-74 K Error reading memory(last but one word always zero)
				//Need to look into this issue in detail..
               // Check each of the data bytes...
               for(uiIndex=0; uiIndex < ulWriteSize/2; uiIndex++)
                  {
                  if(pDataBuffer[uiIndex + pProgramHeaderTable[ulEntry].p_filesz - ulRemainingBlockSize] != szDataBuffer2[uiIndex])
                     {
                     sprintf(tyAshloadProcData.szErrorBuffer, "Verification failed at 0x%02X. Expected:0x%02X, read:0x%02X.", 
                             (uint32_t)(ulDataAddress + uiIndex), (uint32_t)(pDataBuffer[uiIndex + pProgramHeaderTable[ulEntry].p_filesz - ulRemainingBlockSize]), 
                             (uint32_t)(szDataBuffer2[uiIndex]));
                     iError = ERR_MEMORY_VERIFY_FAILURE;
                     goto Exit;
                     }
                  }
               }
            else if(iMode == FLASH_VERIFY_ELF_DOWNLOAD)
               {
               //Do nothing....since for FLASH vification,
               //different method is used
               }
            else
               {
               //Do nothing 
               }
            ulDataAddress += ulWriteSize;
            ulRemainingBlockSize -= ulWriteSize;
            }
         }
      }
   if(iMode == ASHLOAD_DOWNLOAD_ELF)
      {
      PrintMessage(INFO_MESSAGE, MESSAGE_INFO_FILE_DOWNLOADED, tyELFProcData.ElfHeader.e_entry, ulLoadSize);
      tyAshloadProcData.ulTotalLoad = ulLoadSize;
      }
   if(iMode == ASHLOAD_VERIFY_ELF_DOWNLOAD)
      PrintMessage(INFO_MESSAGE, MESSAGE_INFO_FILE_VERIFIED);

   Exit:
   // free buffers
   if(pDataBuffer != NULL)
      free(pDataBuffer);
   if(pProgramHeaderTable != NULL)
      free(pProgramHeaderTable);
   if(szDataBuffer2 != NULL)
	  free(szDataBuffer2);
   return iError;
}

/****************************************************************************
     Function: ShowProgress
     Engineer: Vitezslav Hola
        Input: uint32_t ulRemain - size of data to be proceed
               uint32_t ulFullSize - total size of data processed
       Output: none
  Description: Show progress message to GDB server console.
Date           Initials    Description
02-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static void ShowProgress(uint32_t ulRemain, uint32_t ulFullSize)
{
   uint32_t ulIndex;
   uint32_t ulProgressStatusBar =  30 * (ulFullSize - ulRemain) / ulFullSize;
   static uint32_t ulProgress = 0;
   // check if progress has finished
   if(ulProgressStatusBar == ulProgress)
      return;
   ulProgress = ulProgressStatusBar;
   for(ulIndex = ulProgress; (ulIndex <= ulProgressStatusBar) && (ulIndex < 31); ulIndex++)
      {
      printf(".");      
      fflush(stdout);
      }
}

/****************************************************************************
     Function: SetupErrorMessage
     Engineer: Vitezslav HOla
        Input: int32_t iError : specifies error number for download
               char *pszFileName : pointer to download file name
               char *pcOut : hexadecimal string to be returned to the GDB
       Output: none
  Description: Prints out appropiate error message to GDB server console
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use 'util' function to prepare the outstring
****************************************************************************/
static void SetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut)
{
   char *szErrorMsg = tyAshloadProcData.szErrorBuffer;
   switch(iError)
      {
      case ERR_UNEXPECTED_END_OF_FILE:
         sprintf(szErrorMsg, MESSAGE_ERR_UNEXPECTED_END_OF_FILE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_F_NULL_FILE:
         sprintf(szErrorMsg, MESSAGE_ERR_F_NULL_FILE, tyAshloadProcData.szFileName);
         PrintMessage(ERROR_MESSAGE,szErrorMsg);            
         break;
      case ERR_F_SEEK:
         sprintf(szErrorMsg, MESSAGE_ERR_F_SEEK);
         PrintMessage(ERROR_MESSAGE, szErrorMsg );
         break;
      case ERR_NOT_AN_ELF_FILE:
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_AN_ELF_FILE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_NOT_ABSOLUTE_REC_E :
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_ABSOLUTE_REC_E);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_PROCDWRF00 :
         sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF00);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INSUFFICENT_DATA_BUFFER_MEMORY :
         sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_DATA_BUFFER_MEMORY);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY :
         sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY);
         PrintMessage(ERROR_MESSAGE, szErrorMsg );
         break;
      case ERR_MEMORY_READ_FAILURE :
         sprintf(szErrorMsg, MESSAGE_ERR_MEMORY_READ_FAILURE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_MEMORY_VERIFY_FAILURE :
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_MEMORY_WRITE_FAILURE :
         sprintf(szErrorMsg, MESSAGE_ERR_MEMORY_WRITE_FAILURE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg );
         break;
      case ERR_INVALID_NUMBER_OF_PARMS :
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_NUMBER_OF_PARMS);
         PrintMessage(ERROR_MESSAGE, szErrorMsg,pszFileName);
         break;            
      case ERR_FILE_PATH_TOO_LONG :
         sprintf(szErrorMsg, MESSAGE_ERR_FILE_PATH_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_COMMAND_SWITCH_TOO_LONG:
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_SWITCH_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_COMMAND_SWITCH_USED_TWICE :
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_SWITCH_USED_TWICE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_NOT_ASHLOAD_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_ASHLOAD_COMMAND);
         PrintMessage(ERROR_MESSAGE,szErrorMsg);
         break;
      case ERR_UNKNOWN_SWITCH :
         sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_SWITCH);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);            
         break;
      case ERR_COMMAND_TOO_LONG :
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_PROCDWRF07:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF07);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_PROCDWRF08:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF08);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF04:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF04);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_DUMMY_ALREADY_SHOWN:
         sprintf(szErrorMsg, MESSAGE_ERR_DUMMY_ALREADY_SHOWN);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF03:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF03);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_MEM_MALLOC_C:
         sprintf(szErrorMsg, MESSAGE_ERR_MEM_MALLOC_C);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;             
      case ERR_IE_READDWRF02:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF02);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF01:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF01 );
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF07:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF07);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_FILE_NAME:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_FILE_NAME);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      default:
         sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_ERROR_OCURRED);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      }
   strcat(szErrorMsg, "\n");
   AddStringToOutAsciiz(GDB_CLIENT_ERROR_MESSAGE_PREFIX, pcOut);
   AddStringToOutAsciiz(szErrorMsg, pcOut);
}

/****************************************************************************
     Function: ParseCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput  : input string
               char *pcOutput : output string
       Output: int32_t - error code
  Description: parse ashload command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Added pcOutput (output string)
*****************************************************************************/
static int32_t ParseCommand(char *pcInput, char *pcOutput)
{
   char szSwitchBuffer[MAX_SWITCH_LENGTH + 1];
   int32_t iCounter = 0;
   unsigned char bFileNameEnd = 0;
   unsigned char bFileInsideQuotationMarks = 0;
   char *pcBuffer = tyAshloadProcData.szFileName;
   char *pcIndex = pcInput;
   assert(pcInput != NULL);
   if(*pcInput == '\0')
      return ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND;
   if(*pcInput != ' ')
      return ERR_NOT_ASHLOAD_COMMAND;
   if(strlen(pcInput) > (_MAX_PATH * 2))
      return ERR_COMMAND_TOO_LONG;
   // get rid off spaces
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND;
   bFileInsideQuotationMarks = (*pcIndex == '"');
   if(bFileInsideQuotationMarks)
      pcIndex++;
   iCounter = 0;
   while(!bFileNameEnd && *pcIndex != '\0')
      {
      if(iCounter >= _MAX_PATH)
         {
         SetupErrorMessage(ERR_FILE_PATH_TOO_LONG,pcInput, pcOutput);
         return ERR_FILE_PATH_TOO_LONG;
         }
      *pcBuffer++ = *pcIndex++;
      bFileNameEnd = bFileInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bFileInsideQuotationMarks)
         return ERR_INVALID_FILE_NAME;                                  // only one " invalid file name
      }
   else
      pcIndex++;
   if(strlen(tyAshloadProcData.szFileName) < _MAX_PATH)
      *pcBuffer = '\0';
   PrintMessage(LOG_MESSAGE,"Elf File name: %s.", tyAshloadProcData.szFileName);
   while(*pcIndex != '\0')
      {
      while(CheckSpace(*pcIndex))
         pcIndex++;
      if(*pcIndex == '\0')
         return 0;
      pcBuffer = szSwitchBuffer;
      iCounter = 0;
      while(!CheckSpace(*pcIndex) && (*pcIndex != '\0'))
         {
         if(iCounter >= MAX_SWITCH_LENGTH)
            {
            *pcBuffer = '\0';
            return ERR_COMMAND_SWITCH_TOO_LONG;
            }
         *pcBuffer++ = *pcIndex++;
         iCounter++;
         }
      *pcBuffer = '\0';
      // commands  DO WE RECOGNIZE SWITCH hardcoded to verify
      if(strcmp(szSwitchBuffer, "--verify") == 0)
         {
         if(tyAshloadProcData.verify_download)
            return ERR_COMMAND_SWITCH_USED_TWICE;
         tyAshloadProcData.verify_download = 1;
         PrintMessage(LOG_MESSAGE,"Download Verification Added.");
         }
      else
         return ERR_UNKNOWN_SWITCH;
      }
   return 0;
}

/****************************************************************************
     Function: ProcessELFSectionHeaderTable
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Determine useful section offsets
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
08-Feb-2008    NCH         Remove 'goto'
17-May-2010    DK          Modified for Microchip Flash support
****************************************************************************/
int32_t ProcessELFSectionHeaderTable(int32_t iMode)
{
   uint32_t ulEntry, ulSectionHeaderTableSize;
   Elf32_Shdr *pSectionHeaderTable = NULL;
#ifdef  FLASH_SUPPORT
   uint32_t ulWriteSize = 0,ulMaxDataTransfer = 0,ulRemainingBlockSize = 0;
   uint32_t ulDataBufferSize = 0;
   unsigned char *pucDataBuffer = NULL;
   unsigned char *pucBuffAddress = NULL;
#endif
   int32_t iError = 0;
   char szSectionName[MAX_IDENTIFIER_NAME_SIZE +1];
/*#ifdef  FLASH_SUPPORT
   NOREF(pucBuffAddress);
   NOREF(pucDataBuffer);
   NOREF(ulWriteSize);
   NOREF(ulMaxDataTransfer);
   NOREF(ulRemainingBlockSize);
   NOREF(ulDataBufferSize);
   NOREF(ulDataAddress);
#endif*/

   assert((iMode == ASHLOAD_VERIFY_ELF_DOWNLOAD) || (iMode == ASHLOAD_DOWNLOAD_ELF) ||
          (iMode == FLASH_DOWNLOAD_ELF) || (iMode == FLASH_VERIFY_ELF_DOWNLOAD));

   PrintMessage(LOG_MESSAGE,"\n====== Processing Section Header Table.\n");
   if((tyELFProcData.ElfHeader.e_shoff == 0) || (tyELFProcData.ElfHeader.e_shnum == 0))
      {
      PrintMessage(INFO_MESSAGE, MESSAGE_WARNING_NO_SECTION_HEADER_TABLE);
      return 0;
      }
   if(tyELFProcData.ElfHeader.e_shstrndx == 0)
      {  // no Section Name String Table, so, no debug info, strange for an abs file, but ignore
      PrintMessage(INFO_MESSAGE, MESSAGE_WARNING_NO_SECTION_NAME_STRING_TABLE);
      return 0;
      }
   if(tyELFProcData.ElfHeader.e_shstrndx >= tyELFProcData.ElfHeader.e_shnum)
      return ERR_IE_PROCDWRF07;                                         // should not happen
   if(tyELFProcData.ElfHeader.e_shentsize < sizeof(Elf32_Shdr))
      return ERR_IE_PROCDWRF08;                                         // should not happen
   // Section Header Table Size is the number of entries * entry size
   ulSectionHeaderTableSize = (uint32_t)(tyELFProcData.ElfHeader.e_shnum * tyELFProcData.ElfHeader.e_shentsize);

   if (ulSectionHeaderTableSize < sizeof(Elf32_Shdr) * 2)
	   return ERR_IE_PROCDWRF08;

   // allocate space for the complete Section Header Table
   pSectionHeaderTable = (Elf32_Shdr *)malloc(ulSectionHeaderTableSize);
   if(pSectionHeaderTable == NULL)
      return ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY;
   // read the complete Section Header Table
   iError = ReadBytes(tyELFProcData.pInFile, tyELFProcData.ElfHeader.e_shoff, 1, pSectionHeaderTable, ulSectionHeaderTableSize);
   if(iError)
      {
      if(pSectionHeaderTable != NULL)  //lint !e774 Always true. We can remove it. 
         free(pSectionHeaderTable);
      FreeSectionBuffer(SectionNames);
      return iError;
      }
   if(tyELFProcData.bBigEndian)
      {   // invert values for each entry in the Section Header Table
      for(ulEntry=0; ulEntry < tyELFProcData.ElfHeader.e_shnum; ulEntry++)
         {
         SwapWord(&pSectionHeaderTable[ulEntry].sh_name);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_type);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_flags);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_addr);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_offset);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_size);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_link);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_info);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_addralign);
         SwapWord(&pSectionHeaderTable[ulEntry].sh_entsize);
         }
      }
#ifdef ARM
   // Process each entry in the Section Header Table to find the 
   // start and end addresses of loadable sections...
   for(ulEntry=0; ulEntry<tyELFProcData.ElfHeader.e_shnum; ulEntry++)
      {
      if((pSectionHeaderTable[ulEntry].sh_size  != 0)              &&
         (pSectionHeaderTable[ulEntry].sh_type  == SHT_PROGBITS)   &&
         ((pSectionHeaderTable[ulEntry].sh_flags & SHF_ALLOC) != 0))
         {
         iError = AddToRangeArray(&tyELFProcData.tySectionRanges,
                                  pSectionHeaderTable[ulEntry].sh_addr,
                                  (pSectionHeaderTable[ulEntry].sh_addr + pSectionHeaderTable[ulEntry].sh_size));
         if(iError)
            {
            if(pSectionHeaderTable != NULL)  //lint !e774 Always true. We can remove it. 
               free(pSectionHeaderTable);
            FreeSectionBuffer(SectionNames);
            return iError;
            }
         }
      }
#endif
   // Setup a section buffer for the "section names" string table
   iError = AllocSectionBuffer(SectionNames, pSectionHeaderTable[tyELFProcData.ElfHeader.e_shstrndx].sh_addr,
                               pSectionHeaderTable[tyELFProcData.ElfHeader.e_shstrndx].sh_offset, 
                               pSectionHeaderTable[tyELFProcData.ElfHeader.e_shstrndx].sh_size);
   if(iError)
      {
      if(pSectionHeaderTable != NULL)
         free(pSectionHeaderTable);
      FreeSectionBuffer(SectionNames);
      return iError;
      }
   // Process each entry in t he Section Header Table to find the begin offsets and sizes of of sections that we need e.g. debug, abbrev, line...
   PrintMessage(LOG_MESSAGE, HEADER_OF_SECTION_HEADER_TABLE_ENTRIES);
   for(ulEntry=0; ulEntry < tyELFProcData.ElfHeader.e_shnum; ulEntry++)
      {
      if((pSectionHeaderTable[ulEntry].sh_name != 0) && (pSectionHeaderTable[ulEntry].sh_offset != 0) && (pSectionHeaderTable[ulEntry].sh_addr != 0))
         {  // get this sections name
         iError = ReadSectionString(tyELFProcData.pInFile, SectionNames, pSectionHeaderTable[ulEntry].sh_name, 1,  szSectionName, sizeof(szSectionName));
         if(iError)
            {
            if(pSectionHeaderTable != NULL)
               free(pSectionHeaderTable);
            FreeSectionBuffer(SectionNames);
            return iError;
            }
#ifdef ARM
         if(strcmp(szSectionName,".text") == 0)
            {
            /* This is valid only for Cortex-M3, for CortexM3 SP initialization. */
            ulGlobalPrgLoadAddr = pSectionHeaderTable[ulEntry].sh_addr;
            }
#endif
         PrintMessage(LOG_MESSAGE,SECTION_HEADER_TABLE_ENTRY, ulEntry, szSectionName, pSectionHeaderTable[ulEntry].sh_size, pSectionHeaderTable[ulEntry].sh_addr);
         }
      }

#ifdef FLASH_SUPPORT
   for(ulEntry = 0;ulEntry < tyELFProcData.ElfHeader.e_shnum;ulEntry++)
      {
      if(pSectionHeaderTable[ulEntry].sh_size != 0 &&
         pSectionHeaderTable[ulEntry].sh_name != 0 &&
         pSectionHeaderTable[ulEntry].sh_offset != 0 &&
         pSectionHeaderTable[ulEntry].sh_type != SHT_NOBITS)
         {
         PrintMessage(LOG_MESSAGE, " sh_name        = 0x%08lX", pSectionHeaderTable[ulEntry].sh_name);
         PrintMessage(LOG_MESSAGE, " sh_addr        = 0x%08lX", pSectionHeaderTable[ulEntry].sh_addr);
         PrintMessage(LOG_MESSAGE, " sh_entsize     = 0x%08lX", pSectionHeaderTable[ulEntry].sh_entsize);
         PrintMessage(LOG_MESSAGE, " sh_offset      = 0x%08lX", pSectionHeaderTable[ulEntry].sh_offset);
         PrintMessage(LOG_MESSAGE, " sh_addralign   = 0x%08lX", pSectionHeaderTable[ulEntry].sh_addralign);
         PrintMessage(LOG_MESSAGE, " sh_flags       = 0x%08lX", pSectionHeaderTable[ulEntry].sh_flags);
         PrintMessage(LOG_MESSAGE, " sh_info        = 0x%08lX", pSectionHeaderTable[ulEntry].sh_info);
         PrintMessage(LOG_MESSAGE, " sh_size        = 0x%08lX", pSectionHeaderTable[ulEntry].sh_size);
         PrintMessage(LOG_MESSAGE, " sh_link        = 0x%08lX", pSectionHeaderTable[ulEntry].sh_link);
         PrintMessage(LOG_MESSAGE, " sh_type        = 0x%08lX", pSectionHeaderTable[ulEntry].sh_type);
         if(pucDataBuffer == NULL)
            {  // allocate buffer space for this segments data
            pucDataBuffer = (unsigned char *)malloc(pSectionHeaderTable[ulEntry].sh_size);
            if(iError || (pucDataBuffer == NULL))
               {
               iError = ERR_INSUFFICENT_DATA_BUFFER_MEMORY;
               goto Exit;
               }
            ulDataBufferSize = pSectionHeaderTable[ulEntry].sh_size;
            }
         else if(ulDataBufferSize < pSectionHeaderTable[ulEntry].sh_size)
            {
            // must increase the existing buffer size
            pucDataBuffer = (unsigned char *)realloc((char*)pucDataBuffer, pSectionHeaderTable[ulEntry].sh_size);
            if((pucDataBuffer == NULL))
               {
               iError = ERR_INSUFFICENT_DATA_BUFFER_MEMORY;
               goto Exit;
               }
            ulDataBufferSize = pSectionHeaderTable[ulEntry].sh_size;
            }
         iError = ReadBytes(tyELFProcData.pInFile, pSectionHeaderTable[ulEntry].sh_offset, 1, pucDataBuffer, pSectionHeaderTable[ulEntry].sh_size);
         ulRemainingBlockSize = pSectionHeaderTable[ulEntry].sh_size;
         if(iMode == FLASH_DOWNLOAD_ELF)
            {
            ulMaxDataTransfer =  ((iMode == ASHLOAD_DOWNLOAD_ELF) ||(iMode == FLASH_DOWNLOAD_ELF)) ? (MAX_DATA_CONTENT_WRITE_LENGTH):(MAX_DATA_CONTENT_READ_LENGTH);
            while(ulRemainingBlockSize != 0)
               {
               if(ulRemainingBlockSize < ulMaxDataTransfer)
                  {
                  ulWriteSize = ulRemainingBlockSize;
                  }
               else
                  {
                  ulWriteSize = ulMaxDataTransfer;
                  }                  
               ShowProgress(ulRemainingBlockSize, pSectionHeaderTable[ulEntry].sh_size);
               pucBuffAddress = GetCurSectionWrBufAddress();
               if(NULL == pucBuffAddress)
                  {
                  iError = ERR_MEMORY_WRITE_FAILURE;
                  goto Exit;
                  }
               //Writing to section buffer instead of to RDI if it is being invoked as part of FlashElfToSections
               if(GDBSERV_NO_ERROR != WriteSectionBuffer(pSectionHeaderTable[ulEntry].sh_addr,
                                                         pSectionHeaderTable[ulEntry].sh_addr,
                                                         pucBuffAddress, ulWriteSize,
                                                         &pucDataBuffer[pSectionHeaderTable[ulEntry].sh_size - ulRemainingBlockSize]))
                  {
                  iError = ERR_MEMORY_WRITE_FAILURE;
                  goto Exit;
                  }
               ulRemainingBlockSize -= ulWriteSize;
               }//End of while loop
            }
         }

      if(iError != 0)
         goto Exit;
      }
   Exit :
   if(pucDataBuffer != NULL)
      {
      free(pucDataBuffer);
      }
#endif
   // free buffers
   if(pSectionHeaderTable != NULL)
      free(pSectionHeaderTable);
   FreeSectionBuffer(SectionNames);
   return iError;
}

/****************************************************************************
     Function: SwapHalfword
     Engineer: Vitezslav Hola
        Input: unsigned short *pusValue - pointer to value to swap
       Output: none
  Description: Swap bytes in halfword
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static void SwapHalfword(unsigned short *pusValue)
{
   unsigned short usValue;
   assert(pusValue != NULL);
   usValue = *pusValue;
   *pusValue = ((usValue >> 8) & 0x00FF) | ((usValue << 8) & 0xFF00);
}

/****************************************************************************
     Function: SwapWord
     Engineer: Vitezslav Hola
        Input: uint32_t *pulValue - pointer to value to swap
       Output: none
  Description: Swap bytes in word
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static void SwapWord(uint32_t *pulValue)
{
   uint32_t ulValue;
   assert(pulValue != NULL);
   ulValue = *pulValue;
   *pulValue = ((ulValue >> 24) & 0x000000FF) | ((ulValue >> 8) & 0x0000FF00) | ((ulValue << 8) & 0x00FF0000) | ((ulValue << 24) & 0xFF000000);
}

/****************************************************************************
     Function: CheckSpace
     Engineer: Vitezslav Hola
        Input: int32_t iChar - character
       Output: unsigned char - 1 if character is space, 0 otherwise
  Description: Swap bytes in word
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
unsigned char CheckSpace(int32_t iChar)
{
   if((iChar == ' ') || ((iChar >= 9) && (iChar <= 13)))
      return 1;                                                         // it is space or character between TAB and CR
   else
      return 0;
}
#ifdef ARM
static int32_t AddToRangeArray(TyRangeArray *ptyRangeArray, uint32 uStartAddr, uint32 uEndAddr)
{
   uint32   uCount;

   for(uCount=0; uCount<ptyRangeArray->uRangeCount; uCount++)
      {
      // new range starts after current entry, try next entry
      if(uStartAddr > ptyRangeArray->ptyRanges[uCount].uEndAddr)
         continue;

      // new range ends before current entry, exit loop to insert at this point
      if(uEndAddr < ptyRangeArray->ptyRanges[uCount].uStartAddr)
         break;

      // new range overlaps current entry, so merge ranges
      if(uStartAddr < ptyRangeArray->ptyRanges[uCount].uStartAddr)
         {
         ptyRangeArray->ptyRanges[uCount].uStartAddr = uStartAddr;
         }
      if(uEndAddr > ptyRangeArray->ptyRanges[uCount].uEndAddr)
         {
         ptyRangeArray->ptyRanges[uCount].uEndAddr = uEndAddr;
         }
      return NO_ERROR;
      }

   return InsertRange(ptyRangeArray, uCount, uStartAddr, uEndAddr);
}

/****************************************************************************
     Function: InsertRange
     Engineer: Patrick Shiels
        Input: ptyRangeArray* : pointer to range information structure
               uIndex         : array index to insert at
               uStartAddr     : range start address
               uEndAddr       : range end address +1
       Output: TyError        : NO_ERROR or error number
  Description: Insert a range into an array of ranges allocating memory for the array if needed.

Date           Initials    Description
19-Apr-2008    PJS         initial
****************************************************************************/
static int32_t InsertRange(TyRangeArray *ptyRangeArray, uint32 uIndex, uint32 uStartAddr, uint32 uEndAddr)
{
   TyError  ErrRet = NO_ERROR;
   uint32   uCount;

   // sanity check
   if(ptyRangeArray->uRangeCount > ptyRangeArray->uRangeAlloc)
      return GDBSERV_ERROR;

   // extend the allocated array size if necessary
   if(ptyRangeArray->uRangeCount == ptyRangeArray->uRangeAlloc)
      {
      // allocate another chunk of ranges
      uint32 uOldSize = ptyRangeArray->uRangeAlloc * sizeof(TyAddrRange);
      uint32 uNewSize = (ptyRangeArray->uRangeAlloc + RANGE_ARRAY_ALLOC_CHUNK) * sizeof(TyAddrRange);
      ptyRangeArray->ptyRanges = (TyAddrRange*)realloc((char*)ptyRangeArray->ptyRanges, uNewSize);
      //ptyRangeArray->ptyRanges = (TyAddrRange*)arealloc((char*)ptyRangeArray->ptyRanges, uNewSize, uOldSize, IE_UTILDWRF05, &ErrRet);
      //if (ErrRet || (ptyRangeArray->ptyRanges == NULL))
      //  return MEM_MALLOC_C;
      // clear the new entries to zero
      memset((char*)((char*)ptyRangeArray->ptyRanges + uOldSize), 0, uNewSize-uOldSize);
      ptyRangeArray->uRangeAlloc += RANGE_ARRAY_ALLOC_CHUNK;
      }

   if(uIndex > ptyRangeArray->uRangeCount)
      uIndex = ptyRangeArray->uRangeCount;

   // move any ranges above the insertion point up 1
   for(uCount=ptyRangeArray->uRangeCount; uCount>uIndex; uCount--)
      {
      ptyRangeArray->ptyRanges[uCount] = ptyRangeArray->ptyRanges[uCount-1];
      }

   // insert the new range here
   ptyRangeArray->ptyRanges[uIndex].uStartAddr = uStartAddr;
   ptyRangeArray->ptyRanges[uIndex].uEndAddr   = uEndAddr;
   ptyRangeArray->uRangeCount++;

   return ErrRet;
}

/****************************************************************************
     Function: WriteDataContentXref
     Engineer: Patrick Shiels
        Input: uVirtAddr  : virtual address of this data block
               uLoadAddr  : load address of this data block
               pubData*   : pointer to data to write
               uSize      : size of data block
       Output: TyError: NO_ERROR or error number
  Description: Write OMF data content records for one large block of data by cross
               referencing section address range info to determine loadable data ranges

Date           Initials    Description
19-Apr-2008    PJS         initial
****************************************************************************/
static int32_t WriteDataContentXref(uint32 uVirtAddr, uint32* uLoadAddr, uint32* puSize, ubyte* pubData)
{
   TyError        ErrRet = NO_ERROR;
   uint32         uCount, uStartSecAddr = 0, uEndSecAddr, uWriteOffset, uWriteSize = 0;
   //ubyte*         pubWriteData;
   TyRangeArray * ptyRangeArray = &tyELFProcData.tySectionRanges;

   if((ptyRangeArray->uRangeCount == 0) || (ptyRangeArray->ptyRanges == NULL))
      return NO_ERROR;

   // check each loadable data section
   for(uCount=0; uCount<ptyRangeArray->uRangeCount; uCount++)
      {
      uStartSecAddr = ptyRangeArray->ptyRanges[uCount].uStartAddr;   // start section addr
      uEndSecAddr   = ptyRangeArray->ptyRanges[uCount].uEndAddr;     // end section addr +1

      // check if this section is part of the data block
      if((uStartSecAddr >= uVirtAddr) && (uStartSecAddr < (uVirtAddr + *puSize)))
         {
         // write the portion of this block corresponding to the section
         uWriteOffset = uStartSecAddr - uVirtAddr;
         *uLoadAddr   = *uLoadAddr + uWriteOffset;
         pubData = (ubyte*)(pubData + uWriteOffset);
         uWriteSize   = uEndSecAddr - uStartSecAddr;
         if((uWriteOffset + uWriteSize) > *puSize)
            uWriteSize = *puSize - uWriteOffset;
         }
      } 

   *puSize = uWriteSize;
   return ErrRet;
}
#endif
