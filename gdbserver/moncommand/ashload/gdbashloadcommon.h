/****************************************************************************
       Module: gdbashloadcommon.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ashload global processing information
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
22-Feb-2010    SVL        Modified for scripting support
****************************************************************************/
#ifndef GDBASHLOADCOMMON_H_
#define GDBASHLOADCOMMON_H_

#include "gdbserver/elf.h"
#define ASHLOAD_MESSAGE_BUFFER (_MAX_PATH + _MAX_PATH + 20)
#define FLASH_ELFTOSECTIONS_MESSAGE_BUFFER_SIZE (_MAX_PATH + _MAX_PATH + 20)
#define FLASH_SREC_MESSAGE_BUFFER_SIZE (_MAX_PATH + _MAX_PATH + 20)
#define FLASH_BINARY_MESSAGE_BUFFER_SIZE (_MAX_PATH + _MAX_PATH + 20)
#define MAX_FILE_FORMAT_LEN 10
#define MAX_ADDRESS_BYTE_LEN 10
#ifdef ARM
typedef struct
      {
      uint32       uStartAddr;
      uint32       uEndAddr;
      } TyAddrRange;

typedef struct
      {
      uint32       uRangeCount;
      uint32       uRangeAlloc;
      TyAddrRange* ptyRanges;
      } TyRangeArray;
#endif
typedef struct _ElfProcessData
{
   Elf32_Ehdr ElfHeader;                                                // ELF Header
   FILE* pInFile;                                                       // Output File pointer (CSO file)
   int32_t bBigEndian;                                                      // TRUE if input file is in big-endian format
   uint32_t uEntryPoint;                                           // program entry point
   uint32_t uLowestTextAddress;                                    // lowest .text address referenced in A.OUT file
   uint32_t uTextOffset;                                           // file offset to .text section in A.OUT file
   uint32_t uTextLength;                                           // length of .text section in A.OUT file
   uint32_t uLowestDataAddress;                                    // lowest .data address referenced in A.OUT file
   uint32_t uDataOffset;                                           // file offset to .data section in A.OUT file
   uint32_t uDataLength;                                           // length of .data section in A.OUT file
#ifdef ARM
   TyRangeArray  tySectionRanges;
#endif
} ElfProcessData;

// global variables

typedef struct _AshloadProcessData 
{
   char szFileName[_MAX_PATH];
   int32_t verify_download;
   char* pcGDBReplyOutput;
   char  szErrorBuffer[ASHLOAD_MESSAGE_BUFFER];
   uint32_t ulTotalLoad;
} AshloadProcessData;

//The structure used for processing the monitor command ElfToSectionProcessData
//Fills after parsing the command

typedef struct _FileProcessData 
{
   char szFileName[_MAX_PATH];          //The file name extracted from user request
   char szFileFormat[MAX_FILE_FORMAT_LEN ];
   char* pcGDBReplyOutput;              //The pointer to output reply of "FlashElfToSections" to GDB
   char  szErrorBuffer[FLASH_ELFTOSECTIONS_MESSAGE_BUFFER_SIZE];
   //uint32_t ulTotalLoad;
   /* to indicate script mode */
   int32_t iScriptMode;
   uint32_t ulAddress; //Address for binary
} FileProcessData;

#endif   // GDBASHLOADCOMMON_H_

