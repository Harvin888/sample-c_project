/****************************************************************************
       Module: gdbashloadutils.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ashload util function, and types
Date           Initials    Description
08-Feb-2008    VH          Initial & cleaup
****************************************************************************/
#ifndef GDBASHLOADUTILS_H_
#define GDBASHLOADUTILS_H_

// Linux build change...
#include "protocol/common/types.h"

#define MaxSectionNumber            1
typedef enum { SectionNames   } TySection;

void StringWithinSectionBuffer(TySection Section, unsigned char *pbStringBeginInBuffer, unsigned char *pbStringEndInBuffer, uint32_t *pulStringLength);
int32_t RefillSectionBuffer(FILE * pFile, TySection Section);
int32_t ReadString(FILE *pFile, uint32_t ulFilePosition, unsigned char bSetFilePosition, char *pszString, uint32_t ulMaxStringSize, uint32_t *pulBytesRead);
void AshloadUtilsClean(void);
void FreeSectionBuffer(TySection tySection);
int32_t AllocSectionBuffer(TySection tySection, uint32_t ulSectionAddr, uint32_t ulSectionBegin, uint32_t ulSectionSize);
int32_t ReadBytes(FILE *pFile, uint32_t ulFilePosition, unsigned char bSetFilePosition, void *pBuffer, uint32_t ulNumBytes);
int32_t ReadSectionString(FILE *pFile, TySection tySection, uint32_t ulReadOffset, unsigned char bSetReadOffset, char *pszString, uint32_t ulMaxStringSize);
void WriteReplyToGDB(char* pcInput);

#endif   // GDBASHLOADUTILS_H_

