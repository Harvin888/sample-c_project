/****************************************************************************
       Module: gdbashload.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ashload entry function
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBASHLOAD_H_
#define GDBASHLOAD_H_

#ifdef ARM
//TODO CHECK
#define SHF_ALLOC     0x2        /* Section occupies memory during execution */
#define RANGE_ARRAY_ALLOC_CHUNK (16)
#endif

int32_t Ashload(char *pcInput, char *pcOutput);

#endif   // GDBASHLOAD_H_
