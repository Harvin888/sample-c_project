/****************************************************************************
       Module: gdbmonitor.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, GDB Monitor Command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
08-AUG-2009    DK          Modified to support new monitor commands
22-Mar-2010    DK          Added monitor commands for flash support
22-Mar-2010    VK          Modified for Lint based building
23-Jun-2010	   RSB		   Modified for Advanced Breakpoint support
30-NOv-2011    SV          Modified to add Zephyr Trace Support
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbopt.h"
#include "gdbserver/mips/gdbmips.h"
#include "gdbserver/moncommand/gdbmonitor.h"
#include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
#include "gdbserver/moncommand/ashload/gdbashload.h"
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
#include "gdbserver/jtagcon/gdbmipsjmsg.h"
#include "gdbserver/jtagcon/gdbjutl.h"
#ifdef FLASH_SUPPORT
   #include "gdbserver/moncommand/flashcommands/flashsec/gdbflash.h"
#endif
// To holds the temporary result buffer to be sent back to gdb
// before converting to hex
// static char szResult[GDBSERV_PACKET_BUFFER_SIZE];

// external variables
extern TyServerInfo tySI;
#ifdef  FLASH_SUPPORT
extern unsigned long ulGlobalFlashStartAddress ;
extern unsigned long ulGlobalFlashEndAddress ;
#endif
#ifdef MIPS
int bIsMicroChip = 0;
#endif
// local functions
static int GetMonCommand(char* pcInput, char *pcOutput);
static int MonitorHelp(char *pcInput, char *pcOutput);
static int DoHWReset(char *pcInput, char *pcOutput);
//static int SetPeripheralRegister (char *pcInput, char *pcOutput);
//static int GetPeripheralRegisters (char *pcInput, char *pcOutput);
//static int GetPeripheralRegGroups (char *pcInput, char *pcOutput);
static int Reset (char *pcInput, char *pcOutput);
static int SwReset (char *pcInput, char *pcOutput);
static int Connect (char *pcInput, char *pcOutput);
static int InitialiseTLB (char *pcInput, char *pcOutput);
static int EnableOSMMU (char *pcInput, char *pcOutput);
static int ReadTLBentry (char *pcInput, char *pcOutput);
static int WriteTLBentry (char *pcInput, char *pcOutput);
static int OSAppMMU(char *pcInput, char *pcOutput);
static int SetupMicrochipFlashAddress( char *pcInput , char *pcOutput);
static int EraseMicrochipFlash( char *pcInput , char *pcOutput);
static int SetupAdvanceBreakPoint(char *pcInput, char *pcOutput);
static int DeleteAdvancedBreakPoint(char *pcInput, char *pcOutput);
static int GetEndian(char *pcInput, char *pcOutput);
static int ReadPReg(char *pcInput, char *pcOutput);
static int WritePReg(char *pcInput, char *pcOutput);
static int ReadCP0Reg(char *pcInput, char *pcOutput);
static int WriteCP0Reg(char *pcInput, char *pcOutput);
static int FetchTrace(char *pcInput, char *pcOutput);
static int InvCache(char *pcInput, char *pcOutput);
static int ReadWriteEJTAGReg(char *pcInput, char *pcOutput);
static int WriteEJTAGReg(char *pcInput, char *pcOutput);
static int PassElf(char *pcInput, char *pcOutput);
static int ConfigureTrace(char *pcInput, char *pcOutput); 
static int SetupMicrochipFlash(char *pcInput, char *pcOutput);

typedef int (*TyMonCommandFunction)(char *pcInput, char *pcOutput);

typedef struct _TyMonCommandEntry 
   {
   const char *monCommand;
   const char *pszHelpArg;
   const char *pszHelpLine1;
   const char *pszHelpLine2;
   const char *pszHelpLine3;
   const char *pszHelpLine4;
   TyMonCommandFunction tyMonCommandFunction;
   char bShowThisCommand;
   } TyMonCommandEntry;

// local variables
static TyMonCommandEntry monitorCommandTable[]  =
{
   {
      "help",
      "",
      "List all supported Ashling monitor commands" ,
      "with appropriate parameter help",
      "",
      "",
      MonitorHelp,
      0
   },
   {
      "inittlb",
      "",
      "Initialises MIPS TLB to safe defaults",
      " (no mappings) ",
      "",
      "",
      InitialiseTLB,
      1
   },
   {
      "osmmu",
      " <OS name> <addr>",
      "Sets up the Operating System MMU support.",
      "See user manual for details.",
      "",
      "",
      EnableOSMMU,
      1
   },
   {
      "ashload",
      " <file> <--verify>",
      "Load ELF format <file>.",
      "<file> should be given an absolute path.",
      "--verify will readback/verify data",
      "downloaded (default off).",
      Ashload,
      1
   },
   {
      "connect",
      "",
      "Connects to the target device",
      "Use only when GDB Server is started in PFXD",
      "mode (using --PFXD-mode option).",
      "",
      Connect,
      1
   },
   {
      "hwreset",
      "",
      "Reset target (hardware reset via RST* pin).",
      "",
      "",
      "",
      DoHWReset,
      0
   },
   {
      "readtlb",
      "",
      "Displays MIPS TLB entries.",
      "",
      "",
      "",
      ReadTLBentry,
      1
   },
   {
      "writetlb",
      " <EL0> <EL1> <EH> <PgMsk> <Indx>",
      " ",
      "Edit TLB entry.",
      "<EL0>,<EL1>,<EH>: EntryLo and EntryHi values",
      "<PgMask>: PageMask, <Indx>: TLB index",
      WriteTLBentry,
      1
   },
   {"swreset",
      " ",
      "Resets the target by issuing a software",
      "reset",
      "",
      "",
      SwReset,
      0
   },
   {"reset",
      " ",
      "Resets the target by issuing either a",
      "hardware/software reset. Decision on which",
      "mechanism to be used is made based on reset",
      "options in FAMLYMIP.XML",
      Reset,
      1
   },
   {"readpreg",
	  "<Address>",
	  "",
	  "Read Peripheral register with address.",
	  "",
	  "",
	  ReadPReg,
	  0
   },
   {"writepreg",
      " <Address> <value> ",
      "Sets the value to the given ",
      "peripheral register. ",
      "<value> should be in hexadecimal format",
      "",
      WritePReg,
      0
   },
   {
      "osappmmu",
      "<OS name> <app context> <addr>",
      "<OS name> currently supports only linux26",
      "<app context> is the application context",
      "<addr> application pgd address",
      "",
      OSAppMMU,
      0
   },
#ifdef FLASH_SUPPORT
   {
      "flashPrepareChunk ",
      "<section num> <utility buf addr> <utility buf size>",
      "Transfer chunk of data to utility buffer",
      "",
      "",
      "",
      FlashPrepareChunk,
      0
   },
   {
      "flashProcInFile",
      "<ELF file name> <file type> <relocation address for binary file only>",
      "Converts the ELF file to sections in memory",
      "",
      "",
      "",
      FlashProcInFile,
      0
   },
   {
      "flashGetSizeOfSection",
      "<section num>",
      "Gets the size of section",
      "",
      "",
      "",
      FlashGetSizeOfSection,
      0
   },
   {
      "flashGetOffsetOfSection",
      "<section num>",
      "Gets the offset of section",
      "",
      "",
      "",
      FlashGetOffsetOfSection,
      0
   },
   {
      "flashVerify",
      "",
      "Verifies the Flash Program",
      "",
      "",
      "",
      FlashVerify,
      0
   },
#endif
   {
      "setupMicrochipFlash",
      "",
      "Intitializes ram for flash programming",
      "",
      "",
      "",
      SetupMicrochipFlash,
      0
   },
   {
      "SetupMicrochipFlashAddress",
      "<flash start address> <flash end address>",
      "",
      "Intitializes flash address for flash programming",
      "",
      "",
      SetupMicrochipFlashAddress,
      0
   },
   {
      "EraseMicrochipFlash",
      "",
      "",
      "Erase Microchip flash",
      "",
      "",
      EraseMicrochipFlash,
      0
   },
    {
      "SetupAdvanceBreakPoint",
      " <break point type> <start address> <Address Mask>",
      "<data address> <data mask> <ASID enable> <ASID value> <TransType",
      "Sets advanced break points",
      "",
      "",
      SetupAdvanceBreakPoint,
      0
    },
    {
      "deladvbpt",
      "<start address>",
      " ",
      "Delete advanced break point",    
      "<start address> should be in hexadecimal format",
      "",
      DeleteAdvancedBreakPoint,
      0
    },
   {"getendian",
      "",
      "Get the endianess from target",
      "",
      "",
      "",
      GetEndian,
      0
   },
   {"readcp0",
      "<regno> <select>",
      "Read Coprocessor register",
      "",
      "",
      "",
      ReadCP0Reg,
      1
   },
   {"writecp0",
	  "<regno> <select> <value>",
	  "Write Coprocessor register",
	  "",
	  "",
	  "",
	  WriteCP0Reg,
	  1
   },
   {"fetchtrace",
	  "<begPtr> <endPtr>",
	  "Fetch trace from the target and saves to trace.tmp file",
	  "",
	  "",
	  "",
	  FetchTrace,
	  1
   },
   {"rwejtagreg",
	  "<ejtaginst> <IR length> <DR length> <reg value>",
	  "Read/Write EJTAG register",
	  "For read operation reg value is optional",
	  "",
	  "",
	  ReadWriteEJTAGReg,
	  1
   },
   {"writeejtagreg",
	  "<ejtaginst> <value>",
	  "Write EJTAG register",
	  "",
	  "",
	  "",
	  WriteEJTAGReg,
	  1
   },
   {"invcache",
      "",
      "Invalidate the whole of secondary cache",
      "",
      "",
      "",
      InvCache,
      1
   },
   {"pasself",
      "",
      "Passing the fullpath of .elf",
      "",
      "",
      "",
      PassElf,
      1
   },
   {"configtrace",
    "<TcReg value1> <TcReg value2> <TcReg value3>",
    "Configure Zephyr Trace",
    "",
    "",
    "",
    ConfigureTrace,
    1
   }
};
#define MAX_MON_COMMAND_ENTRIES ((int)(sizeof(monitorCommandTable) / sizeof(TyMonCommandEntry)))
/****************************************************************************
     Function: ShowGDBMonitorCommandsHelp
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: Show GDB monitor command help       
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void ShowGDBMonitorCommandsHelp(void)
{
   int iPadding; 
   int iStrLen; 
   int iIndex;
   char szBuffer[_MAX_PATH*2];

   PrintMessage(HELP_MESSAGE, "\n\nSupported GDB MONITOR commands:\n\n");
   for(iIndex=0; iIndex<MAX_MON_COMMAND_ENTRIES; iIndex++)
      {
      //PrintMessage(HELP_MESSAGE , "%s" , "\n");
      if(1 == monitorCommandTable[iIndex].bShowThisCommand)
         {
         sprintf(szBuffer, "%s%s%s","monitor ",monitorCommandTable[iIndex].monCommand, monitorCommandTable[iIndex].pszHelpArg);
         PrintMessage(HELP_MESSAGE, "%s", szBuffer);
         if((iStrLen = (int)strlen(szBuffer)) < GDBSERV_MON_HELP_COL_WIDTH-1)
            iPadding = GDBSERV_MON_HELP_COL_WIDTH - iStrLen;
         else
            iPadding = 1;
         if(monitorCommandTable[iIndex].pszHelpLine1[0] != '\0')
            PrintMessage(HELP_MESSAGE, "%*s%s\n", iPadding, " ", monitorCommandTable[iIndex].pszHelpLine1);
         if(monitorCommandTable[iIndex].pszHelpLine2[0] != '\0')
            PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine2);
         if(monitorCommandTable[iIndex].pszHelpLine3[0] != '\0')
            PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine3);
         if(monitorCommandTable[iIndex].pszHelpLine4[0] != '\0')
            PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine4);
         }
      }  
}

/****************************************************************************
     Function: ParseMonitorCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: select particular monitor command handler and invoke it
Date           Initials    Description
08-Feb-2008    VH          Initial
08-AUG-2009    DK           Modified to print error message if the command not found
*****************************************************************************/
int ParseMonitorCommand(char *pcInput, char *pcOutput)
{
   int iIndex;
   char szBuffer[256];
   pcOutput[0] = '\0';
   assert(pcInput!=NULL);
   // get monitor command
   if(!GetMonCommand(pcInput, pcOutput))
      return 0;
   // parse command
   for(iIndex = 0; iIndex < MAX_MON_COMMAND_ENTRIES; iIndex++)
      {
      if(!strncmp(monitorCommandTable[iIndex].monCommand, tySI.pszMonitorCommand, strlen(monitorCommandTable[iIndex].monCommand)))
         return monitorCommandTable[iIndex].tyMonCommandFunction(tySI.pszMonitorCommand + strlen(monitorCommandTable[iIndex].monCommand), pcOutput);
      }
   sprintf( szBuffer , "Command not supported.\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}

/****************************************************************************
     Function: GetMonCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput  : hexadecimal string representing monitor command
               char *pcOutput : hexadecimal string to be returned to the GDB
       Output: int - 1 if command is ok, 0 otherwise
  Description: Convert monitor command from hexadecimal to ascii string.
               String will be stored into tySI.pszMonitorCommand
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use 'util' function to prepare the outstring
*****************************************************************************/
static int GetMonCommand(char* pcInput, char *pcOutput)
{
   unsigned int uiStrLen = strlen(pcInput);
   char szBuffer[_MAX_PATH*2];
   // check if string is really hex
   if(uiStrLen % 2)
      {  // not correct string, print message to user
      sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
      PrintMessage(ERROR_MESSAGE, szBuffer);
      strcat(szBuffer, "\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   // check if string is not too long
   if(uiStrLen >= (2 * _MAX_PATH))
      {
      sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
      PrintMessage(ERROR_MESSAGE, szBuffer);
      strcat(szBuffer, "\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   // process command
   for(uiStrLen = 0; uiStrLen < strlen(pcInput) ; uiStrLen+=2)
      {
      int iTemp = FromHex(pcInput[uiStrLen]);
      if(iTemp == -1)
         {
         sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
         PrintMessage(ERROR_MESSAGE, szBuffer);
         strcat(szBuffer, "\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return 0;
         }
      tySI.pszMonitorCommand[uiStrLen / 2] = (char)(iTemp << 4);  //lint !e701
      iTemp = FromHex(pcInput[uiStrLen + 1]);
      if(iTemp == -1)
         {
         sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
         PrintMessage(ERROR_MESSAGE, szBuffer);
         strcat(szBuffer, "\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return 0;
         }
      tySI.pszMonitorCommand[uiStrLen/2] |= (char)iTemp;
      }
   if((uiStrLen / 2) < _MAX_PATH)
      tySI.pszMonitorCommand[uiStrLen / 2] = '\0'; // terminate command string
   //sprintf(AshloadProcData.szErrorBuffer,);
   PrintMessage(LOG_MESSAGE, "Encoded command '%s'.\n", tySI.pszMonitorCommand);
   return 1;
}
/****************************************************************************
     Function: MonitorHelp
     Engineer: Dileep Kumar K
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: select particular monitor command handler and invoke it
Date           Initials    Description
30-JUL-2009    DK          Initial
*****************************************************************************/
static int MonitorHelp(char *pcInput, char *pcOutput)
{
   int iPadding; 
   int iStrLen; 
   int iIndex;
   char szBuffer[_MAX_PATH*2];
   NOREF(pcInput);
   sprintf( szBuffer,"%s" ,"\n\nSupported GDB MONITOR commands:\n\n");
   AddStringToOutAsciiz(szBuffer,pcOutput);
   for(iIndex=0; iIndex<MAX_MON_COMMAND_ENTRIES; iIndex++)
      {
      if(1 == monitorCommandTable[iIndex].bShowThisCommand)
         {
         sprintf(szBuffer, "%s%s%s","monitor ",monitorCommandTable[iIndex].monCommand, monitorCommandTable[iIndex].pszHelpArg);
         AddStringToOutAsciiz(szBuffer,pcOutput);
         if((iStrLen = (int)strlen(szBuffer)) < GDBSERV_MON_HELP_COL_WIDTH-1)
            iPadding = GDBSERV_MON_HELP_COL_WIDTH - iStrLen;
         else
            //iPadding = 1;   
            iPadding = GDBSERV_MON_HELP_COL_WIDTH - (int)strlen(szBuffer);
         if(monitorCommandTable[iIndex].pszHelpLine1[0] != '\0')
            {
            if(iPadding < 0)
               {
               AddStringToOutAsciiz("\n",pcOutput);
               sprintf(szBuffer, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine1);
               AddStringToOutAsciiz(szBuffer,pcOutput);
               }
            else
               {
               sprintf(szBuffer, "%*s%s\n", iPadding, " ", monitorCommandTable[iIndex].pszHelpLine1);
               AddStringToOutAsciiz(szBuffer,pcOutput);
               }
            }
         if(monitorCommandTable[iIndex].pszHelpLine2[0] != '\0')
            {
            sprintf(szBuffer, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine2);
            AddStringToOutAsciiz(szBuffer,pcOutput);
            }
         if(monitorCommandTable[iIndex].pszHelpLine3[0] != '\0')
            {
            sprintf(szBuffer, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine3);
            AddStringToOutAsciiz(szBuffer,pcOutput);
            }
         if(monitorCommandTable[iIndex].pszHelpLine4[0] != '\0')
            {
            sprintf(szBuffer, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine4);   
            AddStringToOutAsciiz(szBuffer,pcOutput);
            }
         }
     // AddStringToOutAsciiz("\n" , pcOutput);
      }
   return 1;
}

/****************************************************************************
     Function: InitialiseTLB
     Engineer: Dileep Kumar K
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: select particular monitor command handler and invoke it
Date           Initials    Description
30-JUL-2009    DK          Initial
*****************************************************************************/
static int InitialiseTLB(char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH];
   int iErrRet=0;
   tyTLBEntry       tyEntry;
   unsigned short usTLBcount=0;
   unsigned short usEntry      = 0x0;
   unsigned long  ulVPN2       = 0xA0000000;
   // int data = 0;
   NOREF(pcInput);
   memset(&tyEntry,0x0,sizeof(tyEntry));
   //Get the count of TLB entries
   iErrRet=GetTLBCount( &usTLBcount );
   if( NO_ERROR != iErrRet)
      return FALSE;
   //Initialize each TLB entry
   for(usEntry = 0x0; usEntry < usTLBcount; usEntry++)
      {
      //data = (int)usEntry*4 ;
      tyEntry.EntryHi.Bits.VPN2  = ulVPN2 >> VPN2_SHIFT; 
      iErrRet=LOW_WriteTLB(&tyEntry, usEntry);
      // iErrRet=LOW_WriteTLB(&tyEntry, usEntry);
      if( NO_ERROR != iErrRet)
         return FALSE;
      ulVPN2 +=0x2000;
      }
   sprintf(szBuffer,"OK.\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}
/****************************************************************************
     Function: EnableOSMMU
     Engineer: Dileep Kumar K
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: select particular monitor command handler and invoke it
Date           Initials    Description
30-JUL-2009    DK          Initial
*****************************************************************************/
static int EnableOSMMU(char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH];
   char cOSMMUAddress[_MAX_PATH]= "0";
   int iTokenIndex=0;
   char *pszToken=0;
   char *pcEnd = NULL;
   int iRetVal=0;
   TyOSType tyOS=linux26;
   unsigned long ulOSMMU;
   assert(pcInput);
   //parse the command line arguments
   pszToken=strtok(pcInput," ");
   while(pszToken != 0)
      {
      //get type of OS 
      if(0 == iTokenIndex)
         {
         if( !strcasecmp(pszToken, "Linux26"))
            tyOS=linux26;
         else
            {
            sprintf(szBuffer, "Unsupported <OS name> %s\n" ,pszToken);
            AddStringToOutAsciiz(szBuffer, pcOutput);
            return 1;
            }
         }
      //Get OS MMU Address
      else if( 1 == iTokenIndex)
         strcpy(cOSMMUAddress, pszToken);
      else
         {
         iTokenIndex++;
         break;
         }
      iTokenIndex++;
      pszToken = strtok( 0, " " );
      }
   if(0 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <OS Name> <addr>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   if(1 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing argument <addr>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   if(2 < iTokenIndex)
      {
      sprintf(szBuffer,"Invalid argument after <addr>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   //This converts string to unsigned long
   ulOSMMU=strtoul(cOSMMUAddress,&pcEnd ,16);
   if(ulOSMMU == 0)
      {
      PrintMessage(ERROR_MESSAGE, "strtoul function failed\n" );
      return 0;
      }
   iRetVal=Low_EnableOSMMU(tyOS, &ulOSMMU);
   if( iRetVal==0)
      {
      sprintf(szBuffer,"OK.\n");
      AddStringToOutAsciiz(szBuffer,pcOutput);
      return 1;
      }
   else
      {
      sprintf(szBuffer,"Unable to set OSMMU.\n");
      AddStringToOutAsciiz(szBuffer,pcOutput);
      }
   return 1;
}
/****************************************************************************
     Function: ReadTLBentry
     Engineer: Dileep Kumar K
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
        Output: int - 1 to send response, 0 - ignore
  Description: read TLB entries
Date           Initials    Description
05-AUG-2009    DK         Initial
****************************************************************************/
static int  ReadTLBentry (char *pcInput, char *pcOutput)
{
   tyTLBEntry *ptyTLBEntry = NULL ; //need to free if it is allocated
   unsigned char ucNumOfTLBEntry = 0 ;
   //char *pPageSize;
   char szBuffer[_MAX_PATH];
   int inTLBIndex;
   int iErrRet=0;
   int iSpace=6;
   assert(pcInput!=NULL);
   assert(pcOutput!=NULL);
   NOREF(pcInput);
   NOREF(iSpace);
   iErrRet=GetTLBCount((unsigned short *)&ucNumOfTLBEntry );
   if( NO_ERROR != iErrRet)
      return FALSE;
   ptyTLBEntry = (tyTLBEntry*)malloc(sizeof(tyTLBEntry)*ucNumOfTLBEntry);
   if( NULL == ptyTLBEntry )
      return FALSE;
   memset(ptyTLBEntry,0,sizeof(tyTLBEntry)*ucNumOfTLBEntry);
   iErrRet=LOW_ReadTLB(&ptyTLBEntry,&ucNumOfTLBEntry);
   if( NO_ERROR == iErrRet )
      {
      free(ptyTLBEntry);
      return FALSE;
      }
   sprintf(szBuffer,"\n%-*s %-*s %-*s %-*s G %-*s C1  D1 V1 %-*s C2  D2 V2\n"
           ,5,"Index",10,"PageSize",5,"ASID",10,"VPN2",10,"PFN1",10,"PFN2");
   AddStringToOutAsciiz(szBuffer,pcOutput);
   for(inTLBIndex=0;inTLBIndex<ucNumOfTLBEntry;++inTLBIndex)
      {
      sprintf(szBuffer,"0x%02X  0x%-8X 0x%02X  0x%-8X %d 0x%08X %-3d %2d %2d 0x%08X %2d %2d %2d\n",
              inTLBIndex,
              ptyTLBEntry[inTLBIndex].PageMask.Bits.Mask,
              ptyTLBEntry[inTLBIndex].EntryHi.Bits.ASID,
              ptyTLBEntry[inTLBIndex].EntryHi.Bits.VPN2<<1,
              ptyTLBEntry[inTLBIndex].EntryLo0.Bits.G,
              ptyTLBEntry[inTLBIndex].EntryLo0.Bits.PFN,               
              ptyTLBEntry[inTLBIndex].EntryLo0.Bits.C,
              ptyTLBEntry[inTLBIndex].EntryLo0.Bits.D,
              ptyTLBEntry[inTLBIndex].EntryLo0.Bits.V,
              ptyTLBEntry[inTLBIndex].EntryLo1.Bits.PFN,
              ptyTLBEntry[inTLBIndex].EntryLo1.Bits.C,
              ptyTLBEntry[inTLBIndex].EntryLo1.Bits.D,
              ptyTLBEntry[inTLBIndex].EntryLo1.Bits.V
             );
      AddStringToOutAsciiz(szBuffer,pcOutput);
      }
   free(ptyTLBEntry);
   return 1;
}
/****************************************************************************
     Function: ValidateData
     Engineer: Dileep Kumar K
        Input: char *pcAddress   
       Output: int - 1 to send response, 0 - ignore
  Description: This is used to check weather the data is 8 nibbles
Date           Initials    Description
27-APR-2010      DK         Initial
****************************************************************************/
static int ValidateData(char *pcAddress)
{
   char *pszAddress ;
   /*This is used to check weather the data is 8 nibbles*/
   int iCount = 0;
   /*This assumes the data is in Hexadecimal format*/
   if(strncmp(pcAddress , "0x" , 2) ||strncmp(pcAddress , "0X" , 2))
      pszAddress = pcAddress + 2;
   else
      pszAddress = pcAddress ;
   while(*pszAddress !='\0')
      {
      /*This checks weather each bit is a valid bit i.e 0-9 or A-Z or a-z*/
      if((*pszAddress >= 48 && *pszAddress <= 57)||
         (*pszAddress >= 97 && *pszAddress <= 122)||
         (*pszAddress >= 65 && *pszAddress <= 90))
         {
         iCount++;
         pszAddress++;
         }
      else
         return 1;
      }
   if( iCount <= 8)
      return 0;
   else
      return 1;
}
/****************************************************************************
     Function: WriteTLBentry
     Engineer: Dileep Kumar K
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
        Output: int - 1 to send response, 0 - ignore
  Description: Write  TLB entry
Date           Initials    Description
5-MAR-2010      DK         Initial
****************************************************************************/
static int WriteTLBentry(char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH];
   unsigned short usCount = 0;
   tyTLBEntry tytyTLBEntry;
   char *pcEnd = NULL;
   int iTokenIndex = 0;
   int iErrRet = 0;
   unsigned long ulGetValue = 0;
   char *pszToken = 0;
   char cGetAddress[128];
   memset(&tytyTLBEntry,0x0,sizeof(tytyTLBEntry));
   pszToken = strtok(pcInput , " ");
   while(pszToken != 0)
      {
      if(iTokenIndex == 0)/*Get the EntryLo0 data*/
         {
         strcpy(cGetAddress , pszToken);
         /*This will check weather the EntryLo0 is 8-nibble data or not*/
         iErrRet = ValidateData(cGetAddress);
         if(iErrRet == 1)
            {
            sprintf(szBuffer,"Invalid data for EntryLo0\n");
            AddStringToOutAsciiz(szBuffer,pcOutput);
            return 1;
            }
         tytyTLBEntry.EntryLo0.Data = strtoul(cGetAddress , NULL ,16);/*Convert String value to unsigned long value*/
         strcpy(cGetAddress , " ");
         iTokenIndex++;
         }
      else if(iTokenIndex == 1)/*Get the EntryLo1 data*/
         {
         strcpy(cGetAddress , pszToken);
         /*This will check weather the EntryLo1 is 8-nibble data or not*/
         iErrRet = ValidateData(cGetAddress);
         if(iErrRet == 1)
            {
            sprintf(szBuffer,"Invalid data for EntryLo1\n");
            AddStringToOutAsciiz(szBuffer,pcOutput);
            return 1;
            }
         tytyTLBEntry.EntryLo1.Data = strtoul(cGetAddress , &pcEnd ,16);/*Convert String value to unsigned long value*/
         strcpy(cGetAddress , " ");
         iTokenIndex++;
         }
      else if(iTokenIndex == 2)/*Get EntryHi data*/
         {
         strcpy(cGetAddress , pszToken);
         /*This will check weather the EntryHi is 8-nibble data or not*/
         iErrRet = ValidateData(cGetAddress);
         if(iErrRet == 1)
            {
            sprintf(szBuffer,"Invalid data for EntryHi\n");
            AddStringToOutAsciiz(szBuffer,pcOutput);
            return 1;
            }
         tytyTLBEntry.EntryHi.Data = strtoul(cGetAddress , NULL ,16);/*Convert String value to unsigned long value*/
         strcpy(cGetAddress , " ");
         iTokenIndex++;
         }
      else if(iTokenIndex == 3)/*Get PageMask information*/
         {
         strcpy(cGetAddress , pszToken);
         /*This will check weather the PageMask is 8-nibble data or not*/
         iErrRet = ValidateData(cGetAddress);
         if(iErrRet == 1)
            {
            sprintf(szBuffer,"Invalid data for PageMask\n");
            AddStringToOutAsciiz(szBuffer,pcOutput);
            return 1;
            }
         tytyTLBEntry.PageMask.Data = strtoul(cGetAddress , NULL ,16);/*Convert String value to unsigned long value*/
         strcpy(cGetAddress , " ");
         iTokenIndex++; 
         }
      else if(iTokenIndex == 4)/*Get the index value to write the TLB at a particular entry*/
         {
         strcpy(cGetAddress , pszToken);
         usCount = (unsigned short)strtoul(cGetAddress , NULL ,16);/*Convert String value to unsigned long value*/
         strcpy(cGetAddress , " ");
         iTokenIndex++;
         }
      pszToken = strtok(0 , " ");
      }

   /*Error Handling.It returns error value corresponding to  the index value*/
   if(0 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <EntryLo0> <EntryLo1> <EntryHi> <PageMask> <index>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   else if( 1 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <EntryLo1> <EntryHi> <PageMask> <index>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   else if(2 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <EntryHi> <PageMask> <index>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   else if(3 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <PageMask> <index>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   else if(4 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <index>.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   else if(5 < iTokenIndex)
      {
      sprintf(szBuffer,"Too many arguments.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }
   else
      {
      /*Writing TLB entry at a Time*/
      iErrRet=LOW_WriteTLB(&tytyTLBEntry, usCount);
      if( NO_ERROR != iErrRet)
         return FALSE;
      NOREF(ulGetValue);
      sprintf(szBuffer,"OK.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      iTokenIndex = 0;
      }
   return 1;
}
/****************************************************************************
     Function: DoHWReset
     Engineer: Vitezslav Hola
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output
       Output: int - 1 to send response, 0 to ignore
  Description: Do target hardware reset.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Added response message
*****************************************************************************/
static int DoHWReset(char *pcInput, char *pcOutput)
{
   char  szBuffer[_MAX_PATH];
   int bReturn=TRUE;

   NOREF(pcInput);
   // do hardware reset


   if(GDBSERV_NO_ERROR != LOW_ResetTarget())
      {
      AddStringToOutAsciiz(tySI.szLastError, pcOutput);
      return bReturn;
      }

   sprintf(szBuffer,"OK\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return bReturn;
}

/****************************************************************************
     Function: Connect
     Engineer: Jobbymol C
        Input: *pcInput : command param string with white char at the beginning 
       Output: boolean stating if operation was successful
  Description: Connect to a target device 

Date           Initials    Description
20-Mar-2008      JM          Initial

****************************************************************************/
static int Connect (char *pcInput, char *pcOutput)
{

   int  bReturn = 1;
   char szDiskWare1[_MAX_PATH];
   char szFirmWare [_MAX_PATH];
   char szFpgaWare1[_MAX_PATH];
   char szFpgaWare2[_MAX_PATH];
   char szFpgaWare3[_MAX_PATH];
   char szFpgaWare4[_MAX_PATH];
   char szBuffer[GDBSERV_PACKET_BUFFER_SIZE];

   NOREF(pcInput);

   // Check whether already connected
   if(tySI.bIsConnected)
      {
      sprintf(szBuffer,"OK\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   // initialize communication with target
   if(GDBSERV_NO_ERROR != LOW_PFXDConnect())
      {
      sprintf(szBuffer,"%s",tySI.szLastError);
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   tySI.bIsConnected = TRUE;
   sprintf(szBuffer,"OK\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);

   PrintMessage(INFO_MESSAGE, "Connected to target device configured as: %s\n(currently in %s Endian mode).",
                tySI.tyProcConfig.szProcName,
                LOW_IsBigEndian(0)?"Big":"Little");

   (void)LOW_GetDWFWVersions(szDiskWare1,
                             szFirmWare,
                             szFpgaWare1,
                             szFpgaWare2,
                             szFpgaWare3,
                             szFpgaWare4);

   PrintMessage(INFO_MESSAGE, "Connected to target via %s (software: %s) at %s.",
                GetEmulatorName(),
                szDiskWare1,
                tySI.szJtagFreq);

   return bReturn;
}

/****************************************************************************
     Function: SwReset
     Engineer: Jobbymol beginning
        Input: *pcInput : command param string with white char at the beginning 
       Output: boolean stating if operation was successful
  Description: Does a software reset

Date           Initials    Description
20-Mar-2008      JM          Initial

****************************************************************************/
static int SwReset (char *pcInput, char *pcOutput)
{
   int bReturn = TRUE;
   char szBuffer[_MAX_PATH];

   NOREF(pcInput);

   // Check whether already connected
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   if(GDBSERV_NO_ERROR != LOW_SWReset())
      {
      AddStringToOutAsciiz(tySI.szLastError, pcOutput);
      return bReturn;
      }

   sprintf(szBuffer,"OK\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return bReturn;
}

/****************************************************************************
     Function: Reset
     Engineer: Jobbymol C
        Input: *pcInput : command param string with white char at the beginning 
       Output: boolean stating if operation was successful
  Description: Does a processor reset in a preferred way as per processor database

Date           Initials    Description
20-Mar-2008      JM          Initial

****************************************************************************/
static int Reset (char *pcInput, char *pcOutput)
{
   int bReturn = TRUE;
   char szBuffer[_MAX_PATH];

   NOREF(pcInput);
   // Check whether already connected
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   if(GDBSERV_NO_ERROR != LOW_ResetTarget())
      {
      AddStringToOutAsciiz(tySI.szLastError, pcOutput);
      return bReturn;
      }

   sprintf(szBuffer,"OK\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return bReturn;
}
#if 0
/****************************************************************************
     Function: GetPeripheralRegGroups
     Engineer: Jiju George
        Input: *pcInput : command param string with white char at the beginning 
       Output: boolean stating if operation was successful
  Description: Returns list of defined peripheral register groups with index in below format
               1,reggroup1
               2,reggroup2
               3,reggroup3

Date           Initials    Description
25-Apr-2008      JG          Initial

****************************************************************************/
static int GetPeripheralRegGroups (char *pcInput, char *pcOutput)
{
   int bReturn = TRUE;
   char szIndex[_MAX_PATH] = {"\0"};
   int iIndex=0;
   char szBuffer[_MAX_PATH];

   NOREF(pcInput);
   // Check whether already connected
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   // Copy all peripheral register group names to output 
   // Clear temp result first
   szResult[0] = '\0';
   PrintMessage(DEBUG_MESSAGE, "tySI.uiGroupDefArrayEntryCount:%d",tySI.uiGroupDefArrayEntryCount);
   for(;iIndex<(int)tySI.uiGroupDefArrayEntryCount;++iIndex)
      {
      PrintMessage(DEBUG_MESSAGE, "GetPeripheralRegGroups:%d:%s",iIndex,tySI.ptyGroupDefArray[iIndex].pszGroupName);
      sprintf(szIndex,"%d",iIndex);
      strcat(szResult,szIndex);
      strcat(szResult,",");
      strcat(szResult,tySI.ptyGroupDefArray[iIndex].pszGroupName);
      strcat(szResult,"\n");
      }

   AddStringToOutAsciiz(szResult, pcOutput);   
   return bReturn;
}

/****************************************************************************
     Function: GetPeripheralRegisters
     Engineer: Jiju George
        Input: *pcInput : command param string with white char at the beginning 
                          Expects a peripheral register group index of which
                          peripheral registers are to be retrieved.
       Output: boolean stating if operation was successful
  Description: Returns the list of defined peripheral registers in the groups
               given peripheral register group. Each entry has a register index, 
               register name and value in below format:
               index1,regname1,value1
               index2,regname2,value2
               index3,regname3,value3

Date           Initials    Description
25-Apr-2008      JG          Initial

****************************************************************************/
static int GetPeripheralRegisters (char *pcInput, char *pcOutput)
{
   int bReturn = TRUE;
   char szTempBuf[_MAX_PATH] = {"\0"};
   unsigned long ulValue = 0;
   char szValue[_MAX_PATH] = {"\0"};
   int iGroupIndex =0;
   int iIndex = 0;
   char szBuffer[_MAX_PATH];

   // Check whether already connected
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }


   // Find register group index from input first
   iGroupIndex = atoi(pcInput);
   if(iGroupIndex < (int)tySI.uiRegDefArrayAllocCount)
      {

      // Clear temp result first
      szResult[0] = '\0';

      // Copy all registers and value
      iIndex=tySI.ptyGroupDefArray[iGroupIndex].iStartRegIndex;
      for(;iIndex<tySI.ptyGroupDefArray[iGroupIndex].iEndRegIndex;++iIndex)
         {
         sprintf(szTempBuf,"%d",iIndex);
         strcat(szResult,szTempBuf);
         strcat(szResult,",");
         strcat(szResult,tySI.ptyRegDefArray[iIndex].pszRegName);
         strcat(szResult,",");
         if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(&(tySI.ptyRegDefArray[iIndex]),&ulValue))
            {
            ulValue = 0;
            }
         sprintf(szValue,"%lx",ulValue);
         strcat(szResult,szValue);
         /* Added for PathFinder-XD's Peripheral register view */
         strcat(szResult,",");
         sprintf(szTempBuf,"%d",tySI.ptyRegDefArray[iIndex].iRegDefType);
         strcat(szResult,szTempBuf);
         strcat(szResult,",");
         sprintf(szTempBuf,"%u",tySI.ptyRegDefArray[iIndex].ubBitSize);
         strcat(szResult,szTempBuf);
         strcat(szResult,",");
         sprintf(szTempBuf,"%u",tySI.ptyRegDefArray[iIndex].ubBitOffset);
         strcat(szResult,szTempBuf); 
         strcat(szResult,"\n");
         }

      AddStringToOutAsciiz(szResult, pcOutput);
      }
   else
      {
      sprintf(szBuffer,"Invalid register group index.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      }

   return bReturn;
}

/****************************************************************************
     Function: SetPeripheralRegister
     Engineer: Jiju George
        Input: *pcInput : command param string with white char at the beginning 
                          Expects a peripheral register name of which
                          value has to be set followed by the value to be set
                          in hexa-decimal format.
       Output: boolean stating if operation was successful
  Description: Sets the given value to the peripheral register with the given 
               name

Date           Initials    Description
25-Apr-2008      JG          Initial

****************************************************************************/
static int SetPeripheralRegister (char *pcInput, char *pcOutput)
{
   int bReturn = TRUE;
//   int iCharIndex = 0;
//   int iNumChars = 0;
   char *pszToken = 0;
   unsigned long lValue = 0;
   int iRegIndex= -1;
   int iTokenIndex = 0;
   char *pszEndPtr=0;
   char szBuffer[_MAX_PATH];
   errno=0;
   // Check whether already connected
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   // Find Register Index and value 
   // Find Value 
   pszToken = strtok( pcInput, " " );
   while( pszToken != 0 )
      {
      if(0 == iTokenIndex)
         {
         iRegIndex = atoi(pszToken);   
         }

      if(1 == iTokenIndex)
         {
         lValue = strtoul(pszToken,&pszEndPtr,16);
         if(errno == ERANGE)
         /* && 
         (lValue == ULONG_MAX || lValue == LONG_MIN)) || 
         (errno != 0 && lValue == 0))*/
            {
            sprintf(szBuffer,"Invalid value.\n");
            AddStringToOutAsciiz(szBuffer, pcOutput);
            return bReturn;
            }

         if(pszEndPtr == pszToken)
            {
            sprintf(szBuffer,"Invalid value2.\n");
            AddStringToOutAsciiz(szBuffer, pcOutput);
            return bReturn;
            }

         break; //Got all the expected tokens. So enough.
         }

      ++iTokenIndex;
      pszToken = strtok( 0, " " );
      }

   // Set Value
   if( iRegIndex >=0 && iRegIndex < (int)tySI.uiRegDefArrayAllocCount )
      {
      if(GDBSERV_NO_ERROR != LOW_WriteRegisterByRegDef(&(tySI.ptyRegDefArray[iRegIndex]),(unsigned long)lValue))
         {
         sprintf(szBuffer,"Register write failed.\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return bReturn;
         }
      else
         {
         sprintf(szBuffer,"OK.\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         }
      }
   else
      {
      sprintf(szBuffer,"Invalid register index.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   return bReturn;
}
#endif
/****************************************************************************
     Function: OSAppMMU
     Engineer: Suresh P.C
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Enables OS App MMU handling
Date           Initials    Description
20-Oct-2009    SPC         Initial
*****************************************************************************/
static int OSAppMMU(char *pcInput, char *pcOutput)
{
   const int MAX_APP_CONTEXT_SIZE = 5;
   const int MAX_APP_PGD_SIZE = 10;
   char szBuffer[_MAX_PATH];
   char szAppMMUAddress[MAX_APP_PGD_SIZE]= "0";
   char szOSAppContext[MAX_APP_CONTEXT_SIZE]= "0";
   int iTokenIndex=0;
   char *pszToken=0;
   char *pcEnd = NULL;
   int iRetVal=0;
   unsigned long ulAppPgd, ulAppContext;

   assert(pcInput);
   //parse the command line arguments
   pszToken=strtok(pcInput," ");
   while(pszToken != 0)
      {
      //Get OS APP context Address
      if( 0 == iTokenIndex)
         strcpy(szOSAppContext, pszToken);
      else if( 1 == iTokenIndex)
         strcpy(szAppMMUAddress, pszToken);
      else
         {
         iTokenIndex++;
         break;
         }
      iTokenIndex++;
      pszToken = strtok( 0, " " );
      }
   if(0 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <OS Name> <context> <addr>.\n");
      goto ErrorExit;
      }
   if(1 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <context> <addr>.\n");
      goto ErrorExit;
      }
   if(2 < iTokenIndex)
      {
      sprintf(szBuffer,"Invalid argument after <addr>.\n");
      goto ErrorExit;
      }
   //This converts string to unsigned long
   ulAppPgd=strtoul(szAppMMUAddress,&pcEnd ,16);
   ulAppContext=strtoul(szOSAppContext,&pcEnd ,16);

   iRetVal=Low_SetOSAppMMU(ulAppContext, ulAppPgd);
   if( iRetVal==0)
      {
      sprintf(szBuffer,"OK.\n");
      }
   else
      {
      sprintf(szBuffer,"Unable to set OSMMU.\n");
      }

   ErrorExit:
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}
/****************************************************************************
     Function: CheckDeviceName
     Engineer: 
        Input: char *pszName -  monitor input command string
       Output: 
  Description: 
Date           Initials    Description
17-MAY-2010    DK          initial
*****************************************************************************/
static int CheckDeviceName(const char *pszName)
{

   if(0 == strcmp((const char*)tySI.tyProcConfig.szProcName ,pszName ))
      {
      return 1;
      }
   return 0;
}

/****************************************************************************
     Function: SetupMicrochipFlashAddress
     Engineer: 
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Set up Flash start and end address for Microchip
               This function  used only for Microchip
Date           Initials    Description
17-MAY-2010    DK          initial
*****************************************************************************/
static int SetupMicrochipFlashAddress( char *pcInput , char *pcOutput)
{
   char *pcEnd = NULL;
   char szBuffer[_MAX_PATH]={0};
   char *pszToken=0;
   int iArgCount = 0;
   unsigned long ulFlashStartAddr = 0;
   unsigned long ulFlashEndAddr = 0;
   assert(pcInput);
   assert(pcOutput);
   pszToken = strtok(pcInput , " ");
   if(!pszToken)
      {
      sprintf(szBuffer , "Missing Flash start and end addresses.\n");
      return 1;
      }
   while(pszToken != NULL)
      {
      if(0 == iArgCount)
         {
         ulFlashStartAddr = strtoul(pszToken , &pcEnd , 16);

         }
      else if(1 == iArgCount)
         {
         ulFlashEndAddr = strtoul(pszToken , &pcEnd , 16);
         }
      iArgCount++;
      pszToken = strtok(NULL , " "); 
      }
   if( 1 == iArgCount)
      {
      sprintf(szBuffer , "Missing Flash end Address.\n");
      return 1;
      }
   else if( 0 == iArgCount)
      {
      sprintf(szBuffer , "Missing both Flash start and  end Addresses.\n");
      return 1;
      }
   else if(2 < iArgCount)
      {
      sprintf(szBuffer , "Number of arguments expected are 2.But number of arguments passed are %d .\n" , iArgCount);
      return 1;
      }
   else
      {
#ifdef FLASH_SUPPORT
      ulGlobalFlashStartAddress = ulFlashStartAddr;
      ulGlobalFlashEndAddress = ulFlashEndAddr;
#else
      NOREF(ulFlashEndAddr);
      NOREF(ulFlashStartAddr);
#endif
      bIsMicroChip = CheckDeviceName("Microchip PIC32");
      sprintf(szBuffer , "OK.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      }
   return 1;
}
/****************************************************************************
     Function: SetupMicrochipFlashAddress
     Engineer: Suresh P.C
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Erases microchip flash chip
Date           Initials    Description
07-Mar-2011    SPC         initial
*****************************************************************************/
static int EraseMicrochipFlash( char *pcInput , char *pcOutput)
{
   char szBuffer[_MAX_PATH]={0};
   NOREF(pcInput);

   if(LOW_EraseChipMicrochipFlash()!=GDBSERV_NO_ERROR)
	  sprintf(szBuffer , "Error.\n");
   else
	  sprintf(szBuffer , "OK.\n");

   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}

/****************************************************************************
     Function: SetupMicrochipFlash
     Engineer: 
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Set up commands for Microchips
Date           Initials    Description
07-MAY-2010    DK/VK       initial
*****************************************************************************/
static int SetupMicrochipFlash(char *pcInput, char *pcOutput)
{
   char szClearCodeProtection[_MAX_PATH]={0};
   char szDataKernelSize[_MAX_PATH]={0};
   int iTokenIndex=0;
   char *pszToken=0;
   char szBuffer[_MAX_PATH]={0};
   char *pcEnd = NULL;
   unsigned long ulCodeProtection;
   unsigned long ulDataKernelSize;
   unsigned long getCodeProtectionStatus;

   assert(pcInput);
   assert(pcOutput);
   pszToken=strtok(pcInput," ");
   while(pszToken != 0)
      {
      //Get OS APP context Address
      if( 0 == iTokenIndex)
         strcpy(szClearCodeProtection, pszToken);
      else if( 1 == iTokenIndex)
         strcpy(szDataKernelSize, pszToken);
      else
         {
         iTokenIndex++;
         break;
         }
      iTokenIndex++;
      pszToken = strtok( 0, " " );
      }
   if(0 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <OS Name> <context> <addr>.\n");
      goto ErrorExit;
      }
   if(1 == iTokenIndex)
      {
      sprintf(szBuffer,"Missing arguments <context> <addr>.\n");
      goto ErrorExit;
      }
   if(2 < iTokenIndex)
      {
      sprintf(szBuffer,"Invalid argument after <addr>.\n");
      goto ErrorExit;
      }
   ulDataKernelSize=strtoul(szDataKernelSize,&pcEnd,16); 
   ulCodeProtection=strtoul(szClearCodeProtection,&pcEnd,16); 

   if(LOW_SetupMicrochipFlash((int)ulCodeProtection, &getCodeProtectionStatus)!=GDBSERV_NO_ERROR)
      {
      goto ErrorExit;
      }
   if(LOW_SaveMicrochipBmxRegisters()!=GDBSERV_NO_ERROR)
      {
      goto ErrorExit;
      }
   if(LOW_PartitionRamForMicrochipFlashSetup(ulDataKernelSize)!=GDBSERV_NO_ERROR)
      {
      goto ErrorExit;
      }
   else
      {
      if(1==getCodeProtectionStatus)
         {
         sprintf(szBuffer , "OK.1.\n");
         }
      else
         {
         sprintf(szBuffer , "OK.0.\n");
         }
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 1;
      }

   ErrorExit:
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}


/****************************************************************************
     Function: SetupAdvanceBreakPoint
     Engineer: Dileep Kumar Kantamneni
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Used to set advanced breakpoints.
Date           Initials    Description
31-MAY-2010    DK          initial
*****************************************************************************/
static int SetupAdvanceBreakPoint(char *pcInput, char *pcOutput)
{
   unsigned long ulBpType = 0x0;
   unsigned long ulStartAddr = 0x0;
   unsigned long ulAddrMask = 0x0;
   unsigned long ulData = 0x0;
   unsigned long ulDataMask = 0x0;
   int ibASIDEnable = 0;
   unsigned long ulASIDValue = 0x0;
   unsigned long ulTansType = 0x0;
   char *pszToken = 0;
   char szBuffer[_MAX_PATH] = {0};
   char *pcEnd = NULL;
   int iTokenCount = 0;
   int iError = 0;
   
   assert(pcInput);
   assert(pcOutput);

   pszToken = strtok(pcInput , " ");
   while(pszToken != NULL)
      {
      if(0 == iTokenCount)//Get the break point type
         {
         ulBpType = strtoul(pszToken , &pcEnd , 16);
         }
      else if(1 == iTokenCount)//Get break point start address
         {
         ulStartAddr = strtoul(pszToken , &pcEnd , 16);
         }
      else if(2 == iTokenCount)//Get Address Mask
         {
         ulAddrMask = strtoul(pszToken , &pcEnd , 16);
         }
      else if(3 == iTokenCount)//Get Data value
         {
         ulData = strtoul(pszToken , &pcEnd , 16);
         }
      else if(4 == iTokenCount)//Get Data Mask Value
         {
         ulDataMask = strtoul(pszToken , &pcEnd , 16);
         }
      else if(5 == iTokenCount)
         {
         ibASIDEnable = (int)strtoul(pszToken , &pcEnd , 16);
         }
      else if(6 == iTokenCount)
         {
         ulASIDValue = strtoul(pszToken , &pcEnd , 16);
         }
      else if(7 == iTokenCount)
         {
         ulTansType = strtoul(pszToken , &pcEnd , 16);
         }
      iTokenCount++ ;
      pszToken = strtok(NULL , " ");
      }
   
   if(0 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <Break point type> <start address> <Address mask> <data> <data mask> <ASID Enable> <ASID Value> <Trasaction Type>.\n");
      goto ErrorExit;
      }
   else if(1 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <start address> <Address mask> <data> <data mask> <data> <data mask> <ASID Enable> <ASID Value> <Trasaction Type>.\n");
      goto ErrorExit;
      }
   else if(2 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <Address mask> <data> <data mask> <data> <data mask> <ASID Enable> <ASID Value> <Trasaction Type>.\n");
      goto ErrorExit;
      }
   else if(3 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <data> <data mask> <ASID Enable> <ASID Value> <Tansaction Type>.\n");
      goto ErrorExit;
      }
   else if(4 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <data mask> <ASID Enable> <ASID Value> <Tansaction Type>.\n");
      goto ErrorExit;
      }
   else if(5 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <ASID Enable> <ASID Value> <Tansaction Type>.\n");
      goto ErrorExit;
      }
   else if(6 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <ASID Value> <Tansaction Type>.\n");
      goto ErrorExit;
      }
   else if(6 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <ASID Value> <Tansaction Type>.\n");
      goto ErrorExit;
      }
   else if(7 == iTokenCount)
      {
      sprintf(szBuffer , "Missing arguments <Tansaction Type>.\n");
      goto ErrorExit;
      }
   else if(8 < iTokenCount)
      {
      sprintf(szBuffer , "Too Many arguments.\n");
      goto ErrorExit;
      }
   else
      {
      iError = LOW_SetAdvanceBreakpoint(ulBpType , ulStartAddr , ulAddrMask ,
                                        ulData , ulDataMask , ibASIDEnable ,
                                        ulASIDValue , ulTansType
                                        );
      if(iError == GDBSERV_NO_ERROR)
         {
         sprintf(szBuffer , "OK\n");
         }
      else
         {
         sprintf(szBuffer , "%s" , tySI.szLastError);
         }
      }
 
   ErrorExit:
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return 1;
}

/****************************************************************************
     Function: DeleteAdvanceBreakPoint
     Engineer: Rejeesh S Babu
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Delete advanced breakpoint.
Date           Initials    Description
24-Jun-2010      RSB         initial
*****************************************************************************/
static int DeleteAdvancedBreakPoint(char *pcInput, char *pcOutput)
{
	unsigned long ulStartAddr = 0x0;
	char *pszToken = 0;
	char szBuffer[_MAX_PATH] = {0};
	char *pcEnd = NULL;
	int iTokenCount = 0;
	int iError = 0;

	assert(pcInput);
	assert(pcOutput);

	pszToken = strtok(pcInput , " ");
	while (pszToken != NULL)
	{
		ulStartAddr = strtoul(pszToken , &pcEnd , 16);
		iTokenCount++;
		pszToken = strtok(NULL," ");
	}

	if (1 < iTokenCount)
	{
		sprintf(szBuffer , "Too Many arguments.\n");
		goto Exit;
	}
	else
	{
		iError=LOW_DeleteAdvancedBreakpoint(ulStartAddr);
		if (iError != GDBSERV_NO_ERROR)
		{
			sprintf(szBuffer , "Unable to find break point at 0x%08lX \n" , ulStartAddr);
		}
		else
		{
			sprintf(szBuffer , "Ok\n");
		}
		goto Exit;
	}

	Exit:
	AddStringToOutAsciiz(szBuffer, pcOutput);
	return 1;
}
/****************************************************************************
     Function: GetEndian
     Engineer: Venkitakrishnan K
        Input: char *pcInput  - not used
               char *pcOutput -  monitor output endianess 
       Output: int - 1 to send response, 0 - ignore
  Description: Get target endianes.
Date           Initials    Description
10-Jul-2010      VK         initial
*****************************************************************************/
static int GetEndian(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH] = {0};
	assert(pcInput);
	assert(pcOutput);
    if(TRUE==tySI.tyProcConfig.Processor._MIPS.bProcIsBigEndian[0]){
         sprintf(szBuffer , "Target Endianess:Big.\n");
       }
    else{
       sprintf(szBuffer , "Target Endianess:Little.\n");
       }
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return TRUE; 
}

/****************************************************************************
     Function: ReadPReg
     Engineer: Suresh P.C
        Input: char *pcInput  - 
               char *pcOutput -  monitor input command string
       Output: int - 1 to send response, 0 - ignore
  Description: To Read Peripheral registers
Date           Initials    Description
10-Jul-2011    SPC         initial
*****************************************************************************/
static int ReadPReg(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH] = {0};
	unsigned long lAddress;
	unsigned long ulRegValue = 0;
	int              ErrRet;
    char *pcEnd = NULL;

    lAddress = strtoul(pcInput , &pcEnd , 16);

	if((ErrRet = MdiReadReg(lAddress, &ulRegValue)) != NO_ERROR)
	   return ErrRet;

	sprintf(szBuffer,"%lx\n", ulRegValue);

	AddStringToOutAsciiz(szBuffer, pcOutput);
    return TRUE; 
}
/****************************************************************************
     Function: WritePReg
     Engineer: Suresh P.C
        Input: char *pcInput  - 
               char *pcOutput -  monitor input command string
       Output: int - 1 to send response, 0 - ignore
  Description: Write Peripheral registers
Date           Initials    Description
31-Dec-2011    SPC         initial
*****************************************************************************/
static int WritePReg(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH] = {0};
	unsigned long ulAddress;
	unsigned long ulRegValue = 0;
	int              ErrRet;
    char *pcEnd = NULL;
    char *res[3];
    int i = 0;

    res[i]=strtok(pcInput," ");
    i++;
    while ((res[i] = strtok(NULL," ")) == NULL )
        i++;

    if (i < 1)
        {
        //Return error
        }
    ulAddress = strtoul(res[0] , &pcEnd , 16); 
    ulRegValue = strtoul(res[1] , &pcEnd , 16); 

	if((ErrRet = MdiWriteReg(ulAddress, ulRegValue)) != NO_ERROR)
	   return ErrRet;

	sprintf(szBuffer,"OK\n");

	AddStringToOutAsciiz(szBuffer, pcOutput);
    return TRUE; 
}

/****************************************************************************
     Function: ReadCP0Reg
     Engineer: Suresh P.C
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output endianess 
       Output: int - 1 to send response, 0 - ignore
  Description: Read CP0 Register.
Date           Initials    Description
22-Aug-2011    SPC         initial
*****************************************************************************/
static int ReadCP0Reg(char *pcInput, char *pcOutput)
{
	char szBuffer[_MAX_PATH] = {0};
	unsigned long     ulRegValue;
	int              ErrRet;
	char *res[3];
	int i = 0;
	unsigned int regno, sel;

	res[i]=strtok(pcInput," ");
	i++;
	while ((res[i] = strtok(NULL," ")) == NULL )
		i++;

	if (i < 1)
		{
		//Return error
		}
	regno = ( unsigned int )atoi(res[0]);
	sel = ( unsigned int )atoi(res[1]);

	if ((ErrRet = Low_ReadCP0Reg(regno, sel, &ulRegValue)) != NO_ERROR)
		return ErrRet;

	sprintf(szBuffer,"%lx\n", ulRegValue);

	AddStringToOutAsciiz(szBuffer, pcOutput);
	return TRUE; 
}

/****************************************************************************
     Function: WriteCP0Reg
     Engineer: Suresh P.C
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output endianess 
       Output: int - 1 to send response, 0 - ignore
  Description: Write CP0 register.
Date           Initials    Description
22-Aug-2011    SPC         initial
*****************************************************************************/
static int WriteCP0Reg(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH] = {0};
    unsigned long     ulRegValue;
    int              ErrRet;
    char *res[3];
    int i = 0;
    unsigned int regno, sel;
    char *pcEnd = NULL;

    res[i]=strtok(pcInput," ");
    while (res[i] != NULL)
    {
        i++;
        res[i] = strtok(0," ");                
    }
    if (i < 2)
        {
        //Return error prperly
         
        }
    regno = (unsigned int)atoi(res[0]);
    sel = (unsigned int)atoi(res[1]);
    ulRegValue = strtoul(res[2] , &pcEnd , 16);

    if ((ErrRet = Low_WriteCP0Reg(regno, sel, ulRegValue)) != NO_ERROR)
        return ErrRet;

    sprintf(szBuffer,"OK\n");

    AddStringToOutAsciiz(szBuffer, pcOutput);
    return TRUE; 
}
/****************************************************************************
     Function: ReadEJTAGReg
     Engineer: Suresh P.C
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output command string 
       Output: int - 1 to send response, 0 - ignore
  Description: Read EJTAG Register.
Date           Initials    Description
22-Aug-2011    SPC         initial
*****************************************************************************/
static int ReadWriteEJTAGReg(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH] = {0};
    static unsigned long pulOutBuffer[SCANCHAIN_ULONG_SIZE];
    //static unsigned long pulInBuffer[SCANCHAIN_ULONG_SIZE];
    unsigned long     ulInstr;
    unsigned long     ulIRlength;
    unsigned long     ulDRlength;
    unsigned long     ulDRValue;
    unsigned int uiUlongsToShow;
    char *res[4];
    char *pcEnd = NULL;

    int i = 0;
    res[i]=strtok(pcInput," ");


    while (res[i] != NULL)
        {
        i++;
        res[i] = strtok(NULL," ");
        }

    ulInstr     =strtoul(res[0],&pcEnd ,16);
    ulIRlength  =strtoul(res[1],&pcEnd ,16);
    ulDRlength  =strtoul(res[2],&pcEnd ,16);
    if(res[3] != NULL)
        {
        ulDRValue  =strtoul(res[3],&pcEnd ,16);
        }
    else{
        ulDRValue = 0x0;
        }

    if (LOW_ScanIR(&ulInstr, pulOutBuffer, ulIRlength)!=GDBSERV_NO_ERROR) //lint !e416
        {
        sprintf(szBuffer,"%s\n", JMSG_COMMAND_SHIR_ERROR);
        AddStringToOutAsciiz(szBuffer, pcOutput);
        return TRUE;
        }

    if (LOW_ScanDR(&ulDRValue, pulOutBuffer, ulDRlength) != GDBSERV_NO_ERROR)
        {
        sprintf(szBuffer,"%s\n", JMSG_COMMAND_SHDR_ERROR);
        AddStringToOutAsciiz(szBuffer, pcOutput);
        return TRUE;
        }
    // how many ulongs to show ?
    uiUlongsToShow = ulDRlength / 32;
    if(ulDRlength % 32)
       uiUlongsToShow++;
    (void)PrintUlongArray((char *)szBuffer, 8+LONGSCANCHAIN_ULONG_SIZE*8, pulOutBuffer, uiUlongsToShow);

    sprintf(szBuffer,"%s\n",szBuffer);

    AddStringToOutAsciiz(szBuffer, pcOutput);
    return TRUE; 

}

/****************************************************************************
     Function: WriteEJTAGReg
     Engineer: Suresh P.C
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output endianess 
       Output: int - 1 to send response, 0 - ignore
  Description: Write EJTAG register.
Date           Initials    Description
22-Aug-2011    SPC         initial
*****************************************************************************/
static int WriteEJTAGReg(char *pcInput, char *pcOutput)
{
	NOREF(pcInput);
	NOREF(pcOutput);
	return TRUE; 
}
/****************************************************************************
     Function: FetchTrace
     Engineer: Rejeesh S Babu
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Fetch trace from the target and stores it in trace.tmp file.
Date           Initials    Description
30-Aug-2011    RSB         initial
*****************************************************************************/
static int FetchTrace(char *pcInput, char *pcOutput)
{
    char  szBuffer[_MAX_PATH];
    int bReturn=TRUE;
    char *res[2];
    char *pcEnd = NULL;
    unsigned long ulBegPtr;
    unsigned long ulEndPtr;

    int i = 0;
    res[i]=strtok(pcInput," ");


    while (res[i] != NULL)
        {
        i++;
        res[i] = strtok(NULL," ");
        }

    ulBegPtr     =strtoul(res[0],&pcEnd ,16);
    ulEndPtr     =strtoul(res[1],&pcEnd ,16);


    if(GDBSERV_NO_ERROR != LOW_FetchTrace(ulBegPtr,ulEndPtr))
       {
       AddStringToOutAsciiz(tySI.szLastError, pcOutput);
       return bReturn;
       }

    sprintf(szBuffer,"OK\n");
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return bReturn;
}
/****************************************************************************
     Function: InvCache
     Engineer: Rejeesh S Babu
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Invalidate cache as a whole
Date           Initials    Description
16-Sep-2011    RSB         initial
*****************************************************************************/
static int InvCache(char *pcInput, char *pcOutput)
{
    char  szBuffer[_MAX_PATH];
    int bReturn=TRUE;

    NOREF(pcInput);

    if(GDBSERV_NO_ERROR != LOW_InvCache())
       {
       AddStringToOutAsciiz(tySI.szLastError, pcOutput);
       return bReturn;
       }

    sprintf(szBuffer,"Done\n");
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return bReturn;
}
/****************************************************************************
     Function: PassElf
     Engineer: Amala Nair G S
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Passing the full path of .elf file
Date           Initials    Description
20-Oct-2011    ANGS        initial
*****************************************************************************/
static int PassElf(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH];
    int bReturn=TRUE;
    char *res[1]; 
  
    res[0]=strtok(pcInput,"\0");     
     
    if(GDBSERV_NO_ERROR !=LOW_ElfPath(res[0]))    
       {        
       AddStringToOutAsciiz(tySI.szLastError, pcOutput);
       return bReturn;
       } 

    sprintf(szBuffer,"OK\n");
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return bReturn;
}

/****************************************************************************
     Function: ConfigureTrace
     Engineer: Sreekanth.V
        Input: char *pcInput  - monitor input command string
               char *pcOutput -  monitor output command string
       Output: int - 1 to send response, 0 - ignore
  Description: Configure Zephyr Trace 
Date           Initials    Description
30-Nov-2011    SV          initial
*****************************************************************************/

static int ConfigureTrace(char *pcInput, char *pcOutput)
{
    char szBuffer[_MAX_PATH];
    int bReturn=TRUE;
    char *tcreg[3];
    unsigned long ulReg,ulReg2,ulReg3;
    int i=0;
    char key[]="0x";
    char *res=NULL;    

    assert(pcInput);
    

    tcreg[i]=strtok(pcInput," ");

   while (tcreg[i]!=NULL)
      {

      tcreg[++i]=strtok(NULL," ");

      if (tcreg[i]==NULL)
         {
         --i;
         break;
         }
      else
         {
         res = strstr(tcreg[i],key);
         if (res==NULL)
            {
            sprintf(szBuffer , "Regval should be in hex \n");
            AddStringToOutAsciiz(szBuffer, pcOutput);
            return bReturn;
            }
         }
      }   
     
   if (i<2)
      {
      sprintf(szBuffer , "Missing arguments <TcReg value> \n");
      }
    else if (i>2)
      {
      sprintf(szBuffer , "Too Many arguments.\n");
      }
    else
      {      
      ulReg = strtoul(tcreg[0],NULL,16);
      ulReg2 = strtoul(tcreg[1],NULL,16);
      ulReg3 = strtoul(tcreg[2],NULL,16);
      
      if (GDBSERV_NO_ERROR !=LOW_ConfigureTrace(ulReg,ulReg2,ulReg3))
         {
         AddStringToOutAsciiz(tySI.szLastError, pcOutput);
         return bReturn;
         }

      sprintf(szBuffer,"OK\n"); 

      }

    AddStringToOutAsciiz(szBuffer, pcOutput);
    return bReturn;
}

