/****************************************************************************
       Module: gdbmonitor.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, Dispatch monitor command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS/VK       Modified for ARM support
08-AUG-2009    DK          Modified for Linux support
23-Sep-2009    SVL         Added support for 'PFXD mode'
25-Sep-2009    SVL         Modified reply for hardware and software reset
16-Oct-2009    SVL         Support for Vector Catch
19-Oct-2009    SVL         Support for forcefully setting breakpoint types
****************************************************************************/
#ifndef GDBMONITOR_H_
#define GDBMONITOR_H_

int32_t ParseMonitorCommand(char *pcInput, char *pcOutput);
void ShowGDBMonitorCommandsHelp(void);

#define VPN2_SHIFT                        13
#define GDB_MONITOR_COMMANDS        "\n\nSupported GDB MONITOR commands:\n\n"
#define GDB_MONITOR_ENCODED_CMD     "Encoded command '%s'.\n"
/* PF_Eclipse expects OK as reply to hardware reset */
#define GDB_MONITOR_HWRESET_ON      "OK. Hardware reset asserted\n"
#define GDB_MONITOR_SWRESET_ON      "OK. Software reset asserted\n"

#if defined (ARM) || defined(RISCV)
int32_t DoSoftReset(char *pcInput, char *pcOutput);
#endif

#ifdef ARM
/* Maximum number of watch points */
#define MAX_WP_NUM 10
/* Maximum number of watch point hardware resources */
#define MAX_HW_WP_RES 2 /* TODO: take care while ARM 11, CORTEX... */
/* Maximum number of breakpoint type that can be set. This is not the limit of
the number of breakpoints that can be set. But its the number of breakpoints that
can be set before a 'continue' */
#define MAX_BP_TYPE_CNT             256
/* PF_Eclipse expects OK as reply to software reset */
#define GDB_MONITOR_SWRESET_ON      "OK. Software reset asserted\n"
#define GDB_MONITOR_HWBREAK_ON      "Hardware breakpoint option ON.\n"
#define GDB_MONITOR_HWBREAK_OFF     "Hardware breakpoint option OFF.\n"
#define GDB_MONITOR_JTFREQ_RTCK     "JTAG frequency set to RTCK.\n"
#define GDB_MONITOR_JTFREQ_SET      "JTAG frequency set to %s.\n" 
#define GDB_MONITOR_WP_ADDR_SET     "Set watchpoint for address 0x%08x.\n"
#define GDB_MONITOR_WP_DATA_SET     "Set watchpoint for data  0x%x.\n"
#define GDB_MONITOR_WP_ADDR_DATA    "Set watchpoint for address 0x%08x & data 0x%x.\n"
#define GDB_MONITOR_SEMIHOST        "Semi-hosting turned %s.\n"
#define GDB_MONITOR_WP_DELETE       "Ok. Deleted the given watchpoint.\n"
#define GDB_MONITOR_NO_WP           "No wachpoints to clear.\n"
#define GDB_MONITOR_NO_SPECIFIC_WP  "Cannot find the specified watchpoint.\n"
#define GDB_MONITOR_DELETED_ALL_WP  "Deleted all watchpoints.\n"
#define GDB_MONITOR_CP_READ_VALUE   "Coprocessor read value : 0x%08X\n"
#define GDB_MONITOR_CP_WRITE_VALUE  "Value written to CoProcessor\n"
#define GDB_MONITOR_VECTOR_CATCH_EN "Ok. Vector Catch is enabled.\n"
#define GDB_MONITOR_SET_BPTYPE      "Ok. Breakpoint type is set.\n"
#define GDB_MONITOR_BPTYPE_ALREADY_SET      "Ok. Breakpoint type is already set.\n"
#define GDB_MONITOR_UNSET_BPTYPE      "Ok. Breakpoint type is removed.\n"
#define GDB_MONITOR_GET_REGS        "Register Value:"
#define GDB_MONITOR_SET_REGS        "Ok.Regiser value set successfully.\n"
/* the type of breakpoint to be set */
typedef enum _tyBpType
{
    /* AUTO mode: Symbol information will determine the breakpoint mode */
    BPTYPE_AUTO = 0,
    /* ARM mode: User will force the type of breakpoint to be set as ARM */
    BPTYPE_ARM,
    /* THUMB mode: User will force the type of breakpoint to be set as THUMB */
    BPTYPE_THUMB
}TyBpType;
/* the break point type information */
typedef struct _tyBpTypeInfo
{
    /* The address where breakpoint is to be set */
    uint32_t ulBpAddress;
   /* The type of breakpoint */
    TyBpType tyBpType;   
    /* Self referencing pointer */
    struct _tyBpTypeInfo *next;
}TyBpTypeInfo;

/* For connecting to the debug target using 'monitor connect', when started in 
PFXD mode */
int32_t Connect  (char *pcInput, char *pcOutput);
int32_t Hwbreak    (char *pcInput, char *pcOutput);
int32_t ProcessCP15Reg  (char *pcInput, char *pcOutput);
int32_t ReadCPReg  (char *pcInput, char *pcOutput);
int32_t WriteCPReg  (char *pcInput, char *pcOutput);
int32_t JTFreq     (char *pcInput, char *pcOutput);
int32_t WatchpointAddress  (char *pcInput, char *pcOutput);
int32_t WatchpointData  (char *pcInput, char *pcOutput);
int32_t WatchpointAddressData  (char *pcInput, char *pcOutput);
int32_t SetWatchpointModeAndSize( char *pszmode, char *pszsize);
int32_t SetWatchpoint(uint32_t uladdress,uint32_t uldata,uint32_t uiBPType);
int32_t DeleteWatchpointAddress(char* pcInput, char *pcOutput);
int32_t DeleteWatchpointData(char* pcInput, char *pcOutput);
int32_t DeleteWatchpointAddressData(char* pcInput, char *pcOutput);
int32_t DeleteWatchpointAddressData(char* pcInput, char *pcOutput);
int32_t DeleteWatchpoint(char *pcInput, char *pcOutput);
int32_t Semihost(char *pcInput, char *pcOutput);//GDB monitor command errors
/* Get the breakpoint type in a given address from the array */
int32_t GetBpType(uint32_t ulAddress, TyBpTypeInfo **pBpTypeNode);
/* On closing the connection, Remove the entire breakpoint type list*/
int32_t RemoveBpTypeList(void);

typedef enum
{
   eWPRead = 0,
   eWPWrite,
   eWPAnyAccessType
} TyWatchpointAccessType;


typedef enum
{
   eWPByte = 0,
   eWPHalfWord,
   eWPWord,
   eWPAnySizeType
} TyWatchpointSizeType;

typedef enum
{
   eWPByteRead = 0x1,
   eWPHalfWordRead = 0x2,
   eWPWordRead = 0x4,
   eWPByteWrite = 0x8,
   eWPHalfWordWrite = 0x10,
   eWPWordWrite = 0x20,
   eWPByteReadWrite = eWPByteRead|eWPByteWrite,
   eWPHalfWordReadWrite = eWPHalfWordRead|eWPHalfWordWrite,
   eWPWordReadWrite = eWPWordRead|eWPWordWrite,
   eWPAnySizeTypeRead = eWPByteRead|eWPHalfWordRead|eWPWordRead,
   eWPAnySizeTypeWrite = eWPByteWrite|eWPHalfWordWrite|eWPWordWrite,
   eWPAnySizeTypeReadWrite = eWPByteReadWrite|eWPHalfWordReadWrite|eWPWordReadWrite
} TyWatchpointDataType;

#define WP_ADDR_TYPE          11
#define WP_DATA_TYPE          19
#define WP_ADDR_DATA_TYPE     18

#endif


#endif // GDBMONITOR_H_

