/****************************************************************************
       Module: gdbmonitor.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, GDB Monitor Command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS/VK       Modified for ARM support
23-Sep-2009    SVL         Added support for 'PFXD mode'
15-Oct-2009    SVL         Support for clearing individual watchpoints
16-Oct-2009    SVL         Support for vector catch
19-Oct-2009    SVL         Support for forcefully setting breakpoint types
21-Oct-2009    SVL         Support for Device register file for ARM   
31-Dec-2009    SVL         Modified 'GetPeripheralRegisters' for 
                           PathFinder-XD's Peripheral Register View. 
01-Jan-2009    SRK         Updated for flash support
12-Apr-2010    SVL         Added target reset modes for ARM in HardReset
24-May-2010    SJ          Added separate monitor commands for clearing
                           address WP's, data WP's and address/data WP's
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbopt.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdberr.h"
#ifdef ARM
#include "gdbserver/arm/gdbarm.h"
#endif
#include "gdbserver/moncommand/gdbmonitor.h"
#include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
#include "gdbserver/moncommand/ashload/gdbashload.h"
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
#include "gdbserver/moncommand/flashcommands/flashsec/gdbflash.h"

// external variables
extern TyServerInfo tySI;
#ifdef ARM
#ifdef __LINUX
#else
#define strcasecmp _stricmp
#endif

#define MIN_JTAGFreq                1
#define MAX_JTAGFreq                100000
#define MAX_ULONG_STRING_SIZE       0x20

TyWatchpointAccessType tyWatchpointAccessType;
TyWatchpointSizeType tyWatchpointSizeType;
TyWatchpointDataType tyWatchpointDataType;
/* Pointer to the list of breakpoint type list */
TyBpTypeInfo *ptyBpTypeList = NULL;
uint32_t          uiBPType                =0;
int32_t iBpTypeCnt = 0;
unsigned short   usMonHwbpOption;

uint32_t ulGSemiState=0x0;
extern void JC_SetJtagFrequency(void);
#endif


// local functions
static int32_t GetMonCommand(char* pcInput, char *pcOutput);
static int32_t DoHWReset(char *pcInput, char *pcOutput);

#ifdef ARM
int32_t RemoveBpTypeNode(uint32_t ulBpAddress);
static int32_t GetRegNumber(char* szRegName, uint32_t* pulRegNum);
static char szArmV7mRegs[][10] = {
    "r0","r1","r2","r3","r4","r5","r6","r7","r8","r9","r10","r11","r12",
    "sp","lr","pc","xpsr","apsr","ipsr","epsr","primask","faultmask","basepri","control","msp","psp"};

static void HelpJtagFrequency(void);
static void HelpWPAddr(void);
static void HelpWPData(void);
static void HelpWPAD(void);
static void HelpSemiHost(void);
static void HelpHwReset(void);
int32_t ConfigVectorCatch(char* pcInput, char *pcOutput);
int32_t SetBpType(char* pcInput, char *pcOutput);
int32_t UnSetBpType(char* pcInput, char *pcOutput);
int32_t GetReg(char* pcInput, char *pcOutput);
int32_t SetReg(char* pcInput, char *pcOutput);
static int32_t GetPeripheralRegGroups (char *pcInput, char *pcOutput);
static int32_t GetPeripheralRegisters (char *pcInput, char *pcOutput);
static int32_t SetPeripheralRegister (char *pcInput, char *pcOutput);
static int32_t UpdateTargetResetMode(int32_t iArgCnt, char szArgList[][_MAX_PATH]);
#endif
//typedef void (TyHelpFunc)(void);
typedef int32_t (*TyMonCommandFunction)(char *pcInput, char *pcOutput);
typedef struct _TyMonCommandEntry 
{
   const char *monCommand;
   const char *pszHelpArg;
   const char *pszHelpLine1;
   const char *pszHelpLine2;
   const char *pszHelpLine3;
   const char *pszHelpLine4;
   TyMonCommandFunction tyMonCommandFunction;
   TyHelpFunc *pfHelpFunc;
} TyMonCommandEntry;

// local variables
static TyMonCommandEntry monitorCommandTable[]  =
{
   {
      "ashload",
      " <file> <--verify>",
      "Load ELF format <file>.",
      "<file> should be given an absolute path.",
      "--verify will readback/verify data",
      "downloaded (default:off).",
      Ashload,
      NULL
   },

#if defined(ARC)
   {
      "hwreset",
      "",
      "Reset target (hardware reset via RST* pin).",
      "",
      "",
      "",
      DoHWReset,
      NULL
    },
#endif
#if defined(ARM) || defined(RISCV)
	 {
	  "swreset",
	  "",
	  "Issue a software reset.",
	  "",
	  "",
	  "",
	  DoSoftReset,
	  NULL
	},
#endif

#ifdef ARM   
    {
      "connect",
       "",
       "Connects to the target device",
       "Use only when GDB Server is started in PFXD",
       "mode (using --PFXD-mode option).",
       "",
       Connect,
       NULL
    },   
    {
       "hwbreak",
       " <on|off>",
       "Force use of hardware breakpoints.",
       "Default is off.",
       "",
       "",
       Hwbreak,
       NULL
    },
    {
      "hwreset",
      " <Reset-Mode> <Delay in Seconds>",
      "Reset target (hardware reset via nSRST pin).",
      "",
      "",
      "",
      DoHWReset,
      HelpHwReset
    }
    {
      "readcpreg",
      "",
      "Read CP register.",
      "",
      "",
      "",
      ReadCPReg,
      NULL
    },
    {
      "writecpreg",
      "",
      "Write CP register.",
      "",
      "",
      "",
      WriteCPReg,
      NULL
    },
    {
      "cp15",
      "",
      "Process CP15 register.",
      "",
      "",
      "",
      ProcessCP15Reg,
      NULL
    },
    {
       "jtfreq",
       " <freq>",
       "Specifies JTAG frequency.",
       "",
       "",
       "",
       JTFreq,
       HelpJtagFrequency
    },

    {
       "wpaddr",
       " [r|w|d] [b|h|w|d] <addr> ",
       "",
       "",
       "",
       "",
       WatchpointAddress,
       HelpWPAddr
    },
    {
       "wpdata",
       " [r|w|d] [b|h|w] <data> ",
       "",
       "",
       "",
       "",
       WatchpointData,
       HelpWPData
    },
    {
       "wpad",
       " [r|w|d] [b|h|w|d] <addr> <data> ",
       "",
       "",
       "",
       "",
       WatchpointAddressData,
       HelpWPAD
    },
    {
       "wpclr",
       "",
       "Clear all watchpoints.",
       "",
       "",
       "",
       DeleteWatchpoint,
       NULL
       
    },
    {
       "wpclraddr",
       " <addr> ",
       "Clear the specified address watchpoint.",
       "",
       "",
       "",
       DeleteWatchpointAddress,
       NULL
       
    },
    {
       "wpclrdata",
       " <data> ",
       "Clear the specified data watchpoint.",
       "",
       "",
       "",
       DeleteWatchpointData,
       NULL
       
    },
    {
       "wpclraddrdata",
       " <addr> <data> ",
       "Clear the specified address & data watchpoint.",
       "",
       "",
       "",
       DeleteWatchpointAddressData,
       NULL
       
    },
    {
       "semi-hosting",
       " <on|off> <addr1> <addr2> ",
       "",
       "",
       "",
       "",
       Semihost,
       HelpSemiHost
    },
    {
       "vectorcatch",
       " <vector mask>",
       "Sets Vector catch.",
       "",
       "",
       "",
       ConfigVectorCatch,
       NULL       
    },
    {
       "setbptype",
       " <arm|thumb|auto> <addr>",
       "Set breakpoint type.",
       "arm - forcefully sets arm-mode breakpoint at <addr>.",
       "thumb - forcefully sets thumb-mode breakpoint at <addr>.",
       "auto - sets breakpoint according to symbol information.",
       SetBpType,
       NULL       
    },
    {
       "unsetbptype",
       " <addr>",
       "UnSet breakpoint type at <addr>.",
       "",
       "",
       "",
       UnSetBpType,
       NULL       
    },
    {
       "getreg",
       "<regnum>",
       "Get selected register value",
       "",
       "",
       "",
       GetReg,
       NULL       
    },
    {
       "setreg",
       "<regnum><value>",
       "Set armv7 advanced registers",
       "",
       "",
       "",
       SetReg,
       NULL       
    },
    {
       "getpreggrps",
       " ",
       "Gets the list of peripheral register groups",
       "configured for the target",
       "",
       "",
       GetPeripheralRegGroups,
       NULL       
    },
    {
       "getpregs",
       " <group index>",
       "Gets the list of peripheral registers in the",
       "given group and value of each. <group index>",
       "should be a valid peripheral register groups",
       "index returned by getpreggrps command",
       GetPeripheralRegisters,
       NULL       
    },
    {
       "setpreg",
       " <register index> <value> ",
       "Sets the value to the given ",
       "peripheral register. <register index> should",
       "be an index returned by getpregs command",
       "<value> should be in hexa-decimal format",
       SetPeripheralRegister,
       NULL       
    },
    {
      "flashPrepareChunk",
      "<section num> <utility buff address> <utility buff size>",
      "Transfer chunk of data to utility buffer",
      "",
      "",
      "",
      FlashPrepareChunk,
      NULL
    },
    {
      "flashProcInFile",
      "<ELF file name>",
      "Converts the ELF file to sections in memory",
      "",
      "",
      "",
      FlashProcInFile,
      NULL
    },
    {
      "flashGetSizeOfSection",
      "<section num>",
      "Gets the size of section",
      "",
      "",
      "",
      FlashGetSizeOfSection,
      NULL
    },
    {
      "flashGetOffsetOfSection",
      "<section num>",
      "Gets the offset of section",
      "",
      "",
      "",
      FlashGetOffsetOfSection,
      NULL
    },
    {
      "flashVerify",
      "",
      "Verifies the Flash Program",
      "",
      "",
      "",
      FlashVerify,
      NULL
    }
#endif
};
#define MAX_MON_COMMAND_ENTRIES ((int32_t)(sizeof(monitorCommandTable) / sizeof(TyMonCommandEntry)))

/****************************************************************************
     Function: ShowGDBMonitorCommandsHelp
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: Show GDB monitor command help       
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void ShowGDBMonitorCommandsHelp(void)
{
   int32_t iPadding, iStrLen, iIndex;
   char szBuffer[_MAX_PATH*2];

   PrintMessage(HELP_MESSAGE,GDB_MONITOR_COMMANDS );
   //PrintMessage(HELP_MESSAGE, "Commands to be used from GDB, after connecting to Ashling GDB ");
   //PrintMessage(HELP_MESSAGE, "Server.\n\n");

   for (iIndex=0; iIndex<MAX_MON_COMMAND_ENTRIES; iIndex++)
      {
      sprintf(szBuffer, "%s%s%s","monitor ",monitorCommandTable[iIndex].monCommand, monitorCommandTable[iIndex].pszHelpArg);
      PrintMessage(HELP_MESSAGE, "%s", szBuffer);
      if ((iStrLen = (int32_t)strlen(szBuffer)) < GDBSERV_MON_HELP_COL_WIDTH-1) 
         iPadding = GDBSERV_MON_HELP_COL_WIDTH - iStrLen;
      else
         iPadding = 1;
      if (monitorCommandTable[iIndex].pszHelpLine1[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", iPadding, " ", monitorCommandTable[iIndex].pszHelpLine1);
      if (monitorCommandTable[iIndex].pszHelpLine2[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine2);
      if (monitorCommandTable[iIndex].pszHelpLine3[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine3);
      if (monitorCommandTable[iIndex].pszHelpLine4[0] != '\0')
         PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_MON_HELP_COL_WIDTH, " ", monitorCommandTable[iIndex].pszHelpLine4);
      if (monitorCommandTable[iIndex].pfHelpFunc)
         monitorCommandTable[iIndex].pfHelpFunc();
      }  
}

/****************************************************************************
     Function: ParseMonitorCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput -  monitor input command string
               char *pcOutput -  monitor output command string
       Output: int32_t - 1 to send response, 0 - ignore
  Description: select particular monitor command handler and invoke it
Date           Initials    Description
08-Feb-2008    VH          Initial
21-May-2010    SJ          Modified for comparing commands given from command 
                           line rather than comparing that from Table. Made 
                           comparison case insensitive.
14-Nov-2012    SV          Modified to handle errors properly
*****************************************************************************/
int32_t ParseMonitorCommand(char *pcInput, char *pcOutput)
{
    uint32_t iIndex;
    char szCommand[_MAX_PATH];
    char szBuffer[_MAX_PATH] = {'\0'};

    szCommand[0] = '\0';
    pcOutput[0] = '\0';

    // get monitor command
    if(!GetMonCommand(pcInput, pcOutput))
        return 0;

    for(iIndex = 0; iIndex < strlen(tySI.pszMonitorCommand); iIndex++)
        {
        if(tySI.pszMonitorCommand[iIndex] == ' ')
            {            
            break;
            }
        szCommand[iIndex] = tySI.pszMonitorCommand[iIndex];
        }

    szCommand[iIndex] = '\0';
    // parse command
    for(iIndex = 0; iIndex < MAX_MON_COMMAND_ENTRIES; iIndex++)
        {
        if(!strcasecmp(monitorCommandTable[iIndex].monCommand, szCommand))
            //if (!strncmp(monitorCommandTable[iIndex].monCommand, tySI.pszMonitorCommand, strlen(monitorCommandTable[iIndex].monCommand)))
            return monitorCommandTable[iIndex].tyMonCommandFunction(tySI.pszMonitorCommand + strlen(monitorCommandTable[iIndex].monCommand), pcOutput);
        }
    sprintf(szBuffer,"Command not supported.\n");
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return 1;//0;
}

/****************************************************************************
     Function: GetMonCommand
     Engineer: Vitezslav Hola
        Input: char *pcInput  : hexadecimal string representing monitor command
               char *pcOutput : hexadecimal string to be returned to the GDB
       Output: int32_t - 1 if command is ok, 0 otherwise
  Description: Convert monitor command from hexadecimal to ascii string.
               String will be stored into tySI.pszMonitorCommand
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use 'util' function to prepare the outstring
*****************************************************************************/
static int32_t GetMonCommand(char* pcInput, char *pcOutput)
{
   uint32_t uiStrLen = (uint32_t)strlen(pcInput);
   char szBuffer[_MAX_PATH*2];

   // check if string is really hex
   if (uiStrLen % 2)
      {  // not correct string, print message to user
      sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
      PrintMessage(ERROR_MESSAGE, szBuffer);
      strcat(szBuffer, "\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   // check if string is not too int32_t
   if (uiStrLen >= (2 * _MAX_PATH))
      {
      sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
      PrintMessage(ERROR_MESSAGE, szBuffer);
      strcat(szBuffer, "\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   // process command
   for (uiStrLen = 0; uiStrLen < strlen(pcInput) ; uiStrLen+=2)
      {
      int32_t iTemp = FromHex(pcInput[uiStrLen]);
      if (iTemp == -1)
         {
         sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
         PrintMessage(ERROR_MESSAGE, szBuffer);
         strcat(szBuffer, "\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return 0;
         }
      tySI.pszMonitorCommand[uiStrLen / 2] = (char)(iTemp << 4);  //lint !e701
      iTemp = FromHex(pcInput[uiStrLen + 1]);
      if (iTemp == -1)
         {
         sprintf(szBuffer, MESSAGE_ERR_MONCMD_RECOGNITION_FAILED);
         PrintMessage(ERROR_MESSAGE, szBuffer);
         strcat(szBuffer, "\n");
         AddStringToOutAsciiz(szBuffer, pcOutput);
         return 0;
         }
      tySI.pszMonitorCommand[uiStrLen/2] |= (char)iTemp;
      }
   if ((uiStrLen / 2) < _MAX_PATH)
      tySI.pszMonitorCommand[uiStrLen / 2] = '\0'; // terminate command string
   //sprintf(AshloadProcData.szErrorBuffer,);
   PrintMessage(LOG_MESSAGE, GDB_MONITOR_ENCODED_CMD, tySI.pszMonitorCommand);
   return 1;
}

/****************************************************************************
     Function: DoHWReset
     Engineer: Vitezslav Hola
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Do target hardware reset.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Added response message
31-Mar-2008    RS          Modified for ARM support
12-Apr-2010    SVL         Added target reset modes for ARM
*****************************************************************************/
static int32_t DoHWReset(char *pcInput, char *pcOutput)
{
   char  szBuffer[_MAX_PATH];
   #ifdef ARM
   int32_t iArgCnt = 0, iRetVal = 0;
   char szArgList[_MAX_PATH][_MAX_PATH];
   #elif RISCV
   uint32_t ulThreadId = 0;
   #endif

   NOREF(pcInput);
   
   /* In the case of ARM, Hardware reset has 3 modes - 
   HARD_RESET_AND_HALT_AT_RESET_VECTOR, HARD_RESET_AND_ENTER_DBG_MODE and 
   HARD_RESET_AND_DELAY. Mode will be provided as argument to the 
   'monitor hwreset' command */

   #ifdef ARM
   /* Parse the command line arguments */
   iArgCnt = GetArgs(pcInput, szArgList);
   if (iArgCnt > 2)
   {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }   

   /* Update target reset mode */
   iRetVal = UpdateTargetResetMode(iArgCnt, szArgList);
   if(iRetVal < 0)
   {
       if (iRetVal == -1)
       {
           sprintf(tySI.szError, "%s", GDB_EMSG_TARGET_RST_TYPE_INVALID);
       }
       else if (iRetVal == -2)
       {
           sprintf(tySI.szError, GDB_EMSG_RST_OPTN_INVALID, szArgList[1]);
       }       
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   #endif

   // do hardware reset
   #if defined (ARC) || defined (ARM)   
   if(GDBSERV_NO_ERROR != LOW_ResetTarget())
   {
      AddStringToOutAsciiz(tySI.szLastError, pcOutput);
      return 1;
   }
 #elif MIPS
   #error Not implemented yet.
   #endif
   sprintf(szBuffer,GDB_MONITOR_HWRESET_ON);
   if (tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "%s", szBuffer);
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}

/****************************************************************************
	 Function: DoSWReset
	 Engineer: Rejeesh S Babu
		Input: char *pcInput - command param string with white char at the beggining
			   char *pcOutput - command param string with output
	   Output: int32_t - 1 to send response, 0 to ignore
  Description: Issue Soft Reset.
Date           Initials    Description
28-May-2008       RS      Initials
27-June-2019      HS      Added reset feature for RISCV 
*****************************************************************************/
int32_t DoSoftReset(char *pcInput, char *pcOutput)
{
	NOREF(pcInput);
#ifdef ARM 
	char  szBuffer[_MAX_PATH];
	LOW_SoftReset();
	sprintf(szBuffer, GDB_MONITOR_SWRESET_ON);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "%s", szBuffer);
	AddStringToOutAsciiz(szBuffer, pcOutput);
	return 1;
#elif RISCV
	char  szBuffer[_MAX_PATH];
	uint32_t ulThreadId = 0;
	pcInput++;
	if (strcmp(pcInput, "1") == 0)
	{
		//read current hartid.
		ulThreadId = LOW_CurrentHart();
		//reset the target.
		if (ulThreadId == -1)
		{
			sprintf(szBuffer, "Invalid hart id for reset operation");
		}
		else if (!LOW_TargetReset(ulThreadId))
		{
			sprintf(szBuffer, GDB_MONITOR_SWRESET_ON);
		}
		else
		{
			sprintf(szBuffer, "target reset failed");
		}
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE, "%s", szBuffer);
		AddStringToOutAsciiz(szBuffer, pcOutput);
	}
	else
	{
		sprintf(szBuffer, "Invalid reset option");
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE, "%s", szBuffer);
		AddStringToOutAsciiz(szBuffer, pcOutput);
	}
	return 1;
#else
	return 0;
#endif
}
#ifdef ARM
/****************************************************************************
     Function: Connect
     Engineer: Sandeep V L
        Input: None
       Output: int32_t : FALSE if error
                     proper return value, on success
  Description: Command handler for 'monitor connect'
Date           Initials    Description
23-Sep-2009    SVL         Initial
****************************************************************************/
int32_t Connect(char *pcInput, char *pcOutput)
{
   int32_t  bReturn = 1;
   char szDiskWare1[_MAX_PATH];
   char szFirmWare [_MAX_PATH];
   char szFpgaWare1[_MAX_PATH];
   char szFpgaWare2[_MAX_PATH];
   char szFpgaWare3[_MAX_PATH];
   char szFpgaWare4[_MAX_PATH];
   char szBuffer[_MAX_PATH];

   NOREF(pcInput);
   
   // Check whether already connected
   if (tySI.bIsConnected) 
      {
      sprintf(szBuffer,"OK\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return bReturn;
      }

   // initialise communication with target
   if (GDBSERV_NO_ERROR != LOW_PFXDConnect()) 
      {
      AddStringToOutAsciiz(tySI.szLastError, pcOutput);
      return FALSE;
      }
   
   tySI.bIsConnected = TRUE;
   sprintf(szBuffer,"OK\n");
   AddStringToOutAsciiz(szBuffer, pcOutput);

   PrintMessage(INFO_MESSAGE, "Connected to target device configured as: %s\n(currently in %s Endian mode).",
               tySI.tyProcConfig.szProcName,
               LOW_IsBigEndian(0)?"Big":"Little");

   (void)LOW_GetDWFWVersions(szDiskWare1,
                             szFirmWare,
                             szFpgaWare1,
                             szFpgaWare2,
                             szFpgaWare3,
                             szFpgaWare4);

   PrintMessage(INFO_MESSAGE, "Connected to target via %s (software: %s) at %s.",   
                  GetEmulatorName(),
                  szDiskWare1,
                  tySI.szJtagFreq);

   return bReturn;
}

/****************************************************************************
     Function: UpdateTargetResetMode
     Engineer: Sandeep V L
        Input: int32_t iArgCnt - Number of arguments
               char szArgList - List of arguments
       Output: 0, On Success. -1, On Failure.
  Description: Update the target reset type with HARD_RESET_AND_HALT_AT_RESET_VECTOR, 
               HARD_RESET_AND_ENTER_DBG_MODE and HARD_RESET_AND_DELAY
Date           Initials    Description
12-Apr-2010     SVL        Initial
*****************************************************************************/
static int32_t UpdateTargetResetMode(int32_t iArgCnt, char szArgList[][_MAX_PATH])
{
    int32_t iRstType = 0, iRstDelay = 5, iValid = 0;
       
    /* NOTE: If 'monitor hwreset' command is issued with no arguments, 
    Hard reset will be issued with already existing TARGET_RESET_MODE 
    in OpellaXD.cnf */

    if (iArgCnt > 0)
    {
        /* Target reset mode */
        iRstType = atoi(szArgList[0]);
        if ((iRstType <= 0) || (iRstType > 3))
        {
            /* Unexpected reset mode */
            return -1;
        }
        if (iRstType == 1)
        {
            /* only Reset type is expected in the argument list */
            if (iArgCnt != 1)
            {
                return -1;
            }
            /* HARD_RESET_AND_HALT_AT_RESET_VECTOR */
            tySI.tyProcConfig.Processor._ARM.tyTargetReset = HardResetAnd_HaltAtRV;
        }
        else if (iRstType == 2)
        {
            /* only Reset type is expected in the argument list */
            if (iArgCnt != 1)
            {
                return -1;
            }
            /* HARD_RESET_AND_ENTER_DBG_MODE */
            /* Set the target reset type to HardResetAnd_Delay */
            tySI.tyProcConfig.Processor._ARM.tyTargetReset = HardResetAnd_Delay;
            /* Set the delay to 0 seconds */
            tySI.bTargetResetDelay = 0;
        }
        else
        {
            /* HARD_RESET_AND_DELAY */
            /* Set the target reset type to HardResetAnd_Delay */
            tySI.tyProcConfig.Processor._ARM.tyTargetReset = HardResetAnd_Delay;
            /* If the delay count is provided as argument, set it. Else set delay 
            to default value of '5 Seconds' */
            if(iArgCnt == 2)
            {
                iRstDelay =(int32_t)StrToUlong(szArgList[1], &iValid);
                if (!iValid || (iRstDelay < 0) || (iRstDelay > 0x10))
                {                    
                    return -2;
                }
            }            
            tySI.bTargetResetDelay = iRstDelay;
        }
    }
   
    return 0;
}
/****************************************************************************
     Function: Hwbreak
     Engineer: Rejeesh S Babu
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Target hwbp enable(on/off). 
Date           Initials    Description
31-Mar-2008       RS      Initials
*****************************************************************************/
int32_t Hwbreak    (char *pcInput, char *pcOutput)
{
   char szHwbreak[_MAX_PATH*2];
   char szBuffer[_MAX_PATH*2];
   uint32_t ulIndex;
   uint32_t ulNoOptn=0;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;
   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }
   
   strncpy(szHwbreak, pcInput , sizeof(szHwbreak)-1);
   szHwbreak[sizeof(szHwbreak)-1] = '\0';
    
   if((strcasecmp(szHwbreak,"ON")==0)||
           (strcasecmp(szHwbreak,"on")==0))
   {
                usMonHwbpOption  =TRUE;
                sprintf(szBuffer, GDB_MONITOR_HWBREAK_ON);
                if (tySI.bDebugOutput)
                    PrintMessage(INFO_MESSAGE, "%s", szBuffer);
                AddStringToOutAsciiz(szBuffer, pcOutput);
                return 1;

   }
   else
   if((strcasecmp(szHwbreak,"OFF")==0)||
          (strcasecmp(szHwbreak,"off")==0))
      {
   
            usMonHwbpOption  =FALSE;
            sprintf(tySI.szError,GDB_MONITOR_HWBREAK_OFF);
            if (tySI.bDebugOutput)
               PrintMessage(LOG_MESSAGE, "%s", szBuffer);
            AddStringToOutAsciiz(tySI.szError, pcOutput);
            return 1;
      }
   else
       {
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return 1;
       }
}

/****************************************************************************
     Function: ProcessCP15Reg
     Engineer: Roshan T R
        Input: char *pcInput - command param string with white char at the begining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Process(Read/ Write) a Coprocessor register.  
Date           Initials    Description
03-Dec-2009    RTR         Initial
*****************************************************************************/
int32_t ProcessCP15Reg  (char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH*2];
   uint32_t uiCRn;
   uint32_t uiCRm;
   uint32_t uiOp1;
   uint32_t uiOp2;
   uint32_t uiOperation = 1;
   uint32_t ulWpCmnd;
   uint32_t ulNoOptn=0;
   uint32_t ulData = 0;
   int32_t iErrRet;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;   

   if(*pcInput == '\0')
   {
      sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return 1;
   }

   for (ulWpCmnd=0; ulWpCmnd < MAX_ULONG_STRING_SIZE; ulWpCmnd++)
   {
      if(*pcInput == ' ')
      {
         pcInput++;
         ulNoOptn = 1;
      }
      else
      {
         ulNoOptn = 0;
         break;
      }
   }
   if(ulNoOptn == 1)
   {
      sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return 1;
   }

   if( strlen(pcInput) > _MAX_PATH * 2)
   {
      sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return 1;
   }

   iErrRet = StringsToProcessCP15Params (pcInput, &uiCRn , &uiCRm,
                                         &uiOp1, &uiOp2, &ulData);

   if(iErrRet)
   {
       switch(iErrRet)
       {
           case ERR_INVALID_CRN:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_CRN);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_INVALID_CRM:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_CRM);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_INVALID_OP1:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_OP1);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_INVALID_OP2:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_OP2);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_INVALID_ARG:
           default:
               sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
       }
       return 1;
   }
   if(ulData)
      uiOperation = 0;

   if(!LOW_ProcessCP15Reg(uiOperation, uiCRn , uiCRm, uiOp1, uiOp2, &ulData))
   {
      return 0;
   }
   if(uiOperation)
   {
       PrintMessage(INFO_MESSAGE, GDB_MONITOR_CP_READ_VALUE, ulData);
       sprintf(szBuffer,GDB_MONITOR_CP_READ_VALUE, (uint32_t)ulData);
       AddStringToOutAsciiz(szBuffer, pcOutput);
   }
   else
   {
       PrintMessage(INFO_MESSAGE, GDB_MONITOR_CP_WRITE_VALUE, ulData);
       sprintf(szBuffer,GDB_MONITOR_CP_WRITE_VALUE);
       AddStringToOutAsciiz(szBuffer, pcOutput);
   }

   return 1;
}

/****************************************************************************
     Function: ReadCPReg
     Engineer: Roshan T R
        Input: char *pcInput - command param string with white char at the begining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Read a Coprocessor register.  
Date           Initials    Description
03-Dec-2009    RTR         Initial
*****************************************************************************/
int32_t ReadCPReg  (char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH*2];
   uint32_t ulCPNumber;
   uint32_t ulRegIndex;
   uint32_t ulWpCmnd;
   uint32_t ulNoOptn=0;
   uint32_t ulData = 0;
   int32_t iErrRet;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;   


   if(*pcInput == '\0')
   {
      sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return 1;
   }

   for (ulWpCmnd=0; ulWpCmnd < MAX_ULONG_STRING_SIZE; ulWpCmnd++)
   {
      if(*pcInput == ' ')
      {
         pcInput++;
         ulNoOptn = 1;
      }
      else
      {
         ulNoOptn = 0;
         break;
      }
   }
   if(ulNoOptn == 1)
   {
      sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return 1;
   }

   if( strlen(pcInput) > _MAX_PATH * 2)
   {
      sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return 1;
   }

   iErrRet = StringsToReadCPRegParams (pcInput, &ulCPNumber , &ulRegIndex);
   if(iErrRet)
   {
       switch(iErrRet)
       {
           case ERR_UNSUPPORTED_CORPOC:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_COPROC);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_UNSUPPORTED_REG:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_REG);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_INVALID_ARG:
           default:
               sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
       }
       return 1;
   }
   if(!LOW_ReadCPReg(ulCPNumber, ulRegIndex, &ulData))
   {
      return 0;
   }
   PrintMessage(INFO_MESSAGE, GDB_MONITOR_CP_READ_VALUE, ulData);
   sprintf(szBuffer,GDB_MONITOR_CP_READ_VALUE, (uint32_t)ulData);
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}

/****************************************************************************
     Function: WriteCPReg
     Engineer: Roshan T R
        Input: char *pcInput - command param string with white char at the begining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Write a Coprocessor register.  
Date           Initials    Description
03-Dec-2009    RTR         Initial
*****************************************************************************/
int32_t WriteCPReg  (char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH*2];
   uint32_t ulCPNumber;
   uint32_t ulRegIndex;
   uint32_t ulWpCmnd;
   uint32_t ulNoOptn=0;
   uint32_t ulData = 0;
   int32_t iErrRet;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;       

   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   for (ulWpCmnd=0; ulWpCmnd < MAX_ULONG_STRING_SIZE; ulWpCmnd++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
           sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
           return 1;
      }


   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }

   iErrRet = StringsToWriteCPRegParams (pcInput, &ulCPNumber , &ulRegIndex, &ulData);

   if(iErrRet)
   {
       switch(iErrRet)
       {
           case ERR_UNSUPPORTED_CORPOC:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_COPROC);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_UNSUPPORTED_REG:
               sprintf(tySI.szError, MONITOR_ERROR_INVALID_REG);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;
           case ERR_INVALID_ARG:
           default:
               sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);       
               break;

       }
       return 1;
   }
   if(!LOW_WriteCPReg(ulCPNumber, ulRegIndex, ulData))
   {
      return 0;
   }
   PrintMessage(INFO_MESSAGE, GDB_MONITOR_CP_WRITE_VALUE);
   sprintf(szBuffer,GDB_MONITOR_CP_WRITE_VALUE);
   AddStringToOutAsciiz(szBuffer, pcOutput);
   return 1;
}

/****************************************************************************
     Function: JTFreq
     Engineer: Rejeesh S Babu
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Target jtfreq selection.  
Date           Initials    Description
31-Mar-2008       RS       Initials
*****************************************************************************/
int32_t JTFreq  (char *pcInput, char *pcOutput)
{
   char  szBuffer[_MAX_PATH];
   uint32_t   ulMultiplier = 1000;
   uint32_t   ulFrequency  = 0;
   int32_t             bValid       = FALSE;
   char           *pszUnit;
   int32_t             iLen;
   char            szFrequency[64];
   uint32_t ulIndex;
   uint32_t ulNoOptn=0;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
     
   pcInput=pcInput;
   pcOutput=pcOutput;
  
   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   for (ulIndex=0; ulIndex < MAX_ULONG_STRING_SIZE; ulIndex++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
           sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
         return 1;
       }

   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }
   
   strncpy(szFrequency, pcInput, sizeof(szFrequency)-1);
   szFrequency[sizeof(szFrequency)-1] = '\0';

   // test for RTCK 
   if ((strcasecmp(szFrequency, "rtck") == 0) ||
       (strcasecmp(szFrequency, "RTCK") == 0))  
        {
            tySI.bJtagRtck = TRUE;
        }
   else
      {
      tySI.bJtagRtck = FALSE;
      // test for kHz or MHz (default) at end of number...
      if ((iLen = (int32_t)strlen(szFrequency)) > 3)
         {
         pszUnit = szFrequency + iLen - 3;
         if (strcasecmp(pszUnit, "khz") == 0)
            {
            pszUnit[0]   = '\0';
            ulMultiplier = 1;
            }
         else if (strcasecmp(pszUnit, "mhz") == 0)
            {
            pszUnit[0]   = '\0';
            ulMultiplier = 1000;
            }
         }
   
      ulFrequency = StrToUlong(szFrequency, &bValid);
   
      if (!bValid)
         {
         
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return 1;
         }
   
      tySI.ulJtagFreqKhz = ulFrequency * ulMultiplier;

      if((tySI.ulJtagFreqKhz<MIN_JTAGFreq) || (tySI.ulJtagFreqKhz>MAX_JTAGFreq))
         {
          sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG); 
          PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
          AddStringToOutAsciiz(tySI.szError, pcOutput);
          return 1;

         }
      }
   JC_SetJtagFrequency();
   if(GDBSERV_NO_ERROR == LOW_SetJtagFreq())      
      {     
      //PrintMessage(INFO_MESSAGE,"JTAG Frequwncy Set to %d .",tySI.ulJtagFreqKhz);
      //strcat(pcOutput, "OK");
      if(tySI.bJtagRtck)
      {
         sprintf(szBuffer,GDB_MONITOR_JTFREQ_RTCK);
         AddStringToOutAsciiz(szBuffer, pcOutput);
         if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, "%s", szBuffer);
         return TRUE;
      }
      else
         {
         sprintf(szBuffer,GDB_MONITOR_JTFREQ_SET,tySI.szJtagFreq);
         AddStringToOutAsciiz(szBuffer, pcOutput);
         if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, "%s", szBuffer);
         return TRUE;
         }
     }
   else
      {
         sprintf(tySI.szError,MONITOR_ERROR_SETTING_JTAG_FREQ);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return TRUE;
      }
}
/****************************************************************************
     Function: WatchpointAddress
     Engineer: Venkitakrishnan K
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Set Watchpoint Address. 
Date           Initials    Description
31-Mar-2008       VK       Initials
*****************************************************************************/
int32_t WatchpointAddress  (char *pcInput, char *pcOutput)
{
   char szBuffer[_MAX_PATH*2];
   uint32_t    uladdress;
   char             szsize[4];
   char             szmode[4];
   //int32_t              iBpIndex=0;
   uint32_t    ulzero=0;
   uint32_t ulWpCmnd;
   uint32_t ulNoOptn=0;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;       

   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   for (ulWpCmnd=0; ulWpCmnd < MAX_ULONG_STRING_SIZE; ulWpCmnd++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
           sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
           return 1;
      }
      

   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }

   StringsToWpModeSizeAndAddressorData (pcInput, szmode ,szsize ,&uladdress);
   if(tySI.bInvalid ==1)
   {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   if(!SetWatchpointModeAndSize(szmode,szsize))
   {
         sprintf(tySI.szError, MONITOR_ERROR_FAILED_MODE_SIZE, szmode,szsize);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return TRUE;
   }

   uiBPType=WP_ADDR_TYPE;

   if(!SetWatchpoint(uladdress,ulzero,uiBPType))
   {
       sprintf(tySI.szError,MONITOR_ERROR_FAILED_WP_SET);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   sprintf(szBuffer,GDB_MONITOR_WP_ADDR_SET,(int32_t)uladdress);
   AddStringToOutAsciiz(szBuffer, pcOutput);
   if (tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "%s", szBuffer);
   return TRUE;

}
/****************************************************************************
     Function: WatchpointData
     Engineer: Venkitakrishnan K
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Set Watchpoint Data.
Date           Initials    Description
31-Mar-2008       VK       Initials
*****************************************************************************/
int32_t WatchpointData  (char *pcInput, char *pcOutput)
{
   
   char szBuffer[_MAX_PATH*2];
   char szmode[4];
   char szsize[4];
   uint32_t uldata;
   uint32_t ulzero=0;
   uint32_t ulWpCmnd;
   uint32_t ulNoOptn=0;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;

   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   for (ulWpCmnd=0; ulWpCmnd < MAX_ULONG_STRING_SIZE; ulWpCmnd++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
           sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
           return 1;
      }

   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }
    

   StringsToWpModeSizeAndAddressorData (pcInput, szmode ,szsize ,&uldata);
   if(tySI.bInvalid ==1)
   {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   if((szsize[0] == 'd') || (szsize[0] == 'D'))
   {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
         
   }
   if(!SetWatchpointModeAndSize(szmode,szsize))
   {
       sprintf(tySI.szError,MONITOR_ERROR_FAILED_MODE_SIZE, szmode,szsize);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   uiBPType=WP_DATA_TYPE;
   if(!SetWatchpoint(ulzero,uldata,uiBPType))
   {
       sprintf(tySI.szError,MONITOR_ERROR_FAILED_WP_SET);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   sprintf(szBuffer,GDB_MONITOR_WP_DATA_SET,(int32_t)uldata);
   AddStringToOutAsciiz(szBuffer, pcOutput);
   if (tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "%s", szBuffer);
   return TRUE;
}
/****************************************************************************
     Function: WatchpointAddressData
     Engineer: Venkitakrishnan K
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: Set Watchpoint Address. 
Date           Initials    Description
31-Mar-2008       VK       Initials
*****************************************************************************/
int32_t WatchpointAddressData  (char *pcInput, char *pcOutput)
{
   
   char szBuffer[_MAX_PATH*2];
   char szmode[4];
   char szsize[4];
   uint32_t uladdress;
   uint32_t uldata;
   //int32_t iBpIndex;
   uint32_t ulWpCmnd;
   uint32_t ulNoOptn=0;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);
   pcInput=pcInput;
   pcOutput=pcOutput;

   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }

   for (ulWpCmnd=0; ulWpCmnd < MAX_ULONG_STRING_SIZE; ulWpCmnd++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
           sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
           return 1;
      }

   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG    );           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }

   StringsToWpModeSizeAddressandData (pcInput,szmode,szsize,&uladdress,&uldata);
   if(tySI.bInvalid ==1)
   {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
   }
   if(!SetWatchpointModeAndSize(szmode,szsize))
   {
      sprintf(tySI.szError, MONITOR_ERROR_FAILED_MODE_SIZE, szmode,szsize);
      PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
      AddStringToOutAsciiz(tySI.szError, pcOutput);
      return TRUE;
      
   }

   uiBPType=WP_ADDR_DATA_TYPE;

   if(!SetWatchpoint(uladdress,uldata,uiBPType))
   {
       sprintf(tySI.szError,MONITOR_ERROR_FAILED_WP_SET);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
   }
   sprintf(szBuffer,GDB_MONITOR_WP_ADDR_DATA,(int32_t)uladdress,(int32_t)uldata);
   AddStringToOutAsciiz(szBuffer, pcOutput);
   if (tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "%s", szBuffer);
   return TRUE;
}
/****************************************************************************
     Function: Semihost
     Engineer: Rejeesh S Babu 
        Input: char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
       Output: int32_t - 1 to send response, 0 to ignore
  Description: set semi-host Data.    
Date           Initials    Description
31-Mar-2008      RS       Initials
*****************************************************************************/
int32_t Semihost(char *pcInput, char *pcOutput)
{
   
   char szBuffer[_MAX_PATH*2];
   char szSemihost[4];
   uint32_t ulTopOfMem;
   uint32_t ulVectorTrapAddr;
   uint32_t ulSHCmnd;
   uint32_t ulNoOptn=0;
   assert(pcInput      !=NULL);
   assert(pcOutput     !=NULL);

   pcInput=pcInput;
   pcOutput=pcOutput;
   
   if(*pcInput == '\0')
       {
       sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);  
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return 1;
       }
   for (ulSHCmnd=0; ulSHCmnd < MAX_ULONG_STRING_SIZE; ulSHCmnd++)
      {
         if(*pcInput == ' ')
         {
            pcInput++;
            ulNoOptn = 1;
         }
         else
         {
            ulNoOptn = 0;
            break;
         }
      }
   if(ulNoOptn == 1)
      {
           sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
           return 1;
      }

   if( strlen(pcInput) > _MAX_PATH * 2)
       {

        sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 1;
       }
   strncpy(szSemihost, pcInput, sizeof(szSemihost)-1);
   szSemihost[sizeof(szSemihost)-1] = '\0';
   
   if ((strcasecmp(szSemihost, "on ") == 0) ||
       (strcasecmp(szSemihost, "ON ") == 0)) 
   {
       ulGSemiState=0x1;
       StringsToTopOfMemandVectorTrapAddr (pcInput+3, &ulTopOfMem ,&ulVectorTrapAddr);
       if(tySI.bInvalid ==1)
       {
           sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
           PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
           AddStringToOutAsciiz(tySI.szError, pcOutput);
           return TRUE;
       }
       if(Low_ConfigureSemihost(ulTopOfMem,ulVectorTrapAddr,ulGSemiState))
        {
          sprintf(tySI.szError,MONITOR_ERROR_SH_FAILED);
          PrintMessage(INFO_MESSAGE, "%s", tySI.szError);
          AddStringToOutAsciiz(tySI.szError, pcOutput);
          return 1;
        }
       sprintf(szBuffer,GDB_MONITOR_SEMIHOST,szSemihost);
       AddStringToOutAsciiz(szBuffer, pcOutput);
       if (tySI.bDebugOutput)
          PrintMessage(LOG_MESSAGE, "%s", szBuffer);
       return TRUE;   
    }
   else   
      if ((strcasecmp(szSemihost, "off") == 0) ||
         (strcasecmp(szSemihost, "OFF") == 0))
         {
          ulGSemiState=0;
          StringsToTopOfMemandVectorTrapAddr (pcInput+3, &ulTopOfMem ,&ulVectorTrapAddr);
          if(Low_ConfigureSemihost(ulTopOfMem,ulVectorTrapAddr,ulGSemiState))
            {
             sprintf(tySI.szError,MONITOR_ERROR_SH_FAILED);
             PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
             AddStringToOutAsciiz(tySI.szError, pcOutput);
             return 1;
            }
          sprintf(szBuffer,GDB_MONITOR_SEMIHOST,szSemihost);
          AddStringToOutAsciiz(szBuffer, pcOutput);
          if (tySI.bDebugOutput)
             PrintMessage(LOG_MESSAGE, "%s", szBuffer);
          return TRUE;
       }
      else
      {
          sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);           
          PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
          AddStringToOutAsciiz(tySI.szError, pcOutput);
          return 1;
      }   
}   
/****************************************************************************
Function:    SetWatchpointModeAndSize
Engineer:    Venkitakrishnan K
Input:       char *pcInput - command param string with white char at the beggining
             char *pcOutput - command param string with output
Output:      int32_t - 1 to send response, 0 to ignore
Description: Sets Watchpoint Mode And Size     
Date             Initials    Description
31-Mar-2008       VK       Initials
*****************************************************************************/
int32_t SetWatchpointModeAndSize( char *pszmode,char *pszsize)
{
   assert(pszmode      !=NULL);
   assert(pszsize     !=NULL);
    switch (*pszmode)
      {
      case 'R': case 'r':
         tyWatchpointAccessType = eWPRead;
         break;
      case 'W': case 'w':
         tyWatchpointAccessType = eWPWrite;
         break;
      case 'D': case 'd':
         tyWatchpointAccessType = eWPAnyAccessType;
         break;
    default: 
      return FALSE;
      }
   switch (*pszsize)
      {
      case 'B': case 'b':
         tyWatchpointSizeType = eWPByte;
         break;
      case 'H': case 'h':
         tyWatchpointSizeType = eWPHalfWord;
         break;
      case 'W': case 'w':
         tyWatchpointSizeType = eWPWord;
         break;
      case 'D': case 'd':
         tyWatchpointSizeType = eWPAnySizeType;
         break;
   default:
         return FALSE;
        
      }
   switch(tyWatchpointSizeType) 
   {
      case eWPByte:
         switch(tyWatchpointAccessType) 
         {
            case eWPRead:
               tyWatchpointDataType = eWPByteRead;
         	   break;
            case eWPWrite:
               tyWatchpointDataType = eWPByteWrite;
         	   break;
            case eWPAnyAccessType:
               tyWatchpointDataType = eWPByteReadWrite;
         	   break;
         default:
             return FALSE;
         }
   	   break;
      case eWPHalfWord:
         switch(tyWatchpointAccessType) 
         {
            case eWPRead:
               tyWatchpointDataType = eWPHalfWordRead;
         	   break;
            case eWPWrite:
               tyWatchpointDataType = eWPHalfWordWrite;
         	   break;
            case eWPAnyAccessType:
               tyWatchpointDataType = eWPHalfWordReadWrite;
         	   break;
         default:
             return FALSE;
         }
   	   break;
      case eWPWord:
         switch(tyWatchpointAccessType) 
         {
            case eWPRead:
               tyWatchpointDataType = eWPWordRead;
         	   break;
            case eWPWrite:
               tyWatchpointDataType = eWPWordWrite;
         	   break;
            case eWPAnyAccessType:
               tyWatchpointDataType = eWPWordReadWrite;
         	   break;
         default:
             return FALSE;
         }
   	   break;
      case eWPAnySizeType:
         switch(tyWatchpointAccessType) 
         {
            case eWPRead:
               tyWatchpointDataType = eWPAnySizeTypeRead;
         	   break;
            case eWPWrite:
               tyWatchpointDataType = eWPAnySizeTypeWrite;
         	   break;
            case eWPAnyAccessType:
               tyWatchpointDataType = eWPAnySizeTypeReadWrite;
         	   break;
         default:
             return FALSE;
         }
   	   break;
   default:
      return FALSE;
   }

return TRUE;
}

/****************************************************************************
     Function: DeleteWatchpoint
     Engineer: Suraj S
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output                       
       Output: TRUE, Always               
  Description: Deletes all watchppoints which are currently set               
Date           Initials    Description
24-May-2010     SJ        Initial
*****************************************************************************/
int32_t DeleteWatchpoint(char *pcInput, char *pcOutput)
{
    char  szBuffer[_MAX_PATH];
    NOREF(pcInput);

    if(LOW_DeleteWatchpoints() != NO_ERROR)
        {
        sprintf(tySI.szError, GDB_MONITOR_NO_WP);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return GDBSERV_ERROR;
        }
    
    sprintf(szBuffer,GDB_MONITOR_DELETED_ALL_WP);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);
    return TRUE;
}

/****************************************************************************
     Function: DeleteWatchpointAddress
     Engineer: Suraj S
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output                       
       Output: TRUE, Always               
  Description: Deletes a specified address watchppoint               
Date           Initials    Description
19-May-2010     SJ        Initial
*****************************************************************************/
int32_t DeleteWatchpointAddress(char* pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0;
    uint32_t ulWpAddress = 0;
    char ppszArgList[_MAX_PATH][_MAX_PATH], szBuffer[_MAX_PATH] = {'\0'};    
    char *pszEndPtr = NULL;
    
    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);

    /* check for invalid args */
    if ((iArgCnt <= 0)
        || (iArgCnt > 1)        
        || (!ValidateStr(ppszArgList[0], HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);     
       return TRUE;
    }

    /* convert the string to address */
    ulWpAddress = strtoul(ppszArgList[0], &pszEndPtr, 16);     

    /*  Delete the particular WP*/    
    if(LOW_DeleteWatchpoint(ulWpAddress, 0, WP_ADDR_TYPE,tySI.tyProcConfig.ulCoreIndex) != NO_ERROR)
        {
        sprintf(tySI.szError, GDB_MONITOR_NO_SPECIFIC_WP);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return GDBSERV_ERROR;
        }
    
    sprintf(szBuffer,GDB_MONITOR_WP_DELETE);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);
    return TRUE;
}

/****************************************************************************
     Function: DeleteWatchpointData
     Engineer: Suraj S
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output                       
       Output: TRUE, Always               
  Description: Deletes a specified data watchppoint               
Date           Initials    Description
21-May-2010     SJ        Initial
*****************************************************************************/
int32_t DeleteWatchpointData(char* pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0;
    uint32_t ulWpData = 0;
    char ppszArgList[_MAX_PATH][_MAX_PATH], szBuffer[_MAX_PATH] = {'\0'};    
    char *pszEndPtr = NULL;
    
    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);

    /* check for invalid args */
    if ((iArgCnt <= 0)
        || (iArgCnt > 1)        
        || (!ValidateStr(ppszArgList[0], HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);     
       return TRUE;
    }

    /* convert the string to address */
    ulWpData = strtoul(ppszArgList[0], &pszEndPtr, 16);     

    /*  Delete the particular WP*/    
    if(LOW_DeleteWatchpoint(0, ulWpData, WP_DATA_TYPE,tySI.tyProcConfig.ulCoreIndex) != NO_ERROR)
        {
        sprintf(tySI.szError, GDB_MONITOR_NO_SPECIFIC_WP);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return GDBSERV_ERROR;
        }
    
    sprintf(szBuffer,GDB_MONITOR_WP_DELETE);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);
    return TRUE;
}

/****************************************************************************
     Function: DeleteWatchpointAddressData
     Engineer: Suraj S
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output                       
       Output: TRUE, Always               
  Description: Deletes a specified address and data watchppoint               
Date           Initials    Description
21-May-2010     SJ        Initial
*****************************************************************************/
int32_t DeleteWatchpointAddressData(char* pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0;
    uint32_t ulWpAddress = 0;
    uint32_t ulWpData = 0;
    char ppszArgList[_MAX_PATH][_MAX_PATH], szBuffer[_MAX_PATH] = {'\0'};    
    char *pszEndPtr = NULL;
    
    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);

    /* check for invalid args */
    if ((iArgCnt <= 0)
        || (iArgCnt > 2)        
        || (!ValidateStr(ppszArgList[0], HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);     
       return TRUE;
    }

    /* convert the string to address */
    ulWpAddress = strtoul(ppszArgList[0], &pszEndPtr, 16);     

    /* convert the string to address */
    ulWpData = strtoul(ppszArgList[1], &pszEndPtr, 16);     

    /*  Delete the particular WP*/    
    if(LOW_DeleteWatchpoint(ulWpAddress, ulWpData, WP_ADDR_DATA_TYPE,tySI.tyProcConfig.ulCoreIndex) != NO_ERROR)
        {
        sprintf(tySI.szError, GDB_MONITOR_NO_SPECIFIC_WP);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return GDBSERV_ERROR;
        }
    
    sprintf(szBuffer,GDB_MONITOR_WP_DELETE);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);
    return TRUE;
}

/****************************************************************************
Function:       SetWatchpoint
Engineer:       Venkitakrishnan K
Input:          char *pcInput - command param string with white char at the beggining
               char *pcOutput - command param string with output
Output:        int32_t - 1 to send response, 0 to ignore
Description:    Set Watchpoint       
Date           Initials    Description
31-Mar-2008       VK       Initials
*****************************************************************************/
int32_t SetWatchpoint(uint32_t ulAddress,uint32_t uldata,uint32_t uiBPtype)
{

    NOREF(uiBPtype);
    if (!LOW_SetWatchpoint(ulAddress, 
                           uldata,
                           (int32_t)tyWatchpointDataType,
                           uiBPType))
     return FALSE;

return TRUE;
}
/****************************************************************************
     Function: HelpJtagFrequency
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for frequency selection
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static void HelpJtagFrequency(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {
      PrintMessage(HELP_MESSAGE, "%*s<freq> for Opella-XD can be in range\n", GDBSERV_HELP_COL_WIDTH+4, " ");
      PrintMessage(HELP_MESSAGE, "%*sfrom 1kHz to 100MHz (default 1MHz) or RTCK.\n", GDBSERV_HELP_COL_WIDTH+4, " ");
      }
}
/****************************************************************************
     Function: HelpHwReset
     Engineer: Sandeep V L
        Input: Void
       Output: Void
  Description: Additional help for Hard Reset
Date           Initials    Description
13-Apr-2010     SVL        Initial
*****************************************************************************/
static void HelpHwReset(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {
      PrintMessage(HELP_MESSAGE, "%*s<Reset-Mode>\n", 
                   GDBSERV_HELP_COL_WIDTH+4, " ");      
      PrintMessage(HELP_MESSAGE, "%*s- 1, HardReset and Halt reset vector.\n", 
                   GDBSERV_HELP_COL_WIDTH+5, " ");
      PrintMessage(HELP_MESSAGE, "%*s- 2, HardReset and Enter Debug Mode.\n", 
                   GDBSERV_HELP_COL_WIDTH+5, " ");
      PrintMessage(HELP_MESSAGE, "%*s- 3, HardReset and Delay.\n", 
                   GDBSERV_HELP_COL_WIDTH+5, " ");
      PrintMessage(HELP_MESSAGE, "%*s<Delay in Seconds>\n", 
                   GDBSERV_HELP_COL_WIDTH+4, " ");      
      PrintMessage(HELP_MESSAGE, "%*sDelay period for HardReset and Delay option.\n", 
                   GDBSERV_HELP_COL_WIDTH+5, " ");
      PrintMessage(HELP_MESSAGE, "%*sDefault is 5 seconds.\n", 
                   GDBSERV_HELP_COL_WIDTH+5, " ");
      }
}
/****************************************************************************
     Function: HelpWPAddr
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for Watchpoint parameters
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static void HelpWPAddr(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {
      PrintMessage(HELP_MESSAGE, "\n%*sSet <addr> watchpoint.\n", GDBSERV_HELP_COL_WIDTH+4, " ");
      PrintMessage(HELP_MESSAGE, "%*sr read.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      PrintMessage(HELP_MESSAGE, "%*sw write.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      PrintMessage(HELP_MESSAGE, "%*sd don't care.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      PrintMessage(HELP_MESSAGE, "%*sb byte.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      PrintMessage(HELP_MESSAGE, "%*sh half-word.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      PrintMessage(HELP_MESSAGE, "%*sw word.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      PrintMessage(HELP_MESSAGE, "%*sd don't care.\n", GDBSERV_HELP_COL_WIDTH+7, " ");
      }
}
/****************************************************************************
     Function: HelpWPData
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for Watchpoint parameters
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static void HelpWPData(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {
      PrintMessage(HELP_MESSAGE, "\n%*sSet <data> watchpoint.\n", GDBSERV_HELP_COL_WIDTH+4, " ");
      }
}
/****************************************************************************
     Function: HelpWPData
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for Watchpoint parameters
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static void HelpWPAD(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {
      PrintMessage(HELP_MESSAGE, "\n%*sSet <addr> and <data> watchpoint.\n", GDBSERV_HELP_COL_WIDTH+4, " ");
      }
}
/****************************************************************************
     Function: HelpSemiHost
     Engineer: Venkitakrishnan K
        Input: none
       Output: none
  Description: Additional help for Semihost selection
Date           Initials    Description
23-Mar-2008      VK         initial
*****************************************************************************/
static void HelpSemiHost(void)
{
   if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
      {  
         PrintMessage(HELP_MESSAGE, "\n%*sEnable semi-hosting.\n", GDBSERV_HELP_COL_WIDTH+4, " ");
         PrintMessage(HELP_MESSAGE, "%*sDefault is off.\n", GDBSERV_HELP_COL_WIDTH+4, " ");
         PrintMessage(HELP_MESSAGE, "%*s<addr1> Top of memory (no default).\n", GDBSERV_HELP_COL_WIDTH+4, " ");
         PrintMessage(HELP_MESSAGE, "%*s<addr2> Vector trap address (default:0x8).\n", GDBSERV_HELP_COL_WIDTH+4, " ");
      }
}

/****************************************************************************
     Function: ConfigVectorCatch
     Engineer: Sandeep V L
        Input: char *pcInput - command param string with white char at the begining
               char *pcOutput - command param string with output
       Output: No of arguments
  Description: Configure vector catch settings  
Date           Initials    Description
16-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t ConfigVectorCatch(char *pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0;
    char ppszArgList[_MAX_PATH][_MAX_PATH], szBuffer[_MAX_PATH], *pArg = NULL;
    uint32_t ulVectorMask = 0xff; //default value to enable all vectors

    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    // Check whether already connected
    if (!tySI.bIsConnected) 
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);
    /* check for invalid args */
    if ((iArgCnt <= 0)
        || (iArgCnt > 1)
        || (!ValidateStr(ppszArgList[0],HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
    }

    /* check if 0X prefix is there */
    if((strncmp(ppszArgList[0], "0x", 2) == 0)
       ||(strncmp(ppszArgList[0], "0x", 2) == 0))
    {
        pArg = &(ppszArgList[0][2]);
    }
    else
    {
        pArg = ppszArgList[0];
    }
    
    StringToUlong(pArg, &ulVectorMask);
    /* if the given mask is invalid */
    if(ulVectorMask > 255)
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
    }
    /* configure the vector catch */
    if(!LOW_CfgVectorCatch(&ulVectorMask))
    {
        sprintf(szBuffer,MONITOR_ERROR_VC_CFG_FAILED);
        AddStringToOutAsciiz(szBuffer, pcOutput);
        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, "%s", szBuffer);
        return FALSE;
    }
    sprintf(szBuffer,GDB_MONITOR_VECTOR_CATCH_EN);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);

    return TRUE;
}

/****************************************************************************
     Function: VeiwArmV7Regs
     Engineer: Deepa V.A.
        Input: 
       Output: 
  Description: Configure vector catch settings  
Date           Initials    Description
07-Dec-2009     DVA        Initial
*****************************************************************************/
void VeiwArmV7Regs(void)
{

}
/****************************************************************************
     Function: ValidateBpType
     Engineer: Sandeep V L
        Input: char *pszArg - Pointer to an argument
               TyBpType *ptyBpType - Pointer to the breakpoint type
       Output: TRUE, On success
               FALSE, On Failure
  Description: Validates the breakpoint type, whether its ARM, THUMB or AUTO  
Date           Initials    Description
19-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t ValidateBpType(char *pszArg, TyBpType *ptyBpType)
{
    if((strcmp(pszArg, "auto") == 0)
       ||(strcmp(pszArg, "AUTO") == 0))
    {
        *ptyBpType = BPTYPE_AUTO;
    }
    else if((strcmp(pszArg, "arm") == 0)
            ||(strcmp(pszArg, "ARM") == 0))
    {
        *ptyBpType = BPTYPE_ARM;
    }
    else if((strcmp(pszArg, "thumb") == 0)
            ||(strcmp(pszArg, "THUMB") == 0))
    {
        *ptyBpType = BPTYPE_THUMB;
    }
    else
    {
        return FALSE;
    }
    return TRUE;
}

/****************************************************************************
     Function: AddToBpTypeList
     Engineer: Sandeep V L
        Input: TyBpType tyBpType - Breakpoint type
               uint32_t ulBpAddress - Breakpoint Address
       Output: TRUE, On success
               FALSE, On Failure
  Description: Add a node to breakpoint type list  
Date           Initials    Description
20-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t AddToBpTypeList(TyBpType tyBpType, uint32_t ulBpAddress)
{
    TyBpTypeInfo *pNewNode = NULL;

    pNewNode = (TyBpTypeInfo *)malloc(sizeof(TyBpTypeInfo));
    if(!pNewNode)
    {
        /* Memory allocation failure */
        PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
        return FALSE;
    }

    /* fill the node with info */
    pNewNode->tyBpType = tyBpType;
    pNewNode->ulBpAddress = ulBpAddress;
    pNewNode->next = NULL;

    /* for the first node */
    if(ptyBpTypeList == NULL)
    {
        ptyBpTypeList = pNewNode;
    }
    else
    {
        /* New node is added to list as in a stack */
        pNewNode->next = ptyBpTypeList;
        ptyBpTypeList = pNewNode;
    }
    return TRUE;
}

/****************************************************************************
     Function: GetBpType
     Engineer: Sandeep V L
        Input: uint32_t ulAddress - Breakpoint Address
               TyBpTypeInfo **pBpTypeNode - Pointer to a node in breakpoint 
               type list               
       Output: Breakpoint type, On success
               -1, On Failure
  Description: Get the breakpoint type in a given address from the list  
Date           Initials    Description
20-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t GetBpType(uint32_t ulAddress, TyBpTypeInfo **pBpTypeNode)
{
    TyBpTypeInfo *pTempNode = NULL;

    NOREF(pBpTypeNode);

    /* Start from the first node */
    pTempNode = ptyBpTypeList;
    while (pTempNode != NULL) 
    {
        if(pTempNode->ulBpAddress == ulAddress)
        {
            /* return the pointer to this node and the breakpoint type */
            *pBpTypeNode = pTempNode;
            return (int32_t)pTempNode->tyBpType;
        }
        pTempNode = pTempNode->next;
    }
    return -1;
}

/****************************************************************************
     Function: UpdateBpTypeInfo
     Engineer: Sandeep V L
        Input: TyBpTypeInfo *pBpNode - Pointer to a node in breakpoint type list
               TyBpType tyBpType     - Breakpoint type                        
       Output: TRUE, On success
               FALSE, On Failure
  Description: Update the breakpoint type information  
Date           Initials    Description
20-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t UpdateBpTypeInfo(TyBpTypeInfo *pBpNode, TyBpType tyBpType)
{
    if(pBpNode)
    {
        pBpNode->tyBpType = tyBpType; 
    }
    else
    {
        /* invalid pointer */
        return FALSE;
    }
    return TRUE;
}

/****************************************************************************
     Function: RemoveBpTypeNode
     Engineer: Sandeep V L
        Input: uint32_t ulBpAddress - Breakpoint address                       
       Output: TRUE, On success
               FALSE, On Failure
  Description: Remove a node from the list of braekpoint types
Date           Initials    Description
20-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t RemoveBpTypeNode(uint32_t ulBpAddress)
{
    TyBpTypeInfo *pCurrNode = NULL, *pPrevNode = NULL;

    /* pointer to the start node */
    pPrevNode = pCurrNode = ptyBpTypeList;
    while(pCurrNode != NULL)
    {
        /* if a match is found */
        if(pCurrNode->ulBpAddress == ulBpAddress)
        {
            /* if its the start node itself */
            if(pCurrNode == ptyBpTypeList)
            {
                /* update the head pointer to the next node and then delete 
                the previous one */
                ptyBpTypeList = pCurrNode->next;
                pCurrNode->next = NULL;
                free(pCurrNode);
                pCurrNode = NULL;
            }
            else
            {
                /* update the pointer of the previous node to point to current 
                node's next and then delete the current node */
                pPrevNode->next = pCurrNode->next;
                pCurrNode->next = NULL;
                free(pCurrNode);
                pCurrNode = NULL;
            }            
            return TRUE;
        }
        /* move the previous node to the current one and move the current pointer
        to the next one */
        pPrevNode = pCurrNode;
        pCurrNode = pCurrNode->next;
    }
    /* no match found */
    return FALSE;
}

/****************************************************************************
     Function: RemoveBpTypeList
     Engineer: Sandeep V L
        Input: None                       
       Output: TRUE, Always               
  Description: Remove entire breakpoint type list, when connection to debug 
               target is closed
Date           Initials    Description
20-Oct-2009     SVL        Initial
*****************************************************************************/
int32_t RemoveBpTypeList(void)
{
    TyBpTypeInfo *ptyCurrNode = NULL, *ptyNextNode = NULL;

    /* Point to the head of the list */
    ptyCurrNode = ptyBpTypeList;

    while(ptyCurrNode != NULL)
    {
        ptyNextNode = ptyCurrNode->next;
        ptyCurrNode->next = NULL;
        free(ptyCurrNode);
        ptyCurrNode = ptyNextNode;
    }

    /* reset the head pointer to NULL */
    ptyBpTypeList = NULL;
    return TRUE;
}

/****************************************************************************
     Function: SetBpType
     Engineer: Sandeep V L
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output                       
       Output: TRUE, Always               
  Description: Set the break point type forcefully                
Date           Initials    Description
19-Oct-2009     SVL        Initial
31-Mar-2010     SVL        StringToUlong does not consider '0x' appended 
                           string. So used strtoul instead.
*****************************************************************************/
int32_t SetBpType(char* pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0, iBpType = -1;
    uint32_t ulBpAddress = 0;
    char ppszArgList[_MAX_PATH][_MAX_PATH], szBuffer[_MAX_PATH] = {'\0'}; 
    char *pszEndPtr = NULL;
    TyBpType tyBpType = BPTYPE_AUTO;
    TyBpTypeInfo *pBpTypeNode = NULL;
    
    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    // Check whether already connected
    if (!tySI.bIsConnected) 
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);
    /* check for invalid args */
    if ((iArgCnt <= 0)
        || (iArgCnt > 2)
        || (!ValidateBpType(ppszArgList[0], &tyBpType))
        || (!ValidateStr(ppszArgList[1], HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
    }

    /* convert the string to address */
    ulBpAddress = strtoul(ppszArgList[1], &pszEndPtr, 16);

    /* Check whether its already added */
    iBpType = GetBpType(ulBpAddress, &pBpTypeNode);
    if(iBpType == -1)
    {
         /* store the breakpoint type in a list */
        if(!AddToBpTypeList(tyBpType, ulBpAddress))
        {
            /* unable to add to the list */
            /* Return error message */
            sprintf(tySI.szError,MONITOR_ERROR_SET_BPTYPE);
            PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
            AddStringToOutAsciiz(tySI.szError, pcOutput);      
            return TRUE;
        }
    }
    else
    {
        /* Already added to the list with a different bptype */
        if((TyBpType)iBpType != tyBpType)
        {
            /* Update the Breakpont type information */
            if(!UpdateBpTypeInfo(pBpTypeNode, (TyBpType)tyBpType))
            {
               /* Return error message */
               sprintf(tySI.szError,MONITOR_ERROR_SET_BPTYPE);
               PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
               AddStringToOutAsciiz(tySI.szError, pcOutput);              
               return TRUE;
            }            
        }
        else
        {
            sprintf(szBuffer,GDB_MONITOR_BPTYPE_ALREADY_SET);
            AddStringToOutAsciiz(szBuffer, pcOutput);
            if (tySI.bDebugOutput)
                PrintMessage(LOG_MESSAGE, "%s", szBuffer);
            return TRUE;
        }
    }
    sprintf(szBuffer,GDB_MONITOR_SET_BPTYPE);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);
    return TRUE;
}

/****************************************************************************
     Function: UnSetBpType
     Engineer: Sandeep V L
        Input: char *pcInput - command param string with white char at the beginning
               char *pcOutput - command param string with output                       
       Output: TRUE, Always               
  Description: Remove a breakpoint type information from the list               
Date           Initials    Description
19-Oct-2009     SVL        Initial
31-Mar-2010    SVL         StringToUlong does not consider '0x' appended 
                           string. So used strtoul instead.
*****************************************************************************/
int32_t UnSetBpType(char* pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0;
    uint32_t ulBpAddress = 0;
    char ppszArgList[_MAX_PATH][_MAX_PATH], szBuffer[_MAX_PATH] = {'\0'};    
    char *pszEndPtr = NULL;
    
    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    // Check whether already connected
    if (!tySI.bIsConnected) 
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);
    /* check for invalid args */
    if ((iArgCnt <= 0)
        || (iArgCnt > 1)        
        || (!ValidateStr(ppszArgList[0], HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);     
       return TRUE;
    }

    /* convert the string to address */
    ulBpAddress = strtoul(ppszArgList[0], &pszEndPtr, 16);     

    /* Remove it from the list */
    if(!RemoveBpTypeNode(ulBpAddress))
    {
        /* Return error message */
       sprintf(tySI.szError,MONITOR_ERROR_UNSET_BPTYPE);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);     
       return TRUE;
    }
    sprintf(szBuffer,GDB_MONITOR_UNSET_BPTYPE);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);
    return TRUE;
}
#if 0
/****************************************************************************
     Function: ViewArmV7Regs
     Engineer: Deepa V.A.
        Input:                   
       Output:         
  Description: Remove a breakpoint type information from the list               
Date           Initials    Description
09-Dec-2009     DVA        Initial
*****************************************************************************/
int32_t ViewArmV7Regs(char* pcInput, char *pcOutput)
{
    uint32_t ulTempRegArray[4]={'\0'};
    uint32_t ulRegNum, i=0;
    char cTempMsg[10];
    char  szBuffer[50] = {'\0'};        // ppszArgList[_MAX_PATH][_MAX_PATH],
    //int32_t nTmpCnt=0;

    assert(pcOutput != NULL);
    NOREF(pcInput);

    for(ulRegNum = (int32_t)REG_CORTEXM3_PRIMASK; ulRegNum <= (int32_t)REG_CORTEXM3_CONTROL; ulRegNum++)
        {
        if((int32_t)REG_CORTEXM3_BASEPRI_MAX == ulRegNum)
            {
            continue;
            }
        if (LOW_ReadRegister(ulRegNum, &ulTempRegArray[i]) == FALSE)
            {
                return FALSE;
            }
        i++;
        }
    (void)itoa((int32_t)ulTempRegArray[0], cTempMsg, 16);
    sprintf(szBuffer,"%s0x%08X\n", GDB_MONITOR_VIEW_PRIMASK, (uint32_t)ulTempRegArray[0]);
    AddStringToOutAsciiz(szBuffer, pcOutput);

    (void)itoa((int32_t)ulTempRegArray[1], cTempMsg, 16);
    sprintf(szBuffer,"%s0x%08X\n", GDB_MONITOR_VIEW_FAULTMASK, (uint32_t)ulTempRegArray[1]);
    AddStringToOutAsciiz(szBuffer, pcOutput);


    (void)itoa((int32_t)ulTempRegArray[2], cTempMsg, 16);
    sprintf(szBuffer,"%s0x%08X\n", GDB_MONITOR_VIEW_BASEPRI, (uint32_t)ulTempRegArray[2]);
    AddStringToOutAsciiz(szBuffer, pcOutput);

    (void)itoa((int32_t)ulTempRegArray[3], cTempMsg, 16);
    sprintf(szBuffer,"%s0x%08X\n", GDB_MONITOR_VIEW_CONTROL, (uint32_t)ulTempRegArray[3]);
    AddStringToOutAsciiz(szBuffer, pcOutput);

    //PrintMessage(INFO_MESSAGE, "REG_PRIMASK %X", ulTempRegArray[0]);
    //PrintMessage(INFO_MESSAGE, "REG_FAULTMASK %X", ulTempRegArray[1]);
    //PrintMessage(INFO_MESSAGE, "REG_BASEPRI %X", ulTempRegArray[2]);
    //PrintMessage(INFO_MESSAGE, "REG_CONTROL %X", ulTempRegArray[3]);

    return TRUE;
}
#endif
/****************************************************************************
     Function: GetReg
     Engineer: Deepa V.A.
        Input:                   
       Output:         
  Description: Remove a breakpoint type information from the list               
Date           Initials    Description
09-Dec-2009     DVA        Initial
*****************************************************************************/
int32_t GetReg(char* pcInput, char *pcOutput)
{
    uint32_t ulRegValue=0;
    uint32_t ulRegNum=0;
    char  szBuffer[50] = {'\0'};        // ppszArgList[_MAX_PATH][_MAX_PATH],
    char ppszArgList[_MAX_PATH][_MAX_PATH];
    int32_t iArgCnt = 0;
    int32_t bRegFound=FALSE;

    assert(pcOutput != NULL);
    assert(pcInput != NULL);

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);
    /* check for invalid args */
    if ((iArgCnt < 0) || (iArgCnt > 1 ))
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
    }

    bRegFound = GetRegNumber(ppszArgList[0], &ulRegNum);
    if(FALSE == bRegFound)
        {
        sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return TRUE;
        }
    //ulRegNum = (uint32_t)atoi(ppszArgList[0]);

    /* For Handling CortexM3 specific registers */
    if (LOW_ReadRegister(ulRegNum, &ulRegValue) == FALSE)
        {
        return FALSE;
        }
    sprintf(szBuffer,"%s:0x%08X\n", ppszArgList[0] , (uint32_t)ulRegValue);//GDB_MONITOR_GET_REGS
    AddStringToOutAsciiz(szBuffer, pcOutput);
    return TRUE;
}
static int32_t GetRegNumber(char* szRegName, uint32_t* pulRegNum)
{
    int32_t nMaxNumRegs;
    int32_t bRegFound = FALSE;
    int32_t i;

    *pulRegNum = 0xFFFFFFFF;
    if(gbCortexM3)
        {
        nMaxNumRegs = 25;
        }
    else
        {
        nMaxNumRegs=15;
        }
    for(i = 0; i <= nMaxNumRegs; i++)
        {
        if(strcmp(szArmV7mRegs[i], szRegName) == 0)
            {
            *pulRegNum  = (uint32_t)i;
            bRegFound = TRUE;
            break;
            }
        }
    if (FALSE == gbCortexM3)
        {
        if(strcmp("cpsr", szRegName) == 0)
            {
            bRegFound = TRUE;
            *pulRegNum = (uint32_t)REG_ARM_CPSR;
            }
        }
    else
        {
        if(*pulRegNum >= 16)
            {/* Adjusting the offset for CortexM3 register format */
            *pulRegNum = (*pulRegNum-16)+(int32_t)REG_CORTEXM3_xPSR;
            }
        }
    return bRegFound;
}
/****************************************************************************
     Function: SetReg
     Engineer: Deepa V.A.
        Input:                   
       Output:         
  Description: Remove a breakpoint type information from the list               
Date           Initials    Description
09-Dec-2009     DVA        Initial
*****************************************************************************/
int32_t SetReg(char* pcInput, char *pcOutput)
{
    int32_t iArgCnt = 0;
    uint32_t ulRegNum, ulRegValue;
    char ppszArgList[_MAX_PATH][_MAX_PATH], *pArg = NULL;
    char szBuffer[_MAX_PATH] = {'\0'};
    int32_t bRegFound = FALSE;

    assert(pcInput != NULL);
    assert(pcOutput != NULL);

    /* get the args */
    iArgCnt = GetArgs(pcInput, ppszArgList);

    /* check for invalid args */
    if ((iArgCnt < 0)
        || (iArgCnt > 3 )
        || (!ValidateStr(ppszArgList[1],HEX, 10))) 
    {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);
       return TRUE;
    }

    bRegFound = GetRegNumber(ppszArgList[0], &ulRegNum);
    if(FALSE == bRegFound)
        {
        sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return TRUE;
        }

    /* check if 0X prefix is there */
    if((strncmp(ppszArgList[1], "0X", 2) == 0)
       ||(strncmp(ppszArgList[1], "0x", 2) == 0))
    {
        pArg = &(ppszArgList[1][2]);
    }
    else
    {
        pArg = ppszArgList[1];
    }
    /* convert the string to address */
    StringToUlong(pArg, &ulRegValue);

    if (LOW_WriteRegister(ulRegNum, ulRegValue) == FALSE)
        {
        /* Return error message */
        sprintf(tySI.szError,MONITOR_ERROR_SET_ARMV7_REGS);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);     
        return TRUE;
        }
    
    sprintf(szBuffer, GDB_MONITOR_SET_REGS);
    AddStringToOutAsciiz(szBuffer, pcOutput);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "%s", szBuffer);

    //PrintMessage(INFO_MESSAGE, "REG_PRIMASK %X", ulTempRegArray[0]);
    //PrintMessage(INFO_MESSAGE, "REG_FAULTMASK %X", ulTempRegArray[1]);
    //PrintMessage(INFO_MESSAGE, "REG_BASEPRI %X", ulTempRegArray[2]);
    //PrintMessage(INFO_MESSAGE, "REG_CONTROL %X", ulTempRegArray[3]);

    return TRUE;
}

/****************************************************************************
     Function: GetPeripheralRegGroups
     Engineer: Jiju George
        Input: *pcInput : command param string with white char at the beginning 
       Output: boolean stating if operation was successful
  Description: Returns list of defined peripheral register groups with index in below format
               1,reggroup1
               2,reggroup2
               3,reggroup3

Date           Initials    Description
25-Apr-2008    JG          Initial
21-Oct-2009    SVL         Added for Device register file support in ARM
                           NOTE: This can be removed from this file if the 
                           same function from gdbmipsmonitor is moved to a 
                           common file
****************************************************************************/
static int32_t GetPeripheralRegGroups (char *pcInput, char *pcOutput)
{
   int32_t bReturn = TRUE;
   char szIndex[_MAX_PATH] = {"\0"};
   int32_t iIndex=0;
   char szBuffer[_MAX_PATH], szResult[_MAX_PATH] = {'\0'};

   NOREF(pcInput);
   // Check whether already connected
   if (!tySI.bIsConnected) 
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

   // Copy all peripheral register group names to output 
   // Clear temp result first
   szResult[0] = '\0';
   PrintMessage(DEBUG_MESSAGE, "tySI.uiGroupDefArrayEntryCount:%d",tySI.uiGroupDefArrayEntryCount);
   for(;iIndex<(int32_t)tySI.uiGroupDefArrayEntryCount;++iIndex)
      {
      PrintMessage(DEBUG_MESSAGE, "GetPeripheralRegGroups:%d:%s",iIndex,tySI.ptyGroupDefArray[iIndex].pszGroupName);
      sprintf(szIndex,"%d",iIndex);
      strcat(szResult,szIndex);
      strcat(szResult,",");
      strcat(szResult,tySI.ptyGroupDefArray[iIndex].pszGroupName);
      strcat(szResult,"\n");
      }

   AddStringToOutAsciiz(szResult, pcOutput);   
   return bReturn;
}

/****************************************************************************
     Function: GetPeripheralRegisters
     Engineer: Jiju George
        Input: *pcInput : command param string with white char at the beginning 
                          Expects a peripheral register group index of which
                          peripheral registers are to be retrieved.
       Output: boolean stating if operation was successful
  Description: Returns the list of defined peripheral registers in the groups
               given peripheral register group. Each entry has a regsiter index, 
               register name and value in below format:
               index1,regname1,value1
               index2,regname2,value2
               index3,regname3,value3

Date           Initials    Description
25-Apr-2008    JG          Initial
21-Oct-2009    SVL         Added for Device register file support in ARM
                           NOTE: This can be removed from this file if the 
                           same function from gdbmipsmonitor is moved to a 
                           common file
31-Dec-2009    SVL         Modified for PathFinder-XD's Peripheral Register 
                           View. RegisterDefintionType will also be stored 
                           in the data structures.
****************************************************************************/
static int32_t GetPeripheralRegisters (char *pcInput, char *pcOutput)
{
   int32_t bReturn = TRUE;
   char szTempBuf[_MAX_PATH] = {"\0"};
   uint32_t ulValue = 0;
   char szValue[_MAX_PATH] = {"\0"};
   int32_t iGroupIndex =0;
   int32_t iIndex = 0;
   char szBuffer[_MAX_PATH],szResult[_MAX_PATH*100];

   // Check whether already connected
   if (!tySI.bIsConnected) 
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }


   // Find register group index from input first
   iGroupIndex = atoi(pcInput);
   if (iGroupIndex < (int32_t)tySI.uiRegDefArrayAllocCount)
      {

      // Clear temp result first
      szResult[0] = '\0';

      // Copy all registers and value
      iIndex=tySI.ptyGroupDefArray[iGroupIndex].iStartRegIndex;
      for(;iIndex<=tySI.ptyGroupDefArray[iGroupIndex].iEndRegIndex;++iIndex)
         {
         sprintf(szTempBuf,"%d",iIndex);
         strcat(szResult,szTempBuf);
         strcat(szResult,",");
         strcat(szResult,tySI.ptyRegDefArray[iIndex].pszRegName);
         strcat(szResult,",");
         if (GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(&(tySI.ptyRegDefArray[iIndex]),&ulValue))
         {
            ulValue = 0;
         }
         /* If the Register values need to be printed in Hex format */
         //sprintf(szValue,"0x%08lx",ulValue);
         sprintf(szValue,"%lx",ulValue);
         strcat(szResult,szValue);
         /* Added for PathFinder-XD's Peripheral register view */
         strcat(szResult,",");
         sprintf(szTempBuf,"%d",tySI.ptyRegDefArray[iIndex].iRegDefType);
         strcat(szResult,szTempBuf);
         strcat(szResult,",");
         sprintf(szTempBuf,"%u",tySI.ptyRegDefArray[iIndex].ubBitSize);
         strcat(szResult,szTempBuf);
         strcat(szResult,",");
         sprintf(szTempBuf,"%u",tySI.ptyRegDefArray[iIndex].ubBitOffset);
         strcat(szResult,szTempBuf); 

         strcat(szResult,"\n");
         }

      AddStringToOutAsciiz(szResult, pcOutput);
      }
   else
      {
      sprintf(szBuffer,"Invalid register group index.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      }
   
   return bReturn;
}

/****************************************************************************
     Function: SetPeripheralRegister
     Engineer: Jiju George
        Input: *pcInput : command param string with white char at the beginning 
                          Expects a peripheral register name of which
                          value has to be set followed by the value to be set
                          in hexa-decimal format.
       Output: boolean stating if operation was successful
  Description: Sets the given value to the peripheral register with the given 
               name

Date           Initials    Description
25-Apr-2008    JG          Initial
21-Oct-2009    SVL         Added for Device register file support in ARM
                           NOTE: This can be removed from this file if the 
                           same function from gdbmipsmonitor is moved to a 
                           common file
31-Mar-2010    SVL         StringToUlong does not consider '0x' appended 
                           string. So used strtoul instead.
****************************************************************************/
static int32_t SetPeripheralRegister (char *pcInput, char *pcOutput)
{
   int32_t bReturn = TRUE;
   uint32_t ulValue = 0;
   int32_t iRegIndex= -1, iArgCnt = 0;
   char szBuffer[_MAX_PATH], szArgList[_MAX_PATH][_MAX_PATH];
   char *pszEndPtr = NULL;

   // Check whether already connected
   if (!tySI.bIsConnected) 
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return 0;
      }
   /* Parse the input */
   iArgCnt = GetArgs(pcInput, szArgList);
   if ((iArgCnt <= 0)
        || (iArgCnt > 2)  
        || (!ValidateStr(szArgList[0], DEC, 5))
        || (!ValidateStr(szArgList[1], HEX, 10))) 
      {
       sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
       PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
       AddStringToOutAsciiz(tySI.szError, pcOutput);     
       return TRUE;
      }
   /* Index value */
   iRegIndex = atoi(szArgList[0]);
   /* Register value */
   ulValue = strtoul(szArgList[1], &pszEndPtr, 16);

   // Set Value
   if( iRegIndex >=0 && iRegIndex < (int32_t)tySI.uiRegDefArrayAllocCount ) 
      {
         if (GDBSERV_NO_ERROR != LOW_WriteRegisterByRegDef(&(tySI.ptyRegDefArray[iRegIndex]),ulValue))
            {
            sprintf(szBuffer,"Register write failed.\n");
            AddStringToOutAsciiz(szBuffer, pcOutput);
            return FALSE;
            }
         else 
            {
            sprintf(szBuffer,"OK.\n");
            AddStringToOutAsciiz(szBuffer, pcOutput);
            }
      }
   else
      {
      sprintf(szBuffer,"Invalid register index.\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

   return bReturn;
}
#endif
