/****************************************************************************
       Module: gdbFlash.h
     Engineer: Sudeep R K
  Description: Ashling GDB Server, ashload entry function
Date           Initials    Description
21-Dec-2009    SRK          Initial & cleanup
****************************************************************************/
#ifndef GDBFLASH_H_
#define GDBFLASH_H_

int32_t FlashWrChunk(char *pcInput, char *pcOutput);
int32_t FlashToBin(char *pcInput, char *pcOutput);
int32_t FlashPrepareChunk(char *pcInput, char *pcOutput);
int32_t FlashGetSizeOfSection(char *pcInput, char *pcOutput);
int32_t FlashProcInFile(char *pcInput, char *pcOutput);
int32_t FlashGetOffsetOfSection(char *pcInput, char *pcOutput);
int32_t FlashVerify(char *pcInput, char *pcOutput);
#endif   // GDBFLASH_H_
