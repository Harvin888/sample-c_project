/****************************************************************************
       Module: gdbFlashPrepareChunk.c
     Engineer: Sudeep RK
  Description: Ashling GDB Server, FLASH program
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
28-Mar-2010    VK         Modifed for building with Lint
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>

#include "../flashutils/gdbflashcommonutils.h"
#include "../flashutils/gdbflashcommonerrors.h"
#include "../../../gdbglob.h"
#include "../../../gdbxml.h"
#include "../../../gdbty.h"
#include "../../../gdbutil.h"
#include "../../../gdberr.h"
#include "../../../elf.h"
#include "gdbserver/moncommand/ashload/gdbashloadutils.h"
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"

#ifdef FLASH_SUPPORT
typedef struct _FlashPrepChunkProcessData 
   {
   char* pcGDBReplyOutput;              //The pointer to output reply of "FlashPrepareChunk" to GDB
   char szErrorBuffer[FLASHPREP_CHUNK_MESSAGE_BUFFER_SIZE];
   uint32_t ulSectionNum;
   int32_t bSecNumValid;
   uint32_t ulUtilBuff;
   int32_t bUtilBuffValid;
   uint32_t ulUtilBuffSize;
   int32_t bUtilBuffSizeValid;
   /* to indicate whether the command is initiated from Script */
   int32_t iScriptMode;
   } FlashPrepChunkProcessData;

//External variales
extern TyServerInfo tySI;
//Static variables
static FlashPrepChunkProcessData tyFlashPrepChunkProcData;
//Static functions
static void FlashPrepareChunkInit(char *pcGDBOutput);
static int32_t FlashPrepareChunkParseCommand(char *pcInput, char *pcOutput);
static void FlashPrepareChunkSetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut);
static char *FlashPrepareChunkParseSectionNum(char *pcInput, char *pcOutput);
static char *FlashPrepareChunkParseUtilBuffAddress(char *pcInput, char *pcOutput);
static char *FlashPrepareChunkParseUtilBuffSize(char *pcIPBuff, char *pcOutput);
//Iported functions
extern unsigned char CheckSpace(int32_t iChar);//
extern int32_t XferChunkToTargetBuffer(int32_t sectionNum, uint32_t ulUtilBuff,uint32_t ulSize);

/****************************************************************************
     Function: FlashPrepareChunk    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
int32_t FlashPrepareChunk(char *pcInput, char *pcOutput)
{
   int32_t iError = 0;
   int32_t bRet = FALSE;
   char szBuffer[_MAX_PATH] = {'\0'};

   // Check whether already connected  
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

   FlashPrepareChunkInit(pcOutput);
   //Parse the command to get section number,address and size of Flash Utility Buffer
   iError = FlashPrepareChunkParseCommand(pcInput,pcOutput);
   if(iError != 0)
      {
      FlashPrepareChunkSetupErrorMessage(iError, tyFlashPrepChunkProcData.szErrorBuffer, pcOutput);
      return FALSE;
      }
   //Prepare the next chunk for the section number
   //i.e. Get the index of the next chunk from the buffer for the section and transfer it to utility buffer
   if((TRUE == tyFlashPrepChunkProcData.bUtilBuffSizeValid) && 
      (TRUE == tyFlashPrepChunkProcData.bUtilBuffValid) &&
      (TRUE == tyFlashPrepChunkProcData.bSecNumValid))
      {
      bRet = XferChunkToTargetBuffer((int32_t)tyFlashPrepChunkProcData.ulSectionNum,
                                     tyFlashPrepChunkProcData.ulUtilBuff,
                                     tyFlashPrepChunkProcData.ulUtilBuffSize);
      }
   if(TRUE == bRet)
      {
      /* If in script mode, dump this to monitor.out */
      if(tyFlashPrepChunkProcData.iScriptMode)
         {
         char szReplyCode[MAX_SCRIPT_MODE] = "\0";
         sprintf(szReplyCode, "%d", 1);
         if(FALSE==WriteMonOut(PREPARE_CHUNK, szReplyCode, pcOutput))
            {
            return FALSE;
            }
         AddStringToOutAsciiz(".", pcOutput);
         }
      else
         {
         sprintf(tyFlashPrepChunkProcData.szErrorBuffer, "OK \n");
         //PrintMessage(INFO_MESSAGE, tyElfToSectionProcData.szErrorBuffer);
         AddStringToOutAsciiz(tyFlashPrepChunkProcData.szErrorBuffer, pcOutput);
         }
      }
   else
      {
      /* If in script mode, dump this to monitor.out */
      if(tyFlashPrepChunkProcData.iScriptMode)
         {
         char szReplyCode[MAX_SCRIPT_MODE] = "\0";
         sprintf(szReplyCode, "%d", 0);
         if(FALSE==WriteMonOut(PREPARE_CHUNK, szReplyCode, pcOutput))
            {
            return 0;
            }
         AddStringToOutAsciiz(".", pcOutput);
         }
      else
         {
         sprintf(tyFlashPrepChunkProcData.szErrorBuffer, "flashPrepareChunk failed secno %d utilbuf %x chnksize%d\n",
                 ((int32_t)tyFlashPrepChunkProcData.ulSectionNum),
                 ((int32_t)tyFlashPrepChunkProcData.ulUtilBuff),
                 ((int32_t)tyFlashPrepChunkProcData.ulUtilBuffSize));
         //PrintMessage(INFO_MESSAGE, tyElfToSectionProcData.szErrorBuffer);
         AddStringToOutAsciiz(tyFlashPrepChunkProcData.szErrorBuffer, pcOutput);
         }
      }
   return 1;
}

/****************************************************************************
     Function: FlashPrepareChunkParseCommand    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
static int32_t FlashPrepareChunkParseCommand(char *pcInput, char *pcOutput)
{
   char *pcIndex = pcInput;
   char *pcRet = NULL;

   assert(pcInput != NULL);
   if(*pcInput == '\0')
      return ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND;
   /* if(*pcInput != ' ')
         return ERR_NOT_FLASHPREPCHUNK_COMMAND;*/
   if(strlen(pcInput) > (_MAX_PATH* 2)) //Including 0x and "" if present
      return ERR_COMMAND_TOO_LONG;
   //parse the Section number
   pcRet = FlashPrepareChunkParseSectionNum(pcIndex,pcOutput);
   if((NULL != pcRet)&&((char *)ERR_INVALID_ADDRESS != pcRet))
      {
      pcIndex = pcRet;
      }
   else
      {
      return ERR_INVALID_ADDRESS;
      }
   pcRet = NULL;
   //Parse the utility buffer address
   pcRet = FlashPrepareChunkParseUtilBuffAddress(pcIndex,pcOutput);
   if((NULL != pcRet)&&((char *)ERR_INVALID_CHUNKSIZE != pcRet))
      {
      pcIndex = pcRet;
      }
   else
      {
      return ERR_INVALID_CHUNKSIZE;
      }
   pcRet = NULL;
   //Parse the utility buffer size
   pcRet = FlashPrepareChunkParseUtilBuffSize(pcIndex,pcOutput);
   if((NULL != pcRet)&&((char *)ERR_INVALID_OFFSET != pcRet))
      {
      pcIndex = pcRet;
      }
   else
      {
      return ERR_INVALID_OFFSET;
      }
   /* to check whther the command is called from GDB script */
   if(isInScriptMode(pcIndex))
      {
      tyFlashPrepChunkProcData.iScriptMode = TRUE;
      if(InitScriptOut(PREPARE_CHUNK, pcOutput))
         {
         return ERR_INVALID_FILE_NAME;
         }

      }

   return 0;
}

/****************************************************************************
     Function: FlashPrepareChunkParseSectionNum    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
*****************************************************************************/
static char *FlashPrepareChunkParseSectionNum(char *pcInput, char *pcOutput)
{
   unsigned char bParseUnitEnd = 0;
   unsigned char bParseUnitInsideQuotationMarks = 0;
   char pucPrepChunkCmdParseBuffer[MAX_PREPCHUNK_CMD_LENGTH];
   int32_t iCounter = 0;
   int32_t bValid =0;
   char * pcIndex = pcInput;
   char *pcBuffer = pucPrepChunkCmdParseBuffer;

   assert(pcInput != NULL);
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_SECTIONNUM;
   /* if (*pcIndex != ' ')
        return(char *)ERR_INVALID_SECTIONNUM;*/
   if(strlen(pcInput) > (_MAX_PATH * 2)) //Including 0x and "" if present
      return(char *)ERR_INVALID_SECTIONNUM;
   /*Processing start for chunk size i/p*/
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)0;
   //if the input stream has quotation mark,user entered size in quotes 
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   //adjust pcIndex to pass the quotation mark
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;
   memset(pucPrepChunkCmdParseBuffer,0,MAX_PREPCHUNK_CMD_LENGTH);
   while(!bParseUnitEnd && *pcIndex != '\0')
      {
      //Checks if number of digits exceeds 10
      if(iCounter > 2*MAX_SIZE_BYTE_LEN)
         {
         sprintf(tyFlashPrepChunkProcData.szErrorBuffer,"Flash Section number Invalid");
         return(char *)ERR_INVALID_SECTIONNUM;
         }
      *pcBuffer++ = *pcIndex++;
      bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_CHUNKSIZE;     // Invalid chunk size
      }
   else
      {
      if(bParseUnitInsideQuotationMarks)
         pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
      }
   if((FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,HEX,2*MAX_ADDR_BYTE_LEN)) &&
      (FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,DEC,2*MAX_ADDR_BYTE_LEN)) )
      {
      tyFlashPrepChunkProcData.bSecNumValid = FALSE;
      FlashPrepareChunkSetupErrorMessage(ERR_SECTIONNUM_TOO_LONG,pcIndex, pcOutput);
      return(char *)ERR_INVALID_SECTIONNUM;      
      }
   else
      {
      *pcBuffer = '\0';
      tyFlashPrepChunkProcData.ulSectionNum = StrToUlong(pucPrepChunkCmdParseBuffer,&bValid);
      if(!bValid)
         {
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return(char *)1;
         }
      else
         {
         tyFlashPrepChunkProcData.bSecNumValid = TRUE;
         }
      }   
   return pcIndex;
}

/****************************************************************************
     Function: FlashPrepareChunkParseUtilBuffAddress    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
*****************************************************************************/
static char *FlashPrepareChunkParseUtilBuffAddress(char *pcInput, char *pcOutput)
{
   unsigned char bParseUnitEnd = 0;
   unsigned char bParseUnitInsideQuotationMarks = 0;
   char pucPrepChunkCmdParseBuffer[MAX_PREPCHUNK_CMD_LENGTH];
   int32_t iCounter = 0;
   int32_t bValid =0;
   char * pcIndex = pcInput;
   char *pcBuffer = pucPrepChunkCmdParseBuffer;

   assert(pcInput != NULL);
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_SECTIONNUM;
   if(*pcIndex != ' ')
      return(char *)ERR_INVALID_SECTIONNUM;
   if(strlen(pcInput) > (_MAX_PATH * 2)) //Including 0x and "" if present
      return(char *)ERR_INVALID_SECTIONNUM;
   /*Processing start for chunk size i/p*/
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)0;
   //if the input stream has quotation mark,user entered size in quotes 
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   //adjust pcIndex to pass the quotation mark
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;
   memset(pucPrepChunkCmdParseBuffer,0,MAX_PREPCHUNK_CMD_LENGTH);
   while(!bParseUnitEnd && *pcIndex != '\0')
      {
      //Checks if number of digits exceeds 10
      if(iCounter > 2*MAX_SIZE_BYTE_LEN)
         {
         sprintf(tyFlashPrepChunkProcData.szErrorBuffer,"Flash Section number Invalid");
         return(char *)ERR_INVALID_SECTIONNUM;
         }
      *pcBuffer++ = *pcIndex++;
      bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_CHUNKSIZE;     // Invalid chunk size
      }
   else
      {
      if(bParseUnitInsideQuotationMarks)
         pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
      }
   if((FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,HEX,2*MAX_ADDR_BYTE_LEN)) &&
      (FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,DEC,2*MAX_ADDR_BYTE_LEN)) )
      {
      tyFlashPrepChunkProcData.bUtilBuffValid = FALSE;
      FlashPrepareChunkSetupErrorMessage(ERR_ADDRESS_TOO_LONG,pcIndex, pcOutput);
      return(char *)ERR_INVALID_ADDRESS;      
      }
   else
      {
      *pcBuffer = '\0';
      tyFlashPrepChunkProcData.ulUtilBuff = StrToUlong(pucPrepChunkCmdParseBuffer,&bValid);
      if(!bValid)
         {
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return(char *)1;
         }
      else
         {
         tyFlashPrepChunkProcData.bUtilBuffValid = TRUE;
         }
      }   
   return pcIndex;
}

/****************************************************************************
     Function: FlashPrepareChunkParseUtilBuffSize    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
*****************************************************************************/
static char *FlashPrepareChunkParseUtilBuffSize(char *pcIPBuff, char *pcOutput)
{
   unsigned char bParseUnitEnd = 0;
   unsigned char bParseUnitInsideQuotationMarks = 0;
   char pucPrepChunkCmdParseBuffer[MAX_PREPCHUNK_CMD_LENGTH];
   int32_t iCounter = 0;
   int32_t bValid =0;
   char * pcIndex = pcIPBuff;
   char *pcBuffer = pucPrepChunkCmdParseBuffer;

   assert(pcIPBuff != NULL);
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_CHUNKSIZE;
   if(*pcIndex != ' ')
      return(char *)ERR_INVALID_CHUNKSIZE;
   if(strlen(pcIPBuff) > (_MAX_PATH * 2)) //Including 0x and "" if present
      return(char *)ERR_INVALID_CHUNKSIZE;
   /*Processing start for chunk size i/p*/
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)0;
   //if the iput stream has quotation mark,user entered size in quotes 
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   //adjust pcIndex to pass he quotation mark
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;
   memset(pucPrepChunkCmdParseBuffer,0,MAX_PREPCHUNK_CMD_LENGTH);
   while(!bParseUnitEnd && *pcIndex != '\0')
      {
      if(iCounter > 2*MAX_SIZE_BYTE_LEN)
         {
         sprintf(tyFlashPrepChunkProcData.szErrorBuffer,"Flash chunk size");
         return(char *)ERR_INVALID_CHUNKSIZE;
         }
      *pcBuffer++ = *pcIndex++;
      bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_CHUNKSIZE;     // Invalid chunk size
      }
   else
      {
      if(bParseUnitInsideQuotationMarks)
         pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
      }
   if(FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,HEX,2*MAX_ADDR_BYTE_LEN))
      {
      tyFlashPrepChunkProcData.bUtilBuffSizeValid = FALSE;
      FlashPrepareChunkSetupErrorMessage(ERR_ADDRESS_TOO_LONG,pcIndex, pcOutput);
      return(char *)ERR_INVALID_ADDRESS;
      }
   else
      {
      *pcBuffer = '\0';
      tyFlashPrepChunkProcData.ulUtilBuffSize = StrToUlong(pucPrepChunkCmdParseBuffer,&bValid);
      if(!bValid)
         {
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return(char *)1;
         }
      else
         {
         tyFlashPrepChunkProcData.bUtilBuffSizeValid = TRUE;
         }
      }   
   return pcIndex;
}

/****************************************************************************
     Function: FlashPrepareChunkInit
     Engineer: Sudeep R K
        Input: char *pcGDBOutput - output string  
       Output: none
  Description: Clear and initialize data before ashload command execution.
Date           Initials    Description
10-Feb-2010    SRK          Initial & cleanup
*****************************************************************************/
static void FlashPrepareChunkInit(char *pcGDBOutput)
{
   tyFlashPrepChunkProcData.pcGDBReplyOutput = pcGDBOutput;
   memset(&tyFlashPrepChunkProcData,0,sizeof(tyFlashPrepChunkProcData));
}

/****************************************************************************
     Function: FlashPrepareChunkSetupErrorMessage
     Engineer: Sudeep R K
        Input: int32_t iError : specifies error number for download
               char *pszFileName : pointer to download file name
               char *pcOut : hexadecimal string to be returned to the GDB
       Output: none
  Description: Prints out appropiate error message to GDB server console
Date           Initials    Description
10-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
static void FlashPrepareChunkSetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut)
{
   char *szErrorMsg = tyFlashPrepChunkProcData.szErrorBuffer;
   NOREF(pszFileName);
   switch(iError)
      {
      
      case ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_NOT_FLASHPREPCHUNK_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_FLASHPREPCHUNK_COMMAND);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_COMMAND_TOO_LONG :
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_FILE_NAME:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_FILE_NAME);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      default:
         sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_ERROR_OCURRED);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      }
   strcat(szErrorMsg, "\n");
   AddStringToOutAsciiz(GDB_CLIENT_ERROR_MESSAGE_PREFIX, pcOut);
   AddStringToOutAsciiz(szErrorMsg, pcOut);
}
#endif // FLASH_SUPPORT
