/****************************************************************************
       Module: gdbFlashGetSectionSize.c
            Engineer: Sudeep RK
  Description: Ashling GDB Server, FLASH program
Date           Initials    Description
21-Dec-2009    SRK          Initial & cleanup
22-Feb-2010    SVL        Modified for scripting support
28-Mar-2010    DK         Modifed for MIPS Flash support
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>

#ifdef FLASH_SUPPORT
   #include "../flashutils/gdbflashcommonutils.h"
   #include "../flashutils/gdbflashcommonerrors.h"
   #include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
   #include "../../../gdbglob.h"
   #include "../../../gdbxml.h"
   #include "../../../gdbty.h"
   #include "../../../gdbutil.h"
   #include "../../../gdberr.h"
   #include "../../../elf.h"

typedef struct _GetSizeOfSectionProcessData 
   {
   //char* pcGDBReplyOutput;              //The pointer to output reply of "FlashPrepareChunk" to GDB
   char szErrorBuffer[FLASH_SIZEOFSECTION_MESSAGE_BUFFER_SIZE];
   uint32_t ulSectionNum;
   int32_t secNumValid;
   /* to indicate whether the command is initiated from Script */
   int32_t iScriptMode;
   } GetSizeOfSectionProcessData;

typedef struct _GetOffsetOfSectionProcessData 
   {
   //char* pcGDBReplyOutput;              //The pointer to output reply of "FlashPrepareChunk" to GDB
   char szErrorBuffer[FLASH_OFFSETOFSECTION_MESSAGE_BUFFER_SIZE];
   uint32_t ulSectionNum;
   int32_t secNumValid;
   /* to indicate whether the command is initiated from Script */
   int32_t iScriptMode;
   } GetOfsetOfSectionProcessData;
//Global variables imported
extern TyServerInfo tySI;
//Global variable
GetSizeOfSectionProcessData tyFlashSizeOfSectionProcData;
GetOfsetOfSectionProcessData tyFlashOffsetOfSectionProcData;
//Imported functions
extern unsigned char CheckSpace(int32_t iChar);
extern int32_t GetSizeOfSection(int32_t sectionNum);
extern uint32_t GetOffsetOfSection(int32_t sectionNum);

// local functions
static void FlashSizeOfSectionErrorMessage(int32_t iError, char *pszFileName, char *pcOut);
static char *FlashSizeOfSectionParseCommand(char *pcInput, char *pcOutput);
static char *FlashOffsetOfSectionParseCommand(char *pcInput, char *pcOutput);
static void FlashSizeOfSectionInit(char *pcGDBOutput);
static void FlashOffsetOfSectionInit(char *pcGDBOutput);

/****************************************************************************
     Function: FlashGetSizeOfSection    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
int32_t FlashGetSizeOfSection(char *pcInput, char *pcOutput)
{
   char *pcRet = NULL;
   int32_t iError = 0;
   int32_t iSectionNum = 0;
   char szBuffer[_MAX_PATH] = {'\0'};

   // Check whether already connected  
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

   FlashSizeOfSectionInit(pcOutput);
   //Parse the command to get section number
   pcRet = FlashSizeOfSectionParseCommand(pcInput,pcOutput);
   if((NULL == pcRet)||((char *)ERR_INVALID_SECTIONNUM == pcRet))
      {
      FlashSizeOfSectionErrorMessage(iError, tyFlashSizeOfSectionProcData.szErrorBuffer, pcOutput);
      return FALSE;
      }
   //Get the size of the section from the global buffer
   if(TRUE == tyFlashSizeOfSectionProcData.secNumValid)
      {
      iSectionNum = (int32_t)GetSizeOfSection((int32_t)tyFlashSizeOfSectionProcData.ulSectionNum);
      sprintf(tyFlashSizeOfSectionProcData.szErrorBuffer,"%d\n",iSectionNum);
      /* If in script mode, dump this to monitor.out */
      if(tyFlashSizeOfSectionProcData.iScriptMode)
         {
         if(FALSE==WriteMonOut(GET_SIZE_OF_SECT, tyFlashSizeOfSectionProcData.szErrorBuffer, pcOutput))
            {
            return FALSE;
            }
         AddStringToOutAsciiz(".", pcOutput);
         }
      else
         {
         /* send to GDB console */
         AddStringToOutAsciiz(tyFlashSizeOfSectionProcData.szErrorBuffer, pcOutput);
         }
      }
   else
      {
      /* If in script mode, dump this to monitor.out */
      if(tyFlashSizeOfSectionProcData.iScriptMode)
         {
         sprintf(tyFlashSizeOfSectionProcData.szErrorBuffer,"%d\n", 0);
         if(FALSE==WriteMonOut(GET_SIZE_OF_SECT, tyFlashSizeOfSectionProcData.szErrorBuffer, pcOutput))
            {
            return FALSE;
            }
         AddStringToOutAsciiz(" ", pcOutput);
         }
      else
         {
         /* send to GDB console */
         sprintf(tyFlashSizeOfSectionProcData.szErrorBuffer,"GetSizeOfSection failed\n");
         AddStringToOutAsciiz(tyFlashSizeOfSectionProcData.szErrorBuffer, pcOutput);
         }
      }   
   return TRUE;
}
/****************************************************************************
     Function: FlashSizeOfSectionParseCommand
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Parses the sectionNum
               specified
Date           Initials    Description
21-Dec-2009    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
static char *FlashSizeOfSectionParseCommand(char *pcInput, char *pcOutput)
{
   unsigned char bParseUnitEnd = 0;
   unsigned char bParseUnitInsideQuotationMarks = 0;
   char pucPrepChunkCmdParseBuffer[MAX_SIZEOFSECTION_CMD_LENGTH];
   int32_t iCounter = 0;
   int32_t bValid =0;
   char *pcIndex = pcInput;
   char *pcBuffer = pucPrepChunkCmdParseBuffer;

   assert(pcInput != NULL);
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_SECTIONNUM;
   if(*pcIndex != ' ')
      return(char *)ERR_INVALID_SECTIONNUM;
   if(strlen(pcInput) > (_MAX_PATH * 2)) //Including 0x and "" if present
      return(char *)ERR_INVALID_SECTIONNUM;
   /*Processing start for chunk size i/p*/
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)0;
   //if the input stream has quotation mark,user entered size in quotes 
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   //adjust pcIndex to pass the quotation mark
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;
   memset(pucPrepChunkCmdParseBuffer,0,MAX_SIZEOFSECTION_CMD_LENGTH);
   while(!bParseUnitEnd && *pcIndex != '\0')
      {
      //Checks if number of digits exceeds 10
      if(iCounter > 2*MAX_SECTIONNUM_BYTE_LEN)
         {
         //sprintf(tyFlashSizeOfSectionProcData.szErrorBuffer,"Flash Section number Invalid");
         return(char *)ERR_INVALID_SECTIONNUM;
         }
      *pcBuffer++ = *pcIndex++;
      bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_SECTIONNUM;     // Invalid section number
      }
   else
      {
      if(bParseUnitInsideQuotationMarks)
         pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
      }

   /* to check whther the command is called from GDB script */
   if(isInScriptMode(pcIndex))
      {
      //printf("GetSize - script mode ON\n");
      tyFlashSizeOfSectionProcData.iScriptMode = TRUE;
      if(InitScriptOut(GET_SIZE_OF_SECT,pcOutput))
         {
         return(char*)1;
         }
      }

   if((FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,HEX,2*MAX_SECTIONNUM_BYTE_LEN)) &&
      (FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,DEC,2*MAX_SECTIONNUM_BYTE_LEN)) )
      {
      tyFlashSizeOfSectionProcData.secNumValid = FALSE;
      //FlashSizeOfSectionErrorMessage(ERR_SECTIONNUM_TOO_LONG,pcIndex, pcOutput);
      return(char *)ERR_INVALID_SECTIONNUM;      
      }
   else
      {
      *pcBuffer = '\0';
      tyFlashSizeOfSectionProcData.ulSectionNum = StrToUlong(pucPrepChunkCmdParseBuffer,&bValid);
      if(!bValid)
         {
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return(char *)ERR_INVALID_SECTIONNUM;
         }
      else
         {
         tyFlashSizeOfSectionProcData.secNumValid = TRUE;
         }
      }   
   return pcIndex;
}

/****************************************************************************
     Function: FlashSizeOfSectionInit
     Engineer: Sudeep R K
        Input: char *pcGDBOutput - output string  
       Output: none
  Description: Clear and initialize data before flashBin command execution.
Date           Initials    Description
21-Dec-2009    SRK          Initial & cleanup
*****************************************************************************/
static void FlashSizeOfSectionInit(char *pcGDBOutput)
{
   NOREF(pcGDBOutput);
   //tyFlashBinProcData.pcGDBReplyOutput = pcGDBOutput;
   memset(&tyFlashSizeOfSectionProcData,0,sizeof(tyFlashSizeOfSectionProcData));
}

/**************************************************************************** 
     Function: FlashSizeOfSectionErrorMessage
     Engineer: Sudeep.RK
        Input: iError - I/p error type
               pszFileName - O/p bin file
               pcOut - 
       Output: Nil
  Description: Prints error message
Date           Initials    Description
02-Nov-2009    SRK          Initial & cleanup
*****************************************************************************/ 
static void FlashSizeOfSectionErrorMessage(int32_t iError, char *pszFileName, char *pcOut)
{
   char *szErrorMsg = tyFlashSizeOfSectionProcData.szErrorBuffer;
   NOREF(pszFileName);
   switch(iError)
      {
      case ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_NOT_SIZEOFSECTION_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_FLASHBIN_COMMAND);
         PrintMessage(ERROR_MESSAGE,szErrorMsg);
         break;
      case ERR_COMMAND_TOO_LONG :
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_FILE_NAME:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_FILE_NAME);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      default:
         sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_ERROR_OCURRED);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      }
   strcat(szErrorMsg, "\n");
   AddStringToOutAsciiz(GDB_CLIENT_ERROR_MESSAGE_PREFIX, pcOut);
   AddStringToOutAsciiz(szErrorMsg, pcOut);
}

/****************************************************************************
     Function: FlashGetOffsetOfSection    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Returns the offset of section in Flash, i.e the physical addr 
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
int32_t FlashGetOffsetOfSection(char *pcInput, char *pcOutput)
{
   char *pcRet = NULL;
   int32_t iError = 0;
   int32_t iSecOffsetAddr = 0;
   char szBuffer[_MAX_PATH] = {'\0'};
   // Check whether already connected  
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

   FlashOffsetOfSectionInit(pcOutput);
   //Parse the command to get section number
   pcRet = FlashOffsetOfSectionParseCommand(pcInput,pcOutput);
   if((NULL == pcRet)||((char *)ERR_INVALID_SECTIONNUM == pcRet))
      {
      FlashSizeOfSectionErrorMessage(iError, tyFlashOffsetOfSectionProcData.szErrorBuffer, pcOutput);
      return FALSE;
      }
   //Get the size of the section from the global buffer
   if(TRUE == (int32_t)tyFlashOffsetOfSectionProcData.secNumValid)
      {
      iSecOffsetAddr = (int32_t)GetOffsetOfSection((int32_t)tyFlashOffsetOfSectionProcData.ulSectionNum);
      sprintf(tyFlashOffsetOfSectionProcData.szErrorBuffer,"0x%x\n",(int32_t)iSecOffsetAddr);
      /* If in script mode, dump to monitor.out */
      if(tyFlashOffsetOfSectionProcData.iScriptMode)
         {
         if(FALSE==WriteMonOut(GET_OFFSET_OF_SECT, tyFlashOffsetOfSectionProcData.szErrorBuffer, pcOutput))
            {
            return FALSE;
            }
         AddStringToOutAsciiz(".", pcOutput);
         }
      else
         {
         /* send to GDB console */
         AddStringToOutAsciiz(tyFlashOffsetOfSectionProcData.szErrorBuffer, pcOutput);
         }
      }
   else
      {
      /* If in script mode, dump to monitor.out */
      if(tyFlashOffsetOfSectionProcData.iScriptMode)
         {
         sprintf(tyFlashOffsetOfSectionProcData.szErrorBuffer,"%d\n", 0);
         if(FALSE==WriteMonOut(GET_OFFSET_OF_SECT, tyFlashOffsetOfSectionProcData.szErrorBuffer, pcOutput))
            {
            return FALSE;
            }
         AddStringToOutAsciiz(" ", pcOutput);
         }
      else
         {
         sprintf(tyFlashOffsetOfSectionProcData.szErrorBuffer,"GetOffsetOfSection failed\n");
         /* send to GDB console */
         AddStringToOutAsciiz(tyFlashOffsetOfSectionProcData.szErrorBuffer, pcOutput);
         }

      }   
   return TRUE;
}
/****************************************************************************
     Function: FlashOffsetOfSectionParseCommand
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Parses the sectionNum
               specified
Date           Initials    Description
21-Dec-2009    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
static char *FlashOffsetOfSectionParseCommand(char *pcInput, char *pcOutput)
{
   unsigned char bParseUnitEnd = 0;
   unsigned char bParseUnitInsideQuotationMarks = 0;
   char pucGetOffsetCmdParseBuffer[MAX_OFFSETOFSECTION_CMD_LENGTH];
   int32_t iCounter = 0;
   int32_t bValid =0;
   char *pcIndex = pcInput;
   char *pcBuffer = pucGetOffsetCmdParseBuffer;

   assert(pcInput != NULL);
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_SECTIONNUM;
   if(*pcIndex != ' ')
      return(char *)ERR_INVALID_SECTIONNUM;
   if(strlen(pcInput) > (_MAX_PATH * 2)) //Including 0x and "" if present
      return(char *)ERR_INVALID_SECTIONNUM;
   /*Processing start for chunk size i/p*/
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)0;
   //if the input stream has quotation mark,user entered size in quotes 
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   //adjust pcIndex to pass the quotation mark
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;
   memset(pucGetOffsetCmdParseBuffer,0,MAX_SIZEOFSECTION_CMD_LENGTH);
   while(!bParseUnitEnd && *pcIndex != '\0')
      {
      //Checks if number of digits exceeds 10
      if(iCounter > 2*MAX_SECTIONNUM_BYTE_LEN)
         {
         sprintf(tyFlashSizeOfSectionProcData.szErrorBuffer,"Flash Section number Invalid");
         return(char *)ERR_INVALID_SECTIONNUM;
         }
      *pcBuffer++ = *pcIndex++;
      bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_SECTIONNUM;     // Invalid section number
      }
   else
      {
      if(bParseUnitInsideQuotationMarks)
         pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
      }

   /* to check whther the command is called from GDB script */
   if(isInScriptMode(pcIndex))
      {
      //printf("OffsetSect - script mode ON\n");
      tyFlashOffsetOfSectionProcData.iScriptMode = TRUE;
      if(InitScriptOut(GET_OFFSET_OF_SECT,pcOutput))
         {
         return(char*)1;
         }
      }

   if((FALSE == ValidateStr(pucGetOffsetCmdParseBuffer,HEX,2*MAX_SECTIONNUM_BYTE_LEN)) &&
      (FALSE == ValidateStr(pucGetOffsetCmdParseBuffer,DEC,2*MAX_SECTIONNUM_BYTE_LEN)) )
      {
      tyFlashOffsetOfSectionProcData.secNumValid = FALSE;
      FlashSizeOfSectionErrorMessage(ERR_SECTIONNUM_TOO_LONG,pcIndex, pcOutput);
      return(char *)ERR_INVALID_SECTIONNUM;      
      }
   else
      {
      *pcBuffer = '\0';
      tyFlashOffsetOfSectionProcData.ulSectionNum = StrToUlong(pucGetOffsetCmdParseBuffer,&bValid);
      if(!bValid)
         {
         sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
         PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
         AddStringToOutAsciiz(tySI.szError, pcOutput);
         return(char *)1;
         }
      else
         {
         tyFlashOffsetOfSectionProcData.secNumValid = TRUE;
         }
      }   
   return pcIndex;
}
/****************************************************************************
     Function: FlashOffsetOfSectionInit
     Engineer: Sudeep R K
        Input: char *pcGDBOutput - output string  
       Output: none
  Description: Clear and initialize data before flashBin command execution.
Date           Initials    Description
21-Dec-2009    SRK          Initial & cleanup
*****************************************************************************/
static void FlashOffsetOfSectionInit(char *pcGDBOutput)
{
   NOREF(pcGDBOutput);
   //tyFlashBinProcData.pcGDBReplyOutput = pcGDBOutput;
   memset(&tyFlashOffsetOfSectionProcData,0,sizeof(tyFlashOffsetOfSectionProcData));
}

#endif //#ifdef FLASH_SUPPORT
