
/****************************************************************************
       Module: gdbFlashProcessInputFile.c
     Engineer: Sudeep RK
  Description: Ashling GDB Server, FLASH program
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
22-Feb-2010    SVL          Modified for scripting support
22-Feb-2010    SRK          Modified flash verification logic
11-Mar-2010	   SVL          Added validity checking for image formats
22-Mar-2010    VK           Modified for Lint based building for MIPS
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>
#ifdef FLASH_SUPPORT
   #include "../flashutils/gdbflashcommonerrors.h"
   #include "../../../gdbglob.h"
   #include "gdbserver/gdbxml.h"
   #include "../../../gdbty.h"
   #include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
   #include "gdbserver/gdbutil.h"
   #include "gdbserver/gdberr.h"

//#include "../../../elf.h"
   #ifdef ARM
      #include "gdbserver/arm/gdbarm.h"
   #endif
   #include "../flashutils/gdbflashcommonutils.h"
   #include "gdbserver/moncommand/ashload/gdbashloadutils.h"
   #include "gdbserver/moncommand/ashload/gdbashloadcommon.h"

   #define MAX_PARSE_ADDRESS_LENGTH 10
//Global varaibles
extern TyServerInfo tySI;
extern ElfProcessData tyELFProcData;
   #ifdef MIPS
extern int32_t bIsMicroChip ;
   #endif

//Process data for elftosection command(Exported)
FileProcessData tyProcInFileData;

//Imported functions
extern int32_t VerifyFileFormat(FILE *pFile);
extern int32_t ProcessELFHeader(void);
extern int32_t ProcessELFProgramHeaderTable(int32_t Mode);
extern unsigned char CheckSpace(int32_t iChar);
extern int32_t GetNumSectionsInBuff(void);
extern int32_t FlashSrecProcessRecords(void);
extern int32_t FlashIHexProcessRecords(void);
extern int32_t FlashBinaryProcessData(void);

extern void InitAndClearBufferMgr(void);
extern void InitialiseSRec(void);
extern void InitialiseHex(void);
//Local functions;
static void ProcInFileInit(char *pcGDBOutput);
static int32_t ProcInFileParseCommand(char *pcInput, char *pcOutput);
static int32_t ProcInFileProcessDwarfFile(void);
static void ProcInFileSetupErrorMessage(int32_t iError,
                                        char *pszFileName, 
                                        char *pcOut);
//Exported functions
//Set buffer valid
void SetBufferValid(void);

static char *ParseFileName(char *pcInput,char *pcOutput);
static char *ParseFileFormat(char *pcInput,char *pcOutput);
static char *ParseAddress(char *pcInput, uint32_t *pulOutput);
static unsigned char checkSlash(int32_t iChar);
   #ifdef MIPS
extern int32_t ProcessELFSectionHeaderTable(int32_t iMode);
   #endif
/****************************************************************************
     Function: FlashProcInFile
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
               Argument format: <IP FILE NAME> <debug file format> <flash start address>
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Converts the elf file to sections and stores in the global buffer.
                Also keeps the section info in a global data structure. It is 
                invoked as part of a monitor command
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL          Modified for scripting support
11-Mar-2010	   SVL          Added validity checking for image formats
*****************************************************************************/
int32_t FlashProcInFile(char *pcInput, char *pcOutput)
{
   int32_t iError = 0;
   char szBuffer[_MAX_PATH] = {'\0'};

   // Check whether already connected  
   if(!tySI.bIsConnected)
      {
      sprintf(szBuffer,"Not connected to target\n");
      AddStringToOutAsciiz(szBuffer, pcOutput);
      return FALSE;
      }

   ProcInFileInit(pcOutput);
   //Parse the command to get elf name
   iError = ProcInFileParseCommand(pcInput,pcOutput);
   if(iError != 0)
      {
      ProcInFileSetupErrorMessage(iError,pcInput,pcOutput);      
      return 0;
      }
   if(!strcasecmp(tyProcInFileData.szFileFormat,"elf"))
      {
      //Process the ELF file
      iError = ProcInFileProcessDwarfFile();      
      }
   else if(!strcasecmp(tyProcInFileData.szFileFormat,"srec"))
      {
      //Initialise the SREC processing
      InitialiseSRec();
      //Prcess SREC file
      iError = FlashSrecProcessRecords();   
      }
   else if(!strcasecmp(tyProcInFileData.szFileFormat,"ihex"))
      {
      //Initialise IHex processing
      InitialiseHex();
      //Process IHEX file
      iError = FlashIHexProcessRecords();       
      }
   else if(!strcasecmp(tyProcInFileData.szFileFormat,"binary"))
      {
      //Process Binary file
      iError = FlashBinaryProcessData();       
      }
   else
      {
      sprintf(tyProcInFileData.szErrorBuffer, "FlashProcInFile failed unidentified file format\n");
      PrintMessage(INFO_MESSAGE, tyProcInFileData.szErrorBuffer);
      AddStringToOutAsciiz(tyProcInFileData.szErrorBuffer, pcOutput);
      return 0;
      }

   if(iError != 0)
      {
      ProcInFileSetupErrorMessage(iError,pcInput,pcOutput);  
      /* If in script mode, dump this to monitor.out */
      /* In script, we need to validate the image format. It expects 
      -1, if not an ELF
      -2, if not a HEX
      -3, if not a S-Record 
      -4, unrecognizable */
      if(tyProcInFileData.iScriptMode)
         {
         char szErrBuf[MAX_SCRIPT_MODE] = "\0";           

         switch(iError)
            {
            case ERR_NOT_AN_ELF_FILE:
               sprintf(szErrBuf, "%d", -1); 
               break;
            case ERR_INVALID_HEX_FORMAT:
            case ERR_INVALID_HEX_RECORD:
               sprintf(szErrBuf, "%d", -2); 
               break;
            case ERR_INVALID_SREC_FORMAT:
               sprintf(szErrBuf, "%d", -3); 
               break;
            default:
               sprintf(szErrBuf, "%d", -4);
            }

         if(FALSE==WriteMonOut(ELF_TO_SEC, szErrBuf, pcOutput))
            {
            return 0;
            }
         }
      return 0;
      }

   //Set the secion buff as valid
   SetBufferValid();

   /* If in script mode, dump this to monitor.out */
   if(tyProcInFileData.iScriptMode)
      {
      char szNumOfSections[MAX_SCRIPT_MODE] = "\0";
      sprintf(szNumOfSections, "%d", GetNumSectionsInBuff());

      //Write the number of sections back to GDB 
      sprintf(tyProcInFileData.szErrorBuffer, "<Status> \"Processing the image...\"\n");
      PrintMessage(INFO_MESSAGE, tyProcInFileData.szErrorBuffer);
      AddStringToOutAsciiz(tyProcInFileData.szErrorBuffer, pcOutput);

      if(FALSE==WriteMonOut(ELF_TO_SEC, szNumOfSections, pcOutput))
         {
         return 0;
         }
      }
   else
      {
      //Write the number of sections back to GDB 
      sprintf(tyProcInFileData.szErrorBuffer, "FlashProcInFile done %d\n",GetNumSectionsInBuff());
      PrintMessage(INFO_MESSAGE, tyProcInFileData.szErrorBuffer);
      AddStringToOutAsciiz(tyProcInFileData.szErrorBuffer, pcOutput);
      }
   return 1;
}

/****************************************************************************
     Function: PtrToSectionInit
     Engineer: Sudeep R K
        Input: char *pcGDBOutput - output string  
       Output: none
  Description: Initialise the buffer for storing section information 
  Date           Initials    Description
02-Feb-2008    SRK          Initial & cleanup
*****************************************************************************/
static void ProcInFileInit(char *pcGDBOutput)
{
   //memset(&tySectionBuffMgr,0,sizeof(tySectionBuffMgr));
   InitAndClearBufferMgr();
   memset(&tyProcInFileData,0,sizeof(tyProcInFileData));
   tyProcInFileData.pcGDBReplyOutput = pcGDBOutput;
}

/****************************************************************************
     Function: ProcInFileParseCommand
     Engineer: Sudeep R K 
        Input: char *pcInput  : input string
               char *pcOutput : output string
       Output: int32_t - error code
  Description: parse elftosections command
Date           Initials    Description
08-Feb-2010    SRK         Initial version
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
static int32_t ProcInFileParseCommand(char *pcInput, char *pcOutput)
{
   char *pcIndex = pcInput;
   char *pcRet = NULL;
   //char *pcBuffer = tyProcInFileData.szFileFormat;
   //parse First argument,file name
   pcRet = ParseFileName(pcIndex,tyProcInFileData.szFileName);
   if((NULL != pcRet)&&((char *)ERR_INVALID_FILE_NAME != pcRet))
      {
      pcIndex = pcRet;
      }
   else
      {
      return ERR_NOT_PROC_IN_FILE_COMMAND;
      }
   //parse file format
   pcRet = ParseFileFormat(pcIndex,tyProcInFileData.szFileFormat);
   if((NULL != pcRet)&&((char *)ERR_INVALID_FILE_FORMAT != pcRet))
      {
      pcIndex = pcRet;
      }
   else
      {
      return ERR_NOT_PROC_IN_FILE_COMMAND;
      }

   if(!strcasecmp(tyProcInFileData.szFileFormat,"binary"))
      {
      pcRet = ParseAddress(pcIndex,&tyProcInFileData.ulAddress);
      if((NULL != pcRet)&&((char *)ERR_INVALID_ADDRESS != pcRet))
         {
         pcIndex = pcRet;
         }
      else
         {
         return ERR_NOT_PROC_IN_FILE_COMMAND;
         }       
      }
   /* to check whther the command is called from GDB script */
   if(isInScriptMode(pcIndex))
      {
      printf("Elftosec - script mode ON\n");
      tyProcInFileData.iScriptMode = TRUE;
      if(InitScriptOut(ELF_TO_SEC, pcOutput))
         {
         return ERR_INVALID_FILE_NAME;
         }
      }

   PrintMessage(LOG_MESSAGE,"Input File name: %s.", tyProcInFileData.szFileName);   
   return 0;
}

/****************************************************************************
     Function: ParseAddress    
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Copies the .
                Also keeps the section info in a global data structure
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
*****************************************************************************/
static char *ParseAddress(char *pcInput, uint32_t *pulOutput)
{
   int32_t bParseUnitEnd = FALSE;
   int32_t bParseUnitInsideQuotationMarks = 0;
   int32_t iValid = FALSE;
   int32_t iCounter = 0;
   char *pcIndex = pcInput;
   char pucParseAddressBuffer[MAX_PARSE_ADDRESS_LENGTH+1];
   char *pcBuffer = pucParseAddressBuffer;   
   assert(pcInput != NULL);
   if(*pcInput == '\0')
      return(char *)ERR_INVALID_ADDRESS;
   if(*pcIndex != ' ')
      return(char *)ERR_INVALID_ADDRESS;
   if(strlen(pcInput) > (_MAX_PATH * 2))
      return(char *)ERR_INVALID_ADDRESS;
   memset(pucParseAddressBuffer,0,sizeof(pucParseAddressBuffer));
   /*Processing start for address*/
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_ADDRESS;;
   //if the input stream has quotation mark,user entered size in quotes 
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   //adjust pcIndex to pass the quotation mark
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;

   while(!bParseUnitEnd && *pcIndex != '\0')
      {
      //Checks if number of digits exceeds 10
      if(iCounter > 2*MAX_ADDRESS_BYTE_LEN)
         {
         return(char *)ERR_INVALID_ADDRESS;
         }
      *pcBuffer++ = *pcIndex++;
      bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_ADDRESS;     // Invalid address
      }
   else
      {
      if(bParseUnitInsideQuotationMarks)
         pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
      }
   if((FALSE == ValidateStr(pucParseAddressBuffer,HEX,2*MAX_ADDR_BYTE_LEN)) &&
      (FALSE == ValidateStr(pucParseAddressBuffer,DEC,2*MAX_ADDR_BYTE_LEN)) )
      {
      return(char *)ERR_INVALID_ADDRESS;      
      }
   else
      {
      if(iCounter <= MAX_ADDRESS_BYTE_LEN)
         *pcBuffer = '\0';
      *pulOutput = StrToUlong(pucParseAddressBuffer,&iValid);
      if(!iValid)
         {
         return(char *)ERR_INVALID_ADDRESS;
         }
      }   
   return pcIndex;
}
/****************************************************************************
     Function: ParseFileName
     Engineer: Sudeep R K 
        Input: char *pcInput  : input string
       Output: int32_t - error code
  Description: parse file name out of 'flashProcInFile' command
Date           Initials    Description
08-Feb-2010    SRK         Initial version
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
static char *ParseFileName(char *pcInput,char *pcOutput)
{
   //Input buffer
   int32_t iCounter = 0;
   char *pcIndex = pcInput;
   unsigned char bFileNameEnd = 0;
   unsigned char bFileInsideQuotationMarks = 0;
   //Output buffer
   char *pcBuffer = pcOutput;
   //char *pcBuffer = tyProcInFileData.szFileName;

   assert(pcInput != NULL);
   if(*pcInput == '\0')
      return(char *)ERR_INVALID_FILE_NAME;
   //Expect a space before the command
   if(*pcInput != ' ')
      return(char *)ERR_INVALID_FILE_NAME;
   //If the output buffer doesn't have enough space to store inout filename
   if(strlen(pcInput) > (_MAX_PATH * 2))
      return(char *)ERR_INVALID_FILE_NAME;
   /*Parse Input name*/
   // get rid off spaces
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_FILE_NAME;
   bFileInsideQuotationMarks = (*pcIndex == '"');
   if(bFileInsideQuotationMarks)
      pcIndex++;
   iCounter = 0;
   while(!bFileNameEnd && *pcIndex != '\0')
      {
      if(iCounter >= _MAX_PATH)
         {
         //ElfToSectionsSetupErrorMessage(ERR_FILE_PATH_TOO_LONG,pcInput, pcOutput);
         return(char *)ERR_INVALID_FILE_NAME;
         }
      *pcBuffer++ = *pcIndex++;
      bFileNameEnd = bFileInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bFileInsideQuotationMarks)
         return(char *)ERR_INVALID_FILE_NAME;                                  // only one " invalid file name
      }
   else
      {
      if(strlen(pcOutput) < _MAX_PATH)
         *pcBuffer = '\0';
      if(bFileInsideQuotationMarks)
         pcIndex++; //Update 'pcIndex' to cover the quotation mark end
      }
   return pcIndex;
}

/****************************************************************************
     Function: ParseFileFormat
     Engineer: Sudeep R K 
        Input: char *pcInput  : input string
       Output: int32_t - error code
  Description: parse file name out of 'flashProcInFile' command
Date           Initials    Description
08-Feb-2010    SRK         Initial version
19-Feb-2010    SVL        Modified for scripting support
*****************************************************************************/
static char *ParseFileFormat(char *pcInput,char *pcOutput)
{
   //Input buffer
   int32_t iCounter = 0;
   char *pcIndex = pcInput;
   unsigned char bFileNameEnd = 0;
   unsigned char bParseUnitInsideQuotationMarks = 0;
   //Output buffer
   //char *pcBuffer = tyProcInFileData.szFileFormat;
   char *pcBuffer = pcOutput;
   assert(pcInput != NULL);
   if(*pcInput == '\0')
      return(char *)ERR_INVALID_FILE_FORMAT;
   while(checkSlash(*pcIndex))
      pcIndex++;
   //Expect a space before the command
   if(*pcInput != ' ')
      return(char *)ERR_INVALID_FILE_FORMAT;
   if(strlen(pcInput) > (_MAX_PATH * 2))
      return(char *)ERR_INVALID_FILE_FORMAT;
   /*Parse Input name*/
   // get rid off spaces
   while(CheckSpace(*pcIndex))
      pcIndex++;
   if(*pcIndex == '\0')
      return(char *)ERR_INVALID_FILE_FORMAT;
   bParseUnitInsideQuotationMarks = (*pcIndex == '"');
   if(bParseUnitInsideQuotationMarks)
      pcIndex++;
   iCounter = 0;
   while(!bFileNameEnd && *pcIndex != '\0')
      {
      if(iCounter >= _MAX_PATH)
         {
         //ElfToSectionsSetupErrorMessage(ERR_FILE_PATH_TOO_LONG,pcInput, pcOutput);
         return(char *)ERR_INVALID_FILE_FORMAT;
         }
      *pcBuffer++ = *pcIndex++;
      bFileNameEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
      iCounter++;
      }
   if(*pcIndex  == '\0')
      {
      if(bParseUnitInsideQuotationMarks)
         return(char *)ERR_INVALID_FILE_FORMAT;                                  // only one " invalid file name
      }
   else
      {
      if(strlen(pcOutput) < _MAX_PATH)
         *pcBuffer = '\0';
      if(bParseUnitInsideQuotationMarks)
         pcIndex++; //Update 'pcIndex' to cover the quotation mark end
      }
   return pcIndex;
}

static unsigned char checkSlash(int32_t iChar)
{
   if((iChar == '\\'))
      return 1;                                                         // it is space or character between TAB and CR
   else
      return 0;
}
/**************************************************************************** 
     Function: ProcInFileProcessDwarfFile
     Engineer: Sudeep R K
        Input: none
       Output: int32_t - error code
  Description: Process DWARF file
Date           Initials    Description
11-MAR-2010    SRK          Initial & cleanup
23-May-2010    DK           Microchip Flash support
*****************************************************************************/ 
static int32_t ProcInFileProcessDwarfFile(void)
{
   int32_t iError = 0;
   // open file
   tyELFProcData.pInFile = fopen(tyProcInFileData.szFileName, "rb");
   if(tyELFProcData.pInFile == NULL)
      return ERR_F_NULL_FILE;
   // check file format
   iError = VerifyFileFormat(tyELFProcData.pInFile);
   // process ELF header
   if(!iError)
      iError = ProcessELFHeader();
   // process ELF section header table
   if(!iError)
      {
#ifdef MIPS
      //For MIPS microchip
      if( bIsMicroChip)
         {
         iError = ProcessELFSectionHeaderTable(FLASH_DOWNLOAD_ELF);
         // close file and return error code
         fclose(tyELFProcData.pInFile);
         return iError;
         }
#endif
      // read ELF header
      // process ELF header table if necessary
      iError = ProcessELFProgramHeaderTable(FLASH_DOWNLOAD_ELF);
      }
   // close file and return error code
   fclose(tyELFProcData.pInFile);
   return iError;
}

/****************************************************************************
     Function: ProcInFileSetupErrorMessage
     Engineer: Sudeep R K
        Input: int32_t iError : specifies error number for download
               char *pszFileName : pointer to download file name
               char *pcOut : hexadecimal string to be returned to the GDB
       Output: none
  Description: Prints out appropiate error message to GDB server console
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
11-Mar-2010	   SVL          Added validity checking for image formats
****************************************************************************/
static void ProcInFileSetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut)
{
   char *szErrorMsg = tyProcInFileData.szErrorBuffer;
   switch(iError)
      {
      case ERR_UNEXPECTED_END_OF_FILE:
         sprintf(szErrorMsg, MESSAGE_ERR_UNEXPECTED_END_OF_FILE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_F_NULL_FILE:
         sprintf(szErrorMsg, MESSAGE_ERR_F_NULL_FILE, tyProcInFileData.szFileName);
         PrintMessage(ERROR_MESSAGE,szErrorMsg);            
         break;
      case ERR_F_SEEK:
         sprintf(szErrorMsg, MESSAGE_ERR_F_SEEK);
         PrintMessage(ERROR_MESSAGE, szErrorMsg );
         break;
      case ERR_NOT_AN_ELF_FILE:
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_AN_ELF_FILE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_HEX_FORMAT:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_HEX_FORMAT);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_HEX_RECORD:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_HEX_RECORD);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_SREC_FORMAT:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_SREC_FORMAT);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_NOT_ABSOLUTE_REC_E :
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_ABSOLUTE_REC_E);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_PROCDWRF00 :
         sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF00);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INSUFFICENT_DATA_BUFFER_MEMORY :
         sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_DATA_BUFFER_MEMORY);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY :
         sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY);
         PrintMessage(ERROR_MESSAGE, szErrorMsg );
         break;
      case ERR_MEMORY_READ_FAILURE :
         sprintf(szErrorMsg, MESSAGE_ERR_MEMORY_READ_FAILURE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_MEMORY_VERIFY_FAILURE :
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_MEMORY_WRITE_FAILURE :
         sprintf(szErrorMsg, MESSAGE_ERR_MEMORY_WRITE_FAILURE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg );
         break;
      case ERR_INVALID_NUMBER_OF_PARMS :
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_NUMBER_OF_PARMS);
         PrintMessage(ERROR_MESSAGE, szErrorMsg,pszFileName);
         break;            
      case ERR_FILE_PATH_TOO_LONG :
         sprintf(szErrorMsg, MESSAGE_ERR_FILE_PATH_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_COMMAND_SWITCH_TOO_LONG:
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_SWITCH_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;            
      case ERR_COMMAND_SWITCH_USED_TWICE :
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_SWITCH_USED_TWICE);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_NOT_ASHLOAD_COMMAND:
         sprintf(szErrorMsg, MESSAGE_ERR_NOT_ASHLOAD_COMMAND);
         PrintMessage(ERROR_MESSAGE,szErrorMsg);
         break;
      case ERR_UNKNOWN_SWITCH :
         sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_SWITCH);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);            
         break;
      case ERR_COMMAND_TOO_LONG :
         sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_TOO_LONG);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_PROCDWRF07:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF07);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_PROCDWRF08:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF08);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF04:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF04);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_DUMMY_ALREADY_SHOWN:
         sprintf(szErrorMsg, MESSAGE_ERR_DUMMY_ALREADY_SHOWN);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF03:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF03);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_MEM_MALLOC_C:
         sprintf(szErrorMsg, MESSAGE_ERR_MEM_MALLOC_C);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;             
      case ERR_IE_READDWRF02:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF02);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF01:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF01 );
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_IE_READDWRF07:
         sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF07);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INVALID_FILE_NAME:
         sprintf(szErrorMsg, MESSAGE_ERR_INVALID_FILE_NAME);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY:
         sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      case GDBSERV_ERROR:
         sprintf(szErrorMsg, MESSAGE_GDBSERV_ERROR);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      default:
         sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_ERROR_OCURRED);
         PrintMessage(ERROR_MESSAGE, szErrorMsg);
         break;
      }
   strcat(szErrorMsg, "\n");
   //AddStringToOutAsciiz(GDB_CLIENT_ERROR_MESSAGE_PREFIX, pcOut);
   AddStringToOutAsciiz(szErrorMsg, pcOut);
}
#endif //FLASH_SUPPORT


