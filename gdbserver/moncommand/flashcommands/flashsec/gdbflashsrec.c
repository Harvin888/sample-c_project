/****************************************************************************
       Module: gdbFlashSrec.c
     Engineer: Sudeep RK
  Description: Ashling GDB Server, FLASH program
Date           Initials    Description
26-Feb-2010    SRK          Initial & cleanup
22-Mar-2010    VK          Modifed for Lint based building for MIPS
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "../flashutils/gdbflashcommonerrors.h"
#include "../../../gdbglob.h"
#include "gdbserver/gdberr.h"
#ifdef FLASH_SUPPORT
   #ifdef ARM
      #include "gdbserver/arm/gdbarm.h"
   #endif
   #include "../flashutils/gdbflashcommonutils.h"
   #include "gdbserver/gdbxml.h"
   #include "../../../gdbty.h"
   #include "gdbserver/gdbutil.h"
   #include "gdbserver/gdblow.h"

   #include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
   #include "gdbserver/moncommand/ashload/gdbashloaderrors.h"

   #define MAX_DATA_BYTES_IN_RECORD 300
   #
typedef uint32_t adtyp;

int32_t FlashSrecProcessRecords(void);
extern TyError BuildDataRecord(uint32_t      ulCodeStart, 
                               uint32_t      ulCodeEnd,
                               uint32_t      ulAddress,
                               uint32_t      ulCount,
                               unsigned char      *pucData);
extern TyError GetDataFromFileRecordSRec(unsigned char *pszFileBuffer,
                                         unsigned char *pszDataBuffer,
                                         unsigned char  *pbDataPresent, 
                                         adtyp          *pulDataAddr,
                                         adtyp          *pulDataCount);

//Global varaibles
extern FileProcessData tyProcInFileData;
   #ifdef FLASH_SUPPORT 
      #ifdef MIPS
typedef  unsigned char ubyte;//Unisigned byte
typedef  uint32_t uint;
typedef  uint32_t ulong;
      #endif
   #endif
   #define CARRIAGE_RETURN 0xA
   #define LINE_FEED 0xD
   #define MAKE_UBYTE(LOW_NIBBLE,HIGH_NIBBLE)  ((ubyte)(((ubyte)LOW_NIBBLE) | (((ubyte)HIGH_NIBBLE)<<4)))
   #define MAKE_UINT(LOW_UBYTE,HIGH_UBYTE)  (((uint)LOW_UBYTE) | (((uint)HIGH_UBYTE)<<8))
   #define MAKE_ULONG(LOW_WORD, HIGH_WORD) ((ulong)(((uint)(LOW_WORD)) | (((ulong)((uint)(HIGH_WORD))) << 16)))
extern unsigned char HexToChar(unsigned char ucHexVal);
adtyp m_ulProgramStartAddr;
/****************************************************************************
     Function: FlashSrecProcessRecords
     Engineer: Sudeep R K
        Input: char void 
       Output: int32_t
  Description: Initialise the buffer for storing section information 
  Date           Initials    Description
02-Feb-2008    SRK          Initial & cleanup
*****************************************************************************/
int32_t FlashSrecProcessRecords()
{
   int32_t ErrRet = ERR_F_NULL_FILE;
   FILE *hInputFileHandle = NULL;
   unsigned char        szFileBuffer[MAX_CHARS_IN_RECORD];
   unsigned char        szDataBuffer[MAX_DATA_BYTES_IN_RECORD];
   unsigned char bDataPresent;
   adtyp         ulDataAddr=0;
   adtyp         ulDataCount;
   hInputFileHandle = fopen (tyProcInFileData.szFileName,"rt");
   if(hInputFileHandle == NULL)
      return F_CANNOT_FIND_READ_FILE;
   while(fgets((char *)szFileBuffer,MAX_CHARS_IN_RECORD,hInputFileHandle) != NULL)
      {

      ErrRet = GetDataFromFileRecordSRec(szFileBuffer, szDataBuffer, &bDataPresent, &ulDataAddr, &ulDataCount);
      if(ErrRet != NO_ERROR)
         goto ErrorHandler;
      if(bDataPresent)
         {
         // Keep track of lowest address encountered, this is where the
         // code windows will be filled from...
         //if (ulDataAddr < start_code_address)
         //  start_code_address = ulDataAddr;

         ErrRet = BuildDataRecord(
                                 CODE_MEM_START,
                                 CODE_MEM_END,
                                 ulDataAddr,
                                 ulDataCount,
                                 szDataBuffer);
         if(ErrRet!= NO_ERROR)
            goto ErrorHandler;

         }
      }

   ErrRet = BuildDataRecord(
                           CODE_MEM_START,
                           CODE_MEM_END,
                           ulDataAddr,
                           0,
                           szDataBuffer);
   if(ErrRet != NO_ERROR)
      goto ErrorHandler;

   ErrorHandler:
   if(hInputFileHandle != NULL)
      fclose(hInputFileHandle);
   return ErrRet;
}


/****************************************************************************
     Function: Initialise
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: Virtual function which should be overridden by derived class.
Version     Date           Initials    Description
1.0.0       01-Jul-2002    MOH         Initial
****************************************************************************/
void InitialiseSRec(void)
{
   m_ulProgramStartAddr = 0;
}
/****************************************************************************
     Function: GetDataFromFileRecord
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: Virtual function which should be overridden by derived class.
Version     Date           Initials    Description
1.0.0       01-Jul-2002    MOH         Initial
1.0.1       04-Mar-2009    SPC         Added support to set Program Entry Point in srec.
****************************************************************************/
TyError GetDataFromFileRecordSRec(unsigned char *pszFileBuffer, 
                                  unsigned char *pszDataBuffer, 
                                  unsigned char  *pbDataPresent, 
                                  adtyp          *pulDataAddr,
                                  adtyp          *pulDataCount)
{
   TyError ErrRet;
   unsigned char ucRecordType = 0;
   unsigned char ucCheckSum   = 0;
   adtyp         i;
   // Get the count...
   unsigned char ucDataCount;

   *pbDataPresent = FALSE;

   ErrRet = NO_ERROR;

   // Check to make sure this isn't empty...
   if((strlen((char *)pszFileBuffer) == 0) ||
      (pszFileBuffer[0] == CARRIAGE_RETURN) ||
      (pszFileBuffer[0] == LINE_FEED))
      return NO_ERROR;

   if(pszFileBuffer[0] != 'S')
      return ERR_INVALID_SREC_FORMAT;

   ucRecordType = HexToChar(pszFileBuffer[1]);
   switch(ucRecordType)
      {
      case 1:
         // Get the count...
         *pulDataCount = MAKE_UBYTE(HexToChar(pszFileBuffer[3]), HexToChar(pszFileBuffer[2]));
         // Get the address...
         *pulDataAddr  = MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[7]), HexToChar(pszFileBuffer[6])),
                                   MAKE_UBYTE(HexToChar(pszFileBuffer[5]), HexToChar(pszFileBuffer[4])));

         // Now check the checksum....
         // ... data+checksum first
         for(i=0; i < *pulDataCount-2; i++)
            {
            ucCheckSum += MAKE_UBYTE(HexToChar(pszFileBuffer[9+i*2]), HexToChar(pszFileBuffer[8+i*2]));
            }
         // ... with the count
         ucCheckSum += (unsigned char)*pulDataCount;

         // ... and finally with the address
         ucCheckSum += (unsigned char)*pulDataAddr;
         ucCheckSum += (unsigned char)(*pulDataAddr >> 8);

         // Hey, someone can't add....
         if(ucCheckSum != 0xFF)
            {
            ErrRet = ERR_INVALID_SREC_FORMAT;
            break;
            }

         // Now adjust the count to remove address & checksum
         *pulDataCount -= 3;

         // extract the data bytes converting to binary
         for(i=0; i<*pulDataCount; i++)
            pszDataBuffer[i] = MAKE_UBYTE(HexToChar(pszFileBuffer[9+i*2]), HexToChar(pszFileBuffer[8+i*2]));

         *pbDataPresent = TRUE;
         break;
      case 2:
         // Get the count...
         *pulDataCount = MAKE_UBYTE(HexToChar(pszFileBuffer[3]), HexToChar(pszFileBuffer[2]));
         // Get the address...
         *pulDataAddr  = MAKE_ULONG(MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[9]), HexToChar(pszFileBuffer[8])),
                                              MAKE_UBYTE(HexToChar(pszFileBuffer[7]), HexToChar(pszFileBuffer[6]))),
                                    MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[5]), HexToChar(pszFileBuffer[4])),
                                              0));
         // Now check the checksum....
         // ... data+checksum first
         for(i=0; i < *pulDataCount-3; i++)
            {
            ucCheckSum += MAKE_UBYTE(HexToChar(pszFileBuffer[11+i*2]), HexToChar(pszFileBuffer[10+i*2]));
            }
         // ... with the count
         ucCheckSum += (unsigned char)*pulDataCount;

         // ... and finally with the address
         ucCheckSum += (unsigned char)*pulDataAddr;
         ucCheckSum += (unsigned char)(*pulDataAddr >> 8);
         ucCheckSum += (unsigned char)(*pulDataAddr >> 16);

         // Hey, someone can't add....
         if(ucCheckSum != 0xFF)
            {
            ErrRet = ERR_INVALID_SREC_FORMAT;
            break;
            }

         // Now adjust the count to remove address & checksum
         *pulDataCount -= 4;

         // extract the data bytes converting to binary
         for(i=0; i<*pulDataCount; i++)
            pszDataBuffer[i] = MAKE_UBYTE(HexToChar(pszFileBuffer[11+i*2]), HexToChar(pszFileBuffer[10+i*2]));

         *pbDataPresent = TRUE;
         break;
      case 3:
         // Get the count...
         *pulDataCount = MAKE_UBYTE(HexToChar(pszFileBuffer[3]), HexToChar(pszFileBuffer[2]));
         // Get the address...
         *pulDataAddr  = MAKE_ULONG(MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[11]),  HexToChar(pszFileBuffer[10])),
                                              MAKE_UBYTE(HexToChar(pszFileBuffer[9]), HexToChar(pszFileBuffer[8]))),
                                    MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[7]), HexToChar(pszFileBuffer[6])),
                                              MAKE_UBYTE(HexToChar(pszFileBuffer[5]), HexToChar(pszFileBuffer[4]))));
         // Now check the checksum....
         // ... data+checksum first
         for(i=0; i < *pulDataCount-4; i++)
            {
            ucCheckSum += MAKE_UBYTE(HexToChar(pszFileBuffer[13+i*2]), HexToChar(pszFileBuffer[12+i*2]));
            }
         // ... with the count
         ucCheckSum += (unsigned char)*pulDataCount;

         // ... and finally with the address
         ucCheckSum += (unsigned char)*pulDataAddr;
         ucCheckSum += (unsigned char)(*pulDataAddr >> 8);
         ucCheckSum += (unsigned char)(*pulDataAddr >> 16);
         ucCheckSum += (unsigned char)(*pulDataAddr >> 24);

         // Hey, someone can't add....
         if(ucCheckSum != 0xFF)
            {
            ErrRet = ERR_INVALID_SREC_FORMAT;
            break;
            }

         // Now adjust the count to remove address & checksum
         *pulDataCount -= 5;

         // extract the data bytes converting to binary
         for(i=0; i<*pulDataCount; i++)
            pszDataBuffer[i] = MAKE_UBYTE(HexToChar(pszFileBuffer[13+i*2]), HexToChar(pszFileBuffer[12+i*2]));

         *pbDataPresent = TRUE;
         break;
      case 7:
         //Program Entry Point

         ucDataCount = MAKE_UBYTE(HexToChar(pszFileBuffer[3]), HexToChar(pszFileBuffer[2]));
         // Get the address...
         m_ulProgramStartAddr  = MAKE_ULONG(MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[11]),  HexToChar(pszFileBuffer[10])),
                                                      MAKE_UBYTE(HexToChar(pszFileBuffer[9]), HexToChar(pszFileBuffer[8]))),
                                            MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[7]), HexToChar(pszFileBuffer[6])),
                                                      MAKE_UBYTE(HexToChar(pszFileBuffer[5]), HexToChar(pszFileBuffer[4]))));
         // Now check the checksum....
         // ... data+checksum first
         for(i=0; i < ((adtyp)ucDataCount-4); i++)
            {
            ucCheckSum += MAKE_UBYTE(HexToChar(pszFileBuffer[13+i*2]), HexToChar(pszFileBuffer[12+i*2]));
            }
         // ... with the count
         ucCheckSum += ucDataCount;

         // ... and finally with the address
         ucCheckSum += (unsigned char)m_ulProgramStartAddr;
         ucCheckSum += (unsigned char)(m_ulProgramStartAddr >> 8);
         ucCheckSum += (unsigned char)(m_ulProgramStartAddr >> 16);
         ucCheckSum += (unsigned char)(m_ulProgramStartAddr >> 24);

         // Hey, someone can't add....
         if(ucCheckSum != 0xFF)
            {
            ErrRet = ERR_INVALID_SREC_FORMAT;
            break;
            }
         break;
      case 0:
      case 4:
      case 5:
      case 6:
      case 8:
      case 9:
         // Ignore these !
         break;
      default:
         ErrRet = ERR_INVALID_SREC_FORMAT;
         break;
      }
   return ErrRet;
}

#endif //#ifdef FLASH_SUPPORT


