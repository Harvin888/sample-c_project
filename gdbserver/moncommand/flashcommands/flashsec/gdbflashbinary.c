/****************************************************************************
       Module: gdbFlashSrec.c
     Engineer: Sudeep RK
  Description: Ashling GDB Server, FLASH program
Date           Initials    Description
26-Feb-2010    SRK          Initial & cleanup
28-Mar-2010    VK          Modifed for lint bulding
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>
#ifdef FLASH_SUPPORT
   #include "../flashutils/gdbflashcommonerrors.h"
   #include "../../../gdbglob.h"
   #include "gdbserver/gdberr.h"

   #ifdef ARM
      #include "gdbserver/arm/gdbarm.h"
   #endif
   #include "../flashutils/gdbflashcommonutils.h"
   #include "gdbserver/gdbxml.h"
   #include "../../../gdbty.h"
   #include "gdbserver/gdbutil.h"
   #include "gdbserver/gdblow.h"
   #include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
   #include "gdbserver/moncommand/ashload/gdbashloaderrors.h"

typedef uint32_t adtyp;
//Global varaibles


extern TyError BuildDataRecord(uint32_t      ulCodeStart, 
                               uint32_t      ulCodeEnd,
                               uint32_t      ulAddress,
                               uint32_t      ulCount,
                               unsigned char      *pucData);
//Static functions
int32_t FlashBinaryProcessData();
extern FileProcessData tyProcInFileData;

/****************************************************************************
     Function: FlashBinaryProcessData
     Engineer: Sudeep R K
        Input: char void 
       Output: int32_t
  Description: Initialise the buffer for storing section information 
  Date           Initials    Description
02-Feb-2008    SRK          Initial & cleanup
*****************************************************************************/
int32_t FlashBinaryProcessData()
{
   int32_t ErrRet = ERR_F_NULL_FILE;
   FILE *hInputFileHandle = NULL;
   unsigned char        szFileBuffer[MAX_CHARS_IN_RECORD];
   //unsigned char        szDataBuffer[MAX_DATA_BYTES_IN_RECORD];
   adtyp         ulDataAddr = 0;
   adtyp         ulDataCount = 0;

   ulDataAddr = tyProcInFileData.ulAddress;
   hInputFileHandle = fopen (tyProcInFileData.szFileName,"rb");
   if(hInputFileHandle == NULL)
      return F_CANNOT_FIND_READ_FILE;

   while((ulDataCount=fread((char *)szFileBuffer, 1, MAX_CHARS_IN_RECORD, hInputFileHandle)) != 0x0)
      {
      ErrRet = BuildDataRecord(
                              CODE_MEM_START,
                              CODE_MEM_END,
                              ulDataAddr,
                              ulDataCount,
                              szFileBuffer);
      if(ErrRet!= NOERROR)
         goto ErrorHandler;

      ulDataAddr += ulDataCount;
      }

   ErrRet = BuildDataRecord(
                           CODE_MEM_START,
                           CODE_MEM_END,
                           ulDataAddr,
                           0,
                           szFileBuffer);
   if(ErrRet != NO_ERROR)
      goto ErrorHandler;

   ErrorHandler:

   if(hInputFileHandle != NULL)
      fclose(hInputFileHandle);

   return ErrRet;
}

#endif //#ifdef FLASH_SUPPORT
