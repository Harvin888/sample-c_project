/****************************************************************************
     Function: FlashIHex
     Engineer: Sudeep R K
        Input: char *pcInput  - command param string with white char 
                                 at the beggining 
               char *pcOutput - string for output
               Argument format: <ELF NAME>
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Converts the SREC file to binary data in memory, ready to be written to
               Flash
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010    SVL        Modified for scripting support
28-Mar-2010    DK         Modifed for MIPS flash support
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifdef FLASH_SUPPORT
#include "../flashutils/gdbflashcommonerrors.h"
#include "../../../gdbglob.h"
#include "gdbserver/gdberr.h"

#ifdef ARM
#include "gdbserver/arm/gdbarm.h"
#endif
#include "../flashutils/gdbflashcommonutils.h"
#include "gdbserver/gdbxml.h"
#include "../../../gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdblow.h"

#include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
#include "gdbserver/moncommand/ashload/gdbashloaderrors.h"

#define MAX_DATA_BYTES_IN_RECORD 300

typedef uint32_t adtyp;

#ifdef FLASH_SUPPORT 
   #ifdef MIPS
      typedef  unsigned char ubyte;//Unisigned byte
      typedef  uint32_t uint;
   #endif
#endif

extern TyError GetDataFromFileRecordIHex(unsigned char *pszFileBuffer, 
                                        unsigned char *pszDataBuffer, 
                                        unsigned char  *pbDataPresent, 
                                        adtyp          *pulDataAddr,
                                        adtyp          *pulDataCount);

int32_t FlashIHexProcessRecords();
extern TyError BuildDataRecord(uint32_t      ulCodeStart, 
                               uint32_t      ulCodeEnd,
                               uint32_t      ulAddress,
                               uint32_t      ulCount,
                               unsigned char      *pucData);
extern FileProcessData tyProcInFileData;

#define CARRIAGE_RETURN 0xA
#define LINE_FEED 0xD
#define MAKE_UBYTE(LOW_NIBBLE,HIGH_NIBBLE) ((ubyte)(((ubyte)LOW_NIBBLE) | (((ubyte)HIGH_NIBBLE)<<4)))
#define MAKE_UINT(LOW_UBYTE,HIGH_UBYTE)  (((uint)LOW_UBYTE) | (((uint)HIGH_UBYTE)<<8))
extern unsigned char HexToChar(unsigned char ucHexVal);
uint32_t m_ulBaseAddress;
/****************************************************************************
     Function: FlashSrecProcessRecords
     Engineer: Sudeep R K
        Input: char void 
       Output: int32_t
  Description: Initialise the buffer for storing section information 
  Date           Initials    Description
02-Feb-2008    SRK          Initial & cleanup
*****************************************************************************/
int32_t FlashIHexProcessRecords()
{
    int32_t ErrRet = ERR_F_NULL_FILE;
    FILE *hInputFileHandle = NULL;
    unsigned char        szFileBuffer[MAX_CHARS_IN_RECORD];
    unsigned char        szDataBuffer[MAX_DATA_BYTES_IN_RECORD];
    unsigned char bDataPresent;
   adtyp         ulDataAddr=0;
   adtyp         ulDataCount;

    hInputFileHandle = fopen (tyProcInFileData.szFileName,"rt");
    if (hInputFileHandle == NULL)
      return F_CANNOT_FIND_READ_FILE;
    while (fgets((char *)szFileBuffer,MAX_CHARS_IN_RECORD,hInputFileHandle) != NULL)
      {

      ErrRet = GetDataFromFileRecordIHex(szFileBuffer, szDataBuffer, &bDataPresent, &ulDataAddr, &ulDataCount);
      if (ErrRet != NO_ERROR)
         goto ErrorHandler;
      if (bDataPresent)
         {
         // Keep track of lowest address encountered, this is where the
         // code windows will be filled from...
         //if (ulDataAddr < start_code_address)
          //  start_code_address = ulDataAddr;

         ErrRet = BuildDataRecord(
                                  CODE_MEM_START,
                                  CODE_MEM_END,
                                  ulDataAddr,
                                  ulDataCount,
                                  szDataBuffer);
         if (ErrRet!= NOERROR)
            goto ErrorHandler;

         }
      }

   ErrRet = BuildDataRecord(
                            CODE_MEM_START,
                            CODE_MEM_END,
                            ulDataAddr,
                            0,
                            szDataBuffer);
   if (ErrRet != NO_ERROR)
      goto ErrorHandler;

ErrorHandler:
   if (hInputFileHandle != NULL)
      fclose(hInputFileHandle);
   return ErrRet;
}

/****************************************************************************
     Function: GetDataFromFileRecord
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: Virtual function which should be overridden by derived class.
Version     Date           Initials    Description
1.0.0       01-Jul-2002    MOH         Initial
****************************************************************************/
TyError GetDataFromFileRecordIHex(unsigned char *pszFileBuffer, 
                                        unsigned char *pszDataBuffer, 
                                        unsigned char  *pbDataPresent, 
                                        adtyp          *pulDataAddr,
                                        adtyp          *pulDataCount)
{
   TyError ErrRet;
   adtyp         i;
   unsigned char ucRecordType;
   adtyp         ulAddress;
   adtyp         ulCount;
   unsigned char ucCheckSum;

   *pbDataPresent = FALSE;

   ErrRet = NO_ERROR;

   // Check to make sure this isn't empty...
   if ((strlen((char *)pszFileBuffer) == 0) ||
       (pszFileBuffer[0] == CARRIAGE_RETURN) ||
       (pszFileBuffer[0] == LINE_FEED))
      return NO_ERROR;

   if (pszFileBuffer[0] != ':')
      return ERR_INVALID_HEX_FORMAT;

   // Right, let's first check the checksum...
   ucRecordType = MAKE_UBYTE(HexToChar(pszFileBuffer[8]), HexToChar(pszFileBuffer[7]));
   ulCount      = MAKE_UBYTE(HexToChar(pszFileBuffer[2]), HexToChar(pszFileBuffer[1]));
   ulAddress    = MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[6]), HexToChar(pszFileBuffer[5])),
                            MAKE_UBYTE(HexToChar(pszFileBuffer[4]), HexToChar(pszFileBuffer[3])));
   ucCheckSum = 0;
   // Add the data + checksum...
   for (i=0; i <= ulCount; i++)
      ucCheckSum += MAKE_UBYTE(HexToChar(pszFileBuffer[10+i*2]), HexToChar(pszFileBuffer[9+i*2]));
   ucCheckSum += ucRecordType;
   ucCheckSum += (unsigned char)ulCount;
   ucCheckSum += (unsigned char)ulAddress;
   ucCheckSum += (unsigned char)(ulAddress >> 8);

   if (ucCheckSum != 0)
      return ERR_INVALID_HEX_FORMAT;

   switch (ucRecordType)
      {
      case INTEL_HEX_DATA_RECORD:
         *pulDataAddr  = m_ulBaseAddress + ulAddress;
         *pulDataCount = ulCount;

         // extract the data bytes converting to binary
         for (i=0; i<*pulDataCount; i++)
            pszDataBuffer[i] = MAKE_UBYTE(HexToChar(pszFileBuffer[10+i*2]), HexToChar(pszFileBuffer[9+i*2]));
         *pbDataPresent = TRUE;
         break;

      case INTEL_HEX_EXTENDED_SEGMENT_ADDRESS_RECORD:
         m_ulBaseAddress = MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[12]), HexToChar(pszFileBuffer[11])),
                                     MAKE_UBYTE(HexToChar(pszFileBuffer[10]), HexToChar(pszFileBuffer[ 9])));
         m_ulBaseAddress <<= 4;
         break;

      case INTEL_HEX_EXTENDED_LINEAR_ADDRESS_RECORD:
         m_ulBaseAddress = MAKE_UINT(MAKE_UBYTE(HexToChar(pszFileBuffer[12]), HexToChar(pszFileBuffer[11])),
                                     MAKE_UBYTE(HexToChar(pszFileBuffer[10]), HexToChar(pszFileBuffer[ 9])));
         m_ulBaseAddress <<= 16;
         break;
      case INTEL_HEX_END_OF_FILE_RECORD:
      case INTEL_HEX_START_SEGMENT_ADDRESS_RECORD:
      case INTEL_HEX_START_LINEAR_ADDRESS_RECORD:
         // Ignore these !
         break;
      default:
         ErrRet = ERR_INVALID_HEX_RECORD;
         break;
      }
   return ErrRet;
}

/****************************************************************************
     Function: InitialiseHex
     Engineer: Sudeep R K
        Input: 
       Output: 
  Description: Initilaise the hex processing
Version     Date           Initials    Description
1.0.0       11-MAR-2010    SRK         Initial
            28-Mar-2010    VK          Modified for Lint building
****************************************************************************/
void InitialiseHex(void)
{
   m_ulBaseAddress = 0;
}

#endif //#ifdef FLASH_SUPPORT
