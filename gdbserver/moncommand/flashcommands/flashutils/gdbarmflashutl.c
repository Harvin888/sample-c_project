/****************************************************************************
       Module: gdbarmflashutl.c
     Engineer: Vitezslav Hola
  Description: utils for ashload command
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
22-Feb-2010    SVL         Modified for scripting support
28-Mar-2010    DK          Modifed microchip flash support
28-Mar-2010    vk          Modifed for Linting
29-05-2010     DK          Modfied file name according to standard
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifdef FLASH_SUPPORT
    #define SCRIPT_MOD 1
    #include "../../../gdbglob.h"
    #include "../../../gdbxml.h"
    #include "../../../gdbty.h"
    #include "../../../gdberr.h"
    #include "../../../gdbutil.h"
    #include "gdbflashcommonerrors.h"
    #include "../flashutils/gdbflashcommonutils.h"
    #include "gdbserver/moncommand/ashload/gdbashloaderrors.h"
    #include "gdbserver/gdblow.h"
    #ifdef ARM
        #include "gdbserver/arm/gdbarm.h"
    #endif

    #ifdef __LINUX
        #include <unistd.h>
        #include <sys/stat.h>
    #endif
    #include "gdbserver/moncommand/ashload/gdbashloadcommon.h"
    #define minimum(a,b) (((a)<(b))?(a):(b))
    #define MAX_REL_OFFSET_BYTE_LEN (5)
    #define MAX_VERIFY_CMD_LENGTH   (20)

typedef struct _SECTION_INFO
    {
    int32_t iLen;                              //Length in bytes of section
    uint32_t ulPAddress;              //Physical address of section
    int32_t iBuffStartIndex;                   //The index of the start of the section in
                                           //global array 
    int32_t iCurChunkIndex;                    //Shows the progress of section write to Flash(read buffer and write flash)
                                           //Index in the global array from where the next chunk of data need be fetched,
                                           //if one section is written in multiple chunks to Flash
    }SECTION_INFO;

typedef struct _SECTION_BUFF_MGR
    {
    int32_t bBuffValid;
    //Write Index for the location of the buffer at which next section can be written
    uint32_t ulBufferWrIndex;
    //Write Index of the section in the SectionInfo array
    int32_t iSectionWrIndex;
    //Read Index of the section in the SectionInfo array
    //int32_t iSectionRdIndex;
    //Global array to keep the parsed sections
    //This may be a file also, if permormance tests suggest so
    unsigned char SectionArray[MAX_LEN_SECTIONARRAY];
    //Global array of data structure to keep the info for sections
    //Each entry corresponds to info for one section
    SECTION_INFO SectionInfo[MAX_NUM_SECTION];
    }SECTION_BUFF_MGR;

typedef struct _VerifyProcessData 
    {
    char szFileName[_MAX_PATH];          //The file name extracted from user request
    //char* pcGDBReplyOutput;              //The pointer to output reply of "FlashElfToSections" to GDB
    char  szErrorBuffer[FLASH_ELFTOSECTIONS_MESSAGE_BUFFER_SIZE];
    uint32_t ulRelOffset; //User specified relocation offset
    /* to indicate script mode */
    //int32_t iScriptMode;
    int32_t bRelOffsetValid;
    }VerifyProcessData;
#ifdef FLASH_SUPPORT
    uint32_t ulGlobalFlashStartAddress = 0;
    uint32_t ulGlobalFlashEndAddress = 0;
#endif

//external functions
extern unsigned char CheckSpace(int32_t iChar);
extern int32_t WriteBinaryRecBlockToBuffer(uint32_t ulPhyAddr,
                                       uint32_t ulSize,
                                       unsigned char* pucSrcAddr);
extern TyServerInfo tySI;
static VerifyProcessData tyVerifyProcData;
static unsigned char pucDataBuffer[MAX_DATA_CONTENT_WRITE_LENGTH];
// Static variables
static uint32_t ulDataBufferAddress; //Used in Build data record
static uint32_t ulDataBufferCount; //Used in Build data record
// Static variables
//Buffer manager for storing secions and info
static SECTION_BUFF_MGR tySectionBuffMgr;
//Array to hold the verification data from Flash
static unsigned char FlashVerficationArray[MAX_DATA_CONTENT_WRITE_LENGTH];
//Print error message to console
static void FlashVerifySetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut);
//Initialise
static void FlashVerifyInit(void);

/****************************************************************************
     Function: isInScriptMode
     Engineer: Sandeep V L 
        Input: char *pcTok  : input string
       Output: int32_t - error code
  Description: Checks whehter the command is initiated from GDBScript 
Date           Initials    Description
17-Feb-2010    SVL         Initial version
*****************************************************************************/
int32_t isInScriptMode(char *pcTok)
{
    char szTok[MAX_SCRIPT_MODE] = "\0";
    int32_t iIdx = 0;

    /* skip the spaces */
    while (CheckSpace(*pcTok))
        pcTok++;

    /* to check for the script mode */
    if (*pcTok != '\0')
        {
        while ((pcTok[iIdx] != '\0') 
               && (pcTok[iIdx] != ' ')
               && (pcTok[iIdx]!= '\t'))
            {
            szTok[iIdx] = pcTok[iIdx];
            iIdx++;
            }
        szTok[iIdx] = '\0';

        if (strcasecmp(szTok, "--script-mode") == 0)
            {
            return 1;
            }
        }

    return 0;
}

/****************************************************************************
     Function: InitScriptOut
     Engineer: Sandeep V L 
        Input: TyFlashCmdType - Command type
               char * - Value to be written
       Output: int32_t - error code
  Description: Dumps the output of a monitor command to a file 
Date           Initials    Description
17-Feb-2010    SVL         Initial version
*****************************************************************************/
int32_t InitScriptOut(TyFlashCmdType tyFlashCmdType, char *pcOutput)
{
    FILE *pfMonOut = NULL;

#ifdef __LINUX
    int32_t status = 0;
    printf ("Creating drectories \n");
    status = mkdir("/tmp/Ashling", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    pfMonOut = fopen("/tmp/Ashling/monitor.out", "w");
#else
    pfMonOut = fopen("c:/Windows/Temp/monitor.out", "w");
#endif

    if (!pfMonOut)
        {
        sprintf(tySI.szError, "%s", GDB_EMSG_CMD_COULDNT_OPEN_FILE);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return -1;
        }

    switch (tyFlashCmdType)
        {
        case ELF_TO_SEC:
            fprintf(pfMonOut, "%s", "set $Num_Of_Sections = 0");
            break;
        case GET_OFFSET_OF_SECT:
            fprintf(pfMonOut, "%s", "set $Offset_Of_Section = 0");
            break;
        case GET_SIZE_OF_SECT:
            fprintf(pfMonOut, "%s", "set $Size_Of_Section = 0");
            break;
        case PREPARE_CHUNK:
            fprintf(pfMonOut, "%s", "set $PC_Reply = 0");
            break;
        case FLASH_VERIFY:
            fprintf(pfMonOut, "set $isVerified = 1");
            break;
        default:
            break;
        }

    fclose(pfMonOut);
    pfMonOut = NULL;
    return 0;
}

/****************************************************************************
     Function: WrtiteMonOut
     Engineer: Sandeep V L 
        Input: TyFlashCmdType - Command type
               char * - Value to be written
       Output: int32_t - error code
  Description: Dumps the output of a monitor command to a file 
Date           Initials    Description
17-Feb-2010    SVL         Initial version
*****************************************************************************/
int32_t WriteMonOut(TyFlashCmdType tyFlashCmdType, char *pcVal, char *pcOutput)
{
    FILE *pfMonOut = NULL;

#ifdef __LINUX
    pfMonOut = fopen("/tmp/Ashling/monitor.out", "w");
#else
    pfMonOut = fopen("c:/Windows/Temp/monitor.out", "w");
#endif

    if (!pfMonOut)
        {
        sprintf(tySI.szError, "%s", GDB_EMSG_CMD_COULDNT_OPEN_FILE);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return FALSE;
        }

    switch (tyFlashCmdType)
        {
        case ELF_TO_SEC:
            fprintf(pfMonOut, "set $Num_Of_Sections = %s", pcVal);
            break;
        case GET_OFFSET_OF_SECT:
            fprintf(pfMonOut, "set $Offset_Of_Section = %s", pcVal);
            break;
        case GET_SIZE_OF_SECT:
            fprintf(pfMonOut, "set $Size_Of_Section = %s", pcVal);
            break;
        case PREPARE_CHUNK:
            fprintf(pfMonOut, "set $PC_Reply = %s", pcVal);
            break;
        case FLASH_VERIFY:
            fprintf(pfMonOut, "set $isVerified = %s", pcVal);
            break;
        default:
            break;
        }

    fclose(pfMonOut);
    pfMonOut = NULL;
    return TRUE;
}

/****************************************************************************
     Function: BuildDataRecord
     Engineer: Patrick Shiels
        Input: 
       Output: 
  Description: Gather as many records as possible into a
               data buffer before writing to emulator.
Version     Date           Initials    Description
1.1.7       28-May-2003    PJS         Initial
****************************************************************************/
TyError BuildDataRecord(uint32_t   ulCodeStart,
                        uint32_t   ulCodeEnd,
                        uint32_t   ulAddress,
                        uint32_t   ulCount,
                        unsigned char   *pucData)
{
    TyError ErrRet = NO_ERROR;
    uint32_t ulLocalCount;
    uint32_t ulLocalAddress;
    unsigned char *pucLocalData;

    // Check if we are to flush the buffer...
    if (ulCount == 0)
        {
        if (ulDataBufferCount != 0)
            {
            //Write binary block parsed from SREC,to buffer
            return WriteBinaryRecBlockToBuffer(ulDataBufferAddress,
                                               ulDataBufferCount,
                                               pucDataBuffer);
            }
        return NO_ERROR;
        }

    // Calculate the true address, count and data...
    ulLocalCount = ulCount;
    ulLocalAddress = ulAddress;
    pucLocalData = pucData;

    // This is to handle the case when we are gone beyond the end of
    //  the code area we can write to
    if (ulAddress > ulCodeEnd)
        return NO_ERROR;

    if (ulCodeStart > ulAddress)
        {
        ulLocalCount   -= minimum(ulLocalCount, ulCodeStart - ulAddress);
        pucLocalData   += ulCodeStart - ulAddress;
        ulLocalAddress  = ulCodeStart;
        }

    if (ulCodeEnd < (ulLocalAddress + ulLocalCount -1))
        {
        ulLocalCount = (ulCodeEnd - ulLocalAddress) + 1;
        }

    // Have we any data left....
    if (ulLocalCount == 0)
        return NO_ERROR;

    // Is there enough space in our buffer for this...
    if (ulDataBufferCount != 0)
        {
        if ((ulDataBufferCount + ulLocalCount) > MAX_DATA_CONTENT_WRITE_LENGTH)
            {
            //Write binary block parsed from SREC,to buffer
            ErrRet = WriteBinaryRecBlockToBuffer(ulDataBufferAddress,
                                                 ulDataBufferCount,
                                                 pucDataBuffer);
            ulDataBufferAddress = ulLocalAddress;
            ulDataBufferCount   = ulLocalCount;
            memcpy(pucDataBuffer, pucLocalData, ulLocalCount);

            return ErrRet;
            }

        // Is this memory disjointed...
        if ((ulDataBufferCount + ulDataBufferAddress) != ulLocalAddress)
            {

            //Write binary block parsed from SREC,to buffer
            ErrRet = WriteBinaryRecBlockToBuffer(ulDataBufferAddress,
                                                 ulDataBufferCount,
                                                 pucDataBuffer);
            ulDataBufferAddress = ulLocalAddress;
            ulDataBufferCount   = ulLocalCount;
            memcpy(pucDataBuffer, pucLocalData, ulLocalCount);

            return ErrRet;
            }
        }

    // Last possible case is to copy the data into the buffer...
    memcpy(&pucDataBuffer[ulDataBufferCount], pucLocalData, ulLocalCount);
    if (ulDataBufferCount == 0)
        ulDataBufferAddress = ulLocalAddress;
    ulDataBufferCount   += ulLocalCount;

    return NO_ERROR;
}
/****************************************************************************
     Function: InitBuildRecordVars
     Engineer: Sudeep R K
        Input: 
       Output: 
  Description: Initilaise the static variables for BuildDataRecord
Version     Date           Initials    Description
           02-Mar-2010      SRK         Initial
****************************************************************************/
void InitVarsForBuildRecord()
{
    ulDataBufferAddress = 0;
    ulDataBufferCount = 0;
}

/****************************************************************************
     Function: GetReadChunkIndexOfSection
     Engineer: Sudeep R K
        Input: int32_t iSectionNum  - Section number
       Output: static int32_t       - Index
  Description: Returns the index of the next chunk of the section to be read
Date           Initials    Description
08-Feb-2008    SRK          Initial & cleanup
****************************************************************************/
static int32_t GetReadChunkIndexOfSection(int32_t iSectionNum)
{
    if (0 == tySectionBuffMgr.SectionInfo[iSectionNum].iCurChunkIndex)
        {
        tySectionBuffMgr.SectionInfo[iSectionNum].iCurChunkIndex = 
        tySectionBuffMgr.SectionInfo[iSectionNum].iBuffStartIndex;
        }
    return(tySectionBuffMgr.SectionInfo[iSectionNum].iCurChunkIndex);
}

/****************************************************************************
     Function: UpdateChunkIndexOfSection
     Engineer: Sudeep R K
        Input: void
       Output: none
  Description: Update the chunk index with in the section
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
static void UpdateReadChunkIndexOfSection(int32_t iSectionNum, int32_t iOffset)
{
    tySectionBuffMgr.SectionInfo[iSectionNum].iCurChunkIndex += iOffset;
}

//Global functions
/****************************************************************************
     Function: GetCurSectionWrBufAddress
     Engineer: Sudeep R K
        Input: void
       Output: unsigned char *
  Description: Returns the address of the buffer at which to write the section
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
unsigned char *GetCurSectionWrBufAddress()
{
    return(&(tySectionBuffMgr.SectionArray[tySectionBuffMgr.ulBufferWrIndex]));
}

/****************************************************************************
     Function: WriteSectionBuffer
     Engineer: Sudeep R K
        Input: uint32_t ulPhyAddr - Physical address of section from elf
               uint32_t ulVirtualAddr - Virtual address of section from elf
               unsigned char* pulDestAddr  - Buffer address to where section is written
               uint32_t ulSize        - Size of section
               unsigned char* pucSrcAddr   - The source buffer address 
       Output: uint32_t ulRet
               GDBSERV_NO_ERROR : On successful completion
               ERR_MEMORY_WRITE_FAILURE : On failure
  Description: Writes the number of bytes "ulSize" from "pucSrcAddr" to "pulDestAddr"
               memory address
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
17-May-2010    DK          Modified for Micro chip flash support
****************************************************************************/
uint32_t WriteSectionBuffer(uint32_t ulPhyAddr,
                                 uint32_t ulVirtualAddr,
                                 unsigned char* pulDestAddr,
                                 uint32_t ulSize,
                                 unsigned char* pucSrcAddr)
{

    uint32_t ulRet = ERR_MEMORY_WRITE_FAILURE;

    ulVirtualAddr=ulVirtualAddr;//AVOIDING LINT ERROR FOR ARM,ARC
    if ((NULL == pulDestAddr)||(NULL == pucSrcAddr))
        {
        return ulRet;
        }
    /*Store virtual address for Micro chip alone*/

    //Checks if buffer is available
    if (ulSize <= (MAX_LEN_SECTIONARRAY - (tySectionBuffMgr.ulBufferWrIndex + 1)))
        {
        //Checks if section info array is available 
        if (tySectionBuffMgr.iSectionWrIndex < MAX_NUM_SECTION)
            {
            memcpy(pulDestAddr,pucSrcAddr,ulSize);
            //Sets the buffer index inside info structure
            tySectionBuffMgr.SectionInfo[tySectionBuffMgr.iSectionWrIndex].iBuffStartIndex 
            =(int32_t) tySectionBuffMgr.ulBufferWrIndex;
            tySectionBuffMgr.SectionInfo[tySectionBuffMgr.iSectionWrIndex].iLen = (int32_t)ulSize;
            //Physical address of the section
            tySectionBuffMgr.SectionInfo[tySectionBuffMgr.iSectionWrIndex].ulPAddress = ulPhyAddr;
            //Update the section write index to next
            tySectionBuffMgr.iSectionWrIndex++;
            //Update the section buffer index to value after 'ulSize' from start
            tySectionBuffMgr.ulBufferWrIndex += ulSize;
            ulRet = GDBSERV_NO_ERROR;
            }
        else
            {
            //Do nothing
            }    
        }
    return ulRet;
}

/****************************************************************************
     Function: SetBufferValid
     Engineer: Sudeep R K
        Input: void
       Output: void
  Description: Sets the status of buffer validity flag
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
void SetBufferValid(void)
{
    tySectionBuffMgr.bBuffValid = TRUE;
}

/****************************************************************************
     Function: InitAndClearBuffer
     Engineer: Sudeep R K
        Input: void
       Output: void
  Description: Initialise and clears the buffer manager and buffer
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
void InitAndClearBufferMgr(void)
{
    memset(&tySectionBuffMgr,0,sizeof(tySectionBuffMgr));
}

/****************************************************************************
     Function: CheckBufferValid
     Engineer: Sudeep R K
        Input: void
       Output: bool
               true - If buffer is valid
               false - If buffer is invalid 
  Description: Sets the status of buffer validity flag
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
int32_t CheckBufferValid(void)
{
    return tySectionBuffMgr.bBuffValid;
}

/****************************************************************************
     Function: GetNumSectionsInBuff
     Engineer: Sudeep R K
        Input: void
       Output: int32_t
  Description: Sets the status of buffer validity flag
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
int32_t GetNumSectionsInBuff(void)
{
    return tySectionBuffMgr.iSectionWrIndex;
}

/****************************************************************************
     Function: XferChunkToTargetBuffer
     Engineer: Sudeep R K
        Input: int32_t iSectionNum          - Secton number
               uint32_t ulUtilBuff - Target Flash Utility buffer address
               uint32_t ulSize     - Size of chunk to be transferred
       Output: bool
               true  - On success
               false - On failure
  Description: Transfer the chunk of data specified by 'ulSize' from the buffer
               to the target memory address 'ulUtilBuff'
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
int32_t XferChunkToTargetBuffer(int32_t iSectionNum, uint32_t ulUtilBuff,uint32_t ulSize)
{
    int32_t bRet = FALSE;
    int32_t iReadChunkIndex = GetReadChunkIndexOfSection(iSectionNum);
    if (iReadChunkIndex != ERR_INVALID_CHUNK_INDEX)
        {
        if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulUtilBuff, ulSize,&(tySectionBuffMgr.SectionArray[iReadChunkIndex])))
            {
            //Do nothing
            }
        else
            {
            //Update the read index of the next chunk for the section
            UpdateReadChunkIndexOfSection(iSectionNum,(int32_t)ulSize);
            bRet = TRUE;
            }
        }
    else
        {
        //Do nothing
        }
    return bRet;
}

/****************************************************************************
     Function: GetSizeOfSection
     Engineer: Sudeep R K
        Input: void
       Output: none
  Description: Returns the size of the section
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
int32_t GetSizeOfSection(int32_t iSectionNum)
{
    return(tySectionBuffMgr.SectionInfo[iSectionNum].iLen);
}

/****************************************************************************
     Function: GetOffsetOfSection
     Engineer: Sudeep R K
        Input: void
       Output: none
  Description: Returns the size of the section
Date           Initials    Description
08-Feb-2010    SRK         Initial & cleanup
****************************************************************************/
uint32_t GetOffsetOfSection(int32_t iSectionNum)
{
    return(tySectionBuffMgr.SectionInfo[iSectionNum].ulPAddress);
}

/****************************************************************************
     Function: GetMaxSizeOfSectionArray
     Engineer: Sudeep R K
        Input: void
       Output: none
  Description: Returns the size of the section
Date           Initials    Description
08-Feb-2010    SRK         Initial & cleanup
****************************************************************************/
int32_t GetMaxSizeOfSectionArray()
{
    return sizeof(tySectionBuffMgr.SectionArray);
}

/****************************************************************************
     Function: VerifyFlashProgram
     Engineer: Sudeep R K
        Input: char *pcGDBOutput - output string  
       Output: none
  Description: Verify the Flash Program
  Date           Initials    Description
02-Feb-2008    SRK          Initial & cleanup
*****************************************************************************/
int32_t VerifyFlashProgram(uint32_t ulRelOffset,char *pcOutput)
{
    uint32_t ulAddress = 0;
    int32_t iError = ERR_MEMORY_READ_FAILURE;
    int32_t i,j;
    if (NULL == pcOutput)
        {
        return iError;
        }
    //Verification logic. 
    if (0 != tySectionBuffMgr.iSectionWrIndex)
        {
        for (i=0; i<tySectionBuffMgr.iSectionWrIndex ; i++)
            {
            memset(FlashVerficationArray,0xff,sizeof(FlashVerficationArray));
            /*Modified for Microchip to read data from virtual memory*/
            ulAddress = tySectionBuffMgr.SectionInfo[i].ulPAddress ;

            //Reads from Flash secion by section

            if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress+ulRelOffset,(uint32_t)tySectionBuffMgr.SectionInfo[i].iLen,FlashVerficationArray))
                {
                iError = ERR_MEMORY_READ_FAILURE;
                goto Exit;
                }
            for (j=0; j< tySectionBuffMgr.SectionInfo[i].iLen; j++)
                {
                if (TRUE==CheckBufferValid())
                    {
                    //Compares with the copy in buffer byte by byte
                    if (tySectionBuffMgr.SectionArray[tySectionBuffMgr.SectionInfo[i].iBuffStartIndex + j] != FlashVerficationArray[j])
                        {
                        sprintf(pcOutput, "Verification failed at 0x%02X. Expected:0x%02X, read:0x%02X.", 
                                (uint32_t)(tySectionBuffMgr.SectionInfo[i].ulPAddress +(uint32_t) j),
                                (tySectionBuffMgr.SectionArray[tySectionBuffMgr.SectionInfo[i].iBuffStartIndex + j]), 
                                (FlashVerficationArray[j]));
                        iError = ERR_MEMORY_VERIFY_FAILURE;
                        goto Exit;
                        }
                    }
                else
                    {
                    iError = ERR_MEMORY_VERIFY_FAILURE;
                    goto Exit;
                    }          
                }
            }
        //Verification over successfully
        iError = GDBSERV_NO_ERROR;
        }
    Exit:
    return iError;
}
 
/****************************************************************************
     Function: WriteBinaryRecBlockToBuffer
     Engineer: Sudeep R K
        Input: uint32_t ulPhyAddr - Physical address of the block in Flash
               unsigned char* pulDestAddr  - Buffer address to where fragmeng is written
               uint32_t ulSize        - Size of block
               unsigned char* pucSrcAddr   - The source buffer address 
       Output: uint32_t ulRet
               GDBSERV_NO_ERROR : On successful completion
               ERR_MEMORY_WRITE_FAILURE : On failure
  Description: Writes the block of size ulSize in the host's buffer. Also keeps
               the physical address(in Flash) where the block need be written to
               Uses the same buffer (tySectionBuffMgr) as used by WriteSectionBuffer()
Date           Initials    Description
26-Feb-2010    SRK          Initial & cleanup
28-Mar-2010    DK           Modified for micrchip flash support for storing virtual address
****************************************************************************/
int32_t WriteBinaryRecBlockToBuffer(uint32_t ulPhyAddr,
                                          uint32_t ulSize,
                                          unsigned char* pucSrcAddr)
{
    int32_t iRet = ERR_MEMORY_WRITE_FAILURE;
    unsigned char* pucDstAddr= NULL;
    
// If the data goes past the end of the flash, trim the count...
    if ((ulPhyAddr+ulSize) > (ulGlobalFlashEndAddress+1))
        {
        ulSize = (ulGlobalFlashEndAddress - ulPhyAddr) + 1;
        }
    //Checks if buffer is available
    if (ulSize <= (MAX_LEN_SECTIONARRAY - (tySectionBuffMgr.ulBufferWrIndex)))
        {
        //Checks if block info array is available 
        if (tySectionBuffMgr.iSectionWrIndex < MAX_NUM_SECTION)
            {
            //Calculate the address in buffer where to write the block
            pucDstAddr = &(tySectionBuffMgr.SectionArray[tySectionBuffMgr.ulBufferWrIndex]);
            //Write the block to buffer
            memcpy(pucDstAddr,pucSrcAddr,ulSize);
            //Sets the buffer index inside info structure
            tySectionBuffMgr.SectionInfo[tySectionBuffMgr.iSectionWrIndex].iBuffStartIndex 
            = (int32_t)tySectionBuffMgr.ulBufferWrIndex;
            tySectionBuffMgr.SectionInfo[tySectionBuffMgr.iSectionWrIndex].iLen = (int32_t)ulSize;
            //Physical address of the section
            tySectionBuffMgr.SectionInfo[tySectionBuffMgr.iSectionWrIndex].ulPAddress = ulPhyAddr;
            //Update the block write index to point to next block
            tySectionBuffMgr.iSectionWrIndex++;
            //Update the block buffer index to value after 'ulSize' from start
            tySectionBuffMgr.ulBufferWrIndex += ulSize;
            iRet = GDBSERV_NO_ERROR;
            }
        else
            {
            //Do nothing
            }    
        }
    return iRet;
}

/****************************************************************************
     Function: FlashVerify
     Engineer: Sudeep R K
        Input: char *pcInput  - Not Used
               char *pcOutput - Not Used
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Verifies the Flash Program
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010	   SVL			Added script mode, to support GDB Script 
29-Mar-2010    DK           Modifed for Micro chip flash verification
                            to get virtual address to verify
*****************************************************************************/
int32_t FlashVerify(char *pcInput, char *pcOutput)
{
    int32_t iError = ERR_MEMORY_VERIFY_FAILURE, iArgCnt = 0, iScriptMode = 0;
    char ppszArgList[SCRIPT_MOD+1][_MAX_PATH];
    char szErrBuf[_MAX_PATH] = {'\0'};
    char pucPrepChunkCmdParseBuffer[MAX_VERIFY_CMD_LENGTH];
    char *pcIndex = NULL;
    int32_t iCounter = 0;
    int32_t bParseUnitEnd = FALSE;
    char *pcBuffer = pucPrepChunkCmdParseBuffer;
    int32_t bParseUnitInsideQuotationMarks = FALSE;
    int32_t iValid = 0;
    int32_t i = 0;
    char szBuffer[_MAX_PATH] = {'\0'};
    // Check whether already connected  
    if (!tySI.bIsConnected)
        {
        sprintf(szBuffer,"Not connected to target\n");
        AddStringToOutAsciiz(szBuffer, pcOutput);
        return FALSE;
        }

    FlashVerifyInit();
    memset(pucPrepChunkCmdParseBuffer,0,sizeof(pucPrepChunkCmdParseBuffer));
    memset(ppszArgList, 0, (SCRIPT_MOD+1)*_MAX_PATH);
    /* get the args list */ 
    iArgCnt = GetArgs(pcInput, ppszArgList); 
    if (iArgCnt > (SCRIPT_MOD+1))
        {
        sprintf(tySI.szError,GDB_ERROR_INVALID_CMD_LINE_ARG);
        PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
        AddStringToOutAsciiz(tySI.szError, pcOutput);
        return 0;
        }
    for (i=0; i<iArgCnt; i++ )
        {
        if (strcasecmp(ppszArgList[i], "--script-mode") == 0)
            {
            iScriptMode = 1;
            if (InitScriptOut(FLASH_VERIFY,pcOutput))
                {
                return FALSE;
                }
            }
        else
            {
            pcIndex = &(ppszArgList[i][0]);
            while (!bParseUnitEnd && *pcIndex != '\0')
                {
                //Checks if number of digits exceeds 10
                if (iCounter > 2*MAX_REL_OFFSET_BYTE_LEN)
                    {
                    //sprintf(tyVerifyProcData.szErrorBuffer,"Relocation offset invalid\n");
                    FlashVerifySetupErrorMessage(ERR_INVALID_REL_OFFSET,pcInput,pcOutput);
                    return FALSE;
                    }
                *pcBuffer++ = *pcIndex++;
                bParseUnitEnd = bParseUnitInsideQuotationMarks  ? (*pcIndex == '"'):(CheckSpace(*pcIndex));
                iCounter++;
                }
            if (*pcIndex  == '\0')
                {
                if (bParseUnitInsideQuotationMarks)
                    {
                    FlashVerifySetupErrorMessage(ERR_INVALID_REL_OFFSET,pcInput,pcOutput);
                    return FALSE;     // Invalid relocation offset
                    }
                }
            else
                {
                if (bParseUnitInsideQuotationMarks)
                    pcIndex++;  //Update 'pcIndex' to cover the quotation mark end
                }

            if ((FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,HEX,2*MAX_REL_OFFSET_BYTE_LEN)) &&
                (FALSE == ValidateStr(pucPrepChunkCmdParseBuffer,DEC,2*MAX_REL_OFFSET_BYTE_LEN)))
                {
                FlashVerifySetupErrorMessage(ERR_INVALID_REL_OFFSET,pcIndex, pcOutput);
                return FALSE;      
                }
            else
                {
                *pcBuffer = '\0';
                tyVerifyProcData.ulRelOffset = StrToUlong(pucPrepChunkCmdParseBuffer,&iValid);
                if (!iValid)
                    {
                    sprintf(tySI.szError, GDB_ERROR_INVALID_CMD_LINE_ARG);
                    PrintMessage(ERROR_MESSAGE, "%s", tySI.szError);
                    AddStringToOutAsciiz(tySI.szError, pcOutput);
                    return FALSE;
                    }
                else
                    {
                    tyVerifyProcData.bRelOffsetValid = TRUE;
                    }
                } 
            }
        }   

    //Verify flash program
    if (tyVerifyProcData.bRelOffsetValid == TRUE)
        {
        iError = VerifyFlashProgram(tyVerifyProcData.ulRelOffset,szErrBuf);
        }
    else
        {
        iError = VerifyFlashProgram(0,szErrBuf);
        }
    if (!iScriptMode)
        {
        if (GDBSERV_NO_ERROR == iError)
            {
            sprintf(tyVerifyProcData.szErrorBuffer, "Flash Program Verification done\n");
            PrintMessage(INFO_MESSAGE, tyVerifyProcData.szErrorBuffer);
            AddStringToOutAsciiz(tyVerifyProcData.szErrorBuffer, pcOutput);
            }
        else
            {
            sprintf(tyVerifyProcData.szErrorBuffer, "Flash Program Verification FAILED\n");
            PrintMessage(INFO_MESSAGE, tyVerifyProcData.szErrorBuffer);
            AddStringToOutAsciiz(tyVerifyProcData.szErrorBuffer, pcOutput);
            }
        }
    else
        {
        AddStringToOutAsciiz(" ", pcOutput);

        /* Write the return code in to a file, if FlashVerify is called from script */
        sprintf(szErrBuf, "%d", iError);
        if (FALSE==WriteMonOut(FLASH_VERIFY, szErrBuf, pcOutput))
            {
            return FALSE;
            }
        }
    return TRUE;
}

/****************************************************************************
     Function: FlashVerifyInit
     Engineer: Sudeep R K
        Input: char *pcInput  - Not Used
               char *pcOutput - Not Used
       Output: int32_t - 1 if operation is siccessful, 0 otherwise
  Description: Verifies the Flash Program
Date           Initials    Description
25-Jan-2010    SRK          Initial & cleanup
19-Feb-2010	   SVL			Added script mode, to support GDB Script 
*****************************************************************************/
static void FlashVerifyInit()
{
    memset(&tyVerifyProcData,0,sizeof(tyVerifyProcData));
}

/****************************************************************************
     Function: FlashVerifySetupErrorMessage
     Engineer: Vitezslav HOla
        Input: int32_t iError : specifies error number for download
               char *pszFileName : pointer to download file name
               char *pcOut : hexadecimal string to be returned to the GDB
       Output: none
  Description: Prints out appropiate error message to GDB server console
Date           Initials    Description
08-Feb-2010    SRK          Initial & cleanup
****************************************************************************/
static void FlashVerifySetupErrorMessage(int32_t iError, char *pszFileName, char *pcOut)
{
    char *szErrorMsg = tyVerifyProcData.szErrorBuffer;
    switch (iError)
        {
        case ERR_UNEXPECTED_END_OF_FILE:
            sprintf(szErrorMsg, MESSAGE_ERR_UNEXPECTED_END_OF_FILE);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_F_NULL_FILE:
            sprintf(szErrorMsg, MESSAGE_ERR_F_NULL_FILE, tyVerifyProcData.szFileName);
            PrintMessage(ERROR_MESSAGE,szErrorMsg);            
            break;
        case ERR_F_SEEK:
            sprintf(szErrorMsg, MESSAGE_ERR_F_SEEK);
            PrintMessage(ERROR_MESSAGE, szErrorMsg );
            break;
        case ERR_NOT_AN_ELF_FILE:
            sprintf(szErrorMsg, MESSAGE_ERR_NOT_AN_ELF_FILE);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_NOT_ABSOLUTE_REC_E :
            sprintf(szErrorMsg, MESSAGE_ERR_NOT_ABSOLUTE_REC_E);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_PROCDWRF00 :
            sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF00);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_INSUFFICENT_DATA_BUFFER_MEMORY :
            sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_DATA_BUFFER_MEMORY);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY :
            sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_PROGRAM_HEADER_TABLE_MEMORY);
            PrintMessage(ERROR_MESSAGE, szErrorMsg );
            break;
        case ERR_MEMORY_READ_FAILURE :
            sprintf(szErrorMsg, MESSAGE_ERR_MEMORY_READ_FAILURE);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_MEMORY_VERIFY_FAILURE :
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_MEMORY_WRITE_FAILURE :
            sprintf(szErrorMsg, MESSAGE_ERR_MEMORY_WRITE_FAILURE);
            PrintMessage(ERROR_MESSAGE, szErrorMsg );
            break;
        case ERR_INVALID_NUMBER_OF_PARMS :
            sprintf(szErrorMsg, MESSAGE_ERR_INVALID_NUMBER_OF_PARMS);
            PrintMessage(ERROR_MESSAGE, szErrorMsg,pszFileName);
            break;            
        case ERR_FILE_PATH_TOO_LONG :
            sprintf(szErrorMsg, MESSAGE_ERR_FILE_PATH_TOO_LONG);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;            
        case ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND:
            sprintf(szErrorMsg, MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;            
        case ERR_COMMAND_SWITCH_TOO_LONG:
            sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_SWITCH_TOO_LONG);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;            
        case ERR_COMMAND_SWITCH_USED_TWICE :
            sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_SWITCH_USED_TWICE);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_NOT_ASHLOAD_COMMAND:
            sprintf(szErrorMsg, MESSAGE_ERR_NOT_ASHLOAD_COMMAND);
            PrintMessage(ERROR_MESSAGE,szErrorMsg);
            break;
        case ERR_UNKNOWN_SWITCH :
            sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_SWITCH);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);            
            break;
        case ERR_COMMAND_TOO_LONG :
            sprintf(szErrorMsg, MESSAGE_ERR_COMMAND_TOO_LONG);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_PROCDWRF07:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF07);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_PROCDWRF08:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_PROCDWRF08);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_READDWRF04:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF04);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_DUMMY_ALREADY_SHOWN:
            sprintf(szErrorMsg, MESSAGE_ERR_DUMMY_ALREADY_SHOWN);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_READDWRF03:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF03);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_MEM_MALLOC_C:
            sprintf(szErrorMsg, MESSAGE_ERR_MEM_MALLOC_C);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;             
        case ERR_IE_READDWRF02:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF02);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_READDWRF01:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF01 );
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_IE_READDWRF07:
            sprintf(szErrorMsg, MESSAGE_ERR_IE_READDWRF07);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_INVALID_FILE_NAME:
            sprintf(szErrorMsg, MESSAGE_ERR_INVALID_FILE_NAME);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY:
            sprintf(szErrorMsg, MESSAGE_ERR_INSUFFICENT_SECTION_HEADER_TABLE_MEMORY);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        case GDBSERV_ERROR:
            sprintf(szErrorMsg, MESSAGE_GDBSERV_ERROR);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        default:
            sprintf(szErrorMsg, MESSAGE_ERR_UNKNOWN_ERROR_OCURRED);
            PrintMessage(ERROR_MESSAGE, szErrorMsg);
            break;
        }
    strcat(szErrorMsg, "\n");
    AddStringToOutAsciiz(GDB_CLIENT_ERROR_MESSAGE_PREFIX, pcOut);
    AddStringToOutAsciiz(szErrorMsg, pcOut);
}
#endif //FLASH_SUPPORT
