/****************************************************************************
       Module: gdbFlashCommonUtils.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ashload util function, and types
Date           Initials    Description
08-Feb-2008    VH          Initial & cleaup
22-Feb-2010    SVL        Modified for scripting support
22-Mar-2010    DK         modifed maximum number of section value 
****************************************************************************/
#ifndef GDBFLASHCOMMONUTILS_H_
#define GDBFLASHCOMMONUTILS_H_

#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
//#define NO_ERROR				(0)
#define NOERROR					(0)
#define CODE_MEM_START				(0)
#define CODE_MEM_END				(0xFFFFFFFF)
#define ASHLOAD_DOWNLOAD_ELF                100
#define ASHLOAD_VERIFY_ELF_DOWNLOAD         200
#define FLASH_DOWNLOAD_ELF                  300
#define FLASH_VERIFY_ELF_DOWNLOAD           400
#define MAX_NUM_SECTION                     (32)
#define MAX_NUM_FRAGMENTS                   MAX_NUM_SECTION
#define MAX_DATA_CONTENT_WRITE_LENGTH       0xFFE0
#define MAX_LEN_SECTIONARRAY                (MAX_NUM_SECTION*MAX_DATA_CONTENT_WRITE_LENGTH)
#define MAX_LEN_FRAGMENT_ARRAY              (MAX_NUM_FRAGMENTS*MAX_DATA_CONTENT_WRITE_LENGTH)
#define MAX_IDENTIFIER_NAME_SIZE            63 

#define MAX_FILE_READ_LENGTH				(100)
#define FLASHPREP_CHUNK_MESSAGE_BUFFER_SIZE (_MAX_PATH + _MAX_PATH + 20)
#define MAX_PREPCHUNK_CMD_LENGTH			(20)
#define MAX_SIZE_BYTE_LEN					(5)
#define MAX_ADDR_BYTE_LEN					MAX_SIZE_BYTE_LEN
#define FLASHWRCHUNK_MESSAGE_BUFFER         (_MAX_PATH + _MAX_PATH + 20)
#define FLASHBIN_MESSAGE_BUFFER (_MAX_PATH + _MAX_PATH + 20)
#define MAX_SCRIPT_MODE 15
#define MAX_MON_OP 64
#define MAX_SECTIONNUM_BYTE_LEN        (5) 
#define MAX_SIZEOFSECTION_CMD_LENGTH   (20)
#define MAX_OFFSETOFSECTION_CMD_LENGTH MAX_SIZEOFSECTION_CMD_LENGTH
#define FLASH_SIZEOFSECTION_MESSAGE_BUFFER_SIZE (_MAX_PATH + _MAX_PATH + 20)
#define FLASH_OFFSETOFSECTION_MESSAGE_BUFFER_SIZE FLASH_SIZEOFSECTION_MESSAGE_BUFFER_SIZE
#define MAX_CHARS_IN_RECORD 600
#define INTEL_HEX_DATA_RECORD 0
#define INTEL_HEX_EXTENDED_SEGMENT_ADDRESS_RECORD 0x02
#define INTEL_HEX_EXTENDED_LINEAR_ADDRESS_RECORD 0x04
#define INTEL_HEX_END_OF_FILE_RECORD 0x01
#define INTEL_HEX_START_SEGMENT_ADDRESS_RECORD 0x03
#define INTEL_HEX_START_LINEAR_ADDRESS_RECORD 0x05

/* Flash command types */
typedef enum _flashCmdType
{
	ELF_TO_SEC = 0,
	GET_OFFSET_OF_SECT,
	GET_SIZE_OF_SECT,
	PREPARE_CHUNK,
	FLASH_VERIFY
}TyFlashCmdType;

/*Used to store start and end address for Microchip*/


int32_t FReadBytes(FILE *pFile, uint32_t ulFilePosition, unsigned char bSetFilePosition, void *pBuffer, uint32_t ulNumBytes);

/* to check whether the monitor commands are called from gdb script */
int32_t isInScriptMode(char *pcTok);
/* init monitor output to a file for GDB Script */
int32_t InitScriptOut(TyFlashCmdType, char*);
/* Dump monitor output to a file for GDB Script */
int32_t WriteMonOut(TyFlashCmdType, char*, char*);


#endif   // GDBFLASHCOMMONUTILS_H_

