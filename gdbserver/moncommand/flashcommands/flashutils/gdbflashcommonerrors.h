/****************************************************************************
       Module: gdbFlashCommonErrors.h
     Engineer: Sudeep R K 
  Description: Ashling GDB Server, ashload errors, ashload messages
Date           Initials    Description
08-Feb-2008    SRK          Initial & cleanup
11-Mar-2010	   SVL          Added validity checking for image formats
****************************************************************************/
#ifndef GDBFLASHCOMMONERRORS_H_
#define GDBFLASHCOMMONERRORS_H_

#define ERR_NOT_AN_ELF_FILE                                       103
#define ERR_F_NULL_IP_FILE                                        101
#define F_CANNOT_FIND_READ_FILE                                   102
#define ERR_FILE_PATH_TOO_LONG                                    115
#define ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND                      116
#define ERR_COMMAND_TOO_LONG                                      117
#define ERR_NOT_FLASHBIN_COMMAND                                  122
#define ERR_NOT_ELFTOSECTION_COMMAND				              122
#define ERR_NOT_SIZEOFSECTION_COMMAND				              122
#define ERR_NOT_FLASHSREC_COMMAND                                 122
#define ERR_NOT_PROC_IN_FILE_COMMAND				              122
#define ERR_INVALID_FILE_FORMAT									(-10)
#define ERR_UNEXPECTED_END_OF_FILE                                100
#define ERR_F_NULL_FILE                                           101
#define ERR_F_SEEK                                                102
#define ERR_NOT_FLASHWRCHUNK_COMMAND                              122
#define ERR_INVALID_ADDRESS                                       132
#define ERR_INVALID_CHUNKSIZE                                     123
#define ERR_INVALID_OFFSET                                        124
#define ERR_ADDRESS_TOO_LONG									  125
#define ERR_INVALID_REL_OFFSET									  126

#define ERR_INVALID_HEX_FORMAT                                    144
#define ERR_INVALID_HEX_RECORD                                    145
#define ERR_INVALID_SREC_FORMAT                                   146

//The index of the chunk is invalid 
#define ERR_INVALID_CHUNK_INDEX									  (-126)
#define ERR_NOT_FLASHPREPCHUNK_COMMAND							  127
#define ERR_INVALID_SECTIONNUM									  (-125)
#define ERR_SECTIONNUM_TOO_LONG									  129
// error messages
#define MESSAGE_ERR_UNEXPECTED_END_OF_FILE                        "Unexpected end of file."
#define MESSAGE_ERR_F_NULL_IP_FILE                                "Could not find/open file '%s'."
#define MESSAGE_ERR_NOT_AN_ELF_FILE                               "Invalid ELF file (magic bytes)."
#define MESSAGE_ERR_INVALID_HEX_FORMAT                            "Invalid Hex Format."
#define MESSAGE_ERR_INVALID_HEX_RECORD                            "Invalid HEX Record Found."
#define MESSAGE_ERR_INVALID_SREC_FORMAT                           "Invalid S-Record Format."
#define MESSAGE_ERR_FILE_PATH_TOO_LONG                            "Invalid parameters."
#define MESSAGE_ERR_NO_ARGUMENTS_PASSED_WITH_COMMAND              "No command arguments."
#define MESSAGE_ERR_NOT_FLASHBIN_COMMAND                           "GDB command recognition failure."
#define MESSAGE_ERR_COMMAND_TOO_LONG                              "Invalid parameters."
#define MESSAGE_ERR_INVALID_FILE_NAME                             "Invalid file name."
#define GDB_CLIENT_ERROR_MESSAGE_PREFIX                           "Error: "
#define MESSAGE_ERR_NOT_FLASHWRCHUNK_COMMAND                      "GDB command recognition failure."
#define MESSAGE_ERR_INVALID_ADDRESS								  "Invalid address."
#define	MESSAGE_ERR_INVALID_CHUNKSIZE							  "Invalid chunk size"
#define MESSAGE_ERR_INVALID_OFFSET								  "Invalid offset"
#define MESSAGE_ERR_UNKNOWN_ERROR_OCURRED                         "Internal Error 02."
#define MESSAGE_ERR_ADDRESS_TOO_LONG							  "Error address too long"
#define MESSAGE_ERR_NOT_FLASHPREPCHUNK_COMMAND					  "GDB command recognition faile"
#define MESSAGE_GDBSERV_ERROR									  "GDB Server Error"
#endif   // GDBFLASHCOMMONERRORS_H_

