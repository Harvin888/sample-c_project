/****************************************************************************
       Module: gdbjcon.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, JTAG console interface
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef __LINUX
#include <windows.h>
#endif
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/jtagcon/gdbjcmd.h"
#include "gdbserver/jtagcon/gdbjmsg.h"
#include "gdbserver/gdbutil.h"

#ifdef __LINUX
#include <readline/readline.h>
#include <readline/history.h>
#include <dlfcn.h>
// DLL name
#define RDLINE_DLL_NAME "/usr/lib/libreadline.so"
#define LoadLibrary(lib)                 dlopen(lib, RTLD_NOW)
#define GetProcAddress(handle, proc)     dlsym(handle, proc)
#define FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
typedef char* (*TyReadline)(const char*);
TyReadline pfReadline = NULL;
#endif

#define MAX_INPUT_LINE              (_MAX_PATH*8)  // maximum length of input line
#define CONSOLE_COMMAND_PROMPT      "JTAG>"        // string for command prompt
#define CFILE_COMMAND_PROMPT        "JTAG:"        // string for command files

// extern variable
extern TyServerInfo tySI;

// local function prototypes
static void FilterInputString(char *pszInputString);

/****************************************************************************
     Function: GetCommandFromFile
     Engineer: Vitezslav Hola
        Input: TyCmdFiles *ptyCFiles - pointer to structure with command files
               char *pszCommand  - output buffer for next command
               uint32_t uiBufferLen - length of output buffer in bytes
       Output: int32_t - 1, if there is any new command
  Description: check structure for open files and read a new command
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
int32_t GetCommandFromFile(TyCmdFiles *ptyCFiles, char *pszCommand, uint32_t uiBufferLen)
{
   FILE **pFile;
   uint32_t uiIndex;

   assert(ptyCFiles != NULL);
   assert(pszCommand != NULL);
   assert(uiBufferLen > 0);
   strcpy(pszCommand, "");
    // get the most nested open file
   pFile = NULL;
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      if(ptyCFiles->pCFile[uiIndex] != NULL)
         {
         pFile = &(ptyCFiles->pCFile[uiIndex]);
         ptyCFiles->uiCurFileIndex = uiIndex;   // save actual file index + 1
         }
      else
         break;
      }
   if(pFile == NULL)
      return 0;   // no opened command file
   // read from file
   if(fgets(pszCommand, (int32_t)uiBufferLen, *pFile) == NULL)
      {   // test for end-of-file
      if(feof(*pFile))
         {  // last reading
         fclose(*pFile);
         *pFile = NULL;
         return 1;
         }
      else
         {  // error while reading file
         fclose(*pFile);
         *pFile = NULL;
         return 0;
         }
      }
   // we have read some command
   return 1;
}

/****************************************************************************
     Function: FilterInputString
     Engineer: Vitezslav Hola
        Input: char *pszInputString - pointer to input string
       Output: none
  Description: remove special characters ('\r', etc.) from the string,
               remove duplicated spaces
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
static void FilterInputString(char *pszInputString)
{
   char *pszString;
   int32_t bNextSpace;
   uint32_t uiLastChar;

   assert(pszInputString != NULL);
   // check for special characters and duplicate space
   pszString = pszInputString;
   bNextSpace = 1;   // remove spaces on the begining
   while(*pszString)
      {
      switch(*pszString)
         {
         case ' '  :
            if(bNextSpace)
               {  // remove (*pszString) from pszInputString
               memmove((void *)pszString, (void *)(pszString+1), strlen(pszString));
               // do not move pszString
               }
            else
               {
               bNextSpace = 1;
               pszString++;
               }
            break;
         case '\r' :
         case '\n' :
         case '\t' :
         case '\b' :
            // remove (*pszString) from pszInputString
            memmove((void *)pszString, (void *)(pszString+1), strlen(pszString));
            // do not move pszString
            break;
         default:
            pszString++;      // next character
            bNextSpace = 0;   // correct character, next space is valid
            break;
         }
      }
   // check for space at the end of the string
   uiLastChar = (uint32_t)strlen(pszInputString);
   if((uiLastChar > 0) && (pszInputString[uiLastChar - 1] == ' '))
      pszInputString[uiLastChar - 1] = '\0';
}

/****************************************************************************
     Function: RunJTAGConsole
     Engineer: Vitezslav Hola
        Input: int32_t bDeviceSpecified - 1 if device type was specified
       Output: none
  Description: execute JTAG console
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
void RunJTAGConsole(int32_t bDeviceSpecified)
{
   #ifdef __LINUX
   char *pszLine;
   #endif   
   char szCommandLine[MAX_INPUT_LINE];
   int32_t bFinishConsole, bCommandFromFile;
   TyCmdFiles tyCmdFiles;
   uint32_t uiIndex;

   NOREF(bDeviceSpecified);
   // show first message
   PrintMessage(INFO_MESSAGE, JMSG_CONSOLE_INFO);
   // initialize command files structure
   tyCmdFiles.uiCurFileIndex = 0;
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      tyCmdFiles.pCFile[uiIndex] = NULL;
      strcpy(tyCmdFiles.pszCFileNames[uiIndex], "");
      }
   // main console loop
   bFinishConsole = 0;
   bCommandFromFile = 0;
   #ifdef __LINUX
   HINSTANCE readline_hdl  = LoadLibrary(RDLINE_DLL_NAME);
   if(readline_hdl == NULL)
      pfReadline = NULL;
   else
      pfReadline = (TyReadline)GetProcAddress(readline_hdl, "readline");
   #endif
   while(!bFinishConsole)
      {
      // when getting commands from command file
      // command is already in pszCommandLine
      if(!bCommandFromFile)
         {
         #ifdef __LINUX
         if(pfReadline == NULL)
            {  // show command prompt (we cannot use PrintMessage because we need stay on the same line)
            printf(CONSOLE_COMMAND_PROMPT);
            // read user input
            if(fgets(szCommandLine, MAX_INPUT_LINE, stdin) == NULL)
               {		// check for errors
               if(!feof(stdin))
                  {
                  // show error message
                  bFinishConsole = 1;
                  continue;
                  }
               else
                  strcpy(szCommandLine, "");
               }
            }
         else
            {
            pszLine = pfReadline(CONSOLE_COMMAND_PROMPT);
            strncpy(szCommandLine, pszLine, MAX_INPUT_LINE);
            //free the buffer allocated from readline()
            if(pszLine != NULL)
               free(pszLine);
			#ifndef PFXDMIPS
            //add command to history list
            if (*szCommandLine)
               add_history (szCommandLine);
			#endif
            }
         #else
	     // show command prompt (we cannot use PrintMessage because we need stay on the same line)
         printf(CONSOLE_COMMAND_PROMPT);
         // read user input
         if(fgets(szCommandLine, MAX_INPUT_LINE, stdin) == NULL)
            {		// check for errors
            if(!feof(stdin))
               {
               // show error message
               bFinishConsole = TRUE;
               continue;
               }
            else
               strcpy(szCommandLine, "");
            }
         #endif
         // log command line if requested
         if (tySI.pDebugFile != NULL)
            {
            fprintf(tySI.pDebugFile, CONSOLE_COMMAND_PROMPT);
            fprintf(tySI.pDebugFile, "%s", szCommandLine);
            }
         }
      // filter and convert command line
      FilterInputString(szCommandLine);
      // show command from command file (not empty commands)
      if(bCommandFromFile && strcmp(szCommandLine, ""))
         {
         // print command to the command prompt
         PrintMessage(INFO_MESSAGE, "%s%s>%s", CFILE_COMMAND_PROMPT, tyCmdFiles.pszCFileNames[tyCmdFiles.uiCurFileIndex], szCommandLine);
         }
      // pass command line to parser
      DoConsoleCommand(szCommandLine, &tyCmdFiles, &bFinishConsole);
      // check for request for commands from files
      if(!bFinishConsole)
         bCommandFromFile = GetCommandFromFile(&tyCmdFiles, szCommandLine, MAX_INPUT_LINE);
      }
   // some files could be still open
   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      if(tyCmdFiles.pCFile[uiIndex] != NULL)
         fclose(tyCmdFiles.pCFile[uiIndex]);
      }
}

