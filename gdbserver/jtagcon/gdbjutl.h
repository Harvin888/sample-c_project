/****************************************************************************
       Module: gdbjutl.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, header file for JTAG utility functions
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBJUTL_H_
#define GDBJUTL_H_

// JTAG scanchain restrictions
#define MAX_SCANCHAIN_LENGTH                  96          // maximum length for scanchain restricted in Opella fw
#define MAX_LONGSCANCHAIN_LENGTH            4096          // maximum length for int32_t scanchain
// uint32_t size (number of bits in uint32_t)
#define ULONG_BITS                           (sizeof(uint32_t) * 8)

// size of scanchain buffers in unsigned longs (32 bits/uint32_t)
#define LONGSCANCHAIN_ULONG_SIZE             (MAX_LONGSCANCHAIN_LENGTH / ULONG_BITS)
#define SCANCHAIN_ULONG_SIZE                 (MAX_SCANCHAIN_LENGTH / ULONG_BITS)
// maximum device on scanchain (2 bits is minimum length for IR)
// to test scan chain, we shift uint32_t (32 bits) -> number of taps = max_scan_chain_length - 32
#define MAX_TAPS_ON_SCANCHAIN                64

#define SHIR_DEFAULT_LENGTH                  5     // default number of bits for IR
#define SHDR_DEFAULT_LENGTH                  32    // default number of bits for DR

#define JTAG_IDCODE                          0x01  // IDCODE instruction for JTAG

#define MAX_WAIT_TIME_MS                     9999  // maximum wait time 9999 ms

typedef struct
{
   uint32_t ulIDCode;                          // ID code for TAP (result of IDCODE (0x01))
   uint32_t  uiIRLength;                        // IR length in bits for TAP
} TyTAPDesc;

typedef struct
{
   uint32_t uiTotalSCLength;                    // total length of scan chain (in bits)
   uint32_t uiNumberOfTAPs;                     // total number of TAP's
   uint32_t uiTotalIRLength;                     // total IR length
   TyTAPDesc    ptyTAPs[MAX_TAPS_ON_SCANCHAIN];     // description for each TAP
} TyScanChain;

int32_t TestScanChain(uint32_t *puiNumberOfTAPs); 
int32_t GetMulticore(uint32_t *pulnumberCore, uint32_t *pulcoreinfo);
int32_t SetMulticore(uint32_t ulnumberCore, uint32_t *pulcoreinfo);
int32_t CompareUlongs(uint32_t *pulUlongs1, uint32_t *pulUlongs2, uint32_t uiNumberOfBits);
int32_t Parse_SHIR_DR_Params(char *pszParamString, uint32_t *pulOutValue, uint32_t ulScanChainUlongs, uint32_t *puiLengthBits, int32_t bIRSelect);
int32_t ParseReadParams(char *pszParamString, uint32_t *pulObjectType, uint32_t *pulStartAddress, uint32_t *pulEndAddress);
int32_t ParseWriteParams(char *pszParamString, uint32_t *pulValue, uint32_t *pulObjectType, 
                     uint32_t *pulStartAddress, uint32_t *pulEndAddress);
int32_t PrintUlongArray(char *pszString, uint32_t uiBufferLen, const uint32_t *pulData, uint32_t uiNumberOfUlongs);
void ShiftUlongArray(uint32_t *pulUlongArray, uint32_t uiNumberOfUlongs, uint32_t uiNumberOfBits, int32_t bShiftToRight);
int32_t AnalyzeScanChain(uint32_t *pulResponse, uint32_t uiMaxNumberOfBits, TyScanChain *ptyScanChainDesc, uint32_t uiTAPs);
void SetIRCommand(uint32_t *pulBuffer, uint32_t uiNumberOfUlongs, TyScanChain *ptyScanChainDesc, 
                  uint32_t uiTAPIndex, uint32_t ulInstruction);
int32_t CheckAddressAllignment(uint32_t ulAddress, uint32_t ulDataSize);
void GetBypassInstruction(uint32_t *pulBypassInstruction, uint32_t *pulLength);

#endif // GDBJUTL_H_

