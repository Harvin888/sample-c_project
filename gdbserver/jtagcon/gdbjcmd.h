/****************************************************************************
       Module: gdbjcmd.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, header file for commands in JTAG console

Date           Initials    Description
06-Mar-2006    VH          initial
30-Apr-2008    VK	   Modified for ARM
****************************************************************************/


#if _WINCONS
#include <windows.h>
#endif

#define MAX_NESTED_CFILE            10             // max depth for calling cfile
#define MAX_CFILE_NAME              256            // max cfile name length
#ifdef ARM
#define MIN_JTAGFreq                1
#define MAX_JTAGFreq                100000
#endif
typedef struct
{
    FILE *(pCFile[MAX_NESTED_CFILE]);
	uint32_t uiCurFileIndex;
    char pszCFileNames[MAX_NESTED_CFILE][MAX_CFILE_NAME];
} TyCmdFiles;

void    DoConsoleCommand(const char *pszCommandLine, TyCmdFiles *ptyCmdFiles, int32_t *pbFinish);


