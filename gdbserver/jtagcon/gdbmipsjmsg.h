/****************************************************************************
       Module: gdbjmsg.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, 
               JTAG console output strings and error messages 
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBMIPSJMSG_H_
#define GDBMIPSJMSG_H_

// minimum buffer length in bytes, every message must be shorter than this length
#define MSG_MIN_BUFFER_LEN          256      

// JTAG console output strings
#define JMSG_CONSOLE_INFO           "Entering JTAG low-level console mode (type \"help\" for supported commands)."
#define JMSG_UKNOWN_COMMAND         "Undefined command: \"%s\". Try \"help\"."
#define JMSG_WRONG_PARAMS           "Invalid command parameters. Type \"help\" for more information."

// Header for listing of supported commands
// functions JCMD_xxxHelp are responsible for dysplaying all lines
#define JMSG_COMMAND_HELP_1         "Supported commands are:"
// help descriptions
#define JMSG_COMMAND_HELP_HELP      "help                        Display supported commands."
#define JMSG_COMMAND_QUIT_HELP      "quit                        Exit program."
#define JMSG_COMMAND_SHIR_HELP      "shir <val> <len>            Shift value into IR and show value shifted out."
#define JMSG_COMMAND_SHIR_HELP_2    "                            <val> specifies value in hex format."
#define JMSG_COMMAND_SHIR_HELP_3    "                            <len> number of bits to shift (default is 5 or "
#define JMSG_COMMAND_SHIR_HELP_4    "                                  previous value)."
#define JMSG_COMMAND_SHDR_HELP      "shdr <val> <len>            Shift value into DR and show value shifted out."
#define JMSG_COMMAND_SHDR_HELP_2    "                            <val> specifies value in hex format."
#define JMSG_COMMAND_SHDR_HELP_3    "                            <len> number of bits to shift (default is 32 or "
#define JMSG_COMMAND_SHDR_HELP_4    "                                  previous value)."
#define JMSG_COMMAND_CFILE_HELP     "cfile <filename>            Run commands from command file."
#define JMSG_COMMAND_CFILE_HELP_2   "                            <filename> specifies command file name."
#define JMSG_COMMAND_JTFREQ_HELP    "jtfreq <freq>               Set JTAG frequency in MHz."
#define JMSG_COMMAND_JTFREQ_HELP_2  "                            <freq> for Opella-XD is:"
#define JMSG_COMMAND_JTFREQ_HELP_3  "                                   from 100kHz to 100MHz"
#define JMSG_COMMAND_JTFREQ_HELP_4  "                            <freq> for Opella is:"
#define JMSG_COMMAND_JTFREQ_HELP_5  "                                   24, 12, 8, 6, 4, 3, 2 or 1(default)."
#define JMSG_COMMAND_CHKSC_HELP     "chksc                       Check scan chain and display contents."
#define JMSG_COMMAND_TSTSC_HELP     "tstsc                       Test scan chain by shifting out patterns."
#define JMSG_COMMAND_READ_HELP      "r <a> <add1> T <add2>       Read memory location(s)."
#define JMSG_COMMAND_READ_HELP_2    "                            <a> specifies memory access b|h|w (byte, half-word"
#define JMSG_COMMAND_READ_HELP_3    "                               or word)."
#define JMSG_COMMAND_READ_HELP_4    "                            <add1> start address in hex format."
#define JMSG_COMMAND_READ_HELP_5    "                            <add2> end address in hex format."
#define JMSG_COMMAND_WRITE_HELP     "w <a> <val> <add1> T <add2> Write value to memory location(s)."
#define JMSG_COMMAND_WRITE_HELP_2   "                            <a> specifies memory access b|h|w (byte, half-word "
#define JMSG_COMMAND_WRITE_HELP_3   "                               or word)."
#define JMSG_COMMAND_WRITE_HELP_4   "                            <val> value to be written in hex format."
#define JMSG_COMMAND_WRITE_HELP_5   "                            <add1> start address in hex format."
#define JMSG_COMMAND_WRITE_HELP_6   "                            <add2> end address in hex format."
#define JMSG_COMMAND_WAIT_HELP      "wait <delayms>              Wait for given time in ms."
#define JMSG_COMMAND_WAIT_HELP_2    "                            <delayms> from range <0,9999>."

// not supported commands - just for testing purpose
#define JMSG_COMMAND_RESET_HELP     "reset                       Reset target device."
                                                                                     
// CFILE messages
// errors
#define JMSG_COMMAND_CFILE_EOPEN    "Cannot open command file."      
#define JMSG_COMMAND_CFILE_ENESTED  "Cannot open more than %d nested files."

// SHDR/SHIR message
#define JMSG_COMMAND_SHIR_RESULT    " IR => %s"
#define JMSG_COMMAND_SHDR_RESULT    " DR => %s"
// errors
#define JMSG_COMMAND_SHIR_ERROR     "Cannot execute SHIR command."               // error message (with prefix Error: )
#define JMSG_COMMAND_SHDR_ERROR     "Cannot execute SHDR command."               // error message (with prefix Error: )
#define JMSG_COMMAND_SHIR_ELEN      "Number of bits shifted into IR must be from range <1,%d>."
#define JMSG_COMMAND_SHDR_ELEN      "Number of bits shifted into DR must be from range <1,%d>."

// CHKSC messages
#define JMSG_COMMAND_CHKSC_ERROR_01 "Unable to detect TAPs on scan chain."       // error message (with prefix Error: )
#define JMSG_COMMAND_CHKSC_DEVS     " Found %d TAP(s) on scan chain."
#define JMSG_COMMAND_CHKSC_IRLEN    " Total scan chain length is %d bits."
#define JMSG_COMMAND_CHKSC_HEADER1  " TAP number   IR length        IDCODE  "
#define JMSG_COMMAND_CHKSC_HEADER2  "   %02d            %2d            0x%08X     "

// Read / Write messages
#define JMSG_COMMAND_RW_ERROR_IML   "Invalid memory location."
#define JMSG_COMMAND_RW_ERROR_AA    "Invalid address allignment."
#define JMSG_COMMAND_READ_HEADER    " Address       Value"
#define JMSG_COMMAND_READ_STEP_B    " 0x%08X    0x%02X "
#define JMSG_COMMAND_READ_STEP_H    " 0x%08X    0x%04X "
#define JMSG_COMMAND_READ_STEP_W    " 0x%08X    0x%08X "
#define JMSG_COMMAND_READ_ERROR     "Cannot read from requested location."       // error message (with prefix Error: )
#define JMSG_COMMAND_WRITE_ERROR    "Cannot write to requested location."        // error message (with prefix Error: )
#define JMSG_COMMAND_WRITE_W        "Word 0x%08X written to memory from 0x%08X to 0x%08X."
#define JMSG_COMMAND_WRITE_H        "Half-word 0x%04X written to memory from 0x%08X to 0x%08X."
#define JMSG_COMMAND_WRITE_B        "Byte 0x%02X written to memory from 0x%08X to 0x%08X."

// JTFREQ messages
#define JMSG_COMMAND_JTFREQ_CURRENT "Current JTAG frequency is %s."
#define JMSG_COMMAND_JTFREQ_VALUES  "Invalid frequency. Type \"help\" to get supported values."
#define JMSG_COMMAND_JTFREQ_SET     "JTAG frequency set to %s."
#define JMSG_COMMAND_JTFREQ_FAILED  "Cannot set new JTAG frequency."              // error message (with prefix Error: )


// TSTSC messages
#define JMSG_COMMAND_TSTSC_START    "Testing scan chain by shifting out 0xAA, 0xCC, 0x55 and 0x33 patterns."
#define JMSG_COMMAND_TSTSC_FAILED   "Test failed - problem on scan chain."      // error message (with prefix Error: )
#define JMSG_COMMAND_TSTSC_PASSED   "Test passed - %d TAP(s) detected on scan chain."

// WAIT messages
#define JMSG_COMMAND_WAIT_RANGE     "Delay must be from range <0,%d> ms."
#define JMSG_COMMAND_WAIT_WAITING   "Waiting for %d ms."

// error messages
#define JMSG_ERROR_INSUFFICIENT_MEM "Not enough memory to allocate buffer."      // error message (with prefix Error: )

#endif // GDBMIPSJMSG_H_

