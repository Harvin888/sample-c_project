/****************************************************************************
       Module: gdbjutl.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, JTAG utility functions

Date           Initials    Description
10-Mar-2006    VH          initial
****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "/opellaxd/gdbserver/gdbglob.h" 
#include "/opellaxd/gdbserver/gdbxml.h"
#include "/opellaxd/gdbserver/gdbty.h"
#include "/opellaxd/gdbserver/gdbopt.h"
#include "/opellaxd/gdbserver/gdbcfg.h"
#include "/opellaxd/gdbserver/gdbutil.h"
#include "/opellaxd/gdbserver/jtagcon/gdbjutl.h"
#include "/opellaxd/gdbserver/gdblow.h"


// local function prototypes and definitions


// local type definitions


// local variables
// initialized IR and DR length (number of bits)
uint32_t uiLastIRLength = SHIR_DEFAULT_LENGTH;
uint32_t uiLastDRLength = SHDR_DEFAULT_LENGTH;
// extern variables
extern int32_t bMIPSSupport;						// TRUE if device is specified
extern TyServerInfo tySI;

/****************************************************************************
     Function: Parse_SHIR_DR_Params
     Engineer: Vitezslav Hola
        Input: const char *pszParamString  : string with parameters of SHIR or SHDR command
               uint32_t *pulOutValue  : pointer to uint32_t buffer for value
                                             length MUST be at least SCANCHAIN_ULONG_SIZE * ulongs
               uint32_t *puiLengthBits : pointer to value for number of bits to shift
               int32_t bIRSelect              : TRUE if IR register, FALSE for DR register
       Output: int32_t, TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: analyse string with parameters for SHDR and SHIR commands
               SHxR <val> <length>
Date           Initials    Description
10-Mar-2006    VH          Initial
****************************************************************************/

int32_t Parse_SHIR_DR_Params(const char *pszParamString,
                          uint32_t *pulOutValue, uint32_t ulScanChainUlongs, 
                          uint32_t *puiLengthBits, int32_t bIRSelect)
{
   uint32_t ulLocLength;
   char *pszLocValue, *pszLocLength;
   uint32_t uiStringPosition, uiUlongIndex, uiUlongPosition;
   unsigned char ucTempValue;
   int32_t bValid;

   assert(pszParamString != NULL);
   assert(pulOutValue != NULL);
   assert(puiLengthBits != NULL);
   assert(ulScanChainUlongs != 0);

   ulLocLength = 1;           // at least 1 bit
   // set default values for length (use last values)
   if(bIRSelect)
      ulLocLength = uiLastIRLength;
   else
      ulLocLength = uiLastDRLength;

   // format of string must be as follows
   // "0Xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbb"
   // where a's are hex numbers for value
   // and b's are decimal value for length - this parameter is optional so it could be empty
   // optionaly, value for length could be also in hex format (0Xbbbbbbbbbb)
   // length must be less or equal as MAX_SCANCHAIN_LENGTH and at least 1
   
   pszLocLength = strstr(pszParamString, " ");				// get first space (between params)

   // check for second parameter and se pszLocValue to last char of value parameter
   if(pszLocLength != NULL)
      {
      pszLocLength++;                                       // shift after space
      // check if it is last parameters
      if(strstr(pszLocLength, " ") != NULL)
         return FALSE;
      ulLocLength = StrToUlong(pszLocLength, &bValid);
	  if(!bValid)
	     return FALSE;
      // set pszLocValue before space between params
      if(pszLocLength == pszParamString)
         return FALSE;
      pszLocValue = pszLocLength-2;				// we have to shift before space !
      }
   else
      {     // just value was specified
      pszLocValue = (char *)(&(pszParamString[strlen(pszParamString)-1]));
      }
   // we have valid length, now check value
   // format is "0Xaaaaaaa", so at least 0X0
   if((strlen(pszParamString) < 3) || (pszParamString[0] != '0') || (pszParamString[1] != 'X'))
      return FALSE;

   uiStringPosition = (uint32_t)(pszLocValue - pszParamString);
   uiUlongIndex = 0;                   // index in uint32_t array 0..SCANCHAIN_ULONG_SIZE-1
   uiUlongPosition = 0;                // position inside uint32_t 0..7
   if(uiStringPosition < 2)
      return FALSE;
   *puiLengthBits = ulLocLength;
   // set uint32_t array to zero
   memset((void *)pulOutValue, 0x00, sizeof(uint32_t) * ulScanChainUlongs);
   while(uiStringPosition > 0)
      {
      switch(*pszLocValue)
         {
         case '0':
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':
            ucTempValue = (unsigned char)((*pszLocValue) - '0');
            break;

         case 'A' : 
         case 'B' : 
         case 'C' : 
         case 'D' : 
         case 'E' : 
         case 'F' : 
            ucTempValue = (unsigned char)(10 + ((*pszLocValue) - 'A'));
            break;

         case 'X' :     // terminating char, must be at position 1
            if(uiStringPosition != 1)
               return FALSE;
            else
               return TRUE;               // everything done, valid parameters
         default:
               return FALSE;
         }
      
	  // check ucTempValue and position in buffer (ignore other chars)
      if((ucTempValue != 0) && (uiUlongIndex < ulScanChainUlongs))
         {
         // add next 4 bits
         pulOutValue[uiUlongIndex] |= (((uint32_t)(ucTempValue)) << (uiUlongPosition*4));
         }

      // move inside the buffer
      if((++uiUlongPosition) > 7)
         {
         uiUlongIndex++;
         uiUlongPosition = 0;
         }

      pszLocValue--;
      uiStringPosition--;
      }
   return FALSE;
}

/****************************************************************************
     Function: ParseReadParams
     Engineer: Vitezslav Hola
        Input: char *pszParamString           : string with parameters
               uint32_t *pulObjectSize   : pointer to object size (1 = byte, 2 = halfword, 4 = word)
               uint32_t *pulStartAddress : start address of the location
               uint32_t *pulEndAddress   : end address of the location
       Output: int32_t, TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: analyse parameters for Read commands
Date           Initials    Description
15-Mar-2006    VH          Initial
****************************************************************************/
int32_t ParseReadParams(char *pszParamString, uint32_t *pulObjectSize,
                     uint32_t *pulStartAddress, uint32_t *pulEndAddress)
{
   char *pszParams[6];
   uint32_t uiIndex;
   int32_t bValid;

   // Read command has following parameters
   // B|H|W  - select object size (byte, halfword, word)
   // <add1> - starting address, must be in this format 0xAAAAAAAA
   // T      - separator between start and end address
   // <add2> - end address, must be in this format 0xAAAAAAAA
   // note: T <add2> could be ommited for a single access

   // all characters must be converted to uppercase

   assert(pszParamString != NULL);
   assert(pulObjectSize != NULL);
   assert(pulStartAddress != NULL);
   assert(pulEndAddress != NULL);

   // set pointers to each parameter string (parameter string will be modified !!!)
   for(uiIndex=0; uiIndex < 6; uiIndex++)
      pszParams[uiIndex] = NULL;
   pszParams[0] = pszParamString;
   for(uiIndex=1; uiIndex < 6; uiIndex++)
      {
      pszParams[uiIndex] = strstr(pszParams[uiIndex-1], " ");        // find next space
      if(pszParams[uiIndex] != NULL)
         *pszParams[uiIndex]++ = 0;                                  // separate strings
      else
         break;                                                      // no more params  
      }

   // check B|H|W parameter
   if(strlen(pszParams[0]) != 1)
      return FALSE;

   switch(*(pszParams[0]))
      {
      case 'B' : 
         *pulObjectSize = 1;                       // access by bytes
         break;
      case 'H' : 
         *pulObjectSize = 2;                       // access by halfwords
         break;
      case 'W' : 
         *pulObjectSize = 4;                       // access by words
         break;
      default:
         return FALSE;
      }

   // check start address
   if(pszParams[1] == NULL)
      return FALSE;
   *pulStartAddress = StrToUlong(pszParams[1], &bValid);
   if(!bValid)
      return FALSE;

   // check for separator, if no sep, just single access
   if(pszParams[2] == NULL)
      {
      *pulEndAddress = *pulStartAddress;
      return TRUE;
      }
   else
      {
      if((strlen(pszParams[2]) != 1) || (*pszParams[2] != 'T'))
         return FALSE;
      }

   // check for end address
   if(pszParams[3] == NULL)
      return FALSE;                 // there was separator but no end address
   *pulEndAddress = StrToUlong(pszParams[3], &bValid);
   if(!bValid)
      return FALSE;

   // check for other params, must be NULL
   if((pszParams[4] != NULL) || (pszParams[5] != NULL))
      return FALSE;

   return TRUE;
}

/****************************************************************************
     Function: ParseWriteParams
     Engineer: Vitezslav Hola
        Input: char *pszParamString     : string with parameters
               uint32_t *pulValue        : pointer to value to be written to location
               uint32_t *pulObjectSize   : pointer to object size (1 = byte, 2 = halfword, 4 = word)
               uint32_t *pulStartAddress : start address of the location
               uint32_t *pulEndAddress   : end address of the location
       Output: int32_t, TRUE if parsing was successful, FALSE if there were wrong parameters
  Description: analyse parameters for Read commands
Date           Initials    Description
15-Mar-2006    VH          Initial
****************************************************************************/
int32_t ParseWriteParams(char *pszParamString, uint32_t *pulValue, uint32_t *pulObjectSize, 
                      uint32_t *pulStartAddress, uint32_t *pulEndAddress)
{
   char *pszParams[6];
   uint32_t uiIndex;
   int32_t bValid;

   // Write commands have following parameters
   // B|H|W  - select object size (byte, halfword, word)
   // <val>  - value to be written to location, for B lower byte is used, for H lower halfword is used
   //          other bits are ignored, must be in this format 0xAAAAAAAA
   // <add1> - starting address, must be in this format 0xAAAAAAAA
   // T      - separator between start and end address
   // <add2> - end address, must be in this format 0xAAAAAAAA
   // note: T <add2> could be ommited for a single access

   // all characters must be converted to uppercase

   assert(pszParamString != NULL);
   assert(pulValue != NULL);
   assert(pulObjectSize != NULL);
   assert(pulStartAddress != NULL);
   assert(pulEndAddress != NULL);

   // set pointers to each parameter string (parameter string will be modified !!!)
   for(uiIndex=0; uiIndex < 6; uiIndex++)
      pszParams[uiIndex] = NULL;
   pszParams[0] = pszParamString;
   for(uiIndex=1; uiIndex < 6; uiIndex++)
      {
      pszParams[uiIndex] = strstr(pszParams[uiIndex-1], " ");        // find next space
      if(pszParams[uiIndex] != NULL)
         *pszParams[uiIndex]++ = 0;                                  // separate strings
      else
         break;                                                      // no more params  
      }

   // check for value
   if(pszParams[1] == NULL)
      return FALSE;
   *pulValue = StrToUlong(pszParams[1], &bValid);
   if(!bValid)
      return FALSE;

   // check B|H|W parameter
   if(strlen(pszParams[0]) != 1)
      return FALSE;

   switch(*(pszParams[0]))
      {
      case 'B' : 
         *pulObjectSize = 1;                       // access by bytes
         *pulValue &= 0x0000000ff;                 // mask byte
         break;
      case 'H' : 
         *pulObjectSize = 2;                       // access by halfwords
         *pulValue &= 0x00000ffff;                 // mask halfword
         break;
      case 'W' : 
         *pulObjectSize = 4;                       // access by words
         break;
      default:
         return FALSE;
      }

   // check start address
   if(pszParams[2] == NULL)
      return FALSE;
   *pulStartAddress = StrToUlong(pszParams[2], &bValid);
   if(!bValid)
      return FALSE;

   // check for separator, if no sep, just single access
   if(pszParams[3] == NULL)
      {
      *pulEndAddress = *pulStartAddress;
      return TRUE;
      }
   else
      {
      if((strlen(pszParams[3]) != 1) || (*pszParams[3] != 'T'))
         return FALSE;
      }

   // check for end address
   if(pszParams[4] == NULL)
      return FALSE;                 // there was separator but no end address
   *pulEndAddress = StrToUlong(pszParams[4], &bValid);
   if(!bValid)
      return FALSE;

   // check for other params, must be NULL
   if(pszParams[5] != NULL)
      return FALSE;

   return TRUE; 
}

/****************************************************************************
     Function: PrintUlongArray
     Engineer: Vitezslav Hola
        Input: char *pszString              : buffer for output string
               uint32_t uiBufferLen     : buffer size in bytes
               const uint32_t *pulData : pointer to data
               uint32_t uiNumberOfUlongs : number of ulongs to print
       Output: int32_t, false if buffer size is insufficient
  Description: print value of array of ulongs into the string
               print 
Date           Initials    Description
10-Mar-2006    VH          Initial
****************************************************************************/
int32_t PrintUlongArray(char *pszString, uint32_t uiBufferLen, const uint32_t *pulData, uint32_t uiNumberOfUlongs)
{
   uint32_t uiIndex;
   char pszTmpString[12];           // buffer for 1 uint32_t (8chars + '\0')

   assert(pszString != NULL);
   assert(uiBufferLen > 0);
   assert(pulData != NULL);

   if(uiNumberOfUlongs < 1)
      uiNumberOfUlongs = 1;

   strcpy(pszString, "");
   // check if buffer len is sufficient (8 chars/uint32_t + "0x" + terminating char)
   if((uiNumberOfUlongs * 8 + 3) > (uint32_t)uiBufferLen)
      return FALSE;
   strcpy(pszString, "0x");
   for(uiIndex=0; uiIndex < uiNumberOfUlongs; uiIndex++)
      {
      sprintf(pszTmpString, "%08lx", pulData[uiNumberOfUlongs - (uiIndex + 1)]);
      strcat(pszString, pszTmpString);
      }
   return TRUE;
}

/****************************************************************************
     Function: PrintUlongArray
     Engineer: Vitezslav Hola
        Input: uint32_t *pulUlongArray  : array of ulongs
               uint32_t uiNumberOfUlongs : number of ulongs in array
               uint32_t uiNumberOfBits   : number of bits to be shifted
               int32_t bShiftToRight            : TRUE when shifting right (FALSE for left)
       Output: none
  Description: shift array of ulongs to right/left (maximum number of  bits to shift is 31)
Date           Initials    Description
12-Mar-2006    VH          Initial
****************************************************************************/
void ShiftUlongArray(uint32_t *pulUlongArray, uint32_t uiNumberOfUlongs,
                     uint32_t uiNumberOfBits, int32_t bShiftToRight)
{
   uint32_t ulTempValue, ulTempShift;
   uint32_t uiActUlong;

   assert(pulUlongArray != NULL);

   // check range of parameters, do nothing if incorrect
   if((uiNumberOfUlongs < 1) || (uiNumberOfBits < 1) || (uiNumberOfBits > 31))
      return;

   ulTempShift = 0;
   if(bShiftToRight)
      {
      // shifting right
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
         {
         ulTempValue = pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] & (0xffffffff >> (32 - uiNumberOfBits));
         pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] = (pulUlongArray[(uiNumberOfUlongs-1) - uiActUlong] >> uiNumberOfBits) 
                                                              | ulTempShift;
         ulTempShift = ulTempValue << (32 - uiNumberOfBits);
         }
      }
   else
      {
      // shifting left
      for(uiActUlong = 0; uiActUlong < uiNumberOfUlongs; uiActUlong++)
         {
         ulTempValue = pulUlongArray[uiActUlong] & (0xffffffff << (32 - uiNumberOfBits));
         pulUlongArray[uiActUlong] = (pulUlongArray[uiActUlong] << uiNumberOfBits) 
                                                              | ulTempShift;
         ulTempShift = ulTempValue >> (32 - uiNumberOfBits);
         }
      }
}

/****************************************************************************
     Function: AnalyzeScanChain
     Engineer: Vitezslav Hola
        Input: uint32_t *pulResponse    : array of ulongs
               uint32_t uiMaxNumberOfBits : maximaum number of bits in scan chain
               TyScanChain *ptyScanChainDesc : pointer to structure for scan chain description
               uint32_t uiTAPs : number of TAPs on SC
       Output: int32_t : TRUE if analysis is successful, FALSE when there is incorrect value of response
  Description: analyze scan chain from value returned after (SHIR 0xff..ff)
               in response, LSB MUST be 1 (LSB from scanchain must start on bit 0)
Date           Initials    Description
13-Mar-2006    VH          Initial
****************************************************************************/
int32_t AnalyzeScanChain(uint32_t *pulResponse, uint32_t uiMaxNumberOfBits, TyScanChain *ptyScanChainDesc,
                      uint32_t uiTAPs)
{
   int32_t bLastBitOne;
   uint32_t uiBitIndex;
   uint32_t uiActTAPNumber;
   
   assert(pulResponse != NULL);
   assert(uiMaxNumberOfBits > 0);
   assert(ptyScanChainDesc != NULL);

   ptyScanChainDesc->uiNumberOfTAPs = uiTAPs;
   ptyScanChainDesc->uiTotalSCLength = 0;

   if(uiTAPs < 1)
      return TRUE;                     // there is nothing to test

   // analyze response
   // 111111111...11111111100..00100....01
   // no TAPs      | TAP(last-1) | TAP(last) |
   // each TAP starts with 01 (at least 2 bits in IR), scanchain ends when getting 11

   // check first bit (must be 1)
   if(!(pulResponse[0] & 0x1))
      return FALSE;                    // wrong response

   uiActTAPNumber = uiTAPs;            // last device on scan chain goes first
   bLastBitOne = TRUE;                 // first bit is one, so set flag and start with second bit
   ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].uiIRLength = 1;
   ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].ulIDCode = 0x00000000;

   for(uiBitIndex=1; uiBitIndex < uiMaxNumberOfBits; uiBitIndex++)
      {
      if((pulResponse[uiBitIndex / ULONG_BITS] >> (uiBitIndex % ULONG_BITS)) & 0x1)
         {
         // current bit is 1
         // check for 11
         if(bLastBitOne)
            {
            // end of scan chain, just signal success
            return TRUE;
            }
         else
            {
            bLastBitOne = TRUE;
            // add information about last TAP
            ptyScanChainDesc->uiTotalSCLength += ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].uiIRLength;
            // starting next TAP
            if(uiActTAPNumber > 0)
               uiActTAPNumber--;
			if(uiActTAPNumber > 0)
               {
               ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].uiIRLength = 1;
               ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].ulIDCode = 0x00000000;
               }
            }
         }
      else
         {
         // current bit is 0
         // just extend actual IR length
         if(uiActTAPNumber > 0)
            {
            ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].uiIRLength++;
            bLastBitOne = FALSE;
            }
         }
      }
   // check if last bit was 0 (if scan chain length == uiMaxNumberOfBits), just add last TAP
   if((!bLastBitOne) && (uiActTAPNumber > 0))
      {
      ptyScanChainDesc->uiTotalSCLength += ptyScanChainDesc->ptyTAPs[uiActTAPNumber-1].uiIRLength;
      }

   return TRUE;
}


/****************************************************************************
     Function: SetIRCommand
     Engineer: Vitezslav Hola
        Input: uint32_t *pulBuffer : array of ulongs, will be passed to scan chain
               uint32_t uiNumberOfUlongs : number of ulongs for command
               TyScanChain *ptyScanChainDesc : pointer to structure with scan chain description
               uint32_t uiTAPIndex : index of selected TAP (in ptyScanChainDesc
               uint32_t ulInstruction : code to IR (max. length 32 bits)
       Output: none
  Description: set IR command for selected TAP, does not change commands for other TAPs 
Date           Initials    Description
29-Mar-2006    VH          Initial
****************************************************************************/
void SetIRCommand(uint32_t *pulBuffer, uint32_t uiNumberOfUlongs, TyScanChain *ptyScanChainDesc,
                  uint32_t uiTAPIndex, uint32_t ulInstruction)
{
   uint32_t uiStartBit, uiIndex;

   assert(pulBuffer != NULL);
   assert(uiNumberOfUlongs > 0);
   assert(ptyScanChainDesc != NULL);

   if(uiTAPIndex >= ptyScanChainDesc->uiNumberOfTAPs)
      return;           // wrong index
   if((uiNumberOfUlongs * ULONG_BITS) < ptyScanChainDesc->uiTotalSCLength)
      return;           // small buffer

   // mask instruction code
   ulInstruction &= ~(0xffffffff << ptyScanChainDesc->ptyTAPs[uiTAPIndex].uiIRLength);
   // get first bit (go from last TAP to selected one)
   uiStartBit = 0;
   for(uiIndex=ptyScanChainDesc->uiNumberOfTAPs; uiIndex > 0; uiIndex--)
      {
      if((uiIndex-1) == uiTAPIndex)
         break;
      else
         uiStartBit += ptyScanChainDesc->ptyTAPs[uiIndex-1].uiIRLength;
      }

   // process all relevant bits from instruction code
   for(uiIndex=0; uiIndex <  ptyScanChainDesc->ptyTAPs[uiTAPIndex].uiIRLength; uiIndex++)
      {
      // set or clear 1 bit in IR command
      if(ulInstruction & 0x1)
         pulBuffer[uiStartBit / ULONG_BITS] |=   (uint32_t)(0x00000001 << (uiStartBit % ULONG_BITS));
	  else
         pulBuffer[uiStartBit / ULONG_BITS] &= ~((uint32_t)(0x00000001 << (uiStartBit % ULONG_BITS)));

      uiStartBit++;
      ulInstruction >>=1;
      }
}


/****************************************************************************
     Function: GetBypassInstruction
     Engineer: Vitezslav Hola
        Input: uint32_t *pulBypassInstruction : pointer to array of ulongs
               (at least SCANCHAIN_ULONG_SIZE)
               uint32_t *pulLength : pointer to uint32_t for length of IR
       Output: none
  Description: determine bypass instruction for scanchain and its length
Date           Initials    Description
27-Mar-2006    VH          Initial
****************************************************************************/
void GetBypassInstruction(uint32_t *pulBypassInstruction, uint32_t *pulLength)
{
   assert(pulBypassInstruction != NULL);
   assert(pulLength != NULL);

   // use different bypass instructions for multicore device
   memset((void *)pulBypassInstruction, 0xff, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   if(bMIPSSupport && tySI.tyProcConfig.Processor._MIPS.DWInfo.ucMultiCoreNumCores)
      {     // device was specified with at least one core
      uint32_t ulIndex, ulBypassInstruction, ulInstLen;
      *pulLength = 0;
      for(ulIndex=0; ulIndex<tySI.tyProcConfig.Processor._MIPS.DWInfo.ucMultiCoreNumCores; ulIndex++)
         {
         // add next core bypass instruction (max. size of bypass instruction is 32 bits)
         ulBypassInstruction = tySI.tyProcConfig.Processor._MIPS.DWInfo.tyMultiCoreDeviceInfo[ulIndex].ulBypassCode;
         ulInstLen = tySI.tyProcConfig.Processor._MIPS.DWInfo.tyMultiCoreDeviceInfo[ulIndex].ulIRLength;
         ShiftUlongArray(pulBypassInstruction, SCANCHAIN_ULONG_SIZE, ulInstLen, FALSE);
         pulBypassInstruction[0] |= (ulBypassInstruction & ~(0xffffffff << ulInstLen));
         *pulLength += ulInstLen;
         }
      }
   else
      {     // no device specified, use 1's as bypass code, max number of bits
      *pulLength = MAX_SCANCHAIN_LENGTH;
      }
}



/****************************************************************************
     Function: TestScanChain
     Engineer: Vitezslav Hola
        Input: uint32_t *puiNumberOfTAPs : pointer to value for number of TAPs on SC
       Output: int32_t : TRUE if scan chain is OK
  Description: test scanchain, shifts patterns 0xaa, 0x55, 0xcc, 0x33
               checks number of TAPs detected in scanchain
Date           Initials    Description
27-Mar-2006    VH          Initial
****************************************************************************/
int32_t TestScanChain(uint32_t *puiNumberOfTAPs)
{
   uint32_t uiShiftIndex;
   uint32_t pulBypassValue[SCANCHAIN_ULONG_SIZE];
   uint32_t pulAAPattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pul55Pattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pulCCPattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pul33Pattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pul00Pattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValue1[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValue2[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValue3[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValue4[SCANCHAIN_ULONG_SIZE];
   uint32_t ulTotalIRLength;

   assert(puiNumberOfTAPs != NULL);

   // define bypass pattern, 0xAA pattern and 0x55 pattern
   memset((void *)pulBypassValue, 0xff, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pulAAPattern, 0xaa, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul55Pattern, 0x55, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pulCCPattern, 0xcc, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul33Pattern, 0x33, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul00Pattern, 0x00, SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));

   // get bypass instruction code and scanchain length
   GetBypassInstruction(pulBypassValue, &ulTotalIRLength);

   // first shift bypass to IR and shift 0's to DR, ignore this step
   LOW_ScanIR(pulBypassValue, pulOutValue1, ulTotalIRLength);
   LOW_ScanDR(pul00Pattern, pulOutValue1, MAX_SCANCHAIN_LENGTH);

   // shift bypass to IR (DR == 1 bit) and shift AA pattern to DR
   LOW_ScanIR(pulBypassValue, pulOutValue1, ulTotalIRLength);
   LOW_ScanDR(pulAAPattern, pulOutValue1, MAX_SCANCHAIN_LENGTH);

   // shift bypass to IR (DR == 1 bit) and shift 55 pattern to DR
   LOW_ScanIR(pulBypassValue, pulOutValue2, ulTotalIRLength);
   LOW_ScanDR(pul55Pattern, pulOutValue2, MAX_SCANCHAIN_LENGTH);

   // shift bypass to IR (DR == 1 bit) and shift CC pattern to DR
   LOW_ScanIR(pulBypassValue, pulOutValue3, ulTotalIRLength);
   LOW_ScanDR(pulCCPattern, pulOutValue3, MAX_SCANCHAIN_LENGTH);

   // shift bypass to IR (DR == 1 bit) and shift 33 pattern to DR
   LOW_ScanIR(pulBypassValue, pulOutValue4, ulTotalIRLength);
   LOW_ScanDR(pul33Pattern, pulOutValue4, MAX_SCANCHAIN_LENGTH);

   // now shift results till get patterns, number of shifts == number of TAPs
   *puiNumberOfTAPs = 0;
   for(uiShiftIndex=0; uiShiftIndex < MAX_TAPS_ON_SCANCHAIN; uiShiftIndex++)
      {
      if(CompareUlongs(pulAAPattern, pulOutValue1, MAX_SCANCHAIN_LENGTH - uiShiftIndex) &&
         CompareUlongs(pul55Pattern, pulOutValue2, MAX_SCANCHAIN_LENGTH - uiShiftIndex) && 
         CompareUlongs(pulCCPattern, pulOutValue3, MAX_SCANCHAIN_LENGTH - uiShiftIndex) && 
         CompareUlongs(pul33Pattern, pulOutValue4, MAX_SCANCHAIN_LENGTH - uiShiftIndex))
         {
         // both patterns shifted by the same number of bits revealed on output
         *puiNumberOfTAPs = uiShiftIndex;
         return TRUE;
         }
      // shift all arrays
      ShiftUlongArray(pulOutValue1, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      ShiftUlongArray(pulOutValue2, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      ShiftUlongArray(pulOutValue3, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      ShiftUlongArray(pulOutValue4, SCANCHAIN_ULONG_SIZE, 1, TRUE);
      }
   // we could not detect scan chain correctly
   return FALSE;
}

/****************************************************************************
     Function: CompareUlongs
     Engineer: Vitezslav Hola
        Input: uint32_t *pulUlongs1   : first array of ulongs to compare
               uint32_t *pulUlongs2   : second array of ulongs to compare
               uint32_t uiNumberOfBits : number of bits from ulongs to compare
       Output: int32_t : TRUE if ulongs are identical in specified bits
  Description: compares to uint32_t arrays with specified number of bits
Date           Initials    Description
18-Mar-2006    VH          Initial
****************************************************************************/
int32_t CompareUlongs(uint32_t *pulUlongs1, uint32_t *pulUlongs2, uint32_t uiNumberOfBits)
{
   uint32_t uiIndex;
   uint32_t ulMask;

   assert(pulUlongs1 != NULL);
   assert(pulUlongs2 != NULL);

   for(uiIndex=0; uiIndex < (uiNumberOfBits / 32); uiIndex++)
      {     // compare whole ulongs
      if(*pulUlongs1++ != *pulUlongs2++)
         return FALSE;
      }

   uiNumberOfBits = uiNumberOfBits % 32;
   // compare rest of uint32_t
   if(uiNumberOfBits == 0)
      return TRUE;
   ulMask = 0xffffffff >> (32 - uiNumberOfBits);
   if((*pulUlongs1 & ulMask) != (*pulUlongs2 & ulMask))
      return FALSE;

   return TRUE;
}

/****************************************************************************
     Function: CheckAddressAllignment
     Engineer: Vitezslav Hola
        Input: uint32_t ulAddress   : memory address
               uint32_t ulDataSize  : data size
       Output: int32_t : FALSE if address is not allign
  Description: check if address is allign
Date           Initials    Description
22-Mar-2006    VH          Initial
****************************************************************************/
int32_t CheckAddressAllignment(uint32_t ulAddress, uint32_t ulDataSize)
{
   switch(ulDataSize)
      {
      case 1 :                // byte - any address is correct
         return TRUE;

      case 2 :                // half-word
         if(ulAddress % 2)
            return FALSE;
         else
            return TRUE;

      case 4 :
         if(ulAddress % 4)
            return FALSE;
         else
            return TRUE;

      default:
         break;
      }
   return FALSE;            // if data size is not correct, return FALSE
}


