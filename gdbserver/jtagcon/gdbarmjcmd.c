/****************************************************************************
       Module: gdbarmjcmd.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ARM implementation of commands for JTAG console
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
26-Apr-2208    VK             Modified for ARM
****************************************************************************/
#if _WINCONS
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/jtagcon/gdbjcmd.h"
#include "gdbserver/jtagcon/gdbjmsg.h"
#include "gdbserver/jtagcon/gdbjutl.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbutil.h"
typedef void (TyCommandExecute) (char *pszParams);
typedef void (TyCommandHelpFunc)(void);

// local function prototypes and definitions
void JCMD_QuitExecute(char *pszParams);
void JCMD_QuitHelp(void);
void JCMD_CFileExecute(char *pszParams);
void JCMD_CFileHelp(void);
void JCMD_HelpExecute(char *pszParams);
void JCMD_HelpHelp(void);
void JCMD_SHIRExecute(char *pszParams);
void JCMD_SHIRHelp(void);
void JCMD_SHDRExecute(char *pszParams);
void JCMD_SHDRHelp(void);
void JCMD_TSTSCExecute(char *pszParams);
void JCMD_TSTSCHelp(void);
void JCMD_JTFREQExecute(char *pszParams);
void JCMD_JTFREQHelp(void);
void JCMD_ResetExecute(char *pszParams);
void JCMD_ResetHelp(void);
void JCMD_CHKSCExecute(char *pszParams);
void JCMD_CHKSCHelp(void);
void JCMD_ReadExecute(char *pszParams);
void JCMD_ReadHelp(void);
void JCMD_WriteExecute(char *pszParams);
void JCMD_WriteHelp(void);
void JCMD_WaitExecute(char *pszParams);
void JCMD_WaitHelp(void);

void JC_SetJtagFrequency(void);
extern char*           GetRegName              (uint32_t ulGdbRegNumber);

extern TyServerInfo tySI;
extern uint32_t uiLastIRLength;
extern uint32_t uiLastDRLength;


// local type definitions
typedef struct _TyConsoleCommand {
   const char                 * pszCommandName;    // name of function
   TyCommandExecute     * pfCommandFunc;     // executing a command
   TyCommandHelpFunc    * pfHelpFunc;        // function to display a help
} TyConsoleCommand;


// local variables

// --------------------------------------
// JTAG Console Command Table
// use UPPER characters to define strings
// --------------------------------------
static TyConsoleCommand tyConsoleCommands[] =
{
   {                                    // command HELP
   (const char*)"HELP",
   JCMD_HelpExecute,
   JCMD_HelpHelp
   },
   {                                    // command CHKSC
   (const char*)"CHKSC",
   JCMD_CHKSCExecute,
   JCMD_CHKSCHelp
   },
   {                                    // command SHIR
   (const char*)"SHIR",
   JCMD_SHIRExecute,
   JCMD_SHIRHelp
   },
   {                                    // command SHDR
   (const char*)"SHDR",
   JCMD_SHDRExecute,
   JCMD_SHDRHelp
   },
   {                                    // command TSTSC
   (const char*)"TSTSC",
   JCMD_TSTSCExecute,
   JCMD_TSTSCHelp
   },
   {                                    // command JTFREQ
   (const char*)"JTFREQ",
   JCMD_JTFREQExecute,
   JCMD_JTFREQHelp
   },

   {                                    // command RESET
   (const char*)"TAPRES",
   JCMD_ResetExecute,			
   JCMD_ResetHelp,
   },
   {                                    // command CFILE
   (const char*)"CFILE",
   JCMD_CFileExecute,
   JCMD_CFileHelp
   },
   {                                    // command R (Read)
   (const char*)"R",
   JCMD_ReadExecute,
   JCMD_ReadHelp
   },
   {                                    // command W (Write)
   (const char*)"W",
   JCMD_WriteExecute,
   JCMD_WriteHelp
   },
   {                                    // command WAIT
   (const char*)"WAIT",
   JCMD_WaitExecute,
   JCMD_WaitHelp
   },
   {                                    // command QUIT
   (const char*)"QUIT",
   JCMD_QuitExecute,
   JCMD_QuitHelp
   }
};

// JTAG supported frequencies


static TyScanChain tyScanChainInfo;       // description of the scan chain  
static int32_t *pbFinishFlag = NULL;          // pointer to flag (quit uses it)
static TyCmdFiles *ptyLocCmdFiles = NULL; // pointer to command files structure (cfile uses it)


/****************************************************************************
     Function: JCMD_QuitExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute QUIT command
Date           Initials    Description
06-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_QuitExecute(char *pszParams)
{
   assert(pszParams != NULL);
   // no parameters for quit
   if(!strcmp(pszParams, ""))
      *pbFinishFlag = 1;
   else
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
}

/****************************************************************************
     Function: JCMD_QuitHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for QUIT command
Date           Initials    Description
06-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_QuitHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_QUIT_HELP);
}

/****************************************************************************
     Function: JCMD_CFileExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute CFILE command
Date           Initials    Description
06-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_CFileExecute(char *pszParams)
{
   FILE *pFile;
   uint32_t uiIndex;
   int32_t bFreeSlot;
   
   assert(pszParams != NULL);
   
   // we require 1 parameter as filename
   if(!strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   else
      {

      // open file for reading
      pFile = fopen(pszParams, "rt");

      if(pFile == NULL)
         {
         PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CFILE_EOPEN);
         return;
         }
      }
	
	// check nested files and find free pointer
   bFreeSlot = 0;

   for(uiIndex=0; uiIndex < MAX_NESTED_CFILE; uiIndex++)
      {
      if(ptyLocCmdFiles->pCFile[uiIndex] == NULL)
         {
         bFreeSlot = TRUE;
         ptyLocCmdFiles->pCFile[uiIndex] = pFile;
         if((strlen(pszParams) + 4) > MAX_CFILE_NAME)
            {
            strncpy(ptyLocCmdFiles->pszCFileNames[uiIndex], pszParams, MAX_CFILE_NAME-4);
            ptyLocCmdFiles->pszCFileNames[uiIndex][MAX_CFILE_NAME-4] = 0;
            strcat(ptyLocCmdFiles->pszCFileNames[uiIndex], "...");
            }
            else
               strcpy(ptyLocCmdFiles->pszCFileNames[uiIndex], pszParams);
            break;
         }
      }
   if(!bFreeSlot)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CFILE_ENESTED, MAX_NESTED_CFILE);
      fclose(pFile);
      }
}


/****************************************************************************
     Function: JCMD_CFileHelp
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: show help for CFILE command
Date           Initials    Description
08-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_CFileHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CFILE_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CFILE_HELP_2);
}
/****************************************************************************
     Function: JCMD_HelpExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute HELP command
Date           Initials    Description
08-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_HelpExecute(char *pszParams)
{
    uint32_t uiIndex;
    TyConsoleCommand *ptyCurrentCommand;

    assert(pszParams != NULL);

    if(strcmp(pszParams, ""))
       {
       PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
       return;
       }

    // show help header
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_HELP_1);

    // show help for all supported commands
    for(uiIndex=0; uiIndex < (sizeof(tyConsoleCommands) / sizeof(TyConsoleCommand)); uiIndex++)
        {
        // call help function for each command, add concatenate all strings
        ptyCurrentCommand = &(tyConsoleCommands[uiIndex]);
        if((ptyCurrentCommand->pfHelpFunc != NULL))
           (ptyCurrentCommand->pfHelpFunc)();
        }
}


/****************************************************************************
     Function: JCMD_HelpHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for HELP command
Date           Initials    Description
06-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_HelpHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_HELP_HELP);
}

/****************************************************************************
     Function: DoConsoleCommand
     Engineer: Vitezslav Hola
        Input: const char *pszCommandLine  : Input string with command
               TyCmdFiles *ptyCmdFiles     : Pointer to structure with command files
               int32_t *pbFinish               : Pointer to int32_t variable      
       Output: none
  Description: get command line, parse it and call particular function
               for special commands (quit and cfile), fill passed structure and variable
               to signal request for reading file or to exit 
Date           Initials    Description
06-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void DoConsoleCommand(const char *pszCommandLine, TyCmdFiles *ptyCmdFiles, int32_t *pbFinish)
{
    uint32_t uiNumberOfCommands, uiIndex;
    TyConsoleCommand *ptyCurrentCommand;
    char *pszParsedCommandLine = NULL;
    char *pszCommandParams;
    char *pszOriginCommand = NULL;
    int32_t bValidCommand;

    assert(pbFinish != NULL);
    assert(ptyCmdFiles != NULL);
    assert(pszCommandLine != NULL);

    // initialize local pointers
    ptyLocCmdFiles = ptyCmdFiles;
    pbFinishFlag = pbFinish;
   *pbFinishFlag = 0;

    // split command line, find first space
    pszParsedCommandLine = strdup(pszCommandLine);              // duplicate string (free must be called at the end)
    if(pszParsedCommandLine == NULL)
        {
        // show critical error
      *pbFinishFlag = 1;
        return;                                   // terminate console and stop parsing command
        }
    pszCommandParams = strstr(pszParsedCommandLine, " ");
    if(pszCommandParams == NULL)
        {       // no parameters (no space), set pointer to empty string (terminating char)
        pszCommandParams = &(pszParsedCommandLine[strlen(pszParsedCommandLine)]);
        }
    else
        {       // split string
        *pszCommandParams = '\0';           // cut pszParsedCommandLine
        pszCommandParams++;                 // move after space
        }

    // save original command
    pszOriginCommand = strdup(pszParsedCommandLine);
    if(pszOriginCommand == NULL)
        {

        // show critical error
        *pbFinishFlag = TRUE;
        free(pszParsedCommandLine);
        return;                                   // terminate console and stop parsing command
        }

    // convert command string to uppercase
    StringToUpr(pszParsedCommandLine);

    // start to parse commands
    bValidCommand = 0;
    uiNumberOfCommands = sizeof(tyConsoleCommands) / sizeof(TyConsoleCommand);
    for(uiIndex=0; uiIndex < uiNumberOfCommands; uiIndex++)
        {
        ptyCurrentCommand = &(tyConsoleCommands[uiIndex]);
        if((!strcmp(pszParsedCommandLine, ptyCurrentCommand->pszCommandName)))
            {
            // valid command found, now execute it
            assert(ptyCurrentCommand->pfCommandFunc != NULL);
            (ptyCurrentCommand->pfCommandFunc)(pszCommandParams);
          bValidCommand = 1;
            break;                          // stop cycle
            }
        }

    // display warning message for undefined command
    if((!bValidCommand) && (strcmp(pszParsedCommandLine, "")))
       {
       // restrict length of pszOriginCommand to _MAX_PATH*2, cut command and add "..."
       if((strlen(pszOriginCommand) + 4) > (_MAX_PATH*2))
          {
          pszOriginCommand[_MAX_PATH*2 - 4] = '\0';            // cut original command
          strcat(pszOriginCommand, "...");                     // and add ...
          }
       PrintMessage(INFO_MESSAGE, JMSG_UKNOWN_COMMAND, pszOriginCommand);
       }

    // free memory after strdup
    if(pszParsedCommandLine != NULL)
        free(pszParsedCommandLine);
    if(pszOriginCommand != NULL)
        free(pszOriginCommand);
}


/****************************************************************************
     Function: JCMD_SHIRExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute SHIR command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_SHIRExecute(char *pszParams)
{
   unsigned char pszUlongString[8+SCANCHAIN_ULONG_SIZE*8];
   uint32_t pulOutBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t pulInBuffer[SCANCHAIN_ULONG_SIZE];
   uint32_t ulNumberOfBits;
   uint32_t uiUlongsToShow;

   assert(pszParams != NULL);

   // clear both buffers (fill with zero's
   memset((void*)pulOutBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
   memset((void*)pulInBuffer, 0x00, sizeof(uint32_t) * SCANCHAIN_ULONG_SIZE);
   // check parameters
   StringToUpr(pszParams);
   if(!Parse_SHIR_DR_Params(pszParams, pulOutBuffer, SCANCHAIN_ULONG_SIZE, &ulNumberOfBits, TRUE))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check number of bits to shift
   if((ulNumberOfBits < 1) || (ulNumberOfBits > MAX_SCANCHAIN_LENGTH))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_ELEN, MAX_SCANCHAIN_LENGTH);
      return;
      }

   if(LOW_ScanIR(pulOutBuffer, pulInBuffer, ulNumberOfBits) != GDBSERV_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_SHIR_ERROR);
      return;
      }
   else
      {
      // how many ulongs to show ?
      uiUlongsToShow = ulNumberOfBits / 32;
      if(ulNumberOfBits % 32)
         uiUlongsToShow++;
      // print result (data shifted out from IR)
      (void)PrintUlongArray((char *)pszUlongString, 8+SCANCHAIN_ULONG_SIZE*8, pulInBuffer, uiUlongsToShow);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_RESULT, pszUlongString);
      // update last used value
      uiLastIRLength = ulNumberOfBits;
      }
}

/****************************************************************************
     Function: JCMD_SHDRExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams - params related to the command
       Output: none
  Description: execute SHDR command
Date           Initials    Description
08-Feb-2008    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_SHDRExecute(char *pszParams)
{
   unsigned char pszUlongString[8+LONGSCANCHAIN_ULONG_SIZE*8];
   uint32_t pulOutBuffer[LONGSCANCHAIN_ULONG_SIZE];
   uint32_t pulInBuffer[LONGSCANCHAIN_ULONG_SIZE];
   uint32_t ulNumberOfBits;
   uint32_t uiUlongsToShow;
   int32_t bValue;

   assert(pszParams != NULL);

   // clear both buffers (fill with zero's
   memset((void*)pulOutBuffer, 0x00, sizeof(uint32_t) * LONGSCANCHAIN_ULONG_SIZE);
   memset((void*)pulInBuffer, 0x00, sizeof(uint32_t) * LONGSCANCHAIN_ULONG_SIZE);
   // check parameters
   StringToUpr(pszParams);
   if(!Parse_SHIR_DR_Params(pszParams, pulOutBuffer, LONGSCANCHAIN_ULONG_SIZE, &ulNumberOfBits, FALSE))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check number of bits to shift
   if((ulNumberOfBits < 1) || (ulNumberOfBits > MAX_LONGSCANCHAIN_LENGTH))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_ELEN, MAX_LONGSCANCHAIN_LENGTH);
      return;
      }

   bValue = LOW_ScanDR(pulOutBuffer, pulInBuffer, ulNumberOfBits);

   if(bValue != GDBSERV_NO_ERROR)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_SHDR_ERROR);
      return;
      }
   else
      {
      // how many ulongs to show ?
      uiUlongsToShow = ulNumberOfBits / 32;
      if(ulNumberOfBits % 32)
         uiUlongsToShow++;
      (void)PrintUlongArray((char *)pszUlongString, 8+LONGSCANCHAIN_ULONG_SIZE*8, pulInBuffer, uiUlongsToShow);
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_RESULT, pszUlongString);
      // update last used value
      uiLastDRLength = ulNumberOfBits;
      }
}


/****************************************************************************
     Function: JCMD_SHIRHelp
     Engineer: Vitezslav Hola
        Input: none  
       Output: none
  Description: show help for SHIR command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_SHIRHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHIR_HELP_4);
}


/****************************************************************************
     Function: JCMD_SHDRHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for SHDR command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_SHDRHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_SHDR_HELP_4);
}

/****************************************************************************
     Function: JCMD_ResetExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams - params related to the command
       Output: none
  Description: execute RESET command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_ResetExecute(char *pszParams)
{
   assert(pszParams != NULL);

   // check parameters
   if(!strcmp(pszParams, ""))
      {
      (void)LOW_ResetTAP();
       PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TAPRES_HELP);
      }
}

/****************************************************************************
     Function: JCMD_ResetHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for reset command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_ResetHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RESET_HELP);
}

/****************************************************************************
     Function: JCMD_JTFREQExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams - params related to the command
       Output: none
  Description: execute JTFREQ command
Date           Initials    Description
19-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_JTFREQExecute(char *pszParams)
{
   uint32_t    ulMultiplier = 1000;
   uint32_t    ulFrequency  = 0;
   int32_t     bValid       = FALSE;
   char    *pszUnit;
   int32_t      iLen;
   char     szFrequency[64];

   assert(pszParams != NULL);

   // check parameters
   // without parameter, just display current frequency
   if(!strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_SET_ARM, tySI.szJtagFreq);
      return;
      }

   strncpy(szFrequency, pszParams, sizeof(szFrequency)-1);
   szFrequency[sizeof(szFrequency)-1] = '\0';

   // test for RTCK 
   if ((strcasecmp(szFrequency, "rtck") == 0) ||
       (strcasecmp(szFrequency, "RTCK") == 0))  
      {
      tySI.bJtagRtck = 1;
      }
   else
      {
      tySI.bJtagRtck = 0;
      // test for kHz or MHz (default) at end of number...
      if ((iLen = (int32_t)strlen(szFrequency)) > 3)
         {
         pszUnit = szFrequency + iLen - 3;
         if (strcasecmp(pszUnit, "khz") == 0)
            {
            pszUnit[0]   = '\0';
            ulMultiplier = 1;
            }
         else if (strcasecmp(pszUnit, "mhz") == 0)
            {
            pszUnit[0]   = '\0';
            ulMultiplier = 1000;
            }
         }
   
      ulFrequency = StrToUlong(szFrequency, &bValid);
   
      if (!bValid)
         {
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_VALUES);
         return;
         }
   
      tySI.ulJtagFreqKhz = ulFrequency * ulMultiplier;
      if((tySI.ulJtagFreqKhz<MIN_JTAGFreq) || (tySI.ulJtagFreqKhz>MAX_JTAGFreq))
         {
             PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_JTFREQ_VALUES); 
             return;

         }
   }
  

   // set selected frequency
   if(GDBSERV_NO_ERROR == LOW_SetJtagFreq())
      {
       JC_SetJtagFrequency();
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_SET, tySI.szJtagFreq);
      }
   else
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_JTFREQ_FAILED);
}

/****************************************************************************
     Function: JCMD_JTFREQHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for JTFREQ command
Date           Initials    Description
19-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_JTFREQHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_JTFREQ_HELP_3);
}


/****************************************************************************
     Function: JCMD_CHKSCExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute CHKSC command
Date           Initials    Description
13-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_CHKSCExecute(char *pszParams)
{
   uint32_t ulTAPIndex=0;
   uint32_t uiTAPs = 0;
   uint32_t pulAAPattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pul55Pattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pulCCPattern[SCANCHAIN_ULONG_SIZE];
   uint32_t pul33Pattern[SCANCHAIN_ULONG_SIZE];
   
   uint32_t pulOutValueAA[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValue55[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValueCC[SCANCHAIN_ULONG_SIZE];
   uint32_t pulOutValue33[SCANCHAIN_ULONG_SIZE];

   uint32_t ulTotalIRlengthAA = 0;
   uint32_t ulTotalIRlength55 = 0;
   uint32_t ulTotalIRlengthCC = 0;
   uint32_t ulTotalIRlength33 = 0;

   uint32_t pulIDCode[MAX_TAPS_ON_SCANCHAIN]= {0};
     
   uint32_t uiShiftIndex;
   uint32_t ulNonJTAGDeviceFound;
   uint32_t ulDelay;

   assert(pszParams != NULL);

   //there should be no parameters for chksc
   if(strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }
   NOREF(tyScanChainInfo);

   tyScanChainInfo.uiNumberOfTAPs = 0;
   tyScanChainInfo.uiTotalSCLength = 0;

   (void)LOW_ResetTAP(); 
    for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}
   (void)LOW_ResetTAP(); 

    for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}

   // first test scan chain and determine number of TAPs
   if(!TestScanChain(&uiTAPs))
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CHKSC_ERROR_01);
	  return;
      }
   else
      {
      tyScanChainInfo.uiNumberOfTAPs = uiTAPs;
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_RESULT, tyScanChainInfo.uiNumberOfTAPs);
      }

   //OK..now find the total IR length
   memset((void *)pulAAPattern,0xaa,SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul55Pattern,0x55,SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pulCCPattern,0xcc,SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));
   memset((void *)pul33Pattern,0x33,SCANCHAIN_ULONG_SIZE * sizeof(uint32_t));

   (void)LOW_ResetTAP();
   for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}
   (void)LOW_ResetTAP(); 
   (void)LOW_ScanIR(pulAAPattern, pulOutValueAA, MAX_SCANCHAIN_LENGTH);

   (void)LOW_ResetTAP();
   for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}
   (void)LOW_ResetTAP(); 
   (void)LOW_ScanIR(pul55Pattern, pulOutValue55, MAX_SCANCHAIN_LENGTH);

   (void)LOW_ResetTAP();
   for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}
   (void)LOW_ResetTAP(); 
   (void)LOW_ScanIR(pul33Pattern, pulOutValue33, MAX_SCANCHAIN_LENGTH);

   (void)LOW_ResetTAP();
   for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}
   (void)LOW_ResetTAP(); 
   (void)LOW_ScanIR(pulCCPattern, pulOutValueCC, MAX_SCANCHAIN_LENGTH);

   for(uiShiftIndex=0; uiShiftIndex < MAX_TAPS_ON_SCANCHAIN; uiShiftIndex++)
       {
            if(CompareUlongs(pulAAPattern, pulOutValueAA, (MAX_SCANCHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlengthAA = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValueAA, SCANCHAIN_ULONG_SIZE, 1, TRUE);
       }

   for(uiShiftIndex=0; uiShiftIndex < MAX_TAPS_ON_SCANCHAIN; uiShiftIndex++)
       {
            if(CompareUlongs(pul55Pattern, pulOutValue55, (MAX_SCANCHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlength55 = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValue55, SCANCHAIN_ULONG_SIZE, 1, TRUE);
       }

   for(uiShiftIndex=0; uiShiftIndex < MAX_TAPS_ON_SCANCHAIN; uiShiftIndex++)
       {
            if(CompareUlongs(pulCCPattern, pulOutValueCC, (MAX_SCANCHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlengthCC = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValueCC, SCANCHAIN_ULONG_SIZE, 1, TRUE);
       }
  
   for(uiShiftIndex=0; uiShiftIndex < MAX_TAPS_ON_SCANCHAIN; uiShiftIndex++)
       {
            if(CompareUlongs(pul33Pattern, pulOutValue33, (MAX_SCANCHAIN_LENGTH - uiShiftIndex))) 
             {
             ulTotalIRlength33 = uiShiftIndex;
             break;
             }
            ShiftUlongArray(pulOutValue33, SCANCHAIN_ULONG_SIZE, 1, TRUE);
       }
   if(((ulTotalIRlengthAA != ulTotalIRlength55) || 
       (ulTotalIRlengthAA != ulTotalIRlengthCC) || 
       (ulTotalIRlengthAA != ulTotalIRlength33))) 
   {
       PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_CHKSC_TOT_IR_FAIL);
       return;
   } 
   else
   {
       tyScanChainInfo.uiTotalIRLength = ulTotalIRlengthAA;
       PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_TOT_IR_SUCC,tyScanChainInfo.uiTotalIRLength);
   }

   //Find the total ID code of all devices in the scan chain 
   (void)LOW_ResetTAP(); 
   for(ulDelay=0; ulDelay < 0xFFFFF; ulDelay++){}
   (void)LOW_ResetTAP(); 
   (void)LOW_ScanDR(pulAAPattern, pulIDCode,(uiTAPs * ULONG_BITS));


   //Adjust for JTAG Compliant device is also done

   ulNonJTAGDeviceFound = 0;

   for(ulTAPIndex=0; ulTAPIndex < tyScanChainInfo.uiNumberOfTAPs; ulTAPIndex++)
     {
       if(pulIDCode[ulTAPIndex] & (0x00000001 << ulNonJTAGDeviceFound))
       {
           if(ulNonJTAGDeviceFound == 0)
           {
               tyScanChainInfo.ptyTAPs[ulTAPIndex].ulIDCode = pulIDCode[ulTAPIndex];
           }
           else
           {
               tyScanChainInfo.ptyTAPs[ulTAPIndex + ulNonJTAGDeviceFound].ulIDCode = (((pulIDCode[ulTAPIndex] >> ulNonJTAGDeviceFound) & (0xFFFFFFFF >> ulNonJTAGDeviceFound)) |
                                                              ((pulIDCode[ulTAPIndex+1] & (0xFFFFFFFF >> (32 - ulNonJTAGDeviceFound))) << (32 - ulNonJTAGDeviceFound)));
           }
       }
       else
       {
           ulNonJTAGDeviceFound++;
           tyScanChainInfo.ptyTAPs[ulTAPIndex + ulNonJTAGDeviceFound -1].ulIDCode = 0x00000000;
           tyScanChainInfo.ptyTAPs[ulTAPIndex + ulNonJTAGDeviceFound].ulIDCode = (((pulIDCode[ulTAPIndex] >> ulNonJTAGDeviceFound) & (0xFFFFFFFF >> ulNonJTAGDeviceFound)) |
                                                             ((pulIDCode[ulTAPIndex+1] & (0xFFFFFFFF >> (32 - ulNonJTAGDeviceFound))) << (32 - ulNonJTAGDeviceFound)));
       }      
     }
	// show header
   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_HEADER3);

   for(ulTAPIndex=0; ulTAPIndex < tyScanChainInfo.uiNumberOfTAPs; ulTAPIndex++)
     {
     PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_HEADER4, 
                  ulTAPIndex,tyScanChainInfo.ptyTAPs[ulTAPIndex].ulIDCode);
     }
}

/****************************************************************************
     Function: JCMD_CHKSCHelp
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: show help for CHKSC command
Date           Initials    Description
03-Dec-2007    NCH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_CHKSCHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_CHKSC_HELP);
}

/****************************************************************************
     Function: JCMD_TSTSCExecute
     Engineer: Nikolay Chokoev
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute TSTSC command
Date           Initials    Description
03-Dec-2007    NCH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_TSTSCExecute(char *pszParams)
{
   uint32_t uiNumberOfTAPs;

   assert(pszParams != NULL);

   //there should be no parameters for tstsc
   if(strcmp(pszParams, ""))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_START);

   // test scan chain and get number of TAPs
   uiNumberOfTAPs = 0;
   if(!TestScanChain(&uiNumberOfTAPs))
      {
      PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_TSTSC_FAILED);
      }
   else
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_PASSED, uiNumberOfTAPs);
      }
}

/****************************************************************************
     Function: JCMD_TSTSCHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for TSTSC command
Date           Initials    Description
18-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_TSTSCHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_TSTSC_HELP);
}


/****************************************************************************
     Function: JCMD_ReadExecute
     Engineer: Nikolay Chokoev
        Input: char *pszParams - params related to the command
       Output: none
  Description: execute Read command
Date           Initials    Description
03-DEC-2007    NCH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_ReadExecute(char *pszParams)
{
   uint32_t ulObjectType, ulStartAddr, ulEndAddr, ulBytesToRead, ulValue;
   uint32_t uiIndex;
   unsigned char *pbReadBuffer = NULL;
//   int32_t bBigEndian;

   assert(pszParams != NULL);

   // convert to upper case and check parameters
   StringToUpr(pszParams);
   if(!ParseReadParams(pszParams, 
                       &ulObjectType, 
                       &ulStartAddr, 
                       &ulEndAddr))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check address range
   if(ulStartAddr > ulEndAddr)
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RW_ERROR_IML);
      return;
      }

   // allocate buffer for data
   ulBytesToRead = ulEndAddr - ulStartAddr;

   pbReadBuffer = (unsigned char *)malloc(ulBytesToRead);
   if(pbReadBuffer == NULL)
      {
      PrintMessage(ERROR_MESSAGE, JMSG_ERROR_INSUFFICIENT_MEM);
      return;
      }

   switch (ulObjectType)
      {
      case 1:
         //Memory
         // check address allignment - just start address, end address is always adjusted regarding data size
         if(ulStartAddr%4)
            {
            PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RW_ERROR_AA);
            free(pbReadBuffer);
            return;
            }
         if(!LOW_ReadMemory(ulStartAddr, ulBytesToRead, pbReadBuffer))
            {
            // error while reading
            PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_READ_ERROR);
            free(pbReadBuffer);
            return;
            }
         break;
      case 2:
         //Core registers
         if(!LOW_ReadRegister(ulStartAddr, (uint32_t *)pbReadBuffer))
            {
            // error while reading
            PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_READ_ERROR);
            free(pbReadBuffer);
            return;
            }
         break;
      case 4:
         //AUX registers
         if(!LOW_ReadRegister(ulStartAddr, (uint32_t *)pbReadBuffer))
            {
            // error while reading
            PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_READ_ERROR);
            free(pbReadBuffer);
            return;
            }
         break;
      default:
         break;
      }

   if(ulObjectType == 2)
      {
              // show header
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_REG_HEADER);
   // show data
      for(uiIndex = 0; uiIndex < ulBytesToRead; uiIndex+=4)
         {
         ulValue = (*((uint32_t *)&pbReadBuffer[uiIndex]));
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_REG_VALUE,GetRegName(ulStartAddr),ulValue);
         ulStartAddr +=4;                       // move actual address
         }
      }
   if(ulObjectType == 1)
      {
                  // show header
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_HEADER);
   // show data
      for(uiIndex = 0; uiIndex < ulBytesToRead; uiIndex+=4)
         {
         ulValue = (*((uint32_t *)&pbReadBuffer[uiIndex]));
         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_STEP_W,ulStartAddr,ulValue);
         ulStartAddr +=4;                       // move actual address
         }
      }
   // free memory after malloc
   free(pbReadBuffer);
}

/****************************************************************************
     Function: JCMD_ReadHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for Read command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_ReadHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_HELP_4);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_READ_HELP_5);
}

/****************************************************************************
     Function: JCMD_WriteExecute
     Engineer: Nikolay Chokoev
        Input: char *pszParams          : params related to the command
       Output: none
  Description: execute Write command
Date           Initials    Description
03-Dec-2007    NCH          Initia
26-Apr-2208    VK          Modified for ARMl
****************************************************************************/
void JCMD_WriteExecute(char *pszParams)
{
   uint32_t ulObjectType, ulStartAddr, ulEndAddr, ulValue;
   unsigned char ucDataBuf[4];
   unsigned char ucDataBuf1[4];
   uint32_t ulTAddr;

   assert(pszParams != NULL);
   // check parameters
   StringToUpr(pszParams);
   if(!ParseWriteParams(pszParams, 
                        &ulValue, 
                        &ulObjectType, 
                        &ulStartAddr, 
                        &ulEndAddr))
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check address range
   if(ulStartAddr > ulEndAddr)
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RW_ERROR_IML);
      return;
      }

   ucDataBuf[3]= (ulValue>>24) & 0xFF;
   ucDataBuf[2]= (ulValue>>16) & 0xFF;
   ucDataBuf[1]= (ulValue>>8) & 0xFF;
   ucDataBuf[0]= ulValue & 0xFF;

   switch (ulObjectType)
      {
      case 1:
         //Memory
         // check address allignment - just start address
         if(ulStartAddr%4)
            {
            PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RW_ERROR_AA);
            return;
            }
         // check address allignment - just start address
         if(ulEndAddr%4)
            {
            PrintMessage(INFO_MESSAGE, JMSG_COMMAND_RW_ERROR_AA);
            return;
            }
         // write memory...
         for(ulTAddr=ulStartAddr; ulTAddr < (ulEndAddr); ulTAddr+=4)
            {
            if(GDBSERV_NO_ERROR != LOW_WriteMemory(ulTAddr, 4, ucDataBuf))
               {
               PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_WRITE_ERROR);
               return;
               }
            (void)LOW_ReadMemory(ulTAddr, 4, ucDataBuf1);
            if (strncmp((const char *)ucDataBuf,(const char *)ucDataBuf1,4) !=0)
               {
               PrintMessage(ERROR_MESSAGE,JMSG_COMMAND_WRITE_ERROR,ulTAddr);
               return;
               }
            }

         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_W, 
                      ulValue, ulStartAddr, ulEndAddr);
         break;

      case 2:
         //Core registers
         if(!LOW_WriteRegister(ulStartAddr, ulValue))
            {
            PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_WRITE_ERROR);
            return;
            }
//         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_W, 
//                      ulValue, ulStartAddr, ulEndAddr);
         break;

      case 4:
         //AUX register
         if(!LOW_WriteRegister(ulStartAddr, ulValue))
            {
            PrintMessage(ERROR_MESSAGE, JMSG_COMMAND_WRITE_ERROR);
            return;
            }
//         PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_W, 
//                      ulValue, ulStartAddr, ulEndAddr);
         break;
      default:
         break;
      }
//TODO: Write registers or memory


}

/****************************************************************************
     Function: JCMD_WriteHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for Write command
Date           Initials    Description
10-Mar-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_WriteHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP_2);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP_3);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP_4);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP_5);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP_6);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WRITE_HELP_7);
}

/****************************************************************************
     Function: JCMD_WaitExecute
     Engineer: Vitezslav Hola
        Input: char *pszParams - params related to the command
       Output: none
  Description: execute Wait command
Date           Initials    Description
02-Aug-2006    VH          Initial
13-Oct-2006	   DM		   Correct bug with wait range change and getting to work
					       on Linux
26-Apr-2208    VK          Modified for ARM					   
****************************************************************************/
void JCMD_WaitExecute(char *pszParams)
{
   int32_t iDelayTime;

   assert(pszParams != NULL);
   // check parameters
   StringToUpr(pszParams);
   

   if(sscanf(pszParams, "%d", &iDelayTime) != 1)
      {
      PrintMessage(INFO_MESSAGE, JMSG_WRONG_PARAMS);
      return;
      }

   // check range
   if ((iDelayTime > MAX_WAIT_TIME_MS) || (iDelayTime < 0))
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_RANGE, MAX_WAIT_TIME_MS);
      return;
      }

   // just do wait
   if (iDelayTime > 0)
      {
      PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_WAITING, iDelayTime);
      #ifdef __LINUX
      usleep((uint32_t)iDelayTime*1000);
      #endif
      #ifdef _WINCONS
      Sleep((uint32_t)iDelayTime);
      #endif
      }
}

/****************************************************************************
     Function: JCMD_WaitHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: show help for Wait command
Date           Initials    Description
02-Aug-2006    VH          Initial
26-Apr-2208    VK          Modified for ARM
****************************************************************************/
void JCMD_WaitHelp(void)
{
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_HELP);
    PrintMessage(INFO_MESSAGE, JMSG_COMMAND_WAIT_HELP_2);
}

/****************************************************************************
     Function: JC_VerifyJtagFrequency
     Engineer: Nikolay Chokoev
        Input: none
       Output: TyError
  Description: Verify user selected Jtag Frequency
Date           Initials    Description
03-Dec-2007    NCH         Initial
26-Apr-2208    VK          Modified for ARM
*****************************************************************************/

void JC_SetJtagFrequency(void)
{
    tySI.ulJtagFreqHz=tySI.ulJtagFreqKhz * FREQ_KHZ(1);
   // check validity of selected frequency (may depend on probe used)
   if (tySI.tyGdbServerTarget == GDBSRVTGT_OPELLAXD)
      {  // for Opella-XD, check if using fixed or RTCK clock
      if (!tySI.bJtagRtck)
         {  // using fixed frequency, is there any restriction ?
         // check mininum frequency as 1 kHz
         if (tySI.ulJtagFreqHz < FREQ_KHZ(1))
            tySI.ulJtagFreqHz = FREQ_KHZ(1);
         // check maximum JTAG frequency and restrict it for sure
         if ((tySI.tyProcConfig.ulMaxJtagFrequency > 0) && (tySI.tyProcConfig.ulMaxJtagFrequency < tySI.ulJtagFreqHz))
            tySI.ulJtagFreqHz = tySI.tyProcConfig.ulMaxJtagFrequency;
         // now get nearest step
         if (tySI.ulJtagFreqHz > FREQ_MHZ(10))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_MHZ(1)) * FREQ_MHZ(1);          // step 1 MHz for frequency above 10 MHz
         else if ((tySI.ulJtagFreqHz <= FREQ_MHZ(10)) && (tySI.ulJtagFreqHz > FREQ_MHZ(1)))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_KHZ(100)) * FREQ_KHZ(100);      // step 100 kHz for frequency between 1 MHz and 10 MHz
         else if ((tySI.ulJtagFreqHz <= FREQ_MHZ(1)) && (tySI.ulJtagFreqHz > FREQ_KHZ(100)))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_KHZ(10)) * FREQ_KHZ(10);        // step 10 kHz for frequency between 100 kHz and 1 MHz
         else if ((tySI.ulJtagFreqHz <= FREQ_KHZ(100)) && (tySI.ulJtagFreqHz > FREQ_KHZ(10)))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_KHZ(1)) * FREQ_KHZ(1);          // step 1 kHz for frequency between 10 kHz and 100 kHz
         else
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / 100) * 100;                          // step 100 Hz for frequency below 10 kHz
         }
      }
   // we need to create also string describing JTAG frequency
   if (!tySI.bJtagRtck)
      {
      if ((tySI.ulJtagFreqHz % FREQ_MHZ(1)) == 0)
         sprintf(tySI.szJtagFreq, "%ldMHz", tySI.ulJtagFreqHz / FREQ_MHZ(1));    // frequency value in MHz
      else if ((tySI.ulJtagFreqHz % FREQ_KHZ(1)) == 0)
         sprintf(tySI.szJtagFreq, "%ldkHz", tySI.ulJtagFreqHz / FREQ_KHZ(1));    // frequency value in kHz
      else
         sprintf(tySI.szJtagFreq, "%ldHz", tySI.ulJtagFreqHz);                   // frequency value in Hz
      }
   else
      strcpy(tySI.szJtagFreq, "adaptive speed");
}





