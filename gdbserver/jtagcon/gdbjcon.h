/****************************************************************************
       Module: gdbjcon.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, JTAG console interface header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBJCON_H_
#define GDBJCON_H_

void RunJTAGConsole(int32_t bDeviceSpecified);

#endif // GDBJCON_H_

