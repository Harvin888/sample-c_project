/****************************************************************************
       Module: gdbdsp.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, Dispatch GDB commands header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBDSP_H_
#define GDBDSP_H_

int32_t DSP_Decode(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
int32_t DSP_ContinueReply(TyStopSignal tyStopSignal, char *pcOutput);

#endif   // GDBDSP_H_

