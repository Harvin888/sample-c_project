/****************************************************************************
       Module: gdbarc.h
     Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, ARC target interface header file
Date           Initials    Description
06-NOV-2007    NCH         initial
****************************************************************************/
#ifndef GDBARC_H_
#define GDBARC_H_


#define ARC_REG_COUNT            0x84 //109
#define MAX_REG_COUNT            500
#define DEFAULT_REG_COUNT        64

typedef enum
{
    RW = 0,     // Read Write
    RO = 1,     // Read only
    WO = 2,      // Write only
    NONE = 0
} TyAccessType;


typedef struct
{
   char *pszRegName;             // register name
   uint32_t ulAddrIndex;
   uint32_t ulGdbRegIndex;
   TyAccessType accessType;
} TyARCReg;


int32_t LOW_TestScanChain(void);
int32_t GetRegAddr(char *pRegName, int32_t *iRegAddr);
int32_t LOW_SetActiveTapIndex(int32_t iIndex);
int32_t Is_StoppedAtWatchpoint(void);
int32_t Low_GetMulticore(uint32_t *pulnumberCore, uint32_t *pulcoreinfo);
int32_t Low_SetMulticore(uint32_t ulnumberCore, uint32_t *pulcoreinfo);

#endif// GDBARC_H_
