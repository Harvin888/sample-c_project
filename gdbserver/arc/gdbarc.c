/****************************************************************************
       Module: gdbarc.c
     Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, ARC target interface
Date           Initials    Description
06-NOV-2007    NCH         initial
09-Sep-2009    RSB         TAP Reset Done
08-Oct-2012    SV          Get Auxiliary Register numbers from XML,if provided
****************************************************************************/
#if _WINCONS
#include <Windows.h>
#include <conio.h>
#include <windef.h>
#elif _LINUX
#include <dlfcn.h>
#define stricmp strcasecmp
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

// ARC interface header (exclude lint from these files)
//lint -save -e*
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
// Linux build change...
#include "protocol/common/types.h"
#include "arcint.h"
//lint -restore
// must be included before other headers
#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/arc/gdbarc.h"
#include "protocol/arc/arcsetup.h"
//#include "arcreg.h"

#if _WINCONS
    #define  OPELLAXD_DLL_NAME                "opxdarc.dll"
    #define  OPELLAXD_DLL_NAME_1              "C:\\AshlingOpellaXDforARC\\opxdarc.dll"
#elif _LINUX
    #define  OPELLAXD_DLL_NAME                "./opxdarc.so"
    #define  OPELLAXD_DLL_NAME_1              "./opxdarc.so"
    #define  LoadLibrary(lib)                 dlopen(lib, RTLD_LAZY)
    #define  GetProcAddress(handle, proc)     dlsym(handle, proc)
    #define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#endif

#define BAD_HEX_VALUE               0xBAD0BAD0
#define MAX_ARC_READ_MEM_AMMOUNT    8192
//#define ARC_BRK_S_INSTR             0x7FFF    // 16-bit breakpoint instruction
#define ARC_BRK_S_INSTR_B1          0x7F        // 2nd byte from 0x7FFF
#define ARC_BRK_S_INSTR_B0          0xFF        // 1st byte from 0x7FFF
//#define ARC_BRK_INSTR               0x256F003F // 32-bit breakpoint instruction
#define ARC_BRK_INSTR_B3            0x25        // 4th byte from 0x256F003F
#define ARC_BRK_INSTR_B2            0x6F        // 3rd byte from 0x256F003F
#define ARC_BRK_INSTR_B1            0x00        // 2nd byte from 0x256F003F
#define ARC_BRK_INSTR_B0            0x3F        // 1st byte from 0x256F003F

#define ARC_REG_DEBUG            (0x45)      // rw  32  Basecase  Debug Register
//#define ARC_REG_DEBUG_LD_BIT   (1 << 31)   // Load pending bit
#define ARC_REG_DEBUG_SH_BIT     (1 << 30)   // Self halt bit (flag 1)
#define ARC_REG_DEBUG_BH_BIT     (1 << 29)   // Break halt bit
#define ARC_REG_DEBUG_RA_BIT     (1 << 22)   // Reset asserted bit
                                             // 
//#define ARC_REG_DEBUG_UB_BIT   (1 << 28)   // User mode bit
#define ARC_REG_DEBUG_ZZ_BIT     (1 << 23)   // Sleeping bit
#define ARC_REG_DEBUG_IS_BIT     (1 << 11)   // Instruction step bit
#define ARC_REG_DEBUG_FH_BIT     (1 << 1)    // Force Halt bi

#define ARC_REG_STATUS           (0x40)      // r  32 ARCompact Status Register
#define ARC_REG_STATUS_H_BIT     (1 << 25)   // Halt 

#define ARC_REG_STATUS2           (0x4A)      // r  32 ARCompact Status Register
#define ARC_REG_STATUS2_H_BIT     (1 << 0)   // Halt 
// 

#define ARC_REG_IDENTITY         (0x44)      // r  32  ARC Identification Register
#define ARC_REG_MEMSUBSYS        (0xA7)      // r  Memory subsystem

#define ARC_DC_CTRL              (0x88)
#define ARC_DC_CTRL_DC_BIT       (1 << 0)
//#define ARC_DC_CTRL_LM_BIT       (1 << 7)
#define ARC_DC_CTRL_FS_BIT       (1 << 8)

#define ARC_DC_FLSH              (0x8B)
#define ARC_DC_FLSH_FL_BIT       (1 << 0)

#define ARC_DC_IVDC              (0x87)
#define ARC_DC_IVDC_IV_BIT       (1 << 0)

#define ARC_ISA_CONFIG           (0xC1)

#define ARC_CACHE_FLUSH_RETRY    2
//#define ARC_CACHE_INVAL_RETRY    5

#define IDERR_LOAD_DLL_FAILED       1
#define IDERR_DLL_NOT_LOADED        2
#define IDERR_GDI_ERROR             3
#define IDERR_ASH_SRCREGISTER       15
#define IDERR_ASH_DSTREGISTER       16
#define IDERR_ADD_BP_NO_MEMORY      17
#define IDERR_ADD_BP_INTERNAL1      18
#define IDERR_ADD_BP_DUPLICATE      19
#define IDERR_REMOVE_BP_INTERNAL1   20
#define IDERR_REMOVE_BP_INTERNAL2   21
#define IDERR_REMOVE_BP_NOT_FOUND   22
#define IDERR_DLL_GET_INTERFACE_ERR 24
#define IDERR_DLL_NULL_FUNCTION     25


//Register array definitions
//#define REGISTER_ARRAY_ALLOC_CHUNK 64

/***********Default Registers*************/
#define REG_R0                     "R0"
#define REG_R1                     "R1"
#define REG_R2                     "R2"
#define REG_R3                     "R3"
#define REG_R4                     "R4"
#define REG_R5                     "R5"
#define REG_R6                     "R6"
#define REG_R7                     "R7"
#define REG_R8                     "R8"
#define REG_R9                     "R9"
#define REG_R10                    "R10"
#define REG_R11                    "R11"
#define REG_R12                    "R12"
#define REG_R13                    "R13"
#define REG_R14                    "R14"
#define REG_R15                    "R15"
#define REG_R16                    "R16"
#define REG_R17                    "R17"
#define REG_R18                    "R18"
#define REG_R19                    "R19"
#define REG_R20                    "R20"
#define REG_R21                    "R21"
#define REG_R22                    "R22"
#define REG_R23                    "R23"
#define REG_R24                    "R24"
#define REG_R25                    "R25"
//#define REG_R26                    "R26"
//#define REG_R27                    "R27"
//#define REG_R28                    "R28"
//#define REG_R29                    "R29"
//#define REG_R30                    "R30"
//#define REG_R31                    "R31"
#define REG_R32                    "R32"
#define REG_R33                    "R33"
#define REG_R34                    "R34"
#define REG_R35                    "R35"
#define REG_R36                    "R36"
#define REG_R37                    "R37"
#define REG_R38                    "R38"
#define REG_R39                    "R39"
#define REG_R40                    "R40"
#define REG_R41                    "R41"
#define REG_R42                    "R42"
#define REG_R43                    "R43"
#define REG_R44                    "R44"
#define REG_R45                    "R45"
#define REG_R46                    "R46"
#define REG_R47                    "R47"
#define REG_R48                    "R48"
#define REG_R49                    "R49"
#define REG_R50                    "R50"
#define REG_R51                    "R51"
#define REG_R52                    "R52"
#define REG_R53                    "R53"
#define REG_R54                    "R54"
#define REG_R55                    "R55"
#define REG_R56                    "R56"
#define REG_R57                    "R57"
#define REG_R58                    "R58"
#define REG_R59                    "R59"
//#define REG_R60                    "R60"
#define REG_R61                    "R61"
//#define REG_R62                    "R62"
//#define REG_R63                    "R63"
#define REG_GP                     "GP"
#define REG_FP                     "FP"
#define REG_SP                     "SP"
#define REG_ILINK1                 "ILINK1"
#define REG_ILINK2                 "ILINK2"
#define REG_BLINK                  "BLINK"
#define REG_LPCOUNT                "LPCOUNT"
#define REG_LIMM                   "LIMM"
#define REG_PCL                    "PCL"
#define REG_UNKNOWN                "UNKNOWN"
/********************************************/

/*************Auxiliary Registers************/
#define REG_STATUS                 "STATUS"
#define REG_SEMAPHORE              "SEMAPHORE"
#define REG_LP_START               "LP_START"
#define REG_LP_END                 "LP_END"
#define REG_IDENTITY               "IDENTITY"
#define REG_DEBUG                  "DEBUG"
#define REG_PC                     "PC"
#define REG_STATUS32               "STATUS32"
#define REG_STATUS32_L1            "STATUS32_L1"
#define REG_STATUS32_L2            "STATUS32_L2"
#define REG_COUNT0                 "COUNT0"
#define REG_CONTROL0               "CONTROL0"
#define REG_LIMIT0                 "LIMIT0"
#define REG_INT_VECTOR_BASE        "INT_VECTOR_BASE"
#define REG_MACMODE                "MACMODE"
#define REG_IRQ_LV12               "IRQ_LV12"
//#define REG_COUNT1                 "COUNT1"
//#define REG_CONTROL1               "CONTROL1"
//#define REG_LIMIT1                 "LIMIT1"
//#define REG_IRQ_LEV                "IRQ_LEV"
//#define REG_IRQ_HINT               "IRQ_HINT"
//#define REG_ERET                   "ERET"
//#define REG_ERBTA                  "ERBTA"
//#define REG_ERSTATUS               "ERSTATUS"
//#define REG_ECR                    "ECR"
//#define REG_EFA                    "EFA"
//#define REG_ICAUSE1                "ICAUSE1"
//#define REG_ICAUSE2                "ICAUSE2"
//#define REG_IENABLE                "IENABLE"
//#define REG_ITRIGGER               "ITRIGGER"
//#define REG_XPU                    "XPU"
//#define REG_BTA                    "BTA"
//#define REG_BTA_L1                 "BTA_L1"
//#define REG_BTA_L2                 "BTA_L2"
//#define REG_PULSE_CANCEL           "IRQ_PULSE_CANCEL"
//#define REG_IRQ_PENDING            "IRQ_PENDING"
/**********************************************/
//Needs dynamic allocation of registers
TyARCReg          *g_ptyARCReg;
uint32_t      g_ulNumberOfRegisters;

//#define  MAX_ARC_REG  ((sizeof(ptyARCReg))/(sizeof(TyARCReg)))
struct TyFunctionTable
{
    int32_t            (*DummyFunction                )(struct TyArcInstance*);
    int32_t            (*version                      )(struct TyArcInstance*);
    const char *   (*id                           )(struct TyArcInstance*);
    void           (*destroy                      )(struct TyArcInstance*);
    const char *   (*additional_possibilities     )(struct TyArcInstance*);
    void*          (*additional_information       )(struct TyArcInstance*, unsigned);
    int32_t            (*prepare_for_new_program      )(struct TyArcInstance*, int32_t);
    int32_t            (*process_property             )(struct TyArcInstance*, const char *, const char *);
    int32_t            (*is_simulator                 )(struct TyArcInstance*);
    int32_t            (*step                         )(struct TyArcInstance*);
    int32_t            (*run                          )(struct TyArcInstance*);
    int32_t            (*read_memory                  )(struct TyArcInstance*, uint32_t, void *, uint32_t, int32_t);
    int32_t            (*write_memory                 )(struct TyArcInstance*, uint32_t, void *, uint32_t, int32_t );
    int32_t            (*read_reg                     )(struct TyArcInstance*, int32_t, uint32_t *);
    int32_t            (*write_reg                    )(struct TyArcInstance*, int32_t, uint32_t);
    unsigned       (*memory_size                  )(struct TyArcInstance*);
    int32_t            (*set_memory_size              )(struct TyArcInstance*, unsigned);
    int32_t            (*set_reg_watchpoint           )(struct TyArcInstance*, int32_t, int32_t);
    int32_t            (*remove_reg_watchpoint        )(struct TyArcInstance*, int32_t, int32_t);
    int32_t            (*set_mem_watchpoint           )(struct TyArcInstance*, uint32_t, int32_t);
    int32_t            (*remove_mem_watchpoint        )(struct TyArcInstance*, uint32_t, int32_t);
    int32_t            (*stopped_at_watchpoint        )(struct TyArcInstance*);
    int32_t            (*stopped_at_exception         )(struct TyArcInstance*);
    int32_t            (*set_breakpoint               )(struct TyArcInstance*, unsigned, void*);
    int32_t            (*remove_breakpoint            )(struct TyArcInstance*, unsigned, void*);
    int32_t            (*retrieve_breakpoint_code     )(struct TyArcInstance*, unsigned, char *, unsigned, void *);
    int32_t            (*breakpoint_cookie_len        )(struct TyArcInstance*);
    int32_t            (*at_breakpoint                )(struct TyArcInstance*);
    int32_t            (*define_displays              )(struct TyArcInstance*, struct Register_display *);
    int32_t            (*fill_memory                  )(struct TyArcInstance*, uint32_t, void *, uint32_t, uint32_t, int32_t);
    int32_t            (*instruction_trace_count      )(struct TyArcInstance*);
    void           (*get_instruction_traces       )(struct TyArcInstance*, uint32_t *);
    void           (*receive_callback             )(struct TyArcInstance*, ARC_callback*);
    int32_t            (*supports_feature             )(struct TyArcInstance*);
    uint32_t  (*data_exchange                )(struct TyArcInstance*, uint32_t, uint32_t, uint32_t, void *, uint32_t, void *);
    int32_t            (*in_same_process_as_debugger  )(struct TyArcInstance*);
    uint32_t  (*max_data_exchange_transfer   )(struct TyArcInstance*);
    int32_t            (*read_banked_reg              )(struct TyArcInstance*, int32_t, int32_t, uint32_t *);
    int32_t            (*write_banked_reg             )(struct TyArcInstance*, int32_t, int32_t, uint32_t *);
    int32_t            (*set_mem_watchpoint2          )(struct TyArcInstance*, ARC_ADDR_TYPE, int32_t, unsigned , void **);
};

struct TyArcInstance
{
    struct TyFunctionTable *ptyFunctionTable;
};

// external variables
extern TyServerInfo tySI;

typedef struct TyArcInstance *(*Tyget_ARC_interface_ex)(PFARCSetupFunction pfExternalARCSetup);
typedef int32_t (*TyAshTestScanchain)(struct TyArcInstance *);
typedef int32_t (*TyAshDetectScanchain)(struct TyArcInstance *p, 
                                    uint32_t uiMaxCores, 
                                    uint32_t *puiNumberOfCores,
                                    uint32_t *pulIRWidth, 
                                    unsigned char *pbARCCores);
typedef void (*TyAshGetSetupItem)(struct TyArcInstance *ptyPtr, 
                                  const char *pszSection, 
                                  const char *pszItem, 
                                  char *pszValue);
typedef void (*TyAshSetSetupItem)(struct TyArcInstance *ptyPtr, 
                                  const char *pszSection, 
                                  const char *pszItem, 
                                  const char *pszValue);
typedef void (*TyAshGetLastErrorMessage)(struct TyArcInstance *p, 
                                         char *pszMessage, 
                                         uint32_t uiSize);
typedef void (*TyASH_ListConnectedProbes)(char ppszInstanceNumber[][16], 
                                          unsigned short usMaxInstances, 
                                          unsigned short *pusConnectedInstances);
typedef int32_t (*TyAshScanIR)(struct TyArcInstance *p, 
                           uint32_t ulTargetIRLength, 
                           uint32_t *pulDataIn, 
                           uint32_t *pulDataOut);
typedef int32_t (*TyAshScanDR)(struct TyArcInstance *p, 
                           uint32_t ulTargetIRLength, 
                           uint32_t *pulDataIn, 
                           uint32_t *pulDataOut);
typedef int32_t (*TyAshTAPReset)(struct TyArcInstance *p);

typedef char* (*TyAshGetInterfaceId) (void);

typedef void (*TyASH_UpdateScanchain)(struct TyArcInstance *p);

// breakpoint array definitions
#define  GDBSERV_BP_ARRAY_ALLOC_CHUNK    64

typedef struct
{
    uint32_t ulBpType;
    uint32_t ulAddress;
    uint32_t ulLength;
    unsigned char ucInstruction[4];
} TyBpElement;

typedef struct
{
    TyBpElement *ptyBpArray; //TODO init elements in function
} TyCoreBPArray_t;

// local function prototypes
//static int32_t SetupDefaultRegs(char *ppszRegName[],uint32_t *pulRegNumber,int32_t iRegCount);
static int32_t GetDefaultRegSettings(uint32_t ulNumberOfRegisters, char *ppszRegName[],uint32_t *pulRegNumber);
static int32_t SetupDefaultRegsNameAndNumber(char *ppszRegName[],uint32_t *pulRegNumber,uint32_t *pulGdbRegIndexint,TyAccessType *pulAccessType,int32_t *iRegCount);
static int32_t SetUpArcRegs();
static int32_t GetRegSettingsFromXML(const char *pszRegFileName,char *ppszRegName[],uint32_t *pulRegNumber,uint32_t *pulGdbRegNumber, TyAccessType *pulAccessType, int32_t *iNumRegs);
static void SetupErrorMessage(int32_t iError);
static void IsBigEndian(void);
static int32_t  ARCLoadLibrary(void);
static int32_t  ARCFreeLibrary(void);
static char *GetRegName(uint32_t ulGdbRegNumber);
static char *GetBpTypeName(uint32_t ulBpType);
static int32_t  AddBpArrayEntry(uint32_t ulBpType, uint32_t ulAddress, uint32_t ulLength, unsigned char *pucInstruction, int32_t iCurrentCoreIndex);
static int32_t  RemoveBpArrayEntry(uint32_t uiIndex, int32_t iCurrentCoreIndex);
static void RemoveAllBpArray(void);
static int32_t  GetBpArrayEntry(uint32_t ulBpType, uint32_t ulAddress, uint32_t ulLength, unsigned char *pucInstruction, int32_t iCurrentCoreIndex);
static int32_t  GetBpArrayEntryForAddr(uint32_t ulAddress, int32_t iCurrentCoreIndex);
static int32_t  ReadFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t *pulRegValue);
static int32_t  WriteFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t ulRegValue);
static int32_t  if_version(struct ARC_callback *);
static int32_t  if_printf(struct ARC_callback *, const char *format, ...);
static void if_meminfo(struct ARC_callback *, unsigned addr, struct Memory_info *m);
static int32_t  if_vprintf(struct ARC_callback *, const char *format, va_list ap);
static int32_t  if_get_verbose(struct ARC_callback *);
static void if_sleep(struct ARC_callback *, unsigned milliseconds);
static int32_t  if_in_same_process_as_debugger(struct ARC_callback *);
static struct ARC_endian* if_get_endian(struct ARC_callback *);
static void * if_get_tcp_factory(struct ARC_callback *);
void * if_get_process(struct ARC_callback *);
struct Debugger_access * if_get_debugger_access(struct ARC_callback *);
void(*my_printf_func)(const char *format);

// local variables
static HINSTANCE           hInstARCDll      = NULL;
static char                szTargetDllName[_MAX_PATH] = {0};
Tyget_ARC_interface_ex     pfget_ARC_interface_ex;
TyAshTestScanchain         pfAshTestScanchain;
TyAshDetectScanchain       pfAshDetectScanchain;
TyAshGetSetupItem          pfAshGetSetupItem;
TyAshSetSetupItem          pfAshSetSetupItem;
TyAshGetLastErrorMessage   pfAshGetLastErrorMessage;
TyASH_ListConnectedProbes  pfASH_ListConnectedProbes;
TyAshScanIR                pfAshScanIR;
TyAshScanDR                pfAshScanDR;
TyAshTAPReset              pfAshTAPReset;
TyAshGetInterfaceId        pfAshGetInterfaceID;
TyASH_UpdateScanchain      pfAshUpdateScanchain;
struct TyArcInstance       *ptyArcInstance;
unsigned short             usConnectedInstances;
static int32_t                 iRegDefEndianMode = -1;
static TyCoreBPArray_t     tyCoreBPArray[MAX_TAPS_IN_CHAIN+1];
static uint32_t        uiBpCount[MAX_TAPS_IN_CHAIN+1] = {0};     //todo
static uint32_t        uiBpAlloc[MAX_TAPS_IN_CHAIN+1] = {0};

ARC_callback_functab pf = {           //lint !e123
    0, 
    if_version,
    if_printf,
    if_meminfo,
    // end of supported interface 1
    if_vprintf,
    if_get_verbose,
    // end of interface 2
    if_sleep,
    if_in_same_process_as_debugger,
    if_get_endian,
    if_get_tcp_factory,
    if_get_process,
    // end of interface 3
    if_get_debugger_access,
    /* not implemented (SPT)
    if_get_system,
    if_target_state_invalidated,
    if_debugger_installation_directory,
     // end of interface Version 4
    if_target_state_changed,
     // end of interface Version 5
    if_eprintf,
    if_evprintf,
    if_lprintf,
    if_lvprintf,
     // end of interface Version 6
    if_meminfo2,
     //end of interface version 7
     */
};

struct ARC_callback
{
    ARC_callback_functab *pft;//lint !e123
};
/****************************************************************************
     Function: SetupDefaultRegs
     Engineer: Sreekanth.V
        Input: ppszRegName :  Array for storing Register names
               ulRegNumber :  Array for storing Register numbers
               iRegCount   :  Number of Registers
       Output: int32_t - Error Code
  Description: Set up Registers in their default order
Date           Initials    Description
08-Oct-2012    SV          Initial
25-10-2012     SPT         Unwanted Registers removed
****************************************************************************/
#if 0
static int32_t SetupDefaultRegs(char *ppszRegName[],uint32_t *pulRegNumber,int32_t iRegCount)
{
    iRegCount = iRegCount;
    ppszRegName[0]    = (char *)REG_R0;
    pulRegNumber[0]   = (int32_t)0x00;

    ppszRegName[1]    = (char *)REG_R1;
    pulRegNumber[1]   = (int32_t)0x01;

    ppszRegName[2]    = (char *)REG_R2;
    pulRegNumber[2]   = (int32_t)0x02;

    ppszRegName[3]    = (char *)REG_R3;
    pulRegNumber[3]   = (int32_t)0x03;

    ppszRegName[4]    = (char *)REG_R4;
    pulRegNumber[4]   = (int32_t)0x04;

    ppszRegName[5]    = (char *)REG_R5;
    pulRegNumber[5]   = (int32_t)0x05;

    ppszRegName[6]    = (char *)REG_R6;
    pulRegNumber[6]   = (int32_t)0x06;

    ppszRegName[7]    = (char *)REG_R7;
    pulRegNumber[7]   = (int32_t)0x07;

    ppszRegName[8]    = (char *)REG_R8;
    pulRegNumber[8]   = (int32_t)0x08;

    ppszRegName[9]    = (char *)REG_R9;
    pulRegNumber[9]   = (int32_t)0x09;

    ppszRegName[10]   = (char *)REG_R10;
    pulRegNumber[10]  = (int32_t)0xA;

    ppszRegName[11]   = (char *)REG_R11;
    pulRegNumber[11]  = (int32_t)0xB;

    ppszRegName[12]   = (char *)REG_R12;
    pulRegNumber[12]  = (int32_t)0xC;

    ppszRegName[13]   = (char *)REG_R13;
    pulRegNumber[13]  = (int32_t)0xD;

    ppszRegName[14]   = (char *)REG_R14;
    pulRegNumber[14]  = (int32_t)0xE;

    ppszRegName[15]   = (char *)REG_R15;
    pulRegNumber[15]  = (int32_t)0xF;

    ppszRegName[16]   = (char *)REG_R16;
    pulRegNumber[16]  = (int32_t)0x10;

    ppszRegName[17]   = (char *)REG_R17;
    pulRegNumber[17]  = (int32_t)0x11;

    ppszRegName[18]   = (char *)REG_R18;
    pulRegNumber[18]  = (int32_t)0x12;

    ppszRegName[19]   = (char *)REG_R19;
    pulRegNumber[19]  = (int32_t)0x13;

    ppszRegName[20]   = (char *)REG_R20;
    pulRegNumber[20]  = (int32_t)0x14;

    ppszRegName[21]   = (char *)REG_R21;
    pulRegNumber[21]  = (int32_t)0x15;

    ppszRegName[22]   = (char *)REG_R22;
    pulRegNumber[22]  = (int32_t)0x16;

    ppszRegName[23]   = (char *)REG_R23;
    pulRegNumber[23]  = (int32_t)0x17;

    ppszRegName[24]   = (char *)REG_R24;
    pulRegNumber[24]  = (int32_t)0x18;

    ppszRegName[25]   = (char *)REG_R25;
    pulRegNumber[25]  = (int32_t)0x19;

    ppszRegName[26]   = (char *)REG_R26;
    pulRegNumber[26]  = (int32_t)0x1A;
/*
   ppszRegName[27]   = (char *)REG_R27;
   pulRegNumber[27]  = (int32_t)0x1B;

   ppszRegName[28]   = (char *)REG_R28;
   pulRegNumber[28]  = (int32_t)0x1C;

   ppszRegName[29]   = (char *)REG_R29;
   pulRegNumber[29]  = (int32_t)0x1D;

   ppszRegName[30]   = (char *)REG_R30;
   pulRegNumber[30]  = (int32_t)0x1E;

   ppszRegName[31]   = (char *)REG_R31;
   pulRegNumber[31]  = (int32_t)0x1F;

   ppszRegName[32]   = (char *)REG_R32;
   pulRegNumber[32]  = (int32_t)0x20;

   ppszRegName[33]   = (char *)REG_R33;
   pulRegNumber[33]  = (int32_t)0x21;

   ppszRegName[34]   = (char *)REG_R34;
   pulRegNumber[34]  = (int32_t)0x22;

   ppszRegName[35]   = (char *)REG_R35;
   pulRegNumber[35]  = (int32_t)0x23;

   ppszRegName[36]   = (char *)REG_R36;
   pulRegNumber[36]  = (int32_t)0x24;

   ppszRegName[37]   = (char *)REG_R37;
   pulRegNumber[37]  = (int32_t)0x25;

   ppszRegName[38]   = (char *)REG_R38;
   pulRegNumber[38]  = (int32_t)0x26;

   ppszRegName[39]   = (char *)REG_R39;
   pulRegNumber[39]  = (int32_t)0x27;

   ppszRegName[40]   = (char *)REG_R40;
   pulRegNumber[40]  = (int32_t)0x28;

   ppszRegName[41]   = (char *)REG_R41;
   pulRegNumber[41]  = (int32_t)0x29;

   ppszRegName[42]   = (char *)REG_R42;
   pulRegNumber[42]  = (int32_t)0x2A;

   ppszRegName[43]   = (char *)REG_R43;
   pulRegNumber[43]  = (int32_t)0x2B;

   ppszRegName[44]   = (char *)REG_R44;
   pulRegNumber[44]  = (int32_t)0x2C;

   ppszRegName[45]   = (char *)REG_R45;
   pulRegNumber[45]  = (int32_t)0x2D;

   ppszRegName[46]   = (char *)REG_R46;
   pulRegNumber[46]  = (int32_t)0x2E;

   ppszRegName[47]   = (char *)REG_R47;
   pulRegNumber[47]  = (int32_t)0x2F;

   ppszRegName[48]   = (char *)REG_R48;
   pulRegNumber[48]  = (int32_t)0x30;

   ppszRegName[49]   = (char *)REG_R49;
   pulRegNumber[49]  = (int32_t)0x31;

   ppszRegName[50]   = (char *)REG_R50;
   pulRegNumber[50]  = (int32_t)0x32;

   ppszRegName[51]   = (char *)REG_R51;
   pulRegNumber[51]  = (int32_t)0x33;

   ppszRegName[52]   = (char *)REG_R52;
   pulRegNumber[52]  = (int32_t)0x34;

   ppszRegName[53]   = (char *)REG_R53;
   pulRegNumber[53]  = (int32_t)0x35;

   ppszRegName[54]   = (char *)REG_R54;
   pulRegNumber[54]  = (int32_t)0x36;

   ppszRegName[55]   = (char *)REG_R55;
   pulRegNumber[55]  = (int32_t)0x37;

   ppszRegName[56]   = (char *)REG_R56;
   pulRegNumber[56]  = (int32_t)0x38;

   ppszRegName[57]   = (char *)REG_R57;
   pulRegNumber[57]  = (int32_t)0x39;

   ppszRegName[58]   = (char *)REG_R58;
   pulRegNumber[58]  = (int32_t)0x3A;

   ppszRegName[59]   = (char *)REG_R59;
   pulRegNumber[59]  = (int32_t)0x3B;

   ppszRegName[60]   = (char *)REG_R60;
   pulRegNumber[60]  = (int32_t)0x3C;

   ppszRegName[61]   = (char *)REG_R61;
   pulRegNumber[61]  = (int32_t)0x3D;

   ppszRegName[62]   = (char *)REG_R62;
   pulRegNumber[62]  = (int32_t)0x3E;

   ppszRegName[63]   = (char *)REG_R63;
   pulRegNumber[63]  = (int32_t)0x3F;

   ppszRegName[64]   = (char *)REG_GP;
   pulRegNumber[64]  = (int32_t)0x1A;
*/
    ppszRegName[27]   = (char *)REG_FP;
    pulRegNumber[27]  = (int32_t)0x1B;

    ppszRegName[28]   = (char *)REG_SP;
    pulRegNumber[28]  = (int32_t)0x1C;

    ppszRegName[29]   = (char *)REG_ILINK1;
    pulRegNumber[29]  = (int32_t)0x1D;

    ppszRegName[30]   = (char *)REG_ILINK2;
    pulRegNumber[30]  = (int32_t)0x1E;

    ppszRegName[31]   = (char *)REG_BLINK;
    pulRegNumber[31]  = (int32_t)0x1F;

    ppszRegName[32]   = (char *)REG_LPCOUNT;
    pulRegNumber[32]  = (int32_t)0x3C;

    //ppszRegName[71]   = (char *)REG_LIMM;
    //pulRegNumber[71]  = (int32_t)0x3E;

    ppszRegName[33]   = (char *)REG_PCL;
    pulRegNumber[33]  = (int32_t)0x3F;

    return GDBSERV_NO_ERROR;
}
#endif

/****************************************************************************
     Function: SetupDefaultRegsNameAndNumber
     Engineer: Sreeshma T.
        Input: ppszRegName :  Array for storing Register names
               ulRegNumber :  Array for storing Register numbers
               iRegCount   :  Returns Number of Registers
       Output: int32_t - Error Code
  Description: Set up Registers in their default order
Date           Initials    Description
04-Mar-2016     ST          Initial
****************************************************************************/
static int32_t SetupDefaultRegsNameAndNumber(char *ppszRegName[],uint32_t *pulRegNumber,uint32_t *pulGdbRegIndexint,TyAccessType *pulAccessType,int32_t *iRegCount)
{
    TyXMLNode *ptyNode;
    TyXMLFile tyXMLSCFile;
    //int32_t i;
    // int32_t iRegCount = 0, iCurrReg = 0, iErrRet = 0;
    int32_t iCurrReg = 0, iErrRet = 0;
    char *pRegNum;

    if (tySI.bIsCoreRegFilePresent)
    {
        iErrRet = XML_ReadFile(tySI.szCoreRegFile,&tyXMLSCFile);
        //printf("tySI.szCoreRegFile = %s\n", tySI.szCoreRegFile);
    }
    else
    {
        iErrRet = XML_ReadFile("arccorereg.xml",&tyXMLSCFile);
        //printf("arccorereg.xml\n");
    }

    if (iErrRet != 0)
        return iErrRet;
    ptyNode = XML_GetChildNode((XML_GetNode(XML_GetBaseNode(&tyXMLSCFile),"target")),"feature");

    if (ptyNode == NULL)
        return GDBSERV_XML_ERROR;

    *iRegCount = XML_GetChildNodeCount(ptyNode,(char *)"register");

    // printf("iRegCount = %d\n", *iRegCount);

    //Ensuring XML contains valid register information
    if (*iRegCount == 0)
        return GDBSERV_XML_ERROR ;

    // *iNumRegs = iRegCount;

    if ((ptyNode = XML_GetChildNode(ptyNode,"register")) == NULL)
        return GDBSERV_XML_ERROR;

    iCurrReg = 0;

    while (ptyNode != NULL)
    {
        ptyNode = XML_GetNode(ptyNode,"register"); 
        if ((ppszRegName[iCurrReg] = XML_GetAttrib(ptyNode,"name")) == NULL)
        {
            // printf("error1\n");
            //   printf("ppszRegName[%d] = %d",iCurrReg, ppszRegName[iCurrReg]);
            break;
        }

        if ((pRegNum = XML_GetAttrib(ptyNode,"number")) == NULL)
        {
            //printf("error2\n");
            break;
        }

        pulRegNumber[iCurrReg] = strtoul(pRegNum,NULL,16);
        pulGdbRegIndexint[iCurrReg] = pulRegNumber[iCurrReg];
        pulAccessType[iCurrReg] = RW;
        // printf("ppszRegName[%d] = %s pulRegNumber[%d] = %d pulGdbRegIndexint[%d] = %d\n",iCurrReg, ppszRegName[iCurrReg], iCurrReg, pulRegNumber[iCurrReg], iCurrReg, pulGdbRegIndexint[iCurrReg]);
        ++iCurrReg;
        ptyNode = ptyNode->pNext;
    }

    if (ptyNode != NULL)
        iErrRet = GDBSERV_XML_ERROR ;

    return iErrRet;

}

/****************************************************************************
     Function: GetDefaultRegSettings
     Engineer: Sreekanth.V
        Input: ppszRegName :  Array for storing Register names
               ulRegNumber :  Array for storing Register numbers
       Output: int32_t - Error Code
  Description: Set up Registers in their default order, this is for GDB 6.3 and below
Date           Initials    Description
08-Oct-2012      SV         Initial
****************************************************************************/
static int32_t GetDefaultRegSettings(uint32_t ulNumberOfRegisters,char *ppszRegName[],uint32_t *pulRegNumber)
{
    uint32_t ulIndex;

    for (ulIndex=0; ulIndex < ulNumberOfRegisters; ulIndex++)
    {
        ppszRegName[ulIndex] = (char *)malloc(sizeof(char)); 
        if (ppszRegName[ulIndex] == NULL )
            return GDBSERV_ERROR;
    }
    memset(*ppszRegName,'\0',(uint32_t)ulNumberOfRegisters);
    memset(pulRegNumber,0, (uint32_t)ulNumberOfRegisters);

    // Default Register Ordering

    ppszRegName[0]    = (char *)REG_R0;
    pulRegNumber[0]   = (int32_t)0x00;

    ppszRegName[1]    = (char *)REG_R1;
    pulRegNumber[1]   = (int32_t)0x01;

    ppszRegName[2]    = (char *)REG_R2;
    pulRegNumber[2]   = (int32_t)0x02;

    ppszRegName[3]    = (char *)REG_R3;
    pulRegNumber[3]   = (int32_t)0x03;

    ppszRegName[4]    = (char *)REG_R4;
    pulRegNumber[4]   = (int32_t)0x04;

    ppszRegName[5]    = (char *)REG_R5;
    pulRegNumber[5]   = (int32_t)0x05;

    ppszRegName[6]    = (char *)REG_R6;
    pulRegNumber[6]   = (int32_t)0x06;

    ppszRegName[7]    = (char *)REG_R7;
    pulRegNumber[7]   = (int32_t)0x07;

    ppszRegName[8]    = (char *)REG_R8;
    pulRegNumber[8]   = (int32_t)0x08;

    ppszRegName[9]    = (char *)REG_R9;
    pulRegNumber[9]   = (int32_t)0x09;

    ppszRegName[0xA]   = (char *)REG_R10;
    pulRegNumber[0xA]  = (int32_t)0xA;

    ppszRegName[0xB]   = (char *)REG_R11;
    pulRegNumber[0xB]  = (int32_t)0xB;

    ppszRegName[0xc]   = (char *)REG_R12;
    pulRegNumber[0xc]  = (int32_t)0xC;

    ppszRegName[0xd]   = (char *)REG_R13;
    pulRegNumber[0xd]  = (int32_t)0xD;

    ppszRegName[0xe]   = (char *)REG_R14;
    pulRegNumber[0xe]  = (int32_t)0xE;

    ppszRegName[0xf]   = (char *)REG_R15;
    pulRegNumber[0xf]  = (int32_t)0xF;

    ppszRegName[0x10]   = (char *)REG_R16;
    pulRegNumber[0x10]  = (int32_t)0x10;

    ppszRegName[0x11]   = (char *)REG_R17;
    pulRegNumber[0x11]  = (int32_t)0x11;

    ppszRegName[0x12]   = (char *)REG_R18;
    pulRegNumber[0x12]  = (int32_t)0x12;

    ppszRegName[0x13]   = (char *)REG_R19;
    pulRegNumber[0x13]  = (int32_t)0x13;

    ppszRegName[0x14]   = (char *)REG_R20;
    pulRegNumber[0x14]  = (int32_t)0x14;

    ppszRegName[0x15]   = (char *)REG_R21;
    pulRegNumber[0x15]  = (int32_t)0x15;

    ppszRegName[0x16]   = (char *)REG_R22;
    pulRegNumber[0x16]  = (int32_t)0x16;

    ppszRegName[0x17]   = (char *)REG_R23;
    pulRegNumber[0x17]  = (int32_t)0x17;

    ppszRegName[0x18]   = (char *)REG_R24;
    pulRegNumber[0x18]  = (int32_t)0x18;

    ppszRegName[0x19]   = (char *)REG_R25;
    pulRegNumber[0x19]  = (int32_t)0x19;

    ppszRegName[0x1A]   = (char *)REG_GP;
    pulRegNumber[0x1A]  = (int32_t)0x1A;

    ppszRegName[0x1B]   = (char *)REG_FP;
    pulRegNumber[0x1B]  = (int32_t)0x1B;

    ppszRegName[0x1C]   = (char *)REG_SP;
    pulRegNumber[0x1C]  = (int32_t)0x1C;

    ppszRegName[0x1D]   = (char *)REG_ILINK1;
    pulRegNumber[0x1D]  = (int32_t)0x1D;

    ppszRegName[0x1E]   = (char *)REG_ILINK2;
    pulRegNumber[0x1E]  = (int32_t)0x1E;

    ppszRegName[0x1F]   = (char *)REG_BLINK;
    pulRegNumber[0x1F]  = (int32_t)0x1F;

    ppszRegName[0x20]   = (char *)REG_R32;
    pulRegNumber[0x20]  = (int32_t)0x20;

    ppszRegName[0x21]   = (char *)REG_R33;
    pulRegNumber[0x21]  = (int32_t)0x21;

    ppszRegName[0x22]   = (char *)REG_R34;
    pulRegNumber[0x22]  = (int32_t)0x22;

    ppszRegName[0x23]   = (char *)REG_R35;
    pulRegNumber[0x23]  = (int32_t)0x23;

    ppszRegName[0x24]   = (char *)REG_R36;
    pulRegNumber[0x24]  = (int32_t)0x24;

    ppszRegName[0x25]   = (char *)REG_R37;
    pulRegNumber[0x25]  = (int32_t)0x25;

    ppszRegName[0x26]   = (char *)REG_R38;
    pulRegNumber[0x26]  = (int32_t)0x26;

    ppszRegName[0x27]   = (char *)REG_R39;
    pulRegNumber[0x27]  = (int32_t)0x27;

    ppszRegName[0x28]   = (char *)REG_R40;
    pulRegNumber[0x28]  = (int32_t)0x28;

    ppszRegName[0x29]   = (char *)REG_R41;
    pulRegNumber[0x29]  = (int32_t)0x29;

    ppszRegName[0x2A]   = (char *)REG_R42;
    pulRegNumber[0x2A]  = (int32_t)0x2A;

    ppszRegName[0x2B]   = (char *)REG_R43;
    pulRegNumber[0x2B]  = (int32_t)0x2B;

    ppszRegName[0x2C]   = (char *)REG_R44;
    pulRegNumber[0x2C]  = (int32_t)0x2C;

    ppszRegName[0x2D]   = (char *)REG_R45;
    pulRegNumber[0x2D]  = (int32_t)0x2D;

    ppszRegName[0x2E]   = (char *)REG_R46;
    pulRegNumber[0x2E]  = (int32_t)0x2E;

    ppszRegName[0x2F]   = (char *)REG_R47;
    pulRegNumber[0x2F]  = (int32_t)0x2F;

    ppszRegName[0x30]   = (char *)REG_R48;
    pulRegNumber[0x30]  = (int32_t)0x30;

    ppszRegName[0x31]   = (char *)REG_R49;
    pulRegNumber[0x31]  = (int32_t)0x31;

    ppszRegName[0x32]   = (char *)REG_R50;
    pulRegNumber[0x32]  = (int32_t)0x32;

    ppszRegName[0x33]   = (char *)REG_R51;
    pulRegNumber[0x33]  = (int32_t)0x33;

    ppszRegName[0x34]   = (char *)REG_R52;
    pulRegNumber[0x34]  = (int32_t)0x34;

    ppszRegName[0x35]   = (char *)REG_R53;
    pulRegNumber[0x35]  = (int32_t)0x35;

    ppszRegName[0x36]   = (char *)REG_R54;
    pulRegNumber[0x36]  = (int32_t)0x36;

    ppszRegName[0x37]   = (char *)REG_R55;
    pulRegNumber[0x37]  = (int32_t)0x37;

    ppszRegName[0x38]   = (char *)REG_R56;
    pulRegNumber[0x38]  = (int32_t)0x38;

    ppszRegName[0x39]   = (char *)REG_R57;
    pulRegNumber[0x39]  = (int32_t)0x39;

    ppszRegName[0x3A]   = (char *)REG_R58;
    pulRegNumber[0x3A]  = (int32_t)0x3A;

    ppszRegName[0x3b]   = (char *)REG_R59;
    pulRegNumber[0x3b]  = (int32_t)0x3B;

    ppszRegName[0x3c]   = (char *)REG_LPCOUNT;
    pulRegNumber[0x3c]  = (int32_t)0x3C;

    ppszRegName[0x3d]   = (char *)REG_R61;
    pulRegNumber[0x3d]  = (int32_t)0x3D;

    ppszRegName[0x3e]   = (char *)REG_LIMM;
    pulRegNumber[0x3e]  = (int32_t)0x3E;

    ppszRegName[0x3f]   = (char *)REG_PCL;
    pulRegNumber[0x3f]  = (int32_t)0x3F;

    ppszRegName[0x40]   = (char *)REG_STATUS;
    pulRegNumber[0x40]  = (int32_t)0x40;

    ppszRegName[0x41]   = (char *)REG_SEMAPHORE;
    pulRegNumber[0x41]  = (int32_t)0x41;

    ppszRegName[0x42]   = (char *)REG_LP_START;
    pulRegNumber[0x42]  = (int32_t)0x42;

    ppszRegName[0x43]   = (char *)REG_LP_END;
    pulRegNumber[0x43]  = (int32_t)0x43;

    ppszRegName[0x44]   = (char *)REG_IDENTITY;
    pulRegNumber[0x44]  = (int32_t)0x44;

    ppszRegName[0x45]   = (char *)REG_DEBUG;
    pulRegNumber[0x45]  = (int32_t)0x45;

    ppszRegName[0x46]   = (char *)REG_PC;
    pulRegNumber[0x46]  = (int32_t)0x46;

    ppszRegName[0x47]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x47]  = (int32_t)0x47;

    ppszRegName[0x48]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x48]  = (int32_t)0x48;

    ppszRegName[0x49]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x49]  = (int32_t)0x49;

    ppszRegName[0x4a]   = (char *)REG_STATUS32;
    pulRegNumber[0x4a]  = (int32_t)0x4a;

    ppszRegName[0x4b]   = (char *)REG_STATUS32_L1;                
    pulRegNumber[0x4b]  = (int32_t)0x4b;

    ppszRegName[0x4c]   = (char *)REG_STATUS32_L2;
    pulRegNumber[0x4c]  = (int32_t)0x4c;

    ppszRegName[0x4d]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x4d]  = (int32_t)0x4d;

    ppszRegName[0x4e]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x4e]  = (int32_t)0x4e;

    ppszRegName[0x4f]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x4f]  = (int32_t)0x4f;

    ppszRegName[0x50]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x50]  = (int32_t)0x50;

    ppszRegName[0x51]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x51]  = (int32_t)0x51;

    ppszRegName[0x52]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x52]  = (int32_t)0x52;

    ppszRegName[0x53]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x53]  = (int32_t)0x53;

    ppszRegName[0x54]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x54]  = (int32_t)0x54;

    ppszRegName[0x55]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x55]  = (int32_t)0x55;

    ppszRegName[0x56]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x56]  = (int32_t)0x56;

    ppszRegName[0x57]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x57]  = (int32_t)0x57;

    ppszRegName[0x58]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x58]  = (int32_t)0x58;

    ppszRegName[0x59]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x59]  = (int32_t)0x59;

    ppszRegName[0x5a]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x5a]  = (int32_t)0x5a;

    ppszRegName[0x5b]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x5b]  = (int32_t)0x5b;

    ppszRegName[0x5c]   = (char *)REG_UNKNOWN; 
    pulRegNumber[0x5c]  = (int32_t)0x5c;

    ppszRegName[0x5d]   = (char *)REG_UNKNOWN; 
    pulRegNumber[0x5d]  = (int32_t)0x5d;

    ppszRegName[0x5e]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x5e]  = (int32_t)0x5e;

    ppszRegName[0x5f]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x5f]  = (int32_t)0x5f;

    ppszRegName[0x60]   = (char *)REG_UNKNOWN;
    pulRegNumber[0x60]  = (int32_t)0x60;

    ppszRegName[0x61]   = (char *)REG_COUNT0;
    pulRegNumber[0x61]  = (int32_t)0x61;

    ppszRegName[0x62]   = (char *)REG_CONTROL0; 
    pulRegNumber[0x62]  = (int32_t)0x62;

    ppszRegName[0x63]   = (char *)REG_LIMIT0; 
    pulRegNumber[0x63]  = (int32_t)0x63;

    ppszRegName[0x64]  = (char *)REG_UNKNOWN; 
    pulRegNumber[0x64] = (int32_t)0x64;

    ppszRegName[0x65]  = (char *)REG_INT_VECTOR_BASE; 
    pulRegNumber[0x65] = (int32_t)0x65;

    ppszRegName[0x66]  = (char *)REG_UNKNOWN; 
    pulRegNumber[0x66] = (int32_t)0x66;

    ppszRegName[0x67]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x67] = (int32_t)0x67;

    ppszRegName[0x68]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x68] = (int32_t)0x68;

    ppszRegName[0x69]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x69] = (int32_t)0x69;

    ppszRegName[0x6a]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x6a] = (int32_t)0x6a;

    ppszRegName[0x6b]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x6b] = (int32_t)0x6b;

    ppszRegName[0x6c]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x6c] = (int32_t)0x6c;

    ppszRegName[0x6d]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x6d] = (int32_t)0x6d;
    ppszRegName[0x6e]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x6e] = (int32_t)0x6e;
    ppszRegName[0x6f]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x6f] = (int32_t)0x6f;
    ppszRegName[0x70]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x70] = (int32_t)0x70;
    ppszRegName[0x71]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x71] = (int32_t)0x71;
    ppszRegName[0x72]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x72] = (int32_t)0x72;
    ppszRegName[0x73]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x73] = (int32_t)0x73;
    ppszRegName[0x74]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x74] = (int32_t)0x74;
    ppszRegName[0x75]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x75] = (int32_t)0x75;
    ppszRegName[0x76]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x76] = (int32_t)0x76;
    ppszRegName[0x77]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x77] = (int32_t)0x77;
    ppszRegName[0x78]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x78] = (int32_t)0x78;
    ppszRegName[0x79]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x79] = (int32_t)0x79;
    ppszRegName[0x7a]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x7a] = (int32_t)0x7a;
    ppszRegName[0x7b]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x7b] = (int32_t)0x7b;
    ppszRegName[0x7c]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x7c] = (int32_t)0x7c;
    ppszRegName[0x7d]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x7d] = (int32_t)0x7d;
    ppszRegName[0x7e]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x7e] = (int32_t)0x7e;
    ppszRegName[0x7f]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x7f] = (int32_t)0x7f;
    ppszRegName[0x80]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x80] = (int32_t)0x80;
    ppszRegName[0x81]  = (char *)REG_MACMODE;
    pulRegNumber[0x81] = (int32_t)0x81;
    ppszRegName[0x82]  = (char *)REG_UNKNOWN;
    pulRegNumber[0x82] = (int32_t)0x82;
    ppszRegName[0x83]  = (char *)REG_IRQ_LV12;
    pulRegNumber[0x83] = (int32_t)0x83;

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: GetRegSettingsFromXML
     Engineer: Sreekanth.V
        Input: ppszRegName  :  Array for storing Register names
               pulRegNumber :  Array for storing Register numbers
               pulGdbRegNumber : Array for storing GDB Register number
               iNumRegs     :  Number of Registers
               pulAccessType: Access type of register - read write, read only or write only
       Output: int32_t  - Error Code
  Description: Get Register Configuration settings from XML 
Date           Initials    Description
08-Oct-2012      SV         Initial
15-Nov-2012     SPT        Added Support for BCR registers
****************************************************************************/
static int32_t GetRegSettingsFromXML(const char *pszRegFileName,char *ppszRegName[],uint32_t *pulRegNumber,uint32_t *pulGdbRegNumber,TyAccessType *pulAccessType, int32_t *iNumRegs)
{
    TyXMLNode *ptyNode;
    TyXMLFile tyXMLSCFile;
    //int32_t i;
    int32_t iRegCount = 0, iCurrReg = 0, iErrRet = 0;
    char *pRegNum;
    char *accessType;
    uint32_t iGdbReg;

    // if ((iErrRet = SetupCoreRegs(ARCH_ARC600, ppszRegName, pulRegNumber, pulGdbRegNumber, iNumRegs)) != GDBSERV_NO_ERROR)
    //     return iErrRet;

    iErrRet = XML_ReadFile(pszRegFileName,&tyXMLSCFile);
    if (iErrRet != 0)
        return iErrRet;
    ptyNode = XML_GetChildNode((XML_GetNode(XML_GetBaseNode(&tyXMLSCFile),"target")),"feature");

    if (ptyNode == NULL)
        return GDBSERV_XML_ERROR;

    iRegCount = XML_GetChildNodeCount(ptyNode,(char *)"register");

    //Ensuring XML contains valid register information
    if (iRegCount == 0)
        return GDBSERV_XML_ERROR ;

    if ((ptyNode = XML_GetChildNode(ptyNode,"register")) == NULL)
        return GDBSERV_XML_ERROR;

    iCurrReg = *iNumRegs;
    *iNumRegs = *iNumRegs + iRegCount;
    iGdbReg = DEFAULT_REG_COUNT;

    while (ptyNode != NULL)
    {
        ptyNode = XML_GetNode(ptyNode,"register"); 
        if ((ppszRegName[iCurrReg] = XML_GetAttrib(ptyNode,"name")) == NULL)
            break;
        if ((pRegNum = XML_GetAttrib(ptyNode,"number")) == NULL)
            break;

        pulRegNumber[iCurrReg] = strtoul(pRegNum,NULL,16) + (int32_t)AUX_BASE;
        pulGdbRegNumber[iCurrReg] = iGdbReg;

        if ((accessType = XML_GetAttrib(ptyNode,"access")) == NULL)
            break;
        if (stricmp(accessType, "RO") == 0)
        {
            pulAccessType[iCurrReg] = RO;
        }
        else if (stricmp(accessType, "WO") == 0)
        {
            pulAccessType[iCurrReg] = WO;
        }
        else if (stricmp(accessType, "RW") == 0)
        {
            pulAccessType[iCurrReg] = RW;
        }
        else
        {
            pulAccessType[iCurrReg] = RO;
        }

        ++iCurrReg;
        ++iGdbReg;
        ptyNode = ptyNode->pNext;
    }

    if (ptyNode != NULL)
        iErrRet = GDBSERV_XML_ERROR ;


    //getting BCR registers from XML
    ptyNode = XML_GetChildNode((XML_GetNode(XML_GetBaseNode(&tyXMLSCFile),"target")),"feature");

    if (ptyNode == NULL)
        return GDBSERV_XML_ERROR;

    iRegCount = XML_GetChildNodeCount(ptyNode,(char *)"bcr");

    //Ensuring XML contains valid register information
    if (iRegCount == 0)
        return iErrRet ;

    if ((ptyNode = XML_GetChildNode(ptyNode,"bcr")) == NULL)
        return GDBSERV_XML_ERROR;

    iCurrReg = *iNumRegs;

    *iNumRegs = iRegCount + *iNumRegs;

    while (ptyNode != NULL)
    {
        ptyNode = XML_GetNode(ptyNode,"bcr"); 
        if ((ppszRegName[iCurrReg] = XML_GetAttrib(ptyNode,"name")) == NULL)
            break;
        if ((pRegNum = XML_GetAttrib(ptyNode,"number")) == NULL)
            break;

        pulRegNumber[iCurrReg] = strtoul(pRegNum,NULL,16) + (int32_t)AUX_BASE;
        pulGdbRegNumber[iCurrReg] = iGdbReg;

        pulAccessType[iCurrReg] = RO;
        ++iGdbReg;
        ++iCurrReg;
        ptyNode = ptyNode->pNext;
    }

    if (ptyNode != NULL)
        iErrRet = GDBSERV_XML_ERROR ;


    return iErrRet;
}

/****************************************************************************
     Function: GetProcSpecificRegSettings
     Engineer: Andre Schmiel
        Input: pszFileName
       Output: Error Code
  Description: Set up processor specific ARC Registers 
Date           Initials    Description
13-Apr-2017    AS          Initial
****************************************************************************/
int32_t GetProcSpecificRegSettings(const char *pszRegFileName,char *ppszRegName[],uint32_t *pulRegNumber,uint32_t *pulGdbRegNumber, TyAccessType *pulAccessType, int32_t *piNumRegs)
{
    int32_t iErr = GDBSERV_NO_ERROR;

    iErr = GetRegSettingsFromXML(pszRegFileName, ppszRegName, pulRegNumber, pulGdbRegNumber, pulAccessType, piNumRegs);

    if (iErr != GDBSERV_NO_ERROR )
        PrintMessage(ERROR_MESSAGE,"Error occurred while getting Register settings  from %s",tySI.szRegFile);

    return iErr;
}

/****************************************************************************
     Function: strtolower
     Engineer: Andre Schmiel
        Input: pszString
       Output: pszString
  Description: convert string to lower case
Date           Initials    Description
13-Apr-2017    AS          Initial
****************************************************************************/
char *strtolower(char *pszString)
{
    while (*pszString)
    {
        if ((*pszString >= 'A') && (*pszString <= 'Z'))
            *pszString -= ('A' - 'a');                             // subtract offset between lower and upper case

        pszString++;
    }

    return(pszString);
}

/****************************************************************************
     Function: SetUpAuxRegs
     Engineer: Sreekanth.V
        Input: Nil
       Output: Error Code
  Description: Set up ARC Registers 
Date           Initials    Description
08-Oct-2012    SV          Initial
25-10-2012     SPT         Correction in register order and memory allocation
13-04-2017     AS          Added multi register file support
****************************************************************************/
static int32_t SetUpArcRegs()
{
    //char *pszFileName;
    char          pszFileName[_MAX_PATH];
    uint32_t ulCurrReg = 0, ulProcNum;
    int32_t           iErr = GDBSERV_NO_ERROR;

    //For storing Register Names
    char *pszRegName[MAX_REG_COUNT];
    int32_t   iNumberOfRegisters;
    //For storing Register Numbers
    uint32_t ulRegNumber[MAX_REG_COUNT] = {0};
    uint32_t ulGdbRegIndex[MAX_REG_COUNT] = {0};
    TyAccessType ulAccessType[MAX_REG_COUNT] = {RW};
	TyARCReg *g_ptyTempARCReg;

    //andre: only read the default register XML file once
    memset(pszRegName,'\0',sizeof(pszRegName));
    memset(ulRegNumber,0,sizeof(ulRegNumber));

    // Get Register configurations from XML file,if valid
    if (  tySI.bIsRegFilePresent
        || (   (tySI.ulCurrentTapNumber > 0)
            && (tySI.bIsMultiRegFilePresent[tySI.ulCurrentTapNumber])))
    {
        if (tySI.bIsRegFilePresent)
            ulProcNum = 0;
        else
            ulProcNum = tySI.ulCurrentTapNumber;

        iErr = SetupDefaultRegsNameAndNumber(pszRegName,ulRegNumber,ulGdbRegIndex,ulAccessType,&iNumberOfRegisters);
        if (iErr != GDBSERV_NO_ERROR )
        {
            PrintMessage(ERROR_MESSAGE,"Error occurred while getting default Register settings");
            return iErr;
        }
    
        strcpy(pszFileName, tySI.szRegFile[ulProcNum]);
        if (tySI.bDebugOutput)
            LogMessage("Create register setting from file '%s'.", pszFileName);

        iErr = GetProcSpecificRegSettings(pszFileName, pszRegName,ulRegNumber,ulGdbRegIndex,ulAccessType,&iNumberOfRegisters);
        if (iErr != GDBSERV_NO_ERROR )
        {
            PrintMessage(ERROR_MESSAGE, "Unable to create register setting from file '%s'.", pszFileName);
            return iErr;
        }
    
        tySI.ulNumberOfRegisters = (uint32_t)iNumberOfRegisters;
    }
    // No XML provided,applying default settings..
    else
    {
        PrintMessage(INFO_MESSAGE, "Use default settings for core %lx", tySI.ulCurrentTapNumber);
    
        tySI.ulNumberOfRegisters = (uint32_t)ARC_REG_COUNT;
        iErr = GetDefaultRegSettings(tySI.ulNumberOfRegisters,pszRegName,ulRegNumber);
    }
    
    if (tySI.ulNumberOfRegisters < 1)
        return GDBSERV_ERROR;

    // Allocate memory for the avilable number of registers
	g_ptyTempARCReg = (TyARCReg *)realloc(g_ptyARCReg,((uint32_t) tySI.ulNumberOfRegisters) * sizeof(TyARCReg));
    if (g_ptyTempARCReg == NULL)
    {
        PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
        return GDBSERV_ERROR; 
    }
	g_ptyARCReg = g_ptyTempARCReg;

    // Set up Registers as per the values in arrays pszRegName & ulRegNumber
    for (ulCurrReg=0; ulCurrReg<tySI.ulNumberOfRegisters; ulCurrReg++)
    {
        g_ptyARCReg[ulCurrReg].ulAddrIndex   = ulRegNumber[ulCurrReg];
        g_ptyARCReg[ulCurrReg].pszRegName    = pszRegName[ulCurrReg];
        g_ptyARCReg[ulCurrReg].ulGdbRegIndex = ulGdbRegIndex[ulCurrReg];
        g_ptyARCReg[ulCurrReg].accessType    = ulAccessType[ulCurrReg];
    }

    g_ulNumberOfRegisters = tySI.ulNumberOfRegisters;

    return iErr;
}

/****************************************************************************
     Function: GdbArcSetupFunction
     Engineer: Nikolay Chokoev
        Input: 
       Output:
  Description: 
Date           Initials    Description
04-Dec-2007    NCH         Initial
12-04-2012     SPT         Added cJTAG support
****************************************************************************/
static int32_t GdbArcSetupFunction(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt) 
{ 
    uint32_t ulIndex;
    char szNumCores[3];
    char szIrWidth[3];
    char szDevices[10];

    for (ulIndex=1; ulIndex < MAX_TAPS_IN_CHAIN+1; ulIndex++)
    {
        if ((tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulIRlength == 0) || 
            (tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulBypassInst == 0))
            break;
    }
    ulIndex--;
    if (ulIndex > MAX_TAPS_IN_CHAIN)
        ulIndex = MAX_TAPS_IN_CHAIN;
    sprintf(szNumCores,"%d",(uint32_t)ulIndex);
    ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_ADAPTIVECLOCK, tySI.bJtagRtck?"1":"0");
    ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, szNumCores);

    for (ulIndex=1; ulIndex < MAX_TAPS_IN_CHAIN+1; ulIndex++)
    {
        if ((tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulIRlength == 0) ||
            (tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulBypassInst == 0))
            break;
        sprintf(szDevices, "Device%d", (uint32_t)ulIndex);
        sprintf(szIrWidth, "%d", (uint32_t)tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulIRlength);
        ptyARCSetupInt->pfSetSetupItem(pvPtr, szDevices, ARC_SETUP_ITEM_ARCCORE,
                                       tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].bIsARCCore?"1":"0");
        ptyARCSetupInt->pfSetSetupItem(pvPtr, szDevices, ARC_SETUP_ITEM_IRWIDTH, szIrWidth);
    }
    if (tySI.szProbeInstance[0] != 0)
    {
        PrintMessage(DEBUG_MESSAGE, "\n%s\n%s\n%s\n", ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, tySI.szProbeInstance);
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_MISCOPTIONS, ARC_SETUP_ITEM_PROBEINSTANCE, tySI.szProbeInstance);
    }
    if (stricmp(tySI.tyProcConfig.szProbeTypeName, "ARCangel") == 0)
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "0");
    else if (stricmp(tySI.tyProcConfig.szProbeTypeName, "ARC-JTAG-TPA-R0") == 0)
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "1");
    else if (stricmp(tySI.tyProcConfig.szProbeTypeName, "ARC-With-Reset") == 0)
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "2");
    else if (stricmp(tySI.tyProcConfig.szProbeTypeName, "ARC-CJTAG-TPA-R1") == 0)
    {
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "3");
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TAPRESET, "1");
    }
    else if (stricmp(tySI.tyProcConfig.szProbeTypeName, "ARC-JTAG-TPA-R1") == 0)
        ptyARCSetupInt->pfSetSetupItem(pvPtr, ARC_SETUP_SECT_TARGETCONFIG, ARC_SETUP_ITEM_TARGETOPTION, "4");
    return 1;
}

/****************************************************************************
     Function: if_version
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t if_version(struct ARC_callback *ArcInt) 
{ 
    ArcInt=ArcInt;
    return ARCINT_BASE_VERSION;  
}

/****************************************************************************
     Function: if_loc_filt_printf
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void if_filt_printf(const char *format) 
{ 
    char *firmwarev;
    char *diskwarew;
    char *pszResult=NULL;

    firmwarev = strstr( (char*)format, "firmware" );
    diskwarew = strstr( (char*)format, "diskware" );

    if (firmwarev!=NULL)
    {
        if ((pszResult=strtok(firmwarev, ":")) == NULL)
        {
        }
        if ((pszResult=strtok(NULL, "\n")) == NULL)
        {
        }
        if (pszResult!=NULL)
        {
            strncpy(tySI.tyProcConfig.Processor._ARC.szFirmwareVer,
                    pszResult,
                    _MAX_PATH);
        }
    }

    if (diskwarew!=NULL)
    {
        if ((pszResult=strtok(diskwarew, ":")) == NULL)
        {
        }
        if ((pszResult=strtok(NULL, "\n")) == NULL)
        {
        }
        if (pszResult!=NULL)
        {
            strncpy(tySI.tyProcConfig.Processor._ARC.szDiskwareVer,
                    pszResult,
                    _MAX_PATH);
        }
    }
}

/****************************************************************************
     Function: if_out_printf
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void if_out_printf(const char *format) 
{ 
    //filter the '*** ' messages from the DLL
    //"*** Ashling Opella-XD ARC Driver, 16-Nov-2007 v0.0.1"
    //"*** Say 'prop dll_help' for internal help."
    if (!strncmp(format,"*** ",4))
        return;

    printf("%s", format);
}

/****************************************************************************
     Function: if_printf
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t if_printf(struct ARC_callback *ArcInt, const char *format, ...) 
{ 
    ArcInt=ArcInt;
    my_printf_func(format);
    return 0; 
}

/****************************************************************************
     Function: if_meminfo
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void if_meminfo(struct ARC_callback *ArcInt, unsigned addr, struct Memory_info *m) 
{ 
    ArcInt=ArcInt;
    addr=addr;
    m=m;
    return; 
}
// end of supported interface 1

/****************************************************************************
     Function: if_vprintf
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t  if_vprintf(struct ARC_callback *ArcInt, const char *format, va_list ap) 
{ 
    ArcInt=ArcInt;
    format=format;
    ap=ap;
    return 0; 
}

/****************************************************************************
     Function: if_get_verbose
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t  if_get_verbose(struct ARC_callback *ArcInt) 
{ 
    ArcInt=ArcInt;
    return 0; 
}
// end of interface 2

/****************************************************************************
     Function: if_sleep
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void  if_sleep(struct ARC_callback *ArcInt, unsigned milliseconds) 
{ 
    ArcInt=ArcInt;
    milliseconds=milliseconds;
    MsSleep(milliseconds); 
}

/****************************************************************************
     Function: if_in_same_process_as_debugger
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t  if_in_same_process_as_debugger(struct ARC_callback *ArcInt) 
{ 
    ArcInt=ArcInt;
    return 1; 
}

/****************************************************************************
     Function: if_get_endian
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static struct ARC_endian*  if_get_endian(struct ARC_callback *ArcInt) 
{
    ArcInt=ArcInt;
    return NULL; 
}

/****************************************************************************
     Function: if_get_tcp_factory
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void *  if_get_tcp_factory(struct ARC_callback *ArcInt) 
{ 
    ArcInt=ArcInt;
    return NULL; 
}

/****************************************************************************
     Function: if_get_process
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
void *  if_get_process(struct ARC_callback *ArcInt) 
{ 
    ArcInt=ArcInt;
    return NULL; 
}
// end of interface 3

/****************************************************************************
     Function: if_get_debugger_access
     Engineer: Nikolay Chokoev
        Input: *******************
       Output: * As per ARCINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
struct Debugger_access * if_get_debugger_access(struct ARC_callback *ArcInt) 
{
    ArcInt=ArcInt;
    return NULL; 
}

/****************************************************************************
     Function: SetupErrorMessage
     Engineer: Nikolay Chokoev
        Input: iError - error code
       Output: none
  Description: step
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void SetupErrorMessage(int32_t iError)
{
    switch (iError)
    {
    case IDERR_LOAD_DLL_FAILED:
        PrintMessage(ERROR_MESSAGE,  "Failed to load target library %s. File not found or is corrupt.", szTargetDllName);
        break;
    case IDERR_DLL_NOT_LOADED:
        PrintMessage(ERROR_MESSAGE,  "Internal server error, target library %s not loaded.", szTargetDllName);
        break;
    case IDERR_ADD_BP_NO_MEMORY:
        PrintMessage(ERROR_MESSAGE,  "Insufficient memory while setting a breakpoint.");
        break;
    case IDERR_ADD_BP_INTERNAL1:
        PrintMessage(ERROR_MESSAGE,  "Internal error 1 occurred while setting a breakpoint.");
        break;
    case IDERR_ADD_BP_DUPLICATE:
        PrintMessage(ERROR_MESSAGE,  "Duplicate breakpoint found at address while setting a breakpoint.");
        break;
    case IDERR_REMOVE_BP_INTERNAL1:
        PrintMessage(ERROR_MESSAGE,  "Internal error 1 occurred while deleting a breakpoint.");
        break;
    case IDERR_REMOVE_BP_INTERNAL2:
        PrintMessage(ERROR_MESSAGE,  "Internal error 2 occurred while deleting a breakpoint.");
        break;
    case IDERR_REMOVE_BP_NOT_FOUND:
        PrintMessage(ERROR_MESSAGE,  "Breakpoint not found, unable to delete a breakpoint.");
        break;
    case IDERR_ASH_SRCREGISTER:
        PrintMessage(LOG_MESSAGE,    "Read attempted from an unsupported register number.");
        break;
    case IDERR_ASH_DSTREGISTER:
        PrintMessage(LOG_MESSAGE,    "Write to an unsupported register number ignored.");
        break;
    case IDERR_GDI_ERROR:
        PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
        break;
    case IDERR_DLL_NULL_FUNCTION:
        PrintMessage(ERROR_MESSAGE, "Cannot find exported functions within specified Opella-XD Interface DLL");
        break;
    case IDERR_DLL_GET_INTERFACE_ERR:
        PrintMessage(ERROR_MESSAGE, "Cannot open debug connection to Opella-XD or connection has been canceled");
        break;
    default:
        PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
        break;
    }
}

/****************************************************************************
     Function: IsBigEndian
     Engineer: Nikolay Chokoev
        Input: none
       Output: void
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
28-Nov-2007    NCH         Initial
16-Nov-2012    SPT         Corrected Big endian Test
****************************************************************************/
static void IsBigEndian()
{
    uint32_t ulIdentity;
    uint32_t ulMemsubsysValue;
    uint32_t ulIndex;

    for (ulIndex=0; ulIndex < MAX_TAP_NUM; ulIndex++)
    {
        if (tySI.tyProcConfig.Processor._ARC.tyScanChainDef[tySI.ulTAP[ ulIndex ]].bIsARCCore == TRUE)
        {
            if (tySI.ulTAP[ulIndex] == 0)
                break;

            tySI.tyProcConfig.Processor._ARC.bProcIsBigEndian[ulIndex] = TRUE;

            //Set current core
            (void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[ulIndex]);

            //andre: check version
            (void)LOW_ReadRegister(ARC_REG_IDENTITY, &ulIdentity);

            if ((ulIdentity & 0xFF) >= 0x40){
                PrintMessage(INFO_MESSAGE, "It is an ARC-v2 core!");

                (void)LOW_ReadRegister((ARC_ISA_CONFIG + AUX_BASE), &ulMemsubsysValue);

                PrintMessage(INFO_MESSAGE, "IDENTITY:  %08x", ulIdentity);
                PrintMessage(INFO_MESSAGE, "MEMSUBSYS: %08x", ulMemsubsysValue);

                if ((ulMemsubsysValue & 0x100000) == 0)
                    tySI.tyProcConfig.Processor._ARC.bProcIsBigEndian[ulIndex] = FALSE;
            }
            else{
                PrintMessage(INFO_MESSAGE, "It is an ARCompact core!");

                (void)LOW_ReadRegister(ARC_REG_MEMSUBSYS, &ulMemsubsysValue);

                PrintMessage(INFO_MESSAGE, "IDENTITY:  %08x", ulIdentity);
                PrintMessage(INFO_MESSAGE, "MEMSUBSYS: %08x", ulMemsubsysValue);

                if ((ulMemsubsysValue & 0x4) == 0)
                    tySI.tyProcConfig.Processor._ARC.bProcIsBigEndian[ulIndex] = FALSE;
            }

            PrintMessage(DEBUG_MESSAGE,
                         "Core %d BigEndian?: %d",
                         tySI.ulTAP[ulIndex],
                         tySI.tyProcConfig.Processor._ARC.bProcIsBigEndian[ulIndex]);
        }
    }
}

/****************************************************************************
     Function: ARCInitVars
     Engineer: Nikolay Chokoev
        Input: none
       Output: void
  Description: Init variables....
Date           Initials    Description
06-Dec-2007    NCH         Initial
****************************************************************************/
static void ARCInitVars()
{
    uint32_t ulIndex;

    for (ulIndex=0; ulIndex < MAX_TAP_NUM; ulIndex++)
    {
        if (tySI.ulTAP[ulIndex] == 0)
        {
            break;
        }
    }
    if (ulIndex == 0)
        ulIndex = 1;
    tySI.tyProcConfig.Processor._ARC.uiSleepTime = MAX_SLEEP_TIME / ulIndex;
}

/****************************************************************************
     Function: GetRunningStatus
     Engineer: Nikolay Chokoev
        Input: none
       Output: void
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
28-Nov-2007    NCH         Initial
****************************************************************************/
static void GetRunningStatus()
{
    int32_t           bExecuting;
    uint32_t ulIndex;

    for (ulIndex=0; ulIndex < MAX_TAP_NUM; ulIndex++)
    {
        if (tySI.tyProcConfig.Processor._ARC.tyScanChainDef[tySI.ulTAP[ ulIndex ]].bIsARCCore == TRUE)
        {
            if (tySI.ulTAP[ulIndex] == 0)
            {
                break;
            }
            //Set Core index
            (void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[ulIndex]);
            if (LOW_IsProcessorExecuting(&bExecuting, (int32_t)ulIndex) == FALSE)
            {
                //TODO: Some Error
            }
        }
    }
}
/****************************************************************************
     Function: AddBpArrayEntry
     Engineer: Nikolay Chokoev
        Input: ulBpType       : breakpoint type
               ulAddress      : breakpoint address
               ulLength       : breakpoint length
               iCurrentCoreIndex : Core index
       Output: int32_t - error code
  Description: Add a breakpoint to the breakpoint array
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static int32_t AddBpArrayEntry(uint32_t ulBpType, uint32_t ulAddress, uint32_t ulLength, unsigned char *pucInstruction, int32_t iCurrentCoreIndex)
{
    TyBpElement * ptyNewBpArray;
    TyBpElement * ptyTempBpArray;
    if (uiBpCount[iCurrentCoreIndex] >= uiBpAlloc[iCurrentCoreIndex])
    {
        ptyTempBpArray = tyCoreBPArray[iCurrentCoreIndex].ptyBpArray;
        // reallocate array adding a chunk of entries...
        ptyNewBpArray = (TyBpElement*)realloc(ptyTempBpArray, 
                                              (((uint64_t)uiBpAlloc[iCurrentCoreIndex]+GDBSERV_BP_ARRAY_ALLOC_CHUNK) * sizeof(TyBpElement)));

        if (ptyNewBpArray == NULL)
            return IDERR_ADD_BP_NO_MEMORY;

        // clear new chunk to 0...
        memset(&ptyNewBpArray[uiBpAlloc[iCurrentCoreIndex]], 0, (GDBSERV_BP_ARRAY_ALLOC_CHUNK * sizeof(TyBpElement)));

        tyCoreBPArray[iCurrentCoreIndex].ptyBpArray = ptyNewBpArray;
        uiBpAlloc[iCurrentCoreIndex]  += GDBSERV_BP_ARRAY_ALLOC_CHUNK;
    }

    // sanity check, this should not happen...
    if (uiBpCount[iCurrentCoreIndex] >= uiBpAlloc[iCurrentCoreIndex])
        return IDERR_ADD_BP_INTERNAL1;

    // sanity check, this should not happen...
    if (uiBpCount[iCurrentCoreIndex] >= uiBpAlloc[iCurrentCoreIndex])
        return IDERR_ADD_BP_INTERNAL1;

    tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiBpCount[iCurrentCoreIndex]].ulBpType     
    = ulBpType;
    tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiBpCount[iCurrentCoreIndex]].ulAddress    
    = ulAddress;
    tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiBpCount[iCurrentCoreIndex]].ulLength     
    = ulLength;
    memcpy(tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiBpCount[iCurrentCoreIndex]].ucInstruction, 
           pucInstruction, 
           ulLength);
    uiBpCount[iCurrentCoreIndex]++;
    return 0;
}

/****************************************************************************
     Function: RemoveBpArrayEntry
     Engineer: Nikolay Chokoev
        Input: iIndex : index of breakpoint to remove
               iCurrentCoreIndex : core index
       Output: TyError
  Description: Remove a breakpoint from the breakpoint array
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static int32_t RemoveBpArrayEntry(uint32_t uiIndex, int32_t iCurrentCoreIndex) 
{
    // sanity check, this should not happen...
    if (uiBpCount[iCurrentCoreIndex] >= uiBpAlloc[iCurrentCoreIndex])
        return IDERR_REMOVE_BP_INTERNAL1;
    if (uiIndex >= uiBpCount[iCurrentCoreIndex])
        return IDERR_REMOVE_BP_INTERNAL2;
    // we will not free any array storage
    // but, move all elements above iIndex down 1 place...
    for (; uiIndex < uiBpCount[iCurrentCoreIndex]-1; uiIndex++)
    {
        tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex] = 
        tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex+1];
    }
    // there is one less BP in the array
    uiBpCount[iCurrentCoreIndex]--;
    // clear the old top element
    memset(&tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiBpCount[iCurrentCoreIndex]], 0, sizeof(TyBpElement));
    return 0;
}

/****************************************************************************
     Function: RemoveAllBpArray
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: Remove all breakpoints from the breakpoint array, free array
               for all cores
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static void RemoveAllBpArray()
{
    uint32_t uiIndex;

    for (uiIndex=0; uiIndex < MAX_TAPS_IN_CHAIN+1; uiIndex++)
    {
        if (tyCoreBPArray[uiIndex].ptyBpArray)
        {
            free(tyCoreBPArray[uiIndex].ptyBpArray);
            tyCoreBPArray[uiIndex].ptyBpArray = NULL;
        }
        uiBpCount[uiIndex] = 0;
    }
}

/****************************************************************************
     Function: GetBpArrayEntry
     Engineer: Nikolay Chokoev
        Input: ulBpType       : breakpoint type
               ulAddress      : breakpoint address
               ulLength       : breakpoint length
               iCurrentCoreIndex : Core index
       Output: int32_t : breakpoint index, else -1
  Description: Returns the index of the breakpoint and instruction
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
static int32_t GetBpArrayEntry(uint32_t  ulBpType,
                           uint32_t  ulAddress,
                           uint32_t  ulLength,
                           unsigned char  *pucInstruction,
                           int32_t iCurrentCoreIndex)
{
    uint32_t uiIndex;

    for (uiIndex=0; uiIndex < uiBpCount[iCurrentCoreIndex]; uiIndex++)
    {
        if ((tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ulBpType  == ulBpType ) &&
            (tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ulAddress == ulAddress) &&
            (tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ulLength  == ulLength ))
        {
            memcpy(pucInstruction, 
                   tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ucInstruction, 
                   ulLength);
            return((int32_t)uiIndex);
        }
    }
    return -1;
}

/****************************************************************************
     Function: GetBpArrayEntryForAddr
     Engineer: Nikolay Chokoev
        Input: ulAddress      : breakpoint address
       Output: int32_t : breakpoint index, else -1
  Description: Get the index of a breakpoint in the array, using address only
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static int32_t GetBpArrayEntryForAddr(uint32_t  ulAddress,
                                  int32_t iCurrentCoreIndex)
{
    uint32_t uiIndex;

    for (uiIndex=0; uiIndex < uiBpCount[iCurrentCoreIndex]; uiIndex++)
    {
        if (tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ulAddress == ulAddress)
        {
            return((int32_t)uiIndex);
        }
    }
    return -1;
}

/****************************************************************************
     Function: LoadProperties
     Engineer: Nikolay Chokoev
        Input: void
       Output: int32_t - error code
  Description: Load file with properties definitions.
Date           Initials    Description
21-Nov-2007    NCH         Initial
*****************************************************************************/
int32_t LoadProperties(void)
{
    FILE *pFile;
    int32_t iLineNumber = 0;
    int32_t iScannedItems;
    int32_t iError = GDBSERV_NO_ERROR;
    int32_t iPropCnt=0;
    char szVariableName[_MAX_PATH];
    char pszPropName[_MAX_PATH];
    char *pszResult=NULL;
    char szLine[_MAX_PATH];
    int32_t iTRet;

    // open in text mode so that CR+LF is converted to LF
    pFile = fopen(tySI.szRegisterFile, "rt");
    if (pFile == NULL)
    {
        sprintf(tySI.szError, "Could not open properties file '%s'.", tySI.szRegisterFile);
        return 1;
    }
    while (!feof(pFile))
    {
        if (fgets(szLine, sizeof(szLine), pFile) == NULL)
            break;
        iLineNumber++;
        szVariableName[0] = '\0';
        iScannedItems = sscanf(szLine, " %s ", szVariableName);
        // if an empty line, continue with next line...
        if (iScannedItems <= 0)
            continue;
        // if a comment, continue with next line...
        if ((szVariableName[0] == '\0') || 
            (szVariableName[0] == '#' ) || 
            (szVariableName[0] == ';' ) ||
            ((szVariableName[0] == '/') && (szVariableName[1] == '/')))
            continue;
        if (iScannedItems == 1)
        {
            if (iPropCnt > MAX_USER_VARS-1)
            {
                iError = 1;
                break;
            }
            if ((pszResult=strtok(szVariableName, "=")) == NULL)
            {
                iError = 1;
                break;
            }

            if (strcmp(pszResult,"-prop"))
            {
                iError = 1;
                break;
            }

            if ((pszResult=strtok(NULL, "=")) == NULL)
            {
                iError = 1;
                break;
            }
            sprintf(pszPropName, "%s",pszResult);
            if ((pszResult=strtok(NULL, "=")) == NULL)
            {
                iError = 1;
                break;
            }
            if (!strcmp(pszPropName,"jtag_frequency"))
                strncpy(tySI.szJtagFreq, pszResult, _MAX_PATH);
            if (ptyArcInstance->ptyFunctionTable->process_property != NULL)
            {
                iTRet=ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, pszPropName, pszResult);
                PrintMessage(LOG_MESSAGE, 
                             "Set property [property] [value] [success]:\n\t\t%s\t%s\t%d", 
                             pszPropName, pszResult, iTRet);
            }
        }
        else
        {
            iError = 1;
            break;
        }
    }
    if (iError != GDBSERV_NO_ERROR)
        sprintf(tySI.szError, 
                "Invalid format at line %d of properties file '%s':\n%s",  
                iLineNumber, tySI.szRegisterFile, szLine);
    if (pFile != NULL)
        (void)fclose(pFile);
    return iError;
}

/****************************************************************************
     Function: LOW_SetJtagFreq
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
23-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_SetJtagFreq(void)
{
    switch (tySI.bJtagRtck)
    {
    case TRUE:
        if (ptyArcInstance->ptyFunctionTable->process_property != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                                   "rtck_enable",
                                                                   "1") != TRUE)
            {
                return GDBSERV_ERROR;
            }
        }
        break;

    case FALSE:
        if (ptyArcInstance->ptyFunctionTable->process_property != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                                   "rtck_enable",
                                                                   "0") != TRUE)
            {
                return GDBSERV_ERROR;
            }
        }
        if (ptyArcInstance->ptyFunctionTable->process_property != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                                   "jtag_frequency",
                                                                   tySI.szJtagFreq) != TRUE)
            {
                return GDBSERV_ERROR;
            }
        }
        break;
    default:
        return GDBSERV_ERROR;
    }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: ARCInitProps
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
23-Nov-2007    NCH          Initial
****************************************************************************/
static void ARCInitProps(void)
{
    if (LOW_IsBigEndian(0) == TRUE)
    {
        PrintMessage(DEBUG_MESSAGE,"Big Endian.");
        if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                               "endian",
                                                               "BE") != TRUE)
        {
            PrintMessage(DEBUG_MESSAGE,"Cannot set Big Endian.");
        }
    }
    else
    {
        PrintMessage(DEBUG_MESSAGE,"Little Endian.");
        if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                               "endian",
                                                               "LE") != TRUE)
        {
            PrintMessage(DEBUG_MESSAGE,"Cannot set Little Endian.");
        }
    }
}

/****************************************************************************
     Function: ARCInitInterface
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
23-Nov-2007    NCH          Initial
****************************************************************************/
static void ARCInitInterface(struct TyArcInstance *pArcInst)
{
    static struct ARC_callback MyArcCallback;

    my_printf_func = if_out_printf;

    MyArcCallback.pft=&pf; //lint !e123

    if (pArcInst->ptyFunctionTable->receive_callback != NULL)
    {
        pArcInst->ptyFunctionTable->receive_callback(pArcInst,(ARC_callback*)&MyArcCallback);
    }
}

/****************************************************************************
     Function: VerifyInterfaceDLL
     Engineer: Roshith R
        Input: void 
       Output: true or false
  Description: Verifies the given interface DLL is a valid one by searching
               for the string 'Ashling' in the ID returned by DLL
Date           Initials    Description
16-Nov-2014    RR         Initial
****************************************************************************/
static int32_t VerifyInterfaceDLL()
{
    char *pcInterfaceID;

    pcInterfaceID = pfAshGetInterfaceID();
    if (NULL == pcInterfaceID)
        return FALSE;

    /* Search for the string "Ashling" in the interface ID returned from DLL */
    if (NULL != strstr(pcInterfaceID, "Ashling"))
    {
        /* String 'Ashling' found, valid DLL */
        return TRUE;
    }
    else
    {
        /* String 'Ashling' not found, invalid DLL */
        return FALSE;
    }
}

/****************************************************************************
     Function: ARCLoadLibrary
     Engineer: Nikolay Chokoev
        Input: int32_t bInitialJtagAccess : enable to use initial JTAG access (MIPS specific)
       Output: int32_t - error code
  Description: Load ARC library for using with JTAG console
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
static int32_t ARCLoadLibrary(void)
{
    int32_t iError = 0;

    // Remove any BP details...
    RemoveAllBpArray();

    szTargetDllName[0] = '\0';
    switch (tySI.tyGdbServerTarget)
    {
    case GDBSRVTGT_OPELLAXD:
        strcpy(szTargetDllName, OPELLAXD_DLL_NAME);
        break;
    case GDBSRVTGT_OPELLA:
    case GDBSRVTGT_INVALID:
    default:
        break;
    }

    if (szTargetDllName[0] != '\0')
    {
        hInstARCDll = LoadLibrary(szTargetDllName);
    }

    if (hInstARCDll == NULL)
    {
        szTargetDllName[0] = '\0';
        switch (tySI.tyGdbServerTarget)
        {
        case GDBSRVTGT_OPELLAXD:
            strcpy(szTargetDllName, OPELLAXD_DLL_NAME_1);
            break;
        case GDBSRVTGT_OPELLA:
        case GDBSRVTGT_INVALID:
        default:
            break;
        }
        if (szTargetDllName[0] != '\0')
        {
            hInstARCDll = LoadLibrary(szTargetDllName);
        }

        if (hInstARCDll == NULL)
        {
#ifdef _LINUX
            PrintMessage(DEBUG_MESSAGE, "DLL Error: %s", dlerror());
#endif
            return IDERR_LOAD_DLL_FAILED;
        }
    }
    pfget_ARC_interface_ex     = (Tyget_ARC_interface_ex)   GetProcAddress(hInstARCDll, "get_ARC_interface_ex");
    pfAshTestScanchain         = (TyAshTestScanchain)       GetProcAddress(hInstARCDll, "ASH_TestScanchain");
    pfAshDetectScanchain       = (TyAshDetectScanchain)     GetProcAddress(hInstARCDll, "ASH_DetectScanchain");
    pfAshGetSetupItem          = (TyAshGetSetupItem)        GetProcAddress(hInstARCDll, "ASH_GetSetupItem");
    pfAshSetSetupItem          = (TyAshSetSetupItem)        GetProcAddress(hInstARCDll, "ASH_SetSetupItem");
    pfAshGetLastErrorMessage   = (TyAshGetLastErrorMessage) GetProcAddress(hInstARCDll, "ASH_GetLastErrorMessage");
    pfASH_ListConnectedProbes  = (TyASH_ListConnectedProbes)GetProcAddress(hInstARCDll, "ASH_ListConnectedProbes");
    pfAshScanIR                = (TyAshScanIR)              GetProcAddress(hInstARCDll, "ASH_GDBScanIR");
    pfAshScanDR                = (TyAshScanDR)              GetProcAddress(hInstARCDll, "ASH_GDBScanDR");
    pfAshTAPReset              = (TyAshTAPReset)            GetProcAddress(hInstARCDll, "ASH_TMSReset");
    pfAshGetInterfaceID        = (TyAshGetInterfaceId)      GetProcAddress(hInstARCDll, "ASH_GetInterfaceID");
    pfAshGetInterfaceID        = (TyAshGetInterfaceId)      GetProcAddress(hInstARCDll, "ASH_GetInterfaceID");
    pfAshUpdateScanchain       = (TyASH_UpdateScanchain)    GetProcAddress(hInstARCDll, "ASH_UpdateScanchain");

    // check if pointers are valid
    if ((pfget_ARC_interface_ex == NULL) || (pfAshTestScanchain == NULL) ||
        (pfAshGetSetupItem == NULL) || (pfAshGetLastErrorMessage == NULL) || (pfAshGetInterfaceID == NULL))
    {
        (void)FreeLibrary(hInstARCDll);
        hInstARCDll = NULL;
        return IDERR_DLL_NULL_FUNCTION;
    }

    // Check the interface DLL is a valid Ashling DLL
    if (TRUE != VerifyInterfaceDLL ())
    {
        PrintMessage(INFO_MESSAGE,"Ashing Opella-XD interface DLL is not a valid one!");
        (void)FreeLibrary(hInstARCDll);
        hInstARCDll = NULL;
        return IDERR_DLL_NULL_FUNCTION;
    }

    PrintMessage(INFO_MESSAGE,"Initializing connection ...");
    //conect to Opella SN

    if (tySI.szProbeInstance[0] == 0)
    {
        if (pfASH_ListConnectedProbes != NULL)
            pfASH_ListConnectedProbes(tySI.szProbeInstanceList, MAX_PROBE_INSTANCES, &usConnectedInstances);
        if (usConnectedInstances>0)
            sprintf(tySI.szProbeInstance, "%s",tySI.szProbeInstanceList[0]);
        PrintMessage(DEBUG_MESSAGE, "\n%s\t%d\n", tySI.szProbeInstance, usConnectedInstances);
    }
    // ok, let open debug connection
    ptyArcInstance = pfget_ARC_interface_ex(GdbArcSetupFunction);
    if (ptyArcInstance == NULL)
    {
        (void)FreeLibrary(hInstARCDll);
        hInstARCDll = NULL;
        return IDERR_DLL_GET_INTERFACE_ERR;
    }
    //NCH this is because of lint
    if (
       (ptyArcInstance->ptyFunctionTable->DummyFunction              == NULL) ||
       (ptyArcInstance->ptyFunctionTable->version                    == NULL) ||    
       (ptyArcInstance->ptyFunctionTable->id                         == NULL) ||
       (ptyArcInstance->ptyFunctionTable->destroy                    == NULL) ||
       (ptyArcInstance->ptyFunctionTable->additional_possibilities   == NULL) ||
       (ptyArcInstance->ptyFunctionTable->additional_information     == NULL) ||
       (ptyArcInstance->ptyFunctionTable->prepare_for_new_program    == NULL) ||
       (ptyArcInstance->ptyFunctionTable->process_property           == NULL) ||
       (ptyArcInstance->ptyFunctionTable->is_simulator               == NULL) ||
       (ptyArcInstance->ptyFunctionTable->step                       == NULL) ||
       (ptyArcInstance->ptyFunctionTable->run                        == NULL) ||
       (ptyArcInstance->ptyFunctionTable->read_memory                == NULL) ||
       (ptyArcInstance->ptyFunctionTable->write_memory               == NULL) ||
       (ptyArcInstance->ptyFunctionTable->read_reg                   == NULL) ||
       (ptyArcInstance->ptyFunctionTable->write_reg                  == NULL) ||
       (ptyArcInstance->ptyFunctionTable->memory_size                == NULL) ||
       (ptyArcInstance->ptyFunctionTable->set_memory_size            == NULL) ||
       (ptyArcInstance->ptyFunctionTable->set_reg_watchpoint         == NULL) ||
       (ptyArcInstance->ptyFunctionTable->remove_reg_watchpoint      == NULL) ||
       (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint         == NULL) ||
       (ptyArcInstance->ptyFunctionTable->remove_mem_watchpoint      == NULL) ||
       (ptyArcInstance->ptyFunctionTable->stopped_at_watchpoint      == NULL) ||
       (ptyArcInstance->ptyFunctionTable->stopped_at_exception       == NULL) ||
       (ptyArcInstance->ptyFunctionTable->set_breakpoint             == NULL) ||
       (ptyArcInstance->ptyFunctionTable->remove_breakpoint          == NULL) ||
       (ptyArcInstance->ptyFunctionTable->retrieve_breakpoint_code   == NULL) ||
       (ptyArcInstance->ptyFunctionTable->breakpoint_cookie_len      == NULL) ||
       (ptyArcInstance->ptyFunctionTable->at_breakpoint              == NULL) ||
       (ptyArcInstance->ptyFunctionTable->define_displays            == NULL) ||
       (ptyArcInstance->ptyFunctionTable->fill_memory                == NULL) ||
       (ptyArcInstance->ptyFunctionTable->instruction_trace_count    == NULL) ||
       (ptyArcInstance->ptyFunctionTable->get_instruction_traces     == NULL) ||
       (ptyArcInstance->ptyFunctionTable->receive_callback           == NULL) ||
       (ptyArcInstance->ptyFunctionTable->supports_feature           == NULL) ||
       (ptyArcInstance->ptyFunctionTable->data_exchange              == NULL) ||
       (ptyArcInstance->ptyFunctionTable->in_same_process_as_debugger== NULL) ||
       (ptyArcInstance->ptyFunctionTable->max_data_exchange_transfer == NULL) ||
       (ptyArcInstance->ptyFunctionTable->read_banked_reg            == NULL) ||
       (ptyArcInstance->ptyFunctionTable->write_banked_reg           == NULL) ||
       (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2        == NULL)
       )
    {
//         return IDERR_DLL_GET_INTERFACE_ERR;
    }
    //first init interface
    ARCInitInterface(ptyArcInstance);
    //second load default settings
    iError = LOW_SetJtagFreq();
    //Load properties
    (void)LoadProperties();

    (void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[0]);
    if (ptyArcInstance->ptyFunctionTable->prepare_for_new_program != NULL)
    {
        if ((ptyArcInstance->ptyFunctionTable->prepare_for_new_program(ptyArcInstance,0)) == 0)
        {
            //Error
        }
    }

    //Init 'is_big_endian' variable
    IsBigEndian();
    //Write initial properties. This must be called *after* "LoadProperties" 
    //function, because of loading the blast file!!!!!!!
    ARCInitProps();
    //Get Initial Running Status
    GetRunningStatus();
    //Set other initial variables
    ARCInitVars();

    return iError;
}

/****************************************************************************
     Function: ARCFreeLibrary
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t - error code
  Description: Free ARC library
Date           Initials    Description
14-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t ARCFreeLibrary(void)
{
    RemoveAllBpArray();              // remove any BP details...
    if (hInstARCDll == NULL)
        return 0;                     // if library not loaded, get out...
    (void)FreeLibrary(hInstARCDll);
    hInstARCDll     = NULL;
    pfAshScanIR     = NULL;
    pfAshScanDR     = NULL;
    pfAshTAPReset   = NULL;

    return ARCFreeLibrary();
}

/****************************************************************************
     Function: GetRegName
     Engineer: Nikolay Chokoev
        Input: none
       Output: uint32_t
  Description: return reg name for GDB reg number
Date           Initials    Description
30-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to use tySI.iNumberOfRegisters as the limit 
****************************************************************************/
static char *GetRegName(uint32_t ulGdbRegNumber)
{
    static char szRegName[16];
    uint32_t ulTRegIndex;

    //PrintMessage(INFO_MESSAGE, "GetRegName current Core = %lx.", g_ulCurrentTapNumber);

    for (ulTRegIndex=0;ulTRegIndex<g_ulNumberOfRegisters;ulTRegIndex++)
    {
        if (g_ptyARCReg[ulTRegIndex].ulAddrIndex == ulGdbRegNumber)
            return(char*)g_ptyARCReg[ulTRegIndex].pszRegName;
    }

    sprintf(szRegName, "0x%X", ulGdbRegNumber);
    return szRegName;
}

/****************************************************************************
     Function: GetRegAddr
     Engineer: Nikolay Chokoev
        Input: register name
       Output: TRUE if the register is found, FALSE if not
  Description: return ARC register address by register name
Date           Initials    Description
20-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to use tySI.iNumberOfRegisters as the limit
****************************************************************************/
int32_t GetRegAddr(char *pRegName, int32_t *iRegAddr)
{
    uint32_t ulIndex;

    //PrintMessage(INFO_MESSAGE, "GetRegAddr current Core = %lx.", g_ulCurrentTapNumber);

    for (ulIndex=0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
    {
        if (strcasecmp(g_ptyARCReg[ulIndex].pszRegName, pRegName) == 0)
        {
            *iRegAddr = (int32_t)g_ptyARCReg[ulIndex].ulAddrIndex;
            return TRUE;
        }
    }
    *iRegAddr = 0xFFFF;
    return FALSE;
}

/****************************************************************************
     Function: GetBpTypeName
     Engineer: Nikolay Chokoev
        Input: none
       Output: uint32_t
  Description: return BP type name
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static char  * GetBpTypeName(uint32_t ulBpType)
{
    static char szBpName[16];

    switch (ulBpType)
    {
    case BPTYPE_SOFTWARE:
        return(char*)"SW";
    case BPTYPE_HARDWARE:
        return(char*)"HW";
    case BPTYPE_WRITE:
    case BPTYPE_READ:
    case BPTYPE_ACCESS:
    default:
        sprintf(szBpName, "0x%X", ulBpType);
        return szBpName;
    }
}

/****************************************************************************
     Function: ReadFullRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               uint32_t *pulRegValue - storage for register value
       Output: int32_t - error code
  Description: Read full register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t ReadFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t *pulRegValue)
{
    NOREF(ptyRegDef);
    assert(pulRegValue != NULL);
    // not implemented
    *pulRegValue = 0;
    return 0;
}

/****************************************************************************
     Function: WriteFullRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: *ptyRegDef   : pointer to register definition struct
               ulRegValue   : register value to write
       Output: TyError
  Description: Write full register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t WriteFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t ulRegValue)
{
    NOREF(ptyRegDef);
    NOREF(ulRegValue);
    // not implemented
    return 0;
}

//==============================================
// global LOW_functions from here...
//==============================================

/****************************************************************************
     Function: LOW_Connect
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t : FALSE if error
  Description: Connect to target
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_Connect(void)
{
    int32_t iError;
    uint32_t ulIndex;

    if ((iError = ARCLoadLibrary()) != 0)
    {
        SetupErrorMessage(iError);
        (void)ARCFreeLibrary();
        return GDBSERV_ERROR;
    }
    for (ulIndex=0; ulIndex < MAX_TAP_NUM; ulIndex++)
    {
        if (tySI.tyProcConfig.Processor._ARC.tyScanChainDef[tySI.ulTAP[ ulIndex ]].bIsARCCore == TRUE)
        {
            if (tySI.ulTAP[ulIndex] == 0)
                break;

            //SWITCH CORE HERE !!!!!
            (void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[ulIndex]);
            // SetCore (tySI.ulTAP[ulIndex])
            if (!LOW_ReadRegister(ARC_REG_IDENTITY, &tySI.tyProcConfig.Processor._ARC.ulTargetIdentity[ulIndex]))
                return GDBSERV_ERROR;
        }
    }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Disconnect
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error Code
  Description: Disconnect from target
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_Disconnect(void)
{
    int32_t iError;
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Disconnecting target ...");
    if ((iError = ARCFreeLibrary()) != 0)
    {
        SetupErrorMessage(iError);
        return GDBSERV_ERROR;
    }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IsProcessorExecuting
     Engineer: Nikolay Chokoev
        Input: *pbExecuting : pointer to flag for result
       Output: int32_t : FALSE if error
  Description: Checks if processor is executing
Date           Initials    Description
26-Nov-2007    NCH         Initial
27-Mar-2008    NCH         Power Down mode
25-Aug-2008    NCH         Add A4 support
****************************************************************************/
int32_t LOW_IsProcessorExecuting(volatile int32_t *pbExecuting, int32_t iCurrentCoreIndex)
{
    uint32_t ulDebugReg;
    uint32_t ulIdentity;
    uint32_t ulStatusReg;

    *pbExecuting = 1;

    if (!LOW_ReadRegister(ARC_REG_IDENTITY, &ulIdentity))
        return GDBSERV_ERROR;

    ulIdentity = ulIdentity & 0xFF;

    ulStatusReg = ARC_REG_STATUS;
    if (ulIdentity >= 10)
    {
        ulStatusReg = ARC_REG_STATUS2;
    }

    switch (LOW_ReadRegister(ulStatusReg, &ulDebugReg))
    {
    case 2:
        tySI.tyProcConfig.Processor._ARC.iPDownFlag[iCurrentCoreIndex] |= 0x01;
        // the core is running
        return 1;
    case 1:
        tySI.tyProcConfig.Processor._ARC.iPDownFlag[iCurrentCoreIndex] &= ~0x01;
        break;
    case 0:
    default:
        return 0;
    }

    if (((ulIdentity < 10) && (ulDebugReg & ARC_REG_STATUS_H_BIT)) ||
        ((ulIdentity >= 10) && (ulDebugReg & ARC_REG_STATUS2_H_BIT)))
    {
        //Update the running flag
        tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = FALSE;
        *pbExecuting =  0;
        return 1;
    }

    if (!LOW_ReadRegister(ARC_REG_DEBUG, &ulDebugReg))
        return 0;

    if ((ulDebugReg & ARC_REG_DEBUG_SH_BIT) || 
        (ulDebugReg & ARC_REG_DEBUG_BH_BIT) || 
        (ulDebugReg & ARC_REG_DEBUG_IS_BIT))
    {
        //Update the running flag
        tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = FALSE;
        *pbExecuting =  0;
        return 1;
    }

    if (ptyArcInstance->ptyFunctionTable->at_breakpoint != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->at_breakpoint(ptyArcInstance) == 1)
        {
            //Update the running flag
            tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = 0;
            *pbExecuting =  0;
            return 1;
        }
    }

    if (ptyArcInstance->ptyFunctionTable->stopped_at_watchpoint != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->stopped_at_watchpoint(ptyArcInstance) == 1)
        {
            //Update the running flag
            tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = 0;
            *pbExecuting =  0;
            return 1;
        }
    }

    if (ptyArcInstance->ptyFunctionTable->stopped_at_exception != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->stopped_at_exception(ptyArcInstance) == 1)
        {
            //Update the running flag
            tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = 0;
            *pbExecuting =  0;
            return 1;
        }
    }

    //Update the running flag
    tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = 1;
    return 1;
}

/****************************************************************************
     Function: LOW_GetCauseOfBreak
     Engineer: Nikolay Chokoev
        Input: *ptyStopSignal : storage for cause of break
       Output: Error Code
  Description: Gets cause of last break
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_GetCauseOfBreak(TyStopSignal *ptyStopSignal)
{
    uint32_t ulDebugReg;

    *ptyStopSignal = TARGET_SIGTRAP;//TARGET_SIGSTOP; //default signal value
    if (ptyArcInstance->ptyFunctionTable->at_breakpoint != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->at_breakpoint(ptyArcInstance) == 1)
        {
            *ptyStopSignal = TARGET_SIGTRAP;
            return GDBSERV_NO_ERROR;
        }
    }
    if (ptyArcInstance->ptyFunctionTable->stopped_at_watchpoint != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->stopped_at_watchpoint(ptyArcInstance) == 1)
        {
            *ptyStopSignal =  TARGET_SIGTRAP;
            return GDBSERV_NO_ERROR;
        }
    }
    if (ptyArcInstance->ptyFunctionTable->stopped_at_exception != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->stopped_at_exception(ptyArcInstance) == 1)
        {
            *ptyStopSignal =  TARGET_SIGTERM;
            return GDBSERV_NO_ERROR;
        }
    }
    if (!LOW_ReadRegister(ARC_REG_DEBUG, &ulDebugReg))
        return GDBSERV_ERROR;
    if (ulDebugReg & ARC_REG_DEBUG_SH_BIT)
        *ptyStopSignal =  TARGET_SIGTRAP;
    if (ulDebugReg & ARC_REG_DEBUG_BH_BIT)
        *ptyStopSignal =  TARGET_SIGTRAP;
    if (ulDebugReg & ARC_REG_DEBUG_ZZ_BIT)
        *ptyStopSignal =  TARGET_SIGTRAP;
    if (ulDebugReg & ARC_REG_DEBUG_IS_BIT)
        *ptyStopSignal =  TARGET_SIGTRAP;
    if (ulDebugReg & ARC_REG_DEBUG_FH_BIT)
        *ptyStopSignal =  TARGET_SIGTRAP;
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadRegisters
     Engineer: Nikolay Chokoev
        Input: *pulRegisters : storage for registers
       Output: int32_t : FALSE if error
  Description: Reads registers
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_ReadRegisters(uint32_t *pulRegisters)
{
    uint32_t ulIndex;
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Read all registers.");

#if defined(ARC)

    //PrintMessage(INFO_MESSAGE, "LOW_ReadRegisters current Core = %lx.", g_ulCurrentTapNumber);

    for (ulIndex=0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
    {
        if ((g_ptyARCReg[ulIndex].accessType == RW) ||
            ((g_ptyARCReg[ulIndex].accessType == RO)))
        {
            if (LOW_ReadRegister(g_ptyARCReg[ulIndex].ulAddrIndex, &pulRegisters[ulIndex]) == 0)
                return 0;
        }
        else
        {
            pulRegisters[ulIndex] = 0;
        }
    }
#else
    for (ulIndex=0; ulIndex < GDBSERV_NUM_REGISTERS; ulIndex++)
    {
        if (LOW_ReadRegister(ulIndex, &pulRegisters[ulIndex]) == 0)
            return 0;
    }
#endif
    return 1;
}

/****************************************************************************
     Function: LOW_ReadRegister
     Engineer: Nikolay Chokoev
        Input: ulRegisterNumber  : register to read
               *pulRegisterValue : storage for one register
       Output: int32_t : FALSE if error; 2 - PowerDown;
  Description: Reads one register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
****************************************************************************/
int32_t LOW_ReadRegister(uint32_t   ulRegisterNumber,
                     uint32_t  *pulRegisterValue)
{
    uint32_t ulReg;

    ulReg = ulRegisterNumber;

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Read register %s ...", 
                     GetRegName(ulReg));

    if (ulReg == 0xFFFFFFFF)
    {
        *pulRegisterValue = BAD_HEX_VALUE;
    }
    else if (ptyArcInstance->ptyFunctionTable->read_reg != NULL)
    {
        switch (ptyArcInstance->ptyFunctionTable->read_reg(ptyArcInstance,
                                                           (int32_t)ulReg, 
                                                           pulRegisterValue))
        {
        case 2:
            //powerdown
            //powerdown
           if (tySI.bDebugOutput)
               PrintMessage(LOG_MESSAGE, 
                            "Failed to read register %s. Target is Powered-down.", 
                            GetRegName(ulRegisterNumber));
           return 2;
        case 1:
            //if DEBUG register has RA_BIT set then reset bit
            if (ulReg == ARC_REG_DEBUG)
            {
                if ((*pulRegisterValue & ARC_REG_DEBUG_RA_BIT) == ARC_REG_DEBUG_RA_BIT)
                {
                    *pulRegisterValue &= ~ARC_REG_DEBUG_RA_BIT;
                    (void)ptyArcInstance->ptyFunctionTable->write_reg(ptyArcInstance,
                                                                      (int32_t)ulReg,
                                                                      *pulRegisterValue);
                }
            }
            break;
        case 0:
        default:
            //failure
            if (tySI.bDebugOutput)
                PrintMessage(LOG_MESSAGE, 
                             "Failed to read register %s ...", 
                             GetRegName(ulRegisterNumber));

            return FALSE;
        }
    }

    if (ulRegisterNumber >= AUX_BASE)
    {
        ulRegisterNumber -= 0x40;

        PrintMessage(DEBUG_MESSAGE, 
                     "Read AUX register: %-16s  Address: 0x%-8lX  Value: 0x%-8lX ", 
                     GetRegName(ulRegisterNumber + 0x40), ulRegisterNumber,*pulRegisterValue);

        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Read AUX register %s value=0x%08X.", 
                         GetRegName(ulRegisterNumber + 0x40), 
                         *pulRegisterValue);
    }
    else
    {
        PrintMessage(DEBUG_MESSAGE, 
                     "Read register: %-16s  Address: 0x%-8lX  Value: 0x%-8lX ", 
                     GetRegName(ulRegisterNumber), ulRegisterNumber,*pulRegisterValue);

        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Read register %s value=0x%08X.", 
                         GetRegName(ulRegisterNumber), 
                         *pulRegisterValue);
    }

    return TRUE;
}

/****************************************************************************
     Function: LOW_WriteRegisters
     Engineer: Nikolay Chokoev
        Input: *pulRegisters : registers values to write
       Output: int32_t : FALSE if error
  Description: Writes registers
Date           Initials    Description
26-Nov-2007    NCH         Initial
23-Oct-2012    SPT         Correction for ARC (XML based register ordering)
****************************************************************************/
int32_t LOW_WriteRegisters(uint32_t *pulRegisters)
{
    uint32_t ulIndex;
    //static int32_t reg_test = 0;

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Write all registers.");

#if defined(ARC)
    //PrintMessage(INFO_MESSAGE, "LOW_WriteRegisters current Core = %lx.", g_ulCurrentTapNumber);

    for (ulIndex=0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
    {
        if ((g_ptyARCReg[ulIndex].accessType == RW))
        {
            if (LOW_WriteRegister(g_ptyARCReg[ulIndex].ulAddrIndex, pulRegisters[ulIndex]) == FALSE)
                return FALSE;
        }
    }
#else
    for (ulIndex=0; ulIndex < (unsigne int32_t)GDBSERV_NUM_REGISTERS; ulIndex++)
    {
        if (LOW_WriteRegister(ulIndex, pulRegisters[ulIndex]) == FALSE)
            return FALSE;
    }
#endif
    return TRUE;
}

/****************************************************************************
     Function: LOW_WriteRegister
     Engineer: Nikolay Chokoev
        Input: ulRegisterNumber  : register to write
               ulRegisterValue   : register value
       Output: int32_t : FALSE if error
  Description: writes one register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
****************************************************************************/
int32_t LOW_WriteRegister(uint32_t ulRegisterNumber,
                      uint32_t ulRegisterValue)
{
    uint32_t    ulReg;

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Write register %s value=0x%08X.", 
                     GetRegName(ulRegisterNumber), 
                     ulRegisterValue);

    ulReg = ulRegisterNumber;

    if (ulReg == 0xFFFFFFFF)
    {
        SetupErrorMessage(IDERR_ASH_DSTREGISTER);
        // having displayed an error message, we give good RC
        return TRUE;
    }

    if (ptyArcInstance->ptyFunctionTable->write_reg != NULL)
    {
        switch (ptyArcInstance->ptyFunctionTable->write_reg(ptyArcInstance,
                                                            (int32_t)ulReg,
                                                            ulRegisterValue))
        {
        case 2:
            //powerdown
           if (tySI.bDebugOutput)
               PrintMessage(LOG_MESSAGE, 
                            "Failed to write register %s. Target is Powered-down.", 
                            GetRegName(ulRegisterNumber));
           return 2;
        case 1:
            //success
            break;
        case 0:
        default:
            //failure
            if (tySI.bDebugOutput)
                PrintMessage(LOG_MESSAGE, 
                             "Failed to write register %s ...", 
                             GetRegName(ulRegisterNumber));

            return FALSE;
        }
    }

    if (ulRegisterNumber >= AUX_BASE)
    {
        ulRegisterNumber -= 0x40;
        PrintMessage(DEBUG_MESSAGE, 
                     "Write AUX register: %-16s  Address: 0x%-8lX  Value: 0x%-8lX.", 
                     GetRegName(ulRegisterNumber + 0x40),
                     ulRegisterNumber, 
                     ulRegisterValue);

        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Write AUX register %s value=0x%08X.", 
                         GetRegName(ulRegisterNumber + 0x40), 
                         ulRegisterValue);
    }
    else
    {
        PrintMessage(DEBUG_MESSAGE, 
                     "Write register: %-16s  Address: 0x%-8lX  Value: 0x%-8lX.", 
                     GetRegName(ulRegisterNumber),
                     ulRegisterNumber, 
                     ulRegisterValue);

        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Write register %s value=0x%08X.", 
                         GetRegName(ulRegisterNumber), 
                         ulRegisterValue);
    }

    return TRUE;
}

/****************************************************************************
     Function: LOW_ReadPC
     Engineer: Nikolay Chokoev
        Input: *pulRegisterValue : storage for PC
       Output: int32_t : FALSE if error
  Description: Reads PC register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
****************************************************************************/
int32_t LOW_ReadPC(uint32_t  *pulRegisterValue)
{
    int32_t iReg;

    if (GetRegAddr((char*)"PC", &iReg))
    {
        if (ptyArcInstance->ptyFunctionTable->read_reg != NULL)
        {
            switch (ptyArcInstance->ptyFunctionTable->read_reg(ptyArcInstance,
                                                               iReg, 
                                                               pulRegisterValue))
            {
            case 2:
                //powerdown
                break;
            case 1:
                //success
                break;
            case 0:
            default:
                //failure
                if (tySI.bDebugOutput)
                    PrintMessage(LOG_MESSAGE, "Failed to read register PC ...");

                return FALSE;
            }
        }
    }

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Read register PC value=0x%08X.", 
                     *pulRegisterValue);

    return TRUE;
}

/****************************************************************************
     Function: LOW_WritePC
     Engineer: Nikolay Chokoev
        Input: ulRegisterValue   : new PC value
       Output: Error Code
  Description: writes PC register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_WritePC(uint32_t ulRegisterValue)
{
    int32_t iReg;

    if (GetRegAddr((char*)"PC", &iReg))
    {
        if (ptyArcInstance->ptyFunctionTable->write_reg != NULL)
        {
            switch (ptyArcInstance->ptyFunctionTable->write_reg(ptyArcInstance,
                                                                iReg, 
                                                                ulRegisterValue))
            {
            case 2:
                //powerdown
                break;
            case 1:
                //success
                break;
            case 0:
            default:
                //failure
                if (tySI.bDebugOutput)
                    PrintMessage(LOG_MESSAGE, "Failed to write register PC ...");

                return GDBSERV_ERROR;
            }
        }
    }
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Write register PC value=0x%08X.", 
                     ulRegisterValue);


    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadMemory
     Engineer: Nikolay Chokoev
        Input: ulAddress  : address to read from
               ulCount    : number of bytes to read
               *pucData   : storage for data
       Output: int32_t : FALSE if error
  Description: reads memory
Date           Initials    Description
20-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_ReadMemory(uint32_t ulAddress,
                   uint32_t ulCount,
                   unsigned char *pucData)
{
    uint32_t ulTAddress;
    uint32_t ulTCount=0;
    int32_t iRCount=0;
    static unsigned char ucTData[GDBSERV_MAX_MEMORY_SIZE]={0};
    uint32_t  uiTRadAddr;
    uint32_t  uiTRadCount;
    int32_t   iContext=0;

    PrintMessage(DEBUG_MESSAGE, 
                 "Read Memory at 0x%08X for length 0x%08X.", 
                 ulAddress, 
                 ulCount);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Read Memory at 0x%08X for length 0x%08X.", 
                     ulAddress, 
                     ulCount);

    ulTAddress = ulAddress;
    ulTCount = ulCount;

    if ((uiTRadAddr = (ulAddress%4)) > 0) //adjust start address
    {
        ulTAddress = ulAddress - uiTRadAddr;
    }

    if ((uiTRadCount = (ulAddress+ulCount)%4) > 0) //adjust length -> end address
    {
        ulTCount = ulCount + (4-uiTRadCount) + uiTRadAddr;
    }
    else
    {
        ulTCount+=uiTRadAddr;
    }

    if ((ulTCount < MAX_ARC_READ_MEM_AMMOUNT) &&
        (ptyArcInstance->ptyFunctionTable->read_memory != NULL))
    {
        iRCount=ptyArcInstance->ptyFunctionTable->read_memory(ptyArcInstance,
                                                              ulTAddress,
                                                              ucTData, 
                                                              ulTCount,
                                                              iContext);

    }

    if (tySI.bDebugOutput)
    {
        uint32_t ulLineOffset, ulByteOffset, ulBytesInLine;
        char  szBuffer[_MAX_PATH];
        char  szByte[16];

        sprintf(szBuffer, "Requested Start Address: 0x%08X\n ", ulAddress);
        sprintf(szBuffer, "Actual    Start Address: 0x%08X\n ", ulTAddress);
        sprintf(szBuffer, "Requested Count: 0x%08X\n ", ulCount);
        sprintf(szBuffer, "Actual    Count: 0x%08X\n ", ulTCount);
        sprintf(szBuffer, "Return    Count: 0x%d\n ", iRCount);
        sprintf(szBuffer, "\n");
        for (ulLineOffset=0; ulLineOffset<ulTCount; ulLineOffset+=16)
        {
            sprintf(szBuffer, "0x%08X:", ulTAddress+ulLineOffset);

            ulBytesInLine = ulTCount-ulLineOffset;
            if (ulBytesInLine > 16)
                ulBytesInLine = 16;

            for (ulByteOffset=0; ulByteOffset<ulBytesInLine; ulByteOffset++)
            {
                sprintf(szByte, " %02X", ucTData[ulLineOffset+ulByteOffset]);
                strcat(szBuffer, szByte);
            }
            if (tySI.bDebugOutput)
                PrintMessage(LOG_MESSAGE, szBuffer);
        }
    }

    if ((uint32_t)iRCount < ulTCount)
    {
        return FALSE;
    }

    memset(pucData,0,ulCount);
    memcpy(pucData,&ucTData[uiTRadAddr],ulCount);

    return TRUE;
}  //lint !e550

/****************************************************************************
     Function: LOW_WriteMemory
     Engineer: Nikolay Chokoev
        Input: ulAddress  : address to write to
               ulCount    : number of bytes to write
               *pucData   : pointer to data to write
       Output: int32_t : FALSE if error
  Description: writes memory
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_WriteMemory(uint32_t ulAddress,
                    uint32_t ulCount,
                    unsigned char *pucData)
{
    int32_t  iRCount=0;
    int32_t   iContext=0;

    PrintMessage(DEBUG_MESSAGE, 
                 "Write Memory at 0x%08X for length 0x%08X.", 
                 ulAddress, ulCount);

    if (tySI.bDebugOutput)
    {
        uint32_t ulLineOffset, ulByteOffset, ulBytesInLine;
        char  szBuffer[_MAX_PATH];
        char  szByte[16];

        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Write Memory at 0x%08X for length 0x%08X.", 
                         ulAddress, ulCount);

        for (ulLineOffset=0; ulLineOffset<ulCount; ulLineOffset+=16)
        {
            sprintf(szBuffer, "0x%08X:", ulAddress+ulLineOffset);

            ulBytesInLine = ulCount-ulLineOffset;
            if (ulBytesInLine > 16)
                ulBytesInLine = 16;


            for (ulByteOffset=0; ulByteOffset<ulBytesInLine; ulByteOffset++)
            {
                sprintf(szByte, " %02X", pucData[ulLineOffset+ulByteOffset]);
                strcat(szBuffer, szByte);
            }
            if (tySI.bDebugOutput)
                PrintMessage(LOG_MESSAGE, szBuffer);
        }
    }

    if (ptyArcInstance->ptyFunctionTable->write_memory != NULL)
    {
        iRCount=ptyArcInstance->ptyFunctionTable->write_memory(ptyArcInstance,
                                                               ulAddress,
                                                               pucData, 
                                                               ulCount,
                                                               iContext); 

        if ((uint32_t)iRCount < ulCount)
        {
            return GDBSERV_ERROR;
        }
    }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SupportedBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
       Output: int32_t : TRUE if BP type supported, else false
  Description: indicate which BP types are supported
Date           Initials    Description
26-Nov-2007    NCH         Initial 
02-Jan-2013    SPT         Added Watchpoint support 
****************************************************************************/
int32_t LOW_SupportedBreakpoint(uint32_t ulBpType)
{
    switch (ulBpType)
    {
    case BPTYPE_SOFTWARE:
        return TRUE;
    case BPTYPE_HARDWARE:
        return TRUE;
    case BPTYPE_WRITE:
        return TRUE;
    case BPTYPE_READ:
        return TRUE;
    case BPTYPE_ACCESS:
        return TRUE;
    default:
        return FALSE;
    }
}

/****************************************************************************
     Function: LOW_DeleteBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
       Output: Error Code
  Description: delete a breakpoint
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
05-Aug-2008    NCH         Error Code 
01-Jan-2013    SPT         added support for Watchpoint 
****************************************************************************/
int32_t LOW_DeleteBreakpoint(uint32_t ulBpType, 
                         uint32_t ulAddress, 
                         uint32_t ulLength, 
                         int32_t iCurrentCoreIndex)
{
    int32_t iError, iBpIndex;
    unsigned char ucData[4];
    unsigned char ucBpInstruction[4];
    unsigned char ucBpTempInstruction[4];

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Delete %s breakpoint at 0x%08X for length %d (Core %d).", 
                     GetBpTypeName(ulBpType), ulAddress, ulLength, iCurrentCoreIndex);
    if (!LOW_SupportedBreakpoint(ulBpType))
    {
        //assert(0);
        //return GDBSERV_ERROR;      //lint !e527
        return IDERR_REMOVE_BP_NOT_SUPPORTED;  //lint !e527
    }

    // check if we know about this BP...
    iBpIndex = GetBpArrayEntry(ulBpType, ulAddress, ulLength, ucBpInstruction, iCurrentCoreIndex);
    if (iBpIndex < 0)
    {
        //This scenario can come in case of watchpoints because an unwanted delete break point
        //comes for each execute (c) command for every watchpoint set
        if ((ulBpType == BPTYPE_WRITE) || (ulBpType == BPTYPE_READ) || (ulBpType == BPTYPE_ACCESS))
            return GDBSERV_NO_ERROR;

        // why didn't we find the BP?
        SetupErrorMessage(IDERR_REMOVE_BP_NOT_FOUND);
        return GDBSERV_ERROR;
    }

    switch (ulBpType)
    {
    default:
    case BPTYPE_SOFTWARE:
        if ((ulLength != 2) && (ulLength != 4))
            return GDBSERV_ERROR;      //lint !e527

        if (!LOW_ReadMemory(ulAddress, ulLength, ucData))
            return GDBSERV_ERROR;
        if (ulLength == 2)
        {
            if (LOW_IsBigEndian(iCurrentCoreIndex))
            {
                if ((ucData[0] != ARC_BRK_S_INSTR_B1) || 
                    (ucData[1] != ARC_BRK_S_INSTR_B0))
                    return GDBSERV_ERROR;
            }
            else
            {
                if ((ucData[1] != ARC_BRK_S_INSTR_B1) || 
                    (ucData[0] != ARC_BRK_S_INSTR_B0))
                    return GDBSERV_ERROR;
            }
            if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulLength, ucBpInstruction))
                return GDBSERV_ERROR;
            if (!LOW_ReadMemory(ulAddress, ulLength, ucBpTempInstruction))
                return GDBSERV_ERROR;
            if ((ucBpInstruction[1] != ucBpTempInstruction[1]) || 
                (ucBpInstruction[0] != ucBpTempInstruction[0]))
            {
                PrintMessage(DEBUG_MESSAGE, "Delete BP Error!\n%02X%02X  !=  %02X%02X\n", 
                             ucBpInstruction[1],ucBpInstruction[0], 
                             ucBpTempInstruction[1],ucBpTempInstruction[0]);
                return GDBSERV_ERROR;
            }
        }
        else if (ulLength == 4)
        {
            //TODO: This is only for ARC700
            if (LOW_IsBigEndian(iCurrentCoreIndex))
            {
                if ((ucData[0] != ARC_BRK_INSTR_B3) || 
                    (ucData[1] != ARC_BRK_INSTR_B2) || 
                    (ucData[2] != ARC_BRK_INSTR_B1) || 
                    (ucData[3] != ARC_BRK_INSTR_B0))
                    return GDBSERV_ERROR;
            }
            else
            {
                if ((ucData[1] != ARC_BRK_INSTR_B3) || 
                    (ucData[0] != ARC_BRK_INSTR_B2) || 
                    (ucData[3] != ARC_BRK_INSTR_B1) || 
                    (ucData[2] != ARC_BRK_INSTR_B0))
                    return GDBSERV_ERROR;
            }
            if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulLength, ucBpInstruction))
                return GDBSERV_ERROR;
            if (!LOW_ReadMemory(ulAddress, ulLength, ucBpTempInstruction))
                return GDBSERV_ERROR;
            if ((ucBpInstruction[3] != ucBpTempInstruction[3]) || 
                (ucBpInstruction[2] != ucBpTempInstruction[2]) ||
                (ucBpInstruction[1] != ucBpTempInstruction[1]) || 
                (ucBpInstruction[0] != ucBpTempInstruction[0]))
            {
                PrintMessage(DEBUG_MESSAGE, "Delete BP Error!\n%02X%02X%02X%02X  !=  %02X%02X%02X%02X\n",
                             ucBpInstruction[3],ucBpInstruction[2], ucBpInstruction[1],ucBpInstruction[0],
                             ucBpTempInstruction[3],ucBpTempInstruction[2], ucBpTempInstruction[1],ucBpTempInstruction[0]);
                return GDBSERV_ERROR;
            }
        }
        else
            return GDBSERV_ERROR;
        break;

    case BPTYPE_HARDWARE:
        if (ptyArcInstance->ptyFunctionTable->remove_breakpoint != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->remove_breakpoint(ptyArcInstance, ulAddress, NULL) == 0)
                return GDBSERV_ERROR;
        }
        break;
    case BPTYPE_WRITE:
    case BPTYPE_READ:
    case BPTYPE_ACCESS:
        if (ptyArcInstance->ptyFunctionTable->remove_mem_watchpoint != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->remove_mem_watchpoint(ptyArcInstance, ulAddress, (int32_t) ulLength) == 0)
                return GDBSERV_ERROR;
        }
        break;
    }

    if ((iError = RemoveBpArrayEntry((uint32_t)iBpIndex, iCurrentCoreIndex)) != 0)
    {
        SetupErrorMessage(iError);
        return GDBSERV_ERROR;
    }

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SetBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
               iCurrentCoreIndex : core index
       Output: int32_t : FALSE if error
  Description: set a breakpoint
Date           Initials    Description
04-Dec-2007    NCH         Initial 
01-Jan-2013    SPT         added support for Watchpoint 
****************************************************************************/
int32_t LOW_SetBreakpoint(uint32_t ulBpType, uint32_t ulAddress, uint32_t ulLength, int32_t iCurrentCoreIndex)
{
    int32_t iError, iBpIndex;
    unsigned char ucData[4];
    unsigned char ucBpInstruction[4];
    unsigned char ucBpTempInstruction[4];

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Set %s breakpoint at 0x%08X for length %d (Core %d).", 
                     GetBpTypeName(ulBpType), ulAddress,ulLength, iCurrentCoreIndex);
    iBpIndex = GetBpArrayEntryForAddr(ulAddress, iCurrentCoreIndex);
    if (iBpIndex > 0)
    {  // there is breakpoint at this address already
        SetupErrorMessage(IDERR_ADD_BP_DUPLICATE);
        return GDBSERV_ERROR;
    }
    if (!LOW_SupportedBreakpoint(ulBpType))
        return IDERR_REMOVE_BP_NOT_SUPPORTED;

    switch (ulBpType)
    {
    default:
    case BPTYPE_SOFTWARE:
        if ((ulLength != 2) && (ulLength != 4))
            return GDBSERV_ERROR;      //lint !e527

        if (!LOW_ReadMemory(ulAddress, ulLength, ucData))
            return GDBSERV_ERROR;

        switch (ulLength)
        {
        case 2:
            if (LOW_IsBigEndian(iCurrentCoreIndex))
            {
                ucBpInstruction[0] = ARC_BRK_S_INSTR_B1;
                ucBpInstruction[1] = ARC_BRK_S_INSTR_B0;
                PrintMessage(DEBUG_MESSAGE, 
                             "Set BP Instr: %02X%02X (SBE)", 
                             ucBpInstruction[1],ucBpInstruction[0]);
            }
            else
            {
                ucBpInstruction[1] = ARC_BRK_S_INSTR_B1;
                ucBpInstruction[0] = ARC_BRK_S_INSTR_B0;
                PrintMessage(DEBUG_MESSAGE, 
                             "Set BP Instr: %02X%02X (SLE)", 
                             ucBpInstruction[1],ucBpInstruction[0]);
            }
            break;
        case 4:
            //TODO: This is only for ARC700
            if (LOW_IsBigEndian(iCurrentCoreIndex))
            {
                ucBpInstruction[0] = ARC_BRK_INSTR_B3;
                ucBpInstruction[1] = ARC_BRK_INSTR_B2;
                ucBpInstruction[2] = ARC_BRK_INSTR_B1;
                ucBpInstruction[3] = ARC_BRK_INSTR_B0;
                PrintMessage(DEBUG_MESSAGE, 
                             "Set BP Instr: %02X%02X%02X%02X (BE)", 
                             ucBpInstruction[3],ucBpInstruction[2], ucBpInstruction[1],ucBpInstruction[0]);
            }
            else
            {
                ucBpInstruction[1] = ARC_BRK_INSTR_B3;
                ucBpInstruction[0] = ARC_BRK_INSTR_B2;
                ucBpInstruction[3] = ARC_BRK_INSTR_B1;
                ucBpInstruction[2] = ARC_BRK_INSTR_B0;
                PrintMessage(DEBUG_MESSAGE, 
                             "Set BP Instr: %02X%02X%02X%02X (LE)", 
                             ucBpInstruction[3], ucBpInstruction[2], ucBpInstruction[1], ucBpInstruction[0]);
            }
            break;
        default:
            return GDBSERV_ERROR;
        }
        if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulLength, ucBpInstruction))
            return GDBSERV_ERROR;

        //in case we try to set software breakpoint to ROM
        if (!LOW_ReadMemory(ulAddress, ulLength, ucBpTempInstruction))
            return GDBSERV_ERROR;

        switch (ulLength)
        {
        case 2:
            if ((ucBpInstruction[1] != ucBpTempInstruction[1]) || 
                (ucBpInstruction[0] != ucBpTempInstruction[0]))
            {
                PrintMessage(DEBUG_MESSAGE, 
                             "%02X%02X  !=  %02X%02X\n", 
                             ucBpInstruction[1], 
                             ucBpInstruction[0], 
                             ucBpTempInstruction[1], 
                             ucBpTempInstruction[0]);
                if (tySI.bDebugOutput)
                    PrintMessage(LOG_MESSAGE, 
                                 "%02X%02X  !=  %02X%02X\n", 
                                 ucBpInstruction[1], 
                                 ucBpInstruction[0], 
                                 ucBpTempInstruction[1], 
                                 ucBpTempInstruction[0]);
                return GDBSERV_ERROR;
            }
            break;
        case 4:
            //TODO: This is only for ARC700
            if ((ucBpInstruction[3] != ucBpTempInstruction[3]) || 
                (ucBpInstruction[2] != ucBpTempInstruction[2]) ||
                (ucBpInstruction[1] != ucBpTempInstruction[1]) || 
                (ucBpInstruction[0] != ucBpTempInstruction[0]))
            {

                PrintMessage(DEBUG_MESSAGE, "%02X%02X%02X%02X  !=  %02X%02X%02X%02X\n",
                             ucBpInstruction[3],ucBpInstruction[2], ucBpInstruction[1],ucBpInstruction[0],                  
                             ucBpTempInstruction[3],ucBpTempInstruction[2], ucBpTempInstruction[1],ucBpTempInstruction[0]);
                if (tySI.bDebugOutput)
                    PrintMessage(LOG_MESSAGE, "%02X%02X%02X%02X  !=  %02X%02X%02X%02X\n",
                                 ucBpInstruction[3],ucBpInstruction[2], ucBpInstruction[1],ucBpInstruction[0],
                                 ucBpTempInstruction[3],ucBpTempInstruction[2], ucBpTempInstruction[1],ucBpTempInstruction[0]);
                return GDBSERV_ERROR;
            }
            break;
        default:
            return GDBSERV_ERROR;
        }
        break;
    case BPTYPE_HARDWARE:
        if (ptyArcInstance->ptyFunctionTable->set_breakpoint != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->set_breakpoint(ptyArcInstance, ulAddress, NULL) == 0)
                return GDBSERV_ERROR;
        }
        break;
    case BPTYPE_WRITE:
        if (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2 != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2(ptyArcInstance, ulAddress, (int32_t) ulLength, ARC_WATCHPOINT_write, NULL ) == 0)
            {
                PrintMessage(DEBUG_MESSAGE, "GDBSERV_ERROR\n");
                return GDBSERV_ERROR;
            }
        }
        break;
    case BPTYPE_READ:
        if (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2 != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2(ptyArcInstance, ulAddress, (int32_t) ulLength, ARC_WATCHPOINT_read, NULL ) == 0)
                return GDBSERV_ERROR;
        }
        break;
    case BPTYPE_ACCESS:
        if (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2 != NULL)
        {
            if (ptyArcInstance->ptyFunctionTable->set_mem_watchpoint2(ptyArcInstance, ulAddress, (int32_t) ulLength, (ARC_WATCHPOINT_read | ARC_WATCHPOINT_write) , NULL ) == 0)return GDBSERV_ERROR;
        }
        break;
    }
    if ((iError = AddBpArrayEntry(ulBpType, ulAddress, ulLength, ucData, iCurrentCoreIndex)) != 0)
    {
        SetupErrorMessage(iError);
        return GDBSERV_ERROR;
    }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IsSWBreak
     Engineer: Nikolay Chokoev
        Input: void
       Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
int32_t LOW_IsSWBreak(uint32_t uiPCValue,
                  int32_t iCurrentCoreIndex)
{
    uint32_t uiIndex;

    for (uiIndex=0; uiIndex < uiBpCount[iCurrentCoreIndex]; uiIndex++)
    {
        if (tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ulAddress == uiPCValue)
        {
            if (tyCoreBPArray[iCurrentCoreIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_SOFTWARE)
            {
                return TRUE;
            }
        }
    }
    return FALSE;
}

/****************************************************************************
     Function: LOW_PostBreakAction
     Engineer: Nikolay Chokoev
        Input: iCurrentCoreIndex : current core index
       Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
int32_t LOW_PostBreakAction(int32_t iCurrentCoreIndex)
{
    uint32_t  ulIdentity;
    uint32_t  ulDebugReg;
    uint32_t  ulPCValue;
    uint32_t  ulPCTempValue;

    //set current core as not-flushed data cache
    tySI.tyProcConfig.Processor._ARC.bDCacheFlush[iCurrentCoreIndex]=FALSE;
    //set current core as not-invalidated data cache
    tySI.tyProcConfig.Processor._ARC.bDCacheInval[iCurrentCoreIndex]=FALSE;
    //set current core as not-flushed instruction cache
    tySI.tyProcConfig.Processor._ARC.bICacheFlush[iCurrentCoreIndex]=FALSE;

    ulIdentity = 
    (tySI.tyProcConfig.Processor._ARC.ulTargetIdentity[iCurrentCoreIndex] & 0xFF);
    if (!LOW_ReadRegister(ARC_REG_DEBUG, &ulDebugReg))
    {
        return FALSE;
    }

    if (ulDebugReg & ARC_REG_DEBUG_SH_BIT)
    {
        return TRUE;
    }

    if (ulDebugReg & ARC_REG_DEBUG_BH_BIT)
    {
        //we are at breakpoint halt instruction
        if (ulIdentity <= 0x0F)
        {//ARCtangent-A4 processor family (Original 32-Bit only processor cores)
        }
        else if ((ulIdentity >= 10) && (ulIdentity <= 0x1F))
        {//ARCtangent-A5 processor family (ARCompact 16/32 Bit processor cores).
        }
        else if ((ulIdentity >= 20) && (ulIdentity <= 0x2F))
        {//ARC 600 processor family (ARCompact 16/32 Bit processor cores).

            if (!LOW_ReadPC(&ulPCValue))
            {
                return FALSE;
            }

            PrintMessage(DEBUG_MESSAGE, 
                         "Core: %d\tPC: %08lX", 
                         iCurrentCoreIndex, 
                         ulPCValue);
            ulPCValue -=2;
            if (LOW_IsSWBreak(ulPCValue, iCurrentCoreIndex))
            {
                PrintMessage(DEBUG_MESSAGE, 
                             "Software breakpoint at %08lX", 
                             ulPCValue);
                if (GDBSERV_NO_ERROR != LOW_WritePC(ulPCValue))
                {
                    return FALSE;
                }

                if (!LOW_ReadPC(&ulPCTempValue))
                {
                    return FALSE;
                }

                if (ulPCValue != ulPCTempValue)
                {
                    PrintMessage(DEBUG_MESSAGE, 
                                 "Write PC:%08lX\tRead PC:%08lX", 
                                 ulPCValue, 
                                 ulPCTempValue);
                    return FALSE;
                }
            }
        }
        else if ((ulIdentity >= 30) && (ulIdentity <= 0x3F))
        {//ARC 700 processor family (ARCompact 16/32 Bit processor cores).
        }
        else
        {//Unknown
        }
    }
    if (ulDebugReg & ARC_REG_DEBUG_ZZ_BIT)
    {
        return TRUE;
    }
    if (ulDebugReg & ARC_REG_DEBUG_IS_BIT)
    {
        return TRUE;
    }
    if (ulDebugReg & ARC_REG_DEBUG_FH_BIT)
    {
        return TRUE;
    }

    return TRUE;
}

/****************************************************************************
     Function: LOW_Continue
     Engineer: Nikolay Chokoev
        Input: *ptyStopSignal : storage for cause of break
       Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_Continue(TyStopSignal *ptyStopSignal,
                 int32_t iCurrentCore)
{
    ptyStopSignal=ptyStopSignal;

    PrintMessage(DEBUG_MESSAGE, "Continue.");
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Continue.");

    if (ptyArcInstance->ptyFunctionTable->run != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->run(ptyArcInstance) == 0)
            return FALSE;
    }
    else
    {
        return FALSE;
    }

    //Set the flag...
    tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCore] = TRUE;

    return TRUE;

}

/****************************************************************************
     Function: LOW_Exexute
     Engineer: Nikolay Chokoev
        Input: void
       Output: int32_t : FALSE if error
  Description: start execution of target for download without listening to server
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_Execute(char* pszDownloadFileName)
{
    int32_t          bExecuting = FALSE;
    pszDownloadFileName=pszDownloadFileName;

    PrintMessage(DEBUG_MESSAGE, "Execute.");

    // Now we must wait until the processor has stopped executing or the user hits any key
    for (;;)
    {
        if (LOW_IsProcessorExecuting(&bExecuting, 0) == FALSE)
            return FALSE;

        if (!bExecuting)
        {
            PrintMessage(DEBUG_MESSAGE,
                         "Execution of %s terminated normally.", 
                         pszDownloadFileName);
            break;
        }

        if (KeyHit())
        {
            // while executing we treat any key press from the user as a halt...
            PrintMessage(INFO_MESSAGE,
                         "Key press detected, halting execution of %s.", 
                         pszDownloadFileName);
            if (LOW_Halt(0) == FALSE)
                return FALSE;
            break;
        }
        MsSleep(100);
    }

    return TRUE;

}

/****************************************************************************
     Function: LOW_Step
     Engineer: Nikolay Chokoev
        Input: *ptyStopSignal : storage for cause of break
       Output: int32_t : FALSE if error
  Description: step
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
int32_t LOW_Step(TyStopSignal *ptyStopSignal)
{

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Step.");

    if (ptyArcInstance->ptyFunctionTable->step != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->step(ptyArcInstance) == 0)
        {
            return FALSE;
        }
    }
    else
    {
        return FALSE;
    }

    if (LOW_GetCauseOfBreak(ptyStopSignal) != GDBSERV_NO_ERROR)
        return FALSE;

    return TRUE;
}

/****************************************************************************
     Function: LOW_Halt
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t : FALSE if error
  Description: halt execution of target
Date           Initials    Description
28-Nov-2007    NCH         Initial
06-Aug-2008    NCH         Used DEBUG Register to halt
****************************************************************************/
int32_t LOW_Halt(int32_t iCurrentCoreIndex)
{
    uint32_t ulRegVal;

	 printf("Halt--------------------\n");
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Halt.");

    if (!LOW_ReadRegister(ARC_REG_DEBUG, &ulRegVal))
    {
        return GDBSERV_ERROR;
    }
    ulRegVal |= ARC_REG_DEBUG_FH_BIT; //set HALT bit
    if (!LOW_WriteRegister(ARC_REG_DEBUG, ulRegVal))
    {
        return GDBSERV_ERROR;
    }

    //Update the running flag
    tySI.tyProcConfig.Processor._ARC.bRunningFlag[iCurrentCoreIndex] = FALSE;
    //set current core as not-flushed data cache
    tySI.tyProcConfig.Processor._ARC.bDCacheFlush[iCurrentCoreIndex] = FALSE;
    //set current core as not-invalidated data cache
    tySI.tyProcConfig.Processor._ARC.bDCacheInval[iCurrentCoreIndex] = FALSE;
    //set current core as not-flushed instruction cache
    tySI.tyProcConfig.Processor._ARC.bICacheFlush[iCurrentCoreIndex] = FALSE;

    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ResetTarget
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error Code
  Description: reset target
Date           Initials    Description
29-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ResetTarget(void)
{
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Hwreset.");

    if (ptyArcInstance->ptyFunctionTable->process_property != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                               "reset_target",
                                                               "1") != TRUE)
        {
            return GDBSERV_ERROR;
        }
    }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_GetDWFWVersions
     Engineer: Nikolay Chokoev
        Input: none
       Output: Error Code
  Description: get diskware firmware version information
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code  
****************************************************************************/
int32_t LOW_GetDWFWVersions(char *pszDiskWare1,
                        char *pszFirmWare,
                        char *pszFpgaWare1,
                        char *pszFpgaWare2,
                        char *pszFpgaWare3,
                        char *pszFpgaWare4)
{
    pszDiskWare1[0] = '\0';
    pszFirmWare [0] = '\0';
    pszFpgaWare1[0] = '\0';
    pszFpgaWare2[0] = '\0';
    pszFpgaWare3[0] = '\0';
    pszFpgaWare4[0] = '\0';

    my_printf_func = if_filt_printf;
    if (ptyArcInstance->ptyFunctionTable->process_property != NULL)
    {
        if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,
                                                               "probe_info",
                                                               "1") != TRUE)
        {
            return GDBSERV_ERROR;
        }
    }
    my_printf_func = if_out_printf;
    strncpy(pszDiskWare1,
            tySI.tyProcConfig.Processor._ARC.szDiskwareVer,
            _MAX_PATH);
    strncpy(pszFirmWare,
            tySI.tyProcConfig.Processor._ARC.szFirmwareVer,
            _MAX_PATH);
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IsBigEndian
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
28-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_IsBigEndian(int32_t iCoreIndex)
{
    return tySI.tyProcConfig.Processor._ARC.bProcIsBigEndian[iCoreIndex];
}

/****************************************************************************
     Function: LOW_ConnectJcon
     Engineer: Nikolay Chokoev
        Input: int32_t bInitialJtagAccess : enable using initial jtag access (MIPS specific)
       Output: int32_t : FALSE if error
  Description: Connect to target (use for JTAG console)
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_ConnectJcon(int32_t bInitialJtagAccess)
{
    bInitialJtagAccess=bInitialJtagAccess;
    return LOW_Connect();
}

/****************************************************************************
     Function: LOW_DisconnectJcon
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t : FALSE if error
  Description: Disconnect from target (use for JTAG console)
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_DisconnectJcon(void)
{
    return LOW_Disconnect();
}


/****************************************************************************
     Function: LOW_ScanIR
     Engineer: Nikolay Chokoev
        Input: uint32_t * pulDataOut : array of uint32_t to be shifted into IR
               uint32_t * pulDataIn  : array of uint32_t for data to be shifted from IR
               uint32_t ulDataLength : number of bits shifted into/from IR
       Output: int32_t : Error Code
  Description: shift specified data into IR and read data shifted out
Date           Initials    Description
10-Dec-2007    NCH          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ScanIR(uint32_t *pulDataIn, 
               uint32_t *pulDataOut, 
               uint32_t ulDataLength)
{
    assert(pulDataOut != NULL);
    assert(pulDataIn  != NULL);

    // call a function in DLL
    if (pfAshScanIR != NULL)
    {
        if (pfAshScanIR(ptyArcInstance, ulDataLength, pulDataIn, pulDataOut) == TRUE)
        {
            return GDBSERV_NO_ERROR;
        }
        return GDBSERV_ERROR;
    }
    else
        return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_ScanDR
     Engineer: Nikolay Chokoev
        Input: uint32_t * pulDataOut : array of uint32_t to be shifted into DR
               uint32_t * pulDataIn  : array of uint32_t for data to be shifted from DR
               uint32_t ulDataLength : number of bits shifted into/from DR
       Output: int32_t : Error code
  Description: shift specified data into DR and read data shifted out
Date           Initials    Description
10-Dec-2007    NCH          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ScanDR(uint32_t *pulDataIn, 
               uint32_t *pulDataOut, 
               uint32_t ulDataLength)
{
    assert(pulDataOut != NULL);
    assert(pulDataIn  != NULL);

    // call a function in DLL
    if (pfAshScanDR != NULL)
    {
        if (pfAshScanDR(ptyArcInstance, ulDataLength, pulDataIn, pulDataOut)==TRUE)
        {
            return GDBSERV_NO_ERROR;
        }
        return GDBSERV_ERROR;
    }
    else
        return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_ResetTAP
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t : FALSE if error
  Description: reset Jtag TAP
Date           Initials    Description
26-Nov-2007    NCH         Initial
09-Sep-2009    RSB         TAP Reset Done
****************************************************************************/
int32_t LOW_ResetTAP(void)
{
    if (pfAshTAPReset != NULL)
    {
        if (pfAshTAPReset(ptyArcInstance)==TRUE)
        {
            return GDBSERV_NO_ERROR;
        }
        return GDBSERV_ERROR;
    }
    else
        return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_ReadRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               uint32_t *pulRegValue - storage for register value
       Output: int32_t - error code
  Description: Read a register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef, 
                             uint32_t *pulRegValue)
{
    int32_t iError;
    uint32_t ulValue = 0;
    assert(ptyRegDef != NULL);
    assert(pulRegValue != NULL);
    if ((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulValue)) != 0)
    {
        if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "Read register %s ...", 
                         GetRegDefDescription(ptyRegDef));
        SetupErrorMessage(iError);
        return GDBSERV_ERROR;
    }
    *pulRegValue = ExtractRegBits(ulValue, ptyRegDef);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Read register %s value=0x%08X.", 
                     GetRegDefDescription(ptyRegDef), *pulRegValue);
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WriteRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               uint32_t ulRegValue - register value to write
       Output: int32_t - error code
  Description: Write a register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef,
                              uint32_t ulRegValue)
{
    int32_t iError;
    uint32_t ulOldValue;
    assert(ptyRegDef != NULL);
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, 
                     "Write register %s value=0x%08X.", 
                     GetRegDefDescription(ptyRegDef), ulRegValue);
    if (ptyRegDef->ubBitSize < 32)
    {
        if ((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulOldValue)) != 0)
        {
            SetupErrorMessage(iError);
            return 0;
        }
        ulRegValue = MergeRegBits(ulOldValue, ulRegValue, ptyRegDef);
    }
    if ((iError = WriteFullRegisterByRegDef(ptyRegDef, ulRegValue)) != 0)
    {
        SetupErrorMessage(iError);
        return 0;
    }
    return 1;
}

/****************************************************************************
     Function: LOW_AddCoreRegDefArrayEntries
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t - error code
  Description: Add register definitions for core registers
Date           Initials    Description
26-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to add SetUpArcRegs function
****************************************************************************/
int32_t LOW_AddCoreRegDefArrayEntries(void)
{
    uint32_t ulIndex;
    int32_t iError;

    //Setting up ARC registers for the first time 
    if ((iError = SetUpArcRegs()) != GDBSERV_NO_ERROR )
        return iError;

    //PrintMessage(INFO_MESSAGE, "LOW_AddCoreRegDefArrayEntries current Core = %lx.", g_ulCurrentTapNumber);

    // add all ARC registers
    for (ulIndex = 0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
    {
        if ((iError = CFG_AddRegDefArrayEntry(g_ptyARCReg[ulIndex].pszRegName, g_ptyARCReg[ulIndex].ulAddrIndex, MEM_REG, 4, 0, 32, -1,1)) != GDBSERV_NO_ERROR)
            return iError;
    }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IndexCommonRegDefArrayEntries
     Engineer: Nikolay Chokoev
        Input: none
       Output: int32_t - error code
  Description: Index common registers in reg definition array for fast access
Date           Initials    Description
26-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to use tySI.iNumberOfRegisters as the limit
****************************************************************************/
int32_t LOW_IndexCommonRegDefArrayEntries(void)
{
    uint32_t ulIndex;
    uint32_t  uiRegDefIndex;
    TyRegDef *ptyRegDef;
    TyRegDef *ptyLastFullRegDef = NULL;

    //PrintMessage(INFO_MESSAGE, " LOW_IndexCommonRegDefArrayEntries current Core = %lx.", g_ulCurrentTapNumber);

    for (uiRegDefIndex=0; uiRegDefIndex < tySI.uiRegDefArrayEntryCount; uiRegDefIndex++)
    {
        ptyRegDef = &tySI.ptyRegDefArray[uiRegDefIndex];
        if (ptyRegDef->ubBitSize >= 32)
            ptyLastFullRegDef = ptyRegDef;
        for (ulIndex=0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
        {
            if ((g_ptyARCReg[ulIndex].ulAddrIndex == 0xFFFFFFFF) && 
                (strcasecmp(g_ptyARCReg[ulIndex].pszRegName, ptyRegDef->pszRegName) == 0))
                g_ptyARCReg[ulIndex].ulAddrIndex = uiRegDefIndex;
        }
        // special case for the endian mode bit
        if ((iRegDefEndianMode < 0) && (ptyLastFullRegDef != NULL) && 
            (strcasecmp(ptyLastFullRegDef->pszRegName, "Config") == 0))
            iRegDefEndianMode = (int32_t)uiRegDefIndex;
    }
    return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Detach
     Engineer: Nikolay Chokoev
        Input: None
       Output: Error Code
  Description: resume execution of target
Date           Initials    Description
26-Nov-2007    NCH          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_Detach(int32_t iCurrentCoreIndex)
{
    int32_t bExecuting = FALSE;

    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Detach.");

    if (LOW_IsProcessorExecuting(&bExecuting, iCurrentCoreIndex) == FALSE)
        return GDBSERV_ERROR;
    else
        return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_TestScanChain
     Engineer: Nikolay Chokoev
        Input: None
       Output: int32_t : FALSE if error
  Description: test scanchain
Date           Initials    Description
03-Dec-2007    NCH          Initial
****************************************************************************/
int32_t LOW_TestScanChain(void)
{
    return(pfAshTestScanchain(ptyArcInstance));
} 

/****************************************************************************
     Function: LOW_DetectScanChain
     Engineer: Nikolay Chokoev
        Input: uint32_t *pulNumCores : pointer to number of cores
       Output: int32_t : FALSE if OK
  Description: detect scanchain
Date           Initials    Description
03-Dec-2007    NCH          Initial
****************************************************************************/
int32_t LOW_DetectScanChain(uint32_t *pulNumCores)
{
    uint32_t uiNumberOfCores;
    uint32_t ulIRWidth[MAX_TAPS_IN_CHAIN];
    unsigned char bARCCores[MAX_TAPS_IN_CHAIN];
    uint32_t uiIndex;

    (void)pfAshDetectScanchain(ptyArcInstance,
                               MAX_TAPS_IN_CHAIN,
                               &uiNumberOfCores,
                               ulIRWidth,
                               bARCCores);

    memset(tySI.tyProcConfig.Processor._ARC.tyScanChainDetected,
           0,
           sizeof(tySI.tyProcConfig.Processor._ARC.tyScanChainDetected));

    for (uiIndex=0; uiIndex < uiNumberOfCores; uiIndex++)
    {
        tySI.tyProcConfig.Processor._ARC.tyScanChainDetected[uiIndex].bIsARCCore =
        bARCCores[uiIndex];
        tySI.tyProcConfig.Processor._ARC.tyScanChainDetected[uiIndex].ulIRlength =
        ulIRWidth[uiIndex];
    }

    *pulNumCores = uiNumberOfCores;
    return FALSE;
}

/****************************************************************************
     Function: LOW_SetActiveTapIndex
     Engineer: Nikolay Chokoev
        Input: iIndex : index to the Arc core; 0 is 1st, 1 is 2nd ...
       Output: int32_t : FALSE if OK
  Description: detect scanchain
Date           Initials    Description
04-Dec-2007    NCH          Initial
****************************************************************************/
int32_t LOW_SetActiveTapIndex(int32_t iIndex)
{
    int32_t  iError;
    char szCpuNum[3];

    if (tySI.ulCurrentTapNumber == (uint32_t)iIndex)
        return 0;

    tySI.ulCurrentTapNumber = (uint32_t)iIndex;

    sprintf(szCpuNum,"%d",(iIndex));

    if (tySI.bMultiRegFileDetected == 1)
    {
        //PrintMessage(INFO_MESSAGE, "SetActiveCoreIndex to %x.", iIndex);

        if (tySI.ulMultiRegFileCount < (uint32_t)iIndex)
        {
            PrintMessage(ERROR_MESSAGE, "No XML file definition found for Core %lx", iIndex);
            return 1;
        }
        
        // now add commonly used registers...
        if ((iError = LOW_AddCoreRegDefArrayEntries()) != GDBSERV_NO_ERROR)
        {
            return iError;
        }
        
        // now index commonly used registers...
        if ((iError = LOW_IndexCommonRegDefArrayEntries()) != GDBSERV_NO_ERROR)
        {
            return iError;
        }
    }

    if (ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "cpunum", szCpuNum) == 0)
      return TRUE;

    return FALSE;
}

/****************************************************************************
     Function: LOW_FlushDCache
     Engineer: Nikolay Chokoev
        Input: iCurrentCoreIndex : index to the Arc core; 0 is 1st, 1 is 2nd ...
       Output: int32_t : FALSE if OK
  Description: Flush the cache
Date           Initials    Description
10-Jan-2008    NCH          Initial
****************************************************************************/
int32_t LOW_FlushDCache(int32_t iCurrentCoreIndex)
{
    uint32_t  ulRegValue;
    int32_t            bDCacheFlushDone;
    int32_t            iTimeOut;
    int32_t            iErr=0;

    bDCacheFlushDone =
    tySI.tyProcConfig.Processor._ARC.bDCacheFlush[iCurrentCoreIndex];

    iTimeOut=0;

    PrintMessage(DEBUG_MESSAGE, "bDCacheFlushDone: %d",bDCacheFlushDone);

    if (!bDCacheFlushDone)
    {
        if (LOW_ReadRegister(ARC_DC_CTRL, &ulRegValue) == FALSE)
            iErr=TRUE;
        //check if cache is enabled
        if ((ulRegValue & ARC_DC_CTRL_DC_BIT)==0)
        {
            //flush
            if (LOW_WriteRegister(ARC_DC_FLSH, ARC_DC_FLSH_FL_BIT) == FALSE)
                iErr=TRUE;
        }
        //if OK continue
        if (!iErr)
        {
            //check if flushed
            if (LOW_ReadRegister(ARC_DC_CTRL, &ulRegValue) == FALSE)
                iErr=TRUE;
            //if not - retry
            while ((ulRegValue & ARC_DC_CTRL_FS_BIT) &&
                   (iTimeOut < ARC_CACHE_FLUSH_RETRY))
            {
                MsSleep(1);
                if (LOW_ReadRegister(ARC_DC_CTRL, &ulRegValue) == FALSE)
                    iErr=TRUE;
                iTimeOut++;
            }
            //check timeout exit
            if (iTimeOut == ARC_CACHE_FLUSH_RETRY)
            {
                //timeout
                PrintMessage(DEBUG_MESSAGE, "Data Cache Flush TimeOut!");
                return TRUE;
            }

            tySI.tyProcConfig.Processor._ARC.bDCacheFlush[iCurrentCoreIndex]=TRUE;

        }
        else
        {
            PrintMessage(DEBUG_MESSAGE,"Flush Cache Error!");
        }
    }

    return iErr;
}

/****************************************************************************
     Function: LOW_InvalidateDCache
     Engineer: Nikolay Chokoev
        Input: iCurrentCoreIndex : index to the Arc core; 0 is 1st, 1 is 2nd ...
       Output: int32_t : FALSE if OK
  Description: Invalidate the cache
Date           Initials    Description
10-Jan-2008    NCH          Initial
****************************************************************************/
int32_t LOW_InvalidateDCache(int32_t iCurrentCoreIndex)
{
    uint32_t  ulRegValue;
    int32_t            bDCacheInvalDone;
    int32_t            iErr=0;

    bDCacheInvalDone =
    tySI.tyProcConfig.Processor._ARC.bDCacheInval[iCurrentCoreIndex];

    PrintMessage(DEBUG_MESSAGE, "bDCacheFlushDone: %d", bDCacheInvalDone);

    if (!bDCacheInvalDone)
    {
        if (LOW_ReadRegister(ARC_DC_CTRL, &ulRegValue) == FALSE)
            iErr=TRUE;
        //check if cache is enabled
        if ((ulRegValue & ARC_DC_CTRL_DC_BIT)==0)
        {
            //invalidate
            if (LOW_WriteRegister(ARC_DC_IVDC, ARC_DC_IVDC_IV_BIT) == FALSE)
                iErr=TRUE;
        }
        //if OK continue
        if (!iErr)
            tySI.tyProcConfig.Processor._ARC.bDCacheInval[iCurrentCoreIndex]=TRUE;
        else
        {
            PrintMessage(DEBUG_MESSAGE, "Invalidate Cache Error!");
        }
    }

    return iErr;
}

/****************************************************************************
     Function: LOW_CloseDevice
     Engineer: Nikolay Chokoev
        Input: void
       Output: void
  Description: Close Opella-XD
Date           Initials    Description
05-Aug-2008    NCH         Initial
****************************************************************************/
int32_t LOW_CloseDevice(int32_t iIndex)
{
    NOREF(iIndex);
    return TRUE;
}

/****************************************************************************
     Function: LOW_StartAsIDEMode
     Engineer: Nikolay Chokoev
        Input: None
       Output: int32_t : FALSE if error
  Description: Starts in AsIDE mode. Loads MDI library functions.
Date           Initials    Description
05-Aug-2008    NCH         Initial
****************************************************************************/
int32_t LOW_StartAsIDEMode(void)
{
    if (tySI.bDebugOutput)
        PrintMessage(LOG_MESSAGE, "Starting in AsIDE mode...Loading MDI libraries...");

    return GDBSERV_ERROR;

}

/****************************************************************************
     Function: Is_StoppedAtWatchpoint
     Engineer: Sreeshma T.
        Input: None
       Output: int32_t : FALSE if error
  Description: 
Date           Initials    Description
16-Mar-2016    ST          Initial
****************************************************************************/
int32_t Is_StoppedAtWatchpoint(void)
{
    if (ptyArcInstance->ptyFunctionTable->stopped_at_watchpoint(ptyArcInstance) == 1)
    {
        return 1;
    }
    else
    {
        return 0;
    }

}

int32_t Low_GetMulticore(uint32_t *pulnumberCore, uint32_t *pulcoreinfo)
{
    //char szDevices[10];
    char szDevInfo[10];

    if(pulcoreinfo == NULL)
        {
        return 0;
        }
    pfAshGetSetupItem(ptyArcInstance, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, szDevInfo);

	if (!sscanf(szDevInfo, "%d", pulnumberCore))
		return 0;

    
    return 0;

}

int32_t Low_SetMulticore(uint32_t ulnumberCore, uint32_t *pulcoreinfo)
{
    uint32_t ulIndex;
    char szDevices[10];

    if(pulcoreinfo == NULL)
        {
        return 0;
        }

    sprintf(szDevices, "%d", (int32_t)ulnumberCore);
    pfAshSetSetupItem(ptyArcInstance, ARC_SETUP_SECT_MULTICORE, ARC_SETUP_ITEM_NUMBEROFCORES, szDevices);

    for (ulIndex=1; ulIndex < ulnumberCore + 1; ulIndex++)
    {        
        sprintf(szDevices, "Device%d", (uint32_t)ulIndex);

        if(pulcoreinfo[ulIndex] == 1)
            {
            pfAshSetSetupItem(ptyArcInstance, szDevices, ARC_SETUP_ITEM_ARCCORE, "1");
            }
        else
            {
            pfAshSetSetupItem(ptyArcInstance, szDevices, ARC_SETUP_ITEM_ARCCORE, "0");
            }
        
    }

    pfAshUpdateScanchain(ptyArcInstance);

    return 0;
}
