/****************************************************************************
	   Module: gdbdsp.c
	 Engineer: Vitezslav Hola
  Description: Ashling GDB Server, Dispatch GDB commands
Date           Initials    Description
12-Dec-2007    VH          Initial
31-Mar-2008    VK/RS       Modified for ARM support
23-Sep-2009    SVL         Modified for PFXD mode
18-May-2010    SPC         Corrected error returns for GDBServer MIPS
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbdsp.h"
#include "gdbserver/moncommand/gdbmonitor.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbserv.h"
#ifdef ARM
extern uint32_t gulPC;
#endif

#ifdef ARC
#include "gdbserver/arc/gdbarc.h"
extern TyARCReg     *g_ptyARCReg;
extern uint32_t g_ulNumberOfRegisters;
#endif

#ifdef RISCV
#include "gdbserver/riscv/gdbriscv.h"
extern TyRISCVReg     *g_ptyRISCVReg;
extern uint32_t g_ulNumberOfRegisters;
extern uint32_t g_ulArraySize;


#endif

extern TyServerInfo tySI;

/*
   ASSUMPTIONS:
   ------------

   All input strings are terminated with a '\0' character
	  Termination must be done prior to any checksum or end of transmission
	  characters.

   All input strings which pass the checksum checking are valid. Therefore
   no checking is done when decoding from the ascii characters into the
   function parameters (i.e. from the input packet to addresses, counts,
   etc.).

   All xxxxToString helper funcitons automatically append an '\0' character
   to the output strings


   NOTES:
   ------
   Return value from all DSP_xxx functions denote whether a response
   packet should be sent back or not.

   Detach doesn't do anything at the moment.
   May be more useful up at the main communications loop?
 */

typedef int32_t(*TyCommandFunction)(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
typedef struct
{
	TyCommandFunction ptyFunction;
	int32_t bIsConnectionLess;
}TyCommandFunctionTable_t;


static int32_t DSP_ReadRegisters(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_WriteRegisters(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_ReadARegister(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_WriteARegister(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_ReadMemory(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_WriteMemory(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_WriteBinary(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_DeleteBreakpoint(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_SetBreakpoint(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_Continue(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_Step(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_LastSignal(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_Reset(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_SearchMemory(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_GeneralQuery(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_StepWithSignal(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_ContinueWithSignal(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_GeneralSet(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_ToggleDebug(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_KillTarget(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_Detach(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_Restart(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_SetThread(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_CheckThreadAlive(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_NotSupported(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_vPacket(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
static int32_t DSP_Halt(char *pcInput, char *pcOutput, unsigned int uiOutputSize, int iCurrentCoreIndex);

// as dispatching GDB commands should be as fast as possible, we are using lookup table indexed with command code
// lookup table is array of 256 pointers to processing function
// if current command is not defined, we use DSP_NotSupported to handle this case
// as a result, decoding GDB command should take O(1) time (we do not have to go through structure everytime)
static TyCommandFunctionTable_t ptyCommandLookupTable[] =
{
		{DSP_NotSupported, 0},          // command 0x00, ASCII NUL
		{DSP_NotSupported, 0},          // command 0x01, ASCII SOH
		{DSP_NotSupported, 0},          // command 0x02, ASCII STX
		{DSP_Halt, 0},					// command 0x03, ASCII ETX
		{DSP_NotSupported, 0},          // command 0x04, ASCII EOT
		{DSP_NotSupported, 0},          // command 0x05, ASCII ENQ
		{DSP_NotSupported, 0},          // command 0x06, ASCII ACK
		{DSP_NotSupported, 0},          // command 0x07, ASCII BEL
		{DSP_NotSupported, 0},          // command 0x08, ASCII BS
		{DSP_NotSupported, 0},          // command 0x09, ASCII TAB
		{DSP_NotSupported, 0},          // command 0x0A, ASCII LF
		{DSP_NotSupported, 0},          // command 0x0B, ASCII VT
		{DSP_NotSupported, 0},          // command 0x0C, ASCII FF
		{DSP_NotSupported, 0},          // command 0x0D, ASCII CR
		{DSP_NotSupported, 0},          // command 0x0E, ASCII SO
		{DSP_NotSupported, 0},          // command 0x0F, ASCII SI
		{DSP_NotSupported, 0},          // command 0x10, ASCII DLE
		{DSP_NotSupported, 0},          // command 0x11, ASCII DC1
		{DSP_NotSupported, 0},          // command 0x12, ASCII DC2
		{DSP_NotSupported, 0},          // command 0x13, ASCII DC3
		{DSP_NotSupported, 0},          // command 0x14, ASCII DC4
		{DSP_NotSupported, 0},          // command 0x15, ASCII NAK
		{DSP_NotSupported, 0},          // command 0x16, ASCII SYN
		{DSP_NotSupported, 0},          // command 0x17, ASCII ETB
		{DSP_NotSupported, 0},          // command 0x18, ASCII CAN
		{DSP_NotSupported, 0},          // command 0x19, ASCII EM
		{DSP_NotSupported, 0},          // command 0x1A, ASCII SUB
		{DSP_NotSupported, 0},          // command 0x1B, ASCII ESC
		{DSP_NotSupported, 0},          // command 0x1C, ASCII FS
		{DSP_NotSupported, 0},          // command 0x1D, ASCII GS
		{DSP_NotSupported, 0},          // command 0x1E, ASCII RS
		{DSP_NotSupported, 0},          // command 0x1F, ASCII US
		{DSP_NotSupported, 0},          // command 0x20, ASCII ' '
		{DSP_NotSupported, 0},          // command 0x21, ASCII '!'
		{DSP_NotSupported, 0},          // command 0x22, ASCII '"'
		{DSP_NotSupported, 0},          // command 0x23, ASCII '#'
		{DSP_NotSupported, 0},          // command 0x24, ASCII '$'
		{DSP_NotSupported, 0},          // command 0x25, ASCII '%'
		{DSP_NotSupported, 0},          // command 0x26, ASCII '&'
		{DSP_NotSupported, 0},          // command 0x27, ASCII '''
		{DSP_NotSupported, 0},          // command 0x28, ASCII '('
		{DSP_NotSupported, 0},          // command 0x29, ASCII ')'
		{DSP_NotSupported, 0},          // command 0x2A, ASCII '*'
		{DSP_NotSupported, 0},          // command 0x2B, ASCII '+'
		{DSP_NotSupported, 0},          // command 0x2C, ASCII ','
		{DSP_NotSupported, 0},          // command 0x2D, ASCII '-'
		{DSP_NotSupported, 0},          // command 0x2E, ASCII '.'
		{DSP_NotSupported, 0},          // command 0x2F, ASCII '/'
		{DSP_NotSupported, 0},          // command 0x30, ASCII '0'
		{DSP_NotSupported, 0},          // command 0x31, ASCII '1'
		{DSP_NotSupported, 0},          // command 0x32, ASCII '2'
		{DSP_NotSupported, 0},          // command 0x33, ASCII '3'
		{DSP_NotSupported, 0},          // command 0x34, ASCII '4'
		{DSP_NotSupported, 0},          // command 0x35, ASCII '5'
		{DSP_NotSupported, 0},          // command 0x36, ASCII '6'
		{DSP_NotSupported, 0},          // command 0x37, ASCII '7'
		{DSP_NotSupported, 0},          // command 0x38, ASCII '8'
		{DSP_NotSupported, 0},          // command 0x39, ASCII '9'
		{DSP_NotSupported, 0},          // command 0x3A, ASCII ':'
		{DSP_NotSupported, 0},          // command 0x3B, ASCII ';'
		{DSP_NotSupported, 0},          // command 0x3C, ASCII '<'
		{DSP_NotSupported, 0},          // command 0x3D, ASCII '='
		{DSP_NotSupported, 0},          // command 0x3E, ASCII '>'
		{DSP_LastSignal,   1},          // command 0x3F, ASCII '?', last signal
		{DSP_NotSupported, 0},          // command 0x40, ASCII '@'
		{DSP_NotSupported, 0},          // command 0x41, ASCII 'A'
		{DSP_NotSupported, 0},          // command 0x42, ASCII 'B'
		{DSP_ContinueWithSignal, 0},    // command 0x43, ASCII 'C', continue with signal
		{DSP_Detach,       0},          // command 0x44, ASCII 'D', detach
		{DSP_NotSupported, 0},          // command 0x45, ASCII 'E'
		{DSP_NotSupported, 0},          // command 0x46, ASCII 'F'
		{DSP_WriteRegisters, 0},        // command 0x47, ASCII 'G', write registers
		{DSP_SetThread,    1},          // command 0x48, ASCII 'H', thread
		{DSP_NotSupported, 0},          // command 0x49, ASCII 'I'
		{DSP_NotSupported, 0},          // command 0x4A, ASCII 'J'
		{DSP_NotSupported, 0},          // command 0x4B, ASCII 'K'
		{DSP_NotSupported, 0},          // command 0x4C, ASCII 'L'
		{DSP_WriteMemory,  0},          // command 0x4D, ASCII 'M', write memory
		{DSP_NotSupported, 0},          // command 0x4E, ASCII 'N'
		{DSP_NotSupported, 0},          // command 0x4F, ASCII 'O'
		{DSP_WriteARegister, 0},        // command 0x50, ASCII 'P', write A register
		{DSP_GeneralSet,   0},          // command 0x51, ASCII 'Q', general set
		{DSP_Restart,      0},          // command 0x52, ASCII 'R', restart
		{DSP_StepWithSignal, 0},        // command 0x53, ASCII 'S', step with signal
		{DSP_CheckThreadAlive, 1},      // command 0x54, ASCII 'T'
		{DSP_NotSupported, 0},          // command 0x55, ASCII 'U'
		{DSP_NotSupported, 0},          // command 0x56, ASCII 'V'
		{DSP_NotSupported, 0},          // command 0x57, ASCII 'W'
		{DSP_WriteBinary,  0},          // command 0x58, ASCII 'X', write binary
		{DSP_NotSupported, 0},          // command 0x59, ASCII 'Y'
		{DSP_SetBreakpoint, 0},         // command 0x5A, ASCII 'Z', set breakpoint
		{DSP_NotSupported, 0},          // command 0x5B, ASCII '['
		{DSP_NotSupported, 0},          // command 0x5C, ASCII '\'
		{DSP_NotSupported, 0},          // command 0x5D, ASCII ']'
		{DSP_NotSupported, 0},          // command 0x5E, ASCII '^'
		{DSP_NotSupported, 0},          // command 0x5F, ASCII '_'
		{DSP_NotSupported, 0},          // command 0x60, ASCII '`'
		{DSP_NotSupported, 0},          // command 0x61, ASCII 'a'
		{DSP_NotSupported, 0},          // command 0x62, ASCII 'b'
		{DSP_Continue,     0},          // command 0x63, ASCII 'c', continue
		{DSP_ToggleDebug,  0},          // command 0x64, ASCII 'd', toggle debug
		{DSP_NotSupported, 0},          // command 0x65, ASCII 'e'
		{DSP_NotSupported, 0},          // command 0x66, ASCII 'f'
		{DSP_ReadRegisters, 1},         // command 0x67, ASCII 'g', read registers
		{DSP_NotSupported, 0},          // command 0x68, ASCII 'h'
		{DSP_NotSupported, 0},          // command 0x69, ASCII 'i'
		{DSP_NotSupported, 0},          // command 0x6A, ASCII 'j'
		{DSP_KillTarget,   0},          // command 0x6B, ASCII 'k', kill target
		{DSP_NotSupported, 0},          // command 0x6C, ASCII 'l'
		{DSP_ReadMemory,   0},          // command 0x6D, ASCII 'm', read memory
		{DSP_NotSupported, 0},          // command 0x6E, ASCII 'n'
		{DSP_NotSupported, 0},          // command 0x6F, ASCII 'o'
		{DSP_ReadARegister, 0},         // command 0x70, ASCII 'p', read A register
		{DSP_GeneralQuery, 1},          // command 0x71, ASCII 'q', general query
		{DSP_Reset,        0},          // command 0x72, ASCII 'r', reset
		{DSP_Step,         0},          // command 0x73, ASCII 's', step
		{DSP_SearchMemory, 0},          // command 0x74, ASCII 't', search memory
		{DSP_NotSupported, 0},          // command 0x75, ASCII 'u'
		{DSP_vPacket,     0},          // command 0x76, ASCII 'v'
		{DSP_NotSupported, 0},          // command 0x77, ASCII 'w'
		{DSP_NotSupported, 0},          // command 0x78, ASCII 'x'
		{DSP_NotSupported, 0},          // command 0x79, ASCII 'y'
		{DSP_DeleteBreakpoint, 0},      // command 0x7A, ASCII 'z', delete breakpoint
		{DSP_NotSupported, 0},          // command 0x7B, ASCII '{'
		{DSP_NotSupported, 0},          // command 0x7C, ASCII '|'
		{DSP_NotSupported, 0},          // command 0x7D, ASCII '}'
		{DSP_NotSupported, 0},          // command 0x7E, ASCII '~'
		{DSP_NotSupported, 0}           // command 0x7F, ASCII DEL
};
#define CMD_LOOKUP_TABLE_SIZE       (sizeof(ptyCommandLookupTable) / (sizeof(TyCommandFunction) + sizeof(int32_t)))
TyStopSignal  tyGlobalStopSignal = TARGET_SIGTRAP;
static unsigned char pucMemoryBuffer[GDBSERV_MAX_MEMORY_SIZE] = { 0 };    // size for memory buffer must be greater that any possible access

#ifdef ARM
extern unsigned short usMonHwbpOption;
#endif

/****************************************************************************
	 Function: DSP_Halt
	 Engineer: Rejeesh S B
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   unsigned int uiOutputSize - size of output buffer
			   int iCurrentCoreIndex - current core index
	   Output: int - >0 if response should be sent
  Description: Halt Target
Date           Initials    Description
18-Apr-2019    RSB          Initial
****************************************************************************/
static int DSP_Halt(char *pcInput, char *pcOutput, unsigned int uiOutputSize, int iCurrentCoreIndex)
{
	NOREF(pcInput);
	NOREF(pcOutput);
	NOREF(uiOutputSize);
	NOREF(iCurrentCoreIndex);

	PrintMessage(DEBUG_MESSAGE, "DSP_Halt");

	if (GDBSERV_NO_ERROR != LOW_Halt(iCurrentCoreIndex))
	{
		strcpy(pcOutput, "E30");
		return TRUE;
	}
	/*Prepare stop reply packet*/
	(void)DSP_ContinueReply(TARGET_SIGINT, pcOutput);

	return TRUE;
}
/****************************************************************************
	 Function: DSP_Decode
	 Engineer: Vitezslav Hola
		Input: char *pcInput  - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - maximum size of output buffer
			   int32_t iCurrentCoreIndex - core selection
	   Output: int32_t -  TRUE if response should be sent
  Description: decode GDB packet and call handler
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
int32_t DSP_Decode(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	pcOutput[0] = '\0';  // it is important to create empty string as initial response
	// we need to dispatch command as fast as possible using dispatch table
	if ((unsigned char)pcInput[0] >= CMD_LOOKUP_TABLE_SIZE)
		return DSP_NotSupported(pcInput, pcOutput, uiOutputSize, iCurrentCoreIndex);
	else
	{
		if (0 == ptyCommandLookupTable[(unsigned char)pcInput[0]].bIsConnectionLess &&
			0 == tySI.bIsConnected)
		{
			// Ignore
			PrintMessage(LOG_MESSAGE, "This command is not supported when GDB Server not connected to target");
			return DSP_NotSupported(pcInput, pcOutput, uiOutputSize, iCurrentCoreIndex);
		}
		else
			return ptyCommandLookupTable[(unsigned char)pcInput[0]].ptyFunction(pcInput, pcOutput, uiOutputSize, iCurrentCoreIndex);
	}
}

/****************************************************************************
	 Function: DSP_vPacket
	 Engineer: Rejeesh S Babu
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: Handle vPacket
Date           Initials    Description
20-Mar-2012    RSB          Initial
20-Sep-2012    RSB          Modidified 't' packet handling to fix GDB crash issue (Mantis ID: 1839)
12-Dec-2012    RSB          Split into different functions
****************************************************************************/
static int32_t DSP_vPacket(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	PrintMessage(DEBUG_MESSAGE, "DSP_vPacket");
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	NOREF(iCurrentCoreIndex);
	assert(uiOutputSize > 0);

#ifdef RISCV
	int iRet = 0;
	/*Check if the packet is vCont packet*/
	if (!strncmp(pcInput, "vCont", 5))
	{
		iRet = LOW_vContPacket(pcInput, pcOutput, uiOutputSize, iCurrentCoreIndex);
		switch (iRet)
		{
		case GDBSERV_TARGET_RUNNING:
			return 0; // No need to send any packet to GDB
		case GDBSERV_NO_ERROR:
			return 1;
		case GDBSERV_ERROR:
		default:
			strncat(pcOutput, "E30", uiOutputSize);
			pcOutput[uiOutputSize - 1] = '\0';
			return 1;
		}
	}
	else if (!strncmp(pcInput, "vStopped", 5))
	{
		//return LOW_vStoppedPacket(pcInput, pcOutput, uiOutputSize, iCurrentCoreIndex);
	}
	else
	{
		strcat(pcOutput, "");
	}
#endif

	return 1;
}
/****************************************************************************
	 Function: DSP_ReadRegisters
	 Engineer: Vitezslav Hola
		Input: char *pcInput  - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - maximum size of output buffer
			   int32_t iCurrentCoreIndex - core selection
	   Output: int32_t -  TRUE if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Support added
31-Mar-2008    VK       Modified for ARM support
23-Sep-2009    SVL      Modified for PFXD mode
 ****************************************************************************/
static int32_t DSP_ReadRegisters(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{

#ifdef ARC
    uint32_t *pulARCRegisters;
#elif defined RISCV
	uint32_t *pulRISCVRegisters;
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
#else
	uint32_t pulRegisters[GDBSERV_NUM_REGISTERS];
#endif


	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// read registers
	PrintMessage(DEBUG_MESSAGE, "DSP_ReadRegisters");
#if defined (ARM)

	/* PFXD Mode - Read only if it is connected */
	if (tySI.bIsConnected)
	{
		if (!LOW_ReadRegisters(pulRegisters))
		{
			strncat(pcOutput, "E30", uiOutputSize);
			pcOutput[uiOutputSize - 1] = '\0';
			return 1;
		}
		// fill register as string
		RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
	}
	else
	{
		int32_t iCnt;
		for (iCnt = 0; iCnt < GDBSERV_NUM_REGISTERS - 1; iCnt++)
		{
			pulRegisters[iCnt] = 0x0;
		}
		/* initialization value for CPSR register.  This can be 0xA00000D3. This
		is done to ensure that processor is in SUPERVISOR mode. */
		pulRegisters[iCnt] = 0xA00000F3;
		//RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
		RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
	}
	if defined(ARM)

		/* PFXD Mode - Read only if it is connected */
		if (tySI.bIsConnected)
		{
			if (!LOW_ReadRegisters(pulRegisters))
			{
				strncat(pcOutput, "E30", uiOutputSize);
				pcOutput[uiOutputSize - 1] = '\0';
				return 1;
			}
			// fill register as string
			RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
		}
		else
		{
			int32_t iCnt;
			for (iCnt = 0; iCnt < GDBSERV_NUM_REGISTERS - 1; iCnt++)
			{
				pulRegisters[iCnt] = 0x0;
			}
			/* initialization value for CPSR register.  This can be 0xA00000D3. This
			is done to ensure that processor is in SUPERVISOR mode. */
			pulRegisters[iCnt] = 0xA00000F3;
			//RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
			RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
		}

#elif defined(ARC)
    /* PFXD Mode - Read only if it is connected */
    pulARCRegisters =  (uint32_t *) malloc(((uint32_t) g_ulNumberOfRegisters) * sizeof(uint32_t));
    if (tySI.bIsConnected)
    {
        if (!LOW_ReadRegisters(pulARCRegisters))
        {
            strncat(pcOutput, "E30", uiOutputSize);
            pcOutput[uiOutputSize-1] = '\0';
            free(pulARCRegisters);
            return 1;
        }
        // fill register as string
        RegistersToStrings(pulARCRegisters, &pcOutput[0], (uint32_t)g_ulNumberOfRegisters);
    }
    else
    {
        uint32_t ulIndex;
        for (ulIndex=0; ulIndex < g_ulNumberOfRegisters-1; ulIndex++)
        {
            if (pulARCRegisters)
                *(pulARCRegisters+ulIndex) = 0x0;
        }
        /* initialization value for CPSR register.  This can be 0xA00000D3. This 
        is done to ensure that processor is in SUPERVISOR mode. */
        if (pulARCRegisters)
            *(pulARCRegisters+ulIndex) = 0xA00000F3;
        //RegistersToStrings(pulARCRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
        RegistersToStrings(pulARCRegisters, &pcOutput[0], g_ulNumberOfRegisters);
    }
    free(pulARCRegisters);
#elif defined(RISCV)
	/* PFXD Mode - Read only if it is connected */
	g_ulNumberOfRegisters = 33;
	if (!tySI.bIsVegaBoard)
	{
		switch (tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32)
		{
		case 1:
			g_ulArraySize = 33;
			break;
		case 2:
			g_ulArraySize = 66;
			break;
		case 4:
			g_ulArraySize = 132;
			break;
		default:
			break;
		}
	}
	else
	{
		g_ulArraySize = 33;
	}
	pulRISCVRegisters = (uint32_t *)malloc(((uint32_t)g_ulArraySize) * sizeof(uint32_t));
	if (tySI.bIsConnected)
	{
		if (GDBSERV_NO_ERROR != LOW_ReadRegisters(pulRISCVRegisters))
		{
			strncat(pcOutput, "E30", uiOutputSize);
			pcOutput[uiOutputSize - 1] = '\0';
			free(pulRISCVRegisters);
			return 1;
		}
		// fill register as string
		//RegistersToStrings(pulRISCVRegisters, &pcOutput[0], (uint32_t)g_ulNumberOfRegisters);
		RegistersToStrings(pulRISCVRegisters, &pcOutput[0], g_ulArraySize);
	}
	else
	{
		uint32_t ulIndex;
		for (ulIndex = 0; ulIndex < g_ulNumberOfRegisters - 1; ulIndex++)
		{
			if (pulRISCVRegisters)
				*(pulRISCVRegisters + ulIndex) = 0x0;
		}
		/* initialization value for CPSR register.  This can be 0xA00000D3. This
		is done to ensure that processor is in SUPERVISOR mode. */
		if (pulRISCVRegisters)
			*(pulRISCVRegisters + ulIndex) = 0xA00000F3;
		//RegistersToStrings(pulRISCVRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
		RegistersToStrings(pulRISCVRegisters, &pcOutput[0], 32);
	}
	free(pulRISCVRegisters);
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.bEJTAGDMAOnly && tySI.bIsConnected)
	{
		if (GDBSERV_NO_ERROR != LOW_ReadRegisters(pulRegisters))
		{
			strncat(pcOutput, "E30", uiOutputSize);
			pcOutput[uiOutputSize - 1] = '\0';
			return 1;
		}
		// fill register as string
		RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
	}
	else
	{
		int32_t iCnt;
		for (iCnt = 0; iCnt < GDBSERV_NUM_REGISTERS; iCnt++)
			pulRegisters[iCnt] = 0x0;
		//RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
		RegistersToStrings(pulRegisters, &pcOutput[0], GDBSERV_NUM_REGISTERS);
	}
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_WriteRegisters
	 Engineer: Vitezslav Hola
		Input: char *pcInput  - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - maximum size of output buffer
			   int32_t iCurrentCoreIndex - core selection
	   Output: int32_t -  TRUE if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Support
31-Mar-2008    VK       Modified for ARM support
 ****************************************************************************/
static int32_t DSP_WriteRegisters(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
#if defined(ARC) || defined (RISCV)
	uint32_t *pulRegisters = NULL;
#else
	uint32_t pulRegisters[GDBSERV_NUM_REGISTERS];
#endif
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// write registers
	PrintMessage(DEBUG_MESSAGE, "DSP_WriteRegisters");

#if defined(ARC) || defined (RISCV)
	pulRegisters = (uint32_t*)malloc(g_ulNumberOfRegisters * sizeof(uint32_t));
	if (NULL == pulRegisters)
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	//g_ulNumberOfRegisters = 32;
	StringsToRegisters(&pcInput[1], pulRegisters, g_ulNumberOfRegisters);
#else
	StringsToRegisters(&pcInput[1], pulRegisters, GDBSERV_NUM_REGISTERS);
#endif
#if defined(ARC) || defined (ARM)
	if (!LOW_WriteRegisters(pulRegisters))
	{
#if defined(ARC)
		free(pulRegisters);
		pulRegisters = NULL;
#endif
		strncat(pcOutput, "E30", uiOutputSize);
		pcOutput[uiOutputSize - 1] = '\0';
		return 1;
	}
	strcat(pcOutput, "OK");
#elif defined(RISCV)
		if (GDBSERV_NO_ERROR != LOW_WriteRegisters(pulRegisters))
		{
			free(pulRegisters);
			pulRegisters = NULL;
			strcat(pcOutput, "E30");
			return 1;
		}
		strcat(pcOutput, "OK");
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (GDBSERV_NO_ERROR != LOW_WriteRegisters(pulRegisters))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
		strcat(pcOutput, "OK");
	}
	else
	{
		strcat(pcOutput, "OK");
	}
#else
#error "This architecture is not supported."
#endif

#if defined(ARC) || defined (RISCV)
	free(pulRegisters);
	pulRegisters = NULL;
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_ReadARegister
	 Engineer: Vitezslav Hola
		Input: char *pcInput  - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - maximum size of output buffer
			   int32_t iCurrentCoreIndex - core selection
	   Output: int32_t -  TRUE if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    VK       Modified for ARM support
 ****************************************************************************/
static int32_t DSP_ReadARegister(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	uint32_t ulRegisterNumber;
#if defined(RISCV)
	uint32_t *ulRegisterValue;
	uint32_t ulThredId;

	ulThredId = LOW_CurrentHart();

	ulRegisterValue = (uint32_t *)calloc((uint32_t)(tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32), sizeof(uint32_t));
	if (ulRegisterValue == NULL)
	{
		PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
		strcat(pcOutput, "E30");
		return 1;
	}
#else
	uint32_t ulRegisterValue;
	uint32_t i;
	uint32_t ulGdbRegisterValue;
#endif

	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	//PrintMessage(INFO_MESSAGE, "DSP_ReadARegister");
	//PrintMessage(DEBUG_MESSAGE, "DSP_ReadARegister");
#if defined (ARM)
	if (!CheckNumberIsPresent(&pcInput[1], &ulRegisterNumber))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	if (!LOW_ReadRegister(ulRegisterNumber, &ulRegisterValue))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	RegistersToStrings(&ulRegisterValue, &pcOutput[0], 1);

#elif defined(ARC) 
	if (!CheckNumberIsPresent(&pcInput[1], &ulGdbRegisterValue))
	{
		strcat(pcOutput, "E30");
		return 1;
	}

	ulRegisterNumber = 0;

	if (tySI.bIsRegFilePresent)
	{
		for (i = 0; ((uint32_t)g_ulNumberOfRegisters); i++)
		{
			if (g_ptyARCReg[i].ulGdbRegIndex == ulGdbRegisterValue)
			{
				ulRegisterNumber = g_ptyARCReg[i].ulAddrIndex;
				break;
			}
		}
		if (!LOW_ReadRegister(ulRegisterNumber, &ulRegisterValue))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	else
	{
		if (!LOW_ReadRegister(ulRegisterNumber, &ulRegisterValue))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	RegistersToStrings(&ulRegisterValue, &pcOutput[0], 1);
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (!CheckNumberIsPresent(&pcInput[1], &ulRegisterNumber))
		{
			strcat(pcOutput, "E30");
			return TRUE;
		}

		if (GDBSERV_NO_ERROR != LOW_ReadRegister(ulRegisterNumber, &ulRegisterValue))
		{
			strcat(pcOutput, "E30");
			return 1;
		}

		RegistersToStrings(&ulRegisterValue, &pcOutput[0], 1);
	}
	else
	{
		return 1;
	}
#elif (RISCV)

	if (!CheckNumberIsPresent(&pcInput[1], &ulRegisterNumber))
	{
		//Returning 0xDEADDEAD value for avoiding error in GDB
		*ulRegisterValue = 0xDEADDEAD;
		RegistersToStrings(ulRegisterValue, &pcOutput[0], (tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32));
		free(ulRegisterValue);
		return 1;
	}

	if (tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS) {
		if (ulRegisterNumber == REG_S0) {
			*ulRegisterValue = tyThrdInfo[ulThredId].ptyDiscovery->ulFpValue;
		}
		else if (ulRegisterNumber == REG_S1) {
			*ulRegisterValue = tyThrdInfo[ulThredId].ptyDiscovery->ulS1Value;
		}
		else if (GDBSERV_NO_ERROR != LOW_ReadRegister(ulRegisterNumber, ulRegisterValue)) {
			//Returning 0xDEADDEAD value for avoiding error in GDB
			*ulRegisterValue = 0xDEADDEAD;
		}
	}
	else if (GDBSERV_NO_ERROR != LOW_ReadRegister(ulRegisterNumber, ulRegisterValue)) {
		//Returning 0xDEADDEAD value for avoiding error in GDB
		*ulRegisterValue = 0xDEADDEAD;
	}
	RegistersToStrings(ulRegisterValue, &pcOutput[0], (tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32));
	free(ulRegisterValue);
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_WriteARegister
	 Engineer: Vitezslav Hola
		Input: char *pcInput  - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - maximum size of output buffer
			   int32_t iCurrentCoreIndex - core selection
	   Output: int32_t -  TRUE if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    VK       Modified for ARM support
 ****************************************************************************/
static int32_t DSP_WriteARegister(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
#if defined(RISCV)
	uint32_t *ulRegisterValue;
	uint32_t **pulvalTemp = &ulRegisterValue;
	uint32_t ulThredId;
#else
	uint32_t ulRegisterNumber;
	uint32_t ulRegisterValue;
#endif
	uint32_t ulGdbRegisterValue;
	uint32_t i = 0;

	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	PrintMessage(DEBUG_MESSAGE, "DSP_WriteARegister");

#if defined(RISCV)
	ulThredId = LOW_CurrentHart();
	//printf("DSP_WriteARegister\n");
	ulRegisterValue = (uint32_t *)calloc((uint32_t)(tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32), sizeof(uint32_t));
	if (ulRegisterValue == NULL)
	{
		PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
		strcat(pcOutput, "E30");
		return 1;
	}
	
	StringsToRegisterAndValue(&pcInput[1], &ulGdbRegisterValue, ulRegisterValue);
	ulRegisterValue = *pulvalTemp;
#else
    StringsToRegisterAndValue(&pcInput[1], &ulGdbRegisterValue, &ulRegisterValue);
#endif

#if defined (ARM)
	ulRegisterNumber = ulGdbRegisterValue;
	if (!LOW_WriteRegister(ulRegisterNumber, ulRegisterValue))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#elif defined(ARC)
    ulRegisterNumber = 0;

    for (i = 0; ((uint32_t) g_ulNumberOfRegisters); i++)
    {
        if (g_ptyARCReg[i].ulGdbRegIndex == ulGdbRegisterValue)
        {
            ulRegisterNumber = g_ptyARCReg[i].ulAddrIndex;
            break;
        }
    }
    PrintMessage(DEBUG_MESSAGE, "ulRegisterNumber= %x %x\n", ulRegisterNumber, ulGdbRegisterValue);
    if (!LOW_WriteRegister(ulRegisterNumber, ulRegisterValue))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	strcat(pcOutput, "OK");
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (GDBSERV_NO_ERROR != LOW_WriteRegister(ulRegisterNumber, ulRegisterValue))
		{
			strcat(pcOutput, "E30");
			return TRUE;
		}
		strcat(pcOutput, "OK");
	}
	else
	{
		strcat(pcOutput, "OK");
	}
#elif defined(RISCV)
	if (ulGdbRegisterValue >= 32)
	{
		if (ulGdbRegisterValue == 32)
		{
			if (GDBSERV_NO_ERROR != LOW_WritePC(ulRegisterValue))
			{
				strcat(pcOutput, "E30");
				return 1;
			}
		}
		else
		{
			if (GDBSERV_NO_ERROR != LOW_WriteCSR(ulRegisterValue, ulGdbRegisterValue))
			{
				strcat(pcOutput, "E30");
				return 1;
			}
		}
	}
	else {
		if (tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS) {
			if (ulGdbRegisterValue == REG_S0) {
				tyThrdInfo[ulThredId].ptyDiscovery->ulFpValue = *ulRegisterValue;
				strcat(pcOutput, "OK");
				return 1;
			}
			else if (ulGdbRegisterValue == REG_S1) {
				tyThrdInfo[ulThredId].ptyDiscovery->ulS1Value = *ulRegisterValue;
				strcat(pcOutput, "OK");
				return 1;
			}
		}
		if (GDBSERV_NO_ERROR != LOW_WriteRegister(ulGdbRegisterValue, ulRegisterValue)) {
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	strcat(pcOutput, "OK");
	
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_ReadMemory
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    RS          Modified for ARM support
 ****************************************************************************/
static int32_t DSP_ReadMemory(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
	uint32_t ulCount;

	NOREF(uiOutputSize);

	PrintMessage(DEBUG_MESSAGE, "DSP_ReadMemory");
	StringsToAddressAndCount(&pcInput[1], &ulAddress, &ulCount);
	if (ulCount > sizeof(pucMemoryBuffer))
		ulCount = sizeof(pucMemoryBuffer);
#if defined(ARC)
	if (LOW_FlushDCache(iCurrentCoreIndex) == TRUE)
	{
		PrintMessage(DEBUG_MESSAGE, "Data Cache Flush Failed!");
	}
	if (!LOW_ReadMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#elif defined(MIPS) || defined (ARM)
	NOREF(iCurrentCoreIndex);
	if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#elif defined(RISCV)
	if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}

#else
#error "This architecture is not supported."
#endif

	MemoryValuesToStrings(pucMemoryBuffer, &pcOutput[0], ulCount);
	return 1;
}

/****************************************************************************
	 Function: DSP_WriteMemory
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
 ****************************************************************************/
static int32_t DSP_WriteMemory(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
	uint32_t ulCount;

	NOREF(uiOutputSize);

	PrintMessage(DEBUG_MESSAGE, "DSP_WriteMemory");
	StringsToAddressCountAndData(&pcInput[1], &ulAddress, &ulCount, pucMemoryBuffer, sizeof(pucMemoryBuffer));

#if defined(ARC)
	//Flush and invalidate memory
	if (LOW_FlushDCache(iCurrentCoreIndex) == TRUE)
	{
		PrintMessage(DEBUG_MESSAGE, "Data Cache Flush Failed!");
	}
	if (LOW_InvalidateDCache(iCurrentCoreIndex) == TRUE)
	{
		PrintMessage(DEBUG_MESSAGE, "Data Cache Invalidate Failed!");
	}
	if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#elif defined(MIPS) || defined (ARM)
	NOREF(iCurrentCoreIndex);
	if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#elif defined (RISCV)
if (GDBSERV_NO_ERROR != LOW_FlushDCache(iCurrentCoreIndex))
	{
		PrintMessage(DEBUG_MESSAGE, "Data Cache Flush Failed!");
		strcat(pcOutput, "E30");
		return 1;
	}
	if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#else
#error "This architecture is not supported."
#endif
	strcat(pcOutput, "OK");
	return 1;
}

/****************************************************************************
	 Function: DSP_DeleteBreakpoint
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    RS             Modified for ARM support
 ****************************************************************************/
static int32_t DSP_DeleteBreakpoint(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
	uint32_t ulLength;
	uint32_t ulBpType;
#if defined(ARC) || defined(RISCV)
	int32_t iError = 0;
#endif
	NOREF(uiOutputSize);
	NOREF(iCurrentCoreIndex);
	PrintMessage(DEBUG_MESSAGE, "DSP_DeleteBreakpoint");
	StringsToBpTypeAddressAndLength(&pcInput[1], &ulBpType, &ulAddress, &ulLength);
#if defined(ARM)
	if (GDBSERV_NO_ERROR != LOW_DeleteBreakpoint(ulBpType, ulAddress, ulLength, iCurrentCoreIndex))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	strcat(pcOutput, "OK");
#elif defined(ARC) || defined(RISCV)
	iError = LOW_DeleteBreakpoint(ulBpType, ulAddress, ulLength, iCurrentCoreIndex);
	if (iError == IDERR_REMOVE_BP_NOT_SUPPORTED)
	{
		strcat(pcOutput, "");
		return 1;
	}
	else if (iError != GDBSERV_NO_ERROR)
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	strcat(pcOutput, "OK");
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (GDBSERV_NO_ERROR != LOW_DeleteBreakpoint(ulBpType, ulAddress, ulLength, iCurrentCoreIndex))
		{
			strcat(pcOutput, "E30");
			return TRUE;
		}
		strcat(pcOutput, "OK");
	}
	else
	{
		strcat(pcOutput, "OK");
	}
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_SetBreakpoint
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    RS             Modified for ARM support
 ****************************************************************************/
static int32_t DSP_SetBreakpoint(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
	uint32_t ulLength;
	uint32_t ulBpType;
	int32_t iError;

	NOREF(iCurrentCoreIndex);
	NOREF(uiOutputSize);
	PrintMessage(DEBUG_MESSAGE, "DSP_SetBreakpoint");
	StringsToBpTypeAddressAndLength(&pcInput[1], &ulBpType, &ulAddress, &ulLength);
#if defined(ARC) || defined(ARM) || defined(RISCV)
#ifdef ARM
	if (usMonHwbpOption == TRUE)
		ulBpType = TRUE;
#endif
	iError = LOW_SetBreakpoint(ulBpType, ulAddress, ulLength, iCurrentCoreIndex);
	if(iError != GDBSERV_NO_ERROR)
	{
		//pcOutput[0]='O';
		//pcOutput[1]=0;
		//AddStringToOutAsciiz(tySI.szLastError, pcOutput+1);
		strcat(pcOutput, "E.fatal.");
		if(iError == MAX_TRIGGER_LIMIT)
			strcat(pcOutput, "Could not insert Hardware Breakpoints:\nYou may have requested too many Hardware breakpoints/Watchpoints\n");
		else 
		//TODO: Improve this error handling
		sprintf(pcOutput, "E.fatal.Could not insert Breakpoints:\nUnable to access memory at address 0x%X\n", ulAddress);
		return TRUE;
	}
	strcat(pcOutput, "OK");
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (GDBSERV_NO_ERROR != LOW_SetBreakpoint(ulBpType, ulAddress, ulLength, iCurrentCoreIndex))
		{
			strcat(pcOutput, "E30");
			return TRUE;
		}
		strcat(pcOutput, "OK");
	}
	else
	{
		strcat(pcOutput, "OK");
	}
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_ContinueReply
	 Engineer: Nikolay Chokoev
		Input: tyStopSignal  : Stop signal
 *pcOutput : output bufffer to return to GDB
	   Output: int32_t : TRUE if response should be sent
  Description: handle GDB command
Date           Initials    Description
04-Dec-2007    NCH         Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    RS             Modified for ARM support
 ****************************************************************************/
int32_t DSP_ContinueReply(TyStopSignal tyStopSignal, char *pcOutput)
{
#if defined(ARC) || defined(MIPS) || defined (ARM) || defined(RISCV)
	StopSignalToString(tyStopSignal, &pcOutput[0]);
	return 1;
#else
#error "This architecture is not supported."
#endif
}

/****************************************************************************
	 Function: DSP_Continue
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
03-Jul-2008    NCH         MIPS Added
31-Mar-2008    RS             Modified for ARM support
 ****************************************************************************/
static int32_t DSP_Continue(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
#if defined (RISCV)	
	uint32_t *pulAddress;
	pulAddress = &ulAddress;	
#endif
	NOREF(uiOutputSize);
	PrintMessage(DEBUG_MESSAGE, "DSP_Continue");
#if defined(ARC) || defined (ARM) || defined(RISCV)
	if (CheckNumberIsPresent(&pcInput[1], &ulAddress))
	{
#if defined (RISCV)		
		if (GDBSERV_NO_ERROR != LOW_WritePC(pulAddress))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	if (GDBSERV_NO_ERROR != LOW_Continue(&tyGlobalStopSignal, iCurrentCoreIndex))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#else
        if (GDBSERV_NO_ERROR != LOW_WritePC(ulAddress))	
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	if (!LOW_Continue(&tyGlobalStopSignal, iCurrentCoreIndex))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#endif
	// don't reply now... later...
	// StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
	// don't reply now!
#if defined(ARM)
	StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
	return 1;
#endif
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (CheckNumberIsPresent(&pcInput[1], &ulAddress))
		{
			if (GDBSERV_NO_ERROR != LOW_WritePC(ulAddress))
			{
				strcat(pcOutput, "E30");
				return 1;
			}
		}
		if (GDBSERV_NO_ERROR != LOW_Continue(&tyGlobalStopSignal, iCurrentCoreIndex))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
		StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
	}
	else
	{
		strcat(pcOutput, "S00");
		return 1;
	}

#else
#error "This architecture is not supported."
#endif
#ifndef ARM
	return 0;
#endif
}

/****************************************************************************
	 Function: DSP_Step
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
03-Jul-2008    NCH         MIPS Added
 ****************************************************************************/
static int32_t DSP_Step(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
#if defined (RISCV)
	uint32_t *pulAddress;
	pulAddress = &ulAddress;
#endif	
	NOREF(iCurrentCoreIndex);
	NOREF(uiOutputSize);
	PrintMessage(DEBUG_MESSAGE, "DSP_Step");
#if defined(ARC) || defined(RISCV)
	if (CheckNumberIsPresent(&pcInput[1], &ulAddress) == TRUE)
	{
#if defined (RISCV)
		if (GDBSERV_NO_ERROR != LOW_WritePC(pulAddress))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	if (GDBSERV_NO_ERROR != LOW_Step(&tyGlobalStopSignal))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#else
        if (GDBSERV_NO_ERROR != LOW_WritePC(ulAddress))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	if (!LOW_Step(&tyGlobalStopSignal))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
#endif
#if defined (RISCV)		
	//set current core as not-flushed data cache
	tySI.tyProcConfig.Processor._RISCV.bDCacheFlush[iCurrentCoreIndex] = FALSE;
	//set current core as not-invalidated data cache
	tySI.tyProcConfig.Processor._RISCV.bDCacheInval[iCurrentCoreIndex] = FALSE;
	//set current core as not-flushed instruction cache
	tySI.tyProcConfig.Processor._RISCV.bICacheFlush[iCurrentCoreIndex] = FALSE;
#else
    //set current core as not-flushed data cache
    tySI.tyProcConfig.Processor._ARC.bDCacheFlush[iCurrentCoreIndex]=FALSE;
    //set current core as not-invalidated data cache
    tySI.tyProcConfig.Processor._ARC.bDCacheInval[iCurrentCoreIndex]=FALSE;
    //set current core as not-flushed instruction cache
    tySI.tyProcConfig.Processor._ARC.bICacheFlush[iCurrentCoreIndex]=FALSE;
#endif	
	StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (CheckNumberIsPresent(&pcInput[1], &ulAddress) == TRUE)
		{
			if (GDBSERV_NO_ERROR != LOW_WritePC(ulAddress))
			{
				strcat(pcOutput, "E30");
				return 1;
			}
		}

		if (GDBSERV_NO_ERROR != LOW_Step(&tyGlobalStopSignal))
		{
			strcat(pcOutput, "E30");
			return 1;
		}

		StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
	}
	else
	{
		strcat(pcOutput, "S00");
	}
#elif ARM
	if (CheckNumberIsPresent(&pcInput[1], &ulAddress) == TRUE)
	{
		if (GDBSERV_NO_ERROR != LOW_WritePC(ulAddress))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
	}
	if (!LOW_Step(&tyGlobalStopSignal))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_LastSignal
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
03-Jul-2008    NCH         MIPS Added
 ****************************************************************************/
static int32_t DSP_LastSignal(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	NOREF(iCurrentCoreIndex);
	NOREF(uiOutputSize);
	NOREF(pcInput);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Last signal");
#if defined(ARC) || defined (ARM) || defined(RISCV)
	if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyGlobalStopSignal))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly && tySI.bIsConnected)
	{
		if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyGlobalStopSignal))
		{
			strcat(pcOutput, "E30");
			return 1;
		}
		StopSignalToString(tyGlobalStopSignal, &pcOutput[0]);
	}
	else
	{
		strcat(pcOutput, "T05thread:01;");
		return 1;
	}
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_Reset
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
03-Jul-2008    NCH         MIPS Added
 ****************************************************************************/
static int32_t DSP_Reset(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	NOREF(iCurrentCoreIndex);
	NOREF(uiOutputSize);
	NOREF(pcInput);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Reset");
#if defined(ARC) || defined(ARM) || defined(RISCV)
	if (GDBSERV_NO_ERROR != LOW_ResetTarget())
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	strcat(pcOutput, "OK");
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (GDBSERV_NO_ERROR != LOW_ResetTarget())
		{
			strcat(pcOutput, "E30");
			return TRUE;
		}
		strcat(pcOutput, "OK");
	}
	else
	{
		strcat(pcOutput, "OK");
	}
#else
#error This architecture is not supported.
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_ToggleDebug
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
03-Jul-2008    NCH         MIPS Added
 ****************************************************************************/
static int32_t DSP_ToggleDebug(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	NOREF(pcOutput);
	NOREF(iCurrentCoreIndex);
	NOREF(uiOutputSize);
	NOREF(pcInput);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Toggle debug");
#ifdef MIPS
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		ToggleDebugModeFlag();
	}
#else
	ToggleDebugModeFlag();
#endif
	return 0;
}

/****************************************************************************
	 Function: DSP_Detach
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
03-Jul-2008    NCH         MIPS Added
 ****************************************************************************/
static int32_t DSP_Detach(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	NOREF(pcOutput);
	NOREF(uiOutputSize);
	NOREF(pcInput);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Detach");
	// detach from target
#if defined(ARC) || defined(ARM) || defined(RISCV)
	if (GDBSERV_NO_ERROR == LOW_Detach(iCurrentCoreIndex))
		PrintMessage(LOG_MESSAGE, "Detach and resume execution ok");
	else
		PrintMessage(LOG_MESSAGE, "Detach but failed to resume execution");
	strcat(pcOutput, "OK");
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		if (GDBSERV_NO_ERROR == LOW_Detach(iCurrentCoreIndex))
			PrintMessage(LOG_MESSAGE, "Detach and resume execution ok");
		else
			PrintMessage(LOG_MESSAGE, "Detach but failed to resume execution");
		strcat(pcOutput, "OK");
	}
	else
	{
		strcat(pcOutput, "OK");
	}
#else
#error "This architecture is not supported."
#endif
	return 1;
}

/****************************************************************************
	 Function: DSP_PrepareFeaturesXML
	 Engineer: Suresh P. C.
		Input: *command  : command from GDB
	   Output: output bufffer to return to GDB
  Description: Prepare XML reply for target feature read command from GDB
Date           Initials    Description
26-Feb-2019    SPC         Initial
****************************************************************************/
static char *DSP_PrepareFeaturesXML(char *command)
{
	FILE *fpXegxml = NULL;
	char *xmlfiledata;
	char *xmldata = NULL;
	int32_t offset, len = 0;
	int32_t filenamelen = 10; //Length for target.xml
	char *separator;

	//The packet format GDB asking will be qXfer:features:read:target.xml:<start>,<size>
	// e.g. qXfer:features:read:target.xml:0,1000
	if (strncmp(command, "target.xml", filenamelen) == 0)
	{
		fpXegxml = fopen(tySI.tyProcConfig.szRegFileName, "rb");
		if (fpXegxml == NULL)
			return NULL;
	}
	else {
		char filename[200] = "regs/";
		char *filename_end;
		filename_end = strchr(command, ':');
		filenamelen = (int32_t)(filename_end - command);
		strncat(filename, command, filenamelen);
		filename[strlen(filename) + filenamelen] = '\0';
		fpXegxml = fopen(filename, "rb");
		if (fpXegxml == NULL)
			return NULL;

	}
	offset = strtoul(command + filenamelen + 1, &separator, 16);
	if (*separator != ',')
		return NULL;
	len = strtoul(separator + 1, NULL, 16);
	xmlfiledata = (char *)calloc(1, (uint64_t)len + 1);
	if (xmlfiledata == NULL)
	{
		PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
		return NULL;
	}
	xmldata = (char *)calloc(1, (uint64_t)len + 2);
	if (xmldata == NULL)
	{
		PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
		return NULL;
	}

	if (offset > 0)
		fseek(fpXegxml, offset, 0);
	
	size_t readsize = fread(xmlfiledata, 1, len, fpXegxml);
	if (readsize > 0 && (xmlfiledata != NULL) && (xmldata != NULL))
	{
		xmlfiledata[readsize] = '\0';
		if (feof(fpXegxml)) {
			strcpy(xmldata, "l");
			strcat(xmldata, xmlfiledata);
		}
		else {
			strcpy(xmldata, "m");
			strcat(xmldata, xmlfiledata);
		}
	}
	free(xmlfiledata);
	fclose(fpXegxml);
	
	return xmldata;
}

/****************************************************************************
	 Function: DSP_GeneralQuery
	 Engineer: Patrick Shiels
		Input: *pcInput  : input buffer from GDB
 *pcOutput : output bufffer to return to GDB
	   Output: int32_t : TRUE if response should be sent
  Description: handle GDB command
Date           Initials    Description
02-Feb-2006    PJS         Initial
31-Mar-2008    RS          Modified for ARM support
14-Nov-2012    SV          Modified to handle errors properly
 ****************************************************************************/
static int32_t DSP_GeneralQuery(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	int32_t   iSendResponse = TRUE;
	char  szBuffer[_MAX_PATH];
#if defined(MIPS)
	char  inputCommands[20][20] = { {0} };
	int32_t i = 0, j = 0;
#endif
#if defined(RISCV)
	static uint32_t  iLocalThreadCnt;
#endif
	NOREF(pcOutput);
	NOREF(iCurrentCoreIndex);
	NOREF(uiOutputSize);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "General query");
#if defined(ARC) || defined(ARM) || defined(RISCV)
	if (strcmp(pcInput + 1, "Offsets") == 0)
	{
		// we don't have target application information, and returning 0 causes
		// GDB to report: "gdb-internal-error: sect_index_data not initialised"
		// for some elf files. So, indicate we dont support with empty response.
		//strcat(pcOutput, "Text=0;Data=0;Bss=0");
	}
	else if (strcmp(pcInput + 1, "C") == 0)
#if defined(ARC) || defined(ARM)	
	{
        // we dont support threads, send empty response
        //strcat(pcOutput, "QC1");
	}
#else
	{
		// we support threads, send empty response
		strcat(pcOutput, "QC1");
	}
	else if (strncmp(pcInput + 1, "fThreadInfo", 11) == 0)
	{
		// we dont support threads, send empty response
		uint32_t i;
		uint32_t ulThredId;
		ulThredId = LOW_CurrentHart();
		iLocalThreadCnt = 0;
		for (i = 0; i < tySI.ulNumberOfHarts; i++)
		{
			tyThrdInfo[i].ulIsActiveThread = 0;
		}
		//ulCurrent_Hart = 0;
		sprintf(pcOutput, "m%lu", tyThrdInfo[ulThredId].ulThreadId);
		tyThrdInfo[ulThredId].ulIsActiveThread = 1;
	}
	else if (strncmp(pcInput + 1, "sThreadInfo", 11) == 0)
	{
		uint32_t ulThredId;
		ulThredId = LOW_CurrentHart();
		if (iLocalThreadCnt < tySI.ulNumberOfHarts)
		{
			if (tyThrdInfo[iLocalThreadCnt].ulIsActiveThread)
			{
				iLocalThreadCnt++;
				if (iLocalThreadCnt < tySI.ulNumberOfHarts)
				{
					sprintf(pcOutput, "m%lu", tyThrdInfo[iLocalThreadCnt].ulThreadId);
					/*Incremement the thread count*/
					iLocalThreadCnt++;
				}
				else
				{
					strcat(pcOutput, "l");
				}
			}
			else
			{
				sprintf(pcOutput, "m%lu", tyThrdInfo[iLocalThreadCnt].ulThreadId);
				iLocalThreadCnt++;
			}
		}
		else
		{
			strcat(pcOutput, "l");
		}
	}
	else if (strncmp(pcInput + 1, "ThreadExtraInfo", 15) == 0)
	{
		//This name will displayed when "info threads" command is posted
		//TODO: Properly specify the Core and Hart details
		strcpy(szBuffer, "");
		if (strncmp(pcInput + 17, "1", 1) == 0) {
			strcpy(szBuffer, "Core#0 Hart#0");
		}if (strncmp(pcInput + 17, "2", 1) == 0) {
			strcpy(szBuffer, "Core#0 Hart#1");
		}

		AddStringToOutAsciiz(szBuffer, pcOutput);
	}
#endif
	else if (strncmp(pcInput + 1, "Xfer:features:read:", 19) == 0)
	{
		//The packet format GDB asking will be qXfer:features:read:target.xml:<start>,<size
		// e.g. qXfer:features:read:target.xml:0,1000
		char *xmldata = DSP_PrepareFeaturesXML(pcInput + 20);
		if (xmldata != NULL)
		{
			strcat(pcOutput, xmldata);
			free(xmldata);
		}
	}
	else if (strncmp(pcInput + 1, "Rcmd,", 5) == 0)
	{
		// skip qRcmd,
		iSendResponse = ParseMonitorCommand(pcInput + 6, pcOutput);
#if defined(ARM) || defined(ARC) || defined(RISCV)
		if (iSendResponse == FALSE)
		{
			return TRUE;
		}
#endif
		//if the file is loaded with 'ashload' command
				//and need to change entry point ...
		if (!strncmp("ashload", tySI.pszMonitorCommand, 7))
		{
			if (tySI.tyProcConfig.bUseEntryPoint == TRUE)
			{
#ifdef ARM
				gulPC = tySI.tyProcConfig.ulProgEntryPoint;
#endif

#if defined(RISCV)
				if (GDBSERV_NO_ERROR != LOW_WritePC(&(tySI.tyProcConfig.ulProgEntryPoint)))
#else
                if (GDBSERV_NO_ERROR != LOW_WritePC(tySI.tyProcConfig.ulProgEntryPoint))
#endif				
				{
					PrintMessage(ERROR_MESSAGE,
						"Failed to set pc at: 0x%02X. \n",
						(uint32_t)tySI.tyProcConfig.ulProgEntryPoint);
				}
				sprintf(szBuffer, "Entry point set to 0x%08X\n",
					(uint32_t)tySI.tyProcConfig.ulProgEntryPoint);
				PrintMessage(INFO_MESSAGE, "%s", szBuffer);
				AddStringToOutAsciiz(szBuffer, pcOutput);
			}
		}
	}
#if defined(ARM)
    else if (strncmp(pcInput+1, "Supported",9) == 0)
    {
        // Return the supported features
        strcpy(pcOutput, "PacketSize=60000;QStartNoAckMode+");
    }
#endif		
#if defined(RISCV) || defined(ARC)
	else if (strncmp(pcInput + 1, "Supported",9) == 0)
	{
		// Return the supported features
		strcpy(pcOutput, "PacketSize=60000;QStartNoAckMode+");
		if (tySI.tyProcConfig.szRegFileName[0] != '\0')
		{
			strcat(pcOutput, ";qXfer:features:read+");
		}
	}
#endif
	return(iSendResponse);
#elif defined(MIPS)
	szBuffer[0] = '\0';
	NOREF(szBuffer[0]);
	NOREF(iSendResponse);
	while (*pcInput != '\0')
	{
		if (*pcInput == ':')
		{
			inputCommands[j][i] = '\0';
			j++;
			i = 0;
		}
		else
		{
			inputCommands[j][i++] = *pcInput;
		}
		pcInput++;
	}
	for (i = 0;i <= j;i++)
	{
		if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
		{
			if (strcmp(inputCommands[i], "qOffsets") == 0)
			{
				// we don't have target application information, and returning 0 causes
				// GDB to report: "gdb-internal-error: sect_index_data not initialised"
				// for some elf files. So, indicate we dont support with empty response.
				//strcat(pcOutput, "Text=0;Data=0;Bss=0");
			}
			else if (strcmp(inputCommands[i], "qC") == 0)
			{
				// we dont support threads, send empty response
				strcat(pcOutput, "QC1");
			}
			else if (strcmp(inputCommands[i], "qAttached") == 0)
			{
				// we dont support threads, send empty response
				//strcat(pcOutput, "1");
			}
			else if ('L' == inputCommands[i][1])
			{
				// we dont support threads, send empty response
				// strcat(pcOutput, "l");
			}
			else if (strcmp(inputCommands[i], "qfThreadInfo") == 0)
			{
				// we dont support threads, send empty response
				strcat(pcOutput, "m1");
			}
			else if (strcmp(inputCommands[i], "qsThreadInfo") == 0)
			{
				// we dont support threads, send empty response
				strcat(pcOutput, "l");
			}
			else if (strncmp(inputCommands[i], "qThreadExtraInfo", 15) == 0)
			{
				// we dont support threads, send empty response
				// NOTE: We can provide CPU # here to display it in Eclipse
				strcpy(szBuffer, "");//"CPU#0");
				AddStringToOutAsciiz(szBuffer, pcOutput);
			}
			else if (strcmp(inputCommands[i], "qSupported") == 0)
			{
				// Return the supported features
				strcpy(pcOutput, "PacketSize=4000;multiprocess-");
			}
			else if (strcmp(inputCommands[i], "qSymbol") == 0)
			{
				// Query any symbols needed from GDB
				// strcpy(pcOutput, "OK");
			}
			else if (strcmp(inputCommands[i], "qTStatus") == 0)
			{
				// we dont support threads, send empty response
			}
			else if (strncmp(inputCommands[i], "qRcmd,", 5) == 0)
			{
				// skip qRcmd,
				(void)ParseMonitorCommand(&(inputCommands[i][6]), pcOutput);
				return TRUE;
			}
		}
		else
		{
			return TRUE;
		}
	}
	return TRUE;
#else
	return TRUE;
#endif
}

/****************************************************************************
	 Function: DSP_SearchMemory
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
static int32_t DSP_SearchMemory(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// not supporting, just empty message
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Search memory");
	return 1;
}

/****************************************************************************
	 Function: DSP_GeneralSet
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command (target reset)
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
static int32_t DSP_GeneralSet(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// not supporting, just empty message
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "General set");

    //Sending empty reply because we do not set anything here now.
	return 1;
}

/****************************************************************************
	 Function: DSP_KillTarget
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command (target reset)
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS          Modified for ARM support
03-Jul-2008    NCH         MIPS added
23-Sep-2009    SVL         Modified for PFXD mode
19-Oct-2009    SVL         Support for forcefully setting breakpoint types
 ****************************************************************************/
static int32_t DSP_KillTarget(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// reset target and halt it after the reset (this command is also invoked when GDB reloads program)
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Kill target");
#if defined(ARC) || defined(ARM)
#ifdef ARM
	/* Clear all the force breakpoint modes set */
	(void)RemoveBpTypeList();
#endif
	(void)LOW_ResetTarget();
	/* NOTE: Logically, when a gdb session is closed, GDB Server should close the
	 existing connection. But for PF_Eclipse's proper woking we are commenting
	 this out. */
	 /* PFXD mode : Disconnecting... */
	 //tySI.bIsConnected = FALSE;
	
#if defined(ARC)
	(void)LOW_Halt(iCurrentCoreIndex);
#endif
#elif defined (MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly && !tySI.bPfxdMode)
	{
		(void)LOW_ResetTarget();
	}
#elif defined (RISCV)

#else
#error "This architecture is not supported."
#endif
	return 0;
}

/****************************************************************************
	 Function: DSP_Restart
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command (target reset)
Date           Initials    Description
08-Feb-2008    VH          Initial
31-Mar-2008    RS             Modified for ARM support
03-Jul-2008    NCH         MIPS added
 ****************************************************************************/
static int32_t DSP_Restart(char *pcInput,
	char *pcOutput,
	uint32_t uiOutputSize,
	int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// call function to reset target
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Restart");
#if defined(ARC) || defined(ARM) || defined(RISCV)
	(void)LOW_ResetTarget();
#elif defined(MIPS)
	if (!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
	{
		(void)LOW_ResetTarget();
	}
#else
#error "This architecture is not supported."
#endif
	return 0;
}

/****************************************************************************
	 Function: DSP_StepWithSignal
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
static int32_t DSP_StepWithSignal(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// not supporting, just print empty message
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Step with signal");
	return 1;   // empty response
}

/****************************************************************************
	 Function: DSP_WriteBinary
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
09-Nov-2012    SPT         Implemented the function
 ****************************************************************************/
static int32_t DSP_WriteBinary(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	uint32_t ulAddress;
	uint32_t ulCount;

	NOREF(uiOutputSize);
	NOREF(*pcOutput);
	NOREF(iCurrentCoreIndex);

	PrintMessage(DEBUG_MESSAGE, "DSP_WriteBinary");
	StringsToAddressCountAndBinaryData(&pcInput[1], &ulAddress, &ulCount, pucMemoryBuffer, sizeof(pucMemoryBuffer));

#if defined(ARC) 
	//Flush and invalidate memory
	if (LOW_FlushDCache(iCurrentCoreIndex) == TRUE)
	{
		PrintMessage(DEBUG_MESSAGE, "Data Cache Flush Failed!");
	}
	if (LOW_InvalidateDCache(iCurrentCoreIndex) == TRUE)
	{
		PrintMessage(DEBUG_MESSAGE, "Data Cache Invalidate Failed!");
	}
	if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	strcat(pcOutput, "OK");
	return 1;
#elif defined(RISCV)
	if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulCount, pucMemoryBuffer))
	{
		strcat(pcOutput, "E30");
		return 1;
	}
	strcat(pcOutput, "OK");
	return 1;
#else
	return 1;
#endif

}

/****************************************************************************
	 Function: DSP_ContinueWithSignal
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
static int32_t DSP_ContinueWithSignal(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// not supporting, just print empty message
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Continue with signal");
	return 1;   // empty response
}

/****************************************************************************
	 Function: DSP_SetThread
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
static int32_t DSP_SetThread(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// not supporting threads, just print debug message
	strcat(pcOutput, "OK");
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Set thread");
#if defined(RISCV)		
	uint32_t i;
	uint32_t ulCurrent_Hart;
	if (pcInput[2] == '0')
	{
		for (i = 0; i < tySI.ulNumberOfHarts; i++)
		{
			tyThrdInfo[i].ulIsActiveThread = 0;
			tyThrdInfo[i].ulCurrentThread = 0;
		}
		tyThrdInfo[0].ulIsActiveThread = 1;
		tyThrdInfo[0].ulCurrentThread = 1;
	}
	if (pcInput[2] != '-' && pcInput[2] != '0')
	{
		uint32_t hart = pcInput[2] - '0';
		ulCurrent_Hart = hart-1;
		
			for (i = 0; i < tySI.ulNumberOfHarts; i++)
			{
				tyThrdInfo[i].ulIsActiveThread = 0;
				tyThrdInfo[i].ulCurrentThread = 0;
			}
			tyThrdInfo[ulCurrent_Hart].ulIsActiveThread = 1;
			tyThrdInfo[ulCurrent_Hart].ulCurrentThread = 1;
	}
#endif	

	return 1;   // empty response
}


/****************************************************************************
	 Function: DSP_CheckThreadAlive
	 Engineer: Suresh P.C
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: handle GDB command
Date           Initials    Description
08-Nov-2011    SPC         Initial
 ****************************************************************************/
static int32_t DSP_CheckThreadAlive(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	NOREF(iCurrentCoreIndex);
	// Currently not fully supporting threads, just print debug message
	strcat(pcOutput, "OK");
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Check Thread Alive or not");
	return 1;   // empty response
}
/****************************************************************************
	 Function: DSP_NotSupported
	 Engineer: Vitezslav Hola
		Input: char *pcInput - input buffer from GDB
			   char *pcOutput - output bufffer to return to GDB
			   uint32_t uiOutputSize - size of output buffer
			   int32_t iCurrentCoreIndex - current core index
	   Output: int32_t - >0 if response should be sent
  Description: Handle case when command is not supported (invalid string)
Date           Initials    Description
08-Feb-2008    VH          Initial
 ****************************************************************************/
static int32_t DSP_NotSupported(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex)
{
	assert(pcInput != NULL);
	assert(pcOutput != NULL);
	assert(uiOutputSize > 0);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "DSP_NotSupported");
	NOREF(iCurrentCoreIndex);
	return 1;   // empty response
}
