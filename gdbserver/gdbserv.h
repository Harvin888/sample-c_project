/****************************************************************************
       Module: gdbserv.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ethernet socket communication header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBSERV_H_
#define GDBSERV_H_

int32_t ServerMain(unsigned short usPort);

#endif // GDBSERV_H_

