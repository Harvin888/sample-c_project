/****************************************************************************
	   Module: gdbRISCV.c
	 Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, RISCV target interface
Date           Initials    Description
06-NOV-2007    NCH         initial
09-Sep-2009    RSB         TAP Reset Done
08-Oct-2012    SV          Get Auxiliary Register numbers from XML,if provided
****************************************************************************/
#if _WINCONS
#include <windows.h>
#include <conio.h>
#include <windef.h>
#elif _LINUX
#include <dlfcn.h>
#define stricmp strcasecmp
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

// RISCV interface header (exclude lint from these files)
//lint -save -e*
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "riscvint.h"
//lint -restore
// must be included before other headers
#include "../gdbglob.h"
#include "../gdbxml.h"
#include "../gdbty.h"
#include "../gdbutil.h"
#include "../gdblow.h"
#include "../gdbcfg.h"
#include "../gdberr.h"
#include "gdbriscv.h"
#include "riscvsetup.h"
//#include "RISCVreg.h"
#include <sys\timeb.h> 

#if _WINCONS
#define  OPELLAXD_DLL_NAME                "opxdriscv.dll"
#define  OPELLAXD_DLL_NAME_1              "C:\\AshlingOpellaXDforRISCV\\opxdriscv.dll"
#elif _LINUX
#define  OPELLAXD_DLL_NAME                "./opxdRISCV.so"
#define  OPELLAXD_DLL_NAME_1              "./opxdRISCV.so"
#define  LoadLibrary(lib)                 dlopen(lib, RTLD_LAZY)
#define  GetProcAddress(handle, proc)     dlsym(handle, proc)
#define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#endif

#define BAD_HEX_VALUE               0xBAD0BAD0
#define MAX_RISCV_READ_MEM_AMMOUNT    8192
//#define RISCV_BRK_S_INSTR             0x9002    // 16-bit breakpoint instruction
#define RISCV_BRK_S_INSTR_B1          0x90        // 2nd byte from 0x7FFF
#define RISCV_BRK_S_INSTR_B0          0x02        // 1st byte from 0x7FFF
//#define RISCV_BRK_INSTR               0x256F003F // 32-bit breakpoint instruction
#define RISCV_BRK_INSTR_B3            0x00       // 4th byte from 0x256F003F
#define RISCV_BRK_INSTR_B2            0x10        // 3rd byte from 0x256F003F
#define RISCV_BRK_INSTR_B1            0x00        // 2nd byte from 0x256F003F
#define RISCV_BRK_INSTR_B0            0x73        // 1st byte from 0x256F003F

#define RISCV_REG_DEBUG            (0x45)      // rw  32  Basecase  Debug Register
//#define RISCV_REG_DEBUG_LD_BIT   (1 << 31)   // Load pending bit
#define RISCV_REG_DEBUG_SH_BIT     (1 << 30)   // Self halt bit (flag 1)
#define RISCV_REG_DEBUG_BH_BIT     (1 << 29)   // Break halt bit
#define RISCV_REG_DEBUG_RA_BIT     (1 << 22)   // Reset asserted bit
											 // 
//#define RISCV_REG_DEBUG_UB_BIT   (1 << 28)   // User mode bit
#define RISCV_REG_DEBUG_ZZ_BIT     (1 << 23)   // Sleeping bit
#define RISCV_REG_DEBUG_IS_BIT     (1 << 11)   // Instruction step bit
#define RISCV_REG_DEBUG_FH_BIT     (1 << 1)    // Force Halt bi

#define RISCV_REG_STATUS           (0x40)      // r  32 RISCVompact Status Register
#define RISCV_REG_STATUS_H_BIT     (1 << 25)   // Halt 

#define RISCV_REG_STATUS2           (0x4A)      // r  32 RISCVompact Status Register
#define RISCV_REG_STATUS2_H_BIT     (1 << 0)   // Halt 
// 

#define RISCV_ENDINESS				(0x09)      

#define RISCV_DC_CTRL              (0x88)
#define RISCV_DC_CTRL_DC_BIT       (1 << 0)
//#define RISCV_DC_CTRL_LM_BIT       (1 << 7)
#define RISCV_DC_CTRL_FS_BIT       (1 << 8)

#define RISCV_DC_FLSH              (0x8B)
#define RISCV_DC_FLSH_FL_BIT       (1 << 0)

#define RISCV_DC_IVDC              (0x87)
#define RISCV_DC_IVDC_IV_BIT       (1 << 0)

#define RISCV_ISA_CONFIG           (0xC1)

#define RISCV_CACHE_FLUSH_RETRY    2
//#define RISCV_CACHE_INVAL_RETRY    5

#define IDERR_LOAD_DLL_FAILED       1
#define IDERR_DLL_NOT_LOADED        2
#define IDERR_GDI_ERROR             3
#define IDERR_ASH_SRCREGISTER       15
#define IDERR_ASH_DSTREGISTER       16
#define IDERR_ADD_BP_NO_MEMORY      17
#define IDERR_ADD_BP_INTERNAL1      18
#define IDERR_ADD_BP_DUPLICATE      19
#define IDERR_REMOVE_BP_INTERNAL1   20
#define IDERR_REMOVE_BP_INTERNAL2   21
#define IDERR_REMOVE_BP_NOT_FOUND   22
#define IDERR_DLL_GET_INTERFACE_ERR 24
#define IDERR_DLL_NULL_FUNCTION     25
#define IDERR_NOT_HALT				26
#define IDERR_TARGET_PWDOFF			27
#define IDERR_DISCOVER_HARTS		28
#define IDERR_DISCOVERY				29
#define IDERR_RISCV_MECH_SETUP_FAIL 30

#define GDB_PWDOFF					4
#define GDB_PWDOWN					2
#define GDB_TRUE					1
#define GDB_FALSE					0

#define GDB_RISCV_ABSTRACT_REG_WRITE_BUSY						0x5 //abstarct reg cmd error busy
#define GDB_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED				0x6 //abstarct reg cmd error not supported
#define GDB_RISCV_ABSTRACT_REG_WRITE_EXCEPTION					0x7 //abstarct reg cmd error exception
#define GDB_RISCV_ABSTRACT_REG_WRITE_HART_STATE					0x8 //abstarct reg cmd error hart state
#define GDB_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR					0x9 //abstarct reg cmd bus error
#define GDB_RISCV_ABSTRACT_REG_WRITE_OTHER_ERROR				0xA //abstarct reg cmd other errors
#define GDB_RISCV_SYSTEM_BUS_TIMEOUT_FAILED						0xB //system bus sberror timeout
#define GDB_RISCV_SYSTEM_BUS_ADDRESS_FAILED						0xC //system bus sberror incorrect address
#define GDB_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED					0xD //system bus sberror alignment error
#define GDB_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED			0xE //system bus sberror unsupported size access
#define GDB_RISCV_SYSTEM_BUS_OTHER_REASON						0xF //system bus sberror due to other reasons
#define GDB_RISCV_SYSTEM_BUS_MASTER_BUSY						0x10 //system bus master busy error



#define RISCV_DATA_DISCOVERY_SIZE   13
//Register array definitions
//#define REGISTER_ARRAY_ALLOC_CHUNK 64

/***********Default Registers*************/
#define REG_R0                     "R0"
#define REG_R1                     "R1"
#define REG_R2                     "R2"
#define REG_R3                     "R3"
#define REG_R4                     "R4"
#define REG_R5                     "R5"
#define REG_R6                     "R6"
#define REG_R7                     "R7"
#define REG_R8                     "R8"
#define REG_R9                     "R9"
#define REG_R10                    "R10"
#define REG_R11                    "R11"
#define REG_R12                    "R12"
#define REG_R13                    "R13"
#define REG_R14                    "R14"
#define REG_R15                    "R15"
#define REG_R16                    "R16"
#define REG_R17                    "R17"
#define REG_R18                    "R18"
#define REG_R19                    "R19"
#define REG_R20                    "R20"
#define REG_R21                    "R21"
#define REG_R22                    "R22"
#define REG_R23                    "R23"
#define REG_R24                    "R24"
#define REG_R25                    "R25"
//#define REG_R26                    "R26"
//#define REG_R27                    "R27"
//#define REG_R28                    "R28"
//#define REG_R29                    "R29"
//#define REG_R30                    "R30"
//#define REG_R31                    "R31"
#define REG_R32                    "R32"
#define REG_R33                    "R33"
#define REG_R34                    "R34"
#define REG_R35                    "R35"
#define REG_R36                    "R36"
#define REG_R37                    "R37"
#define REG_R38                    "R38"
#define REG_R39                    "R39"
#define REG_R40                    "R40"
#define REG_R41                    "R41"
#define REG_R42                    "R42"
#define REG_R43                    "R43"
#define REG_R44                    "R44"
#define REG_R45                    "R45"
#define REG_R46                    "R46"
#define REG_R47                    "R47"
#define REG_R48                    "R48"
#define REG_R49                    "R49"
#define REG_R50                    "R50"
#define REG_R51                    "R51"
#define REG_R52                    "R52"
#define REG_R53                    "R53"
#define REG_R54                    "R54"
#define REG_R55                    "R55"
#define REG_R56                    "R56"
#define REG_R57                    "R57"
#define REG_R58                    "R58"
#define REG_R59                    "R59"
//#define REG_R60                    "R60"
#define REG_R61                    "R61"
//#define REG_R62                    "R62"
//#define REG_R63                    "R63"
#define REG_GP                     "GP"
#define REG_FP                     "FP"
#define REG_SP                     "SP"
#define REG_ILINK1                 "ILINK1"
#define REG_ILINK2                 "ILINK2"
#define REG_BLINK                  "BLINK"
#define REG_LPCOUNT                "LPCOUNT"
#define REG_LIMM                   "LIMM"
#define REG_PCL                    "PCL"
#define REG_UNKNOWN                "UNKNOWN"
/********************************************/

/*************Auxiliary Registers************/
#define REG_STATUS                 "STATUS"
#define REG_SEMAPHORE              "SEMAPHORE"
#define REG_LP_START               "LP_START"
#define REG_LP_END                 "LP_END"
#define REG_IDENTITY               "IDENTITY"
#define REG_DEBUG                  "DEBUG"
#define REG_PC                     "PC"
#define REG_STATUS32               "STATUS32"
#define REG_STATUS32_L1            "STATUS32_L1"
#define REG_STATUS32_L2            "STATUS32_L2"
#define REG_COUNT0                 "COUNT0"
#define REG_CONTROL0               "CONTROL0"
#define REG_LIMIT0                 "LIMIT0"
#define REG_INT_VECTOR_BASE        "INT_VECTOR_BASE"
#define REG_MACMODE                "MACMODE"
#define REG_IRQ_LV12               "IRQ_LV12"
//#define REG_COUNT1                 "COUNT1"
//#define REG_CONTROL1               "CONTROL1"
//#define REG_LIMIT1                 "LIMIT1"
//#define REG_IRQ_LEV                "IRQ_LEV"
//#define REG_IRQ_HINT               "IRQ_HINT"
//#define REG_ERET                   "ERET"
//#define REG_ERBTA                  "ERBTA"
//#define REG_ERSTATUS               "ERSTATUS"
//#define REG_ECR                    "ECR"
//#define REG_EFA                    "EFA"
//#define REG_ICAUSE1                "ICAUSE1"
//#define REG_ICAUSE2                "ICAUSE2"
//#define REG_IENABLE                "IENABLE"
//#define REG_ITRIGGER               "ITRIGGER"
//#define REG_XPU                    "XPU"
//#define REG_BTA                    "BTA"
//#define REG_BTA_L1                 "BTA_L1"
//#define REG_BTA_L2                 "BTA_L2"
//#define REG_PULSE_CANCEL           "IRQ_PULSE_CANCEL"
//#define REG_IRQ_PENDING            "IRQ_PENDING"
/**********************************************/
//Needs dynamic allocation of registers
TyRISCVReg          *g_ptyRISCVReg;
uint32_t      g_ulNumberOfRegisters;
uint32_t		g_ulArraySize;
uint32_t		ulTriggerMap[20];
unsigned char  ucStopReason;

uint32_t static  ulTriggerCount = 0;
//#define  MAX_RISCV_REG  ((sizeof(ptyRISCVReg))/(sizeof(TyRISCVReg)))
struct TyFunctionTable
{
	int32_t(*DummyFunction)(struct TyRISCVInstance*);
	int32_t(*version)(struct TyRISCVInstance*);
	const char *   (*id)(struct TyRISCVInstance*);
	void(*destroy)(struct TyRISCVInstance*);
	const char *   (*additional_possibilities)(struct TyRISCVInstance*);
	void*          (*additional_information)(struct TyRISCVInstance*, unsigned);
	int32_t(*prepare_for_new_program)(struct TyRISCVInstance*, int32_t);
	int32_t(*process_property)(struct TyRISCVInstance*, const char *, const char *);
	int32_t(*is_simulator)(struct TyRISCVInstance*);
	int32_t(*step)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t, uint32_t);
	int32_t(*Run)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t);
	int32_t(*Halt)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t);
	int32_t(*StatusProc)(struct TyRISCVInstance*, unsigned short, unsigned short, uint32_t, unsigned char *, unsigned char *, uint32_t, uint32_t);
	int32_t(*Discovery)(struct TyRISCVInstance*, unsigned short, uint32_t *, uint32_t);
	int32_t(*Discover_Harts)(struct TyRISCVInstance*, unsigned short, uint32_t *);
	int32_t(*riscv_mechanism)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t,uint32_t);
	int32_t(*TriggerInfo)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t *, uint32_t, uint32_t);
	int32_t(*read_memory)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t, unsigned char *, uint32_t, uint32_t);
	int32_t(*write_memory)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t, unsigned char *, uint32_t, uint32_t);
	int32_t(*read_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t, uint32_t, uint32_t);
	int32_t(*write_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t, uint32_t, uint32_t);
	int32_t(*write_a_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t, uint32_t, uint32_t);
	int32_t(*read_a_reg)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t *, uint32_t, uint32_t, uint32_t);
	int32_t(*writeword)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t *, uint32_t, uint32_t);
	int32_t(*readword)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t *, uint32_t, uint32_t);
	int32_t(*writedpc)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t*, uint32_t, uint32_t);
	int32_t(*writecsr)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t*, uint32_t, uint32_t, uint32_t);
	int32_t(*addtrigger)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t *,uint32_t, uint32_t, uint32_t,uint32_t,uint32_t,uint32_t);
	int32_t(*removetrigger)(struct TyRISCVInstance *, unsigned short, uint32_t, uint32_t, uint32_t, uint32_t,uint32_t);
	unsigned(*memory_size)(struct TyRISCVInstance*);
	int32_t(*set_memory_size)(struct TyRISCVInstance*, unsigned);
	int32_t(*set_reg_watchpoint)(struct TyRISCVInstance*, int32_t, int32_t);
	int32_t(*remove_reg_watchpoint)(struct TyRISCVInstance*, int32_t, int32_t);
	int32_t(*set_mem_watchpoint)(struct TyRISCVInstance*, uint32_t, int32_t);
	int32_t(*remove_mem_watchpoint)(struct TyRISCVInstance*, uint32_t, int32_t);
	int32_t(*stopped_at_watchpoint)(struct TyRISCVInstance*);
	int32_t(*stopped_at_exception)(struct TyRISCVInstance*);
	int32_t(*set_breakpoint)(struct TyRISCVInstance*, unsigned, void*);
	int32_t(*remove_breakpoint)(struct TyRISCVInstance*, unsigned, void*);
	int32_t(*retrieve_breakpoint_code)(struct TyRISCVInstance*, unsigned, char *, unsigned, void *);
	int32_t(*breakpoint_cookie_len)(struct TyRISCVInstance*);
	int32_t(*at_breakpoint)(struct TyRISCVInstance*);
	int32_t(*define_displays)(struct TyRISCVInstance*, struct Register_display *);
	int32_t(*fill_memory)(struct TyRISCVInstance*, uint32_t, void *, uint32_t, uint32_t, int32_t);
	int32_t(*instruction_trace_count)(struct TyRISCVInstance*);
	void(*get_instruction_traces)(struct TyRISCVInstance*, uint32_t *);
	void(*receive_callback)(struct TyRISCVInstance*, RISCV_callback*);
	int32_t(*supports_feature)(struct TyRISCVInstance*);
	uint32_t(*data_exchange)(struct TyRISCVInstance*, uint32_t, uint32_t, uint32_t, void *, uint32_t, void *);
	int32_t(*in_same_process_as_debugger)(struct TyRISCVInstance*);
	uint32_t(*max_data_exchange_transfer)(struct TyRISCVInstance*);
	int32_t(*read_banked_reg)(struct TyRISCVInstance*, int32_t, int32_t, uint32_t *);
	int32_t(*write_banked_reg)(struct TyRISCVInstance*, int32_t, int32_t, uint32_t *);
	int32_t(*configure_vega)(struct TyRISCVInstance*, unsigned short, uint32_t, uint32_t uiCore, uint32_t uiIRLen);
	int32_t(*target_reset)(struct TyRISCVInstance*, uint32_t ulHartId);
	//int32_t(*set_mem_watchpoint2)(struct TyRISCVInstance*, RISCV_ADDR_TYPE, int32_t, unsigned, void **);
};

struct	TyRISCVInstance
{
	struct TyFunctionTable *ptyFunctionTable;
};

// external variables
extern TyServerInfo tySI;

typedef struct TyRISCVInstance *(*Tyget_RISCV_interface_ex)(PFRISCVSetupFunction pfExternalRISCVSetup);
typedef int32_t(*TyAshTestScanchain)(struct TyRISCVInstance *);
typedef int32_t(*TyAshDetectScanchain)(struct TyRISCVInstance *p,
	uint32_t uiMaxCores,
	uint32_t *puiNumberOfCores,
	uint32_t *pulIRWidth,
	unsigned char *pbRISCVCores);
typedef void(*TyAshGetSetupItem)(struct TyRISCVInstance *ptyPtr,
	const char *pszSection,
	const char *pszItem,
	char *pszValue);
typedef void(*TyAshSetSetupItem)(struct TyRISCVInstance *ptyPtr,
	const char *pszSection,
	const char *pszItem,
	char *pszValue);
typedef void(*TyAshGetLastErrorMessage)(struct TyRISCVInstance *p,
	char *pszMessage,
	uint32_t uiSize);
typedef void(*TyASH_ListConnectedProbes)(char ppszInstanceNumber[][16],
	unsigned short usMaxInstances,
	unsigned short *pusConnectedInstances);
typedef int32_t(*TyAshScanIR)(struct TyRISCVInstance *p,
	uint32_t ulTargetIRLength,
	uint32_t *pulDataIn,
	uint32_t *pulDataOut);
typedef int32_t(*TyAshScanDR)(struct TyRISCVInstance *p,
	uint32_t ulTargetIRLength,
	uint32_t *pulDataIn,
	uint32_t *pulDataOut);
typedef int32_t(*TyAshTAPReset)(struct TyRISCVInstance *p);

typedef char* (*TyAshGetInterfaceId) (void);

typedef void(*TyASH_UpdateScanchain)(struct TyRISCVInstance *p);

// breakpoint array definitions
#define  GDBSERV_BP_ARRAY_ALLOC_CHUNK    64

typedef struct
{
	uint32_t ulBpType;
	uint32_t ulAddress;
	uint32_t ulLength;
	unsigned char ucInstruction[4];
	uint32_t ulResIdx;
	uint32_t ulMatchValue;//Match value(0=exactly same match ,1=masking match so no..) for each Watchpoint.
	uint32_t ulResUsed;//Number of trigger resources used by each H/W breakpoints and Watchpoint.
} TyBpElement;

typedef struct
{
	TyBpElement *ptyBpArray; //TODO init elements in function
} TyCoreBPArray_t;

// local function prototypes
//static int32_t SetupDefaultRegs(char *ppszRegName[],uint32_t *pulRegNumber,int32_t iRegCount);
static int32_t GetDefaultRegSettings(uint32_t ulNumberOfRegisters, char *ppszRegName[], uint32_t *pulRegNumber);
static int32_t SetupDefaultRegsNameAndNumber(char *ppszRegName[], uint32_t *pulRegNumber, uint32_t *pulGdbRegIndexint, TyAccessType *pulAccessType, int32_t *iRegCount);
static int32_t SetUpRISCVRegs();
static int32_t GetRegSettingsFromXML(const char *pszRegFileName, char *ppszRegName[], uint32_t *pulRegNumber, uint32_t *pulGdbRegNumber, TyAccessType *pulAccessType, int32_t *iNumRegs);
static void SetupErrorMessage(int32_t iError);
static void IsBigEndian(void);
static int32_t  RISCVLoadLibrary(void);
static int32_t  RISCVConnect(void);
static int32_t  RISCVFreeLibrary(void);
static char *GetRegName(uint32_t ulGdbRegNumber);
static char *GetBpTypeName(uint32_t ulBpType);
static int32_t  AddBpArrayEntry(uint32_t ulBpType, uint32_t ResIdx, uint32_t ulMatch, uint32_t ulResrequired, uint32_t ulAddress, uint32_t ulLength, unsigned char *pucInstruction, int32_t iCurrentTapIndex);
static int32_t  RemoveBpArrayEntry(uint32_t uiIndex, int32_t iCurrentTapIndex);
static void RemoveAllBpArray(void);
static int32_t  GetBpArrayEntry(uint32_t ulBpType, uint32_t ulAddress, uint32_t ulLength, unsigned char *pucInstruction, int32_t iCurrentTapIndex);
static int32_t  GetBpArrayEntryForAddr(uint32_t ulAddress, uint32_t ulBPType, int32_t iCurrentTapIndex);
static int32_t  ReadFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t *pulRegValue);
static int32_t  WriteFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t ulRegValue);
static int32_t  if_version(struct RISCV_callback *);
static int32_t  if_printf(struct RISCV_callback *, const char *format, ...);
static void if_meminfo(struct RISCV_callback *, unsigned addr, struct Memory_info *m);
static int32_t  if_vprintf(struct RISCV_callback *, const char *format, va_list ap);
static int32_t  if_get_verbose(struct RISCV_callback *);
static void if_sleep(struct RISCV_callback *, unsigned milliseconds);
static int32_t  if_in_same_process_as_debugger(struct RISCV_callback *);
static struct RISCV_endian* if_get_endian(struct RISCV_callback *);
static void * if_get_tcp_factory(struct RISCV_callback *);
void * if_get_process(struct RISCV_callback *);
struct Debugger_access * if_get_debugger_access(struct RISCV_callback *);
void(*my_printf_func)(const char *format);
int32_t getMemoryError(int32_t imemError);

// local variables
static HINSTANCE           hInstRISCVDll = NULL;
static char                szTargetDllName[_MAX_PATH] = { 0 };
Tyget_RISCV_interface_ex     pfget_RISCV_interface_ex;
TyAshTestScanchain         pfAshTestScanchain;
TyAshDetectScanchain       pfAshDetectScanchain;
TyAshGetSetupItem          pfAshGetSetupItem;
TyAshSetSetupItem          pfAshSetSetupItem;
TyAshGetLastErrorMessage   pfAshGetLastErrorMessage;
TyASH_ListConnectedProbes  pfASH_ListConnectedProbes;
TyAshScanIR                pfAshScanIR;
TyAshScanDR                pfAshScanDR;
TyAshTAPReset              pfAshTAPReset;
TyAshGetInterfaceId        pfAshGetInterfaceID;
TyASH_UpdateScanchain      pfAshUpdateScanchain;
struct TyRISCVInstance       *ptyRISCVInstance;
unsigned short             usConnectedInstances;
static int32_t                 iRegDefEndianMode = -1;
static TyCoreBPArray_t     tyCoreBPArray[MAX_TAPS_IN_CHAIN + 1];
static uint32_t        uiBpCount[MAX_TAPS_IN_CHAIN + 1] = { 0 };     //todo
static uint32_t        uiBpAlloc[MAX_TAPS_IN_CHAIN + 1] = { 0 };

 RISCV_callback_functab pf = {           //lint !e123
	0,
	if_version,
	if_printf,
	if_meminfo,
	// end of supported interface 1
	if_vprintf,
	if_get_verbose,
	// end of interface 2
	if_sleep,
	if_in_same_process_as_debugger,
	if_get_endian,
	if_get_tcp_factory,
	if_get_process,
	// end of interface 3
	if_get_debugger_access,
	/* not implemented (SPT)
	if_get_system,
	if_target_state_invalidated,
	if_debugger_installation_directory,
	 // end of interface Version 4
	if_target_state_changed,
	 // end of interface Version 5
	if_eprintf,
	if_evprintf,
	if_lprintf,
	if_lvprintf,
	 // end of interface Version 6
	if_meminfo2,
	 //end of interface version 7
	 */
};

struct RISCV_callback
{
	  RISCV_callback_functab *pft;//lint !e123
};

/****************************************************************************
	 Function: SetupDefaultRegsNameAndNumber
	 Engineer: Sreeshma T.
		Input: ppszRegName :  Array for storing Register names
			   ulRegNumber :  Array for storing Register numbers
			   iRegCount   :  Returns Number of Registers
	   Output: int32_t - Error Code
  Description: Set up Registers in their default order
Date           Initials    Description
04-Mar-2016     ST          Initial
****************************************************************************/
static int32_t SetupDefaultRegsNameAndNumber(char *ppszRegName[], uint32_t *pulRegNumber, uint32_t *pulGdbRegIndexint, TyAccessType *pulAccessType, int32_t *iRegCount)
{
	TyXMLNode *ptyNode;
	TyXMLFile tyXMLSCFile;
	//int32_t i;
	// int32_t iRegCount = 0, iCurrReg = 0, iErrRet = 0;
	int32_t iCurrReg = 0, iErrRet = 0;
	char *pRegNum;

	if (tySI.bIsCoreRegFilePresent)
	{
		iErrRet = XML_ReadFile(tySI.szCoreRegFile, &tyXMLSCFile);
		//printf("tySI.szCoreRegFile = %s\n", tySI.szCoreRegFile);
	}
	else
	{
		iErrRet = XML_ReadFile("RISCVcorereg.xml", &tyXMLSCFile);
		//printf("RISCVcorereg.xml\n");
	}

	if (iErrRet != 0)
		return iErrRet;
	ptyNode = XML_GetChildNode((XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "target")), "feature");

	if (ptyNode == NULL)
		return GDBSERV_XML_ERROR;

	*iRegCount = XML_GetChildNodeCount(ptyNode, (char *)"register");

	// printf("iRegCount = %d\n", *iRegCount);

	//Ensuring XML contains valid register information
	if (*iRegCount == 0)
		return GDBSERV_XML_ERROR;

	// *iNumRegs = iRegCount;

	if ((ptyNode = XML_GetChildNode(ptyNode, "register")) == NULL)
		return GDBSERV_XML_ERROR;

	iCurrReg = 0;

	while (ptyNode != NULL)
	{
		ptyNode = XML_GetNode(ptyNode, "register");
		if ((ppszRegName[iCurrReg] = XML_GetAttrib(ptyNode, "name")) == NULL)
		{
			// printf("error1\n");
			//   printf("ppszRegName[%d] = %d",iCurrReg, ppszRegName[iCurrReg]);
			break;
		}

		if ((pRegNum = XML_GetAttrib(ptyNode, "number")) == NULL)
		{
			//printf("error2\n");
			break;
		}

		pulRegNumber[iCurrReg] = strtoul(pRegNum, NULL, 16);
		pulGdbRegIndexint[iCurrReg] = pulRegNumber[iCurrReg];
		pulAccessType[iCurrReg] = RW;
		// printf("ppszRegName[%d] = %s pulRegNumber[%d] = %d pulGdbRegIndexint[%d] = %d\n",iCurrReg, ppszRegName[iCurrReg], iCurrReg, pulRegNumber[iCurrReg], iCurrReg, pulGdbRegIndexint[iCurrReg]);
		++iCurrReg;
		ptyNode = ptyNode->pNext;
	}

	if (ptyNode != NULL)
		iErrRet = GDBSERV_XML_ERROR;

	return iErrRet;

}

/****************************************************************************
	 Function: GetDefaultRegSettings
	 Engineer: Sreekanth.V
		Input: ppszRegName :  Array for storing Register names
			   ulRegNumber :  Array for storing Register numbers
	   Output: int32_t - Error Code
  Description: Set up Registers in their default order, this is for GDB 6.3 and below
Date           Initials    Description
08-Oct-2012      SV         Initial
****************************************************************************/
static int32_t GetDefaultRegSettings(uint32_t ulNumberOfRegisters, char *ppszRegName[], uint32_t *pulRegNumber)
{
	uint32_t ulIndex;

	for (ulIndex = 0; ulIndex < ulNumberOfRegisters; ulIndex++)
	{
		ppszRegName[ulIndex] = (char *)malloc(sizeof(char));
		if (ppszRegName[ulIndex] == NULL)
			return GDBSERV_ERROR;
	}
	memset(*ppszRegName, '\0', (uint32_t)ulNumberOfRegisters);
	memset(pulRegNumber, 0, (uint32_t)ulNumberOfRegisters);

	// Default Register Ordering

	ppszRegName[0] = (char *)REG_R0;
	pulRegNumber[0] = (int32_t)0x00;

	ppszRegName[1] = (char *)REG_R1;
	pulRegNumber[1] = (int32_t)0x01;

	ppszRegName[2] = (char *)REG_R2;
	pulRegNumber[2] = (int32_t)0x02;

	ppszRegName[3] = (char *)REG_R3;
	pulRegNumber[3] = (int32_t)0x03;

	ppszRegName[4] = (char *)REG_R4;
	pulRegNumber[4] = (int32_t)0x04;

	ppszRegName[5] = (char *)REG_R5;
	pulRegNumber[5] = (int32_t)0x05;

	ppszRegName[6] = (char *)REG_R6;
	pulRegNumber[6] = (int32_t)0x06;

	ppszRegName[7] = (char *)REG_R7;
	pulRegNumber[7] = (int32_t)0x07;

	ppszRegName[8] = (char *)REG_R8;
	pulRegNumber[8] = (int32_t)0x08;

	ppszRegName[9] = (char *)REG_R9;
	pulRegNumber[9] = (int32_t)0x09;

	ppszRegName[0xA] = (char *)REG_R10;
	pulRegNumber[0xA] = (int32_t)0xA;

	ppszRegName[0xB] = (char *)REG_R11;
	pulRegNumber[0xB] = (int32_t)0xB;

	ppszRegName[0xc] = (char *)REG_R12;
	pulRegNumber[0xc] = (int32_t)0xC;

	ppszRegName[0xd] = (char *)REG_R13;
	pulRegNumber[0xd] = (int32_t)0xD;

	ppszRegName[0xe] = (char *)REG_R14;
	pulRegNumber[0xe] = (int32_t)0xE;

	ppszRegName[0xf] = (char *)REG_R15;
	pulRegNumber[0xf] = (int32_t)0xF;

	ppszRegName[0x10] = (char *)REG_R16;
	pulRegNumber[0x10] = (int32_t)0x10;

	ppszRegName[0x11] = (char *)REG_R17;
	pulRegNumber[0x11] = (int32_t)0x11;

	ppszRegName[0x12] = (char *)REG_R18;
	pulRegNumber[0x12] = (int32_t)0x12;

	ppszRegName[0x13] = (char *)REG_R19;
	pulRegNumber[0x13] = (int32_t)0x13;

	ppszRegName[0x14] = (char *)REG_R20;
	pulRegNumber[0x14] = (int32_t)0x14;

	ppszRegName[0x15] = (char *)REG_R21;
	pulRegNumber[0x15] = (int32_t)0x15;

	ppszRegName[0x16] = (char *)REG_R22;
	pulRegNumber[0x16] = (int32_t)0x16;

	ppszRegName[0x17] = (char *)REG_R23;
	pulRegNumber[0x17] = (int32_t)0x17;

	ppszRegName[0x18] = (char *)REG_R24;
	pulRegNumber[0x18] = (int32_t)0x18;

	ppszRegName[0x19] = (char *)REG_R25;
	pulRegNumber[0x19] = (int32_t)0x19;

	ppszRegName[0x1A] = (char *)REG_GP;
	pulRegNumber[0x1A] = (int32_t)0x1A;

	ppszRegName[0x1B] = (char *)REG_FP;
	pulRegNumber[0x1B] = (int32_t)0x1B;

	ppszRegName[0x1C] = (char *)REG_SP;
	pulRegNumber[0x1C] = (int32_t)0x1C;

	ppszRegName[0x1D] = (char *)REG_ILINK1;
	pulRegNumber[0x1D] = (int32_t)0x1D;

	ppszRegName[0x1E] = (char *)REG_ILINK2;
	pulRegNumber[0x1E] = (int32_t)0x1E;

	ppszRegName[0x1F] = (char *)REG_BLINK;
	pulRegNumber[0x1F] = (int32_t)0x1F;

	ppszRegName[0x20] = (char *)REG_R32;
	pulRegNumber[0x20] = (int32_t)0x20;

	ppszRegName[0x21] = (char *)REG_R33;
	pulRegNumber[0x21] = (int32_t)0x21;

	ppszRegName[0x22] = (char *)REG_R34;
	pulRegNumber[0x22] = (int32_t)0x22;

	ppszRegName[0x23] = (char *)REG_R35;
	pulRegNumber[0x23] = (int32_t)0x23;

	ppszRegName[0x24] = (char *)REG_R36;
	pulRegNumber[0x24] = (int32_t)0x24;

	ppszRegName[0x25] = (char *)REG_R37;
	pulRegNumber[0x25] = (int32_t)0x25;

	ppszRegName[0x26] = (char *)REG_R38;
	pulRegNumber[0x26] = (int32_t)0x26;

	ppszRegName[0x27] = (char *)REG_R39;
	pulRegNumber[0x27] = (int32_t)0x27;

	ppszRegName[0x28] = (char *)REG_R40;
	pulRegNumber[0x28] = (int32_t)0x28;

	ppszRegName[0x29] = (char *)REG_R41;
	pulRegNumber[0x29] = (int32_t)0x29;

	ppszRegName[0x2A] = (char *)REG_R42;
	pulRegNumber[0x2A] = (int32_t)0x2A;

	ppszRegName[0x2B] = (char *)REG_R43;
	pulRegNumber[0x2B] = (int32_t)0x2B;

	ppszRegName[0x2C] = (char *)REG_R44;
	pulRegNumber[0x2C] = (int32_t)0x2C;

	ppszRegName[0x2D] = (char *)REG_R45;
	pulRegNumber[0x2D] = (int32_t)0x2D;

	ppszRegName[0x2E] = (char *)REG_R46;
	pulRegNumber[0x2E] = (int32_t)0x2E;

	ppszRegName[0x2F] = (char *)REG_R47;
	pulRegNumber[0x2F] = (int32_t)0x2F;

	ppszRegName[0x30] = (char *)REG_R48;
	pulRegNumber[0x30] = (int32_t)0x30;

	ppszRegName[0x31] = (char *)REG_R49;
	pulRegNumber[0x31] = (int32_t)0x31;

	ppszRegName[0x32] = (char *)REG_R50;
	pulRegNumber[0x32] = (int32_t)0x32;

	ppszRegName[0x33] = (char *)REG_R51;
	pulRegNumber[0x33] = (int32_t)0x33;

	ppszRegName[0x34] = (char *)REG_R52;
	pulRegNumber[0x34] = (int32_t)0x34;

	ppszRegName[0x35] = (char *)REG_R53;
	pulRegNumber[0x35] = (int32_t)0x35;

	ppszRegName[0x36] = (char *)REG_R54;
	pulRegNumber[0x36] = (int32_t)0x36;

	ppszRegName[0x37] = (char *)REG_R55;
	pulRegNumber[0x37] = (int32_t)0x37;

	ppszRegName[0x38] = (char *)REG_R56;
	pulRegNumber[0x38] = (int32_t)0x38;

	ppszRegName[0x39] = (char *)REG_R57;
	pulRegNumber[0x39] = (int32_t)0x39;

	ppszRegName[0x3A] = (char *)REG_R58;
	pulRegNumber[0x3A] = (int32_t)0x3A;

	ppszRegName[0x3b] = (char *)REG_R59;
	pulRegNumber[0x3b] = (int32_t)0x3B;

	ppszRegName[0x3c] = (char *)REG_LPCOUNT;
	pulRegNumber[0x3c] = (int32_t)0x3C;

	ppszRegName[0x3d] = (char *)REG_R61;
	pulRegNumber[0x3d] = (int32_t)0x3D;

	ppszRegName[0x3e] = (char *)REG_LIMM;
	pulRegNumber[0x3e] = (int32_t)0x3E;

	ppszRegName[0x3f] = (char *)REG_PCL;
	pulRegNumber[0x3f] = (int32_t)0x3F;

	ppszRegName[0x40] = (char *)REG_STATUS;
	pulRegNumber[0x40] = (int32_t)0x40;

	ppszRegName[0x41] = (char *)REG_SEMAPHORE;
	pulRegNumber[0x41] = (int32_t)0x41;

	ppszRegName[0x42] = (char *)REG_LP_START;
	pulRegNumber[0x42] = (int32_t)0x42;

	ppszRegName[0x43] = (char *)REG_LP_END;
	pulRegNumber[0x43] = (int32_t)0x43;

	ppszRegName[0x44] = (char *)REG_IDENTITY;
	pulRegNumber[0x44] = (int32_t)0x44;

	ppszRegName[0x45] = (char *)REG_DEBUG;
	pulRegNumber[0x45] = (int32_t)0x45;

	ppszRegName[0x46] = (char *)REG_PC;
	pulRegNumber[0x46] = (int32_t)0x46;

	ppszRegName[0x47] = (char *)REG_UNKNOWN;
	pulRegNumber[0x47] = (int32_t)0x47;

	ppszRegName[0x48] = (char *)REG_UNKNOWN;
	pulRegNumber[0x48] = (int32_t)0x48;

	ppszRegName[0x49] = (char *)REG_UNKNOWN;
	pulRegNumber[0x49] = (int32_t)0x49;

	ppszRegName[0x4a] = (char *)REG_STATUS32;
	pulRegNumber[0x4a] = (int32_t)0x4a;

	ppszRegName[0x4b] = (char *)REG_STATUS32_L1;
	pulRegNumber[0x4b] = (int32_t)0x4b;

	ppszRegName[0x4c] = (char *)REG_STATUS32_L2;
	pulRegNumber[0x4c] = (int32_t)0x4c;

	ppszRegName[0x4d] = (char *)REG_UNKNOWN;
	pulRegNumber[0x4d] = (int32_t)0x4d;

	ppszRegName[0x4e] = (char *)REG_UNKNOWN;
	pulRegNumber[0x4e] = (int32_t)0x4e;

	ppszRegName[0x4f] = (char *)REG_UNKNOWN;
	pulRegNumber[0x4f] = (int32_t)0x4f;

	ppszRegName[0x50] = (char *)REG_UNKNOWN;
	pulRegNumber[0x50] = (int32_t)0x50;

	ppszRegName[0x51] = (char *)REG_UNKNOWN;
	pulRegNumber[0x51] = (int32_t)0x51;

	ppszRegName[0x52] = (char *)REG_UNKNOWN;
	pulRegNumber[0x52] = (int32_t)0x52;

	ppszRegName[0x53] = (char *)REG_UNKNOWN;
	pulRegNumber[0x53] = (int32_t)0x53;

	ppszRegName[0x54] = (char *)REG_UNKNOWN;
	pulRegNumber[0x54] = (int32_t)0x54;

	ppszRegName[0x55] = (char *)REG_UNKNOWN;
	pulRegNumber[0x55] = (int32_t)0x55;

	ppszRegName[0x56] = (char *)REG_UNKNOWN;
	pulRegNumber[0x56] = (int32_t)0x56;

	ppszRegName[0x57] = (char *)REG_UNKNOWN;
	pulRegNumber[0x57] = (int32_t)0x57;

	ppszRegName[0x58] = (char *)REG_UNKNOWN;
	pulRegNumber[0x58] = (int32_t)0x58;

	ppszRegName[0x59] = (char *)REG_UNKNOWN;
	pulRegNumber[0x59] = (int32_t)0x59;

	ppszRegName[0x5a] = (char *)REG_UNKNOWN;
	pulRegNumber[0x5a] = (int32_t)0x5a;

	ppszRegName[0x5b] = (char *)REG_UNKNOWN;
	pulRegNumber[0x5b] = (int32_t)0x5b;

	ppszRegName[0x5c] = (char *)REG_UNKNOWN;
	pulRegNumber[0x5c] = (int32_t)0x5c;

	ppszRegName[0x5d] = (char *)REG_UNKNOWN;
	pulRegNumber[0x5d] = (int32_t)0x5d;

	ppszRegName[0x5e] = (char *)REG_UNKNOWN;
	pulRegNumber[0x5e] = (int32_t)0x5e;

	ppszRegName[0x5f] = (char *)REG_UNKNOWN;
	pulRegNumber[0x5f] = (int32_t)0x5f;

	ppszRegName[0x60] = (char *)REG_UNKNOWN;
	pulRegNumber[0x60] = (int32_t)0x60;

	ppszRegName[0x61] = (char *)REG_COUNT0;
	pulRegNumber[0x61] = (int32_t)0x61;

	ppszRegName[0x62] = (char *)REG_CONTROL0;
	pulRegNumber[0x62] = (int32_t)0x62;

	ppszRegName[0x63] = (char *)REG_LIMIT0;
	pulRegNumber[0x63] = (int32_t)0x63;

	ppszRegName[0x64] = (char *)REG_UNKNOWN;
	pulRegNumber[0x64] = (int32_t)0x64;

	ppszRegName[0x65] = (char *)REG_INT_VECTOR_BASE;
	pulRegNumber[0x65] = (int32_t)0x65;

	ppszRegName[0x66] = (char *)REG_UNKNOWN;
	pulRegNumber[0x66] = (int32_t)0x66;

	ppszRegName[0x67] = (char *)REG_UNKNOWN;
	pulRegNumber[0x67] = (int32_t)0x67;

	ppszRegName[0x68] = (char *)REG_UNKNOWN;
	pulRegNumber[0x68] = (int32_t)0x68;

	ppszRegName[0x69] = (char *)REG_UNKNOWN;
	pulRegNumber[0x69] = (int32_t)0x69;

	ppszRegName[0x6a] = (char *)REG_UNKNOWN;
	pulRegNumber[0x6a] = (int32_t)0x6a;

	ppszRegName[0x6b] = (char *)REG_UNKNOWN;
	pulRegNumber[0x6b] = (int32_t)0x6b;

	ppszRegName[0x6c] = (char *)REG_UNKNOWN;
	pulRegNumber[0x6c] = (int32_t)0x6c;

	ppszRegName[0x6d] = (char *)REG_UNKNOWN;
	pulRegNumber[0x6d] = (int32_t)0x6d;
	ppszRegName[0x6e] = (char *)REG_UNKNOWN;
	pulRegNumber[0x6e] = (int32_t)0x6e;
	ppszRegName[0x6f] = (char *)REG_UNKNOWN;
	pulRegNumber[0x6f] = (int32_t)0x6f;
	ppszRegName[0x70] = (char *)REG_UNKNOWN;
	pulRegNumber[0x70] = (int32_t)0x70;
	ppszRegName[0x71] = (char *)REG_UNKNOWN;
	pulRegNumber[0x71] = (int32_t)0x71;
	ppszRegName[0x72] = (char *)REG_UNKNOWN;
	pulRegNumber[0x72] = (int32_t)0x72;
	ppszRegName[0x73] = (char *)REG_UNKNOWN;
	pulRegNumber[0x73] = (int32_t)0x73;
	ppszRegName[0x74] = (char *)REG_UNKNOWN;
	pulRegNumber[0x74] = (int32_t)0x74;
	ppszRegName[0x75] = (char *)REG_UNKNOWN;
	pulRegNumber[0x75] = (int32_t)0x75;
	ppszRegName[0x76] = (char *)REG_UNKNOWN;
	pulRegNumber[0x76] = (int32_t)0x76;
	ppszRegName[0x77] = (char *)REG_UNKNOWN;
	pulRegNumber[0x77] = (int32_t)0x77;
	ppszRegName[0x78] = (char *)REG_UNKNOWN;
	pulRegNumber[0x78] = (int32_t)0x78;
	ppszRegName[0x79] = (char *)REG_UNKNOWN;
	pulRegNumber[0x79] = (int32_t)0x79;
	ppszRegName[0x7a] = (char *)REG_UNKNOWN;
	pulRegNumber[0x7a] = (int32_t)0x7a;
	ppszRegName[0x7b] = (char *)REG_UNKNOWN;
	pulRegNumber[0x7b] = (int32_t)0x7b;
	ppszRegName[0x7c] = (char *)REG_UNKNOWN;
	pulRegNumber[0x7c] = (int32_t)0x7c;
	ppszRegName[0x7d] = (char *)REG_UNKNOWN;
	pulRegNumber[0x7d] = (int32_t)0x7d;
	ppszRegName[0x7e] = (char *)REG_UNKNOWN;
	pulRegNumber[0x7e] = (int32_t)0x7e;
	ppszRegName[0x7f] = (char *)REG_UNKNOWN;
	pulRegNumber[0x7f] = (int32_t)0x7f;
	ppszRegName[0x80] = (char *)REG_UNKNOWN;
	pulRegNumber[0x80] = (int32_t)0x80;
	ppszRegName[0x81] = (char *)REG_MACMODE;
	pulRegNumber[0x81] = (int32_t)0x81;
	ppszRegName[0x82] = (char *)REG_UNKNOWN;
	pulRegNumber[0x82] = (int32_t)0x82;
	ppszRegName[0x83] = (char *)REG_IRQ_LV12;
	pulRegNumber[0x83] = (int32_t)0x83;

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: GetRegSettingsFromXML
	 Engineer: Sreekanth.V
		Input: ppszRegName  :  Array for storing Register names
			   pulRegNumber :  Array for storing Register numbers
			   pulGdbRegNumber : Array for storing GDB Register number
			   iNumRegs     :  Number of Registers
			   pulAccessType: Access type of register - read write, read only or write only
	   Output: int32_t  - Error Code
  Description: Get Register Configuration settings from XML
Date           Initials    Description
08-Oct-2012      SV         Initial
15-Nov-2012     SPT        Added Support for BCR registers
****************************************************************************/
static int32_t GetRegSettingsFromXML(const char *pszRegFileName, char *ppszRegName[], uint32_t *pulRegNumber, uint32_t *pulGdbRegNumber, TyAccessType *pulAccessType, int32_t *iNumRegs)
{
	TyXMLNode *ptyNode;
	TyXMLFile tyXMLSCFile;
	//int32_t i;
	int32_t iRegCount = 0, iCurrReg = 0, iErrRet = 0;
	char *pRegNum;
	char *accessType;
	uint32_t iGdbReg;

	// if ((iErrRet = SetupCoreRegs(RISCVH_RISCV600, ppszRegName, pulRegNumber, pulGdbRegNumber, iNumRegs)) != GDBSERV_NO_ERROR)
	//     return iErrRet;

	iErrRet = XML_ReadFile(pszRegFileName, &tyXMLSCFile);
	if (iErrRet != 0)
		return iErrRet;
	ptyNode = XML_GetChildNode((XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "target")), "feature");

	if (ptyNode == NULL)
		return GDBSERV_XML_ERROR;

	iRegCount = XML_GetChildNodeCount(ptyNode, (char *)"register");

	//Ensuring XML contains valid register information
	if (iRegCount == 0)
		return GDBSERV_XML_ERROR;

	if ((ptyNode = XML_GetChildNode(ptyNode, "register")) == NULL)
		return GDBSERV_XML_ERROR;

	iCurrReg = *iNumRegs;
	*iNumRegs = *iNumRegs + iRegCount;
	iGdbReg = DEFAULT_REG_COUNT;

	while (ptyNode != NULL)
	{
		ptyNode = XML_GetNode(ptyNode, "register");
		if ((ppszRegName[iCurrReg] = XML_GetAttrib(ptyNode, "name")) == NULL)
			break;
		if ((pRegNum = XML_GetAttrib(ptyNode, "number")) == NULL)
			break;

		pulRegNumber[iCurrReg] = strtoul(pRegNum, NULL, 16) + (int32_t)AUX_BASE;
		pulGdbRegNumber[iCurrReg] = iGdbReg;

		if ((accessType = XML_GetAttrib(ptyNode, "access")) == NULL)
			break;
		if (stricmp(accessType, "RO") == 0)
		{
			pulAccessType[iCurrReg] = RO;
		}
		else if (stricmp(accessType, "WO") == 0)
		{
			pulAccessType[iCurrReg] = WO;
		}
		else if (stricmp(accessType, "RW") == 0)
		{
			pulAccessType[iCurrReg] = RW;
		}
		else
		{
			pulAccessType[iCurrReg] = RO;
		}

		++iCurrReg;
		++iGdbReg;
		ptyNode = ptyNode->pNext;
	}

	if (ptyNode != NULL)
		iErrRet = GDBSERV_XML_ERROR;


	//getting BCR registers from XML
	ptyNode = XML_GetChildNode((XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "target")), "feature");

	if (ptyNode == NULL)
		return GDBSERV_XML_ERROR;

	iRegCount = XML_GetChildNodeCount(ptyNode, (char *)"bcr");

	//Ensuring XML contains valid register information
	if (iRegCount == 0)
		return iErrRet;

	if ((ptyNode = XML_GetChildNode(ptyNode, "bcr")) == NULL)
		return GDBSERV_XML_ERROR;

	iCurrReg = *iNumRegs;

	*iNumRegs = iRegCount + *iNumRegs;

	while (ptyNode != NULL)
	{
		ptyNode = XML_GetNode(ptyNode, "bcr");
		if ((ppszRegName[iCurrReg] = XML_GetAttrib(ptyNode, "name")) == NULL)
			break;
		if ((pRegNum = XML_GetAttrib(ptyNode, "number")) == NULL)
			break;

		pulRegNumber[iCurrReg] = strtoul(pRegNum, NULL, 16) + (int32_t)AUX_BASE;
		pulGdbRegNumber[iCurrReg] = iGdbReg;

		pulAccessType[iCurrReg] = RO;
		++iGdbReg;
		++iCurrReg;
		ptyNode = ptyNode->pNext;
	}

	if (ptyNode != NULL)
		iErrRet = GDBSERV_XML_ERROR;


	return iErrRet;
}

/****************************************************************************
	 Function: GetProcSpecificRegSettings
	 Engineer: Andre Schmiel
		Input: pszFileName
	   Output: Error Code
  Description: Set up processor specific RISCV Registers
Date           Initials    Description
13-Apr-2017    AS          Initial
****************************************************************************/
int32_t GetProcSpecificRegSettings(const char *pszRegFileName, char *ppszRegName[], uint32_t *pulRegNumber, uint32_t *pulGdbRegNumber, TyAccessType *pulAccessType, int32_t *piNumRegs)
{
	int32_t iErr = GDBSERV_NO_ERROR;

	iErr = GetRegSettingsFromXML(pszRegFileName, ppszRegName, pulRegNumber, pulGdbRegNumber, pulAccessType, piNumRegs);

	if (iErr != GDBSERV_NO_ERROR)
		PrintMessage(ERROR_MESSAGE, "Error occurred while getting Register settings  from %s", tySI.szRegFile);

	return iErr;
}

/****************************************************************************
	 Function: strtolower
	 Engineer: Andre Schmiel
		Input: pszString
	   Output: pszString
  Description: convert string to lower case
Date           Initials    Description
13-Apr-2017    AS          Initial
****************************************************************************/
char *strtolower(char *pszString)
{
	while (*pszString)
	{
		if ((*pszString >= 'A') && (*pszString <= 'Z'))
			*pszString -= ('A' - 'a');                             // subtract offset between lower and upper case

		pszString++;
	}

	return(pszString);
}

/****************************************************************************
	 Function: SetUpAuxRegs
	 Engineer: Sreekanth.V
		Input: Nil
	   Output: Error Code
  Description: Set up RISCV Registers
Date           Initials    Description
08-Oct-2012    SV          Initial
25-10-2012     SPT         Correction in register order and memory allocation
13-04-2017     AS          Added multi register file support
****************************************************************************/
static int32_t SetUpRISCVRegs()
{
	//char *pszFileName;
	char          pszFileName[_MAX_PATH];
	uint32_t ulCurrReg = 0, ulProcNum;
	int32_t           iErr = GDBSERV_NO_ERROR;
	TyRISCVReg *g_TempptyRISCVReg;

	//For storing Register Names
	char *pszRegName[MAX_REG_COUNT];
	int32_t   iNumberOfRegisters;
	//For storing Register Numbers
	uint32_t ulRegNumber[MAX_REG_COUNT] = { 0 };
	uint32_t ulGdbRegIndex[MAX_REG_COUNT] = { 0 };
	TyAccessType ulAccessType[MAX_REG_COUNT] = { RW };

	//andre: only read the default register XML file once
	memset(pszRegName, '\0', sizeof(pszRegName));
	memset(ulRegNumber, 0, sizeof(ulRegNumber));

	// Get Register configurations from XML file,if valid
	if (((tySI.ulCurrentTapNumber == 0)
		&& (tySI.bIsRegFilePresent))
		|| ((tySI.ulCurrentTapNumber > 0)
			&& (tySI.bIsMultiRegFilePresent[tySI.ulCurrentTapNumber])))
	{
		if (tySI.bIsRegFilePresent)
			ulProcNum = 0;
		else
			ulProcNum = tySI.ulCurrentTapNumber;

		iErr = SetupDefaultRegsNameAndNumber(pszRegName, ulRegNumber, ulGdbRegIndex, ulAccessType, &iNumberOfRegisters);
		if (iErr != GDBSERV_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, "Error occurred while getting default Register settings");
			return iErr;
		}

		strcpy(pszFileName, tySI.szRegFile[ulProcNum]);
		if (tySI.bDebugOutput)
			LogMessage("Create register setting from file '%s'.", pszFileName);

		iErr = GetProcSpecificRegSettings(pszFileName, pszRegName, ulRegNumber, ulGdbRegIndex, ulAccessType, &iNumberOfRegisters);
		if (iErr != GDBSERV_NO_ERROR)
		{
			PrintMessage(ERROR_MESSAGE, "Unable to create register setting from file '%s'.", pszFileName);
			return iErr;
		}

		tySI.ulNumberOfRegisters = (uint32_t)iNumberOfRegisters;
	}
	// No XML provided,applying default settings..
	else
	{
		//PrintMessage(INFO_MESSAGE, "Use default settings for core %lx", tySI.ulCurrentTapNumber);

		tySI.ulNumberOfRegisters = (uint32_t)RISCV_REG_COUNT;
		iErr = GetDefaultRegSettings(tySI.ulNumberOfRegisters, pszRegName, ulRegNumber);
	}

	// Allocate memory for the avilable number of registers
	g_TempptyRISCVReg = (TyRISCVReg *)realloc(g_ptyRISCVReg, ((uint32_t)tySI.ulNumberOfRegisters) * sizeof(TyRISCVReg));
	if (g_TempptyRISCVReg == NULL)
	{
		PrintMessage(ERROR_MESSAGE, GDBSERV_ERR_MALLOC);
		return GDBSERV_ERROR;
	}
	g_ptyRISCVReg = g_TempptyRISCVReg;

	if (sizeof(g_ptyRISCVReg) < sizeof(TyRISCVReg))
		return GDBSERV_ERROR;

	// Set up Registers as per the values in arrays pszRegName & ulRegNumber
	for (ulCurrReg = 0; ulCurrReg < tySI.ulNumberOfRegisters; ulCurrReg++)
	{
		g_ptyRISCVReg[ulCurrReg].ulAddrIndex = ulRegNumber[ulCurrReg];
		g_ptyRISCVReg[ulCurrReg].pszRegName = pszRegName[ulCurrReg];
		g_ptyRISCVReg[ulCurrReg].ulGdbRegIndex = ulGdbRegIndex[ulCurrReg];
		g_ptyRISCVReg[ulCurrReg].accessType = ulAccessType[ulCurrReg];
	}

	g_ulNumberOfRegisters = tySI.ulNumberOfRegisters;

	return iErr;
}

/****************************************************************************
	 Function: GdbRISCVSetupFunction
	 Engineer: Nikolay Chokoev
		Input:
	   Output:
  Description:
Date           Initials    Description
04-Dec-2007    NCH         Initial
12-04-2012     SPT         Added cJTAG support
****************************************************************************/
static int32_t GdbRISCVSetupFunction(void *pvPtr, TyRISCVSetupInterface *ptyRISCVSetupInt)
{
	uint32_t ulIndex;
	char szNumTAPs[3];
	char szIrWidth[3];
	char szDevices[10];
	char szByPassInst[3];

	for (ulIndex = 1; ulIndex < MAX_TAPS_IN_CHAIN + 1; ulIndex++)
	{
		if ((tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulIRlength == 0) ||
			(tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulBypassInst == 0))
			break;
	}
	ulIndex--;
	if (ulIndex > MAX_TAPS_IN_CHAIN)
		ulIndex = MAX_TAPS_IN_CHAIN;
	sprintf(szNumTAPs, "%d", (uint32_t)ulIndex);
	ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_ADAPTIVECLOCK, tySI.bJtagRtck ? "1" : "0");
	ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_MULTICORE, RISCV_SETUP_ITEM_NUMBEROFTAPS, szNumTAPs);

	for (ulIndex = 1; ulIndex < MAX_TAPS_IN_CHAIN + 1; ulIndex++)
	{
		if ((tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulIRlength == 0) ||
			(tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulBypassInst == 0))
			break;
		sprintf(szDevices, "Device%d", (uint32_t)ulIndex);
		sprintf(szIrWidth, "%d", (uint32_t)tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulIRlength);
		sprintf(szByPassInst, "%d", (uint32_t)tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulBypassInst);
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, szDevices, RISCV_SETUP_ITEM_RISCVCORE,
			tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].bIsRISCVCore ? "1" : "0");
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, szDevices, RISCV_SETUP_ITEM_IRWIDTH, szIrWidth);
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, szDevices, RISCV_SETUP_ITEM_BYPASSINST, szByPassInst);
	}
	if (tySI.szProbeInstance[0] != 0)
	{
		PrintMessage(DEBUG_MESSAGE, "\n%s\n%s\n%s\n", RISCV_SETUP_SECT_MISCOPTIONS, RISCV_SETUP_ITEM_PROBEINSTANCE, tySI.szProbeInstance);
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_MISCOPTIONS, RISCV_SETUP_ITEM_PROBEINSTANCE, tySI.szProbeInstance);
	}
	/*if (strcmp(tySI.tyProcConfig.szInternalName, "RISCVangel") == 0)
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, "0");
	else if (strcmp(tySI.tyProcConfig.szInternalName, "RISCV") == 0)
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, "1");
	else if (strcmp(tySI.tyProcConfig.szInternalName, "RISCV_With_Reset") == 0)
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, "2");
	else if (strcmp(tySI.tyProcConfig.szInternalName, "RISCV_CJTAG_With_TPA_R1") == 0)
	{
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, "3");
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TAPRESET, "1");
	}
	else if (strcmp(tySI.tyProcConfig.szInternalName, "RISCV_JTAG_With_TPA_R1") == 0)*/
		ptyRISCVSetupInt->pfSetSetupItem(pvPtr, RISCV_SETUP_SECT_TARGETCONFIG, RISCV_SETUP_ITEM_TARGETOPTION, "4");
	return 1;
}

/****************************************************************************
	 Function: if_version
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t if_version(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return RISCVINT_BASE_VERSION;
}

/****************************************************************************
	 Function: if_loc_filt_printf
	 Engineer: Nikolay Chokoev
		Input:
	   Output:
  Description:
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void if_filt_printf(const char *format)
{
	char *firmwarev;
	char *diskwarew;
	char *pszResult = NULL;

	firmwarev = strstr((char*)format, "firmware");
	diskwarew = strstr((char*)format, "diskware");

	if (firmwarev != NULL)
	{
		if ((pszResult = strtok(firmwarev, ":")) == NULL)
		{
		}
		if ((pszResult = strtok(NULL, "\n")) == NULL)
		{
		}
		if (pszResult != NULL)
		{
			strncpy(tySI.tyProcConfig.Processor._RISCV.szFirmwareVer,
				pszResult,
				_MAX_PATH);
		}
	}

	if (diskwarew != NULL)
	{
		if ((pszResult = strtok(diskwarew, ":")) == NULL)
		{
		}
		if ((pszResult = strtok(NULL, "\n")) == NULL)
		{
		}
		if (pszResult != NULL)
		{
			strncpy(tySI.tyProcConfig.Processor._RISCV.szDiskwareVer,
				pszResult,
				_MAX_PATH);
		}
	}
}

/****************************************************************************
	 Function: if_out_printf
	 Engineer: Nikolay Chokoev
		Input:
	   Output:
  Description:
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void if_out_printf(const char *format)
{
	//filter the '*** ' messages from the DLL
	//"*** Ashling Opella-XD RISCV Driver, 16-Nov-2007 v0.0.1"
	//"*** Say 'prop dll_help' for internal help."
	if (!strncmp(format, "*** ", 4))
		return;

	fprintf(stdout,"%s", format);
	fflush(stdout);
}

/****************************************************************************
	 Function: if_printf
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t if_printf(struct RISCV_callback *RISCVInt, const char *format, ...)
{
	RISCVInt = RISCVInt;
	my_printf_func(format);
	return 0;
}

/****************************************************************************
	 Function: if_meminfo
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void if_meminfo(struct RISCV_callback *RISCVInt, unsigned addr, struct Memory_info *m)
{
	RISCVInt = RISCVInt;
	addr = addr;
	m = m;
	return;
}
// end of supported interface 1

/****************************************************************************
	 Function: if_vprintf
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t  if_vprintf(struct RISCV_callback *RISCVInt, const char *format, va_list ap)
{
	RISCVInt = RISCVInt;
	format = format;
	ap = ap;
	return 0;
}

/****************************************************************************
	 Function: if_get_verbose
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t  if_get_verbose(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return 0;
}
// end of interface 2

/****************************************************************************
	 Function: if_sleep
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void  if_sleep(struct RISCV_callback *RISCVInt, unsigned milliseconds)
{
	RISCVInt = RISCVInt;
	milliseconds = milliseconds;
	MsSleep(milliseconds);
}

/****************************************************************************
	 Function: if_in_same_process_as_debugger
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t  if_in_same_process_as_debugger(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return 1;
}

/****************************************************************************
	 Function: if_get_endian
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static struct RISCV_endian*  if_get_endian(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return NULL;
}

/****************************************************************************
	 Function: if_get_tcp_factory
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void *  if_get_tcp_factory(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return NULL;
}

/****************************************************************************
	 Function: if_get_process
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
void *  if_get_process(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return NULL;
}
// end of interface 3

/****************************************************************************
	 Function: if_get_debugger_access
	 Engineer: Nikolay Chokoev
		Input: *******************
	   Output: * As per RISCVINT.H *
  Description: *******************
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
struct Debugger_access * if_get_debugger_access(struct RISCV_callback *RISCVInt)
{
	RISCVInt = RISCVInt;
	return NULL;
}

/****************************************************************************
	 Function: SetupErrorMessage
	 Engineer: Nikolay Chokoev
		Input: iError - error code
	   Output: none
  Description: step
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static void SetupErrorMessage(int32_t iError)
{
	switch (iError)
	{
	case IDERR_LOAD_DLL_FAILED:
		PrintMessage(ERROR_MESSAGE, "Failed to load target library %s. File not found or is corrupt.", szTargetDllName);
		break;
	case IDERR_DLL_NOT_LOADED:
		PrintMessage(ERROR_MESSAGE, "Internal server error, target library %s not loaded.", szTargetDllName);
		break;
	case IDERR_ADD_BP_NO_MEMORY:
		PrintMessage(ERROR_MESSAGE, "Insufficient memory while setting a breakpoint.");
		break;
	case IDERR_ADD_BP_INTERNAL1:
		PrintMessage(ERROR_MESSAGE, "Internal error 1 occurred while setting a breakpoint.");
		break;
	case IDERR_ADD_BP_DUPLICATE:
		PrintMessage(ERROR_MESSAGE, "Duplicate breakpoint found at address while setting a breakpoint.");
		break;
	case IDERR_REMOVE_BP_INTERNAL1:
		PrintMessage(ERROR_MESSAGE, "Internal error 1 occurred while deleting a breakpoint.");
		break;
	case IDERR_REMOVE_BP_INTERNAL2:
		PrintMessage(ERROR_MESSAGE, "Internal error 2 occurred while deleting a breakpoint.");
		break;
	case IDERR_REMOVE_BP_NOT_FOUND:
		PrintMessage(ERROR_MESSAGE, "Breakpoint not found, unable to delete a breakpoint.");
		break;
	case IDERR_ASH_SRCREGISTER:
		PrintMessage(LOG_MESSAGE, "Read attempted from an unsupported register number.");
		break;
	case IDERR_ASH_DSTREGISTER:
		PrintMessage(LOG_MESSAGE, "Write to an unsupported register number ignored.");
		break;
	case IDERR_GDI_ERROR:
		PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
		break;
	case IDERR_DLL_NULL_FUNCTION:
		PrintMessage(ERROR_MESSAGE, "Cannot find exported functions within specified Opella-XD Interface DLL.");
		break;
	case IDERR_DLL_GET_INTERFACE_ERR:
		PrintMessage(ERROR_MESSAGE, "Cannot open debug connection to Opella-XD or connection has been canceled.");
		break;
#ifdef RISCV
	case IDERR_NOT_HALT:
		PrintMessage(ERROR_MESSAGE, "Error occurred when attempting to halt the target.");
		break;
	case IDERR_RISCV_MECH_SETUP_FAIL:
		PrintMessage(ERROR_MESSAGE, "Error occurred when attempting to set system access mechanism.");
#endif // RISCV
		break;
	case IDERR_TARGET_PWDOFF:
		PrintMessage(ERROR_MESSAGE, "Target not detected. Ensure the Opella-XD is connected to the target and that the target is powered on.");
		break;
	case IDERR_DISCOVER_HARTS:
		PrintMessage(ERROR_MESSAGE, "Error occurred during enumeration of RISC-V harts.");
		break;
	case IDERR_DISCOVERY:
		PrintMessage(ERROR_MESSAGE, "Error occurred during discovery of RISC-V target info.");
		break;
	default:
		PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
		break;
	}
}

/****************************************************************************
	 Function: IsBigEndian
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: void
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
28-Nov-2007    NCH         Initial
16-Nov-2012    SPT         Corrected Big endian Test
****************************************************************************/
static void IsBigEndian()
{
	//uint32_t *pulRegValRead;
	//uint32_t *pulRegValWrite;
	//uint32_t *pulValue;
	uint32_t ulIndex;
	//uint32_t ulThreadId;
	//ulThreadId = LOW_CurrentHart();
	for (ulIndex = 0; ulIndex < MAX_TAP_NUM; ulIndex++)
	{
		if (tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[tySI.ulTAP[ulIndex]].bIsRISCVCore == TRUE)
		{
			if (tySI.ulTAP[ulIndex] == 0)
				break;

			//tySI.tyProcConfig.Processor._RISCV.bProcIsBigEndian[ulIndex] = TRUE;

			//if (!tySI.bIsVegaBoard)
			//{
			//	//Set current core
			//	(void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[ulIndex]);

			//	//andre: check version
			//	pulRegValRead = (uint32_t *)malloc((uint32_t)(tyTh[ulThreadId].ptyDiscovery->ulArch / 32)*sizeof(uint32_t));
			//	pulRegValWrite = (uint32_t *)malloc((uint32_t)(tyTh[ulThreadId].ptyDiscovery->ulArch / 32)*sizeof(uint32_t));
			//	pulValue = (uint32_t *)calloc((uint32_t)(tyTh[ulThreadId].ptyDiscovery->ulArch / 32), sizeof(uint32_t));
			//	pulValue[(tyTh[ulThreadId].ptyDiscovery->ulArch / 32) - 1] = 0x01;
			//	(void)LOW_ReadRegister(RISCV_ENDINESS, pulRegValRead);
			//	(void)(LOW_WriteRegister(RISCV_ENDINESS, pulValue));
			//	(void)LOW_ReadRegister(RISCV_ENDINESS, pulRegValWrite);
			//	(void)(LOW_WriteRegister(RISCV_ENDINESS, pulRegValRead));

			//	if ((pulRegValWrite[(tyTh[ulThreadId].ptyDiscovery->ulArch /32)-1] & 0x01) == 1)
			//	{
			//		tySI.tyProcConfig.Processor._RISCV.bProcIsBigEndian[ulIndex] = FALSE;
			//	}
			//		free(pulValue);
			//free(pulRegValRead);
			//free(pulRegValWrite);
			//	//tySI.tyProcConfig.Processor._RISCV.bProcIsBigEndian[ulIndex] = FALSE;
			//}
			//else
			//{
				/* VEGA is little endian and there is no RISCV_ENDINESS register to read from. */
				/* TODO: This information needs to be a part of FamilyRISCV.xml*/
				tySI.tyProcConfig.Processor._RISCV.bProcIsBigEndian[ulIndex] = FALSE;
			//}
			PrintMessage(DEBUG_MESSAGE,
				"Core %d BigEndian?: %d",
				tySI.ulTAP[ulIndex],
				tySI.tyProcConfig.Processor._RISCV.bProcIsBigEndian[ulIndex]);
		}
	}
}

/****************************************************************************
	 Function: RISCVInitVars
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: void
  Description: Init variables....
Date           Initials    Description
06-Dec-2007    NCH         Initial
****************************************************************************/
static void RISCVInitVars()
{
	uint32_t ulIndex;

	for (ulIndex = 0; ulIndex < MAX_TAP_NUM; ulIndex++)
	{
		if (tySI.ulTAP[ulIndex] == 0)
		{
			break;
		}
	}
	if (ulIndex == 0)
		ulIndex = 1;
	tySI.tyProcConfig.Processor._RISCV.uiSleepTime = MAX_SLEEP_TIME / ulIndex;
}

/****************************************************************************
	 Function: GetRunningStatus
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: void
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
28-Nov-2007    NCH         Initial
****************************************************************************/
static void GetRunningStatus()
{
	int32_t  bExecuting;
	uint32_t ulIndex;
	uint32_t ulTapIndex;

	for (ulIndex = 0; ulIndex < MAX_TAP_NUM; ulIndex++)
	{
		if (tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[tySI.ulTAP[ulIndex]].bIsRISCVCore == TRUE)
		{
			if (tySI.ulTAP[ulIndex] == 0)
			{
				break;
			}
			ulTapIndex = tySI.ulTAP[ulIndex] - 1;
			//Set Tap index
			(void)LOW_SetActiveTapIndex(ulTapIndex);
			if (LOW_IsProcessorExecuting(&bExecuting, ulTapIndex) == FALSE)
			{
				//TODO: Some Error
			}
		}
	}
}

/****************************************************************************
	 Function: GetHWResCfg
	 Engineer: Harvinder Singh
		Input: *pulAddress	: pointer to address value
			   ulLength     :length of breakpoint
	   Output: *pulMatch    :pointer to get Match value.
								0 - Matches when the trigger address equals to pulAddress.
								1 - Matches when the top M bits of the trigger address
								    match the top M bits of pulAddress. M is XLEN-1,
							        minus the index of the least-signicant bit containing 0 in pulAddress.
			   *pulResource        :pointer to get resources    
  Description: Get the resource configuration(resources and matchvalue) for each H/W breakpoint and Watchpoint. 
Date           Initials    Description
02-May-2019    HS         Initial
****************************************************************************/
uint32_t GetHWResCfg(uint32_t ulBpType,uint32_t *pulAddress,uint32_t ulLength,uint32_t *pulMatch,uint32_t *pulResource)
{
	//if type is hardware breakpoint. 
	//then set match = 0 and resource to 1.
	if (ulBpType == BPTYPE_HARDWARE)
	{
		 *pulMatch = MATCH_0;
		*pulResource = RESOURCE_1;
	}

	//Rest of all case is for watchpoint only.
	//in case  when length=1, put WP on 1 byte. 
	else if (ulLength == BYTE_1)
	{
		//set match=0 and resources required to 1. 
		*pulMatch = MATCH_0;
		*pulResource = RESOURCE_1;
	}

	//in case  when length=2, put WP on 2 bytes.
	else if (ulLength == BYTE_2)
	{
		// In RISCV Core,for 1-bit Address masking ulAddress should have 1st bit from LSB as 0. 
		//prepare ulAddress value for 1 bit masking.
		//first right shift by 1 then left shift by 1 to make 1st  1 bit from LSB as 0.
		*pulAddress = *pulAddress >> ADDRESS_1_BIT;
		*pulAddress = *pulAddress << ADDRESS_1_BIT;
		
		//set match=1 for masking and resources required is 1. 
		*pulMatch = MATCH_1;
		*pulResource = RESOURCE_1;
	}

	//in case  when length=4, put WP on 4 bytes.
	else if (ulLength == BYTE_4)
	{
		//In RISCV Core,for 2-bit Address masking ulAddress should have 1st bit from LSB as 1 and 2nd bit as 0.
		//prepare ulAddress value for 2 bit masking.
		//first right shift by 2 then left shift by 2 to make 1st 2 bits from LSB as 0.
		//then OR with 0x1 to set 1st bit.
		*pulAddress = *pulAddress >> ADDRESS_2_BIT;
		*pulAddress = (*pulAddress << ADDRESS_2_BIT) | 0x1;

		//set match=1 for masking and resources required is 1. 
		*pulMatch = MATCH_1;
		*pulResource = RESOURCE_1;
	}

	//in case of arrays of variable length.
	//WP on range of addresses.
	else
	{
		//set match=2  and resources required is 2.
		*pulMatch = MATCH_2;
		*pulResource = RESOURCE_2;
	}
	return TRUE;
}
/****************************************************************************
	 Function: GetHWBPResIdx
	 Engineer: Rejeesh S B
		Input: ulBPType         : breakpoint type
			   iCurrentTapIndex : current core index
			   ResRequired      : number of resource required for this BP
	   Output: int32_t : resource number if available, else -1
  Description: Get the free resource for adding HW breakpoint or HW watchpoint
Date           Initials    Description
17-Apr-2019    RSB         Initial
05-May-2019    HS          Modified for multiple resource case
TODO: This function logic needs review
****************************************************************************/
static int32_t GetFreeHWBPResIdx(uint32_t ulBpType, uint32_t ResRequired, uint32_t iCurrentTapIndex)
{
	uint32_t uiIndex = 0, i = 0, uiTempRes = 0;
	int  FirstRes = FALSE;

	for (i = 0; i < tyThrdInfo[iCurrentTapIndex].ptyDiscovery->ulTriggers; i++)
	{
		for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
		{
			if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_HARDWARE ||
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_WRITE ||
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_READ ||
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_ACCESS)
			{
				if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx == i)
				{
					//check if we need 2 resources 
					if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed == RESOURCE_2)
					{
						//increment i here becz 2 resources used for this 
						i = i + 1;
					}
					break;
				}
			}

		}

		//initially when no breakpoint is there ,return first resource.
		if (uiBpCount[iCurrentTapIndex] == 0)
		{
			return i;
		}

		// if uiIndex is equal to uiBpCount,means current resource is available.
		else if (uiIndex == uiBpCount[iCurrentTapIndex])
		{
			//if only 1 resource required 
			if (ResRequired == RESOURCE_1)
				return i;

			//if 2 resources required and FirstRes is false.
			else if (ResRequired == RESOURCE_2 && FirstRes == FALSE)
			{
				///store first resource number and set FirstRes to TRUE.
				uiTempRes = i;
				FirstRes = TRUE;
			}

			//when 2 resources required and second resource is also available.
			else if (ResRequired == RESOURCE_2 && FirstRes == TRUE)
			{
				//return first stored resource
				return uiTempRes;
			}
		}
	}
	return -1;
}
#if 0
static int32_t GetFreeHWBPResIdx(uint32_t ulBpType, uint32_t ResRequired, uint32_t iCurrentTapIndex)
{
	uint32_t uiIndex = 0, i = 0;
	int bResUsed = FALSE;

	for (i = 0; i <= (tyThrdInfo[iCurrentTapIndex].ptyDiscovery->ulTriggers - ResRequired); i++)
	{
		bResUsed = FALSE;
		for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
		{
			if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_HARDWARE ||
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_WRITE ||
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_READ ||
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_ACCESS)
			{
				if (ResRequired == 1)
				{
					if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx == i)
					{
						// This resource is not free
						bResUsed = TRUE;
						if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed == 2)
							i++; // next resourse also not free
						break;
					}
				}
				else if (ResRequired == 2)
				{
					if ((tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx == i) ||
						(tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx == i + 1))
					{
						// This resource is not free
						bResUsed = TRUE;
						if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed == 2)
							i++; // next resourse also not free
						break;
					}
				}
				else
					return -1;
			}
		}
		if (bResUsed == FALSE)
			return i;
	}
	return -1;
}
#endif

/****************************************************************************
	 Function: GetHWBPResIdx
	 Engineer: Harvinder Singh
		Input: ulBPType             : breakpoint type
			   ulAddress            : breakpoint address
			   iCurrentTapIndex     : current Tap index
			   *pulResource         : pointer to store no: of resource used
	   Output: int32_t : resource index of BP address, else -1
  Description: Get the resource index and number of resoource used by particular breakpoint 
Date           Initials    Description
17-Apr-2019    HS         Initial
****************************************************************************/
static int32_t GetHWBPResIdx(uint32_t ulBpType, uint32_t ulAddress, uint32_t iCurrentTapIndex,uint32_t *pulResource)
{
	uint32_t uiIndex = 0;

		for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
		{
			if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == ulBpType &&
				tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress ==ulAddress)
			{
			   *pulResource = tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed;
				return((int32_t)tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx);
			}
		}
		return -1;
}
/****************************************************************************
	 Function: AddBpArrayEntry
	 Engineer: Nikolay Chokoev
		Input: ulBpType       : breakpoint type
			   ulAddress      : breakpoint address
			   ulLength       : breakpoint length
			   iCurrentTapIndex : Core index
	   Output: int32_t - error code
  Description: Add a breakpoint to the breakpoint array
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static int32_t AddBpArrayEntry(uint32_t ulBpType,uint32_t uiResIdx,uint32_t ulMatch,uint32_t ulResrequired, uint32_t ulAddress, uint32_t ulLength, unsigned char *pucInstruction, int32_t iCurrentTapIndex)
{
	TyBpElement * ptyNewBpArray;
	TyBpElement * ptyTempBpArray;
	if (uiBpCount[iCurrentTapIndex] >= uiBpAlloc[iCurrentTapIndex])
	{
		ptyTempBpArray = tyCoreBPArray[iCurrentTapIndex].ptyBpArray;
		// reallocate array adding a chunk of entries...
		ptyNewBpArray = (TyBpElement*)realloc(ptyTempBpArray,
			(((uint64_t)uiBpAlloc[iCurrentTapIndex] + GDBSERV_BP_ARRAY_ALLOC_CHUNK) * sizeof(TyBpElement)));

		if (ptyNewBpArray == NULL)
			return IDERR_ADD_BP_NO_MEMORY;

		// clear new chunk to 0...
		memset(&ptyNewBpArray[uiBpAlloc[iCurrentTapIndex]], 0, (GDBSERV_BP_ARRAY_ALLOC_CHUNK * sizeof(TyBpElement)));

		tyCoreBPArray[iCurrentTapIndex].ptyBpArray = ptyNewBpArray;
		uiBpAlloc[iCurrentTapIndex] += GDBSERV_BP_ARRAY_ALLOC_CHUNK;
	}

	// sanity check, this should not happen...
	if (uiBpCount[iCurrentTapIndex] >= uiBpAlloc[iCurrentTapIndex])
		return IDERR_ADD_BP_INTERNAL1;

	// sanity check, this should not happen...
	if (uiBpCount[iCurrentTapIndex] >= uiBpAlloc[iCurrentTapIndex])
		return IDERR_ADD_BP_INTERNAL1;

	tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulBpType
		= ulBpType;
	tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulAddress
		= ulAddress;
	tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulLength
		= ulLength;
	if (ulBpType == BPTYPE_SOFTWARE)
	{
		tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulResIdx = -1;
			memcpy(tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ucInstruction,
		pucInstruction,
		ulLength);
	}
	else
	{ 
		//Add resource number of particular breakpoint
		tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulResIdx = uiResIdx;
		tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulMatchValue = ulMatch;
		tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]].ulResUsed = ulResrequired;
	}
	uiBpCount[iCurrentTapIndex]++;
	return 0;
}

/****************************************************************************
	 Function: RemoveBpArrayEntry
	 Engineer: Nikolay Chokoev
		Input: iIndex : index of breakpoint to remove
			   iCurrentTapIndex : core index
	   Output: TyError
  Description: Remove a breakpoint from the breakpoint array
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static int32_t RemoveBpArrayEntry(uint32_t uiIndex, int32_t iCurrentTapIndex)
{
	// sanity check, this should not happen...
	if (uiBpCount[iCurrentTapIndex] >= uiBpAlloc[iCurrentTapIndex])
		return IDERR_REMOVE_BP_INTERNAL1;
	if (uiIndex >= uiBpCount[iCurrentTapIndex])
		return IDERR_REMOVE_BP_INTERNAL2;
	// we will not free any array storage
	// but, move all elements above iIndex down 1 place...
	for (; uiIndex < uiBpCount[iCurrentTapIndex] - 1; uiIndex++)
	{
		tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex] =
			tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex + 1];
	}
	// there is one less BP in the array
	uiBpCount[iCurrentTapIndex]--;
	// clear the old top element
	memset(&tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiBpCount[iCurrentTapIndex]], 0, sizeof(TyBpElement));
	return 0;
}

/****************************************************************************
	 Function: RemoveAllBpArray
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: none
  Description: Remove all breakpoints from the breakpoint array, free array
			   for all cores
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static void RemoveAllBpArray()
{
	uint32_t uiIndex;

	for (uiIndex = 0; uiIndex < MAX_TAPS_IN_CHAIN + 1; uiIndex++)
	{
		if (tyCoreBPArray[uiIndex].ptyBpArray)
		{
			free(tyCoreBPArray[uiIndex].ptyBpArray);
			tyCoreBPArray[uiIndex].ptyBpArray = NULL;
		}
		uiBpCount[uiIndex] = 0;
	}
}

/****************************************************************************
	 Function: GetBpArrayEntry
	 Engineer: Nikolay Chokoev
		Input: ulBpType       : breakpoint type
			   ulAddress      : breakpoint address
			   ulLength       : breakpoint length
			   iCurrentTapIndex : Core index
	   Output: int32_t : breakpoint index, else -1
  Description: Returns the index of the breakpoint and instruction
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
static int32_t GetBpArrayEntry(uint32_t  ulBpType,
	uint32_t  ulAddress,
	uint32_t  ulLength,
	unsigned char  *pucInstruction,
	int32_t iCurrentTapIndex)
{
	uint32_t uiIndex;

	for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
	{
		if ((tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == ulBpType) &&
			(tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress == ulAddress) &&
			(tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulLength == ulLength))
		{
			if (ulBpType == BPTYPE_SOFTWARE)
			{
				memcpy(pucInstruction,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ucInstruction,
					ulLength);
			}
			return((int32_t)uiIndex);
		}
	}
	return -1;
}

/****************************************************************************
	 Function: GetBpArrayEntryForAddr
	 Engineer: Harvinder Singh
		Input: ulAddress             : breakpoint address
		       ulBPType              : breakpoint type
			   iCurrentTapIndex     :current core index
	   Output: int32_t : breakpoint index, else -1
  Description: Get the index of a breakpoint in the array, using address and type 
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
static int32_t GetBpArrayEntryForAddr(uint32_t  ulAddress, uint32_t ulBPType,
	int32_t iCurrentTapIndex)
{
	uint32_t uiIndex;

	for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
	{
		if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress == ulAddress && tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == ulBPType)
		{
			return((int32_t)uiIndex);
		}
	}
	return -1;
}

/****************************************************************************
	 Function: LoadProperties
	 Engineer: Nikolay Chokoev
		Input: void
	   Output: int32_t - error code
  Description: Load file with properties definitions.
Date           Initials    Description
21-Nov-2007    NCH         Initial
*****************************************************************************/
int32_t LoadProperties(void)
{
	FILE *pFile;
	int32_t iLineNumber = 0;
	int32_t iScannedItems;
	int32_t iError = GDBSERV_NO_ERROR;
	int32_t iPropCnt = 0;
	char szVariableName[_MAX_PATH];
	char pszPropName[_MAX_PATH];
	char *pszResult = NULL;
	char szLine[_MAX_PATH];
	int32_t iRet;

	// open in text mode so that CR+LF is converted to LF
	pFile = fopen(tySI.szRegisterFile, "rt");
	if (pFile == NULL)
	{
		sprintf(tySI.szError, "Could not open properties file '%s'.", tySI.szRegisterFile);
		return 1;
	}
	while (!feof(pFile))
	{
		if (fgets(szLine, sizeof(szLine), pFile) == NULL)
			break;
		iLineNumber++;
		szVariableName[0] = '\0';
		iScannedItems = sscanf(szLine, " %s ", szVariableName);
		// if an empty line, continue with next line...
		if (iScannedItems <= 0)
			continue;
		// if a comment, continue with next line...
		if ((szVariableName[0] == '\0') ||
			(szVariableName[0] == '#') ||
			(szVariableName[0] == ';') ||
			((szVariableName[0] == '/') && (szVariableName[1] == '/')))
			continue;
		if (iScannedItems == 1)
		{
			if (iPropCnt > MAX_USER_VARS - 1)
			{
				iError = 1;
				break;
			}
			if ((pszResult = strtok(szVariableName, "=")) == NULL)
			{
				iError = 1;
				break;
			}

			if (strcmp(pszResult, "-prop"))
			{
				iError = 1;
				break;
			}

			if ((pszResult = strtok(NULL, "=")) == NULL)
			{
				iError = 1;
				break;
			}
			sprintf(pszPropName, "%s", pszResult);
			if ((pszResult = strtok(NULL, "=")) == NULL)
			{
				iError = 1;
				break;
			}
			if (!strcmp(pszPropName, "jtag_frequency"))
				strncpy(tySI.szJtagFreq, pszResult, _MAX_PATH);
			if (ptyRISCVInstance->ptyFunctionTable->process_property != NULL)
			{
				iRet = ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance, pszPropName, pszResult);
				if (iRet != GDBSERV_NO_ERROR)
					return GDBSERV_ERROR;
				PrintMessage(LOG_MESSAGE,
					"Set property [property] [value] [success]:\n\t\t%s\t%s\t%d",
					pszPropName, pszResult, iRet);
			}
		}
		else
		{
			iError = 1;
			break;
		}
	}
	if (iError != GDBSERV_NO_ERROR)
		sprintf(tySI.szError,
			"Invalid format at line %d of properties file '%s':\n%s",
			iLineNumber, tySI.szRegisterFile, szLine);
	if (pFile != NULL)
		(void)fclose(pFile);
	return iError;
}

/****************************************************************************
	 Function: LOW_SetJtagFreq
	 Engineer: Nikolay Chokoev
		Input:
	   Output:
  Description:
Date           Initials    Description
23-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_SetJtagFreq(void)
{
	switch (tySI.bJtagRtck)
	{
	case TRUE:
		if (ptyRISCVInstance->ptyFunctionTable->process_property != NULL)
		{
			if (ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance, "rtck_enable", "1") != GDBSERV_NO_ERROR)
			{
				return GDBSERV_ERROR;
			}
		}
		break;

	case FALSE:
		if (ptyRISCVInstance->ptyFunctionTable->process_property != NULL)
		{
			if (ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance, "jtag_frequency", tySI.szJtagFreq) != GDBSERV_NO_ERROR)
			{
				return GDBSERV_ERROR;
			}
		}
		break;
	default:
		return GDBSERV_ERROR;
	}

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: RISCVInitProps
	 Engineer: Nikolay Chokoev
		Input:
	   Output:
  Description:
Date           Initials    Description
23-Nov-2007    NCH          Initial
****************************************************************************/
static void RISCVInitProps(void)
{
	if (LOW_IsBigEndian(0) == TRUE)
	{
		PrintMessage(DEBUG_MESSAGE, "Big Endian.");
		/*if (ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance,
			"endian",
			"BE") != TRUE)
		{
			PrintMessage(DEBUG_MESSAGE, "Cannot set Big Endian.");
		}*/
	}
	else
	{
		/*PrintMessage(DEBUG_MESSAGE, "Little Endian.");
		if (ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance,
			"endian",
			"LE") != TRUE)
		{
			PrintMessage(DEBUG_MESSAGE, "Cannot set Little Endian.");
		}*/
	}
}

/****************************************************************************
	 Function: RISCVInitInterface
	 Engineer: Nikolay Chokoev
		Input:
	   Output:
  Description:
Date           Initials    Description
23-Nov-2007    NCH          Initial
****************************************************************************/
static void RISCVInitInterface(struct TyRISCVInstance *pRISCVInst)
{
	static struct RISCV_callback MyRISCVCallback;

	my_printf_func = if_out_printf;

	MyRISCVCallback.pft = &pf; //lint !e123

	if (pRISCVInst->ptyFunctionTable->receive_callback != NULL)
	{
		pRISCVInst->ptyFunctionTable->receive_callback(pRISCVInst, (RISCV_callback*)&MyRISCVCallback);
	}
}

/****************************************************************************
	 Function: VerifyInterfaceDLL
	 Engineer: Roshith R
		Input: void
	   Output: true or false
  Description: Verifies the given interface DLL is a valid one by seRISCVhing
			   for the string 'Ashling' in the ID returned by DLL
Date           Initials    Description
16-Nov-2014    RR         Initial
****************************************************************************/
static int32_t VerifyInterfaceDLL()
{
	char *pcInterfaceID;

	pcInterfaceID = pfAshGetInterfaceID();
	if (NULL == pcInterfaceID)
		return FALSE;

	/* SeRISCVh for the string "Ashling" in the interface ID returned from DLL */
	if (NULL != strstr(pcInterfaceID, "Ashling"))
	{
		/* String 'Ashling' found, valid DLL */
		return TRUE;
	}
	else
	{
		/* String 'Ashling' not found, invalid DLL */
		return FALSE;
	}
}

/****************************************************************************
	 Function: RISCVLoadLibrary
	 Engineer: Nikolay Chokoev
		Input: int32_t bInitialJtagAccess : enable to use initial JTAG access (MIPS specific)
	   Output: int32_t - error code
  Description: Load RISCV library for using with JTAG console
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
static int32_t RISCVLoadLibrary(void)
{
	int32_t iError = 0;

	// Remove any BP details...
	RemoveAllBpArray();

	szTargetDllName[0] = '\0';
	switch (tySI.tyGdbServerTarget)
	{
	case GDBSRVTGT_OPELLAXD:
		strcpy(szTargetDllName, OPELLAXD_DLL_NAME);
		break;
	case GDBSRVTGT_OPELLA:
	case GDBSRVTGT_INVALID:
	default:
		break;
	}

	if (szTargetDllName[0] != '\0')
	{
		hInstRISCVDll = LoadLibrary(szTargetDllName);
	}

	if (hInstRISCVDll == NULL)
	{
		szTargetDllName[0] = '\0';
		switch (tySI.tyGdbServerTarget)
		{
		case GDBSRVTGT_OPELLAXD:
			strcpy(szTargetDllName, OPELLAXD_DLL_NAME_1);
			break;
		case GDBSRVTGT_OPELLA:
		case GDBSRVTGT_INVALID:
		default:
			break;
		}
		if (szTargetDllName[0] != '\0')
		{
			hInstRISCVDll = LoadLibrary(szTargetDllName);
		}

		if (hInstRISCVDll == NULL)
		{
#ifdef _LINUX
			PrintMessage(DEBUG_MESSAGE, "DLL Error: %s", dlerror());
#endif
			return IDERR_LOAD_DLL_FAILED;
		}
	}
	pfget_RISCV_interface_ex	= (Tyget_RISCV_interface_ex)	GetProcAddress(hInstRISCVDll, "get_RISCV_interface_ex");
	pfAshTestScanchain			= (TyAshTestScanchain)			GetProcAddress(hInstRISCVDll, "ASH_TestScanchain");
	pfAshSetSetupItem			= (TyAshGetSetupItem)			GetProcAddress(hInstRISCVDll, "ASH_SetSetupItem");
	pfAshDetectScanchain		= (TyAshDetectScanchain)		GetProcAddress(hInstRISCVDll, "ASH_DetectScanchain");
	pfAshGetSetupItem			= (TyAshGetSetupItem)			GetProcAddress(hInstRISCVDll, "ASH_GetSetupItem");
	pfAshGetLastErrorMessage	= (TyAshGetLastErrorMessage)	GetProcAddress(hInstRISCVDll, "ASH_GetLastErrorMessage");
	pfASH_ListConnectedProbes	= (TyASH_ListConnectedProbes)	GetProcAddress(hInstRISCVDll, "ASH_ListConnectedProbes");
	pfAshScanIR					= (TyAshScanIR)					GetProcAddress(hInstRISCVDll, "ASH_GDBScanIR");
	pfAshScanDR					= (TyAshScanDR)					GetProcAddress(hInstRISCVDll, "ASH_GDBScanDR");
	pfAshTAPReset				= (TyAshTAPReset)				GetProcAddress(hInstRISCVDll, "ASH_TMSReset");
	pfAshGetInterfaceID			= (TyAshGetInterfaceId)			GetProcAddress(hInstRISCVDll, "ASH_GetInterfaceID");
	pfAshUpdateScanchain		= (TyASH_UpdateScanchain)		GetProcAddress(hInstRISCVDll, "ASH_UpdateScanchain");

	// check if pointers are valid
	if ((pfget_RISCV_interface_ex == NULL) || (pfAshTestScanchain == NULL) ||
		(pfAshGetSetupItem == NULL) || (pfAshGetLastErrorMessage == NULL) || (pfAshGetInterfaceID == NULL))
	{
		(void)FreeLibrary(hInstRISCVDll);
		hInstRISCVDll = NULL;
		return IDERR_DLL_NULL_FUNCTION;
	}

	// Check the interface DLL is a valid Ashling DLL
	if (TRUE != VerifyInterfaceDLL())
	{
		PrintMessage(INFO_MESSAGE, "Ashing Opella-XD interface DLL is not a valid one!");
		(void)FreeLibrary(hInstRISCVDll);
		hInstRISCVDll = NULL;
		return IDERR_DLL_NULL_FUNCTION;
	}

	PrintMessage(INFO_MESSAGE, "Initializing connection ...");
	//conect to Opella SN

	if (tySI.szProbeInstance[0] == 0)
	{
		if (pfASH_ListConnectedProbes != NULL)
			pfASH_ListConnectedProbes(tySI.szProbeInstanceList, MAX_PROBE_INSTANCES, &usConnectedInstances);
		if (usConnectedInstances > 0)
			sprintf(tySI.szProbeInstance, "%s", tySI.szProbeInstanceList[0]);
		else
			return IDERR_DLL_GET_INTERFACE_ERR;

		PrintMessage(DEBUG_MESSAGE, "\n%s\t%d\n", tySI.szProbeInstance, usConnectedInstances);
	}
	// ok, let open debug connection
	ptyRISCVInstance = pfget_RISCV_interface_ex(GdbRISCVSetupFunction);
	if (ptyRISCVInstance == NULL)
	{

		(void)FreeLibrary(hInstRISCVDll);
		hInstRISCVDll = NULL;
		return IDERR_DLL_GET_INTERFACE_ERR;
	}
	
	//NCH this is because of lint
	if ((ptyRISCVInstance->ptyFunctionTable->Halt == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->StatusProc == NULL)||
		(ptyRISCVInstance->ptyFunctionTable->Run == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->Discovery == NULL) || 
		(ptyRISCVInstance->ptyFunctionTable->read_reg == NULL) || 
		(ptyRISCVInstance->ptyFunctionTable->write_reg == NULL) || 
		(ptyRISCVInstance->ptyFunctionTable->write_a_reg == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->writeword == NULL) || 
		(ptyRISCVInstance->ptyFunctionTable->readword == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->writedpc == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->read_memory == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->write_memory == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->step == NULL)||
		(ptyRISCVInstance->ptyFunctionTable->read_a_reg == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->addtrigger == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->configure_vega == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->removetrigger == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->TriggerInfo == NULL) || 
		(ptyRISCVInstance->ptyFunctionTable->process_property == NULL)||
		(ptyRISCVInstance->ptyFunctionTable->receive_callback == NULL)||
		(ptyRISCVInstance->ptyFunctionTable->riscv_mechanism == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->writecsr == NULL) ||
		(ptyRISCVInstance->ptyFunctionTable->Discover_Harts == NULL)||
		(ptyRISCVInstance->ptyFunctionTable->target_reset == NULL))

	{
		return IDERR_DLL_GET_INTERFACE_ERR;
	}

	//first init interface
	RISCVInitInterface(ptyRISCVInstance);
	
	if (tySI.bIsVegaBoard)
	{
		iError = Low_ConfigureVega(tySI.tyProcConfig.uiInstrtoSelectADI, (unsigned short)tySI.ulCore, tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[tySI.ulTAP[0]].ulIRlength);
	}
	//second load default settings
	iError = LOW_SetJtagFreq();
	//Load properties
	(void)LoadProperties();
	(void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[0] - 1); //TODO: check if its fine to pass 0 here
	IsBigEndian();
	RISCVInitProps();
	RISCVInitVars();

	return iError;
}

/****************************************************************************
	 Function: RISCVConnect
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t - error code
  Description: Connect to target
Date           Initials    Description
13-Mar-2019     AG         Initial
****************************************************************************/
static int32_t  RISCVConnect(void)
{
	int32_t iError = 0;
	if (!tySI.bJtagConsoleMode)
	{
		
		//enumeration of harts
		iError = LOW_Discover_Harts();
		switch (iError)
		{
		case GDB_PWDOFF:
			return IDERR_TARGET_PWDOFF;
		case GDBSERV_NO_ERROR:
			iError = 0;
			break;
		case GDBSERV_ERROR:
			return IDERR_DISCOVER_HARTS;
		}

		//discovery of enumerated harts
		iError = LOW_Discovery();
		switch (iError)
		{
		case GDB_PWDOFF:
			return IDERR_TARGET_PWDOFF;
		case GDBSERV_NO_ERROR:
			iError = 0;
			break;
		case GDBSERV_ERROR:
			return IDERR_DISCOVERY;
		}
		//activating the hart 1 as default active hart
		tyThrdInfo[0].ulCurrentThread = 1;
		//TODO: if target is already reset and halted ,no need to halt again.
		iError = LOW_Halt(tySI.ulTAP[0] - 1); // by default, connect to 1st requested TAP
		switch (iError)
		{
		case GDB_PWDOFF:
			return IDERR_TARGET_PWDOFF;
		case GDBSERV_NO_ERROR:
			iError = 0;
			break;
		case GDBSERV_ERROR:
			return IDERR_NOT_HALT;
		}

		iError = LOW_RiscvMechanism(UNDEFINED_MEMORY_ACCESS);
		switch (iError)
		{
		case GDB_PWDOFF:
			return IDERR_TARGET_PWDOFF;
		case GDBSERV_NO_ERROR:
			iError = 0;
			break;
		case GDBSERV_ERROR:
			return IDERR_RISCV_MECH_SETUP_FAIL;
		}
		//check the status of target
		GetRunningStatus();
	}
	return iError;
}
/****************************************************************************
	 Function: RISCVFreeLibrary
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t - error code
  Description: Free RISCV library
Date           Initials    Description
14-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t RISCVFreeLibrary(void)
{
	RemoveAllBpArray();              // remove any BP details...
	if (hInstRISCVDll == NULL)
		return 0;                     // if library not loaded, get out...
	(void)FreeLibrary(hInstRISCVDll);
	hInstRISCVDll = NULL;
	pfAshScanIR = NULL;
	pfAshScanDR = NULL;
	pfAshTAPReset = NULL;

	return RISCVFreeLibrary();
}

/****************************************************************************
	 Function: GetRegName
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: uint32_t
  Description: return reg name for GDB reg number
Date           Initials    Description
30-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to use tySI.iNumberOfRegisters as the limit
****************************************************************************/
static char *GetRegName(uint32_t ulGdbRegNumber)
{
	static char szRegName[16];
	uint32_t ulTRegIndex;
	uint32_t ulTempValue = 0;
	//PrintMessage(INFO_MESSAGE, "GetRegName current Core = %lx.", g_ulCurrentCoreNumber);
	ulTempValue = g_ulNumberOfRegisters;
	g_ulNumberOfRegisters = 0;
	for (ulTRegIndex = 0;ulTRegIndex < g_ulNumberOfRegisters;ulTRegIndex++)
	{
		if (g_ptyRISCVReg[ulTRegIndex].ulAddrIndex == ulGdbRegNumber)
			return(char*)g_ptyRISCVReg[ulTRegIndex].pszRegName;
	}
	g_ulNumberOfRegisters = ulTempValue;
	sprintf(szRegName, "0x%lX", ulGdbRegNumber);
	return szRegName;
}

/****************************************************************************
	 Function: GetRegAddr
	 Engineer: Nikolay Chokoev
		Input: register name
	   Output: TRUE if the register is found, FALSE if not
  Description: return RISCV register address by register name
Date           Initials    Description
20-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to use tySI.iNumberOfRegisters as the limit
****************************************************************************/
int32_t GetRegAddr(char *pRegName, int32_t *iRegAddr)
{
	uint32_t ulIndex;

	//PrintMessage(INFO_MESSAGE, "GetRegAddr current Core = %lx.", g_ulCurrentCoreNumber);

	for (ulIndex = 0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
	{
		if (strcasecmp(g_ptyRISCVReg[ulIndex].pszRegName, pRegName) == 0)
		{
			*iRegAddr = (int32_t)g_ptyRISCVReg[ulIndex].ulAddrIndex;
			return TRUE;
		}
	}
	*iRegAddr = 0xFFFF;
	return FALSE;
}

/****************************************************************************
	 Function: GetBpTypeName
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: uint32_t
  Description: return BP type name
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static char  * GetBpTypeName(uint32_t ulBpType)
{
	static char szBpName[16];

	switch (ulBpType)
	{
	case BPTYPE_SOFTWARE:
		return(char*)"SW";
	case BPTYPE_HARDWARE:
		return(char*)"HW";
	case BPTYPE_WRITE:
	case BPTYPE_READ:
	case BPTYPE_ACCESS:
	default:
		sprintf(szBpName, "0x%lX", ulBpType);
		return szBpName;
	}
}

/****************************************************************************
	 Function: ReadFullRegisterByRegDef
	 Engineer: Nikolay Chokoev
		Input: TyRegDef *ptyRegDef - pointer to register definition struct
			   uint32_t *pulRegValue - storage for register value
	   Output: int32_t - error code
  Description: Read full register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t ReadFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t *pulRegValue)
{
	NOREF(ptyRegDef);
	assert(pulRegValue != NULL);
	// not implemented
	*pulRegValue = 0;
	return 0;
}

/****************************************************************************
	 Function: WriteFullRegisterByRegDef
	 Engineer: Nikolay Chokoev
		Input: *ptyRegDef   : pointer to register definition struct
			   ulRegValue   : register value to write
	   Output: TyError
  Description: Write full register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
static int32_t WriteFullRegisterByRegDef(TyRegDef *ptyRegDef, uint32_t ulRegValue)
{
	NOREF(ptyRegDef);
	NOREF(ulRegValue);
	// not implemented
	return 0;
}

//==============================================
// global LOW_functions from here...
//==============================================

/****************************************************************************
	 Function: LOW_Connect
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t : FALSE if error
  Description: Connect to target
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_Connect(const char *EmulatorName,char *szDiskWare1, char *szFirmWare, char * freq)
{
	int32_t iError;
	if ((iError = RISCVConnect()) != 0)
	{
		PrintMessage(INFO_MESSAGE, "%s detected. \ndiskware:%s firmware:%s", EmulatorName, szDiskWare1, szFirmWare);
		SetupErrorMessage(iError);
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_LoadLibrary
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t : FALSE if error
  Description: LoadLibrary
Date           Initials    Description
13-Mar-2019      AG         Initial
****************************************************************************/
int32_t LOW_LoadLibrary(void)
{
	int32_t iError;

	if ((iError = RISCVLoadLibrary()) != 0)
	{
		SetupErrorMessage(iError);
		(void)RISCVFreeLibrary();
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}
/****************************************************************************
	 Function: LOW_Disconnect
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: Error Code
  Description: Disconnect from target
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_Disconnect(void)
{
	int32_t iError;
	//free(pulS1Val);
	//free(pulFpVal);
	free(tyThrdInfo);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Disconnecting target ...");
	if ((iError = RISCVFreeLibrary()) != 0)
	{
		SetupErrorMessage(iError);
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_IsProcessorExecuting
	 Engineer: Nikolay Chokoev
		Input: *pbExecuting : pointer to flag for result
	   Output: int32_t : FALSE if error
  Description: Checks if processor is executing
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_IsProcessorExecuting(volatile int32_t *pbExecuting, int32_t iCurrentTapIndex)
{
	*pbExecuting = 1;

	//Update the running flag
	unsigned char ucGlobalStatus = 0;

	uint32_t ulThreadId;
	ulThreadId = LOW_CurrentHart();

	ucStopReason = GDB_STEP_SET_HALT;
	switch (ptyRISCVInstance->ptyFunctionTable->StatusProc(ptyRISCVInstance, (unsigned short)iCurrentTapIndex, (unsigned short)tySI.ulCore, ulThreadId, &ucGlobalStatus, &ucStopReason, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits))
	{
	case GDBSERV_NO_ERROR:
		if (ucGlobalStatus == 0)
			tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentTapIndex] = 1;
		else
		{
			//tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentTapIndex] == 1 means target just halted, so take reg back up now
			if (tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentTapIndex] == 1)
			{
				if (GDBSERV_NO_ERROR != LOW_BackupFPS1())
					return GDBSERV_ERROR;
			}
			*pbExecuting = 0;
			tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentTapIndex] = 0;
		}
		break;
	case GDB_PWDOFF:
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE, "Error occurred fetching target status (target is powered-off).");
			PrintMessage(ERROR_MESSAGE, GDB_EMSG_NO_TARGET);
		return GDBSERV_ERROR;
	case GDBSERV_ERROR:
	default:
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE, "Error occurred fetching target status.");
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_GetCauseOfBreak
	 Engineer: Nikolay Chokoev
		Input: *ptyStopSignal : storage for cause of break
	   Output: Error Code
  Description: Gets cause of last break
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_GetCauseOfBreak(TyStopSignal *ptyStopSignal)
{
	*ptyStopSignal = TARGET_SIGTRAP;//TARGET_SIGSTOP; //default signal value
	if(ucStopReason == GDB_STEP_SET_HALT || ucStopReason == GDB_TRIGGER_MODULE_HALT || ucStopReason == GDB_EBREAK_EXECUTION_HALT)
		*ptyStopSignal = TARGET_SIGTRAP;
	if (ucStopReason == GDB_DEBUGGER_REQUESTED_HALT)
		*ptyStopSignal = TARGET_SIGINT;
#if 0
	if (GDBSERV_NO_ERROR != LOW_ReadRegister(RISCV_REG_DEBUG, &ulDebugReg))
		return GDBSERV_ERROR;
	if (ulDebugReg & RISCV_REG_DEBUG_SH_BIT)
		*ptyStopSignal = TARGET_SIGTRAP;
	if (ulDebugReg & RISCV_REG_DEBUG_BH_BIT)
		*ptyStopSignal = TARGET_SIGTRAP;
	if (ulDebugReg & RISCV_REG_DEBUG_ZZ_BIT)
		*ptyStopSignal = TARGET_SIGTRAP;
	if (ulDebugReg & RISCV_REG_DEBUG_IS_BIT)
		*ptyStopSignal = TARGET_SIGTRAP;
	if (ulDebugReg & RISCV_REG_DEBUG_FH_BIT)
		*ptyStopSignal = TARGET_SIGTRAP;
#endif
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_ReadRegisters
	 Engineer: Nikolay Chokoev
		Input: *pulRegisters : storage for registers
	   Output: int32_t : FALSE if error
  Description: Reads registers
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_ReadRegisters(uint32_t *pulRegisters)
{
	//uint32_t ulIndex;
	int iRet = GDBSERV_NO_ERROR;

	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Read all registers.");

	//PrintMessage(INFO_MESSAGE, "LOW_ReadRegisters current Core = %lx.", g_ulCurrentCoreNumber);
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	iRet = ptyRISCVInstance->ptyFunctionTable->read_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, pulRegisters, 
		g_ulNumberOfRegisters, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);

	if (tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS) {
		pulRegisters[REG_S0] = tyThrdInfo[ulThredId].ptyDiscovery->ulFpValue;
		pulRegisters[REG_S1] = tyThrdInfo[ulThredId].ptyDiscovery->ulS1Value;
	}
	return iRet;
}

/****************************************************************************
	 Function: LOW_ReadRegister
	 Engineer: Nikolay Chokoev
		Input: ulRegisterNumber  : register to read
			   *pulRegisterValue : storage for one register
	   Output: int32_t : FALSE if error; 2 - PowerDown;
  Description: Reads one register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
****************************************************************************/
int32_t LOW_ReadRegister(uint32_t   ulRegisterNumber,
	uint32_t  *pulRegisterValue)
{
	uint32_t ulReg;
	int iRet = GDBSERV_NO_ERROR;

	ulReg = ulRegisterNumber;

	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE,
			"Read register %s ...",
			GetRegName(ulReg));

	if (ulReg == 0xFFFFFFFF)
	{
		*pulRegisterValue = BAD_HEX_VALUE;
	}

	iRet = ptyRISCVInstance->ptyFunctionTable->read_a_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, pulRegisterValue, ulRegisterNumber, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
	return iRet;
}

/****************************************************************************
	 Function: LOW_WriteRegisters
	 Engineer: Nikolay Chokoev
		Input: *pulRegisters : registers values to write
	   Output: int32_t : FALSE if error
  Description: Writes registers
Date           Initials    Description
26-Nov-2007    NCH         Initial
23-Oct-2012    SPT         Correction for RISCV (XML based register ordering)
****************************************************************************/
int32_t LOW_WriteRegisters(uint32_t *pulRegisters)
{
	int iRet = GDBSERV_NO_ERROR;

	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Write all registers.");

	//PrintMessage(INFO_MESSAGE, "LOW_WriteRegisters current Core = %lx.", g_ulCurrentCoreNumber);
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	if (tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS) {
		tyThrdInfo[ulThredId].ptyDiscovery->ulFpValue = pulRegisters[REG_S0];
		tyThrdInfo[ulThredId].ptyDiscovery->ulS1Value = pulRegisters[REG_S1];
	}
	iRet = ptyRISCVInstance->ptyFunctionTable->write_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, pulRegisters, 32, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
	return iRet;
}

/****************************************************************************
	 Function: LOW_WriteRegister
	 Engineer: Nikolay Chokoev
		Input: ulRegisterNumber  : register to write
			   ulRegisterValue   : register value
	   Output: int32_t : FALSE if error
  Description: writes one register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
****************************************************************************/
int32_t LOW_WriteRegister(uint32_t ulRegisterNumber,
	uint32_t *ulRegisterValue)
{
	int iRet = GDBSERV_NO_ERROR;
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	iRet = ptyRISCVInstance->ptyFunctionTable->write_a_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, 
		ulThredId, ulRegisterValue, ulRegisterNumber, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
	return iRet;
}

/****************************************************************************
	 Function: LOW_ReadPC
	 Engineer: Nikolay Chokoev
		Input: *pulRegisterValue : storage for PC
	   Output: int32_t : FALSE if error
  Description: Reads PC register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
****************************************************************************/
int32_t LOW_ReadPC(uint32_t  *pulRegisterValue)
{
	int iRet = GDBSERV_NO_ERROR;
	uint32_t ulThreadId;
	ulThreadId = LOW_CurrentHart();
	iRet = ptyRISCVInstance->ptyFunctionTable->read_a_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, pulRegisterValue, 32, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits);
	return iRet;
}

/****************************************************************************
	 Function: LOW_WritePC
	 Engineer: Nikolay Chokoev
		Input: ulRegisterValue   : new PC value
	   Output: Error Code
  Description: writes PC register
Date           Initials    Description
20-Nov-2007    NCH         Initial
28-Mar-2008    NCH         PD mode
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_WritePC(uint32_t *ulRegisterValue)
{
	int iRet = GDBSERV_NO_ERROR;
	uint32_t ulThredId;

	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Write register PC value=0x%08lX.",*ulRegisterValue);

	ulThredId = LOW_CurrentHart();
	iRet = ptyRISCVInstance->ptyFunctionTable->writedpc(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulRegisterValue, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
	return iRet;
}

/****************************************************************************
	 Function: LOW_WriteCSR
	 Engineer: Ashutosh Garg
		Input: ulRegisterValue   : RISCV CSR Value
			   ulGDBRegisterNumber : CSR number
	   Output: Error Code
  Description: writes CSR
Date           Initials    Description
5-JAN-2019		  AG         Initial
****************************************************************************/
int32_t LOW_WriteCSR(uint32_t *ulRegisterValue, uint32_t ulGDBRegisterNumber)
{
	uint32_t ulThredId;
	int iRet = GDBSERV_NO_ERROR;
	ulThredId = LOW_CurrentHart();
	iRet = ptyRISCVInstance->ptyFunctionTable->writecsr(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulRegisterValue, ulGDBRegisterNumber, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
	return iRet;
}
/****************************************************************************
	 Function: LOW_ReadMemory
	 Engineer: Nikolay Chokoev
		Input: ulAddress  : address to read from
			   ulCount    : number of bytes to read
			   *pucData   : storage for data
	   Output: int32_t : FALSE if error
  Description: reads memory
Date           Initials    Description
20-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_ReadMemory(uint32_t ulAddress,
	uint32_t ulCount,
	unsigned char *pucData)
{
	uint32_t ulSize;
	int32_t iretValue;
	uint32_t ulThreadId;

	ulThreadId = LOW_CurrentHart();

	//added for DCCM and ICCM check in case of wdc-swrev core
#ifdef RISCV
		//updating the riscv mechanism if the address is in the range of DCCM and ICCM values
		if (GDBSERV_NO_ERROR != LOW_SetRiscvMemAccessMechanism(ulAddress))
			return GDBSERV_ERROR;
#endif

	for (ulSize = 1; ulSize < 4 && ulCount >= ulSize * 2 + (ulAddress & ulSize); ulSize *= 2)
	{
		if (ulAddress & ulSize)
		{
			iretValue = ptyRISCVInstance->ptyFunctionTable->read_memory(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, ulSize, 1, ulAddress, pucData, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits);
			if (iretValue != GDBSERV_NO_ERROR)
				return GDBSERV_ERROR;
			ulAddress += ulSize;
			ulCount -= ulSize;
			pucData += ulSize;
		}
	}

	for (; ulSize > 0; ulSize /= 2)
	{
		uint32_t ulAligned = ulCount - ulCount % ulSize;
		if (ulAligned > 0)
		{
			iretValue = ptyRISCVInstance->ptyFunctionTable->read_memory(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, ulSize, ulAligned / ulSize, ulAddress, pucData, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits);
			if (iretValue != GDBSERV_NO_ERROR)
				return GDBSERV_ERROR;
			ulAddress += ulAligned;
			ulCount -= ulAligned;
			pucData += ulAligned;
		}
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_WriteMemory
	 Engineer: Nikolay Chokoev
		Input: ulAddress  : address to write to
			   ulCount    : number of bytes to write
			   *pucData   : pointer to data to write
	   Output: int32_t : FALSE if error
  Description: writes memory
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_WriteMemory(uint32_t ulAddress,
	uint32_t ulCount,
	unsigned char *pucData)
{
	uint32_t ulSize;
	int32_t iretValue;
	uint32_t ulThredId;

	ulThredId = LOW_CurrentHart();

	//parameter for LOW_SetRiscvMechanism function to manipulate memory access as per mem type

	//added for DCCM and ICCM check in case of wdc-swrev core
#ifdef RISCV
		//updating the riscv mechanism if the address is in the range of DCCM and ICCM values
		if (GDBSERV_NO_ERROR != LOW_SetRiscvMemAccessMechanism(ulAddress))
			return GDBSERV_ERROR;
#endif

	for (ulSize = 1; ulSize < 4 && ulCount >= ulSize * 2 + (ulAddress & ulSize); ulSize *= 2)
	{
		if (ulAddress & ulSize)
		{
			iretValue = ptyRISCVInstance->ptyFunctionTable->write_memory(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulSize, 1, ulAddress, pucData, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
			if (iretValue != GDBSERV_NO_ERROR)
				return GDBSERV_ERROR;
			ulAddress += ulSize;
			ulCount -= ulSize;
			pucData += ulSize;
		}
	}

	for (;ulSize > 0; ulSize /= 2)
	{
		uint32_t ulAligned = ulCount - ulCount % ulSize;
		if (ulAligned > 0)
		{
			iretValue = ptyRISCVInstance->ptyFunctionTable->write_memory(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulSize, ulAligned/ulSize, ulAddress, pucData, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits);
			if (iretValue != GDBSERV_NO_ERROR)
				return GDBSERV_ERROR;
			ulAddress += ulAligned;
			ulCount -= ulAligned;
			pucData += ulAligned;
		}	
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_SupportedBreakpoint
	 Engineer: Nikolay Chokoev
		Input: ulBpType  : breakpoint type
	   Output: int32_t : TRUE if BP type supported, else false
  Description: indicate which BP types are supported
Date           Initials    Description
26-Nov-2007    NCH         Initial
02-Jan-2013    SPT         Added Watchpoint support
****************************************************************************/
int32_t LOW_SupportedBreakpoint(uint32_t ulBpType)
{
	switch (ulBpType)
	{
	case BPTYPE_SOFTWARE:
		return TRUE;
	case BPTYPE_HARDWARE:
		return TRUE;
	case BPTYPE_WRITE:
		return TRUE;
	case BPTYPE_READ:
		return TRUE;
	case BPTYPE_ACCESS:
		return TRUE;
	default:
		return FALSE;
	}
}

/****************************************************************************
	 Function: LOW_DeleteBreakpoint
	 Engineer: Nikolay Chokoev
		Input: ulBpType  : breakpoint type
			   ulAddress : breakpoint address
			   ulLength  : breakpoint length
	   Output: Error Code
  Description: delete a breakpoint
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
05-Aug-2008    NCH         Error Code
01-Jan-2013    SPT         added support for Watchpoint
****************************************************************************/
int32_t LOW_DeleteBreakpoint(uint32_t ulBpType,
	uint32_t ulAddress,
	uint32_t ulLength,
	int32_t iCurrentTapIndex)
{
	int32_t iError, iBpIndex, iResIdx = -1;
	uint32_t ulMatch = 0, ulResUsed = 0;
	unsigned char ucData[4];
	unsigned char ucBpInstruction[4];
	unsigned char ucBpTempInstruction[4];
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE,
			"Delete %s breakpoint at 0x%08lX for length %ld (Core %d).",
			GetBpTypeName(ulBpType), ulAddress, ulLength, iCurrentTapIndex);
	if (!LOW_SupportedBreakpoint(ulBpType))
	{
		//assert(0);
		//return GDBSERV_ERROR;      //lint !e527
		return IDERR_REMOVE_BP_NOT_SUPPORTED;  //lint !e527
	}

	if (ulBpType != BPTYPE_SOFTWARE)
	{
		//Get resource configuration(matchvalue and resources used)  for each H/W breakpoint and watchpoint. 
		GetHWResCfg(ulBpType, &ulAddress, ulLength, &ulMatch, &ulResUsed);
	}
	// check if we know about this BP...
	iBpIndex = GetBpArrayEntry(ulBpType, ulAddress, ulLength, ucBpInstruction, iCurrentTapIndex);
	if (iBpIndex < 0)
	{
		//This scenario can come in case of watchpoints because an unwanted delete break point
		//comes for each execute (c) command for every watchpoint set
		if ((ulBpType == BPTYPE_WRITE) || (ulBpType == BPTYPE_READ) || (ulBpType == BPTYPE_ACCESS))
			return GDBSERV_NO_ERROR;

		// why didn't we find the BP?
		SetupErrorMessage(IDERR_REMOVE_BP_NOT_FOUND);
		return GDBSERV_ERROR;
	}
	if ( ulBpType != BPTYPE_SOFTWARE)
	{
		//Get resource number for breakpoint
		iResIdx = GetHWBPResIdx(ulBpType, ulAddress, iCurrentTapIndex,&ulResUsed);
		if (iResIdx < 0)
		{
			// why didn't we find the BP?
			SetupErrorMessage(IDERR_REMOVE_BP_NOT_FOUND);
			return GDBSERV_ERROR;
		}
	}

	switch (ulBpType)
	{
	default:
	case BPTYPE_SOFTWARE:
		if ((ulLength != 2) && (ulLength != 4))
			return GDBSERV_ERROR;      //lint !e527

		if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress, ulLength, ucData))
			return GDBSERV_ERROR;
		if (ulLength == 2)
		{
			if (LOW_IsBigEndian(iCurrentTapIndex))
			{
				if ((ucData[0] != RISCV_BRK_S_INSTR_B1) ||
					(ucData[1] != RISCV_BRK_S_INSTR_B0))
					return GDBSERV_ERROR;
			}
			else
			{
				if ((ucData[1] != RISCV_BRK_S_INSTR_B1) ||
					(ucData[0] != RISCV_BRK_S_INSTR_B0))
					return GDBSERV_ERROR;
			}
			if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulLength, ucBpInstruction))
				return GDBSERV_ERROR;
			if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress, ulLength, ucBpTempInstruction))
				return GDBSERV_ERROR;
			if ((ucBpInstruction[1] != ucBpTempInstruction[1]) ||
				(ucBpInstruction[0] != ucBpTempInstruction[0]))
			{
				PrintMessage(DEBUG_MESSAGE, "Delete BP Error!\n%02X%02X  !=  %02X%02X\n",
					ucBpInstruction[1], ucBpInstruction[0],
					ucBpTempInstruction[1], ucBpTempInstruction[0]);
				return GDBSERV_ERROR;
			}
		}
		else if (ulLength == 4)
		{
			//TODO: This is only for RISCV700
			if (LOW_IsBigEndian(iCurrentTapIndex))
			{
				if ((ucData[0] != RISCV_BRK_INSTR_B3) ||
					(ucData[1] != RISCV_BRK_INSTR_B2) ||
					(ucData[2] != RISCV_BRK_INSTR_B1) ||
					(ucData[3] != RISCV_BRK_INSTR_B0))
					return GDBSERV_ERROR;
			}
			else
			{
				if ((ucData[3] != RISCV_BRK_INSTR_B3) ||
					(ucData[2] != RISCV_BRK_INSTR_B2) ||
					(ucData[1] != RISCV_BRK_INSTR_B1) ||
					(ucData[0] != RISCV_BRK_INSTR_B0))
					return GDBSERV_ERROR;
			}
			if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulLength, ucBpInstruction))
				return GDBSERV_ERROR;
			if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress, ulLength, ucBpTempInstruction))
				return GDBSERV_ERROR;
			if ((ucBpInstruction[3] != ucBpTempInstruction[3]) ||
				(ucBpInstruction[2] != ucBpTempInstruction[2]) ||
				(ucBpInstruction[1] != ucBpTempInstruction[1]) ||
				(ucBpInstruction[0] != ucBpTempInstruction[0]))
			{
				PrintMessage(DEBUG_MESSAGE, "Delete BP Error!\n%02X%02X%02X%02X  !=  %02X%02X%02X%02X\n",
					ucBpInstruction[3], ucBpInstruction[2], ucBpInstruction[1], ucBpInstruction[0],
					ucBpTempInstruction[3], ucBpTempInstruction[2], ucBpTempInstruction[1], ucBpTempInstruction[0]);
				return GDBSERV_ERROR;
			}
		}
		else
			return GDBSERV_ERROR;
		break;

	case BPTYPE_HARDWARE:
	case BPTYPE_WRITE:
	case BPTYPE_READ:
	case BPTYPE_ACCESS:
		if (ptyRISCVInstance->ptyFunctionTable->removetrigger != NULL)
		{
			if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->removetrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, iResIdx, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits,ulResUsed))
				return GDBSERV_ERROR;
		}
		break;
	}

	if ((iError = RemoveBpArrayEntry((uint32_t)iBpIndex, iCurrentTapIndex)) != 0)
	{
		SetupErrorMessage(iError);
		return GDBSERV_ERROR;
	}

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_SetBreakpoint
	 Engineer: Nikolay Chokoev
		Input: ulBpType  : breakpoint type
			   ulAddress : breakpoint address
			   ulLength  : breakpoint length
			   iCurrentTapIndex : core index
	   Output: int32_t : FALSE if error
  Description: set a breakpoint
Date           Initials    Description
04-Dec-2007    NCH         Initial
01-Jan-2013    SPT         added support for Watchpoint
****************************************************************************/
int32_t LOW_SetBreakpoint(uint32_t ulBpType, uint32_t ulAddress, uint32_t ulLength, int32_t iCurrentTapIndex)
{		int32_t iError, iBpIndex, iResIdx = -1;
	uint32_t ulMatch=0,ulResrequired = 0;
	unsigned char ucData[4];
	unsigned char ucBpInstruction[4];
	unsigned char ucBpTempInstruction[4];
	uint32_t ulThreadId;
	uint32_t ulTriggerOut = 0;
	ulThreadId = LOW_CurrentHart();
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE,
			"Set %s breakpoint at 0x%08lX for length %ld (Core %d).",
			GetBpTypeName(ulBpType), ulAddress, ulLength, iCurrentTapIndex);
	if (ulBpType != BPTYPE_SOFTWARE)
	{
		//Get resource configuration(matchvalue and resources required)  for each H/W breakpoint and watchpoint. 
		GetHWResCfg(ulBpType, &ulAddress, ulLength, &ulMatch, &ulResrequired);
	}
	iBpIndex = GetBpArrayEntryForAddr(ulAddress, ulBpType, iCurrentTapIndex);
	if (iBpIndex > 0)
	{  // there is breakpoint at this address already
		SetupErrorMessage(IDERR_ADD_BP_DUPLICATE);
		return GDBSERV_ERROR;
	}
	if (ulBpType != BPTYPE_SOFTWARE)
	{
		//Get the free resource number
		iResIdx = GetFreeHWBPResIdx(ulBpType, ulResrequired, iCurrentTapIndex);
		if (iResIdx == -1)
		{  // No free resourcs. TODO:see if error handling can be improved.
			return MAX_TRIGGER_LIMIT;
		}
	}

	if (!LOW_SupportedBreakpoint(ulBpType))
		return IDERR_REMOVE_BP_NOT_SUPPORTED;

	switch (ulBpType)
	{
	default:
	case BPTYPE_SOFTWARE:
		if ((ulLength != 2) && (ulLength != 4))
			return GDBSERV_ERROR;      //lint !e527

		if (LOW_ReadMemory(ulAddress, ulLength, ucData))
			return GDBSERV_ERROR;

		switch (ulLength)
		{
		case 2:
			if (LOW_IsBigEndian(iCurrentTapIndex))
			{
				ucBpInstruction[0] = RISCV_BRK_S_INSTR_B1;
				ucBpInstruction[1] = RISCV_BRK_S_INSTR_B0;
				PrintMessage(DEBUG_MESSAGE,
					"Set BP Instr: %02X%02X (SBE)",
					ucBpInstruction[1], ucBpInstruction[0]);
			}
			else
			{
				ucBpInstruction[1] = RISCV_BRK_S_INSTR_B1;
				ucBpInstruction[0] = RISCV_BRK_S_INSTR_B0;
				PrintMessage(DEBUG_MESSAGE,
					"Set BP Instr: %02X%02X (SLE)",
					ucBpInstruction[1], ucBpInstruction[0]);
			}
			break;
		case 4:
			//TODO: This is only for RISCV700
			if (LOW_IsBigEndian(iCurrentTapIndex))
			{
				ucBpInstruction[0] = RISCV_BRK_INSTR_B3;
				ucBpInstruction[1] = RISCV_BRK_INSTR_B2;
				ucBpInstruction[2] = RISCV_BRK_INSTR_B1;
				ucBpInstruction[3] = RISCV_BRK_INSTR_B0;
				PrintMessage(DEBUG_MESSAGE,
					"Set BP Instr: %02X%02X%02X%02X (BE)",
					ucBpInstruction[3], ucBpInstruction[2], ucBpInstruction[1], ucBpInstruction[0]);
			}
			else
			{
				ucBpInstruction[3] = RISCV_BRK_INSTR_B3;
				ucBpInstruction[2] = RISCV_BRK_INSTR_B2;
				ucBpInstruction[1] = RISCV_BRK_INSTR_B1;
				ucBpInstruction[0] = RISCV_BRK_INSTR_B0;
				PrintMessage(DEBUG_MESSAGE,
					"Set BP Instr: %02X%02X%02X%02X (LE)",
					ucBpInstruction[3], ucBpInstruction[2], ucBpInstruction[1], ucBpInstruction[0]);
			}
			break;
		default:
			return GDBSERV_ERROR;
		}
		if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulAddress, ulLength, ucBpInstruction))
			return GDBSERV_ERROR;

		//in case we try to set software breakpoint to ROM
		if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulAddress, ulLength, ucBpTempInstruction))
			return GDBSERV_ERROR;

		switch (ulLength)
		{
		case 2:
			if ((ucBpInstruction[1] != ucBpTempInstruction[1]) ||
				(ucBpInstruction[0] != ucBpTempInstruction[0]))
			{
				PrintMessage(DEBUG_MESSAGE,
					"%02X%02X  !=  %02X%02X\n",
					ucBpInstruction[1],
					ucBpInstruction[0],
					ucBpTempInstruction[1],
					ucBpTempInstruction[0]);
				if (tySI.bDebugOutput)
					PrintMessage(LOG_MESSAGE,
						"%02X%02X  !=  %02X%02X\n",
						ucBpInstruction[1],
						ucBpInstruction[0],
						ucBpTempInstruction[1],
						ucBpTempInstruction[0]);
				return GDBSERV_ERROR;
			}
			break;
		case 4:
			//TODO: This is only for RISCV700
			if ((ucBpInstruction[3] != ucBpTempInstruction[3]) ||
				(ucBpInstruction[2] != ucBpTempInstruction[2]) ||
				(ucBpInstruction[1] != ucBpTempInstruction[1]) ||
				(ucBpInstruction[0] != ucBpTempInstruction[0]))
			{

				PrintMessage(DEBUG_MESSAGE, "%02X%02X%02X%02X  !=  %02X%02X%02X%02X\n",
					ucBpInstruction[3], ucBpInstruction[2], ucBpInstruction[1], ucBpInstruction[0],
					ucBpTempInstruction[3], ucBpTempInstruction[2], ucBpTempInstruction[1], ucBpTempInstruction[0]);
				if (tySI.bDebugOutput)
					PrintMessage(LOG_MESSAGE, "%02X%02X%02X%02X  !=  %02X%02X%02X%02X\n",
						ucBpInstruction[3], ucBpInstruction[2], ucBpInstruction[1], ucBpInstruction[0],
						ucBpTempInstruction[3], ucBpTempInstruction[2], ucBpTempInstruction[1], ucBpTempInstruction[0]);
				return GDBSERV_ERROR;
			}
			break;
		default:
			return GDBSERV_ERROR;
		}
		break;
	case BPTYPE_HARDWARE:
		if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
		{
			if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, ulAddress, iResIdx, &ulTriggerOut, RISCV_HARDWARE_BREAKPOINT, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits,ulLength,ulMatch,ulResrequired))
			{
				if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
					return TRIGGER_DOES_NOT_EXIST;
			}
			else return GDBSERV_ERROR;
		}
		break;
	case BPTYPE_WRITE:
		if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
		{
			if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber,ulThreadId,ulAddress, iResIdx, &ulTriggerOut,RISCV_WATCHPOINT_WRITE, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits,ulLength,ulMatch,ulResrequired))
			{
				if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
					return TRIGGER_DOES_NOT_EXIST;
			}
			else return GDBSERV_ERROR;
		}
		break;
	case BPTYPE_READ:
		if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
		{
			if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, ulAddress, iResIdx, &ulTriggerOut, RISCV_WATCHPOINT_READ, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits,ulLength,ulMatch,ulResrequired))
			{
				if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
					return TRIGGER_DOES_NOT_EXIST;
			}
			else return GDBSERV_ERROR;
		}
		break;
	case BPTYPE_ACCESS:
		if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
		{
			if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, ulAddress, iResIdx, &ulTriggerOut, RISCV_WATCHPOINT_ACCESS, tyThrdInfo[ulThreadId].ptyDiscovery->ulArch, tySI.ulAbits,ulLength,ulMatch,ulResrequired))
			{
				if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
					return TRIGGER_DOES_NOT_EXIST;
			}
			else return GDBSERV_ERROR;
		}
		break;	
	 }	
	if ((iError = AddBpArrayEntry(ulBpType, iResIdx, ulMatch, ulResrequired, ulAddress, ulLength, ucData, iCurrentTapIndex)) != 0)
	{
		SetupErrorMessage(iError);
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}


/****************************************************************************
	 Function: LOW_IsSWBreak
	 Engineer: Nikolay Chokoev
		Input: void
	   Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
int32_t LOW_IsSWBreak(uint32_t uiPCValue,
	int32_t iCurrentTapIndex)
{
	uint32_t uiIndex;

	for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
	{
		if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress == uiPCValue)
		{
			if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_SOFTWARE)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

/****************************************************************************
	 Function: LOW_PostBreakAction
	 Engineer: Nikolay Chokoev
		Input: iCurrentTapIndex : current core index
	   Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
int32_t LOW_PostBreakAction(int32_t iCurrentTapIndex)
{
		uint32_t  *pulPCValue;
	//uint32_t  ulPCTempValue;

		uint32_t ulThredId;
		ulThredId = LOW_CurrentHart();

	pulPCValue = (uint32_t *)calloc((uint32_t)(tyThrdInfo[ulThredId].ptyDiscovery->ulArch / 32), sizeof(uint32_t));
	
	//set current core as not-flushed data cache
	tySI.tyProcConfig.Processor._RISCV.bDCacheFlush[iCurrentTapIndex] = FALSE;
	//set current core as not-invalidated data cache
	tySI.tyProcConfig.Processor._RISCV.bDCacheInval[iCurrentTapIndex] = FALSE;
	//set current core as not-flushed instruction cache
	tySI.tyProcConfig.Processor._RISCV.bICacheFlush[iCurrentTapIndex] = FALSE;

	return TRUE;
}

/****************************************************************************
	 Function: LOW_RestoreFPS1
	 Engineer: Rejeesh Shaji Babu
		Input: none
	   Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
02-08-2019      RSB         Initial
****************************************************************************/
int32_t LOW_RestoreFPS1(void)
{
	uint32_t ulThreadId = LOW_CurrentHart();

	if (tyThrdInfo[ulThreadId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS) // Required only for program buffer case
	{
		if (GDBSERV_NO_ERROR != LOW_WriteRegister(REG_S0, &tyThrdInfo[ulThreadId].ptyDiscovery->ulFpValue))
			return GDBSERV_ERROR;
		if (GDBSERV_NO_ERROR != LOW_WriteRegister(REG_S1, &tyThrdInfo[ulThreadId].ptyDiscovery->ulS1Value))
			return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_BackupFPS1
	 Engineer: Rejeesh Shaji Babu
		Input: none
	   Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
02-08-2019      RSB         Initial
****************************************************************************/
int32_t LOW_BackupFPS1(void)
{
	uint32_t ulThreadId = LOW_CurrentHart();

	if (tyThrdInfo[ulThreadId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS)
	{
		if (GDBSERV_NO_ERROR != LOW_ReadRegister(REG_S0, &tyThrdInfo[ulThreadId].ptyDiscovery->ulFpValue))
			return GDBSERV_ERROR;

		if (GDBSERV_NO_ERROR != LOW_ReadRegister(REG_S1, &tyThrdInfo[ulThreadId].ptyDiscovery->ulS1Value))
			return GDBSERV_ERROR;
	}

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Continue
	 Engineer: Nikolay Chokoev
		Input: *ptyStopSignal : storage for cause of break
	   Output: int32_t : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_Continue(TyStopSignal *ptyStopSignal,
	int32_t iCurrentCore)
{
	ptyStopSignal = ptyStopSignal;

	PrintMessage(DEBUG_MESSAGE, "Continue.");
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Continue.");

	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();

	if (ptyRISCVInstance->ptyFunctionTable->Run != NULL)
	{
		if (GDBSERV_NO_ERROR != LOW_RestoreFPS1())
			return GDBSERV_ERROR;

		if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->Run(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, tySI.ulAbits))
			return GDBSERV_ERROR;
		//Set the flag...
		tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentCore] = TRUE;
	}
	else
	{
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Exexute
	 Engineer: Nikolay Chokoev
		Input: void
	   Output: int32_t : FALSE if error
  Description: start execution of target for download without listening to server
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_Execute(char* pszDownloadFileName)
{
	int32_t          bExecuting = FALSE;
	pszDownloadFileName = pszDownloadFileName;

	PrintMessage(DEBUG_MESSAGE, "Execute.");
	uint32_t ulHartId = 0;
	
	// Now we must wait until the processor has stopped executing or the user hits any key
	for (;;)
	{
		if (GDBSERV_NO_ERROR != LOW_IsProcessorExecuting(&bExecuting, 0))
			return GDBSERV_ERROR;
		
		if (!bExecuting)
		{
			PrintMessage(DEBUG_MESSAGE,
				"Execution of %s terminated normally.",
				pszDownloadFileName);
			break;
		}

		if (KeyHit())
		{
			// while executing we treat any key press from the user as a halt...
			PrintMessage(INFO_MESSAGE,
				"Key press detected, halting execution of %s.",
				pszDownloadFileName);
			if (GDBSERV_NO_ERROR != LOW_Halt(tySI.ulCurrentTapNumber))
				return GDBSERV_ERROR;
			break;
		}
		MsSleep(100);
	}

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Step
	 Engineer: Nikolay Chokoev
		Input: *ptyStopSignal : storage for cause of break
	   Output: int32_t : FALSE if error
  Description: step
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
int32_t LOW_Step(TyStopSignal *ptyStopSignal)
{

	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Step.");
	
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();

	if (ptyRISCVInstance->ptyFunctionTable->step != NULL)
	{
		if (GDBSERV_NO_ERROR != LOW_RestoreFPS1())
			return GDBSERV_ERROR;

		if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->step(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits))
		{
			return GDBSERV_ERROR;
		}

		if (GDBSERV_NO_ERROR != LOW_BackupFPS1())
			return GDBSERV_ERROR;
	}
	else
	{
		return GDBSERV_ERROR;
	}

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_addTrigger
	 Engineer: Ashutosh Garg
		Input: ulTriggerAddress : Trigger address
			   ulTriggerNumber  : Trigger number
			   ulOut            : Result
	   Output: int32_t : FALSE if error
  Description: Add Trigger
Date           Initials    Description
05-Jan-2019      AG         Initial
****************************************************************************/
int32_t LOW_addTrigger(uint32_t ulTriggerAddress, uint32_t ulTriggerNumber,uint32_t ulLength,uint32_t ulMatch,uint32_t ulResrequired, uint32_t *ulOut,uint32_t ulType)
{

	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulTriggerAddress, ulTriggerNumber, ulOut,ulType, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits, ulLength, ulMatch,ulResrequired))
	{
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_RemoveTrigger
	 Engineer: Ashutosh Garg
		Input: 
			   ulTriggerNumber  : Trigger number
			   
	   Output: int32_t : FALSE if error
  Description: Remove Trigger
Date           Initials    Description
05-Jan-2019      AG         Initial
****************************************************************************/
int32_t LOW_RemoveTrigger(uint32_t ulTriggerNum ,uint32_t ulResUsed)
{
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();
	if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->removetrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulTriggerNum, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits, ulResUsed))
	{
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Halt
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t : FALSE if error
  Description: halt execution of target
Date           Initials    Description
28-Nov-2018    AG         Initial

****************************************************************************/
int32_t LOW_Halt(int32_t iCurrentTapIndex)
{
	uint32_t ulThreadId;
	ulThreadId = LOW_CurrentHart();
	
	if (ptyRISCVInstance->ptyFunctionTable->Halt != NULL)
	{
		switch (ptyRISCVInstance->ptyFunctionTable->Halt(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThreadId, tySI.ulAbits))
		{
		case GDBSERV_NO_ERROR:
			tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentTapIndex] = FALSE;

			if (GDBSERV_NO_ERROR != LOW_BackupFPS1())
				return GDBSERV_ERROR;

			break;
		case GDB_PWDOFF:
			if (tySI.bDebugOutput)
				PrintMessage(LOG_MESSAGE, "Error occurred halting target (target is powered-off).");
			return GDB_PWDOFF;
		case GDBSERV_ERROR:
		default:
			if (tySI.bDebugOutput)
				PrintMessage(LOG_MESSAGE, "Error occurred halting target.");
			return GDBSERV_ERROR;
		}
	}
	else
		return GDBSERV_ERROR;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_CheckForWatchpoints
	 Engineer: Harvinder Singh
		Input: iCurrentTapIndex     :current core index
	   Output: TRUE     :on success
	            FALSE   :if no watchpoint
  Description: Check for HW WP in tyCoreBPArray list
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
int32_t LOW_CheckForWatchpoints(int32_t iCurrentTapIndex)
{
	uint32_t uiIndex;
	for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
	{
		if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_WRITE ||
			tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_READ ||
			tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_ACCESS)
		{
			return TRUE;
		}
	}
	return FALSE;
}

/****************************************************************************
	 Function: LOW_RemoveWatchpointInternal
	 Engineer: Harvinder Singh
		Input: ulThreadId             :thread id
			   iCurrentTapIndex     :current core index
	   Output: None
  Description: Remove watchpoints from target internally irrespective of GDB packets
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
int32_t LOW_RemoveWatchpointInternal(int32_t ulThredId,int32_t iCurrentTapIndex)
{
	uint32_t uiIndex, i = 0;
	for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
	{
		if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_WRITE ||
			tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_READ ||
			tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_ACCESS)
		{
			if (ptyRISCVInstance->ptyFunctionTable->removetrigger != NULL)
			{
				if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->removetrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx,
					tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits, tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed))
					return GDBSERV_ERROR;
			}
		}
	}
	return GDBSERV_NO_ERROR;
}
/****************************************************************************
	 Function: LOW_AddWatchpointInternal
	 Engineer: Harvinder Singh
		Input: ulThreadId             :thread id
			   iCurrentTapIndex     :current core index
	   Output: None
  Description: Add watchpoints to target internally irrespective of GDB packets
Date           Initials    Description
04-Dec-2007    NCH         Initial
****************************************************************************/
int32_t LOW_AddWatchpointInternal(int32_t ulThredId,int32_t iCurrentTapIndex)
{
	uint32_t uiIndex, i = 0;
	uint32_t ulAddress;
	for (uiIndex = 0; uiIndex < uiBpCount[iCurrentTapIndex]; uiIndex++)
	{
		if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_WRITE)
		{
			ulAddress = tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress;
			if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
			{
				uint32_t ulTriggerOut;
				if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulAddress,tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx,
					&ulTriggerOut, RISCV_WATCHPOINT_WRITE, tyThrdInfo[ulThredId].ptyDiscovery->ulArch,tySI.ulAbits,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulLength,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulMatchValue, 
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed))
				{
					if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
						return TRIGGER_DOES_NOT_EXIST;
				}
				else return GDBSERV_ERROR;
			}
		}
		else if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_READ)
		{
			ulAddress = tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress;
			if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
			{
				uint32_t ulTriggerOut = 0;
				if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulAddress, tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx,
					&ulTriggerOut, RISCV_WATCHPOINT_READ, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulLength,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulMatchValue,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed))
				{
					if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
						return TRIGGER_DOES_NOT_EXIST;
				}
				else return GDBSERV_ERROR;
			}
		}
		else if (tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulBpType == BPTYPE_ACCESS)
		{
			ulAddress = tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulAddress;
			if (ptyRISCVInstance->ptyFunctionTable->addtrigger != NULL)
			{
				uint32_t ulTriggerOut;
				if (GDBSERV_NO_ERROR == ptyRISCVInstance->ptyFunctionTable->addtrigger(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, ulAddress, tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResIdx,
					&ulTriggerOut, RISCV_WATCHPOINT_ACCESS, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulLength,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulMatchValue,
					tyCoreBPArray[iCurrentTapIndex].ptyBpArray[uiIndex].ulResUsed))
				{
					if (ulTriggerOut == TRIGGER_DOES_NOT_EXIST)
						return TRIGGER_DOES_NOT_EXIST;
				}
				else return GDBSERV_ERROR;
			}
		}
	}
	return GDBSERV_NO_ERROR;
}
int32_t LOW_vContPacket(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentTapIndex)
{
	
	assert(uiOutputSize > 0);
	NOREF(iCurrentTapIndex);

	PrintMessage(DEBUG_MESSAGE, " LOW_vContPacket");
	/*Check if GDB requested list of actions supported by the `vCont' packet*/
	if (pcInput[5] == '?')
	{
		strcat(pcOutput, "vCont;c;C;s;S;t");
	}
	else
	{
		if (pcInput[5] == ';')
		{
				uint32_t ulThredId;
				uint32_t i,addWatchFlag = 0;
				ulThredId = LOW_CurrentHart();
				switch (pcInput[6]) {
				case 'C':
				case 'c':
					//before continue the target we have to check for watchpoints already set if any  will remove them ,do single step than again will
					//add removed watchpoints.
					//check for watchpoints in tyCoreBPArray 
					if ((LOW_CheckForWatchpoints(iCurrentTapIndex)))
					{
						//remove watchpoints internally from target 
						if (GDBSERV_NO_ERROR != LOW_RemoveWatchpointInternal(ulThredId,iCurrentTapIndex))
							return GDBSERV_ERROR;

						if (GDBSERV_NO_ERROR != LOW_RestoreFPS1())
							return GDBSERV_ERROR;

						if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->step(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits))
						{
							return GDBSERV_ERROR;
						}
						else
						{
							if (GDBSERV_NO_ERROR != LOW_BackupFPS1())
								return GDBSERV_ERROR;
						}
						//add watchpoints internally to target 
						if (GDBSERV_NO_ERROR != LOW_AddWatchpointInternal(ulThredId, iCurrentTapIndex))
							return GDBSERV_ERROR;
					}

					if (GDBSERV_NO_ERROR != LOW_RestoreFPS1())
						return GDBSERV_ERROR;

					if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->Run(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, tySI.ulAbits))
					{
						return GDBSERV_ERROR;
					}
					else
					{
						tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iCurrentTapIndex] = TRUE;
						//strcat(pcOutput, "OK"); //Only needed for non-stop mode
						return GDBSERV_TARGET_RUNNING;
					}
					break;
				case 'S':
				case 's':
					ulThredId = LOW_GetThreadId(pcInput);
					for (i = 0; i < tySI.ulNumberOfHarts; i++)
					{
						tyThrdInfo[i].ulIsActiveThread = 0;
						tyThrdInfo[i].ulCurrentThread = 0;
					}
					tyThrdInfo[ulThredId].ulIsActiveThread = 1;
					tyThrdInfo[ulThredId].ulCurrentThread = 1;

					if (GDBSERV_NO_ERROR != LOW_RestoreFPS1())
						return GDBSERV_ERROR;

					//before doing the step we have to check for watchpoints already set if any  will remove them ,do single step than again 
					//add removed watchpoints
					//check for watchpoints in tyCoreBPArray 
					if (LOW_CheckForWatchpoints(iCurrentTapIndex))
					{
						//remove watchpoints internally from target 
						if (GDBSERV_NO_ERROR != LOW_RemoveWatchpointInternal(ulThredId, iCurrentTapIndex))
							return GDBSERV_ERROR;
						//set this flag to add watchpoints to target that we have removed just now
						addWatchFlag = 1;
					}

					if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->step(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulThredId, tyThrdInfo[ulThredId].ptyDiscovery->ulArch, tySI.ulAbits))
					{
						return GDBSERV_ERROR;
					}
					else
					{
						char th[5];
						strncpy_s(pcOutput,sizeof(pcOutput), "T05", 4);
						if (ulThredId / 10 == 0)
						{
							strcat(pcOutput, "thread:0");
						}
						else
						{
							strcat(pcOutput, "thread:");
						}
						sprintf(th, "%lu;", ulThredId + 1);
						strcat(pcOutput, th);

						if (GDBSERV_NO_ERROR != LOW_BackupFPS1())
							return GDBSERV_ERROR;
					}
					//here check for flag if set add watchpoints
					if (addWatchFlag)
					{
						//add watchpoints internally to target 
						if (GDBSERV_NO_ERROR != LOW_AddWatchpointInternal(ulThredId, iCurrentTapIndex))
							return GDBSERV_ERROR;
					}
					break;
				default:
					return GDBSERV_ERROR;
				}
			
		}
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Discovery
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t : FALSE if error
  Description: Discovery of enumerated harts
Date           Initials    Description
28-Nov-2018    AG         Initial

****************************************************************************/
int32_t LOW_Discovery(void)
{
	uint32_t i;
	uint32_t *pulDiscoveryData;

	for (i = 0; i < tySI.ulNumberOfHarts; i++)
	{
		if (ptyRISCVInstance->ptyFunctionTable->Halt != NULL)
		{
			switch (ptyRISCVInstance->ptyFunctionTable->Halt(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, i, tySI.ulAbits))
			{
			case GDBSERV_NO_ERROR:
				pulDiscoveryData = (uint32_t *)malloc(RISCV_DATA_DISCOVERY_SIZE * sizeof(uint32_t));
				if (pulDiscoveryData == NULL)
				{
					if (tySI.bDebugOutput)
						PrintMessage(LOG_MESSAGE, GDBSERV_ERR_MALLOC);
					return GDBSERV_ERROR;
				}
				tyThrdInfo[i].ptyDiscovery = (struct TyDiscoveryData *)malloc(sizeof(struct TyDiscoveryData));
				if (tyThrdInfo[i].ptyDiscovery == NULL)
				{
					if (tySI.bDebugOutput)
						PrintMessage(LOG_MESSAGE, GDBSERV_ERR_MALLOC);
					return GDBSERV_ERROR;
				}
				if (ptyRISCVInstance->ptyFunctionTable->Discovery != NULL)
				{
					switch (ptyRISCVInstance->ptyFunctionTable->Discovery(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, pulDiscoveryData, i))
					{
					case GDBSERV_NO_ERROR:
						if (tyThrdInfo[i].ptyDiscovery != NULL)
						{
							tyThrdInfo[i].ulThreadId = i + 1;
							tyThrdInfo[i].ulCurrentThread = 0;
							tyThrdInfo[i].ulIsActiveThread = 0;
							tyThrdInfo[i].ptyDiscovery->ulFence_iSupport = pulDiscoveryData[0];
							tyThrdInfo[i].ptyDiscovery->ulIdleOut = pulDiscoveryData[1];
							tyThrdInfo[i].ptyDiscovery->ulVersion = pulDiscoveryData[2];
							tyThrdInfo[i].ptyDiscovery->ulAbsCommSupport = pulDiscoveryData[3];
							tyThrdInfo[i].ptyDiscovery->ulProgBuffRegSupp = pulDiscoveryData[4];
							tyThrdInfo[i].ptyDiscovery->ulMemoryMechSupport = pulDiscoveryData[5];
							tyThrdInfo[i].ptyDiscovery->ulArch = pulDiscoveryData[6];
							tyThrdInfo[i].ptyDiscovery->ulAbsCSRSupp = pulDiscoveryData[7];
							tyThrdInfo[i].ptyDiscovery->ulTriggers = pulDiscoveryData[8];

							if (!tySI.bIsVegaBoard)
							{
								if (tyThrdInfo[i].ptyDiscovery->ulArch == 32)
								{
									tyThrdInfo[i].ptyDiscovery->ulFloatingProc = pulDiscoveryData[9];
									if ((tyThrdInfo[i].ptyDiscovery->ulFloatingProc >> 3) & 1)
										tyThrdInfo[i].ptyDiscovery->ulFloatingProc = 1;
									else
										tyThrdInfo[i].ptyDiscovery->ulFloatingProc = 0;
								}
								else if (tyThrdInfo[i].ptyDiscovery->ulArch == 64)
								{
									tyThrdInfo[i].ptyDiscovery->ulFloatingProc = pulDiscoveryData[9];
									if ((tyThrdInfo[i].ptyDiscovery->ulFloatingProc >> 3) & 1)
										tyThrdInfo[i].ptyDiscovery->ulFloatingProc = 1;
									else
										tyThrdInfo[i].ptyDiscovery->ulFloatingProc = 0;
								}
							}
							if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->read_a_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, i, &tyThrdInfo[i].ptyDiscovery->ulS1Value, 9, tyThrdInfo[i].ptyDiscovery->ulArch, tySI.ulAbits))
							{
								if (tySI.bDebugOutput)
									PrintMessage(LOG_MESSAGE, "Error occurred reading reg 0x09.");
								return GDBSERV_ERROR;
							}
							if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->read_a_reg(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, i, &tyThrdInfo[i].ptyDiscovery->ulFpValue, 8, tyThrdInfo[i].ptyDiscovery->ulArch, tySI.ulAbits))
							{
								if (tySI.bDebugOutput)
									PrintMessage(LOG_MESSAGE, "Error occurred reading reg 0x08.");
								return GDBSERV_ERROR;
							}
							free(pulDiscoveryData);
						}
						else
							return GDBSERV_ERROR;
						break;
						case GDB_PWDOFF:
							if (tySI.bDebugOutput)
								PrintMessage(LOG_MESSAGE, "Error occurred during discovery (target is powered-off).");
							return GDB_PWDOFF;
						case GDBSERV_ERROR:
						default:
							if (tySI.bDebugOutput)
								PrintMessage(LOG_MESSAGE, "Error occurred during discovery.");
							return GDBSERV_ERROR;
						}
					}
				break;
			case GDB_PWDOFF:
				if (tySI.bDebugOutput)
					PrintMessage(LOG_MESSAGE, "Error occurred attempting to halt the target during discovery(target is powered-off).");
				return GDBSERV_ERROR;
			case GDBSERV_ERROR:
			default:
				if (tySI.bDebugOutput)
					PrintMessage(LOG_MESSAGE, "Error occurred attempting to halt the target during discovery.");
				return GDBSERV_ERROR;

			}
		}
		else
			return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Discover_Harts
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t : FALSE if error
  Description: Harts enumeration
Date           Initials    Description
28-Nov-2018    AG         Initial

****************************************************************************/
int32_t LOW_Discover_Harts() {

	uint32_t *pulNumberofHarts;
	pulNumberofHarts = (uint32_t *)malloc(2 * sizeof(uint32_t));

	if (pulNumberofHarts != NULL)
	{
		if (ptyRISCVInstance->ptyFunctionTable->Discover_Harts != NULL)
		{
			switch (ptyRISCVInstance->ptyFunctionTable->Discover_Harts(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, pulNumberofHarts))
			{
			case GDB_PWDOFF:
				if (tySI.bDebugOutput)
					PrintMessage(LOG_MESSAGE, "Error occurred during harts enumeration (target is powered-off).");
				return GDB_PWDOFF;
				break;
			case GDBSERV_NO_ERROR:
                if (pulNumberofHarts[0] < 1) {
                    PrintMessage(LOG_MESSAGE, "Error occurred during harts enumeration (no harts found).");
                    return GDBSERV_ERROR;
                }
				tyThrdInfo = (struct _Tythread *)malloc(pulNumberofHarts[0] * sizeof(struct _Tythread));
				if (tyThrdInfo == NULL)
				{
					if (tySI.bDebugOutput)
						PrintMessage(LOG_MESSAGE, GDBSERV_ERR_MALLOC);
					return GDBSERV_ERROR;
				}
				tySI.ulNumberOfHarts = pulNumberofHarts[0];
				tySI.ulAbits = pulNumberofHarts[1];
				for (unsigned int i = 0; i < tySI.ulNumberOfHarts; i++)
				{
					tyThrdInfo[i].uiCurrentMemMechanism = UNDEFINED_MEMORY_ACCESS;
					tyThrdInfo[i].ulCurrentThread = 0;
					tyThrdInfo[i].ulIsActiveThread = 0;
					tyThrdInfo[i].ulThreadId = 0;
				}
				break;
			case GDBSERV_ERROR:
			default:
				if (tySI.bDebugOutput)
					PrintMessage(LOG_MESSAGE, "Error occurred during harts discovery.");
				return GDBSERV_ERROR;
			}
		}
		else
			return GDBSERV_ERROR;
	}
	else
		return GDBSERV_ERROR;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_CurrentHart
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t : FALSE if error
  Description: return the current selected hart
Date           Initials    Description
28-Nov-2018    AG         Initial

****************************************************************************/
int32_t LOW_CurrentHart()
{
	uint32_t i;
	for (i = 0;i < tySI.ulNumberOfHarts ; i++)
	{
		if (tyThrdInfo[i].ulCurrentThread == 1)
			return i;
	}
	return -1;
}

/****************************************************************************
	 Function: LOW_TriggerInfo
	 Engineer: Ashutosh Garg
		Input: none
	   Output: int32_t : FALSE if error
  Description: return the number of triggers 
Date           Initials    Description
28-Nov-2018    AG         Initial

****************************************************************************/
int32_t LOW_TriggerInfo(void)
{
	/*uint32_t *ulTriggerInfo;
	ulTriggerInfo = (uint32_t *)malloc(2 * sizeof(uint32_t));
	if (ptyRISCVInstance->ptyFunctionTable->TriggerInfo(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber,0,ulTriggerInfo,pDdata->ulArch))
	{
		pDdata->ulTriggers = ulTriggerInfo[0];
		pDdata->ulArch = ulTriggerInfo[1];
		free(ulTriggerInfo);
		return TRUE;
	}
	else
	{
		free(ulTriggerInfo);
		return FALSE;
	}*/
	return TRUE;
}

/****************************************************************************
	 Function: LOW_RiscvMechanism
	 Engineer: Ashutosh Garg
		Input: ulMechanism - for system bus
	   Output: int32_t : FALSE if error
  Description: riscv setup
Date           Initials    Description
28-Nov-2018    AG         Initial

****************************************************************************/
int32_t LOW_RiscvMechanism(uint32_t ulMechanism)
{
	uint32_t ulRegSup = 0;
	uint32_t ulMemSup = 0; 
	uint32_t ulCurMemMech;
	//for reg Support
	uint32_t ulThredId;
	ulThredId = LOW_CurrentHart();

	//store the previous memory mechanism
	ulCurMemMech = tyThrdInfo[ulThredId].uiCurrentMemMechanism;

	if (tyThrdInfo[ulThredId].ptyDiscovery->ulAbsCommSupport >= 1)
	{
		ulRegSup = 1;
	}
	//for mem support
	if (ulMechanism == UNDEFINED_MEMORY_ACCESS)
	{
		if (tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & SYSTEM_BUS_MEMORY_ACCESS)
		{
			ulMemSup = tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & SYSTEM_BUS_MEMORY_ACCESS;
			tyThrdInfo[ulThredId].uiCurrentMemMechanism = SYSTEM_BUS_MEMORY_ACCESS; //store memory access mechanism
		}
		else if (tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS)
		{
			ulMemSup = tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS;
			tyThrdInfo[ulThredId].uiCurrentMemMechanism = PROGRAM_BUFFER_MEMORY_ACCESS;
		}
		else
		{
			ulMemSup = tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & ABSTRACT_COMMAND_MEMORY_ACCESS;
			tyThrdInfo[ulThredId].uiCurrentMemMechanism = ABSTRACT_COMMAND_MEMORY_ACCESS;
		}			
	}
	else
	{
		//in case of if machanism is system bus pass 1. 
		if(ulMechanism == SYSTEM_BUS_MEMORY_ACCESS)
			ulMemSup = tyThrdInfo[ulThredId].ptyDiscovery->ulMemoryMechSupport & SYSTEM_BUS_MEMORY_ACCESS;
		else
			ulMemSup = ulMechanism; //in the case of program buffer and abstract access we need to pass 2 and 3 values respactively

		tyThrdInfo[ulThredId].uiCurrentMemMechanism = ulMechanism;
	}

	//checking is there a need to change the memory mechanism
	if (ulCurMemMech != tyThrdInfo[ulThredId].uiCurrentMemMechanism)
	{
		if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->riscv_mechanism(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, ulRegSup, ulMemSup, tyThrdInfo[ulThredId].ptyDiscovery->ulAbsCSRSupp, tyThrdInfo[ulThredId].ptyDiscovery->ulFloatingProc, tyThrdInfo[ulThredId].ptyDiscovery->ulFence_iSupport))
		{
			return GDBSERV_ERROR;
		}
	}
	return GDBSERV_NO_ERROR;
}
/****************************************************************************
	 Function: LOW_ResetTarget
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: Error Code
  Description: reset target
Date           Initials    Description
29-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ResetTarget(void)
{
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Hwreset.");

	if (ptyRISCVInstance->ptyFunctionTable->process_property != NULL)
	{
		if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance, "reset_target", "1"))
		{
			return GDBSERV_ERROR;
		}
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_GetDWFWVersions
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: Error Code
  Description: get diskware firmware version information
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_GetDWFWVersions(char *pszDiskWare1,
	char *pszFirmWare,
	char *pszFpgaWare1,
	char *pszFpgaWare2,
	char *pszFpgaWare3,
	char *pszFpgaWare4)
{
	pszDiskWare1[0] = '\0';
	pszFirmWare[0] = '\0';
	pszFpgaWare1[0] = '\0';
	pszFpgaWare2[0] = '\0';
	pszFpgaWare3[0] = '\0';
	pszFpgaWare4[0] = '\0';

	my_printf_func = if_filt_printf;
	if (ptyRISCVInstance->ptyFunctionTable->process_property != NULL)
	{
		if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance, "probe_info", "1"))
		{
			return GDBSERV_ERROR;
		}
	}
	my_printf_func = if_out_printf;
	strncpy(pszDiskWare1, tySI.tyProcConfig.Processor._RISCV.szDiskwareVer, _MAX_PATH);
	strncpy(pszFirmWare, tySI.tyProcConfig.Processor._RISCV.szFirmwareVer, _MAX_PATH);
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_IsBigEndian
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
28-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_IsBigEndian(int32_t iCoreIndex)
{
	return tySI.tyProcConfig.Processor._RISCV.bProcIsBigEndian[iCoreIndex];
}

/****************************************************************************
	 Function: LOW_ConnectJcon
	 Engineer: Nikolay Chokoev
		Input: int32_t bInitialJtagAccess : enable using initial jtag access (MIPS specific)
	   Output: int32_t : FALSE if error
  Description: Connect to target (use for JTAG console)
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_ConnectJcon(int32_t bInitialJtagAccess)
{
	bInitialJtagAccess = bInitialJtagAccess;
	return LOW_LoadLibrary();
}

/****************************************************************************
	 Function: LOW_DisconnectJcon
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t : FALSE if error
  Description: Disconnect from target (use for JTAG console)
Date           Initials    Description
26-Nov-2007    NCH          Initial
****************************************************************************/
int32_t LOW_DisconnectJcon(void)
{
	return LOW_Disconnect();
}


/****************************************************************************
	 Function: LOW_ScanIR
	 Engineer: Nikolay Chokoev
		Input: uint32_t * pulDataOut : array of uint32_t to be shifted into IR
			   uint32_t * pulDataIn  : array of uint32_t for data to be shifted from IR
			   uint32_t ulDataLength : number of bits shifted into/from IR
	   Output: int32_t : Error Code
  Description: shift specified data into IR and read data shifted out
Date           Initials    Description
10-Dec-2007    NCH          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ScanIR(uint32_t *pulDataIn,
	uint32_t *pulDataOut,
	uint32_t ulDataLength)
{
	assert(pulDataOut != NULL);
	assert(pulDataIn != NULL);

	// call a function in DLL
	if (pfAshScanIR != NULL)
	{
		if (pfAshScanIR(ptyRISCVInstance, ulDataLength, pulDataIn, pulDataOut) == GDBSERV_NO_ERROR)
		{
			return GDBSERV_NO_ERROR;
		}
		return GDBSERV_ERROR;
	}
	else
		return GDBSERV_ERROR;
}

/****************************************************************************
	 Function: LOW_ScanDR
	 Engineer: Nikolay Chokoev
		Input: uint32_t * pulDataOut : array of uint32_t to be shifted into DR
			   uint32_t * pulDataIn  : array of uint32_t for data to be shifted from DR
			   uint32_t ulDataLength : number of bits shifted into/from DR
	   Output: int32_t : Error code
  Description: shift specified data into DR and read data shifted out
Date           Initials    Description
10-Dec-2007    NCH          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ScanDR(uint32_t *pulDataIn,
	uint32_t *pulDataOut,
	uint32_t ulDataLength)
{
	assert(pulDataOut != NULL);
	assert(pulDataIn != NULL);

	// call a function in DLL
	if (pfAshScanDR != NULL)
	{
		if (pfAshScanDR(ptyRISCVInstance, ulDataLength, pulDataIn, pulDataOut) == GDBSERV_NO_ERROR)
		{
			return GDBSERV_NO_ERROR;
		}
		return GDBSERV_ERROR;
	}
	else
		return GDBSERV_ERROR;
}

/****************************************************************************
	 Function: LOW_ResetTAP
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t : FALSE if error
  Description: reset Jtag TAP
Date           Initials    Description
26-Nov-2007    NCH         Initial
09-Sep-2009    RSB         TAP Reset Done
****************************************************************************/
int32_t LOW_ResetTAP(void)
{
	if (pfAshTAPReset != NULL)
	{
		if (pfAshTAPReset(ptyRISCVInstance) == GDBSERV_NO_ERROR)
		{
			return GDBSERV_NO_ERROR;
		}
		return GDBSERV_ERROR;
	}
	else
		return GDBSERV_ERROR;
}

/****************************************************************************
	 Function: LOW_ReadRegisterByRegDef
	 Engineer: Nikolay Chokoev
		Input: TyRegDef *ptyRegDef - pointer to register definition struct
			   uint32_t *pulRegValue - storage for register value
	   Output: int32_t - error code
  Description: Read a register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef,
	uint32_t *pulRegValue)
{
	int32_t iError;
	uint32_t ulValue = 0;
	assert(ptyRegDef != NULL);
	assert(pulRegValue != NULL);
	if ((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulValue)) != 0)
	{
		if (tySI.bDebugOutput)
			PrintMessage(LOG_MESSAGE,
				"Read register %s ...",
				GetRegDefDescription(ptyRegDef));
		SetupErrorMessage(iError);
		return GDBSERV_ERROR;
	}
	*pulRegValue = ExtractRegBits(ulValue, ptyRegDef);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE,
			"Read register %s value=0x%08lX.",
			GetRegDefDescription(ptyRegDef), *pulRegValue);
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_WriteRegisterByRegDef
	 Engineer: Nikolay Chokoev
		Input: TyRegDef *ptyRegDef - pointer to register definition struct
			   uint32_t ulRegValue - register value to write
	   Output: int32_t - error code
  Description: Write a register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int32_t LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef,
	uint32_t ulRegValue)
{
	int32_t iError;
	uint32_t ulOldValue;
	assert(ptyRegDef != NULL);
	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE,
			"Write register %s value=0x%08lX.",
			GetRegDefDescription(ptyRegDef), ulRegValue);
	if (ptyRegDef->ubBitSize < 32)
	{
		if ((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulOldValue)) != 0)
		{
			SetupErrorMessage(iError);
			return 0;
		}
		ulRegValue = MergeRegBits(ulOldValue, ulRegValue, ptyRegDef);
	}
	if ((iError = WriteFullRegisterByRegDef(ptyRegDef, ulRegValue)) != 0)
	{
		SetupErrorMessage(iError);
		return 0;
	}
	return 1;
}

/****************************************************************************
	 Function: LOW_AddCoreRegDefArrayEntries
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t - error code
  Description: Add register definitions for core registers
Date           Initials    Description
26-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to add SetUpRISCVRegs function
****************************************************************************/
int32_t LOW_AddCoreRegDefArrayEntries(void)
{
	uint32_t ulIndex;
	int32_t iError;

	//Setting up RISCV registers for the first time 
	if ((iError = SetUpRISCVRegs()) != GDBSERV_NO_ERROR)
		return iError;

	//PrintMessage(INFO_MESSAGE, "LOW_AddCoreRegDefArrayEntries current Core = %lx.", g_ulCurrentCoreNumber);

	// add all RISCV registers
	for (ulIndex = 0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
	{
		if ((iError = CFG_AddRegDefArrayEntry(g_ptyRISCVReg[ulIndex].pszRegName, g_ptyRISCVReg[ulIndex].ulAddrIndex, MEM_REG, 4, 0, 32, -1, 1)) != GDBSERV_NO_ERROR)
			return iError;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_IndexCommonRegDefArrayEntries
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t - error code
  Description: Index common registers in reg definition array for fast access
Date           Initials    Description
26-Nov-2007    NCH         Initial
12-Oct-2012    SV          Modified to use tySI.iNumberOfRegisters as the limit
****************************************************************************/
int32_t LOW_IndexCommonRegDefArrayEntries(void)
{
	uint32_t ulIndex;
	uint32_t  uiRegDefIndex;
	TyRegDef *ptyRegDef;
	TyRegDef *ptyLastFullRegDef = NULL;

	//PrintMessage(INFO_MESSAGE, " LOW_IndexCommonRegDefArrayEntries current Core = %lx.", g_ulCurrentCoreNumber);

	for (uiRegDefIndex = 0; uiRegDefIndex < tySI.uiRegDefArrayEntryCount; uiRegDefIndex++)
	{
		ptyRegDef = &tySI.ptyRegDefArray[uiRegDefIndex];
		if (ptyRegDef->ubBitSize >= 32)
			ptyLastFullRegDef = ptyRegDef;
		for (ulIndex = 0; ulIndex < g_ulNumberOfRegisters; ulIndex++)
		{
			if ((g_ptyRISCVReg[ulIndex].ulAddrIndex == 0xFFFFFFFF) &&
				(strcasecmp(g_ptyRISCVReg[ulIndex].pszRegName, ptyRegDef->pszRegName) == 0))
				g_ptyRISCVReg[ulIndex].ulAddrIndex = uiRegDefIndex;
		}
		// special case for the endian mode bit
		if ((iRegDefEndianMode < 0) && (ptyLastFullRegDef != NULL) &&
			(strcasecmp(ptyLastFullRegDef->pszRegName, "Config") == 0))
			iRegDefEndianMode = (int32_t)uiRegDefIndex;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_Detach
	 Engineer: Nikolay Chokoev
		Input: None
	   Output: Error Code
  Description: resume execution of target
Date           Initials    Description
26-Nov-2007    NCH          Initial
05-Aug-2008    NCH         Error Code
****************************************************************************/
int32_t LOW_Detach(int32_t iCurrentTapIndex)
{
	int32_t bExecuting = FALSE;

	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Detach.");

	if (LOW_IsProcessorExecuting(&bExecuting, iCurrentTapIndex) == GDBSERV_ERROR)
		return GDBSERV_ERROR;
	else
		return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_TestScanChain
	 Engineer: Nikolay Chokoev
		Input: None
	   Output: int32_t : FALSE if error
  Description: test scanchain
Date           Initials    Description
03-Dec-2007    NCH          Initial
****************************************************************************/
int32_t LOW_TestScanChain(void)
{
	return(pfAshTestScanchain(ptyRISCVInstance));
}

/****************************************************************************
	 Function: LOW_DetectScanChain
	 Engineer: Nikolay Chokoev
		Input: uint32_t *pulNumCores : pointer to number of cores
	   Output: int32_t : FALSE if OK
  Description: detect scanchain
Date           Initials    Description
03-Dec-2007    NCH          Initial
****************************************************************************/
int32_t LOW_DetectScanChain(uint32_t *pulNumCores)
{
	uint32_t uiNumberOfCores;
	uint32_t ulIRWidth[MAX_TAPS_IN_CHAIN];
	unsigned char bRISCVCores[MAX_TAPS_IN_CHAIN];
	uint32_t uiIndex;

	(void)pfAshDetectScanchain(ptyRISCVInstance,
		MAX_TAPS_IN_CHAIN,
		&uiNumberOfCores,
		ulIRWidth,
		bRISCVCores);

	memset(tySI.tyProcConfig.Processor._RISCV.tyScanChainDetected,
		0,
		sizeof(tySI.tyProcConfig.Processor._RISCV.tyScanChainDetected));

	for (uiIndex = 0; uiIndex < uiNumberOfCores; uiIndex++)
	{
		tySI.tyProcConfig.Processor._RISCV.tyScanChainDetected[uiIndex].bIsRISCVCore =
			bRISCVCores[uiIndex];
		tySI.tyProcConfig.Processor._RISCV.tyScanChainDetected[uiIndex].ulIRlength =
			ulIRWidth[uiIndex];
	}

	*pulNumCores = uiNumberOfCores;
	return FALSE;
}

/****************************************************************************
	 Function: LOW_SetActiveTapIndex
	 Engineer: Nikolay Chokoev
		Input: iIndex : TAP index ; 0 is 1st, 1 is 2nd ...
	   Output: int32_t : FALSE if OK
  Description: detect scanchain
Date           Initials    Description
04-Dec-2007    NCH          Initial
****************************************************************************/
int32_t LOW_SetActiveTapIndex(int32_t iIndex)
{
	int32_t  iError;
	char szTapNum[3];

	if (tySI.ulCurrentTapNumber == (uint32_t)iIndex)
		return 0;

	tySI.ulCurrentTapNumber = (uint32_t)(iIndex);

	sprintf(szTapNum, "%d", (tySI.ulCurrentTapNumber));

	if (tySI.bMultiRegFileDetected == 1)
	{
		//PrintMessage(INFO_MESSAGE, "SetActiveCoreIndex to %x.", iIndex);

		//TODO: Condition might change (tap index already adjusted (-1))
		if (tySI.ulMultiRegFileCount < (uint32_t)tySI.ulCurrentTapNumber)
		{
			PrintMessage(ERROR_MESSAGE, "No XML file definition found for Tap %lx", tySI.ulCurrentTapNumber);
			return 1;
		}

		// now add commonly used registers...
		if ((iError = LOW_AddCoreRegDefArrayEntries()) != GDBSERV_NO_ERROR)
		{
			return iError;
		}

		// now index commonly used registers...
		if ((iError = LOW_IndexCommonRegDefArrayEntries()) != GDBSERV_NO_ERROR)
		{
			return iError;
		}
	}

	if (ptyRISCVInstance->ptyFunctionTable->process_property(ptyRISCVInstance, "tapnum", szTapNum) != GDBSERV_NO_ERROR)
		return GDBSERV_ERROR;

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_FlushDCache
	 Engineer: Nikolay Chokoev
		Input: iCurrentTapIndex : index to the RISCV core; 0 is 1st, 1 is 2nd ...
	   Output: int32_t : FALSE if OK
  Description: Flush the cache
Date           Initials    Description
10-Jan-2008    NCH          Initial
****************************************************************************/
int32_t LOW_FlushDCache(int32_t iCurrentTapIndex)
{	
	int32_t iErr = 0;
	return iErr;
}

/****************************************************************************
	 Function: LOW_InvalidateDCache
	 Engineer: Nikolay Chokoev
		Input: iCurrentTapIndex : index to the RISCV core; 0 is 1st, 1 is 2nd ...
	   Output: int32_t : FALSE if OK
  Description: Invalidate the cache
Date           Initials    Description
10-Jan-2008    NCH          Initial
****************************************************************************/
int32_t LOW_InvalidateDCache(int32_t iCurrentTapIndex)
{
	int32_t iErr = 0;
	return iErr;
}

/****************************************************************************
	 Function: LOW_CloseDevice
	 Engineer: Nikolay Chokoev
		Input: void
	   Output: void
  Description: Close Opella-XD
Date           Initials    Description
05-Aug-2008    NCH         Initial
****************************************************************************/
int32_t LOW_CloseDevice(int32_t iIndex)
{
	NOREF(iIndex);
	return TRUE;
}

/****************************************************************************
	 Function: Is_StoppedAtWatchpoint
	 Engineer: Sreeshma T.
		Input: None
	   Output: int32_t : FALSE if error
  Description:
Date           Initials    Description
16-Mar-2016    ST          Initial
****************************************************************************/
int32_t Is_StoppedAtWatchpoint(void)
{
	/*if (ptyRISCVInstance->ptyFunctionTable->stopped_at_watchpoint(ptyRISCVInstance) == 1)
	{
		return 1;
	}
	else
	{
		return 0;
	}*/
	return 0;
}

int32_t Low_GetMulticore(uint32_t *pulnumberCore, uint32_t *pulcoreinfo)
{
	//char szDevices[10];
	char szDevInfo[10];

	if (pulcoreinfo == NULL)
	{
		return 0;
	}
	pfAshGetSetupItem(ptyRISCVInstance, RISCV_SETUP_SECT_MULTICORE, RISCV_SETUP_ITEM_NUMBEROFTAPS, szDevInfo);

	sscanf_s(szDevInfo, "%ld", pulnumberCore);

	return 0;

}

int32_t Low_SetMulticore(uint32_t ulnumberCore, uint32_t *pulcoreinfo)
{
	uint32_t ulIndex;
	char szDevices[10];

	if (pulcoreinfo == NULL)
	{
		return 0;
	}

	sprintf(szDevices, "%d", (int32_t)ulnumberCore);
	pfAshSetSetupItem(ptyRISCVInstance, RISCV_SETUP_SECT_MULTICORE, RISCV_SETUP_ITEM_NUMBEROFTAPS, szDevices);

	for (ulIndex = 1; ulIndex < ulnumberCore + 1; ulIndex++)
	{
		sprintf(szDevices, "Device%d", (uint32_t)ulIndex);

		if (pulcoreinfo[ulIndex] == 1)
		{
			pfAshSetSetupItem(ptyRISCVInstance, szDevices, RISCV_SETUP_ITEM_RISCVCORE, "1");
		}
		else
		{
			pfAshSetSetupItem(ptyRISCVInstance, szDevices, RISCV_SETUP_ITEM_RISCVCORE, "0");
		}

	}

	pfAshUpdateScanchain(ptyRISCVInstance);

	return 0;
}

int32_t Low_ConfigureVega(uint32_t uiInstrtoSelectADI, uint32_t uiCore, uint32_t uiIRLen)
{
	if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->configure_vega(ptyRISCVInstance, (unsigned short)tySI.ulCurrentTapNumber, uiInstrtoSelectADI, uiCore, uiIRLen))
	{
		return GDBSERV_ERROR;
	}
	else
		return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_GetThreadId
	 Engineer: Ashutosh Garg
		Input: char *PcInput - Input packet
	   Output: uint32_t : return thread number from vCont Packet
  Description:
Date           Initials    Description
11-Apr-2019     AG          Initial
****************************************************************************/
uint32_t LOW_GetThreadId(char * pcInput)
{
	unsigned long ulIndex;

	ulIndex = 0;
	while (pcInput[ulIndex] != ':')
	{
		ulIndex++;
	}
	//it will return thread id starting from 1 but we are working on thread id starting with 0
	return ((pcInput[ulIndex + 1] - '0')-1);
}

/****************************************************************************
	 Function: LOW_SetRiscvMemAccessMechanism
	 Engineer: Ashutosh Garg
		Input: uint32_t ulAddress - Address of the memory to access
	   Output: GDBSERVER_ERROR
  Description: set the riscv mechanism as per the memory range to be accessed
Date           Initials    Description
14-Apr-2019     AG          Initial
****************************************************************************/
int32_t LOW_SetRiscvMemAccessMechanism(uint32_t ulAddress)
{
		uint32_t ulMemmechanism = UNDEFINED_MEMORY_ACCESS; //memory access prefrence is system bus but can be manipulated 
															//as per discovery and access type in familyriscv.xml
		uint32_t ulIndex = 0;
		uint32_t ulThreadId;

		ulThreadId = LOW_CurrentHart();
		
		//checking if the uladdress falls with in the given range of memory in familyriscv.xml
		for (ulIndex = 0; ulIndex < tySI.ulXMLRISCVMemBlockCount; ulIndex++)
		{
			if (((ulAddress >= tySI.tyriscvMemoryAccess[ulIndex].ulXMLRISCVMemAddress) && (ulAddress < (tySI.tyriscvMemoryAccess[ulIndex].ulXMLRISCVMemAddress + tySI.tyriscvMemoryAccess[ulIndex].ulXMLRISCVMemSize))))
			{
				if(tySI.tyriscvMemoryAccess[ulIndex].ulXMLRISCVMemAccess != UNDEFINED_MEMORY_ACCESS)
					ulMemmechanism = tySI.tyriscvMemoryAccess[ulIndex].ulXMLRISCVMemAccess;
				break;
			}
		}
		
		if (GDBSERV_NO_ERROR != LOW_RiscvMechanism(ulMemmechanism))
		{
			return GDBSERV_ERROR;
		}
		return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: LOW_TargetReset
	 Engineer: Harvinder Singh
		Input: uint32_t ulHartId - hart id of hart to halt. 
	   Output: GDBSERVER_ERROR
  Description: Reset the RISCV target.
Date           Initials    Description
06-June-2019     Hs          Initial
****************************************************************************/
int32_t LOW_TargetReset(uint32_t ulHartId)
{
	if (GDBSERV_NO_ERROR != ptyRISCVInstance->ptyFunctionTable->target_reset(ptyRISCVInstance, ulHartId))
	{
		return GDBSERV_ERROR;
	}
	return GDBSERV_NO_ERROR;
}