/****************************************************************************
	   Module: gdbxmlcfgscarc.c
	 Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, contains needed resources to parse
			   scan chain structure from XML file.
Date           Initials    Description
12-NOV-2007    NCH         initial
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h" 

// external variables
extern TyServerInfo tySI;

/****************************************************************************
	 Function: CFG_ProcessorNode
	 Engineer: Nikolay Chokoev
		Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
	   Output: none
  Description:
Date           Initials    Description
12-Nov-2007    NCH         Initial
*****************************************************************************/
/*void CFG_ScanChainNode(TyXMLNode *ptyThisNode)
{
	char         *pszName;
	char         *pszState;
	TyXMLNode    *ptyNode;
	unsigned long ulIndex = 1;
	unsigned long ulFirstArcCore = 0;

	tySI.bMultiRegFileDetected = 0;
	tySI.ulMultiRegFileCount = 0;

	assert(ptyThisNode != NULL);

	//init for core 0
	tySI.bMultiRegFileDetected = 0;
	tySI.bIsMultiRegFilePresent[0] = 0;

	// process child nodes
	if ((ptyNode = XML_GetChildNode(ptyThisNode, "Core")) != NULL)
	{
		do
		{
			ulIndex = XML_GetDec(XML_GetAttrib(ptyNode, "Number"), 0);
			if (ulIndex > MAX_CORES_IN_CHAIN)
				break;

			pszState = XML_GetAttrib(ptyNode, "IsARCCore");
			if (strcasecmp(pszState, "true") == 0)
			{
				tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].bIsARCCore = 1; //lint !e661
				if (ulFirstArcCore == 0)
				{
					tySI.ulTAP[ulIndex - 1] = ulIndex;
				}
				ulFirstArcCore = 1;
			}
			else
			{
				tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].bIsARCCore = 0; //lint !e661
				tySI.ulTAP[ulIndex - 1] = 0;
			}

			//init new values for multi register file assignments
			if (ulIndex < MAX_CORES_IN_CHAIN - 1) //lint
			{
				strcpy(tySI.szRegFile[ulIndex], "");
				tySI.bIsMultiRegFilePresent[ulIndex] = 0;
			}

			tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulIRlength = XML_GetChildElementDec(ptyNode, "IRlength", 0);   //lint !e661
			tySI.tyProcConfig.Processor._ARC.tyScanChainDef[ulIndex].ulBypassInst = XML_GetChildElementHex(ptyNode, "BypassInst", 0); //lint !e661

			if (!tySI.bIsRegFilePresent)
			{
				pszName = XML_GetChildElement(ptyNode, "RegFile");

				if (pszName != NULL)
				{
					strcpy(tySI.szRegFile[ulIndex], pszName);

					if ((!strcmp(tySI.szRegFile[ulIndex], "arc600-cpu.xml"))
						|| (!strcmp(tySI.szRegFile[ulIndex], "arc700-cpu.xml"))
						|| (!strcmp(tySI.szRegFile[ulIndex], "arc-em-cpu.xml"))
						|| (!strcmp(tySI.szRegFile[ulIndex], "arc-hs-cpu.xml"))
						|| (!strcmp(tySI.szRegFile[ulIndex], "arc-a5-cpu")))
					{
						PrintMessage(INFO_MESSAGE, "Found Core%lx ARC register file '%s'.", ulIndex, tySI.szRegFile[ulIndex]);
						tySI.bMultiRegFileDetected = 1;
						tySI.bIsMultiRegFilePresent[ulIndex] = 1;
					}
					else
					{
						PrintMessage(ERROR_MESSAGE, "Spelling error of Core%lx ARC register file '%s'.", ulIndex, tySI.szRegFile[ulIndex]);
						strcpy(tySI.szRegFile[ulIndex], "");
					}
				}
			}

			ulIndex++;
		} while (((ptyNode = XML_GetNextNode(ptyNode)) != NULL) && (ulIndex < MAX_CORES_IN_CHAIN - 1));

		if (tySI.bMultiRegFileDetected == 1)
		{
			PrintMessage(INFO_MESSAGE, ""); //insert line
			tySI.ulMultiRegFileCount = ulIndex;
		}
	}
}*/

