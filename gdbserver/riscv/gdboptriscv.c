/****************************************************************************
	   Module: gdboptarc.c
	 Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, ARC specific command line options
Date           Initials    Description
08-Nov-2007    NCH         initial
08-Oct-2012    SV          Support for providing Register File
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbopt.h"



// default ARC options

// external variables
extern TyServerInfo tySI;

// local function prototypes
static int32_t OptionRegisterFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionCoreRegisterFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionHelp(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionVersion(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionJtagConsoleMode(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionDebugStdout(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionDebugFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionCommandFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionGdbPort(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionProbeInstance(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionJtagFrequency(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionDevice(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionScan(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionProgramEntryPoint(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionPropertiesFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionTapNumber(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionAdvanceInfo(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionCoreNumber(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static int32_t OptionTargetReset(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
static void HelpJtagFrequency(void);
static void HelpDevice(void);
static char *TranslateName(char *pszDst, char *pszSrc, uint32_t uiSize);
extern void CFG_ScanChainNode(TyXMLNode *ptyThisNode);
void XML_GetTargetMemoryInfo(TyXMLNode *ptyThisNode);
uint32_t XML_GetScanChainInfo(TyXMLNode *ptyThisNode);

// command line options (must be global variable)
TyOption tyaOptions[] =
{
   {
   "help",
   "",
   "Display usage information.",
   "",
   "",
   "",
   OptionHelp,
   NULL,
   FALSE
   },
   {
   "version",
   "",
   "Display program version.",
   "",
   "",
   "",
   OptionVersion,
   NULL,
   FALSE
   },
   {
   "debug-stdout",
   "",
   "Display debug messages to standard output.",
   "",
   "",
   "",
   OptionDebugStdout,
   NULL,
   FALSE
   },
   {
   "debug-file",
   " <file>",
   "Log debug messages to <file>.",
   "",
   "",
   "",
   OptionDebugFile,
   NULL,
   FALSE
   },
   {
   "command-file",
   " <file>",
   "Read command line options from <file>.",
   "",
   "",
   "",
   OptionCommandFile,
   NULL,
   FALSE
   },
   {
   "gdb-port",
   " <port>",
   "Listen on <port> for remote connections",
   "from GDB. Default is port 2331.",
   "",
   "",
   OptionGdbPort,
   NULL,
   FALSE
   },
   {
   "instance",
   " <number>",
   "Select Opella-XD from multiple Opella-XD(s)",
   "connected.",
   "<number> is Opella-XD serial number.",
   "",
   OptionProbeInstance,
   NULL,
   FALSE
   },
   {
   "jtag-frequency",
   " <freq>",
   "Specifies JTAG Frequency.",
   "",
   "",
   "",
   OptionJtagFrequency,
   HelpJtagFrequency,
   FALSE
   },
   {
   "device",
   " <dev>",
   "Target device name can be specified.",
   "The following devices are supported:",
   "",
   "",
   OptionDevice,
   HelpDevice,
   FALSE
   },
   {
   "scan-file",
   " <file>",
   "Specifies target scanchain. The scanchain",
   "information is extracted from .XML file - ",
   "<file>",
   "",
   OptionScan,
   NULL,
   FALSE
   },
   {
   "program-entry-point",
   " <addr>",
   "Set the program entry point to <addr>.",
   "",
   "",
   "",
   OptionProgramEntryPoint,
   NULL,
   FALSE
   },
#ifdef ARC
   {
   "arc-prop-file",
   " <file>",
   "Read and apply ARC properties from <file>.",
   "",
   "",
   "",
   OptionPropertiesFile,
   NULL,
   FALSE
   },
#endif
   {
   "tap-number",
   " <number>",
   "Specifies the JTAG Test Access Point",
   " (TAP) <number> for Multi-core targets.",
   "",
   "",
   OptionTapNumber,
   NULL,
   FALSE
   },
   {
   "core-number",
   " <number>",
   "Specifies the core to be connected",
   " indexed from 0",
   "",
   "",
   OptionCoreNumber,
   NULL,
   FALSE
   },
   {
   "jtag-console-mode",
   "",
   "Open JTAG low-level console mode.",
   "",
   "",
   "",
   OptionJtagConsoleMode,
   NULL,
   FALSE
   },
#if defined (ARC)
   {
   "arc-reg-file",
   " <file>",
   "Read Auxiliary registers from XML register file.",
   "",
   "",
   "",
   OptionRegisterFile,
   NULL,
   FALSE
   },
   {
   "arc-corereg-file",
   " <file>",
   "Read Core registers from XML register file.",
   "",
   "",
   "",
   OptionCoreRegisterFile,
   NULL,
   FALSE
   }
#endif 
#ifdef RISCV
   {
		"show-target-info",
	    "",
		"Read Advanced Target info.",
		"",
		"",
		"",
		OptionAdvanceInfo,
		NULL,
		FALSE
   },

	{
		"target-reset",
		"",
		"Reset the target",
		"",
		"",
		"",
		OptionTargetReset,
		NULL,
		FALSE
	}
#endif // RISCV

};
#define MAX_OPTIONS ((int32_t)(sizeof(tyaOptions) / sizeof(TyOption)))
int32_t iGdb_options_number = MAX_OPTIONS;

// local variables

/****************************************************************************
	 Function: OptionRegisterFile
	 Engineer: Sreekanth.V
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for Register File.
Date           Initials    Description
08-Oct-2012      SV         Initial
*****************************************************************************/
static int32_t OptionRegisterFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{

	char szFileName[_MAX_PATH];
	FILE *RegFile;

	assert(puiArg != NULL);
	assert(ppszArgs != NULL);

	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing file name after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;

	// using this might cause an exploitable buffer overrun or crash. (found during code analysis)
	/*strncpy(szFileName, ppszArgs[*puiArg], sizeof(szFileName) - 1);
	strcat(szFileName, "\0");*/
	 //using safe string manipulation strncpy_s function
	strncpy_s(szFileName, sizeof(szFileName), ppszArgs[*puiArg], strlen(ppszArgs[*puiArg]));

	//Just to ensure the given file exist,before saving it in tySI.szRegFile...
	RegFile = fopen(szFileName, "rt");
	if (RegFile == NULL)
	{
		sprintf(tySI.szError, "Could not open Arc register file '%s'.", szFileName);
		return GDBSERV_OPT_ERROR;
	}
	fclose(RegFile);

	//Save the Register file name
	strcpy(tySI.szRegFile[0], szFileName);
	tySI.bIsRegFilePresent = 1;

	return GDBSERV_NO_ERROR;

}

/****************************************************************************
	 Function: OptionCoreRegisterFile
	 Engineer: Sreeshma T.
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for Register File.
Date           Initials    Description
08-Mar-2016      ST         Initial
*****************************************************************************/
static int32_t OptionCoreRegisterFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{

	char szFileName[_MAX_PATH];
	FILE *CoreRegFile;

	assert(puiArg != NULL);
	assert(ppszArgs != NULL);

	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing file name after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	// using this might cause an exploitable buffer overrun or crash. (found during code analysis)
	/*strncpy(szFileName, ppszArgs[*puiArg], sizeof(szFileName) - 1);
	strcat(szFileName, "\0");*/

	//using safe string manipulation strncpy_s function
	strncpy_s(szFileName, sizeof(szFileName), ppszArgs[*puiArg], strlen(ppszArgs[*puiArg]));

	//Just to ensure the given file exist,before saving it in tySI.szRegFile...
	CoreRegFile = fopen(szFileName, "rt");
	if (CoreRegFile == NULL)
	{
		sprintf(tySI.szError, "Could not open Core register file '%s'.", szFileName);
		return GDBSERV_OPT_ERROR;
	}
	fclose(CoreRegFile);

	//Save the Register file name
	strcpy(tySI.szCoreRegFile, szFileName);
	PrintMessage(INFO_MESSAGE, "tySI.bIsCoreRegFilePresent=%x", tySI.bIsCoreRegFilePresent);
	tySI.bIsCoreRegFilePresent = 1;

	return GDBSERV_NO_ERROR;

}

/****************************************************************************
	 Function: OptionHelp
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for option help.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionHelp(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	NOREF(uiArgCount);
	NOREF(puiArg);
	NOREF(ppszArgs);
	tySI.bHelp = 1;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionAdvanceInfo
	 Engineer: Ashutosh Garg
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Display Advance Discovery Info
Date           Initials    Description
19-NOV-2018    AG          Initial & cleanup

*****************************************************************************/
static int32_t OptionAdvanceInfo(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	NOREF(uiArgCount);
	NOREF(puiArg);
	NOREF(ppszArgs);
	tySI.DiscoveryAdvanceInfo = 1;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionVersion
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for option version.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionVersion(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	NOREF(uiArgCount);
	NOREF(puiArg);
	NOREF(ppszArgs);
	tySI.bVersion = 1;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionJtagConsoleMode
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for option jtag console mode.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionJtagConsoleMode(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	NOREF(uiArgCount);
	NOREF(puiArg);
	NOREF(ppszArgs);
	tySI.bJtagConsoleMode = 1;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionDebugStdout
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for option debug output.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionDebugStdout(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	NOREF(uiArgCount);
	NOREF(puiArg);
	NOREF(ppszArgs);
	tySI.bDebugStdout = 1;
	tySI.bDebugOutput = 1;
	PrintMessage(INFO_MESSAGE, "Displaying debug messages to standard output.");
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionDebugFile
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Action for debug file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionDebugFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing filename after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	strncpy(tySI.szDebugFile, ppszArgs[*puiArg], sizeof(tySI.szDebugFile) - 1);
	tySI.szDebugFile[sizeof(tySI.szDebugFile) - 1] = '\0';
	tySI.pDebugFile = fopen(tySI.szDebugFile, "w");
	if (tySI.pDebugFile == NULL)
	{
		sprintf(tySI.szError, "Could not open debug file '%s'.", tySI.szDebugFile);
		return GDBSERV_OPT_ERROR;
	}
	tySI.bDebugOutput = 1;
	// put title and version in log file
	LogTitleAndVersion();
	PrintMessage(INFO_MESSAGE, "Logging debug messages to file '%s'.", tySI.szDebugFile);
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionCommandFile
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option command file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionCommandFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	int32_t iError;
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing filename after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	strncpy(tySI.szCommandFile, ppszArgs[*puiArg], sizeof(tySI.szCommandFile) - 1);
	tySI.szCommandFile[sizeof(tySI.szCommandFile) - 1] = '\0';
	PrintMessage(INFO_MESSAGE, "Reading command line options from file '%s'.", tySI.szCommandFile);
	iError = ProcessCmdFileArgs(tySI.szCommandFile, 0);
	return iError;
}

/****************************************************************************
	 Function: OptionGdbPort
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option GDB port.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionGdbPort(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	uint32_t ulPort = 0;
	int32_t  bValid = 0;
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing port number after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	ulPort = StrToUlong(ppszArgs[*puiArg], &bValid);
	if (!bValid || (ulPort > 0xFFFF))
	{
		sprintf(tySI.szError, "Invalid port number '%s'.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	tySI.usGdbPort = (unsigned short)ulPort;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionProbeInstance
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Selecting probe instance number if necessary
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionProbeInstance(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing Opella-XD serial number after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	memset((void *)tySI.szProbeInstance, 0, PROBE_STRING_LEN * sizeof(char));
	if (strlen(ppszArgs[*puiArg]) < PROBE_STRING_LEN)
	{  // string length is less than buffer size
		strcpy(tySI.szProbeInstance, ppszArgs[*puiArg]);
		tySI.szProbeInstance[PROBE_STRING_LEN - 1] = '\0';
	}
	else
	{
		sprintf(tySI.szError, "Invalid Opella-XD serial number format.");
		return GDBSERV_OPT_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionJtagFrequency
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option jtag frequency.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Setting frequency bug fix & Return error code
*****************************************************************************/
static int32_t OptionJtagFrequency(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	char szFrequency[64];
	// check options
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing JTAG frequency after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	strncpy(szFrequency, ppszArgs[*puiArg], sizeof(szFrequency) - 1);
	szFrequency[sizeof(szFrequency) - 1] = '\0';
	// check if requesting RTCK
	if (!strcasecmp(szFrequency, "rtck"))
	{  // just set adaptive clock
		tySI.bJtagRtck = 1;
		strcpy(tySI.szJtagFreq, "adaptive speed");
	}
	else
	{  // fixed frequency
		int32_t iStrLen;
		int32_t bValid = 0;
		uint32_t ulFrequency = 1;
		uint32_t ulMultiplier = FREQ_MHZ(1);
		tySI.bJtagRtck = 0;
		tySI.uiRtckTimeout = DEFAULT_RTCK_TIMEOUT;
		tySI.ulJtagFreqHz = DEFAULT_FREQUENCY_HZ;
		strcpy(tySI.szJtagFreq, "");
		iStrLen = (int32_t)strlen(szFrequency);
		if (iStrLen < 3)
		{
			strcat(szFrequency, "MHz");
		}
		iStrLen = (int32_t)strlen(szFrequency);
		char *pszUnit = (szFrequency + ((uint64_t)iStrLen - 3));
		if (strcasecmp(pszUnit, "khz") == 0)
			ulMultiplier = FREQ_KHZ(1);
		else if (strcasecmp(pszUnit, "mhz") == 0)
			ulMultiplier = FREQ_MHZ(1);
		else
		{
			//sprintf(tySI.szError, "--jtag-frequency does not contain the valid value.");
			sprintf(tySI.szError, "Invalid JTAG frequency '%s'.", ppszArgs[*puiArg]);
			return GDBSERV_OPT_ERROR;
		}
		//terminate frequency string just before the unit
		szFrequency[iStrLen - 3] = '\0';
		ulFrequency = StrToUlong(szFrequency, &bValid);
		if (!bValid)
		{
			sprintf(tySI.szError, "Invalid JTAG frequency '%s'.", ppszArgs[*puiArg]);
			return GDBSERV_OPT_ERROR;
		}
		tySI.ulJtagFreqHz = ulFrequency * ulMultiplier;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionDevice
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option device.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionDevice(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	TyXMLNode *ptyNode;
	int32_t iError;
	char *pszDevice;
	char szBuffer[_MAX_PATH];
	uint32_t ulIndex = 0;
	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	tySI.ulProcessor = 0;
	tySI.ptyNodeProcessor = NULL;
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing device name after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	// check for a matching device...
	ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Processor");
	while (ptyNode != NULL)
	{
		if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
		{
			ulIndex++;
			(void)TranslateName(szBuffer, pszDevice, sizeof(szBuffer));
			if (strcasecmp(ppszArgs[*puiArg], szBuffer) == 0)
			{
				tySI.ulProcessor = ulIndex;
				tySI.ptyNodeProcessor = ptyNode;
				if (strcasecmp("rv32m1-vega", szBuffer) == 0)
				{
					tySI.bIsVegaBoard = 1;
				}
				//added to store the DCCM AND ICCM Memory values locally in case of WDC Core
				(void)XML_GetTargetMemoryInfo(ptyNode);
				iError = XML_GetScanChainInfo(ptyNode);
				if (iError != 0)
					return GDBSERV_OPT_ERROR;
				break;
			}
		}
		ptyNode = XML_GetNextNode(ptyNode);
	}
	if (tySI.ulProcessor == 0)
	{
		sprintf(tySI.szError, "Invalid device name '%s'.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionScan
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option scan.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionScan(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	TyXMLNode *ptyNode;
	TyXMLFile tyXMLSCFile;
	int32_t iError;
	char szFileName[_MAX_PATH];
	assert(puiArg != NULL);
	assert(ppszArgs != NULL);

	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing file name after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}

	(*puiArg)++;
	strncpy(szFileName, ppszArgs[*puiArg], sizeof(szFileName) - 1);
	szFileName[sizeof(szFileName) - 1] = '\0';

	//Just to ensure the given file exist,before saving it in tySI.szRegFile...
	iError = XML_ReadFile(szFileName, &tyXMLSCFile);
	if (iError != 0)
		return iError;

	ptyNode = XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "TargetDefinition");
	if (ptyNode == NULL)
	{
		sprintf(tySI.szError, "Invalid scanchain file '%s',\ntag '<TargetDefinition>' not found.", tySI.tyXMLFile.szName);
		return GDBSERV_CFG_ERROR;
	}

	CFG_ScanChainNode(ptyNode);
	XML_Free(&tyXMLSCFile);

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionProgramEntryPoint
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option entry point.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Added program entry point
*****************************************************************************/
static int32_t OptionProgramEntryPoint(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	uint32_t ulEntryPoint = 0;
	int32_t  bValid = 0;
	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing entry point address after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	ulEntryPoint = StrToUlong(ppszArgs[*puiArg], &bValid);
	if (!bValid)
	{
		sprintf(tySI.szError, "Invalid entry point address '%s'.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	tySI.tyProcConfig.ulProgEntryPoint = ulEntryPoint;
	tySI.tyProcConfig.bUseEntryPoint = TRUE;
	PrintMessage(INFO_MESSAGE, "Setting program entry point (PC) to 0x%08lX.", ulEntryPoint);
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionPropertiesFile
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option properties file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use error code
*****************************************************************************/
static int32_t OptionPropertiesFile(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	FILE *pFile;
	int32_t iLineNumber = 0;
	int32_t iScannedItems;
	int32_t iError = 0;
	int32_t iPropCnt = 0;
	char szVariableName[_MAX_PATH];
	char szLine[_MAX_PATH];
	char *pszResult = NULL;

	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing filename after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	strncpy(tySI.szRegisterFile, ppszArgs[*puiArg], sizeof(tySI.szRegisterFile) - 1);
	tySI.szRegisterFile[sizeof(tySI.szRegisterFile) - 1] = '\0';
	// open in text mode so that CR+LF is converted to LF
	pFile = fopen(tySI.szRegisterFile, "rt");
	if (pFile == NULL)
	{
		sprintf(tySI.szError, "Could not open properties file '%s'.", tySI.szRegisterFile);
		return GDBSERV_OPT_ERROR;
	}
	while (!feof(pFile))
	{
		if (fgets(szLine, sizeof(szLine), pFile) == NULL)
			break;
		iLineNumber++;
		szVariableName[0] = '\0';
		iScannedItems = sscanf(szLine, " %s ", szVariableName);
		// if an empty line, continue with next line...
		if (iScannedItems <= 0)
			continue;
		// if a comment, continue with next line...
		if ((szVariableName[0] == '\0') || (szVariableName[0] == '#') || (szVariableName[0] == ';') ||
			((szVariableName[0] == '/') && (szVariableName[1] == '/')))
			continue;
		if (iScannedItems == 1)
		{
			if (iPropCnt > MAX_USER_VARS - 1)
			{
				iError = GDBSERV_OPT_ERROR;
				break;
			}
			if ((pszResult = strtok(szVariableName, "=")) == NULL)
			{
				iError = GDBSERV_OPT_ERROR;
				break;
			}
			if (strcmp(pszResult, "-prop"))
			{
				iError = GDBSERV_OPT_ERROR;
				break;
			}
			if ((pszResult = strtok(NULL, "=")) == NULL)
			{
				iError = GDBSERV_OPT_ERROR;
				break;
			}
			if ((pszResult = strtok(NULL, "=")) == NULL)
			{
				iError = GDBSERV_OPT_ERROR;
				break;
			}
		}
		else
		{
			iError = GDBSERV_OPT_ERROR;
			break;
		}
	}
	if (iError != GDBSERV_NO_ERROR)
		sprintf(tySI.szError, "Invalid format at line %d of properties file '%s':\n%s", iLineNumber, tySI.szRegisterFile, szLine);
	if (pFile != NULL)
		(void)fclose(pFile);
	return iError;
}

/****************************************************************************
	 Function: OptionTapNumber
	 Engineer: Vitezslav Hola
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option tap number.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Return error code
*****************************************************************************/
static int32_t OptionTapNumber(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	uint32_t ulTap = 0;
	char szTAPNumbers[MAX_TAP_NUM * 2];
	char szCurTAPNumber[MAX_TAP_NUM * 2];
	int32_t bValid = 0;
	uint32_t uiTapCnt = 0;
	uint32_t uiInd = 0;
	int32_t iCnt;

	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing Test Access Point number after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	strncpy(szTAPNumbers, ppszArgs[*puiArg], (MAX_TAP_NUM * 2));
	//get TAP numbers from command line argument and 
	//store them as a strings array

	//return if tap number entered does not exist
	szCurTAPNumber[uiInd] = '\0';
	ulTap = StrToUlong(szTAPNumbers, &bValid);

	if ((ulTap < 1) || (ulTap > tySI.tyProcConfig.Processor._RISCV.uiNumberofTAPs))
	{
		sprintf(tySI.szError, "Invalid Test Access Point number '%s'.", szTAPNumbers);
		return GDBSERV_OPT_ERROR;
	}

	for (iCnt = 0; iCnt < (MAX_TAP_NUM); iCnt++)
	{
		tySI.ulTAP[iCnt] = 0;
	}

	for (iCnt = 0; iCnt < (MAX_TAP_NUM * 2); iCnt++)
	{
		if ((szTAPNumbers[iCnt] != ',') && (szTAPNumbers[iCnt] != '\0'))
		{  //get the last TAP
			szCurTAPNumber[uiInd++] = szTAPNumbers[iCnt];
		}
		else
		{
			szCurTAPNumber[uiInd] = '\0';
			ulTap = StrToUlong(szCurTAPNumber, &bValid);
			if (!bValid || (ulTap > MAX_TAP_NUM) || (uiTapCnt > (MAX_TAP_NUM - 1)))
			{
				sprintf(tySI.szError, "Invalid Test Access Point number '%s'.", szCurTAPNumber);
				return GDBSERV_OPT_ERROR;
			}
			tySI.ulTAP[uiTapCnt++] = (uint32_t)ulTap;
			if (szTAPNumbers[iCnt] == '\0')
				break;   //this was the last TAP in arguments
			uiInd = 0;
		}
	}

	//tySI.iTapNumberPresent = 1;
 //TODO:   PrintMessage(INFO_MESSAGE, "TAP number is %d.", (uint32_t)ulTap);
	return GDBSERV_NO_ERROR;
}

static int32_t OptionCoreNumber(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	uint32_t ulCore = 0;
	int32_t bValid = 0;

	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	if (((*puiArg) + 1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing core number after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}

	(*puiArg)++;
	ulCore = StrToUlong(ppszArgs[*puiArg], &bValid);
	tySI.ulCore = (uint32_t)ulCore;

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: OptionTargetReset
	 Engineer: Harvinder Singh
		Input: uint32_t *puiArg - pointer to index of next arg
			   uint32_t uiArgCount - count of args in the list
			   char **ppszArgs - list of args
	   Output: int32_t - error code
  Description: Option Target Reset.
                0- no reset.
				1- soft reset.
				2- hard reset.
Date           Initials    Description
06-June-2019    HS          Initial & cleanup
*****************************************************************************/
static int32_t OptionTargetReset(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs)
{
	
	assert(puiArg != NULL);
	assert(ppszArgs != NULL);
	//check if required aruguments are there.
	if (((*puiArg)+1) >= uiArgCount)
	{
		sprintf(tySI.szError, "Missing reset value after %s option.", ppszArgs[*puiArg]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	if (strcmp(ppszArgs[*puiArg], "1")== 0)
	{
		tySI.ucTargetResetFlag = 1;
	}
	else if(strcmp(ppszArgs[*puiArg], "0")== 0)
	{
		tySI.ucTargetResetFlag = 0;
	}
	else
	{
		sprintf(tySI.szError, "Invalid value entered after %s option.", ppszArgs[*puiArg - 1]);
		return GDBSERV_OPT_ERROR;
	}
	(*puiArg)++;
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: TranslateName
	 Engineer: Vitezslav Hola
		Input: char *pszDst - storage for translated name
			   char *pszSrc - name to translate
			   uint32_t uiSize - size of destination storage
	   Output: char * - copy of pszDst
  Description: Translate device names to be command line option compatible
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static char *TranslateName(char *pszDst, char *pszSrc, uint32_t uiSize)
{
	char *pszTemp;

	assert(pszDst != NULL);
	assert(pszSrc != NULL);
	assert(uiSize > 0);
	strncpy(pszDst, pszSrc, uiSize - 1);
	pszDst[uiSize - 1] = '\0';
	StringToLwr(pszDst);
	// first convert anything unusual to '-'
	pszTemp = pszDst;
	while (*pszTemp)
	{
		if (((*pszTemp < 'a') || (*pszTemp > 'z')) && ((*pszTemp < '0') || (*pszTemp > '9')))
			*pszTemp = '-';
		pszTemp++;
	}
	// remove any leading '-'
	while (*pszDst == '-')
		strcpy(pszDst, pszDst + 1);
	// remove any trailing '-'
	pszTemp = pszDst + strlen(pszDst) - 1;
	while ((pszTemp > pszDst) && (*pszTemp == '-'))
		*pszTemp-- = '\0';
	// remove any duplicate '-'
	pszTemp = pszDst;
	while (*pszTemp)
	{
		if ((pszTemp[0] == '-') && (pszTemp[1] == '-'))
			strcpy(pszTemp, pszTemp + 1);
		else
			pszTemp++;
	}
	// remove leading 'philips-'
	if (strncmp(pszDst, "philips-", 8) == 0)
		strcpy(pszDst, pszDst + 8);
	// remove '-core' if present
	if ((pszTemp = strstr(pszDst, "-core")) != NULL)
		strcpy(pszTemp, pszTemp + 5);
	// change '-pin' to 'pin'  (e.g. -14-pin => -14pin)
	if ((pszTemp = strstr(pszDst, "-pin")) != NULL)
		strcpy(pszTemp, pszTemp + 1);
	return pszDst;
}

/****************************************************************************
	 Function: HelpDevice
	 Engineer: Vitezslav Hola
		Input: none
	   Output: none
  Description: Additional help for device option
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
30-Apr-2012    SPT         Changed to Hardcoded help rather than reading from xml
*****************************************************************************/
static void HelpDevice(void)
{
#ifdef RISCV
	TyXMLNode *ptyNode;
	char *pszDevice;
	char szBuffer[_MAX_PATH];
	ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Processor");

	while (ptyNode != NULL)
	   {
	   if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
		  {
		  PrintMessage(HELP_MESSAGE, "%*s  %s\n", GDBSERV_HELP_COL_WIDTH, " ", TranslateName(szBuffer, pszDevice, sizeof(szBuffer)));
		  }
	   ptyNode = XML_GetNextNode(ptyNode);
	   }
	//commented device name reading from xml file and device name otions given here with some aditional info.
#elif defined ARC
	PrintMessage(HELP_MESSAGE, "%*s arc (using JTAG/TPAOP-ARC20 R0)\n", GDBSERV_HELP_COL_WIDTH, " ");
	PrintMessage(HELP_MESSAGE, "%*s arcangel (using JTAG/TPAOP-ARC20 R0/ADOP-ARC15)\n", GDBSERV_HELP_COL_WIDTH, " ");
	PrintMessage(HELP_MESSAGE, "%*s arc-cjtag-tpa-r1 (using cJTAG/TPAOP-ARC20 R1)\n", GDBSERV_HELP_COL_WIDTH, " ");
	PrintMessage(HELP_MESSAGE, "%*s arc-jtag-tpa-r1 (using JTAG/TPAOP-ARC20 R1)\n", GDBSERV_HELP_COL_WIDTH, " ");
#endif // RISCV

	//PrintMessage(HELP_MESSAGE, "%*s SiFive-E31\n", GDBSERV_HELP_COL_WIDTH, " ");
	//PrintMessage(HELP_MESSAGE, "%*s RV32M1-VEGA\n", GDBSERV_HELP_COL_WIDTH, " ");

}

/****************************************************************************
	 Function: HelpJtagFrequency
	 Engineer: Vitezslav Hola
		Input: none
	   Output: none
  Description: Additional help for frequency selection
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void HelpJtagFrequency(void)
{
	if (tySI.tyProcessorFamily == GDBSERV_PROC_FAMILY)
	{
		PrintMessage(HELP_MESSAGE, "%*s<freq> for Opella-XD can be in range\n", GDBSERV_HELP_COL_WIDTH, " ");
		PrintMessage(HELP_MESSAGE, "%*sfrom 10kHz to 100MHz (default 1MHz) or RTCK.\n", GDBSERV_HELP_COL_WIDTH, " ");
		PrintMessage(HELP_MESSAGE, "%*sDefault Unit for Frequency is in MHz\n", GDBSERV_HELP_COL_WIDTH, " ");
	}
}

/****************************************************************************
	 Function: XML_GetTargetMemoryInfo
	 Engineer: Ashutosh Garg
		Input: TyXMLNode *ptyNode - pointer to Device node
	   Output: none
  Description: store the values Memory ranges mentioned in familyriscv.xml internally
Date           Initials    Description
14-MAY-2019		AG			Initial
*****************************************************************************/
void XML_GetTargetMemoryInfo(TyXMLNode *ptyNode)
{
	uint32_t uIndex = 0;
	TyXMLNode *ptyCurNode;
	uint32_t ulCount = 0;
	char *pszDevice ,*stopString;
	
	//get the node to the Memory tag
	ptyNode = XML_GetChildNode(ptyNode, "Memory");
	if (ptyNode == NULL)
		return;
	
	ptyCurNode = XML_GetChildNode(ptyNode, "MemArea");
	while (ptyCurNode != NULL)
	{
		//take the backup of node for next node address
		ptyNode = ptyCurNode;
		pszDevice = XML_GetAttrib(ptyCurNode, "Start");
		tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemAddress = strtoul(pszDevice + 2, &stopString, 16);

		pszDevice = XML_GetAttrib(ptyCurNode, "Size");
		tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemSize = strtoul(pszDevice, &stopString, 10);
		switch (*stopString)
		{
		case 'K': //KiloBytes
			tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemSize = tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemSize * 1024;
			break;
		case 'M': //MegaBytes
			tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemSize = tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemSize * 1024 * 1024;
		}

		pszDevice = XML_GetAttrib(ptyCurNode, "Access");
		if(pszDevice == NULL)
			tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemAccess = UNDEFINED_MEMORY_ACCESS;
		else {
			if (strcmp(pszDevice, "System_bus") == 0)
				tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemAccess = SYSTEM_BUS_MEMORY_ACCESS;
			else if (strcmp(pszDevice, "Program_buf") == 0)
				tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemAccess = PROGRAM_BUFFER_MEMORY_ACCESS;
			else if (strcmp(pszDevice, "Abstract") == 0)
				tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemAccess = ABSTRACT_COMMAND_MEMORY_ACCESS;
		}

		pszDevice = XML_GetAttrib(ptyCurNode, "Type");
		if(pszDevice == NULL) //if the 'Type' tag is not present the by default type is RW(read-write)
			tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemType = GDB_RISCV_READ_WRITE_MEM;
		else
		{
			if (strcmp(pszDevice, "RW") == 0)
				tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemType = GDB_RISCV_READ_WRITE_MEM;
			else if (strcmp(pszDevice, "RO") == 0)
				tySI.tyriscvMemoryAccess[ulCount].ulXMLRISCVMemType = GDB_RISCV_READ_ONLY_MEM;
		}

		//pointing to next node
		ptyCurNode = XML_GetNextNode(ptyNode);
		ulCount++;
	}
	//store the number of memory blocks mentioned in the familyriscv.xml file
	tySI.ulXMLRISCVMemBlockCount = ulCount;
}

/****************************************************************************
	 Function: XML_GetScanChainInfo
	 Engineer: Ashutosh Garg
		Input: TyXMLNode *ptyNode - pointer to Device node
	   Output: none
  Description: Store the scanchain info for selected device
Date           Initials    Description
20-MAY-2019		AG			Initial
*****************************************************************************/
uint32_t XML_GetScanChainInfo(TyXMLNode *ptyNode)
{
	TyXMLFile tyXMLSCFile;
	int32_t iError;
	char *pszDevice;

	//get the node to the CoreInfo tag
	ptyNode = XML_GetChildNode(ptyNode, "CoreInfo");
	if (ptyNode == NULL)
		return GDBSERV_NO_ERROR;			//no need to return error if CoreInfo Tag is not present it may contain a single tap.

	//node to the child tag of CoreInfo
	ptyNode = XML_GetChildNode(ptyNode, "xi:include");
	if (ptyNode == NULL)
		return GDBSERV_NO_ERROR;			//may contain coreInfo, can it be a empty tag?

	//get th CoreInfo file name
	pszDevice = XML_GetAttrib(ptyNode, "href");

	iError = XML_ReadFile(pszDevice, &tyXMLSCFile);
	if (iError != 0)
		return GDBSERV_ERROR;				//return error if not able to open file

	ptyNode = XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "TargetDefinition");
	if (ptyNode == NULL)
	{
		sprintf(tySI.szError, "Invalid scanchain file '%s',\ntag '<TargetDefinition>' not found.", tySI.tyXMLFile.szName);
		return GDBSERV_ERROR;
	}

	CFG_ScanChainNode(ptyNode);
	XML_Free(&tyXMLSCFile);

	return GDBSERV_NO_ERROR;
}