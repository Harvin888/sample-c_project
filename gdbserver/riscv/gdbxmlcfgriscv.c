/****************************************************************************
	   Module: gdbxmlcfgarc.c
	 Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, contains needed resources to parse
			   family specific XML file parameters
Date           Initials    Description
12-NOV-2007    NCH         initial
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdberr.h"

// external functions
extern TyServerInfo tySI;

// local functions
static void CFG_DebuggerControlNode(TyXMLNode *ptyThisNode);
static void CFG_ARCEmulatorNode(TyXMLNode *ptyThisNode);
void CFG_ScanChainNode(TyXMLNode *ptyThisNode);

/****************************************************************************
	 Function: CFG_DebuggerControlNode
	 Engineer: Nikolay Chokoev
		Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
	   Output: none
  Description: Get debugger control node
Date           Initials    Description
12-Nov-2007    NCH         Initial
*****************************************************************************/
static void CFG_DebuggerControlNode(TyXMLNode *ptyThisNode)
{
	tySI.tyProcConfig.ulMaxJtagFrequency = XML_GetChildElementDec(ptyThisNode, "MaxJtagFrequencyKhz", 0);
	tySI.tyProcConfig.ulMaxJtagFrequency *= FREQ_KHZ(1);                 // convert to Hz
}

/****************************************************************************
	 Function: CFG_ARCEmulatorNode
	 Engineer: Vitezslav Hola
		Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
	   Output: none
  Description: Get ARC emulator node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void CFG_ARCEmulatorNode(TyXMLNode *ptyThisNode)
{
	assert(ptyThisNode != NULL);
	tySI.ulTargetVoltagemV = TGTVOLTAGE_MATCH;                           // set default target voltage to match target
	if ((XML_GetChildNode(ptyThisNode, "TargetVoltage")) != NULL)
	{  // check target voltage type
		char *pszSupply = XML_GetChildAttrib(ptyThisNode, "TargetVoltage", "SupplyType");
		if (pszSupply != NULL)
		{
			if (!strcasecmp(pszSupply, TGTVOLTAGE_MATCH_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_MATCH;                  // match target voltage
			else if (!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_3V3_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(3300);         // fixed voltage 3.3V
			else if (!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_3V0_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(3000);         // fixed voltage 3.0V
			else if (!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_2V5_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(2500);         // fixed voltage 2.5V
			else if (!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_1V8_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(1800);         // fixed voltage 1.8V
			else if (!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_1V5_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(1500);         // fixed voltage 1.5V
			else if (!strcasecmp(pszSupply, TGTVOLTAGE_FIXED_1V2_NAME))
				tySI.ulTargetVoltagemV = TGTVOLTAGE_FIXED_MV(1200);         // fixed voltage 1.2V
		}
	}
}

/****************************************************************************
	 Function: CFG_ProcessorNode
	 Engineer: Nikolay Chokoev
		Input: TyXMLNode *ptyThisNode - pointer to XML node structure for this node
	   Output: none
  Description: Get processor node
Date           Initials    Description
12-Nov-2007    NCH         Initial
*****************************************************************************/
void CFG_ProcessorNode(TyXMLNode *ptyThisNode)
{
	TyXMLNode  *ptyNode;
	char       *pszName;

	assert(ptyThisNode != NULL);
	if ((pszName = XML_GetAttrib(ptyThisNode, "Device")) != NULL)
	{  // processor name
		strncpy(tySI.tyProcConfig.szProcName, pszName, sizeof(tySI.tyProcConfig.szProcName) - 1);
		tySI.tyProcConfig.szProcName[sizeof(tySI.tyProcConfig.szProcName) - 1] = '\0';
	}
	if ((pszName = XML_GetChildElement(ptyThisNode, "InternalName")) != NULL)
	{  // internal Name (for identification purposes)
		strncpy(tySI.tyProcConfig.szInternalName, pszName, sizeof(tySI.tyProcConfig.szInternalName) - 1);
		tySI.tyProcConfig.szInternalName[sizeof(tySI.tyProcConfig.szInternalName) - 1] = '\0';
	}
	tySI.tyProcConfig.uiInstrtoSelectADI = XML_GetChildElementHex(ptyThisNode, "InstructionToSelectADI", 0x0);
	if ((ptyNode = XML_GetChildNode(ptyThisNode, "MultiCore")) != NULL)
		CFG_ScanChainNode(ptyNode);
	if ((pszName = XML_GetChildElement(ptyThisNode, "GDBRegFile")) != NULL)
	{  // Register file send to GDB via feature description packet
		strncpy(tySI.tyProcConfig.szRegFileName, pszName, sizeof(tySI.tyProcConfig.szRegFileName) - 1);
		tySI.tyProcConfig.szRegFileName[sizeof(tySI.tyProcConfig.szRegFileName) - 1] = '\0';
	}
}

/****************************************************************************
	 Function: CFG_ProcessRegFile
	 Engineer: Nikolay Chokoev
		Input: none
	   Output: int32_t - error code
  Description: Process the register file
Date           Initials    Description
03-Jul-2008	   NCH         Initial
*****************************************************************************/
int32_t CFG_ProcessRegFile(void)
{
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
	 Function: CFG_ScanChainNode
	 Engineer: Rejeesh SB
		Input: TyXMLNode *ptyThisNode : pointer to Tap/core node
	   Output: none
  Description: Process Tap/core file 
Date           Initials    Description
22-May-2019	      RSB        Initial
*****************************************************************************/
void CFG_ScanChainNode(TyXMLNode *ptyThisNode)
{
	TyXMLNode      *ptyNode;
	uint32_t  ulIndex = 0;
	char *pszIsRiscvCore = NULL;
	// Process child nodes
	if ((ptyNode = XML_GetChildNode(ptyThisNode, "Tap")) != NULL)
	{
		do
		{
			ulIndex = XML_GetDec(XML_GetAttrib(ptyNode, "Number"), 0);
			pszIsRiscvCore = XML_GetAttrib(ptyNode, "IsRISCVCore");
			if (pszIsRiscvCore != NULL)
				tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].bIsRISCVCore = strncmp(pszIsRiscvCore, "true", 4) ? 0 : 1;
			else
				tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].bIsRISCVCore = 0;

			if (ulIndex > MAX_TAPS_IN_CHAIN)
				break;
			tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulIRlength = XML_GetDec(XML_GetChildElement(ptyNode, "IRlength"), 0);
			tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[ulIndex].ulBypassInst = XML_GetHex(XML_GetChildElement(ptyNode, "BypassInst"), 0);

		} while ((ptyNode = XML_GetNextNode(ptyNode)) != NULL);
	}
	tySI.tyProcConfig.Processor._RISCV.uiNumberofTAPs = ulIndex;
}
