/****************************************************************************
       Module: gdbxml.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, XML parser
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbglob.h" 
#include "gdbxml.h"
#include "gdbty.h"
#include "gdberr.h"
#include "gdbutil.h" 

// local defines
#define  GDBSERV_XML_ATTRIB_ALLOC_CHUNK   8

// external variables
extern TyServerInfo tySI;

// local function prototypes
static int32_t XML_OpenFile(const char * pszName, TyXMLFile *ptyFile);
static void XML_CloseFile(TyXMLFile *ptyFile);
static int32_t XML_Space(int32_t iChar);
static int32_t XML_Letter(int32_t iChar);
static int32_t XML_Number(int32_t iChar);
static int32_t XML_FirstIdChar(int32_t iChar);
static int32_t XML_IdChar(int32_t iChar);
static int32_t XML_NextChar(TyXMLFile *ptyFile, int32_t *piChar);
static int32_t XML_NextNonSpaceChar(TyXMLFile *ptyFile, int32_t *piChar);
static int32_t XML_SkipToLineEnd(TyXMLFile *ptyFile);
static int32_t XML_NextToken(TyXMLFile *ptyFile, char *pszToken, int32_t iTokenSize, TyXMLToken *ptyToken, int32_t bTestForElementText, int32_t bTestForCppComment);
static int32_t XML_AddAttribute(TyXMLNode *ptyNode, char * pszAttrib, char * pszValue);
static int32_t XML_ReadNode(TyXMLFile *ptyFile, TyXMLNode **pptyNode);
static void XML_FreeNode(TyXMLNode **pptyNode);
static int32_t XML_Parse(TyXMLFile *ptyFile);
static int32_t XML_SkipToEndofComment(TyXMLFile *ptyFile);

/****************************************************************************
     Function: XML_OpenFile
     Engineer: Vitezslav Hola
        Input: char *pszName - pointer to file name to open
               TyXMLFile *ptyFile - pointer to XML file structure
       Output: int32_t - error code
  Description: Open XML file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_OpenFile(const char *pszName, TyXMLFile *ptyFile)
{
   memset(ptyFile, 0, sizeof(TyXMLFile));
   ptyFile->iNextChar = 0;
   ptyFile->iLine     = 1;
   ptyFile->iIndent   = 0;
   // open in text mode so that CR+LF is converted to LF
   ptyFile->pFile = fopen(pszName, "rt");
   if (ptyFile->pFile == NULL)
      {
      sprintf(tySI.szError, "Cannot find device database file '%s'.", pszName);
      return GDBSERV_XML_ERROR;
      }
   strncpy(ptyFile->szName, pszName, sizeof(ptyFile->szName) -1);
   ptyFile->szName[sizeof(ptyFile->szName) -1] = '\0';
   return 0;
}

/****************************************************************************
     Function: XML_CloseFile
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
       Output: none
  Description: Close XML file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void XML_CloseFile(TyXMLFile *ptyFile)
{
   if (ptyFile->pFile != NULL)
      {
      (void)fclose(ptyFile->pFile);
      ptyFile->pFile = NULL;
      }
}

/****************************************************************************
     Function: XML_Space
     Engineer: Vitezslav Hola
        Input: int32_t iChar - character to test
       Output: int32_t - 1 if character is space, 0 otherwise
  Description: Check space character in XML.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_Space(int32_t iChar)
{
   return ((iChar == ' ') || ((iChar >= XML_TAB) && (iChar <= XML_CR)));
}

/****************************************************************************
     Function: XML_Letter
     Engineer: Vitezslav Hola
        Input: int32_t iChar - character to test
       Output: int32_t - 1 if character is letter, 0 otherwise
  Description: Check if character is letter.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_Letter(int32_t iChar)
{
   return (((iChar >= 'A') && (iChar <= 'Z')) || ((iChar >= 'a') && (iChar <= 'z')));
}

/****************************************************************************
     Function: XML_Number
     Engineer: Vitezslav Hola
        Input: int32_t iChar - character to test
       Output: int32_t - 1 if character is number, 0 otherwise
  Description: Check if character is number.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_Number(int32_t iChar)
{
   return ((iChar >= '0') && (iChar <= '9'));
}

/****************************************************************************
     Function: XML_FirstIdChar
     Engineer: Vitezslav Hola
        Input: int32_t iChar - character to test
       Output: int32_t - 1 if character is letter or '_', 0 otherwise
  Description: Check first id character.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_FirstIdChar(int32_t iChar)
{
   return (XML_Letter(iChar) || (iChar == '_'));
}

/****************************************************************************
     Function: XML_IdChar
     Engineer: Vitezslav Hola
        Input: int32_t iChar - character to test
       Output: int32_t - 1 of character is id, 0 otherwise
  Description: Check if character is id character.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_IdChar(int32_t iChar)
{
   return (XML_Letter(iChar) || XML_Number(iChar) || (iChar == '_') || (iChar == ':'));
}

/****************************************************************************
     Function: XML_NextChar
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
               int32_t *piChar - storage for character
       Output: int32_t - error code
  Description: Get next character from XML.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_NextChar(TyXMLFile *ptyFile, int32_t *piChar)
{
   if (ptyFile->iNextChar != 0)
      {
      *piChar = ptyFile->iNextChar;
      ptyFile->iNextChar = 0;
      }
   else
      {
      *piChar = fgetc(ptyFile->pFile);
      // expect only LF at line end as the file was opened in text mode
      if (*piChar == XML_LF)
         ptyFile->iLine++;
      }
   if (feof(ptyFile->pFile) || (*piChar == EOF))
      return GDBSERV_XML_END_OF_FILE;
   return 0;
}

/****************************************************************************
     Function: XML_NextNonSpaceChar
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
               int32_t *piChar - storage for character
       Output: int32_t - error code
  Description: Get next non-space character.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_NextNonSpaceChar(TyXMLFile *ptyFile, int32_t *piChar)
{
   int32_t iError;
   for (;;)
      {
      iError = XML_NextChar(ptyFile, piChar);
      if (iError != 0)
         return iError;
      if (!XML_Space(*piChar))
         break;
      }
   return 0;
}

/****************************************************************************
     Function: XML_SkipToLineEnd
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile pointer to XML file structure
       Output: int32_t - error code
  Description: Skip to line end.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_SkipToLineEnd(TyXMLFile *ptyFile)
{
   int32_t iError, iChar;
   for (;;)
      {
      iError = XML_NextChar(ptyFile, &iChar);
      if (iError != 0)
         return iError;
      // expect only LF at line end as the file was opened in text mode
      if (iChar == XML_LF)
         break;
      }
   return 0;
}

/****************************************************************************
     Function: XML_NextToken
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile  pointer to XML file structure
               char *pszToken - storage for token string
               int32_t iTokenSize - size of storage for token string
               TyXMLToken *ptyToken - storage for token enum
               int32_t bTestForElementText - if should test for elementtext at this point
               int32_t bTestForCppComment - if should test for C++ comment at this point
       Output: int32_t - error code
  Description: Get next token from XML.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
10-Oct-2012    SPT         Added XML comment cheking <!-- comments -->
*****************************************************************************/
static int32_t XML_NextToken(TyXMLFile *ptyFile, char *pszToken, int32_t iTokenSize, TyXMLToken *ptyToken, int32_t bTestForElementText, int32_t bTestForCppComment)
{
   int32_t iError, iChar, iTempChar, iIndex;
   fpos_t position;
   *ptyToken = XMLTokenUndefined;
   
   if (iTokenSize > 0)
      pszToken[0] = '\0';
   // get next character, skipping single line C++ comment, if requested...
   for (;;)
      {
      iError = XML_NextNonSpaceChar(ptyFile, &iChar);
      if (iError != 0)
         return iError;

      //Checking XML comment
      if ((char)iChar == '<')
         {
         iError = fgetpos (ptyFile->pFile, &position);
         if (iError != 0)
            return iError;
         iError = XML_NextChar(ptyFile, &iTempChar);
         if (iError != 0)
            return iError;
         if ((char)iTempChar != '!')
            {  // not a XML comment
            iError = fsetpos (ptyFile->pFile, &position);
            if (iError != 0)
               return iError;
            }
         else
            {
            iError = XML_NextChar(ptyFile, &iTempChar);
            if (iError != 0)
               return iError;
            if ((char)iTempChar != '-')
               {  // not a XML comment.
               iError = fsetpos (ptyFile->pFile, &position);
               if (iError != 0)
                  return iError;
               }
            else
               {
               iError = XML_NextChar(ptyFile, &iTempChar);
               if (iError != 0)
                  return iError;
               if ((char)iTempChar != '-')
                  {  // not a XML comment.
                  iError = fsetpos (ptyFile->pFile, &position);
                  if (iError != 0)
                     return iError;
                  }
               else
                  {
                  iError = XML_SkipToEndofComment(ptyFile);
                  if (iError != 0)
                     return iError;
                  continue;
                  }
               }
            }
         }

      if (!bTestForCppComment)
         break;
      if ((char)iChar != '/')
         break;
      // have first '/', test if really a C++ comment...
      iError = XML_NextChar(ptyFile, &iTempChar);
      if (iError != 0)
         return iError;
      if ((char)iTempChar != '/')
         {  // not a C++ comment, remember this character for next call to XML_NextChar
         ptyFile->iNextChar = iTempChar;
         break;
         }
      // it is a C++ comment, so skip it...
      iError = XML_SkipToLineEnd(ptyFile);
      if (iError != 0)
         return iError;
      }

   if (bTestForElementText)
      {  // testing if elementtext present eg: <node>elementtext</node>
      if ((char)iChar == '<')
         {  // no, must be: <node> <othernode..., remember this character for next call to XML_NextChar
         ptyFile->iNextChar = iChar;
         }
      else
         {  // yes, read elementtext from: <node>elementtext</node>
         *ptyToken = XMLTokenElementText;
         iIndex    = 0;
         for (;;)
            {  // discard above iTokenSize
            if (iIndex < iTokenSize-1)
               pszToken[iIndex++] = (char)iChar;
            iError = XML_NextChar(ptyFile, &iChar);
            if (iError != 0)
               return iError;
            if ((char)iChar == '<')
               {  // remember this character for next call to XML_NextChar
               ptyFile->iNextChar = iChar;
               break;
               }
            }
         if (iIndex < iTokenSize)
            pszToken[iIndex] = '\0';
         }
      return 0;
      }
   switch ((char)iChar)
      {
      case '<':
         *ptyToken = XMLTokenLess;
         break;
      case '>':
         *ptyToken = XMLTokenGreater;
         break;
      case '=':
         *ptyToken = XMLTokenEqual;
         break;
      case '/':
         *ptyToken = XMLTokenFSlash;
         break;
      case '\\':
         *ptyToken = XMLTokenBSlash;
         break;
      case '-':
         *ptyToken = XMLTokenMinus;
         break;
      case '?':
         *ptyToken = XMLTokenQuestion;
         break;
      case '!':
         *ptyToken = XMLTokenExclaim;
         break;
      case '"':
         *ptyToken = XMLTokenQuotedString;
         iIndex = 0;
         for (;;)
            {
            iError = XML_NextChar(ptyFile, &iChar);
            if (iError != 0)
               return iError;
            if ((char)iChar == '"')
               break;
            // discard above iTokenSize
            if (iIndex < iTokenSize-1)
               pszToken[iIndex++] = (char)iChar;
            }
         if (iIndex < iTokenSize)
            pszToken[iIndex] = '\0';
         break;
      default:
         if (XML_FirstIdChar(iChar))
            {
            *ptyToken = XMLTokenIdentifier;
            iIndex    = 0;
            for (;;)
               {
               // discard above iTokenSize
               if (iIndex < iTokenSize-1)
                  pszToken[iIndex++] = (char)iChar;
               iError = XML_NextChar(ptyFile, &iChar);
               if (iError != 0)
                  return iError;
               if (!XML_IdChar(iChar))
                  {  // remember this character for next call to XML_NextChar
                  ptyFile->iNextChar = iChar;
                  break;
                  }
               }
            if (iIndex < iTokenSize)
               pszToken[iIndex] = '\0';
            }
         break;
      }
   return 0;
}

/****************************************************************************
     Function: XML_AddAttribute
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - pointer to Node
               char *pszAttrib - pointer to attribute name
               char *pszValue - pointer to attribute value
       Output: int32_t - error code
  Description: Add one attribute="value" pair to ptyNode's attribute list
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_AddAttribute(TyXMLNode *ptyNode, char *pszAttrib, char *pszValue)
{
   char      **ppszOldAttrib;
   char      **ppszOldValue;
   int32_t         iNewSize, iOldSize;
   int32_t         iAttribLen, iValueLen;

   if (ptyNode == NULL)
      return 0;
   // Check if our Attrib list has room for one more attribute="value" pair...
   if (ptyNode->iAttribCount >= ptyNode->iAllocCount)
      {
      // No, allocate a new list...
      iOldSize      = ptyNode->iAllocCount * (int32_t)sizeof(char**);
      iNewSize      = (ptyNode->iAllocCount+GDBSERV_XML_ATTRIB_ALLOC_CHUNK) * (int32_t)sizeof(char**);
      ppszOldAttrib = ptyNode->ppszAttrib;
      ppszOldValue  = ptyNode->ppszValue;
      ptyNode->ppszAttrib = (char**)malloc((uint32_t)iNewSize);
      ptyNode->ppszValue  = (char**)malloc((uint32_t)iNewSize);
      if ((ptyNode->ppszAttrib == NULL) || (ptyNode->ppszValue == NULL))
         return GDBSERV_XML_NO_MEMORY_ERROR;
      ptyNode->iAllocCount += GDBSERV_XML_ATTRIB_ALLOC_CHUNK;
      memset(ptyNode->ppszAttrib, 0, (uint32_t)iNewSize);
      memset(ptyNode->ppszValue,  0, (uint32_t)iNewSize);
      if (ppszOldAttrib != NULL)
         {
         // copy the old Attrib list over, then free it...
         memcpy(ptyNode->ppszAttrib, ppszOldAttrib, (uint32_t)iOldSize);
         free(ppszOldAttrib);
         }
      if (ppszOldValue != NULL)
         {
         // copy the old Value list over, then free it...
         memcpy(ptyNode->ppszValue, ppszOldValue, (uint32_t)iOldSize);
         free(ppszOldValue);
         }
      }
   // allocate storage for the new Attrib...
   iAttribLen = (int32_t)strlen(pszAttrib);
   ptyNode->ppszAttrib[ptyNode->iAttribCount] = (char*)malloc((uint64_t)iAttribLen+1);
   if (ptyNode->ppszAttrib[ptyNode->iAttribCount] == NULL)
      return GDBSERV_XML_NO_MEMORY_ERROR;
   strncpy(ptyNode->ppszAttrib[ptyNode->iAttribCount], pszAttrib, (uint32_t)iAttribLen);
   ptyNode->ppszAttrib[ptyNode->iAttribCount][iAttribLen] = '\0';
   // allocate storage for the new Value...
   iValueLen = (int32_t)strlen(pszValue);
   ptyNode->ppszValue[ptyNode->iAttribCount] = (char*)malloc((uint64_t)iValueLen+1);
   if (ptyNode->ppszValue[ptyNode->iAttribCount] == NULL)
      return GDBSERV_XML_NO_MEMORY_ERROR;
   strncpy(ptyNode->ppszValue[ptyNode->iAttribCount], pszValue, (uint32_t)iValueLen);
   ptyNode->ppszValue[ptyNode->iAttribCount][iValueLen] = '\0';
   ptyNode->iAttribCount++;
   return 0;
}

/****************************************************************************
     Function: XML_ReadNode
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
               TyXMLNode **pptyNode - storage for pointer to Node
       Output: int32_t - error code
  Description: Read XML node.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_ReadNode(TyXMLFile *ptyFile, TyXMLNode **pptyNode)
{
   int32_t iError, bValidEndNode;
   TyXMLToken tyToken;
   TyXMLNode *ptyNode;
   char szToken[_MAX_PATH];
   char szAttrib[_MAX_PATH];

   *pptyNode = NULL;
   // get next token, skipping single line C++ comments, if any...
   iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 1);
   if (iError != 0)
      return iError;
   if (tyToken != XMLTokenLess)
      {
      sprintf(tySI.szError, "XML parser expected '<' at line %d of '%s'.", ptyFile->iLine, ptyFile->szName);
      return GDBSERV_XML_ERROR;
      }
   ptyNode = (TyXMLNode*)malloc(sizeof(TyXMLNode));
   if (ptyNode == NULL)
      return GDBSERV_XML_NO_MEMORY_ERROR;
   memset(ptyNode, 0, sizeof(TyXMLNode));
   *pptyNode = ptyNode;
   ptyNode->iIndent = ptyFile->iIndent;
   ptyNode->iLine   = ptyFile->iLine;
   iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0);
   if (iError != 0)
      return iError;
   if ((tyToken == XMLTokenFSlash) || (tyToken == XMLTokenQuestion) || (tyToken == XMLTokenExclaim))
      {
      if (tyToken == XMLTokenFSlash)
         ptyNode->tyBlock = XMLBlockEnd;
      else if (tyToken == XMLTokenExclaim) 
         ptyNode->tyBlock = XMLBlockDTD;
      else
         ptyNode->tyBlock = XMLBlockInfo;
      iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0);
      if (iError != 0)
         return iError;
      }
   else
      {
      ptyNode->tyBlock = XMLBlockStart;
      }
   if (tyToken != XMLTokenIdentifier)
      {
      sprintf(tySI.szError, "XML parser expected a tag name at line %d of '%s'.", ptyFile->iLine, ptyFile->szName);
      return GDBSERV_XML_ERROR;
      }
   ptyNode->pszNodeName = (char *)malloc(strlen(szToken)+1);
   if (ptyNode->pszNodeName == NULL)
      return GDBSERV_XML_NO_MEMORY_ERROR;
   strcpy(ptyNode->pszNodeName, szToken);
   // process if present: attribute="value" pairs
   for (;;)
      {
      iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0);
      if (iError != 0)
         return iError;
      if (tyToken == XMLTokenGreater)
         {
         break;
         }
      if (ptyNode->tyBlock == XMLBlockDTD) 
         {
         continue;
         }
      if ((tyToken == XMLTokenFSlash) || (tyToken == XMLTokenQuestion))
         {
         if (tyToken == XMLTokenFSlash)
            {
            ptyNode->tyBlock = XMLBlockStartEnd;
            }
         iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0);
         if (iError != 0)
            return iError;
         if (tyToken != XMLTokenGreater)
            {
            sprintf(tySI.szError, "XML parser expected '>' at line %d of '%s'.", ptyFile->iLine, ptyFile->szName);
            return GDBSERV_XML_ERROR;
            }
         break;
         }
      if (tyToken != XMLTokenIdentifier)
         {
         sprintf(tySI.szError, "XML parser expected an attribute name at line %d of '%s'.", ptyFile->iLine, ptyFile->szName);
         return GDBSERV_XML_ERROR;
         }
      // save attribute name for now...
      strcpy(szAttrib, szToken);
      iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0);
      if (iError != 0)
         return iError;
      if (tyToken != XMLTokenEqual)
         {
         sprintf(tySI.szError, "XML parser expected '=' after '%s' at line %d of '%s'.", szAttrib, ptyFile->iLine, ptyFile->szName);
         return GDBSERV_XML_ERROR;
         }

      iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0);
      if (iError != 0)
         return iError;
      if (tyToken != XMLTokenQuotedString)
         {
         sprintf(tySI.szError, "XML parser expected an attribute value after '%s='\nat line %d of '%s'.", szAttrib, ptyFile->iLine, ptyFile->szName);
         return GDBSERV_XML_ERROR;
         }
      // save attribute="value" pair
      if ((iError = XML_AddAttribute(ptyNode, szAttrib, szToken)) != 0)
         return iError;
      }
   if ((ptyNode->tyBlock == XMLBlockStart) && (ptyNode->iAttribCount == 0))
      {
      // test for: <node>elementtext</node>
      if ((iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 1, 1)) != 0)
         return iError;
      if (tyToken == XMLTokenElementText)
         {
         ptyNode->tyBlock = XMLBlockElementText;
         ptyNode->pszElementText = (char *)malloc(strlen(szToken)+1);
         if (ptyNode->pszElementText == NULL)
            return GDBSERV_XML_NO_MEMORY_ERROR;
         strcpy(ptyNode->pszElementText, szToken);
         // ensure elementtext is followed by </node>
         bValidEndNode = 0;
         if ((iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0)) != 0)
            return iError;
         if (tyToken == XMLTokenLess)
            {
            if ((iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0)) != 0)
               return iError;
            if (tyToken == XMLTokenFSlash)
               {
               if ((iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0)) != 0)
                  return iError;
               if ((tyToken == XMLTokenIdentifier) &&
                   (strcmp(szToken, ptyNode->pszNodeName) == 0))
                  {
                  if ((iError = XML_NextToken(ptyFile, szToken, sizeof(szToken), &tyToken, 0, 0)) != 0)
                     return iError;
                  if (tyToken == XMLTokenGreater)
                     {
                     bValidEndNode = 1;
                     }
                  }
               }
            }
         if (!bValidEndNode)
            {
            sprintf(tySI.szError, "XML parser expected end tag '</%s>'\nat line %d of '%s'.", ptyNode->pszNodeName, ptyFile->iLine, ptyFile->szName);
            return GDBSERV_XML_ERROR;
            }
         }
      }
   if (ptyNode->tyBlock == XMLBlockStart)
      ptyFile->iIndent++;
   else if (ptyNode->tyBlock == XMLBlockEnd)
      ptyFile->iIndent--;
   return 0;
}

/****************************************************************************
     Function: XML_FreeNode
     Engineer: Vitezslav Hola
        Input: TyXMLNode **pptyNode - pointer to pointer to Node to free
       Output: none
  Description: Frees all storage belonging to this node, nodes linked to this 
               with pNext and all their children (pChild link).
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void XML_FreeNode(TyXMLNode **pptyNode)
{
   int32_t i;
   TyXMLNode *ptyNode;
   TyXMLNode *ptyPrev;
   if (pptyNode == NULL)
      return;
   ptyNode = *pptyNode;
   while (ptyNode != NULL)
      {  // free children, if any
      if (ptyNode->pChild)
         XML_FreeNode(&ptyNode->pChild);
      if (ptyNode->pszNodeName)
         free(ptyNode->pszNodeName);
      if (ptyNode->pszElementText)
         free(ptyNode->pszElementText);
      // free attribute="value" pairs, if any
      if (ptyNode->ppszAttrib)
         {
         for (i=0; i<ptyNode->iAttribCount; i++)
            {
            if (ptyNode->ppszAttrib[i])
               free(ptyNode->ppszAttrib[i]);
            }
         free(ptyNode->ppszAttrib);
         }
      if (ptyNode->ppszValue)
         {
         for (i=0; i<ptyNode->iAttribCount; i++)
            {
            if (ptyNode->ppszValue[i])
               free(ptyNode->ppszValue[i]);
            }
         free(ptyNode->ppszValue);
         }
      ptyPrev  = ptyNode;
      ptyNode  = ptyNode->pNext;
      free(ptyPrev);
      }
   *pptyNode = NULL;
}

/****************************************************************************
     Function: XML_Parse
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
       Output: int32_t - error code
  Description: Parse XML.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t XML_Parse(TyXMLFile *ptyFile)
{
   int32_t iError;
   TyXMLNode **pptyNext = &ptyFile->ptyNodeTree;
   TyXMLNode *ptyCurrent = NULL;
   TyXMLNode *ptyParent = NULL;

   for (;;)
      {
      if ((iError = XML_ReadNode(ptyFile, &ptyCurrent)) != 0)
      {
         if ((iError == GDBSERV_XML_END_OF_FILE) && (ptyParent == NULL))
            iError = 0;

         return iError;
      }

      if (ptyCurrent == NULL)
      {
         PrintMessage(INFO_MESSAGE, "GDBSERV_XML_NO_MEMORY_ERROR.");
         return GDBSERV_XML_NO_MEMORY_ERROR;
      }
      // don't store end node, use to adjust parent and next nodes link position
      if (ptyCurrent->tyBlock == XMLBlockEnd)
         {
         if ((ptyParent == NULL) || (ptyParent->tyBlock != XMLBlockStart) || (strcmp(ptyCurrent->pszNodeName, ptyParent->pszNodeName) != 0))
            {
            sprintf(tySI.szError, "XML parser found unmatched end tag '</%s>'\nat line %d of '%s'.", ptyCurrent->pszNodeName, ptyCurrent->iLine, ptyFile->szName);
            return GDBSERV_XML_ERROR;
            }
         XML_FreeNode(&ptyCurrent);
         pptyNext  = &ptyParent->pNext;
         ptyParent = ptyParent->pParent;
         continue;
         }
      // remember this nodes parent
      ptyCurrent->pParent = ptyParent;
      // link this node to the tree
      *pptyNext = ptyCurrent;
      // determine the link in position for the next node
      if (ptyCurrent->tyBlock == XMLBlockStart)
         {
         pptyNext  = &ptyCurrent->pChild;
         ptyParent = ptyCurrent;
         }
      else
         {
         pptyNext  = &ptyCurrent->pNext;
         // next has same parent
         }
      }
   // unreachable
}


/****************************************************************************
     Function: XML_Print
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - pointer to Node to print
       Output: none
  Description: Prints this node and all child nodes, useful for debug purposes
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void XML_Print(TyXMLNode *ptyNode)
{
   int32_t i, iSpace;
   while (ptyNode != NULL)
      {  // new line and indentation
      iSpace = ptyNode->iIndent * XML_SPACES_PER_TAB;
      if (iSpace < 0)
         iSpace = 0;
      printf("\n%-*s", iSpace, "");
      printf("<");
      if (ptyNode->tyBlock == XMLBlockInfo)
         printf("?");
      if (ptyNode->tyBlock == XMLBlockDTD)
         printf("!");
      printf("%s", ptyNode->pszNodeName);
      // print if present: attribute="value" pairs
      for (i=0; i<ptyNode->iAttribCount; i++)
         {
         if ((ptyNode->ppszAttrib[i] != NULL) &&
             (ptyNode->ppszValue [i] != NULL))
            {
            printf(" %s=\"%s\"", ptyNode->ppszAttrib[i], ptyNode->ppszValue[i]);
            }
         }
      if (ptyNode->tyBlock == XMLBlockInfo)
         printf("?");
      else if (ptyNode->tyBlock == XMLBlockStartEnd)
         printf("/");
      printf(">");
      if ((ptyNode->tyBlock == XMLBlockElementText) && (ptyNode->pszElementText))
         {
         printf("%s</%s>", ptyNode->pszElementText, ptyNode->pszNodeName);
         }
      // print children nodes, if any
      XML_Print(ptyNode->pChild);
      if (ptyNode->tyBlock == XMLBlockStart)
         {// print a corresponding end node (on a new line if children): </node>
         if (ptyNode->pChild)
            printf("\n%-*s", iSpace, "");
         printf("</%s>", ptyNode->pszNodeName);
         }
      ptyNode = ptyNode->pNext;
      }
}

/****************************************************************************
     Function: XML_PrintToFile
     Engineer: Shameerudheen P T
        Input: TyXMLNode *ptyNode - pointer to Node to print
        Input: FILE * pfile - Pointer to a File
       Output: none
  Description: Prints this node and all child nodes to a file, useful for debug purposes
Date           Initials    Description
12-Oct-2012    SPT         Initial & cleanup
*****************************************************************************/
void XML_PrintToFile(TyXMLNode *ptyNode, FILE * pfile)
{
   int32_t i, iSpace;
   while (ptyNode != NULL)
      {  // new line and indentation
      iSpace = ptyNode->iIndent * XML_SPACES_PER_TAB;
      if (iSpace < 0)
         iSpace = 0;
      fprintf(pfile,"\n%-*s", iSpace, "");
      fprintf(pfile,"<");
      if (ptyNode->tyBlock == XMLBlockInfo)
         fprintf(pfile,"?");
      if (ptyNode->tyBlock == XMLBlockDTD)
         fprintf(pfile,"!");
      fprintf(pfile,"%s", ptyNode->pszNodeName);
      // print if present: attribute="value" pairs
      for (i=0; i<ptyNode->iAttribCount; i++)
         {
         if ((ptyNode->ppszAttrib[i] != NULL) &&
             (ptyNode->ppszValue [i] != NULL))
            {
            fprintf(pfile," %s=\"%s\"", ptyNode->ppszAttrib[i], ptyNode->ppszValue[i]);
            }
         }
      if (ptyNode->tyBlock == XMLBlockInfo)
         fprintf(pfile,"?");
      else if (ptyNode->tyBlock == XMLBlockStartEnd)
         fprintf(pfile,"/");
      fprintf(pfile,">");
      if ((ptyNode->tyBlock == XMLBlockElementText) && (ptyNode->pszElementText))
         {
         fprintf(pfile,"%s</%s>", ptyNode->pszElementText, ptyNode->pszNodeName);
         }
      // print children nodes, if any
      XML_PrintToFile(ptyNode->pChild, pfile);
      if (ptyNode->tyBlock == XMLBlockStart)
         {// print a corresponding end node (on a new line if children): </node>
         if (ptyNode->pChild)
            fprintf(pfile,"\n%-*s", iSpace, "");
         fprintf(pfile,"</%s>", ptyNode->pszNodeName);
         }
      ptyNode = ptyNode->pNext;
      }
}

/****************************************************************************
     Function: XML_ReadFile
     Engineer: Vitezslav Hola
        Input: char *pszName - pointer to file name to open
               TyXMLFile *ptyFile - poincter to XML file structure
       Output: int32_t - error code
  Description: Read XML file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t XML_ReadFile(const char *pszName, TyXMLFile *ptyFile)
{
   int32_t iError = 0;
#ifdef ARC
   FILE * pfile;
#endif   

   if ((iError = XML_OpenFile(pszName, ptyFile)) != 0)
   {
       PrintMessage(ERROR_MESSAGE, "Unable to open %s file", pszName);
       return iError;
   }

   if ((iError = XML_Parse(ptyFile)) == GDBSERV_XML_NO_MEMORY_ERROR)
      {
      sprintf(tySI.szError, "Insufficient memory while parsing device database file '%s'.", ptyFile->szName);
      }
#ifdef ARC
   pfile = fopen("XMLoutput.xml","w");
   if (pfile == NULL)
   {
       PrintMessage(ERROR_MESSAGE, "pfile == NULL");
       return 1;
   }

   XML_PrintToFile(ptyFile->ptyNodeTree, pfile);

   fclose(pfile);
#endif   

   XML_CloseFile(ptyFile);

   return iError;
}

/****************************************************************************
     Function: XML_Free
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
       Output: none
  Description: Free XML.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void XML_Free(TyXMLFile *ptyFile)
{
   if (ptyFile == NULL)
      return;
   XML_FreeNode(&(ptyFile->ptyNodeTree));
}

/****************************************************************************
     Function: XML_GetHex
     Engineer: Vitezslav Hola
        Input: char *pszHex - pointer to hex string or NULL
               uint32_t ulDefault - default value if pszHex NULL or text invalid
       Output: uint32_t - hex value
  Description: Get hex value of string, expect XML hexbinary strings.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
uint32_t XML_GetHex(char *pszHex, uint32_t ulDefault)
{
   uint32_t ulHex = 0;
   if (pszHex == NULL)
      return ulDefault;
   if (sscanf(pszHex, "%x", &ulHex) != 1)
      return ulDefault;
   return ulHex;
}

/****************************************************************************
     Function: XML_GetDec
     Engineer: Vitezslav Hola
        Input: char *pszDec - pointer to dec string or NULL
               uint32_t ulDefault - default value if pszDec NULL or text invalid
       Output: uint32_t - dec value
  Description: Get dec value of string, expect XML dec strings.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
uint32_t XML_GetDec(char *pszDec, uint32_t ulDefault)
{
   uint32_t ulDec = 0;
   if (pszDec == NULL)
      return ulDefault;
   if (sscanf(pszDec, "%u", &ulDec) != 1)
      return ulDefault;
   return ulDec;
}

/****************************************************************************
     Function: XML_GetBaseNode
     Engineer: Vitezslav Hola
        Input: TyXMLFile *ptyFile - pointer to XML file structure
       Output: TyXMLNode* - pointer to base node if any
  Description: Get the base node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
TyXMLNode *XML_GetBaseNode(TyXMLFile *ptyFile)
{
   if (ptyFile == NULL)
      return NULL;
   return ptyFile->ptyNodeTree;
}

/****************************************************************************
     Function: XML_GetNode
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - node name of sibling
       Output: TyXMLNode * - pointer to node if found
  Description: Get a sibling node with the given name
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
TyXMLNode *XML_GetNode(TyXMLNode *ptyNode, const char *pszNodeName)
{
   if (pszNodeName == NULL)
      return NULL;

   while (ptyNode != NULL)
      {
      if ((ptyNode->pszNodeName) &&
          (strcmp(ptyNode->pszNodeName, pszNodeName) == 0))
         return ptyNode;
      ptyNode = ptyNode->pNext;
      }

   return NULL;
}

/****************************************************************************
     Function: XML_GetChildNode
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a parent node
               char *pszNodeName - child node name to get
       Output: TyXMLNode * - pointer to node if found
  Description: Get a child node with the given name
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
TyXMLNode *XML_GetChildNode(TyXMLNode *ptyNode, const char *pszNodeName)
{
   if (ptyNode == NULL)
      return NULL;
   return XML_GetNode(ptyNode->pChild, pszNodeName);
}

/****************************************************************************
     Function: XML_GetNextNode
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
       Output: TyXMLNode * - pointer to next node if any
  Description: Get the next node for a given node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
TyXMLNode *XML_GetNextNode(TyXMLNode *ptyNode)
{
   if (ptyNode == NULL)
      return NULL;
   return ptyNode->pNext;
}

/****************************************************************************
     Function: XML_GetChildNodeCount
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - parent node
               char *pszNodeName - child node name to count, NULL to count all children
       Output: int32_t - the count
  Description: Count child nodes with the given name
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t XML_GetChildNodeCount(TyXMLNode *ptyNode, char *pszNodeName)
{
   int32_t iCount;
   if (ptyNode == NULL)
      return 0;
   ptyNode = ptyNode->pChild;
   iCount  = 0;
   while (ptyNode != NULL)
      {
      if (pszNodeName)
         {
         if ((ptyNode->pszNodeName) &&
             (strcmp(ptyNode->pszNodeName, pszNodeName) == 0))
            iCount++;
         }
      else
         {
         // count all nodes
         iCount++;
         }
      ptyNode = ptyNode->pNext;
      }
   return iCount;
}

/****************************************************************************
     Function: XML_GetAttrib
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszAttrib - attribute name
       Output: char * - pointer to attribute value if found
  Description: Get an attribute value for a node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
char *XML_GetAttrib(TyXMLNode *ptyNode, const char *pszAttrib)
{
   int32_t iAttrib;

   if ((ptyNode == NULL) || (pszAttrib == NULL))
      return NULL;

   for (iAttrib=0; iAttrib<ptyNode->iAttribCount; iAttrib++)
      {
      if (strcmp(ptyNode->ppszAttrib[iAttrib], pszAttrib) == 0)
         return ptyNode->ppszValue[iAttrib];
      }
   return NULL;
}

/****************************************************************************
     Function: XML_GetState
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
       Output: int32_t - 1 if State="true" attribute is present
  Description: Get State attribute for a node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t XML_GetState(TyXMLNode *ptyNode)
{
   char *pszState;
   if ((pszState = XML_GetAttrib(ptyNode, "State")) == NULL)
      return 0;
   if (strcasecmp(pszState, "true") == 0)
      return 1;
   return 0;
}

/****************************************************************************
     Function: XML_GetElement
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
       Output: char * - pointer to element text if found
  Description: Get element text for a node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
char *XML_GetElement(TyXMLNode *ptyNode)
{
   if (ptyNode == NULL)
      return NULL;
   return ptyNode->pszElementText;
}

/****************************************************************************
     Function: XML_GetChildAttrib
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - child node name
               char *pszAttrib - attribute name
       Output: char * - pointer to attribute value if found
  Description: Get an attribute value for a child node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
char *XML_GetChildAttrib(TyXMLNode *ptyNode, const char *pszNodeName, const char *pszAttrib)
{
   return XML_GetAttrib(XML_GetChildNode(ptyNode, pszNodeName), pszAttrib);
}

/****************************************************************************
     Function: XML_GetChildState
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - child node name
       Output: int32_t - 1 if State="true" attribute is present
  Description: Get State attribute for a node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t XML_GetChildState(TyXMLNode *ptyNode, const char *pszNodeName)
{
   return XML_GetState(XML_GetChildNode(ptyNode, pszNodeName));
}

/****************************************************************************
     Function: XML_GetChildElement
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - child node name
       Output: char * - pointer to element text if found
  Description: Get element text for a child node
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
char *XML_GetChildElement(TyXMLNode *ptyNode, const char *pszNodeName)
{
   return XML_GetElement(XML_GetChildNode(ptyNode, pszNodeName));
}

/****************************************************************************
     Function: XML_GetElementHex
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               uint32_t ulDefault - default value if element text not available
       Output: uint32_t - hex value of element text
  Description: Get hex value of element text for a node
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t XML_GetElementHex(TyXMLNode *ptyNode, uint32_t ulDefault)
{
   return XML_GetHex(XML_GetElement(ptyNode), ulDefault);
}

/****************************************************************************
     Function: XML_GetElementDec
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               uint32_t ulDefault - default value if element text not available
       Output: uint32_t        : dec value of element text
  Description: Get dec value of element text for a node
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t XML_GetElementDec(TyXMLNode *ptyNode, uint32_t ulDefault)
{
   return XML_GetDec(XML_GetElement(ptyNode), ulDefault);
}

/****************************************************************************
     Function: XML_GetChildElementHex
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - child node name
               uint32_t ulDefault - default value if element text not available
       Output: uint32_t - hex value of element text
  Description: Get hex value of element text for a child node
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t XML_GetChildElementHex(TyXMLNode *ptyNode, const char *pszNodeName, uint32_t ulDefault)
{
   return XML_GetHex(XML_GetChildElement(ptyNode, pszNodeName), ulDefault);
}

/****************************************************************************
     Function: XML_GetAttribHex
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszAttrib - attribute name
               uint32_t ulDefault - default value if Attrib text not available
       Output: uint32_t  - hex value of Attrib text
  Description: Get hex value of Attrib text for a node
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t XML_GetAttribHex(TyXMLNode *ptyNode, char *pszAttrib, uint32_t ulDefault)
{
   return XML_GetHex(XML_GetAttrib(ptyNode, pszAttrib), ulDefault);
}

/****************************************************************************
     Function: XML_GetChildAttribHex
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - child node name
               char *pszAttrib - attribute name
               uint32_t ulDefault - default value if Attrib text not available
       Output: uint32_t - hex value of Attrib text
  Description: Get hex value of Attrib text for a child node
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
uint32_t XML_GetChildAttribHex(TyXMLNode *ptyNode, char *pszNodeName, char *pszAttrib, uint32_t ulDefault)
{
   return XML_GetHex(XML_GetChildAttrib(ptyNode, pszNodeName, pszAttrib), ulDefault);
}

/****************************************************************************
     Function: XML_GetChildElementDec
     Engineer: Vitezslav Hola
        Input: TyXMLNode *ptyNode - a node
               char *pszNodeName - child node name
               uint32_t ulDefault - default value if element text not available
       Output: uint32_t - dec value of element text
  Description: Get dec value of element text for a child node
Date           Initials    Description
12-Jun-2007    VH          Initial
*****************************************************************************/
uint32_t XML_GetChildElementDec(TyXMLNode *ptyNode, const char *pszNodeName, uint32_t ulDefault)
{
   return XML_GetDec(XML_GetChildElement(ptyNode, pszNodeName), ulDefault);
}

/****************************************************************************
     Function: XML_SkipToEndofComment
     Engineer: Shameerudheen P T
        Input: TyXMLFile *ptyFile pointer to XML file structure
       Output: int32_t - error code
  Description: Skip to end of comment.
Date           Initials    Description
10-Oct-2012    SPT         Initial & cleanup
*****************************************************************************/
static int32_t XML_SkipToEndofComment(TyXMLFile *ptyFile)
{
   int32_t iError, iChar, iTempChar;
   for (;;)
      {
      iError = XML_NextChar(ptyFile, &iChar);
      if (iError != 0)
         return iError;
      if (iChar != '-')
         continue;
      iError = XML_NextChar(ptyFile, &iTempChar);
      if (iError != 0)
         return iError;
      if (iTempChar != '-')
         continue;
      iError = XML_NextChar(ptyFile, &iTempChar);
      if (iError != 0)
         return iError;
      if (iTempChar != '>')
         return GDBSERV_XML_ERROR;
      else
         break;
      }
   return 0;
}


