/****************************************************************************
       Module: gdblow.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, Low level target interface
Date           Initials    Description
10-Jan-2008    NCH         Initial
08-Feb-2008    VH          Cleanup
31-Mar-2008    RS/VK       Modified for ARM support
23-Sep-2009    SVL         Modified for PFXD mode
16-Oct-2009    SVL         Added vector catch support
3-Nov-2009     SVL         Added support for Device register file
****************************************************************************/
#ifndef GDBLOW_H_
#define GDBLOW_H_


// Linux build change...
#ifndef __LINUX
#define stricmp _stricmp
#define putenv _putenv
#define strdup _strdup
#endif
#ifdef RISCV
uint32_t  Hw_array[10];
#define REG_S0                     0x8
#define REG_S1                     0x9

int32_t LOW_Connect(const char *,char *, char *,char *);
#else
int32_t LOW_Connect(void);
#endif

int32_t LOW_LoadLibrary();
int32_t LOW_Disconnect(void);
int32_t LOW_IsProcessorExecuting(volatile int32_t *pbExecuting,
	int32_t iCurrentCoreIndex);
int32_t LOW_GetCauseOfBreak(TyStopSignal *ptyStopSignal);
int32_t LOW_ReadRegisters(uint32_t *pulRegisters);
int32_t LOW_ReadRegister(uint32_t   ulRegisterNumber,
	uint32_t *pulRegisterValue);
int32_t LOW_WriteRegisters(uint32_t *pulRegisters);
#ifdef RISCV
int32_t LOW_WriteRegister(uint32_t ulRegisterNumber,
	uint32_t *ulRegisterValue);
int32_t LOW_addTrigger(uint32_t ulTriggerAddress, uint32_t ulTriggerNumber, uint32_t ulLength, uint32_t ulMatch, uint32_t ulResrequired, uint32_t *ulOut, uint32_t ulType);
int32_t LOW_RemoveTrigger(uint32_t ulTriggerNumber, uint32_t ulResUsed);
int32_t LOW_WriteCSR(uint32_t *ulRegisterValue, uint32_t ulRegisterNumber);
int32_t LOW_WritePC(uint32_t *ulRegisterValue);
int32_t LOW_SetRiscvMemAccessMechanism(uint32_t ulAddress);
int32_t LOW_TargetReset(uint32_t ulHartId);
int32_t LOW_WritePC(uint32_t *ulRegisterValue);
#else
int32_t LOW_WriteRegister(uint32_t ulRegisterNumber, 
					  uint32_t ulRegisterValue);
int32_t LOW_WritePC(uint32_t ulRegisterValue);					  
#endif
int32_t LOW_ReadPC(uint32_t  *pulRegisterValue);

int32_t LOW_ReadMemory(uint32_t ulAddress,
	uint32_t ulCount,
	unsigned char *pucData);
int32_t LOW_WriteMemory(uint32_t ulAddress,
	uint32_t ulCount,
	unsigned char *pucData);
int32_t LOW_SupportedBreakpoint(uint32_t ulBpType);
int32_t LOW_DeleteBreakpoint(uint32_t ulBpType,
	uint32_t ulAddress,
	uint32_t ulLength,
	int32_t iCurrentCoreIndex);
int32_t LOW_DeleteAdvancedBreakpoint(uint32_t ulAddress);
int32_t LOW_SetBreakpoint(uint32_t ulBpType,
	uint32_t ulAddress,
	uint32_t ulLength,
	int32_t iCurrentCoreIndex);
int32_t LOW_CheckForWatchpoints(int32_t iCurrentCoreIndex);
int32_t LOW_RemoveWatchpointInternal(int32_t ulThredId, int32_t iCurrentCoreIndex);
int32_t LOW_AddWatchpointInternal(int32_t ulThredId, int32_t iCurrentCoreIndex);
int32_t LOW_SetAdvanceBreakpoint(uint32_t ulBpType,
	uint32_t ulAddress,
	uint32_t ulLength,
	uint32_t ulData,
	uint32_t ulDataMask,
	int32_t ibASIDenable,
	uint32_t ulASIDValue,
	uint32_t ulTrasType);
int32_t LOW_Continue(TyStopSignal *ptyStopSignal,
	int32_t iCurrentCore);
int32_t LOW_RestoreFPS1(void);
int32_t LOW_BackupFPS1(void);
int32_t LOW_Step(TyStopSignal *ptyStopSignal);
int32_t LOW_Halt(int32_t iCurrentCoreIndex);
int32_t LOW_RiscvMechanism(uint32_t ulMechanism);
int32_t LOW_ResetTarget(void);
int32_t LOW_WriteFp(void);
uint32_t LOW_GetThreadId(char *pcInput);
int32_t LOW_Discovery(void);
int32_t LOW_Discover_Harts(void);
int32_t LOW_CurrentHart();
int32_t LOW_TriggerInfo(void);
int32_t LOW_GetDWFWVersions(char *pszDiskWare1,
	char *pszFirmWare,
	char *pszFpgaWare1,
	char *pszFpgaWare2,
	char *pszFpgaWare3,
	char *pszFpgaWare4);
int32_t LOW_IsBigEndian(int32_t iCoreIndex);
int32_t LOW_ScanIR(uint32_t *pulDataIn,
	uint32_t *pulDataOut,
	uint32_t ulDataLength);
int32_t LOW_ScanDR(uint32_t *pulDataIn,
	uint32_t *pulDataOut,
	uint32_t ulDataLength);
int32_t LOW_ScanDRLong(uint32_t *pulDataIn,
	uint32_t *pulDataOut,
	uint32_t ulDataLength);
int32_t LOW_ConnectJcon(int32_t bInitialJtagAccess);
int32_t LOW_DisconnectJcon(void);
int32_t LOW_ResetTAP(void);
int32_t LOW_SetJtagFreq(void);
int32_t LOW_Execute(char* pszDownloadFileName);
//int32_t LOW_ReadRegisterByRegDef(TyRegDef  *ptyRegDef, uint32_t *pulRegValue);
//int32_t LOW_WriteRegisterByRegDef(TyRegDef  *ptyRegDef, uint32_t ulRegValue);
int32_t LOW_AddCoreRegDefArrayEntries(void);
int32_t LOW_IndexCommonRegDefArrayEntries(void);
int32_t LOW_vContPacket(char *pcInput, char *pcOutput, uint32_t uiOutputSize, int32_t iCurrentCoreIndex);
int32_t LOW_Detach(int32_t iCurrentCoreIndex);
int32_t LOW_DetectScanChain(uint32_t *pulNumCores);
int32_t LOW_CloseDevice(int32_t iIndex);
/* To load the RDI library when started in PFXD mode */
int32_t LOW_StartPFXDMode(void);
/* To connect to the debug target through 'monitor connect',
when started in PFXD mode */
int32_t LOW_PFXDConnect(void);
#ifdef ARM //todo complete interface
void LOW_SetActiveTapIndex(int32_t iIndex);
int32_t LOW_ScanIRScanDR(uint32_t * pulDataIn, uint32_t *ulLength, uint32_t *pulDataOut);
int32_t LOW_DeleteWatchpoints(void);
//int32_t LOW_DeleteWatchpoint(uint32_t ulWpIndex);
int32_t Low_ConfigureSemihost(uint32_t ultop, uint32_t ulvector, uint32_t ulSemiState);
int32_t LOW_DeleteWatchpoint(uint32_t ulAddress, uint32_t ulData, uint32_t ulWpType, int32_t iCurrentCoreIndex);
int32_t LOW_SetWatchpoint(uint32_t ulAddress, uint32_t uldata, uint32_t DataType, uint32_t uiBPType);
void LOW_SoftReset(void);
int32_t LOW_ProcessCP15Reg(uint32_t uiOperation, uint32_t uiCRn, uint32_t uiCRm, uint32_t uiOp1, uint32_t uiOp2, uint32_t *pulData);
int32_t LOW_ReadCPReg(uint32_t ulCPNo, uint32_t ulRegIndex, uint32_t *pulData);
int32_t LOW_WriteCPReg(uint32_t ulCPNo, uint32_t ulRegIndex, uint32_t ulData);
int32_t LOW_CfgVectorCatch(uint32_t *pulvectorCatchCfg);
int32_t LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef,
	uint32_t *pulRegValue);
int32_t LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef,
	uint32_t ulRegValue);
#else
int32_t LOW_PostBreakAction(int32_t iCurrentCoreIndex);
int32_t LOW_FlushDCache(int32_t iCurrentCoreIndex);
int32_t LOW_InvalidateDCache(int32_t iCurrentCoreIndex);
#endif

//todo
#ifdef MIPS
int32_t LOW_ReadMem(uint32_t ulStartAddr,
	uint32_t ulLength,
	unsigned char *pbBuffer,
	uint32_t ulDataSize);
int32_t LOW_WriteMem(uint32_t ulStartAddr,
	uint32_t ulLength,
	uint32_t ulDataSize,
	uint32_t ulValue);
int32_t Low_ReadCP0Reg(uint32_t uiRegNumber, uint32_t ulSelect, uint32_t *pulDataValue);
int32_t Low_WriteCP0Reg(uint32_t uiRegNumber, uint32_t ulSelect, uint32_t ulDataValue);
#endif
//todo
#endif // GDBLOW_H_

