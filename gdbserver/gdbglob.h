/****************************************************************************
       Module: gdbglob.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, global header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-08      VK/RS       Modified for ARM support
****************************************************************************/
#ifndef GDBGLOB_H_
#define GDBGLOB_H_

// Architecture Specific
#if defined(MIPS)
#define GDBSERV_PROG_NAME              "ash-mips-gdb-server"
#define GDBSERV_PROC_FAMILY            _MIPS
#define GDBSERV_ARCH_NAME              "MIPS"
#define GDBSERV_XML_FAMILY_FILE        "famlymip.xml"
#define GDBSERV_NUM_REGISTERS          38
#define GDBSERV_MAX_MEMORY_SIZE        0x10000
#define GDBSERV_PROG_TITLE             "Ashling GDB Server for "GDBSERV_ARCH_NAME" ("GDBSERV_PROG_NAME")."
#define GDBSERV_PROG_VERSION           "v1.0.7-H-PF4, 08-Jul-2010, (c)Ashling Microsystems Ltd 2010."
#define MAX_SLEEP_TIME                 1 //1ms
#define MAX_ATTEMPT_TO_STOP_CORE   		5
#elif defined(ARC)
#define GDBSERV_PROG_NAME              "ash-arc-gdb-server"
#define GDBSERV_PROC_FAMILY            _ARC
#define GDBSERV_ARCH_NAME              "ARC"
#define GDBSERV_XML_FAMILY_FILE        "famlyarc.xml"
//#define GDBSERV_NUM_REGISTERS          80 //64
#define GDBSERV_MAX_MEMORY_SIZE        0x10000
#define GDBSERV_PROG_TITLE             "Ashling GDB Server for " GDBSERV_ARCH_NAME " (" GDBSERV_PROG_NAME ")."
#define GDBSERV_PROG_VERSION           "v1.2.7, 16-Aug-2019, (c)Ashling Microsystems Ltd 2019."
#define GDB_ARC_INI_DEFAULT_NAME       "gdbopxdarc.ini"
#define GDB_ARC_INI_ENV_VARIABLE       "OPXDARC_INI_FILE"
#define MAX_SLEEP_TIME                 100 //100mS
#define MAX_ATTEMPT_TO_STOP_CORE   		5
#elif defined(ARM)
#define GDBSERV_PROG_NAME              "ash-arm-gdb-server"
#define GDBSERV_PROC_FAMILY            _ARM
#define GDBSERV_ARCH_NAME              "ARM"
#define GDBSERV_XML_FAMILY_FILE        "familyarm.xml"
#define GDBSERV_NUM_REGISTERS          42//changed from 38 to 42 so that GDB displays CPSR value properly.
#define GDBSERV_GEN_PURPOSE_REG        16
#define GDBSERV_MAX_MEMORY_SIZE        0x10000
#define GDBSERV_PROG_TITLE             "Ashling GDB Server for "GDBSERV_ARCH_NAME" ("GDBSERV_PROG_NAME")."
#define GDBSERV_PROG_VERSION           "v1.0.3-A 25-Jun-2010, (c)Ashling Microsystems Ltd 2010."
#elif defined(RISCV)
#define GDBSERV_PROG_NAME              "ash-riscv-gdb-server"
#define GDBSERV_PROC_FAMILY            _RISCV
#define GDBSERV_ARCH_NAME              "RISC-V"
#define GDBSERV_XML_FAMILY_FILE        "famlyriscv.xml"
#define GDBSERV_NUM_REGISTERS          32 //64
#define GDBSERV_MAX_MEMORY_SIZE        0x10000
#define GDBSERV_PROG_TITLE             "Ashling GDB Server for "GDBSERV_ARCH_NAME" ("GDBSERV_PROG_NAME")."
#define GDBSERV_PROG_VERSION           "v1.1.2, 16-Aug-2019, (c)Ashling Microsystems Ltd 2019."
#define GDB_RISCV_INI_DEFAULT_NAME       "gdbopxdriscv.ini"
#define GDB_RISCV_INI_ENV_VARIABLE       "OPXDRISCV_INI_FILE"
#define MAX_SLEEP_TIME                 100 //100mS
#define MAX_ATTEMPT_TO_STOP_CORE   		5
#else
#error  Architecture Not Supported.                                     // invalid compiler option
#endif

#define GDBSERV_REGDEF_ALLOC_CHUNK    128

#define GDBSERV_COMMAND_FILE           GDBSERV_PROG_NAME".ini"
#define GDBSERV_DEFAULT_GDB_PORT       2331

#define GDBSERV_PACKET_BUFFER_SIZE     0x10000

#define SCREENWIDTH                    150
#define GDBSERV_HELP_COL_WIDTH         31
#define GDBSERV_MON_HELP_COL_WIDTH     35

#define MAX_PROBE_INSTANCES            16                               // maximum probe instances
#define PROBE_STRING_LEN               16                               // length of instance number for the probe

#define MAX_PROP_NAME_LEN              25                               // maximum property size
#define MAX_USER_VARS                  0x100                            // maximum user variables
#define MAX_TAPS_IN_CHAIN             256                              // maximum number of cores on scanchain supported
#define MAX_TAP_NUM                    256                              // maximum TAPs on scanchain
#define MAX_STRING_LENGTH              81                               // maximum string length

#define NOREF(var)                     (var = var)                      // no reference in the function

#define FREQ_MHZ(x)                    ((x) * 1000000)
#define FREQ_KHZ(x)                    ((x) * 1000)

#ifndef NO_ERROR
#define NO_ERROR                       0x0                              // value NO_ERROR is always 0x0
#endif

#ifdef ARM

#define ONE                            1
#define DEFAULT_FREQUENCY_KHZ          1000
#define ARMCORE                        1
#define BYPASS_INSTRUCTION             0x0F
#define IR_LENGTH                      4
#define IR_TOTAL_SUPP_LENGTH           32   // Combined instruction length for multiple devices in chain
#define DR_LENGTH                      32   // JTAG Data Register is 32 bits in length 
#else
#define IR_LENGTH                      5    // Default JTAG Instruction Register is 5 bits in length  ( May Vary)
#define IR_TOTAL_SUPP_LENGTH           32   // Combined instruction length for multiple devices in chain
#define DR_LENGTH                      32   // JTAG Data Register is 32 bits in length 
#endif

#ifndef TRUE
#define TRUE                           1
#endif
#ifndef FALSE
#define FALSE                          0
#endif
#ifndef NULL
#define NULL                           ((void *) 0)
#endif

//Global default defines
#define DEFAULT_RTCK_TIMEOUT    50       // default ARC RTCK timeout
#define DEFAULT_FREQUENCY_HZ    1000000  // default frequency of 1MHz
#ifdef __LINUX
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#endif

#ifndef __min
#define __min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

/*

// Stop signals
typedef enum
{
   TARGET_SIGHUP = 1,         //Hangup
   TARGET_SIGINT,             //Interrupt
   TARGET_SIGQUIT,            //Quit
   TARGET_SIGILL,             //Illegal instruction
   TARGET_SIGTRAP,            //Trace/breakpoint trap
   TARGET_SIGABRT,            //Aborted
   TARGET_SIGEMT,             //Emulation trap
   TARGET_SIGFPE,             //Arithmetic exception
   TARGET_SIGKILL,            //Killed
   TARGET_SIGBUS,             //Bus error
   TARGET_SIGSEGV,            //Segmentation fault
   TARGET_SIGSYS,             //Bad system call
   TARGET_SIGPIPE,            //Broken pipe
   TARGET_SIGALRM,            //Alarm clock
   TARGET_SIGTERM,            //Terminated
   TARGET_SIGURG,             //Urgent I/O condition
   TARGET_SIGSTOP,            //Stopped (signal)
   TARGET_SIGTSTP,            //Stopped (user)
   TARGET_SIGCONT,            //Continued
   TARGET_SIGCHLD,            //Child status changed
   TARGET_SIGTTIN,            //Stopped (tty input)
   TARGET_SIGTTOU,            //Stopped (tty output)
   TARGET_SIGIO,              //I/O possible
   TARGET_SIGXCPU,            //CPU time limit exceeded
   TARGET_SIGXFSZ,            //File size limit exceeded
   TARGET_SIGVTALRM,          //Virtual timer expired
   TARGET_SIGPROF,            //Profiling timer expired
   TARGET_SIGWINCH,           //Window size changed
   TARGET_SIGLOST,            //Resource lost
   TARGET_SIGUSR1,            //User defined signal 1
   TARGET_SIGUSR2,            //User defined signal 2
   TARGET_SIGPWR,             //Power fail/restart
   TARGET_SIGPOLL,            //Pollable event occurred
   TARGET_SIGWIND,            //SIGWIND
   TARGET_SIGPHONE,           //SIGPHONE
   TARGET_SIGWAITING,         //Process's LWPs are blocked
   TARGET_SIGLWP,             //Signal LWP
   TARGET_SIGDANGER,          //Swap space dangerously low
   TARGET_SIGGRANT,           //Monitor mode granted
   TARGET_SIGRETRACT,         //Need to relinquish monitor mode
   TARGET_SIGMSG,             //Monitor mode data available
   TARGET_SIGSOUND,           //Sound completed
   TARGET_SIGSAK,             //Secure attention
   TARGET_SIGPRIO,            //SIGPRIO
   TARGET_SIG33,              //Real-time event 33
   TARGET_SIG34,              //Real-time event 34
   TARGET_SIG35,              //Real-time event 35
   TARGET_SIG36,              //Real-time event 36
   TARGET_SIG37,              //Real-time event 37
   TARGET_SIG38,              //Real-time event 38
   TARGET_SIG39,              //Real-time event 39
   TARGET_SIG40,              //Real-time event 40
   TARGET_SIG41,              //Real-time event 41
   TARGET_SIG42,              //Real-time event 42
   TARGET_SIG43,              //Real-time event 43
   TARGET_SIG44,              //Real-time event 44
   TARGET_SIG45,              //Real-time event 45
   TARGET_SIG46,              //Real-time event 46
   TARGET_SIG47,              //Real-time event 47
   TARGET_SIG48,              //Real-time event 48
   TARGET_SIG49,              //Real-time event 49
   TARGET_SIG50,              //Real-time event 50
   TARGET_SIG51,              //Real-time event 51
   TARGET_SIG52,              //Real-time event 52
   TARGET_SIG53,              //Real-time event 53
   TARGET_SIG54,              //Real-time event 54
   TARGET_SIG55,              //Real-time event 55
   TARGET_SIG56,              //Real-time event 56
   TARGET_SIG57,              //Real-time event 57
   TARGET_SIG58,              //Real-time event 58
   TARGET_SIG59,              //Real-time event 59
   TARGET_SIG60,              //Real-time event 60
   TARGET_SIG61,              //Real-time event 61
   TARGET_SIG62,              //Real-time event 62
   TARGET_SIG63,              //Real-time event 63
   TARGET_SIGCANCEL,          //LWP internal signal
   TARGET_SIG32,              //Real-time event 32
   TARGET_SIG64,              //Real-time event 64
   TARGET_SIG65,              //Real-time event 65
   TARGET_SIG66,              //Real-time event 66
   TARGET_SIG67,              //Real-time event 67
   TARGET_SIG68,              //Real-time event 68
   TARGET_SIG69,              //Real-time event 69
   TARGET_SIG70,              //Real-time event 70
   TARGET_SIG71,              //Real-time event 71
   TARGET_SIG72,              //Real-time event 72
   TARGET_SIG73,              //Real-time event 73
   TARGET_SIG74,              //Real-time event 74
   TARGET_SIG75,              //Real-time event 75
   TARGET_SIG76,              //Real-time event 76
   TARGET_SIG77,              //Real-time event 77
   TARGET_SIG78,              //Real-time event 78
   TARGET_SIG79,              //Real-time event 79
   TARGET_SIG80,              //Real-time event 80
   TARGET_SIG81,              //Real-time event 81
   TARGET_SIG82,              //Real-time event 82
   TARGET_SIG83,              //Real-time event 83
   TARGET_SIG84,              //Real-time event 84
   TARGET_SIG85,              //Real-time event 85
   TARGET_SIG86,              //Real-time event 86
   TARGET_SIG87,              //Real-time event 87
   TARGET_SIG88,              //Real-time event 88
   TARGET_SIG89,              //Real-time event 89
   TARGET_SIG90,              //Real-time event 90
   TARGET_SIG91,              //Real-time event 91
   TARGET_SIG92,              //Real-time event 92
   TARGET_SIG93,              //Real-time event 93
   TARGET_SIG94,              //Real-time event 94
   TARGET_SIG95,              //Real-time event 95
   TARGET_SIG96,              //Real-time event 96
   TARGET_SIG97,              //Real-time event 97
   TARGET_SIG98,              //Real-time event 98
   TARGET_SIG99,              //Real-time event 99
   TARGET_SIG100,             //Real-time event 100
   TARGET_SIG101,             //Real-time event 101
   TARGET_SIG102,             //Real-time event 102
   TARGET_SIG103,             //Real-time event 103
   TARGET_SIG104,             //Real-time event 104
   TARGET_SIG105,             //Real-time event 105
   TARGET_SIG106,             //Real-time event 106
   TARGET_SIG107,             //Real-time event 107
   TARGET_SIG108,             //Real-time event 108
   TARGET_SIG109,             //Real-time event 109
   TARGET_SIG110,             //Real-time event 110
   TARGET_SIG111,             //Real-time event 111
   TARGET_SIG112,             //Real-time event 112
   TARGET_SIG113,             //Real-time event 113
   TARGET_SIG114,             //Real-time event 114
   TARGET_SIG115,             //Real-time event 115
   TARGET_SIG116,             //Real-time event 116
   TARGET_SIG117,             //Real-time event 117
   TARGET_SIG118,             //Real-time event 118
   TARGET_SIG119,             //Real-time event 119
   TARGET_SIG120,             //Real-time event 120
   TARGET_SIG121,             //Real-time event 121
   TARGET_SIG122,             //Real-time event 122
   TARGET_SIG123,             //Real-time event 123
   TARGET_SIG124,             //Real-time event 124
   TARGET_SIG125,             //Real-time event 125
   TARGET_SIG126,             //Real-time event 126
   TARGET_SIG127,             //Real-time event 127
   TARGET_SIGINFO,            //Information request
   TARGET_NULLSIG,            //Unknown signal
   TARGET_NULLSIG1,           //Internal error: printing TARGET_SIGNAL_DEFAULT
   TARGET_EXC_BAD_ACCESS,     //Could not access memory
   TARGET_EXC_BAD_INSTRUCTION,//Illegal instruction/operand
   TARGET_EXC_ARITHMETIC,     //Arithmetic exception
   TARGET_EXC_EMULATION,      //Emulation instruction
   TARGET_EXC_SOFTWARE ,      //Software generated exception
   TARGET_EXC_BREAKPOINT      //Breakpoint
} TyStopSignal;

*/

#endif // GDBGLOB_H_

