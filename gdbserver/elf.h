/****************************************************************************
       Module: elf.h
     Engineer: Vitezslav Hola
  Description: ELF declarations and definitions
               Based on ELF specification v1.1
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef ELF_H_
#define ELF_H_

//////////////////////////////////////////////////////////////
// 32 bit ELF Data Types as per Ref.1. Table 8 reproduced below
// -----------------------------------------------------------
// Name            Size   Alignment   Purpose
// -----------------------------------------------------------
// Elf32_Addr       4         4       Unsigned program address
// Elf32_Half       2         2       Unsigned medium integer
// Elf32_Off        4         4       Unsigned file offset
// Elf32_Sword      4         4       Signed large integer
// Elf32_Word       4         4       Unsigned large integer
// unsigned char    1         1       Unsigned small integer
//////////////////////////////////////////////////////////////
typedef  uint32_t   Elf32_Addr;
typedef  unsigned short  Elf32_Half;
typedef  uint32_t   Elf32_Off;
typedef  int32_t            Elf32_Sword;
typedef  uint32_t   Elf32_Word;

/////////////////////////////////////////////////////////////////////
// Definitions and Structure that defines ELF Header as per Ref.1. //
/////////////////////////////////////////////////////////////////////
#define ELFCLASSNONE 0
#define ELFCLASS32   1
#define ELFCLASS64   2
#define ELFDATANONE  0
#define ELFDATA2LSB  1
#define ELFDATA2MSB  2

#define EI_MAG0      0
#define EI_MAG1      1
#define EI_MAG2      2
#define EI_MAG3      3
#define EI_CLASS     4
#define EI_DATA      5
#define EI_VERSION   6
#define EI_PAD       7
#define EI_NIDENT    16

#define ET_NONE      0           // No file type
#define ET_REL       1           // Relocatable file
#define ET_EXEC      2           // Executable file
#define ET_DYN       3           // Shared object file
#define ET_CORE      4           // Core file
#define ET_LOPROC    0xff00      // Processor-specific
#define ET_HIPROC    0xffff      // Processor-specific

// machine types
#define EM_NONE            0        // No machine
#define EM_M32             1        // AT&T WE 32100
#define EM_SPARC           2        // SPARC
#define EM_386             3        // Intel 80386
#define EM_68K             4        // Motorola 68000
#define EM_88K             5        // Motorola 88000
#define EM_860             7        // Intel 80860
#define EM_MIPS            8        // MIPS RS3000 Big-Endian
#define EM_MIPS_RS4_BE    10        // MIPS RS4000 Big-Endian
#define EM_ETPU           11        // eTPU Architecture
#define EM_POWERPC        20        // PowerPC Architecture
#define EM_ARM            40        // ARM/Thumb Architecture
#define EM_TRICORE        44        // TriCore
#define EM_AVR          6317        // AVR II Architecture

typedef struct
      {
      unsigned char  e_ident[EI_NIDENT];
      Elf32_Half     e_type;
      Elf32_Half     e_machine;
      Elf32_Word     e_version;
      Elf32_Addr     e_entry;
      Elf32_Off      e_phoff;
      Elf32_Off      e_shoff;
      Elf32_Word     e_flags;
      Elf32_Half     e_ehsize;
      Elf32_Half     e_phentsize;
      Elf32_Half     e_phnum;
      Elf32_Half     e_shentsize;
      Elf32_Half     e_shnum;
      Elf32_Half     e_shstrndx;
      } Elf32_Ehdr;

/////////////////////////////////////////////////////////////////////////////
// Definitions and Structure that defines ELF Program Header as per Ref.1. //
/////////////////////////////////////////////////////////////////////////////
#define PT_NULL      0
#define PT_LOAD      1
#define PT_DYNAMIC   2
#define PT_INTERP    3
#define PT_NOTE      4
#define PT_SHLIB     5
#define PT_PHDR      6
#define PT_LOPROC    0x70000000
#define PT_HIPROC    0x7fffffff

// Program header flags
#define PF_PPC_VLE   0x10000000  /* PowerPC specifc VLE program header attribute flag */

typedef struct
      {
      Elf32_Word     p_type;
      Elf32_Off      p_offset;
      Elf32_Addr     p_vaddr;
      Elf32_Addr     p_paddr;
      Elf32_Word     p_filesz;
      Elf32_Word     p_memsz;
      Elf32_Word     p_flags;
      Elf32_Word     p_align;
      } Elf32_Phdr;

/////////////////////////////////////////////////////////////////////////////
// Definitions and Structure that defines ELF Section Header as per Ref.1. //
/////////////////////////////////////////////////////////////////////////////
#define SHT_NULL      0
#define SHT_PROGBITS  1
#define SHT_SYMTAB    2
#define SHT_STRTAB    3
#define SHT_RELA      4
#define SHT_HASH      5
#define SHT_DYNAMIC   6
#define SHT_NOTE      7
#define SHT_NOBITS    8
#define SHT_REL       9
#define SHT_SHLIB     10
#define SHT_DYNSYM    11
#define SHT_LOPROC    0x70000000
#define SHT_HIPROC    0x7fffffff
#define SHT_LOUSER    0x80000000
#define SHT_HIUSER    0xffffffff

// Section header flags
#define SHF_PPC_VLE   0x10000000 /* PowerPC specifc VLE section header attribute flag */
#define SHF_EXECINSTR 0x4        /* Marks a sections as having executable code */
typedef struct _Elf32_Shdr
{
   Elf32_Word  sh_name;
   Elf32_Word  sh_type;
   Elf32_Word  sh_flags;
   Elf32_Addr  sh_addr;
   Elf32_Off   sh_offset;
   Elf32_Word  sh_size;
   Elf32_Word  sh_link;
   Elf32_Word  sh_info;
   Elf32_Word  sh_addralign;
   Elf32_Word  sh_entsize;
} Elf32_Shdr;

/////////////////////////////////////////////////////////////////////////////
// Definitions and Structure that define ELF Symbol Table Entries (Ref.1.) //
/////////////////////////////////////////////////////////////////////////////
#define ELF32_ST_BIND(i)      ((i)>>4)
#define ELF32_ST_TYPE(i)      ((i)&0x0F)
#define ELF32_ST_INFO(b,t)    (((b)<<4)+((t)&0x0F))
#define STB_LOCAL             0
#define STB_GLOBAL            1
#define STB_WEAK              2
#define STB_LOPROC            13
#define STB_HIPROC            15
#define STT_NOTYPE            0
#define STT_OBJECT            1
#define STT_FUNC              2
#define STT_SECTION           3
#define STT_FILE              4
#define STT_LOPROC            13
#define STT_TFUNC             13          /*older ARM SDT uses 13 for Thumb Function*/
#define STT_HIPROC            15
#define STO_GHS_THUMB_CODE    1
#define STO_GHS_ARM_CODE      0
#define STO_GNU_MIPS16_CODE   240
#define STO_GNU_MIPS32_CODE   0

typedef struct _Elf32_Sym {
   Elf32_Word        st_name;
   Elf32_Addr        st_value;
   Elf32_Word        st_size;
   unsigned char     st_info;
   unsigned char     st_other;
   Elf32_Half        st_shndx;
} Elf32_Sym;

#endif // ELF_H_

