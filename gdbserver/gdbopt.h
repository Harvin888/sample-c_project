/****************************************************************************
       Module: gdbopt.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, command line options header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBOPT_H_
#define GDBOPT_H_

// Linux build change...
#include "protocol/common/types.h"

typedef int32_t (TyOptionFunc)(uint32_t *puiArg, uint32_t uiArgCount, char **ppszArgs);
typedef void (TyHelpFunc)(void);

typedef struct _TyOption
{
   const char *pszOption;                                               // option string
   const char *pszHelpArg;                                              // arguments showed in help
   const char *pszHelpLine1;                                            // 1st line of help
   const char *pszHelpLine2;                                            // 2nd line of help
   const char *pszHelpLine3;                                            // 3rd line of help
   const char *pszHelpLine4;                                            // 4th line of help
   TyOptionFunc *pfOptionFunc;                                          // function to be called when option is in command line (parsing, etc.)
   TyHelpFunc *pfHelpFunc;                                              // additional help function                                                         // 
   int32_t bOptionUsed;                                                     // flag indicating if option has been defined in command line
#ifdef MIPS
   int32_t iShowCommand;                                                   //show this command 
#endif
} TyOption;

void DisplayHelp(void);
void SetDefaultOptions(void);
int32_t ProcessCmdLineArgs(int32_t argc, char *argv[]);
int32_t PreConfigVerifyOptions(void);
int32_t PostConfigVerifyOptions(void);
int32_t ProcessCmdFileArgs(const char * pszCommandFile, int32_t bDefaultCmdFile);
const char *GetEmulatorName(void);
const char *GetEmulatorConnectionName(void);

#endif // GDBOPT_H_

