/****************************************************************************
       Module: gdbopt.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, command line options
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS/VK       Modified for ARM support
15-Jan-2009    SPC         Opella-USB support added for MIPS
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbopt.h"
#include "gdbglob.h" 
#include "gdbxml.h"
#include "gdbty.h"
#include "gdbutil.h"
#include "gdberr.h"

// local defines
#define GDBSERV_FILE_ARGS_ALLOC_CHUNK     64
#define LF                                10

// external variables
extern TyServerInfo tySI;                                               // global GDB server structure
extern TyOption tyaOptions[];                                           // supported options
extern int32_t iGdb_options_number;                                         // number of options

// local functions 
static int32_t VerifyJtagFrequency(void);
static int32_t VerifyTAPNumbers(void);
static int32_t ProcessArgList(uint32_t uiArgCount, char **ppszArgs);

#ifdef MIPS
//Static Global variables
static uint32_t pulMipsOpellaUsb[] = {24000,12000,8000,6000,4000,3000,2000,1000,0};
#define OPELLA_FREQ_LIST_SIZE (sizeof(pulMipsOpellaUsb)/sizeof(int32_t))
#endif
/****************************************************************************
     Function: VerifyTAPNumbers
     Engineer: Nikolay chokoev
        Input: none
       Output: int32_t - error code
  Description: Verify user selected TAP Numbers
Date           Initials    Description
14-Nov-2007    NCH         Initial
31-Mar-2008    RS          Modified for ARM support
05-Aug-2008    NCH         Build dependant
13-Apr-2016    ST          Modified to return error if all cores are non-Arc cores
*****************************************************************************/
static int32_t VerifyTAPNumbers(void)
{
#if defined (ARC)
    int32_t iCnt;
    int32_t iARCCore = 0;

    for (iCnt=1; iCnt < MAX_TAPS_IN_CHAIN; iCnt++) // 1 is the first core
    {
        if (tySI.ulTAP[iCnt-1] !=0)
        {
            if (tySI.tyProcConfig.Processor._ARC.tyScanChainDef[tySI.ulTAP[ iCnt - 1 ]].bIsARCCore == TRUE)
            {
                iARCCore = 1;
                break;
            }
        }
    }

    if (iARCCore == 0)
    {
        sprintf(tySI.szError, "Not an ARC core.");
        return -1;
    }

#elif defined (ARM)
    int32_t iCnt;
    for (iCnt=0; iCnt < MAX_TAPS_IN_CHAIN; iCnt++)
    {
        if (tySI.ulTAP[iCnt] !=0)
        {
            if (tySI.tyProcConfig.Processor._ARM.tyScanChainDef[iCnt].bIsARMCore == FALSE)
            {
                sprintf(tySI.szError, "Core number %d is not an ARM core.",
                        tySI.ulTAP[iCnt]);
                return -1;
            }
        }
    }
#elif defined (MIPS)
#elif defined (RISCV)

#else
    return 0;
#endif
    return 0;
}

/****************************************************************************
     Function: VerifyJtagFrequency
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Verify user selected JTAG frequency
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
26-Apr-2208    VK          Modified for ARM
15-Jan-2009    SPC         Opella-USB support added for MIPS
*****************************************************************************/
static int32_t VerifyJtagFrequency(void)
{ 
 #ifdef ARM
   char   szValue[_MAX_PATH];
   tySI.ulJtagFreqHz =tySI.ulJtagFreqKhz * FREQ_KHZ(1);
 #endif
   // check validity of selected frequency (may depend on probe used)
   if (tySI.tyGdbServerTarget == GDBSRVTGT_OPELLAXD)
      {  // for Opella-XD, check if using fixed or RTCK clock
      if (!tySI.bJtagRtck)
         {  // using fixed frequency, is there any restriction ?
         // check mininum frequency as 1 kHz
         if (tySI.ulJtagFreqHz < FREQ_KHZ(1))
            tySI.ulJtagFreqHz = FREQ_KHZ(1);
         // check maximum JTAG frequency and restrict it for sure
         if ((tySI.tyProcConfig.ulMaxJtagFrequency > 0) && (tySI.tyProcConfig.ulMaxJtagFrequency < tySI.ulJtagFreqHz))
            tySI.ulJtagFreqHz = tySI.tyProcConfig.ulMaxJtagFrequency;
         // now get nearest step
         if (tySI.ulJtagFreqHz > FREQ_MHZ(10))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_MHZ(1)) * FREQ_MHZ(1);          // step 1 MHz for frequency above 10 MHz
         else if ((tySI.ulJtagFreqHz <= FREQ_MHZ(10)) && (tySI.ulJtagFreqHz > FREQ_MHZ(1)))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_KHZ(100)) * FREQ_KHZ(100);      // step 100 kHz for frequency between 1 MHz and 10 MHz
         else if ((tySI.ulJtagFreqHz <= FREQ_MHZ(1)) && (tySI.ulJtagFreqHz > FREQ_KHZ(100)))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_KHZ(10)) * FREQ_KHZ(10);        // step 10 kHz for frequency between 100 kHz and 1 MHz
         else if ((tySI.ulJtagFreqHz <= FREQ_KHZ(100)) && (tySI.ulJtagFreqHz > FREQ_KHZ(10)))
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / FREQ_KHZ(1)) * FREQ_KHZ(1);          // step 1 kHz for frequency between 10 kHz and 100 kHz
         else
            tySI.ulJtagFreqHz = (tySI.ulJtagFreqHz / 100) * 100;                          // step 100 Hz for frequency below 10 kHz
         }
      }
#ifdef MIPS
   else
      { //For Opella-USB
      uint32_t iIndex;
      for (iIndex = 0; iIndex<OPELLA_FREQ_LIST_SIZE-1; iIndex++)
         {
         if (pulMipsOpellaUsb[iIndex+1] == 0)
            break;
         if (tySI.ulJtagFreqHz >= FREQ_KHZ(pulMipsOpellaUsb[iIndex]))
            break;
         }
      tySI.ulJtagFreqHz = FREQ_KHZ(pulMipsOpellaUsb[iIndex]);
      tySI.uiJTAGFreqIndex = iIndex;
      }
#endif
   // we need to create also string describing JTAG frequency
   if (!tySI.bJtagRtck)
      {
      if ((tySI.ulJtagFreqHz % FREQ_MHZ(1)) == 0)
         sprintf(tySI.szJtagFreq, "%dMHz", tySI.ulJtagFreqHz / FREQ_MHZ(1));    // frequency value in MHz
      else if ((tySI.ulJtagFreqHz % FREQ_KHZ(1)) == 0)
         sprintf(tySI.szJtagFreq, "%dkHz", tySI.ulJtagFreqHz / FREQ_KHZ(1));    // frequency value in kHz
      else
         sprintf(tySI.szJtagFreq, "%dHz", tySI.ulJtagFreqHz);                   // frequency value in Hz
#ifdef ARM
      strcpy(szValue,"at ");
      strcat(szValue,tySI.szJtagFreq);
      strcpy(tySI.szJtagFreq,szValue);
#endif
      }
   else
#ifdef ARM
      strcpy(tySI.szJtagFreq, "using RTCK");
#else
      strcpy(tySI.szJtagFreq, "adaptive speed");
#endif
   // now we are finished
   return 0;
}

/****************************************************************************
     Function: ProcessCmdFileArgs
     Engineer: Vitezslav Hola
        Input: char *pszCommandFile - file to read commands from
               int32_t bDefaultCmdFile - if processing defulat command file
       Output: int32_t - error code
  Description: Process command options from file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Remove 'goto'
04-Apr-2008    VK          Displaying default.ini file message
*****************************************************************************/
int32_t ProcessCmdFileArgs(const char * pszCommandFile, int32_t bDefaultCmdFile)
{
   int32_t iError = 0;
   uint32_t uiArgAllocCount = 0;
   uint32_t uiArgIndex      = 0;
   char **ppszArgs = NULL;
   char **ppszNewArgs;
   char **ppszOldArgs;
   FILE *pFile;
   int32_t iChar;
   uint32_t uiBufferIndex;
   char szBuffer[_MAX_PATH];

   // open in text mode so that CR+LF is converted to LF
   pFile = fopen(pszCommandFile, "rt");
   if (pFile == NULL)
      {
      if (bDefaultCmdFile)
         return GDBSERV_OPT_DEFAULT_CMDFILE;
      sprintf(tySI.szError, GDB_EMSG_CMD_COULDNT_OPEN_FILE, pszCommandFile);
      return GDBSERV_OPT_ERROR;
      }
   if (bDefaultCmdFile)
      PrintMessage(INFO_MESSAGE, "Reading command line options from file %s.",GDBSERV_COMMAND_FILE);
   iChar = ' ';
   while (!feof(pFile) && (iChar != EOF))
      {
      // skip white space including LF...
      while (!feof(pFile) && (iChar != EOF) && IsSpace(iChar))
         iChar = fgetc(pFile);
      if (feof(pFile) || (iChar == EOF))
         break;
      if ((iChar == ';') || (iChar == '#'))
         {
         // single line comment, skip to end of line...
         while (!feof(pFile) && (iChar != EOF) && (iChar != LF))
            iChar = fgetc(pFile);
         if (feof(pFile) || (iChar == EOF))
            break;
         // back to start of loop to process next line...
         continue;
         }
      uiBufferIndex = 0;
      if (iChar == '"')
         {
         iChar = fgetc(pFile);
         // collect all characters until next double quote (") ...
         while (!feof(pFile) && (iChar != EOF) && (iChar != LF) && (iChar != '"'))
            {
            if (uiBufferIndex < ((int32_t)sizeof(szBuffer) -1))
               szBuffer[uiBufferIndex++] = (char)iChar;
            iChar = fgetc(pFile);
            }
         }
      else
         {
         // collect all characters until next white space...
         while (!feof(pFile) && (iChar != EOF) && !IsSpace(iChar))
            {
            if (uiBufferIndex < ((int32_t)sizeof(szBuffer) -1))
               szBuffer[uiBufferIndex++] = (char)iChar;
            iChar = fgetc(pFile);
            }
         }

      szBuffer[uiBufferIndex] = '\0';
      // check if our list has room for one more Arg pointer...
      if (uiArgIndex >= uiArgAllocCount)
         {
         // reallocate array adding a chunk of entries...
         ppszOldArgs = ppszArgs;
         ppszNewArgs = (char**)realloc(ppszOldArgs, (((uint64_t)uiArgAllocCount+GDBSERV_FILE_ARGS_ALLOC_CHUNK) * sizeof(char*)));
         if (ppszNewArgs == NULL)
            {
            sprintf(tySI.szError, GDB_EMSG_CMD_FILE_ISUFF_MEM, pszCommandFile);
            iError = GDBSERV_OPT_ERROR;
            //free allocated memory before return from the function
            if (pFile != NULL)
               (void)fclose(pFile);
            if ((uiArgAllocCount > 0) && (ppszArgs != NULL))
               {
               for (uiArgIndex=0; uiArgIndex < uiArgAllocCount; uiArgIndex++)
                  free(ppszArgs[uiArgIndex]);
               free(ppszArgs);
               }
            return iError;
            }
         // clear new chunk to 0...
         memset(&ppszNewArgs[uiArgAllocCount], 0, (GDBSERV_FILE_ARGS_ALLOC_CHUNK * sizeof(char*)));
         ppszArgs = ppszNewArgs;
         uiArgAllocCount += GDBSERV_FILE_ARGS_ALLOC_CHUNK;
         }
      // sanity check, this should not happen...
      if (uiArgIndex >= uiArgAllocCount)
         {
         sprintf(tySI.szError, GDB_EMSG_CMD_FILE_INT_ERR2, pszCommandFile);
         iError = GDBSERV_OPT_ERROR;
         //free allocated memory before return from the function
         if (pFile != NULL)
            (void)fclose(pFile);
         if ((uiArgAllocCount > 0) && (ppszArgs != NULL))
            {
            for (uiArgIndex=0; uiArgIndex < uiArgAllocCount; uiArgIndex++)
               free(ppszArgs[uiArgIndex]);
            free(ppszArgs);
            }
         return iError;
         }
      if (ppszArgs == NULL)
         {
         sprintf(tySI.szError, GDB_EMSG_CMD_FILE_INT_ERR, pszCommandFile);
         iError = GDBSERV_OPT_ERROR;
         //free allocated memory before return from the function
         if (pFile != NULL)
            (void)fclose(pFile);
         return iError;
         }   
      // allocate storage for the new Arg...
      ppszArgs[uiArgIndex] = (char*)malloc((uint64_t)uiBufferIndex+1);
      if (ppszArgs[uiArgIndex] == NULL)
         {
         sprintf(tySI.szError, GDB_EMSG_CMD_FILE_ISUFF_MEM, pszCommandFile);
         iError = GDBSERV_OPT_ERROR;
         //free allocated memory before return from the function
         if (pFile != NULL)
            (void)fclose(pFile);
         if (uiArgAllocCount > 0)
            {
            for (uiArgIndex=0; uiArgIndex < uiArgAllocCount; uiArgIndex++)
               free(ppszArgs[uiArgIndex]);
            free(ppszArgs);
            }
         return iError;
         }

      // copy the new Arg...
      strncpy(ppszArgs[uiArgIndex], szBuffer, uiBufferIndex);
      ppszArgs[uiArgIndex][uiBufferIndex] = '\0';
      uiArgIndex++;
      }
   if ((uiArgIndex > 0) && (ppszArgs != NULL))
      iError = ProcessArgList(uiArgIndex, ppszArgs);
   // retain iError from above, but free any memory...
   if (pFile != NULL)
      (void)fclose(pFile);
   if ((uiArgAllocCount > 0) && (ppszArgs != NULL))
      {
      for (uiArgIndex=0; uiArgIndex < uiArgAllocCount; uiArgIndex++)
         free(ppszArgs[uiArgIndex]);
      free(ppszArgs);
      }
   return iError;
}

/****************************************************************************
     Function: ProcessArgList
     Engineer: Vitezslav Hola
        Input: uint32_t uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int32_t - error code
  Description: Process a list of arguments. May be called recursively when --command-file option is used.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Feb-2008    NCH         Use and return error code
*****************************************************************************/
static int32_t ProcessArgList(uint32_t uiArgCount, char **ppszArgs)
{
   int32_t iError, iOption, bValidOption;
   uint32_t uiArg;
   char *pszArg;


   // for each user arg specified...
   for (uiArg=0; uiArg < uiArgCount; uiArg++)
      {
      bValidOption = 0;
      pszArg = ppszArgs[uiArg];
      if ((pszArg[0] == '-') && (pszArg[1] == '-'))
         {  // compare arg with the valid options table...
         for (iOption=0; iOption < iGdb_options_number; iOption++)
            {
            if (strcmp(pszArg+2, tyaOptions[iOption].pszOption) == 0)
               {
               if (tyaOptions[iOption].bOptionUsed)
                  {
                  sprintf(tySI.szError, GDB_EMSG_CLINE_OPT_RPTED, pszArg);
                  return GDBSERV_OPT_ERROR;
                  }
               tyaOptions[iOption].bOptionUsed = 1;
               if (tyaOptions[iOption].pfOptionFunc)
                  {  // call the function that processes this option...
                  iError = tyaOptions[iOption].pfOptionFunc(&uiArg, uiArgCount, ppszArgs);
                  if (iError != GDBSERV_NO_ERROR)
                     return iError;
                  }
               bValidOption = 1;
               break;
               }
            }
         }
      if (!bValidOption)
         {  // currently support long format args only: "--something"
         sprintf(tySI.szError, GDB_EMSG_CLINE_INV_OPTION, pszArg);
         return GDBSERV_OPT_ERROR;
         }
      }
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: DisplayHelp
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Show program help
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void DisplayHelp(void)
{
   int32_t iIndex, iStrLen,iPadding;
   char szBuffer[_MAX_PATH*2];
   // print first part
   PrintMessage(HELP_MESSAGE, "%s [options]\n\n", GDBSERV_PROG_NAME);
   PrintMessage(HELP_MESSAGE, "If no options are specified then, %s will read options from\n", GDBSERV_PROG_NAME);
   PrintMessage(HELP_MESSAGE, "the command file %s.ini if it exists.\n", GDBSERV_PROG_NAME);
   PrintMessage(HELP_MESSAGE, "\nValid options are:\n\n");
   // print available options
   for (iIndex=0; iIndex < iGdb_options_number; iIndex++)
      {
#ifdef MIPS
      if(1 == tyaOptions[iIndex].iShowCommand)
          {
#endif
          sprintf(szBuffer, "--%s%s", tyaOptions[iIndex].pszOption, tyaOptions[iIndex].pszHelpArg);
          PrintMessage(HELP_MESSAGE, "%s", szBuffer);
          if ((iStrLen = (int32_t)strlen(szBuffer)) < GDBSERV_HELP_COL_WIDTH-1)
              iPadding = GDBSERV_HELP_COL_WIDTH - iStrLen;
          else
              iPadding = 1;
          if (tyaOptions[iIndex].pszHelpLine1[0] != '\0')
              PrintMessage(HELP_MESSAGE, "%*s%s\n", iPadding, " ", tyaOptions[iIndex].pszHelpLine1);
          if (tyaOptions[iIndex].pszHelpLine2[0] != '\0')
              PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", tyaOptions[iIndex].pszHelpLine2);
          if (tyaOptions[iIndex].pszHelpLine3[0] != '\0')
              PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", tyaOptions[iIndex].pszHelpLine3);
          if (tyaOptions[iIndex].pszHelpLine4[0] != '\0')
              PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", tyaOptions[iIndex].pszHelpLine4);
          if (tyaOptions[iIndex].pfHelpFunc)
              tyaOptions[iIndex].pfHelpFunc();                               // call the function that provides extra help information
#ifdef MIPS
          }
#endif
      }
}

/****************************************************************************
     Function: SetDefaultOptions
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: Initialize default option in GDB server structures
Date           Initials     Description
10-Jan-2008    NCH          Initial
31-Mar-2008    VK           Modified for ARM support
09-Apr-2008    VK           Set Default Options for Reset
07-Aug-2008    NCH          Redundant removed
11-Apr-2017    AS           Added register file support
*****************************************************************************/
void SetDefaultOptions(void)
{
   // initialize tySI structure
   tySI.bDownloadFile      = FALSE;
   tySI.bVerifyDownloadFile= FALSE;
   tySI.tyProcessorFamily  = GDBSERV_PROC_FAMILY;
   tySI.usGdbPort          = GDBSERV_DEFAULT_GDB_PORT;
   tySI.ulTAP[0]           = 1;
   tySI.tyGdbServerTarget  = GDBSRVTGT_OPELLAXD;
   tySI.tyTargetConnection = GDBSRVTGTCON_USB;
   tySI.bJtagRtck          = 0;
   tySI.ulJtagFreqHz       = DEFAULT_FREQUENCY_HZ;
   tySI.uiRtckTimeout      = DEFAULT_RTCK_TIMEOUT;
   tySI.tyProcConfig.ulProgEntryPoint = 0;
   tySI.tyProcConfig.ulMaxJtagFrequency = 100000000; //100MHz
   tySI.tyProcConfig.bUseEntryPoint   = FALSE;
   tySI.bIsConnected = 1;
   memset(tySI.szProbeInstance, 0, 15);
   // initialize processor config
   tySI.tyProcConfig.tyInitialEndianMode = DefaultEndianMode;
   // initialize ARC specific structure
   
#ifdef RISCV
   {
       tySI.tyProcConfig.Processor._RISCV.uiNumberofTAPs = 1;
	   tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[1].bIsRISCVCore = 1;
	   tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[1].ulBypassInst = 0x1F;
	   tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[1].ulIRlength = 5;
	   strcpy(tySI.tyProcConfig.Processor._RISCV.tyScanChainDef[1].pszRegFileName, "");
   
	   tySI.bIsVegaBoard = 0;
   }
   #elif defined (ARC)
   {
   //int32_t iCnt;
     uint32_t iCnt;
   //printf("SetDefaultOptions-----ARC-----\n");
  // int32_t iTAP; 
  // for (iTAP = tySI.ulTAP[0]; iTAP < MAX_TAPS_IN_CHAIN; iTAP++)
  // {
    //   printf("SetDefaultOptions-----tySI.ulTAP[iTAP] = %d\n", tySI.ulTAP[iTAP]);
    //    if(tySI.tyProcConfig.Processor._ARC.tyScanChainDef[iTAP].bIsARCCore == 1)
    //    {
      //      printf("SetDefaultOptions-----Iside if-------------tySI.ulTAP[iTAP] = %d\n", tySI.ulTAP[iTAP]);
       //      tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].bIsARCCore = 1;
      //       tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].ulBypassInst = 0xFF;
      //       tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].ulIRlength = 4;
         //    break;
      //  }
   //    printf("SetDefaultOptions-----end of for loop-------------tySI.ulTAP[iTAP] = %d\n", tySI.ulTAP[iTAP]);  
  // }
  
   tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].bIsARCCore = 1;
   tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].ulBypassInst = 0xFF;
   tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].ulIRlength = 4;
   strcpy(tySI.tyProcConfig.Processor._ARC.tyScanChainDef[1].pszRegFileName, "");
   
   for(iCnt=0; iCnt < MAX_TAPS_IN_CHAIN; iCnt++)
      {
      /* if(tySI.tyProcConfig.Processor._ARC.tyScanChainDef[iCnt + 1].bIsARCCore == 1)
            {
        
           tySI.ulTAP[1] = 2;
             tySI.ulTAP[iCnt] = iCnt+1;
             tySI.tyProcConfig.Processor._ARC.tyScanChainDef[iCnt+1].bIsARCCore = 1;
             tySI.tyProcConfig.Processor._ARC.tyScanChainDef[iCnt+1].ulBypassInst = 0xFF;
             tySI.tyProcConfig.Processor._ARC.tyScanChainDef[iCnt+1].ulIRlength = 4;
             break;
            }*/

      tySI.tyProcConfig.Processor._ARC.iPDownFlag[iCnt]  = 0; // !PD state
      tySI.tyProcConfig.Processor._ARC.iAttemptStop[iCnt]= 0;
      tySI.tyProcConfig.Processor._ARC.bDCacheFlush[iCnt]= FALSE;
      tySI.tyProcConfig.Processor._ARC.bDCacheInval[iCnt]= FALSE;
      tySI.tyProcConfig.Processor._ARC.bICacheFlush[iCnt]= FALSE;
     // printf("SetDefaultOptions-----end of for loop-------------tySI.ulTAP[iTAP]\n");

}

   }
#elif defined (ARM )
   {
   int32_t iCnt;
   tySI.bTargetResetDelay                                                   = 5;
   tySI.tyGdbServerTarget                                                   = GDBSRVTGT_OPELLAXD;
   tySI.tyProcConfig.Processor._ARM.tyEndianess                             = Little_Endian;
   tySI.tyProcConfig.Processor._ARM.tyTargetReset                           = NoHard_Reset;
   tySI.fVoltage                                                            = 0.0;
   tySI.ulJtagFreqKhz                                                       =DEFAULT_FREQUENCY_KHZ;
   tySI.tyProcConfig.Processor._ARM.uiNumberofTAPs                         =ONE;
   for(iCnt=0;iCnt<MAX_TAPS_IN_CHAIN;iCnt++)
     {
      tySI.tyProcConfig.Processor._ARM.tyScanChainDef[iCnt].bIsARMCore      =ARMCORE;
      tySI.tyProcConfig.Processor._ARM.tyScanChainDef[iCnt].ulBypassInst    =BYPASS_INSTRUCTION;
      tySI.tyProcConfig.Processor._ARM.tyScanChainDef[iCnt].ulIRlength      =IR_LENGTH;
     }
   tySI.tyProcConfig.Processor._ARM.bProcIsBigEndian[0]                     =0;
   tySI.tyProcConfig.Processor._ARM.tySemihostSupport                       =GNUARMCompiler;
   tySI.tyProcConfig.Processor._ARM.uiVectorTrap                            =0x00000008;
   tySI.tyProcConfig.Processor._ARM.uiarmSWI                                =0x123456;
   tySI.tyProcConfig.Processor._ARM.uiarmThumb                              =0xab;
   tySI.tyProcConfig.Processor._ARM.uidisableSemihosting                    =0;
   tySI.tyProcConfig.Processor._ARM.uiTopOfMem                              =0x00000000;
   tySI.uiTargetVoltage                                                     =TRUE;
   tySI.bDbrq                                                               =TRUE;
   tySI.bnTRst                                                              =TRUE;
   tySI.tyProcConfig.Processor._ARM.ulCacheCleanAddress                     =0x50;
   tySI.tyProcConfig.Processor._ARM.ulSafeNonVectorAddress                  =65536;
   
   tySI.bIsConnected = 1;
   }
#elif MIPS
   tySI.tyProcConfig.Processor._MIPS.isPEPset = 0;
   tySI.bIsConnected = 0;
   tySI.tyProcConfig.Processor._MIPS.bDoNotIssueHardReset=0;
//todo MIPS specific options here
#elif ARM
#error "Target architecture currently not implemented."
#endif
}

/****************************************************************************
     Function: ProcessCmdLineArgs
     Engineer: Vitezslav Hola
        Input: int32_t argc - number of arguments
               char *argv[] - array of strings with arguments
       Output: int32_t - errpr code
  Description: process all command line arguments
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t ProcessCmdLineArgs(int32_t argc, char *argv[])
{
   int32_t iError;

   if (argc <= 1)
      {
      iError = ProcessCmdFileArgs(GDBSERV_COMMAND_FILE, 1);
      if (iError != 0)
         {
         if (iError == GDBSERV_OPT_DEFAULT_CMDFILE)
            {  // show help
            iError = 0;
            tySI.bHelp = 1;
            }
         return iError;
         }
      }
   else
      {
      iError = ProcessArgList((uint32_t) (argc-1), argv+1);
      if (iError != 0)
         return iError;
      }
   return 0;
}

/****************************************************************************
     Function: PreConfigVerifyOptions
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Verify some user selected options prior to configuration
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t PreConfigVerifyOptions(void)
{
   // a device must be specified in jtag console mode
   if ((!tySI.bJtagConsoleMode) && (tySI.ulProcessor == 0))
      {
      sprintf(tySI.szError, GDB_EMSG_TGT_DEVICE_NOT_SPEC);
      return GDBSERV_OPT_ERROR;
      }
   return 0;
}

/****************************************************************************
     Function: PostConfigVerifyOptions
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Verify some user selected options after configuration
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t PostConfigVerifyOptions(void)
{
   int32_t iError;
   // verify TAP Numbers...
   iError = VerifyTAPNumbers();
   if (iError != 0)
      return iError;
   // verify Jtag Frequency...
   iError = VerifyJtagFrequency();
   if (iError != 0)
      return iError;
   return 0;
}

/****************************************************************************
     Function: GetEmulatorName
     Engineer: Vitezslav Hola
        Input: none
       Output: char * - emulator name
  Description: return name of selected emulator
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
const char *GetEmulatorName()
{
   switch (tySI.tyGdbServerTarget)
      {
      case GDBSRVTGT_OPELLA:
         return GDBSRVTGT_OPELLA_NAME;
      case GDBSRVTGT_OPELLAXD:
         return GDBSRVTGT_OPELLAXD_NAME;
      case GDBSRVTGT_INVALID:
      default:
         return GDBSRVTGT_INVALID_NAME;
      }
}

/****************************************************************************
     Function: GetEmulatorConnectionName
     Engineer: Vitezslav Hola
        Input: none
       Output: char * - emulator connection name
  Description: return name of selected emulator connection
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
const char *GetEmulatorConnectionName(void)
{
   switch (tySI.tyTargetConnection)
      {
      case GDBSRVTGTCON_SERIAL:
         return GDBSRVTGTCON_SERIAL_NAME;
      case GDBSRVTGTCON_ETH:
         return GDBSRVTGTCON_ETH_NAME;
      case GDBSRVTGTCON_USB:
         return GDBSRVTGTCON_USB_NAME;
      case GDBSRVTGTCON_LPT:
         return GDBSRVTGTCON_LPT_NAME;
      case GDBSRVTGTCON_INVALID:
      default:
         return GDBSRVTGTCON_INVALID_NAME;
      }
}

