/****************************************************************************
       Module: gdbty.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, global structure and type definition
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS/VK       Modified for ARM support
14-Nov-2008    SPC         Added support for Device Enable Password for MIPS
31-Dec-2009    SVL         Modified for PathFinder-XD's Peripheral Register 
                           View. RegisterDefintionType will also be stored 
                           in the data structures.
29-Apr-2010    SJ          Added Support for Coresight devices
06-Aug-2011    ANGS        Added multi-core support for MIPS
19-Oct-2011    ANGS        Modified for Zephyr trace support
****************************************************************************/
#ifndef __GDBTY_H__
#define __GDBTY_H__

// Linux build change...
#include "protocol/common/types.h"
// breakpoint types
#define BPTYPE_SOFTWARE             0   
#define BPTYPE_HARDWARE             1
#define BPTYPE_WRITE                2
#define BPTYPE_READ                 3
#define BPTYPE_ACCESS               4


//address bits
#define ADDRESS_1_BIT 1
#define ADDRESS_2_BIT 2
#define ADDRESS_4_BIT 4

//bytes 
#define BYTE_1 1
#define BYTE_2 2
#define BYTE_4 4
#define BYTE_8 8
#define MATCH_0  0
#define MATCH_1  1
#define MATCH_2  2



//resource 
#define RESOURCE_1 1
#define RESOURCE_2 2
// target voltage
#define TGTVOLTAGE_MATCH            0
#define TGTVOLTAGE_FIXED_MV(x)      (x)
#define TGTVOLTAGE_MATCH_NAME       "Match_Target"
#define TGTVOLTAGE_FIXED_3V3_NAME   "Fixed_3v3"
#define TGTVOLTAGE_FIXED_3V0_NAME   "Fixed_3v0"
#define TGTVOLTAGE_FIXED_2V5_NAME   "Fixed_2v5"
#define TGTVOLTAGE_FIXED_1V8_NAME   "Fixed_1v8"
#define TGTVOLTAGE_FIXED_1V5_NAME   "Fixed_1v5"
#define TGTVOLTAGE_FIXED_1V2_NAME   "Fixed_1v2"

// GDB server supported emulators and their names
typedef enum { GDBSRVTGT_OPELLA, GDBSRVTGT_OPELLAXD, GDBSRVTGT_INVALID } TyGDBServerTarget_t;
#define GDBSRVTGT_OPELLA_NAME       "Opella"
#define GDBSRVTGT_OPELLAXD_NAME     "Opella-XD"
#define GDBSRVTGT_INVALID_NAME      "Unknown"

// GDB server supported emulator connections
typedef enum { GDBSRVTGTCON_SERIAL, GDBSRVTGTCON_ETH, GDBSRVTGTCON_LPT, GDBSRVTGTCON_USB, GDBSRVTGTCON_INVALID } TyTargetConnection;
#define GDBSRVTGTCON_SERIAL_NAME    "Serial"
#define GDBSRVTGTCON_ETH_NAME       "Ethernet"
#define GDBSRVTGTCON_LPT_NAME       "Parallel"
#define GDBSRVTGTCON_USB_NAME       "USB"
#define GDBSRVTGTCON_INVALID_NAME   "Unknown"

//stop reasons for riscv hart
#define GDB_EBREAK_EXECUTION_HALT				0x01
#define GDB_TRIGGER_MODULE_HALT					0x02
#define GDB_DEBUGGER_REQUESTED_HALT				0x03
#define GDB_STEP_SET_HALT						0x04

//system memory access type
#define GDB_RISCV_READ_ONLY_MEM				0x01 //type tag in familyriscv.xml mentions as RO
#define GDB_RISCV_READ_WRITE_MEM			0x02 //type tag in familyriscv.xml mentions as RW


//system memory access mechanism		
#define UNDEFINED_MEMORY_ACCESS				0x00 //if the memory access tag in familyriscv.xml file is not present 
#define SYSTEM_BUS_MEMORY_ACCESS			0x01 //if the memory access tag in familyriscv.xml file mentions system_bus 
#define PROGRAM_BUFFER_MEMORY_ACCESS		0x02 //if the memory access tag in familyriscv.xml file mentions program buffer
#define ABSTRACT_COMMAND_MEMORY_ACCESS		0x04 //if the memory access tag in familyriscv.xml file mentions abstract command

//maximum numbers of DCCM and ICCM memory blocks
#define MAX_MEM_BLOCKS					0X0A
// serial link baudrates
typedef enum {
	BAUDRATE_9600, BAUDRATE_14400, BAUDRATE_19200, BAUDRATE_38400, BAUDRATE_57600, BAUDRATE_115200, BAUDRATE_128000,
	BAUDRATE_153600, BAUDRATE_NOT_SELECTED
} TyBaudRate;

// processor families supported by GDB server
typedef enum
{
	START_PROC_FAMILY = 0x00,
	_MIPS = 0x01,                                          // MIPS processors
	_ARM = 0x02,                                          // ARM processors
	_ARC = 0x03,                                          // ARC processors
	_RISCV = 0x04,                                        // RISCV processors
	INVALID_PROC_FAMILY = 0x05
}  TyProcessorFamily;

// processor types
typedef enum
{
   // MIPS section
   MIPS_4Kc                         = 0x00,
   MIPS_PR1900                      = 0x01,
   MIPS_PR1910                      = 0x02,
   MIPS_P9SC096                     = 0x03,
   MIPS_CRISP                       = 0x04,
   MIPS_VMIPS                       = 0x05,
   MIPS_ADOC                        = 0x06,
   MIPS_MIPS4Ks                     = 0x07,
   MIPS_MIPS4Km                     = 0x08,
   MIPS_MIPS4Kp                     = 0x09,
   MIPS_MOJO                        = 0x0A,
   MIPS_MOJO_QUICKTURN              = 0x0B,
   MIPS_MP4KSSIM                    = 0x0C,
   MIPS_CENTARUS                    = 0x0D,
   MIPS_XILLION225                  = 0x0E,
   MIPS_MIPS4KEc                    = 0x0F,
   MIPS_P9SC128EP                   = 0x10,
   // add more if you wish

   // ARM section
   ARM7DI                           = 0x00,
   ARM7TDMI                         = 0x01,
   ARM7TDMIS                        = 0x02,
   ARM720T                          = 0x03,
   ARM740T                          = 0x04,
   ARM9TDMI                         = 0x05,
   ARM920T                          = 0x06,
   ARM922T                          = 0x07,
   ARM926EJS                        = 0x08,
   ARM940T                          = 0x09,
   ARM946ES                         = 0x0A,
   ARM966ES                         = 0x0B,
   ARM968ES                         = 0x0C,
   ARM1136JFS                       = 0x0D,
   ARM1156T2S                       = 0x0E,
   ARM1176JZS                       = 0x0F,
   ARM11MP                          = 0x10,
   CORTEX_M3						= 0x11,
   // add more if you wish

   // ARC section
   ARCTANGENT_A4                    = 0x00,
   ARCTANGENT_A5                    = 0x01,
   ARC600                           = 0x02,
   ARC700                           = 0x03, 
   // add more if you wish
   PROCESSOR_LAST
} TyProcessorTypes;

// stop signals
typedef enum
{
	TARGET_SIGHUP = 1,         //Hangup
	TARGET_SIGINT,             //Interrupt
	TARGET_SIGQUIT,            //Quit
	TARGET_SIGILL,             //Illegal instruction
	TARGET_SIGTRAP,            //Trace/breakpoint trap
	TARGET_SIGABRT,            //Aborted
	TARGET_SIGEMT,             //Emulation trap
	TARGET_SIGFPE,             //Arithmetic exception
	TARGET_SIGKILL,            //Killed
	TARGET_SIGBUS,             //Bus error
	TARGET_SIGSEGV,            //Segmentation fault
	TARGET_SIGSYS,             //Bad system call
	TARGET_SIGPIPE,            //Broken pipe
	TARGET_SIGALRM,            //Alarm clock
	TARGET_SIGTERM,            //Terminated
	TARGET_SIGURG,             //Urgent I/O condition
	TARGET_SIGSTOP,            //Stopped (signal)
	TARGET_SIGTSTP,            //Stopped (user)
	TARGET_SIGCONT,            //Continued
	TARGET_SIGCHLD,            //Child status changed
	TARGET_SIGTTIN,            //Stopped (tty input)
	TARGET_SIGTTOU,            //Stopped (tty output)
	TARGET_SIGIO,              //I/O possible
	TARGET_SIGXCPU,            //CPU time limit exceeded
	TARGET_SIGXFSZ,            //File size limit exceeded
	TARGET_SIGVTALRM,          //Virtual timer expired
	TARGET_SIGPROF,            //Profiling timer expired
	TARGET_SIGWINCH,           //Window size changed
	TARGET_SIGLOST,            //Resource lost
	TARGET_SIGUSR1,            //User defined signal 1
	TARGET_SIGUSR2,            //User defined signal 2
	TARGET_SIGPWR,             //Power fail/restart
	TARGET_SIGPOLL,            //Pollable event occurred
	TARGET_SIGWIND,            //SIGWIND
	TARGET_SIGPHONE,           //SIGPHONE
	TARGET_SIGWAITING,         //Process's LWPs are blocked
	TARGET_SIGLWP,             //Signal LWP
	TARGET_SIGDANGER,          //Swap space dangerously low
	TARGET_SIGGRANT,           //Monitor mode granted
	TARGET_SIGRETRACT,         //Need to relinquish monitor mode
	TARGET_SIGMSG,             //Monitor mode data available
	TARGET_SIGSOUND,           //Sound completed
	TARGET_SIGSAK,             //Secure attention
	TARGET_SIGPRIO,            //SIGPRIO
	TARGET_SIG33,              //Real-time event 33
	TARGET_SIG34,              //Real-time event 34
	TARGET_SIG35,              //Real-time event 35
	TARGET_SIG36,              //Real-time event 36
	TARGET_SIG37,              //Real-time event 37
	TARGET_SIG38,              //Real-time event 38
	TARGET_SIG39,              //Real-time event 39
	TARGET_SIG40,              //Real-time event 40
	TARGET_SIG41,              //Real-time event 41
	TARGET_SIG42,              //Real-time event 42
	TARGET_SIG43,              //Real-time event 43
	TARGET_SIG44,              //Real-time event 44
	TARGET_SIG45,              //Real-time event 45
	TARGET_SIG46,              //Real-time event 46
	TARGET_SIG47,              //Real-time event 47
	TARGET_SIG48,              //Real-time event 48
	TARGET_SIG49,              //Real-time event 49
	TARGET_SIG50,              //Real-time event 50
	TARGET_SIG51,              //Real-time event 51
	TARGET_SIG52,              //Real-time event 52
	TARGET_SIG53,              //Real-time event 53
	TARGET_SIG54,              //Real-time event 54
	TARGET_SIG55,              //Real-time event 55
	TARGET_SIG56,              //Real-time event 56
	TARGET_SIG57,              //Real-time event 57
	TARGET_SIG58,              //Real-time event 58
	TARGET_SIG59,              //Real-time event 59
	TARGET_SIG60,              //Real-time event 60
	TARGET_SIG61,              //Real-time event 61
	TARGET_SIG62,              //Real-time event 62
	TARGET_SIG63,              //Real-time event 63
	TARGET_SIGCANCEL,          //LWP internal signal
	TARGET_SIG32,              //Real-time event 32
	TARGET_SIG64,              //Real-time event 64
	TARGET_SIG65,              //Real-time event 65
	TARGET_SIG66,              //Real-time event 66
	TARGET_SIG67,              //Real-time event 67
	TARGET_SIG68,              //Real-time event 68
	TARGET_SIG69,              //Real-time event 69
	TARGET_SIG70,              //Real-time event 70
	TARGET_SIG71,              //Real-time event 71
	TARGET_SIG72,              //Real-time event 72
	TARGET_SIG73,              //Real-time event 73
	TARGET_SIG74,              //Real-time event 74
	TARGET_SIG75,              //Real-time event 75
	TARGET_SIG76,              //Real-time event 76
	TARGET_SIG77,              //Real-time event 77
	TARGET_SIG78,              //Real-time event 78
	TARGET_SIG79,              //Real-time event 79
	TARGET_SIG80,              //Real-time event 80
	TARGET_SIG81,              //Real-time event 81
	TARGET_SIG82,              //Real-time event 82
	TARGET_SIG83,              //Real-time event 83
	TARGET_SIG84,              //Real-time event 84
	TARGET_SIG85,              //Real-time event 85
	TARGET_SIG86,              //Real-time event 86
	TARGET_SIG87,              //Real-time event 87
	TARGET_SIG88,              //Real-time event 88
	TARGET_SIG89,              //Real-time event 89
	TARGET_SIG90,              //Real-time event 90
	TARGET_SIG91,              //Real-time event 91
	TARGET_SIG92,              //Real-time event 92
	TARGET_SIG93,              //Real-time event 93
	TARGET_SIG94,              //Real-time event 94
	TARGET_SIG95,              //Real-time event 95
	TARGET_SIG96,              //Real-time event 96
	TARGET_SIG97,              //Real-time event 97
	TARGET_SIG98,              //Real-time event 98
	TARGET_SIG99,              //Real-time event 99
	TARGET_SIG100,             //Real-time event 100
	TARGET_SIG101,             //Real-time event 101
	TARGET_SIG102,             //Real-time event 102
	TARGET_SIG103,             //Real-time event 103
	TARGET_SIG104,             //Real-time event 104
	TARGET_SIG105,             //Real-time event 105
	TARGET_SIG106,             //Real-time event 106
	TARGET_SIG107,             //Real-time event 107
	TARGET_SIG108,             //Real-time event 108
	TARGET_SIG109,             //Real-time event 109
	TARGET_SIG110,             //Real-time event 110
	TARGET_SIG111,             //Real-time event 111
	TARGET_SIG112,             //Real-time event 112
	TARGET_SIG113,             //Real-time event 113
	TARGET_SIG114,             //Real-time event 114
	TARGET_SIG115,             //Real-time event 115
	TARGET_SIG116,             //Real-time event 116
	TARGET_SIG117,             //Real-time event 117
	TARGET_SIG118,             //Real-time event 118
	TARGET_SIG119,             //Real-time event 119
	TARGET_SIG120,             //Real-time event 120
	TARGET_SIG121,             //Real-time event 121
	TARGET_SIG122,             //Real-time event 122
	TARGET_SIG123,             //Real-time event 123
	TARGET_SIG124,             //Real-time event 124
	TARGET_SIG125,             //Real-time event 125
	TARGET_SIG126,             //Real-time event 126
	TARGET_SIG127,             //Real-time event 127
	TARGET_SIGINFO,            //Information request
	TARGET_NULLSIG,            //Unknown signal
	TARGET_NULLSIG1,           //Internal error: printing TARGET_SIGNAL_DEFAULT
	TARGET_EXC_BAD_ACCESS,     //Could not access memory
	TARGET_EXC_BAD_INSTRUCTION,//Illegal instruction/operand
	TARGET_EXC_ARITHMETIC,     //Arithmetic exception
	TARGET_EXC_EMULATION,      //Emulation instruction
	TARGET_EXC_SOFTWARE,      //Software generated exception
	TARGET_EXC_BREAKPOINT      //Breakpoint
} TyStopSignal;

// target endianess
typedef enum { DefaultEndianMode, LittleEndianMode, BigEndianMode } TyEndianMode;

//memory space type
typedef enum {
   MEM_XDATA         = 0x00,
   MEM_CODE          = 0x01,
   MEM_DP_RAM        = 0x02,
   MEM_SFR           = 0x03,
   MEM_RAM           = 0x04,
   MEM_REG           = 0x05,
   MEM_BANKED_CODE   = 0x06,
   MEM_DATA_PSN      = 0x07,
   MEM_CODE_PSN      = 0x08,
   MEM_DDCHECK       = 0x09,
   MEM_BIT           = 0x0A,
   MEM_CONTROL       = 0x0B,
   MEM_EEPROM        = 0x0C,
   MEM_ESFR          = 0x0D,
   MEM_FLASH         = 0x0E,
   MEM_UNIFIED       = 0x0F,
   MEM_EEP_PG_ZERO   = 0x10,
   MEM_BONDOUT_FLASH = 0x11,
   MEM_PHYSICL       = 0x12,
   MEM_COCO          = 0x13,
   MEM_INVALID       = 0x14
} TyMemTypes;

typedef struct
{
   char           pszGroupName[30];
   int32_t            iStartRegIndex;
   int32_t            iEndRegIndex;
   int32_t            iRegDefType;
} TyGroupDef;

// Register definition file entry types
typedef enum
{
   REGDEFTYPE_GROUP = 0,
   REGDEFTYPE_REGSITER  = 1,
   REGDEFTYPE_BIT  = 2,
   REGDEFTYPE_NONE  = 3
} TyRegisterDefTypes;

// register structure
typedef struct
{
   char *pszRegName;
   uint32_t ulAddress;
   TyMemTypes ubMemType;
   unsigned char ubByteSize;
   unsigned char ubBitOffset;
   unsigned char ubBitSize;
   int32_t iRegDefType;
   int32_t iFullRegIndex;
} TyRegDef;

// user specific variable structure
typedef struct _TyUserSpecific
{
   char szVariableName[MAX_STRING_LENGTH];
   uint32_t ulVariableAddress;
   uint32_t ulVariableSize;
   uint32_t ulVariableValue;
} TyUserSpecific;

// ARC scanchain definition
typedef struct _TyARCScanchain
{
   int32_t           bIsARCCore;
   uint32_t ulIRlength;
   uint32_t ulBypassInst;
   char          pszRegFileName[100];
   unsigned char bIsRegFilePresent;
   int32_t           iNumberOfRegisters;
} TyARCScanchain;
// ARM scanchain definition
typedef struct _TyARMScanchain
{
   int32_t bIsARMCore;
   uint32_t ulIRlength;
   uint32_t ulBypassInst;
} TyARMScanchain;
// MIPS scanchain definition
typedef struct _TyMIPSScanchain
{
   int32_t bIsDMACore;
   uint32_t ulIRlength;
   uint32_t ulBypassInst;   
} TyMIPSScanchain;
//RISCV scanchain definition
typedef struct _TyRISCVScanchain
{
	int32_t           bIsRISCVCore;
	uint32_t ulIRlength;
	uint32_t ulBypassInst;
	char          pszRegFileName[100];
	unsigned char bIsRegFilePresent;
	int32_t           iNumberOfRegisters;
} TyRISCVScanchain
;
typedef enum _TySemiHostSupport
{
    ARMCompiler = 0,
    GNUARMCompiler =1
} TySemiHostSupport;

//ARM Coresight Configuration info
typedef struct _TyCoresightConfigInfo
{
   uint32_t   ulCoresightBaseAddr;
   uint32_t   ulDbgApIndex;
   uint32_t   ulMemApIndex;
   uint32_t   ulJtagApIndex;
    uint32_t   ulUseMemApMemAccess;
}TyCoresightConfigInfo;

typedef enum _TyEndianess 
{
    Big_Endian=0,
    Little_Endian=1
} TyEndianess;

typedef enum _TyTargetReset
{
	NoHard_Reset = 0,
	HardResetAnd_Delay = 1,
	HardResetAnd_HaltAtRV = 2,
	//HardResetAndGo=3,
	Hot_Plug = 3,
	Core_Only = 4,
	Core_And_Peripherals = 5
} TyTargetReset;

// ARC property structure
typedef struct _TyARCProperty
{
   char szName[MAX_PROP_NAME_LEN];
   char szValue[_MAX_PATH];
} TyARCProperty;
//RISCV property structure
typedef struct _TyRISCVProperty
{
	char szName[MAX_PROP_NAME_LEN];
	char szValue[_MAX_PATH];
} TyRISCVProperty;

// ARC processor specific structure
typedef struct _TySpecificARC
{
   uint32_t    ulUserSpecificVariablesCount;
   TyUserSpecific   pUserSpecificVariables[MAX_USER_VARS];
   char             szFirmwareVer[_MAX_PATH];
   char             szDiskwareVer[_MAX_PATH];
   int32_t              bProcIsBigEndian[MAX_TAPS_IN_CHAIN+1]; //Is Big  Endian?
   int32_t              bDCacheFlush[MAX_TAPS_IN_CHAIN+1]; //Is Data Cache Flush done
   int32_t              bDCacheInval[MAX_TAPS_IN_CHAIN+1]; //Is Data Cache Flush done
   int32_t              bICacheFlush[MAX_TAPS_IN_CHAIN+1]; //Is Instruction Cache Flush done
   uint32_t    ulTargetIdentity[MAX_TAPS_IN_CHAIN+1]; //Target processor ID
   int32_t              bRunningFlag[MAX_TAPS_IN_CHAIN+1]; //Set when we run the core; clear when halted
                    //iPDownFlag & 0xF - last state; iPDownFlag & 0xF0 - prev state;
   int32_t              iPDownFlag[MAX_TAPS_IN_CHAIN+1];   //'1' - PD, '0' - normal; 
   int32_t              iAttemptStop[MAX_TAPS_IN_CHAIN+1];
   unsigned short   usTargetSupply;               // Should we supply fixed 3v3 or match the target
   uint32_t     uiSleepTime;
   TyARCScanchain   tyScanChainDef[MAX_TAPS_IN_CHAIN+1];
   TyARCScanchain   tyScanChainDetected[MAX_TAPS_IN_CHAIN+1];
} TySpecificARC;

// RISCV processor specific structure
typedef struct _TySpecificRISCV
{

	uint32_t    ulUserSpecificVariablesCount;
	TyUserSpecific   pUserSpecificVariables[MAX_USER_VARS];
	char             szFirmwareVer[_MAX_PATH];
	char             szDiskwareVer[_MAX_PATH];
	int32_t              bProcIsBigEndian[MAX_TAPS_IN_CHAIN + 1]; //Is Big  Endian?
	int32_t              bDCacheFlush[MAX_TAPS_IN_CHAIN + 1]; //Is Data Cache Flush done
	int32_t              bDCacheInval[MAX_TAPS_IN_CHAIN + 1]; //Is Data Cache Flush done
	int32_t              bICacheFlush[MAX_TAPS_IN_CHAIN + 1]; //Is Instruction Cache Flush done
	uint32_t    ulTargetIdentity[MAX_TAPS_IN_CHAIN + 1]; //Target processor ID
	int32_t              bRunningFlag[MAX_TAPS_IN_CHAIN + 1]; //Set when we run the core; clear when halted
	int32_t              iAttemptStop[MAX_TAPS_IN_CHAIN + 1];
	unsigned short   usTargetSupply;               // Should we supply fixed 3v3 or match the target
	uint32_t     uiSleepTime;
	//TODO: Check how is MC info stored here - based on TAP number index or normal index
	TyRISCVScanchain   tyScanChainDef[MAX_TAPS_IN_CHAIN + 1];
	TyRISCVScanchain   tyScanChainDetected[MAX_TAPS_IN_CHAIN + 1];
	uint32_t  uiNumberofTAPs;
} TySpecificRISCV;

// ARM processor specific structure
typedef struct _TySpecificARM          
{
   uint32_t            ulUserSpecificVariablesCount;
   TyUserSpecific           pUserSpecificVariables[MAX_USER_VARS];
   uint32_t             uiSleepTime;
   int32_t                      bProcIsBigEndian[MAX_TAPS_IN_CHAIN+1]; //Is Big  Endian?
   TyARMScanchain           tyScanChainDef[MAX_TAPS_IN_CHAIN+1];
   TyARMScanchain           tyScanChainDetected[MAX_TAPS_IN_CHAIN+1];
   TySemiHostSupport        tySemihostSupport; 
   TyEndianess              tyEndianess;
   TyTargetReset            tyTargetReset;
   uint32_t             uiVectorTrap;
   uint32_t             uiarmSWI;
   uint32_t             uiarmThumb;
   uint32_t             uidisableSemihosting;
   uint32_t             uiTopOfMem;
   uint32_t            ulCacheCleanAddress;
   uint32_t            ulSafeNonVectorAddress; // Safe Non-vector address
   uint32_t             uiNumberofTAPs;
   unsigned char            ucShiftIRPreTCKs; 
   unsigned char            ucShiftIRPostTCKs;
   unsigned char            ucShiftDRPreTCKs; 
   unsigned char            ucShiftDRPostTCKs;
   int32_t                      bRunningFlag[MAX_TAPS_IN_CHAIN+1]; //Set when we run the core; clear when halted
   uint32_t            ulDummy;
   int32_t                      bCoresightCompliant;
   int32_t                      bTiDevice;
   uint32_t             uiTiSubPortNum;
   int32_t                      bDapPcControl;
	int32_t                      bCacheInvalidationNeeded;
   TyCoresightConfigInfo    tyCoresightConfigInfo;
} TySpecificARM;

typedef struct 
{
   uint32_t			  ulIRLength;			   		// Device IR Length	
   uint32_t			  ulBypassCode;		   			// Device Bypass Code	
}  TyMultiCoreDeviceInfo;

typedef struct
{
   int32_t bFixedCoreNums;
   int32_t EJTAGStyle;
   unsigned short usTargetSupply;
   int32_t bUseSlowJTAG;
   int32_t bEJTAGDMAOnly;
   
   int32_t bNoMMUTranslation;
   int32_t bFreeRunningJTAGClk;
   int32_t bUseEJTAGBootDuringReset;
   int32_t bEJTAGBootSupported;
   uint32_t ulNumInstHWBps;
   uint32_t ulNumDataHWBps;
   int32_t bHWBpASIDSupported;
   int32_t bOddAddressFor16BitModeBps;
   int32_t bSSIn16BitModeSupported;
   int32_t bEnableMIPS16ModeForTrace;
   unsigned char ucMultiCoreCoreNum;
   unsigned char ucMultiCoreDMACoreNum;
   int32_t bPerformHardReset;
   int32_t bPerformSoftProcessorReset;
   int32_t bPerformSoftPeripheralReset;
   int32_t bPerformHardBetweenSoftReset;
   //to int32_t bInvalidateCacheOnAReset;
   uint32_t ulInvalidateInst;
   uint32_t ulInvalidateWBInst;
   int32_t bOnlyFlushDataCache;
   int32_t bDoubleReadOnDMA;
   int32_t bAssumeWordAccess;
   int32_t bEnableWriteRequired;
   uint32_t ulEnableWriteAddress;
   uint32_t ulEnableWriteValue;
   uint32_t ulEnableWriteMask;
   // uint32_t ulDebugMemStart;
   // uint32_t ulDebugMemEnd;
   unsigned char ucMultiCoreNumCores;
   unsigned char ucIDCODEInstCode;
   unsigned char ucLengthOfAddressRegister;
   int32_t bSelectAddrRegAfterEveryAccess;
   int32_t bSyncInstNotSupported;
   int32_t bAssertHaltTwice   ;
   int32_t bUseFastDMAFunc    ;
   uint32_t ulCoreSelectCode   ;
   uint32_t ulCoreSelectIRLength;
   unsigned char ucNumberOfNOPSInERETBDS;
   unsigned char ucNumberOfNOPSInJUMPBDS;
   
   TyMultiCoreDeviceInfo tyMultiCoreDeviceInfo[MAX_TAPS_IN_CHAIN+1];
   
   uint32_t ulDelayAfterResetMS ;
   uint32_t ulResetDelayMS ;
   unsigned char ucDMAWriteAfterReset;
   uint32_t ulDMAResetWriteAddr ;
   uint32_t ulDMAResetWriteValue;
   unsigned char ucEJTAGSecSequence  ;
   int32_t bUseInstrWatchForBP;
   unsigned char ucMemReadAfterReset;
   int32_t bCoreSelectBeforeReset;
   int32_t bUseRestrictedDmaAddressRange;
   uint32_t ulStartOfValidDmaAddressRange;
   uint32_t ulEndOfValidDmaAddressRange;
   unsigned char ucMemWriteBeforeReset;
   uint32_t ulMemResetWriteAddr;
   uint32_t ulMemResetWriteValue;
   int32_t bMicroChipM4KFamily;   
   uint32_t ulNumberOfShadowSetsPresent;
   uint32_t ulLimitCacheRdBpS;
   uint32_t ulLimitCacheWrBpS;
   uint32_t ulLimitDmaRdBpS;
   uint32_t ulLimitDmaWrBpS;
}TyMDInfo;

typedef struct _TySpecificMIPS
{
   uint32_t    ulUserSpecificVariablesCount;
   TyUserSpecific   pUserSpecificVariables[MAX_USER_VARS];
   // not implemented yet
   uint32_t    ulDummy;
   int32_t              bZephyrTraceSupport;
   int32_t              bEJTAGDMAOnly;
   int32_t              isPEPset;
   int32_t              bDMATurnedOn;
   int32_t              bDisableIntSS;  		// Disable Interupts on Single Step
   int32_t              bProcIsBigEndian[MAX_TAPS_IN_CHAIN+1]; //Is Big  Endian?
   int32_t              iAttemptStop[MAX_TAPS_IN_CHAIN+1];
   int32_t              bSingleStepUsingSWBP;  // Single Step using Software BP
   int32_t              bUserHaltCountersIDM;  // User wants to Halt Counters in Debug Mode
   int32_t              bCacheDebugEnabled;    // User has chosen to enable cache debugging
   int32_t              bCacheFlushSupported;  // Copied from Config File ( Derivative suppports cache flush)
   int32_t              bCacheFlushVerify;     // User wants us to verify cache flush routine before running it
   int32_t              bRunningFlag[MAX_TAPS_IN_CHAIN+1]; //Set when we run the core; clear when halted
   int32_t 				bDoNotIssueHardReset;
   char             szCacheFlushFilename[_MAX_PATH];    // Copied from Config File ( Filename of SREC )
   char 			szCacheRelocationAddress[MAX_STRING_LENGTH];    // Common User Relocated address
   char 			szEndianModeRegBitName[MAX_STRING_LENGTH];
   TyMIPSScanchain  tyScanChainDef[MAX_TAPS_IN_CHAIN+1];
   int32_t              bSelectedCore;
   uint32_t     uiNumberofTAPs;
   uint32_t     uiDMAcore;
   uint32_t     uiSleepTime;
   uint32_t 	ulDataScratchPadRamStart;   // Not stored in MIPS.CFG. Retrieved from famlymip.dat
   uint32_t 	ulDataScratchPadRamEnd;     // Not stored in MIPS.CFG. Retrieved from famlymip.dat
   uint32_t 	ulInstScratchPadRamStart;   // Not stored in MIPS.CFG. Retrieved from famlymip.dat
   uint32_t 	ulInstScratchPadRamEnd;     // Not stored in MIPS.CFG. Retrieved from famlymip.dat
   TyMDInfo         *ptyMDInfo;
} TySpecificMIPS;

typedef union
{
	TySpecificARM  _ARM;
	TySpecificARC  _ARC;
	TySpecificMIPS _MIPS;
	TySpecificRISCV _RISCV;

} TyProcSpecific;

typedef struct
{
	uint32_t			ulXMLRISCVMemAddress;		//address of memarea	
	uint32_t			ulXMLRISCVMemSize;			//size of memarea
	uint32_t			ulXMLRISCVMemAccess;		//memaccess mechanism
	uint32_t			ulXMLRISCVMemType;			//access type R/W
}TyMemoryAccessInfo;

// processor configuration
typedef struct 
{
   char              szProcName[SCREENWIDTH];      // processor description
   char              szInternalName[SCREENWIDTH];  // processor name for identifier purposes
   char              szProbeTypeName[SCREENWIDTH];  // Probe type name (Currently used only in ARC)
   char              szRegFileName[SCREENWIDTH];   // Processor register file name
   char              szSymFile[_MAX_PATH];         // symbols file
#ifdef MIPS
   char              szPasswordSequence[_MAX_PATH];
   char              szPasswordFile1[_MAX_PATH];
   char              szPasswordFile2[_MAX_PATH];
   char              szPasswordFile3[_MAX_PATH];
#endif
   uint32_t     ulMaxJtagFrequency;           // maximum JTAG frequency in Hz (0xFFFFFFFF if frequency is not restricted)
   char              szDiskWare1[_MAX_PATH];       // diskware 1 file name
   char              szDiskWare2[_MAX_PATH];       // diskware 2 file name
   char              szDiskWare3[_MAX_PATH];       // diskware 3 file name
   TyEndianMode      tyInitialEndianMode;          // initial endian mode to set on the target, if modifiable
   uint32_t     ulProgEntryPoint;             // program entry point
   int32_t               bUseEntryPoint;               // if to use ulProgEntryPoint
                                                   // 
#ifdef ARM                                                // 
   uint32_t      uiJTAGFreqIndex;     // used to reference the list in target.cpp (ARM only for now, others to follow)
   uint32_t     ulMaxJtagFrequencyKhz;         // Maximum JTAG frequency for OpellaXD (in units of Khz)
   int32_t               ulCoreIndex;
#endif

#ifdef RISCV
   uint32_t	uiInstrtoSelectADI;
#endif
//   char              szFpgaWare[(uint32_t)FPGA_INVALID][_MAX_PATH]; // FpgaWare file names
//   TXRXProcTypes     ProcType;            // processor type
//NCH: IS the "TyProcFamGrp      FamilyGroup;" needed ???????????????
//   TyProcFamGrp      FamilyGroup;
//   TXRXProcModes     ProcMode;            // processor mode
//   TXRXProcVoltage   ProcVoltage;         // processor voltage
//   TXRXClockSource   ClockSource;         // clock source
//   adtyp             StartOfOnChipROM;    // start of on chip ROM
//   uint32_t     OnChipROM;           // size of OnChip ROM
//   adtyp             StartOfOnChipRAM;    // start of on chip RAM
//   adtyp             EndOfOnChipRAM;      // end of on chip RAM
//   adtyp             StartOfSFR;          // start of SFR 
//   adtyp             EndOfSFR;            // end of SFR 
//   int32_t               iNumOfPorts;         // number of ports on device
//   int32_t               iNumOfTracedPorts;               // number of ports traced on this device
//   char              szTracedPortTitles[_MAX_PATH];   // port titles (a max of MAX_PORT_TITLES)
//   int32_t               bEnableTargetReset;  // enable target reset
//   TXRXWatchDog      WatchDog;            // Watchdog
//   TXRXProcBusWidth  ProcDataBusWidth;    // processor data bus width
//   TXRXProcBusWidth  ProcAddrBusWidth;    // processor addr data bus width
//   char              ProgEntryPointText[_MAX_PATH];   // Text to evaluate for program entry point
//   uint32_t     uInstructionSet;     // differentiates between instruction sets
   int32_t               bEndianModeModifiable;  // TRUE if PathFinder can modify the Endian mode on the processor
//   int32_t               bHWCodeCoverageAvailable;        // TRUE if hardware code coverage is available
//   int32_t               bTraceCodeCoverageAvailable;     // TRUE if code coverage based on trace info is available
//   char              szDeviceRegFile[_MAX_PATH];   // store the name of the .reg file
//   TyFlashProgConfig tyFlashProgConfig;      // Configuration for the Flash Programming dialog
   // General
//   int32_t               bAlternativeResetOptions;     // Are the alternative reset options available
//   int32_t               bGoFromInsertionSupported;    // Go From Insertion supported
//   char              szTarget1RegFile[_MAX_PATH];  // Target 1 filename
   TyProcSpecific    Processor;                                         // processor specific structure
} ProcessorConfig;

// structure containing configuration information and user selected options
typedef struct _TyServerInfo
{
   // GDB server user options
   int32_t                  bHelp;
   int32_t                  bVersion;
   int32_t                  bPfxdMode;
   int32_t                  bJtagConsoleMode;
   int32_t                  bDownloadFile;
   int32_t                  bVerifyDownloadFile;
   int32_t                  bDebugOutput;
   int32_t                  bDebugStdout;
   int32_t                  bIsConnected;
   FILE                 *pDebugFile;
   char                 szDebugFile[_MAX_PATH];
   char                 szCommandFile[_MAX_PATH];
   char                 szRegisterFile[_MAX_PATH];
   unsigned short       usGdbPort;
#ifdef ARM
   uint32_t         bVoltageTracking;
   float                fVoltage;
   uint32_t         bnTRst;
   uint32_t         bDbrq;
   uint32_t         uiTargetVoltage;        // Target voltage 3.3V,2.5V,1.8V
   uint32_t        ulJtagFreqKhz;
   uint32_t        ulEnableTargetReset;
   int32_t                  bTargetResetDelay;
   int32_t                  bTap;
   int32_t                  bInvalid;
   char                 bEnableSafenonVectorAddress;
	int32_t                  bEnableRDILogging;
#endif
   uint32_t        ulTAP[MAX_TAP_NUM];                             // array of flag indicating core is active for debug or not
   // emulator selection
   TyGDBServerTarget_t  tyGdbServerTarget;
   TyTargetConnection   tyTargetConnection;
   char                 szProbeInstanceList[MAX_PROBE_INSTANCES][PROBE_STRING_LEN];    // list of currently connected probe instances
   char                 szProbeInstance[PROBE_STRING_LEN];              // current probe instance (serial number)
   // XML entry nodes
   TyXMLFile            tyXMLFile;
   TyXMLNode          * ptyNodeAshDB;
   // target frequency selection
   int32_t                  bJtagRtck;                                      // target using adaptive clock (RTCK)
   uint32_t         uiRtckTimeout;                                  // adaptive clock timeout
   uint32_t        ulJtagFreqHz;                                   // selected JTAG frequency in Hz
   uint32_t uiJTAGFreqIndex; // This is only for Opella-USB JTAG Freq selection. Should be removed later.
   char                 szJtagFreq[_MAX_PATH];                          // frequency value stored as string
   // target voltage selection
   uint32_t        ulTargetVoltagemV;                              // target voltage in mV, 0 for match target
   // target processor settings
   uint32_t        ulProcessor;
   TyProcessorFamily    tyProcessorFamily;
   ProcessorConfig      tyProcConfig;
   TyXMLNode          * ptyNodeProcessor;
   // error handling
   char                 szError[_MAX_PATH];
   char                 szLastError[_MAX_PATH*8];
   // file download feature
   char                 szDownloadFile[_MAX_PATH];
   unsigned char        bELFType;                                       // downloaded file is ELF (SREC otherwise)
   uint32_t        ulDownloadAddr;
   // GDB server monitor
   char                 pszMonitorCommand[_MAX_PATH];                   // GDB server monitor command
   // registers
   TyRegDef           * ptyRegDefArray;
   uint32_t         uiRegDefArrayAllocCount;
   uint32_t         uiRegDefArrayEntryCount;

   TyGroupDef         * ptyGroupDefArray;
   uint32_t         uiGroupDefArrayAllocCount;
   uint32_t         uiGroupDefArrayEntryCount;

   uint32_t         uiResetDuration;
   uint32_t         uiResetDelay;
#ifdef ARM
   char                 szDevRegFile[_MAX_PATH];
   int32_t                 bDevRegFile;
#endif
#ifdef ARC
	uint32_t        ulCurrentTapNumber;
	char                 szRegFile[MAX_TAPS_IN_CHAIN][_MAX_PATH];
	uint32_t        ulNumberOfRegisters;
	unsigned char        bIsRegFilePresent;
	unsigned char        bIsMultiRegFilePresent[MAX_TAPS_IN_CHAIN];
	unsigned char        bMultiRegFileDetected;
	uint32_t        ulMultiRegFileCount;
	unsigned char        bSingleRegFile;
	uint32_t        ulCurActiveCore;
	char                 szCoreRegFile[_MAX_PATH];
	int32_t                  bIsCoreRegFilePresent;
	//int32_t                  iTapNumberPresent;
	//int32_t                  iFirstARCCore;
#endif
#ifdef RISCV
	uint32_t        ulCurrentTapNumber;
	char                 szRegFile[MAX_TAPS_IN_CHAIN][_MAX_PATH];
	uint32_t        ulNumberOfRegisters;
	unsigned char        bIsRegFilePresent;
	unsigned char        bIsMultiRegFilePresent[MAX_TAPS_IN_CHAIN];
	unsigned char        bMultiRegFileDetected;
	uint32_t        ulMultiRegFileCount;
	unsigned char        bSingleRegFile;
	uint32_t        ulCurActiveCore;
	char                 szCoreRegFile[_MAX_PATH];
	int32_t                  bIsCoreRegFilePresent;
	int32_t                  iTapNumberPresent;
	int32_t                  iFirstARCCore;
	int32_t					 DiscoveryAdvanceInfo;
	//Variables for storing the memory address and size mentioned in the case of familyriscv.xml
	uint32_t			ulXMLRISCVMemBlockCount;					//number of memarea mentioned in familyriscv.xml
	TyMemoryAccessInfo   tyriscvMemoryAccess[MAX_MEM_BLOCKS];
	// Indicate whether the core is a PULPino core
	unsigned char		 ucIsPulpinoCore;
	char				 bIsVegaBoard;
	uint32_t        ulCore;
	uint32_t		 ulNumberOfHarts;
	uint32_t			ulAbits;
	unsigned char  ucTargetResetFlag;
#endif
} TyServerInfo;

//structure for discovery information in case of RISCV
struct TyDiscoveryData {
	uint32_t ulIdleOut;						//waiting cycle 
	uint32_t ulVersion;					//spec version
	uint32_t ulXlen;						//lenght of the gprs reg
	uint32_t ulFloatingProc;					//for checking the floating point instruction size
	uint32_t ulAbsCommSupport;
	uint32_t ulProgBuffRegSupp;
	uint32_t ulMemoryMechSupport;   //contains memory mechanism (1- system bus, 2-program buffer, 4 -abstract access memory).
	uint32_t ulArch;
	uint32_t ulAbsCSRSupp;
	uint32_t ulTriggers;
	uint32_t ulFpValue;
	uint32_t ulS1Value;
	uint32_t ulFence_iSupport;				//support fence_i or not
};

#ifdef RISCV
//riscv thread info
struct _Tythread {
	uint32_t ulThreadId;
	uint32_t ulIsActiveThread;
	uint32_t ulCurrentThread;
	uint32_t uiCurrentMemMechanism;					//hart memory access mechanism
	struct TyDiscoveryData *ptyDiscovery;
}*tyThrdInfo;
#endif //RISCV
#endif// _GDBTY_H_
