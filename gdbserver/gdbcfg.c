/****************************************************************************
       Module: gdbcfg.c
     Engineer: Nikolay Chokoev
  Description: Ashling GDB Server, target configuration
Date           Initials    Description
06-NOV-2007    NCH         initial
31-Mar-2008    VK/RS       Modified for ARM Support
13-Jul-2008    NCH         MIPS GDB Server support
31-Dec-2009    SVL         Modified for PathFinder-XD's Peripheral Register 
                           View.
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbglob.h" 
#include "gdbxml.h"
#include "gdbty.h"
#include "gdbcfg.h"
#include "gdberr.h"
#include "gdbutil.h"
#include "gdblow.h"

#ifdef MIPS
#include "protocol/mips/commdefs.h"
#include "gdbserver/mips/gdbmips.h"

// Memory Spaces, MEM_REG is never sent to Emulator
//todo: clean it
TyEnumLookup tyEnumLookup[] = { 
   {"EJTAG26StyleTraceSupport"      ,(int32_t)EJTAG26StyleTraceSupport     },
   {"EJTAG20StyleTraceSupportStd"   ,(int32_t)EJTAG20StyleTraceSupportStd  },
   {"EJTAG20StyleTraceSupportExt"   ,(int32_t)EJTAG20StyleTraceSupportExt  },
   {"TraceSupportNotAvailable"      ,(int32_t)TraceSupportNotAvailable     },
   {"StandardR3000"                 ,(int32_t)StandardR3000                },
   {"StandardR4000"                 ,(int32_t)StandardR4000                },
   {"SmartMIPSR4000"                ,(int32_t)SmartMIPSR4000               },
   {"UnSupportedTLB"                ,(int32_t)UnSupportedTLB               },
   {"MIPS_I"                        ,(int32_t)Disa_MIPS_I                  },
   {"MIPS_II"                       ,(int32_t)Disa_MIPS_II                 },
   {"MIPS_III"                      ,(int32_t)Disa_MIPS_III                },
   {"MIPS_IV"                       ,(int32_t)Disa_MIPS_IV                 },
   {"MIPS_32"                       ,(int32_t)Disa_MIPS_32                 },
   {"MIPS_64"                       ,(int32_t)Disa_MIPS_64                 },
   {"MIPS_4KSc"                     ,(int32_t)Disa_MIPS_4KSc               },
   {"MIPS_All"                      ,(int32_t)Disa_MIPS_All                },
   {"MIPS_UnSpecified"              ,(int32_t)Disa_MIPS_UnSpecified        },
   {"NO_PRESCALAR"                  ,(int32_t)NO_PRESCALAR                 },
   {"PRESCALAR"                     ,(int32_t)PRESCALAR                    },
   {"P3_FOR_XDATA"                  ,(int32_t)P3_FOR_XDATA                 },
   {"P3_FOR_IO"                     ,(int32_t)P3_FOR_IO                    },
   {"P3_AUTO"                       ,(int32_t)P3_AUTO                      },
   {"NO_WDOG"                       ,(int32_t)NO_WDOG                      },
   {"WDOG_NOT_USED"                 ,(int32_t)WDOG_NOT_USED                },
   {"ENABLE_WDOG"                   ,(int32_t)ENABLE_WDOG                  },
   {"WDOG_HWDI_SUPPORT"             ,(int32_t)WDOG_HWDI_SUPPORT            }, 
   {"EJTAG26"                       ,(int32_t)EJTAG26Style                 }, 
   {"EJTAG20Std"                    ,(int32_t)EJTAG20StyleStd              }, 
   {"EJTAG20Ext"                    ,(int32_t)EJTAG20StyleExt              },
   {"XDATA_SWITCH"                  ,(int32_t)XDATA_SWITCH                 },
   {"PORT_SWITCH"                   ,(int32_t)PORT_SWITCH                  },
   {"SFR_SWITCH_EXTEN"              ,(int32_t)SFR_SWITCH_EXTEN             },
   {"SFR_SWITCH_BANKSW"             ,(int32_t)SFR_SWITCH_BANKSW            },
   {"SFR_SWITCH_ROMBK"              ,(int32_t)SFR_SWITCH_ROMBK             },
   {"SFR_SWITCH_LBS"                ,(int32_t)SFR_SWITCH_LBS               },
   {"SFR_SWITCH_HBS"                ,(int32_t)SFR_SWITCH_HBS               },
   {"SFR_SWITCH_LBS_HBS"            ,(int32_t)SFR_SWITCH_LBS_HBS           },
   {"SFR_SWITCH_TAMAEMU"            ,(int32_t)SFR_SWITCH_TAMAEMU           },
   {"UndefinedEEWrRt"               ,(int32_t)UndefinedEEWrRt              },
   {"P8xx5000EEWrRt"                ,(int32_t)P8xx5000EEWrRt               },
   {"SmartMXEEWrRt"                 ,(int32_t)SmartMXEEWrRt                },
   {"MF2D80EEWrRt"                  ,(int32_t)MF2D80EEWrRt                 },
   {"MF2D8184EEWrRt"                ,(int32_t)MF2D8184EEWrRt               },
   {"StandardCodeRdRt"              ,(int32_t)StandardCodeRdRt             },
   {"MF2D80CodeRdRt"                ,(int32_t)MF2D80CodeRdRt               },
   {"MF2D8184CodeRdRt"              ,(int32_t)MF2D8184CodeRdRt             },
   {"UndefinedProbe"                ,(int32_t)UndefinedProbe               },
   {"PRU_XA"                        ,(int32_t)PRU_XA                       },
   {"PRU_S3"                        ,(int32_t)PRU_S3                       },
   {"Disa_AVR32_Morgan"             ,(int32_t)Disa_AVR32_Morgan            },
   {"Disa_AVR32_Bravo"              ,(int32_t)Disa_AVR32_Bravo             }, 
   {"Match_Target"                  ,(int32_t)Match_Target                 }, 
   {"Fixed_3v3"                     ,(int32_t)Fixed_3v3                    } 
   };
#define  MAX_ENUM_LOOKUP_ENTRIES    ((int32_t)(sizeof(tyEnumLookup) / sizeof(TyEnumLookup)))
#else
// enumeration table, see CFG_Enum() for more details
static TyEnumLookup tyEnumLookup[] = 
   { 
   {"Default"        ,0       },    // add more items into the table if you wish
   };
#define  MAX_ENUM_LOOKUP_ENTRIES    ((int32_t)(sizeof(tyEnumLookup) / sizeof(TyEnumLookup)))
#endif

// external functions
extern TyServerInfo tySI;
extern void CFG_ProcessorNode(TyXMLNode *ptyThisNode);
// local functions
static void CFG_FreeRegDefArray(void);

/****************************************************************************
     Function: CFG_Init
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Initialize configuration
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t CFG_Init(void)
{
   int32_t iError;

   if ((iError = XML_ReadFile(GDBSERV_XML_FAMILY_FILE, &tySI.tyXMLFile)) != 0)
      return iError;
   tySI.ptyNodeAshDB = XML_GetNode(XML_GetBaseNode(&tySI.tyXMLFile), 
                                   "AshlingProcessorDataBase");
   if (tySI.ptyNodeAshDB == NULL)
      {
      sprintf(tySI.szError, GDB_EMSG_INV_DEV_DATABASE, tySI.tyXMLFile.szName);
      return GDBSERV_CFG_ERROR;
      }
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: CFG_FreeInitial
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Free configuration
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void CFG_FreeInitial(void)
{
   XML_Free(&tySI.tyXMLFile);
}

/****************************************************************************
     Function: CFG_FreeAll
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Free all configuration structures.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void CFG_FreeAll(void)
{
   XML_Free(&tySI.tyXMLFile);
   CFG_FreeRegDefArray();
}

/****************************************************************************
     Function: CFG_Configure
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - error code
  Description: Configure GDB server.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
03-Sep-2008    SPC         Allocating memory for ptyMDInfo in EJTAG console
                           mode to fix a crash.
21-Oct-2009    SVL         Modified for Device register file support in ARM
*****************************************************************************/
int32_t CFG_Configure(void)
{
#ifndef RISCV
   int32_t iError;
#endif
   // in JTAG console mode, the device name is an option
   if (tySI.bJtagConsoleMode && 
      ((tySI.ulProcessor == 0) || (tySI.ptyNodeProcessor == NULL)))
      {
	   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo = (TyMDInfo*)calloc(1,sizeof(TyMDInfo));
	   return GDBSERV_NO_ERROR;
      }
   if ((tySI.ulProcessor == 0) || (tySI.ptyNodeProcessor == NULL))
      {
      sprintf(tySI.szError, "As target device must be specified, use the --device option.");
      return GDBSERV_OPT_ERROR;
      }
   // Process the Processor node and all its children...
   CFG_ProcessorNode(tySI.ptyNodeProcessor);

#ifndef RISCV
   // first add core register definitions...
   if ((iError = LOW_AddCoreRegDefArrayEntries()) != GDBSERV_NO_ERROR)
      return iError;
   // configure processor
#ifdef ARM
   /* only if device register file is specified */
   if(tySI.bDevRegFile)
#endif
   {
   if ((iError = CFG_ProcessRegFile()) != GDBSERV_NO_ERROR)
      return iError;
   }   
   // now index commonly used registers...
   if ((iError = LOW_IndexCommonRegDefArrayEntries()) != GDBSERV_NO_ERROR)
      return iError;
#endif
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: CFG_FamilyGroup
     Engineer: Vitezslav Hola
        Input: *pszName    : Family group name or NULL
       Output: TyProcFamGrp : corresponding enum
  Description: 
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
// TyProcFamGrp CFG_FamilyGroup(char * pszName)
// {
//    if (pszName == NULL)
//       return UndefinedGroup;
//    if (_stricmp(pszName, "UndefinedGroup") == 0)
//       return UndefinedGroup;
//    if (_stricmp(pszName, "_8xC190") == 0)
//       return _8xC190;
//    if (_stricmp(pszName, "_8xC51MX_Grp") == 0)
//       return _8xC51MX_Grp;
//    if (_stricmp(pszName, "_8xC55x") == 0)
//       return _8xC55x;
//    if (_stricmp(pszName, "_8xC66x") == 0)
//       return _8xC66x;
//    if (_stricmp(pszName, "Calva") == 0)
//       return Calva;
//    if (_stricmp(pszName, "HiPerSmart") == 0)
//       return HiPerSmart;
//    if (_stricmp(pszName, "Mifare") == 0)
//       return Mifare;
//    if (_stricmp(pszName, "P8RF5000") == 0)
//       return P8RF5000;
//    if (_stricmp(pszName, "P8WE5000") == 0)
//       return P8WE5000;
//    if (_stricmp(pszName, "Painter1") == 0)
//       return Painter1;
//    if (_stricmp(pszName, "Painter2") == 0)
//       return Painter2;
//    if (_stricmp(pszName, "PCD509X") == 0)
//       return PCD509X;
//    if (_stricmp(pszName, "PCD509XY") == 0)
//       return PCD509XY;
//    if (_stricmp(pszName, "PNX") == 0)
//       return PNX;
//    if (_stricmp(pszName, "SmartMX") == 0)
//       return SmartMX;
//    if (_stricmp(pszName, "SmartXA1") == 0)
//       return SmartXA1;
//    if (_stricmp(pszName, "SmartXA2") == 0)
//       return SmartXA2;
//    if (_stricmp(pszName, "Tamaemu") == 0)
//       return Tamaemu;
//    if (_stricmp(pszName, "NoDebugCore") == 0)
//       return NoDebugCore;
//
//    return UndefinedGroup;
// }

/****************************************************************************
     Function: CFG_Enum
     Engineer: Vitezslav Hola
        Input: char *pszName - enum name in string form
       Output: int32_t - corresponding int32_t value of enum
  Description: Convert string into index (enumeration)
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t CFG_Enum(char *pszName)
{
   int32_t iIndex;
   if (pszName == NULL)
      return 0;
   for (iIndex=0; iIndex < MAX_ENUM_LOOKUP_ENTRIES; iIndex++)
      {
      if (strcasecmp(pszName, tyEnumLookup[iIndex].pszName) == 0)
         return tyEnumLookup[iIndex].iEnumValue;
      }
   return 0;
}

/****************************************************************************
     Function: CFG_AddRegDefArrayEntry
     Engineer: Vitezslav Hola
        Input: char *pszRegName - register/bit name
               uint32_t ulAddress - address
               unsigned char ubMemType - memory type: reg, physical, etc
               unsigned char ubByteSize - size of containing reg in bytes (1, 2 or 4)
               unsigned char ubBitOffset - offset in bits (0..31)
               unsigned char ubBitSize - size in bits (1..32)
               int32_t iFullRegIndex - index to containing reg
       Output: int32_t - error code
  Description: Add a register definition to the array
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t CFG_AddRegDefArrayEntry( const char *pszRegName, 
							uint32_t ulAddress, 
							TyMemTypes tyMemType, 
							unsigned char ubByteSize, 
							unsigned char ubBitOffset,
                            unsigned char ubBitSize, 
							int32_t iFullRegIndex,
                            int32_t iRegDefType
                            )
{
   uint32_t uiRegNameLen;
   TyRegDef *ptyNewRegDefArray;

   if (tySI.uiRegDefArrayEntryCount >= tySI.uiRegDefArrayAllocCount)
      {
      // reallocate array adding a chunk of entries...
      ptyNewRegDefArray = (TyRegDef*)realloc(tySI.ptyRegDefArray, (((uint64_t)tySI.uiRegDefArrayAllocCount+GDBSERV_REGDEF_ALLOC_CHUNK) * sizeof(TyRegDef)));
      if (ptyNewRegDefArray == NULL)
         {
         sprintf(tySI.szError, "Insufficient memory while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
         return GDBSERV_CFG_ERROR;
         }
      // clear new chunk to 0...
      memset(&ptyNewRegDefArray[tySI.uiRegDefArrayAllocCount], 0, (GDBSERV_REGDEF_ALLOC_CHUNK * sizeof(TyRegDef)));
      tySI.ptyRegDefArray = ptyNewRegDefArray;
      tySI.uiRegDefArrayAllocCount += GDBSERV_REGDEF_ALLOC_CHUNK;
      }
   // sanity check, this should not happen...
   if (tySI.uiRegDefArrayEntryCount >= tySI.uiRegDefArrayAllocCount)
      {
      sprintf(tySI.szError, "Internal error 1 while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }
   uiRegNameLen = (uint32_t)strlen(pszRegName);
   // allocate storage for the new register name...
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName = (char*)malloc((uint64_t)uiRegNameLen+1);
   if (tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName == NULL)
      {
      sprintf(tySI.szError, "Insufficient memory while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }
   // copy the new register name...
   strncpy(tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName, pszRegName, uiRegNameLen);
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName[uiRegNameLen] = '\0';
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ulAddress = ulAddress;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubMemType = tyMemType;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubByteSize = ubByteSize;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubBitOffset = ubBitOffset;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubBitSize = ubBitSize;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].iFullRegIndex = iFullRegIndex;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].iRegDefType = iRegDefType;

   tySI.uiRegDefArrayEntryCount++;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: CFG_FreeRegDefArray
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Free the register definition array
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void CFG_FreeRegDefArray(void)
{
   uint32_t uiIndex;
   if (tySI.ptyRegDefArray == NULL)
      return;
   for (uiIndex=0; uiIndex < tySI.uiRegDefArrayEntryCount; uiIndex++)
      {
      if (tySI.ptyRegDefArray[uiIndex].pszRegName != NULL)
         {
         free(tySI.ptyRegDefArray[uiIndex].pszRegName);
         }
      }
   free(tySI.ptyRegDefArray);
   free(tySI.ptyGroupDefArray);
   tySI.ptyRegDefArray = NULL;
   tySI.uiRegDefArrayEntryCount = tySI.uiRegDefArrayAllocCount = 0;
}

