/****************************************************************************
       Module: gdbmain.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, startup
Date           Initials    Description
08-Feb-2008    VH          Initial
30-Apr-2008    VK	       Modified for ARM
15-Jan-2009    SPC         Opella-USB support added for MIPS
23-Sep-2009    SVL         Added the support for 'PFXD mode'
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdbopt.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbserv.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/jtagcon/gdbjcon.h"
#include "gdbserver/moncommand/gdbmonitor.h"
#include "gdbserver/flashprog/gdbflashprog.h"

// global variable
TyServerInfo tySI;   // global structure with GDB server information
#ifdef ARM
char filename[_MAX_PATH];
#endif
/****************************************************************************
     Function: PrintTitleAndVersion
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Print program title and version
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
void PrintTitleAndVersion(void)
{
   PrintMessage(INFO_MESSAGE, "%s", GDBSERV_PROG_TITLE);
   PrintMessage(INFO_MESSAGE, "%s\n", GDBSERV_PROG_VERSION);
}

/****************************************************************************
     Function: PrepareEnviromentVariables
     Engineer: Vitezslav Hola
        Input: none               
       Output: none
  Description: Prepare enviroment variables for GDB server.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void PrepareEnviromentVariables(void)
{
#ifdef ARC
   //set variable for the ARC dll
#ifdef __LINUX
   (void)setenv(GDB_ARC_INI_ENV_VARIABLE, GDB_ARC_INI_DEFAULT_NAME, 1);
#else
   char szEnvName[_MAX_PATH];
   strcpy(szEnvName, GDB_ARC_INI_ENV_VARIABLE);
   strcat(szEnvName, "=");
   strcat(szEnvName, GDB_ARC_INI_DEFAULT_NAME);
   (void)putenv(szEnvName);
#endif
#endif
#ifdef RISCV
   //set variable for the ARC dll
#ifdef __LINUX
   (void)setenv(GDB_RISCV_INI_ENV_VARIABLE, GDB_RISCV_INI_DEFAULT_NAME, 1);
#else
   char szEnvName[_MAX_PATH];
   strcpy(szEnvName, GDB_RISCV_INI_ENV_VARIABLE);
   strcat(szEnvName, "=");
   strcat(szEnvName, GDB_RISCV_INI_DEFAULT_NAME);
   (void)putenv(szEnvName);
#endif
#endif
}

/****************************************************************************
     Function: RestoreEnviromentVariables
     Engineer: Vitezslav Hola
        Input: none               
       Output: none
  Description: Restore enviroment variables when exiting GDB server.
Date           Initials    Description
08-Feb-2008    VH          Initial
*****************************************************************************/
void RestoreEnviromentVariables(void)
{
#ifdef ARC
#ifdef __LINUX
   (void)unsetenv(GDB_ARC_INI_ENV_VARIABLE);
#else
   (void)putenv(GDB_ARC_INI_ENV_VARIABLE);
#endif
#endif
#ifdef RISCV
#ifdef __LINUX
   (void)unsetenv(GDB_RISCV_INI_ENV_VARIABLE);
#else
   (void)putenv(GDB_RISCV_INI_ENV_VARIABLE);
#endif
#endif
}

/****************************************************************************
     Function: MainExit
     Engineer: Nikolay Chokoev
        Input: iMainRet : reason to exit main function (error code)
       Output: none
  Description: Cleanup all before exit from 'main'.
Date           Initials    Description
08-Feb-2008    NCH          Initial
21-Mar-2010    DK           Removed "Press any key to continue" option
*****************************************************************************/
void MainExit(int32_t iMainRet)
{
   NOREF(iMainRet);
   // free all storage associated with config
   CFG_FreeAll();
   // restore enviroment variables
   RestoreEnviromentVariables();
   /*Removed Press a Key to continue...*/
   // pause for user confirmation in error case...
   /*if (iMainRet != 0)
      {
     // PrintMessage(INFO_MESSAGE, "Press any key to continue...");
      //(void)GetchWithoutEcho();
      }*/
   // close debug file if used
   if (tySI.pDebugFile != NULL)
      {
      fflush(tySI.pDebugFile);
      fclose(tySI.pDebugFile);
      }
}

/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: int32_t argc - number of program arguments
               char *argv[] - array of argument (strings) from command line
       Output: int32_t - program return value
  Description: Main GDB server function.
Date           Initials    Description
08-Feb-2008    VH          Initial
08-Feb-2008    NCH         Remove 'goto'
14-Nov-2008    SPC         Disconnecting from target before exiting gdbserver
29-Jan-2009    SPC         Opella-USB support added for MIPS, fixed ld-exe cmd.
14-Apr-2017    AS          Added multi register file support
06-June-2019   HS          Added check for RISCV target reset.
*****************************************************************************/
int32_t main(int32_t argc, char *argv[])
{
   int32_t iMainRet = 0;
   char szDiskWare1[_MAX_PATH];
   char szFirmWare [_MAX_PATH];
   char szFpgaWare1[_MAX_PATH];
   char szFpgaWare2[_MAX_PATH];
   char szFpgaWare3[_MAX_PATH];
   char szFpgaWare4[_MAX_PATH];

   // initialize global GDB server structure with zeros
   memset(&tySI, 0, sizeof(tySI));
#ifdef ARM
   memset(filename,0,_MAX_PATH);
#endif
   //ensure that only core 0 is handled in SetupARCReg()
   tySI.ulMultiRegFileCount = 1;
   tySI.ulCurrentTapNumber = 0;

   // print GDB server header and version
   PrintTitleAndVersion();
   // prepare enviroment variables for GDB server
   PrepareEnviromentVariables();
   // initialise config, parses XML file
   if (CFG_Init())
      {
      PrintMessage(ERROR_MESSAGE, tySI.szError);
      iMainRet = 1;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   SetDefaultOptions();
   // process GDB server command line options
   if (ProcessCmdLineArgs(argc, argv))
      {  // invalid command line options
      PrintMessage(ERROR_MESSAGE, tySI.szError);
      iMainRet = 1;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }

   //use multi-register file settings for initialisation
   if (   (!tySI.bIsRegFilePresent)
       && (tySI.ulCurrentTapNumber == 0))
       tySI.ulCurrentTapNumber = 1;

   if (tySI.bVersion)
      {  // have already displayed version, so exit.
      iMainRet = 0;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   if (tySI.bHelp)
      {  // requesting help
      DisplayHelp();
      ShowGDBMonitorCommandsHelp();
      iMainRet = 0;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   // error scenario trying to verify download file 
   // without specifying ld-exe options
   if ((!tySI.bDownloadFile)&&(tySI.bVerifyDownloadFile))
       {
       PrintMessage(ERROR_MESSAGE, GDB_EMSG_VERIFY_WITHOUT_LDEXE);
       iMainRet = 1;    // finish program
       MainExit(iMainRet);
       // exit GDB server
       return iMainRet;
       }
   if (PreConfigVerifyOptions())
      {
      PrintMessage(ERROR_MESSAGE, tySI.szError);
      iMainRet = 1;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   // configuration itself
   if (CFG_Configure())
      {
      PrintMessage(ERROR_MESSAGE, tySI.szError);
      iMainRet = 1;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   if (PostConfigVerifyOptions())
      {
      PrintMessage(ERROR_MESSAGE, tySI.szError);
      iMainRet = 1;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   // free any initial storage associated with config
   CFG_FreeInitial();
   //todo: in init fn!
   tySI.bIsConnected = FALSE;

   // run JTAG console if selected
   if (tySI.bJtagConsoleMode)
      {
      unsigned char bJtagConsoleModeSupported = 0;
      // check if JTAG console is supported
      switch (tySI.tyGdbServerTarget)
         {
         case GDBSRVTGT_OPELLAXD:
#ifdef MIPS
         case GDBSRVTGT_OPELLA:
#endif
            bJtagConsoleModeSupported = 1;
            break;
#ifndef MIPS
         case GDBSRVTGT_OPELLA:
#endif
         case GDBSRVTGT_INVALID:
         default:
            PrintMessage(INFO_MESSAGE, "JTAG console mode is not available for selected probe.");
            break;
         }
      // start JTAG console mode only if supported for current probe
      if (bJtagConsoleModeSupported)
         {
         int32_t bDeviceSpecified;
         // disable logging debug messages (when --debug-file is specified, log just commands and responses)
         tySI.bDebugOutput = 0;
         // JTAG console may support architecture specific commands like read/write memory, etc. however it needs to have device specified for that purpose
         if(tySI.ulProcessor == 0)
            bDeviceSpecified = 0;   // user did not specified device type
         else
            bDeviceSpecified = 1;   // user specified device type

         if (LOW_ConnectJcon(bDeviceSpecified) != GDBSERV_NO_ERROR)
           {
           iMainRet = 1;
           MainExit(iMainRet);   // error when initializing connection
           // exit GDB server
           return iMainRet;
           }
         RunJTAGConsole(bDeviceSpecified);   // run JTAG console
         if (LOW_DisconnectJcon() != GDBSERV_NO_ERROR)           // finish JTAG console
            iMainRet = 1;                    // successful
         else
            iMainRet = 0;
         MainExit(iMainRet);                 // finish program
         // exit GDB server
         return iMainRet;
         }
      iMainRet = 1;                          // JTAG console is not supported
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
   // not using JTAG console mode
   // initialise communication with target
#ifdef ARM
   strcpy(filename,argv[0]);
#endif
#if !defined(ARC) &&  !defined(RISCV)
   if (tySI.bPfxdMode)
      {
      PrintMessage(INFO_MESSAGE, "Starting in PFXD mode...");
      if(GDBSERV_NO_ERROR != LOW_StartPFXDMode())
         {
         iMainRet = 1;
         MainExit(iMainRet);
         return iMainRet;
         }
      if (!ServerMain(tySI.usGdbPort))
         {
         iMainRet = 1;
         MainExit(iMainRet);
         }
      return iMainRet;
      }
   #endif
#ifdef RISCV   
   //loadlibrary
   if (LOW_LoadLibrary() != GDBSERV_NO_ERROR)
   {
	   iMainRet = 1;
	   (void)LOW_Disconnect();
	   MainExit(iMainRet);
	   // exit GDB server
	   return iMainRet;
   }
   if (LOW_GetDWFWVersions(szDiskWare1, szFirmWare, szFpgaWare1, szFpgaWare2, szFpgaWare3, szFpgaWare4) != GDBSERV_NO_ERROR)
   {
	   iMainRet = 1;
	   PrintMessage(ERROR_MESSAGE, GDB_EMSG_CANNOT_GET_FWDW_VER);
	   MainExit(iMainRet);
	   // exit GDB server
	   return iMainRet;
   }
   //if target reset flag is set,Reset the target.
   if (tySI.ucTargetResetFlag)
   {
	   //0 as default hart id after reset
	   if (LOW_TargetReset(0))
	   {
		 iMainRet = 1;
		PrintMessage(ERROR_MESSAGE, GDB_EMSG_UNABLE_TO_RESET_TARGET);
		return iMainRet;
	   }
	   else
	   {
		   PrintMessage(INFO_MESSAGE,"Target reset is asserted");
	   }
   }

   //get target info
   if (LOW_Connect(GetEmulatorName(),szDiskWare1, szFirmWare, tySI.szJtagFreq) != GDBSERV_NO_ERROR)
   {
	   iMainRet = 1;
	   (void)LOW_Disconnect();
	   MainExit(iMainRet);
	   // exit GDB server
	   return iMainRet;
   }
  
   tySI.bIsConnected = TRUE;
   //todo: this message for MIPS
   // show info about connected target
   PrintMessage(INFO_MESSAGE, "Connected to target device configured as: %s\n(currently in %s Endian mode).",
                tySI.tyProcConfig.szProcName, LOW_IsBigEndian(0)?"Big":"Little");

#else
   if (LOW_Connect() != GDBSERV_NO_ERROR)
      {
      iMainRet = 1;
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }

   tySI.bIsConnected = TRUE;
   //todo: this message for MIPS
   // show info about connected target
   PrintMessage(INFO_MESSAGE, "Connected to target device configured as: %s\n(currently in %s Endian mode).",
                tySI.tyProcConfig.szProcName, LOW_IsBigEndian(0)?"Big":"Little");

   // show info about probe (diskware, firmware, etc.)
   if (LOW_GetDWFWVersions(szDiskWare1, szFirmWare, szFpgaWare1, szFpgaWare2, szFpgaWare3, szFpgaWare4) != GDBSERV_NO_ERROR)
      {
      iMainRet = 1;
      PrintMessage(ERROR_MESSAGE, GDB_EMSG_CANNOT_GET_FWDW_VER);
      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
#endif   
   PrintMessage(INFO_MESSAGE, "Connected to target via %s \n(diskware:%s, firmware:%s) at %s.", GetEmulatorName(), szDiskWare1, szFirmWare, tySI.szJtagFreq);

   // checking download file feature
#ifdef RISCV
   if (!tySI.bIsVegaBoard)
   {
	   if ((tySI.ulProcessor == 1 && tyThrdInfo[0].ptyDiscovery->ulArch != 32) || (tySI.ulProcessor == 2 && tyThrdInfo[0].ptyDiscovery->ulArch != 64) || (tySI.ulProcessor == 3 && tyThrdInfo[0].ptyDiscovery->ulArch != 32))
	   {
		   PrintMessage(ERROR_MESSAGE, GDB_EMSG_UNK_DEVICE);
		   iMainRet = 1;
		   MainExit(iMainRet);
		   // exit GDB server
		   return iMainRet;
	   }
   }
#endif // RISCV
   if (tySI.bDownloadFile)
      {
      if (tySI.bVerifyDownloadFile)
         {  // verifying download
         if(!DownloadAndVerifyFile(tySI.szDownloadFile, tySI.bELFType))
            (void)ExecuteDownloadFile(tySI.szDownloadFile, tySI.ulDownloadAddr);
         }
      else
         {  // download without verification
         if(!DownloadFile(tySI.szDownloadFile, tySI.bELFType))
            (void)ExecuteDownloadFile(tySI.szDownloadFile, tySI.ulDownloadAddr);       
         }
      (void)LOW_Disconnect();
      iMainRet = 1;  
//      MainExit(iMainRet);
      // exit GDB server
      return iMainRet;
      }
  //displaying the discovery data;
#ifdef RISCV
   //default Arch size
  if (!tySI.bIsVegaBoard)
  {
	  uint32_t ulDisc;
	  PrintMessage(INFO_MESSAGE, "Info : Harts detected : %d", tySI.ulNumberOfHarts);

	  for (ulDisc = 0; ulDisc < tySI.ulNumberOfHarts; ulDisc++)
	  {
		  if (tyThrdInfo[ulDisc].ptyDiscovery->ulArch == 32)
		  {
			  tyThrdInfo[ulDisc].ptyDiscovery->ulXlen = 32;
		  }
		  if (tyThrdInfo[ulDisc].ptyDiscovery->ulArch == 64)
		  {
			  tyThrdInfo[ulDisc].ptyDiscovery->ulXlen = 64;
		  }
		  PrintMessage(INFO_MESSAGE, "Info : [%d] System architecture : RV%d", ulDisc, tyThrdInfo[ulDisc].ptyDiscovery->ulArch);
		  if (tyThrdInfo[ulDisc].ptyDiscovery->ulVersion == 1)
			  PrintMessage(INFO_MESSAGE, "Info : [%d] Debug version : v0.11", ulDisc);
		  if (tyThrdInfo[ulDisc].ptyDiscovery->ulVersion == 2)
			  PrintMessage(INFO_MESSAGE, "Info : [%d] Debug version : v0.13", ulDisc);
		  PrintMessage(INFO_MESSAGE, "Info : [%d] Number of hardware breakpoints available : %d ", ulDisc, tyThrdInfo[ulDisc].ptyDiscovery->ulTriggers);
		  PrintMessage(INFO_MESSAGE, "Info : [%d] XLEN of GPRs detected : %d bit", ulDisc, tyThrdInfo[ulDisc].ptyDiscovery->ulArch);
		  PrintMessage(INFO_MESSAGE, "Info : [%d] Number of program buffers: %d", ulDisc, tyThrdInfo[ulDisc].ptyDiscovery->ulProgBuffRegSupp);
		  PrintMessage(INFO_MESSAGE, "Info : [%d] Number of data registers: %d", ulDisc, tyThrdInfo[ulDisc].ptyDiscovery->ulAbsCommSupport);
		  if (tySI.DiscoveryAdvanceInfo == 1)
		  {
			  PrintMessage(INFO_MESSAGE, "Target info --show-target-info");
			  PrintMessage(INFO_MESSAGE, "Info : [%d] Size of address in DMI detected [Abits] : %d bits", ulDisc, tySI.ulAbits);

			  //Memory Mechanism info
			  if (tyThrdInfo[ulDisc].ptyDiscovery->ulMemoryMechSupport & SYSTEM_BUS_MEMORY_ACCESS)
			  {
				  PrintMessage(INFO_MESSAGE, "Info : [%d] Memory Access -> System bus", ulDisc);
			  }
			  if (tyThrdInfo[ulDisc].ptyDiscovery->ulMemoryMechSupport & PROGRAM_BUFFER_MEMORY_ACCESS)
			  {
				  PrintMessage(INFO_MESSAGE, "Info : [%d] Memory Access -> Program buffer", ulDisc);
			  }
			  if (tyThrdInfo[ulDisc].ptyDiscovery->ulMemoryMechSupport & ABSTRACT_COMMAND_MEMORY_ACCESS)
			  {
				  PrintMessage(INFO_MESSAGE, "Info : [%d] Memory Access -> Abstract access memory", ulDisc);
			  }


			  //Register mechnism info
			  if (tyThrdInfo[ulDisc].ptyDiscovery->ulAbsCommSupport >= 1)
			  {
				  PrintMessage(INFO_MESSAGE, "Info : [%d] Register [GPRs] access -> Abstract commands\n", ulDisc);
			  }
			  else
			  {
				  PrintMessage(INFO_MESSAGE, "Info : [%d] Register [GPRs] access -> Program buffer", ulDisc);
			  }

		  }
	  }
	}
  if (tySI.bIsVegaBoard)
  {
	  tyThrdInfo[0].ptyDiscovery->ulArch = 32;
  }

#endif
   // starting GDB server main loop
   if (ServerMain(tySI.usGdbPort))
      {
      iMainRet = 1;
      }   
   (void)LOW_Disconnect();
   MainExit(iMainRet);
   // exit GDB server
   return iMainRet;
}

