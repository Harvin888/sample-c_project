/****************************************************************************
       Module: gdbfiles.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, interface header file containing utils for file download
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBFILES_H_
#define GDBFILES_H_

// Linux build change...
#include "protocol/common/types.h"

// error codes for download and verification
#define USER_ABORT_DOWNLOAD                  1 
#define USER_ABORT_VERIFICATION              2
#define ERR_CANNOT_FIND_FILE                 3  
#define ERR_MEMORY_READ_FAILURE              4
#define ERR_MEMORY_VERIFY_FAILURE            5
#define ERR_MEMORY_WRITE_FAILURE             6
#define ERR_INVALID_SREC_RECORD              7
#define ERR_INVALID_HEX_RECORD               8

// function prototypes
int32_t ReadInputFile(char *pszInputFileName, uint32_t ulRelocation, uint32_t ulCodeMemStart, uint32_t ulCodeMemEnd, unsigned char bELFFile);
int32_t VerifyInputFile(char *pszInputFileName, uint32_t ulRelocation, uint32_t ulCodeMemStart, uint32_t ulCodeMemEnd, unsigned char bELFFile);

#endif // GDBFILES_H_

