/****************************************************************************
       Module: gdbflashprog.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, (Flash) file download interface header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#ifndef GDBFLASHPROG_H_
#define GDBFLASHPROG_H_

// Linux build change...
#include "protocol/common/types.h"

int32_t DownloadFile(char *pszDownloadFileName, unsigned char bELFFile);
int32_t DownloadAndVerifyFile(char *pszDownloadFileName, unsigned char bELFFile);
int32_t ExecuteDownloadFile(char *pszDownloadFileName, uint32_t ulDownloadEntryAddress);

#endif // GDBFLASHPROG_H_

