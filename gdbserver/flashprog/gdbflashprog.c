/****************************************************************************
       Module: gdbflashprog.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, (Flash) file download interface definitions
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "gdbserver/gdberr.h"
#include "gdbserver/flashprog/gdbfiles.h"
#include "gdbserver/flashprog/gdbflashprog.h"

// external functions
int32_t LOW_WritePC(uint32_t ulRegisterValue);
int32_t LOW_ReadPC(uint32_t *pulRegisterValue);
int32_t LOW_Execute(char* pszDownloadFileName);

/****************************************************************************
     Function: DownloadFile
     Engineer: Vitezslav Hola
        Input: char *pszDownloadFileName - pointer to download file name
               unsigned char bELFFile - 1 for ELF format, 0 for SREC format
       Output: int32_t - error code (0 is success)
  Description: download program specified by file to memory 
               (only SREC format supported)
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t DownloadFile(char *pszDownloadFileName, unsigned char bELFFile)
{
   assert(pszDownloadFileName != NULL);
   assert(bELFFile == 0);  // only SREC format currently supported
   return ReadInputFile(pszDownloadFileName, 0, 0, 0xFFFFFFFF, bELFFile);
}

/****************************************************************************
     Function: DownloadAndVerifyFile
     Engineer: Vitezslav Hola
        Input: char *pszDownloadFileName - pointer to download file name
               unsigned char bELFFile - 1 for ELF format, 0 for SREC format
       Output: int32_t - error code (0 is success)
  Description: Download program specified by file to memory. 
               It also verifies the download.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t DownloadAndVerifyFile(char *pszDownloadFileName, unsigned char bELFFile)
{   
   int32_t iError;
   assert(pszDownloadFileName != NULL);
   assert(bELFFile == 0);  // only SREC format currently supported

   iError = ReadInputFile(pszDownloadFileName, 0, 0, 0xFFFFFFFF, bELFFile);
   if (iError != 0)
      return iError;
   iError = VerifyInputFile(pszDownloadFileName, 0, 0, 0xFFFFFFFF, bELFFile);
   return iError;
}

/****************************************************************************
     Function: ExecuteDownloadFile
     Engineer: Vitezslav Hola
        Input: char *pszDownloadFileName - pointer to download file name
               uint32_t ulDownloadEntryAddress - entry point to start execution
       Output: int32_t - error code (0 is success)
  Description: Executes downloaded file from specified address (entry point).
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
int32_t ExecuteDownloadFile(char *pszDownloadFileName, uint32_t ulDownloadEntryAddress)
{
   uint32_t ulPCRegisterValue = 0x0;
   assert(pszDownloadFileName != NULL);
   assert(ulDownloadEntryAddress > 0);
   if(GDBSERV_NO_ERROR != LOW_WritePC(ulDownloadEntryAddress))  //assume error message is sufficient , lets test!!
      return 1;       
   if(!LOW_Execute(pszDownloadFileName))
      return 1;
   //this is a simple way of verifying the target address has a new PC value 
   // when testing in  debug mode
   (void)LOW_ReadPC(&ulPCRegisterValue);
   return 0;
}

