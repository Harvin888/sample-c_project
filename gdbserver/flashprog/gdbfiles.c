/****************************************************************************
       Module: gdbfiles.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, definition file containing utils for file download
Date           Initials    Description
12-Dec-2007    VH          Initial
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/flashprog/gdbfiles.h"

// local macros
#define DATA_BUFFER_SIZE              0x8000
#define MAX_CHARS_IN_RECORD              600
#define MAX_DATA_BYTES_IN_RECORD         300
#define MAX_STRING_SIZE_LOCAL           0xFF

// external functions
int32_t LOW_ReadMemory(uint32_t ulAddress, uint32_t ulCount, unsigned char *pucData);
int32_t LOW_WriteMemory(uint32_t ulAddress, uint32_t ulCount, unsigned char *pucData);

// local variables
static unsigned char pucDataBuffer[DATA_BUFFER_SIZE]; 

//Exported function
unsigned char HexToChar(unsigned char ucHexVal);
// local functions
static void SetupErrorMessage(int32_t iError, char *pszFileName, char *pszErrorText);
static int32_t GetSrecDataFromFileRecord(char *pszFileBuffer, char *pszDataBuffer, int32_t *piDataPresent, uint32_t *pulDataAddr, uint32_t *pulDataCount);
static uint32_t GetWordFromChars(char cChar1, char cChar2, char cChar3, char cChar4,
                                      char cChar5, char cChar6, char cChar7, char cChar8);
static unsigned char GetByteFromChars(char cChar1, char cChar2);
static int32_t BuildDataRecord(int32_t iMemType, int32_t iMemWriteType, uint32_t ulCodeStart, uint32_t ulCodeEnd, uint32_t ulAddress, 
                           uint32_t ulCount, char *pucData, char *pszInputFileName);

/****************************************************************************
     Function: ReadInputFile
     Engineer: Vitezslav Hola
        Input: char *pszInputFileName - pointer to download file name
               uint32_t ulRelocation - relocation size of code segment/data segment
               uint32_t ulCodeMemStart - start address of code segment/data segment
               uint32_t ulCodeMemEnd - end address of code segment/data segment
               unsigned char bELFFile - using ELF or SREC format
       Output: int32_t - error code (0 for success)
  Description: Read and process all records in the file, writing appropriate records to MIPS target memory
Date           Initials    Description
08-Feb-2007    VH          Initial & cleanup
08-Feb-2008    NCH         Remove 'goto'
****************************************************************************/
int32_t ReadInputFile(char *pszInputFileName, uint32_t ulRelocation, uint32_t ulCodeMemStart, uint32_t ulCodeMemEnd, unsigned char bELFFile)
{
   int32_t iError = 0;
   FILE *pFile = NULL;
   uint32_t ulDataAddr = 0;
   uint32_t ulDataCount = 0;
   uint32_t ulStartCodeAddress = 0xFFFFFFFF;
   uint32_t ulCount = 0;
   int32_t iDataPresent = 0;
   char szFileBuffer[MAX_CHARS_IN_RECORD];
   char szDataBuffer[MAX_DATA_BYTES_IN_RECORD];
   char szDisplayProgress[MAX_STRING_SIZE_LOCAL];

   assert(pszInputFileName != NULL);
   assert(ulRelocation == 0);
   assert(ulCodeMemStart == 0);
   assert(ulCodeMemEnd == 0xFFFFFFFF);
   assert(bELFFile == 0);                                               // only SREC supported
   // open file
   pFile = fopen (pszInputFileName, "rt");
   if (pFile == NULL)
      {
      SetupErrorMessage(ERR_CANNOT_FIND_FILE, pszInputFileName, NULL);
      return ERR_CANNOT_FIND_FILE;
      }                    
   PrintMessage(INFO_MESSAGE,"Downloading %s.", pszInputFileName);
   while (fgets((char *)szFileBuffer, MAX_CHARS_IN_RECORD, pFile) != NULL)
      {  // user may abort from download process at any stage by pressing any key
      if(KeyHit())
         {
         iError = USER_ABORT_DOWNLOAD;
         PrintMessage(INFO_MESSAGE, "Download aborted.");           
         if (pFile != NULL)
            fclose(pFile);
         return iError;
         }
      if ((ulCount % 50) == 0)
         {                
         sprintf(szDisplayProgress,"* ");
         printf("%s", szDisplayProgress);
         }
      ulCount++;
      if(!bELFFile)
          iError = GetSrecDataFromFileRecord(szFileBuffer, szDataBuffer, &iDataPresent, &ulDataAddr, &ulDataCount);
      if (iError != 0)
         {
         if (pFile != NULL)
            fclose(pFile);
         return iError;
         }
      if (iDataPresent)
         {  // keep track of lowest address
         if (ulDataAddr < ulStartCodeAddress)
            ulStartCodeAddress = ulDataAddr;
         iError = BuildDataRecord(0x01, 0x00, ulCodeMemStart, ulCodeMemEnd, ulRelocation + ulDataAddr, ulDataCount, szDataBuffer, pszInputFileName);
         if (iError != 0)
            {
            if (pFile != NULL)
               fclose(pFile);
            return iError;
            }
         }
      }
   //finished download so stop displaying szDisplayProgress and go to new line
   printf("\n");
   iError = BuildDataRecord(0x01, 0x00, ulCodeMemStart, ulCodeMemEnd, ulRelocation + ulDataAddr, 0, szDataBuffer, pszInputFileName);
   if (pFile != NULL)
      fclose(pFile);
   return iError;
}

/****************************************************************************
     Function: VerifyInputFile
     Engineer: Daniel Madden
         Input: pszInputFileName     *: pointer to download file name
               ulRelocation           : relocation size of code segment/data segment
               CodeMemStart           : start address of code segment/data segment
               CodeMemEnd             : end address of code segment/data segment
               tyDownloadFileType     : file type for download file (currently only srec)
       Output: TyError as a return value, 0 if sucess and non-zero if error 
  Description: Read and process all records in the input Hex file, verifying
               appropriate records to emulator memory..
Date           Initials    Description
08-Feb-2007    VH          Initial & cleanup
08-Feb-2008    NCH         Remove 'goto'
****************************************************************************/
int32_t VerifyInputFile(char *pszInputFileName, uint32_t ulRelocation, uint32_t ulCodeMemStart, uint32_t ulCodeMemEnd, unsigned char bELFFile)
{
   int32_t iError = 0;
   FILE *pFile = NULL;
   int32_t iDataPresent = 0;
   uint32_t ulDataAddr = 0;
   uint32_t ulDataCount = 0;
   uint32_t ulCount, ulIndex;
   char szFileBuffer[MAX_CHARS_IN_RECORD];
   char szDataBuffer1[MAX_DATA_BYTES_IN_RECORD];
   char szDataBuffer2[MAX_DATA_BYTES_IN_RECORD];
   char szDisplayProgress[MAX_STRING_SIZE_LOCAL];

   assert(pszInputFileName != NULL);
   assert(ulRelocation == 0);
   assert(ulCodeMemStart == 0);
   assert(ulCodeMemEnd == 0xFFFFFFFF);
   assert(bELFFile == 0);     // only SREC supported
   szDataBuffer1[0] = '\0';
   szDataBuffer2[0] = '\0';
   // open file
   pFile = fopen (pszInputFileName, "rt");
   if (pFile == NULL)
      {
      SetupErrorMessage(ERR_CANNOT_FIND_FILE, pszInputFileName, NULL);
      return ERR_CANNOT_FIND_FILE;
      }
   PrintMessage(INFO_MESSAGE, "Verifying download of %s, press any key to abort.", pszInputFileName);
   ulCount = 0;
   // for all records in the input Hex file
   while (fgets((char *)szFileBuffer, MAX_CHARS_IN_RECORD, pFile) != NULL)
      {
      //user may abort from download process at any stage by pressing any key
      if(KeyHit())
         {          
         iError = USER_ABORT_VERIFICATION;
         PrintMessage(INFO_MESSAGE, "Verification aborted.");
         if (pFile != NULL)
            fclose(pFile);
         return iError;
         }
      if ((ulCount % 50) == 0)
         {
         sprintf(szDisplayProgress, "* ");
         printf("%s", szDisplayProgress);
         }
      ulCount++;
      if(bELFFile == 0)
         iError = GetSrecDataFromFileRecord(szFileBuffer, szDataBuffer1, &iDataPresent, &ulDataAddr, &ulDataCount);
      if (iError != 0)
         {
         if (pFile != NULL)
            fclose(pFile);
         return iError;
         }
      if (iDataPresent)
         {
         if (GDBSERV_NO_ERROR != LOW_ReadMemory(ulRelocation+ulDataAddr, ulDataCount, (unsigned char *)szDataBuffer2))
            {             
            iError = ERR_MEMORY_READ_FAILURE;
            SetupErrorMessage(iError, pszInputFileName, NULL);
            if (pFile != NULL)
               fclose(pFile);
            return iError;
            }
         // check each of the data bytes...
         for (ulIndex = 0; ulIndex < ulDataCount; ulIndex++)
            { 
            if (((ulRelocation + ulDataAddr + ulIndex) >= ulCodeMemStart) && ((ulRelocation+ulDataAddr + ulIndex) <= ulCodeMemEnd))
               {
               if (szDataBuffer1[ulIndex] != szDataBuffer2[ulIndex])
                  {
                  char szErrorText[MAX_STRING_SIZE_LOCAL];                   
                  sprintf(szErrorText, "Verification failed at 0x%02x. Expected:0x%02X, read:0x%02X.", 
                          (uint32_t)(ulRelocation+ulDataAddr+ulIndex), szDataBuffer1[ulIndex], szDataBuffer2[ulIndex]);
                  iError = ERR_MEMORY_VERIFY_FAILURE;
                  SetupErrorMessage(iError, pszInputFileName, szErrorText);					 
                  if (pFile != NULL)
                     fclose(pFile);
                  return iError;
                  }
               }
            }
         }
      }
   //finished verification so stop displaying szDisplayProgress and go to new line
   printf("\n");
   if (pFile != NULL)
      fclose(pFile);
   return iError;
}

/****************************************************************************
     Function: BuildDataRecord
     Engineer: Vitezslav Hola
       Input:  int32_t iMemType - type of MDI memory space used for writing
               int32_t iMemWriteType - type of MDI access type used for writing
               uint32_t ulCodeStart - start address of code segment/data segment
               uint32_t ulCodeEnd - end address of code segment/data segment
               uint32_t ulAddress - address to write to on target
               uint32_t ulCount - number of bytes to write to target
               char *pucData - pointer to buffer containing bytes to be downloaded to target
               char *pszInputFileName - pointer to input file name
       Output: int32_t - error code (0 success)
  Description: Gather as many records as possible into a data buffer before writing to emulator.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static int32_t BuildDataRecord(int32_t iMemType, int32_t iMemWriteType, uint32_t ulCodeStart, uint32_t ulCodeEnd, uint32_t ulAddress, 
                           uint32_t ulCount, char *pcData, char *pszInputFileName)
{
   int32_t iError = 0;
   uint32_t ulLocalCount, ulLocalAddress;
   unsigned char *pucLocalData = (unsigned char *)pcData;
   static uint32_t ulDataBufferAddress = 0;
   static uint32_t ulDataBufferCount = 0;
   assert(iMemType == 0x01);      // 0x01 means MEM_CODE, only supported currently
   assert(iMemWriteType == 0x00); // 0x00 means MEM_NORMAL_ACCESS, only supported currently
   // check if we are to flush the buffer...
   if (ulCount == 0) 
      {
      if (ulDataBufferCount != 0)
         {             
         if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulDataBufferAddress, ulDataBufferCount, pucDataBuffer))
            {
            SetupErrorMessage(ERR_MEMORY_WRITE_FAILURE, pszInputFileName, NULL);
            return ERR_MEMORY_WRITE_FAILURE;
            }
         }
      return 0;
      }
   // calculate the true address, count and data...
   ulLocalCount = ulCount;
   ulLocalAddress = ulAddress;
   // handle case when we are gone beyond the end of code area we can write to
   if (ulAddress > ulCodeEnd)
      return 0;
   if (ulCodeStart > ulAddress)
      {
      if (ulLocalCount < (ulCodeStart - ulAddress))
         ulLocalCount -= ulLocalCount;
      else
         ulLocalCount -= (ulCodeStart - ulAddress);
      pucLocalData += ulCodeStart - ulAddress;
      ulLocalAddress = ulCodeStart;
      }
   if (ulCodeEnd < (ulLocalAddress + (ulLocalCount - 1)))
      ulLocalCount = (ulCodeEnd - ulLocalAddress) + 1;
   // any data left
   if (ulLocalCount == 0)
      return 0;
   // is there enough space in our buffer for this
   if (ulDataBufferCount != 0)
      {
      if ((ulDataBufferCount + ulLocalCount) > DATA_BUFFER_SIZE)
         {
         if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulDataBufferAddress, ulDataBufferCount, pucDataBuffer))
            {
            SetupErrorMessage(ERR_MEMORY_WRITE_FAILURE, pszInputFileName, NULL);
            return ERR_MEMORY_WRITE_FAILURE;              
            }          
         ulDataBufferAddress = ulLocalAddress;
         ulDataBufferCount = ulLocalCount;
         memcpy(pucDataBuffer, pucLocalData, ulLocalCount);
         return iError;
         }
      // is this memory disjointed...
      if ((ulDataBufferCount + ulDataBufferAddress) != ulLocalAddress)
         {   
         if (GDBSERV_NO_ERROR != LOW_WriteMemory(ulDataBufferAddress, ulDataBufferCount, pucDataBuffer))
            {
            SetupErrorMessage(ERR_MEMORY_WRITE_FAILURE, pszInputFileName, NULL);
            return ERR_MEMORY_WRITE_FAILURE;                
            }
         ulDataBufferAddress = ulLocalAddress;
         ulDataBufferCount = ulLocalCount;
         memcpy(pucDataBuffer, pucLocalData, ulLocalCount);
         return iError;
         }
      }
   // last possible case is to copy the data into the buffer...
   memcpy(&pucDataBuffer[ulDataBufferCount], pucLocalData, ulLocalCount);
   if (ulDataBufferCount == 0)
      ulDataBufferAddress = ulLocalAddress;
   ulDataBufferCount += ulLocalCount;
   return 0;
}

/****************************************************************************
     Function: GetSrecDataFromFileRecord
     Engineer: Vitezslav Hola
        Input: unsigned char *pszFileBuffer - pointer to input filename containing SREC records
               unsigned char *pszDataBuffer - pointer to buffer containing data extracted from SREC record
               int32_t *piDataPresent - pointer indicating if valid data was extracted (1) or not (0)
               uint32_t *pulDataAddr - pointer to data address extracted from SREC record
               uint32_t *pulDataCount - pointer to length of data extracted from SREC record               
       Output: int32_t - error code (0 success)
  Description: Extracts an SREC record from the file and puts data and address into buffers.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
18-Aug-2008    NCH         Fixed GetWordFromChars() call bug
****************************************************************************/
static int32_t GetSrecDataFromFileRecord(char *pszFileBuffer, char *pszDataBuffer, int32_t *piDataPresent, uint32_t *pulDataAddr, 
                                     uint32_t *pulDataCount)
{
   uint32_t ulIndex;
   int32_t iError = 0;
   unsigned char ucRecordType = 0;
   unsigned char ucCheckSum = 0;
   // check parameters
   assert(pszFileBuffer != NULL);
   assert(pszDataBuffer != NULL);
   assert(piDataPresent != NULL);
   assert(pulDataAddr   != NULL);
   assert(pulDataCount  != NULL);
   *piDataPresent = 0;
   // check to make sure this isn't empty...
   if ((strlen(pszFileBuffer) == 0) || (pszFileBuffer[0] == 0xD) || (pszFileBuffer[0] == 0xA))
      return 0;
   if (pszFileBuffer[0] != 'S')
      {
      SetupErrorMessage(ERR_INVALID_SREC_RECORD, pszFileBuffer, NULL);
      return ERR_INVALID_SREC_RECORD;
      }
   ucRecordType = HexToChar((unsigned char)pszFileBuffer[1]);
   switch (ucRecordType)
      {
      case 1:
         // get count and address
         *pulDataCount = GetWordFromChars('0', '0', '0', '0', '0', '0', pszFileBuffer[2], pszFileBuffer[3]);
         *pulDataAddr = GetWordFromChars('0', '0', '0', '0', pszFileBuffer[4], pszFileBuffer[5], pszFileBuffer[6], pszFileBuffer[7]);
         // now calculate checksum
         for (ulIndex = 0; ulIndex < (*pulDataCount - 2); ulIndex++)
            ucCheckSum += GetByteFromChars(pszFileBuffer[8 + ulIndex*2], pszFileBuffer[9 + ulIndex*2]);
         // ... with the count
         ucCheckSum += (unsigned char)*pulDataCount;
         // ... and finally with the address
         ucCheckSum += (unsigned char)*pulDataAddr;
         ucCheckSum += (unsigned char)(*pulDataAddr >> 8);
         if (ucCheckSum != 0xFF)
            {  // invalid checksum
            iError = ERR_INVALID_SREC_RECORD;
            SetupErrorMessage(iError, pszFileBuffer, NULL);
            break;
            }
         // adjust count
         *pulDataCount -= 3;
         // extract the data bytes converting to binary
         for (ulIndex = 0; ulIndex < *pulDataCount; ulIndex++)
            pszDataBuffer[ulIndex] = (char)GetByteFromChars(pszFileBuffer[8 + ulIndex*2], pszFileBuffer[9 + ulIndex*2]);
         *piDataPresent = 1;
         break;
      case 2:
         // get count and address
         *pulDataCount = GetWordFromChars('0', '0', '0', '0', '0', '0', pszFileBuffer[2], pszFileBuffer[3]);
         *pulDataAddr = GetWordFromChars('0', '0', pszFileBuffer[4], pszFileBuffer[5], pszFileBuffer[6], pszFileBuffer[7], pszFileBuffer[8], pszFileBuffer[9]);
         // ... data+checksum first
         for (ulIndex = 0; ulIndex < (*pulDataCount - 3); ulIndex++)
            ucCheckSum += GetByteFromChars(pszFileBuffer[10 + ulIndex*2], pszFileBuffer[11 + ulIndex*2]);
         // ... with the count
         ucCheckSum += (unsigned char)*pulDataCount;
         // ... and finally with the address
         ucCheckSum += (unsigned char)*pulDataAddr;
         ucCheckSum += (unsigned char)(*pulDataAddr >> 8);
         ucCheckSum += (unsigned char)(*pulDataAddr >> 16);
         if (ucCheckSum != 0xFF)
            {
            iError = ERR_INVALID_SREC_RECORD;
            SetupErrorMessage(iError, pszFileBuffer, NULL);
            break;
            }
         // adjust the count to remove address & checksum
         *pulDataCount -= 4;
         // extract the data bytes converting to binary
         for (ulIndex = 0; ulIndex < *pulDataCount; ulIndex++)
            pszDataBuffer[ulIndex] = (char)GetByteFromChars(pszFileBuffer[10 + ulIndex*2], pszFileBuffer[11 + ulIndex*2]);
         *piDataPresent = 1;
         break;
      case 3:
         // get count and address
         *pulDataCount = GetWordFromChars('0', '0', '0', '0', '0', '0', pszFileBuffer[2], pszFileBuffer[3]);
         *pulDataAddr = GetWordFromChars(pszFileBuffer[4], pszFileBuffer[5], pszFileBuffer[6], pszFileBuffer[7], 
                                         pszFileBuffer[8], pszFileBuffer[9], pszFileBuffer[10], pszFileBuffer[11]);
         // ... data+checksum first
         for (ulIndex = 0; ulIndex < (*pulDataCount - 4); ulIndex++)
            ucCheckSum += GetByteFromChars(pszFileBuffer[12 + ulIndex*2], pszFileBuffer[13 + ulIndex*2]);
         // ... with the count
         ucCheckSum += (unsigned char)*pulDataCount;
         // ... and finally with the address
         ucCheckSum += (unsigned char)*pulDataAddr;
         ucCheckSum += (unsigned char)(*pulDataAddr >> 8);
         ucCheckSum += (unsigned char)(*pulDataAddr >> 16);
         ucCheckSum += (unsigned char)(*pulDataAddr >> 24);
         if (ucCheckSum != 0xFF)
            {
            iError = ERR_INVALID_SREC_RECORD;
            SetupErrorMessage(iError, pszFileBuffer, NULL);
            break;
            }
         // adjust the count to remove address & checksum
         *pulDataCount -= 5;
         // extract the data bytes converting to binary
         for (ulIndex = 0; ulIndex < *pulDataCount; ulIndex++)
            pszDataBuffer[ulIndex] = (char)GetByteFromChars(pszFileBuffer[12 + ulIndex*2], pszFileBuffer[13 + ulIndex*2]);
         *piDataPresent = 1;
         break;
      case 0:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
         // ignoring these types of records
         break;
      default:           
          iError = ERR_INVALID_SREC_RECORD;
          SetupErrorMessage(iError, pszFileBuffer, NULL);
         break;
      }
   return iError;
}

/****************************************************************************
     Function: HexToChar
     Engineer: Vitezslav Hola
        Input: unsigned char ucHexVal - ASCII character to covert
       Output: unsigned char - coverted value
  Description: Convert hexadecimal ASCII to binary
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
unsigned char HexToChar(unsigned char ucHexVal)
{
   if ((ucHexVal >= '0') && (ucHexVal <= '9'))
      return (unsigned char)(ucHexVal - '0');
   else if ((ucHexVal >= 'A') && (ucHexVal <= 'F'))
      return (unsigned char)(ucHexVal - 'A' + 10);
   else
      return (unsigned char)(ucHexVal - 'a' + 10);
}

/****************************************************************************
     Function: SetupErrorMessage
     Engineer: Daniel Madden
        Input: int32_t iError - specifies error number for download
               char *pszFileName - pointer to download file name
               char *pszErrorText - pointer to additional error text asssociated with error.
       Output: none
  Description: Prints out appropiate error message to GDB server console
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static void SetupErrorMessage(int32_t iError, char *pszFileName, char *pszErrorText)
{
   switch (iError)
      {     
      case ERR_CANNOT_FIND_FILE:
         PrintMessage(ERROR_MESSAGE, "Cannot find or open %s.", pszFileName);
         break;
      case ERR_MEMORY_READ_FAILURE:
         PrintMessage(ERROR_MESSAGE, "Download verification of %s failed due to memory read failure.", pszFileName);
         break;
      case ERR_MEMORY_VERIFY_FAILURE:
         if(pszErrorText != NULL)
            PrintMessage(ERROR_MESSAGE, "%s", pszErrorText);
         else
            PrintMessage(ERROR_MESSAGE, "Verification failed.");
         break;
      case ERR_MEMORY_WRITE_FAILURE:
         PrintMessage(ERROR_MESSAGE, "Download of %s failed due to memory write failure.", pszFileName);
         break;
      case ERR_INVALID_SREC_RECORD:
         PrintMessage(ERROR_MESSAGE, "Download of %s failed due to invalid srec record.", pszFileName);
         break;         
      default:
         PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
         break;
      }
}

/****************************************************************************
     Function: GetWordFromChars
     Engineer: Vitezslav Hola
        Input: char cChar1 - 1st ASCII character
               ...
               char cChar8 - last ASCII character
       Output: uint32_t - coverted value
  Description: Convert 8 ASCII characters to 32-bit value (1st character is most significant, etc.)
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static uint32_t GetWordFromChars(char cChar1, char cChar2, char cChar3, char cChar4, char cChar5, char cChar6, char cChar7, char cChar8)
{
   uint32_t ulValue = 0;
   ulValue = ((uint32_t)HexToChar((unsigned char)cChar1)) & 0x0000000F;
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar2)) & 0x0000000F);
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar3)) & 0x0000000F);
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar4)) & 0x0000000F);
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar5)) & 0x0000000F);
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar6)) & 0x0000000F);
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar7)) & 0x0000000F);
   ulValue = (ulValue << 4) | (((uint32_t)HexToChar((unsigned char)cChar8)) & 0x0000000F);
   return ulValue;
}

/****************************************************************************
     Function: GetByteFromChars
     Engineer: Vitezslav Hola
        Input: char cChar1 - 1st ASCII character
               char cChar2 - 2nd ASCII character
       Output: uint32_t - coverted value
  Description: Convert 2 ASCII characters to byte value (1st character is most significant, etc.)
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
****************************************************************************/
static unsigned char GetByteFromChars(char cChar1, char cChar2)
{
   char cValue1, cValue2;
   // convert 1st character
   if ((cChar1 >= '0') && (cChar1 <= '9'))
      cValue1 = cChar1 - '0';
   else if ((cChar1 >= 'A') && (cChar1 <= 'F'))
      cValue1 = (cChar1 - 'A') + 10;
   else
      cValue1 = (cChar1 - 'a') + 10;
   // convert 2nd character
   if ((cChar2 >= '0') && (cChar2 <= '9'))
      cValue2 = cChar2 - '0';
   else if ((cChar2 >= 'A') && (cChar2 <= 'F'))
      cValue2 = (cChar2 - 'A') + 10;
   else
      cValue2 = (cChar2 - 'a') + 10;
   // combine result
   return (((unsigned char)cValue1 & 0x0F) << 4) | ((unsigned char)cValue2 & 0x0F);
}
