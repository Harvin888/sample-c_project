/****************************************************************************
       Module: gdbutil.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, utility functions header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS/VK       Modified for ARM support
17-Oct-2009    SVL         Added functions for parsing and validating arguments
****************************************************************************/
#ifndef GDBUTIL_H_
#define GDBUTIL_H_


#ifndef __LINUX
#ifndef strcasecmp
#define strcasecmp   _stricmp
#endif
#endif

typedef enum
{
   INFO_MESSAGE      = 0,
   ERROR_MESSAGE     = 1,
   HELP_MESSAGE      = 2,
   DEBUG_MESSAGE     = 3,
   LOG_MESSAGE       = 4
} TyMessageType;

#ifdef ARM
    #define ERR_UNSUPPORTED_CORPOC 2
    #define ERR_UNSUPPORTED_REG 3
	#define ERR_INVALID_ARG             1
	#define ERR_INVALID_CRN		        2
	#define ERR_INVALID_CRM		        3
	#define ERR_INVALID_OP1		        4
	#define ERR_INVALID_OP2		        5

	#define MAX_CRN 15
	#define MAX_CRM 15
	#define MAX_OP1 7
	#define MAX_OP2 7
#endif
void MsSleep(uint32_t uiMiliseconds);
int32_t KeyHit(void);
int32_t GetchWithoutEcho(void);
void StringToUpr(char *pszString);
void StringToLwr(char *pszString);
uint32_t SwapUlongBytes(uint32_t ulData);
unsigned short SwapUshortBytes(unsigned short usData);
int32_t ToHex(int32_t iNibble);
int32_t FromHex(int32_t iChar);
uint32_t ExStrToUlong(char * pszValue, int32_t * pbValid);
uint32_t StrToUlong(char * pszValue, int32_t * pbValid);
int32_t IsSpace(int32_t iChar);
int32_t IsHexChar(int32_t iChar);

void LogMessage(const char * pszSendRecv, const char * pszData);
void PrintMessage(TyMessageType tyMessageType, const char * pszToFormat, ...);
void LogTitleAndVersion(void);
void ToggleDebugModeFlag(void);
int32_t CheckNumberIsPresent(char *pszInput, uint32_t *pulNumber);
void StopSignalToString(TyStopSignal tyStopSignal, char *pszOutput);
void StringsToBpTypeAddressAndLength(char *pszInput, uint32_t *pulBpType, uint32_t *pulAddress, uint32_t *pulLength);
void StringsToAddressCountAndData(char *pszInput, uint32_t *pulAddress, uint32_t *pulCount, unsigned char *pucData, uint32_t  ulDataSize);
void MemoryValuesToStrings(unsigned char *pucData, char *pszOutput, uint32_t ulCount);
void StringsToAddressAndCount(char *pszInput, uint32_t *pulAddress, uint32_t *pulCount);
void StringsToRegisterAndValue(char *pszInput, uint32_t *pulRegisterNumber, uint32_t *pulRegisterValue);
void StringsToRegisters(char *pszInput, uint32_t *pulRegisters, uint32_t ulCount);
void RegistersToStrings(uint32_t *pulRegisters, char *pszOutput, uint32_t ulCount);
void StringToUChar(char *pszString, unsigned char *pucValue);
void UCharToString(unsigned char ucValue, char *pszString);
void StringToUlong(char *pszString, uint32_t *pulValue);
void UlongToString(uint32_t ulValue, char *pszString);
void StringsToAddressCountAndBinaryData(char *pszInput, uint32_t *pulAddress, uint32_t *pulCount, unsigned char *pucData, uint32_t ulDataSize);

TyMemTypes StringToMemType(char *pszMemType);

uint32_t ExtractRegBits(uint32_t ulRegValue, TyRegDef *ptyRegDef);
uint32_t MergeRegBits(uint32_t ulRegValue, uint32_t ulBitValue, TyRegDef *ptyRegDef);
TyRegDef * GetRegDefForName(const char *pszRegName);
TyRegDef * GetRegDefForIndex(int32_t iIndex);
#if 0000
int32_t ReadRegisterByName(char *pszRegName, uint32_t *pulRegValue);
int32_t WriteRegisterByName(char *pszRegName, uint32_t ulRegValue);
#endif
const char *GetRegDefDescription(TyRegDef *ptyRegDef);
void AddStringToOutAsciiz(const char *szBuffer, char *pcOutput);
int32_t GetArgs(char *pcInput, char ppszArgList[][_MAX_PATH]);

typedef enum _BASE
{
    BINARY = 0,
    OCTAL,
    DEC,
    HEX
}TyBase;
int32_t ValidateStr(char *pszStr, TyBase base, uint32_t uiExpLen);
#if defined ARM

void StringsToWpModeSizeAddressandData  (char          *pszInput,
                                         char         *pszWpMode,
                                         char         *pszSize,
                                         uint32_t *pulAddress,
                                         uint32_t *pulData);

void StringsToWpModeSizeAndAddressorData (char          *pszInput,
                                          char          *pszWpMode,
                                          char          *pszSize,
                                          uint32_t *pulvalue);

void StringsToTopOfMemandVectorTrapAddr (char          *pszInput,
                                         uint32_t *pulTop,
                                         uint32_t *pulvector);

int32_t StringsToProcessCP15Params          (char *pszInput, uint32_t *puiCRn,
                                         uint32_t *puiCRm, uint32_t *puiOp1,
                                         uint32_t *puiOp2, uint32_t *pulData);

int32_t StringsToReadCPRegParams           (char *pszInput,
										 uint32_t *pulCPNumber,
										 uint32_t *pulRegIndex);

int32_t StringsToWriteCPRegParams          (char *pszInput,
										 uint32_t *pulCPNumber,
                                         uint32_t *pulRegIndex,
										 uint32_t *pulData);

#endif

#endif // GDBUTIL_H_

