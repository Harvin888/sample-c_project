/****************************************************************************
       Module: gdboptmips.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, MIPS specific command line options
Date           Initials    Description
08-Feb-2008    VH          Initial
20-Jan-2008    RS          DoNotIssueHardReset while connection option added.
28-July-2011   ANGS        Added multi-core support for MIPS
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
//#include <windows.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbopt.h"

// default MIPS options
#define DEFAULT_MIPS_FREQUENCY_HZ    1000000                             // default frequency of 1MHz

// external variables
extern TyServerInfo tySI;

// local function prototypes
static int OptionRegisterSettingsFile              (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionCacheFlushRoutine                 (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionVerifyDownloadFile                (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDownloadFile                      (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionHaltCountersInDebugMode           (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionSingleStepUsingSoftwareBreakpoint (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDisableIntsDuringSingleStep       (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionEnableDmaMode                     (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionEndian                            (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
//static int OptionVerifyCacheFlushRoutine           (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionHelp                              (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionVersion                           (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionJtagConsoleMode                   (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDebugStdout                       (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDebugFile                         (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionCommandFile                       (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionGdbPort                           (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionProbe                             (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionProbeInstance                     (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionJtagFrequency                     (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDevice                            (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionProgramEntryPoint                 (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
//static int OptionTapNumber                         (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionDoNotIssueHardReset			   (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionMulticore         			       (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static int OptionSelectedcore         			   (unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
static void HelpJtagFrequency(void);
static void HelpDevice(void);
static void HelpRegisterSettingsFile(void);
static char *TranslateName(char *pszDst, char *pszSrc, unsigned int uiSize);
//extern void CFG_ScanChainNode(TyXMLNode *ptyThisNode);
static int OptionPFXDMode(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
extern void CFG_ScanChainNode(TyXMLNode *ptyThisNode);
// command line options (must be global variable)
TyOption tyaOptions[] =
{
   {
   "help",
   "",
   "Display usage information.",
   "",
   "",
   "",
   OptionHelp,
   NULL,
   FALSE,
   1
   },
   {
   "version",
   "",
   "Display program version.",
   "",
   "",
   "",
   OptionVersion,
   NULL,
   FALSE,
   1
   },
   {
   "debug-stdout",
   "",
   "Display debug messages to standard output.",
   "",
   "",
   "",
   OptionDebugStdout,
   NULL,
   FALSE,
   1
   },
   {
   "debug-file",
   " <file>",
   "Log debug messages to <file>.",
   "",
   "",
   "",
   OptionDebugFile,
   NULL,
   FALSE,
   1
   },
   {
   "command-file",
   " <file>",
   "Read command line options from <file>.",
   "",
   "",
   "",
   OptionCommandFile,
   NULL,
   FALSE,
   1
   },
   {
   "gdb-port",
   " <port>",
   "Listen on <port> for remote connections",
   "from GDB. Default is port 2331.",
   "",
   "",
   OptionGdbPort,
   NULL,
   FALSE,
   1
   },
   {
   "probe",
   " <name>",
   "Connect with <name> probe.",
   "<name> is: Opella-XD (default) or Opella.",
   "",
   "",
   OptionProbe,
   NULL,
   FALSE,
   1
   },
   {
   "instance",
   " <number>",
   "Select Opella-XD from multiple Opella-XD(s)",
   "connected.",
   "<number> is Opella-XD serial number.",
   "",
   OptionProbeInstance,
   NULL,
   FALSE,
   1
   },
   {
   "jtag-frequency",
   " <freq>",
   "Specifies JTAG Frequency.",
   "",
   "",
   "",
   OptionJtagFrequency,
   HelpJtagFrequency,
   FALSE,
   1
   },
   {
   "device",
   " <dev>",
   "Specifies target device. Device information",
   "is extracted from FAMLYMIP.XML.",
   "<dev> is:",
   "",
   OptionDevice,
   HelpDevice,
   FALSE,
   1
   },
   {
   "program-entry-point",
   " <addr>",
   "Set the program entry point to <addr>.",
   "",
   "",
   "",
   OptionProgramEntryPoint,
   NULL,
   FALSE,
   1
   },
   {
   "reg-settings-file",
   " <file>",
   "Read and apply register settings from <file>.",
   "",
   "",
   "",
   OptionRegisterSettingsFile,
   HelpRegisterSettingsFile,
   FALSE,
   1
   },
   {
   "cache-flush-routine",
   " <addr>",
   "Enable debugging of cached code and set the",
   "cache invalidation routine to <addr>.",
   "Requires 0x200 bytes at <addr>.",
   "",
   OptionCacheFlushRoutine,
   NULL,
   FALSE,
   0
   },
   {
   "verify-cache-flush-routine",
   "",
   "Verify the cache invalidation routine before",
   "execution.",
   "",
   "",
   NULL,//OptionVerifyCacheFlushRoutine,
   NULL,
   FALSE,
   0
   },
   {
   "endian",
   " <endianess>",
   "Set initial target byte order to <endianess>.",
   "<endianess> is: default, little or big.",
   "",
   "",
   OptionEndian,
   NULL,
   FALSE,
   1
   },
   {
   "enable-dma-mode",
   "",
   "Enables DMA mode on the target (if available).",
   "",
   "",
   "",
   OptionEnableDmaMode,
   NULL,
   FALSE,
   1
   },
   /*{
   "tap-number",
   " <number>",
   "Specifies the JTAG Test Access Point <number>",
   "for Multi-core targets.",
   "",
   "",
   OptionTapNumber,
   NULL,
   FALSE,
   1
   },*/
   {
   "disable-ints-for-singlestep",
   "",
   "Disable interrupts during single step.",
   "",
   "",
   "",
   OptionDisableIntsDuringSingleStep,
   NULL,
   FALSE,
   1
   },
   {
   "singlestep-using-sw-bp",
   "",
   "Single step using software breakpoint.",
   "",
   "",
   "",
   OptionSingleStepUsingSoftwareBreakpoint,
   NULL,
   FALSE,
   1
   },
   {
   "halt-counters-in-debug-mode",
   "",
   "Halt counters in debug mode.",
   "",
   "",
   "",
   OptionHaltCountersInDebugMode,
   NULL,
   FALSE,
   1
   },
   {
   "jtag-console-mode",
   "",
   "Open JTAG low-level console mode.",
   "",
   "",
   "",
   OptionJtagConsoleMode,
   NULL,
   FALSE,
   1
   },
   {
   "ld-exe",
   " <file> <type> <addr>",
   "Load <file> of specified <type> and execute",
   "from <addr>. <type> is: srec.",
   "",
   "",
   OptionDownloadFile,
   NULL,
   FALSE,
   1
   },
   {
   "verify-download",
   "",
   "Verifies file downloaded to memory(default off).",
   "",
   "",
   "",
   OptionVerifyDownloadFile,
   NULL,
   FALSE,
   1
   },
   {
   "no-reset-on-connection",
   "",
   "Do not reset target before connection.",
   "",
   "",
   "",
   OptionDoNotIssueHardReset,
   NULL,
   FALSE,
   1
   },
   /*PFXD mode*/
   {
   "pfxd-mode",
   "",
   "Start in PFXD mode which defers connecting to",
   "target until monitor connect command is received",
   "",
   "",
   OptionPFXDMode,
   NULL,
   FALSE,
   1
   },
   {
   "scan-file",
   "",
   "Calling GDBServer using mixedCoreFile 'mixedcores_mips.xml'",
   "which is created based on the updated multi-core details",
   "",
   "",
   OptionMulticore,
   NULL,
   FALSE,
   1
   },
   {
   "tap-number",
   "",
   "Telling the core, which is selected in MIPS multi-core selection, to GDBServer",
   "",
   "",
   "",
   OptionSelectedcore,
   NULL,
   FALSE,
   1
   }
};

#define MAX_OPTIONS ((int)(sizeof(tyaOptions) / sizeof(TyOption)))

int iGdb_options_number = MAX_OPTIONS;

// local variables

/****************************************************************************
     Function: OptionVerifyDownloadFile
     Engineer: Daniel Madden
        Input: *piArg    : pointer to index of next arg
               iArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: Fills in flag, bVerifyDownloadFile to show --verify-download was parsed
Date           Initials    Description
10-Mar-2006    DM          Initial
*****************************************************************************/
static int OptionVerifyDownloadFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.bVerifyDownloadFile = TRUE;
   return NO_ERROR;
}

/****************************************************************************
     Function: OptionDownloadFile
     Engineer: Daniel Madden
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: Pareses opton of --ld-exe <file> <type> <addr>
Date           Initials    Description
10-Mar-2006    DM          Initial
*****************************************************************************/
static int OptionDownloadFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
    unsigned long ulDownloadEntryAddr = 0;
    int bValid=FALSE;

    if ((*puiArg)+1 >= uiArgCount)
       {
       sprintf(tySI.szError, "Missing <file>, <type> and <addr> after %s option.", ppszArgs[*puiArg]);
       return GDBSERV_OPT_ERROR;
       }
    (*puiArg)++;

    strncpy(tySI.szDownloadFile, ppszArgs[*puiArg], sizeof(tySI.szDownloadFile)-1);
    tySI.szDownloadFile[sizeof(tySI.szDownloadFile)-1] = '\0';

    if ((*puiArg)+1 >= uiArgCount)
       {
       sprintf(tySI.szError, "Missing <type> and <addr> after %s option.", ppszArgs[*puiArg]);
       return GDBSERV_OPT_ERROR;
       }
    (*puiArg)++;

    if (strcasecmp(ppszArgs[*puiArg], "srec") == 0)
       tySI.bELFType = 0;
    else if (strcasecmp(ppszArgs[*puiArg], "elf") == 0)
       tySI.bELFType = 1;
    else
       {
       sprintf(tySI.szError, "Invalid <type> name after %s option.", ppszArgs[*puiArg]);
       return GDBSERV_OPT_ERROR;
       }

    if ((*puiArg)+1 >= uiArgCount)
        {
        sprintf(tySI.szError, "Missing <addr> after %s option.", ppszArgs[*puiArg]);
        return GDBSERV_OPT_ERROR;
        }
    (*puiArg)++;

    ulDownloadEntryAddr = StrToUlong(ppszArgs[*puiArg], &bValid);

    if (!bValid)
        {
        sprintf(tySI.szError, "Invalid <addr> after %s option", ppszArgs[*puiArg]);
        return GDBSERV_OPT_ERROR;
        }

    tySI.ulDownloadAddr = ulDownloadEntryAddr;


    tySI.bDownloadFile = TRUE;
    return NO_ERROR;
}

/****************************************************************************
     Function: OptionHaltCountersInDebugMode
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
static int OptionHaltCountersInDebugMode(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.tyProcConfig.Processor._MIPS.bUserHaltCountersIDM = TRUE;
   PrintMessage(INFO_MESSAGE, "Counters will be halted in debug mode.");
   return NO_ERROR;
}

/****************************************************************************
     Function: OptionSingleStepUsingSoftwareBreakpoint
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
static int OptionSingleStepUsingSoftwareBreakpoint(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.tyProcConfig.Processor._MIPS.bSingleStepUsingSWBP = TRUE;
   PrintMessage(INFO_MESSAGE, "Single stepping will use software breakpoints.");
   return NO_ERROR;
}

/****************************************************************************
     Function: OptionDisableIntsDuringSingleStep
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
static int OptionDisableIntsDuringSingleStep(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.tyProcConfig.Processor._MIPS.bDisableIntSS = TRUE;
   PrintMessage(INFO_MESSAGE, "Interrupts are disabled for single stepping.");
   return NO_ERROR;
}

/****************************************************************************
     Function: OptionEnableDmaMode
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
static int OptionEnableDmaMode(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.tyProcConfig.Processor._MIPS.bDMATurnedOn = TRUE;
   PrintMessage(INFO_MESSAGE, "DMA mode is enabled.");
   return NO_ERROR;
}

/****************************************************************************
     Function: OptionEndian
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
static int OptionEndian(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing endianness after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   if (strcasecmp(ppszArgs[*puiArg], "default") == 0)
      tySI.tyProcConfig.tyInitialEndianMode = DefaultEndianMode;
   else if (strcasecmp(ppszArgs[*puiArg], "little") == 0)
      tySI.tyProcConfig.tyInitialEndianMode = LittleEndianMode;
   else if (strcasecmp(ppszArgs[*puiArg], "big") == 0)
      tySI.tyProcConfig.tyInitialEndianMode = BigEndianMode;
   else
      {
      sprintf(tySI.szError, "Invalid endianness '%s'.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   return NO_ERROR;
}

/****************************************************************************
     Function: OptionVerifyCacheFlushRoutine
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
/*static int OptionVerifyCacheFlushRoutine(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.tyProcConfig.Processor._MIPS.bCacheFlushVerify = TRUE;
   PrintMessage(INFO_MESSAGE, "Cache flush routine will be verified.");
   return NO_ERROR;
}*/

/****************************************************************************
     Function: OptionCacheFlushRoutine
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
08-AUG-2009    DK          Modifed for cache invalidation
*****************************************************************************/
static int OptionCacheFlushRoutine(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
    const char *pMessage= "Warning:--cache-flush-routine and --verify-flush-routine is deprecated.Cache flushing now handled automatically.\n";
    NOREF( uiArgCount );
    NOREF( ppszArgs );
    (*puiArg)++;
    PrintMessage( HELP_MESSAGE , pMessage );
    return NO_ERROR;
}

/****************************************************************************
     Function: OptionRegisterSettingsFile
     Engineer: Patrick Shiels
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: 
Date           Initials    Description
16-Dec-2005    PJS         Initial
21-Mar-2010    DK          Added delay
*****************************************************************************/
static int OptionRegisterSettingsFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   FILE    *pFile;
   int      iLineNumber = 0;
   int      iScannedItems;
   unsigned long    ulArg1;
   unsigned long    ulArg2;
   unsigned long    ulArg3;
   char     szVariableName[_MAX_PATH];
   char     szLine[_MAX_PATH];
   

   tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount = 0;

   if ((*puiArg)+1 >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing filename after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;

   strncpy(tySI.szRegisterFile, ppszArgs[*puiArg], sizeof(tySI.szRegisterFile)-1);
   tySI.szRegisterFile[sizeof(tySI.szRegisterFile)-1] = '\0';


   // open in text mode so that CR+LF is converted to LF
   pFile = fopen(tySI.szRegisterFile, "rt");

   if (pFile == NULL)
      {
      sprintf(tySI.szError, "Could not open register settings file '%s'.", tySI.szRegisterFile);
      return GDBSERV_OPT_ERROR;
      }

   PrintMessage(INFO_MESSAGE, "Processing register settings file '%s'...", tySI.szRegisterFile);

   while (!feof(pFile))
      {
      if (fgets(szLine, sizeof(szLine), pFile) == NULL)
         break;

      iLineNumber++;

      szVariableName[0] = '\0';
      ulArg1 = ulArg2 = ulArg3 = 0;

      iScannedItems = sscanf(szLine, " %s %lx %lx %lx ", szVariableName,
                                                      &ulArg1,
                                                      &ulArg2,
                                                      &ulArg3);
      // if an empty line, continue with next line...
      if (iScannedItems <= 0)
         continue;

      // if a comment, continue with next line...
      if ((szVariableName[0] == '\0') ||
          (szVariableName[0] == '#' ) ||
          (szVariableName[0] == ';' ) ||
          ((szVariableName[0] == '/') && (szVariableName[1] == '/')))
         continue;

      if (iScannedItems == 2)
         {
         // Format: Name Value.
         // E.g.  : CMEM0 0xF
         if( strcasecmp( szVariableName , "sleep" ) )
            {
            MsSleep( ulArg1 ) ;
            }
         strcpy(tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].szVariableName, szVariableName);
         tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].ulVariableSize    = 0;
         tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].ulVariableAddress = 0;
         tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].ulVariableValue   = ulArg1;
         tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount++;

         //PrintMessage(INFO_MESSAGE, "Writing 0x%lX to %s.",
         //               ulArg1, szVariableName);
         }
      else if (iScannedItems == 4)
         {
         // Format: Name Size Addr Value.
         // E.g.  : R0 0x00000004 0xb800380c 0x18000000

         if ((ulArg1 != 1) && (ulArg1 != 2) && (ulArg1 != 4))
            {
            sprintf(tySI.szError, "Invalid register size at line %d of register settings\nfile '%s', only sizes 1, 2 or 4 allowed:\n%s", iLineNumber, tySI.szRegisterFile, szLine);
            return GDBSERV_OPT_ERROR;
            }

         strcpy(tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].szVariableName, szVariableName);
         tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].ulVariableSize    = ulArg1;
         tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].ulVariableAddress = ulArg2;
         tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount].ulVariableValue   = ulArg3;
         tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount++;

         //PrintMessage(INFO_MESSAGE, "Writing 0x%lX to %s at 0x%08lX of %d bytes.",
         //               ulArg3, szVariableName, ulArg2, ulArg1);
         }
      else
         {
         sprintf(tySI.szError, "Invalid format at line %d of register settings file '%s':\n%s", iLineNumber, tySI.szRegisterFile, szLine);
         return GDBSERV_OPT_ERROR;
         }
      }

   if (pFile != NULL)
      (void)fclose(pFile);

   return NO_ERROR;
}

/****************************************************************************
     Function: OptionPFXDMode
     Engineer: Nikolay Chokoev
        Input: none
       Output: char * - emulator connection name
  Description: return name of selected emulator connection
Date           Initials    Description
24-Jul-2008    NCH         Initial & cleanup
*****************************************************************************/
static int OptionPFXDMode(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.bPfxdMode = TRUE; 
   tySI.bIsConnected = FALSE;
   return 0;
}

/****************************************************************************
     Function: OptionHelp
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option help.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionHelp(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bHelp = 1;
   return 0;
}

/****************************************************************************
     Function: OptionVersion
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option version.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionVersion(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bVersion = 1;
   return 0;
}

/****************************************************************************
     Function: OptionJtagConsoleMode
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option jtag console mode.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionJtagConsoleMode(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bJtagConsoleMode = 1;
   return 0;
}

/****************************************************************************
     Function: OptionDebugStdout
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for option debug output.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionDebugStdout(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);
   tySI.bDebugStdout = 1;
   tySI.bDebugOutput = 1;
   PrintMessage(INFO_MESSAGE, "Displaying debug messages to standard output.");
   return 0;
}

/****************************************************************************
     Function: OptionDebugFile
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Action for debug file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionDebugFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing filename after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(tySI.szDebugFile, ppszArgs[*puiArg], sizeof(tySI.szDebugFile)-1);
   tySI.szDebugFile[sizeof(tySI.szDebugFile)-1] = '\0';
   tySI.pDebugFile = fopen(tySI.szDebugFile, "w");
   if (tySI.pDebugFile == NULL)
      {
      sprintf(tySI.szError, "Could not open debug file '%s'.", tySI.szDebugFile);
      return GDBSERV_OPT_ERROR;
      }
   tySI.bDebugOutput = 1;
   // put title and version in log file
   LogTitleAndVersion();
   PrintMessage(INFO_MESSAGE, "Logging debug messages to file '%s'.", tySI.szDebugFile);
   return 0;
}

/****************************************************************************
     Function: OptionCommandFile
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option command file.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionCommandFile(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   int iError;
   if (((*puiArg) + 1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing filename after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(tySI.szCommandFile, ppszArgs[*puiArg], sizeof(tySI.szCommandFile)-1);
   tySI.szCommandFile[sizeof(tySI.szCommandFile)-1] = '\0';
   PrintMessage(INFO_MESSAGE, "Reading command line options from file '%s'.", tySI.szCommandFile);
   iError = ProcessCmdFileArgs(tySI.szCommandFile, 0);
   return iError;
}

/****************************************************************************
     Function: OptionGdbPort
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option GDB port.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionGdbPort(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   unsigned long ulPort = 0;
   int  bValid = 0;
   if (((*puiArg) + 1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing port number after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   ulPort = StrToUlong(ppszArgs[*puiArg], &bValid);
   if (!bValid || (ulPort > 0xFFFF))
      {
      sprintf(tySI.szError, "Invalid port number '%s'.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   tySI.usGdbPort = (unsigned short)ulPort;
   return 0;
}

/****************************************************************************
     Function: OptionProbe
     Engineer: Suresh P.C
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Select Probe as Opella-USB or Opella-XD
Date           Initials    Description
27-Nov-2008    SPC         Initial
*****************************************************************************/
static int OptionProbe(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
	if (((*puiArg)+1) >= uiArgCount)
	   {
	   sprintf(tySI.szError, "Missing Opella-XD serial number after %s option.", ppszArgs[*puiArg]);
	   return GDBSERV_OPT_ERROR;
	   }
	(*puiArg)++;

   // only Opella and Opella-XD are supported at the moment
   if (strcasecmp(ppszArgs[*puiArg], "opella") == 0)
      tySI.tyGdbServerTarget = GDBSRVTGT_OPELLA;
   else if (strcasecmp(ppszArgs[*puiArg], "opella-xd") == 0)
      tySI.tyGdbServerTarget = GDBSRVTGT_OPELLAXD;
   else
      {
	  tySI.tyGdbServerTarget = GDBSRVTGT_INVALID;
      sprintf(tySI.szError, "Invalid emulator name '%s'.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   return 0;
}

/****************************************************************************
     Function: OptionProbeInstance
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Selecting probe instance number if necessary
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionProbeInstance(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing Opella-XD serial number after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   memset((void *)tySI.szProbeInstance, 0, PROBE_STRING_LEN * sizeof(char));
   if (strlen(ppszArgs[*puiArg]) < PROBE_STRING_LEN)
      {  // string length is less than buffer size
      strcpy(tySI.szProbeInstance, ppszArgs[*puiArg]);
      tySI.szProbeInstance[PROBE_STRING_LEN-1] = '\0';
      }
   else
      {
      sprintf(tySI.szError, "Invalid Opella-XD serial number format.");
      return GDBSERV_OPT_ERROR;
      }
   return 0;
}

/****************************************************************************
     Function: OptionJtagFrequency
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option jtag frequency.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionJtagFrequency(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   char szFrequency[64];
   int iStrLen;
   int bValid = 0;
   unsigned long ulFrequency = 1;
   unsigned long ulMultiplier = FREQ_MHZ(1);
   // check options
   if (((*puiArg) + 1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing JTAG frequency after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(szFrequency, ppszArgs[*puiArg], sizeof(szFrequency)-1);
   szFrequency[sizeof(szFrequency)-1] = '\0';
   // check if requesting RTCK

   tySI.ulJtagFreqHz = DEFAULT_MIPS_FREQUENCY_HZ;
   strcpy(tySI.szJtagFreq, "");
   iStrLen = (int)strlen(szFrequency);
   if (iStrLen > 3)
      {
      char *pszUnit = szFrequency + (iStrLen - 3);
      if (strcasecmp(pszUnit, "khz") == 0)
         {
         pszUnit[0]   = '\0';
         ulMultiplier = FREQ_KHZ(1);
         }
      else if (strcasecmp(pszUnit, "mhz") == 0)
         {
         pszUnit[0]   = '\0';
         ulMultiplier = FREQ_MHZ(1);
         }
      else
         return GDBSERV_OPT_ERROR;
      }

   ulFrequency = StrToUlong(szFrequency, &bValid);
   if (!bValid)
      {
      sprintf(tySI.szError, "Invalid JTAG frequency '%s'.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   tySI.ulJtagFreqHz = ulFrequency * ulMultiplier;

   return 0;
}

/****************************************************************************
     Function: OptionDevice
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option device.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionDevice(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   TyXMLNode *ptyNode;
   char *pszDevice;
   char szBuffer[_MAX_PATH];
   unsigned long ulIndex = 0;
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   tySI.ulProcessor = 0;
   tySI.ptyNodeProcessor = NULL;
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing device name after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   // check for a matching device...
   ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Processor");
   while (ptyNode != NULL)
      {
      if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
         {
         ulIndex++;
         (void)TranslateName(szBuffer, pszDevice, sizeof(szBuffer));
         if (strcasecmp(ppszArgs[*puiArg] ,szBuffer) == 0)
            {
            tySI.ulProcessor = ulIndex;
            tySI.ptyNodeProcessor = ptyNode;
            break;
            }
         }
      ptyNode = XML_GetNextNode(ptyNode);
      }
   if (tySI.ulProcessor == 0)
      {
      sprintf(tySI.szError, "Invalid device name '%s'.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   return 0;
}

/****************************************************************************
     Function: OptionProgramEntryPoint
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option entry point.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int OptionProgramEntryPoint(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   unsigned long ulEntryPoint = 0;
   int  bValid = 0;
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing entry point address after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   ulEntryPoint = StrToUlong(ppszArgs[*puiArg], &bValid);
   if (!bValid)
      {
      sprintf(tySI.szError, "Invalid entry point address '%s'.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   tySI.tyProcConfig.ulProgEntryPoint = ulEntryPoint;
   tySI.tyProcConfig.Processor._MIPS.isPEPset = 1;
   PrintMessage(INFO_MESSAGE, "Setting program entry point (PC) to 0x%08lX.", ulEntryPoint);
   return 0;
}

/****************************************************************************
     Function: OptionTapNumber
     Engineer: Vitezslav Hola
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option tap number.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************//*
static int OptionTapNumber(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   unsigned long ulTap  = 0;
   char szTAPNumbers[MAX_TAP_NUM*2];
   char szCurTAPNumber[MAX_TAP_NUM*2];
   int bValid = 0;
   unsigned int uiTapCnt = 0;
   unsigned int uiInd = 0;
   int iCnt;

   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing Test Access Point number after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(szTAPNumbers, ppszArgs[*puiArg], (MAX_TAP_NUM*2));
   //get TAP numbers from command line argument and 
   //store them as a strings array
   for (iCnt=0; iCnt <(MAX_TAP_NUM*2); iCnt++)
      {
      if ((szTAPNumbers[iCnt] != ',') && (szTAPNumbers[iCnt] != '\0'))  
         {  //get the last TAP
         szCurTAPNumber[uiInd++] = szTAPNumbers[iCnt]; 
         }
      else
         {
         szCurTAPNumber[uiInd] = '\0';
         ulTap = StrToUlong(szCurTAPNumber, &bValid);
         if (!bValid || (ulTap > MAX_TAP_NUM) || (uiTapCnt > (MAX_TAP_NUM-1)))
            {
            sprintf(tySI.szError, "Invalid Test Access Point number '%s'.", szCurTAPNumber);
            return GDBSERV_OPT_ERROR;
            }
         tySI.ulTAP[uiTapCnt++] = (unsigned char)ulTap;
         if(szTAPNumbers[iCnt] == '\0')   
            break;   //this was the last TAP in arguments
         uiInd = 0;
         }
      }
//TODO:   PrintMessage(INFO_MESSAGE, "TAP number is %d.", (unsigned int)ulTap);
   return 0;
}*/
/****************************************************************************
     Function: OptionDoNotIssueHardReset
     Engineer: Rejeesh S Babu
        Input: *puiArg    : pointer to index of next arg
               uiArgCount : count of args in the list
               **ppszArgs: list of args
       Output: none
  Description: Do not Issue Hard Reset On Connection.
Date           Initials    Description
16-Dec-2005    PJS         Initial
*****************************************************************************/
static int OptionDoNotIssueHardReset(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   NOREF(uiArgCount);
   NOREF(puiArg);
   NOREF(ppszArgs);

   tySI.tyProcConfig.Processor._MIPS.bDoNotIssueHardReset= TRUE;
   return 0;
}

/****************************************************************************
     Function: TranslateName
     Engineer: Vitezslav Hola
        Input: char *pszDst - storage for translated name
               char *pszSrc - name to translate
               unsigned int uiSize - size of destination storage
       Output: char * - copy of pszDst
  Description: Translate device names to be command line option compatible
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static char *TranslateName(char *pszDst, char *pszSrc, unsigned int uiSize)
{
   char *pszTemp;

   assert(pszDst != NULL);
   assert(pszSrc != NULL);
   assert(uiSize > 0);
   strncpy(pszDst, pszSrc, uiSize-1);
   pszDst[uiSize-1] = '\0';
   StringToLwr(pszDst);
   // first convert anything unusual to '-'
   pszTemp = pszDst;
   while (*pszTemp)
      {
      if (((*pszTemp < 'a') || (*pszTemp > 'z'))  && ((*pszTemp < '0') || (*pszTemp > '9')))
         *pszTemp = '-';
      pszTemp++;
      }
   // remove any leading '-'
   while (*pszDst == '-')
      strcpy(pszDst, pszDst+1);
   // remove any trailing '-'
   pszTemp = pszDst + strlen(pszDst) -1;
   while ((pszTemp > pszDst) && (*pszTemp == '-'))
      *pszTemp-- = '\0';
   // remove any duplicate '-'
   pszTemp = pszDst;
   while (*pszTemp)
      {
      if ((pszTemp[0] == '-') && (pszTemp[1] == '-'))
         strcpy(pszTemp, pszTemp+1);
      else
         pszTemp++;
      }
   // remove leading 'nxp-'
   if (strncmp(pszDst, "nxp-", 4) == 0)
      strcpy(pszDst, pszDst+4);

   // remove '-core' if present
   if ((pszTemp = strstr(pszDst, "-core")) != NULL)
      strcpy(pszTemp, pszTemp+5);
   // change '-pin' to 'pin'  (e.g. -14-pin => -14pin)
   if ((pszTemp = strstr(pszDst, "-pin")) != NULL)
      strcpy(pszTemp, pszTemp+1);
   return pszDst;
}

/****************************************************************************
     Function: HelpDevice
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Additional help for device option
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void HelpDevice(void)
{
   TyXMLNode *ptyNode;
   char *pszDevice;
   char szBuffer[_MAX_PATH];
   ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Processor");
   while (ptyNode != NULL)
      {
      if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
         {
         PrintMessage(HELP_MESSAGE, "%*s  %s\n", GDBSERV_HELP_COL_WIDTH, " ", TranslateName(szBuffer, pszDevice, sizeof(szBuffer)));
         }
      ptyNode = XML_GetNextNode(ptyNode);
      }
}

/****************************************************************************
     Function: HelpJtagFrequency
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Additional help for frequency selection
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void HelpJtagFrequency(void)
{
   PrintMessage(HELP_MESSAGE, "%*s  <freq> for Opella is: 24MHz, 12MHz,8MHz,\n", GDBSERV_HELP_COL_WIDTH, " ");
   PrintMessage(HELP_MESSAGE, "%*s  6MHz, 4MHz, 3MHz, 2MHz or 1MHz (default).\n", GDBSERV_HELP_COL_WIDTH, " ");
   PrintMessage(HELP_MESSAGE, "%*s  <freq> for Opella-XD can be in range\n", GDBSERV_HELP_COL_WIDTH, " ");
   PrintMessage(HELP_MESSAGE, "%*s  from 10kHz to 100MHz (default 1MHz)\n", GDBSERV_HELP_COL_WIDTH, " ");
}

/****************************************************************************
     Function: HelpRegisterSettingsFile
     Engineer: Patrick Shiels
        Input: none
       Output: none
  Description: 
Date           Initials    Description
21-Mar-2006    PJS         Initial
*****************************************************************************/
static void HelpRegisterSettingsFile()
{
   PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", "Text file format is: Name Size Addr Value. E.g.:");
   PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", " R0 0x00000004 0xb800380c 0x18000000"            );
   PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", " R1 0x00000004 0xb8003808 0x00000006"            );
   PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", "Or: Name Value. E.g.:"                           );
   PrintMessage(HELP_MESSAGE, "%*s%s\n", GDBSERV_HELP_COL_WIDTH, " ", " CMEM0 0x0F"                                     );
}
/****************************************************************************
     Function: OptionMulticore
     Engineer: Amala Nair G S
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Calling GDBServer using mixedCoreFile "mixedcores_mips.xml"
Date           Initials    Description
28-July-2011   ANGS        Initial
*****************************************************************************/
static int OptionMulticore(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   TyXMLNode *ptyNode;
   TyXMLFile tyXMLSCFile;
   int iError;
   char szFileName[_MAX_PATH];
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);
   if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, GDB_EMSG_FILENAME_MISSING, ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   strncpy(szFileName, ppszArgs[*puiArg], sizeof(szFileName)-1);
   szFileName[sizeof(szFileName)-1] = '\0';
   iError = XML_ReadFile(szFileName, &tyXMLSCFile);
   if (iError != 0)
      return iError;
   ptyNode = XML_GetNode(XML_GetBaseNode(&tyXMLSCFile), "TargetDefinition");
   if (ptyNode == NULL)
      {
      sprintf(tySI.szError, GDB_EMSG_SCANCHAIN_FILE_INVALID, tySI.tyXMLFile.szName);
      return GDBSERV_CFG_ERROR;
      }
   CFG_ScanChainNode(ptyNode);
   XML_Free(&tyXMLSCFile);
   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: OptionSelectedcore
     Engineer: Amala Nair G S
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Telling the core, which is selected in MIPS multi-core selection, to GDBServer
Date           Initials    Description
28-July-2011   ANGS        Initial
*****************************************************************************/
static int OptionSelectedcore(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
    if (((*puiArg)+1) >= uiArgCount)
      {
      sprintf(tySI.szError, "Missing Selected Core number after %s option.", ppszArgs[*puiArg]);
      return GDBSERV_OPT_ERROR;
      }
   (*puiArg)++;
   tySI.tyProcConfig.Processor._MIPS.bSelectedCore = ((atoi(ppszArgs[*puiArg]))+1);    
   return GDBSERV_NO_ERROR;
} 
