/****************************************************************************
       Module: gdbmips.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, MIPS target interface header file
Date           Initials    Description
08-Feb-2008    VH          Initial
08-AUG-2009    DK          Modified for adding TLB data structures
01-Nov-2011    ANGS        Modified to add the function LOW_ElfPath()
30-NOv-2011    SV          Modified to add function LOW_ConfigureTrace()
****************************************************************************/
#ifndef GDBMIPS_H_
#define GDBMIPS_H_

#define MAX_TXCMD_WR_MEM_LEN              0x7FF8

#define Config1RegNo 16
#define Config1RegSel 1
#define MMUSizeBitMask 0x7E000000
#define MMUSizeBitShift 25

typedef unsigned long	ulong;
typedef unsigned short  uint16;
typedef unsigned long  uint32;
// Reset Types
typedef enum
{
   RES_EMULATOR   =0x00,
   RES_PROCESSOR  =0x01,
   RES_VIA_PRRST  =0x02,
   RES_VIA_PERRST =0x03,
   RES_VIA_JTAGRST=0x04,
   RES_VIA_TRST   =0x05,
   RES_VIA_RST    =0x06
} TXRXResetTypes;

// Read Types - NEVER change these values after
// they have been assigned
typedef enum
{
// Common
   MEM_NORMAL_ACCESS                = 0x00,
   MEM_VIRTUAL_ACCESS               = 0x01,
   MEM_PHYSICAL_ACCESS              = 0x02,
// Keep 0x3 to 0x0F free for future use
   
// MEM_CODE read types
   MEM_CODE_EEPROM_ACCESS              = 0x10,
   MEM_CODE_PHYSICAL_ACCESS            = 0x11,
   MEM_CODE_MIFARE_EEPROM_ACCESS       = 0x12,
   MEM_USER_MODE_CODE_SEG_0_ACCESS     = 0x13,
   MEM_USER_MODE_CODE_SEG_1_ACCESS     = 0x14,
   MEM_CODE_VIRTUAL_ACCESS             = 0x15,
   MEM_EXTENDED_P8XX_ACCESS            = 0x16,
   MEM_SMARTMX_RAM_EDATA_ACCESS        = 0x17,
   MEM_CODE_FLASH_ACCESS               = 0x18,
   MEM_CODE_BOOT_MODE_READ_ACCESS      = 0x19,
   MEM_CODE_BOOT_MODE_WRITE_ACCESS     = 0x1A,
   MEM_CODE_NORMAL_MODE_READ_ACCESS    = 0x1B,
   MEM_CODE_NORMAL_MODE_WRITE_ACCESS   = 0x1C,
   MEM_CODE_FLASH_READ_ACCESS          = 0x1D,
   MEM_CODE_FLASH_WRITE_ACCESS         = 0x1E,

// MEM_XDATA read types
   MEM_XDATA_PHYSICAL_ACCESS           = 0x40,
   MEM_XDATA_BOOT_MODE_ACCESS          = 0x41,
   MEM_XDATA_NORMAL_MODE_ACCESS        = 0x42,
   
   //MEM_EEPROM
   MEM_EEPROM_PAGE_ZERO_WRITE_ACCESS   = 0x60,
   MEM_EEPROM_MIFARE_ACCESS            = 0x61,
   MEM_EEPROM_PAGE_ZERO_READ_ACCESS    = 0x60,

// MEM_RAM read types
   MEM_RAM_USER_MODE_ACCESS            = 0x80,
   MEM_RAM_PHYSICAL_ACCESS             = 0x81,
   MEM_RAM_SEG0_ACCESS                 = 0x82,
   MEM_RAM_SEG1_ACCESS                 = 0x83,
   MEM_RAM_SEG2_ACCESS                 = 0x84,
   MEM_RAM_SEG3_ACCESS                 = 0x85,
   MEM_RAM_SEG4_ACCESS                 = 0x86,
   MEM_RAM_SEG5_ACCESS                 = 0x87,
   MEM_RAM_SEG6_ACCESS                 = 0x88,
   MEM_RAM_SEG7_ACCESS                 = 0x89,
   MEM_RAM_SEG8_ACCESS                 = 0x8A,
   MEM_RAM_SEG9_ACCESS                 = 0x8B,
   MEM_RAM_SEG10_ACCESS                = 0x8C,
   MEM_RAM_SEG11_ACCESS                = 0x8D,
   MEM_RAM_SEG12_ACCESS                = 0x8F,
   MEM_RAM_SEG13_ACCESS                = 0x90,
   MEM_RAM_SEG14_ACCESS                = 0x91,
   MEM_RAM_SEG15_ACCESS                = 0x92,
   MEM_RAM_VIRTUAL_ACCESS              = 0x93,
   MEM_TLB_ENTRY_ACCESS                = 0x100,
   MEM_ICACHE_ENTRY_TAG_ACCESS         = 0x101,
   MEM_ICACHE_ENTRY_DATA_ACCESS        = 0x102,
   MEM_DCACHE_ENTRY_TAG_ACCESS         = 0x103,
   MEM_DCACHE_ENTRY_DATA_ACCESS        = 0x104,
   MEM_ICACHE_ENTRY_WS_ACCESS          = 0x105,
   MEM_DCACHE_ENTRY_WS_ACCESS          = 0x106
} TXRXMemAccessTypes;
//structure to initialize TLB entry data structure
typedef union __TyEntryLo
{
   unsigned int Data;
   struct
   {
      unsigned int G    :1;
      unsigned int V    :1;
      unsigned int D    :1;
      unsigned int C    :3;
      unsigned int PFN  :20;
      unsigned int Zero :4;
      unsigned int XI   :1;
      unsigned int RI   :1;
   }Bits;
} tyEntryLo;

typedef union __TyEntryHi
{
   unsigned int Data;
   struct
   {
      unsigned int ASID    :8;
      unsigned int Zero    :3;
      unsigned int VPN2X   :2;
      unsigned int VPN2    :19;
   }Bits;
} tyEntryHi;

typedef union __TyPageMask
{
   unsigned int Data;
   struct
   {
      unsigned int LowerZero  :11;
      unsigned int Mask       :18; 
      unsigned int UpperZero  :3;
   }Bits;
} tyPageMask;

typedef struct __TyTLBEntry
{
   tyEntryLo   EntryLo0;
   tyEntryLo   EntryLo1;
   tyEntryHi   EntryHi;
   tyPageMask  PageMask;
}tyTLBEntry;

typedef enum {
	linux26
	} TyOSType;

typedef struct
{
   char *pszRegName;             // register name
   unsigned long ulAddrIndex;
} TyARCReg;

int GetTLBCount( unsigned short *usCount );
int  LOW_WriteTLB(tyTLBEntry *ptyTLBentry, unsigned short iTLBcount);
int LOW_TestScanChain(void);
int GetRegAddr(char *pRegName, int *iRegAddr);
int LOW_SetActiveTapIndex(int iIndex);
int  ReadTLB (char *pcInput, char *pcOutput);
int LOW_ReadTLB(tyTLBEntry **pptyTLBentry, unsigned char *pucNumOfTLBEntry);

//int LOW_AddCoreRegDefArrayEntries(void);
//int LOW_IndexCommonRegDefArrayEntries(void);

int LOW_PFXDConnect(void);
int LOW_SWReset(void);
int LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef, 
                             unsigned long *pulRegValue);
int LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef,
                              unsigned long ulRegValue);
int Low_EnableOSMMU(TyOSType tyos, unsigned long *pulOSMMUAddrs);
int Low_SetOSAppMMU(unsigned long ulAppCon, unsigned long ulPgDirAddrs);

int LOW_SaveMicrochipBmxRegisters(void);
int LOW_EraseChipMicrochipFlash(void);
int LOW_SetupMicrochipFlash(int bClearCodeProtection,unsigned long* getCodeProtectionStatus);
int LOW_PartitionRamForMicrochipFlashSetup(unsigned long ulDataKernelSpace);
int LOW_FetchTrace(unsigned long ulBegPtr,unsigned long ulEndptr);
int LOW_InvCache(void);
int LOW_ElfPath(char* ulElfPath);
int LOW_ConfigureTrace(unsigned long ulReg1, unsigned long ulReg2, unsigned long ulReg3);
int MdiReadReg(unsigned long tyReg, unsigned long *pulDataValue);
int MdiWriteReg(unsigned long tyReg, unsigned long ulDataValue);

#if 0
   int LOW_EnableAdvBpEntryForAddr(unsigned long  ulAddress , int iCurrentCoreIndex);
   int LOW_DisableAdvBpEntryForAddr(unsigned long  ulAddress , int iCurrentCoreIndex);
#endif

#endif// GDBMIPS_H_

