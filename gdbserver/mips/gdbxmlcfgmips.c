/****************************************************************************
       Module: gdbxmlcfgmips.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, contains needed resources to parse 
               family specific XML file parameters for MIPS.
Date           Initials    Description
08-Feb-2008    VH          Initial
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "gdbserver/gdbglob.h" 
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/mips/gdbmips.h"
#include "protocol/mips/commdefs.h"

#ifdef __LINUX
#else
#define strncasecmp _strnicmp
#endif
// external functions
extern TyServerInfo tySI;

// local function prototypes
static void CFG_DebuggerControlNode(TyXMLNode *ptyThisNode);
static void CFG_DebuggerControlFilesNode(TyXMLNode *ptyThisNode);
static void CFG_MIPSControlNode(TyXMLNode *ptyThisNode);
static void CFG_EndianNode(TyXMLNode *ptyThisNode);
static void CFG_DialogNode(TyXMLNode *ptyThisNode);
static void CFG_ProcessorConfigurationNode(TyXMLNode *ptyThisNode);
static void CFG_MIPSProcessorConfigDialogNode(TyXMLNode *ptyThisNode);
static void CFG_GenericControlNode(TyXMLNode *ptyThisNode);
static void CFG_EmulatorNode(TyXMLNode *ptyThisNode);
static void CFG_EmulatorControlFilesNode(TyXMLNode *ptyThisNode);
static void CFG_DiskwareNode(TyXMLNode *ptyThisNode);
static void CFG_MIPSEmulatorNode(TyXMLNode *ptyThisNode);
static void CFG_MIPSDiskwareInfoNode(TyXMLNode *ptyThisNode);
static void CFG_MIPSWriteEnableNode(TyXMLNode *ptyThisNode);
static void CFG_DebugMemoryNode(TyXMLNode *ptyThisNode);
static void CFG_RangeNode(TyXMLNode *ptyThisNode, unsigned long * pulStart, unsigned long * pulEnd);
static void CFG_CacheNode(TyXMLNode *ptyThisNode);
//static void CFG_DataCacheNode(TyXMLNode *ptyThisNode);
//static void CFG_InstructionCacheNode(TyXMLNode *ptyThisNode);
static void CFG_ScratchPadNode(TyXMLNode *ptyThisNode);
static void CFG_DataScratchPadNode(TyXMLNode *ptyThisNode);
static void CFG_InstructionScratchPadNode(TyXMLNode *ptyThisNode);
//static void CFG_MemoryNode(TyXMLNode *ptyThisNode);
//static void CFG_OnChipNode(TyXMLNode *ptyThisNode);
//static void CFG_FLASHNode(TyXMLNode *ptyThisNode);
//static void CFG_EEPROMNode(TyXMLNode *ptyThisNode);
//static void CFG_TLBNode(TyXMLNode *ptyThisNode);
static void CFG_PeripheralNode(TyXMLNode *ptyThisNode);
//static TyProcFamGrp CFG_FamilyGroup(char * pszName);
static void CFG_TraceNode(TyXMLNode *ptyThisNode);
static void CFG_MIPSTraceNode(TyXMLNode *ptyThisNode);
int CFG_AddGroupDefArrayEntry(char  *pszGroupName,
                              int   iStartRegIndex,
                              int   iEndRegIndex, 
                              int   iRegDefType);
static int CFG_ValidRegDefLine(char                *pszLine,
                                   int             iLineNumber,
                                   int             *pbValidReg,
                                   char            *pszRegName,
                                   unsigned long   *pulAddress,
                                   unsigned char   *pubMemType,
                                   unsigned char   *pubByteSize,
                                   unsigned char   *pubBitOffset,
                                   unsigned char   *pubBitSize,
                                   int             *piFullRegIndex,
                                   int             *piType);
void CFG_ScanChainNode(TyXMLNode *ptyThisNode);
/****************************************************************************
     Function: CFG_ProcessorNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
19-Oct-2011    ANGS        Modified for Zephyr trace support 
*****************************************************************************/
void CFG_ProcessorNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   char       *pszName;

   // Process child nodes
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "DebuggerControl")) != NULL)
      CFG_DebuggerControlNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Emulator")) != NULL)
      CFG_EmulatorNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Cache")) != NULL)
      CFG_CacheNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Trace")) != NULL)
      CFG_TraceNode(ptyNode);
//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Memory")) != NULL)
//      CFG_MemoryNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Peripheral")) != NULL)
      CFG_PeripheralNode(ptyNode);

   // Processor Name
   if ((pszName = XML_GetAttrib(ptyThisNode, "Device")) != NULL)
      {
      strncpy(tySI.tyProcConfig.szProcName, pszName, sizeof(tySI.tyProcConfig.szProcName) -1);
      tySI.tyProcConfig.szProcName[sizeof(tySI.tyProcConfig.szProcName) -1] = '\0';
      }
   // Internal Name (for identification purposes)
   if ((pszName = XML_GetChildElement(ptyThisNode, "InternalName")) != NULL)
      {
      strncpy(tySI.tyProcConfig.szInternalName, pszName, sizeof(tySI.tyProcConfig.szInternalName) -1);
      tySI.tyProcConfig.szInternalName[sizeof(tySI.tyProcConfig.szInternalName) -1] = '\0';
      }
   if (tySI.tyProcessorFamily == _MIPS)
      {
      // TO_DO:
      //strcpy(tySI.tyProcConfig.szDiskWare1,"MIPS.DW1");
      //strcpy(tySI.tyProcConfig.szDiskWare2,"MIPS.DW2");

      if (!tySI.tyProcConfig.Processor._MIPS.bCacheFlushSupported)
         strcpy(tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename,"NONE");
      }
}

/****************************************************************************
     Function: CFG_DebuggerControlNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_DebuggerControlNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "DebuggerControlFiles")) != NULL)
      CFG_DebuggerControlFilesNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "MIPSControl")) != NULL)
      CFG_MIPSControlNode(ptyNode);

   if (tySI.tyProcessorFamily == _MIPS)
      {
      if ((ptyNode = XML_GetChildNode(ptyThisNode, "Endian")) != NULL)
         CFG_EndianNode(ptyNode);
      else
         strcpy(tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName,"---");
      }

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Dialog")) != NULL)
      CFG_DialogNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "GenericControl")) != NULL)
      CFG_GenericControlNode(ptyNode);
}

/****************************************************************************
     Function: CFG_DebuggerControlFilesNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
14-Nov-2008    SPC         Added support for Device Enable Password config.
*****************************************************************************/
static void CFG_DebuggerControlFilesNode(TyXMLNode *ptyThisNode)
{
   char       *pszName;

   if ((pszName = XML_GetChildAttrib(ptyThisNode, "RegisterFile", "FileName")) != NULL)
      {
      strncpy(tySI.tyProcConfig.szSymFile, pszName, sizeof(tySI.tyProcConfig.szSymFile) -1);
      tySI.tyProcConfig.szSymFile[sizeof(tySI.tyProcConfig.szSymFile) -1] = '\0';
      }
   if ((pszName = XML_GetChildElement(ptyThisNode, "PasswordSequence")) != NULL)
	  {
	  strncpy(tySI.tyProcConfig.szPasswordSequence, pszName, sizeof(tySI.tyProcConfig.szPasswordSequence) -1);
	  tySI.tyProcConfig.szPasswordSequence[sizeof(tySI.tyProcConfig.szPasswordSequence) -1] = '\0';
	  }
   else
	  {
	   strcpy(tySI.tyProcConfig.szPasswordSequence, "");
      }
   if ((pszName = XML_GetChildElement(ptyThisNode, "PasswordFile1")) != NULL)
	  {
	  strncpy(tySI.tyProcConfig.szPasswordFile1, pszName, sizeof(tySI.tyProcConfig.szPasswordFile1) -1);
	  tySI.tyProcConfig.szPasswordFile1[sizeof(tySI.tyProcConfig.szPasswordFile1) -1] = '\0';
	  }
   else
	  {
	   strcpy(tySI.tyProcConfig.szPasswordFile1, "");
      }
   if ((pszName = XML_GetChildElement(ptyThisNode, "PasswordFile2")) != NULL)
	  {
	  strncpy(tySI.tyProcConfig.szPasswordFile2, pszName, sizeof(tySI.tyProcConfig.szPasswordFile2) -1);
	  tySI.tyProcConfig.szPasswordFile2[sizeof(tySI.tyProcConfig.szPasswordFile2) -1] = '\0';
	  }
   else
	  {
	   strcpy(tySI.tyProcConfig.szPasswordFile2, "");
      }
   if ((pszName = XML_GetChildElement(ptyThisNode, "PasswordFile3")) != NULL)
	  {
	  strncpy(tySI.tyProcConfig.szPasswordFile3, pszName, sizeof(tySI.tyProcConfig.szPasswordFile3) -1);
	  tySI.tyProcConfig.szPasswordFile3[sizeof(tySI.tyProcConfig.szPasswordFile3) -1] = '\0';
	  }
   else
	  {
	   strcpy(tySI.tyProcConfig.szPasswordFile3, "");
      }
}

/****************************************************************************
     Function: CFG_MIPSControlNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_MIPSControlNode(TyXMLNode *ptyThisNode)
{
   NOREF(ptyThisNode);
//   char       *pszName;

//   if ((pszName = XML_GetChildElement(ptyThisNode, "ProcessorSwitch")) != NULL)
//      {
//      strncpy(tySI.tyProcConfig.Processor._MIPS.szSimTransferProcName, pszName, sizeof(tySI.tyProcConfig.Processor._MIPS.szSimTransferProcName) -1);
//      tySI.tyProcConfig.Processor._MIPS.szSimTransferProcName[sizeof(tySI.tyProcConfig.Processor._MIPS.szSimTransferProcName) -1] = '\0';
//      }
}

/****************************************************************************
     Function: CFG_EndianNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_EndianNode(TyXMLNode *ptyThisNode)
{
   char       *pszName;

   tySI.tyProcConfig.bEndianModeModifiable = 
            (int)XML_GetChildState(ptyThisNode, "ModeModifiable");

   if ((pszName = XML_GetChildAttrib(ptyThisNode, "ModeRegBitName", "Name")) != NULL)
      {
      strncpy(tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName, pszName, sizeof(tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName) -1);
      tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName[sizeof(tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName) -1] = '\0';
      }
}

/****************************************************************************
     Function: CFG_DialogNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_DialogNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "ProcessorConfiguration")) != NULL)
      CFG_ProcessorConfigurationNode(ptyNode);
}

/****************************************************************************
     Function: CFG_ProcessorConfigurationNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_ProcessorConfigurationNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "MIPSProcessorConfigDialog")) != NULL)
      CFG_MIPSProcessorConfigDialogNode(ptyNode);
}

/****************************************************************************
     Function: CFG_MIPSProcessorConfigDialogNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_MIPSProcessorConfigDialogNode(TyXMLNode *ptyThisNode)
{
   NOREF(ptyThisNode);

//   tySI.tyProcConfig.Processor._MIPS.bDMASupported =
//            XML_GetChildState(ptyThisNode, "ShowDMA");
}

/****************************************************************************
     Function: CFG_GenericControlNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
12-Jun-2007    VH          Added support for Opella-XD
*****************************************************************************/
static void CFG_GenericControlNode(TyXMLNode *ptyThisNode)
{

   tySI.tyProcConfig.ulMaxJtagFrequency =
            (unsigned long)XML_GetChildElementDec(ptyThisNode, "MaxJtagFrequencyKhz", 100000)*1000;        // max frequency of 100 MHz for Opella-XD

}

/****************************************************************************
     Function: CFG_EmulatorNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_EmulatorNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "EmulatorControlFiles")) != NULL)
      CFG_EmulatorControlFilesNode(ptyNode);
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "MIPSEmulator")) != NULL)
      CFG_MIPSEmulatorNode(ptyNode);
}

/****************************************************************************
     Function: CFG_EmulatorControlFilesNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_EmulatorControlFilesNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Diskware")) != NULL)
      CFG_DiskwareNode(ptyNode);
}

/****************************************************************************
     Function: CFG_DiskwareNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_DiskwareNode(TyXMLNode *ptyThisNode)
{
   char       *pszName;
   if ((pszName = XML_GetChildAttrib(ptyThisNode, "Diskware1", "FileName")) != NULL)
      {
      strncpy(tySI.tyProcConfig.szDiskWare1, pszName, sizeof(tySI.tyProcConfig.szDiskWare1) -1);
      tySI.tyProcConfig.szDiskWare1[sizeof(tySI.tyProcConfig.szDiskWare1) -1] = '\0';
      }
   if ((pszName = XML_GetChildAttrib(ptyThisNode, "Diskware2", "FileName")) != NULL)
      {
      strncpy(tySI.tyProcConfig.szDiskWare2, pszName, sizeof(tySI.tyProcConfig.szDiskWare2) -1);
      tySI.tyProcConfig.szDiskWare2[sizeof(tySI.tyProcConfig.szDiskWare2) -1] = '\0';
      }
}

/****************************************************************************
     Function: CFG_MIPSEmulatorNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_MIPSEmulatorNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "MIPSDiskwareInfo")) != NULL)
      CFG_MIPSDiskwareInfoNode(ptyNode);
}

/****************************************************************************
     Function: CFG_MIPSDiskwareInfoNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description

*****************************************************************************/
static void CFG_MIPSDiskwareInfoNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   unsigned long       ulMultiCoreCoreNum, ulMultiCoreDMACoreNum;
   unsigned int        uiIndex;
   char        szNodeName[_MAX_PATH];

   if(NULL == tySI.tyProcConfig.Processor._MIPS.ptyMDInfo)
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo = (TyMDInfo*)malloc(sizeof(TyMDInfo));
   if(NULL == tySI.tyProcConfig.Processor._MIPS.ptyMDInfo)
      return;
   memset(tySI.tyProcConfig.Processor._MIPS.ptyMDInfo,0x00,sizeof(TyMDInfo));
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->usTargetSupply                = (unsigned short)CFG_Enum(XML_GetChildAttrib(ptyThisNode, "TargetVoltage", "SupplyType"));
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseSlowJTAG                  = (unsigned char)XML_GetChildState(ptyThisNode, "UseSlowJTAG");
   /* support for pnx5100*/   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly               = (unsigned char)XML_GetChildState(ptyThisNode, "bEJTAGDMAOnly");
   /* support for pnx5100*/
      /* support for pnx5100*/   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bNoMMUTranslation              = (unsigned char)XML_GetChildState(ptyThisNode, "bNoMMUTranslation");  //Dong Ji for PNX5100 No address translation
   /* support for pnx5100*/
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bFreeRunningJTAGClk           = (unsigned char)XML_GetChildState(ptyThisNode, "FreeRunningJTAGClk");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseEJTAGBootDuringReset      = (unsigned char)XML_GetChildState(ptyThisNode, "UseEJTAGBootDuringReset");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGBootSupported           = (unsigned char)XML_GetChildState(ptyThisNode, "EJTAGBootSupported");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bHWBpASIDSupported            = (unsigned char)XML_GetChildState(ptyThisNode, "HWBpASIDSupported");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bOddAddressFor16BitModeBps    = (unsigned char)XML_GetChildState(ptyThisNode, "OddAddressFor16BitModeBps");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bSSIn16BitModeSupported       = (unsigned char)XML_GetChildState(ptyThisNode, "SSIn16BitModeSupported");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulNumInstHWBps                = XML_GetChildElementHex(ptyThisNode, "ulNumInstHWBps", 0);   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulNumDataHWBps                = XML_GetChildElementHex(ptyThisNode, "ulNumDataHWBps", 0);   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucNumberOfNOPSInERETBDS       = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ulNumberOfNOPSInBDS", 1);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEnableMIPS16ModeForTrace     = (unsigned char)XML_GetChildState(ptyThisNode, "EnableMIPS16ModeForTrace");    
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformHardReset             = (unsigned char)XML_GetChildState(ptyThisNode, "PerformHardReset");            
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformSoftProcessorReset    = (unsigned char)XML_GetChildState(ptyThisNode, "PerformSoftProcessorReset");   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformSoftPeripheralReset   = (unsigned char)XML_GetChildState(ptyThisNode, "PerformSoftPeripheralReset");  
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformHardBetweenSoftReset  = (unsigned char)XML_GetChildState(ptyThisNode, "PerformHardBetweenSoftReset"); 
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bDoubleReadOnDMA              = (unsigned char)XML_GetChildState(ptyThisNode, "DoubleReadOnDMA");
//   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bInvalidateCacheOnAReset      = (unsigned char)XML_GetChildState(ptyThisNode, "InvalidateCacheOnAReset");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bOnlyFlushDataCache           = (unsigned char)XML_GetChildState(ptyThisNode, "OnlyFlushDataCache");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulInvalidateInst              = XML_GetChildElementHex(ptyThisNode, "ulInvalidateInst", 0xBCC00000);   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulInvalidateWBInst            = XML_GetChildElementHex(ptyThisNode, "ulInvalidateWBInst", 0xBCDC0000);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->EJTAGStyle                    = (int)CFG_Enum(XML_GetAttrib(ptyThisNode, "Type"));
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bCoreSelectBeforeReset        = (unsigned char)XML_GetChildState(ptyThisNode, "CoreSelectBeforeReset");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseRestrictedDmaAddressRange = (unsigned char)XML_GetChildState(ptyThisNode, "bUseRestrictedDmaAddressRange");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulStartOfValidDmaAddressRange = XML_GetChildElementHex(ptyThisNode, "ulStartOfValidDmaAddressRange", 0x0);   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEndOfValidDmaAddressRange   = XML_GetChildElementHex(ptyThisNode, "ulEndOfValidDmaAddressRange", 0x1FFFFFFF);   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMemWriteBeforeReset         = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucMemWriteBeforeReset", 0x0);   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulMemResetWriteAddr           = XML_GetChildElementHex(ptyThisNode, "ulMemResetWriteAddr", 0x0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulMemResetWriteValue          = XML_GetChildElementHex(ptyThisNode, "ulMemResetWriteValue", 0x0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bMicroChipM4KFamily           = (unsigned char)XML_GetChildState(ptyThisNode, "bMicroChipM4KFamily");   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulNumberOfShadowSetsPresent   = XML_GetChildElementHex(ptyThisNode, "ulNumberOfShadowSetsPresent", 0x0);   

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "MIPSWriteEnable")) != NULL)
      CFG_MIPSWriteEnableNode(ptyNode);

   // if any of the cores are specified then do not allow the user to override
   ulMultiCoreCoreNum     =  XML_GetChildElementHex(ptyThisNode, "ucMultiCoreCoreNum",    0xFFFFFFFF);
   ulMultiCoreDMACoreNum  =  XML_GetChildElementHex(ptyThisNode, "ucMultiCoreDMACoreNum", 0xFFFFFFFF);

   if ((ulMultiCoreCoreNum != 0xFFFFFFFF) || (ulMultiCoreDMACoreNum != 0xFFFFFFFF))
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bFixedCoreNums     = TRUE;      
   else
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bFixedCoreNums     = FALSE;

   // Core number of device to use (if multiple devices!)  default to first device
   if (ulMultiCoreCoreNum != 0xFFFFFFFF)
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreCoreNum = (unsigned char)ulMultiCoreCoreNum;
   else
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreCoreNum = 0x1;

   // Use this core for DMA accesses - some systems provide a second device that handles DMA (current PNX8550)
   // default to the first core
   if (ulMultiCoreDMACoreNum != 0xFFFFFFFF)
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreDMACoreNum  = (unsigned char)ulMultiCoreDMACoreNum;
   else
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreDMACoreNum  = 0x1;

   // How many NOPS for the JUMP BDS (1 or 3 on PR4450)
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucNumberOfNOPSInJUMPBDS   = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucNOPSInJUMPBDS", 1);

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "DebugMemory")) != NULL)
      CFG_DebugMemoryNode(ptyNode);

   // Some extra fields that were added for Yoda

   // Number of cores in the chain - if non-zero we will not try to detect this and
   // instead use the information in the XML file
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreNumCores = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucMultiCoreNumCores", 0x1);

   // Philips MOJO uses a non-standard code to select the IDCODE register. If not present then use the default
   // Core Select Code
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucIDCODEInstCode    = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucIDCODEInstCode", MIPS_IR_IDCODE);
   
   // AMD AU1500 has a non-standard EJTAG Address register length. If not present then use the default
   // length of 32
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucLengthOfAddressRegister = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucLengthOfAddressRegister", 0x20);

   // Select the ALL/ADDRESS after every access
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bSelectAddrRegAfterEveryAccess = (unsigned char)XML_GetChildElementHex(ptyThisNode, "bSelectAddrRegAfterEveryAccess", 0x0);

   // Sync instruction not supported
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bSyncInstNotSupported = (unsigned char)XML_GetChildElementHex(ptyThisNode, "bSyncInstNotSupported", 0x0);

   // Do a second reset request
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bAssertHaltTwice    = (unsigned char)XML_GetChildElementHex(ptyThisNode, "bAssertHaltTwice", 0x0);

   // Enable the faster DMA routines
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseFastDMAFunc     = (unsigned char)XML_GetChildElementHex(ptyThisNode, "bUseFastDMAFunc", 0x0);

   for (uiIndex=0; uiIndex<(unsigned int)MAX_CORES_IN_CHAIN; uiIndex++)
      {
      // Device n IR Length
      sprintf(szNodeName, "%s%u", "ulIRLength", uiIndex+1);
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->tyMultiCoreDeviceInfo[uiIndex].ulIRLength =
                  XML_GetChildElementHex(ptyThisNode, szNodeName, IR_LENGTH);
         
      // Device n Bypass Code
      sprintf(szNodeName, "%s%u", "ulBypassCode", uiIndex+1);
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->tyMultiCoreDeviceInfo[uiIndex].ulBypassCode =
                  XML_GetChildElementHex(ptyThisNode, szNodeName, MIPS_IR_BYPASS);
      }
   // Core Select Code
   // If this is present and not BYPASS, then this value is scanned into the instruction register after a
   // reset to select the core that we want to debug. It is also used as the bypass code when we are scanning
   // data into the instruction registers instead of the normal BYPASS code. This is to support YODA where the
   // 4KEc must first be enabled (at which point there are two cores on the chain) and then kept enabled by
   // scanning this value into the PR190 IR register
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulCoreSelectCode     = XML_GetChildElementHex(ptyThisNode, "ulCoreSelectCode", MIPS_IR_BYPASS);

   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulCoreSelectIRLength = XML_GetChildElementHex(ptyThisNode, "ulCoreSelectIRLength", 0);

   // Delay after a reset
   // Some systems need a longer time after a reset to stabilise. This is an added delay on top
   // of a fixed delay
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDelayAfterResetMS  = XML_GetChildElementHex(ptyThisNode, "ulDelayAfterResetMS", 0);

   // Delay for processor to stabilize after giving reset command. Default is 500 Ms.
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulResetDelayMS  = XML_GetChildElementHex(ptyThisNode, "ulResetDelayMS", 500);

   // Do a DMA write after reset of the system - this is used by PNX8550 (Viper2) to release the proc from reset
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucDMAWriteAfterReset = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucDMAWriteAfterReset", 0);

   // DMA after reset write address
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDMAResetWriteAddr  = XML_GetChildElementHex(ptyThisNode, "ulDMAResetWriteAddr", 0);

   // DMA after reset write value
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDMAResetWriteValue = XML_GetChildElementHex(ptyThisNode, "ulDMAResetWriteValue", 0);

   // EJTAG Security sequence
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucEJTAGSecSequence   = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucEJTAGSeq", 0);

   // TO_DO: this does not seem to appear in XML file any longer
   // Lexra 4180 says ReadByte where he should be saying ReadWord
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bAssumeWordAccess    = (unsigned char)XML_GetChildElementHex(ptyThisNode, "bAssumeWordAccess", 0);

   //AMD Au1x00 uses emulated hardware breakpoint as EJTAG Debug core doesn't support HW BPs
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseInstrWatchForBP = (unsigned char)XML_GetChildState(ptyThisNode, "UseInstrWatchForBP");                                                                                                                

   //PNX8535 (TV520) uses ucMemReadAfterReset to enable access to memory after reset   
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMemReadAfterReset = (unsigned char)XML_GetChildElementHex(ptyThisNode, "ucMemReadAfterReset", 0x0);

   // restriction for transfers
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitCacheRdBpS = (unsigned long)XML_GetChildElementDec(ptyThisNode, "ulLimitCacheRdBpS", 0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitCacheWrBpS = (unsigned long)XML_GetChildElementDec(ptyThisNode, "ulLimitCacheWrBpS", 0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitDmaRdBpS   = (unsigned long)XML_GetChildElementDec(ptyThisNode, "ulLimitDmaRdBpS", 0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitDmaWrBpS   = (unsigned long)XML_GetChildElementDec(ptyThisNode, "ulLimitDmaWrBpS", 0);
}

/****************************************************************************
     Function: CFG_MIPSWriteEnableNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_MIPSWriteEnableNode(TyXMLNode *ptyThisNode)
{
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEnableWriteRequired  = (unsigned char)XML_GetChildState(ptyThisNode, "WriteEnableRequired");
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEnableWriteAddress  = XML_GetChildElementHex(ptyThisNode, "EnableWriteAddress", 0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEnableWriteValue    = XML_GetChildElementHex(ptyThisNode, "EnableWriteValue",   0);
   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEnableWriteMask     = XML_GetChildElementHex(ptyThisNode, "EnableWriteMask",    0);
}

/****************************************************************************
     Function: CFG_DebugMemoryNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_DebugMemoryNode(TyXMLNode *ptyThisNode)
{
   NOREF(ptyThisNode);
//   TyXMLNode  *ptyNode;
//   unsigned long      ulStart = 0; 
//   unsigned long      ulEnd   = 0; 

//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Range")) != NULL)
//      CFG_RangeNode(ptyNode, &ulStart, &ulEnd);

//   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDebugMemStart  = ulStart;
//   tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDebugMemEnd    = ulEnd;
}

/****************************************************************************
     Function: CFG_RangeNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_RangeNode(TyXMLNode *ptyThisNode, unsigned long * pulStart, unsigned long * pulEnd)
{
   *pulStart = XML_GetChildElementHex(ptyThisNode, "Start", 0);
   *pulEnd   = XML_GetChildElementHex(ptyThisNode, "End",   0);
}

/****************************************************************************
     Function: CFG_CacheNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_CacheNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   char       *pszName;

   if ((pszName = XML_GetChildAttrib(ptyThisNode, "CacheFlushFileName", "FileName")) != NULL)
      {
      strncpy(tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename, pszName, sizeof(tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename) -1);
      tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename[sizeof(tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename) -1] = '\0';
      }
   else
      {
      strcpy(tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename,"NONE");
      }

   tySI.tyProcConfig.Processor._MIPS.bCacheFlushSupported = XML_GetChildState(ptyThisNode, "CacheFlushSupported");

//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "DataCache")) != NULL)
//      CFG_DataCacheNode(ptyNode);

//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "InstructionCache")) != NULL)
//      CFG_InstructionCacheNode(ptyNode);

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "ScratchPad")) != NULL)
      CFG_ScratchPadNode(ptyNode);
}

/****************************************************************************
     Function: CFG_DataCacheNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_DataCacheNode(TyXMLNode *ptyThisNode)
//{
//   tySI.tyProcConfig.Processor._MIPS.DCacheInfo.uiBytesPerTag = (uint16)XML_GetChildElementHex(ptyThisNode, "BytesPerTag", 0);
//   tySI.tyProcConfig.Processor._MIPS.DCacheInfo.uiCacheSize   = (uint16)XML_GetChildElementHex(ptyThisNode, "NumWays",     0);
//   tySI.tyProcConfig.Processor._MIPS.DCacheInfo.uiNumWays     = (uint16)XML_GetChildElementHex(ptyThisNode, "CacheSize",   0);
//}

/****************************************************************************
     Function: CFG_InstructionCacheNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_InstructionCacheNode(TyXMLNode *ptyThisNode)
//{
//   tySI.tyProcConfig.Processor._MIPS.ICacheInfo.uiBytesPerTag = (uint16)XML_GetChildElementHex(ptyThisNode, "BytesPerTag", 0);
//   tySI.tyProcConfig.Processor._MIPS.ICacheInfo.uiCacheSize   = (uint16)XML_GetChildElementHex(ptyThisNode, "NumWays",     0);
//   tySI.tyProcConfig.Processor._MIPS.ICacheInfo.uiNumWays     = (uint16)XML_GetChildElementHex(ptyThisNode, "CacheSize",   0);
//}

/****************************************************************************
     Function: CFG_ScratchPadNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_ScratchPadNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "DataScratchPad")) != NULL)
      CFG_DataScratchPadNode(ptyNode);

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "InstructionScratchPad")) != NULL)
      CFG_InstructionScratchPadNode(ptyNode);
}

/****************************************************************************
     Function: CFG_DataScratchPadNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_DataScratchPadNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   unsigned long      ulStart = 0; 
   unsigned long      ulEnd   = 0; 

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Range")) != NULL)
      CFG_RangeNode(ptyNode, &ulStart, &ulEnd);

   tySI.tyProcConfig.Processor._MIPS.ulDataScratchPadRamStart = ulStart;
   tySI.tyProcConfig.Processor._MIPS.ulDataScratchPadRamEnd   = ulEnd;
}

/****************************************************************************
     Function: CFG_InstructionScratchPadNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_InstructionScratchPadNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   unsigned long      ulStart = 0; 
   unsigned long      ulEnd   = 0; 

   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Range")) != NULL)
      CFG_RangeNode(ptyNode, &ulStart, &ulEnd);

   tySI.tyProcConfig.Processor._MIPS.ulInstScratchPadRamStart = ulStart;
   tySI.tyProcConfig.Processor._MIPS.ulInstScratchPadRamEnd   = ulEnd;
}

/****************************************************************************
     Function: CFG_MemoryNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_MemoryNode(TyXMLNode *ptyThisNode)
//{
//   TyXMLNode  *ptyNode;
//   char       *pszName;
//   int        bUseInitCodes = FALSE;

//   if ((pszName = XML_GetAttrib(ptyThisNode, "MemoryConfigType")) != NULL)
//      {
//      if (strcmp(pszName, "UseInitCodes") == 0)
//         bUseInitCodes = TRUE;
//      }

//   if (!bUseInitCodes)
//      {
//      if ((ptyNode = XML_GetChildNode(ptyThisNode, "OnChip")) != NULL)
//         CFG_OnChipNode(ptyNode);
//      }

//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "TLB")) != NULL)
//      CFG_TLBNode(ptyNode);
//}

/****************************************************************************
     Function: CFG_OnChipNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_OnChipNode(TyXMLNode *ptyThisNode)
//{
//   TyXMLNode  *ptyNode;

//   if (tySI.tyProcessorFamily == _MIPS)
//      {
//      if ((ptyNode = XML_GetChildNode(ptyThisNode, "FLASH")) != NULL)
//         CFG_FLASHNode(ptyNode);

//      if ((ptyNode = XML_GetChildNode(ptyThisNode, "EEPROM")) != NULL)
//         CFG_EEPROMNode(ptyNode);
//      }
//}

/****************************************************************************
     Function: CFG_FLASHNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_FLASHNode(TyXMLNode *ptyThisNode)
//{
//   TyXMLNode  *ptyNode;
//   unsigned long      ulStart = 0; 
//   unsigned long      ulEnd   = 0; 

//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Range")) != NULL)
//      CFG_RangeNode(ptyNode, &ulStart, &ulEnd);

//   tySI.tyProcConfig.Processor._MIPS.ulStartOfOnChipFLASH = ulStart;
//   tySI.tyProcConfig.Processor._MIPS.ulEndOfOnChipFLASH   = ulEnd;
//}

/****************************************************************************
     Function: CFG_EEPROMNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_EEPROMNode(TyXMLNode *ptyThisNode)
//{
//   TyXMLNode  *ptyNode;
//   unsigned long      ulStart = 0; 
//   unsigned long      ulEnd   = 0; 

//   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Range")) != NULL)
//      CFG_RangeNode(ptyNode, &ulStart, &ulEnd);

//   tySI.tyProcConfig.Processor._MIPS.ulStartOfOnChipEEPROM = ulStart;
//   tySI.tyProcConfig.Processor._MIPS.ulEndOfOnChipEEPROM   = ulEnd;
//}

/****************************************************************************
     Function: CFG_TLBNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
//static void CFG_TLBNode(TyXMLNode *ptyThisNode)
//{
//   tySI.tyProcConfig.Processor._MIPS.TLBType                   = (TyTLBType)CFG_Enum(XML_GetChildAttrib(ptyThisNode, "Type", "Type"));
//   tySI.tyProcConfig.Processor._MIPS.b1KAnd2KTLBPagesSupported = XML_GetChildState(ptyThisNode, "OneAndTwoKBTLBPages");
//   tySI.tyProcConfig.Processor._MIPS.ubNumTLBEntries           = (unsigned char)XML_GetChildElementHex(ptyThisNode, "NumberOfTLBEntries", 0);
//}

/****************************************************************************
     Function: CFG_PeripheralNode
     Engineer: Patrick Shiels
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
static void CFG_PeripheralNode(TyXMLNode *ptyThisNode)
{
   // currently not used by MIPS
   ptyThisNode = ptyThisNode;
}

/****************************************************************************
     Function: CFG_FamilyGroup
     Engineer: Patrick Shiels
        Input: *pszName    : Family group name or NULL
       Output: TyProcFamGrp : corresponding enum
  Description: 
Date           Initials    Description
03-Dec-2005    PJS         Initial
*****************************************************************************/
/*static TyProcFamGrp CFG_FamilyGroup(char * pszName)
{
   if (pszName == NULL)
      return UndefinedGroup;
   if (strcasecmp(pszName, "UndefinedGroup") == 0)
      return UndefinedGroup;
   if (strcasecmp(pszName, "_8xC190") == 0)
      return _8xC190;
   if (strcasecmp(pszName, "_8xC51MX_Grp") == 0)
      return _8xC51MX_Grp;
   if (strcasecmp(pszName, "_8xC55x") == 0)
      return _8xC55x;
   if (strcasecmp(pszName, "_8xC66x") == 0)
      return _8xC66x;
   if (strcasecmp(pszName, "Calva") == 0)
      return Calva;
   if (strcasecmp(pszName, "HiPerSmart") == 0)
      return HiPerSmart;
   if (strcasecmp(pszName, "Mifare") == 0)
      return Mifare;
   if (strcasecmp(pszName, "P8RF5000") == 0)
      return P8RF5000;
   if (strcasecmp(pszName, "P8WE5000") == 0)
      return P8WE5000;
   if (strcasecmp(pszName, "Painter1") == 0)
      return Painter1;
   if (strcasecmp(pszName, "Painter2") == 0)
      return Painter2;
   if (strcasecmp(pszName, "PCD509X") == 0)
      return PCD509X;
   if (strcasecmp(pszName, "PCD509XY") == 0)
      return PCD509XY;
   if (strcasecmp(pszName, "PNX") == 0)
      return PNX;
   if (strcasecmp(pszName, "SmartMX") == 0)
      return SmartMX;
   if (strcasecmp(pszName, "SmartXA1") == 0)
      return SmartXA1;
   if (strcasecmp(pszName, "SmartXA2") == 0)
      return SmartXA2;
   if (strcasecmp(pszName, "Tamaemu") == 0)
      return Tamaemu;
   if (strcasecmp(pszName, "NoDebugCore") == 0)
      return NoDebugCore;

   return UndefinedGroup;
}
*/

/****************************************************************************
     Function: CFG_ProcessRegFile
     Engineer: Patrick Shiels
        Input: none
       Output: int
  Description: Process the register file
Date           Initials    Description
03-Dec-2005    PJS         Initial
14-Mar-2006    PJS         Save register info in an array
*****************************************************************************/
int CFG_ProcessRegFile()
{
   int         ErrRet        = NO_ERROR;
   FILE            *pFile        = NULL;
   int             iFullRegIndex = -1;
   int             iLineNumber   = 0;
   unsigned long           ulAddress     = 0;
   unsigned char           ubMemType     = 0;
   unsigned char           ubByteSize    = 4;
   unsigned char           ubBitOffset   = 0;
   unsigned char           ubBitSize     = 0;
   int            bValidReg     = FALSE;
   char            szLine[_MAX_PATH];
   char            szRegName[_MAX_PATH];
   int             iType = (int)REGDEFTYPE_NONE;
   char            szPrevGroupName[_MAX_PATH];
   int             iPrevGrpStartRegIndex = -1;
   int             iPrevGrpEndRegIndex = -1;
   
   if (tySI.tyProcConfig.szSymFile[0] == '\0')
      {
      sprintf(tySI.szError, "Device specific <RegisterFile..> entry not found in XML file.");
      return GDBSERV_CFG_ERROR;
      }

   // open in text mode so that CR+LF is converted to LF
   pFile = fopen(tySI.tyProcConfig.szSymFile, "rt");
   if (pFile == NULL)
      {
      sprintf(tySI.szError, "Cannot find device specific register definition file '%s'.", tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }

   // Have a default group name just in case some registers are defined wihtout having any
   // preceeding group 
   strcpy (szPrevGroupName,"Default");

   while (!feof(pFile))
      {
      if (fgets(szLine, sizeof(szLine), pFile) == NULL)
         break;

      iLineNumber++;

      if ((ErrRet = CFG_ValidRegDefLine(szLine,
                                        iLineNumber,
                                        &bValidReg,
                                        szRegName,
                                        &ulAddress,
                                        &ubMemType,
                                        &ubByteSize,
                                        &ubBitOffset,
                                        &ubBitSize,
                                        &iFullRegIndex,
                                        &iType)) != NO_ERROR)
         return ErrRet;

      if (bValidReg)
         {
         if ((int)REGDEFTYPE_GROUP == iType) 
            {
            if (-1 != iPrevGrpStartRegIndex && -1 != iPrevGrpEndRegIndex) 
               {
               if ((ErrRet = CFG_AddGroupDefArrayEntry(szPrevGroupName,
                                                       iPrevGrpStartRegIndex,
                                                       iPrevGrpEndRegIndex, 
                                                       iType)) != NO_ERROR)
                  return ErrRet;
               }
               
            iPrevGrpStartRegIndex = -1;
            iPrevGrpEndRegIndex = -1;
            strcpy(szPrevGroupName, szRegName);

            }
         else 
            {
            if ((ErrRet = CFG_AddRegDefArrayEntry(szRegName,
                                                  ulAddress,
                                                  (TyMemTypes)ubMemType,
                                                  ubByteSize,
                                                  ubBitOffset,
                                                  ubBitSize,
                                                  iFullRegIndex,
                                                  iType)) != NO_ERROR)
               return ErrRet;

            if (iFullRegIndex < 0)
               {
               // have just added a full register, remember its index...
               iFullRegIndex = (int)tySI.uiRegDefArrayEntryCount -1;
               }

            iPrevGrpEndRegIndex = (int)tySI.uiRegDefArrayEntryCount -1;
            if (-1 == iPrevGrpStartRegIndex) 
               {
               iPrevGrpStartRegIndex = iPrevGrpEndRegIndex;
               }
            }
         }
      }

   // Add last group
   if (-1 != iPrevGrpStartRegIndex && -1 != iPrevGrpEndRegIndex) 
      {
      if ((ErrRet = CFG_AddGroupDefArrayEntry(szPrevGroupName,
                                              iPrevGrpStartRegIndex,
                                              iPrevGrpEndRegIndex, 
                                              iType)) != NO_ERROR)
         return ErrRet;
      }

   if (pFile != NULL)
      (void)fclose(pFile);

   return NO_ERROR;
}

/****************************************************************************
     Function: CFG_ValidRegDefLine
     Engineer: Patrick Shiels
        Input: *pszLine        : one line from register definition file
               iLineNumber     : line number of this line
               *pbValidReg     : storage for flag if valid reg/bit definition
               *pszRegName     : storage for register/bit name
               *pulAddress     : storage for address
               *pubMemType     : storage for memory type: reg, physical, etc
               *pubByteSize    : storage for size of containing reg in bytes (1, 2 or 4)
               *pubBitOffset   : storage for offset in bits (0..31)
               *pubBitSize     : storage for size in bits (1..32)
               *piFullRegIndex : storage for index to containing reg
       Output: int
  Description: Test if register/bit definition is valid and return details
Date           Initials    Description
25-Mar-2006    PJS         Initial
29-Apr-2008    JG          Added support to read names having space enclosed in ""
                           Also added support for returning type
*****************************************************************************/
static int CFG_ValidRegDefLine(char          *pszLine,
                               int           iLineNumber,
                               int           *pbValidReg,
                               char          *pszRegName,
                               unsigned long *pulAddress,
                               unsigned char *pubMemType,
                               unsigned char *pubByteSize,
                               unsigned char *pubBitOffset,
                               unsigned char *pubBitSize,
                               int           *piFullRegIndex,
                               int           *piType)
{
   unsigned long          ulAddress, ulSize, ulOffset;
   TyMemTypes   tyMemType;
   TyRegDef      *ptyFullRegDef;
   int            iValidArgs;
   int            iIndex;
   char          *psz;
   int           bValidNum = FALSE;
   char           szLine   [_MAX_PATH];
   char           szRegName[_MAX_PATH];
   char           szType   [_MAX_PATH];
   char           szMemory [_MAX_PATH];
   char           szAddress[_MAX_PATH];
   char           szSize   [_MAX_PATH];
   char           szOffset [_MAX_PATH];

   *pbValidReg = FALSE;

   if (pszLine == NULL)
      return NO_ERROR;

   strncpy(szLine, pszLine, sizeof(szLine)-1);
   szLine[sizeof(szLine)-1] = '\0';

   szRegName[0] = '\0';
   szType   [0] = '\0';
   szMemory [0] = '\0';
   szAddress[0] = '\0';
   szSize   [0] = '\0';
   szOffset [0] = '\0';

   // parse the hard way, wish to preserve case of register name...

   // find first non space character ...
   for (psz=szLine; (psz[0] != '\0') && IsSpace(psz[0]); psz++)
      {
      // keep lint happy with a block!
      }
   if (psz[0] == '\0')
      return NO_ERROR;

   // definition must must start with predef
//   if (_strnicmp(psz, "predef", 6) != 0)
   if (strncasecmp(psz, "predef", 6) != 0)
      return NO_ERROR;
   psz+=6;

   // find next non space character (start of register name) ...
   for (; (psz[0] != '\0') && IsSpace(psz[0]); psz++)
      {
      // keep lint happy with a block!
      }
   // Handle names with space too
   //if ((psz[0] == '\0') || (psz[0] == '"'))
   if (psz[0] == '\0') 
      return NO_ERROR;

   // collect register name preserving case...
   iIndex = 0;
   // If the name starts with a " then treat all chars till the ending " as name
   if (psz[0] == '"') 
      {
      // Skip first "
      psz++;
      for (; (psz[0] != '\0') && psz[0] != '"'; psz++)
         {
         if (iIndex < (int)(sizeof(szRegName)-1))
            szRegName[iIndex++] = psz[0];
         }
      // Skip ending "
      if (psz[0] == '"') 
         {
         psz++;
         }
      }
   else 
      {
      for (; (psz[0] != '\0') && !IsSpace(psz[0]); psz++)
         {
         if (iIndex < (int)(sizeof(szRegName)-1))
            szRegName[iIndex++] = psz[0];
         }
      }
   szRegName[iIndex] = '\0';


   // now convert to lowercase and let sscanf parse the rest...  
   StringToLwr(psz);

   iValidArgs = sscanf(psz, " type = %s mem = %s addr = %s size = %s offset = %s ",
                       szType, szMemory, szAddress, szSize, szOffset);

   if (iValidArgs < 1)
      return NO_ERROR;

   // from here on, for register or bit type, enforce strict format...
   if (strcasecmp(szType, "register") == 0)
      {
      if (iValidArgs != 4)
         goto BadRegFileLine;
      *piType = (int)REGDEFTYPE_REGSITER;
      }
   else if (strcasecmp(szType, "bit") == 0)
      {
      if (iValidArgs != 5)
         goto BadRegFileLine;
      *piType = (int)REGDEFTYPE_BIT;
      }
   else if (strcasecmp(szType, "group") == 0)
      {
      if (iValidArgs != 1)
         goto BadRegFileLine;
      *piType = (int)REGDEFTYPE_GROUP;
      }
   else
      {
      return NO_ERROR;
      }

   if( *piType != (int)REGDEFTYPE_GROUP )
      {
      if ((tyMemType = StringToMemType(szMemory)) == MEM_INVALID)
         goto BadRegFileLine;

      ulAddress = ExStrToUlong(szAddress, &bValidNum);
      if (!bValidNum)
         goto BadRegFileLine;

      ulSize    = ExStrToUlong(szSize,    &bValidNum);
      if (!bValidNum)
         goto BadRegFileLine;

      if (*piType == (int)REGDEFTYPE_BIT)
         {
         // reject if there is no containing full reg...
         if ((ptyFullRegDef = GetRegDefForIndex(*piFullRegIndex)) == NULL)
            goto BadRegFileLine;
//printf("17\n");
//printf("Index:%08lX\n",*piFullRegIndex);
//printf("Address:%08lX\n",(unsigned long)&tySI.ptyRegDefArray[*piFullRegIndex]);
//printf("Address:%08lX\n",(unsigned long)ptyFullRegDef);
//printf("%08lX\t%08lX\n",ulAddress,ptyFullRegDef->ulAddress);
         // reject if bitfield address is not equal to containing full reg address...
         if (ulAddress != ptyFullRegDef->ulAddress)
            goto BadRegFileLine;

         // reject if bitfield size is larger than containing full reg...
         if ((ulSize == 0) || (ulSize > (unsigned long)ptyFullRegDef->ubBitSize))
            goto BadRegFileLine;

         ulOffset = ExStrToUlong(szOffset,  &bValidNum);
         if (!bValidNum || ((ulOffset + ulSize) > (unsigned long)ptyFullRegDef->ubBitSize))
            goto BadRegFileLine;

         // *pubByteSize and *piFullRegIndex remain unchanged, i.e. the size in
         // bytes and array index of the register that contains this bit field.
         *pubBitSize   = (unsigned char)ulSize;
         *pubBitOffset = (unsigned char)ulOffset;
         }
      else     // register
         {
         if ((ulSize != 1) && (ulSize != 2) && (ulSize != 4))
            goto BadRegFileLine;

         *pubByteSize    = (unsigned char)ulSize;
         *pubBitSize     = (unsigned char)(ulSize *8);
         *pubBitOffset   = 0;
         *piFullRegIndex = -1;
         }

      *pulAddress = ulAddress;
      *pubMemType = (unsigned char)tyMemType;
   }

   strcpy(pszRegName, szRegName);
   *pbValidReg = TRUE;
   return NO_ERROR;

BadRegFileLine:
//printf("22\n");
   sprintf(tySI.szError, "Invalid register definition at line %d in device specific register definition file '%s'.", iLineNumber, tySI.tyProcConfig.szSymFile);
   return GDBSERV_CFG_ERROR;
}

/****************************************************************************
     Function: CFG_AddRegDefArrayEntry
     Engineer: Patrick Shiels
        Input: *pszRegName   : register/bit name
               ulAddress     : address
               ubMemType     : memory type: reg, physical, etc
               ubByteSize    : size of containing reg in bytes (1, 2 or 4)
               ubBitOffset   : offset in bits (0..31)
               ubBitSize     : size in bits (1..32)
               iFullRegIndex : index to containing reg
       Output: int
  Description: Add a register definition to the array
Date           Initials    Description
15-Mar-2006    PJS         Initial
*****************************************************************************/
/*int CFG_AddRegDefArrayEntry(char           *pszRegName,
                                unsigned long           ulAddress,
                                unsigned char           ubMemType,
                                unsigned char           ubByteSize,
                                unsigned char           ubBitOffset,
                                unsigned char           ubBitSize,
                                int             iFullRegIndex)
{
   int         iRegNameLen;
   TyRegDef   *ptyNewRegDefArray;

   if (tySI.uiRegDefArrayEntryCount >= tySI.uiRegDefArrayAllocCount)
      {
      // reallocate array adding a chunk of entries...
      ptyNewRegDefArray = (TyRegDef*)realloc(tySI.ptyRegDefArray, ((tySI.uiRegDefArrayAllocCount+GDBSERV_REGDEF_ALLOC_CHUNK) * sizeof(TyRegDef)));
      if (ptyNewRegDefArray == NULL)
         {
         sprintf(tySI.szError, "Insufficient memory while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
         return GDBSERV_CFG_ERROR;
         }

      // clear new chunk to 0...
      memset(&ptyNewRegDefArray[tySI.uiRegDefArrayAllocCount], 0, (GDBSERV_REGDEF_ALLOC_CHUNK * sizeof(TyRegDef)));

      tySI.ptyRegDefArray = ptyNewRegDefArray;
      tySI.uiRegDefArrayAllocCount += GDBSERV_REGDEF_ALLOC_CHUNK;
      }

   // sanity check, this should not happen...
   if (tySI.uiRegDefArrayEntryCount >= tySI.uiRegDefArrayAllocCount)
      {
      sprintf(tySI.szError, "Internal error 1 while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }

   iRegNameLen = (int)strlen(pszRegName);

   // allocate storage for the new register name...
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName = malloc(iRegNameLen+1);
   if (tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName == NULL)
      {
      sprintf(tySI.szError, "Insufficient memory while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }

   // copy the new register name...
   strncpy(tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName, pszRegName, iRegNameLen);
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].pszRegName[iRegNameLen] = '\0';

   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ulAddress     = ulAddress;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubMemType     = ubMemType;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubByteSize    = ubByteSize;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubBitOffset   = ubBitOffset;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].ubBitSize     = ubBitSize;
   tySI.ptyRegDefArray[tySI.uiRegDefArrayEntryCount].iFullRegIndex = iFullRegIndex;

   tySI.uiRegDefArrayEntryCount++;

   return NO_ERROR;
}*/

/****************************************************************************
     Function: CFG_AddGroupDefArrayEntry
     Engineer: Jiju George
        Input: *pszGroupName   : group name
               iStartRegIndex  : index of first register in this group
               iEndRegIndex    : index of first last in this group
       Output: int
  Description: Add a group definition to the array
Date           Initials    Description
26-Apr-2008    JG         Initial
*****************************************************************************/
int CFG_AddGroupDefArrayEntry(char           *pszGroupName,
                                  int             iStartRegIndex,
                                  int             iEndRegIndex, 
                                  int             iRegDefType)
{
   unsigned int uiGroupNameLen;
   TyGroupDef   *ptyNewGroupDefArray;

   if (tySI.uiGroupDefArrayEntryCount >= tySI.uiGroupDefArrayAllocCount)
      {
      // reallocate array adding a chunk of entries...
      ptyNewGroupDefArray = (TyGroupDef*)realloc(tySI.ptyGroupDefArray, ((tySI.uiGroupDefArrayAllocCount+GDBSERV_REGDEF_ALLOC_CHUNK) * sizeof(TyGroupDef)));
      if (ptyNewGroupDefArray == NULL)
         {
         sprintf(tySI.szError, "Insufficient memory while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
         return GDBSERV_CFG_ERROR;
         }

      // clear new chunk to 0...
      memset(&ptyNewGroupDefArray[tySI.uiGroupDefArrayAllocCount], 0, (GDBSERV_REGDEF_ALLOC_CHUNK * sizeof(TyGroupDef)));

      tySI.ptyGroupDefArray = ptyNewGroupDefArray;
      tySI.uiGroupDefArrayAllocCount += GDBSERV_REGDEF_ALLOC_CHUNK;
      }

   // sanity check, this should not happen...
   if (tySI.uiGroupDefArrayEntryCount >= tySI.uiGroupDefArrayAllocCount)
      {
      sprintf(tySI.szError, "Internal error 1 while processing register definition file '%s'.", tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }

   uiGroupNameLen = (unsigned int)strlen(pszGroupName);

   // copy the new register name...
   strncpy(tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].pszGroupName, pszGroupName, uiGroupNameLen);
   tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].pszGroupName[uiGroupNameLen] = '\0';

   tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].iStartRegIndex  = iStartRegIndex;
   tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].iEndRegIndex    = iEndRegIndex;
   tySI.ptyGroupDefArray[tySI.uiGroupDefArrayEntryCount].iRegDefType    = iRegDefType;

   tySI.uiGroupDefArrayEntryCount++;

   return NO_ERROR;
}
/****************************************************************************
     Function: CFG_ScanChainNode
     Engineer: Amala Nair G S
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: Parse the xml file created based on the multicore selection in MIPS
               (mixedcores_mips.xml)
Date           Initials    Description
05-Aug-2011    ANGS        Initial
*****************************************************************************/
void CFG_ScanChainNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode      *ptyNode;
   unsigned long  ulIndex=0;
   char *pszState = NULL;
   // Process child nodes
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "Core")) != NULL)
      {
      do
         {   
         ulIndex = XML_GetDec(XML_GetAttrib(ptyNode,"Number"),0);

         if (ulIndex > MAX_CORES_IN_CHAIN)
            break;

         pszState = XML_GetAttrib(ptyNode,"IsDMACore");
         /* If IsDMACore information is not provided in the scan-file, 
         it will be treated as 'Not a DMA core' by default */
         if(pszState != NULL)
             {
             if (strcasecmp(pszState, "true") == 0)
                {
                tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[ulIndex].bIsDMACore = 1; //lint !e661
                tySI.tyProcConfig.Processor._MIPS.uiDMAcore = ulIndex+1;

                }
             else
                {
                tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[ulIndex].bIsDMACore = 0; //lint !e661
                }
             }
         else
             {
             tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[ulIndex].bIsDMACore = 0; //lint !e661
             }

         tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[ulIndex].ulIRlength =    
            XML_GetChildElementDec(ptyNode,"IRlength",0);   //lint !e661

         tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[ulIndex].ulBypassInst =  
            XML_GetChildElementHex(ptyNode,"BypassCode",0); //lint !e661

         }while((ptyNode=XML_GetNextNode(ptyNode)) != NULL);
      }
   tySI.tyProcConfig.Processor._MIPS.uiNumberofCores = (ulIndex+1);      
}
/****************************************************************************
     Function: CFG_TraceNode
     Engineer: Amala Nair G S
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
19-Oct-2011    ANGS        Initial
*****************************************************************************/
static void CFG_TraceNode(TyXMLNode *ptyThisNode)
{
   TyXMLNode  *ptyNode;
   if ((ptyNode = XML_GetChildNode(ptyThisNode, "MIPSTrace")) != NULL)
      CFG_MIPSTraceNode(ptyNode);
}
/****************************************************************************
     Function: CFG_MIPSTraceNode
     Engineer: Amala Nair G S
        Input: *ptyThisNode : pointer to XML node structure for this node
       Output: none
  Description: 
Date           Initials    Description
19-Oct-2011    ANGS        Initial
*****************************************************************************/
static void CFG_MIPSTraceNode(TyXMLNode *ptyThisNode)
{
   tySI.tyProcConfig.Processor._MIPS.bZephyrTraceSupport = XML_GetChildState(ptyThisNode, "ZephyrTraceSupport");
}
