/****************************************************************************
       Module: gdbmips.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, MIPS target interface
Date           Initials    Description
08-Feb-2008    VH          Initial
08-AUG-2009    DK          Modified to support initialisation of TLB
02-May-2010    VK          Added MDI command-Microchp
18-May-2010    SPC         Added Read/Write watchpoint support in GDBServer
23-Jun-2010		RSB			Advanced Breakpoint support
28-Sep-2011    ANGS        Modified to make the multi-core details for MIPS 
                           reflect in AshMDI.cfg file
19-Oct-2011    ANGS        Modified for Zephyr trace support
30-Nov-2011    SV          Modified for Zephyr Trace Configuration
****************************************************************************/
#ifdef __LINUX
   #include <dlfcn.h>
#else 
   #include <windows.h>
   #include <conio.h>
   #include <windef.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdbcfg.h"
#include "gdbserver/gdberr.h"
#include "gdbserver/mips/gdbmips.h"
#include "protocol/mips/mdi.h"
#include "protocol/mips/mips.h"
#include "protocol/mips/commdefs.h"

#if _WINCONS
   #define  OPELLAXD_DLL_NAME                "opxdmips.dll"
   #define  OPELLAUSB_DLL_NAME               "gdbopelmips.dll"
#elif _LINUX
   #define  OPELLAXD_DLL_NAME                "./opxdmips.so"
   #define  OPELLAUSB_DLL_NAME               "./gdbopelmips.so"
   #define  LoadLibrary(lib)                 dlopen(lib, RTLD_LAZY)
   #define  GetProcAddress(handle, proc)     dlsym(handle, proc)
   #define  FreeLibrary(handle)              dlclose(handle)
typedef void* HINSTANCE;
#endif

//#define BAD_HEX_VALUE               0xBAD0BAD0
#define BAD_HEX_VALUE               0x0

#define IDERR_LOAD_DLL_FAILED       1
#define IDERR_DLL_NOT_LOADED        2
#define IDERR_GDI_ERROR             3
#define IDERR_MDI_PARAM             4
#define IDERR_MDI_VERSION           5
#define IDERR_MDI_INTERNAL          6
#define IDERR_MDI_TGHANDLE          7
#define IDERR_MDI_HANDLE            8
#define IDERR_MDI_DEVICE            9
#define IDERR_MDI_SRCOFFSET         10
#define IDERR_MDI_SRCRESOURCE       11
#define IDERR_MDI_DSTOFFSET         12
#define IDERR_MDI_DSTRESOURCE       13
#define IDERR_MDI_RANGE             14
#define IDERR_ASH_SRCREGISTER       15
#define IDERR_ASH_DSTREGISTER       16
#define IDERR_ADD_BP_NO_MEMORY      17
#define IDERR_ADD_BP_INTERNAL1      18
#define IDERR_ADD_BP_DUPLICATE      19
#define IDERR_REMOVE_BP_INTERNAL1   20
#define IDERR_REMOVE_BP_INTERNAL2   21
#define IDERR_REMOVE_BP_NOT_FOUND   22
#define IDERR_CREATE_CONFIG_FILE    23
#define IDERR_ZEPHYR_TRACE_ERROR    24

typedef int           (*TyAshConfigureTrace)    (unsigned long ulTC, unsigned long ulTC2, unsigned long ulTC3);
typedef int           (*TyAshTraceElfPath)      (char* pszElfPath);
typedef int           (*TyEnableOSMMU)          (TyOS os ,unsigned long *pulPgdirAddrs);
typedef int           (*TyAshInitOSAppMMU)      (unsigned long ulAppCon, unsigned long ulPgDirAddrs);
typedef int           (*TyAshFetchTrace)        (unsigned long ulBegPtr, unsigned long ulEndPtr);
typedef int           (*TyMDIConnect)           (MDIVersionT, MDIHandleT *, MDIConfigT *);               
typedef int           (*TyMDIDisconnect)        (MDIHandleT, MDIUint32); 
typedef int           (*TyMDICacheFlush)        (MDIHandleT, MDIUint32, MDIUint32);
typedef int           (*TyMDIOpen)              (MDIHandleT, MDIDeviceIdT, MDIUint32, MDIHandleT *);        
typedef int           (*TyMDIClose)             (MDIHandleT, MDIUint32);                                   
typedef int           (*TyMDIRead)              (MDIHandleT, MDIResourceT SrcR, MDIOffsetT SrcO,void * Buffer, MDIUint32 ObjectSize, MDIUint32 Count);          
typedef int           (*TyMDIWrite)             (MDIHandleT, MDIResourceT DstR, MDIOffsetT DstO,void * Buffer, MDIUint32 ObjectSize, MDIUint32 Count);         
typedef int           (*TyMDIExecute)           (MDIHandleT);                                            
typedef int           (*TyMDIStep)              (MDIHandleT, MDIUint32 Steps, MDIUint32 Mode);              
typedef int           (*TyMDIStop)              (MDIHandleT);                                               
typedef int           (*TyMDIReset)             (MDIHandleT, MDIUint32 Flag);                              
typedef int           (*TyMDIRunState)          (MDIHandleT, MDIInt32 WaitTime, MDIRunStateT *runstate);
typedef int           (*TyMDISetBp)             (MDIHandleT, MDIBpDataT *);                                
typedef int           (*TyMDIClearBp)           (MDIHandleT, MDIBpIdT);

typedef int           (*TyMDIEraseChipMircochipFlash)  (TyMicrochipStatusRegister *ptyMicrochipStatusRegister);
typedef int           (*TyMDISetupMrchpFlash)   (TyMicrochipStatusRegister *ptyMicrochipStatusRegister, int bClearCodeProtection);
typedef int           (*TyMDISaveMrchpBmxRegs)  (TyMicrochipBmxRegContext *tyMicrochipBmxRegContext);
typedef int           (*TyMDIPartitionMrchpRam) (unsigned long ulDataKernelSpace);

typedef unsigned long (*TyAshErrorGetMessage)   (char ** ppszErrorMsg);
typedef unsigned long (*TyAshGetDWFWVersions)   (char *pszDiskware1Version, 
                                                 char *pszFirmwareVersion, 
                                                 char *pszFpgaware1Version, 
                                                 char *pszFpgaware2Version, 
                                                 char *pszFpgaware3Version, 
                                                 char *pszFpgaware4Version);
typedef void          (*TyAshScanIR) (unsigned long *pulDataOut, 
                                      unsigned long *pulDataIn, 
                                      unsigned long ulTargetIRLength);
typedef void          (*TyAshScanDR) (unsigned long *pulDataOut, 
                                      unsigned long *pulDataIn, 
                                      unsigned long ulTargetDRLength);
typedef void          (*TyAshScanDRLong)        (ulong *pulDataOut, ulong *pulDataIn, ulong ulTargetDRLength);
typedef unsigned long         (*TyAshMDIOpenEx)         (MDIHandleT, MDIDeviceIdT, MDIUint32, MDIHandleT *, int);        
typedef unsigned long         (*TyAshSetJtagFrequency)  (double);   

TyAshConfigureTrace       pfAshConfigureTrace ; 
TyAshTraceElfPath         pfAshTraceElfPath   ;
TyEnableOSMMU             pfEnableOSMMU       ;
TyAshInitOSAppMMU         pfAshInitOSAppMMU   ;
TyAshFetchTrace           pfAshFetchTrace     ;
TyMDICacheFlush           pfMDICacheFlush     ;
TyMDIConnect              pfMDIConnect        ;
TyMDIDisconnect           pfMDIDisconnect     ;
TyMDIOpen                 pfMDIOpen           ;
TyMDIClose                pfMDIClose          ;
TyMDIRead                 pfMDIRead           ;
TyMDIWrite                pfMDIWrite          ;
TyMDIExecute              pfMDIExecute        ;
TyMDIStep                 pfMDIStep           ;
TyMDIStop                 pfMDIStop           ;
TyMDIReset                pfMDIReset          ;
TyMDIRunState             pfMDIRunState       ;
TyMDISetBp                pfMDISetBp          ;
TyMDIClearBp              pfMDIClearBp        ;
TyMDIEraseChipMircochipFlash pfMDIEraseMrchpFlash ;
TyMDISetupMrchpFlash      pfMDISetupMrchpFlash    ;
TyMDISaveMrchpBmxRegs     pfMDISaveMrchpBmxRegs   ;
TyMDIPartitionMrchpRam    pfMDIPartitionMrchpRam  ;

TyAshErrorGetMessage      pfAshErrorGetMessage;
TyAshGetDWFWVersions      pfAshGetDWFWVersions;
TyAshScanIR               pfAshScanIR;
TyAshScanDR               pfAshScanDR;
TyAshScanDRLong           pfAshScanDRLong;
TyAshMDIOpenEx            pfAshMDIOpenEx;
TyAshSetJtagFrequency     pfAshSetJtagFrequency;

static HINSTANCE              hMipsInstMdiDll      = NULL;
static MDIHandleT             tyCurrentMDIHandle   = 0;
static MDIHandleT             tyCurrentDevHndl     = 0;
static char                   szTargetDllName[_MAX_PATH] = {0};
static char                   szMdiConfigName[_MAX_PATH] = {0};


#define     AshConfigureTrace_Export_Name    "AshConfigureTrace"
#define     AshTraceElfPath_Export_Name      "AshTraceElfPath"
#define     OSMMU_Export_Name                "AshInitOSMMU"
#define     AshOSApp_Export_Name             "AshInitOSAppMMU"
#define     AshFetchTrace_Export_Name        "AshFetchTrace"
#define     MDICacheFlush_Export_Name        "MDICacheFlush"
#define     MDIClearBp_Export_Name           "MDIClearBp"     
#define     MDIClose_Export_Name             "MDIClose"       
#define     MDIConnect_Export_Name           "MDIConnect"    
#define     MDIDisconnect_Export_Name        "MDIDisconnect"  
#define     MDIExecute_Export_Name           "MDIExecute"     
#define     MDIOpen_Export_Name              "MDIOpen"       
#define     MDIRead_Export_Name              "MDIRead"       
#define     MDIReset_Export_Name             "MDIReset"       
#define     MDIRunState_Export_Name          "MDIRunState"   
#define     MDISetBp_Export_Name             "MDISetBp"       
#define     MDIStep_Export_Name              "MDIStep"      
#define     MDIStop_Export_Name              "MDIStop"        
//#define     MDIVersion_Export_Name           "MDIVersion"
#define     MDIWrite_Export_Name             "MDIWrite"        
#define     AshErrorGetMessage_Export_Name   "AshErrorGetMessage"
#define     AshGetDWFWVersions_Export_Name   "AshGetDWFWVersions"
#define     MDIEraseMrchpFlash_Export_Name    "MLN_EraseChipMicrochipFlash"
#define     MDISetupMrchpFlash_Export_Name   "MLN_SetupMicrochipFlash"
#define     MDISaveMrchpBmxReg_Export_Name   "MLN_SaveMicrochipBmxRegisters"
#define     MDIPartitionMrchpRam_Export_Name "MLN_PartitionRamForMicrochipFlashSetup"

#define COMMON_BUFFER_SIZE 0x10000
static unsigned char  pucCommonBuffer[COMMON_BUFFER_SIZE];
static TyMicrochipBmxRegContext  tyMicrochipBmxRegContext = {0x0,0x0,0x0,0x0,FALSE};
TyMicrochipStatusRegister tyMicrochipStatusRegister;

// breakpoint array definitions
#define  GDBSERV_BP_ARRAY_ALLOC_CHUNK    64

typedef struct
   {
   unsigned long ulBpType;
   unsigned long ulAddress;
   unsigned long ulLength;
   unsigned long ulIdentifier;
   } TyBpElement;

typedef  struct TyAdvBpArray
   {
   //MDIBpDataT tyAdvBpInfo;
   unsigned long ulAddress;
   struct TyAdvBpArray *pNext;
   }TyAdvanceBpArray;


static TyBpElement  *ptyBpArray = NULL;
static TyAdvanceBpArray *ptyAdvanceBpArray = NULL;


static unsigned int uiBpCount   = 0;
static unsigned int uiBpAlloc   = 0;


typedef struct
   {
   const char   * pszRegName;    // register name
   int     bCoreReg;      // determines meaning of ulAddrIndex
   unsigned long    ulAddrIndex;   // a register address if bCoreReg, else regdef index
   } TyMipsReg;

typedef struct
   {
   const char   *pszRegName;    // register name
   unsigned int uiRegIndex;
   unsigned long ulAddr;   // a register address if bCoreReg, else regdef index
   } TyMipsExtraReg;

// Register positions here are as observed with SDE-GDB for MIPS.
TyMipsReg ptyMipsReg[] = {
   {"ZERO",       TRUE,    REG_MIPS_ZERO}, //0
   {"AT",         TRUE,    REG_MIPS_AT},
   {"V0",         TRUE,    REG_MIPS_V0},
   {"V1",         TRUE,    REG_MIPS_V1},
   {"A0",         TRUE,    REG_MIPS_A0},
   {"A1",         TRUE,    REG_MIPS_A1},
   {"A2",         TRUE,    REG_MIPS_A2},
   {"A3",         TRUE,    REG_MIPS_A3},
   {"T0",         TRUE,    REG_MIPS_T0},
   {"T1",         TRUE,    REG_MIPS_T1},
   {"T2",         TRUE,    REG_MIPS_T2}, //10
   {"T3",         TRUE,    REG_MIPS_T3},
   {"T4",         TRUE,    REG_MIPS_T4},
   {"T5",         TRUE,    REG_MIPS_T5},
   {"T6",         TRUE,    REG_MIPS_T6},
   {"T7",         TRUE,    REG_MIPS_T7},
   {"S0",         TRUE,    REG_MIPS_S0},
   {"S1",         TRUE,    REG_MIPS_S1},
   {"S2",         TRUE,    REG_MIPS_S2},
   {"S3",         TRUE,    REG_MIPS_S3},
   {"S4",         TRUE,    REG_MIPS_S4}, //20
   {"S5",         TRUE,    REG_MIPS_S5},
   {"S6",         TRUE,    REG_MIPS_S6},
   {"S7",         TRUE,    REG_MIPS_S7},
   {"T8",         TRUE,    REG_MIPS_T8},
   {"T9",         TRUE,    REG_MIPS_T9},
   {"K0",         TRUE,    REG_MIPS_K0},
   {"K1",         TRUE,    REG_MIPS_K1},
   {"GP",         TRUE,    REG_MIPS_GP},
   {"SP",         TRUE,    REG_MIPS_SP},
   {"S8",         TRUE,    REG_MIPS_FP}, //30
   {"RA",         TRUE,    REG_MIPS_RA},
   {"STATUS",     TRUE,    0x40A0000C}, //CP0 Status register
   {"LO",         TRUE,    REG_MIPS_LO},
   {"HI",         TRUE,    REG_MIPS_HI},
   {"BADVADDR",   TRUE,    0x40A00008}, //CP0 BadVAddr register
   {"CAUSE",      TRUE,    0x40A0000D}, //CP0 Cause register
   {"PC",         TRUE,    REG_MIPS_PC} //37
};

/* The MIPS Extra registors asked by GDB */
/* TODO: please update if nessessory in order of uiRegIndex */
TyMipsExtraReg ptyMipsExtraReg[] = {
   {"Index",     0x4a, 0x40A00000}, // CP0 Register 0, Select 0
   {"Random",    0x4b, 0x40A00001}, // CP0 Register 1, Select 0
   {"EntryLo0",  0x4c, 0x40A00002}, // CP0 Register 2, Select 0
   {"EntryLo1",  0x4d, 0x40A00003}, // CP0 Register 3, Select 0
   {"Context",   0x4e, 0x40A00004}, // CP0 Register 4, Select 0
   {"PageMask",  0x4f, 0x40A00005}, // CP0 Register 5, Select 0
   {"Wired",     0x50, 0x40A00006}, // CP0 Register 6, Select 0
   {"HWREna",    0x51, 0x40A00007}, // CP0 Register 7, Select 0
   {"Count",     0x53, 0x40A00009}, // CP0 Register 9, Select 0
   {"EntryHi",   0x54, 0x40A0000A}, // CP0 Register 10, Select 0
   {"Compare",   0x55, 0x40A0000B}, // CP0 Register 11, Select 0
   {"EPC",       0x58, 0x40A0000E}, // CP0 Register 14, Select 0
   {"PRId",      0x59, 0x40A0000F}, // CP0 Register 15, Select 0
   {"Config",    0x5A, 0x40A00010}, // CP0 Register 16, Select 0
   {"LLAddr",    0x5B, 0x40A00011}, // CP0 Register 17, Select 0
   {"WatchLo",   0x5C, 0x40A00012}, // CP0 Register 18, Select 0
   {"WatchHi",   0x5D, 0x40A00013}, // CP0 Register 19, Select 0
   {"Debug",     0x61, 0x40A00017}, // CP0 Register 23, Select 0
   {"DEPC",      0x62, 0x40A00018}, // CP0 Register 24, Select 0                                    
   {"PerfCnt0",  0x63, 0x40A00019}, // CP0 Register 25, Select 0
   {"ErrCtl",    0x64, 0x40A0001A}, // CP0 Register 26, Select 0
   {"CacheErr",  0x65, 0x40A0001B}, // CP0 Register 27, Select 0
   {"TagLo",     0x66, 0x40A0001C}, // CP0 Register 28, Select 0
   {"TagHi",     0x67, 0x40A0001D}, // CP0 Register 29, Select 0
   {"ErrorEPC",  0x68, 0x40A0001E}, // CP0 Register 30, Select 0
   {"DESAVE",    0x69, 0x40A0001F}, // CP0 Register 31, Select 0
   {"IntCtl",    0x76, 0x40A1000C}, // CP0 Register 12, Select 1
   {"EBase",     0x79, 0x40A1000F}, // CP0 Register 15, Select 1
   {"Config1",   0x7a, 0x40A10010}, // CP0 Register 16, Select 1                                     
   {"WatchLo1",  0x7c, 0x40A10012}, // CP0 Register 18, Select 1 
   {"WatchHi1",  0x7d, 0x40A10013}, // CP0 Register 19, Select 1 
   {"TraceCtrl", 0x81, 0x40A10017}, // CP0 Register 23, Select 1
   {"PerfCnt1",  0x83, 0x40A10019}, // CP0 Register 25, Select 1
   {"DataLo",    0x86, 0x40A1001C}, // CP0 Register 28, Select 1
   {"DataHi",    0x87, 0x40A1001D}, // CP0 Register 29, Select 1
   {"SRSCtl",    0x96, 0x40A2000C}, // CP0 Register 12, Select 2
   {"Config2",   0x9a, 0x40A20010}, // CP0 Register 16, Select 2
   {"WatchLo2",  0x9c, 0x40A20012}, // CP0 Register 18, Select 2
   {"WatchHi2",  0x9d, 0x40A20013}, // CP0 Register 19, Select 2
   {"TrcCtrl2",  0xa1, 0x40A20017}, // CP0 Register 23, Select 2
   {"PerfCnt2",  0xa3, 0x40A20019},
   {"TagLo1",    0xa6, 0x40A2001C},
   {"TagHi1",    0xa7, 0x40A2001D},
   {"SRSMap",    0xb6, 0x40A3000C}, // CP0 Register 12, Select 3
   {"Config3",   0xba, 0x40A30010}, // CP0 Register 16, Select 3
   {"WatchLo3",  0xbc, 0x40A30012}, // CP0 Register 18, Select 3
   {"WatchHi3",  0xbd, 0x40A30013}, // CP0 Register 19, Select 3
   {"UsrTrcDta1",0xc1, 0x40A30017}, // CP0 Register 23, Select 3
   {"PerfCnt3",  0xc3, 0x40A30019},
   {"DataLo1",   0xc6, 0x40A3001C},
   {"DataHi1",   0xc7, 0x40A3001D},
   {"WatchLo4",  0xdc, 0x40A40012}, // CP0 Register 18, Select 4
   {"WatchHi4",  0xdd, 0x40A40013}, // CP0 Register 19, Select 4
   {"TraceIBPC", 0xe1, 0x40A40017}, // CP0 Register 23, Select 4
   {"PerfCnt4",  0xe3, 0x40A40019},
   {"PerfCnt5",  0x103,0x40A50019},
   {"Config6",   0x11a,0x40A60010}, // CP0 Register 16, Select 6
   {"PerfCnt6",  0x123,0x40A60019},
   {"Config7",   0x13a,0x40A70010}, // CP0 Register 16, Select 7
   {"PerfCnt7",  0x143,0x40A70019},
};

#define  MAX_MIPS_REG  ((sizeof(ptyMipsReg))/(sizeof(TyMipsReg)))
#define  MAX_EXTRA_MIPS_REG  ((sizeof(ptyMipsExtraReg))/(sizeof(TyMipsExtraReg)))

// RegDef index of the EndianMode register bit (e.g. Config.BE)
static int  iRegDefEndianMode = -1;


// local function prototypes
#ifdef __LINUX
static MDIInt32 MDICBOutput(MDIHandleT Device, MDIInt32 Type,char *Buffer, MDIInt32 Count);
static MDIInt32 MDICBInput(MDIHandleT Device, MDIInt32 Type,MDIInt32 Mode, char **Buffer, MDIInt32 *Count);
#else
static MDIInt32 __stdcall MDICBOutput(MDIHandleT Device, MDIInt32 Type,char *Buffer, MDIInt32 Count);
static MDIInt32 __stdcall MDICBInput(MDIHandleT Device, MDIInt32 Type,MDIInt32 Mode, char **Buffer, MDIInt32 *Count);
#endif
static void    SetupErrorMessage(int tyError);
static void    IsBigEndian(void);
static int     GetRegDefForUserSpecificReg(TyRegDef         *ptyRegDef,
                                           TyUserSpecific    *ptyUserSpecReg);
static int     SetupRegisterSettings(void);
static int     MdiReadMem(TXRXMemAccessTypes   MemReadType,
                          unsigned long                adStartAddr,
                          unsigned long                adReadLen,
                          unsigned char                *pBuffer,
                          unsigned long                adObjectSize);
static int MdiWriteMem(TXRXMemAccessTypes MemWriteType,
                       unsigned long              adStartAddr,
                       unsigned long              adWriteLen,
                       unsigned char              *pBuffer,
                       unsigned long              adBufLen,
                       unsigned long              adObjectSize);

static int MdiLoadLibrary(void);
static int MdiFreeLibrary(void);

// use for JTAG console (allow to control initial JTAG access)
static int MdiLoadLibraryCon(int bInitialJtagAccess);
static int MdiFreeLibraryCon(void);

static int MdiResetTarget(TXRXResetTypes tyResetType);
static unsigned long   GetTxRegNumber(unsigned long ulGdbRegNumber, int *pbValid);
static const char  * GetRegName(unsigned long ulGdbRegNumber);
static const char  * GetBpTypeName(unsigned long ulBpType);
static int AddBpArrayEntry(unsigned long ulBpType,
                           unsigned long ulAddress,
                           unsigned long ulLength,
                           unsigned long ulIdentifier);
//static int AddAdvancedBpEntry(MDIBpDataT *ptyAdvanceBp);
static int RemoveBpArrayEntry(unsigned  int uiIndex, 
                              int iCurrentCoreIndex);
static void    RemoveAllBpArray(void);
static int     GetBpArrayEntry(unsigned long  ulBpType,
                               unsigned long  ulAddress,
                               unsigned long  ulLength,
                               unsigned long *pulIdentifier,
                               int iCurrentCoreIndex);
/*static int GetAdvBpEntry(unsigned long  ulAddress,
                           unsigned long *pulIdentifier,
                           int iCurrentCoreIndex);*/
static int GetBpArrayEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex);
/*static int GetAdvBpEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex);*/
static int ReadFullRegisterByRegDef(TyRegDef  *ptyRegDef,
                                    unsigned long     *pulRegValue);
static int WriteFullRegisterByRegDef(TyRegDef  *ptyRegDef,
                                     unsigned long     ulRegValue);

//For PFXD
static int MdiLoadLibraryPFXD(void);
static int MdiConnectPFXD(void);
extern TyServerInfo tySI;

/****************************************************************************
     Function: MDICBOutput
     Engineer: Nikolay Chokoev
        Input: none
       Output: MDIInt32
  Description: MDI callback which we do not use
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
#ifdef __LINUX
static MDIInt32 MDICBOutput(MDIHandleT Device, MDIInt32 Type,char *Buffer, MDIInt32 Count )
#else
static MDIInt32 __stdcall MDICBOutput(MDIHandleT Device, MDIInt32 Type,char *Buffer, MDIInt32 Count )
#endif
{
   NOREF(Device);
   NOREF(Type);
   NOREF(Buffer);
   NOREF(Count);

   assert(FALSE);
   return MDISuccess;//lint !e527
}

/****************************************************************************
     Function: MDICBInput
     Engineer: Nikolay Chokoev
        Input: none
       Output: MDIInt32
  Description: MDI callback which we do not use
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
#ifdef __LINUX
static MDIInt32 MDICBInput(MDIHandleT Device, MDIInt32 Type,MDIInt32 Mode, char **Buffer,MDIInt32 *Count)
#else
static MDIInt32 __stdcall MDICBInput(MDIHandleT Device, MDIInt32 Type,MDIInt32 Mode, char **Buffer,MDIInt32 *Count)
#endif
{
   NOREF(Device);
   NOREF(Type);
   NOREF(Mode);
   NOREF(Buffer);
   NOREF(Count);

   assert(FALSE);
   return MDISuccess;//lint !e527
}

/****************************************************************************
     Function: SetupErrorMessage
     Engineer: Nikolay Chokoev
        Input: iError - error code
       Output: none
  Description: step
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static void SetupErrorMessage(int iError)
{
   switch(iError)
      {
      case IDERR_LOAD_DLL_FAILED:
         PrintMessage(ERROR_MESSAGE,  "Failed to load target library %s. File not found or is corrupt.", szTargetDllName);
         break;
      case IDERR_DLL_NOT_LOADED:
         PrintMessage(ERROR_MESSAGE,  "Internal server error, target library %s not loaded.", szTargetDllName);
         break;
      case IDERR_ADD_BP_NO_MEMORY:
         PrintMessage(ERROR_MESSAGE,  "Insufficient memory while setting a breakpoint.");
         break;
      case IDERR_ADD_BP_INTERNAL1:
         PrintMessage(ERROR_MESSAGE,  "Internal error 1 occurred while setting a breakpoint.");
         break;
      case IDERR_ADD_BP_DUPLICATE:
         PrintMessage(ERROR_MESSAGE,  "Duplicate breakpoint found at address while setting a breakpoint.");
         break;
      case IDERR_REMOVE_BP_INTERNAL1:
         PrintMessage(ERROR_MESSAGE,  "Internal error 1 occurred while deleting a breakpoint.");
         break;
      case IDERR_REMOVE_BP_INTERNAL2:
         PrintMessage(ERROR_MESSAGE,  "Internal error 2 occurred while deleting a breakpoint.");
         break;
      case IDERR_REMOVE_BP_NOT_FOUND:
         PrintMessage(ERROR_MESSAGE,  "Breakpoint not found, unable to delete a breakpoint.");
         break;
      case IDERR_CREATE_CONFIG_FILE:
         PrintMessage(ERROR_MESSAGE,  "Could not create config file %s.", szMdiConfigName);
         break;
      case IDERR_ASH_SRCREGISTER:
         PrintMessage(LOG_MESSAGE,    "Read from unsupported register number. Returning 0x%08lX.", BAD_HEX_VALUE);
         break;
      case IDERR_ASH_DSTREGISTER:
         PrintMessage(LOG_MESSAGE,    "Write to an unsupported register number ignored.");
         break;
       case IDERR_GDI_ERROR:
       case IDERR_ZEPHYR_TRACE_ERROR:
         if(pfAshErrorGetMessage != NULL)
            {
            char *pszAshError = NULL;
            (void)pfAshErrorGetMessage(&pszAshError);
            if(pszAshError != NULL)
               {
               PrintMessage(INFO_MESSAGE, pszAshError);
               break;
               }
            }
         PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
         break;
      case IDERR_MDI_PARAM      :
         PrintMessage(ERROR_MESSAGE, "Invalid Paramater error occurred.");
         break;
      case IDERR_MDI_VERSION    :
         PrintMessage(ERROR_MESSAGE, "Invalid Version of target library %s.", szTargetDllName);
         break;
      case IDERR_MDI_INTERNAL   :
         PrintMessage(ERROR_MESSAGE, "Internal error occurred.");
         break;
      case IDERR_MDI_TGHANDLE   :
         PrintMessage(ERROR_MESSAGE, "Invalid handle error occurred.");
         break;
      case IDERR_MDI_HANDLE     :
         PrintMessage(ERROR_MESSAGE, "Invalid handle error occurred.");
         break;
      case IDERR_MDI_DEVICE     :
         PrintMessage(ERROR_MESSAGE, "Invalid Device error occurred.");
         break;
      case IDERR_MDI_SRCOFFSET  :
         PrintMessage(ERROR_MESSAGE, "Read from Invalid Address.");
         break;
      case IDERR_MDI_SRCRESOURCE:
         PrintMessage(ERROR_MESSAGE, "Read from Invalid Resource.");
         break;
      case IDERR_MDI_DSTOFFSET  :
         PrintMessage(ERROR_MESSAGE, "Write to Invalid Address.");
         break;
      case IDERR_MDI_DSTRESOURCE:
         PrintMessage(ERROR_MESSAGE, "Invalid Destination Resource.");
         break;
      case IDERR_MDI_RANGE      :
         PrintMessage(ERROR_MESSAGE, "Invalid Range error occurred.");
         break;
      default:
         PrintMessage(ERROR_MESSAGE, "Unknown target error occurred.");
         break;
      }
}

/****************************************************************************
     Function: MIPSInitVars
     Engineer: Nikolay Chokoev
        Input: none
       Output: void
  Description: Init variables....
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static void MIPSInitVars()
{
   //todo for multicore
   tySI.tyProcConfig.Processor._MIPS.uiSleepTime = MAX_SLEEP_TIME / 1;
}

/****************************************************************************
     Function: IsBigEndian
     Engineer: Nikolay Chokoev
        Input: none
       Output: void
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static void IsBigEndian(void)
{
   unsigned long ulRegValue;
   TyRegDef      *ptyRegDef;
   static int    bEndianModeRead = FALSE;
   static int    bEndianMode     = FALSE;

   if(!tySI.tyProcConfig.bEndianModeModifiable && bEndianModeRead)
      {
      tySI.tyProcConfig.Processor._MIPS.bProcIsBigEndian[0] = bEndianMode;
      return;
      }

   //todo: check
   tySI.tyProcConfig.Processor._MIPS.bProcIsBigEndian[0] = FALSE;
   if((ptyRegDef = GetRegDefForIndex(iRegDefEndianMode)) == NULL)
      {
      return;
      }

   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegDef, &ulRegValue))
      return;

   bEndianMode     = (ulRegValue != 0);
   bEndianModeRead = TRUE;

   tySI.tyProcConfig.Processor._MIPS.bProcIsBigEndian[0] = bEndianMode;
   return;
}

/****************************************************************************
     Function: SetEndianMode
     Engineer: Nikolay Chokoev
        Input: bBigEndian : TRUE to set Big Endian, else set Little Endian mode
       Output: int: NO_ERROR or error number
  Description: set the Endian mode on the processor.
Date           Initials    Description
03-Jul-2008    NCH         Initial
*****************************************************************************/
static int SetEndianMode(int bBigEndian)
{
   TyRegDef      *ptyRegDef;

   if(!tySI.tyProcConfig.bEndianModeModifiable)
      return GDBSERV_NO_ERROR;

   if((ptyRegDef = GetRegDefForIndex(iRegDefEndianMode)) == NULL)
      return GDBSERV_NO_ERROR;

   if(GDBSERV_NO_ERROR != LOW_WriteRegisterByRegDef(ptyRegDef, bBigEndian?1:0))
      {
      // error message already displayed
      return GDBSERV_NO_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: SetInitialEndianMode
     Engineer: Nikolay Chokoev
        Input: none
       Output: int: NO_ERROR or error number
  Description: Set the Initial Endian Mode as configured by the user
Date           Initials    Description
03-Jul-2008    NCH         Initial
*****************************************************************************/
static int SetInitialEndianMode()
{
   switch(tySI.tyProcConfig.tyInitialEndianMode)
      {
      case BigEndianMode:
         return SetEndianMode(TRUE);
      case LittleEndianMode:
         return SetEndianMode(FALSE);
      case DefaultEndianMode:
      default:
         break;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: AddAdvancedBpArrayEntry
     Engineer: Rejeesh S Babu
        Input: ulAddress     : Address
       Output: nil
  Description: Add this address to the Adv. breakpoint list
Date           Initials    			Description
23-Jun-2010    Rejeesh S Babu          Initial
****************************************************************************/
void AddAdvancedBpArrayEntry(unsigned long ulAddress)
{
   
   if(NULL == ptyAdvanceBpArray)
	  {
		 ptyAdvanceBpArray=(TyAdvanceBpArray*)malloc(sizeof(TyAdvanceBpArray));
		 ptyAdvanceBpArray->ulAddress=ulAddress;
		 ptyAdvanceBpArray->pNext=NULL;

	  }
   else
	 {
	     TyAdvanceBpArray *ptemp=ptyAdvanceBpArray;
		 while(ptemp->pNext != NULL)
			 {
			 ptemp=ptemp->pNext;
			 }
		 ptemp->pNext=(TyAdvanceBpArray*)malloc(sizeof(TyAdvanceBpArray));
		 ptemp->pNext->ulAddress=ulAddress;
		 ptemp->pNext->pNext=NULL;
	 }

}
/****************************************************************************
     Function: SearchforAdvancedBpEntry
     Engineer: Rejeesh S Babu
        Input: uladdr :BP address
       Output: 1  - BP found
			   0 - BP not found
  Description: Search for an Adv BP in ADvBP list
Date           Initials    			Description
23-Jun-2010    Rejeesh S Babu         Initial
****************************************************************************/
int SearchforAdvancedBpEntry(unsigned long ulAddr)
{
   int iBPfound = 0;
   TyAdvanceBpArray *ptemp = ptyAdvanceBpArray;
   while(ptemp != NULL)
	  {
	  if(ptemp->ulAddress == ulAddr)
		 {
		 iBPfound = 1;
		 break;
		 }
	  ptemp = ptemp->pNext;
	  }
   return iBPfound;
}

/****************************************************************************
     Function: DeleteAdvancedBpArrayEntry
     Engineer: Rejeesh S Babu
        Input: ulAddress     : Address
       Output: error code
  Description: Delete this address from the Adv. breakpoint list
Date           Initials    			Description
23-Jun-2010    Rejeesh S Babu          Initial
****************************************************************************/
int DeleteAdvancedBpArrayEntry(unsigned long ulAddr)
{
int iErr = 1;
TyAdvanceBpArray *ptemp;
if (NULL != ptyAdvanceBpArray)
{
	 ptemp = ptyAdvanceBpArray;
	 if (ptemp->ulAddress == ulAddr)
	 {
		  ptyAdvanceBpArray = ptemp->pNext;
		  free(ptemp);
		  iErr = 0;
	 }
	 else
	 {
		  while (ptemp->pNext != NULL)
		  {
				if (ptemp->pNext->ulAddress == ulAddr)
				{
					 TyAdvanceBpArray *ptemp1=ptemp->pNext;
					 ptemp->pNext=ptemp->pNext->pNext;
					 free(ptemp1);
					 iErr = 0;
					 break;
				}
		  }

	 }
}
return iErr;
}

/****************************************************************************
     Function: AddBpArrayEntry
     Engineer: Nikolay Chokoev
        Input: ulBpType       : breakpoint type
               ulAddress      : breakpoint address
               ulLength       : breakpoint length
               iCurrentCoreIndex : Core index
       Output: int - error code
  Description: Add a breakpoint to the breakpoint array
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static int  AddBpArrayEntry(unsigned long ulBpType,
                           unsigned long ulAddress,
                           unsigned long ulLength,
                           unsigned long ulIdentifier)
{
   TyBpElement * ptyNewBpArray;
   //(void)AddAdvancedBpArrayEntry(NULL);

   if(uiBpCount >= uiBpAlloc)
      {
      // reallocate array adding a chunk of entries...
      ptyNewBpArray = (TyBpElement*)realloc(ptyBpArray, (((unsigned int)uiBpAlloc+GDBSERV_BP_ARRAY_ALLOC_CHUNK) * sizeof(TyBpElement)));

      if(ptyNewBpArray == NULL)
         return IDERR_ADD_BP_NO_MEMORY;

      // clear new chunk to 0...
      memset(&ptyNewBpArray[uiBpAlloc], 0, (GDBSERV_BP_ARRAY_ALLOC_CHUNK * sizeof(TyBpElement)));

      ptyBpArray = ptyNewBpArray;
      uiBpAlloc  += GDBSERV_BP_ARRAY_ALLOC_CHUNK;
      }

   // sanity check, this should not happen...
   if(uiBpCount >= uiBpAlloc)
      return IDERR_ADD_BP_INTERNAL1;

   ptyBpArray[uiBpCount].ulBpType     = ulBpType;
   ptyBpArray[uiBpCount].ulAddress    = ulAddress;
   ptyBpArray[uiBpCount].ulLength     = ulLength;
   ptyBpArray[uiBpCount].ulIdentifier = ulIdentifier;

   uiBpCount++;

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: RemoveBpArrayEntry
     Engineer: Nikolay Chokoev
        Input: iIndex : index of breakpoint to remove
               iCurrentCoreIndex : core index
       Output: TyError
  Description: Remove a breakpoint from the breakpoint array
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static int RemoveBpArrayEntry(unsigned  int uiIndex, 
                              int iCurrentCoreIndex)
{
   iCurrentCoreIndex = iCurrentCoreIndex;
   // sanity check, this should not happen...
   if(uiBpCount >= uiBpAlloc)
      return IDERR_REMOVE_BP_INTERNAL1;

   if(uiIndex >= uiBpCount)
      return IDERR_REMOVE_BP_INTERNAL2;

   // we will not free any array storage
   // but, move all elements above iIndex down 1 place...
   for(; uiIndex < uiBpCount-1; uiIndex++)
      {
      ptyBpArray[uiIndex] = ptyBpArray[uiIndex+1];
      }

   // there is one less BP in the array
   uiBpCount--;

   // clear the old top element
   memset(&ptyBpArray[uiBpCount], 0, sizeof(TyBpElement));

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: RemoveAllBpArray
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: Remove all breakpoints from the breakpoint array, free array
               for all cores
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static void RemoveAllBpArray(void)
{
//todo ..for all cores
   if(ptyBpArray)
      {
      free(ptyBpArray);
      ptyBpArray = NULL;
      }

   uiBpCount = 0;
}

/****************************************************************************
     Function: GetBpArrayEntry
     Engineer: Nikolay Chokoev
        Input: ulBpType       : breakpoint type
               ulAddress      : breakpoint address
               ulLength       : breakpoint length
               *pulIdentifier : storage for breakpoint identifier from MDI
               iCurrentCoreIndex : Core index
       Output: int : breakpoint index, else -1
  Description: Get the index of a breakpoint in the breakpoint array
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static int GetBpArrayEntry(unsigned long  ulBpType,
                           unsigned long  ulAddress,
                           unsigned long  ulLength,
                           unsigned long *pulIdentifier,
                           int iCurrentCoreIndex)
{
   unsigned int uiIndex;
   NOREF(ulBpType);
   NOREF(ulLength);

   iCurrentCoreIndex = iCurrentCoreIndex; //todo multicore
   for(uiIndex=0; uiIndex < uiBpCount; uiIndex++)
      {
     /* if((ptyBpArray[uiIndex].ulBpType  == ulBpType ) &&
         (ptyBpArray[uiIndex].ulAddress == ulAddress) &&
         (ptyBpArray[uiIndex].ulLength  == ulLength ))*/
      if((ptyBpArray[uiIndex].ulAddress == ulAddress))
         {
         *pulIdentifier = ptyBpArray[uiIndex].ulIdentifier;
         return(int)uiIndex;
         }
      }
   return -1;
}
/****************************************************************************
     Function: DisableAdvanceBpEntryForAddr
     Engineer: Dileep Kumar K
        Input: ulAddress      : breakpoint address
       Output: int : 0, else -1
  Description: Eable advanced breakpoint in the list, using address only
Date           Initials    Description
07-Jun-2010    DK          Initial
****************************************************************************/
/*static int EnableAdvanceBpEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex)*/
#if 0
int LOW_EnableAdvBpEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex)
{
   //unsigned int uiIndex;
   TyAdvanceBpArray *ptytmpAdvanceBP = NULL;
  // unsigned int uiIndex = 0;
   int iMDIRet = 0;

   ptytmpAdvanceBP = ptyAdvanceBpArray;
   iCurrentCoreIndex = iCurrentCoreIndex; //todo multicore
   //check weather is there is any breakpoint at ulAddress is present and
   // weather it is already enabled then do nothing else enable it
   for(ptytmpAdvanceBP = ptyAdvanceBpArray->pNext ; 
       ptytmpAdvanceBP != ptyAdvanceBpArray ; 
       ptytmpAdvanceBP = ptytmpAdvanceBP->pNext
      )
      {
      if((ulAddress == ptytmpAdvanceBP->ptyAdvBpInfo.Range.Start) &&
         (ptytmpAdvanceBP->ptyAdvBpInfo.Enabled == 0) )
         {
         ptytmpAdvanceBP->ptyAdvBpInfo.Enabled = 1;
         iMDIRet=pfMDISetBp(tyCurrentDevHndl,
                            &ptytmpAdvanceBP->ptyAdvBpInfo);
         switch(iMDIRet)
            {
            case MDISuccess:
               break;      // Good do nothing
            case MDIErrFailure:
               SetupErrorMessage(IDERR_GDI_ERROR);
               return GDBSERV_ERROR;
            case MDIErrDevice:
               SetupErrorMessage(IDERR_MDI_DEVICE);
               return GDBSERV_ERROR;
            case MDIErrRange:
               SetupErrorMessage(IDERR_MDI_RANGE);
               return GDBSERV_ERROR;
            default:
               SetupErrorMessage(IDERR_MDI_INTERNAL);
               return GDBSERV_ERROR;
            }

         return GDBSERV_NO_ERROR;
         }
      }
   return -1;
}
#endif
/****************************************************************************
     Function: DisableAdvanceBpEntryForAddr
     Engineer: Dileep Kumar K
        Input: ulAddress      : breakpoint address
       Output: int : 0, else -1
  Description: Disable advanced breakpoint in the list, using address only
Date           Initials    Description
07-Jun-2010    DK          Initial
****************************************************************************/
/*static int DisableAdvanceBpEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex)*/
#if 0
int LOW_DisableAdvBpEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex)
{
   //unsigned int uiIndex;
   TyAdvanceBpArray *ptytmpAdvanceBP = NULL;
   //unsigned int uiIndex = 0;
   //unsigned long ulAdvBpId = 0;
   int iMDIRet = 0;

   ptytmpAdvanceBP = ptyAdvanceBpArray;
   iCurrentCoreIndex = iCurrentCoreIndex; //todo multicore
   //iMDIRet = GetAdvBpEntry(ulAddress, &ulAdvBpId , 0);
   //if(iMDIRet != GDBSERV_ERROR )
   //   return iMDIRet;
   //check weather is there is any breakpoint at ulAddress is present and
   // weather it is already disabled then do nothing else enable it
   for(ptytmpAdvanceBP = ptyAdvanceBpArray->pNext ;
       ptytmpAdvanceBP != ptyAdvanceBpArray ;
        ptytmpAdvanceBP = ptytmpAdvanceBP->pNext
      )
      {
      if((ulAddress == ptytmpAdvanceBP->ptyAdvBpInfo.Range.Start) &&
         (ptytmpAdvanceBP->ptyAdvBpInfo.Enabled == 1))
         {
         ptytmpAdvanceBP->ptyAdvBpInfo.Enabled = 0;
         iMDIRet=pfMDIClearBp(tyCurrentDevHndl, ptytmpAdvanceBP->ptyAdvBpInfo.Id);
         switch (iMDIRet)
            {
            case MDISuccess:
               break;      // Good do nothing
            case MDIErrFailure:
               SetupErrorMessage(IDERR_GDI_ERROR);
               return GDBSERV_ERROR;
            case MDIErrDevice:
               SetupErrorMessage(IDERR_MDI_DEVICE);
               return GDBSERV_ERROR;
            default:
               SetupErrorMessage(IDERR_MDI_INTERNAL);
               return GDBSERV_ERROR;
            }
         return 0;
         }
      }
   return -1;
}
#endif 
/****************************************************************************
     Function: GetBpArrayEntry
     Engineer: Dileep Kumar K
        Input: ulAddress      : breakpoint address
              *pulIdentifier : storage for breakpoint identifier from MDI
               iCurrentCoreIndex : Core index
       Output: int : Advabce breakpoint index, else -1
  Description: Get the index of advanced  breakpoint in the advanced 
               breakpoint liost
Date           Initials    Description
08-Jun-2010    DK          Initial
****************************************************************************/
#if 0
static int GetAdvBpEntry(unsigned long  ulAddress,
                           unsigned long *pulIdentifier,
                           int iCurrentCoreIndex)
{
   unsigned int uiIndex = GDBSERV_NO_ERROR;
   TyAdvanceBpArray *ptytmpAdvanceBP = NULL;

   iCurrentCoreIndex = iCurrentCoreIndex; //todo multicore
   for(ptytmpAdvanceBP = ptyAdvanceBpArray->pNext ;
        ptytmpAdvanceBP !=ptyAdvanceBpArray ; 
        ptytmpAdvanceBP = ptytmpAdvanceBP->pNext)
   //for(uiIndex=0; uiIndex < uiBpCount; uiIndex++)
      {
      //if( (ptyBpArray[uiIndex].ulAddress == ulAddress))
      if((ptytmpAdvanceBP->ptyAdvBpInfo.Range.Start == ulAddress))
         {
         //*pulIdentifier = ptyBpArray[uiIndex].ulIdentifier;
            *pulIdentifier = ptytmpAdvanceBP->ptyAdvBpInfo.Id;
         return(int)uiIndex;
         }
      }
   return -1;
}
#endif

/****************************************************************************
     Function: GetAdvanceBpEntryForAddr
     Engineer: Dileep Kumar K
        Input: ulAddress      : breakpoint address
       Output: int : breakpoint index, else -1
  Description: Get the index of advanced breakpoint in the list, using address only
Date           Initials    Description
07-Jun-2010    DK          Initial
****************************************************************************/
#if 0
static int GetAdvBpEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex)
{
   //unsigned int uiIndex;
   TyAdvanceBpArray *ptytmpAdvanceBP = NULL;
   unsigned int uiIndex = 0;

   ptytmpAdvanceBP = ptyAdvanceBpArray;
   iCurrentCoreIndex = iCurrentCoreIndex; //todo multicore
   for(ptytmpAdvanceBP = ptyAdvanceBpArray->pNext ; 
       ptytmpAdvanceBP != ptyAdvanceBpArray ;
       ptytmpAdvanceBP = ptytmpAdvanceBP->pNext
      )
      {
      if(ulAddress == ptytmpAdvanceBP->ptyAdvBpInfo.Range.Start)
         {
         return(int)(++uiIndex);
         }
      }
   return -1;
}
#endif
/****************************************************************************
     Function: GetBpArrayEntryForAddr
     Engineer: Nikolay Chokoev
        Input: ulAddress      : breakpoint address
       Output: int : breakpoint index, else -1
  Description: Get the index of a breakpoint in the array, using address only
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
static int GetBpArrayEntryForAddr(unsigned long  ulAddress,
                                  int iCurrentCoreIndex)
{
   unsigned int uiIndex;

   iCurrentCoreIndex = iCurrentCoreIndex; //todo multicore
   for(uiIndex=0; uiIndex < uiBpCount; uiIndex++)
      {
      if(ptyBpArray[uiIndex].ulAddress == ulAddress)
         {
         return(int)uiIndex;
         }
      }
   return -1;
}

/****************************************************************************
     Function: GetRegDefForUserSpecificReg
     Engineer: Nikolay Chokoev
        Input: adAddr
               ulData
       Output: int
  Description: Convert a user specific reg struct to a TyRegDef struct
Date           Initials    Description
03-Feb-2006    PJS         Initial
****************************************************************************/
static int GetRegDefForUserSpecificReg(TyRegDef         *ptyRegDef,
                                       TyUserSpecific    *ptyUserSpecReg)
{
   TyRegDef    *ptyTempRegDef;

   if(ptyUserSpecReg->ulVariableSize == 0)
      {
      // reference to a register in the .REG file
      if((ptyTempRegDef = GetRegDefForName(ptyUserSpecReg->szVariableName)) == NULL)
         {
         // TO_DO: when to display message?
         PrintMessage(ERROR_MESSAGE, "Unknown register '%s'.", ptyUserSpecReg->szVariableName);
         return IDERR_ASH_DSTREGISTER;
         }

      *ptyRegDef = *ptyTempRegDef;
      }
   else
      {
      // this is a memory mapped register defined in reg-settings-file
      ptyRegDef->pszRegName    = ptyUserSpecReg->szVariableName;
      ptyRegDef->ulAddress     = ptyUserSpecReg->ulVariableAddress;
      ptyRegDef->ubMemType     = MEM_CODE;
      if(ptyUserSpecReg->ulVariableSize > 4)
         ptyRegDef->ubByteSize = 4;
      else
         ptyRegDef->ubByteSize = (unsigned char)ptyUserSpecReg->ulVariableSize;
      ptyRegDef->ubBitOffset   = 0;
      ptyRegDef->ubBitSize     = (unsigned char)(ptyRegDef->ubByteSize * 8);
      ptyRegDef->iFullRegIndex = -1;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: SetupRegisterSettings
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Update all register settings after a reset.
Date           Initials    Description
03-Feb-2006    PJS         Initial
22-Mar-2006    PJS         use register definitions (.REG file support)
09-May-2008    SPC         Not setting PC, if --program-entry-point not used.
****************************************************************************/
static int SetupRegisterSettings(void)
{
   int  ErrRet;
   unsigned long    i;
   TyRegDef       tyRegDef;

   // refresh endian mode copy
   IsBigEndian();

   if(tySI.tyProcConfig.Processor._MIPS.isPEPset)
      {
      if((ErrRet = MdiWriteReg(REG_MIPS_PC, tySI.tyProcConfig.ulProgEntryPoint)) != GDBSERV_NO_ERROR)
         return ErrRet;
      }

   for(i=0; i < tySI.tyProcConfig.Processor._MIPS.ulUserSpecificVariablesCount; i++)
      {
      if((ErrRet = GetRegDefForUserSpecificReg(&tyRegDef,
                                               &(tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[i]))) != GDBSERV_NO_ERROR)
         return ErrRet;

      if(GDBSERV_NO_ERROR != LOW_WriteRegisterByRegDef(&tyRegDef, tySI.tyProcConfig.Processor._MIPS.pUserSpecificVariables[i].ulVariableValue))
         break;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: MdiReadReg
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Read A Register 
Date           Initials    Description
03-Feb-2006    PJS         Initial
****************************************************************************/
int MdiReadReg(unsigned long tyReg, unsigned long *pulDataValue)
{
   int  iMDIRet;
   unsigned long    ulSelect;
   unsigned long    ulOffset;
   unsigned long    ulResource;
   unsigned long    ulSpecialOffset;
   unsigned long    ulRegisterSpecified;

   if(!pfMDIRead)
      {
      //TODO: ASSERT_NOW();
      return IDERR_DLL_NOT_LOADED;
      }

   // First copy the address of the register specified
   ulRegisterSpecified = (unsigned long)tyReg;

   if(ulRegisterSpecified == REG_MIPS_PC)
      {
      ulRegisterSpecified = 0x40200000;
      }
   else if( ulRegisterSpecified == REG_MIPS_HI || ulRegisterSpecified == REG_MIPS_LO )
      {
      if(ulRegisterSpecified == REG_MIPS_HI)
         {
         ulRegisterSpecified = 0x40300000;
         }
      else
         {
         ulRegisterSpecified = 0x40300001;
         }
      }
   else
      {
      if(((int)ulRegisterSpecified >= REG_START_OF_MIPS_GP) && ((int)ulRegisterSpecified < REG_START_OF_MIPS_GP + 32))
         {
         ulRegisterSpecified = 0x40100000 + (ulRegisterSpecified - REG_START_OF_MIPS_GP);
         }
      }

   // Registers are MDI Encoded as follows
   // Bits 0-15   :  Offset
   // Bits 16-19  :  Select
   // Bits 20-29  :  Resource Type
   // Bits 30     :  Indicates MDI type encoding used if set
   // Note: Cannot use Bit 31 as the flag becaure the type unsigned long is a singed enumerated type

   if(!(ulRegisterSpecified & 0x40000000))
      {
      // Old Ashling style encoding, Used for Special MIPS registers
      iMDIRet=pfMDIRead(tyCurrentDevHndl,ASHLING_INT_REG,(MDIOffsetT)ulRegisterSpecified,pulDataValue,4,1);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;      // Good do nothing
         case MDIErrFailure:
            ASSERT_NOW();
            return IDERR_GDI_ERROR;       // Calls Setup Error Message
         case MDIErrDevice:
            ASSERT_NOW();
            return IDERR_MDI_DEVICE; 
         case MDIErrInvalidSrcOffset:
            ASSERT_NOW();
            return IDERR_MDI_SRCOFFSET;
         case MDIErrSrcResource:
            ASSERT_NOW();
            return IDERR_MDI_SRCRESOURCE;
         case MDIErrInvalidDstOffset:
            ASSERT_NOW();
            return IDERR_MDI_DSTOFFSET;
         case MDIErrDstResource:
            ASSERT_NOW();
            return IDERR_MDI_DSTRESOURCE;
         default:              
            ASSERT_NOW();
            return IDERR_MDI_INTERNAL; 
         }
      }
   else
      {
      ulOffset = ulRegisterSpecified & 0xFFFF;
      ulSelect = (ulRegisterSpecified &0x000F0000) >> 16;
      ulResource = (ulRegisterSpecified & 0x3FF00000) >> 20;
      ulSpecialOffset = ulOffset + (ulSelect * 32);
      iMDIRet=pfMDIRead(tyCurrentDevHndl,ulResource,(MDIOffsetT)ulSpecialOffset,pulDataValue,4,1);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;      // Good do nothing
         case MDIErrFailure:
            ASSERT_NOW();
            return IDERR_GDI_ERROR;       // Calls Setup Error Message
         case MDIErrDevice:
            ASSERT_NOW();
            return IDERR_MDI_DEVICE; 
         case MDIErrInvalidSrcOffset:
            ASSERT_NOW();
            return IDERR_MDI_SRCOFFSET;
         case MDIErrSrcResource:
            ASSERT_NOW();
            return IDERR_MDI_SRCRESOURCE;
         case MDIErrInvalidDstOffset:
            ASSERT_NOW();
            return IDERR_MDI_DSTOFFSET;
         case MDIErrDstResource:
            ASSERT_NOW();
            return IDERR_MDI_DSTRESOURCE;
         default:
            ASSERT_NOW();
            return IDERR_MDI_INTERNAL; 
         }
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: MdiWriteReg
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Write A Register 
Date           Initials    Description
03-Feb-2006    PJS         Initial
****************************************************************************/
int MdiWriteReg(unsigned long tyReg, unsigned long ulDataValue)
{
   int  iMDIRet;
   unsigned long    ulSelect;
   unsigned long    ulOffset;
   unsigned long    ulResource;
   unsigned long    ulSpecialOffset;
   unsigned long    ulRegisterSpecified;

   if(!pfMDIWrite)
      {
      //TODO: ASSERT_NOW();
      return IDERR_DLL_NOT_LOADED;
      }

   // First copy the address of the register specified
   ulRegisterSpecified = (unsigned long)tyReg;

   if(ulRegisterSpecified == REG_MIPS_PC)
      {
      ulRegisterSpecified = 0x40200000;
      }
   else
      {
      if(((int)ulRegisterSpecified >= REG_START_OF_MIPS_GP) && ((int)ulRegisterSpecified < REG_START_OF_MIPS_GP + 32))
         {
         ulRegisterSpecified = 0x40100000 + (ulRegisterSpecified - REG_START_OF_MIPS_GP);
         }
      }

   // Registers are MDI Encoded as follows
   // Bits 0-15   :  Offset
   // Bits 16-19  :  Select
   // Bits 20-29  :  Resource Type
   // Bits 30     :  Indicates MDI type encoding used if set
   // Note: Cannot use Bit 31 as the flag becaure the type unsigned long is a singed enumerated type

   if(!(ulRegisterSpecified & 0x40000000))
      {
      // Old Ashling style encoding, Used for Special MIPS registers
      iMDIRet=pfMDIWrite(tyCurrentDevHndl,ASHLING_INT_REG,(MDIOffsetT)ulRegisterSpecified,&ulDataValue,4,1);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;      // Good do nothing
         case MDIErrFailure:
            ASSERT_NOW();
            return IDERR_GDI_ERROR;       // Calls Setup Error Message
         case MDIErrDevice:
            ASSERT_NOW();
            return IDERR_MDI_DEVICE; 
         case MDIErrInvalidSrcOffset:
            ASSERT_NOW();
            return IDERR_MDI_SRCOFFSET;
         case MDIErrSrcResource:
            ASSERT_NOW();
            return IDERR_MDI_SRCRESOURCE;
         case MDIErrInvalidDstOffset:
            ASSERT_NOW();
            return IDERR_MDI_DSTOFFSET;
         case MDIErrDstResource:
            ASSERT_NOW();
            return IDERR_MDI_DSTRESOURCE;
         default:
            ASSERT_NOW();
            return IDERR_MDI_INTERNAL; 
         }
      }
   else
      {
      // Mdi Style Encoding
      ulOffset   = ulRegisterSpecified & 0xFFFF;
      ulSelect   = (ulRegisterSpecified &0x000F0000) >> 16;
      ulResource = (ulRegisterSpecified & 0x3FF00000) >> 20;
      ulSpecialOffset = ulOffset + (ulSelect * 32);

      iMDIRet=pfMDIWrite(tyCurrentDevHndl,ulResource,(MDIOffsetT)ulSpecialOffset,&ulDataValue,4,1);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;      // Good do nothing
         case MDIErrFailure:
            ASSERT_NOW();
            return IDERR_GDI_ERROR;       // Calls Setup Error Message
         case MDIErrDevice:
            ASSERT_NOW();
            return IDERR_MDI_DEVICE; 
         case MDIErrInvalidSrcOffset:
            ASSERT_NOW();
            return IDERR_MDI_SRCOFFSET;
         case MDIErrSrcResource:
            ASSERT_NOW();
            return IDERR_MDI_SRCRESOURCE;
         case MDIErrInvalidDstOffset:
            ASSERT_NOW();
            return IDERR_MDI_DSTOFFSET;
         case MDIErrDstResource:
            ASSERT_NOW();
            return IDERR_MDI_DSTRESOURCE;
         default:
            ASSERT_NOW();
            return IDERR_MDI_INTERNAL; 
         }
      }

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: GetTLBCount
     Engineer: Dileep Kumar K
        Input: unsigned short :get the number of TLB entries
       Output: int
  Description: Get TLB count
Date           Initials    Description
05-AUG-2009    DK         Initial
****************************************************************************/
int GetTLBCount( unsigned short *pusCount )
{
   int iErrRet;
   unsigned long ulConfig1Val;
   unsigned int uiMMUSizeMasked;
   assert(pusCount!=NULL);
   iErrRet = pfMDIRead(tyCurrentDevHndl, MDIMIPCP0,(MDIOffsetT)(Config1RegNo+32),&ulConfig1Val,4,Config1RegSel);
   if( NO_ERROR != iErrRet )
      return FALSE;
   uiMMUSizeMasked = ulConfig1Val & MMUSizeBitMask;
   *pusCount = (uiMMUSizeMasked >> MMUSizeBitShift)+1;
   return iErrRet;
}
/****************************************************************************
     Function: LOW_WriteTLB
     Engineer: Dileep Kumar K
        Input: tyTLBEntry 
       Output: int
  Description: Write TLB entries
Date           Initials    Description
05-AUG-2009    DK         Initial
****************************************************************************/
int LOW_WriteTLB(tyTLBEntry *ptyTLBentry, unsigned short usTLBcount)
{
   int iError;
   assert(ptyTLBentry != NULL);
   iError = pfMDIWrite( tyCurrentDevHndl, MDIMIPTLB, (MDIOffsetT)(usTLBcount*4), (void*)ptyTLBentry, 4, (unsigned int)4);
   switch(iError)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         ASSERT_NOW();
         return IDERR_GDI_ERROR;       // Calls Setup Error Message
      case MDIErrDevice:
         ASSERT_NOW();
         return IDERR_MDI_DEVICE; 
      case MDIErrInvalidSrcOffset:
         ASSERT_NOW();
         return IDERR_MDI_SRCOFFSET;
      case MDIErrSrcResource:
         ASSERT_NOW();
         return IDERR_MDI_SRCRESOURCE;
      case MDIErrInvalidDstOffset:
         ASSERT_NOW();
         return IDERR_MDI_DSTOFFSET;
      case MDIErrDstResource:
         ASSERT_NOW();
         return IDERR_MDI_DSTRESOURCE;
      default:
         ASSERT_NOW();
         return IDERR_MDI_INTERNAL; 
      }     
   return NO_ERROR;
}
/****************************************************************************
     Function: LOW_ReadTLB
     Engineer: Dileep Kumar K
        Input: tyTLBEntry:doublepointer for TLB entries
             : unsigned char :number of tlb entries
       Output: int
  Description: Write TLB entries
Date           Initials    Description
05-AUG-2009    DK         Initial
****************************************************************************/
int LOW_ReadTLB(tyTLBEntry **pptyTLBentry, unsigned char *pucNumOfTLBEntry)
{
   int iErrRet = TRUE;
   tyTLBEntry *ptyEntries =*pptyTLBentry;
   unsigned char ucNumOfTLBEntry=32;
   unsigned long ulTLBIndex;
   unsigned long ultlbSize;
   assert(pptyTLBentry != NULL);
   assert(pucNumOfTLBEntry!=NULL);
   ucNumOfTLBEntry=*pucNumOfTLBEntry;
   ulTLBIndex = 0;
   ultlbSize = ucNumOfTLBEntry * 4;
   iErrRet = pfMDIRead(tyCurrentDevHndl,MDIMIPTLB,(MDIOffsetT)ulTLBIndex,(void*) ptyEntries,4 ,ultlbSize); //0x10
   if( NO_ERROR != iErrRet )
      {
      return FALSE;
      }
   return TRUE;
}

/****************************************************************************
     Function: Low_EnableOSMMU
     Engineer: Dileep Kumar K
        Input: TyOSType:Linux Os type that is going to debug
               unsigned long*:Address for OSMMU initialization 
        Output: int
  Description: Write TLB entries
Date           Initials    Description
05-AUG-2009    DK         Initial
****************************************************************************/
int Low_EnableOSMMU(TyOSType tyos, unsigned long *pulOSMMUAddrs)
//int Low_EnableOSMMU(TyOSType tyos, unsigned long ulOSMMUAddrs)
{
   int iRetVal;
   TyOS tyOS=Linux26;
   if(pfEnableOSMMU == NULL)
      return -1;
   if( tyos==linux26)
      tyOS=Linux26;

   iRetVal=pfEnableOSMMU(tyOS,pulOSMMUAddrs);
   return iRetVal;
}
/****************************************************************************
     Function: Low_SetOSAppMMU
     Engineer: Suresh P.C
        Input: unsigned long ulAppCon - Application context
               unsigned long ulPgDirAddrs - Address to App PGD
       Output: 0 : Success ; Error code: otherwise
  Description: Initializes the current OS Application MMU mapping
Date           Initials    Description
20-Oct-2009    SPC         Initial
****************************************************************************/
int Low_SetOSAppMMU(unsigned long ulAppCon, unsigned long ulPgDirAddrs)
{
   int iRetVal;

   if(pfAshInitOSAppMMU == NULL)
      return -1;

   iRetVal=pfAshInitOSAppMMU(ulAppCon, ulPgDirAddrs);

   return iRetVal;
}

/****************************************************************************
     Function: MdiReadMem
     Engineer: Nikolay Chokoev
        Input: MemReadType  :
               adStartAddr  :
               adReadLen    :
               *pBuffer     :
               adObjectSize :
       Output: int
  Description: read memory
Date           Initials    Description
03-Feb-2006    PJS         Initial
****************************************************************************/
static int MdiReadMem(TXRXMemAccessTypes   MemReadType,
                      unsigned long                adStartAddr,
                      unsigned long                adReadLen,
                      unsigned char                *pBuffer,
                      unsigned long                adObjectSize)
{
   int        iMDIRet;
   unsigned long          ulDefaultObjectSize;
   unsigned long          ulObjectCount;
   MDIResourceT   ResType = MDIMIPGVIRTUAL;

   if(!pfMDIRead)
      {
      ASSERT_NOW();
      return IDERR_DLL_NOT_LOADED;
      }

   switch(MemReadType)
      {
      case MEM_TLB_ENTRY_ACCESS:
         ResType              = MDIMIPTLB;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_ICACHE_ENTRY_TAG_ACCESS:
         ResType = MDIMIPPICACHET;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_DCACHE_ENTRY_TAG_ACCESS:
         ResType = MDIMIPPDCACHET;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_ICACHE_ENTRY_DATA_ACCESS:
         ResType = MDIMIPPICACHE;
         ulDefaultObjectSize  =  1;
         break;
      case MEM_ICACHE_ENTRY_WS_ACCESS:
         ResType = MDIMIPSICACHEWS;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_DCACHE_ENTRY_WS_ACCESS:
         ResType = MDIMIPSDCACHEWS;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_DCACHE_ENTRY_DATA_ACCESS:
         ResType = MDIMIPPDCACHE;
         ulDefaultObjectSize  =  1;
         break;
      case MEM_PHYSICAL_ACCESS:
         ResType = MDIMIPPHYSICAL;
         ulDefaultObjectSize=adObjectSize;
         break;
      default:
         ResType = MDIMIPGVIRTUAL;
         ulDefaultObjectSize=adObjectSize;
         break;
      } //lint !e788


   if(ulDefaultObjectSize <= 1)
      ulObjectCount = adReadLen;
   else
      ulObjectCount = adReadLen/ulDefaultObjectSize;

   iMDIRet=pfMDIRead(tyCurrentDevHndl,ResType,(MDIOffsetT)adStartAddr,pBuffer,ulDefaultObjectSize,ulObjectCount);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         ASSERT_NOW();
         return IDERR_GDI_ERROR;       // Calls Setup Error Message
      case MDIErrDevice:
         ASSERT_NOW();
         return IDERR_MDI_DEVICE; 
      case MDIErrInvalidSrcOffset:
         ASSERT_NOW();
         return IDERR_MDI_SRCOFFSET;   // TO_DO: mdi_mips.cpp ignores this?
      case MDIErrSrcResource:
         ASSERT_NOW();
         return IDERR_MDI_SRCRESOURCE;
      case MDIErrInvalidDstOffset:
         ASSERT_NOW();
         return IDERR_MDI_DSTOFFSET;   // TO_DO: mdi_mips.cpp ignores this?
      case MDIErrDstResource:
         ASSERT_NOW();
         return IDERR_MDI_DSTRESOURCE;
      default:
         ASSERT_NOW();
         return IDERR_MDI_INTERNAL; 
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: MdiWriteMem
     Engineer: Nikolay Chokoev
        Input: MemWriteType :
               adStartAddr  :
               adWriteLen   :
               *pBuffer     :
               adBufLen     :
               adObjectSize :
       Output: int
  Description: Write memory
Date           Initials    Description
03-Feb-2006    PJS         Initial
****************************************************************************/
static int MdiWriteMem(TXRXMemAccessTypes MemWriteType,
                       unsigned long              adStartAddr,
                       unsigned long              adWriteLen,
                       unsigned char              *pBuffer,
                       unsigned long              adBufLen,
                       unsigned long              adObjectSize)
{
   int        iMDIRet;
   unsigned long          i;
   unsigned long          adCurrentAddr;
   unsigned long          adRemainingBytesToWrite;
   unsigned long          adBytesThisWrite;
   unsigned long          adMaximumWrite;
   unsigned long          ulDefaultObjectSize;
   unsigned long          ulObjectCount;
   MDIResourceT   ResType = MDIMIPGVIRTUAL;

   if(!pfMDIWrite)
      {
      ASSERT_NOW();
      return IDERR_DLL_NOT_LOADED;
      }

   switch(MemWriteType)
      {
      case MEM_TLB_ENTRY_ACCESS:
         ResType = MDIMIPTLB;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_ICACHE_ENTRY_TAG_ACCESS:
         ResType = MDIMIPPICACHET;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_DCACHE_ENTRY_TAG_ACCESS:
         ResType = MDIMIPPDCACHET;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_ICACHE_ENTRY_DATA_ACCESS:
         ResType = MDIMIPPICACHE;
         ulDefaultObjectSize  =  4;
         break;
      case MEM_DCACHE_ENTRY_DATA_ACCESS:
         ResType = MDIMIPPDCACHE;
         ulDefaultObjectSize=1;
         break;
      case MEM_PHYSICAL_ACCESS:
         ResType = MDIMIPPHYSICAL;
         ulDefaultObjectSize=adObjectSize;
         break;
      default:
         ResType = MDIMIPGVIRTUAL;
         ulDefaultObjectSize=adObjectSize;
         break;
      }         //lint !e788

   if(adBufLen == 0)
      {
      ASSERT_NOW();
      return GDBSERV_NO_ERROR;
      }


   // Fill the buffer, repeating the pattern if necessary....
   for(i=0; i < __min(COMMON_BUFFER_SIZE, adWriteLen); i++)
      pucCommonBuffer[i] = pBuffer[i % adBufLen];

   //todo: is it needed?
   // Now figure out how many bytes we can write in one go...
   adMaximumWrite = __min(MAX_TXCMD_WR_MEM_LEN, adWriteLen);

   // Note that the repeating writes must be in even multiples of the
   // adBufLen....
   if((adMaximumWrite == MAX_TXCMD_WR_MEM_LEN) && (adMaximumWrite != adWriteLen))
      adMaximumWrite -= (MAX_TXCMD_WR_MEM_LEN % adBufLen);

   if( (adBufLen > MAX_TXCMD_WR_MEM_LEN) && (adBufLen == adWriteLen))
      {
      // For example, loading a CSO file with a data buffer of 64K
      // Currently MAX_TXCMD_WR_MEM_LEN is equal to 32K
      // Need to increase the write len of the MDI command to support any size!!!
      // MDI doesn't specify any minimum write length

      adCurrentAddr           = adStartAddr;
      adRemainingBytesToWrite = adWriteLen;
      adMaximumWrite          = MAX_TXCMD_WR_MEM_LEN;

      do
         {
         adBytesThisWrite = __min(adRemainingBytesToWrite, adMaximumWrite);
         if(ulDefaultObjectSize <= 1)
            {
            ulObjectCount = adBytesThisWrite;
            }
         else
            {
            ulObjectCount = adBytesThisWrite/ulDefaultObjectSize;
            // ensure a whole number of objects
            adBytesThisWrite = ulObjectCount*ulDefaultObjectSize;
            }

         iMDIRet=pfMDIWrite(tyCurrentDevHndl,                              
                            ResType,                                       
                            (MDIOffsetT)adCurrentAddr,                                 
                            pucCommonBuffer + (adCurrentAddr - adStartAddr), 
                            ulDefaultObjectSize,                           
                            ulObjectCount);
         switch(iMDIRet)
            {
            case MDISuccess:
               break;      // Good do nothing
            case MDIErrFailure:
               ASSERT_NOW();
               return IDERR_GDI_ERROR;       // Calls Setup Error Message
            case MDIErrDevice:
               ASSERT_NOW();
               return IDERR_MDI_DEVICE; 
            case MDIErrInvalidSrcOffset:
               ASSERT_NOW();
               return IDERR_MDI_SRCOFFSET;   // TO_DO: mdi_mips.cpp ignores this?
            case MDIErrSrcResource:
               ASSERT_NOW();
               return IDERR_MDI_SRCRESOURCE;
            case MDIErrInvalidDstOffset:
               ASSERT_NOW();
               return IDERR_MDI_DSTOFFSET;   // TO_DO: mdi_mips.cpp ignores this?
            case MDIErrDstResource:
               ASSERT_NOW();
               return IDERR_MDI_DSTRESOURCE;
            default:
               ASSERT_NOW();
               return IDERR_MDI_INTERNAL; 
            }
         adCurrentAddr           += adBytesThisWrite;
         adRemainingBytesToWrite -= adBytesThisWrite;
         }
      while((adRemainingBytesToWrite != 0) &&
            (adRemainingBytesToWrite >= ulDefaultObjectSize));
      }
   else
      {
      adCurrentAddr           = adStartAddr;
      adRemainingBytesToWrite = adWriteLen;
      do
         {
         adBytesThisWrite = __min(adRemainingBytesToWrite, adMaximumWrite);
         if(ulDefaultObjectSize <= 1)
            {
            ulObjectCount = adBytesThisWrite;
            }
         else
            {
            ulObjectCount = adBytesThisWrite/ulDefaultObjectSize;
            // ensure a whole number of objects
            adBytesThisWrite = ulObjectCount*ulDefaultObjectSize;
            }

         iMDIRet=pfMDIWrite(tyCurrentDevHndl,                               
                            ResType,                                        
                            (MDIOffsetT)adCurrentAddr,                                  
                            pucCommonBuffer,                                
                            ulDefaultObjectSize,                            
                            ulObjectCount);
         switch(iMDIRet)
            {
            case MDISuccess:
               break;      // Good do nothing
            case MDIErrFailure:
               ASSERT_NOW();
               return IDERR_GDI_ERROR;       // Calls Setup Error Message
            case MDIErrDevice:
               ASSERT_NOW();
               return IDERR_MDI_DEVICE; 
            case MDIErrInvalidSrcOffset:
               ASSERT_NOW();
               return IDERR_MDI_SRCOFFSET;   // TO_DO: mdi_mips.cpp ignores this?
            case MDIErrSrcResource:
               ASSERT_NOW();
               return IDERR_MDI_SRCRESOURCE;
            case MDIErrInvalidDstOffset:
               ASSERT_NOW();
               return IDERR_MDI_DSTOFFSET;   // TO_DO: mdi_mips.cpp ignores this?
            case MDIErrDstResource:
               ASSERT_NOW();
               return IDERR_MDI_DSTRESOURCE;
            default:
               ASSERT_NOW();
               return IDERR_MDI_INTERNAL; 
            }

         adCurrentAddr           += adBytesThisWrite;
         adRemainingBytesToWrite -= adBytesThisWrite;
         }
      while((adRemainingBytesToWrite != 0) &&
            (adRemainingBytesToWrite >= ulDefaultObjectSize));
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: MdiSaveConfigurationSettings
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Saves all configuration settings to a file
               This file is then loaded by the MDI_LIB
Date           Initials    Description
14-Nov-2008    SPC         Added support for Device Enable Password config.
28-Sep-2011    ANGS        Added multi-core support for MIPS
19-Oct-2011    ANGS        Modified for Zephyr trace support
****************************************************************************/
static int MdiSaveConfigurationSettings(void)
{

   int      iLoop;
   FILE    *pConfigFile;

   sprintf(szMdiConfigName,"%s",MDI_CONFIG_FILE_NAME);
   pConfigFile = fopen(szMdiConfigName, "w");
   if(pConfigFile == NULL)
      return IDERR_CREATE_CONFIG_FILE;

   // We use one section only...
   fprintf(pConfigFile, "[General]\n");
   // Finally all the common Settings
   if(tySI.tyGdbServerTarget == GDBSRVTGT_OPELLAXD)
      {
      fprintf(pConfigFile,"TargetType=16\n");
      }
   else
      {
      fprintf(pConfigFile,"TargetType=0\n");
      }

   //todo   fprintf(pConfigFile, "DeviceType=%d\n",                     (int)tySI.tyProcConfig.ProcType);
   fprintf(pConfigFile, "DMATurnedOn=%d\n",                    (int)(tySI.tyProcConfig.Processor._MIPS.bDMATurnedOn == 1));
   fprintf(pConfigFile, "RstTgtBforeConn=%d\n",                (int)(tySI.tyProcConfig.Processor._MIPS.bDoNotIssueHardReset == 0));
   fprintf(pConfigFile, "DisableIntSS=%d\n",                   (int)(tySI.tyProcConfig.Processor._MIPS.bDisableIntSS == 1));
   fprintf(pConfigFile, "SingleStepUsingSWBP=%d\n",            (int)(tySI.tyProcConfig.Processor._MIPS.bSingleStepUsingSWBP == 1));
   fprintf(pConfigFile, "UserHaltCountersIDM=%d\n",            (int)(tySI.tyProcConfig.Processor._MIPS.bUserHaltCountersIDM == 1));
   fprintf(pConfigFile, "MchpRecoveryByChipErase=%d\n",         0x00);
   if(tySI.tyProcConfig.Processor._MIPS.bCacheDebugEnabled)
      {
      fprintf(pConfigFile, "CacheFlushSupported=%d\n",         (int)(tySI.tyProcConfig.Processor._MIPS.bCacheFlushSupported == 1));
      fprintf(pConfigFile, "CacheFlushVerify=%d\n",            (int)(tySI.tyProcConfig.Processor._MIPS.bCacheFlushVerify == 1));
      fprintf(pConfigFile, "CacheFlushFilename=%s\n",          tySI.tyProcConfig.Processor._MIPS.szCacheFlushFilename);
      if(strcmp(tySI.tyProcConfig.Processor._MIPS.szCacheRelocationAddress,""))
         {
         int bValidCacheFlushLocation = FALSE;
         unsigned long ulCacheFlushLocation = StrToUlong(tySI.tyProcConfig.Processor._MIPS.szCacheRelocationAddress, 
                                                         &bValidCacheFlushLocation);
         if(bValidCacheFlushLocation)
#ifdef _LINUX
            fprintf(pConfigFile, "CacheFlushLocation=%d\n",    (int)ulCacheFlushLocation);
#else
            fprintf(pConfigFile, "CacheFlushLocation=%d\n",    (int)ulCacheFlushLocation);
#endif
         else
            fprintf(pConfigFile, "CacheFlushLocation=0\n" );
         }
      else
         fprintf(pConfigFile, "CacheFlushLocation=0\n" );
      }
   else
      {
      fprintf(pConfigFile, "CacheFlushSupported=0\n");
      fprintf(pConfigFile, "CacheFlushVerify=0\n"   );
      fprintf(pConfigFile, "CacheFlushFilename=\n"  );
      fprintf(pConfigFile, "CacheFlushLocation=0\n" );
      }
   fprintf(pConfigFile, "DSPRStart=%d\n",                      (int)tySI.tyProcConfig.Processor._MIPS.ulDataScratchPadRamStart);
   fprintf(pConfigFile, "DSPREnd=%d\n",                        (int)tySI.tyProcConfig.Processor._MIPS.ulDataScratchPadRamEnd);
   fprintf(pConfigFile, "ISPRStart=%d\n",                      (int)tySI.tyProcConfig.Processor._MIPS.ulInstScratchPadRamStart);
   fprintf(pConfigFile, "ISPREnd=%d\n",                        (int)tySI.tyProcConfig.Processor._MIPS.ulInstScratchPadRamEnd);
   fprintf(pConfigFile, "JtagFreqInKHz=%d\n",                  (int)(tySI.ulJtagFreqHz/1000));

   if(tySI.tyGdbServerTarget == GDBSRVTGT_OPELLA)
      {
      fprintf(pConfigFile,"JtagFreq=%d\n",tySI.uiJTAGFreqIndex);
      fprintf(pConfigFile,"ucNoUseEJTAGBoot=%d\n",(int)(tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseEJTAGBootDuringReset?0:1));
      }

   fprintf(pConfigFile, "EndianModeModifiable=%d\n",           (int)(tySI.tyProcConfig.bEndianModeModifiable == 1));
   // MIPS Specific Settings
   fprintf(pConfigFile, "EJTAGStyle=%d\n",                     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->EJTAGStyle);
   fprintf(pConfigFile, "usTargetSupply=%d\n",                 (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->usTargetSupply);
   fprintf(pConfigFile, "bUseSlowJTAG=%d\n",                   (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseSlowJTAG);
   fprintf(pConfigFile, "bEJTAGDMAOnly=%d\n",                  (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly);
   /* This flag is only for PNX5100 */
   fprintf(pConfigFile, "bNoMMUTranslation=%d\n",              (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bNoMMUTranslation);  //Dong Ji for PNX5100
//   fprintf(pConfigFile, "bFreeRunningJTAGClk=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bFreeRunningJTAGClk);
   fprintf(pConfigFile, "bUseEJTAGBootDuringReset=%d\n",       (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseEJTAGBootDuringReset);
   fprintf(pConfigFile, "bEJTAGBootSupported=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGBootSupported);
   fprintf(pConfigFile, "ulNumInstHWBps=%d\n",                 (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulNumInstHWBps);
   fprintf(pConfigFile, "ulNumDataHWBps=%d\n",                 (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulNumDataHWBps);
   fprintf(pConfigFile, "bHWBpASIDSupported=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bHWBpASIDSupported);
   fprintf(pConfigFile, "bOddAddressFor16BitModeBps=%d\n",     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bOddAddressFor16BitModeBps);
   fprintf(pConfigFile, "ulNumberOfNOPSInERETBDS=%d\n",        (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucNumberOfNOPSInERETBDS);
   fprintf(pConfigFile, "ulNumberOfNOPSInJUMPBDS=%d\n",        (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucNumberOfNOPSInJUMPBDS);
   fprintf(pConfigFile, "bSSIn16BitModeSupported=%d\n",        (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bSSIn16BitModeSupported);
   fprintf(pConfigFile, "bEnableMIPS16ModeForTrace=%d\n",      (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEnableMIPS16ModeForTrace);
   fprintf(pConfigFile, "bZephyrTraceSupport=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.bZephyrTraceSupport);
   if(((int)tySI.tyProcConfig.Processor._MIPS.uiNumberofCores) != 0)
   {
       fprintf(pConfigFile, "ucMultiCoreNumCores=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.uiNumberofCores);
       fprintf(pConfigFile, "ucMultiCoreCoreNum=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.bSelectedCore);
       fprintf(pConfigFile, "ucMultiCoreDMACoreNum=%d\n",          (int)tySI.tyProcConfig.Processor._MIPS.uiDMAcore);
   } else {
       fprintf(pConfigFile, "ucMultiCoreNumCores=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreNumCores);
       fprintf(pConfigFile, "ucMultiCoreCoreNum=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreCoreNum);
       fprintf(pConfigFile, "ucMultiCoreDMACoreNum=%d\n",          (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMultiCoreDMACoreNum);
   }
   fprintf(pConfigFile, "bPerformHardReset=%d\n",              (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformHardReset);
   fprintf(pConfigFile, "bPerformSoftProcessorReset=%d\n",     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformSoftProcessorReset);
   fprintf(pConfigFile, "bPerformSoftPeripheralReset=%d\n",    (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformSoftPeripheralReset);
   fprintf(pConfigFile, "bPerformHardBetweenSoftReset=%d\n",   (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bPerformHardBetweenSoftReset);
//   fprintf(pConfigFile, "bInvalidateCacheOnAReset=%d\n",       (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bInvalidateCacheOnAReset);
   fprintf(pConfigFile, "ulInvalidateInst=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulInvalidateInst);
   fprintf(pConfigFile, "ulInvalidateWBInst=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulInvalidateWBInst);
   fprintf(pConfigFile, "bOnlyFlushDataCache=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bOnlyFlushDataCache);
   fprintf(pConfigFile, "bDoubleReadOnDMA=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bDoubleReadOnDMA);
   fprintf(pConfigFile, "bAssumeWordAccess=%d\n",              (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bAssumeWordAccess);
   fprintf(pConfigFile, "bWriteEnableRequired=%d\n",           (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEnableWriteRequired);
   fprintf(pConfigFile, "ulEnableWriteAddress=%d\n",           (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEnableWriteAddress);
   fprintf(pConfigFile, "ulEnableWriteValue=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEnableWriteValue);
   fprintf(pConfigFile, "ulEnableWriteMask=%d\n",              (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEnableWriteMask);                               
//   fprintf(pConfigFile, "ulDebugMemStart=%d\n",                (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDebugMemStart);
//   fprintf(pConfigFile, "ulDebugMemEnd=%d\n",                  (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDebugMemEnd);
 //  fprintf(pConfigFile, "ucMultiCoreNumCores=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.uiNumberofCores);
   fprintf(pConfigFile, "ucIDCODEInstCode=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucIDCODEInstCode   );
   fprintf(pConfigFile, "ucLengthOfAddressRegister=%d\n",      (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucLengthOfAddressRegister);
   fprintf(pConfigFile, "bSelectAddrRegAfterEveryAccess=%d\n", (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bSelectAddrRegAfterEveryAccess);
   fprintf(pConfigFile, "bSyncInstNotSupported=%d\n",          (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bSyncInstNotSupported);
   fprintf(pConfigFile, "bAssertHaltTwice=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bAssertHaltTwice   );
   fprintf(pConfigFile, "bUseFastDMAFunc=%d\n",                (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseFastDMAFunc    );
   fprintf(pConfigFile, "ulCoreSelectCode=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulCoreSelectCode   );
   fprintf(pConfigFile, "ulCoreSelectIRLength=%d\n",           (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulCoreSelectIRLength);
   for(iLoop = 0; iLoop < MAX_CORES_IN_CHAIN; iLoop++)
   {//todo IRLen?? not in MDI?
       if(((int)tySI.tyProcConfig.Processor._MIPS.uiNumberofCores) != 0)
       {
           fprintf(pConfigFile, "ulIRLength[%d]=%d\n",   iLoop,     (int)tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[iLoop].ulIRlength);
           fprintf(pConfigFile, "ulBypassCode[%d]=%d\n", iLoop,     (int)tySI.tyProcConfig.Processor._MIPS.tyScanChainDef[iLoop].ulBypassInst);
       } else {
           fprintf(pConfigFile, "ulIRLength[%d]=%d\n",   iLoop,     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->tyMultiCoreDeviceInfo[iLoop].ulIRLength);
           fprintf(pConfigFile, "ulBypassCode[%d]=%d\n", iLoop,     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->tyMultiCoreDeviceInfo[iLoop].ulBypassCode);
       }
   }
   fprintf(pConfigFile, "ulDelayAfterResetMS=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDelayAfterResetMS );
   fprintf(pConfigFile, "ulResetDelayMS=%d\n",                 (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulResetDelayMS );
   fprintf(pConfigFile, "ucDMAWriteAfterReset=%d\n",           (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucDMAWriteAfterReset);
   fprintf(pConfigFile, "ulDMAResetWriteAddr=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDMAResetWriteAddr );
   fprintf(pConfigFile, "ulDMAResetWriteValue=%d\n",           (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulDMAResetWriteValue);
   fprintf(pConfigFile, "ucEJTAGSeq=%d\n",                     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucEJTAGSecSequence  );
   fprintf(pConfigFile,"bUseInstrWatchForBP=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseInstrWatchForBP);
   fprintf(pConfigFile,"ucMemReadAfterReset=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMemReadAfterReset);
   fprintf(pConfigFile,"bCoreSelectBeforeReset=%d\n",          (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bCoreSelectBeforeReset);
   fprintf(pConfigFile,"bUseRestrictedDmaAddressRange=%d\n",   (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bUseRestrictedDmaAddressRange);
   fprintf(pConfigFile,"ulStartOfValidDmaAddressRange=%d\n",   (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulStartOfValidDmaAddressRange);
   fprintf(pConfigFile,"ulEndOfValidDmaAddressRange=%d\n",     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulEndOfValidDmaAddressRange);
   fprintf(pConfigFile,"ucMemWriteBeforeReset=%d\n",           (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ucMemWriteBeforeReset);
   fprintf(pConfigFile,"ulMemResetWriteAddr=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulMemResetWriteAddr);
   fprintf(pConfigFile,"ulMemResetWriteValue=%d\n",            (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulMemResetWriteValue);
   fprintf(pConfigFile,"bMicroChipM4KFamily=%d\n",             (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bMicroChipM4KFamily);   
   fprintf(pConfigFile,"ulNumberOfShadowSetsPresent=%d\n",     (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulNumberOfShadowSetsPresent);
   fprintf(pConfigFile,"ulLimitCacheRdBpS=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitCacheRdBpS);
   fprintf(pConfigFile,"ulLimitCacheWrBpS=%d\n",               (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitCacheWrBpS);
   fprintf(pConfigFile,"ulLimitDmaRdBpS=%d\n",                 (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitDmaRdBpS);
   fprintf(pConfigFile,"ulLimitDmaWrBpS=%d\n",                 (int)tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->ulLimitDmaWrBpS);
   fprintf(pConfigFile,"szPasswordSequence=%s\n",        tySI.tyProcConfig.szPasswordSequence);
   fprintf(pConfigFile,"szPasswordFile1=%s\n",        tySI.tyProcConfig.szPasswordFile1);
   fprintf(pConfigFile,"szPasswordFile2=%s\n",        tySI.tyProcConfig.szPasswordFile2);
   fprintf(pConfigFile,"szPasswordFile3=%s\n",        tySI.tyProcConfig.szPasswordFile3);
   fprintf(pConfigFile,"ProcessorName=%s\n",                   tySI.tyProcConfig.szProcName);

   //todo: delete
   fprintf(pConfigFile,"LptNumber=1\n");
   fprintf(pConfigFile,"ComPortNumber=1\n");
   fprintf(pConfigFile,"ComPortBaud=5\n");
   fprintf(pConfigFile,"TargetConnection=4\n");
   fprintf(pConfigFile,"EthernetAddr=0.0.0.0\n");
   fprintf(pConfigFile,"DeviceType=9\n");
   fprintf(pConfigFile,"ulStartOfOnChipEEPROM=0\n");
   fprintf(pConfigFile,"ulEndOfOnChipEEPROM=0\n");
   fprintf(pConfigFile,"ulStartOfOnChipFLASH=0\n");
   fprintf(pConfigFile,"ulEndOfOnChipFLASH=0\n");
   fprintf(pConfigFile,"TraceType=0\n");
   fprintf(pConfigFile,"bFreeRunningJTAGClk=0\n");
   fprintf(pConfigFile,"bExtTrigSupported=0\n");
   fprintf(pConfigFile,"bInvalidateCacheOnAReset=0\n");
   fprintf(pConfigFile,"ulDebugMemStart=0\n");
   fprintf(pConfigFile,"ulDebugMemEnd=0\n");
   fprintf(pConfigFile,"ucGeniaWrAlignDelay=85\n");
   fprintf(pConfigFile,"ucGeniaRdAlignDelay=85\n");
   fprintf(pConfigFile,"ucOpUSBWrAlignDelay=1\n");
   fprintf(pConfigFile,"ucOpUSBRdAlignDelay=1\n");
   fprintf(pConfigFile,"szFpgaware=vtmipe26.abf\n");
   fprintf(pConfigFile,"ulTraceClockRatio=8\n");
   //todo
   if(strcmp(tySI.szProbeInstance, ""))
      fprintf(pConfigFile,"OpellaXDInstance=%s\n",                tySI.szProbeInstance);

   fflush(pConfigFile);
   fclose(pConfigFile);

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: MdiLoadLibrary
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Load MIPS MDI library
Date           Initials    Description
31-Jul-2008    NCH         Initial
12-Jun-2007    VH          Added Opella-XD support
19-Oct-2011    ANGS        Modified by adding AshTraceElfPath
21-Nov-2007    DJ          Added support for code download to PNX5100
30-Nov-2011    SV          Modified by adding AshConfigureTrace
****************************************************************************/
static int MdiLoadLibrary(void)
{
   int              ErrRet = NO_ERROR;
   int              iMDIRet;
   MDIDeviceIdT     tyMDIDeviceID;
   MDIConfigT       tyMDIConfig;
   MDIUint32        tyFlags;
   unsigned long    ulRtnAddress = 0;
   // Remove any BP details...
   RemoveAllBpArray();
   hMipsInstMdiDll    = NULL;
   szTargetDllName[0] = '\0';

   switch(tySI.tyGdbServerTarget)
      {
      case GDBSRVTGT_OPELLAXD:
         strcpy(szTargetDllName, OPELLAXD_DLL_NAME);
         break;
      case GDBSRVTGT_OPELLA:
         strcpy(szTargetDllName, OPELLAUSB_DLL_NAME);
         break;
      case GDBSRVTGT_INVALID:
      default:
         break;
      }

   if(szTargetDllName[0] != '\0')
      {
      hMipsInstMdiDll = LoadLibrary(szTargetDllName);
      }

   if(hMipsInstMdiDll == NULL)
      return IDERR_LOAD_DLL_FAILED;

   

   pfAshConfigureTrace       = (TyAshConfigureTrace)       GetProcAddress(hMipsInstMdiDll,AshConfigureTrace_Export_Name);
   pfAshTraceElfPath         = (TyAshTraceElfPath)         GetProcAddress(hMipsInstMdiDll,AshTraceElfPath_Export_Name);
   pfEnableOSMMU             = (TyEnableOSMMU)             GetProcAddress(hMipsInstMdiDll,OSMMU_Export_Name);
   pfAshInitOSAppMMU         = (TyAshInitOSAppMMU)         GetProcAddress(hMipsInstMdiDll,AshOSApp_Export_Name);
   pfAshFetchTrace           = (TyAshFetchTrace)           GetProcAddress(hMipsInstMdiDll,AshFetchTrace_Export_Name);
   pfMDICacheFlush           = (TyMDICacheFlush)           GetProcAddress(hMipsInstMdiDll,MDICacheFlush_Export_Name);
   pfMDIConnect              = (TyMDIConnect)              GetProcAddress(hMipsInstMdiDll,MDIConnect_Export_Name);
   pfMDIDisconnect           = (TyMDIDisconnect)           GetProcAddress(hMipsInstMdiDll,MDIDisconnect_Export_Name);
   pfMDIOpen                 = (TyMDIOpen)                 GetProcAddress(hMipsInstMdiDll,MDIOpen_Export_Name);
   pfMDIClose                = (TyMDIClose)                GetProcAddress(hMipsInstMdiDll,MDIClose_Export_Name);
   pfMDIRead                 = (TyMDIRead)                 GetProcAddress(hMipsInstMdiDll,MDIRead_Export_Name);
   pfMDIWrite                = (TyMDIWrite)                GetProcAddress(hMipsInstMdiDll,MDIWrite_Export_Name);
   pfMDIExecute              = (TyMDIExecute)              GetProcAddress(hMipsInstMdiDll,MDIExecute_Export_Name);
   pfMDIStep                 = (TyMDIStep)                 GetProcAddress(hMipsInstMdiDll,MDIStep_Export_Name);
   pfMDIStop                 = (TyMDIStop)                 GetProcAddress(hMipsInstMdiDll,MDIStop_Export_Name);
   pfMDIReset                = (TyMDIReset)                GetProcAddress(hMipsInstMdiDll,MDIReset_Export_Name);
   pfMDIRunState             = (TyMDIRunState)             GetProcAddress(hMipsInstMdiDll,MDIRunState_Export_Name);
   pfMDISetBp                = (TyMDISetBp)                GetProcAddress(hMipsInstMdiDll,MDISetBp_Export_Name);
   pfMDIClearBp              = (TyMDIClearBp)              GetProcAddress(hMipsInstMdiDll,MDIClearBp_Export_Name);
   pfAshErrorGetMessage      = (TyAshErrorGetMessage)      GetProcAddress(hMipsInstMdiDll,AshErrorGetMessage_Export_Name);
   pfAshGetDWFWVersions      = (TyAshGetDWFWVersions)      GetProcAddress(hMipsInstMdiDll,AshGetDWFWVersions_Export_Name);
   pfMDIEraseMrchpFlash      = (TyMDIEraseChipMircochipFlash)      GetProcAddress(hMipsInstMdiDll,MDIEraseMrchpFlash_Export_Name);
   pfMDISetupMrchpFlash      = (TyMDISetupMrchpFlash)      GetProcAddress(hMipsInstMdiDll,MDISetupMrchpFlash_Export_Name);
   pfMDISaveMrchpBmxRegs     = (TyMDISaveMrchpBmxRegs)     GetProcAddress(hMipsInstMdiDll,MDISaveMrchpBmxReg_Export_Name);
   pfMDIPartitionMrchpRam    = (TyMDIPartitionMrchpRam)    GetProcAddress(hMipsInstMdiDll,MDIPartitionMrchpRam_Export_Name);      
   pfAshScanIR               = (TyAshScanIR)               GetProcAddress(hMipsInstMdiDll,"AshScanIR");
   pfAshScanDR               = (TyAshScanDR)               GetProcAddress(hMipsInstMdiDll,"AshScanDR");

   // First check that all MDI compulsary functions are present
   if(
     !pfAshTraceElfPath          ||
     !pfAshScanIR                ||
     !pfAshScanDR                ||
     !pfAshFetchTrace            ||
     !pfMDICacheFlush            ||
     !pfMDIConnect               ||
     !pfMDIDisconnect            ||
     !pfMDIOpen                  ||
     !pfMDIClose                 ||
     !pfMDIRead                  ||
     !pfMDIWrite                 ||
     !pfMDIExecute               ||
     !pfMDIStep                  ||
     !pfMDIStop                  ||
     !pfMDIReset                 ||
     !pfMDIRunState              ||
     !pfMDISetBp                 ||
     !pfMDIClearBp               ||
	 !pfMDIEraseMrchpFlash       ||
     !pfAshErrorGetMessage       ||
     !pfMDISetupMrchpFlash       ||
     !pfMDISaveMrchpBmxRegs      ||
     !pfMDIPartitionMrchpRam)
      {
      return IDERR_LOAD_DLL_FAILED;
      }
   if((ErrRet = MdiSaveConfigurationSettings()) != NO_ERROR)
      return ErrRet;

   memset(&tyMDIConfig,0,sizeof(tyMDIConfig));

   // The following Call back functions are not optional
   tyMDIConfig.MDICBOutput = MDICBOutput;
   tyMDIConfig.MDICBInput  = MDICBInput;

   strcpy(tyMDIConfig.User,"Ashling MIPS GDB Server");

   iMDIRet = pfMDIConnect(MDICurrentRevision,&tyCurrentMDIHandle,&tyMDIConfig);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;    // Good do nothing
      case MDIErrFailure:
         return IDERR_GDI_ERROR;
      case MDIErrParam:
         return IDERR_MDI_PARAM;
      case MDIErrVersion:
      case MDIErrNoResource:
      case MDIErrAlreadyConnected:
      case MDIErrConfig:
      case MDIErrInvalidFunction:
         return IDERR_MDI_VERSION;
      default:
         return IDERR_MDI_INTERNAL;
      }

   tyMDIDeviceID = 0x0;

   // We are assuming this DLL does not support TargetGroups 
   // As such we use the MDIHandle instead of the TGHandle
   tyFlags = MDIExclusiveAccess;


   iMDIRet = pfMDIOpen(tyCurrentMDIHandle,tyMDIDeviceID,tyFlags,&tyCurrentDevHndl);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;    // Good do nothing
      case MDIErrFailure:
         return IDERR_GDI_ERROR;
      case MDIErrParam:
         return IDERR_MDI_PARAM;
      case MDIErrTGHandle:
         return IDERR_MDI_TGHANDLE;
      default:
         return IDERR_MDI_INTERNAL;
      }


   // We have reset the device and we should be able to write to registers now.
   // It is important to do this now as otherwise we might not be able to write
   // the cache invalidation routine right afterwards.
   if(!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
      {
      //We need to set endian before we set PC and other registers.
      if((ErrRet = SetInitialEndianMode()) != NO_ERROR)
         return ErrRet;
      if((ErrRet = SetupRegisterSettings()) != NO_ERROR)
         return ErrRet;

      // set up cache flush routines, if required...
      if(sscanf(tySI.tyProcConfig.Processor._MIPS.szCacheRelocationAddress, "%lx", &ulRtnAddress) == 1)
         {
         if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_CACHE_FLUSH_RTN_ADDR, ulRtnAddress)) != NO_ERROR)
            return ErrRet;
         }

      // Next We Invalidate the cache as it is always safe to do this after reset
      if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_INVALIDATE_CACHE_NOW, 0)) != NO_ERROR)
         return ErrRet;
      }

   //Set other initial variables
   MIPSInitVars();

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: MdiLoadLibraryCon
     Engineer: Vitezslav Hola
        Input: int bInitialJtagAccess : enable to use initial JTAG access (MIPS specific)
       Output: int
  Description: Load MDI library for using with JTAG console
Date           Initials    Description
08-Mar-2006    VH          Initial - derived from MDILoadLibrary
12-Jun-2007    VH          Added support for Opella-XD
****************************************************************************/
static int MdiLoadLibraryCon(int bInitialJtagAccess)
{
   int         ErrRet = NO_ERROR;
   int             iMDIRet;
   MDIDeviceIdT    tyMDIDeviceID;
   MDIConfigT      tyMDIConfig;
   MDIUint32       tyFlags;
   unsigned long   ulRtnAddress = 0;

   // Remove any BP details...
   RemoveAllBpArray();
   hMipsInstMdiDll    = NULL;
   szTargetDllName[0] = '\0';

   switch(tySI.tyGdbServerTarget)
      {
      case GDBSRVTGT_OPELLAXD:
         strcpy(szTargetDllName, OPELLAXD_DLL_NAME);
         break;
      case GDBSRVTGT_OPELLA:
         strcpy(szTargetDllName, OPELLAUSB_DLL_NAME);
         break;
      case GDBSRVTGT_INVALID:
      default:
         break;
      }

   if(szTargetDllName[0] != '\0')
      {
      hMipsInstMdiDll = LoadLibrary(szTargetDllName);
      }

   if(hMipsInstMdiDll == NULL)
      return IDERR_LOAD_DLL_FAILED;
   // similar to MDI interface, using pfAshMDIOpenEx instead of MDIOpen, using subset of original MDI
   // added low level JTAG functions
   pfMDIConnect         = (TyMDIConnect)              GetProcAddress(hMipsInstMdiDll,MDIConnect_Export_Name);
   pfMDIDisconnect      = (TyMDIDisconnect)           GetProcAddress(hMipsInstMdiDll,MDIDisconnect_Export_Name);
   pfMDIClose           = (TyMDIClose)                GetProcAddress(hMipsInstMdiDll,MDIClose_Export_Name);
   pfMDIRead            = (TyMDIRead)                 GetProcAddress(hMipsInstMdiDll,MDIRead_Export_Name);
   pfMDIWrite           = (TyMDIWrite)                GetProcAddress(hMipsInstMdiDll,MDIWrite_Export_Name);
   pfMDIReset           = (TyMDIReset)                GetProcAddress(hMipsInstMdiDll,MDIReset_Export_Name);
   pfAshErrorGetMessage = (TyAshErrorGetMessage)      GetProcAddress(hMipsInstMdiDll,AshErrorGetMessage_Export_Name);
   pfAshGetDWFWVersions = (TyAshGetDWFWVersions)      GetProcAddress(hMipsInstMdiDll,AshGetDWFWVersions_Export_Name);
   pfAshMDIOpenEx       = (TyAshMDIOpenEx)            GetProcAddress(hMipsInstMdiDll,"AshMDIOpenEx");
   pfAshScanIR          = (TyAshScanIR)               GetProcAddress(hMipsInstMdiDll,"AshScanIR");
   pfAshScanDR          = (TyAshScanDR)               GetProcAddress(hMipsInstMdiDll,"AshScanDR");
   pfAshScanDRLong      = (TyAshScanDRLong)           GetProcAddress(hMipsInstMdiDll,"AshScanDRLong");
   pfAshSetJtagFrequency= (TyAshSetJtagFrequency)     GetProcAddress(hMipsInstMdiDll,"AshSetJtagFrequency");

   pfMDISetupMrchpFlash   = (TyMDISetupMrchpFlash)    GetProcAddress(hMipsInstMdiDll,MDISetupMrchpFlash_Export_Name)   ;
   pfMDIEraseMrchpFlash   = (TyMDIEraseChipMircochipFlash)    GetProcAddress(hMipsInstMdiDll,MDIEraseMrchpFlash_Export_Name)   ;
   pfMDISaveMrchpBmxRegs  = (TyMDISaveMrchpBmxRegs)   GetProcAddress(hMipsInstMdiDll,MDISaveMrchpBmxReg_Export_Name)  ;
   pfMDIPartitionMrchpRam = (TyMDIPartitionMrchpRam)  GetProcAddress(hMipsInstMdiDll,MDIPartitionMrchpRam_Export_Name)  ;

   // Check for interface functions
   if(
     !pfMDIConnect               ||
     !pfMDIDisconnect            ||
     !pfAshMDIOpenEx           ||
     !pfMDIClose                 ||
     !pfMDIRead                  ||
     !pfMDIWrite                 ||
     !pfMDIReset                 ||
     !pfAshScanIR              ||
     !pfAshScanDR              ||
     !pfAshSetJtagFrequency    ||  
     !pfAshErrorGetMessage)
      {
      return IDERR_LOAD_DLL_FAILED;
      }

   if( (tySI.tyGdbServerTarget == GDBSRVTGT_OPELLA) && !pfAshScanDRLong)
      {
      return IDERR_LOAD_DLL_FAILED;
      }
   if((ErrRet = MdiSaveConfigurationSettings()) != NO_ERROR)
      return ErrRet;

   memset(&tyMDIConfig,0,sizeof(tyMDIConfig));

   // The following Call back functions are not optional
   tyMDIConfig.MDICBOutput = MDICBOutput;
   tyMDIConfig.MDICBInput  = MDICBInput;

   strcpy(tyMDIConfig.User,"Ashling MIPS GDB Server");

   iMDIRet = pfMDIConnect(MDICurrentRevision,&tyCurrentMDIHandle,&tyMDIConfig);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;    // Good do nothing
      case MDIErrFailure:
         return IDERR_GDI_ERROR;
      case MDIErrParam:
         return IDERR_MDI_PARAM;
      case MDIErrVersion:
      case MDIErrNoResource:
      case MDIErrAlreadyConnected:
      case MDIErrConfig:
      case MDIErrInvalidFunction:
         return IDERR_MDI_VERSION;
      default:
         return IDERR_MDI_INTERNAL;
      }
   tyMDIDeviceID = 0x0;

   // We are assuming this DLL does not support TargetGroups 
   // As such we use the MDIHandle instead of the TGHandle
   tyFlags = MDIExclusiveAccess;

   // if no device specified, do not use initial JTAG access
   iMDIRet = (int)pfAshMDIOpenEx(tyCurrentMDIHandle,
                                 tyMDIDeviceID,
                                 tyFlags,
                                 &tyCurrentDevHndl, 
                                 bInitialJtagAccess);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;    // Good do nothing
      case MDIErrFailure:
         return IDERR_GDI_ERROR;
      case MDIErrParam:
         return IDERR_MDI_PARAM;
      case MDIErrTGHandle:
         return IDERR_MDI_TGHANDLE;
      default:
         return IDERR_MDI_INTERNAL;
      }
   // do as MDIOpen when bInitialJtagAccess is set
   if(bInitialJtagAccess)
      {
      // We have reset the device and we should be able to write to registers now.
      // It is important to do this now as otherwise we might not be able to write
      // the cache invalidation routine right afterwards.

      if((ErrRet = SetupRegisterSettings()) != GDBSERV_NO_ERROR)
         return ErrRet;

      if((ErrRet = SetInitialEndianMode()) != GDBSERV_NO_ERROR)
         return ErrRet;

      // set up cache flush routines, if required...
      if(sscanf(tySI.tyProcConfig.Processor._MIPS.szCacheRelocationAddress, "%lx", &ulRtnAddress) == 1)
         {
         if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_CACHE_FLUSH_RTN_ADDR, ulRtnAddress)) != GDBSERV_NO_ERROR)
            return ErrRet;
         }

      // Next We Invalidate the cache as it is always safe to do this after reset
      if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_INVALIDATE_CACHE_NOW, 0)) != GDBSERV_NO_ERROR)
         return ErrRet;
      }
   //Set other initial variables
   MIPSInitVars();

   return GDBSERV_NO_ERROR;
}


/****************************************************************************
     Function: MdiFreeLibrary
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Free MIPS MDI library
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
static int MdiFreeLibrary(void)
{
   int      iMDIRet;

   // Remove any BP details...
   RemoveAllBpArray();

   // if library not loaded, get out...
   if(hMipsInstMdiDll == NULL)
      return GDBSERV_NO_ERROR;

   if(pfMDIClose != NULL)
      {
      iMDIRet = pfMDIClose(tyCurrentDevHndl,MDICurrentState);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;
         case MDIErrFailure:
            return IDERR_GDI_ERROR;
         case MDIErrParam:
            return IDERR_MDI_PARAM;
         default:
            return IDERR_MDI_INTERNAL;
         }
      }

   if(pfMDIDisconnect != NULL)
      {
      iMDIRet = pfMDIDisconnect(tyCurrentMDIHandle,MDICurrentState);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;
         case MDIErrFailure:
            return IDERR_GDI_ERROR;
         case MDIErrParam:
            return IDERR_MDI_PARAM;
         case MDIErrMDIHandle:
            return IDERR_MDI_HANDLE;
         default:
            return IDERR_MDI_INTERNAL;
         }
      }


   (void)FreeLibrary(hMipsInstMdiDll);

   hMipsInstMdiDll     = NULL;

   pfMDIConnect          = NULL;
   pfMDIDisconnect       = NULL;
   pfMDIOpen             = NULL;
   pfMDIClose            = NULL;
   pfMDIRead             = NULL;
   pfMDIWrite            = NULL;
   pfMDIExecute          = NULL;
   pfMDIStep             = NULL;
   pfMDIStop             = NULL;
   pfMDIReset            = NULL;
   pfMDIRunState         = NULL;
   pfMDISetBp            = NULL;
   pfMDIClearBp          = NULL;
   pfAshErrorGetMessage  = NULL;
   pfAshGetDWFWVersions  = NULL;
   pfMDISetupMrchpFlash  = NULL;
   pfMDIEraseMrchpFlash  = NULL;
   pfMDISaveMrchpBmxRegs = NULL;
   pfMDIPartitionMrchpRam =NULL;

   return GDBSERV_NO_ERROR;
}


/****************************************************************************
     Function: MdiFreeLibraryCon
     Engineer: Vitezslav Hola
        Input: none
       Output: int - error code
  Description: Free MIPS MDI library (use with JTAG console)
Date           Initials    Description
08-Mar-2006    VH          Initial
****************************************************************************/
static int MdiFreeLibraryCon(void)
{
   int      iMDIRet;

   // Remove any BP details...
   RemoveAllBpArray();

   // if library not loaded, get out...
   if(hMipsInstMdiDll == NULL)
      return GDBSERV_NO_ERROR;

   if(pfMDIClose != NULL)
      {
      iMDIRet = pfMDIClose(tyCurrentDevHndl,MDICurrentState);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;
         case MDIErrFailure:
            return IDERR_GDI_ERROR;
         case MDIErrParam:
            return IDERR_MDI_PARAM;
         default:
            return IDERR_MDI_INTERNAL;
         }
      }

   if(pfMDIDisconnect != NULL)
      {
      iMDIRet = pfMDIDisconnect(tyCurrentMDIHandle,MDICurrentState);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;
         case MDIErrFailure:
            return IDERR_GDI_ERROR;
         case MDIErrParam:
            return IDERR_MDI_PARAM;
         case MDIErrMDIHandle:
            return IDERR_MDI_HANDLE;
         default:
            return IDERR_MDI_INTERNAL;
         }
      }


   (void)FreeLibrary(hMipsInstMdiDll);

   hMipsInstMdiDll      = NULL;

   pfMDIConnect             = NULL;
   pfMDIDisconnect          = NULL;
   pfAshMDIOpenEx       = NULL;
   pfMDIClose               = NULL;
   pfMDIRead                = NULL;
   pfMDIWrite               = NULL;
   pfMDIReset               = NULL;
   pfAshErrorGetMessage  = NULL;
   pfAshGetDWFWVersions  = NULL;
   pfAshScanIR           = NULL;
   pfAshScanDR           = NULL;
   pfAshScanDRLong       = NULL;
   pfAshSetJtagFrequency = NULL;

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: GetRegAddr
     Engineer: Nikolay Chokoev
        Input: register name
       Output: TRUE if the register is found, FALSE if not
  Description: return ARC register address by register name
Date           Initials    Description
20-Nov-2007    NCH         Initial
****************************************************************************/
int GetRegAddr(char *pRegName, int *iRegAddr)
{
   int iIndex;


   for(iIndex=0; iIndex < (int)MAX_MIPS_REG; iIndex++)
      {
      if(strcasecmp(ptyMipsReg[iIndex].pszRegName, pRegName) == 0)
         {
         *iRegAddr = (int)ptyMipsReg[iIndex].ulAddrIndex;
         return TRUE;
         }
      }
   *iRegAddr = 0xFFFF;
   return FALSE;
}

/****************************************************************************
     Function: MdiResetTarget
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: Reset as per ResetType
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
static int MdiResetTarget(TXRXResetTypes tyResetType)
{
   int         iMDIRet;
   int     ErrRet;
   MDIUint32   Mode;
   int        bSetRegSettings = TRUE;
   unsigned long       ulRtnAddress    = 0;

   if(!pfMDIReset)
      {
      ASSERT_NOW();
      return IDERR_DLL_NOT_LOADED;
      }

   switch(tyResetType)
      {
      default:
      case RES_EMULATOR:
      case RES_PROCESSOR:
         Mode = MDIFullReset;
         break;
      case RES_VIA_PRRST:
         Mode = MDIPrRstReset;
         break;
      case RES_VIA_PERRST:
         Mode = MDIPerRstReset;
         bSetRegSettings=FALSE;
         break;
      case RES_VIA_JTAGRST:
         Mode = MDIJtagRstReset;
         bSetRegSettings=FALSE;
         break;
      case RES_VIA_TRST:
         Mode = MDITRSTReset;
         bSetRegSettings=FALSE;
         break;
      case RES_VIA_RST:
         Mode = MDIRSTReset;
         break;
      }

   iMDIRet = pfMDIReset(tyCurrentDevHndl,Mode);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         ASSERT_NOW();
         return IDERR_GDI_ERROR;       // Calls Setup Error Message
      case MDIErrDevice:
         ASSERT_NOW();
         return IDERR_MDI_DEVICE; 
      default:
         ASSERT_NOW();
         return IDERR_MDI_INTERNAL; 
      }


   if(bSetRegSettings)
      {
      if((ErrRet = SetupRegisterSettings()) != NO_ERROR)
         return ErrRet;
      if((ErrRet = SetInitialEndianMode()) != NO_ERROR)
         return ErrRet;
      }

   // set up cache flush routines, if required...
   if(sscanf(tySI.tyProcConfig.Processor._MIPS.szCacheRelocationAddress, "%lx", &ulRtnAddress) == 1)
      {
      if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_CACHE_FLUSH_RTN_ADDR, ulRtnAddress)) != NO_ERROR)
         return ErrRet;
      }

   // Next We Invalidate the cache as it is always safe to do this after reset
   if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_INVALIDATE_CACHE_NOW, 0)) != NO_ERROR)
      return ErrRet;

   return GDBSERV_NO_ERROR;
}

unsigned char SearchExtraReg(unsigned int uiIndex, unsigned long* ulAddr)
{
   unsigned int i;
   for(i=0; i < MAX_EXTRA_MIPS_REG; i++)
      {
      if(uiIndex == ptyMipsExtraReg[i].uiRegIndex)
         {
         *ulAddr = ptyMipsExtraReg[i].ulAddr;
         NOREF(ptyMipsExtraReg[i].pszRegName);
         return TRUE;
         }
      }
   *ulAddr = 0;
   return FALSE;
}

/****************************************************************************
     Function: GetTxRegNumber
     Engineer: Nikolay Chokoev
        Input: none
       Output: unsigned long
  Description: return Ashling TxRx reg number for GDB reg number
Date           Initials    Description
31-Jul-2008    NCH         Initial
25-Mar-2006    PJS         use register definitions (.REG file support)
28-Jun-2006    PJS         support extra peripheral registers
****************************************************************************/
static unsigned long GetTxRegNumber(unsigned long ulGdbRegNumber, int *pbValid)
{
   TyRegDef    *ptyRegDef;
   unsigned long ulAddrs;

   if(ulGdbRegNumber < MAX_MIPS_REG)
      {
      if(ptyMipsReg[ulGdbRegNumber].bCoreReg)
         {
         *pbValid = TRUE;
         return ptyMipsReg[ulGdbRegNumber].ulAddrIndex;
         }

      if(ptyMipsReg[ulGdbRegNumber].ulAddrIndex != 0xFFFF)
         {
         if((ptyRegDef = GetRegDefForIndex((int)ptyMipsReg[ulGdbRegNumber].ulAddrIndex)) != NULL)
            {
            *pbValid = TRUE;
            return ptyRegDef->ulAddress;
            }
         }
      }
   else
      {
      if(SearchExtraReg(ulGdbRegNumber,&ulAddrs))
         {
         *pbValid = TRUE;
         return ulAddrs;
         }
      }
   *pbValid = FALSE;
   return 0xFFFFFFFF;
}

/****************************************************************************
     Function: GetRegName
     Engineer: Nikolay Chokoev
        Input: none
       Output: unsigned long
  Description: return MIPS reg name for GDB reg number
Date           Initials    Description
28-Feb-2006    PJS         Initial
****************************************************************************/
static const char  * GetRegName(unsigned long ulGdbRegNumber)
{
   static char szRegName[16];

   if(ulGdbRegNumber < MAX_MIPS_REG)
      return ptyMipsReg[ulGdbRegNumber].pszRegName;

   sprintf(szRegName, "0x%lX", ulGdbRegNumber);
   return szRegName;
}

/****************************************************************************
     Function: GetBpTypeName
     Engineer: Nikolay Chokoev
        Input: none
       Output: unsigned long
  Description: return BP type name
Date           Initials    Description
28-Feb-2006    PJS         Initial
****************************************************************************/
static const char  * GetBpTypeName(unsigned long ulBpType)
{
   static char szBpName[16];

   switch(ulBpType)
      {
      case BPTYPE_SOFTWARE:
         return "SW";
      case BPTYPE_HARDWARE:
         return "HW";
      case BPTYPE_WRITE:
      case BPTYPE_READ:
      case BPTYPE_ACCESS:
      default:
         sprintf(szBpName, "0x%lX", ulBpType);
         return szBpName;
      }
}


/****************************************************************************
     Function: ReadFullRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: *ptyRegDef   : pointer to register definition struct
               *pulRegValue : storage for register value
       Output: int
  Description: Read full register using a register definition
Date           Initials    Description
19-Mar-2006    PJS         Initial
****************************************************************************/
static int ReadFullRegisterByRegDef(TyRegDef  *ptyRegDef,
                                    unsigned long     *pulRegValue)
{
   int              ErrRet;
   unsigned long                ulValue;
   unsigned long                ulCount;
   TXRXMemAccessTypes   tyMemAccessType;

   if((TyMemTypes)ptyRegDef->ubMemType == MEM_REG)
      {
      if((ErrRet = MdiReadReg((unsigned long)ptyRegDef->ulAddress, pulRegValue)) != NO_ERROR)
         return ErrRet;

      return GDBSERV_NO_ERROR;
      }

   // Otherwise its a Memory mapped register, default is virtual address...
   if((TyMemTypes)ptyRegDef->ubMemType == MEM_PHYSICL)
      tyMemAccessType = MEM_PHYSICAL_ACCESS;
   else
      tyMemAccessType = MEM_VIRTUAL_ACCESS;

   if((ulCount = (unsigned long)ptyRegDef->ubByteSize) > 4)
      ulCount = 4;

   if((ErrRet = MdiReadMem(tyMemAccessType,
                           ptyRegDef->ulAddress,
                           ulCount,
                           (unsigned char*)&ulValue,
                           0)) != NO_ERROR)
      return ErrRet;

   // handle endianness...
   switch(ulCount)
      {
      case 1:
         break;

      case 2:
         if(LOW_IsBigEndian(0))
            ulValue = (unsigned long)SwapUshortBytes((unsigned short)ulValue);
         break;

      case 4:
      default:
         if(LOW_IsBigEndian(0))
            ulValue = SwapUlongBytes(ulValue);
         break;
      }

   *pulRegValue = ulValue;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: WriteFullRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: *ptyRegDef   : pointer to register definition struct
               ulRegValue   : register value to write
       Output: int
  Description: Write full register using a register definition
Date           Initials    Description
19-Mar-2006    PJS         Initial
****************************************************************************/
static int WriteFullRegisterByRegDef(TyRegDef  *ptyRegDef,
                                     unsigned long     ulRegValue)
{
   int              ErrRet;
   unsigned long                ulCount;
   TXRXMemAccessTypes   tyMemAccessType;

   if((TyMemTypes)ptyRegDef->ubMemType == MEM_REG)
      {
      if((ErrRet = MdiWriteReg((unsigned long)ptyRegDef->ulAddress, ulRegValue)) != NO_ERROR)
         return ErrRet;

      return GDBSERV_NO_ERROR;
      }

   // Otherwise its a Memory mapped register, default is virtual address...
   if((TyMemTypes)ptyRegDef->ubMemType == MEM_PHYSICL)
      tyMemAccessType = MEM_PHYSICAL_ACCESS;
   else
      tyMemAccessType = MEM_VIRTUAL_ACCESS;

   if((ulCount = (unsigned long)ptyRegDef->ubByteSize) > 4)
      ulCount = 4;

   // handle endianness...
   switch(ulCount)
      {
      case 1:
         break;

      case 2:
         if(LOW_IsBigEndian(0))
            ulRegValue = (unsigned long)SwapUshortBytes((unsigned short)ulRegValue);
         break;

      case 4:
      default:
         if(LOW_IsBigEndian(0))
            ulRegValue = SwapUlongBytes(ulRegValue);
         break;
      }

   if((ErrRet = MdiWriteMem(tyMemAccessType,
                            ptyRegDef->ulAddress,
                            ulCount,
                            (unsigned char*)&ulRegValue,
                            ulCount,
                            0)) != NO_ERROR)
      return ErrRet;

   return GDBSERV_NO_ERROR;
}


//==============================================
// global LOW_functions from here...
//==============================================

/****************************************************************************
     Function: LOW_Connect
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: Connect to target
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_Connect(void)
{
   int ErrRet;

   //if (tySI.bDebugOutput)
   //   PrintMessage(LOG_MESSAGE, "Connecting target ...");


   if((ErrRet = MdiLoadLibrary()) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      (void)MdiFreeLibrary();
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Disconnect
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: Disconnect from target
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_Disconnect(void)
{
   int ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Disconnecting target ...");

   if((ErrRet = MdiFreeLibrary()) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }
   if(NULL != tySI.tyProcConfig.Processor._MIPS.ptyMDInfo)
      free(tySI.tyProcConfig.Processor._MIPS.ptyMDInfo);
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IsProcessorExecuting
     Engineer: Nikolay Chokoev
        Input: *pbExecuting : pointer to flag for result
       Output: int : FALSE if error
  Description: Checks if processor is executing
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_IsProcessorExecuting(volatile int *pbExecuting, 
                             int iCurrentCoreIndex)
{
   int          iMDIRet;
   MDIRunStateT tyRunState;

   iCurrentCoreIndex = iCurrentCoreIndex;

   if(1 == tySI.bIsConnected)
      {
      iMDIRet = pfMDIRunState(tyCurrentDevHndl,0,&tyRunState);
      switch(iMDIRet)
         {
         case MDISuccess:
            break;      // Good do nothing
         case MDIErrFailure:
            SetupErrorMessage(IDERR_GDI_ERROR);
            return GDBSERV_ERROR;
         case MDIErrDevice:
            SetupErrorMessage(IDERR_MDI_DEVICE);
            return GDBSERV_ERROR;
         default:
            SetupErrorMessage(IDERR_MDI_INTERNAL);
            return GDBSERV_ERROR;
         }

      switch(tyRunState.Status & MDIStatusMask)
         {
         case MDIStatusRunning:
            tySI.tyProcConfig.Processor._MIPS.bRunningFlag[iCurrentCoreIndex] = TRUE;
            *pbExecuting = TRUE;
            break;
         default:
            tySI.tyProcConfig.Processor._MIPS.bRunningFlag[iCurrentCoreIndex] = FALSE;
            *pbExecuting = FALSE;
            break;
         }
      }
   else
      {
      tySI.tyProcConfig.Processor._MIPS.bRunningFlag[iCurrentCoreIndex] = FALSE;
      *pbExecuting = FALSE;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_GetCauseOfBreak
     Engineer: Nikolay Chokoev
        Input: *ptyStopSignal : storage for cause of break
       Output: int : FALSE if error
  Description: Gets cause of last break
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_GetCauseOfBreak(TyStopSignal *ptyStopSignal)
{
   int          iMDIRet;
   MDIRunStateT tyRunState; 

   iMDIRet= pfMDIRunState(tyCurrentDevHndl,0,&tyRunState);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   switch((tyRunState.Status & MDIStatusMask))
      {
      case MDIStatusStepsDone:
      case MDIStatusBPHit:
      case MDIStatusInstBPHit:
      case MDIStatusDataReadBPHit:
      case MDIStatusDataWriteBPHit:
      case MDIStatusUsrBPHit:
      case MDIStatusNotRunning:
         *ptyStopSignal = TARGET_SIGTRAP;
         break;

      case MDIStatusHalted:
         *ptyStopSignal = TARGET_SIGINT;
         break;

      case MDIStatusExited:
      case MDIStatusException:
      case MDIStatusTraceFull:
      default:
         *ptyStopSignal = TARGET_SIGTERM;
         break;
      }


   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadRegisters
     Engineer: Nikolay Chokoev
        Input: *pulRegisters : storage for registers
       Output: int : FALSE if error
  Description: Reads registers
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_ReadRegisters(unsigned long *pulRegisters)
{
   unsigned long i;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Read all registers.");

   for(i=0; i < GDBSERV_NUM_REGISTERS; i++)
      {
      if(GDBSERV_NO_ERROR != LOW_ReadRegister(i, &pulRegisters[i]))
         return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadRegister
     Engineer: Nikolay Chokoev
        Input: ulRegisterNumber  : register to read
               *pulRegisterValue : storage for one register
       Output: int : FALSE if error
  Description: Reads one register
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_ReadRegister(unsigned long   ulRegisterNumber,
                     unsigned long  *pulRegisterValue)
{
   int  ErrRet;
   unsigned long    ulReg;
   int     bValidReg = FALSE;

   ulReg = GetTxRegNumber(ulRegisterNumber, &bValidReg);

   if(!bValidReg)
      {
      if(tySI.bDebugOutput)
         PrintMessage(LOG_MESSAGE, "Read register %s ...", GetRegName(ulRegisterNumber));

      SetupErrorMessage(IDERR_ASH_SRCREGISTER);
      *pulRegisterValue = BAD_HEX_VALUE;
      // having displayed an error message, we give good RC
      return GDBSERV_NO_ERROR;
      }

   if(ulReg == 0xFFFFFFFF)
      {
      *pulRegisterValue = BAD_HEX_VALUE;
      }
   else if((ErrRet = MdiReadReg((unsigned long)ulReg, pulRegisterValue)) != NO_ERROR)
      {
      if(tySI.bDebugOutput)
         PrintMessage(LOG_MESSAGE, "Read register %s ...", GetRegName(ulRegisterNumber));

      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Read register %s value=0x%08lX.", GetRegName(ulRegisterNumber), *pulRegisterValue);

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WriteRegisters
     Engineer: Nikolay Chokoev
        Input: *pulRegisters : registers values to write
       Output: int : FALSE if error
  Description: Writes registers
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_WriteRegisters(unsigned long *pulRegisters)
{
   unsigned long i;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Write all registers.");

   for(i=0; i < GDBSERV_NUM_REGISTERS; i++)
      {
      if(GDBSERV_NO_ERROR != LOW_WriteRegister(i, pulRegisters[i]))
         return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WriteRegister
     Engineer: Nikolay Chokoev
        Input: ulRegisterNumber  : register to write
               ulRegisterValue   : register value
       Output: int : FALSE if error
  Description: writes one register
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_WriteRegister(unsigned long ulRegisterNumber,
                      unsigned long ulRegisterValue)
{
   int  ErrRet;
   unsigned long    ulReg;
   int     bValidReg = FALSE;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Write register %s value=0x%08lX.", GetRegName(ulRegisterNumber), ulRegisterValue);

   ulReg = GetTxRegNumber(ulRegisterNumber, &bValidReg);

   if(ulReg == 0xFFFFFFFF)
      {
      SetupErrorMessage(IDERR_ASH_DSTREGISTER);
      // having displayed an error message, we give good RC
      return GDBSERV_NO_ERROR;
      }

   if((ErrRet = MdiWriteReg((unsigned long)ulReg, ulRegisterValue)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadPC
     Engineer: Nikolay Chokoev
        Input: *pulRegisterValue : storage for PC
       Output: int : FALSE if error
  Description: Reads PC register
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_ReadPC(unsigned long  *pulRegisterValue)
{
   int  ErrRet;

   if((ErrRet = MdiReadReg(REG_MIPS_PC, pulRegisterValue)) != NO_ERROR)
      {
      if(tySI.bDebugOutput)
         PrintMessage(LOG_MESSAGE, "Read register PC ...");

      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Read register PC value=0x%08lX.", *pulRegisterValue);

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WritePC
     Engineer: Nikolay Chokoev
        Input: ulRegisterValue   : new PC value
       Output: int : FALSE if error
  Description: writes PC register
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_WritePC(unsigned long ulRegisterValue)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Write register PC value=0x%08lX.", ulRegisterValue);

   if((ErrRet = MdiWriteReg(REG_MIPS_PC, ulRegisterValue)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ReadMemory
     Engineer: Nikolay Chokoev
        Input: ulAddress  : address to read from
               ulCount    : number of bytes to read
               *pucData   : storage for data
       Output: int : FALSE if error
  Description: reads memory
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_ReadMemory(unsigned long          ulAddress,
                   unsigned long          ulCount,
                   unsigned char *pucData)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Read Memory at 0x%08lX for length 0x%08lX.", ulAddress, ulCount);

   if((ErrRet = MdiReadMem(MEM_VIRTUAL_ACCESS,
                           ulAddress,
                           ulCount,
                           pucData,
                           0)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   if(tySI.bDebugOutput)
      {
      unsigned long ulLineOffset, ulByteOffset, ulBytesInLine;
      char  szBuffer[_MAX_PATH];
      char  szByte[16];

      for(ulLineOffset=0; ulLineOffset<ulCount; ulLineOffset+=16)
         {
         sprintf(szBuffer, "0x%08lX:", ulAddress+ulLineOffset);

         ulBytesInLine = ulCount-ulLineOffset;
         if(ulBytesInLine > 16)
            ulBytesInLine = 16;

         for(ulByteOffset=0; ulByteOffset<ulBytesInLine; ulByteOffset++)
            {
            sprintf(szByte, " %02X", pucData[ulLineOffset+ulByteOffset]);
            strcat(szBuffer, szByte);
            }

         PrintMessage(LOG_MESSAGE, szBuffer);
         }
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WriteMemory
     Engineer: Nikolay Chokoev
        Input: ulAddress  : address to write to
               ulCount    : number of bytes to write
               *pucData   : pointer to data to write
       Output: int : FALSE if error
  Description: writes memory
Date           Initials    Description
31-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_WriteMemory(unsigned long          ulAddress,
                    unsigned long          ulCount,
                    unsigned char *pucData)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      {
      unsigned long ulLineOffset, ulByteOffset, ulBytesInLine;
      char  szBuffer[_MAX_PATH];
      char  szByte[16];

      PrintMessage(LOG_MESSAGE, "Write Memory at 0x%08lX for length 0x%08lX.", ulAddress, ulCount);

      for(ulLineOffset=0; ulLineOffset<ulCount; ulLineOffset+=16)
         {
         sprintf(szBuffer, "0x%08lX:", ulAddress+ulLineOffset);

         ulBytesInLine = ulCount-ulLineOffset;
         if(ulBytesInLine > 16)
            ulBytesInLine = 16;

         for(ulByteOffset=0; ulByteOffset<ulBytesInLine; ulByteOffset++)
            {
            sprintf(szByte, " %02X", pucData[ulLineOffset+ulByteOffset]);
            strcat(szBuffer, szByte);
            }

         PrintMessage(LOG_MESSAGE, szBuffer);
         }
      }

   if((ErrRet = MdiWriteMem(MEM_VIRTUAL_ACCESS,
                            ulAddress,
                            ulCount,
                            pucData,
                            ulCount,
                            0)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SupportedBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
       Output: int : TRUE if BP type supported, else false
  Description: indicate which BP types are supported
Date           Initials    Description
03-Jul-2008    NCH         Initial
18-May-2010    SPC         Added Read/Write watchpoint support in GDBServer
****************************************************************************/
int LOW_SupportedBreakpoint(unsigned long ulBpType)
{
   switch(ulBpType)
      {
      case BPTYPE_SOFTWARE:
         return GDBSERV_NO_ERROR;
      case BPTYPE_HARDWARE:
         return GDBSERV_NO_ERROR;
      case BPTYPE_WRITE:
         return GDBSERV_NO_ERROR;
      case BPTYPE_READ:
         return GDBSERV_NO_ERROR;
      case BPTYPE_ACCESS:
         return GDBSERV_NO_ERROR;
      default:
         return GDBSERV_ERROR;
      }
}

/****************************************************************************
     Function: LOW_DeleteBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
       Output: int : FALSE if error
  Description: delete a breakpoint
Date           Initials    Description
03-Jul-2008         Initial
18-May-2010    SPC         Added Read/Write watchpoint support in GDBServer
23-Jun-2010    RSB	      Modification for Adv BP in PFXD
****************************************************************************/
int LOW_DeleteBreakpoint(unsigned long ulBpType, 
						 unsigned long ulAddress, 
						 unsigned long ulLength, 
						 int iCurrentCoreIndex)
{
   int          ErrRet;
   int          iMDIRet;
   int          iBpIndex;
   unsigned long ulIdentifier;

   iCurrentCoreIndex = iCurrentCoreIndex;
   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Delete %s breakpoint at 0x%08lX for length %ld.", GetBpTypeName(ulBpType), ulAddress, ulLength);

   //In PFXD,Adv BP should not be deleted by GDB command.
   if(tySI.bPfxdMode)
	  {
	  if(SearchforAdvancedBpEntry(ulAddress))
		 {
		 //Adv Bp found,so just return no error
		 return GDBSERV_NO_ERROR;
		 }

	  }

   if(GDBSERV_NO_ERROR != LOW_SupportedBreakpoint(ulBpType))
      {
      assert(FALSE);
      return GDBSERV_ERROR;//lint !e527
      }

   // We assume that the length is used for specifing whether or not
   // the BP is for a 32bit or 16bit instruction.

   if((ulLength == 0) && (ulBpType == BPTYPE_HARDWARE))
      ulLength = 4;

   /*if ((ulLength != 2) && (ulLength != 4))
      {
      assert(FALSE);
      return GDBSERV_ERROR;//lint !e527
      }*/

   // check if we know about this BP...
   iBpIndex = GetBpArrayEntry(ulBpType,
                              ulAddress,
                              ulLength,
                              &ulIdentifier,
                              0);
   if(iBpIndex < 0)
      {
		//This scenario can come in case of advanced BPs
		if(tySI.bPfxdMode)
		{
			return GDBSERV_NO_ERROR;
		}

      // Why didn't we find the BP?
      SetupErrorMessage(IDERR_REMOVE_BP_NOT_FOUND);
      return GDBSERV_ERROR;
      }


   // We found a matching BP so clear it...
   iMDIRet=pfMDIClearBp(tyCurrentDevHndl, ulIdentifier);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   if((ErrRet = RemoveBpArrayEntry((unsigned int)iBpIndex,0)) != 0)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: LOW_DeleteAdvancedBreakpoint
     Engineer: Rejeesh S Babu
        Input: ulAddress : breakpoint address
       Output: int : FALSE if error
  Description: Delete an Advanced Breakpoint
Date           Initials    Description
23-Jun-2010      RSB	      Initial
****************************************************************************/
int LOW_DeleteAdvancedBreakpoint(unsigned long ulAddress)
{
	int          ErrRet;
	int          iMDIRet;
	int          iBpIndex;
	unsigned long ulIdentifier;

	if (tySI.bDebugOutput)
		PrintMessage(LOG_MESSAGE, "Delete Adv breakpoint at 0x%08lX",ulAddress);


	// check if we know about this BP...
	iBpIndex = GetBpArrayEntry(0,
										ulAddress,
										0,
										&ulIdentifier,
										0);
	if (iBpIndex < 0)
	{
		// Why didn't we find the BP?
		SetupErrorMessage(IDERR_REMOVE_BP_NOT_FOUND);
		return GDBSERV_ERROR;
	}


	// We found a matching BP so clear it...
	iMDIRet=pfMDIClearBp(tyCurrentDevHndl, ulIdentifier);
	switch (iMDIRet)
	{
	case MDISuccess:
		break;		// Good do nothing
	case MDIErrFailure:
		SetupErrorMessage(IDERR_GDI_ERROR);
		return GDBSERV_ERROR;
	case MDIErrDevice:
		SetupErrorMessage(IDERR_MDI_DEVICE);
		return GDBSERV_ERROR;
	default:
		SetupErrorMessage(IDERR_MDI_INTERNAL);
		return GDBSERV_ERROR;
	}

	if ((ErrRet = RemoveBpArrayEntry((unsigned int)iBpIndex,0)) != 0)
	{
		(void)DeleteAdvancedBpArrayEntry(ulAddress);
		SetupErrorMessage(ErrRet);
		return GDBSERV_ERROR;
	}
	if ((ErrRet = DeleteAdvancedBpArrayEntry(ulAddress)) != 0)
	{
		SetupErrorMessage(ErrRet);
		return GDBSERV_ERROR;
	}

	return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SetAdvanceBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint address mask length
               ulData    : data break point data 
               ulDataMask: data break point mask data
             ibASIDenable: ASID is enabled
             ulASIDValue : ASID value
             ulTransType : used to know weather it is load,store or 
                           don't care
       Output: int : FALSE if error
  Description: set a breakpoint
Date           Initials    Description
02-Jun-2010    DK          Added support for advanced break points.
23-Jun-2010		RSB		   Adv Breakpoints added to a new array
26-Nov-2011     SV         Added support for Trigger & Trigger only breakpoints
****************************************************************************/
int LOW_SetAdvanceBreakpoint(unsigned long ulBpType,
                      unsigned long ulAddress,
                      unsigned long ulLength, 
                      unsigned long ulData,
                      unsigned long ulDataMask,
                      int ibASIDenable,
                      unsigned long ulASIDValue ,
                      unsigned long ulTransType)
{
   int 			ErrRet;
   int 			iMDIRet;
   int 			iBpIndex ;
   MDIBpDataT 	tyBpData;
   int      iIsDataPresent = 0x0;
   unsigned long ulIdentifier;
   unsigned long ulCheckType; //for checking hardware trigger breakpoint

   ulCheckType=ulBpType;
   iIsDataPresent = ulBpType & 0xF0;
   ulBpType = ulBpType & 0xF;
   memset( &tyBpData , 0 , sizeof(MDIBpDataT));

   if (tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Set %s breakpoint at 0x%08lX for length %ld.", GetBpTypeName(ulBpType), ulAddress, ulLength);

   iBpIndex = GetBpArrayEntryForAddr(ulAddress,0);

 
   if (iBpIndex >= 0)
      {
	  //A BP already set in this address.This will be a BP set using GDB command
   	  //This BP should be removed before setting the advanced PB
      iBpIndex = GetBpArrayEntry(ulBpType,
                                 ulAddress,
                                 ulLength,
                                 &ulIdentifier,
                                 0);
      if (iBpIndex < 0)
         {
         // Why didn't we find the BP?
         SetupErrorMessage(IDERR_REMOVE_BP_NOT_FOUND);
         return GDBSERV_ERROR;
         }
      iMDIRet=pfMDIClearBp(tyCurrentDevHndl, ulIdentifier);
      switch (iMDIRet)
         {
         case MDISuccess:
            break;      // Good do nothing
         case MDIErrFailure:
            SetupErrorMessage(IDERR_GDI_ERROR);
            return GDBSERV_ERROR;
         case MDIErrDevice:
            SetupErrorMessage(IDERR_MDI_DEVICE);
            return GDBSERV_ERROR;
         default:
            SetupErrorMessage(IDERR_MDI_INTERNAL);
            return GDBSERV_ERROR;
         }

      if ((ErrRet = RemoveBpArrayEntry((unsigned int)iBpIndex,0)) != 0)
         {
         SetupErrorMessage(ErrRet);
         return GDBSERV_ERROR;
         }

      }

   if (GDBSERV_NO_ERROR != LOW_SupportedBreakpoint(ulBpType))
      {
      //assert(FALSE);
      return GDBSERV_ERROR;//lint !e527
      }

  if(ibASIDenable == 1)
      {
      tyBpData.Resource    = MDIMIPVIRTUAL + ulASIDValue;
      }
   else
      {
      tyBpData.Resource    = MDIMIPGVIRTUAL;
      }
   /*Set the breakpoint address*/
   tyBpData.Range.Start = ulAddress;
   tyBpData.PassCount   = 1;
   tyBpData.Enabled     = 1;

   

   switch (ulBpType)
      {
      case MDIBPT_SWInstruction:
         tyBpData.Type = MDIBPT_SWInstruction;
         break;
      case MDIBPT_HWInstruction:/*Create details for hardware instruction breakpoint */
     
          tyBpData.Type =  MDIBPT_HWInstruction |MDIBPT_HWFlg_AddrMask ;
 
          /*Check for hardware instruction trigger flag*/
         if ( ( ulCheckType & MDIBPT_HWFlg_Trigger )== MDIBPT_HWFlg_Trigger )
            {
            tyBpData.Type |= MDIBPT_HWFlg_Trigger;
            }
         /*Check for trigger only condition*/
         else if ( ( ulCheckType & MDIBPT_HWFlg_TriggerOnly )== MDIBPT_HWFlg_TriggerOnly )
            {
            tyBpData.Type |= MDIBPT_HWFlg_TriggerOnly;
            }
         
        // tyBpData.Type |= MDIBPT_HWFlg_AddrMask;
         tyBpData.Range.End = ulLength;
         break;

   
      case MDIBPT_HWData:/*Create details for hardware datapoint*/
         tyBpData.Type =  MDIBPT_HWData ;

         /*Check for hardware data trigger flag*/
         if ( ( ulCheckType & MDIBPT_HWFlg_Trigger )== MDIBPT_HWFlg_Trigger )
            {
            tyBpData.Type |= MDIBPT_HWFlg_Trigger;
            }
         /*Check for trigger only condition*/
         else if ( ( ulCheckType & MDIBPT_HWFlg_TriggerOnly )== MDIBPT_HWFlg_TriggerOnly )
            {
            tyBpData.Type |= MDIBPT_HWFlg_TriggerOnly;
            }
         //Check for Transacrtion Type -load ,store ,Don't care
         if( ulTransType == 0x3)
            {
            tyBpData.Type = tyBpData.Type| MDIBPT_HWFlg_DataRead | MDIBPT_HWFlg_DataWrite;
            }
         else
            {
            if(ulTransType == 0x1)
               {
               tyBpData.Type = tyBpData.Type|MDIBPT_HWFlg_DataRead;
               }
            else if(ulTransType == 0x2)
               {
               tyBpData.Type = tyBpData.Type|MDIBPT_HWFlg_DataWrite;
               }
            }
         //tyBpData.Type = tyBpData.Type | MDIBPT_HWFlg_TriggerOnly | MDIBPT_HWFlg_Trigger;
         tyBpData.Type |= MDIBPT_HWFlg_AddrMask ;
         tyBpData.Range.End = ulLength;
         if(iIsDataPresent == 0x10)
           //if((iIsDataPresent = (ulBpType && 0xF0)) == 1) 
            {
            tyBpData.Type |= MDIBPT_HWFlg_DataValue;
            tyBpData.Data = ulData;
            tyBpData.Type |= MDIBPT_HWFlg_DataMask ; 
            tyBpData.DataMask  =  ulDataMask;
            }
         break;
      default:
    	 return GDBSERV_ERROR;//lint !e527
      }

   if (ulLength == 2)
      tyBpData.Range.Start = tyBpData.Range.Start | MDIMIP_Flg_MIPS16;

   iMDIRet=pfMDISetBp(tyCurrentDevHndl,&tyBpData);
   switch (iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      case MDIErrRange:
         SetupErrorMessage(IDERR_MDI_RANGE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   if ((ErrRet = AddBpArrayEntry(ulBpType,
                                 ulAddress,
                                 ulLength,
                                 tyBpData.Id)) != NO_ERROR)
	  {
			   SetupErrorMessage(ErrRet);
			   return GDBSERV_ERROR;
      }
   AddAdvancedBpArrayEntry(ulAddress);
   return GDBSERV_NO_ERROR;
}


/****************************************************************************
     Function: LOW_SetBreakpoint
     Engineer: Nikolay Chokoev
        Input: ulBpType  : breakpoint type
               ulAddress : breakpoint address
               ulLength  : breakpoint length
       Output: int : FALSE if error
  Description: set a breakpoint
Date           Initials    Description
31-Jul-2008    NCH         Initial
18-May-2010    SPC         Added Read/Write watchpoint support in GDBServer
****************************************************************************/
int LOW_SetBreakpoint(unsigned long ulBpType,
                      unsigned long ulAddress,
                      unsigned long ulLength, 
                      int iCurrentCoreIndex)
{
   int          ErrRet;
   int          iMDIRet;
   int          iBpIndex;
   MDIBpDataT   tyBpData;

   iCurrentCoreIndex = iCurrentCoreIndex;
   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Set %s breakpoint at 0x%08lX for length %ld.", GetBpTypeName(ulBpType), ulAddress, ulLength);

   iBpIndex = GetBpArrayEntryForAddr(ulAddress,0);
   if(iBpIndex >= 0)
      {
      // MIPS diskware does not support more than one BP at an address,
      // so ash-mips-gdb-server will also have to reject.
      // Note that GDB considers it OK to have more than one BP at an address.

	  // For PFXD,we shouldnt return error too.
      if (tySI.bPfxdMode)
         {
         return GDBSERV_NO_ERROR;
         } 
      else
         {
         SetupErrorMessage(IDERR_ADD_BP_DUPLICATE);
         return GDBSERV_ERROR;
         }
     }

   if(GDBSERV_NO_ERROR != LOW_SupportedBreakpoint(ulBpType))
      {
      //assert(FALSE);
      return GDBSERV_ERROR;//lint !e527
      }

   // We assume that the length is used for specifing whether or not
   // the BP is for a 32bit or 16bit instruction.

   if((ulLength == 0) && (ulBpType == BPTYPE_HARDWARE))
      ulLength = 4;

   /*if ((ulLength != 2) && (ulLength != 4))
      {
      assert(FALSE);
      return GDBSERV_ERROR;//lint !e527
      }*/

   tyBpData.Resource    = MDIMIPGVIRTUAL;
   tyBpData.Range.Start = ulAddress;
   tyBpData.PassCount   = 1;
   tyBpData.Enabled     = 1;

   switch(ulBpType)
      {
      case BPTYPE_SOFTWARE:
         tyBpData.Type = MDIBPT_SWInstruction;
         break;
      case BPTYPE_HARDWARE:
         tyBpData.Type = MDIBPT_HWInstruction;
         break;
      case BPTYPE_WRITE:
         tyBpData.Type = MDIBPT_HWData | MDIBPT_HWFlg_DataWrite;
         tyBpData.Range.End = (MDIOffsetT)ulAddress + ulLength - 1;
         break;
      case BPTYPE_READ:
         tyBpData.Type = MDIBPT_HWData | MDIBPT_HWFlg_DataRead;
         tyBpData.Range.End = (MDIOffsetT)ulAddress + ulLength - 1;
         break;
      case BPTYPE_ACCESS:
         tyBpData.Type = MDIBPT_HWData | MDIBPT_HWFlg_DataRead | MDIBPT_HWFlg_DataWrite;
         tyBpData.Range.End = (MDIOffsetT)ulAddress + ulLength - 1;
         break;
      default:
         return GDBSERV_ERROR;//lint !e527
      }

   if(ulLength == 2)
      tyBpData.Range.Start = tyBpData.Range.Start | MDIMIP_Flg_MIPS16;

   iMDIRet=pfMDISetBp(tyCurrentDevHndl,&tyBpData);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      case MDIErrRange:
         SetupErrorMessage(IDERR_MDI_RANGE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   if((ErrRet = AddBpArrayEntry(ulBpType,
                                ulAddress,
                                ulLength,
                                tyBpData.Id)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }
      //display();

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Continue
     Engineer: Nikolay Chokoev
        Input: *ptyStopSignal : storage for cause of break
       Output: int : FALSE if error
  Description: start execution of target
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_Continue(TyStopSignal *ptyStopSignal,
                 int iCurrentCore)
{
   int  iMDIRet;

   ptyStopSignal = ptyStopSignal;
   iCurrentCore = iCurrentCore;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Continue.");

   iMDIRet=pfMDIExecute(tyCurrentDevHndl);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }
   tySI.tyProcConfig.Processor._MIPS.bRunningFlag[iCurrentCore] = TRUE;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Exexute
     Engineer: Nikolay Chokoev
        Input: void
       Output: int : FALSE if error
  Description: start execution of target for download without listening to server
Date           Initials    Description
03-Jul-2008    NCH          Initial
****************************************************************************/
int LOW_Execute(char* pszDownloadFileName)
{
   int           iMDIRet;
//   int          bExecuting = FALSE;

   PrintMessage(DEBUG_MESSAGE, "Execute.");

   iMDIRet=pfMDIExecute(tyCurrentDevHndl);
   switch(iMDIRet)
      {
      case MDISuccess:
         pszDownloadFileName = pszDownloadFileName ;
         // PrintMessage(INFO_MESSAGE,"Executing %s, press any key to halt.", pszDownloadFileName);
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Step
     Engineer: Nikolay Chokoev
        Input: *ptyStopSignal : storage for cause of break
       Output: int : FALSE if error
  Description: step
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_Step(TyStopSignal *ptyStopSignal)
{
   int    iMDIRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Step.");

   // TO_DO: Assumming that step won't return until step is finished.
   // This would normally be a safe assumption...
   iMDIRet=pfMDIStep(tyCurrentDevHndl,1,MDIStepInto);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   if(GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(ptyStopSignal))
      return GDBSERV_ERROR;

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Halt
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: halt execution of target
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_Halt(int iCurrentCoreIndex)
{
   int  iMDIRet;

   iCurrentCoreIndex = iCurrentCoreIndex;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Halt.");

   iMDIRet=pfMDIStop(tyCurrentDevHndl);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }
   tySI.tyProcConfig.Processor._MIPS.bRunningFlag[iCurrentCoreIndex] = FALSE;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_ResetTarget
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: reset target
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_ResetTarget(void)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Reset target.");

   if((ErrRet = MdiResetTarget(RES_PROCESSOR)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_GetDWFWVersions
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: get diskware firmware version information
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_GetDWFWVersions(char *pszDiskWare1,
                        char *pszFirmWare,
                        char *pszFpgaWare1,
                        char *pszFpgaWare2,
                        char *pszFpgaWare3,
                        char *pszFpgaWare4)
{
   int  ErrRet;

   pszDiskWare1[0] = '\0';
   pszFirmWare [0] = '\0';
   pszFpgaWare1[0] = '\0';
   pszFpgaWare2[0] = '\0';
   pszFpgaWare3[0] = '\0';
   pszFpgaWare4[0] = '\0';

   //todo do we need these?

   if(pfAshGetDWFWVersions != NULL)
      {
      ErrRet=(int)pfAshGetDWFWVersions(pszDiskWare1,
                                       pszFirmWare,
                                       pszFpgaWare1,
                                       pszFpgaWare2,
                                       pszFpgaWare3,
                                       pszFpgaWare4);
      if(ErrRet != NO_ERROR)
         {
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
         }
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IsBigEndian
     Engineer: Nikolay Chokoev
        Input: none
       Output: int
  Description: determine if processor is in Big Endian mode
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_IsBigEndian(int iCoreIndex)
{
   //Update register cache before returing.
   if(tySI.bIsConnected)
      IsBigEndian();
   iCoreIndex = iCoreIndex;
   return tySI.tyProcConfig.Processor._MIPS.bProcIsBigEndian[iCoreIndex];
}


/****************************************************************************
     Function: LOW_ConnectJcon
     Engineer: Nikolay Chokoev
        Input: int bInitialJtagAccess : enable using initial jtag access (MIPS specific)
       Output: int : FALSE if error
  Description: Connect to target (use for JTAG console)
Date           Initials    Description
03-Jul-2008    NCH          Initial
****************************************************************************/
int LOW_ConnectJcon(int bInitialJtagAccess)
{
   int ErrRet;

   //if (tySI.bDebugOutput)
   //   PrintMessage(LOG_MESSAGE, "Connecting target ...");

   if((ErrRet = MdiLoadLibraryCon(bInitialJtagAccess)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      (void)MdiFreeLibraryCon();
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_DisconnectJcon
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: Disconnect from target (use for JTAG console)
Date           Initials    Description
03-Jul-2008    NCH          Initial
****************************************************************************/
int LOW_DisconnectJcon(void)
{
   int ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Disconnecting target ...");

   if((ErrRet = MdiFreeLibraryCon()) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }
   if(NULL != tySI.tyProcConfig.Processor._MIPS.ptyMDInfo)
      {
      free(tySI.tyProcConfig.Processor._MIPS.ptyMDInfo);
      tySI.tyProcConfig.Processor._MIPS.ptyMDInfo = NULL;
      }
   return GDBSERV_NO_ERROR;
}


/****************************************************************************
     Function: LOW_ScanIR
     Engineer: Nikolay Chokoev
        Input: unsigned long * pulDataOut : array of unsigned long to be shifted into IR
               unsigned long * pulDataIn  : array of unsigned long for data to be shifted from IR
               unsigned long ulDataLength : number of bits shifted into/from IR
       Output: int : FALSE if error
  Description: shift specified data into IR and read data shifted out
Date           Initials    Description
03-Jul-2008    NCH          Initial
****************************************************************************/
int LOW_ScanIR(unsigned long *pulDataIn, 
               unsigned long *pulDataOut, 
               unsigned long ulDataLength)
{
   assert(pulDataOut != NULL);
   assert(pulDataIn  != NULL);

   // call a function in DLL
   if(pfAshScanIR != NULL)
      {
      pfAshScanIR(pulDataIn, pulDataOut, ulDataLength);
      return GDBSERV_NO_ERROR;
      }
   else
      return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_ScanDR
     Engineer: Nikolay Chokoev
        Input: unsigned long * pulDataOut : array of unsigned long to be shifted into DR
               unsigned long * pulDataIn  : array of unsigned long for data to be shifted from DR
               unsigned long ulDataLength : number of bits shifted into/from DR
       Output: int : FALSE if error
  Description: shift specified data into DR and read data shifted out
Date           Initials    Description
03-Jul-2008    NCH          Initial
****************************************************************************/
int LOW_ScanDR(unsigned long *pulDataIn, 
               unsigned long *pulDataOut, 
               unsigned long ulDataLength)
{
   assert(pulDataOut != NULL);
   assert(pulDataIn  != NULL);

   // call a function in DLL
   if(pfAshScanDR != NULL)
      {
      pfAshScanDR(pulDataIn, pulDataOut, ulDataLength);
      return GDBSERV_NO_ERROR;
      }
   else
      return GDBSERV_ERROR;
}
/****************************************************************************
     Function: LOW_ScanDRLong
     Engineer: Sureh P.C
        Input: unsigned long * pulDataOut : array of unsigned long to be shifted into DR
               unsigned long * pulDataIn  : array of unsigned long for data to be shifted from DR
               unsigned long ulDataLength : number of bits shifted into/from DR
       Output: int : FALSE if error
  Description: shift specified data into DR and read data shifted out
Date           Initials    Description
07-Dec-2009    SPC         Initial
****************************************************************************/
int LOW_ScanDRLong(unsigned long *pulDataIn, 
                   unsigned long *pulDataOut, 
                   unsigned long ulDataLength)
{
   assert(pulDataOut != NULL);
   assert(pulDataIn  != NULL);

   // call a function in DLL
   if(pfAshScanDRLong != NULL)
      {
      pfAshScanDRLong(pulDataIn, pulDataOut, ulDataLength);
      return GDBSERV_NO_ERROR;
      }
   else
      return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_ReadMem
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddr : location start address
               unsigned long ulLength    : location length in bytes
               unsigned char *pbBuffer   : pointer to buffer
               unsigned long ulDataSize  : size of data to read (1 for bytes, etc.)
       Output: int : FALSE if error
  Description: read specific location in memory, if enable-dma-mode specified, use address
               as physical otherwise as virtual
               
Date           Initials    Description
24-Mar-2006    VH          Initial
****************************************************************************/
int LOW_ReadMem(unsigned long ulStartAddr, unsigned long ulLength, 
                unsigned char *pbBuffer, unsigned long ulDataSize)
{
   int bErrRet = TRUE;

   assert(pbBuffer != NULL);
   assert(ulDataSize > 0);

   if(tySI.tyProcConfig.Processor._MIPS.bDMATurnedOn)
      {
      // DMA mode is on, use address as physical 
      if(MdiReadMem(MEM_PHYSICAL_ACCESS, ulStartAddr, ulLength, pbBuffer, ulDataSize) != NO_ERROR)
         bErrRet = FALSE;
      else
         bErrRet = TRUE;
      }
   else
      {
      // DMA mode is off, use address as virtual 
      if(MdiReadMem(MEM_VIRTUAL_ACCESS, ulStartAddr, ulLength, pbBuffer, ulDataSize) != NO_ERROR)
         bErrRet = FALSE;
      else
         bErrRet = TRUE;
      }
   return bErrRet;
}


/****************************************************************************
     Function: LOW_WriteMem
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddr : location start address
               unsigned long ulLength    : location length in bytes
               unsigned long ulDataSize  : size of data to read (1 for bytes, etc.)
               unsigned long ulValue     : value to be written to location
       Output: int : FALSE if error
  Description: write pattern to specific location in memory,
               if enable-dma-mode specified, use address as physical 
               otherwise as virtual
Date           Initials    Description
24-Mar-2006    VH          Initial
****************************************************************************/
int LOW_WriteMem(unsigned long ulStartAddr, unsigned long ulLength, 
                 unsigned long ulDataSize,  unsigned long ulValue)
{
   unsigned int uiIndex;
   int bWriteSuccess;
   unsigned char *ulWriteData;

   assert(ulDataSize > 0);

   bWriteSuccess = TRUE;

   ulWriteData = (unsigned char*)malloc(ulLength);
   if(ulWriteData == NULL)
      return FALSE;

   memset(ulWriteData, 0, ulLength);
   for(uiIndex = 0; uiIndex < ulLength; uiIndex += ulDataSize)
      {
      memcpy((ulWriteData + uiIndex), &ulValue, ulDataSize);
      }
   // must be implemented as repetead writing 
   if(tySI.tyProcConfig.Processor._MIPS.bDMATurnedOn)
      {
      // DMA mode is on, use address as physical
      if(MdiWriteMem(MEM_PHYSICAL_ACCESS, ulStartAddr, ulLength, ulWriteData, ulLength, ulDataSize) != NO_ERROR)
         bWriteSuccess = FALSE;
      }
   else
      {
      // DMA mode is off, use address as virtual
      if(MdiWriteMem(MEM_VIRTUAL_ACCESS, ulStartAddr, ulLength, ulWriteData, ulLength, ulDataSize) != NO_ERROR)
         bWriteSuccess = FALSE;
      }

   free(ulWriteData);
   return bWriteSuccess;
}

/****************************************************************************
     Function: LOW_ResetTAP
     Engineer: Nikolay Chokoev
        Input: none
       Output: int : FALSE if error
  Description: reset Jtag TAP
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_ResetTAP(void)
{
   int  ErrRet;

   if((ErrRet = MdiResetTarget(RES_VIA_JTAGRST)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SetJtagFreq
     Engineer: Vitezslav Hola
        Input: double udJtagFreq : value for frequency (depends on device type)
       Output: int : FALSE if error
  Description: set jtag frequency
Date           Initials    Description
20-Mar-2006    VH          Initial
09-May-2008    SPC         Changed the parameter type to double for passing
                           JTAG frequency in MHz.
****************************************************************************/
int LOW_SetJtagFreq(void)
{
   double udJtagFreq;

   udJtagFreq = (double)tySI.ulJtagFreqHz/1000000;
   if((pfAshSetJtagFrequency != NULL) && 
      (pfAshSetJtagFrequency(udJtagFreq) == NO_ERROR))
      return GDBSERV_NO_ERROR;
   else
      return GDBSERV_ERROR;
}

/****************************************************************************
     Function: LOW_ReadRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               unsigned long *pulRegValue - storage for register value
       Output: int - error code
  Description: Read a register using a register definition
Date           Initials    Description
26-Nov-2007    NCH         Initial
****************************************************************************/
int LOW_ReadRegisterByRegDef(TyRegDef *ptyRegDef, 
                             unsigned long *pulRegValue)
{
   int iError;
   unsigned long ulValue = 0;
   assert(ptyRegDef != NULL);
   assert(pulRegValue != NULL);
   if((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulValue)) != GDBSERV_NO_ERROR)
      {
      if(tySI.bDebugOutput)
         PrintMessage(LOG_MESSAGE, 
                      "Read register %s ...", 
                      GetRegDefDescription(ptyRegDef));
      SetupErrorMessage(iError);
      return GDBSERV_ERROR;
      }
   *pulRegValue = ExtractRegBits(ulValue, ptyRegDef);
   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, 
                   "Read register %s value=0x%08lX.", 
                   GetRegDefDescription(ptyRegDef), *pulRegValue);
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_WriteRegisterByRegDef
     Engineer: Nikolay Chokoev
        Input: TyRegDef *ptyRegDef - pointer to register definition struct
               unsigned long ulRegValue - register value to write
       Output: int - error code
  Description: Write a register using a register definition
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_WriteRegisterByRegDef(TyRegDef *ptyRegDef,
                              unsigned long ulRegValue)
{
   int iError;
   unsigned long ulOldValue;
   assert(ptyRegDef != NULL);
   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, 
                   "Write register %s value=0x%08lX.", 
                   GetRegDefDescription(ptyRegDef), ulRegValue);
   if(ptyRegDef->ubBitSize < 32)
      {
      if((iError = ReadFullRegisterByRegDef(ptyRegDef, &ulOldValue)) != GDBSERV_NO_ERROR)
         {
         SetupErrorMessage(iError);
         return GDBSERV_ERROR;
         }
      ulRegValue = MergeRegBits(ulOldValue, ulRegValue, ptyRegDef);
      }
   if((iError = WriteFullRegisterByRegDef(ptyRegDef, ulRegValue)) != GDBSERV_NO_ERROR)
      {
      SetupErrorMessage(iError);
      return GDBSERV_ERROR;
      }
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_AddCoreRegDefArrayEntries
     Engineer: Nikolay Chokoev
        Input: none
       Output: int - error code
  Description: Add register definitions for core registers
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_AddCoreRegDefArrayEntries(void)
{
   int  ErrRet;
   unsigned long    ulGdbReg;

   // add the PC
   if((ErrRet = CFG_AddRegDefArrayEntry("PC",
                                        REG_MIPS_PC,
                                        MEM_REG,
                                        4, 0, 32, -1, 1)) != NO_ERROR)
      return ErrRet;

   // add core registers 1..31 (AT..RA)
   for(ulGdbReg = REG_MIPS_AT; ulGdbReg <= REG_MIPS_RA; ulGdbReg++)
      {
      if((ErrRet = CFG_AddRegDefArrayEntry(ptyMipsReg[ulGdbReg].pszRegName,
                                           ptyMipsReg[ulGdbReg].ulAddrIndex,
                                           MEM_REG,
                                           4, 0, 32, -1, 1)) != NO_ERROR)
         return ErrRet;
      }
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_IndexCommonRegDefArrayEntries
     Engineer: Nikolay Chokoev
        Input: none
       Output: int - error code
  Description: Index common registers in reg definition array for fast access
Date           Initials    Description
03-Jul-2008    NCH         Initial
****************************************************************************/
int LOW_IndexCommonRegDefArrayEntries(void)
{
   int          iMipsIndex;
   unsigned int uiRegDefIndex;
   TyRegDef     *ptyRegDef;
   TyRegDef     *ptyLastFullRegDef = NULL;

   for(uiRegDefIndex=0; uiRegDefIndex < tySI.uiRegDefArrayEntryCount; uiRegDefIndex++)
      {
      ptyRegDef = &tySI.ptyRegDefArray[uiRegDefIndex];
      if(ptyRegDef->ubBitSize >= 32)
         ptyLastFullRegDef = ptyRegDef;

      for(iMipsIndex=0; iMipsIndex < (int)MAX_MIPS_REG; iMipsIndex++)
         {
         if((ptyMipsReg[iMipsIndex].bCoreReg    == FALSE)      &&
            (ptyMipsReg[iMipsIndex].ulAddrIndex == 0xFFFFFFFF) &&
            (strcasecmp(ptyMipsReg[iMipsIndex].pszRegName, ptyRegDef->pszRegName) == 0))
            {
            ptyMipsReg[iMipsIndex].ulAddrIndex = (unsigned long)uiRegDefIndex;
            }
         }

      // special case for the endian mode bit
      if((iRegDefEndianMode < 0)                                  &&
         (ptyLastFullRegDef != NULL)                              &&
         (strcasecmp(ptyLastFullRegDef->pszRegName, "Config") == 0) &&
         (strcasecmp(ptyRegDef->pszRegName, tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName) == 0))
         {
         iRegDefEndianMode = (int)uiRegDefIndex;
         }
      }


   // In case someone changes the XML file <ModeRegBitName Name=BE> entry!!
   // Only END and BE bits are valid for ModeRegBitName
   if((strcasecmp(tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName, "END") != 0) &&
      (strcasecmp(tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName, "BE" ) != 0))
      {
      sprintf(tySI.szError, "Invalid entry <ModeRegBitName Name=\"%s\"> in XML file,\nvalid endian mode bit names in the Config register are \"BE\" or \"END\".",
              tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName);
      return GDBSERV_CFG_ERROR;
      }

   if(iRegDefEndianMode < 0)
      {
      // Could not find ModeRegBitName in .REG file
      sprintf(tySI.szError, "A register bit definition for '%s' of the Config register was not\nfound in device specific register definition file '%s'.",
              tySI.tyProcConfig.Processor._MIPS.szEndianModeRegBitName,
              tySI.tyProcConfig.szSymFile);
      return GDBSERV_CFG_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_Detach
     Engineer: Nikolay Chokoev
        Input: None
       Output: int : FALSE if error
  Description: resume execution of target
Date           Initials    Description
03-Jul-2008    NCH          Initial
****************************************************************************/
int LOW_Detach(int iCurrentCoreIndex)
{
   int  iMDIRet;
   int  bExecuting = FALSE;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Detach.");

   iMDIRet=pfMDIExecute(tyCurrentDevHndl);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   if(GDBSERV_NO_ERROR != LOW_IsProcessorExecuting(&bExecuting, iCurrentCoreIndex))    //todo: multicore
      return GDBSERV_ERROR;
   else
      return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_HWReset
     Engineer: Jiju George
        Input: None
       Output: int : FALSE if error
  Description: Does Hardware reset
Date           Initials    Description
10-Apr-2008    JG          Initial
****************************************************************************/
int LOW_HWReset(void)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "HW Reset target.");

   if((ErrRet = MdiResetTarget(RES_VIA_RST)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_SWReset
     Engineer: Jiju George
        Input: None
       Output: int : FALSE if error
  Description: Does software reset
Date           Initials    Description
10-Apr-2008    JG          Initial
****************************************************************************/
int LOW_SWReset(void)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "SW Reset target.");

   if((ErrRet = MdiResetTarget(RES_VIA_PRRST)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_PostBreakAction
     Engineer: Nikolay Chokoev
        Input: iCurrentCoreIndex : current core index
       Output: int : FALSE if error
  Description: start execution of target
Date           Initials    Description
26-Nov-2007    NCH         Initial
04-Dec-2007    NCH         Multicore
****************************************************************************/
int LOW_PostBreakAction(int iCurrentCoreIndex)
{
   iCurrentCoreIndex = iCurrentCoreIndex;
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_StartPFXDMode
     Engineer: Jobbymol
        Input: None
       Output: int : FALSE if error
  Description: Starts in PFXD mode. Loads MDI library functions.
Date           Initials    Description
10-Apr-2008    JG          Initial
****************************************************************************/
int LOW_StartPFXDMode(void)
{
   int ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Starting in PFXD mode...Loading MDI libraries...");

   if((ErrRet = MdiLoadLibraryPFXD()) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      (void)MdiFreeLibrary();
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;

}

/****************************************************************************
     Function: MdiLoadLibraryPFXD
     Engineer: Jobbymol
        Input: none
       Output: int
  Description: Load MIPS MDI library. Does not tries to connect.
Date           Initials    Description
11-Apr-2008    JM         Initial
09-Nov-2011    ANGS       Modified by adding AshTraceElfPath
30-Nov-2011    SV         Modified by adding AshConfigureTrace
****************************************************************************/
static int MdiLoadLibraryPFXD(void)
{
   int         ErrRet = NO_ERROR;
   int             iMDIRet;
   MDIConfigT      tyMDIConfig;

   // Remove any BP details...
   RemoveAllBpArray();

   hMipsInstMdiDll    = NULL;
   szTargetDllName[0] = '\0';

   strcpy(szTargetDllName, OPELLAXD_DLL_NAME);

   if(szTargetDllName[0] != '\0')
      {
      hMipsInstMdiDll = LoadLibrary(szTargetDllName);
      }

   if(hMipsInstMdiDll == NULL)
      return IDERR_LOAD_DLL_FAILED;

   pfAshConfigureTrace       = (TyAshConfigureTrace)       GetProcAddress(hMipsInstMdiDll,AshConfigureTrace_Export_Name);
   pfAshTraceElfPath         = (TyAshTraceElfPath)         GetProcAddress(hMipsInstMdiDll,AshTraceElfPath_Export_Name);
   pfEnableOSMMU             = (TyEnableOSMMU)             GetProcAddress(hMipsInstMdiDll,OSMMU_Export_Name);
   pfAshInitOSAppMMU         = (TyAshInitOSAppMMU)         GetProcAddress(hMipsInstMdiDll,AshOSApp_Export_Name);
   pfAshFetchTrace           = (TyAshFetchTrace)           GetProcAddress(hMipsInstMdiDll,AshFetchTrace_Export_Name);
   pfMDIConnect              = (TyMDIConnect)              GetProcAddress(hMipsInstMdiDll,MDIConnect_Export_Name);
   pfMDIDisconnect           = (TyMDIDisconnect)           GetProcAddress(hMipsInstMdiDll,MDIDisconnect_Export_Name);
   pfMDICacheFlush           = (TyMDICacheFlush)           GetProcAddress(hMipsInstMdiDll,MDICacheFlush_Export_Name);
   pfMDIOpen                 = (TyMDIOpen)                 GetProcAddress(hMipsInstMdiDll,MDIOpen_Export_Name);
   pfMDIClose                = (TyMDIClose)                GetProcAddress(hMipsInstMdiDll,MDIClose_Export_Name);
   pfMDIRead                 = (TyMDIRead)                 GetProcAddress(hMipsInstMdiDll,MDIRead_Export_Name);
   pfMDIWrite                = (TyMDIWrite)                GetProcAddress(hMipsInstMdiDll,MDIWrite_Export_Name);
   pfMDIExecute              = (TyMDIExecute)              GetProcAddress(hMipsInstMdiDll,MDIExecute_Export_Name);
   pfMDIStep                 = (TyMDIStep)                 GetProcAddress(hMipsInstMdiDll,MDIStep_Export_Name);
   pfMDIStop                 = (TyMDIStop)                 GetProcAddress(hMipsInstMdiDll,MDIStop_Export_Name);
   pfMDIReset                = (TyMDIReset)                GetProcAddress(hMipsInstMdiDll,MDIReset_Export_Name);
   pfMDIRunState             = (TyMDIRunState)             GetProcAddress(hMipsInstMdiDll,MDIRunState_Export_Name);
   pfMDISetBp                = (TyMDISetBp)                GetProcAddress(hMipsInstMdiDll,MDISetBp_Export_Name);
   pfMDIClearBp              = (TyMDIClearBp)              GetProcAddress(hMipsInstMdiDll,MDIClearBp_Export_Name);
   pfAshErrorGetMessage      = (TyAshErrorGetMessage)      GetProcAddress(hMipsInstMdiDll,AshErrorGetMessage_Export_Name);
   pfAshGetDWFWVersions      = (TyAshGetDWFWVersions)      GetProcAddress(hMipsInstMdiDll,AshGetDWFWVersions_Export_Name);
   pfMDISetupMrchpFlash      = (TyMDISetupMrchpFlash)      GetProcAddress(hMipsInstMdiDll,MDISetupMrchpFlash_Export_Name);
   pfMDIEraseMrchpFlash      = (TyMDIEraseChipMircochipFlash)      GetProcAddress(hMipsInstMdiDll,MDIEraseMrchpFlash_Export_Name);
   pfMDISaveMrchpBmxRegs     = (TyMDISaveMrchpBmxRegs)     GetProcAddress(hMipsInstMdiDll,MDISaveMrchpBmxReg_Export_Name);
   pfMDIPartitionMrchpRam    = (TyMDIPartitionMrchpRam)    GetProcAddress(hMipsInstMdiDll,MDIPartitionMrchpRam_Export_Name);      

   // First check that all MDI compulsary functions are present
   if(
     !pfAshTraceElfPath          ||
     !pfMDIConnect               ||
     !pfMDIDisconnect            ||
	 !pfAshFetchTrace            ||
     !pfMDICacheFlush            ||
     !pfMDIOpen                  ||
     !pfMDIClose                 ||
     !pfMDIRead                  ||
     !pfMDIWrite                 ||
     !pfMDIExecute               ||
     !pfMDIStep                  ||
     !pfMDIStop                  ||
     !pfMDIReset                 ||
     !pfMDIRunState              ||
     !pfMDISetBp                 ||
     !pfMDIClearBp               ||
     !pfEnableOSMMU              ||
     !pfAshErrorGetMessage       ||
     !pfMDISetupMrchpFlash       ||
     !pfMDIEraseMrchpFlash       ||
     !pfMDISaveMrchpBmxRegs      ||
     !pfMDIPartitionMrchpRam)
      {
      return IDERR_LOAD_DLL_FAILED;
      }

   if((ErrRet = MdiSaveConfigurationSettings()) != NO_ERROR)
      return ErrRet;

   memset(&tyMDIConfig,0,sizeof(tyMDIConfig));

   // The following Call back functions are not optional
   tyMDIConfig.MDICBOutput = MDICBOutput;
   tyMDIConfig.MDICBInput  = MDICBInput;

   strcpy(tyMDIConfig.User,"Ashling MIPS GDB Server");

   iMDIRet = pfMDIConnect(MDICurrentRevision,&tyCurrentMDIHandle,&tyMDIConfig);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;    // Good do nothing
      case MDIErrFailure:
         return IDERR_GDI_ERROR;
      case MDIErrParam:
         return IDERR_MDI_PARAM;
      case MDIErrVersion:
      case MDIErrNoResource:
      case MDIErrAlreadyConnected:
      case MDIErrConfig:
      case MDIErrInvalidFunction:
         return IDERR_MDI_VERSION;
      default:
         return IDERR_MDI_INTERNAL;
      }

   //Set other initial variables
   MIPSInitVars();

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_PFXDConnect
     Engineer: Jobbymol
        Input: none
       Output: int
  Description: Connects to target
Date           Initials    Description
11-Apr-2008    JM         Initial
****************************************************************************/
int LOW_PFXDConnect(void)
{
   int ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Connecting to target...");

   if((ErrRet = MdiConnectPFXD()) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}


/****************************************************************************
     Function: MdiConnectPFXD
     Engineer: Jobbymol
        Input: none
       Output: int
  Description: Connects to target.
Date           Initials    Description
11-Apr-2008    JM         Initial
****************************************************************************/
static int MdiConnectPFXD(void)
{
   int ErrRet = NO_ERROR;
   MDIDeviceIdT    tyMDIDeviceID = 0x0;
   MDIUint32       tyFlags;
   int             iMDIRet;
   unsigned long   ulRtnAddress = 0;


   // We are assuming this DLL does not support TargetGroups 
   // As such we use the MDIHandle instead of the TGHandle
   tyFlags = MDIExclusiveAccess;

   iMDIRet = pfMDIOpen(tyCurrentMDIHandle,tyMDIDeviceID,tyFlags,&tyCurrentDevHndl);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;    // Good do nothing
      case MDIErrFailure:
         return IDERR_GDI_ERROR;
      case MDIErrParam:
         return IDERR_MDI_PARAM;
      case MDIErrTGHandle:
         return IDERR_MDI_TGHANDLE;
      default:
         return IDERR_MDI_INTERNAL;
      }


   // We have reset the device and we should be able to write to registers now.
   // It is important to do this now as otherwise we might not be able to write
   // the cache invalidation routine right afterwards.
   if(!tySI.tyProcConfig.Processor._MIPS.ptyMDInfo->bEJTAGDMAOnly)
      {
      if((ErrRet = SetupRegisterSettings()) != NO_ERROR)
         return ErrRet;
      if((ErrRet = SetInitialEndianMode()) != NO_ERROR)
         return ErrRet;

      // set up cache flush routines, if required...
      if(sscanf(tySI.tyProcConfig.Processor._MIPS.szCacheRelocationAddress, "%lx", &ulRtnAddress) == 1)
         {
         if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_CACHE_FLUSH_RTN_ADDR, ulRtnAddress)) != NO_ERROR)
            return ErrRet;
         }

      // Next We Invalidate the cache as it is always safe to do this after reset
      if((ErrRet = MdiWriteReg((unsigned long)REG_MIPS_INVALIDATE_CACHE_NOW, 0)) != NO_ERROR)
         return ErrRet;
      }

   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: LOW_CloseDevice
     Engineer: Nikolay Chokoev
        Input: void
       Output: void
  Description: Close Opella-XD
Date           Initials    Description
05-Aug-2008    NCH         Initial
****************************************************************************/
int LOW_CloseDevice(int iIndex)
{
   NOREF(iIndex);
   return TRUE;
}

int TestR(void)
{
   TyRegDef *ptyRegD;
   unsigned long ulRegValue;



   if((ptyRegD = GetRegDefForName("Config")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Config\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);   
   if((ptyRegD = GetRegDefForName("Config1")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_WriteRegisterByRegDef(ptyRegD, ulRegValue | 1))
      return 1;
   printf("Config1\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);     


   if((ptyRegD = GetRegDefForName("Config2")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Config2\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Index")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Index\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Random")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Random\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("EntryLo0")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("EntryLo0\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("EntryLo1")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("EntryLo1\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Context")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Context\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("PageMask")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("PageMask\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Wired")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Wired\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("BadVAddr")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("BadVAddr\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Count")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Count\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("EntryHi")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("EntryHi\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Compare")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Compare\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Status")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Status\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Cause")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Cause\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("EPC")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("EPC\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("PRId")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("PRId\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Config")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Config\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Config1")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Config1\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Config2")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Config2\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Config3")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Config3\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("LLAddr")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("LLAddr\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("WatchLo")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("WatchLo\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("WatchHi")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("WatchHi\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("Debug")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("Debug\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("DEPC")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("DEPC\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("TagLo")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("TagLo\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("DataLo")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("DataLo\t\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   if((ptyRegD = GetRegDefForName("ErrorEPC")) == NULL)
      return 1;
   if(GDBSERV_NO_ERROR != LOW_ReadRegisterByRegDef(ptyRegD, &ulRegValue))
      return 1;
   printf("ErrorEPC\t%08lX\t%08lX\n",ptyRegD->ulAddress,ulRegValue);

   return 0;
}
/****************************************************************************
     Function: SaveMicrochipBmxRegisters
     Engineer: Daniel Madden
        Input: none
       Output: Error Status               
  Description: Top level function to save the context of Microchip BMX registers
               that are modified by Pathfinder (for example during flash programming).            
Version     Date           Initials    Description
1.0.0       06-Feb-2007    DM          Initial
****************************************************************************/
int LOW_SaveMicrochipBmxRegisters(void)
{
   int iMDIRet;

   iMDIRet = pfMDISaveMrchpBmxRegs(&tyMicrochipBmxRegContext);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      case MDIErrRange:
         SetupErrorMessage(IDERR_MDI_RANGE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;

}

/****************************************************************************
     Function: LOW_EraseChipMicrochipFlash
     Engineer: Suresh P.C
        Input: none
       Output: Error Status               
  Description: Erases Microchip flash chip       
Version     Date           Initials    Description
1.0.0       07-Mar-2010    SPC         Initial
****************************************************************************/
int LOW_EraseChipMicrochipFlash (void)
{
   int iMDIRet;

   iMDIRet = pfMDIEraseMrchpFlash(&tyMicrochipStatusRegister);

   if(iMDIRet == MDISuccess)
	  return GDBSERV_NO_ERROR;
   else
	  return GDBSERV_ERROR;
}
/****************************************************************************
     Function: SetupMicrochipFlash
     Engineer: Daniel Madden
        Input: none
       Output: Error Status               
  Description: Top level function to set up the flash controller via the 
               Microchip TAP for flash programming               
Version     Date           Initials    Description
1.0.0       06-Feb-2007    DM          Initial
****************************************************************************/
int LOW_SetupMicrochipFlash (int bClearCodeProtection, unsigned long* getCodeProtectionStatus)
{
   int iMDIRet;

   iMDIRet = pfMDISetupMrchpFlash(&tyMicrochipStatusRegister, bClearCodeProtection);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      case MDIErrRange:
         SetupErrorMessage(IDERR_MDI_RANGE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }
   *getCodeProtectionStatus=tyMicrochipStatusRegister.Bits.CPS;
   return GDBSERV_NO_ERROR;

}
/****************************************************************************
     Function: PartitionRamForMicrochipFlashSetup
     Engineer: Daniel Madden
        Input: none
       Output: Error Status               
  Description: Top level function to partition the ram for flash programming               
Version     Date           Initials    Description
1.0.0       06-Feb-2007    DM          Initial
****************************************************************************/
int LOW_PartitionRamForMicrochipFlashSetup(unsigned long ulDataKernelSpace)
{
   int iMDIRet;

   iMDIRet = pfMDIPartitionMrchpRam(ulDataKernelSpace);
   switch(iMDIRet)
      {
      case MDISuccess:
         break;      // Good do nothing
      case MDIErrFailure:
         SetupErrorMessage(IDERR_GDI_ERROR);
         return GDBSERV_ERROR;
      case MDIErrDevice:
         SetupErrorMessage(IDERR_MDI_DEVICE);
         return GDBSERV_ERROR;
      case MDIErrRange:
         SetupErrorMessage(IDERR_MDI_RANGE);
         return GDBSERV_ERROR;
      default:
         SetupErrorMessage(IDERR_MDI_INTERNAL);
         return GDBSERV_ERROR;
      }
   return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: Low_ReadCP0Reg
     Engineer: Suresh P.C
        Input: CP0 register number and Select
       Output: pulDataValue: Value of CP0 register
               returns Error Status               
  Description: Reads CP0 register       
Date           Initials    Description
24-Aug-2011    SPC         Initial
****************************************************************************/
int Low_ReadCP0Reg(unsigned int uiRegNumber, unsigned int ulSelect, unsigned long *pulDataValue)
{
	unsigned long ulSpecialOffset;
	int iMDIRet;

	if (pfMDIRead == NULL)
		return IDERR_GDI_ERROR;

	ulSpecialOffset = uiRegNumber + (ulSelect * 32);
	iMDIRet=pfMDIRead(tyCurrentDevHndl,MDIMIPCP0,(MDIOffsetT)ulSpecialOffset,pulDataValue,4,1);
	switch (iMDIRet)
		{
		case MDISuccess:
			break;		// Good do nothing
		case MDIErrFailure:
			ASSERT_NOW();
			return IDERR_GDI_ERROR;			// Calls Setup Error Message
		case MDIErrDevice:
			ASSERT_NOW();
			return IDERR_MDI_DEVICE; 
		case MDIErrInvalidSrcOffset:
			ASSERT_NOW();
			return IDERR_MDI_SRCOFFSET;
		case MDIErrSrcResource:
			ASSERT_NOW();
			return IDERR_MDI_SRCRESOURCE;
		case MDIErrInvalidDstOffset:
			ASSERT_NOW();
			return IDERR_MDI_DSTOFFSET;
		case MDIErrDstResource:
			ASSERT_NOW();
			return IDERR_MDI_DSTRESOURCE;
		default:
			ASSERT_NOW();
			return IDERR_MDI_INTERNAL; 
		}
	return GDBSERV_NO_ERROR;
}

/****************************************************************************
     Function: Low_WriteCP0Reg
     Engineer: Suresh P.C
        Input: CP0 register number and Select
       Output: pulDataValue: Value of CP0 register
               returns Error Status               
  Description: Writes to CP0 register       
Date           Initials    Description
24-Aug-2011    SPC         Initial
****************************************************************************/
int Low_WriteCP0Reg(unsigned int uiRegNumber, unsigned int ulSelect, unsigned long ulDataValue)
{
	unsigned long ulSpecialOffset;
	int iMDIRet;

	if (pfMDIRead == NULL)
		return IDERR_GDI_ERROR;

	ulSpecialOffset = uiRegNumber + (ulSelect * 32);
	iMDIRet=pfMDIWrite(tyCurrentDevHndl,MDIMIPCP0,(MDIOffsetT)ulSpecialOffset,&ulDataValue, 4, 1);
	switch (iMDIRet)
		{
		case MDISuccess:
			break;		// Good do nothing
		case MDIErrFailure:
			ASSERT_NOW();
			return IDERR_GDI_ERROR;			// Calls Setup Error Message
		case MDIErrDevice:
			ASSERT_NOW();
			return IDERR_MDI_DEVICE; 
		case MDIErrInvalidSrcOffset:
			ASSERT_NOW();
			return IDERR_MDI_SRCOFFSET;
		case MDIErrSrcResource:
			ASSERT_NOW();
			return IDERR_MDI_SRCRESOURCE;
		case MDIErrInvalidDstOffset:
			ASSERT_NOW();
			return IDERR_MDI_DSTOFFSET;
		case MDIErrDstResource:
			ASSERT_NOW();
			return IDERR_MDI_DSTRESOURCE;
		default:
			ASSERT_NOW();
			return IDERR_MDI_INTERNAL; 
		}
	return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: LOW_FetchTrace
     Engineer: Rejeesh S Babu
        Input: none
       Output: int : FALSE if error
  Description: Fetch trace from target and stores it in trace.temp file
Date           Initials    Description
30-Aug-2011    RSB         Initial
****************************************************************************/
int LOW_FetchTrace(unsigned long ulBegPtr,unsigned long ulEndptr)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Fetch Trace.");

   if((ErrRet = pfAshFetchTrace(ulBegPtr,ulEndptr)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: LOW_InvCache
     Engineer: Rejeesh S Babu
        Input: none
       Output: int : FALSE if error
  Description: Invalidate the whole of cache memory
Date           Initials    Description
16-Sep-2011    RSB         Initial
****************************************************************************/
int LOW_InvCache(void)
{
   int  ErrRet;

   if(tySI.bDebugOutput)
      PrintMessage(LOG_MESSAGE, "Invalidate Cache.");

   if((ErrRet = pfMDICacheFlush(tyCurrentDevHndl,MDICacheTypeUnified,MDICacheWriteBack|MDICacheInvalidate )) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: LOW_ElfPath
     Engineer: Amala Nair G S
        Input: elf path for trace
       Output: int : FALSE if error
  Description: Passing the FullPath of .elf
Date           Initials    Description
1-Nov-2011     ANGS        Initial
****************************************************************************/
int LOW_ElfPath(char* pszElfPath)
{
   int  ErrRet;

   //if(tySI.bDebugOutput)
   //   PrintMessage(LOG_MESSAGE, "Invalidate Cache.");

   /*pszElfPath+1:Work aorund to skip the blank space coming*/
   if((ErrRet = pfAshTraceElfPath(pszElfPath+1)) != NO_ERROR)
      {
      SetupErrorMessage(ErrRet);
      return GDBSERV_ERROR;
      }

   return GDBSERV_NO_ERROR;
}
/****************************************************************************
     Function: LOW_ConfigureTrace
     Engineer: Sreekanth.V
        Input: Zephyr Trace register values 
       Output: int : FALSE if error
  Description: Passing register values
Date           Initials    Description
30-Nov-2011    SV          initial
*****************************************************************************/
int LOW_ConfigureTrace(unsigned long ulReg1, unsigned long ulReg2, unsigned long ulReg3)
{
 int ErrRet;

    if ((ErrRet = pfAshConfigureTrace(ulReg1,ulReg2,ulReg3)) !=NO_ERROR)
        {
        SetupErrorMessage(ErrRet);
        return GDBSERV_ERROR;
        }
  return GDBSERV_NO_ERROR;
}

