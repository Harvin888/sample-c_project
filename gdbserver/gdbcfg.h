/****************************************************************************
       Module: gdbcfg.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, target configuration header file
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Dec-2009    SVL         Modified 'CFG_AddRegDefArrayEntry' for PathFinder-XD's 
                           Peripheral Register View. RegisterDefintionType will 
                           also be stored in the data structures.
****************************************************************************/
#ifndef GDBCFG_H_
#define GDBCFG_H_

typedef struct
{
   const char * pszName;
   int32_t    iEnumValue;
} TyEnumLookup;

int32_t CFG_Init(void);
void CFG_FreeInitial(void);
void CFG_FreeAll(void);
int32_t CFG_Configure(void);
int32_t CFG_AddRegDefArrayEntry(const char *pszRegName, 
							uint32_t ulAddress, 
							TyMemTypes tyMemType, 
							unsigned char ubByteSize,
                            unsigned char ubBitOffset, 
							unsigned char ubBitSize, 
							int32_t iFullRegIndex, 
                            int32_t iRegDefType);
int32_t CFG_Enum(char *pszName);
int32_t CFG_ProcessRegFile(void);

#endif   // GDBCFG_H_
 
