/****************************************************************************
       Module: gdberr.h
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, error numbers
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
11-Apr-2008    RS/VK       Added error codes and messages for ARM GDB Server
16-Oct-2009    SVL         Support for Vector Catch
19-Oct-2009    SVL         Support for forcefully setting breakpoint types
3-Nov-2009     SVL         Error value for malloc and read memory failures
12-Apr-2010    SVL         Added error code for target reset modes for ARM
29-Apr-2010    SJ          Support for Coresight devices
28-July-2011   ANGS        Make 'Missing filename' and 'Invalid scanchain file'
                           errors common to both MIPS and ARM
****************************************************************************/
#ifndef GDBERR_H_
#define GDBERR_H_

// Linux build change...
#include "protocol/common/types.h"

// Return Error Codes
#define GDBSERV_NO_ERROR                0x0
#define GDBSERV_ERROR                   0x1
#define MAX_TRIGGER_LIMIT				0x2
#define TRIGGER_DOES_NOT_EXIST			0x3
#define GDBSERV_TARGET_RUNNING			0x4
#define GDBSERV_OPT_DEFAULT_CMDFILE     0x1000
#define GDBSERV_OPT_ERROR               0x1001
#define GDBSERV_XML_ERROR               0x1002
#define GDBSERV_XML_END_OF_FILE         0x1003
#define GDBSERV_XML_NO_MEMORY_ERROR     0x1004
#define GDBSERV_CFG_ERROR               0x1005
#ifdef ARM
#define BAD_HEX_VALUE                   0xBAD0BAD0
#define IDERR_LOAD_DLL_FAILED           11
#define IDERR_DLL_NOT_LOADED            12
#define IDERR_ASH_SRCREGISTER           15
#define IDERR_ASH_DSTREGISTER           16
#define IDERR_ADD_BP_NO_MEMORY          17
#define IDERR_ADD_BP_INTERNAL1          18
#define IDERR_ADD_BP_DUPLICATE          19
#define IDERR_REMOVE_BP_INTERNAL1       20
#define IDERR_REMOVE_BP_INTERNAL2       21
#define IDERR_REMOVE_BP_NOT_FOUND       22
#endif

#if defined(ARC) || defined (RISCV) 
#define IDERR_REMOVE_BP_NOT_SUPPORTED   23
#endif

typedef int32_t TyError;
// Error Messages
#define GDB_EMSG_VERIFY_WITHOUT_LDEXE  "Invalid command line arguements, --ld-exe option must be specified to use --verify-download"
#define GDB_EMSG_CANNOT_GET_FWDW_VER   "Cannot get firmware/diskware version information."
#define GDB_EMSG_INV_DEV_DATABASE      "Invalid device database file '%s',\ntag '<AshlingProcessorDataBase>' not found."

#define GDB_EMSG_CMD_COULDNT_OPEN_FILE "Could not open command file '%s'."
#define GDB_EMSG_CMD_FILE_ISUFF_MEM    "Insufficient memory while processing command file '%s'."
#define GDB_EMSG_CMD_FILE_INT_ERR2     "Internal error 2 while processing command file '%s'."
#define GDB_EMSG_CMD_FILE_INT_ERR      "Internal error while processing command file '%s'."
#define GDB_EMSG_CLINE_OPT_RPTED       "Command line option '%s' repeated."
#define GDB_EMSG_CLINE_INV_OPTION      "Invalid command line option '%s'."
#define GDB_EMSG_TGT_DEVICE_NOT_SPEC   "A target device must be specified, use the --device option."
#define GDB_EMSG_SRV_NO_WSOCK_IFACE    "Could not find a Winsock interface."
#define GDB_EMSG_SRV_NO_LISTN_SOCKET   "Could not open a listener socket, error %d occurred."
#define GDB_EMSG_SRV_ERR_SET_NONBLK    "Could not set non-blocking socket."
#define GDB_EMSG_SRV_ERR_SET_OPT       "Could not set the socket options."
#define GDB_EMSG_SRV_ERR_BIND_ADDR     "Could not bind address to socket."
#define GDB_EMSG_SRV_ERR_CON_PORT      "Connection on port %d closed."
#define GDB_EMSG_SRV_ERR_RCV_DATA      "Socket error %d receiving data from debugger."
#define GDB_EMSG_SRV_ERR_LONG_PACKET   "Packet received from debugger is too int32_t, exceeds maximum\nlength of %d bytes."
#define GDB_EMSG_SRV_INV_PACK_CHKSUM   "Invalid packet checksum received from debugger, received=0x%x,\nexpected=0x%x, packet=%s"
#define GDB_EMSG_SRV_ERR_NUM_SND_DATA  "Socket error %d sending packet to debugger: %s"
#define GDB_EMSG_SRV_ERR_SND_DATA      "Socket error sending packet to debugger: %s"
#define GDB_EMSG_SRV_NO_ACK            "Acknowledge not received from debugger after sending packet."
#define GDB_EMSG_SRV_UNABLE_HALT_CORE  "Unable to halt core"
#define GDB_EMSG_SRV_ATTEMPT_HALT_CORE "Core is running (unexpected), attempting to halt..."
#define GDB_ERROR_INVALID_CMD_LINE_ARG "Invalid command line arguments, please recheck.\n"
#define GDBSERV_ERR_MALLOC             "Insufficient memory.\n"
#define GDB_EMSG_FILENAME_MISSING      "Missing filename after %s option."
#define GDB_EMSG_SCANCHAIN_FILE_INVALID  "Invalid scanchain file '%s',\ntag '<TargetDefinition>' not found."
#define GDB_EMSG_SRV_ATTEMPT_STATUS_CORE "Error while reading status of the the target"
#define GDB_EMSG_UNABLE_TO_RESET_TARGET  "Error occured while resetting target."
#ifdef ARM
#define GDB_EMSG_LOAD_LIB_FAILED         "Failed to load target library %s. File not found or is corrupt.\n"
#define RDI_ERROR_RESET                  "Target processor reset has been detected.\n"
#define RDI_ERROR_TARGET_RUNNING         "Option is not available while the processor is running.\n"
#define RDI_ERROR_TARGET_BROKEN          "Cannot communicate with target. Ensure that Opella-XD is configured correctly, connected to the target and that the target is powered."
#define RDI_ERROR_LOOPBACK_OPELLA_XD     "The PC is not able to communicate with Opella-XD. Please ensure it is connected to your PC"
#define RDI_ERROR_BADPOINT_TYPE          "Internal error. The requested point type is invalid.\n"
#define RDI_ERROR_INCORRECT_PROC_TYPE    "The processor detected is not the same as the processor selected.\n"
#define RDI_ERROR_CANT_SET_POINT         "Cannot set breakpoint or watchpoint. No more hardware resources are available. Please free existing hardware resources and retry.\n"
#define RDI_ERROR_NO_SUCH_POINT          "Cannot clear the breakpoint or watchpoint resource. (This breakpoint or watchpoint may already have been cleared).\n"
#define RDI_ERROR_POINT_USE              "Cannot set breakpoint or  watchpoint as a breakpoint or watchpoint has already been set at this address or Data.\n"
#define RDI_ERROR_DW1_FAILED             "Failed to find required diskware file.\n"
#define RDI_ERROR_FPGA_FAILED            "Failed to find required FPGAware file.\n"
#define RDI_ERROR_NO_TOOL_CONF           "No tool configuration is present.\n"
#define RDI_ERROR_UNKNOWN_DW_ERROR       "Unknown diskware error occurred.\n"
#define RDI_ERROR_NO_RTCK_SUPPORT        "Opella-XD timed out waiting for target RTCK to respond.\n"
#define RDI_ERROR_UNKNOWN_ERROR          "Unknown target error occurred.\n"
#define RDI_ERROR_ROM_AREA               "Cannot set a software breakpoint in ROM area\n"
#define RDI_ERROR_CONFIG_ERROR           "Failed to initialise target. Please reconfigure the Opella-XD RDI driver appropriately for your target."
#define RDI_ERROR_TPA_ERROR              "Check whether the TPA is connected with Opella-XD.\n"
#define RDI_ERROR_DEVICE_IN_USE          "Opella-XD is already in use by another debugger\n"
#define MONITOR_ERROR_SETTING_JTAG_FREQ  "Error setting JTAG frequency\n"
#define MONITOR_ERROR_FAILED_MODE_SIZE   "Failed to set mode '%s' and size '%s'.\n"
#define MONITOR_ERROR_FAILED_WP_SET      "Setting watchpoint failed.\n"
#define MONITOR_ERROR_SH_FAILED          "Setting up semi-hosting failed.\n"
#define MONITOR_ERROR_WP_FAILED          "Deleting watchpoint failed."
#define MONITOR_ERROR_WP_FAILED_IDX      "Deleting watchpoint failed - "
#define MONITOR_ERROR_INVALID_COPROC     "Unsupported coprocessor selected. Please recheck\n"
#define MONITOR_ERROR_INVALID_REG        "Unsupported register selected. Please recheck\n"
#define MONITOR_ERROR_INVALID_CRN        "Invalid value for CRn. Please recheck\n"
#define MONITOR_ERROR_INVALID_CRM        "Invalid value for CRm. Please recheck\n"
#define MONITOR_ERROR_INVALID_OP1        "Invalid value for Op1. Please recheck\n"
#define MONITOR_ERROR_INVALID_OP2        "Invalid value for Op2. Please recheck\n"
#define MONITOR_ERROR_VC_CFG_FAILED      "Vector Catch configuration failed.\n"
#define MONITOR_ERROR_SET_BPTYPE         "Error while setting breakpoint type.\n"
#define MONITOR_ERROR_UNSET_BPTYPE       "Error while removing breakpoint type.\n"
#define MONITOR_ERROR_SET_ARMV7_REGS     "Error while writing to the register.\n"

#define GDB_EMSG_ENDIANESS_MISSING              "Missing endianess after %s option."
#define GDB_EMSG_ENDIANESS_INVALID              "Invalid endianess '%s'."
#define GDB_EMSG_PORT_NO_MISSING                "Missing port number after %s option."
#define GDB_EMSG_RST_OPTN_INVALID               "Invalid reset option '%s'.\n"
#define GDB_EMSG_RDELAY_OPTN_INVALID            "Invalid reset delay option '%s'.\n"
#define GDB_EMSG_ARG_MISSING                    "Missing <0/1> after %s option."
#define GDB_EMSG_NTRST_OPTN_INVALID             "Invalid nTRST option "
#define GDB_EMSG_DBRQ_OPTN_INVALID              "Invalid Dbrq option."
#define GDB_EMSG_CACHE_ADDR_MISSING             "Missing cache flush routine address after %s\noption."
#define GDB_EMSG_CACHE_ADDR_INVALID             "Invalid cache flush routine address '%s'."
#define GDB_EMSG_SAFE_NON_VECTOR_ADDR_MISSING   "Missing safe non-vector address after %s\noption."
#define GDB_EMSG_SAFE_NON_VECTOR_ADDR_INVALD    "Invalid safe non-vector address '%s'."
#define GDB_EMSG_TARGET_VOLTAGE_MISSING         "Missing target voltage after %s option."
#define GDB_EMSG_TARGET_VOLTAGE_INVALID         "Invalid target voltage '%s'."
#define GDB_EMSG_TARGET_RST_TYPE_INVALID        "Invalid target reset type.\n"
#define GDB_EMSG_DEBUG_FILE_ERROR               "Could not open debug file '%s'."
#define GDB_EMSG_PORT_NUMBER_MISSING            "Missing port number after %s option."
#define GDB_EMSG_PORT_NUMBER_INVALID            "Invalid port number '%s'."
#define GDB_EMSG_OPELLAXD_SERIAL_MISSING       "Missing Opella-XD serial number after %s option."
#define GDB_EMSG_OPELLAXD_SERIAL_INVALID       "Invalid Opella-XD serial number format."
#define GDB_EMSG_JTFREQ_MISSING                 "Missing JTAG frequency after %s option."
#define GDB_EMSG_JTFREQ_INVALID                 "Invalid JTAG frequency %s."
#define GDB_EMSG_DEVICE_NAME_MISSING            "Missing device name after %s option."
#define GDB_EMSG_DEVICE_NAME_INVALID            "Invalid device name '%s'."
#define GDB_EMSG_ENTRYPOINT_ADDR_MISSING        "Missing entry point address after %s option."
#define GDB_EMSG_ENTRYPOINT_ADDR_INVALID        "Invalid entry point address '%s'."
#define GDB_EMSG_TAP_MISSING                    "Missing Test Access Point number after %s option."
#define GDB_EMSG_TAP_INVALID                    "Invalid Test Access Point number '%s'."
#define GDB_EMSG_CONN_INTERFACE_MISSING         "Missing Connection interface name after %s option."
#define GDB_EMSG_PROBE_MISSING                  "Missing Debug probe name after %s option."
#define GDB_EMSG_TOTCORES_MISSING               "Missing Total number of cores after %s option."
#define GDBSERV_ERR_READ_MEM                    "Failed to read memory.\n"
#define GDBSERV_ERR_WRITE_MEM                   "Failed to write memory.\n"
#define GDBSERV_INV_WP_IDX                      "Invalid watchpoint index. Please recheck.\n"
#define GDB_EMSG_CORESIGHT_BASE_ADDR_MISSING    "Missing Coresight Base Address after %s\noption."
#define GDB_EMSG_CORESIGHT_BASE_ADDR_INVALD     "Invalid Coresight Base Address '%s'."
#define GDB_EMSG_DEBUG_AP_INDEX_MISSING         "Missing Debug AP Index after %s\noption."
#define GDB_EMSG_DEBUG_AP_INDEX_INVALD          "Invalid Debug AP Index '%s'."
#define GDB_EMSG_MEM_AP_INDEX_MISSING           "Missing Memory AP Index after %s\noption."
#define GDB_EMSG_JTAG_AP_INDEX_MISSING          "Missing MJtag AP Index after %s\noption."
#define GDB_EMSG_MEM_AP_INDEX_INVALD            "Invalid Memory AP Index '%s'."
#define GDB_EMSG_JTAG_AP_INDEX_INVALD           "Invalid Jtag AP Index '%s'."
#endif

#ifdef RISCV

#define GDB_EMSG_NO_TARGET						"Target not detected. Ensure the Opella-XD is connected to the target and that the target is powered on."
#define GDB_EMSG_UNK_DEVICE						"Error occurred due to an unknown RISC-V device."  
#endif
#endif   // GDBERR_H_

