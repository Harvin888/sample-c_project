# Ashling Linux GDB Support Scripts
# Version 1.1.0
# Date 04/11/2009

init-if-undefined $is_loaded= 0
if ( $is_loaded == 0 )
	set $is_loaded = 1
end
if( $is_loaded == 2 )
	set confirm off
end

set $modulestat = 0
set $LocalError = 0
set $GlobalError = 0

# display running tasks
define pslist
	dont-repeat
	set $initthread = &init_task
	set $athread = &init_task
	printf "Address\t\tPID\tCMD\n"
	printf "-------\t\t---\t---\n"
	if( $athread->mm )
		printf "0x%8x\t%d\t%s\n", $athread, $athread->pid, $athread->comm
	else
		printf "0x%8x\t%d\t[%s]\n", $athread, $athread->pid, $athread->comm
	end
	set $offset=(int)&($initthread->tasks)-(int)&init_task
        set $athread = (struct task_struct *)((int)$athread->tasks->next-$offset)
        while $athread != ($initthread)
		if ($athread->pid) != (0)
			if( $athread->mm )
				printf "0x%8x\t%d\t%s\n", $athread, $athread->pid, $athread->comm
			else
				printf "0x%8x\t%d\t[%s]\n", $athread, $athread->pid, $athread->comm
			end
                end
                set $athread = (struct task_struct *)((int)$athread->tasks->next-$offset)
        end
end

document pslist
Usage: pslist
List currently running processes
end

# List loaded modules
define lsmod
	dont-repeat
        set $modstart = &modules
        set $mod = (struct module *)((char*)(modules->next)-4)
	printf "Address\t\tModule\t\tSize\n"
	printf "-------\t\t------\t\t----\n"
	if $modstart != $modstart->next
		printf "0x%X \t%s\t\t%d\n", $mod, $mod->name, $mod->core_size
	else
		printf "No modules inserted\n"
	end
        while  $modstart != $mod->list->next
		set $mod = (struct module *)((char*)($mod->list->next)-4)
		printf "0x%X \t%s\t\t%d\n", $mod, $mod->name, $mod->core_size
        end
end

document lsmod
Usage: lsmod
List currently loaded modules
end


define dbgmodfrominit 
	# Set bp @ blocking_notifier_call_chain			
	dont-repeat
	set $var1=$arg0
	set $NextAddr = (int)blocking_notifier_call_chain	
	tbreak *$NextAddr
	commands
		silent
		printf "Module insertion detected\n"
	end
	printf "Please insert module via the Linux OS console (using INSMOD command)\n"
			
	# Execute to bp
	continue
	# Check for pc
	if ($pc != $NextAddr)
	    printf "Error. Failed to reach software breakpoint at 0x%08x (PC=0x%08x)\n", $NextAddr, $pc
		set $LocalError += 1
	else
	    set $modulestat=2
		addsyms $arg0 v
		set $modulestat=0
		runtoinit
	end
end

document dbgmodfrominit 
Usage: dbgmodfrominit <module_symbol_file_with_path>
Loads module symbols and runs to init_module(). Module must be inserted via Linux OS console using INSMOD command
end

define loadmodsym
	dont-repeat
	set $modname=$arg0
	set $modpath=$arg1
	set $m = (struct list_head *)&modules
	set $done = 0
	set $find = 0
	set $strlen1 = 0
	set $strlen2 = 0
	set $modulename = 0
	set $Error = 0
	strlen $modname $strlen1 1
	printf "Searching for module in loaded modules list...\n"
	while ( !$done && !$find )
		# list_head is 4-bytes into struct module
		set $mp = (struct module *)((char *)$m->next - (char *)4)
		set $modulename = $mp->name
		strlen $modulename $strlen2 0
		# Search module in module list
		if( $strlen1 == $strlen2 )
			strcmp $modname $modulename $strlen1 $Error
			if( 0 == $Error )
				set $modulestat = 2
				addsyms $arg1 $mp
				set $find = 1
				set $strlen1 = 0
				set $strlen2 = 0				
			end
		end
		if ( $mp->list->next == &modules)
			if( !$find )
				printf "Module %s is not loaded\n", $modname
			end
			set $modulestat = 2
			set $done = 1
		end
		set $m = $m->next
	end
end
 
document loadmodsym

Usage: loadmodulesym <"module_name"> <"module_symbol_file_with_path">
Loads module symbols. Module name/path is enclosed in "".
	
end

define addsyms
	dont-repeat
	set $nsyms=(*(*(struct module *)$arg1)->sect_attrs)->nsections
	set $datasect = ".data"
	set $datasectaddr = 0
	set $bsssect = ".bss"
	set $bsssectaddr = 0
	set $textsect = ".text"
	set $textsectaddr = 0
	set $reginfosect = ".reginfo"
	set $reginfosectaddr = 0
	set $datasectlen = 0
	set $textsectlen = 0
	set $reginfosectlen	= 0
	set $bsssectlen = 0
	set $strlen	= 0
	set $retval	= 0
	set $nsectfound	= 0
	set $sectfound = 0
	strlen $datasect $datasectlen 0
	strlen $textsect $textsectlen 0
	strlen $bsssect $bsssectlen 0
	strlen $reginfosect $reginfosectlen 0
	set $cnt=0
	printf "Loading module symbol information...\n"
	if( 2 == $modulestat )
		while( ($cnt!=$nsyms) && (4 > $nsectfound ) )
			set $sectfound = 0
			set $sectname=(*(*(struct module *)$arg1)->sect_attrs)->attrs[$cnt]->name
			set $sectaddr=(*(*(struct module *)$arg1)->sect_attrs)->attrs[$cnt]->address
			set $strlen = 0
			strlen $sectname $strlen 0
			if( ( $reginfosectlen == $strlen ) && ( 0 == $sectfound ) )
				strcmp $reginfosect $sectname $reginfosectlen $retval
				if( 0 == $retval )
					set $reginfosectaddr = $sectaddr
					set $reginfosect = $sectname
					set $nsectfound = $nsectfound+1
					set $sectfound = 1
				else
					set $retval = 0
				end
			end
			if( ( $textsectlen == $strlen )&&( 0 == $sectfound ) )
				set $retval				 = 0
				strcmp $textsect $sectname $textsectlen $retval
				if( ( 0 == $retval ) )
					set $textsectaddr = $sectaddr
					set $textsect = $sectname
					set $nsectfound = $nsectfound+1
					set $sectfound = 1
				else
					set $retval	= 0
					strcmp $datasect $sectname $textsectlen $retval
					if( ( 0 == $retval ) )
						set $datasectaddr = $sectaddr
						set $datasect = $sectname
						set $nsectfound = $nsectfound+1
						set $sectfound = 1
					end
				end
			end
			if( ( $bsssectlen == $strlen )&&( 0 == $sectfound ) )
				strcmp $bsssect $sectname $bsssectlen $retval
				if( ( 0 == $retval ) )
					set $bsssectaddr = $sectaddr
					set $bsssect = $sectname
					set $nsectfound = $nsectfound+1
					set $sectfound = 1
				else
					set $retval = 0
				end
			end
			set $cnt = $cnt+1
		end
	add-symbol-file $arg0 $textsectaddr -s .data $datasectaddr -s .bss $bsssectaddr -s .reginfo $reginfosectaddr 
	else
		if( 1 == $modulestat )
			printf "Module currently not running\n"
		else
			printf "Check is module running\n"
		end
	end
end

document addsyms

Usage: addsyms absolute-path module-struct-variable 
This will add the .text, .bss, .data, .reginfo symbols to add-symbol-file.
Warning: This is implicit function. If you call this, it will load undesired information to add-symbol-file.

end
 
#Compares two strings upto the number of string lenth passed as third argument
define strcmp
	set $i = 0
	while( $i != $arg2 )
		if( $arg0[$i] != $arg1[$i] )
			set $arg3 = 1
		end
		set $i = $i+1
	end
end

document strcmp

Usage: strcmp str1 str2 strlen retval
This will compare two strings str1 and str2 for length strlen and return the result in retval 
end

#This finds the length of the string
define strlen
	set $i = 0
	set $tmp_value = 0
	set $tmp_value1 = 0
	set $tmp_value2 = 0
	while ( '\0' != $arg0[$i] )
		set $arg1 = $arg1+1 
		set $i = $i+1
	end
	#This will check for .ko in string if the fourth argument value is 1.
	#If the string contains .ko at the end then 
	#the string length will be 3 less than the
	#original string length,
	if ( 1 == $arg2 )
		set $i = $i-3
		if( '.' == $arg0[$i] )
			set $tmp_value = $i+1
			set $tmp_value1 = $i+2
			set $tmp_value2 = $i+3
			if( 'k' == $arg0[$tmp_value] )
				if( 'o' == $arg0[$tmp_value1] )
					if( '\0' == $arg0[$tmp_value2] )
						set $arg1 = $arg1-3 
						set $tmp_value = 0
						set $tmp_value1 = 0
						set $tmp_value2 = 0
						set $i = 0	
					end
				end
			end
		end
	end
end

document strlen

Usage: strlen str1 strlen checktype
This will calculate the length of string str1 and store it in strlen.
if checktype=1 then  will check for ".ko" at the end of the string and remove it if found
end

define runtoinit
	set $NextAddr = *((struct module *)v)->init
	thbreak *$NextAddr
	c
end

define ashlinuxhelp
	dont-repeat

	printf "\nenablelinuxdbg    Enables Linux debugging in Ashling GDB Server\n"
	printf "lsmod             Lists all currently running modules\n"
	printf "pslist            Lists all currently running processes\n"
	printf "dbgmodfrominit    Debug specified module from init_module()\n"
	printf "loadmodsym        Load module symbols for debugging\n"
	printf "dbgprocfrommain   Debug the specified process from main()\n"
	printf "loadprocsym       Load process symbols for debugging\n\n"
	printf "For more information type help <command_name>\n\n"

end

document ashlinuxhelp

Usage: ashlinuxhelp
Lists all Ashling gdb script commands for Linux debug.

end

define dbgprocfrommain
	dont-repeat
	set $app=$arg0
	tbreak set_binfmt
	commands
		silent
	end
	printf "Please run the process\n"
	c
	set $curtask = (struct task_struct*)(((struct thread_info*)$gp)->task)
	printf "Loading process symbols...\n"
	file $arg0
	setosappmmu $curtask
	thbreak main
	c
end

document dbgprocfrommain
Usage: dbgprocfrommain "<app_path/appexe>"
Loads the process symbols and runs to main()
end

define loadprocsym
	dont-repeat
	set $procid=$arg0
	set $app=$arg1
	set $notfound=0
	set $initthread = &init_task
	set $athread = &init_task
	set $offset=(int)&($initthread->tasks)-(int)&init_task
        set $athread = (struct task_struct *)((int)$athread->tasks->prev-$offset)
	set $exitloop=0
        while $exitloop==0
		if ($athread->pid) == ($procid)
			printf "Loading process symbols...\n"
			setosappmmu $athread
			set $exitloop=1
                end
                set $athread = (struct task_struct *)((int)$athread->tasks->prev-$offset)
		if ($athread) == ($initthread)
			set $exitloop=1
			set $notfound=1
			printf "The process with process ID %d not found\n",$procid
		end
        end
	if $notfound == 0
		file $arg1
	end
end

document loadprocsym
Usage: loadprocsym <PID_of_process> "<app_path/appexe>" 
Loads symbols for a running process			
end

define getcurprocess
	dont-repeat
	set $curtask = (struct task_struct*)(((struct thread_info*)$gp)->task)
	printf "Current task: Address:0x%8x PID:%d Name:%s \n",$curtask, $curtask->pid, $curtask->comm
end

document getcurprocess
Usage: getcurprocess
Displays current process
end

define getsyms
	dont-repeat
	set $nsyms=(*(*(struct module *)$arg0)->sect_attrs)->nsections
	set $loop=0
	printf "Sect.Name \t\t Sect.Adr\n"
	while $nsyms != $loop
		printf "%s ",(*(*(struct module *)$arg0)->sect_attrs)->attrs[$loop]->name 
		printf "\t\t 0x%x \n",(*(*(struct module *)$arg0)->sect_attrs)->attrs[$loop]->address
		set $loop=$loop+1
	end
end

define enablelinuxdbg
	dont-repeat
	file
	add-symbol-file $arg0 $arg1
	set logging file test.gdb
	set logging overwrite on
	set logging redirect on
	set logging on
	printf "monitor osmmu linux26 0x%x", &swapper_pg_dir
	set logging off
	source test.gdb
end

document enablelinuxdbg

Usage: enablelinuxdbg <path>/vmlinux <.text_sec_addrs>
Enables Linux debugging.

end

define listlibs
	dont-repeat
	printf "\nAddress\t\tCodeAreaName\n"
	printf "=======\t\t============\n"
	set $task = (struct task_struct*)$arg0
	set $codestart = (struct vm_area_struct *)($task->mm->mmap)
	while $codestart->vm_next != 0
		printf "0x%08x\t", $codestart->vm_start
		printf "%s\n", $codestart->vm_file->f_path->dentry->d_name->name
		set $codestart=$codestart->vm_next
	end
end

define setosappmmu
	dont-repeat
	set $task = (struct task_struct*)$arg0
	set logging file test.gdb
	set logging overwrite on
	set logging redirect on
	set logging on
	printf "monitor osappmmu 0x%x 0x%x\n", $task->mm->context[0], $task->mm->pgd
	set logging off
	source test.gdb
end

echo Ashling Linux support added successfully\n

if( $is_loaded != 2 )
	set $is_loaded = 2
end


set confirm on
