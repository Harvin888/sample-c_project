/****************************************************************************
       Module: gdbserv.c
     Engineer: Vitezslav Hola
  Description: Ashling GDB Server, ethernet socket communication
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS       Modified for ARM support
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
// Linux build change...
#include "protocol/common/types.h"

#ifndef __LINUX
#include <winsock2.h>
#include <conio.h> 
#include <winerror.h>
#define  GetSocketError()  WSAGetLastError()
typedef int32_t socklen_t;
#define O_NONBLOCK FIONBIO
#define fcntl(fd,b,c)   ioctlsocket(fd, c, &arg)
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#define  GetSocketError()  errno
#define  closesocket       close
#endif
#include "gdbserver/gdbglob.h"
#include "gdbserver/gdbxml.h"
#include "gdbserver/gdbty.h"
#include "gdbserver/gdbutil.h"
#include "gdbserver/gdbdsp.h"
#include "gdbserver/gdbserv.h"
#include "gdbserver/gdblow.h"
#include "gdbserver/gdberr.h"
#if defined(ARC)
#include "gdbserver/arc/gdbarc.h"
#elif defined(RISCV)
#include "gdbserver/riscv/gdbriscv.h"
#elif defined(MIPS)
#include "gdbserver/mips/gdbmips.h"
#elif defined(ARM)
#include "gdbserver/arm/gdbarm.h"
#endif
#define  GDBSERV_SOCKET_BUFFER_SIZE       0x1000
#define  GDBSERV_PACKET_RESERVE_BYTES     0x5

typedef struct sockaddr       TySockAddr;
typedef struct sockaddr_in    TySockAddrIn;

typedef struct
{
   unsigned short usCurrentPort;
   int32_t            iListener;
   int32_t            iRemote;
   int32_t            bConnected; 
   char               bNoAckMode;
   char               szAddress[20];
}TyConnection_t;

// gloabl variables
static TySockAddrIn   tySockAddr[MAX_TAP_NUM];
static TyConnection_t tyConnection[MAX_TAP_NUM];
static char           szInPacket [GDBSERV_PACKET_BUFFER_SIZE];
static char           szOutPacket[GDBSERV_PACKET_BUFFER_SIZE];
static int32_t gbFinished = 0;
// local functions
static int32_t InitSocketSupport(void);
static int32_t OpenListenerSocket(unsigned short usPort);
static int32_t CheckForConnection(int32_t iConnection);
static void CloseConnection(int32_t iConnection);
static void CloseAllConnections(void);
static void CloseAllListeners(void);
static int32_t ReadChar(int32_t iConnection);
static int32_t ReadPacket(char *pszBuf, int32_t iBufSize, int32_t iConnection);
int32_t WritePacket(char *pszBuf, int32_t iBufSize, int32_t iConnection);
int32_t RemoteDataPresent(int32_t iConnectionIndex);

// external variables
extern TyServerInfo tySI;

/****************************************************************************
	 Function: intHandler
	 Engineer: Rejeesh S Babu
		Input: none
	   Output: none
  Description: handles Ctrl+C
Date           Initials    Description
11-Apr-2019    RSB          Initial & cleanup
*****************************************************************************/
void intHandler(int nothing)
{
	PrintMessage(INFO_MESSAGE, "Quitting (Ctrl + C)...\n");
	//LOW_CloseDevice() might have to be called from here once implemented. 
	gbFinished = 1;

}
/****************************************************************************
     Function: InitSocketSupport
     Engineer: Vitezslav Hola
        Input: none
       Output: int32_t - 1 if socket support could be initialised
  Description: Initialize socket support.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t InitSocketSupport(void)
{
   int32_t iCnt;
#ifndef __LINUX
   static WSADATA wsaData;
   if (WSAStartup(MAKEWORD(1,1), &wsaData) != 0)
      {
      PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_NO_WSOCK_IFACE);
      return 0;
      }
#endif
   //init some variables
   for (iCnt=0; iCnt < MAX_TAP_NUM; iCnt++)
      {
      tyConnection[iCnt].bConnected = 0;
      tyConnection[iCnt].iListener = -1;
      tyConnection[iCnt].iRemote = -1;
      tyConnection[iCnt].usCurrentPort=0;
      tyConnection[iCnt].bNoAckMode = 0;
      }
   return 1;
}

/****************************************************************************
     Function: OpenListenerSocket
     Engineer: Vitezslav Hola
        Input: unsigned short usPort - port to open the listner socket on
       Output: int32_t - 1 if socket could be opened successfully
  Description: Opean listener socket
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t OpenListenerSocket(unsigned short usPort)
{
   int32_t iTemp, iTCnt, iFlags, iError;
#ifndef __LINUX
   uint32_t arg = 0x1;
#endif
   iTCnt = 0;     // starting from first TAP
   while(tySI.ulTAP[iTCnt] != 0)
      {
      memset(&tySockAddr[iTCnt], 0, sizeof(tySockAddr[iTCnt]));
      tyConnection[iTCnt].iListener = -1;
      tyConnection[iTCnt].iListener = (int32_t)socket(PF_INET, SOCK_STREAM, 0);
      if (tyConnection[iTCnt].iListener < 0)
         {
         iError = GetSocketError();
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_NO_LISTN_SOCKET, iError);
         return 0;
         }
      // set to non-blocking  
      iFlags = fcntl((uint32_t)tyConnection[iTCnt].iListener, F_GETFL, 0);   //lint !e569 
      if(iFlags < 0)
         iFlags = 0;
      iError = fcntl((uint32_t)tyConnection[iTCnt].iListener, F_SETFL, iFlags | O_NONBLOCK );  //lint !e569 !e737 !e713
      if(iError)
         {
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_SET_NONBLK);       
         return 0;
         }
      iTemp = 1;
      if(setsockopt((uint32_t)tyConnection[iTCnt].iListener, SOL_SOCKET, SO_REUSEADDR, (char *)&iTemp, sizeof(iTemp)))
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_SET_OPT);
      tySockAddr[iTCnt].sin_family      = PF_INET;
      tySockAddr[iTCnt].sin_port        = htons(usPort);
      tySockAddr[iTCnt].sin_addr.s_addr = INADDR_ANY;
      if (bind((uint32_t)tyConnection[iTCnt].iListener, (TySockAddr *)&tySockAddr[iTCnt], sizeof(tySockAddr[iTCnt])) ||
          listen((uint32_t)tyConnection[iTCnt].iListener, 1))
         {
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_BIND_ADDR);
         return 0;
         }
      tyConnection[iTCnt].usCurrentPort = usPort++;
      if(++iTCnt > (MAX_TAP_NUM-1))
         break;
      }
   return 1;
}

/****************************************************************************
     Function: CheckForConnection
     Engineer: Vitezslav Hola
        Input: int32_t iConnection - connection number
       Output: int32_t - 1 if we got a connection, 0 if accept failed
  Description: Check specified connection.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static int32_t CheckForConnection(int32_t iConnection)
{
   socklen_t   iTemp;
   int32_t iError, iFlags;
#ifndef __LINUX
   uint32_t arg = 0x1;
#endif
   memset(&tySockAddr[iConnection], 0, sizeof(tySockAddr[iConnection]));
   // keep log file as up to date as possible...
   if (tySI.pDebugFile != NULL)
      fflush(tySI.pDebugFile);
   iTemp = sizeof(tySockAddr[iConnection]);
   if(tyConnection[iConnection].iRemote < 0)
      {  //there is no connection 
      if(tyConnection[iConnection].bConnected)
         {  //it was connected before
         tyConnection[iConnection].bConnected = 0;
         PrintMessage(INFO_MESSAGE, 
                      GDB_EMSG_SRV_ERR_CON_PORT, 
                      tyConnection[iConnection].usCurrentPort);
         tyConnection[iConnection].bNoAckMode = 0;
         }
      // so try to connect
      tyConnection[iConnection].iRemote = (int32_t)accept((uint32_t)tyConnection[iConnection].iListener, (TySockAddr *)&tySockAddr[iConnection], &iTemp);
	  if (tyConnection[iConnection].iRemote == -1)
		  return 0;
      // set to non-blocking for connection.  
      iFlags = fcntl((uint32_t)tyConnection[iConnection].iRemote, F_GETFL, 0);  //lint !e569
      if(iFlags < 0)
         iFlags = 0;
      iError = fcntl((uint32_t)tyConnection[iConnection].iRemote, F_SETFL, iFlags | O_NONBLOCK );  //lint !e569 !e737 !e713
      if(iError)
         {
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_SET_NONBLK);       
         return 0;
         }
      iTemp = 1;
      if(setsockopt((uint32_t)tyConnection[iConnection].iRemote, SOL_SOCKET, SO_KEEPALIVE, (char *)&iTemp, sizeof(iTemp)))
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_SET_OPT);
      iTemp = 1;
      if(setsockopt((uint32_t)tyConnection[iConnection].iRemote, IPPROTO_TCP, TCP_NODELAY, (char *)&iTemp, sizeof(iTemp)))
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_SET_OPT);
      if(tyConnection[iConnection].bConnected == FALSE)
         { 
         PrintMessage(INFO_MESSAGE, 
                      "Got a debugger connection from %s on port %d.", 
                      inet_ntoa(tySockAddr[iConnection].sin_addr), 
                      tyConnection[iConnection].usCurrentPort);
         strncpy(tyConnection[iConnection].szAddress, 
                 inet_ntoa(tySockAddr[iConnection].sin_addr), 
                 19);
         tyConnection[iConnection].bNoAckMode = 0;
         }
      // TODO: what happens if setsockopt() not succeed ?
      }
   tyConnection[iConnection].bConnected = 1;
   return 1;
}

/****************************************************************************
     Function: CloseConnection
     Engineer: Vitezslav Hola
        Input: int32_t iConnnection - current connection
       Output: none
  Description: Close the remote connection
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void CloseConnection(int32_t iConnection)
{
   if (tyConnection[iConnection].iRemote == -1)
      return;
   (void)closesocket((uint32_t)tyConnection[iConnection].iRemote);
   tyConnection[iConnection].iRemote= -1;
   PrintMessage(INFO_MESSAGE, 
                "Debugger disconnected, closed connection to %s on port %d.", 
                tyConnection[iConnection].szAddress, 
                tyConnection[iConnection].usCurrentPort);
   strncpy(tyConnection[iConnection].szAddress, "                   ", 19);
   tyConnection[iConnection].bConnected = 0;
}

/****************************************************************************
     Function: CloseAllConnections
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Close all active remote connections
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
static void CloseAllConnections(void)
{
   int32_t iTCnt;
   for(iTCnt=0; iTCnt < MAX_TAP_NUM; iTCnt++)
      {
      if (tyConnection[iTCnt].iRemote != -1)
         {
         (void)closesocket((uint32_t)tyConnection[iTCnt].iRemote);
         tyConnection[iTCnt].iRemote = -1;
         PrintMessage(INFO_MESSAGE, 
                      "Debugger disconnected, closed connection to %s on port %d.", 
                      tyConnection[iTCnt].szAddress, tyConnection[iTCnt].usCurrentPort);
         strncpy(tyConnection[iTCnt].szAddress, "                   ", 19);
         tyConnection[iTCnt].bConnected = 0;
         }
      }
}

/****************************************************************************
     Function: CloseAllListeners
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Close the listner socket
Date           Initials    Description
08-Feb-2007    VH          Initial & cleanup
*****************************************************************************/
static void CloseAllListeners(void)
{
   int32_t iCnt;
   for(iCnt=0; iCnt < MAX_TAP_NUM; iCnt++)
      {
      if (tyConnection[iCnt].iListener != -1)
         {
         (void)closesocket((uint32_t)tyConnection[iCnt].iListener);
         tyConnection[iCnt].iListener = -1;
         }
      }
}

/****************************************************************************
     Function: ReadChar
     Engineer: Vitezslav Hola
        Input: int32_t iConnection - connection number
       Output: int32_t - next character from remote port buffer, or -1 if fail
  Description: Read next character from connection
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
07-Aug-2008    NCH         Fixed bug with long packets & nonblocked calls
*****************************************************************************/
static int32_t ReadChar(int32_t iConnection)
{
   static char    szReadBuffer[GDBSERV_SOCKET_BUFFER_SIZE];
   static int32_t     iBytesLeft = 0;
   static char   *pszNextChar;
   int32_t            iError;

   if (iBytesLeft <= 0)
      {
      iBytesLeft = recv((uint32_t)tyConnection[iConnection].iRemote, szReadBuffer, sizeof(szReadBuffer), 0);
      if (iBytesLeft <= 0)
         {
         if (iBytesLeft < 0)
            {
            iError = GetSocketError();
            switch (iError)
               {
#ifdef __LINUX
               case EAGAIN: //there is no data in the rcv buffer
                  return -EAGAIN;
               case ECONNREFUSED : //connection lost
                  tyConnection[iConnection].iRemote = -1;
                  break;
               default:
                  PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_RCV_DATA, iError);
                  PrintMessage(LOG_MESSAGE, "Connection: %d, iRemote: %d", iConnection, tyConnection[iConnection].iRemote);
                  PrintMessage(DEBUG_MESSAGE, "Socket Error: %i, %s", errno, strerror(errno));
                  break;
#else
               case WSAEWOULDBLOCK: //there is no data in the rcv buffer
                  return -WSAEWOULDBLOCK;
               case WSAECONNRESET: //connection lost
                  tyConnection[iConnection].iRemote = -1;
                  break;
               default:
                  PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_RCV_DATA, iError);
                  PrintMessage(LOG_MESSAGE, "Connection: %d, iRemote: %d", iConnection, tyConnection[iConnection].iRemote);
                  break;
#endif
               }
            }
         else
            {
//            PrintMessage(INFO_MESSAGE, "Connection closed by debugger.");
            tyConnection[iConnection].iRemote = -1;
            }
         return -1;
         }
      pszNextChar = szReadBuffer;
      }
   iBytesLeft--;
#if defined(ARC) || defined(RISCV)
   return *pszNextChar++ & 0xFF;
#else
   return *pszNextChar++ & 0x7F;
#endif
}

/****************************************************************************
     Function: ReadPacket
     Engineer: Vitezslav Hola
        Input: char *pszBuf - buffer to receive Packet data
               int32_t iBufSize - maximum size of buffer
               int32_t iConnection - connection number to read from
       Output: int32_t : length of packet returned, or -1 if fail
  Description: Read packet.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
07-Aug-2008    NCH         Fixed bug with long packets & nonblocked calls
*****************************************************************************/
static int32_t ReadPacket(char *pszBuf, int32_t iBufSize, int32_t iConnection)
{
   unsigned char ucCheckSum, ucCheckSumReceived;
   int32_t iChar, iCH, iCL, iCheckH, iCheckL, iBufIndex;
   int32_t bGotFirstChar;

   if (iBufSize <= 0)
      return -1;
   for (;;)
      {
      // skip characters up to next '$'
      bGotFirstChar = 0;
      for (;;)
         {
         iChar = ReadChar(iConnection);
         if (iChar < 0)
            {
            if(0 == bGotFirstChar)
               return -1; // come again later
            /* we have the first char, so continue to wait for other */
            switch(iChar)
               {
#ifdef __LINUX
               case (-EAGAIN):
#else
               case (-WSAEWOULDBLOCK):
#endif
                  continue;
               default:
                  return -1;
               }
            }
         bGotFirstChar = 1; //we have a valid char
         if (iChar == '$')
            break;
         /*0x3 is Ctrl+C packet*/
         if (iChar == 0x3)
            {
            /*Exiting from the function after assigning the read value(0x3) to pszBuf*/
            *pszBuf = (char)iChar;
            pszBuf[1] = '\0';
			if (tySI.bDebugOutput)
				LogMessage("Recv packet:", "Ctrl+C");
            return 2;
            }
           
         if (tySI.bDebugOutput)
            PrintMessage(LOG_MESSAGE, 
                         "discarding character '%c' (0x%02x).", 
                         (unsigned char)iChar, (unsigned char)iChar);
         }
      // collect characters up to next '#'
      ucCheckSum = 0;
      iBufIndex  = 0;
      for (;;)
         {
         iChar = ReadChar(iConnection);
         if (iChar < 0)
            {
            switch(iChar)
               {
#ifdef __LINUX
               case (-EAGAIN):
#else
               case (-WSAEWOULDBLOCK):
#endif
                  continue;
               default:
                  return -1;
               }
            }
		 if (iChar == '#')
			 break;
         ucCheckSum += (unsigned char)iChar;
         if (iBufIndex >= iBufSize-1)
            {
            PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_LONG_PACKET, iBufSize);
            return -1;
            }
         pszBuf[iBufIndex++] = (char)iChar;

         }
      // terminate packet string
      pszBuf[iBufIndex] = '\0';
      // read the received CheckSum
      for(;;)
         {
         iCH = ReadChar(iConnection);
         if (iCH < 0)
            {
            switch(iCH)
               {
#ifdef __LINUX
               case (-EAGAIN):
#else
               case (-WSAEWOULDBLOCK):
#endif
                  continue;
               default:
                  return -1;
               }
            }
         else
            break;
         }
      for(;;)
         {
         iCL = ReadChar(iConnection);
         if (iCL < 0)
            {
            switch(iCL)
               {
#ifdef __LINUX
               case (-EAGAIN):
#else
               case (-WSAEWOULDBLOCK):
#endif
                  continue;
               default:
                  return -1;
               }
            }
         else
            break;
         }
      // convert received CheckSum
      iCheckH = FromHex(iCH);
      if (iCheckH < 0)
         return -1;
      iCheckL = FromHex(iCL);
      if (iCheckL < 0)
         return -1;
      ucCheckSumReceived = (unsigned char)(iCheckH*16 + iCheckL);
      // verify CheckSum
      if (ucCheckSumReceived == ucCheckSum)
         break;
      PrintMessage(ERROR_MESSAGE, 
                   GDB_EMSG_SRV_INV_PACK_CHKSUM, 
                   ucCheckSumReceived, ucCheckSum, pszBuf);
      // indicate resend required
      (void)send((uint32_t)tyConnection[iConnection].iRemote, "-", 1, 0);
      // continue to wait for a valid packet...
      }
   // send ACK that packet received...
	  if(tyConnection[iConnection].bNoAckMode == 0)
		(void)send((uint32_t)tyConnection[iConnection].iRemote, "+", 1, 0);
#ifdef DEBUG_L1
   PrintMessage(DEBUG_MESSAGE, 
                "(%u)\t<= %s => +", 
                tyConnection[iConnection].usCurrentPort, pszBuf);
#endif
   if (tySI.bDebugOutput)
      LogMessage("Recv packet:", pszBuf);
   
   //Handle QStartNoAckMode here itself
   if (strncmp(pszBuf, "QStartNoAckMode", 15) == 0) {
       strcpy(szOutPacket + 1, "OK");
       WritePacket(szOutPacket, sizeof(szOutPacket), iConnection);
       tyConnection[iConnection].bNoAckMode = 1;
       iBufIndex = 0;
   }

   return iBufIndex;
}

/****************************************************************************
     Function: WritePacket
     Engineer: Vitezslav Hola
        Input: char *pszBuf - buffer containing Packet data to send at pszBuf+1
               int32_t iBufSize - maximum size of buffer
               int32_t iConnection - connection number to write to
       Output: int32_t - 1 if packet could be sent
  Description: Write packet
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t WritePacket(char *pszBuf, int32_t iBufSize, int32_t iConnection)
{
   int32_t iIndex, iStrLen, iError, iBytesReceived, iBytesSent;
   unsigned char ucCheckSum = 0;
   char szAckBuffer[8];
   // the first byte is reserved for us...
   pszBuf[0] = '$';        //lint !e527
   iStrLen = (int32_t)strlen(pszBuf);
   if (iStrLen > (iBufSize - GDBSERV_PACKET_RESERVE_BYTES))
      iStrLen = iBufSize - GDBSERV_PACKET_RESERVE_BYTES;
   // calculate checksum...
   for (iIndex=1; iIndex<iStrLen; iIndex++)
      ucCheckSum += (unsigned char)pszBuf[iIndex];    //TODO: make uc buffer
   // and the next four bytes are reserved for us...
   pszBuf[iIndex++] = '#';
   pszBuf[iIndex++] = (char)ToHex(ucCheckSum >> 4);
   pszBuf[iIndex++] = (char)ToHex(ucCheckSum);
   pszBuf[iIndex  ] = '\0';
   iBytesSent = send((uint32_t)tyConnection[iConnection].iRemote, pszBuf, iIndex, 0);
   if (iBytesSent != iIndex)
      {
      if (iBytesSent < 0)
         {
         iError = GetSocketError();
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_NUM_SND_DATA, iError, pszBuf);
         }
      else
         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ERR_SND_DATA, pszBuf);
      return 0;
      }
   else
      {
      if (tySI.bDebugOutput)
         LogMessage("Sent packet:", pszBuf);
      // now wait for positive ACK
	 
      for (;;)
         {
		  if (tyConnection[iConnection].bNoAckMode == 1)
			  break;
         iBytesReceived = recv((uint32_t)tyConnection[iConnection].iRemote, szAckBuffer, 1, 0);
         if (iBytesReceived <= 0)
            {
            iError = GetSocketError();
            switch (iError)
               {
#ifdef __LINUX
               case EAGAIN:
                  continue;
               default:
               PrintMessage(DEBUG_MESSAGE, "Socket Error: %i, %s", errno, strerror(errno));
               PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_NO_ACK);
               return 0;
#else
               case WSAEWOULDBLOCK: 
                  continue;
               default:
               PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_NO_ACK);
               return 0;
#endif
               }
            }
         if (szAckBuffer[0] == '+')
            break;
         }
      }
#ifdef DEBUG_L1
   PrintMessage(DEBUG_MESSAGE, 
                "(%u)\t=> %s <= %c", 
                tyConnection[iConnection].usCurrentPort, pszBuf, szAckBuffer[0]);
#endif
   return 1;
}

/****************************************************************************
     Function: ServerMain
     Engineer: Vitezslav Hola
        Input: unsigned short usPort - port to open the listner socket on
       Output: int32_t : TO_DO
  Description: main part of GDB server
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
31-Mar-2008    RS          Modified for ARM support
24-Jul-2008    NCH         Added MIPS Support
13-Jan-2010    SVL         Fix for ininite loop, when a socket error occurs
26-Apr-2013    SV          Fix for Socket error without a valid connection
*****************************************************************************/
int32_t ServerMain(unsigned short usPort)
{
   int32_t iPacketSize;
   int32_t iIndex = 0;
   int32_t iTapIdx = 0;
   int32_t iChar = 0;
   int32_t bExecuting = FALSE;
   TyStopSignal tyStopSignal;
   signal(SIGINT, intHandler);
   // init support
   if (!InitSocketSupport())
      return 0;
   if (!OpenListenerSocket(usPort))
      return 0;

   //dbg:
#if defined(MIPS) && defined(DEBUG)
   {
      extern int32_t TestR(void); 
      if (1 == tySI.bIsConnected)
         (void)TestR();
   }
#endif
   //dbg:

   while (tySI.ulTAP[iIndex] != 0) //lint !e661
      {
#if defined(ARC) || defined(MIPS) || defined(RISCV)
      PrintMessage(INFO_MESSAGE, 
                   "Waiting for debugger connection on port %u for core %d.", 
                   tyConnection[iIndex].usCurrentPort, tySI.ulTAP[iIndex]);
#elif defined(ARM)
      PrintMessage(INFO_MESSAGE, 
                   "Waiting for debugger connection on port %u for core %d.", 
                   tyConnection[iIndex].usCurrentPort, tySI.ulTAP[iIndex]-1);
#endif
      if (++iIndex > (MAX_TAP_NUM-1))
         break;
      }
   PrintMessage(INFO_MESSAGE,"Press 'Q' to Quit."); 
   while (!gbFinished)
      {
      for (iIndex=0; iIndex < MAX_TAP_NUM; iIndex++)
         {  // check all connections (by polling them in infinite loop)
         if (tySI.ulTAP[iIndex] == 0)
            break; // core is not active, just skip it
		 // TAP index starts from 0 in lower layers and 1 in GDB Server
		 iTapIdx = tySI.ulTAP[iIndex] - 1;
         if (KeyHit())
            {  // user hit some key, he may want to quit GDB server ...
            iChar = GetchWithoutEcho();
            if ((iChar == 'q') || (iChar == 'Q'))
               {
               PrintMessage(INFO_MESSAGE,"Quitting...\n");
               (void)LOW_CloseDevice(iTapIdx);
               gbFinished = 1;
               break;
               }
            }
         // 
         // Handling GDB Server main loop could be different for each supported architecture.
         // So be free to add any support you wish for your architecture using proper switches.
         //
#if defined(ARC)
         /*Check if we have established a connection with GDB*/
         if (CheckForConnection(iIndex)) {
             if (tySI.tyProcConfig.Processor._ARC.bRunningFlag[iIndex] == TRUE) {
                 (void)LOW_SetActiveTapIndex((int32_t)tySI.ulTAP[iIndex]);
                 //We expect the core is running. Check if it is a TRUE! but first take a rest, slip ...
                 PrintMessage(DEBUG_MESSAGE, "Sleep(%d);", tySI.tyProcConfig.Processor._ARC.uiSleepTime);
                 MsSleep(tySI.tyProcConfig.Processor._ARC.uiSleepTime); //TODO: choose optimum sleep

                 if (!LOW_IsProcessorExecuting(&bExecuting, iIndex)) {
                     PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ATTEMPT_STATUS_CORE);
                     //TODO: Return error to GDB if GDB-GDB server is connected and target is running
                 }

                 /*If core just now halted*/
                 if (!bExecuting) {
                     //check if the core is awakened from powerdown
                     //todo Depends on the GDB...
                     if ((tySI.tyProcConfig.Processor._ARC.iPDownFlag[iIndex] == 0x10)) {// last state = !PD; prev. state = PD;
                         char szTmpOutPacket[100];
                         // make prev state = last state;
                         tySI.tyProcConfig.Processor._ARC.iPDownFlag[iIndex] = 0x00;
                         sprintf(szTmpOutPacket, "ARC core has exited power-down mode\n");
                         szOutPacket[0] = 'O';
                         szOutPacket[1] = 'O';
                         szOutPacket[2] = 0;
                         AddStringToOutAsciiz(szTmpOutPacket, szOutPacket + 2);
                         // return a response
                         (void)WritePacket(szOutPacket, sizeof(szOutPacket), iIndex);
                     }
                     (void)LOW_PostBreakAction(iIndex);

                     /*Get Cause of break*/
                     if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyStopSignal)) {
                         PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ATTEMPT_STATUS_CORE);
                     }
                     /*Prepare stop reply packet*/
                     (void)DSP_ContinueReply(tyStopSignal, szOutPacket + 1);

                     /*Send this notification packet to GDB*/
                     if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex)) {
                         CloseConnection(iIndex);
                         break;
                     }
                 } else {
                     /*Core is still running; ARC specific code. */ 
                     /*TODO: Check it is relevent */
                     uint32_t ulPDown;
                     if ((tySI.tyProcConfig.Processor._ARC.iPDownFlag[iIndex] == 0x01)) {// last state = PD; prev. state = !PD;
                         char szTmpOutPacket[100];
                         // make prev state = last state;
                         tySI.tyProcConfig.Processor._ARC.iPDownFlag[iIndex] = 0x11;
                         (void)LOW_ReadRegister(0x490, &ulPDown);
                         // tyStopSignal = TARGET_SIGTRAP;//TARGET_SIGCONT;
                         // if (!DSP_ContinueReply(tyStopSignal, szOutPacket+1))
                         //     return 0;
                         sprintf(szTmpOutPacket, "ARC core is in power-down mode\n");
                         szOutPacket[0] = 'O';
                         szOutPacket[1] = 'O';
                         szOutPacket[2] = 0;
                         AddStringToOutAsciiz(szTmpOutPacket, szOutPacket + 2);
                         // return a response
                         if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex)) {
                             CloseConnection(iIndex);
                             continue;
                         }
                     }
                     //check if the core is awakened from powerdown
                     //todo Depends on the GDB...
                     else if ((tySI.tyProcConfig.Processor._ARC.iPDownFlag[iIndex] == 0x10)) {// last state = !PD; prev. state = PD;
                         char szTmpOutPacket[100];
                         // make prev state = last state;
                         tySI.tyProcConfig.Processor._ARC.iPDownFlag[iIndex] = 0x00;
                         sprintf(szTmpOutPacket, "ARC core has exited power-down mode\n");
                         szOutPacket[0] = 'O';
                         szOutPacket[1] = 'O';
                         szOutPacket[2] = 0;
                         AddStringToOutAsciiz(szTmpOutPacket, szOutPacket + 2);
                         // return a response
                         (void)WritePacket(szOutPacket, sizeof(szOutPacket), iIndex);
                     }
                 }
             }
             /*Now, we have finished sending notification packets to GDB,Check if GDB send any packet*/
             iPacketSize = ReadPacket(szInPacket, sizeof(szInPacket), iIndex);
             if (iPacketSize <= 0)
                 continue;

             /*Decode the Packet and process*/
             if (DSP_Decode(szInPacket, szOutPacket + 1, sizeof(szOutPacket) - GDBSERV_PACKET_RESERVE_BYTES, iTapIdx)) {
                 if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex)) {
                     CloseConnection(iIndex);
                     continue;
                 }
             }

             /*TODO: maybe DSP_Decode should return flag to indicate which packets should Disconnect.
                     For now we Disconnect on Detach.*/
             if (szInPacket[0] == 'D')
                 CloseConnection(iIndex);

         } else {
             /*No connection; check for next connection*/
             continue;
         }
#elif defined(RISCV) 
			/*Check if we have established a connection with GDB*/
			if (CheckForConnection(iIndex))
			{
				if (tySI.tyProcConfig.Processor._RISCV.bRunningFlag[iTapIdx] == TRUE)
				{
					(void)LOW_SetActiveTapIndex(iTapIdx);
					//We expect the core is running. Check if it is a TRUE! but first take a rest, slip ...
					PrintMessage(DEBUG_MESSAGE, "Sleep(%d);", tySI.tyProcConfig.Processor._RISCV.uiSleepTime);
					//TODO: provide a lesser sleep here
					MsSleep(tySI.tyProcConfig.Processor._RISCV.uiSleepTime);

					/*Update the status core.If the core just now halted, let GDB know this*/
					if (GDBSERV_NO_ERROR != LOW_IsProcessorExecuting(&bExecuting, iTapIdx))
					{
						PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ATTEMPT_STATUS_CORE);
						//TODO: Return error to GDB if GDB-GDB server is connected and target is running
					}
					/*If core just now halted*/
					if (!bExecuting)
					{
						/*Get Cause of break*/
						if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyStopSignal))
						{
							PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ATTEMPT_STATUS_CORE);
						}
						/*Prepare stop reply packet*/
						(void)DSP_ContinueReply(tyStopSignal, szOutPacket + 1);

						/*Send this notification packet to GDB*/
						if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
						{
							CloseConnection(iIndex);
							break;
						}
						break;
					}
					else
					{
						/*Core is still running;do nothing*/
					}
				}

				/*Now, we have finished sending notification packets to GDB,Check if GDB send any packet*/
				iPacketSize = ReadPacket(szInPacket, sizeof(szInPacket), iIndex);
				if (iPacketSize <= 0)
					continue;
				/*No idea, why this check is required here. Without this check, step fails (target just continues) at times!!!*/
				(void)LOW_SetActiveTapIndex(iTapIdx);
				if (GDBSERV_NO_ERROR != LOW_IsProcessorExecuting(&bExecuting, iTapIdx))
				{
					PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ATTEMPT_STATUS_CORE);
					//TODO: Return error to GDB if GDB-GDB server is connected and target is running
				}
				/*Decode the Packet and process*/
				if (DSP_Decode(szInPacket,szOutPacket + 1,sizeof(szOutPacket) - GDBSERV_PACKET_RESERVE_BYTES, iTapIdx))
				{
					if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
					{
						CloseConnection(iIndex);
						continue;
					}
				}
				/*TODO: maybe DSP_Decode should return flag to indicate which packets should Disconnect.
						For now we Disconnect on Detach.*/
				if (szInPacket[0] == 'D')
					CloseConnection(iIndex);
			}
			else
			{
				/*No connection; check for next connection*/
				continue;
			}
#elif defined(MIPS) 
         if (tySI.tyProcConfig.Processor._MIPS.bRunningFlag[iIndex] == TRUE)
            {
            //todo not supported            (void)LOW_SetActiveTapIndex(iIndex);
            // We expect the core is running. 
            // Check if it is a TRUE! but first take a rest, slip ...
            //PrintMessage(DEBUG_MESSAGE, "Sleep(%d);", tySI.tyProcConfig.Processor._MIPS.uiSleepTime);
            //MsSleep(tySI.tyProcConfig.Processor._MIPS.uiSleepTime);

            if (GDBSERV_NO_ERROR != LOW_IsProcessorExecuting(&bExecuting, iIndex))
               return 0;
            if (bExecuting)
               {  // core is still running, check if there is a GDB data waiting. If so, halt the core and process.
               if (CheckForConnection(iIndex) == TRUE)
                  {
                  if (RemoteDataPresent(iIndex) == TRUE)
                     {  // we have GDB data. Halt the core! Process the data!
                     if (GDBSERV_NO_ERROR != LOW_Halt(iIndex))
                        return 0;
                     (void)LOW_PostBreakAction(iIndex);
                     if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyStopSignal))
                        return 0;
                     if (!DSP_ContinueReply(tyStopSignal, szOutPacket+1))
                        return 0;
                     // return a response
                     if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
                        {
                        CloseConnection(iIndex);
                        continue;
                        }
                     }
                  //todo Depends on the GDB...
                  else
                     // we have no GDB data waiting. Continue with next core.
                     continue; 
                  }
               }
            else
               {  // the core is halted. Why? Check and return response!
               (void)LOW_PostBreakAction(iIndex);
               if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyStopSignal))
                  return 0;
               if (!DSP_ContinueReply(tyStopSignal, szOutPacket+1))
                  return 0;   // error
               // return a response
               if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
                  {
                  CloseConnection(iIndex);
                  continue;
                  }
               }
            }
         else
            {  // here we expect the core to be halted. Is it TRUE? 
            // Check and process.
            if (CheckForConnection(iIndex))
               {  //conected
               iPacketSize = ReadPacket(szInPacket, sizeof(szInPacket), iIndex);
               if (iPacketSize <= 0)
                  continue;
               // just check if the core is running
               //todo not supported               (void)LOW_SetActiveTapIndex(iIndex);
               if (GDBSERV_NO_ERROR != LOW_IsProcessorExecuting(&bExecuting, iIndex)){
                      szOutPacket[0]='O';
                      szOutPacket[1]=0;
                      AddStringToOutAsciiz(tySI.szLastError, szOutPacket+1);
                      (void)WritePacket(szOutPacket, sizeof(szOutPacket), iIndex);
                      return 0;
                  }  
               if (bExecuting)
                  {  // the core is running?!?!? Why?
                  if (tySI.tyProcConfig.Processor._MIPS.iAttemptStop[iIndex] < 
                      MAX_ATTEMPT_TO_STOP_CORE)
                     {// Attempt to stop the core message
                     tySI.tyProcConfig.Processor._MIPS.iAttemptStop[iIndex]++;
                     PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_ATTEMPT_HALT_CORE);
                     }
                  else
                     {
                     if (tySI.tyProcConfig.Processor._MIPS.iAttemptStop[iIndex] == 
                         MAX_ATTEMPT_TO_STOP_CORE)
                        {// Maximum attempt count is reached. 
                        tySI.tyProcConfig.Processor._MIPS.iAttemptStop[iIndex]++;
                        PrintMessage(ERROR_MESSAGE, GDB_EMSG_SRV_UNABLE_HALT_CORE);
                        }
                     }
                  if (GDBSERV_NO_ERROR != LOW_Halt(iIndex))
                     return 0; // error!
                  }
               else
                  {
                  tySI.tyProcConfig.Processor._MIPS.iAttemptStop[iIndex]=0;
                  }
               // note, we reserve 1 byte at beginning of the outbound packet
               // and another 4 bytes at the end...
               if (DSP_Decode(szInPacket, 
                              szOutPacket+1, 
                              sizeof(szOutPacket)-GDBSERV_PACKET_RESERVE_BYTES, 
                              iIndex))
                  {  // return a response
                  if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
                     {
                     CloseConnection(iIndex);
                     continue;
                     }
                  }
               // TODO: maybe DSP_Decode should return flag to indicate 
               // which packets should Disconnect.
               // For now we Disconnect on Detach.
               if (szInPacket[0] == 'D')
                  CloseConnection(iIndex);
               }
            else
               {
               //printf("no conn\n");
               //connection closed.
               }
            }

#elif defined (ARM)
         if (tySI.tyProcConfig.Processor._ARM.bRunningFlag[tySI.ulTAP[iIndex]-1] == TRUE)
            {
            LOW_SetActiveTapIndex(tySI.ulTAP[iIndex]-1);

            //Save the current core index
            tySI.tyProcConfig.ulCoreIndex = (tySI.ulTAP[iIndex]-1);

            //We expect the core is running. Check if it is a TRUE! but first take a rest, slip ...
            //PrintMessage(DEBUG_MESSAGE, "Sleep(%d);", tySI.tyProcConfig.Processor._ARC.uiSleepTime);
            //MsSleep(tySI.tyProcConfig.Processor._ARC.uiSleepTime);
            if (!LOW_IsProcessorExecuting(&bExecuting, tySI.ulTAP[iIndex]-1))
               return 0;
            if (bExecuting)
               {  // core is still running, check if there is a GDB data waiting. If so, halt the core and process.
               if (CheckForConnection(iIndex) == TRUE)
                  {
                  if (RemoteDataPresent(iIndex) == TRUE)
                     {  // we have GDB data. Halt the core! Process the data!
                     if (!LOW_Halt(tySI.ulTAP[iIndex]-1))
                        return 0;
                     //(void)LOW_PostBreakAction(iIndex);
                     if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyStopSignal))
                        return 0;
                     if (!DSP_ContinueReply(tyStopSignal, szOutPacket+1))
                        return 0;
                     // return a response
                     if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
                        {
                        CloseConnection(iIndex);
                        continue;
                        }
                     }
                  else
                     continue; // we have no GDB data waiting. Continue with next core.
                  }
               }
            else
               {  // the core is halted. Why? Check and return response!

               //(void)LOW_PostBreakAction(iIndex);
               if (GDBSERV_NO_ERROR != LOW_GetCauseOfBreak(&tyStopSignal))
                  return 0;
               if (!DSP_ContinueReply(tyStopSignal, szOutPacket+1))
                  return 0;   // error
               // return a response
               if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
                  {
                  CloseConnection(iIndex);
                  continue;
                  }
               }
            }
         else
            {  // here we expect the core to be halted. Is it TRUE? 
            // Check and process.
            if (CheckForConnection(iIndex))
               {  //connected
               iPacketSize = ReadPacket(szInPacket, sizeof(szInPacket), iIndex);
               if (iPacketSize <= 0)
                  continue;
               // just check if the core is running

               LOW_SetActiveTapIndex(tySI.ulTAP[iIndex]-1);

               if (!LOW_IsProcessorExecuting(&bExecuting, tySI.ulTAP[iIndex]-1))
                  return 0;  // error! 
               if (bExecuting)
                  {
                  if (LOW_Halt(tySI.ulTAP[iIndex]-1) == FALSE)
                     return FALSE; // error!
                  }
               else
                  {
                  }

               // note, we reserve 1 byte at beginning of the outbound packet
               // and another 4 bytes at the end...
               if (DSP_Decode(szInPacket, 
                              szOutPacket+1, 
                              sizeof(szOutPacket)-GDBSERV_PACKET_RESERVE_BYTES, 
                              tySI.ulTAP[iIndex]-1))
                  {  // return a response
                  if (!WritePacket(szOutPacket, sizeof(szOutPacket), iIndex))
                     {
                     CloseConnection(iIndex);
                     continue;
                     }
                  }
               // TODO: maybe DSP_Decode should return flag to indicate 
               // which packets should Disconnect.
               // For now we Disconnect on Detach.
               if (szInPacket[0] == 'D')
                  CloseConnection(iIndex);
               }
            else
               {
               //printf("no conn\n");
               //connection closed.
               }
            }
#else
#error "Architecture not supported."
#endif
         }
      }
#ifdef RISCV
   TyStopSignal  tyStopSig = TARGET_SIGTRAP;
   // When exiting GDB server, just run all connected cores
   for (iIndex = 0; iIndex < MAX_TAP_NUM; iIndex++)
   {
	   if (tySI.ulTAP[iIndex] == 0)
		   break; // no more active TAPS, exit

	   (void)LOW_SetActiveTapIndex(tySI.ulTAP[iIndex]);
	   (void)LOW_RestoreFPS1();
   }
#endif
   CloseAllConnections();
   CloseAllListeners();
   return 0;
}

/****************************************************************************
     Function: RemoteDataPresent
     Engineer: Vitezslav Hola
        Input: int32_t iConnectionIndex - index for connection
       Output: int32_t - 1 if data is present to be read from the socket, 0 otherwise
  Description: Checks if data is present to be read from the socket.
Date           Initials    Description
08-Feb-2008    VH          Initial & cleanup
*****************************************************************************/
int32_t RemoteDataPresent(int32_t iConnectionIndex)
{
   fd_set tyfdsRead;
   struct timeval tyWaitTime;
   
   if (tyConnection[iConnectionIndex].iRemote < 0)
      return 0; // there is not such a connection, so there is not data waiting here
   else
      {
      FD_ZERO(&tyfdsRead);
      FD_SET((uint32_t)tyConnection[iConnectionIndex].iRemote, &tyfdsRead);   //lint !e717
      tyWaitTime.tv_sec = 0;     
      tyWaitTime.tv_usec = 0;
      if (select(tyConnection[iConnectionIndex].iRemote+1, &tyfdsRead, NULL, NULL, &tyWaitTime) > 0)
         return 1;
      }
   return 0;
}

int32_t wp(char *pszBuf, int32_t iBufSize, int32_t iConnection)
{
	 if(WritePacket(pszBuf,iBufSize,iConnection))
	 {
         CloseConnection(iConnection);
         return 1;
	 }
	 return 0;
}