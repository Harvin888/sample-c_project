File 60-ashling.rules is udev rules file used in Linux to control access 
to Ashling Opella-XD emulators for different users/groups.

Use following steps to provide access to Ashling Opella-XD emulator regarding your access rights policy.
1. Modify "60-ashling.rules" file with intended access rights for users/groups. 
   Default settings in this file allows all users to access Ashling Opella-XD probe.
2. Copy modified file "60-ashling.rules" into /etc/udev/rules.d directory.
   You will need root access rights for this operation.
3. Log as normal user and connect Ashling Opella-XD emulator to PC.
   You should be able to run Ashling Opella-XD software without any restriction.
