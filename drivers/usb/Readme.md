Readme for Opella-XD USB driver
===============================

## Softwares Used
### Driver
#### Windows
libusb-win32<br>
Version: 1.2.6<br>
Website: https://sourceforge.net/projects/libusb-win32/

#### Linux
Builtin with Linux kernel

### Library
#### Windows
libusb-1.0<br>
Version: v1.0.22<br>
https://github.com/libusb/libusb/releases/download/v1.0.22/libusb-1.0.22.7z

libusb0 : v1.2.6<br>
libusb-win32 DLL version for backward compatibilty<br>
Website: https://sourceforge.net/projects/libusb-win32/

#### Linux:
libusb-1.0 : 
v1.0.22 or use the available version with linux distribution
