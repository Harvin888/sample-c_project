/******************************************************************************
       Module: ash510comm.h
     Engineer: Vitezslav Hola
  Description: Header for ASH510 commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/

#ifndef _ASH510COMM_H_
#define _ASH510COMM_H

// function prototype (API)
int ProcessAsh510Command(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _ASH510COMM_H
