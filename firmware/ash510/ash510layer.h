/******************************************************************************
       Module: ash510layer.h
     Engineer: Vitezslav Hola
  Description: Header for ASH510 layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/

#ifndef _ASH510LAYER_H_
#define _ASH510LAYER_H

// maximum supported cores on scanchain
#define MAX_ASH510_CORES_ON_SCANCHAIN           32

// ASH510 memory/register constants
#define ASH510_ACCESS_TYPE_MASK     0x000F
#define ASH510_ACCESS_MEM           0x0000   // access to memory
#define ASH510_ACCESS_CORE          0x0001   // access to core register

// invalid constants
#define ASH510_BAD_SHORT            0xDEAD                              
#define ASH510_BAD_WORD             0xDEADDEAD

// ASH510 memory/register functions
unsigned long ASH510L_WriteWord(unsigned short usAccessSel, unsigned long ulAddress, unsigned long *pulData);
unsigned long ASH510L_ReadWord(unsigned short usAccessSel, unsigned long ulAddress, unsigned long *pulData);
unsigned long ASH510L_WriteWords(unsigned long ulCount, 
                                 unsigned long ulStartAddress, 
                                 unsigned long ulOptions, 
                                 unsigned long *pulWordsWritten, 
                                 unsigned long *pulData);

unsigned long ASH510L_ReadWords(unsigned long ulCount, 
                                unsigned long ulStartAddress, 
                                unsigned long ulOptions, 
                                unsigned long *pulWordsRead, 
                                unsigned long *pulData);

// ASH510 target reset control/sensing
unsigned long ASH510L_TargetReset(unsigned char bAssertReset, unsigned char bEnableResetSensing, unsigned char *pbCurrentResetStatus);
unsigned long ASH510L_SenseTargetReset(void);
unsigned long ASH510L_TargetDebug(unsigned char bDebugRequest, unsigned char *pbDebugAcknowledge);
unsigned long Ash510ConfigureClock(unsigned long ulNewJtagFrequency, 
                                   unsigned char ucFpgaClockDivider, 
                                   unsigned char ucUsePllClockSource);
#endif // #define _ASH510LAYER_H
