/******************************************************************************
       Module: ash510comm.c
     Engineer: Nikolay Chokoev
  Description: ASH510 commands in Opella-XD firmware
  Date           Initials    Description
  11-Apr-2008    NCH         initial
******************************************************************************/
#include "common/common.h"
#include "common/comms.h"
#include "ash510/ash510comm.h"
#include "ash510/ash510layer.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/timer.h"
#include "common/i2c/i2c.h"

// global variable (referred from other modules in diskware)
unsigned short usCurrentCore = 0;
// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;

/****************************************************************************
     Function: ProcessAsh510Command
     Engineer: Nikolay Chokoev
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incomming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing ASh510 commands
Date           Initials    Description
28-May-2007    NCH         Initial
****************************************************************************/
int ProcessAsh510Command(unsigned long ulCommandCode, unsigned long ulSize)
{
   int iReturnValue = 1;

   switch(ulCommandCode)
      {
      // ASH510 specific commands

      case CMD_CODE_ASH510_WRITE:                     
         {     // write block of memory/core or aux registers
         unsigned long ulWordsWritten = 0;
         unsigned long ulResult = ERR_NO_ERROR;
         
         // call function to write words
         ulResult = ASH510L_WriteWords(*((unsigned long *)(ptyRxData->pucDataBuf + 0x04)),   // count
                                       *((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),   // address/offset
                                       *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),   // write options
                                       &ulWordsWritten,                                      // number of words written
                                       ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));   // pointer to data to write
 
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;                      // result code
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulWordsWritten;                // number of words written
         ptyTxData->ulDataSize = 8;
         }
         break;

      case CMD_CODE_ASH510_READ:                     
         {     // read block of memory/core or aux registers
         unsigned long ulWordsRead = 0;
         unsigned long ulResult = ERR_NO_ERROR;
         // call function to read words
         ulResult = ASH510L_ReadWords(*((unsigned long *)(ptyRxData->pucDataBuf + 0x04)),    // count
                                      *((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),    // address/offset
                                      *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),    // read options
                                      &ulWordsRead,                                          // number of words read
                                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x08))      // pointer to store data
                                      );
         // prepare response
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulWordsRead;
         // read words stored from offset 0x08
         ptyTxData->ulDataSize = (8 + ulWordsRead * 4);
         }
         break;

      case CMD_CODE_ASH510_TGT_RESET:
         {  // control target reset status
         unsigned long ulResult = ERR_NO_ERROR;
         ulResult = ASH510L_TargetReset(ptyRxData->pucDataBuf[0x04],    // assert/deassert reset
                                        ptyRxData->pucDataBuf[0x05],    // enable reset sensing
                                      &(ptyTxData->pucDataBuf[0x04]));  // pointer to current reset status
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
         ptyTxData->ulDataSize = 5;
         }
         break;

      case CMD_CODE_ASH510_SET_JTAG_FREQUENCY : 
         {
         unsigned char pucData[8];
         TyPllClockConfiguration tyPllClockConfig;
         unsigned long ulNewFrequency = *((unsigned long *)(ptyRxData->pucDataBuf + 0x4));
         unsigned long ulResult = ERR_NO_ERROR;

         // first try to get settings for requested frequency
         if (ConvertFrequencyToPllSettings(ulNewFrequency, &tyPllClockConfig) != I2C_NO_ERROR)
            ulResult = ERR_FREQ_INVALID;

         // changing frequency is quite tricky, needs to be careful
         ms_wait(1);                      // be sure no scan is running
         if (ulResult == ERR_NO_ERROR)
            // set max divider with internal clock source (60 kHz clock)
            //todo
            ulResult = Ash510ConfigureClock(60000, 0x03, 0);              
         ms_wait(3);                      // wait to stabilize

         // now FPGA is driven fully by internal clock, we can shuffle with PLL synthesizer
         // first disable all clock outputs
         pucData[0] = 0x09;      
         pucData[1] = 0x00;         // set CLKOE to 0x00
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(1);                      // wait to stabilize

         // set clock reference for CY22150 (external 60 MHz clock)
         pucData[0] = 0x12;      
         pucData[1] = 0x30;      
         pucData[2] = 0x00;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 3, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(1);                      // wait to stabilize

         // set default dividers, crosspoint matrix and clock sources
         pucData[0] = 0x0C;      
         pucData[1] = 0x84;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         pucData[0] = 0x44;      
         pucData[1] = 0x00;      
         pucData[2] = 0x00;      
         pucData[3] = 0x3F;      
         pucData[4] = 0x84;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 5, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(1);                      // wait to stabilize

         // now we can change VCO with new settings
         pucData[0] = 0x40;                              
         pucData[1] = tyPllClockConfig.ucReg_40H;
         pucData[2] = tyPllClockConfig.ucReg_41H;        
         pucData[3] = tyPllClockConfig.ucReg_42H;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 4, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         // we need to wait maximum 4 ms to lock up PLL
         ms_wait(4);

         // set divider and src for clk1
         pucData[0] = 0x0C;                              
         pucData[1] = tyPllClockConfig.ucReg_0CH;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(3);

         // set divider and src for clk2
         pucData[0] = 0x47;                              
         pucData[1] = tyPllClockConfig.ucReg_47H;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(3);

         // set crosspoint matrix for LCLK1 and LCLK2
         pucData[0] = 0x44;                              
         pucData[1] = tyPllClockConfig.ucReg_44H;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(3);

         // finally set CLKOE
         pucData[0] = 0x09;                              
         pucData[1] = tyPllClockConfig.ucReg_09H;
         if ((ulResult == ERR_NO_ERROR) && 
             (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(4);

         // set new FPGA divider with internal clock source
         if (ulResult == ERR_NO_ERROR)
            ulResult = Ash510ConfigureClock(60000000, tyPllClockConfig.ucFpgaDiv, 0);     
         ms_wait(3);

         // finally enable new clock settings
         if (ptyRxData->pucDataBuf[0x8])
            {
            if (ulResult == ERR_NO_ERROR)
               ulResult = Ash510ConfigureClock(tyPllClockConfig.ulResultFreq, tyPllClockConfig.ucFpgaDiv, 0x1);     
            }
         else
            {     // internal 60 MHz clock source selected
            if (ulResult == ERR_NO_ERROR)
               ulResult = Ash510ConfigureClock(60000000, 0x0, 0x0);     
            }
         ms_wait(3);       // final wait to be sure frequency inside FPGA is stabilize

         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = tyPllClockConfig.ulResultFreq;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x08)) = tyPllClockConfig.ulPllFreq;
         switch (tyPllClockConfig.ucFpgaDiv)
            {
            case 1:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 10;
               break;
            case 2:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 100;
               break;
            case 3:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 1000;
               break;
            default:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 1;
               break;
            }
         ptyTxData->ulDataSize = 16;
         }
         break;

      default :      // unknown command code
         DLOG(("Unknown ASH510 command"));
         iReturnValue = 0;
         break;
      }
   return iReturnValue;
}



