/******************************************************************************
       Module: ash510layer.c
     Engineer: Nikolay Chokoev
  Description: ASH510 layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    NCH         initial
******************************************************************************/
#include "common/common.h"
#include "common/ml69q6203.h"
#include "common/timer.h"
#include "ash510/ash510layer.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "common/led/led.h"
#include "export/api_err.h"
#include "export/api_cmd.h"

#define EXT_CMD_DELAY               10

// ASH510 JTAG IR instructions
#define ASH510_IR_STATUS_REG           0x8
#define ASH510_IR_TRANSACTION_REG      0x9
#define ASH510_IR_ADDRESS_REG          0xA
#define ASH510_IR_DATA_REG             0xB
#define ASH510_IR_PROCNUM_REG          0xC                     // this is valid for ASH510tanget4
#define ASH510_IR_IDCODE               0xC                     // this is valid for ASH510600 and ASH510700
#define ASH510_IR_BYPASS               0xF

// ASH510 JTAG transactions
#define ASH510_CMD_WRITE_MEM           0x0
#define ASH510_CMD_WRITE_CORE          0x1
#define ASH510_CMD_DUMMY               0x3
#define ASH510_CMD_READ_MEM            0x4
#define ASH510_CMD_READ_CORE           0x5

#define ASH510_DR_LENGTH_CMD           4
#define ASH510_DR_LENGTH_DATA          32
#define ASH510_DR_LENGTH_ADDRESS       32
#define ASH510_DR_LENGTH_STATUS        8                       // status register (only 6 lowest bits)

// local macros (select 2, 4 or 8 buffers)
#define DUAL_JTAG_JASR_BUF(x,y)              (JTAG_JASR_BUF(x) | JTAG_JASR_BUF(y))        // select 2 buffers in FPGA
#define QUAD_JTAG_JASR_BUF(x,y,z,t)          (DUAL_JTAG_JASR_BUF(x,y) | DUAL_JTAG_JASR_BUF(z,t))
#define OCT_JTAG_JASR_BUF(x,y,z,t,a,b,c,d)   (QUAD_JTAG_JASR_BUF(x,y,z,t) | QUAD_JTAG_JASR_BUF(a,b,c,d))

// external variables
extern unsigned char bLedTargetResetAsserted;               // flag to signal that ASH510 reset is asserted
extern PTyTpaInfo ptyTpaInfo;
extern unsigned short usCurrentCore;
extern unsigned char bLedTargetDataWriteFlag;               // flag for LED indicator
extern unsigned char bLedTargetDataReadFlag;                // flag for LED indicator

// global variables 
TyJtagScanConfig tyJtagScanConfig;

/****************************************************************************
     Function: ASH510L_TargetReset
     Engineer: Nikolay Chokoev
        Input: unsigned char bAssertReset - assert/deassert target reset (0x0 for deassert)
               unsigned char bEnableResetSensing - enable/disable target reset sensing
               unsigned char *pbCurrentResetStatus - pointer to value with current reset status
       Output: unsigned long - error code ERR_xxx
  Description: This function controls target reset (nSRST) by asserting/deasserting the line.
               Value in *pbCurrentResetStatus just reflects bAssertReset.
Date           Initials    Description
05-Jul-2007    NCH         Initial
****************************************************************************/
unsigned long ASH510L_TargetReset(unsigned char bAssertReset, 
                                  unsigned char bEnableResetSensing, 
                                  unsigned char *pbCurrentResetStatus)
{
   DLOG2(("ASH510L_TargetReset called"));
   DLOG3(("ASH510L_TargetReset - reset status %d", bAssertReset));
   unsigned long ulValue = get_wvalue(JTAG_TPOUT);

   if (!bLedTargetResetAsserted && bAssertReset)
      bLedTargetResetAsserted = 1;  // indicate target reset status
   if (bAssertReset)
      ulValue &= ~JTAG_TPA_RST;     // clear RST pin to assert reset
   else
      ulValue |= JTAG_TPA_RST;      // set RST pin to deassert reset
   put_wvalue(JTAG_TPOUT, ulValue);
   
   *pbCurrentResetStatus = bAssertReset;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ASH510L_SenseTargetReset
     Engineer: Nikolay Chokoev
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: Check if target reset occured. This function should be called 
               only if target supports this feature.
Date           Initials    Description
09-Nov-2007    NCH         Initial
****************************************************************************/
unsigned long ASH510L_SenseTargetReset(void)
{
   unsigned long ulResult = ERR_NO_ERROR;

   DLOG2(("ASH510L_SenseTargetReset called"));
//todo check reset status
   ulResult = ERR_ASH510_TARGET_RESET_OCCURED;              // reset has occured

   return ulResult;
}

/****************************************************************************
     Function: ASH510L_WriteWords
     Engineer: Nikolay Chokoev
        Input: unsigned long ulCount - 
               unsigned long ulStartAddress - start address/offset
               unsigned long ulOptions - 
               unsigned long *pulWordsWritten - pointer to number of words successfuly written
               unsigned long *pulData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: Write given number of words to ASH510 memory/registers. Access type is 
               given by usAccessType parameter (core, aux registers or memory).
Date           Initials    Description
04-Jul-2007    NCH         Initial
****************************************************************************/
unsigned long ASH510L_WriteWords(unsigned long ulCount, 
                                 unsigned long ulStartAddress, 
                                 unsigned long ulOptions, 
                                 unsigned long *pulWordsWritten, 
                                 unsigned long *pulData)
{
   unsigned long  ulResult = ERR_NO_ERROR;
   unsigned long  ulLocCount;
   unsigned long  ulLocAddr;
   unsigned long  ulData;

   DLOG2(("ASH510L_WriteWords called"));

   if (ulCount > MAX_ASH510_DATA_BLOCK_BYTES)
      return ERR_ASH510_INVALID_MEMORY;
   *pulWordsWritten = 0;
   // and show both data LEDs
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();

   ulLocCount=ulCount;
   ulLocAddr=ulStartAddress;
   if(ulLocAddr < 0x4000) //todo define for Support Register address
      {
      while(ulLocCount--)
         {
         ulData=*pulData++;
         put_hvalue(JTAG_BASE_ADDRESS + ulLocAddr, ulData);
         ulLocAddr++;
         (*pulWordsWritten)++;
         }
      }
   else
      {
      return ERR_ASH510_INVALID_MEMORY;
      }
//todo here
   return ulResult;
}

/****************************************************************************
     Function: ASH510L_ReadWords
     Engineer: Nikolay Chokoev
        Input: unsigned long ulCount - 
               unsigned long ulStartAddress - start address/offset
               unsigned long ulOptions - 
               unsigned long *pulWordsRead - pointer to number of words successfuly read
               unsigned long *pulData - pointer to data to read
       Output: unsigned long - error code ERR_xxx
  Description: Read given number of words to ASH510 memory/registers. Access type is 
               given by usAccessType parameter (core, aux registers or memory).
Date           Initials    Description
04-Jul-2007    NCH         Initial
****************************************************************************/
unsigned long ASH510L_ReadWords(unsigned long ulCount, 
                                unsigned long ulStartAddress, 
                                unsigned long ulOptions, 
                                unsigned long *pulWordsRead, 
                                unsigned long *pulData)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long  ulLocCount;
   unsigned long  ulLocAddr;
   DLOG2(("ASH510L_ReadWords called"));

   if (ulCount > MAX_ASH510_DATA_BLOCK_BYTES)
      return ERR_ASH510_INVALID_MEMORY;
   *pulWordsRead = 0;
   // and show both data LEDs
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();

   ulLocCount=ulCount;
   ulLocAddr=ulStartAddress;
   if(ulLocAddr < 0x4000) //todo define for Support Register address
      {
      while(ulLocCount--)
         {
         *pulData++=get_hvalue(JTAG_BASE_ADDRESS + ulLocAddr);
         ulLocAddr++;
         (*pulWordsRead)++;
         }
      }
   else
      {
      return ERR_ASH510_INVALID_MEMORY;
      }
//todo here
   return ulResult;
}

/****************************************************************************
     Function: Ash510ConfigureClock
     Engineer: Nikolay Chokoev
        Input: unsigned long ulNewJtagFrequency - new JTAG frequency value in Hz
               unsigned char ucFpgaClockDivider - internal FPGA divider ratio
                             0x00 - no dividing of clock source (div by 1)
                             0x01 - divide clock source by 10
                             0x02 - divide clock source by 100
                             0x03 - divide clock source by 1000
               unsigned char ucUsePllClockSource - using external PLL clock 
                                                   source for JTAG engine
                             0x01 - use PLL clock source for JTAG engine
                             0x00 - use fixed internal clock source (60MHz) 
                                    for JTAG engine
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: configure JTAG clock registers inside FPGA 
               (not PLL frequency itself) note this function disables adaptive 
               clock mode and changes it to fixed frequency
Date           Initials    Description
19-May-2008    NCH         Initial
****************************************************************************/
unsigned long Ash510ConfigureClock(unsigned long ulNewJtagFrequency, 
                                   unsigned char ucFpgaClockDivider, 
                                   unsigned char ucUsePllClockSource)
{
   unsigned short usRegValue;

   // assign the new frequency value
   tyJtagScanConfig.ulJtagFrequency = ulNewJtagFrequency;         
   tyJtagScanConfig.bAdaptiveClock = 0;
   tyJtagScanConfig.usAdaptiveClockTimeout = 0;
   usRegValue = 0x0000;
   usRegValue |= (unsigned short)(ucFpgaClockDivider & 0x03);
   if (ucUsePllClockSource)
      usRegValue |= JTAG_JCTR_PS;
   // use 16-bit access to the register
   put_hvalue(JTAG_BASE_ADDRESS + 0x20, usRegValue);
   return ERR_NO_ERROR;
}

