/******************************************************************************
       Module: loader.c
     Engineer: Vitezslav Hola
  Description: Loader main module - part of Opella-XD firmware
  Date           Initials    Description
  09-Aug-2006    VH          initial
******************************************************************************/
#include "common/ml69q6203.h"
#include "common/common.h"
#include "common/flash/flash.h"
#include "common/device/memmap.h"

// defines
#define UPGRADE_BUFFER_SIZE         2048                    // 2048*4 = 8kB buffer

// function prototypes
int main(void);
int UpgradeFirmware(void);
bool VerifyFirmware(unsigned long ulBaseAddress, bool bHasValidUpdateFlag);
bool CompareFirmware(unsigned long ulBaseAddress1, unsigned long ulBaseAddress2);
void ShowStatusOnLED(unsigned char bPowerGreen, unsigned char bPowerRed);

// local variables
unsigned long pulUpgradeBuffer[UPGRADE_BUFFER_SIZE];


/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: none
       Output: return value - 0 when there is a valid firmware (to jump there)
                              1 otherwise
  Description: main program
Date           Initials    Description
09-Aug-2006    VH          Initial
****************************************************************************/
int main(void)
{
   ShowStatusOnLED(1, 0);              // show green LED
   // first for any upgrade
   if (VerifyFirmware(FIRMWARE_UPGRADE_START_ADDRESS, TRUE))
      {
      // there is valid new firmware so check if we have already copied it to right location
      if (!CompareFirmware(FIRMWARE_ADDRESS, FIRMWARE_UPGRADE_START_ADDRESS))
         {
         (void)UpgradeFirmware();
         // compare firmware after upgrade
         if (!CompareFirmware(FIRMWARE_ADDRESS, FIRMWARE_UPGRADE_START_ADDRESS))
            {
            ShowStatusOnLED(0, 1);              // show red LED
            return 2;
            }
         }
      }

   // before jumping to firmware, just verify if it is valid
   if (!VerifyFirmware(FIRMWARE_ADDRESS, TRUE))
      {
      ShowStatusOnLED(0, 1);              // show red LED
      return 1;
      }

   // we have valid firmware, so jump there
   ShowStatusOnLED(1, 0);              // show green LED
   return 0;
}


/****************************************************************************
     Function: UpgradeFirmware
     Engineer: Vitezslav Hola
        Input: none
       Output: int - 0 if upgrade was successful, otherwise != 0
  Description: upgrade a new firmware
Date           Initials    Description
10-Aug-2006    VH          Initial
****************************************************************************/
int UpgradeFirmware(void)
{
   unsigned long ulBytesToUpgrade, ulLen, ulCnt, ulDst;
   unsigned long *pulSrc, *pulBuf;
   unsigned char bSuccess = 1;

   if (FlashInit() != ERR_FLASH_OK)
      return 1;

   // try to get correct number of bytes to upgrade
   ulBytesToUpgrade = get_wvalue(FIRMWARE_UPGRADE_LENGTH_ADDRESS);
   if (ulBytesToUpgrade > FIRMWARE_SIZE)
      ulBytesToUpgrade = FIRMWARE_SIZE;

   // ensure that value is alligned to words
   if (ulBytesToUpgrade & 0x3)
      ulBytesToUpgrade = (ulBytesToUpgrade & 0xFFFFFFFC) + 4;
   pulSrc = (unsigned long *)FIRMWARE_UPGRADE_START_ADDRESS;
   ulDst = FIRMWARE_OFFSET;

   // first clear firmware area
   if (FlashErase(FIRMWARE_OFFSET, FIRMWARE_SIZE) != ERR_FLASH_OK)
      return 2;

   // copy whole area
   while(ulBytesToUpgrade > 0)
      {
      if (ulBytesToUpgrade < (UPGRADE_BUFFER_SIZE * 4))
         ulLen = ulBytesToUpgrade;
      else
         ulLen = UPGRADE_BUFFER_SIZE * 4;

      // copy data to buffer in RAM
      pulBuf = pulUpgradeBuffer;
      for(ulCnt=0; ulCnt < ulLen; ulCnt += 4)
         *pulBuf++ = *pulSrc++;
      // change upgrade flag to invalid (writing first block of data)
      // we assume that length of buffer is big enough to load update flag in first block
      if (ulDst == FIRMWARE_OFFSET)
         pulUpgradeBuffer[FIRMWARE_UPG_FLAG_OFFSET/sizeof(unsigned long)] = 0xFFFFFFFF;

      if (FlashProgram((unsigned char *)pulUpgradeBuffer, ulDst, ulLen) != ERR_FLASH_OK)
         bSuccess = 0;

      ulDst += ulLen;
      ulBytesToUpgrade -= ulLen;
      }

   // finish programming with upgrade flag
   pulUpgradeBuffer[0] = FIRMWARE_VALID_UPGRADE_FLAG;
   if (bSuccess &&
       (FlashProgram((unsigned char *)pulUpgradeBuffer, FIRMWARE_OFFSET + FIRMWARE_UPG_FLAG_OFFSET, 4) != ERR_FLASH_OK))
      {
      bSuccess = 0;
      }

   if (!bSuccess)
      return 2;

   return 0;
}

/****************************************************************************
     Function: VerifyFirmware
     Engineer: Vitezslav Hola
        Input: unsigned long ulBaseAddress - start address of firmware
               bool bHasValidUpdateFlag - there is supposed to be a valid update flag
       Output: bool - TRUE when firmware is valid
  Description: verify if firmware starting at given address is valid
Date           Initials    Description
18-Aug-2006    VH          Initial
****************************************************************************/
bool VerifyFirmware(unsigned long ulBaseAddress, bool bHasValidUpdateFlag)
{
   unsigned long ulLength, ulChecksum;
   unsigned long *pulPtr;

   if (ulBaseAddress & 0x00000003)
      return FALSE;

   // check ID flag
   if (*((unsigned long *)(ulBaseAddress + FIRMWARE_ID_FLAG_OFFSET)) != FIRMWARE_VALID_ID_FLAG)
      return FALSE;
   // check update flag
   if (bHasValidUpdateFlag)
      {
      if (*((unsigned long *)(ulBaseAddress + FIRMWARE_UPG_FLAG_OFFSET)) != FIRMWARE_VALID_UPGRADE_FLAG)
         return FALSE;
      }
   // check length
   ulLength = *((unsigned long *)(ulBaseAddress + FIRMWARE_LENGTH_OFFSET));
   if ((ulLength > FIRMWARE_SIZE) || (ulLength < FIRMWARE_ENTRY_POINT_OFFSET))
      return FALSE;

   // calculate checksum of all words in firmware
   if (ulLength & 0x3)
      ulLength = (ulLength & 0xfffffffc)+4;
   
   // calculate checksum
   ulChecksum = 0;
   pulPtr = (unsigned long *)(ulBaseAddress);
   while (pulPtr < ((unsigned long *)(ulBaseAddress + ulLength)))
      {
      ulChecksum += *pulPtr;
      pulPtr++;
      }
   // sum of all words should be 0
   if (ulChecksum != 0x00000000)
      return FALSE;

   return TRUE;
}


/****************************************************************************
     Function: CompareFirmware
     Engineer: Vitezslav Hola
        Input: unsigned long ulBaseAddress1 - start address of 1st firmware
               unsigned long ulBaseAddress2 - start address of 2nd firmware
       Output: bool - TRUE when both firmwares are same
  Description: compare two firmwares
Date           Initials    Description
18-Aug-2006    VH          Initial
****************************************************************************/
bool CompareFirmware(unsigned long ulBaseAddress1, unsigned long ulBaseAddress2)
{
   unsigned long ulLength, ulCnt;
   unsigned long *pulPtr1, *pulPtr2;

   if ((ulBaseAddress1 & 0x3) || (ulBaseAddress2 & 0x3))
      return FALSE;

   ulLength = *((unsigned long *)(ulBaseAddress1 + FIRMWARE_LENGTH_OFFSET));
   if ((ulLength > FIRMWARE_SIZE) || (ulLength != *((unsigned long *)(ulBaseAddress2 + FIRMWARE_LENGTH_OFFSET))))
      return FALSE;

   pulPtr1 = (unsigned long *)ulBaseAddress1;
   pulPtr2 = (unsigned long *)ulBaseAddress2;

   for (ulCnt=0; ulCnt < ulLength; ulCnt+=4)          // length is in bytes
      {
      if (*pulPtr1 != *pulPtr2)
         return FALSE;
      pulPtr1++;                                     // incrementing pointer to words
      pulPtr2++;
      }
   return TRUE;
}

/****************************************************************************
     Function: ShowStatusOnLED
     Engineer: Vitezslav Hola
        Input: unsigned char bPowerGreen - status for green power LED
               unsigned char bPowerRed - status for red power LED
       Output: none
  Description: set Power LED status (green, red or both)
               use only port F (4mA) restriction
Date           Initials    Description
24-Jan-2006    VH          Initial
****************************************************************************/
void ShowStatusOnLED(unsigned char bPowerGreen, unsigned char bPowerRed)
{
   unsigned short usRegValue;

   // set port F output value for D4 (bits 5 and 6)
   usRegValue = get_hvalue(GPPOF);
   if (bPowerGreen)
      usRegValue &= 0xFFBF;
   else
      usRegValue |= 0x0040;

   if (bPowerRed)
      usRegValue &= 0xFFDF;
   else
      usRegValue |= 0x0020;
   put_hvalue(GPPOF, usRegValue);

   // set port F mode value for D4 (bits 5 and 6)
   usRegValue = get_hvalue(GPPMF);
   usRegValue |= 0x0060;            // bits 5 and 6 as outputs
   usRegValue &= 0xFFE0;            // other LEDs as inputs (not active yet)
   put_hvalue(GPPMF, usRegValue);
}


