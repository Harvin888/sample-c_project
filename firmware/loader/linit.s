@/*********************************************************************
@       Module: linit.s
@     Engineer: Vitezslav Hola
@  Description: Opella-XD loader startup code
@  Date           Initials    Description
@  12-Jul-2006    VH          initial
@*********************************************************************/
.name "linit.s"

@Include files
.include "common/device/memmap.inc"
.include "common/regioninit.inc"

@ Local definition
.equ BIC_BASE,  0x78100000         @ BIC base address

@/*********************************************************************
@ Loader should be located at address 0x00000000
@*********************************************************************/
.section ".text","ax"
.code 32


@ entry point + exception vector table
VectorReset:
         b     StartupEntry        @ reset vector(offset +0x00)
VectorUndef:
         b     VectorUndef         @ undef. inst. (offset +0x04)
VectorSWI:
         b     VectorSWI           @ SWI exception (offset +0x08)
VectorPrefetch:
         b     VectorPrefetch      @ prefetch exception (offset +0x0c)
VectorData:
         b     VectorData          @ data abort (offset +0x10)
         nop                       @ not defined (offset +0x14)
VectorIRQ:
         b     VectorIRQ           @ IRQ exception (offset +0x18)
VectorFIQ:
         b     VectorFIQ           @ FIQ exception (offset +0x18)


@/*********************************************************************
@  Startup routine
@*********************************************************************/

@ jump here after reset
StartupEntry:
         @ reduce power consumption immediately
         @ not implemented at the moment

         @ setup memory controller
         bl    setup_mem_controller
         @ initialize stack in current mode (SVC)
         @ keep all interrupts disabled
         ldr   sp, =Stack_Top
         bl    init_regions
         
         @ now we can jump to C-code
.extern main
         mov   r0, #0x0            @ set argc and argv to 0 (NULL)
         mov   r1, #0x0
         ldr   r2, =main
         mov   lr, pc              @ set return address
         bx    r2                  @ jump to main

         @ we are back from main
         @ if return code is 0, we can jump to firmware, otherwise hang
         cmp   r0, #0x0
         beq   JumpToFirmware         

InfiniteLoop:
         b     InfiniteLoop        @ infinite loop
                  
JumpToFirmware:         
         ldr   r0, =FIRMWARE_OFFSET
         ldr   r1, =FIRMWARE_ENTRY_POINT_OFS
         add   r0, r0, r1  
         mov   pc, r0
         @ ok, this is end of loader


@ setup for memory controller
@ setup could be redefined later in firmware to meet other settings
setup_mem_controller:
         ldr   r0, =BIC_BASE
         mov   r1, #0x28
         str   r1, [r0, #0x0]
         @ setup BWC register (ROM:16bit,RAM:16bit)
         mov    r1, #0x3
         str    r1, [r0, #0x4]     @ setup ROMAC register (slowest)
         @ keep default settings for RAMAC on reset value                
         mov    pc, lr
         @ return from subroutine

@  Initialize regions
.extern __idata_start       @ start address of initialized data in ram
.extern __idata_end         @ end address of initialized data in ram
.extern __idata_rom         @ start address of initialized data in rom
.extern __bss_start         @ start address of region to clear
.extern __bss_end           @ end address of region to clear

@ copying initialized data from rom to ram
init_regions:
         stmfd sp!,{lr}
         COPY_REGION __idata_rom,__idata_start,__idata_end
         CLEAR_REGION __bss_start,__bss_end
         ldmfd sp!,{pc}

@ Stack map for Loader 
@|-----------------------|    start address of .stacks section
@|                       |
@|       Stack           |    
@|       16 kB           |
@|-----------------------| <- Stack_Top

@ placing stack into .stacks section (RW data with no initialization)

.section ".stacks"
.code 32
         .rept 0x1000              @ 0x1000 x 4 bytes = 16kB
         .int  0
         .endr
Stack_Top:

.end

@ End of file
