/******************************************************************************
       Module: armdw.c
     Engineer: Vitezslav Hola
  Description: Opella-XD ARM Diskware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/
#include "common/ml69q6203.h"
#include "common/common.h"
#include "common/comms.h"
#include "common/tpa/tpa.h"
#include "common/irq.h"
#include "common/led/led.h"
#include "common/fpga/jtag.h"
#include "arm/armlayer.h"
#ifdef DEBUG
#include "common/sio/sio.h"
#endif

// external variables
extern unsigned char bLedUsbActivityFlag;

// global variables
unsigned char bTimer100msElapsed = 0;

// function prototypes
int main(int argc, char *argv[]);
void timer_100ms(unsigned char bInit);

/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: none
       Output: return value
  Description: main program
Date           Initials    Description
28-May-2007    VH          Initial
****************************************************************************/
int main(int argc, char *argv[])
{
   unsigned char ucTerminateDiskware;

   // initialize interrupts and enabled them
   init_irq();
   (void)irq_en();
   // initialize LED and TPA management
   InitLed(LED_PWR_READY);
   InitTpa();
#ifdef DEBUG
   // initialize serial interface for debug messages
   (void)sio_init();
   sio_printf("\n\rStarting ARM diskware\n\r");
#endif

   // initialize JTAG engine for ARM targets and data structures
   InitializeJtagScanConfig();
   ARML_InitializeJtagForARM();
//   ARML_InitializeVariables();

   // initialize command processing, if not possible, finish immediately
   if (InitCommandProcessing(NULL, NULL, 0, 0))
      {
      (void)irq_dis();
      return 1;
      }

   // main loop - get&process command, send response, process led and again
   ucTerminateDiskware = 0;
   timer_100ms(TRUE);
   do
      {  // main loop, process any pending commands
      if(ProcessCommand(&ucTerminateDiskware))
         bLedUsbActivityFlag = 1;
      if(ProcessResponse())
         bLedUsbActivityFlag = 1;
      timer_100ms(FALSE);       // check for 100 ms timer
      ProcessTpa();
      ProcessLedIndicators();
      bTimer100msElapsed = 0;
      }
   while(!ucTerminateDiskware);

#ifdef DEBUG
   sio_printf("Exiting ARM diskware\n\r");
#endif

   (void)irq_dis();      // disable interrupts
   return 0;
}

/****************************************************************************
     Function: timer_100ms
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: setting flag with 100 ms period
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void timer_100ms(unsigned char bInit)
{
   if (bInit)
      {
      bTimer100msElapsed = 0;
      // set timer 2 for interval about 100 ms, one shot mode
      put_wvalue(TIMESTAT2, TIMESTAT_STATUS);
      put_wvalue(TIMEBASE2, 0x0);
      put_wvalue(TIMECMP2, 0xB71A);
      put_wvalue(TIMECNTL2, TIMECNTL_CLK16 | TIMECNTL_OS | TIMECNTL_START);
      }
   else if (get_wvalue(TIMESTAT2) & TIMESTAT_STATUS)
      {  // timer elapsed
      bTimer100msElapsed = 1;
      put_wvalue(TIMESTAT2, TIMESTAT_STATUS);
      put_wvalue(TIMEBASE2, 0x0);
      put_wvalue(TIMECMP2, 0xB71A);
      put_wvalue(TIMECNTL2, TIMECNTL_CLK16 | TIMECNTL_OS | TIMECNTL_START);
      }
}

