/******************************************************************************
       Module: armlayer.h
     Engineer: Vitezslav Hola
  Description: Header for ARM layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
  01-Aug-2007    SPT         Included ARM Diskware Commands
******************************************************************************/

#ifndef _ARMLAYER_H_
#define _ARMLAYER_H_

typedef enum
{
   ARM_LitleEndian,
   ARM_bigEndian
}ARMEndian;

/*ARM11MODSTATRT*/
typedef enum
{
   ARM11ModeUser,
   ARM11ModeFIQ,
   ARM11ModeIRQ,
   ARM11ModeSupervisor,
   ARM11ModeAbort,
   ARM11ModeUndefined
}ARM11Mode;
/*ARM11MODEND*/

typedef enum
{
   ARM9ModeUser,
   ARM9ModeFIQ,
   ARM9ModeIRQ,
   ARM9ModeSupervisor,
   ARM9ModeAbort,
   ARM9ModeUndefined
}ARM9Mode;

typedef enum
{
   ARM7ModeUser,
   ARM7ModeFIQ,
   ARM7ModeIRQ,
   ARM7ModeSupervisor,
   ARM7ModeAbort,
   ARM7ModeUndefined
}ARM7Mode;

//ARM specific constants // added by Shameerudheen P.T.
#define ARM7                           0x1
#define ARM7S                          0x2
#define ARM9                           0x3
#define ARM11                          0x4
#define CORTEXM                        0x5
#define ARM920                         0x06
#define ARM922                         0x07
#define ARM926                         0x08
#define ARM940                         0x09
#define ARM946                         0x0A
#define ARM966                         0x0B
#define ARM968                         0x0C
#define ARM1136                        0x0D
#define ARM1156                        0x0E
#define ARM1176                        0x0F
#define ARM11MP                        0x10
#define CORTEXA                        0x11

//device specific constants
#define ISCORE      0x00
//Marvell Cores
#define FEROCEON    0x01
#define DRAGONITE   0x02

#define ARM7_IR_LENGTH                    0x4
#define ARM9_IR_LENGTH                    0x4
#define ARM11_IR_LENGTH                   0x5
#define DAP_IR_LENGTH                     0x4
#define ICEPICK_IR_LENGTH                 0x6
#define ICEPICK_DR_LENGTH                 0x8

#define ARM7_SCAN_CHAIN_1_LENGTH          0x21 //33 Debug Scan Cahin
#define ARM7_SCAN_CHAIN_2_LENGTH          0x26 //38
#define ARM7_SCAN_CHAIN_SELECT_REG_LENGTH 0x4  //Scan chain select register length
#define ARM7_SCAN_CHAIN_2_INST_LENGTH     6
                                               
#define ARM7_SCAN_CHAIN_ICE               0x2
#define ARM7_SCAN_CHAIN_DEBUG             0x1
#define ARM7_SCAN_CHAIN_PERIPHERY         0x0   
#define ARM7_SCAN_CHAIN_CP15              0xF

#define ARM9_SCAN_CHAIN_1_LENGTH          0x43 //67
#define ARM9_SCAN_CHAIN_2_LENGTH          0x26 //38
#define ARM9_SCAN_CHAIN_1_DATA_LENGTH     32
#define ARM9_SCAN_CHAIN_1_INST_LENGTH     35
#define ARM9_SCAN_CHAIN_2_INST_LENGTH     6
                                               

#define ARM920_922_SCAN_CHAIN_15_LENGTH   0x28 //40
#define ARM926_SCAN_CHAIN_15_LENGTH       0x30 //48
#define ARM940_946_SCAN_CHAIN_15_LENGTH   0x27 //39  
#define ARM966_SCAN_CHAIN_15_LENGTH       0x27 //39 
#define ARM968_SCAN_CHAIN_15_LENGTH       0x27 //39 
                                               
                                                
#define ARM9_SCAN_CHAIN_SELECT_REG_LENGTH 0x5
#define ARM9_SCAN_CHAIN_CP15              0xF
#define ARM9_SCAN_CHAIN_ICE               0x2
#define ARM9_SCAN_CHAIN_DEBUG             0x1
#define ARM9_SCAN_CHAIN_PERIPHERY         0x3

#define CORTEX_DR_LENGTH                  35

#define MAX_WAIT_CYCLES                   0x500   

#define ARM_DBGACK_BIT                    0x01
#define ARM_DEBUG_MODE                    0x09
#define THUMB_MODE_BIT                    0x10

/*ARM11MODSTART*/

#define ARM11_ITR_READY_BIT                0x1
#define ARM11_DTR_READY_BIT                0x1
#define ARM11_wDTR_VALID_BIT               0x2
#define ARM11_rDTR_RETRY_BIT               0x2

#define ARM_USER_MODE                      0x10
#define ARM_FIQ_MODE                       0x11
#define ARM_IRQ_MODE                       0x12
#define ARM_SPVR_MODE                      0x13
#define ARM_ABORT_MODE                     0x17
#define ARM_UNDEF_MODE                     0x1B

#define ARM11_SCAN_CHAIN_SELECT_REG_LENGTH  0x05  
                                                 
#define ARM11_SCAN_CHAIN_DSCR               0x01 
#define ARM11_SCAN_CHAIN_DSCR_LENGTH        0x20 
                                                 
#define ARM11_SCAN_CHAIN_DTR                0x05
#define ARM11_SCAN_CHAIN_DTR_LENGTH         0x22                                               
#define ARM11_SCAN_CHAIN_ITR_LENGTH         0x21 
 
#define ARM11_SCAN_CHAIN_7                  0x07                                                 
#define ARM11_SCAN_CHAIN_7_LENGTH           0x28 
#define ARM11_SCAN_CHAIN_7_READY            0x01

/* DSCR Related Bits */
#define ARM11_DSCR_HALT_DEBUG_MODE_BIT          0x00004000
#define ARM11_DSCR_INST_EXEC_ENABL_BIT          0x00002000

#define ARM11_DSCR_CORE_HALTED_RESTARTED_MASK   0x00000003
#define ARM11_DSCR_CORE_HALTED                  0x1
#define ARM11_DSCR_CORE_RESTARTED               0x2
#define ARM11_rDTR_FULL                         0x40000000
#define ARM11_wDTR_FULL                         0x20000000

#define ARM11_DSCR_ARM_INSTR_DISABLE_BIT   0xFFFFDFFF
#define ARM11_DSCR_STICKY_PRECISE_BIT      0x40
#define ARM11_DSCR_STICKY_IMPRECISE_BIT    0x80
#define ARM11_DSCR_HALT_BIT                0x01


#define ARM11_DSCR_ENTRY_FIELD_MASK        0x0000003C

#define ARM11_HALT_INSTRUCTION      0x0
#define ARM11_BREAK_POINT           0x1
#define ARM11_WATCH_POINT           0x2
#define ARM11_BKPT_INSTRUCTION      0x3
#define ARM11_EDBGRQ                0x4
#define ARM11_VECTOR_CATCH          0x5
#define ARM11_DATA_ABORT            0x6
#define ARM11_INSTRUCTION_ABORT     0x7


// CPSR Related Bits
#define ARM11_CPSR_T_BIT        0x00000020
#define ARM11_CPSR_J_BIT        0x01000000

// For Enter debug state Response packet
#define ARM11_wDTR_VALID        0xFFFFFFFF
#define ARM11_rDTR_VALID        0xFFFFFFFF

#define ARM11_BVR_BASE      0x40
#define ARM11_BVR_1         0x41
#define ARM11_BVR_2         0x42
#define ARM11_BVR_3         0x43
#define ARM11_BVR_4         0x44
#define ARM11_BVR_5         0x45


#define ARM11_BCR_BASE      0x50
#define ARM11_BCR_1         0x51
#define ARM11_BCR_2         0x52
#define ARM11_BCR_3         0x53
#define ARM11_BCR_4         0x54
#define ARM11_BCR_5         0x55





#define ARM11_CONTROL_REG_M_BIT      0x1
#define ARM11_CONTROL_REG_A_BIT      0x2
#define ARM11_CONTROL_REG_DC_BIT     0x4
#define ARM11_CONTROL_REG_W_BIT      0x8

#define ARM11_CONTROL_REG_B_BIT      0x80

#define ARM11_CONTROL_REG_S_BIT       0x100
#define ARM11_CONTROL_REG_R_BIT       0x200
#define ARM11_CONTROL_REG_F_BIT       0x400
#define ARM11_CONTROL_REG_Z_BIT       0x800

#define ARM11_CONTROL_REG_IC_BIT      0x1000
#define ARM11_CONTROL_REG_V_BIT       0x2000
#define ARM11_CONTROL_REG_RR_BIT      0x4000
#define ARM11_CONTROL_REG_L4_BIT      0x8000

#define ARM11_CONTROL_REG_DT_BIT      0x10000
#define ARM11_CONTROL_REG_IT_BIT      0x40000

#define ARM11_CONTROL_REG_FI_BIT      0x200000
#define ARM11_CONTROL_REG_U_BIT       0x400000
#define ARM11_CONTROL_REG_XP_BIT      0x800000

#define ARM11_CONTROL_REG_VE_BIT      0x1000000
#define ARM11_CONTROL_REG_EE_BIT      0x2000000

#define ARM11_CONTROL_REG_TRE_BIT     0x10000000
#define ARM11_CONTROL_REG_AFE_BIT     0x20000000


#define ARM11_CACHE_DEBUG_CONTROL_DL     0x00000001
#define ARM11_CACHE_DEBUG_CONTROL_IL     0x00000002
#define ARM11_CACHE_DEBUG_CONTROL_WT     0x00000004


#define ICEPICK_C_IR_LENGTH            0x6

//IcePick_C commands
#define ICEPICK_C_BYPASS               0x00
#define ICEPICK_C_ROUTER               0x02
#define ICEPICK_C_IDCODE               0x04
#define ICEPICK_C_CONNECT              0x07

//ICEPick_C register size
#define ICEPICK_C_IDCODE_REG_LENGTH    32
#define ICEPICK_C_DCON_REG_LENGTH      8
#define ICEPICK_C_SEC_TAP_REG_LENGTH   32

//IcePick_C connect pattern
#define ICEPICK_C_CONNECT_PATTERN      0x89

//DM6446 specific
#define ARM_TAP_SELECT_PATTERN         0xA0280127
#define ARM_TAP_ADDRESS                0x20000000

/*ARM11MODEND*/

// function prototype (API)
void ARML_InitializeJtagForARM(void);
//void PerformARMNops(unsigned char ucCount);
unsigned long ARML_ARM9ReadRegisters(unsigned long *pulResultData);
unsigned long  ARM9_ChangeMode(ARM9Mode tyARM9Mode);
unsigned long ARML_ARM9ReadWord(unsigned long ulAddress, unsigned long *pulResultData);
unsigned long ARML_ARM9CheckDataAbort (unsigned char *pucReadData);
unsigned long ARML_ARM9SetSNVectorAddress (unsigned long *pulAddress);
unsigned long ARML_ARM9ReadBlock(unsigned long *pulAddress, unsigned long *pulReadData);
unsigned long ARML_ARM9WriteWord(unsigned long *pulAddress,
                     unsigned long *pulWriteData);
unsigned long ARML_ARM9WriteBlock(unsigned long *pulAddress,
                      unsigned long *pulWriteData);
unsigned long ARML_ARM9WriteRegisters (unsigned long *pulWriteData);
unsigned long ARML_ARM9SetupExecuteProc (unsigned long *pulData);
unsigned long ARML_ARM9ExecuteProc (unsigned char *bThumbMode,
                       unsigned long *pulData);
unsigned long ARML_ARM9StatusProc (unsigned char *pucSelectScanChain2,
                       unsigned char *pucStartedExecution,
                       unsigned char *pucCoreExecuting,
                       unsigned char *pucThumbMode,
                       unsigned char *pucWatchPoint);
unsigned long ARML_ARM920_922InvalidateICache(void);
unsigned long ARML_ARM9RunCacheClean (unsigned long *pulData);
unsigned long ARML_Read920_922CPReg (unsigned long *pulReadData);
unsigned long ARML_Write920_922CPReg (unsigned char *pucRestoreIModeRegisters,
                               unsigned long *pulWriteData);
unsigned long ARML_ARM920_922DisableCache (unsigned long *ulControlValueData);
unsigned long ARML_Read926CPReg (unsigned long *pulReadData);
unsigned long ARML_DebugRequest(unsigned char bDbgrq);
unsigned long ARML_GetDebugAcknowledge(unsigned char *pbDbgack);
unsigned long ARML_InitTarget(unsigned long ulTarget);

unsigned long ARML_SelectScanChain(unsigned char ucProcessor, 
                                   unsigned char ucScanChainNumber,
                                   unsigned char ucNoOfIntest);

unsigned long ARML_ResetProc(unsigned char bAssertReset, unsigned char *pbResetAsserted);
unsigned long ARML_SetSafeNonVectorAddress(unsigned char ucProcessor, 
                                   unsigned long *pulAddress, unsigned char ucUseSafeNonVectorAddress);
unsigned long ARML_ReadICEBreaker(unsigned long *pulAddress, 
                                   unsigned long *pulReadData);
unsigned long ARML_WriteICEBreaker(unsigned long *pulAddress, 
                                   unsigned long *pulData);

unsigned long ARML_ARMStatusProc(unsigned char ucProcessor, 
                                 unsigned char *pucSelectScanChain2,
                                 unsigned char *pucStartedExecution,
                                 unsigned char *pucARM7StopProblem,
                                 unsigned char *pucCoreExecuting,
                                 unsigned char *pucThumbMode,
                                 unsigned char *pucWatchPoint);
unsigned long ARML_ARMReadRegisters(unsigned char ucProcessor, unsigned long *pulResultData);
unsigned long ARML_ARMWriteRegisters (unsigned char ucProcessor, unsigned long *pulWriteData);
unsigned long ARML_ARMPerfromARMNops (unsigned char ucProcessor, unsigned long ulNumberOfnops);
unsigned long ARML_ARMChangeModeToARMMode (unsigned char ucProcessor, unsigned char ucWatchPoint,
                                            unsigned char ucUserHalt, unsigned char * pucOffsetNeeded);
unsigned long ARML_Write940_946CPSingleReg (unsigned char ucRegAddress, unsigned long ulRegValue);
unsigned long ARML_JtagRestart(void);
unsigned long ARML_BringToRTIFromTLR(void);
unsigned long ARML_ARMWriteWord (unsigned char ucProcessor, unsigned long *pulAddress, unsigned long *pulWriteData);
unsigned long ARML_ARMReadWord (unsigned char ucProcessor, unsigned long ulAddress, unsigned long *pulReadData);
unsigned long ARML_ARMCheckDataAbort (unsigned char ucProcessor, unsigned char *pucReadData);
unsigned long ARML_ARM9WriteByte(unsigned char ucWriteData, unsigned long ulAddress);
unsigned long ARML_ARMWriteByte (unsigned char ucProcessor, unsigned long ulAddress, unsigned char ucData);
unsigned long ARML_Write926CPReg (unsigned long *pulWriteData);
unsigned long ARML_Read940_946CPReg (unsigned long *pulReadData);
unsigned long ARML_Write940_946CPReg (unsigned long *pulWriteData);
unsigned long ARML_Read966CPReg (unsigned long *pulReadData);
unsigned long ARML_Write966CPReg (unsigned long *pulWriteData);
unsigned long ARML_ARM7ReadWord(unsigned long ulAddress, unsigned long *pulReadData);
unsigned long ARML_ARM7ReadBlock(unsigned long ulAddress, unsigned long *pulReadData);
unsigned long ARML_ARM7WriteWord(unsigned long ulAddress, unsigned long ulWriteData);
unsigned long ARML_ARM7WriteBlock(unsigned long ulAddress, unsigned long *pulWriteData);
unsigned long ARML_ARM7ReadRegisters (unsigned long *pulReadData);
unsigned long ARML_ARM7WriteRegisters (unsigned long *pulWriteData);
unsigned long ARML_ARMReadBlock (unsigned char ucProcessor, unsigned long ulAddress, unsigned long *pulReadData);
unsigned long ARML_ARMWriteBlock (unsigned char ucProcessor, unsigned long *pulAddress, unsigned long *pulWriteData);
unsigned long ARML_ARMExecuteProc (unsigned char ucProcessor, unsigned char *bThumbMode, unsigned long *pulData);
unsigned long ARML_ARMSetupExecuteProc (unsigned char ucProcessor, unsigned long *pulData);
unsigned long ARML_ARMChangeToThumbMode (unsigned char ucProcessor, unsigned long ulReg0,
                                         unsigned long ulRegPC);
unsigned long ARML_WriteARM920_922CPReg (unsigned char *pucRestoreIModeRegisters,
                               unsigned long *pulWriteData);
unsigned long ARML_WriteARM926CPReg (unsigned long *pulWriteData);
unsigned long ARML_WriteARM940_946CPReg (unsigned long *pulWriteData);
unsigned long ARML_WriteARM966CPReg (unsigned long *pulWriteData);
unsigned long ARML_ARM7StatusProc (unsigned char *pucSelectScanChain2,
                       unsigned char *pucStartedExecution,
                       unsigned char *pucStopProblem,
                       unsigned char *pucCoreExecuting,
                       unsigned char *pucThumbMode,
                       unsigned char *pucWatchPoint);
unsigned long ARML_ARM7SStatusProc (unsigned char *pucSelectScanChain2,
                       unsigned char *pucStartedExecution,
                       unsigned char *pucStopProblem,
                       unsigned char *pucCoreExecuting,
                       unsigned char *pucThumbMode,
                       unsigned char *pucWatchPoint);
unsigned long ARML_Write920_922CPSingleReg (unsigned char ucRegAddress, unsigned long ulRegValue);
unsigned long ARML_Write926CPSingleReg (unsigned char ucRegAddress, unsigned long ulRegValue);
unsigned long ARML_Read920_922CPSingleReg (unsigned char ucRegAddress, unsigned long *pulRegReadValue);
unsigned long ARML_ReadARM920_922CPReg (unsigned long *pulReadData);
unsigned long ARML_ARM7SetupExecuteProc (unsigned long *pulData);
unsigned long ARML_ARM7ExecuteProc (unsigned char *bThumbMode, unsigned long *pulData);
unsigned long ARML_ARM7WriteByte(unsigned long ulAddress, unsigned char ucWriteData);
unsigned long ARML_ARM7SetSNVectorAddress (unsigned long *pulAddress);
unsigned long ARML_ARM7CheckDataAbort (unsigned char *pucReadData);
unsigned long ARML_ARM720ReadCPReg(unsigned long *pulReadData);
unsigned long ARML_ARM720WriteCPReg(unsigned long *pulWriteData);
unsigned long ARML_ARM740ReadCPReg(unsigned long *pulReadData);
unsigned long ARML_ARM740WriteCPReg(unsigned long *pulWriteData);
unsigned long ARML_ARMReadWriteDCC (unsigned char ucProcessor, 
                                     unsigned char *pucDataToWrite,
                                     unsigned long *pulWriteData,
                                     unsigned char *pucDataWriteSuccessful,
                                     unsigned char *pucDataReadFromCore,
                                     unsigned long *pulReadData);
unsigned long ARML_ARM7ChangeModeToARMMode(unsigned char ucUserHalt, unsigned char * pucOffsetNeeded);
unsigned long ARML_ARM7SChangeModeToARMMode(unsigned char * pucOffsetNeeded);
unsigned long ARML_ARMGetMultiCoreInfo(unsigned long * pulReturnData);
unsigned long ARML_ARMWriteMultipleBlocks (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulBlockCount, unsigned long *pulWriteData);
unsigned long ARML_ARMReadMultipleBlocks (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulBlockCount, unsigned long *pulReadData);
unsigned long ARML_ARMReadMultipleWords (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulWordCount, unsigned long *pulReadData);
unsigned long ARML_ARMWriteMultipleWords (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulWordCount, unsigned long *pulWriteData);
unsigned long ARML_Write968CPReg (unsigned long *pulWriteData);
unsigned long ARML_Read968CPReg (unsigned long *pulReadData);
unsigned long ARML_ARM9WriteMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long ulBlockCount);
unsigned long ARML_ARM9WriteMultipleWord_ReallyFast(unsigned long *pulAddress, unsigned long *pulWriteData, unsigned long ulWordcount);
unsigned long ARML_ARM9ReadMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long *pulReadData, unsigned long ulBlockcount);
unsigned long ARML_ARM9ReadMultipleWord_ReallyFast(unsigned long ulAddress, unsigned long *pulResultData,unsigned long ulWordcount);
unsigned long ARML_ARM7WriteMultipleWord_ReallyFast(unsigned long ulAddress, unsigned long *pulWriteData, unsigned long ulWordCount);
unsigned long ARML_ARM7WriteMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long ulBlockCount);
unsigned long ARML_ARM7ReadMultipleWord_ReallyFast(unsigned long ulAddress, unsigned long *pulReadData,unsigned long ulWordcount);
unsigned long ARML_ARM7ReadMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long *pulReadData,unsigned long ulBlockcount);

/*ARM11MODSTATRT*/
unsigned long ARML_ARM11EnterDebugState(unsigned char ucARM11Type,unsigned long *pulData);
unsigned long ARML_ARM11_LeaveDebugState(unsigned char *bThumbMode,unsigned char ucARM11Type,unsigned long *pulData);
unsigned long ARML_ARM11ReadDSCR(unsigned long*pulData);
unsigned long ARML_ARM11ReadSPSR(unsigned long* pulData);
unsigned long ARML_ARM11ReadCPSR(unsigned long* pulData);
unsigned long ARML_ARM11ReadRegisters(unsigned long *pulData);
unsigned long ARML_ARM11WriteRegisters(unsigned long *pulData);
unsigned long ARML_ARM11ExeInstEnable(unsigned long* pulData);
unsigned long ARML_ARM11_Read_R0(unsigned long* pulData);
unsigned long ARML_ARM11_Read_R1(unsigned long* pulData);
unsigned long ARML_ARM11_Write_R0(unsigned long ulDatatoR0);
unsigned long ARML_ARM11StatusProc(unsigned char* pucCoreExecuting ,unsigned char* pucThumbMode,unsigned char* pucWatchPoint);
unsigned long ARML_ARM11ReadCPReg(unsigned long *pulData);
unsigned long ARML_ARM11WriteCPReg(unsigned long *pulData);
unsigned long ARML_ARM11WriteMemoryWord(unsigned long ulAddress,unsigned long*pulData);
unsigned long ARML_ARM11ReadMemoryWord(unsigned long ulAddress, unsigned long*pulData);
unsigned long ARML_ARM11_ReadSC7 (unsigned long ulAddr,unsigned long *pulData);
unsigned long ARML_ARM11_WriteSC7 (unsigned long ulAddr,unsigned long *pulData);
unsigned long ARML_ARM11WriteMultipleWord(unsigned long ulAddress,unsigned long ulWordCount,unsigned long*pulData);
unsigned long ARML_ARM11ReadMultipleWord(unsigned long ulAddress,unsigned long ulWordCount,unsigned long*pulData);
unsigned long ARML_ARM11ReadMultipleBlock(unsigned long ulAddress,unsigned long ulBlockCount,unsigned long*pulData);
unsigned long ARML_ARM11WriteMultipleBlock(unsigned long ulAddress,unsigned long ulBlockCount,unsigned long*pulData);
unsigned long ARML_ARM11ReadMemoryHalfword(unsigned long ulAddress,unsigned long*pulData);
unsigned long ARML_ARM11Read_MemoryByte(unsigned long ulAddress,unsigned long*pulData);
unsigned long ARML_ARM11CheckDataAbort(unsigned char *pucReadData);
unsigned long ARML_ARM11SetupExecuteProc(unsigned long *pulData);
unsigned long ARML_ARM11ReadWriteDCC (unsigned char *pucDataToWrite,unsigned long *pulWriteData,unsigned char *pucDataWriteSuccessful,
                                      unsigned char *pucDataReadFromCore,unsigned long *pulReadData);
unsigned long ARML_ARM11Write_MemoryHalfWord(unsigned long ulAddress,unsigned short ucWriteData);
unsigned long ARML_ARM11InvalidateCleanCache(void);
unsigned long ARML_ARM11ChangeToThumbMode (unsigned long ulReg0, unsigned long ulRegPC);
unsigned long ARML_ARM11ReadCDCR(unsigned char ucARM11Type,unsigned long* pulData);
unsigned long ARML_ARM11WriteMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long ulBlockCount,unsigned long* ulData);
unsigned long ARML_ARM11ReadMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long *pulReadData, unsigned long ulBlockcount);
unsigned long ARML_ARMReadSelectedCPReg(unsigned long ulProcessor,
                                        unsigned long *pulARMFamily,
                                        unsigned long *pulOpcode_Address,
                                        unsigned long *pulReadData);
unsigned long ARML_ARMWriteSelectedCPReg(unsigned long ulProcessor,
                                         unsigned long *pulARMFamily,
                                         unsigned long *pulOpcode_Address,
                                         unsigned long *pulWriteData);
unsigned long ARML_InitilizeICePick( unsigned long ulSubPortNum );
/*ARM11MODEND*/
#endif // #define _ARMLAYER_H
