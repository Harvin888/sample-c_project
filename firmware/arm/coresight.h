/******************************************************************************
       Module: Dap.h
     Engineer: Jeenus C.K.
  Description: Header for Coresight implementations
  Date           Initials    Description
  28-May-2009    JCK          Initial  
  09-Aug-2010    SJ           Added some useful comments. Corrected DP/AP register 
                              addresses.
******************************************************************************/
#ifndef CORESIGHT_H
#define CORESIGHT_H

typedef volatile unsigned long CM_REG;
typedef volatile unsigned long DAP_REG;
typedef const volatile unsigned long READ_ONLY_REG;
typedef unsigned long RESULT;  
typedef unsigned long BOOL;

/***********************************************************************
                    DPACC Register  Defintions
***********************************************************************/
/* Addres defenitions for DPACC registers. Here the defenitions are as per ADIv5 specification ARM IHI 0031A.
   In the function PrepareDpaccApaccData() the address is shifted and used to get ADDR[3:2](bits 0 and 1 not used).
   See below the format of DPACC regiter
       34                                3       2        1     0                                  
    ----------------------------------------------------------
    |           DATAIN[34:3]          |     ADDR[3:2]   |RnW  |   
    ----------------------------------------------------------

    */
#define JTAG_DP_ABORT_REG           0x00  /* DAP Abort Register */
#define JTAG_DP_CTRL_STAT_REG		0x04  /* Control Status Register */
#define JTAG_DP_AP_SELECT_REG		0x08  /* AP Select register */
#define JTAG_DP_RDBUFF			    0x0C  /* Read Buffer Register */

/***********************************************************************
                    APACC Register  Defintions
***********************************************************************/
/* Addres defenitions for APACC registers. Here the defenitions are as per ADIv5 specification ARM IHI 0031A.
   In the function PrepareDpaccApaccData() the address is shifted and used to get ADDR[3:2](bits 0 and 1 not used).
   See below the format of APACC regiter
       34                                3       2        1     0                                  
    ----------------------------------------------------------
    |           DATAIN[34:3]          |     ADDR[3:2]   |RnW  |   
    ----------------------------------------------------------
*/
/* Memory access port register definitions*/
/* BANK 0 registers */ 
#define MEM_AP_CSW_REG			0x00
#define MEM_AP_TAR_REG			0x04
#define MEM_AP_BNK0_RES_REG		0x08
#define MEM_AP_DRW_REG			0x0C

/* BANK 1 Registers */
#define MEM_AP_BD0_REG			0x10
#define MEM_AP_BD1_REG			0x14
#define MEM_AP_BD2_REG			0x18
#define MEM_AP_BD3_REG			0x1C

/* Bank 15 Registers */
#define MEM_AP_BNK15_RES_REG	0xF0
#define MEM_AP_CFG_REG			0xF4
#define MEM_AP_DBG_BASE_REG		0xF8
#define MEM_AP_IDR_REG			0xFC

/* DPACC registers. To save the value */
typedef struct 
{
    DAP_REG Reserved;
    DAP_REG CTRL_STAT;
    DAP_REG APSELECT;
    DAP_REG RDBUFF;
}DAP_DPACC;


/* CTRL_STAT: Control/Status Register */ 
#define CSYS_PWRUP_ACK              (0x1 << 31)
#define CSYS_PWRUP_REQ              (0x1 << 30)
#define CDBG_PWRUP_ACK              (0x1 << 29)
#define CDBG_PWRUP_REQ              (0x1 << 28)
#define CDBG_RSTACK                 (0x1 << 27)
#define CDBG_RSTREQ                 (0x1 << 26)
#define TRNCNT                      (0x7FF << 12)
#define MASKLANE                    (0xF << 8)
#define WDATAERR                    (0x1 << 7)
#define READOK                      (0x1 << 6)
#define STICKYERR                   (0x1 << 5)
#define STICKYCMP                   (0x1 << 4)
#define TRNMODE                     (0x3 << 2)
#define STICKYORUN                  (0x1 << 1)
#define ORUNDETECT                  (0x1 << 0)

/* SELECT: AP Select Register,*/
#define APSEL                       (0x7F << 24) 
#define     AHB_AP                  (0 << 24)
#define     APB_AP                  (1 << 24)
#define     JTAG_AP                 (2 << 24
#define     CORTEX_AP               (3 << 24) 
#define APSEL_SHIFT(x)              (x << 24)      
#define APBANKSEL                   (0xF  << 4)
#define     AP_BANK_0               (0x0 << 4) 
#define     AP_BANK_1               (0x1 << 4)
#define     AP_BANK_15              (0xF << 4)

#define ACCESS_DRW                 0x1
#define ACCESS_BD0                 0x2
#define ACCESS_BD1                 0x4
#define ACCESS_BD2                 0x8
#define ACCESS_BD3                 0x10
/* APACC register : Bank0 */
typedef struct
{
    DAP_REG CSW;
    DAP_REG TAR;
    DAP_REG Reserved;
    DAP_REG DRW;
}AP_BANK0_REG;

/* APACC register: Bank1 */
typedef struct
{
    DAP_REG BD0;
    DAP_REG BD1;
    DAP_REG BD2;
    DAP_REG BD3;
}AP_BANK1_REG;

/* APACC register: Bank 15 */
typedef struct
{
    DAP_REG Reserved;
    DAP_REG CFG;
    DAP_REG DBG_BASE;
    DAP_REG IDR;
}AP_BANK15_REG;


/* CSW: CSW Register */
#define DBG_SW_ENABLE               (0x80000000) /* (0x1 << 31)*/ /* Debug software access enable.*/
#define CSW_PROT                    (0x7F << 24)    /* Bus access protection control */
#define SPIDEN                      (0x1 << 23)     /* Secure Privileged Debug Enabled */
#define MODE                        (0xF << 8)      /* Mode of operation */
#define TRIN_PROG                   (0x1 << 7)      /* Transfer in progress status */
#define DEVICE_EN                   (0x1 << 6)      /* Device enabled status */
#define ADDR_INC                    (0x3 << 4)      /* Address auto-increment and packing mode */
#define     AUTO_INC_OFF                    (0x0 << 4)     /* Auto-increment off */
#define     INC_SINGLE                      (0x1 << 4)     /* Increment single */
#define     INC_PACKED                      (0x2 << 4)    /* Increment packed */
#define ACCESS_SIZE                 (0x7 << 0)      /* Size of the access to perform */
#define     ACCESS_SIZE_8               (0x0 << 0)  /* Byte access */
#define     ACCESS_SIZE_16              (0x1 << 0)  /* Half-Word access */
#define     ACCESS_SIZE_32              (0x2 << 0)  /* Word access */

/* IDR: Identification register */
#define AP_TYPE                     (0xF << 0)
#define AP_VARIENT                  (0xF << 4)
#define CLASS                       (0x1 << 16)
#define     MEM_AP_TYPE                 (0x1 << 16)
#define     NOT_MEM_AP_TYPE             (0x0 << 16)
#define IDENTITY_CODE               (0x7F << 17)
#define CONT_CODE                   (0xF << 4)

/* Debug base address */
#define DBG_ENTRY_NOT_PRESENT       0xFFFFFFFF
#define END_OF_ROM_TABLE            0x00000000

typedef enum
{
    AP_TYPE_START               = (0x0 << 0),
    JTAG_AP_TYPE                = (0x0 << 0),
    AHB_AP_TYPE                 = (0x1 << 0),
    APB_AP_TYPE                 = (0x2 << 0),
    CORTEXM_AP_TYPE            = (0x3 << 0),
    AP_TYPE_END                 = (0x3 << 0)
}AccessType;

/* BASE: Debug base address register and ROM table format */
#define ENTRY_PRESENT               (0x1 << 0)
#define ROM_TABLE_FORMAT            (0x1 << 1)
#define FORMAT                      (0x1 << 1)
#define FORMAT_8_BIT                    (0x0 << 1)
#define FORMAT_32_BIT                   (0x1 << 1)
#define BASEADDR_OFFSET             (0xFFFFF000) 

/* CID0 to CID3: Component ID Register */
#define CID0_PREAMBLE               (0xFF << 0)
#define CID1_PREAMBLE               (0xF  << 0)
#define CID1_COMPO_CLASS            (0xF  << 4)
#define     GENERIC_COMPO               (0x0 << 4)
#define     ROM_TABLE_COMPO             (0x1 << 4)
#define     DBG_COMPO                   (0x9 << 4)
#define     PERI_TST_BLOCK_COMPO        (0xB << 4)
#define     OPT_DATA_COMPO              (0xD << 4)
#define     GENERIC_IP_COMPO            (0xE << 4)
#define     PRIME_CELL_COMPO            (0xF << 4)
#define CID2_PREAMBLE               (0xFF << 0)
#define CID3_PREAMBLE               (0xFF << 0)

/* Default values of Component ID Preamble */
#define DEF_CID0_PREAMBLE           0xD
#define DEF_CID1_PREAMBLE           0x0
#define DEF_CID2_PREAMBLE           0x5
#define DEF_CID3_PREAMBLE           0xB1

/* PID0 to PID7 Peripheral ID Register */
#define PID0_PART_NUM               (0xFF << 0)
#define PID1_PART_NUM               (0xF  << 0)
#define PID1_IDEN_CODE              (0xF  << 4)
#define PID2_IDEN_CODE              (0x7  << 0)
#define PID2_JEP_USED               (0x1  << 3)
#define PID2_REVISION               (0xF  << 4)
#define PID3_CUST_MODIF             (0xF  << 0)
#define PID3_REV                    (0xF  << 4)
#define PID4_JEP_CODE               (0xF  << 0)
#define PID4_4KB_CNT                (0xF  << 4)

/* ROM table specifications */
#define NUM_COMPO_ID_REG            0x4
#define NUM_PERI_ID_REG             0x8
#define COMPO_ID_OFFSET             (0xFF0)
#define PERI_ID_OFFSET              (0xFD0)
#define DEV_TYPE_ID_OFFSET          (0xFCC)


/* Flag to return the MEM-AP status */
#define VALID_MEM_AP                1
#define NOT_VALID_MEM_AP            0

/* Mask In case of 8 bit ROM table entry format */
#define MASK_8_BIT                  0x000000FF

/* Precount and Postcount 8 Prepatten and postpattern 0xFF (8 ones) */
#define TMS_PATTERN_16_ONES         0x80FF80FF  
#define TMS_PATTERN_16_ZEROS        0x80008000 

/* The 16-bit JTAG-to-SWD select sequence is defined to be 0b0011110011100111, MSB
first. This can be represented as 16'h3CE7 if transmitted MSB first or 16'hE73C if
transmitted LSB first. */
#define SWD_JTAG_SEQUENCE           0x80E7803C

#define DBG_ENTRY_PRESENT           0x1

#define LENGTH_32_BIT_FORMAT        4
#define LENGTH_8_BIT_FORMAT         0x10

/* Coresight Component defintions. Ref ARM IHI0029B Coresight Components.*/
/* In case of 32 bit format maximum number is 960 but in case of 8 bit 
format maximum number is 240. */
#define MAX_CORESIGHT_COMP      960
#define BASEADDR_OFFSET_MASK        (0xFFFFF000) 
enum TyCompClass
{
    COMPO_CLASS_START = 0x0,
    GENERIC_VERIF_COMPO = 0x0,
    ROM_TABLE_COMP = 0x1,
    DEBUG_COMPO = 0x9,
    //PERI_TST_BLOCK_COMPO = 0xB,
    DATA_ENGINE_SUBSYSTEM = 0xD,
    //GENERIC_IP_COMPO = 0xE,
    PRIMECELL_PERIP = 0xF,
    NOT_VALID_COM = 0x10, 
    COMPO_CLASS_END
};

enum TyCompType
{
    CORTEX_M_NVIC      = 0x000,
    CORTEX_M_ITM       = 0x001,
    CORTEX_M_DWT       = 0x002,  
    CORTEX_M_FBP       = 0x003,
    CORESIGHT_ETM11     = 0x00d,
    TI_SDTI             = 0x120,
    TI_DAPCTL           = 0x343,
    CORTEX_M_ETM1      = 0x4e0,
    CORESIGHT_CTI       = 0x906,
    CORESIGHT_ETB       = 0x907,
    CORESIGHT_CSTF      = 0x908,
    CORESIGHT_ETM9      = 0x910,
    CORESIGHT_TPIU      = 0x912,
    CORTEX_A_ETM       = 0x921,
    CORTEX_A_CTI       = 0x922,
    CORTEX_M_TPIU      = 0x923,
    CORTEX_M_ETM2      = 0x924,
    CORTEX_A_DEBUG     = 0xc08,
    UNKNOWN_TYPE        = 0xFFF
};

/* Abort Register bit definition */
#define DAP_ABORT               (0x1 << 0)

#define WRITE               0x0
#define READ                0x1

#define RETURN_VALID        0x1
#define RETURN_NOT_VALID    0x0

/* Acknowledgment types */
#define ACK_OK_FAULT        0x2
#define WAIT                0x1

#define MAX_WAIT_CNT        1000

#define COMP_ID_0   0x0
#define COMP_ID_1   0x1
#define COMP_ID_2   0x2
#define COMP_ID_3   0x3

#define PERIP_ID_4   0x0
#define PERIP_ID_5   0x1
#define PERIP_ID_6   0x2
#define PERIP_ID_7   0x3
#define PERIP_ID_0   0x4
#define PERIP_ID_1   0x5
#define PERIP_ID_2   0x6
#define PERIP_ID_3   0x7

typedef struct
{
    BOOL bEntryPresent;
    unsigned long ulCompBaseAddr;
    enum TyCompClass tyCompClass;
    unsigned long ulCompType;
} TyRomTableEntry;

typedef struct
{
    unsigned long ulNumEntries;
    unsigned long ulIdrRegValue;
    unsigned long ulDbgBaseRegValue;
    unsigned long ulConfiRegValue;
    TyRomTableEntry tyRomTableEntry[MAX_CORESIGHT_COMP];
}TyCoresightComp;

typedef struct
{
    unsigned long ulDbgAccessApIndex;
    unsigned long ulMemAccessApIndex;
    unsigned long ulDbgBaseAddr;
    unsigned long ulUseMemApMemAccess;
} TyCoreSightParam;


#define COMPONENT_ID0_OFFSET	             (0xFF0)
#define COMPONENT_ID1_OFFSET	             (0xFF4)
#define COMPONENT_ID2_OFFSET	             (0xFF8)
#define COMPONENT_ID3_OFFSET	             (0xFFC)

#define PERIPHERAL_ID4_OFFSET		         (0xFD0)
#define PERIPHERAL_ID5_OFFSET		         (0xFD4)
#define PERIPHERAL_ID6_OFFSET		         (0xFD8)
#define PERIPHERAL_ID7_OFFSET		         (0xFDC)
#define PERIPHERAL_ID0_OFFSET		         (0xFE0)
#define PERIPHERAL_ID1_OFFSET		         (0xFE4)
#define PERIPHERAL_ID2_OFFSET		         (0xFE8)
#define PERIPHERAL_ID3_OFFSET		         (0xFEC)


#define GEN_VER_COMP		(0x0)
#define ROM_TABLE			(0x1)
#define DEBUG_COMP			(0x9)
#define PERI_TEST_BLOCK		(0xB)
#define DESS_COMP			(0xD)
#define GEN_IP_COMP			(0xE)
#define PRICEL_COMP			(0xF)
#define DEVTYPE_MAJOR_MASK		(0x0F)
#define DEVTYPE_SUBTYPE_MASK    (0xF0)
#define DEVTYPE_SUBTYPE_SHIFT   (0x04)
/*----------------------------------------------------------------------------------------*/
/* Peripheral ID Register */
/*----------------------------------------------------------------------------------------*/
typedef union 
	{
		unsigned char ucVal;		
		unsigned long ulCompIDRegVal;
	}TyCompIDVal;

/*----------------------------------------------------------------------------------------*/
/* Peripheral ID Register */
/*----------------------------------------------------------------------------------------*/
typedef union
	{
		unsigned char ucVal;
		unsigned long ulperiIDRegVal;
	}TyPeriIDVal;

/*----------------------------------------------------------------------------------------*/
/* Device Type Register */
/*----------------------------------------------------------------------------------------*/
typedef union
	{
		unsigned char ucVal;
		unsigned long ulDeviceIDRegVal;
	}TyDevIDVal;

typedef	union _TyEntry
			 {
			   struct 
				  {
					 unsigned int uiEntryPresent:1;         /* 	[0]Entry present:This bit indicates whether an entry is present at this location 
        															in the ROM table. Its
        															possible values are:
        															   0: Entry not present
        															   1: Entry present
															*/
					 unsigned int  uiFormat:1;           	/* 	[1]Format:This bit indicates the format of the ROM Table. 
															    Its possible values are:
															            0: 8-bit ROM Table.
															            1: 32-bit ROM Table
															*/
					 unsigned int  uiReserved:10;        	/* 	[11:2]-Reserved. Should Be Zero */

					 int iAddOffset:20;			            /* 	[31:12]Address offset:The base address of the component,
															    relative to the base address of this ROM Table.
															    Negative values are permitted, using two�s complement(entry must not be zero).
															*/
					 }tyEntryFormat;

				 unsigned long ulTableEntry;	   			/* ROM Table entries + Reserved region 0x000-0xFCB:4044-bytes*/

			 }TyEntry;


typedef struct _TyDebugComp
				{
					unsigned long ulCompStart;
					unsigned long ulCompBase;
					unsigned long ulcompSize;
					TyCompIDVal tyCompID[4];
					TyPeriIDVal tyPeriID[8];
					TyDevIDVal  tyDevType;
					unsigned long ulMajorType;
					unsigned long ulSubType;
				}TyDebugComp;

typedef struct _TyROMTable
			   {
				  unsigned long ulbaseAddress;
				  TyEntry tyEntry;
				  unsigned long ulMemTypeReg;
				  TyCompIDVal tyCompIDVal[4];
				  TyPeriIDVal tyPeriID[8];
			   }TyROMTable;


typedef struct _TyBank15regs
			   {
				  unsigned long ulDbgBaseRegValue;
				  unsigned long ulIdrRegValue;
				  unsigned long ulConfiRegValue;
			   }TyBank15regs;

RESULT ARML_DapResetProc(void);
RESULT CoreSight_ConfigDebugPort(    
    unsigned long* pulDebugPortParam);

RESULT ReadRomTableEntries(	unsigned long ulBaseAddr,
							unsigned long ulApIndex,
							unsigned long* ulNumData,
                            unsigned long* pulNumEntries,
							unsigned long* pulData);

RESULT GetBank15RegValue(unsigned long ulApIndex,
						 unsigned long* pulData);

RESULT GetRomTableEntry( unsigned long ulRomTableEntryAddr,
						 unsigned long* pulRomTableEntry);

RESULT GetCompIdValue(unsigned long ulCompBaseAddr,
					  unsigned long ulRegOffset,
					  unsigned long* pulCompIdVal);

RESULT GetPeriIdValue(	unsigned long ulCompBaseAddr,
						unsigned long ulRegOffset,
						unsigned long* pulPeriIdVal);

RESULT GetDevTypeRegValue(
    unsigned long ulCompBaseAddr,
    unsigned long* pulDevTypeRegVal);

RESULT CoreSight_WriteDapReg(unsigned long ulRegAddr, unsigned long ulData);
RESULT CoreSight_ReadDapReg(unsigned long ulRegAddr, unsigned long* pulData);
RESULT Coresight_SelectBank(unsigned long ulBankNo , 
                            unsigned long ulApindex);
RESULT CoreSight_ReadDebugReg(unsigned long ulMemApIndex,
                              unsigned long ulDebugBaseAddr,
                              unsigned long* pulDebugRegVal);

void CoreSight_MaxTARBlockSize (
    unsigned long ulMaxTarIncSize,
    unsigned long ulStartAddr,
    unsigned long* pulMaxSize);

RESULT CoreSight_WriteMultipleMem(
    unsigned long ulNumData,
    unsigned char* pucData,
    unsigned long  ulAccessType);
RESULT CoreSight_ClearStickyError (void);
RESULT CoreSight_SetupAccessPort (unsigned long ulAccessSize);
unsigned long OMAP_ActivateDebugPower(void);

#endif // #define DAP_H
