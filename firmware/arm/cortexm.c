/******************************************************************************
       Module: cortexm.c
     Engineer: Jeenus Chalattu Kunnath
  Description: Implementation of Cortex-M debug functionalities 
 Date           Initials    Description
  28-May-2009    JCK          initial
  12-Oct-2009    DVA          CortexM fcns added.      
******************************************************************************/
#include "../common/common.h"  /* Definition of debug message and internal I/O macros */
#include "cortexm.h"  /* Definition of Cortex-M3 specific registers. */
#include "common/fpga/jtag.h"   /* JTAG functions like JtagScanDR */
#include "common/fpga/fpga.h"   /* Definitions of JTAG registers and buffers in the FPGA */
#include "export/api_err.h" /* Definitions of Error returned by the Firmware*/
#include "armlayer.h"   /* Defintion of Cortex-M3 IR and DR lengths and Cortex-M3 core type */

/* Added for Cortex M3 support */
extern UWORD gulARM_JTAG_DPACC;
extern UWORD gulARM_JTAG_APACC;

/* To save the Access port type */
static UWORD ulCurrentApType = AHB_AP;

DAP_DPACC     DpaccReg;
AP_BANK0_REG  ApBank0Reg;
AP_BANK1_REG  ApBank1Reg;
AP_BANK15_REG ApBank15Reg;
CortexM CortexMDbgAddr = {NULL,NULL,NULL,NULL,NULL,NULL};

/* Local function prototypes */
static RESULT AccessMemData(
                           UBYTE* pucDataToScan,
                           UBYTE* pucDataFromScan,
                           UWORD  ulNumData,
                           UWORD  ulDataReg,
                           UWORD  ulTypeAccess,
                           UWORD  ulSizeAccess);

static RESULT SetupAccessPort(
                             UWORD ulAccessSize,
                             UWORD ulAddrIncType,
                             UWORD ulMemAddr);

static RESULT AccessDbgPortReg(
                              UWORD  ulRegister,
                              UWORD  ulDataToScan,
                              UWORD* pulDataFromScan,
                              UWORD  ulTypeScan);

/****************************************************************************
Function:   PrepareDpaccApaccData
Engineer:   Jeenus C.K.
Input   :   UWORD*  pulTDIVal  - To return the data to be scanned.
        :   UBYTE   ucAddr     - Address of the DAP register 
        :   UBYTE   ulData     - Data to be scanned
        :   BOOLEAN         bAction    - Read or Write action.
Output  :   void
Description: To prepare the data to be scanned to the TDI in the format of 
           : DR scan chain.
Date            Initials    Description
08-Oct-2009     JCK         Initial
09-Aug-2010     SJ          Modified to shift and use the proper DAP register address 
                            value. Only bits 3 and 2 are used. Bits 1 and 0 are ignored. 
****************************************************************************/
void PrepareDpaccApaccData(
                          UWORD* pulTDIVal,
                          UWORD  ulAddr,
                          UWORD  ulData,
                          UWORD  bAction
                          )
{     
    /* The format of DPACC and APACC data to be scanned is - 
    1. The length of DR is 35 bits.
    2. Bit 0 represents read or write action to be performed.
    3. Bit 1 and 2 represents the address of the DAP register.
    4. Bits 3 to 34 represents the 32 bit data to be scanned. 

    34                                3       2        1     0                                  
    ----------------------------------------------------------
    |           DATAIN[34:3]          |     ADDR[3:2]   |RnW  |   
    ----------------------------------------------------------
    */ 
    pulTDIVal[0] = ((ulData << 3) | (((ulAddr & 0xC) >> 2) << 1) | (bAction & 0x1));
    pulTDIVal[1] = (ulData >> 29);
}

/****************************************************************************
Function:   FilterDpaccApaccData
Engineer:   Jeenus C.K.
Input   :   UWORD* pulData
        :   UWORD* pulDataFromScan
        :   UBYTE  pucAck
Output  :   void
Description: To filter the data to received from the TDO.
Date            Initials    Description
08-Oct-2009     JCK         Initial
****************************************************************************/
void FilterDpaccApaccData(
                         UWORD* pulDataFromScan,
                         UWORD* pulData,    
                         UBYTE* pucAck
                         )
{
    /* The format of data shifted out for DPACC and APACC is,
    1. Bits 0 to 2 represents the ACK
    2. Bits 3 to 34 represents the responce data */
    pulData[0]   = ((pulDataFromScan[0] >> 3 ) | (pulDataFromScan[1] << 29));
    pucAck[0]    = (pulDataFromScan[0] & 0x00000007);
}

/****************************************************************************
Function:   CortexM_LeaveDebugState
Engineer:   Jeenus C.K.
Input   :   void  
Output  :   UWORD - error code for commands (ERR_xxx)
Description: To change the processor state from debug mode.
Date            Initials    Description
13-Oct-2009     JCK         Initial
17-MAR-2011     ADK         Fix for interrupt during debug probelm.
****************************************************************************/
unsigned long CortexM_LeaveDebugState(unsigned long ulAddr)
{
    unsigned long   ulResult        = ERR_NO_ERROR;    
    unsigned long   ulTDIVal[2];
    unsigned long   ulTDOVal[2];    
    unsigned long   ulData;
    unsigned char   ucAck;
    unsigned long   ulRegData;
    unsigned long   ulWaitCount = 0;

    DLOG(("CortexM_LeaveDebugState\r\n"));

    /* APSELECT BANK0 of the AP bank using the AP APSELECT register */
    ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_CTRL_STAT_REG, 0x50000020, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* Transfer Address Register, TAR is used to write the address. TAR 
    register is in the BANK 0 of the MEM-AP register banks. */
    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_AP_SELECT_REG, AP_BANK_0 | 0, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* APSELECT BANK0 of the AP bank using the AP APSELECT register */
    ulResult = JtagScanIR(&gulARM_JTAG_APACC,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /*  
    Note:Commented to avoid Auto increment bit in DEFAULT_CSW_VAL
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_CSW_REG, DEFAULT_CSW_VAL, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    */

    /* Without Auto increment,so that write twice in DHCSR is possible :see below steps */
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_CSW_REG, CSW_VAL_INC_OFF, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    PrepareDpaccApaccData(ulTDIVal, MEM_AP_TAR_REG, (UWORD)&(CORTEXM_CDBGR_BASE->CDR_DHCSR), WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* As per ARM Support mail 
    - Set DHCSR to 0xA05F0003 (disable C_STEP and C_MASKINTS), then
    - Set DHCSR to 0xA05F0001 (unhalt core)
    */
    ulRegData = (DHCR_WR_DBGKEY | DHCR_WR_CHALT | DHCR_WR_CDEBUGG_EN);

    PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, ulRegData, WRITE);
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulRegData = (DHCR_WR_DBGKEY | DHCR_WR_CDEBUGG_EN); 

    PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, ulRegData, WRITE);
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    PrepareDpaccApaccData(ulTDIVal, MEM_AP_TAR_REG, 0x0, READ);
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_CTRL_STAT_REG, 0x0, READ);
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_RDBUFF, 0x0, READ);
    ulResult = JtagScanDR(35,ulTDIVal,ulTDOVal);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL, &ulRegData, READ);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    do
       {
         FilterDpaccApaccData(ulTDOVal,&ulData, &ucAck);
         if (WAIT == ucAck)
             {
             ulResult = JtagScanDR(
                           CORTEXM_DR_LENGTH,
                           ulTDIVal,
                           ulTDOVal
                           );
             if (ulResult != ERR_NO_ERROR)
                 {
                 return ulResult;
                 }
             }
         ulWaitCount++;
         if (MAX_WAIT_CNT == ulWaitCount)
             {
             ulTDIVal[0] = ulData;
             return ERR_ARM_MAXIMUM_COUNT_REACHED;
             }
    }while (ucAck != ACK_OK_FAULT);            

    return ulResult;
}

/****************************************************************************
Function:   CortexM_EnterDebugState
Engineer:   Jeenus C.K.
Input   :   void  
Output  :   UWORD - error code for commands (ERR_xxx)
Description: To change the processor state to debug mode.
Date            Initials    Description
13-Oct-2009     JCK         Initial
17-MAR-2011     ADK         Fix for interrupt during debug probelm.
****************************************************************************/
RESULT CortexM_EnterDebugState(UWORD addr)
{
    RESULT  Result        = ERR_NO_ERROR;    
    UWORD   ulTDIVal[2];  
    UWORD   ulTDOVal[2];    
    UWORD   ulRegData;
    UWORD   ulReadData;
    UBYTE   ucAck;
    UWORD   ulWaitCount = 0;
    
    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }
    
    Result = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, (AP_BANK_0 | ulCurrentApType), NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }        

    Result = JtagScanIR(&gulARM_JTAG_APACC,NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ApBank0Reg.CSW = DEFAULT_CSW_VAL;
    ulRegData = (ApBank0Reg.CSW & CSW_PROT) | ACCESS_SIZE_32 |INC_SINGLE;   
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_CSW_REG, ulRegData, WRITE);
    ulWaitCount = 0;
    do
        {
        Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,ulTDOVal);    
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        FilterDpaccApaccData(ulTDOVal, &ulReadData, &ucAck);
        ulWaitCount = ulWaitCount + 1;
        if(MAX_WAIT_CNT == ulWaitCount)
            {
            ulTDIVal[0] = ulReadData;
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
        }while (WAIT == ucAck);

    ulRegData = (UWORD)&(CORTEXM_CDBGR_BASE->CDR_DHCSR);   
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_TAR_REG, ulRegData, WRITE);
    Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Added DHCR_WR_CMASKINTS bit to avoid Interruprt during Debug ON 17-3-2011*/ 
    ulRegData = (DHCR_WR_DBGKEY | DHCR_WR_CHALT | DHCR_WR_CDEBUGG_EN | DHCR_WR_CMASKINTS);
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, ulRegData, WRITE);
    ulWaitCount = 0;
    do
        {
        Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,ulTDOVal);    
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        FilterDpaccApaccData(ulTDOVal, &ulReadData, &ucAck);
        ulWaitCount = ulWaitCount + 1;
        if(MAX_WAIT_CNT == ulWaitCount)
            {
            ulTDIVal[0] = ulReadData;
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
        }while (WAIT == ucAck);   

    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, READ);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function:   AccessDbgPortReg
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
13-Oct-2009     JCK         Initial
****************************************************************************/
static RESULT AccessDbgPortReg(
                              UWORD  ulRegister,
                              UWORD  ulDataToScan,
                              UWORD* pulDataFromScan,
                              UWORD  ulTypeScan)
{
    RESULT  Result  = ERR_NO_ERROR;
    UWORD   ulTDIVal[2];
    UWORD   ulTDOVal[2];
    UWORD   ulReadData;
    UWORD   ulWaitCount = 0;

    Result = JtagScanIR(&gulARM_JTAG_DPACC,NULL);    
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    PrepareDpaccApaccData(ulTDIVal, ulRegister, ulDataToScan, ulTypeScan); 
    if (NULL == pulDataFromScan)
        {
        UBYTE   ucAck;
        do
            {
            Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,ulTDOVal);    
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
            FilterDpaccApaccData(ulTDOVal, &ulReadData, &ucAck);
            ulWaitCount = ulWaitCount + 1;
            if(MAX_WAIT_CNT == ulWaitCount)
                {
                ulTDIVal[0] = ulReadData;
                return ERR_ARM_MAXIMUM_COUNT_REACHED;
                }
            }while (WAIT == ucAck);
        }
    else
        {
        UBYTE   ucAck;
        do
            {
            Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,ulTDOVal); 
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
            FilterDpaccApaccData(ulTDOVal, pulDataFromScan, &ucAck);
            ulWaitCount = ulWaitCount + 1;
            if(MAX_WAIT_CNT == ulWaitCount)
                {
                return ERR_ARM_MAXIMUM_COUNT_REACHED;
                }
            }while (WAIT == ucAck);        
        }

    return Result;

}

/****************************************************************************
Function:   SetupAccessPort
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
13-Oct-2009     JCK         Initial
****************************************************************************/
static RESULT SetupAccessPort(
                             UWORD ulAccessSize,
                             UWORD ulAddrIncType,
                             UWORD ulMemAddr)
{
    RESULT  Result  = ERR_NO_ERROR; 
    UWORD   ulAccessPortReg;
    UWORD   ulDatatoScan;
    UWORD   ulTDIVal[2];

    Result = JtagScanIR(&gulARM_JTAG_APACC,NULL);    
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ApBank0Reg.CSW = DEFAULT_CSW_VAL;
    ulAccessPortReg = MEM_AP_CSW_REG;
    ulDatatoScan = (ApBank0Reg.CSW & CSW_PROT) | DBG_SW_ENABLE |ulAccessSize |ulAddrIncType;   
    PrepareDpaccApaccData(ulTDIVal, ulAccessPortReg, ulDatatoScan, WRITE);
    Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,NULL);   
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulAccessPortReg = MEM_AP_TAR_REG;
    ulDatatoScan = ulMemAddr;   
    PrepareDpaccApaccData(ulTDIVal, ulAccessPortReg, ulDatatoScan, WRITE);
    Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,NULL);
    return Result;    
}

/****************************************************************************
Function:   ARML_CortexMReadMemory
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
16.10.09          JCK         Initial
****************************************************************************/
RESULT ARML_CortexMReadMemory(
    UWORD*  pulDataFromMem,
    UWORD   ulAddress,
    UWORD   ulNumData,
    UWORD   ulAccessSize)
{   
    RESULT  Result  = ERR_NO_ERROR;
    UWORD   ulRegData;
    UWORD   ulNumDataTemp;
    UWORD   ulAddrBoundary;
    UWORD   ulAddrIncSize;

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Select BANK0 of AP registers using the AP select registers. */
    Result = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, (AP_BANK_0 | ulCurrentApType), NULL, WRITE);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    do
        {
        switch (ulAccessSize)
            {
            case ACCESS_SIZE_8:
                /* If access is Byte, the Address incrment by 1 in case of
                auto increment mode. */
                ulAddrIncSize = ADDR_INC_SIZE_ACCESS_8;     
                break;
            case ACCESS_SIZE_16:
                /* If access is Half-Word, the Address incrment by 2 in case of
                auto increment mode. */
                ulAddrIncSize = ADDR_INC_SIZE_ACCESS_16;                
                break;
            case ACCESS_SIZE_32:
                /* If access is Word, the Address incrment by 4 in case of
                auto increment mode. */
                ulAddrIncSize = ADDR_INC_SIZE_ACCESS_32;                
                break;
            default:
                return ERR_ARM_INVALID_PARM;
            }        

        /* If address boundary is more than maximum address boundary, change the number of data 
        to be written to be written according to the access size.*/
        ulAddrBoundary = ulNumData * ulAddrIncSize; 
        if (MAX_AUTO_INC_BOUNDARY <= ulAddrBoundary)
            {
            ulNumDataTemp = MAX_AUTO_INC_BOUNDARY / ulAddrIncSize;
            }
        else
            {
            ulNumDataTemp = ulNumData;
            }

        /* Setup the CTRL/STAT register with Access size and Increment single. Also 
        configure TAR register witht the address to which data is to be written. */
        Result = SetupAccessPort(ulAccessSize, INC_SINGLE, ulAddress);
        if(Result != ERR_NO_ERROR)
            {
            return Result;
            }

        //DLOG(("Number Of Data: 0x%8x", ulNumDataTemp));
        /* Read the data from the memory. */
        Result = AccessMemData(NULL, (UBYTE *)pulDataFromMem, ulNumDataTemp, MEM_AP_DRW_REG, READ, ulAccessSize);
        if(Result != ERR_NO_ERROR)
            {
            DLOG(("Error: AccessMemData\r\n"));
            return Result;
            }

        ulNumData = ulNumData - ulNumDataTemp;
        ulAddress = ulAddress + MAX_AUTO_INC_BOUNDARY;
        /* Since interger pointer is used and data corresponding to MAX_AUTO_INC_BOUNDARY is read 
        independent of the access size, increment the data pointer by MAX_AUTO_INC_BOUNDARY / 4.
        Divide by 4 is necessary because of the interger pointer. */
        pulDataFromMem = pulDataFromMem + (MAX_AUTO_INC_BOUNDARY / 4);
        }
    while (ulNumData != 0);          

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function:   ARML_CortexMWriteMemory
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
16-Oct-2009     JCK         Initial
****************************************************************************/
RESULT ARML_CortexMWriteMemory(
    UWORD*  pulDataToMem,
    UWORD   ulAddress,
    UWORD   ulNumData,
    UWORD   ulAccessSize)
{   
    RESULT  Result  = ERR_NO_ERROR;
    UWORD   ulRegData;
    UWORD   ulNumDataTemp;
    UWORD   ulAddrBoundary;
    UWORD   ulAddrIncSize;

    if (NULL == pulDataToMem)
        {
        return ERR_ARM_INVALID_PARM;
        }    

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }
    
    /* Select BANK0 of AP registers using the AP select registers. */
    Result = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, (AP_BANK_0 | ulCurrentApType), NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    do
        {
        switch (ulAccessSize)
            {
            case ACCESS_SIZE_8:
                /* If access is Byte, the Address incrment by 1 in case of
                auto increment mode. */
                ulAddrIncSize = ADDR_INC_SIZE_ACCESS_8;                
                break;
            case ACCESS_SIZE_16:
                /* If access is Half-Word, the Address incrment by 2 in case of
                auto increment mode. */
                ulAddrIncSize = ADDR_INC_SIZE_ACCESS_16;                
                break;
            case ACCESS_SIZE_32:
                /* If access is Word, the Address incrment by 4 in case of
                auto increment mode. */
                ulAddrIncSize = ADDR_INC_SIZE_ACCESS_32;                
                break;
            default:
                return ERR_ARM_INVALID_PARM;
            }        

        /* If address boundary is more than maximum maximum address boundary, change the number of data 
        to be written to be written according to the access size.*/
        ulAddrBoundary = ulNumData * ulAddrIncSize; 
        if (MAX_AUTO_INC_BOUNDARY <= ulAddrBoundary)
            {
            ulNumDataTemp = MAX_AUTO_INC_BOUNDARY / ulAddrIncSize;
            }
        else
            {
            ulNumDataTemp = ulNumData;
            }

        /* Setup the CTRL/STAT register with Access size and Increment single. Also 
        configure TAR register witht the address to which data is to be written. */
        Result = SetupAccessPort(ulAccessSize, INC_SINGLE, ulAddress);
        if(Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Write the data to the memory. */
        Result = AccessMemData((UBYTE *)pulDataToMem, NULL, ulNumDataTemp, MEM_AP_DRW_REG, WRITE, ulAccessSize);
        if(Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulNumData = ulNumData - ulNumDataTemp;
        ulAddress = ulAddress + MAX_AUTO_INC_BOUNDARY;
        /* Since interger pointer is used and data corresponding to MAX_AUTO_INC_BOUNDARY is written 
        independent of the access size, increment the data pointer by MAX_AUTO_INC_BOUNDARY / 4.
        Divide by 4 is necessary because of the interger pointer. */
        pulDataToMem = pulDataToMem + (MAX_AUTO_INC_BOUNDARY / 4);
        }
    while (ulNumData != 0);    

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }
    
    return Result;
}

/****************************************************************************
Function:   AccessMemData
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
20-Oct-2009     JCK         Initial
****************************************************************************/
static RESULT AccessMemData(
                           UBYTE* pucDataToScan,
                           UBYTE* pucDataFromScan,
                           UWORD  ulNumData,
                           UWORD  ulDataReg,
                           UWORD  ulTypeAccess,
                           UWORD  ulSizeAccess)
{
    RESULT  Result  = ERR_NO_ERROR;
    UWORD   ulTDIVal[2];
    UWORD   ulTDOVal[2];
    UWORD   ulReadData;    
    UBYTE   ucAck;
    UWORD   ulNumMemAccess;
    UWORD   ulWaitCount = 0;
    void*   pReadData;

    pReadData = (void*)&ulReadData;

    for (ulNumMemAccess = 0; ulNumMemAccess < ulNumData; ulNumMemAccess++)
        {
        ulWaitCount = 0;
        if (NULL == pucDataToScan)
            {
            PrepareDpaccApaccData(ulTDIVal, ulDataReg, 0, ulTypeAccess);
            }
        else
            {
            PrepareDpaccApaccData(ulTDIVal, ulDataReg, *((UWORD*)pucDataToScan), ulTypeAccess);
            }

        if (pucDataFromScan != NULL)
            {
            do
                {
                Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,ulTDOVal);
                if (Result != ERR_NO_ERROR)
                    {
                    return Result;
                    }
                FilterDpaccApaccData(ulTDOVal, (UWORD*)pReadData, &ucAck); 


                ulWaitCount = ulWaitCount + 1;
                if (MAX_WAIT_CNT == ulWaitCount)
                    {
                    return ERR_ARM_MAXIMUM_COUNT_REACHED;
                    }
                }while (WAIT == ucAck);


            if (0 == ulNumMemAccess)
                {
                /* Ignore the read data. Data out in the first read is not valid.*/
                }
            else if (ACCESS_SIZE_32 == ulSizeAccess)
                {
                *(UWORD*)pucDataFromScan = *(UWORD*)pReadData;                
                pucDataFromScan = pucDataFromScan + 4;
                }
            else if (ACCESS_SIZE_16 == ulSizeAccess)
                {
                *(UHWORD*)pucDataFromScan = *(UHWORD*)pReadData;
                pucDataFromScan = pucDataFromScan + 2;
                }
            else if (ACCESS_SIZE_8 == ulSizeAccess)
                {
                *(UBYTE*)pucDataFromScan = *(UBYTE*)pReadData;
                pucDataFromScan = pucDataFromScan + 1;
                }
            else
                {
                return ERR_ARM_INVALID_PARM;
                }

            }
        else if(pucDataToScan != NULL)
            {            
            do
                {
                Result = JtagScanDR(CORTEX_DR_LENGTH,ulTDIVal,ulTDOVal);
                if (Result != ERR_NO_ERROR)
                    {
                    return Result;
                    }
                FilterDpaccApaccData(ulTDOVal, (UWORD*)pReadData, &ucAck);
                ulWaitCount = ulWaitCount + 1;
                if (MAX_WAIT_CNT == ulWaitCount)
                    {
                    return ERR_ARM_MAXIMUM_COUNT_REACHED;
                    }
                }while (WAIT == ucAck);

            if(ACCESS_SIZE_32 == ulSizeAccess)
                {                
                pucDataToScan = pucDataToScan + 4;
                }
            else if(ACCESS_SIZE_16 == ulSizeAccess)
                {                
                pucDataToScan = pucDataToScan + 2;
                }
            else if(ACCESS_SIZE_8 == ulSizeAccess)
                {                
                pucDataToScan = pucDataToScan + 1;
                }
            else
                {
                return ERR_ARM_INVALID_PARM;
                }
            }
        else
            {
            return ERR_ARM_INVALID_PARM;
            }
        }

    if (pucDataFromScan != NULL)
        {
        /* To get the last value of read, need to read the RDBUFF register. */
        Result = AccessDbgPortReg(JTAG_DP_RDBUFF, 0, (UWORD*)pucDataFromScan, READ);
        }

    return Result;
}
/****************************************************************************
     Function: ARML_CortexMStatusProc
     Engineer: Deepa V.A.  
        Input: pucCoreExecuting: Output if the core is executing or not.
               pucThumbMode:Thumb mode or not
               pucWatchPoint: If the core halted becz of a watchpoint/breakpoint
       Output: unsigned long - error code ERR_xxx 
  Description: Reads the status of the processor.
 Date           Initials    Description
 13-Oct-2009     DVA        Initial
****************************************************************************/
unsigned long ARML_CortexMStatusProc(unsigned char* pucCoreExecuting,
                                      unsigned char* pucThumbMode,
                                      unsigned char* pucWatchPoint)
{
    unsigned long   ulResult = ERR_NO_ERROR;
    volatile unsigned long   ulDHCSR;
    UWORD    ulRegData;

    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL, &ulRegData, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    /* Read Debug Halting Control and Status Register. */
    ulResult = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, AP_BANK_0 | ulCurrentApType, NULL, WRITE);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    ulResult = SetupAccessPort(ACCESS_SIZE_32, AUTO_INC_OFF, DHCSR_ADDR);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulResult = AccessMemData(0, (UBYTE*)&ulDHCSR, 1, MEM_AP_DRW_REG, READ, ACCESS_SIZE_32);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    if (DHCSR_RD_SHALT == (ulDHCSR & DHCSR_RD_SHALT))
        {
        *pucCoreExecuting = FALSE;
        }    
    else 
        {
        *pucCoreExecuting = TRUE;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
   
    return ulResult;
}

/****************************************************************************
     Function: ARML_CortexMReadRegisters
     Engineer: Deepa V.A. 
        Input: pulData: pointer to store read/write Data
       Output: unsigned long - error code ERR_xxx 
  Description: Read Cortex M3 Core registers.
  Date         Initials    Description
  14-Oct-2009    DVA        Initial
****************************************************************************/
unsigned long ARML_CortexMReadWriteRegisters(unsigned long *pulData, unsigned char ucAccessType)
{
    RESULT          ulResult = ERR_NO_ERROR; 
    unsigned long   ulTDIVal[2];
    unsigned long   ulTDOVal[2];
    unsigned long   ulDapRegAddr;
    unsigned long   ulDatatoScan = 0;
    unsigned long   ulDHCSR;
    unsigned char   ucAck;
    unsigned long   ulWaitCount = 0;
    Register        eRegId;
    unsigned long   ulRegData;

    /* Write DPACC instruction to IR */
    ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* To select Bank 0 of AP register, write to AP Select register. */
    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_AP_SELECT_REG, AP_BANK_0 | 0, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* Write APACC instruction to IR */
    ulResult = JtagScanIR(&gulARM_JTAG_APACC,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* Write the address of the Debug Core Register Selector Register to TAR */
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_TAR_REG, (unsigned long)(DCRSR_ADDR-4), WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    /* Write DPACC instruction to IR */
    ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* To select Bank1 of AP register, write to AP Select register. */
    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_AP_SELECT_REG, AP_BANK_1 | 0, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    /* For each register read/write, Reg0 in Bank1 represents the DHCSR,
        Reg1  is the DCRSR, Reg2 is the DCRDR */
    for (eRegId = REG_R0; eRegId <= REG_CONTROL; eRegId++)//REG_PSP//REG_DbgRetAdd
        {
        if (19 == (int)eRegId)
            {
            continue;
            }

        /* Write APACC instruction to IR */
        ulResult = JtagScanIR(&gulARM_JTAG_APACC,NULL);
        if (ulResult != ERR_NO_ERROR)
            {
            return ulResult;
            }
        /* For a write operation,
           DCRDR should be written prior to write to the DCRSR */
        if (WRITE == ucAccessType)
            {
            /* Write to Debug Core Register Data Register. */
            ulDapRegAddr = MEM_AP_BD2_REG;
            ulDatatoScan = pulData[eRegId];
            PrepareDpaccApaccData(ulTDIVal, ulDapRegAddr, ulDatatoScan, WRITE); 
            ulResult = JtagScanDR(35,ulTDIVal,NULL);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }
            }
        /* Read from Debug Halting Control and Status register. 
           This is to check if the S_REGRDY bit before writing to DCRSR */
        do
            {
            ulDapRegAddr = MEM_AP_BD0_REG;
            ulDatatoScan = 0;
            PrepareDpaccApaccData(ulTDIVal, ulDapRegAddr, ulDatatoScan, READ); 
            ulResult = JtagScanDR(35,ulTDIVal,NULL);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }
            /* Write DPACC instruction to IR */
            ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }

            /* Read the data from  RDBUFF register */
            ulDapRegAddr = JTAG_DP_RDBUFF;
            ulDatatoScan = 0;
            PrepareDpaccApaccData(ulTDIVal, JTAG_DP_RDBUFF, 0x0, READ);
            ulResult = JtagScanDR(35,ulTDIVal,ulTDOVal);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }
            FilterDpaccApaccData(ulTDOVal, &ulDHCSR, &ucAck);
            ulWaitCount++;
            if (MAX_WAIT_CNT < ulWaitCount)
                {
                break;
                }
            }while ((0 == (DHCSR_S_REGRDY_BIT & ulDHCSR))||
                    (ucAck != ACK_OK_FAULT));
        ulWaitCount=0; // For reusing.

        /* Write APACC instruction to IR */
        ulResult = JtagScanIR(&gulARM_JTAG_APACC,NULL);
        if (ulResult != ERR_NO_ERROR)
            {
            return ulResult;
            }
        /* Write to debug Core Register Selector Register. */
        ulDapRegAddr = MEM_AP_BD1_REG;
        if (READ == ucAccessType)
            {
            ulDatatoScan = SELECT_READ_DCRSR | (WORD)eRegId; //0x00000000 | RegisterId
            }
        else
            {
            ulDatatoScan = SELECT_WRITE_DCRSR |(WORD)eRegId; //0x00010000 | RegisterId
            }         
        PrepareDpaccApaccData(ulTDIVal, ulDapRegAddr, ulDatatoScan, WRITE); 
        ulResult = JtagScanDR(35,ulTDIVal,NULL);
        if (ulResult != ERR_NO_ERROR)
            {
            return ulResult;
            }

        if (READ == ucAccessType)
            {
            /* Read from Debug Core Register Data Register */
            ulDapRegAddr = MEM_AP_BD2_REG;
            ulDatatoScan = 0;
            PrepareDpaccApaccData(ulTDIVal, ulDapRegAddr, ulDatatoScan, READ); 
            ulResult = JtagScanDR(35,ulTDIVal,NULL);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }

            /* Write DPACC instruction to IR */
            ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }
            /* Read the data from  RDBUFF register */
            ulDapRegAddr = JTAG_DP_RDBUFF;
            ulDatatoScan = 0;
            PrepareDpaccApaccData(ulTDIVal, JTAG_DP_RDBUFF, 0x0, READ);
            ulResult = JtagScanDR(35,ulTDIVal,ulTDOVal);
            if (ulResult != ERR_NO_ERROR)
                {
                return ulResult;
                }

            do
                {
                FilterDpaccApaccData(ulTDOVal, &pulData[eRegId], &ucAck);
                if (WAIT == ucAck)
                    {
                    ulResult = JtagScanDR(
                                         CORTEXM_DR_LENGTH,
                                         ulTDIVal,
                                         ulTDOVal
                                         );
                    if (ulResult != ERR_NO_ERROR)
                        {
                        return ulResult;
                        }
                    }
                ulWaitCount++;  
                if (MAX_WAIT_CNT == ulWaitCount)
                    {
                    return ERR_ARM_MAXIMUM_COUNT_REACHED;
                    }
                }while (ucAck != ACK_OK_FAULT);
            ulWaitCount=0; //For Reusing
            }
        }//Loop End

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    return ulResult;

}

/****************************************************************************
     Function: ARML_CortexMSingleStep
     Engineer: Deepa V.A. 
        Input: 
       Output: unsigned long - error code ERR_xxx 
  Description: Make the target single step the next instruction.
  Date         Initials    Description
  26-Oct-2009    DVA        Initial
****************************************************************************/
unsigned long ARML_CortexMSingleStep(void)
{
    unsigned long   ulResult        = ERR_NO_ERROR; 
    unsigned long   ulTDIVal[2];
    unsigned long   ulRegData;

    //DLOG(("ARML_CortexMSingleStep"));

    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL, &ulRegData, READ);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* Write DPACC instruction to IR */
    ulResult = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* To select Bank 0 of AP register, write to AP Select register. */
    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_AP_SELECT_REG, AP_BANK_0 | ulCurrentApType, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if (ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }  

    ulResult = SetupAccessPort(ACCESS_SIZE_32, AUTO_INC_OFF, (UWORD)(&CORTEXM_CDBGR_BASE->CDR_DHCSR));
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    /* Write data to enable Single step in DHCSR. */
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, DHCSR_MASK_INT, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    /* Write data to enable Single step in DHCSR. */
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, DHCSR_S_STEP_BIT, WRITE); 
    ulResult = JtagScanDR(35,ulTDIVal,NULL);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    ulResult = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(ulResult != ERR_NO_ERROR)
        {
        return ulResult;
        }
    return ulResult;
}
/****************************************************************************
Function:   ARML_CortexMHWBreakpoint
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
21-Oct-2009     JCK         Initial
****************************************************************************/
RESULT ARML_CortexMHWBreakpoint(
    UBYTE ucNumComparators,
    UWORD *pulComparatorData)
{
    RESULT  Result      = ERR_NO_ERROR;
    UWORD   ulRegData   = 0;

    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL, &ulRegData, READ);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }     

    Result = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, (AP_BANK_0 | ulCurrentApType), NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }    

    Result = SetupAccessPort(ACCESS_SIZE_32, INC_SINGLE, (UWORD)&CORTEXM_FPB_BASE->FP_CTRL);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write the data to the Flash Patch Control Register. Disable FPB */
    ulRegData = FP_CTRL_KEY;
    Result = AccessMemData((UBYTE *)&ulRegData, NULL, 1, MEM_AP_DRW_REG, WRITE, ACCESS_SIZE_32);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = SetupAccessPort(ACCESS_SIZE_32, INC_SINGLE, (UWORD)&CORTEXM_FPB_BASE->FP_COMP0);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = AccessMemData((UBYTE*)pulComparatorData, NULL, ucNumComparators, MEM_AP_DRW_REG, WRITE, ACCESS_SIZE_32);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = SetupAccessPort(ACCESS_SIZE_32, INC_SINGLE, (UWORD)&CORTEXM_FPB_BASE->FP_CTRL);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

     /* Write the data to the Flash Patch Control Register. Enable the FPB */
    ulRegData = FP_CTRL_ENABLE | FP_CTRL_KEY;
    Result = AccessMemData((UBYTE *)&ulRegData, NULL, 1, MEM_AP_DRW_REG, WRITE, ACCESS_SIZE_32);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function:   ARML_CortexMWatchpoint
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
22-Oct-2009     JCK         Initial
****************************************************************************/
RESULT ARML_CortexMWatchpoint(
    UBYTE  ulNumWatchPointUnits,
    UWORD* pulWatchPointData)
{
    RESULT       Result      = ERR_NO_ERROR;
    WATCHPOINT*  pWatchPointCompAddr = NULL;   
    UBYTE        ucNumWatchPoint     = 0;
    UWORD        ulRegData   = 0;    
   
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL, &ulRegData, READ);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, (AP_BANK_0 | ulCurrentApType), NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }    

    pWatchPointCompAddr = (WATCHPOINT*)&CORTEXM_DWT_BASE->DWT_WATCHPOINT;

    for (ucNumWatchPoint = 0; ucNumWatchPoint < ulNumWatchPointUnits; ucNumWatchPoint++)
        {        
        Result = SetupAccessPort(ACCESS_SIZE_32, INC_SINGLE, (UWORD)pWatchPointCompAddr);
        if(Result != ERR_NO_ERROR)
            {
            return Result;
            }

        Result = AccessMemData((UBYTE*)pulWatchPointData, NULL, NUM_WATCHPOINT_REG, 
                               MEM_AP_DRW_REG, WRITE, ACCESS_SIZE_32);
        if(Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* To get the next watch point data */
        pulWatchPointData   = pulWatchPointData + NUM_WATCHPOINT_REG;

        /* To get next Watchpoint unit Address */
        pWatchPointCompAddr = pWatchPointCompAddr + 1;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }
    
    return Result;
}

/****************************************************************************
Function:   ARML_CortexMCheckDataAbort
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
22-Oct-2009     JCK         Initial
****************************************************************************/
RESULT ARML_CortexMCheckDataAbort(UBYTE* pucReadData)
{
    RESULT   Result      = ERR_NO_ERROR;
    UBYTE    ucFaultStat[NUM_CONFIG_FAULT_STAT_REG];
    UWORD    ulRegData;

    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL,&ulRegData, READ);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = AccessDbgPortReg(JTAG_DP_AP_SELECT_REG, (AP_BANK_0 | ulCurrentApType), NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = SetupAccessPort(ACCESS_SIZE_8, INC_SINGLE, CORTEXM_CONFI_FAULT_STAT_REG);
        if(Result != ERR_NO_ERROR)
            {
            return Result;
            }

    
    Result = AccessMemData(NULL, ucFaultStat, NUM_CONFIG_FAULT_STAT_REG, 
                               MEM_AP_DRW_REG, WRITE, ACCESS_SIZE_8);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    if ((ucFaultStat[1] & DATA_BUS_ABORT) != 0)
        {
        *pucReadData = 1;   /* Data abort occured */
        }
    else
        {
        *pucReadData = 0;   /* No data abort */
        }

    *pucReadData = 0;

    return Result;
}

/****************************************************************************
Function:   ARML_DapResetProc
Engineer:   Jeenus C.K.
Input   :     
Output  :   UWORD - error code for commands (ERR_xxx)
Description: 
Date            Initials    Description
22-Oct-2009     JCK         Initial
****************************************************************************/
RESULT ARML_DapResetProc(void)
{
    RESULT   Result      = ERR_NO_ERROR;
    UWORD    ulRegData;
    unsigned short usReg;
    unsigned long  ulTmsTemp;
    unsigned long ulSavedMcDRCount;

    DLOG(("ARML_DapResetProc\r\n"));

    /* Save current multi core settings */
    ulSavedMcDRCount = get_wvalue(JTAG_MCDRC);
    put_wvalue(JTAG_MCDRC, 0);
    
    /* As per Coresight Component TRM, to ensure JTAG is in the reset state 
    we must send more than 50 TCK clock cycles with TMS HIGH. */
    ulTmsTemp = TMS_PATTERN_16_ONES; 

    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);

    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(1,0));    
    put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(1,0));

    put_wvalue(JTAG_TDI_BUF(0), 0x0);
    put_wvalue(JTAG_TDI_BUF(1), 0x0); 
    put_wvalue(JTAG_TDI_BUF(2), 0x0); 
    put_wvalue(JTAG_TDI_BUF(3), 0x0); 

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    put_hvalue(JTAG_JASR, 0xF);

    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    /* If the current state of the core is SWD change the state to JTAG. */
    ulTmsTemp = SWD_JTAG_SEQUENCE; 
    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_TDI_BUF(0), 0x0);

    put_hvalue(JTAG_JASR, 0x1);
    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers

    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    ulTmsTemp = TMS_PATTERN_16_ONES; // 16 consutive ones

    /* In order to ensure reset, issue minimum 50TCK with TMS = 1 */
    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);

    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(1,0));    
    put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(1,0));

    put_wvalue(JTAG_TDI_BUF(0), 0x0);
    put_wvalue(JTAG_TDI_BUF(1), 0x0); 
    put_wvalue(JTAG_TDI_BUF(2), 0x0); 
    put_wvalue(JTAG_TDI_BUF(3), 0x0); 

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    put_hvalue(JTAG_JASR, 0xF);

    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers

    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    /* Restore multi core settings */
    put_wvalue(JTAG_MCDRC, ulSavedMcDRCount);

    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, NULL,&ulRegData, READ);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulRegData = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR | CDBG_RSTREQ; 
    Result = AccessDbgPortReg(JTAG_DP_CTRL_STAT_REG, ulRegData, NULL, WRITE);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}


