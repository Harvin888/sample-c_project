/******************************************************************************
       Module: armlayer.c
     Engineer: Vitezslav Hola
  Description: ARM layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
  01-Aug-2007    SPT         Included ARM Diskware Commands
******************************************************************************/
#include "common/common.h"
#include "common/timer.h"
#include "armlayer.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "export/api_err.h"
#include "cortexm.h"
#include "cortexa.h"
//#include "common/ml69q6203.h"

//defines

#define ARM940_946_CP_REG_C0_ID_REG                         0x00
#define ARM940_946_CP_REG_C0_CACHE_TYPE                     0x01
#define ARM940_946_CP_REG_C0_TCM_SIZE                       0x08
#define ARM940_946_CP_REG_C1_CONTROL                        0x02
#define ARM940_946_CP_REG_C2_DCACHE_BITS                    0x04
#define ARM940_946_CP_REG_C2_ICACHE_BITS                    0x05
#define ARM940_946_CP_REG_C3_WRITEBUF_CTRL                  0x06
#define ARM940_946_CP_REG_C5_DSPACE_PERMISSIONS             0x0A
#define ARM940_946_CP_REG_C5_ISPACE_PERMISSIONS             0x0B
#define ARM940_946_CP_REG_C6_REG0_MEMPROTECTION             0x20
#define ARM940_946_CP_REG_C6_REG1_MEMPROTECTION             0x22
#define ARM940_946_CP_REG_C6_REG2_MEMPROTECTION             0x24
#define ARM940_946_CP_REG_C6_REG3_MEMPROTECTION             0x26
#define ARM940_946_CP_REG_C6_REG4_MEMPROTECTION             0x28
#define ARM940_946_CP_REG_C6_REG5_MEMPROTECTION             0x2A
#define ARM940_946_CP_REG_C6_REG6_MEMPROTECTION             0x2C
#define ARM940_946_CP_REG_C6_REG7_MEMPROTECTION             0x2E
#define ARM940_946_CP_REG_C9_DCACHE_LOCKDOWN                0x12
#define ARM940_946_CP_REG_C9_ICACHE_LOCKDOWN                0x13
#define ARM940_946CP_WRITEMASK                              0x40


#define ARM966_CP_REG_C0_ID_REG                             0x00
#define ARM966_CP_REG_C1                                    0x02
#define ARM966_CP_REG_C15_1                                 0xFC
#define ARM966_CP_REG_C15_2                                 0xF9
#define ARM966_CP_REG_C15_3                                 0xFD
#define ARM966_CP_REG_C15_4                                 0xFB
#define ARM966_CP_REG_C15_5                                 0xFF
#define ARM966CP_WRITEMASK                                  0x40

#define ARM968_CP_REG_C0_ID_REG                             0x00
#define ARM968_CP_REG_C1                                    0x02
#define ARM968_CP_WRITEMASK                                 0x40

#define ARM920_922CP_PHYSICAL_ACCESS                        0x01
#define ARM920_922CP_WRITEMASK                              0x80
#define ARM920_922_CP_REG_C0_ID_REG                         0x00
#define ARM920_922_CP_REG_C0_CACHE_TYPE                     0x02
#define ARM920_922_CP_REG_C1_CONTROL                        0x04
#define ARM920_922_CP_REG_C9_DCACHE_L_DOWN                  0x24
#define ARM920_922_CP_REG_C9_ICACHE_L_DOWN                  0x26
#define ARM920_922_CP_REG_C13_PROCESS_ID                    0x34
#define ARM920_922_CP_REG_C15_TEST_STATE                    0x3C
#define ARM920_922_CP_REG_C15_INST_C_INDEX                  0x36
#define ARM920_922_CP_REG_C15_DATA_C_INDEX                  0x7A

#define ARM920_922_CP_REG_C2_WRITE_I_TTB                    0xEEAF0F51   
#define ARM920_922_CP_REG_C2_READ_I_TTB                     0xEEBF0F51 
#define ARM920_922_CP_REG_C2_WRITE_D_TTB                    0xEEAF0F52
#define ARM920_922_CP_REG_C2_READ_D_TTB                     0xEE120F52 
#define ARM920_922_CP_REG_C3_WRITE_I_DAC                    0xEEAF0F71
#define ARM920_922_CP_REG_C3_READ_I_DAC                     0xEEBF0F71
#define ARM920_922_CP_REG_C3_WRITE_D_DAC                    0xEEAF0F72
#define ARM920_922_CP_REG_C3_READ_D_DAC                     0xEE130F10
#define ARM920_922_CP_REG_C5_WRITE_I_FSR                    0xEE050F30
#define ARM920_922_CP_REG_C5_READ_I_FSR                     0xEE150F30
#define ARM920_922_CP_REG_C5_WRITE_D_FSR                    0xEE050F10
#define ARM920_922_CP_REG_C5_READ_D_FSR                     0xEE150F10
#define ARM920_922_CP_REG_C6_WRITE_I_FAR                    0xEE060F30
#define ARM920_922_CP_REG_C6_READ_I_FAR                     0xEE160F30
#define ARM920_922_CP_REG_C6_WRITE_D_FAR                    0xEE060F10
#define ARM920_922_CP_REG_C6_READ_D_FAR                     0xEE160F10
#define ARM920_922_CP_REG_C7_INVALIDATE_ICACHE_ALL          0xEE070F15
#define ARM920_922_CP_REG_C7_INVALIDATE_I_AND_D_CACHE       0xEE070F17
#define ARM920_922_CP_REG_C8_INVALIDATE_ALL_TLBS            0xEE080F17
#define ARM920_922_CP_REG_C8_INVALIDATE_I_TLB               0xEE080F15
#define ARM920_922_CP_REG_C8_INVALIDATE_D_TLB               0xEE080F16
#define ARM920_922_CP_REG_C10_WRITE_I_TLB_LOCKDOWN          0xEE0A0F30
#define ARM920_922_CP_REG_C10_READ_I_TLB_LOCKDOWN           0xEE1A0F30
#define ARM920_922_CP_REG_C10_WRITE_D_TLB_LOCKDOWN          0xEE0A0F10
#define ARM920_922_CP_REG_C10_READ_D_TLB_LOCKDOWN           0xEE1A0F10


#define ARM926_INITIATE_NEW_CP_ACCESS                       0x0001
#define ARM926_CP_REG_C0_ID_REG                             0x0000
#define ARM926_CP_REG_C0_CACHE_TYPE                         0x0100
#define ARM926_CP_REG_C0_TCM_STATUS                         0x0200
#define ARM926_CP_REG_C1_CONTROL                            0x0010
#define ARM926_CP_REG_C2_TTB                                0x0020
#define ARM926_CP_REG_C3_DAC                                0x0030
#define ARM926_CP_REG_C5_DFSR                               0x0050
#define ARM926_CP_REG_C5_IFSR                               0x0150
#define ARM926_CP_REG_C6_FAR                                0x0060
#define ARM926_CP_REG_C9_DCACHE_L_DOWN                      0x0090
#define ARM926_CP_REG_C9_ICACHE_L_DOWN                      0x0190
#define ARM926_CP_REG_C9_DCACHE_TCM                         0x0091
#define ARM926_CP_REG_C9_ICACHE_TCM                         0x0191
#define ARM926_CP_REG_C10_TLB                               0x00A0
#define ARM926_CP_REG_C13_PROCESS_ID                        0x00D0
#define ARM926_CP_ACCESS_WRITE_MASK                         0x8000

#define ARM720_REVERSED_CP_REG_C0_READ_ID_REG               0x11E010EF
#define ARM720_REVERSED_CP_REG_C1_READ_CONTROL_REG          0x11E110EF
#define ARM720_REVERSED_CP_REG_C2_READ_TTB                  0x11E090EF
#define ARM720_REVERSED_CP_REG_C3_READ_DAC                  0x11E190EF
#define ARM720_REVERSED_CP_REG_C5_READ_FSR                  0x11E150EF
#define ARM720_REVERSED_CP_REG_C6_READ_FAR                  0x11E0D0EF
#define ARM720_REVERSED_CP_REG_C13_READ_PID                 0x11E170EF

#define ARM720_REVERSED_CP_REG_C2_WRITE_TTB                 0x11E080EF
#define ARM720_REVERSED_CP_REG_C3_WRITE_DAC                 0x11E180EF
#define ARM720_REVERSED_CP_REG_C5_WRITE_FSR                 0x11E140EF
#define ARM720_REVERSED_CP_REG_C6_WRITE_FAR                 0x11E0C0EF
#define ARM720_REVERSED_CP_REG_C13_WRITE_PID                0x11E160EF
#define ARM720_REVERSED_CP_REG_C1_WRITE_CONTROL_REG         0x11E100EF

#define ARM740_REVERSED_CP_REG_C0_READ_ID_REG               0x11E010EF
#define ARM740_REVERSED_CP_REG_C1_READ_CONTROL_REG          0x11E110EF
#define ARM740_REVERSED_CP_REG_C2_READ_TTB                  0x11E090EF
#define ARM740_REVERSED_CP_REG_C3_READ_DAC                  0x11E190EF
#define ARM740_REVERSED_CP_REG_C5_READ_FSR                  0x11E150EF
#define ARM740_REVERSED_CP_REG_C6_READ_FAR                  0x11E0D0EF

#define ARM740_REVERSED_CP_REG_C2_WRITE_TTB                 0x11E080EF
#define ARM740_REVERSED_CP_REG_C3_WRITE_DAC                 0x11E180EF
#define ARM740_REVERSED_CP_REG_C5_WRITE_FSR                 0x11E140EF
#define ARM740_REVERSED_CP_REG_C6_WRITE_FAR                 0x11E0C0EF
#define ARM740_REVERSED_CP_REG_C1_WRITE_CONTROL_REG         0x11E100EF


#define DCC_WRITE_MASK           0x01
#define DCC_READ_MASK            0x02
#define WRITE_TO_DCC_DATA_REG    0x25

#define ICE_WRITEMASK               0x20

// external variables
extern unsigned char bLedTargetResetAsserted;                                    // flag to signal that MIPS reset is asserted
extern PTyTpaInfo ptyTpaInfo;
//External Variables
extern TyJtagScanConfig tyJtagScanConfig;  



//ARM TAP commands SCAN IR

unsigned long ulARM_JTAG_EXTEST  = 0x0;
unsigned long ulARM_JTAG_SAMPLE  = 0x3;
unsigned long ulARM_JTAG_SCAN_N  = 0x2;
unsigned long ulARM_JTAG_INTEST  = 0xC;
unsigned long ulARM_JTAG_IDCODE  = 0xE;
unsigned long ulARM_JTAG_BYPASS  = 0xF;
unsigned long ulARM_JTAG_RESTART = 0x4;
unsigned long ulARM_JTAG_ITRSEL  = 0x1D;    
unsigned long ulARM_JTAG_HALT    = 0x08;    

//ARM7 Commands SCAN CHAIN 1
unsigned long ulARM7_SAFE_NON_VEC_ADDRESS   = 0x00010000;

unsigned long pulARM7_ARM_NOP_NORMAL[2]     = {0x00000B0E, 0x0};
unsigned long pulARM7_ARM_NOP_CLOCK[2]      = {0x00000B0F, 0x0};

unsigned long pulARM7_LDMIA_R0_R0[2]        = {0x0000122E, 0x1};
unsigned long pulARM7_LDMIA_R0_R1[2]        = {0x8000122E, 0x0};
unsigned long pulARM7_LDMIA_R0_R0R1[2]      = {0x8000122E, 0x1};
unsigned long pulARM7_LDMIA_R0_R0toR2[2]    = {0xC000122E, 0x1};
unsigned long pulARM7_LDMIA_R0_PC[2]        = {0x0002122E, 0x0};
unsigned long pulARM7_LDMIA_R0_R1toR14[2]   = {0xFFFC122E, 0x0};
unsigned long pulARM7_LDMIA_R0_R0toR14[2]   = {0xFFFC122E, 0x1};
unsigned long pulARM7_LDMIA_R0_R8toR14[2]   = {0x01FC122E, 0x0};
unsigned long pulARM7_LDMIA_R0_R13toR14[2]  = {0x000C122E, 0x0};
unsigned long pulARM7_LDMIA_R0_R0toR7_PC[2] = {0xFE02122E, 0x1};


unsigned long pulARM7_STMIA_R0_R1[2]        = {0x8000022E, 0x0};
unsigned long pulARM7_STMIA_R0_R1toR2[2]    = {0xC000022E, 0x0};
unsigned long pulARM7_STMIA_R0_R1toR14[2]   = {0xFFFC022E, 0x0};
unsigned long pulARM7_STMIA_R0_R0toR7_PC[2] = {0xFE02022E, 0x1};
unsigned long pulARM7_STMIA_R0_R0[2]        = {0x0000022E, 0x1};
unsigned long pulARM7_STMIA_R0_R8toR14[2]   = {0x01FC022E, 0x0};
unsigned long pulARM7_STMIA_R0_R13toR14[2]  = {0x000C022E, 0x0};
unsigned long pulARM7_STMIA_R0_PC[2]        = {0x0002022E, 0x0};

unsigned long pulARM7_MSR_CPSRc_R0[2]       = {0x001F090E, 0x0};
unsigned long pulARM7_MSR_SPSRcsxf_R0[2]    = {0x001FED0E, 0x0};
unsigned long pulARM7_MSR_CPSRcsxf_R0[2]    = {0x001FE90E, 0x0};

unsigned long pulARM7_MRS_R0_CPSR[2]        = {0x0001E10E, 0x0};
unsigned long pulARM7_MRS_R0_SPSR[2]        = {0x0001E50E, 0x0};

unsigned long pulARM7_BX_R0[2]              = {0x11FFE90E, 0x0};
unsigned long pulARM7_B_PC_4[2]             = {0xBFFFFEAE, 0x0};
unsigned long pulARM7_B_PC_10[2]            = {0xBFFFFEAE, 0x1};
unsigned long pulARM7_STRB_R0_R1[2]         = {0x0001074E, 0x0};

unsigned long pulARM7_MOV_PC_R0[2]          = {0x001E0B0E, 0x0};

//ARM7 Scan chain1 Thumb instructions
unsigned long pulARM7_THUMB_NOP_NORMAL[2]   = {0x06C406C4, 0x0};
unsigned long pulARM7_THUMB_NOP_CLOCK[2]    = {0x06C406C5, 0x0};
unsigned long pulARM7_THUMB_LDMIA_R0_R0[2]  = {0x00270026, 0x1};
unsigned long pulARM7_THUMB_B_PC_5[2]       = {0x3FCF3FCE, 0x1};
unsigned long pulARM7_THUMB_STR_R0_R0[2]    = {0x000C000C, 0x0};
unsigned long pulARM7_THUMB_MOV_R0_PC[2]    = {0x3CC43CC4, 0x0};
unsigned long pulARM7_THUMB_BX_R0[2]        = {0x01C401C4, 0x0};


unsigned long pulARM7_CHECK_THUMB_WP[2]     = {0x06C406C4, 0x0};
unsigned long pulARM7_CHECK_ARM_WP[2]       = {0x00000B0E, 0x0};


//ARM9 mode change commands SCAN CHAIN 1
unsigned long pulARM7_MODE_USER[2]          = {0x10000000, 0x0};
unsigned long pulARM7_MODE_FIQ[2]           = {0x10000000, 0x1};
unsigned long pulARM7_MODE_IRQ[2]           = {0x90000000, 0x0};
unsigned long pulARM7_MODE_SUPERVISOR[2]    = {0x90000000, 0x1};
unsigned long pulARM7_MODE_ABORT[2]         = {0xD0000000, 0x1};
unsigned long pulARM7_MODE_UNDEFINED[2]     = {0xB0000000, 0x1};



//ARM9 Commands SCAN CHAIN 1
unsigned long pulARM9_STMIA_R0_PC[3]        = {0x00000000, 0x000808B8, 0x0};
unsigned long pulARM9_STMIA_R0_R0toR7_PC[3] = {0x00000000, 0xF80808B8, 0x7};
unsigned long pulARM9_STMIA_R0_R0toR7[3]    = {0x00000000, 0xF80008B8, 0x7};
unsigned long pulARM9_STMIA_R0_R0[3]        = {0x00000000, 0x000008B8, 0x4};
unsigned long pulARM9_STMIA_R0_R0R1[3]      = {0x00000000, 0x000008B8, 0x6};
unsigned long pulARM9_STMIA_R0_R1toR10[3]   = {0x00000000, 0xFF0008B8, 0x3};
unsigned long pulARM9_STMIA_R0_R8toR14[3]   = {0x00000000, 0x07F008B8, 0x0};
unsigned long pulARM9_STMIA_R0_R13toR14[3]  = {0x00000000, 0x003008B8, 0x0};
unsigned long pulARM9_STMIA_R0_R1[3]        = {0x00000000, 0x000008B8, 0x2};
unsigned long pulARM9_STMIA_R0_R1toR2[3]    = {0x00000000, 0x000008B8, 0x3};
unsigned long pulARM9_STMIA_R0_R1toR14[3]   = {0x00000000, 0xFFF008B8, 0x3};

unsigned long pulARM9_ARM_NOP_NORMAL[3]     = {0x00000000, 0x00002C38, 0x0};
unsigned long pulARM9_ARM_NOP_CLOCK[3]      = {0x00000000, 0x00002C3C, 0x0};
unsigned long ulARM9_ARM_NOP_NORMAL = 0x00000587;

unsigned long pulARM9_LDMIA_R0_R1toR14[3]   = {0x00000000, 0xFFF048B8, 0x3};
unsigned long pulARM9_LDMIA_R0_PC[3]        = {0x00000000, 0x000848B8, 0x0};
unsigned long pulARM9_LDMIA_R0_R0[3]        = {0x00000000, 0x000048B8, 0x4};
unsigned long pulARM9_LDMIA_R0_R1[3]        = {0x00000000, 0x000048B8, 0x2};
unsigned long pulARM9_LDMIA_R0_R0R1[3]      = {0x00000000, 0x000048B8, 0x6};
unsigned long pulARM9_LDMIA_R0_R0toR2[3]    = {0x00000000, 0x000048B8, 0x7};
unsigned long pulARM9_LDMIA_R0_R0toR10[3]   = {0x00000000, 0xFF0048B8, 0x7};
unsigned long pulARM9_LDMIA_R0_R0toR14[3]   = {0x00000000, 0xFFF048B8, 0x7};
unsigned long pulARM9_LDMIA_R0_R8toR14[3]   = {0x00000000, 0x07F048B8, 0x0};
unsigned long pulARM9_LDMIA_R0_R13toR14[3]  = {0x00000000, 0x003048B8, 0x0};
unsigned long pulARM9_LDMIA_R0_R0toR7_PC[3] = {0x00000000, 0xF80848B8, 0x7};
unsigned long pulARM9_LDMIA_R0_R0toR7[3]    = {0x00000000, 0xF80048B8, 0x7};

unsigned long pulARM9_MSR_CPSRc_R0[3]       = {0x00000000, 0x007C2438, 0x0};
unsigned long pulARM9_MSR_SPSR_csxf_R0[3]   = {0x00000000, 0x007FB438, 0x0};
unsigned long pulARM9_MSR_CPSR_csxf_R0[3]   = {0x00000000, 0x007FA438, 0x0};

unsigned long pulARM9_MRS_R0_SPSR[3]        = {0x00000000, 0x00079438, 0x0};
unsigned long pulARM9_MRS_R0_CPSR[3]        = {0x00000000, 0x00078438, 0x0};

unsigned long pulARM9_ARM_E28F0001[3]       = {0x00000000, 0x00078A38, 0x4};
unsigned long pulARM9_ARM_BX_R0[3]          = {0x00000000, 0x47FFA438, 0x0};
unsigned long pulARM9_B_PC_3[3]             = {0x00000000, 0xFFFFFAB8, 0x6};//{0x00000000, 0xFFFFFAB8, 0x6}; 
unsigned long pulARM9_B_PC_4[3]             = {0x00000000, 0xFFFFFAB8, 0x2};
unsigned long pulARM9_B_PC_F4[3]            = {0x00000000, 0xFFFFFAB8, 0x1};
unsigned long pulARM9_MOV_PC_R0[3]          = {0x00000000, 0x00782C38, 0x0};
unsigned long pulARM9_STR_R0_R14[3]         = {0x00000000, 0x00038D38, 0x0};
unsigned long pulARM9_STRB_R0_R1[3]         = {0x00000000, 0x00041D38, 0x0};



//ARM9 Thumb instructions SCAN CHAIN 1
unsigned long pulARM9_THUMB_LDMIA_R0_R0[3]  = {0x00000000, 0x009C0098, 0x4};
unsigned long pulARM9_THUMB_NOP_NORMAL[3]   = {0x00000000, 0x1B101B10, 0x0};
unsigned long pulARM9_THUMB_NOP_CLOCK[3]    = {0x00000000, 0x1B101B14, 0x0};
unsigned long pulARM9_THUMB_B_PC_5[3]       = {0x00000000, 0xFF3CFF38, 0x4};
//unsigned long pulARM9_THUMB_B_PC_5_F[3]     = {0x00000000, 0xBF3EBF38, 0x6};
unsigned long pulARM9_THUMB_B_PC_5_F[3]     = {0x00000000, 0xBF39BF38, 0x1};
unsigned long pulARM9_THUMB_STR_R0_R0[3]    = {0x00000000, 0x00300030, 0x0};
unsigned long pulARM9_THUMB_MOV_R0_PC[3]    = {0x00000000, 0xF310F310, 0x0};
unsigned long pulARM9_THUMB_BX_R0[3]        = {0x00000000, 0x07100710, 0x0};


unsigned long pulARM9_CHECK_THUMB_WP[3]     = {0x00000001, 0x36203620, 0x0}; // need to check later
unsigned long pulARM9_CHECK_ARM_WP[3]       = {0x00000001, 0x00005870, 0x0}; // need to check later




unsigned long ulARM9_SET_SAFE_NON_VEC   = 0x00010000;

//ARM9 mode change commands SCAN CHAIN 1
unsigned long pulARM9_MODE_USER[3]          = {0x00000010, 0x00002C38, 0x0};
unsigned long pulARM9_MODE_FIQ[3]           = {0x00000011, 0x00002C38, 0x0};
unsigned long pulARM9_MODE_IRQ[3]           = {0x00000012, 0x00002C38, 0x0};
unsigned long pulARM9_MODE_SUPERVISOR[3]    = {0x00000013, 0x00002C38, 0x0};
unsigned long pulARM9_MODE_ABORT[3]         = {0x00000017, 0x00002C38, 0x0};
unsigned long pulARM9_MODE_UNDEFINED[3]     = {0x0000001B, 0x00002C38, 0x0};

//unsigned long pulARM9ScanChain1PreparedData[3] = {0x0, 0x00002C38, 0x00000000};



#define ARM9ScanChain1PrepareData(var,dat) (var)[0] = (dat); (var)[1] = 0x00002C38;\
                                           (var)[2] = 0x0;

#define ARM9ScanChain1PrepareDataThumb(var,dat) (var)[0] = (dat); (var)[1] = 0x1B101B10;\
                                                (var)[2] = 0x0


//ARM9 Scan Chain 2 
unsigned long pulREAD_DBG_STATUS[2]         = {0x00000000, 0x01};
unsigned long pulREAD_DBG_CONTROL[2]        = {0x00000002, 0x20}; //TEMPORARILY ADDED TO CHECK MEMORY READ
unsigned long pulALL_ZEROS[2]               = {0x00000000, 0x00};
unsigned long pulDISABLE_WP0[2]             = {0x00000000, 0x2C};
unsigned long pulDISABLE_WP1[2]             = {0x00000000, 0x34};
unsigned long pulINTDIS_DBGAK_CTRL[2]       = {0x00000005, 0x20};
unsigned long pulREAD_DCC_CTRL[2]           = {0x00000000, 0x04};
unsigned long pulREAD_DCC_DATA[2]           = {0x00000000, 0x05};

//ARM7 Scan Chain 2 
unsigned long pulITDS_DBGAKRQ_CTRL[2]       = {0x00000007, 0x20};

unsigned char ucgUseSafeNonVectorAddress = 0; //0 means not using safe non vector adddress 1 means use safe non vector address
/*ARM11 instructions*/
unsigned long pulARM11_ARM_MSR_CPSR_R0[2]   =   {0xE129F000,0x0};/*MSR CPSR,R0*/
unsigned long pulARM11_ARM_MSR_SPSR_R0[2]   =   {0xE169F000,0x0};/*MSR SPSR,R0*/
unsigned long pulARM11_ARM_MRS_R0_CPSR[2]   =   {0xE10F0000,0x0};/*MRS R0,CPSR*/
unsigned long pulARM11_ARM_MRS_R0_SPSR[2]   =   {0xE14F0000,0x0};/*MRS R0,SPSR*/

unsigned long pulARM11_ARM_WRITE_BUFFER[2]  =   {0xEE070F9A,0x0};/*MCR p15,0,R0,C7,C10,4*/
unsigned long pulARM11_DATA_SYNC_BARRIER[2] =   {0xEE150E1A,0x0};/* MRC p14,0,R0,c5,c10,0*/

unsigned long pulARM11_ARM_MCR_R0_CP15_CONTROL[2]  =  {0xEE010F10,0x0};    /* MCR P15,0,R0,c1,c0,0 */
unsigned long pulARM11_ARM_MRC_R0_CP15_CONTROL[2]  =  {0xEE110F10,0x0};    /* MRC P15,0,R0,c1,c0,0 */

unsigned long pulARM11_ARM_MCR_R0_CP15_AUX_CTL[2]  =  {0xEE010F30,0x0};    /* MCR P15,0,R0,c1,c0,1 */
unsigned long pulARM11_ARM_MRC_R0_CP15_AUX_CTL[2]  =  {0xEE110F30,0x0};    /* MRC P15,0,R0,c1,c0,1 */

unsigned long pulARM11_ARM_MRC_R0_CACHE_DEBUG[2]   = {0xEEFF0F10,0x0};     /*  Read-MRC p15,7,R0,c15,c0,0  */
unsigned long pulARM11_ARM_MCR_R0_CACHE_DEBUG[2]   = {0xEEEF0F10,0x0};     /*  Write-MCR p15,7,R0,c15,c0,0 */

unsigned long pulARM11_ARM_MRC_R0_CACHE_BEHAV_OVER[2] = {0xEE190F18,0x0};   /* Read- MRC p15,0,R0,c9,c8,0 ARM1176 */
unsigned long pulARM11_ARM_MCR_R0_CACHE_BEHAV_OVER[2] = {0xEE090F18,0x0};   /* Write-MCR p15,0,R0,c9,c8,0 ARM1176 */
unsigned long pulARM11_ARM_MCR_CP_READ_R0[4][2] = {{0xEE100F10,0x0},       /* MRC p15,0,R0,c0,c0,0 */
    {0xEE100F30,0x0},       /* MRC p15,0,R0,c0,c0,1 */
    {0xEE110F10,0x0},       /* MRC p15,0,R0,c1,c0,0 */
    {0xEE110F30,0x0}        /* MRC p15,0,R0,c1,c0,1 */
};  


/*
unsigned long pulARM11_ARM_MRC_DTR_Rd[18][2]=   {{0xE169F000,0x0},//MSR SPSR,R0
                                                 {0xEE108E15,0x0},//MRC P14,0,R8,c0,c5,0
                                                 {0xEE109E15,0x0},//MRC P14,0,R9,c0,c5,0
                                                 {0xEE10AE15,0x0},//MRC P14,0,R10,c0,c5,0
                                                 {0xEE10BE15,0x0},//MRC P14,0,R11,c0,c5,0
                                                 {0xEE10CE15,0x0},//MRC P14,0,R12,c0,c5,0
                                                 {0xEE10DE15,0x0},//MRC P14,0,R13,c0,c5,0
                                                 {0xEE10EE15,0x0},//MRC P14,0,R14,c0,c5,0
                                                 {0xE129F000,0x0},//MSR CPSR,R0*
                                                 {0xEE100E15,0x0},//MRC P14,0,R0,c0,c5,0
                                                 {0xEE101E15,0x0},//MRC P14,0,R1,c0,c5,0
                                                 {0xEE102E15,0x0},//MRC P14,0,R2,c0,c5,0
                                                 {0xEE103E15,0x0},//MRC P14,0,R3,c0,c5,0
                                                 {0xEE104E15,0x0},//MRC P14,0,R4,c0,c5,0
                                                 {0xEE105E15,0x0},//MRC P14,0,R5,c0,c5,0
                                                 {0xEE106E15,0x0},//MRC P14,0,R6,c0,c5,0
                                                 {0xEE107E15,0x0},//MRC P14,0,R7,c0,c5,0
                                                 {0xE1A0F000,0x0}};//MOV PC,R0
*/
unsigned long pulARM11_ARM_MRC_DTR_Rd[15][2]=    {{0xEE100E15,0x0},  /*MRC P14,0,R0,c0,c5,0*/
    {0xEE101E15,0x0},  /*MRC P14,0,R1,c0,c5,0*/
    {0xEE102E15,0x0},  /*MRC P14,0,R2,c0,c5,0*/
    {0xEE103E15,0x0},  /*MRC P14,0,R3,c0,c5,0*/
    {0xEE104E15,0x0},  /*MRC P14,0,R4,c0,c5,0*/
    {0xEE105E15,0x0},  /*MRC P14,0,R5,c0,c5,0*/
    {0xEE106E15,0x0},  /*MRC P14,0,R6,c0,c5,0*/
    {0xEE107E15,0x0},  /*MRC P14,0,R7,c0,c5,0*/
    {0xEE108E15,0x0},  /*MRC P14,0,R8,c0,c5,0*/
    {0xEE109E15,0x0},  /*MRC P14,0,R9,c0,c5,0*/
    {0xEE10AE15,0x0},  /*MRC P14,0,R10,c0,c5,0*/
    {0xEE10BE15,0x0},  /*MRC P14,0,R11,c0,c5,0*/
    {0xEE10CE15,0x0},  /*MRC P14,0,R12,c0,c5,0*/
    {0xEE10DE15,0x0},  /*MRC P14,0,R13,c0,c5,0*/
    {0xEE10EE15,0x0}}; /*MRC P14,0,R14,c0,c5,0*/

unsigned long pulARM11_ARM_MCR_Rd_DTR[15][2]=   {{0xEE000E15,0x0},  /*MCR P14,0,R0,c0,c5,0*/
    {0xEE001E15,0x0},  /*MCR P14,0,R1,c0,c5,0*/ 
    {0xEE002E15,0x0},  /*MCR P14,0,R2,c0,c5,0*/
    {0xEE003E15,0x0},  /*MCR P14,0,R3,c0,c5,0*/
    {0xEE004E15,0x0},  /*MCR P14,0,R4,c0,c5,0*/
    {0xEE005E15,0x0},  /*MCR P14,0,R5,c0,c5,0*/
    {0xEE006E15,0x0},  /*MCR P14,0,R6,c0,c5,0*/
    {0xEE007E15,0x0},  /*MCR P14,0,R7,c0,c5,0*/
    {0xEE008E15,0x0},  /*MCR P14,0,R8,c0,c5,0*/
    {0xEE009E15,0x0},  /*MCR P14,0,R9,c0,c5,0*/
    {0xEE00AE15,0x0},  /*MCR P14,0,R10,c0,c5,0*/
    {0xEE00BE15,0x0},  /*MCR P14,0,R11,c0,c5,0*/
    {0xEE00CE15,0x0},  /*MCR P14,0,R12,c0,c5,0*/
    {0xEE00DE15,0x0},  /*MCR P14,0,R13,c0,c5,0*/
    {0xEE00EE15,0x0}}; /*MCR P14,0,R14,c0,c5,0*/



unsigned long pulARM11_MOV_R0_PC[2] = {0xE1A0000F,0x0};    /* MOV R0,PC */
unsigned long pulARM11_MOV_PC_R0[2] = {0xE1A0F000,0x0};    /* MOV PC,R0 */

unsigned long pulARM11_ARM_STC_R0[2]  =   {0xECA05E01,0x0};     /* STC p14,c5,[R0],#4 */
unsigned long pulARM11_ARM_LDC_R0[2]  =   {0xECB05E01,0x0};     /* LDC p14,c5,[R0],#4 */
unsigned long pulARM11_ARM_LDRH_R1_R0_2[2] = {0xE0D010B2,0x0};  /* LDRH R1,[R0],#2 */
unsigned long pulARM11_ARM_LDRB_R1_R0_1[2] = {0xE4D01001,0x0};  /* LDRB R1,[R0],#1 */

unsigned long pulARM11_ARM_STRH_R1_R0_2[2] = {0xE0C010B2,0x0};  /* STRH R1,[R0],#2*/
unsigned long pulARM11_ARM_STRB_R1_R0_1[2] = {0xE4C01001,0x0};  /* STRB R1,[R0],#1 */

unsigned long pulARM11_ARM_NOP[2]     =   {0xE1A00000,0x0};     /*NOP*/

unsigned long pulARM11_CACHE_CLEAN[5][2] =  {
    {0xEE070F15,0x0},/* MCR p15,0,R0,c7,c5,0  */
    {0xEE070F16,0x0},/* MCR p15,0,R0,c7,c6,0  */
    {0xEE070F1A,0x0},/* MCR p15,0,R0,c7,c10,0 */
    {0xEE070F1E,0x0},/* MCR p15,0,R0,c7,c14,0 */
    {0xEE070F17,0x0} /* MCR p15,0,R0,c7,c7,0  */
};

unsigned long pulCP15ReadInstr[112][2] =    {
    {0xEE100F10,0x0},   /*  Read Only-MRC p15,0,R0,c0,c0,0	*/
    {0xEE100F30,0x0},   /*  Read Only-MRC p15,0,R0,c0,c0,0	*/
    {0xEE100F50,0x0},   /*  Read Only-MRC p15,0,R0,c0,c0,0	*/
    {0xEE100F70,0x0},   /*  Read Only-MRC p15,0,R0,c0,c0,0	*/                                                      
    {0xEE100F11,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,0	*/
    {0xEE100F31,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,1	*/
    {0xEE100F51,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,2	*/
    {0xEE100F71,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,3	*/
    {0xEE100F91,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,4	*/
    {0xEE100FB1,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,5	*/
    {0xEE100FD1,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,6	*/
    {0xEE100FF1,0x0},   /*  Read Only-MRC p15,0,R0,c0,c1,7	*/
    {0xEE100F12,0x0},   /*  Read Only-MRC p15,0,R0,c0,c2,0	*/
    {0xEE100F32,0x0},   /*  Read Only-MRC p15,0,R0,c0,c2,1	*/
    {0xEE100F52,0x0},   /*  Read Only-MRC p15,0,R0,c0,c2,2	*/
    {0xEE100F72,0x0},   /*  Read Only-MRC p15,0,R0,c0,c2,3	*/
    {0xEE100F92,0x0},   /*  Read Only-MRC p15,0,R0,c0,c2,4	*/
    {0xEE100FB2,0x0},   /*  Read Only-MRC p15,0,R0,c0,c2,5	*/
    {0xEE110F10,0x0},   /*  Read-MRC p15,0,R0,c1,c0,0	    */
    {0xEE110F30,0x0},   /*  Read-MRC p15,0,R0,c1,c0,1	    */
    {0xEE110F50,0x0},   /*  Read-MRC p15,0,R0,c1,c0,2	    */
    {0xEE120F10,0x0},   /*  Read-MRC p15,0,R0,c2,c0,0	    */
    {0xEE120F30,0x0},   /*  Read-MRC p15,0,R0,c2,c0,1	    */
    {0xEE120F50,0x0},   /*  Read-MRC p15,0,R0,c2,c0,2	    */
    {0xEE130F10,0x0},   /*  Read-MRC p15,0,R0,c3,c0,0	    */
    {0xEE150F10,0x0},   /*  Read-MRC p15,0,R0,c5,c0,0	    */
    {0xEE150F30,0x0},   /*  Read-MRC p15,0,R0,c5,c0,1	    */
    {0xEE160F10,0x0},   /*  Read-MRC p15,0,R0,c6,c0,0	    */
    {0xEE160F30,0x0},   /*  Read-MRC p15,0,R0,c6,c0,1	    */
    {0xEE170FDA,0x0},   /*  Read Only-MRC P15,0,R0,c7,c10,6	*/
    {0xEE170F9C,0x0},   /*  Read Only -MRC P15,0,R0,c7,c12,4*/
    {0xEE190F10,0x0},   /*  Read-MRC p15,0,R0,c9,c0,0	    */
    {0xEE190F30,0x0},   /*  Read-MRC p15,0,R0,c9,c0,1	    */
    {0xEE190F11,0x0},   /*  Read-MRC p15,0,R0,c9,c1,0	    */
    {0xEE190F31,0x0},   /*  Read-MRC p15,0,R0,c9,c1,1	    */
    {0xEE1A0F10,0x0},   /*  Read-MRC p15,0,R0,c10,c0,0	    */                                                              
    {0xEE1A0F12,0x0},   /*  Read-MRC p15,0,R0,c10,c2,0	    */
    {0xEE1A0F32,0x0},   /*  Read-MRC p15,0,R0,c10,c2,1	    */
    {0xEE1B0F10,0x0},   /*  Read Only-MRC p15,0,R0,c11,c0,0	*/
    {0xEE1B0F30,0x0},   /*  Read Only-MRC p15,0,R0,c11,c0,1	*/
    {0xEE1B0F50,0x0},   /*  Read Only-MRC p15,0,R0,c11,c0,2	*/ 
    {0xEE1B0F70,0x0},   /*  Read Only-MRC p15,0,R0,c11,c0,3	*/
    {0xEE1B0F11,0x0},   /*  Read-MRC p15,0,R0,c11,c1,0	    */
    {0xEE1B0F12,0x0},   /*  Read-MRC p15,0,R0,c11,c2,0	    */
    {0xEE1B0F14,0x0},   /*  Read-MRC p15,0,R0,c11,c4,0	    */
    {0xEE1B0F15,0x0},   /*  Read-MRC p15,0,R0,c11,c5,0	    */
    {0xEE1B0F16,0x0},   /*  Read-MRC p15,0,R0,c11,c6,0      */
    {0xEE1B0F17,0x0},   /*  Read-MRC p15,0,R0,c11,c7,0	    */
    {0xEE1B0F18,0x0},   /*  Read Only-MRC p15,0,R0,c11,c8,0	*/
    {0xEE1B0F1F,0x0},   /*  Read-MRC p15,0,R0,c11,c15,0	    */
    {0xEE1D0F10,0x0},   /*  Read-MRC p15,0,R0,c13,c0,0		*/
    {0xEE1D0F30,0x0},   /*  Read-MRC p15,0,R0,c13,c0,1		*/
    {0xEE1D0F50,0x0},   /*  Read-MRC p15,0,R0,c13,c0,2		*/
    {0xEE1D0F70,0x0},   /*  Read-MRC p15,0,R0,c13,c0,3		*/
    {0xEE1D0F90,0x0},   /*  Read-MRC p15,0,R0,c13,c0,4		*/
    {0xEE1F0F12,0x0},   /*  Read-MRC p15,0,R0,c15,c2,0		*/
    {0xEE1F0F32,0x0},   /*  Read-MRC p15,0,R0,c15,c2,1		*/
    {0xEE1F0F52,0x0},   /*  Read-MRC p15,0,R0,c15,c2,2		*/
    {0xEE1F0F92,0x0},   /*  Read-MRC p15,0,R0,c15,c2,4		*/
    {0xEE1F0F1C,0x0},   /*  Read-MRC p15,0,R0,c15,c12,0	    */
    {0xEE1F0F3C,0x0},   /*  Read-MRC p15,0,R0,c15,c12,1	    */
    {0xEE1F0F5C,0x0},   /*  Read-MRC p15,0,R0,c15,c12,2	    */
    {0xEE1F0F7C,0x0},   /*  Read-MRC p15,0,R0,c15,c12,3	    */
    {0xEE7F0F10,0x0},   /*	Read Only-MRC p15,3,R0,c15,c0,0 */
    {0xEE7F0F30,0x0},   /*	Read Only-MRC p15,3,R0,c15,c0,1 */
    {0xEE7F0F18,0x0},   /*	Read-MRC p15,3,R0,c15,c8,0      */
    {0xEE7F0F38,0x0},   /*	Read-MRC p15,3,R0,c15,c8,1      */
    {0xEE7F0F58,0x0},   /*	Read-MRC p15,3,R0,c15,c8,2      */
    {0xEE7F0F78,0x0},   /*	Read-MRC p15,3,R0,c15,c8,3      */
    {0xEE7F0F98,0x0},   /*	Read-MRC p15,3,R0,c15,c8,4      */
    {0xEE7F0FB8,0x0},   /*	Read-MRC p15,3,R0,c15,c8,5      */
    {0xEE7F0FD8,0x0},   /*	Read-MRC p15,3,R0,c15,c8,6      */
    {0xEE7F0FF8,0x0},   /*	Read-MRC p15,3,R0,c15,c8,7      */
    {0xEE7F0F1A,0x0},   /*	Read-MRC p15,3,R0,c15,c10,0	    */
    {0xEE7F0F3A,0x0},   /*	Read-MRC p15,3,R0,c15,c10,1	    */
    {0xEE7F0F5A,0x0},   /*	Read-MRC p15,3,R0,c15,c10,2	    */
    {0xEE7F0F7A,0x0},   /*	Read-MRC p15,3,R0,c15,c10,3	    */
    {0xEE7F0F9A,0x0},   /*	Read-MRC p15,3,R0,c15,c10,4	    */
    {0xEE7F0FBA,0x0},   /*	Read-MRC p15,3,R0,c15,c10,5	    */
    {0xEE7F0FDA,0x0},   /*	Read-MRC p15,3,R0,c15,c10,6	    */
    {0xEE7F0FFA,0x0},   /*	Read-MRC p15,3,R0,c15,c10,7	    */
    {0xEE7F0F1C,0x0},   /*	Read-MRC p15,3,R0,c15,c12,0	    */
    {0xEE7F0F3C,0x0},   /*	Read-MRC p15,3,R0,c15,c12,1	    */
    {0xEE7F0F5C,0x0},   /*	Read-MRC p15,3,R0,c15,c12,2	    */
    {0xEE7F0F7C,0x0},   /*	Read-MRC p15,3,R0,c15,c12,3	    */
    {0xEE7F0F9C,0x0},   /*	Read-MRC p15,3,R0,c15,c12,4	    */
    {0xEE7F0FBC,0x0},   /*	Read-MRC p15,3,R0,c15,c12,5	    */
    {0xEE7F0FDC,0x0},   /*	Read-MRC p15,3,R0,c15,c12,6	    */
    {0xEE7F0FFC,0x0},   /*	Read-MRC p15,3,R0,c15,c12,7	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,0	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,1	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,2	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,3	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,4	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,5	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,6	    */
    {0xEE7F0F1E,0x0},   /*	Read-MRC p15,3,R0,c15,c14,7	    */
    {0xEEBF0F14,0x0},   /*	Read-MRC p15,5,R0,c15,c4,0		*/                                                      
    {0xEEBF0F34,0x0},   /*	Read-MRC p15,5,R0,c15,c4,1		*/
    {0xEEBF0F15,0x0},   /*	Read-MRC p15,5,R0,c15,c5,0		*/
    {0xEEBF0F35,0x0},   /*	Read-MRC p15,5,R0,c15,c5,1		*/
    {0xEEBF0F55,0x0},   /*	Read-MRC p15,5,R0,c15,c5,2		*/
    {0xEEBF0F16,0x0},   /*	Read Only-MRC p15,5,R0,c15,c6,0 */
    {0xEEBF0F36,0x0},   /*	Read Only-MRC p15,5,R0,c15,c6,1 */
    {0xEEBF0F56,0x0},   /*	Read-MRC p15,5,R0,c15,c6,2		*/
    {0xEEBF0F17,0x0},   /*	Read-MRC p15,5,R0,c15,c7,0		*/
    {0xEEBF0F37,0x0},   /*	Read-MRC p15,5,R0,c15,c7,1		*/
    {0xEEBF0F57,0x0},   /*	Read-MRC p15,5,R0,c15,c7,2		*/
    {0xEEBF0F1E,0x0},   /*	Read-MRC p15,5,R0,c15,c14,0	    */                                                      
    {0xEEBF0F3E,0x0},   /*	Read-MRC p15,5,R0,c15,c14,1	    */
    {0xEEFF0F10,0x0},   /*	Read-MRC p15,7,R0,c15,c0,0		*/                                                                                                              
    {0xEEFF0F00,0x0}    /*	Read-MRC p15,7,R0,c15,c1,0		*/                                                      
};

unsigned long pulCP15WriteInstr[79][2] =    {
    {0xEE010F10,0x0}, /*  Write-MCR p15,0,R0,c1,c0,0      */
    {0xEE010F30,0x0}, /*  Write-MCR p15,0,R0,c1,c0,1      */
    {0xEE010F50,0x0}, /*  Write-MCR p15,0,R0,c1,c0,2      */
    {0xEE020F10,0x0}, /*  Write-MCR p15,0,R0,c2,c0,0      */
    {0xEE020F30,0x0}, /*  Write-MCR p15,0,R0,c2,c0,1      */
    {0xEE020F50,0x0}, /*  Write-MCR p15,0,R0,c2,c0,2      */
    {0xEE030F10,0x0}, /*  Write-MCR p15,0,R0,c3,c0,0      */
    {0xEE050F10,0x0}, /*  Write-MCR p15,0,R0,c5,c0,0      */
    {0xEE050F30,0x0}, /*  Write-MCR p15,0,R0,c5,c0,1      */
    {0xEE060F10,0x0}, /*  Write-MCR p15,0,R0,c6,c0,0      */
    {0xEE060F30,0x0}, /*  Write-MCR p15,0,R0,c6,c0,1      */
    {0xEE090F10,0x0}, /*  Write-MCR p15,0,R0,c9,c0,0      */
    {0xEE090F30,0x0}, /*  Write-MCR p15,0,R0,c9,c0,1      */
    {0xEE090F11,0x0}, /*  Write-MCR p15,0,R0,c9,c1,0      */
    {0xEE090F31,0x0}, /*  Write-MCR p15,0,R0,c9,c1,1      */
    {0xEE0A0F10,0x0}, /*  Write-MCR p15,0,R0,c10,c0,0     */
    {0xEE0A0F12,0x0}, /*  Write-MCR p15,0,R0,c10,c2,0     */    
    {0xEE0A0F32,0x0}, /*  Write-MCR p15,0,R0,c10,c2,1     */    
    {0xEE0B0F11,0x0}, /*  Write-MCR p15,0,R0,c11,c1,0     */            
    {0xEE0B0F12,0x0}, /*  Write-MCR p15,0,R0,c11,c2,0     */                
    {0xEE0B0F14,0x0}, /*  Write-MCR p15,0,R0,c11,c4,0     */                                
    {0xEE0B0F15,0x0}, /*  Write-MCR p15,0,R0,c11,c5,0     */                
    {0xEE0B0F16,0x0}, /*  Write-MCR p15,0,R0,c11,c6,0     */                
    {0xEE0B0F17,0x0}, /*  Write-MCR p15,0,R0,c11,c7,0     */                
    {0xEE0B0F1F,0x0}, /*  Write-MCR p15,0,R0,c11,c15,0    */                        
    {0xEE0D0F10,0x0}, /*  Write-MCR p15,0,R0,c13,c0,0     */                                    
    {0xEE0D0F30,0x0}, /*  Write-MCR p15,0,R0,c13,c0,1     */                                    
    {0xEE0D0F50,0x0}, /*  Write-MCR p15,0,R0,c13,c0,2     */    
    {0xEE0D0F70,0x0}, /*  Write-MCR p15,0,R0,c13,c0,3     */
    {0xEE0D0F90,0x0}, /*  Write-MCR p15,0,R0,c13,c0,4     */
    {0xEE0F0F12,0x0}, /*  Write-MCR p15,0,R0,c15,c2,0     */
    {0xEE0F0F32,0x0}, /*  Write-MCR p15,0,R0,c15,c2,1     */
    {0xEE0F0F52,0x0}, /*  Write-MCR p15,0,R0,c15,c2,2     */
    {0xEE0F0F92,0x0}, /*  Write-MCR p15,0,R0,c15,c2,4     */
    {0xEE0F0F1C,0x0}, /*  Write-MCR p15,0,R0,c15,c12,0    */
    {0xEE0F0F3C,0x0}, /*  Write-MCR p15,0,R0,c15,c12,1    */
    {0xEE0F0F5C,0x0}, /*  Write-MCR p15,0,R0,c15,c12,2    */
    {0xEE0F0F7C,0x0}, /*  Write-MCR p15,0,R0,c15,c12,3    */
    {0xEE6F0F18,0x0}, /*  Write-MCR p15,3,R0,c15,c8,0     */
    {0xEE6F0F38,0x0}, /*  Write-MCR p15,3,R0,c15,c8,1     */
    {0xEE6F0F58,0x0}, /*  Write-MCR p15,3,R0,c15,c8,2     */
    {0xEE6F0F78,0x0}, /*  Write-MCR p15,3,R0,c15,c8,3     */
    {0xEE6F0F98,0x0}, /*  Write-MCR p15,3,R0,c15,c8,4     */
    {0xEE6F0FB8,0x0}, /*  Write-MCR p15,3,R0,c15,c8,5     */
    {0xEE6F0FD8,0x0}, /*  Write-MCR p15,3,R0,c15,c8,6     */
    {0xEE6F0FF8,0x0}, /*  Write-MCR p15,3,R0,c15,c8,7     */
    {0xEE6F0F1A,0x0}, /*  Write-MCR p15,3,R0,c15,c10,0    */
    {0xEE6F0F3A,0x0}, /*  Write-MCR p15,3,R0,c15,c10,1    */
    {0xEE6F0F5A,0x0}, /*  Write-MCR p15,3,R0,c15,c10,2    */
    {0xEE6F0F7A,0x0}, /*  Write-MCR p15,3,R0,c15,c10,3    */
    {0xEE6F0F9A,0x0}, /*  Write-MCR p15,3,R0,c15,c10,4    */
    {0xEE6F0FBA,0x0}, /*  Write-MCR p15,3,R0,c15,c10,5    */
    {0xEE6F0FDA,0x0}, /*  Write-MCR p15,3,R0,c15,c10,6    */
    {0xEE6F0FFA,0x0}, /*  Write-MCR p15,3,R0,c15,c10,7    */
    {0xEE6F0F1C,0x0}, /*  Write-MCR p15,3,R0,c15,c12,0    */
    {0xEE6F0F3C,0x0}, /*  Write-MCR p15,3,R0,c15,c12,1    */
    {0xEE6F0F5C,0x0}, /*  Write-MCR p15,3,R0,c15,c12,2    */
    {0xEE6F0F7C,0x0}, /*  Write-MCR p15,3,R0,c15,c12,3    */
    {0xEE6F0F9C,0x0}, /*  Write-MCR p15,3,R0,c15,c12,4    */
    {0xEE6F0FBC,0x0}, /*  Write-MCR p15,3,R0,c15,c12,5    */
    {0xEE6F0FDC,0x0}, /*  Write-MCR p15,3,R0,c15,c12,6    */
    {0xEE6F0FFC,0x0}, /*  Write-MCR p15,3,R0,c15,c12,7    */
    {0xEE6F0F1E,0x0}, /*  Write-MCR p15,3,R0,c15,c14,0    */
    {0xEE6F0F3E,0x0}, /*  Write-MCR p15,3,R0,c15,c14,1    */
    {0xEE6F0F5E,0x0}, /*  Write-MCR p15,3,R0,c15,c14,2    */
    {0xEE6F0F7E,0x0}, /*  Write-MCR p15,3,R0,c15,c14,3    */
    {0xEE6F0F9E,0x0}, /*  Write-MCR p15,3,R0,c15,c14,4    */
    {0xEE6F0FBE,0x0}, /*  Write-MCR p15,3,R0,c15,c14,6    */
    {0xEE6F0FDE,0x0}, /*  Write-MCR p15,3,R0,c15,c14,5    */
    {0xEE6F0FFE,0x0}, /*  Write-MCR p15,3,R0,c15,c14,7    */
    {0xEEAF0F14,0x0}, /*  Write-MCR p15,5,R0,c15,c4,0     */
    {0xEEAF0F34,0x0}, /*  Write-MCR p15,5,R0,c15,c4,1     */
    {0xEEAF0F55,0x0}, /*  Write-MCR p15,5,R0,c15,c5,2     */
    {0xEEAF0F56,0x0}, /*  Write-MCR p15,5,R0,c15,c6,2     */
    {0xEEAF0F57,0x0}, /*  Write-MCR p15,5,R0,c15,c7,2     */                                                        
    {0xEEAF0F1E,0x0}, /*  Write-MCR p15,5,R0,c15,c14,0    */                                                        
    {0xEEAF0F3E,0x0}, /*  Write-MCR p15,5,R0,c15,c14,1    */
    {0xEEEF0F10,0x0}, /*  Write-MCR p15,7,R0,c15,c0,0     */
    {0xEEEF0F11,0x0}  /*  Write-MCR p15,7,R0,c15,c1,0     */
};
static unsigned long ulTargetType = ISCORE;

//function prototype declaration
static unsigned long ARML_PerformARM9Nops(unsigned char ucCount);
static unsigned long ARML_PerformARM7Nops(unsigned char ucCount);


static unsigned long ARML_ARM9ReadWriteDCC (unsigned char *pucDataToWrite,
                                            unsigned long *pulWriteData,
                                            unsigned char *pucDataWriteSuccessful,
                                            unsigned char *pucDataReadFromCore,
                                            unsigned long *pulReadData);

static unsigned long ARML_ARM7ReadWriteDCC (unsigned char *pucDataToWrite,
                                            unsigned long *pulWriteData,
                                            unsigned char *pucDataWriteSuccessful,
                                            unsigned char *pucDataReadFromCore,
                                            unsigned long *pulReadData);

unsigned long ARML_ARM7ChangeToThumbMode(unsigned long ulReg0, unsigned long ulRegPC);

/****************************************************************************
     Function: ARML_InitializeJtagForArm
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Initialize JTAG engine in FPGA for ARM targets.
Date           Initials    Description
28-May-2007    VH          Initial
****************************************************************************/
void ARML_InitializeJtagForARM(void)
{
    TyJtagTmsSequence tyTmsForIR, tyTmsForDR;
    unsigned long ulValue;
    (void)JtagSetMulticore(0, NULL, NULL, 0, NULL);                         // no MC support, just initialize structures
    // initialize TMS sequences for IR, DR and IRandDR scans
    tyTmsForIR.usPreTmsCount = 5;          tyTmsForIR.usPreTmsBits = 0x06;
    tyTmsForIR.usPostTmsCount = 4;         tyTmsForIR.usPostTmsBits = 0x03;
    tyTmsForDR.usPreTmsCount = 4;          tyTmsForDR.usPreTmsBits = 0x02;
    tyTmsForDR.usPostTmsCount = 4;         tyTmsForDR.usPostTmsBits = 0x03;
    (void)JtagSetTMS(&tyTmsForIR, &tyTmsForDR, &tyTmsForIR, &tyTmsForDR);
    // configure I/O for TPA in FPGA
    put_wvalue(JTAG_TPDIR, 0x0);                                            // disable all outputs (no hazards during mode change)
    us_wait(5);                                                             // wait 5 us
    // first select mode
    ulValue = 0;
    ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK | JTAG_TPA_TDO | JTAG_TPA_RSCK);
    ulValue |= JTAG_TPA_TRST;                                                        
    put_wvalue(JTAG_TPMODE, ulValue);
    // now set output values (except output driver)
    ulValue = 0;
    ulValue |= (JTAG_TPA_RST | JTAG_TPA_TRST);                                                        
    put_wvalue(JTAG_TPOUT, ulValue);
    // enable output drivers
    ulValue = 0;
    ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK);
    ulValue |= (JTAG_TPA_DRIEN | JTAG_TPA_ENTRST | JTAG_TPA_DBGRQ | JTAG_TPA_TRST | JTAG_TPA_RST);
    put_wvalue(JTAG_TPDIR, ulValue);
    ptyTpaInfo->ulSavedTPDIRValue = ulValue;
    us_wait(5);                                                             // wait 5 us
    // driver enable will be enabled later as part of TPA reconnection (when diskware starts)
}

/****************************************************************************
    Function: ARML_PerformARM9Nops
    Engineer: Shameerudheen P T
       Input: unsigned char ucCount - number of NOPS to execute
      Output: unsigned long - error code ERR_xxx
 Description: Performs an ARM NOP on ARM9 core
   Date        Initials    Description
09-Jul-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_PerformARM9Nops(unsigned char ucCount)
{
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;
    //DLOG2(("ARML_PerformARM9Nops"));

    for ( i=0; i < ucCount; i++ )
        {
        ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH, pulARM9_ARM_NOP_NORMAL, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9_ChangeMode
     Engineer: Shameerudheen P T
        Input: ARM9Mode tyARM9Mode - Mode ARM9ModeUser,ARM9ModeFIQ,ARM9ModeIRQ,ARM9ModeSupervisor,
               ARM9ModeAbort,ARM9ModeUndefined
        Output: unsigned long - error code ERR_xxx
  Description: Changes to the specified mode of ARM9 core
   Date        Initials    Description
10-Jul-2007      SPT       Initial
18-Mar-2008      SPT       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
static unsigned long ARML_ARM9_ChangeMode(ARM9Mode tyARM9Mode)
{
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    switch ( tyARM9Mode )
        {
        case ARM9ModeUser:
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_MODE_USER;
            break;
        case ARM9ModeFIQ:
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_MODE_FIQ; 
            break;
        case ARM9ModeIRQ:
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_MODE_IRQ; 
            break;
        case ARM9ModeAbort:
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_MODE_ABORT;
            break;
        case ARM9ModeSupervisor:
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_MODE_SUPERVISOR;
            break;
        case ARM9ModeUndefined:
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_MODE_UNDEFINED;
            break;
        }
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_CPSRc_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    return JtagScanDRMultiple(11, pulDRLength, pulDataToScan, NULL);   
}
/****************************************************************************
    Function: ARML_ARM9SetPCtoSafeNonVectorAddress
    Engineer: Shameerudheen P T
       Input: None
      Output: unsigned long - error code ERR_xxx
 Description: Sets the PC to the safe non vector address
   Date        Initials    Description
11-Jul-2007      SPT       Initial
14-Feb-2008      JCK       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
static unsigned long ARML_ARM9SetPCtoSafeNonVectorAddress(void)
{
    unsigned long pulTDIVal[3];
    unsigned long *pulTempTDIVal;
    unsigned long *pulDataToScan[10];
    unsigned long pulDRLength[10];
    unsigned char ucBuffercount = 0;

    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_PC;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);   
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    return JtagScanDRMultiple(10, pulDRLength, pulDataToScan, NULL);
}

/****************************************************************************
    Function: ARML_ARM9SelectScanChain
    Engineer: Shameerudheen P T
       Input: unsigned long ulChainNumber - number of the scan chain   
      Output: unsigned long - error code ERR_xxx
 Description: Selects scan chain on ARM9 target.
   Date        Initials    Description
12-Jul-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_ARM9SelectScanChain(unsigned long ulChainNumber)
{
    unsigned long pulTemp[1];
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG3(("SelectScanChain %d",ulChainNumber));
    tyJtagScanConfig.ulTmsForIRandDR_IR =  JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
    tyJtagScanConfig.ulTmsForIRandDR_DR =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01


    ulResult = JtagScanIR_DR(&ulARM_JTAG_SCAN_N,ARM9_SCAN_CHAIN_SELECT_REG_LENGTH,&ulChainNumber,pulTemp);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    DLOG3(("Scan Chain Select DR Value = %08x",*pulTemp));
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
    ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL); // INTEST
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);          // TMS parameters for DR scan
    tyJtagScanConfig.ulTmsForIRandDR_IR = tyJtagScanConfig.ulTmsForIR;
    tyJtagScanConfig.ulTmsForIRandDR_DR = tyJtagScanConfig.ulTmsForDR;
    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7SelectScanChain
    Engineer: Shameerudheen P T
       Input: unsigned long ulChainNumber - number of the scan chain
      Output: unsigned long - error code ERR_xxx
 Description: Selects scan chain on ARM7 target.
   Date        Initials    Description
12-Jul-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_ARM7SelectScanChain(unsigned long ulChainNumber)
{
    unsigned long pulTemp[1];
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG3(("SelectScanChain %d",ulChainNumber));
    tyJtagScanConfig.ulTmsForIRandDR_IR =  JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
    tyJtagScanConfig.ulTmsForIRandDR_DR =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01


    ulResult = JtagScanIR_DR(&ulARM_JTAG_SCAN_N,ARM7_SCAN_CHAIN_SELECT_REG_LENGTH,&ulChainNumber,pulTemp);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    DLOG3(("Scan Chain Select DR Value = %08x",*pulTemp));
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
    ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL); // INTEST
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);          // TMS parameters for DR scan
    tyJtagScanConfig.ulTmsForIRandDR_IR = tyJtagScanConfig.ulTmsForIR;
    tyJtagScanConfig.ulTmsForIRandDR_DR = tyJtagScanConfig.ulTmsForDR;
    return ulResult;
}
/*ARM11MODSTATRT*/
/****************************************************************************
    Function: ARML_ARM11SelectScanChain
    Engineer: Amerdas D K
       Input: unsigned long ulChainNumber - number of the scan chain to select
      Output: unsigned long - error code ERR_xxx
 Description: Selects SCREG register and issues the required scan chain number
              into it.
 Date         Initials    Description
05-Aug-2009     ADK       Initial
****************************************************************************/
static unsigned long ARML_ARM11SelectScanChain(unsigned long ulChainNumber)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulTemp[1];                                                    

    unsigned long ulTempIR = tyJtagScanConfig.ulTmsForIRandDR_IR;
    unsigned long ulTempDR = tyJtagScanConfig.ulTmsForIRandDR_DR;
    tyJtagScanConfig.ulTmsForIRandDR_IR =  JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x03);   
    tyJtagScanConfig.ulTmsForIRandDR_DR =  JTAG_SPARAM_TMS_VAL(4, 0x02, 4, 0x03); 

    ulResult = JtagScanIR_DR(&ulARM_JTAG_SCAN_N,ARM11_SCAN_CHAIN_SELECT_REG_LENGTH,&ulChainNumber,pulTemp);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    tyJtagScanConfig.ulTmsForIRandDR_IR = ulTempIR;
    tyJtagScanConfig.ulTmsForIRandDR_DR = ulTempDR;
    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadDSCR
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer to store DSCR value
       Output: unsigned long - error code ERR_xxx 
  Description: Selects the scan chain 1 (DSCR) and reads the content
               Reads theDSCR register.
 Date          Initials    Description
 14-Aug-2009      ADK       Initial
****************************************************************************/
unsigned long ARML_ARM11ReadDSCR(unsigned long*pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH, NULL,&pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulData = pulDataFromScan;

    DLOG3(("ARM11 MSG: ARML_ARM11ReadDSCR:Value 0x%08x", *pulData ));                      

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadPC
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer to store PC value
       Output: unsigned long - error code ERR_xxx 
  Description: R0 is used as a temporary register.
               Issues the instruction to move PC content to R0 and then
               reads R0.
 Date          Initials    Description
 18-Aug-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ReadPC(unsigned long* pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long ulBackUpR0;
    unsigned long pulReadR0;

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITR and issue the Instruction to move PC -> R0*/
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);              
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_MOV_R0_PC, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Read R0 to get the PC content */
    ulResult = ARML_ARM11_Read_R0(&pulReadR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulData = pulReadR0;

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulBackUpR0);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG3(("ARM11 MSG:ARML_ARM11ReadPC 0x%08x",*pulData));

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11WritePC
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer of PC value
       Output: unsigned long - error code ERR_xxx 
  Description: R0 is used as a temporary register.
               Write the desired value in to R0 first.
               Issue instruction to move R0 to PC.
  Date         Initials    Description
  18-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11WritePC(unsigned long* pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long ulBackUpR0;

    DLOG3(("ARM11 MSG:ARML_ARM11WritePC()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write the data in to R0 first */
    ulResult = ARML_ARM11_Write_R0(*pulData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITR and issue the Instruction to move R0 -> PC */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);              
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_MOV_PC_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulBackUpR0);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;

}
/****************************************************************************
     Function: ARML_ARM11_Read_R0
     Engineer: Amerdas D K
        Input: unsigned long* pulData- Data from to R0
       Output: unsigned long - error code ERR_xxx 
  Description: Reads the contents of R0 by issuing instruction
               to move R0 content to DTR and then reding DTR.
  Date         Initials    Description
  12-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11_Read_R0(unsigned long* pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,0x00000000};

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITR and issue the Instruction to move R0 -> DTR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[0],NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue INTEST to read from DTR */
    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Get the Scanned out data */
    *pulData = pulDataFromScan[0];

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_Read_R1
     Engineer: Amerdas D K
        Input: unsigned long* pulData- Data from to R0
       Output: unsigned long - error code ERR_xxx 
  Description: Reads the contents of R0 by issuing instruction
               to move R0 content to DTR and then reding DTR.
  Date         Initials    Description
  12-Aug-2009   ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11_Read_R1(unsigned long* pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,0x00000000};

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITR and issue the Instruction to move R1 -> DTR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[1],NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue INTEST to read from DTR */
    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Get the Scanned out data */
    *pulData = pulDataFromScan[0];

    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM11_Write_R0
     Engineer: Amerdas D K
        Input: unsigned long ulDatatoR0 - Value to be written into R0, 
       Output: unsigned long - error code ERR_xxx 
  Description: Write in to R0 by isuing the instruction to move DTR -> R0.
               the data is placed in DTR before issuing the instruction.
  Date         Initials    Description
  12-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11_Write_R0(unsigned long ulDatatoR0)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2];
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};

    /* Prepare the Data to Scan in */
    pulDataToScan[0] = ulDatatoR0;
    pulDataToScan[1] = 0x00000000;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);  
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select EXTESt and Place the DATA in DTR */
    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue the instruction to move DTR -> R0 */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);              
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_DTR_Rd[0], NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_Write_R1
     Engineer: Amerdas D K
        Input: unsigned long ulDatatoR0 - Value to be written into R0, 
       Output: unsigned long - error code ERR_xxx 
  Description: Write in to R1 by isuing the instruction to move DTR -> R0.
               the data is placed in DTR before issuing the instruction.
  Date         Initials    Description
  12-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11_Write_R1(unsigned long ulDatatoR1)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2];
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};

    /* Prepare the Data to Scan in */
    pulDataToScan[0] = ulDatatoR1;
    pulDataToScan[1] = 0x00000000;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);  
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select EXTESt and Place the DATA in DTR */
    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Issue the instruction to move DTR -> R1 */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);              
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_DTR_Rd[1], NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadCPSR
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer to store CPSR value
       Output: unsigned long - error code ERR_xxx 
  Description: Read the CPSR using Instruction to move CPSR -> R0.
               R0 is a temporary Register
  Date         Initials    Description
  14-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ReadCPSR(unsigned long* pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long pulReadR0;
    unsigned long ulBackUpR0;

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue the instruction to move CPSR -> R0 */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRS_R0_CPSR, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Read R0 to get the CPSR value */
    ulResult = ARML_ARM11_Read_R0(&pulReadR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulData = pulReadR0;

    //DLOG2(("ARM11 MSG:CPSR Read value 0x%08x",*pulData));

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulBackUpR0);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadSPSR
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer to store CPSR value
       Output: unsigned long - error code ERR_xxx 
  Description: Read the SPSR using Instruction to move SPSR -> R0.
               R0 is a temporary Register
 Date          Initials    Description
 18-Aug-2009      ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ReadSPSR(unsigned long* pulData)
{  
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long pulReadR0;
    unsigned long ulBackUpR0;

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue the instruction to move SPSR -> R0 */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRS_R0_SPSR, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Read R0 to get the CPSR value */
    ulResult = ARML_ARM11_Read_R0(&pulReadR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulData = pulReadR0;

    DLOG3(("ARM11 MSG:ARML_ARM11ReadSPSR: 0x%08x",*pulData));

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulBackUpR0);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11WriteCPSR
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer of CPSR value
       Output: unsigned long - error code ERR_xxx 
  Description: Writes CPSR by loading R0 and then 
               using instruction to move R0 to CPSR
  Date         Initials    Description
  14-Aug-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11WriteCPSR(unsigned long* pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long ulBackUpR0;

    DLOG3(("ARM11 MSG:ARML_ARM11WriteCPSR()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the data in R0 */
    ulResult = ARML_ARM11_Write_R0(*pulData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instruction to move R0 -> CPSR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MSR_CPSR_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulBackUpR0);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;

}
/****************************************************************************
     Function: ARML_ARM11WriteSPSR
     Engineer: Amerdas D K
        Input: unsigned long *pulData - Pointer of SPSR value
       Output: unsigned long - error code ERR_xxx 
  Description: Writes SPSR by loading R0 and then 
               using instruction to move R0 to SPSR
  Date         Initials    Description
  18-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11WriteSPSR(unsigned long* pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long ulBackUpR0;

    DLOG3(("ARM11 MSG:ARML_ARM11WriteSPSR()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the data in R0 */
    ulResult = ARML_ARM11_Write_R0(*pulData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instruction to move R0 -> SPSR */
    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MSR_SPSR_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulBackUpR0);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_ChangeMode
     Engineer: Amerdas D K
        Input: ARM11Mode tyARM11Mode - ARM mode
       Output: unsigned long - error code ERR_xxx
  Description: Changes to the specified mode of ARM11 core
               Reads CPSR, modifies and writes back.
  Date         Initials    Description
  12-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11_ChangeMode(ARM11Mode tyARM11Mode)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[16];
    unsigned long ulDataToR0 = 0;
    unsigned long ulCPSRValue;
    unsigned long ulR0Value;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};

    DLOG3(("ARM11 MSG:ARML_ARM11_ChangeMode()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Read the cutrrent CPSR */
    ulResult = ARML_ARM11ReadCPSR(&ulCPSRValue);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Prepare the data according to mode */
    switch ( tyARM11Mode )
        {
        case ARM11ModeUser:
            ulDataToR0    =    ((ulCPSRValue & 0xFFFFFFE0) | ARM_USER_MODE);       
            break;
        case ARM11ModeFIQ:
            ulDataToR0    =    ((ulCPSRValue & 0xFFFFFFE0) | ARM_FIQ_MODE);       
            break;
        case ARM11ModeIRQ:
            ulDataToR0    =    ((ulCPSRValue & 0xFFFFFFE0) | ARM_IRQ_MODE);
            break;
        case ARM11ModeSupervisor:
            ulDataToR0    =    ((ulCPSRValue & 0xFFFFFFE0) | ARM_SPVR_MODE);
            break;
        case ARM11ModeAbort:
            ulDataToR0    =    ((ulCPSRValue & 0xFFFFFFE0) | ARM_ABORT_MODE);
            break;
        case ARM11ModeUndefined:
            ulDataToR0    =    ((ulCPSRValue & 0xFFFFFFE0) | ARM_UNDEF_MODE);
            break;
        default:
            break;        
        }

    /* Write the prepared data in to R0 */
    ulResult = ARML_ARM11_Write_R0(ulDataToR0);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instruction to move R0 -> CPSR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MSR_CPSR_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill the Instruction Ends */
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11WriteMemoryWord
     Engineer: Amerdas D K
        Input: unsigned long ulAddress : Target memory address to Write into.
               unsigned long*pulData   : pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: Writes data(word) to memory,R0 holds the address
  Date         Initials    Description
  13-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11WriteMemoryWord(unsigned long ulAddress,unsigned long *pulData)
{  
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2];
    unsigned long pulDataFromScan[2];
    unsigned long ulR0Value;

    DLOG3(("ARM11 MSG:ARML_ARM11WriteMemoryWord()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITRSEL for issuing instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* Place the instruction to move DTR -> [R0] */
    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_STC_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    /* Issue EXTEST for writing in to DTR;Scan chain 5 */
    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

    /* Prepare data to scan in */
    pulDataToScan[0] = *pulData;
    pulDataToScan[1] = 0x00000000;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* NOP -> ITR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Restore previous settings */ 
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadMemoryWord
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to read from.
               unsigned long*pulData: pointer to store the result
       Output: unsigned long - error code ERR_xxx
  Description: Read word from memory;R0 holds the address.
  Date         Initials    Description
  13-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11ReadMemoryWord(unsigned long ulAddress, unsigned long*pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long ulR0Value;

    DLOG3(("ARM11 MSG:ARML_ARM11ReadMemoryWord()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                    
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITRSEL for issuing instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* Place the instruction to move DTR <- [R0] */
    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_LDC_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    /* Select ITRSEL for issuing instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(4, 0x02, 5, 0x0D);

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Get the scanned out Data */
    *pulData = pulDataFromScan[0];

    /* NOP -> ITR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Restore previous settings */ 
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11WriteMultipleWord
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to Write into.
               unsigned long ulWordCount: Number of words to Write.
               unsigned long*pulData: pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: Writes multiple words to memory;R0 is memory pointer
  Date         Initials    Description
  25-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11WriteMultipleWord(unsigned long ulAddress,unsigned long ulWordCount,unsigned long*pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2];
    unsigned long pulDataFromScan[2];
    unsigned long ulWriteCnt;
    //unsigned long ulR0Value;
    unsigned long ulDSCRValue;

    unsigned long ulTempDR;
    unsigned long ulTempIR;

    ulTempDR = tyJtagScanConfig.ulTmsForDR;
    ulTempIR = tyJtagScanConfig.ulTmsForIR;
    DLOG3(("ARM11 MSG:ARML_ARM11WriteMultipleWord()"));

    /* R0 is going to be used as a temporary Register */
    /*ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if (ulResult != ERR_NO_ERROR)
        return ulResult;*/

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITRSEL for issuing instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* Place the instruction to move DTR -> [R0] */
    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_STC_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

    for ( ulWriteCnt=1;ulWriteCnt<=ulWordCount;ulWriteCnt++ )
        {
        pulDataToScan[0] = *pulData++;
        pulDataToScan[1] = 0x00000000;

        do
            {
            pulDataFromScan[0] = 0x00000000;
            pulDataFromScan[1] = 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulDataToScan, pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        }

    /* NOP -> ITR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Restore previous settings */ 
    //tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    //tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    tyJtagScanConfig.ulTmsForDR = ulTempDR; 
    tyJtagScanConfig.ulTmsForIR = ulTempIR;


    /* Check for data Abort */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH, NULL,&ulDSCRValue);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( (ulDSCRValue & 0xC0) )
        {
        DLOG3(("Data Abort Occured"));
        return ulResult;
        }

    /* Restore R0 */
    /*ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if (ulResult != ERR_NO_ERROR)
        return ulResult;*/

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadMultipleWord
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to read from.
               unsigned long ulWordCount: Number of words to read from memory.
               unsigned long*pulData: pointer to store the result
       Output: unsigned long - error code ERR_xxx
  Description: Reads Multiple words from memory
  Date         Initials    Description
  25-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11ReadMultipleWord(unsigned long ulAddress,unsigned long ulWordCount,unsigned long*pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long ulWriteCnt;
    //unsigned long ulR0Value;
    unsigned long ulTempDR;
    unsigned long ulTempIR;

    ulTempDR = tyJtagScanConfig.ulTmsForDR;
    ulTempIR = tyJtagScanConfig.ulTmsForIR;
    DLOG3(("ARM11 MSG:ARML_ARM11ReadMultipleWord()"));

    /* R0 is going to be used as a temporary Register */
    /*ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if (ulResult != ERR_NO_ERROR)
        return ulResult;*/

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                    
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select DTR as the Scan Chain */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Select ITRSEL for issuing instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* Place the instruction to move DTR <- [R0] */
    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_LDC_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(4, 0x02, 5, 0x0D);

    for ( ulWriteCnt=1;ulWriteCnt<=ulWordCount;ulWriteCnt++ )
        {
        do
            {
            pulDataFromScan[0] = 0x00000000;
            pulDataFromScan[1] = 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern, pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        *pulData++ = pulDataFromScan[0];
        }

    /* NOP -> ITR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Restore previous settings */ 
    //tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    //tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    tyJtagScanConfig.ulTmsForDR = ulTempDR; 
    tyJtagScanConfig.ulTmsForIR = ulTempIR;

    /* Restore R0 */
    /*ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if (ulResult != ERR_NO_ERROR)
        return ulResult;*/

    //DLOG2(("ARM11 MSG:ulTempDR 0x%08x",ulTempDR));
    //DLOG2(("ARM11 MSG:ulTempDR 0x%08x",ulTempIR));

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadMemoryHalfword
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to read from.
               unsigned long*pulData: pointer to store the result
       Output: unsigned long - error code ERR_xxx
  Description: Read half words from memory;
               R0 is memory pointer;
               R1 is for getting data after instruction execution.
  Date         Initials    Description
  13-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11ReadMemoryHalfword(unsigned long ulAddress,unsigned long*pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long ulR0Value;
    unsigned long ulR1Value;


    DLOG3(("ARM11 MSG:ARML_ARM11ReadMemoryHalfword()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* R1 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R1(&ulR1Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                    
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue the instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_LDRH_R1_R0_2, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill instruction ends*/
    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Read  data from R1 */
    ulResult = ARML_ARM11_Read_R1(pulData);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Restore R1 */
    ulResult = ARML_ARM11_Write_R1(ulR1Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11Read_MemoryByte
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to read from.
               unsigned long*pulData: pointer to store the result
        Output:unsigned long - error code ERR_xxx
  Description: Read Byte from memory
  Date         Initials    Description
  13-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11Read_MemoryByte(unsigned long ulAddress,unsigned long*pulData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long ulR0Value,ulR1Value;


    DLOG3(("ARM11 MSG:ARML_ARM11Read_MemoryByte()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* R1 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R1(&ulR1Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue the instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_LDRB_R1_R0_1, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Wait untill instruction ends*/
    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Read  data from R1 */
    ulResult = ARML_ARM11_Read_R1(pulData);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Restore R1 */
    ulResult = ARML_ARM11_Write_R1(ulR1Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11Write_MemoryByte
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to Write In to.
               unsigned long*pulData: pointer to Incoming Data.
       Output: unsigned long - error code ERR_xxx
  Description: Write Byte to memory
  Date         Initials    Description
  13-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11Write_MemoryByte(unsigned long ulAddress,unsigned char ucWriteData)
{ 
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long ulR0Value;
    unsigned long ulR1Value;


    DLOG3(("ARM11 MSG:ARML_ARM11Write_MemoryByte()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* R1 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R1(&ulR1Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* place data in R1 */
    ulResult = ARML_ARM11_Write_R1((unsigned long)ucWriteData);                   
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* issue instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_STRB_R1_R0_1, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* NOP -> ITR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT)!= ARM11_DTR_READY_BIT );

    /* Restore previous settings */ 
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Restore R1 */
    ulResult = ARML_ARM11_Write_R1(ulR1Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11Write_MemoryHalfWord
     Engineer: Amerdas D K
        Input: unsigned long ulAddress: Target memory address to Write in to.
               unsigned long*pulData: pointer to Incoming Data.
        Output:unsigned long - error code ERR_xxx
  Description: Read words from memory
  Date        Initials    Description
  13-Aug-2009    ADK         Initial
****************************************************************************/
unsigned long ARML_ARM11Write_MemoryHalfWord(unsigned long ulAddress,unsigned short ucWriteData)
{   
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long ulR0Value;
    unsigned long ulR1Value;


    DLOG3(("ARM11 MSG:ARML_ARM11Write_MemoryHalfWord()"));

    /* R0 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* R1 is going to be used as a temporary Register */
    ulResult = ARML_ARM11_Read_R1(&ulR1Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Place the address in R0 */
    ulResult = ARML_ARM11_Write_R0(ulAddress);                     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* place data in R1 */
    ulResult = ARML_ARM11_Write_R1((unsigned long)ucWriteData);                   
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* issue instruction */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_STRH_R1_R0_2, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* NOP -> ITR */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

    /* Restore previous settings */ 
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    /* Restore R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Restore R1 */
    ulResult = ARML_ARM11_Write_R1(ulR1Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;

}

/****************************************************************************
     Function: ARML_ARM11WriteMultipleBlock
     Engineer: Amerdas D K
        Input: unsigned long ulAddress:    Target memory address to read from.
               unsigned long ulBlockCount: Number of Blocks to write.
               unsigned long*pulData:      Pointer to Incoming data
       Output: unsigned long - error code ERR_xxx
  Description: Read words from memory
   Date        Initials    Description
 25-Aug-2009    ADK         Initial
****************************************************************************/
/* This function works;but is not used;replaced by ARML_ARM11WriteMultipleBlock_ReallyFast */
unsigned long ARML_ARM11WriteMultipleBlock(unsigned long ulAddress,unsigned long ulBlockCount,unsigned long*pulData)
{ 
    unsigned long ulResult = ERR_NO_ERROR;
/*    unsigned long ulLoopCnt;
      unsigned long ulBlockWordSize = 14;
*/

    //DLOG3(("ARM11 MSG:ARML_ARM11WriteMultipleBlock()"));
/*
    for (ulLoopCnt=1;ulLoopCnt<=ulBlockCount;ulLoopCnt++)
        {
        ulResult = ARML_ARM11WriteMultipleWord(ulAddress,ulBlockWordSize,pulData);
        if (ulResult != ERR_NO_ERROR)
            return ulResult;

        pulData = pulData + ulBlockWordSize;
        ulAddress += (ulBlockWordSize*4);
        }
*/
    ulResult = ARML_ARM11WriteMultipleWord(ulAddress,(ulBlockCount*14),pulData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadMultipleBlock
     Engineer: Amerdas D K
        Input: unsigned long ulAddress:    Target memory address to read from.
               unsigned long ulBlockCount: Number of Blocks to Read From.
               unsigned long*pulData:      Pointer to Incoming data
       Output: unsigned long - error code ERR_xxx
  Description: Read words from memory
   Date        Initials    Description
 25-Aug-2009    ADK         Initial
****************************************************************************/
/* This function works;but not called;replaced by ARML_ARM11ReadMultipleBlock_ReallyFast */
unsigned long ARML_ARM11ReadMultipleBlock(unsigned long ulAddress,unsigned long ulBlockCount,unsigned long*pulData)
{ 
    unsigned long ulResult = ERR_NO_ERROR;
/*    unsigned long ulLoopCnt;
      unsigned long ulBlockWordSize = 14;
*/
    DLOG3(("ARM11 MSG:ARML_ARM11ReadMultipleBlock()"));
/*   
    for (ulLoopCnt=1;ulLoopCnt<=ulBlockCount;ulLoopCnt++)
    {
        ulResult = ARML_ARM11ReadMultipleWord(ulAddress,ulBlockWordSize,pulData);
        if (ulResult != ERR_NO_ERROR)
            return ulResult;

        pulData = pulData + ulBlockWordSize;
        ulAddress += (ulBlockWordSize*4);
    }
*/
    ulResult = ARML_ARM11ReadMultipleWord(ulAddress,(ulBlockCount*14),pulData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11StatusProc
     Engineer: Amerdas D K  
        Input: pucCoreExecuting: pointer to store Result Data
               pucThumbMode:Thumb mode or not
               pucWatchPoint:
       Output: unsigned long - error code ERR_xxx 
  Description: Reads the status of the processor.
 Date           Initials    Description
 14-Aug-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11StatusProc(unsigned char* pucCoreExecuting ,unsigned char* pucThumbMode,unsigned char* pucWatchPoint)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulStatusDSCR;
    unsigned long ulStatusCPSR;

    DLOG3(("ARM11 MSG:ARML_ARM11StatusProc()"));

    *pucCoreExecuting = TRUE;
    *pucWatchPoint = FALSE;
    *pucThumbMode = FALSE;

    ulResult = ARML_ARM11ReadDSCR(&ulStatusDSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ulStatusDSCR & ARM11_DSCR_CORE_HALTED )
        *pucCoreExecuting = FALSE;

    if ( ((ulStatusDSCR & ARM11_DSCR_ENTRY_FIELD_MASK) >> 2) == ARM11_WATCH_POINT )
        *pucWatchPoint = TRUE;

    ulStatusDSCR |=ARM11_DSCR_INST_EXEC_ENABL_BIT;

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH,&ulStatusDSCR,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM11ReadCPSR(&ulStatusCPSR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ulStatusCPSR & ARM11_CPSR_T_BIT )
        *pucThumbMode = TRUE;
/*
    ulStatusDSCR &= ~ARM11_DSCR_INST_EXEC_ENABL_BIT;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
    if (ulResult != ERR_NO_ERROR)
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if (ulResult != ERR_NO_ERROR)
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH,&ulStatusDSCR,NULL);
    if (ulResult != ERR_NO_ERROR)
        return ulResult;
*/
    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_Enter_Debug_State
     Engineer: Amerdas D K  
        Input: pulResultData: pointer to store Result Data
       Output: unsigned long - error code ERR_xxx 
  Description: Enter Debug State.
 Date           Initials    Description
 14-Aug-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11EnterDebugState(unsigned char ucARM11Type,unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,0x00000000};
    unsigned long ulStatusDSCR;
    unsigned long ulStatusDSCR_2;
    unsigned long ulWDTR;
    unsigned long ulRDTR;
    unsigned long ulR0Value;
    unsigned long ulCacheDbgCtrlReg;

    DLOG3(("ARM11 MSG:ARML_ARM11EnterDebugState()"));

    ulResult = ARML_ARM11ReadDSCR(&ulStatusDSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulStatusDSCR |= ARM11_DSCR_HALT_DEBUG_MODE_BIT; 

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH,&ulStatusDSCR,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM11ReadDSCR(&ulStatusDSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_HALT, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH, pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[0]& ARM11_DSCR_CORE_HALTED) != ARM11_DSCR_CORE_HALTED );


    ulStatusDSCR_2 = pulDataFromScan[0];

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);     
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;


    pulDataFromScan[0] = 0x00000000;
    pulDataFromScan[1] = 0x00000000;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( (pulDataFromScan[1]& ARM11_wDTR_VALID_BIT) )
        {

        ulWDTR = pulDataFromScan[0];
        *pulData++ = ARM11_wDTR_VALID; 
        *pulData++ = ulWDTR;

        }
    else
        {
        *pulData++ = 0;             
        *pulData++ = 0;
        }

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulStatusDSCR_2 |= ARM11_DSCR_INST_EXEC_ENABL_BIT;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH,&ulStatusDSCR_2,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucARM11Type == ARM1136 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_WRITE_BUFFER,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else if ( ucARM11Type == ARM1176 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_DATA_SYNC_BARRIER,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    do
        {
        do
            {
            pulDataFromScan[0] = 0x00000000;
            pulDataFromScan[0] = 0x00000000;

            ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        }
    while ( (pulDataFromScan[0] & ARM11_DSCR_STICKY_IMPRECISE_BIT) );


    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );


    ulResult = ARML_ARM11ReadDSCR(&ulStatusDSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulData++ = ulR0Value;

    if ( ulStatusDSCR_2 & ARM11_rDTR_FULL )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_DTR_Rd[0], NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0] = 0x00000000;
            pulDataFromScan[0] = 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        ulResult = ARML_ARM11_Read_R0(&ulRDTR);                      
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        *pulData++ = ARM11_rDTR_VALID;    
        *pulData++ = ulRDTR;
        }
    else
        {

        *pulData++ = NULL;                
        *pulData++ = NULL;
        }
/*---------------------------------------------------------------*/
    /* Read Cache Debug Control register */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucARM11Type == ARM1136 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_R0_CACHE_DEBUG, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else if ( ucARM11Type == ARM1176 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_R0_CACHE_BEHAV_OVER, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Get the result from R0 */
    ulResult = ARML_ARM11_Read_R0(&ulCacheDbgCtrlReg);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG2(("ARM11 MSG: ARML_ARM11EnterDebugState:ulCacheDbgCtrlReg: 0x%08x",ulCacheDbgCtrlReg));  // Remove    

    /* Send to save the value for future write back in Leave Debug State */
    *pulData = ulCacheDbgCtrlReg;

#if 0
    /* Disable I-cache fill;D-Cache fill & Enable Write through in Cache Debug Control Register */
    ulCacheDbgCtrlReg |=  (ARM11_CACHE_DEBUG_CONTROL_DL | 
                           ARM11_CACHE_DEBUG_CONTROL_IL | 
                           ARM11_CACHE_DEBUG_CONTROL_WT);

    DLOG2(("ARM11 MSG: ARML_ARM11EnterDebugState:ulCacheDbgCtrlReg Modified: 0x%08x",ulCacheDbgCtrlReg));  // Remove    

    /* Place the modified data in R0 */
    ulResult= ARML_ARM11_Write_R0(ulCacheDbgCtrlReg); 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write back the modified value */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucARM11Type == ARM1136 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CACHE_DEBUG, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else if ( ucARM11Type == ARM1176 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CACHE_BEHAV_OVER, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_NOP,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );
#endif
/*---------------------------------------------------------------*/
    /* Write Back R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadCDCR
     Engineer: Amerdas D K  
        Input: pulResultData: pointer to send Data (Cache debug Contro register)
       Output: unsigned long - error code ERR_xxx 
  Description: Reads the cache debug control register
 Date           Initials    Description
 30-Oct-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ReadCDCR(unsigned char ucARM11Type,unsigned long* pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long ulR0Value;
    unsigned long ulCacheDbgCtrlReg;


    /* R0 is going to be used temporarly */
    ulResult = ARML_ARM11_Read_R0(&ulR0Value);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Read Cache Debug Control register */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucARM11Type == ARM1136 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_R0_CACHE_DEBUG, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else if ( ucARM11Type == ARM1176 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_R0_CACHE_BEHAV_OVER, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Get the result from R0 */
    ulResult = ARML_ARM11_Read_R0(&ulCacheDbgCtrlReg);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Send to save the value for future write back in Leave Debug State */
    *pulData = ulCacheDbgCtrlReg;


    DLOG2(("ARM11 MSG: ARML_ARM11ReadCDCR:ulCacheDbgCtrlReg: 0x%08x",ulCacheDbgCtrlReg));  // Remove    

    /* Disable I-cache fill;D-Cache fill & Enable Write through in Cache Debug Control Register */
    ulCacheDbgCtrlReg |=  (ARM11_CACHE_DEBUG_CONTROL_DL | 
                           ARM11_CACHE_DEBUG_CONTROL_IL | 
                           ARM11_CACHE_DEBUG_CONTROL_WT);

    DLOG2(("ARM11 MSG: ARML_ARM11ReadCDCR:ulCacheDbgCtrlRegModified: 0x%08x",ulCacheDbgCtrlReg));  // Remove    

    /* Place the modified data in R0 */
    ulResult= ARML_ARM11_Write_R0(ulCacheDbgCtrlReg); 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write back the modified value */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucARM11Type == ARM1136 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CACHE_DEBUG, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else if ( ucARM11Type == ARM1176 )
        {
        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CACHE_BEHAV_OVER, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_NOP,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Write Back R0 */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_LeaveDebugState
     Engineer: Amerdas D K  
        Input: pulResultData: pointer to store Result Data
       Output: unsigned long - error code ERR_xxx 
  Description: Leaving  Debug State.
               Restore wDTR,R0 and rDTR
 Date           Initials    Description
 17-Aug-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11_LeaveDebugState(unsigned char *bThumbMode,
                                         unsigned char ucARM11Type,
                                         unsigned long *pulData)
//unsigned long ARML_ARM11_LeaveDebugState(unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,0x00000000};
    unsigned long pulDataFromScan[2];
    unsigned long pulDataToScan[2];
    unsigned long ulStatusDSCR;

    unsigned long pulARM11_ARM_BX_R0[2] = {0xE12FFF10,0x0};

/*  
    unsigned long ulPCAddress = 0x8;
    unsigned long pulPCValue;
   
    unsigned long pulARM11_ARM_BLX[2] = {0xFAFFFFFE,0x0};

    if((*(pulData+2)& 0x2) == 0x2)
        {
        DLOG2(("ARM11 MSG:Half Word aligned"));
        pulARM11_ARM_BLX[0] = 0xFBFFFFFE;
        }
        
    DLOG3(("ARM11 MSG:ARML_ARM11_LeaveDebugState()"));
*/


    /* Execute BX R0 -> To go to Thumb Mode:R0 must hold PC value */
    if ( *bThumbMode == 1 )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_BX_R0, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        /* Write Back R0 */
        ulResult= ARML_ARM11_Write_R0(*(pulData + 2));
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
/*
        pulPCValue = 0x00000000;

        ulResult = ARML_ARM11_ReadSC7(ulPCAddress,&pulPCValue);                
        if (ulResult != ERR_NO_ERROR)
            return ulResult;

        DLOG2(("ARM11 MSG: ARML_ARM11_LeaveDebugState:pulPCValueread back: 0x%08x",pulPCValue));  // Remove    
*/                                                                                                // 
        }
    else
        {
        /* ---------------------------------------------------------*/

        DLOG2(("ARM11 MSG: ARML_ARM11_LeaveDebugState:*(pulData + 5): 0x%08x",*(pulData + 5))); // Remove    
        /* Modification for "enable write through" behaviour */
        *(pulData + 5) |= ARM11_CACHE_DEBUG_CONTROL_WT;  

        /* Place the Cache Debug Control Register value in R0 */
        ulResult= ARML_ARM11_Write_R0(*(pulData + 5)); 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        /* Write back the modified value */
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( ucARM11Type == ARM1136 )
            {
            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CACHE_DEBUG, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        else if ( ucARM11Type == ARM1176 )
            {
            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CACHE_BEHAV_OVER, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_NOP,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        /* ---------------------------------------------------------*/
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_Rd_DTR[0], NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;
            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( *pulData )                                /* wDTR data valid?  */
            {
            pulData++;
            ulResult= ARML_ARM11_Write_R0(*pulData);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;


            ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_MCR_Rd_DTR[0], NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            do
                {
                pulDataFromScan[0]= 0x00000000;
                pulDataFromScan[1]= 0x00000000;

                ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                }
            while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

            pulData++;
            }
        else
            {
            pulData++;
            pulData++;
            }

        ulResult= ARML_ARM11_Write_R0(*pulData); 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulData++;

        ulResult = ARML_ARM11ReadDSCR(&ulStatusDSCR);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulStatusDSCR &= (~ARM11_DSCR_INST_EXEC_ENABL_BIT );

        ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;


        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH,&ulStatusDSCR,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( *pulData )
            {
            pulData++;
            ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            pulDataToScan[0]=*pulData;
            pulDataToScan[1]=0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        }
    /* Issue RESTART instruction To TAP */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 6, 0x0D);

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Restore previous TMS setting */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_ReadSC7
     Engineer: Amerdas D K
        Input: unsigned long ulAddr - Address of SC7 Register from which data is to be read
               unsigned long *pulData  - Pointer to store data.
       Output: unsigned long - error code ERR_xxx 
  Description: Read from a specific  SC7 resister.-{BCR/BVR/WCR/WVR/PC/VCR}.
   Date        Initials    Description
28-Aug-2009      ADK        Initial
****************************************************************************/
unsigned long  ARML_ARM11_ReadSC7 (unsigned long ulAddr,unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2]={0};
    unsigned long pulShiftinPattern[2];
    unsigned long pulDataFromScan[2];

    /* Prepare data for SC7 */
    pulDataToScan[0]=  0x00000000;                       
    pulDataToScan[1]= ((ulAddr << 1) & 0x000000FE);     

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_7);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Scan in the Data */
    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_7_LENGTH, pulDataToScan,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulShiftinPattern[0] = 0x00000000;
        pulShiftinPattern[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_7_LENGTH, pulShiftinPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[0]& ARM11_SCAN_CHAIN_7_READY)!=ARM11_SCAN_CHAIN_7_READY );

    /* Get the result */
    *pulData = (((pulDataFromScan[1]& 0x00000001)<<31) | ((pulDataFromScan[0]>>1) & 0x7FFFFFFF));

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11_WriteSC7
     Engineer: Amerdas D K
        Input:unsigned long ulAddr - Address of SC7 Register to which data is to be written
               unsigned long *pulData  - Pointer holding data to be written.
       Output: unsigned long - error code ERR_xxx 
  Description: Write to  a specific SC7 register.-{BCR/BVR/WCR/WVR/PC/VCR}.
   Date        Initials    Description
28-Aug-2009      ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11_WriteSC7 (unsigned long ulAddr,unsigned long *pulData)
{  
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftinPattern[2];
    unsigned long pulDataToScan[2] = {0};
    unsigned long pulDataFromScan[2];


    DLOG3(("ARM11 MSG:ARML_ARM11_WriteSC7()"));

    pulDataToScan[0]= (((*pulData <<1) & 0xFFFFFFFE) | 0x00000001);
    pulDataToScan[1]= (((ulAddr << 1) & 0xFFFFFFFE)  |(*pulData >>31));

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_7);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_7_LENGTH, pulDataToScan,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    do
        {
        pulShiftinPattern[0] = 0x00000000;
        pulShiftinPattern[1] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_7_LENGTH, pulShiftinPattern, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[0]& ARM11_SCAN_CHAIN_7_READY)!=ARM11_SCAN_CHAIN_7_READY );

    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM11ExeInstEnable
     Engineer: Amerdas D K  
        Input: pulData: pointer to store Result Data
       Output: unsigned long - error code ERR_xxx 
  Description: Enables Execution of Instructions in degug state
  Date         Initials    Description
  17-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ExeInstEnable(unsigned long* pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulStatus;

    DLOG3(("ARM11 MSG:ARML_ARM11ExeInstEnable()"));

    ulResult = ARML_ARM11ReadDSCR(&ulStatus);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulStatus |= ARM11_DSCR_INST_EXEC_ENABL_BIT;

    ulResult = JtagScanIR(&ulARM_JTAG_EXTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH,&ulStatus,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM11ReadDSCR(&ulStatus);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9ExecuteProc
     Engineer: Amerdas D K
        Input: Pointer to incoming Data
       Output: unsigned long - error code ERR_xxx 
  Description: Writes Scan Chain 7 registers of ARM11:All BCRs and BVRs
   Date        Initials    Description
11-AUG-2009      ADK      Initial
****************************************************************************/
unsigned long ARML_ARM11SetupExecuteProc(unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulLoopCnt;

    DLOG3(("ARM11 MSG:ARML_ARM11SetupExecuteProc()"));

    for ( ulLoopCnt=0;ulLoopCnt<6;ulLoopCnt++ )
        {
        ulResult = ARML_ARM11_WriteSC7((ARM11_BVR_BASE + ulLoopCnt),pulData++);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = ARML_ARM11_WriteSC7((ARM11_BCR_BASE + ulLoopCnt),pulData++);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadRegisters
     Engineer: Amerdas D K  
        Input: pulData: pointer to store Result Data
       Output: unsigned long - error code ERR_xxx 
  Description: Read ARM11 Registers;all modes.
  Date         Initials    Description
  17-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ReadRegisters(unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulLoopCnt;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,0x00000000};
    unsigned long ulCPSRValue;

    DLOG2(("ARM11 MSG:ARML_ARM11ReadRegisters()"));

    /* Read the Current CPSR */
    ulResult = ARML_ARM11ReadCPSR(&ulCPSRValue);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG2(("ARM11 MSG:CPSR Read value 0x%08x",ulCPSRValue));

    /* Select Scan Chain 5 (DTR) */
    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instructions to move Registers R0:R7 -> DTR:Unbanked registers*/
    for ( ulLoopCnt=0;ulLoopCnt<=7;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];

        //       DLOG3(("ARM11 MSG:ARML_ARM11ReadRegisters:Register,value0x%08x  0x%08x",ulLoopCnt,*pulData));

        pulData++;
        }
    /* Read the PC */
    ulResult = ARML_ARM11ReadPC(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //   DLOG3(("ARM11 MSG:PC  0x%08x",*pulData));

    pulData++;

    /* Read the CPSR */
    ulResult = ARML_ARM11ReadCPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //   DLOG3(("ARM11 MSG:CPSR  0x%08x",*pulData));

    /* Change mode to USER */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeUser);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instructions to move Registers R8:14 -> DTR:USER*/
    for ( ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];
        //      DLOG3(("ARM11 MSG: ARM11ModeUser:Register,data ->0x%08x,0x%08x",ulLoopCnt,*pulData));     // Remove
        pulData++;
        }

    /* Change mode to FIQ */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeFIQ);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instructions to move Registers R8:R14 -> DTR:FIQ */
    for ( ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];

        //      DLOG3(("ARM11 MSG: ARM11ModeFIQ:Register,data ->0x%08x,0x%08x",ulLoopCnt,*pulData));     // Remove

        pulData++;
        }

    /* Read the SPSR:FIQ */
    ulResult = ARML_ARM11ReadSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //   DLOG3(("ARM11 MSG: ARM11ModeFIQ:SPSR 0x%08x",*pulData));

    /* Change mode to IRQ */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeIRQ);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;
    /* Issue instructions to move Registers R13 & R14 -> DTR:IRQ */
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];

        //       DLOG3(("ARM11 MSG: ARM11ModeIRQ:Register,data ->0x%08x,0x%08x",ulLoopCnt,*pulData));     // Remove

        pulData++;
        }

    /* Read the SPSR:IRQ*/
    ulResult = ARML_ARM11ReadSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //   DLOG3(("ARM11 MSG: ARM11ModeIRQ:SPSR 0x%08x",*pulData));

    /* Change mode to SUPERVISOR */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeSupervisor);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    /* Issue instructions to move Registers R13 & R14->DTR:SUPERVISOR */
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];

        //      DLOG3(("ARM11 MSG: ARM11ModeSupervisor:Register,data ->0x%08x,0x%08x",ulLoopCnt,*pulData));     // Remove

        pulData++;
        }

    /* Read the SPSR:SUPERVISOR */
    ulResult = ARML_ARM11ReadSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //   DLOG3(("ARM11 MSG: ARM11ModeSupervisor:SPSR 0x%08x",*pulData));

    /* Change mode to ABORT */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeAbort);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;
    /* Issue instructions to move Registers R13 & R14 -> DTR:ABORT */
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];

        //      DLOG3(("ARM11 MSG: ARM11ModeAbort:Register,data ->0x%08x,0x%08x",ulLoopCnt,*pulData));     // Remove

        pulData++;
        }
    /* Read the SPSR:ABORT */
    ulResult = ARML_ARM11ReadSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //   DLOG3(("ARM11 MSG: ARM11ModeAbort:SPSR 0x%08x",*pulData));

    /* Change mode to UNDEFINED*/
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeUndefined);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;
    /* Issue instructions to move Registers R13 & R14->DTR:UNDEFINED*/
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MCR_Rd_DTR[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        /* Get the scanned out values */
        *pulData = pulDataFromScan[0];

        //       DLOG3(("ARM11 MSG: ARM11ModeUndefined:Register,data ->0x%08x,0x%08x",ulLoopCnt,*pulData));     // Remove

        pulData++;
        }

    /* Read the SPSR:UNDEFINED*/
    ulResult = ARML_ARM11ReadSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //  DLOG3(("ARM11 MSG: ARM11ModeUndefined:SPSR 0x%08x",*pulData));

    /* Restore CPSR */
    ulResult = ARML_ARM11WriteCPSR(&ulCPSRValue);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;  
}
/****************************************************************************
     Function: ARML_ARM11WriteRegisters
     Engineer: Amerdas D K  
        Input: pulData: pointer to incoming Data
       Output: unsigned long - error code ERR_xxx 
  Description: Read back ARM11 Registers.
 Date           Initials    Description
 17-Aug-2009     ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11WriteRegisters(unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulShiftInPattern[2]={0x00000000,0x00000000};
    unsigned long ulLoopCnt;

    DLOG3(("ARM11 MSG: Came to function ARML_ARM11WriteRegisters"));    // Remove

    /* Change mode to FIQ*/
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeFIQ);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Write SPSR:FIQ */
    ulResult = ARML_ARM11WriteSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Issue instructions to move Registers R8:R14 <- DTR:FIQ*/
    for ( ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                     
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        pulData++;
        }

    /* Change mode to IRQ*/
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeIRQ);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Write SPSR:IRQ */
    ulResult = ARML_ARM11WriteSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    /* Issue instructions to move Registers R13 & R14 <- DTR:IRQ*/
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                     
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            }while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );
        pulData++;
        }

    /* Change mode to SUPERVISOR*/
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeSupervisor);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Write SPSR:SUPERVISOR */
    ulResult = ARML_ARM11WriteSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    /* Issue instructions to move Registers R13 & R14 <- DTR:SUPERVISOR*/
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                     
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);              
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( !(pulDataFromScan[1]& 0x1) );

        pulData++;
        }

    /* Change mode to ABORT*/
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeAbort);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write SPSR:ABORT */
    ulResult = ARML_ARM11WriteSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;
    /* Issue instructions to move Registers R13 & R14 <- DTR:SUPERVISOR*/
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                     
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        pulData++;
        }

    /* Change mode to UNDEFINED */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeUndefined);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    /* Write SPSR:UNDEFINED */
    ulResult = ARML_ARM11WriteSPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    /* Issue instructions to move Registers R13 & R14 <- DTR:UNDEFINED*/
    for ( ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                     
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        pulData++;
        }
    /* Change mode to USER */
    ulResult = ARML_ARM11_ChangeMode(ARM11ModeUser);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Issue instructions to move Registers R8 & R14 <- DTR:USER*/
    for ( ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);                     
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        pulData++;
        }
    /* Write CPSR */
    ulResult = ARML_ARM11WriteCPSR(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulData++;

    /* Issue instructions to move Registers R0 & R7 <- DTR:USER*/
    for ( ulLoopCnt=0;ulLoopCnt<=7;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulData,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_MRC_DTR_Rd[ulLoopCnt],NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH, pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }while ( (pulDataFromScan[1]& ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT );

        pulData++;
        }

    /* Write PC */
    ulResult = ARML_ARM11WritePC(pulData);             
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;  
}


/****************************************************************************
     Function: ARML_ARM1136WriteCPReg
     Engineer: Amerdas D K  
        Input: pulData: pointer to incoming Data
       Output: unsigned long - error code ERR_xxx 
  Description: Read ARM11 CoProcessor Registers.
  Date         Initials    Description
  14-Oct-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11WriteCPReg(unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long ulBackUpR0;

    /* R0 is going to be used as a temporary register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG2(("ARM11 MSG: ARML_ARM1136WriteCPReg:*(pulData + 2) ->0x%08x",*(pulData + 2)));     // Remove

    /* Place the value in R0 :Control register*/
    ulResult = ARML_ARM11_Write_R0(*(pulData + 2));
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CP15_CONTROL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    DLOG2(("ARM11 MSG: ARML_ARM1136WriteCPReg:*(pulData + 3) ->0x%08x",*(pulData + 3)));     // Remove

    /* Place the value in R0:Auxilliary Control Register */
    ulResult = ARML_ARM11_Write_R0(*(pulData + 3));
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CP15_AUX_CTL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Write Back the R0 Value */
    ulResult= ARML_ARM11_Write_R0(ulBackUpR0); 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;  
}
/****************************************************************************
     Function: ARML_ARM1136ReadCPReg
     Engineer: Amerdas D K  
        Input: pulData: pointer to resultData
       Output: unsigned long - error code ERR_xxx 
  Description: Read ARM11 CoProcessor Registers.
  Date         Initials    Description
  21-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11ReadCPReg(unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataFromScan[2];
    unsigned long pulReadR0;
    unsigned long ulLoopCnt;
    unsigned long ulBackUpR0;

    /* R0 is going to be used as a temporary register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( ulLoopCnt=0;ulLoopCnt<=3;ulLoopCnt++ )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_CP_READ_R0[ulLoopCnt], NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0] = 0x00000000;
            pulDataFromScan[1] = 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        ulResult = ARML_ARM11_Read_R0(&pulReadR0);                      
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        *pulData++ = pulReadR0;
        }

    ulResult= ARML_ARM11_Write_R0(ulBackUpR0); 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;

}
/****************************************************************************
     Function: ARML_ARM1136
     Engineer: Amerdas D K  
        Input: pulData: pointer to resultData
       Output: unsigned long - error code ERR_xxx 
  Description: Read ARM11 CoProcessor Registers.
  Date         Initials    Description
  21-Aug-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARM11InvalidateCleanCache(void)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long ulControlReg;
    //unsigned long ulControlRegTemp;
    unsigned long ulLoopCnt;
    unsigned long ulBackUpR0;

    DLOG3(("ARM11 MSG: ARML_ARM1136InvalidateCleanCache()"));

    /* R0 is going to be used as a temporary register */
    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Read Control register */
    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MRC_R0_CP15_CONTROL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    /* Get the result from R0 */
    ulResult = ARML_ARM11_Read_R0(&ulControlReg);                      
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG2(("ARM11 MSG: ARML_ARM1136InvalidateCleanCache:ulControlRegTemp: 0x%08x",ulControlReg)); 

#if 0
    /* keep the Value */
    ulControlReg = ulControlRegTemp;

    /* Disable I-cache,D-Cache,Flow predection and MMU;adjust vector location */
    ulControlRegTemp &=  (~(ARM11_CONTROL_REG_M_BIT  | ARM11_CONTROL_REG_DC_BIT |                                   
                            ARM11_CONTROL_REG_Z_BIT  | ARM11_CONTROL_REG_IC_BIT |    
                            ARM11_CONTROL_REG_V_BIT ));

    ulControlRegTemp &=  (~(ARM11_CONTROL_REG_M_BIT | 
                            ARM11_CONTROL_REG_Z_BIT | 
                            ARM11_CONTROL_REG_V_BIT));

    /* Place the modified value in R0 */
    ulResult = ARML_ARM11_Write_R0(ulControlRegTemp);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MCR_R0_CP15_CONTROL, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0] = 0x00000000;
        pulDataFromScan[0] = 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

#endif

    /* Place the data in R0 -0x00000000 for invalidating & flushing caches */
    ulResult= ARML_ARM11_Write_R0(0x00000000); 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Check cache Enabled */
    if ( ulControlReg & ARM11_CONTROL_REG_IC_BIT )
        {
        /* Invalidate Entire Instruction Cache.Also flushes the branch target cache.*/
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_CACHE_CLEAN[0], NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulARM11_ARM_NOP,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        }
    if ( ulControlReg & ARM11_CONTROL_REG_DC_BIT )
        {
        /* Invalidate Entire Data Cache */
        /* Clean Entire Data Cache */
        /* Clean and Invalidate Entire Data Cache */

        for ( ulLoopCnt=1;ulLoopCnt<=3;ulLoopCnt++ )
            {
            ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_CACHE_CLEAN[ulLoopCnt], NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            do
                {
                pulDataFromScan[0]= 0x00000000;
                pulDataFromScan[1]= 0x00000000;

                ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                }
            while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );
            }
        }
    if ( ulControlReg & (ARM11_CONTROL_REG_IC_BIT | ARM11_CONTROL_REG_DC_BIT) )
        {
        /* Invalidate Both Caches */
        ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_CACHE_CLEAN[4], NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

        }

    /* Restore R0 */
    ulResult= ARML_ARM11_Write_R0(ulBackUpR0); 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;

}

/****************************************************************************
     Function: ARML_ARM9CheckDataAbort
     Engineer: Amerdas D K
        Input: unsigned char *pucReadData - pointer to Return Status    
       Output: unsigned long - error code ERR_xxx
  Description: Checks the DSCR for data aborts
   Date        Initials    Description
7-Sep-2009      ADK       Initial
***************************************************************************/
unsigned long ARML_ARM11CheckDataAbort(unsigned char *pucReadData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulDSCRValue;

    ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DSCR_LENGTH, NULL,&ulDSCRValue);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( (ulDSCRValue & 0xC0) )
        {
        *pucReadData = 1;   
        }
    else
        {
        *pucReadData = 0;   
        }

    return ulResult;
}

/****************************************************************************

     Function: ARML_ARM11ReadWriteDCC
     Engineer: Amerdas D K
        Input: unsigned char *pucDataToWrite - Is there Data to Write, 
               unsigned long *pulWriteData - the Data to write
               unsigned char *pucDataWriteSuccessful - Return is Write successful, 
               unsigned char *pucDataReadFromCore - return is there data from core, 
               unsigned long *pulReadData - Return Data from core
       Output: unsigned long - error code ERR_xxx
  Description: Read and Write to Debug communication Channel
   Date        Initials    Description
11-Sep-2009      ADK      Initial

****************************************************************************/

unsigned long ARML_ARM11ReadWriteDCC (unsigned char *pucDataToWrite,
                                      unsigned long *pulWriteData,
                                      unsigned char *pucDataWriteSuccessful,
                                      unsigned char *pucDataReadFromCore,
                                      unsigned long *pulReadData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long pulDataToScan[2];   
    unsigned long pulDSCR;

    /* Read the DSCR */
    ulResult = ARML_ARM11ReadDSCR(&pulDSCR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Make the flags False */
    *pucDataWriteSuccessful = FALSE;
    *pucDataReadFromCore    = FALSE;

    /* Check whether to write : Write the data and make the flag true */
    if ( *pucDataToWrite == TRUE )
        {
        ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);    
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_EXTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulDataToScan[0]= *pulWriteData;
        pulDataToScan[1]= 0x00000000;

        do
            {
            pulDataFromScan[0]= 0x00000000;
            pulDataFromScan[1]= 0x00000000;

            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulDataToScan,pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            }while ( (pulDataFromScan[1]& ARM11_rDTR_RETRY_BIT) != ARM11_rDTR_RETRY_BIT );     //nRetry flag

        *pucDataWriteSuccessful = TRUE;
        }
    else if ( (pulDSCR & ARM11_wDTR_FULL)== ARM11_wDTR_FULL )   /* Check whether the core has written data */
        {
        /* If so,Read the Data */    
        ulResult = ARML_ARM11SelectScanChain(ARM11_SCAN_CHAIN_DTR);    
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_INTEST,NULL);             
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_DTR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( (pulDataFromScan[1] & ARM11_wDTR_VALID_BIT) == ARM11_wDTR_VALID_BIT )
            {
            *pulReadData = pulDataFromScan[0];
            *pucDataReadFromCore    = TRUE;
            }
        else
            {
            *pucDataReadFromCore    = FALSE;
            }
        }
    return ulResult;

}
/****************************************************************************

     Function: ARML_ARM11ChangeModeToARMMode
     Engineer: Amerdas D K
       Input: unsigned char ucWatchPoint - input (how the core entered into debug mode)
              unsigned char * pucOffsetNeeded - Pointer to return whether the core stopped on a non word aligned address  
      Output: unsigned long - error code ERR_xxx
 Description: Change from Thumb mode to ARM mode
   Date        Initials    Description
19-Oct-2009      ADK       Initial

****************************************************************************/

unsigned long ARML_ARM11ChangeModeToARMMode (unsigned char ucWatchPoint, unsigned char * pucOffsetNeeded)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    unsigned long ulPCAddress = 0x8;
    unsigned long pulPCValue;
    unsigned long pulPCvalue1;
    unsigned long ulR0Value;
    //unsigned long pulARM11_THUMB_BLX[2] = {0xEFCCF7FF,0x0};
    unsigned long pulARM11_ARM_BX_R0[2] = {0xE12FFF10,0x0};

    unsigned long ulCPSR;                                   // remove

    *pucOffsetNeeded = FALSE;

    /* Read PC for back Up */
    ulResult = ARML_ARM11_ReadSC7(ulPCAddress,&pulPCValue);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Read R0 for back up */
    ulResult = ARML_ARM11_Read_R0 (&ulR0Value);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write R0 -> 0*/
    ulResult = ARML_ARM11_Write_R0 (0x00000000);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulPCvalue1 = (pulPCValue & 0xFFFFFFFE);

    /* Ignore the Last bit:for thumb this bit is '1' and for arm '0' */
    DLOG2(("ARM11 MSG: ARML_ARM11ChangeModeToARMMode:pulPCValue: 0x%08x",pulPCValue));     // Remove

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_BX_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

#if 0
    /* Adjust PC */
    if ( (pulPCValue & 0x00000003) == 0 )/* If D1:D0 = 0:0 -> ARM Instruction*/
        {
        pulPCValue = (pulPCValue & 0xFFFFFFFE);
        pulPCValue -= 0x08;      
        DLOG2(("ARM11 MSG: ARML_ARM11ChangeModeToARMMode:ARM 0x%08x",pulPCValue));     // Remove
        }
    else if ( (pulPCValue & 0x00000001) == 0x1 )/* If D0 = 0 -> THUMB Instruction*/
        {
        pulPCValue = (pulPCValue & 0xFFFFFFFE);
        pulPCValue -= 0x04;
        DLOG2(("ARM11 MSG: ARML_ARM11ChangeModeToARMMode:THUMB 0x%08x",pulPCValue));     // Remove
        }
#endif

    pulPCValue = (pulPCValue & 0xFFFFFFFE);
    pulPCValue -= 0x8;
    DLOG2(("ARM11 MSG: ARML_ARM11ChangeModeToARMMode:THUMB 0x%08x",pulPCValue));     // Remove

/*
   if (ucWatchPoint)
      pulPCValue -= 0x08;
   else
      pulPCValue -= 0x04;
*/

    if ( (pulPCvalue1 & 0x3) != 0 )
        {                          /* If the PC is at a half word aligned address then it is 
                                      changed to the previous word aligned address */
        pulPCValue -= 0x2;
        *pucOffsetNeeded = TRUE;
        }

    DLOG2(("ARM11 MSG: ARML_ARM11ChangeModeToARMMode:pulPCValue 0x%08x",pulPCValue));     // Remove

    ulResult = ARML_ARM11ReadCPSR(&ulCPSR);                                                // Remove          
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write back the R0 Value */
    ulResult = ARML_ARM11_Write_R0(ulR0Value);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    /* Write back the Modified PC value*/
    ulResult = ARML_ARM11WritePC(&pulPCValue);                
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ulResult;
}
/****************************************************************************

     Function: ARML_ARM11ChangeToThumbMode
     Engineer: Amerdas D K
        Input: unsigned long ulReg0 - R0 register Value
               unsigned long ulRegPC - Program counter value      
       Output: unsigned long - error code ERR_xxx
  Description: Change from Thumb mode to ARM mode
   Date        Initials    Description
19-Oct-2009      ADK       Initial

****************************************************************************/

unsigned long ARML_ARM11ChangeToThumbMode (unsigned long ulReg0, unsigned long ulRegPC)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulShiftInPattern[2]={0x00000000,00000000};
    unsigned long pulDataFromScan[2];
    //unsigned long ulBackUpR0;
    //unsigned long ulBackUpPC;

    unsigned long pulARM11_ARM_MOV_R0_0[2] = {0xE3A00001,0x0};
    unsigned long pulARM11_ARM_BX_R0[2] = {0xE12FFF10,0x0};

    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_MOV_R0_0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );



    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_BX_R0, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    do
        {
        pulDataFromScan[0]= 0x00000000;
        pulDataFromScan[1]= 0x00000000;

        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulShiftInPattern,pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadSelectedCPReg
     Engineer: Amerdas D K  
        Input: pulData: pointer to resultData
       Output: unsigned long - error code ERR_xxx 
  Description: Read ARM11 CoProcessor Registers.
  Date         Initials    Description
  08-Dec-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARMReadSelectedCPReg(unsigned long ulProcessor,
                                        unsigned long *pulARMFamily,
                                        unsigned long *pulOpcode_Address,
                                        unsigned long *pulReadData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2]  = {0x00000000,0x00000000};
    unsigned long pulDataFromScan[2]= {0x00000000,0x00000000};
    unsigned short usCount;
    unsigned long pulReadR0;                                
    unsigned long ulBackUpR0;

    DLOG3(("Family:0x%08x\n",*pulARMFamily));
    DLOG3(("Coprocessor:Opcode/address:0x%08x\n",*pulOpcode_Address));

    switch ( ulProcessor )
        {
        case ARM11:
            {
                if ( (*pulARMFamily == ARM1136)||(*pulARMFamily == ARM1156)||(*pulARMFamily == ARM1176) ) /* For ARM1136,ARM1156,ARM1176 */
                    {
                    /* Opcode comes from RDI */
                    pulDataToScan[0] = *pulOpcode_Address;
                    pulDataToScan[1] = 0x00000000;

                    /* R0 is going to be used as a temporary register */
                    ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    /* Select ITR for issuing Instruction */
                    ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL,NULL);                 
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    /* Issue the instruction */
                    ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH,pulDataToScan, NULL);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    /* Issue the NOP instruction */
                    do
                        {
                        pulDataFromScan[0] = 0x00000000;
                        pulDataFromScan[1] = 0x00000000;

                        ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
                        if ( ulResult != ERR_NO_ERROR )
                            return ulResult;
                        }
                    while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

                    /* Result will be in R0 */
                    ulResult = ARML_ARM11_Read_R0(&pulReadR0);                      
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    /* Send the result */
                    *pulReadData = pulReadR0;

                    /* Write Back R0 */
                    ulResult= ARML_ARM11_Write_R0(ulBackUpR0); 
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;
                    }
            }
            break;

        case ARM9:
            {
                if ( (*pulARMFamily == ARM920)||(*pulARMFamily == ARM922) ) /* For ARM920,ARM922*/
                    {
                    /* Address comes from RDI*/
                    pulDataToScan[0] = ARM920_922CP_PHYSICAL_ACCESS;
                    pulDataToScan[1] = (*pulOpcode_Address << 1);

                    /*  Select Scan Chain 15 */
                    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulDataToScan, NULL);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulDataToScan, pulDataFromScan);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    pulDataFromScan[0] = (pulDataFromScan[0] >> 1);
                    *pulReadData = (pulDataFromScan[0] | ((pulDataFromScan[1] & 0x00000001) << 31));

                    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG); 
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;
                    }
                else if ( (*pulARMFamily == ARM940)||(*pulARMFamily == ARM946)||(*pulARMFamily == ARM966)||(*pulARMFamily == ARM968) )
                    {   /* ARM940,ARM946,ARM966,ARM968 */
                    /* CP15 Address comes from RDI */

                    pulDataToScan[0] = 0x00000000;
                    pulDataToScan[1] = *pulOpcode_Address;

                    DLOG3(("Scan Chain lower 32:0x%08x\n",pulDataToScan[0]));
                    DLOG3(("Scan Chain Upper 32:0x%08x\n",pulDataToScan[1]));

                    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulDataToScan, NULL);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulDataToScan, pulDataFromScan);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    *pulReadData = pulDataFromScan[0];

                    DLOG3(("Scan Out Data:0x%08x\n",pulDataFromScan[0]));

                    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG); 
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    }
                else if ( *pulARMFamily == ARM926 ) /* ARM926 */
                    {
                    pulDataToScan[0] = 0x0;
                    pulDataToScan[1] = ((*pulOpcode_Address << 1) + ARM926_INITIATE_NEW_CP_ACCESS);

                    DLOG3(("Scan Chain lower 32:0x%08x\n",pulDataToScan[0]));
                    DLOG3(("Scan Chain Upper 32:0x%08x\n",pulDataToScan[1]));

                    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulDataToScan, NULL);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    pulDataToScan[0] = 0x0;
                    pulDataToScan[1] = 0x0;

                    usCount = 0;

                    do
                        {
                        ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulDataToScan, pulDataFromScan);
                        if ( ulResult != ERR_NO_ERROR )
                            return ulResult;

                        usCount++;

                        if ( usCount >= MAX_WAIT_CYCLES )
                            {
                            return ERR_ARM_CP926_ACCESS_TIME_OUT;
                            }
                        }
                    while ( (pulDataFromScan[1] & 0x1) != 0x1 );

                    *pulReadData = pulDataFromScan[0];

                    DLOG3(("Scan Out Data:0x%08x\n",pulDataFromScan[0]));

                    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG); 
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;
                    }
            }
            break;

        case CORTEXA:
            {            
                ulResult = CortexA_ReadCoProcessor(pulOpcode_Address,pulReadData); 
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
            }
            break;
        default:
            ulResult = ERR_ARM_INVALID_PROCESSOR;
            break;
        }        
    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11WriteSelectedCPReg
     Engineer: Amerdas D K  
        Input: pulData: pointer to resultData
       Output: unsigned long - error code ERR_xxx 
  Description: Read ARM11 CoProcessor Registers.
  Date         Initials    Description
  08-Dec-2009    ADK        Initial
****************************************************************************/
unsigned long ARML_ARMWriteSelectedCPReg(unsigned long ulProcessor,
                                         unsigned long *pulARMFamily,
                                         unsigned long *pulOpcode_Address,
                                         unsigned long *pulWriteData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulDataToScan[2]  = {0x00000000,0x00000000};
    unsigned long pulDataFromScan[2]= {0x00000000,0x00000000};
    unsigned short usCount;
    unsigned long ulBackUpR0;

    DLOG2(("ARML_ARMWriteSelectedCPReg: Arm Family = 0x%0x8, "));

    switch ( ulProcessor )
        {
        case ARM11: /* Common for ARM1136, ARM1156 and ARM1176 */
            pulDataToScan[0] = *pulOpcode_Address;
            pulDataToScan[1] = 0x00000000;

            /* R0 is going to be used as a temporary register */
            ulResult = ARML_ARM11_Read_R0(&ulBackUpR0);                      
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            /* Place the value in R0 :Control register*/
            ulResult = ARML_ARM11_Write_R0(*pulWriteData);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            /* Select ITR for issuing Instruction */
            ulResult = JtagScanIR(&ulARM_JTAG_ITRSEL, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            /* Issue the instruction */
            ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            /* Issue the NOP instruction */
            do
                {
                pulDataFromScan[0] = 0x00000000;
                pulDataFromScan[0] = 0x00000000;

                ulResult = JtagScanDR(ARM11_SCAN_CHAIN_ITR_LENGTH, pulARM11_ARM_NOP, pulDataFromScan);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                }
            while ( (pulDataFromScan[1]& ARM11_ITR_READY_BIT) != ARM11_ITR_READY_BIT );

            /* Write Back the R0 Value */
            ulResult= ARML_ARM11_Write_R0(ulBackUpR0); 
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            break;

        case ARM9:
            if ( (*pulARMFamily == ARM920)||(*pulARMFamily == ARM922) ) /* For ARM920,ARM922 */
                {
                pulDataToScan[0] = ((*pulWriteData << 1) + ARM920_922CP_PHYSICAL_ACCESS);
                pulDataToScan[1]= (*pulOpcode_Address + ARM920_922CP_WRITEMASK);

                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulDataToScan, NULL);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG); 
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                }
            else if ( (*pulARMFamily == ARM940)||(*pulARMFamily == ARM946)||(*pulARMFamily == ARM966)||(*pulARMFamily == ARM968) )
                {   /* ARM940,ARM946,ARM966 */
                    /* CP15 Address comes from RDI */
                pulDataToScan[0] = *pulWriteData;
                pulDataToScan[1] = (*pulOpcode_Address + ARM940_946CP_WRITEMASK);

                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulDataToScan, NULL);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG); 
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                }
            else if ( *pulARMFamily == ARM926 ) /* ARM926 */
                {
                pulDataToScan[0] = *pulWriteData;
                pulDataToScan[1] = ((*pulOpcode_Address << 1) + ARM926_CP_ACCESS_WRITE_MASK + ARM926_INITIATE_NEW_CP_ACCESS);

                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulDataToScan, NULL);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                pulDataToScan[0] = 0x0;
                pulDataToScan[1] = 0x0;

                usCount = 0;
                do
                    {
                    ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulDataToScan, pulDataFromScan);
                    if ( ulResult != ERR_NO_ERROR )
                        return ulResult;

                    usCount++;

                    if ( usCount >= MAX_WAIT_CYCLES )
                        {
                        return ERR_ARM_CP926_ACCESS_TIME_OUT;
                        }
                    }
                while ( (pulDataFromScan[1] & 0x1) != 0x1 );
                }
            break;
        case CORTEXA:
            ulResult = CortexA_WriteCoProcessor(pulOpcode_Address,*pulWriteData); 
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            break;

        default:
            ulResult = ERR_ARM_INVALID_PROCESSOR;
            break;
        }

    return ulResult; 
}

/****************************************************************************

Function: ARML_BringToRTIFromTLR
Engineer: Shameerudheen P T
   Input: none
  Output: unsigned long - error code ERR_xxx
Description: Bringing TAP state machine to Run Test Idle state.
Date        Initials    Description
10-Aug-2007      SPT       Initial
09-Jan-2007      SPT       Corrected TMS Value Reassignment
****************************************************************************/
unsigned long ARML_BringToRTIFromTLR(void)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulReturnValue;
    unsigned long ulOldValue;
    DLOG2(("ARML_BringToRTIFromTLR"));
    ulOldValue = tyJtagScanConfig.ulTmsForIR;
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(5, 0x06, 5, 0x0D);
    ulResult = JtagScanIR(&ulARM_JTAG_BYPASS, &ulReturnValue); // IDCODE
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    //tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 5, 0x0D);
    tyJtagScanConfig.ulTmsForIR = ulOldValue;
    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM9WaitForSystemSpeed
    Engineer: Shameerudheen P T
       Input: None
      Output: unsigned long - error code ERR_xxx
 Description: this function wait upto the time, the core return from a system
              speed operation.
   Date        Initials    Description
11-Jul-2007    SPT         Initial
14-Feb-2008    JCK         Included FPGA JTAG 16 Buffers feature      
****************************************************************************/
static unsigned long ARML_ARM9WaitForSystemSpeed(void)
{
    unsigned long pulReadData[0x2];
    unsigned long usCount;
    unsigned long ulResult;

    unsigned long *pulDataToScan[2];
    unsigned long pulDRLength[2];
    unsigned char ucBuffercount = 0;
    unsigned long *pulDataFromScan[2];

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    usCount = 0x0;

    do
        {

        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulReadData;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        usCount++;
        DLOG3(("Status Value 0x%08x",pulReadData[0]));
//      DLOG2(("Count 0x%08x",usCount));
        if ( usCount >= MAX_WAIT_CYCLES )
            {
            DLOG2(("Count Reached"));
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
        }  
    while ( (pulReadData[0] & ARM_DEBUG_MODE) != ARM_DEBUG_MODE );
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9ReadCPSR
     Engineer: Shameerudheen PT
        Input: unsigned long *pulCPSR - Pointer for returning CPCR Value
       Output: unsigned long - error code ERR_xxx
  Description: Reads the CPSR from the core
   Date        Initials    Description
13-Jul-2007      SPT       Initial
19-Mar-2008      SPT       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
static unsigned long ARML_ARM9ReadCPSR(unsigned long *pulCPSR)
{  
    unsigned long ulResult;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long *pulDataFromScan[16];
    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_CPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulTempTDOVal = pulTDOVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulCPSR = pulTDOVal[0];
    return ulResult;
}

/****************************************************************************
     Function: ARML_JtagRestart
     Engineer: Shameerudheen P T
        Input: Nil
       Output: unsigned long - error code ERR_xxx
  Description: IR REASTART Command to TAP
   Date        Initials    Description
01-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_JtagRestart(void)
{
    DLOG2(("ARML_JtagRestart"));
    return JtagScanIR(&ulARM_JTAG_RESTART, NULL);
}

/****************************************************************************
     Function: ARML_Write940_946CPSingleReg
     Engineer: Shameerudheen P T
        Input: unsigned char ucRegAddress - Co-processor address Register
               unsigned long ulRegValue - Register Value
       Output: unsigned long - error code ERR_xxx
  Description: Write a single ARM940/946 Co-Processor register
   Date        Initials    Description
01-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Write940_946CPSingleReg (unsigned char ucRegAddress, unsigned long ulRegValue)
{

    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_Write940_946CPSingleReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulLocalData[0] = ulRegValue;
    pulLocalData[1]= (ucRegAddress | ARM940_946CP_WRITEMASK);
    ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_Write920_922CPSingleReg
     Engineer: Shameerudheen P T
        Input: unsigned char ucRegAddress - Co-processor Address
               unsigned long ulRegValue - Register Value to write
       Output: unsigned long - error code ERR_xxx
  Description: Write ARM920/922 Co-Processor Single register write
   Date        Initials    Description
04-Sep-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Write920_922CPSingleReg (unsigned char ucRegAddress, unsigned long ulRegValue)
{

    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_Write920_922CPSingleReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    DLOG3(("Address 0x%x", ucRegAddress));
    DLOG3(("Value 0x%x", ulRegValue));
    pulLocalData[0] = ((ulRegValue << 1) + ARM920_922CP_PHYSICAL_ACCESS);
    pulLocalData[1]= (ucRegAddress + ARM920_922CP_WRITEMASK);
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_Read920_922CPSingleReg
     Engineer: Shameerudheen P T
        Input: unsigned char ucRegAddress - Co-processor register
               unsigned long *pulRegReadValue - pointer for read register value
       Output: unsigned long - error code ERR_xxx
  Description: Read ARM920/922 Co-Processor Single register Value
   Date        Initials    Description
05-Sep-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Read920_922CPSingleReg (unsigned char ucRegAddress, unsigned long *pulRegReadValue)
{
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15,
    unsigned long pulLocalReturnData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_Read920_922CPSingleReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulLocalData[0] = ARM920_922CP_PHYSICAL_ACCESS;
    pulLocalData[1]= ucRegAddress;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReturnData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    //*pulRegReadValue = (pulLocalReturnData[0] >> 1);
    pulLocalReturnData[0] = (pulLocalReturnData[0] >> 1);
    *pulRegReadValue = pulLocalReturnData[0] | ((pulLocalReturnData[1] & 0x00000001) << 31);

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}


/****************************************************************************
     Function: ARML_Write926CPSingleReg
     Engineer: Shameerudheen P T
        Input: unsigned char ucRegAddress - co-processor register address
               unsigned long ulRegValue - register value to write
       Output: unsigned long - error code ERR_xxx
  Description: Write ARM926 Co-Processor Single register write
   Date        Initials    Description
04-Sep-2007    SPT         Initial
25-Jan-2008    SPT         Error Correction
****************************************************************************/
unsigned long ARML_Write926CPSingleReg (unsigned char ucRegAddress, unsigned long ulRegValue)
{
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending
    unsigned long pulLocalReceivedData[0x2] = {0x00000000, 0x00000000};
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned short usCount;

    DLOG2(("ARML_Write926CPSingleReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulLocalData[0] = ulRegValue;
    pulLocalData[1] = (ucRegAddress << 1) + ARM926_CP_ACCESS_WRITE_MASK +
                      ARM926_INITIATE_NEW_CP_ACCESS;
    ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Error correction
    //ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;
    //end Error Correction

    pulLocalData[0] = 0x0;
    pulLocalData[1] = 0x0;
    usCount = 0;
    do
        {
        ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        usCount++;
        if ( usCount >= MAX_WAIT_CYCLES )
            {
            DLOG3(("CP Count Reached"));
            return ERR_ARM_CP926_ACCESS_TIME_OUT;
            }
        }
    while ( (pulLocalReceivedData[1] & 0x1) != 0x1 );
    return ulResult;
}

/****************************************************************************
     Function: ARML_ResetProc
     Engineer: Shameerudheen PT
        Input: unsigned char bAssertReset - 0x1 to assert and hold reset, 0x0 to deassert reset
               unsigned char *pbResetAsserted - pointer for reset status (can be NULL)
       Output: unsigned long - error code ERR_xxx
  Description: controlling reset of ARM processor using nSRST pin
Date           Initials    Description
31-Jul-2007    SPT         Initial
20-Nov-2007    VH          Modified LED indicator
01-Aug-2007    JR         Modified for Opella-XD Reset
****************************************************************************/
unsigned long ARML_ResetProc(unsigned char bAssertReset, unsigned char *pbResetAsserted)
{
    DLOG2(("ARML_ResetProc"));
    // first control nSRST signal
    if ( bAssertReset )
        {
        if ( !bLedTargetResetAsserted )
            bLedTargetResetAsserted = 1;                                                  // indicate target reset status
        put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~JTAG_TPA_RST);                  // clear RST signal to assert nSRST
        bLedTargetResetAsserted = 1;                                                     // set flag to signal reset on LED
        }
    else
        {
        put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | JTAG_TPA_RST);                   // set RST signal to de assert nSRST
        }
    us_wait(5);                                                                         // wait 5 us
    // if requested, check nRST sense signal
    if ( pbResetAsserted != NULL )
        {
        if ( get_wvalue(JTAG_TPIN) & JTAG_TPA_SENSRST )
            *pbResetAsserted = 0;
        else
            *pbResetAsserted = 1;
        }
    return ERR_NO_ERROR;
}

/****************************************************************************
    Function: ARML_WriteICEBreaker
    Engineer: Shameerudheen P T
       Input: unsigned long *pulAddress - Address,
              unsigned long *pulData - Data to write in to the address.
      Output: unsigned long - error code ERR_xxx
 Description: Write to ICE breaker.
   Date        Initials    Description
31-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_WriteICEBreaker(unsigned long *pulAddress, 
                                   unsigned long *pulData)
{
    unsigned long pulTempVar[2];

    DLOG2(("ARML_WriteICEBreaker %08x %08x ",*pulAddress,*pulData ));
    pulTempVar[0] = *pulData;
    pulTempVar[1] = ((*pulAddress) | ICE_WRITEMASK);
    return JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulTempVar, NULL);
}

/****************************************************************************
    Function: ARML_ReadICEBreaker
    Engineer: Shameerudheen P T
       Input: unsigned long *pulAddress - Address,
              unsigned long *pulReadData - Data read from the address.
      Output: unsigned long - error code ERR_xxx
 Description: Read from ICE Breaker.
   Date        Initials    Description
31-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ReadICEBreaker(unsigned long *pulAddress, 
                                  unsigned long *pulReadData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulTempVar[2];
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 2 (ice)
    DLOG2(("ARML_ReadICEBreaker"));
    DLOG2(("Address %x",(unsigned char)pulAddress[0]));
    pulTempVar[0] = 0x00000000;
    pulTempVar[1] = (unsigned char)pulAddress[0];
    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulTempVar, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempVar[0] = 0x00000000;
    pulTempVar[1] = 0x00000000;
    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulTempVar, pulLocalData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    *pulReadData = pulLocalData[0];
    DLOG2(("Data 1st = %x, 2nd = %x",pulLocalData[0],pulLocalData[1]));
    return ulResult;
}

/****************************************************************************
    Function: ARML_FeroceonChangeModeToARMMode
    Engineer: Roshan T R
       Input: unsigned char ucWatchPoint - input (how the core entered into debug mode)
              unsigned char * pucOffsetNeeded - Pointer to return whether the core stopped on a non word aligned address  
      Output: unsigned long - error code ERR_xxx
 Description: Change from Thumb mode to ARM mode
   Date        Initials    Description
18-Mar-2010    RTR         Initial
****************************************************************************/
unsigned long ARML_FeroceonChangeModeToARMMode(unsigned char ucWatchPoint, unsigned char * pucOffsetNeeded)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulReg0;
    unsigned long ulRegPC;

    //unsigned long ulBXValue = 0x0;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[3 * 16];
    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulTempTDOVal;

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;

    DLOG2(("ARML_ARM9ChangeModeToARMMode"));
    *pucOffsetNeeded = FALSE;
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    // Read R0 and store value.
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_STR_R0_R0;   

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    // MOV R0, PC
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_MOV_R0_PC;

    //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    // Read R0 (now equal to the value of PC) and store value.

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_STR_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    pulTempTDOVal +=3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //ulRegPC = pulTempTDOVal[3];
    //Put the value 0x0 into R0 so that we can BX to a word aligned address.

    //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_LDMIA_R0_R0;

    //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    //ARM9ScanChain1PrepareDataThumb(pulTempTDIVal,ulBXValue);
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    //pulTempTDIVal += 3;

    // Branch and exchange with the R0 (ie 0x0 ARM mode instructions have to be word
    // aligned). This changes back to ARM mode.

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_BX_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;


    ulResult = JtagScanDRMultiple(5, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulReg0 = pulTDOVal[0];
    ulRegPC = pulTDOVal[3];

    DLOG2(("ARM9 MSG: ARML_ARM9ChangeModeToARMMode:pulPCValue: 0x%08x",ulRegPC));     // Remove

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    // These NOP's are in thumb mode but really the pipeline is 
    // flushed when the BX is executed so it doesn't matter.
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine

    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //This offset accounts for the instructions taken since debug mode was entered,
    //if (ucWatchPoint)
    //   ulRegPC -= (0x28 + 0x4);
    //else
    //   ulRegPC -= (0x2A + 0x4);
    //if (ucWatchPoint)
    //   ulRegPC -= (0x38 + 0x4);
    //else
    //   ulRegPC -= (0x3A + 0x4);

    if ( ucWatchPoint )
        ulRegPC -= (0x3A + 0x4);
    else
        ulRegPC -= (0x3C + 0x4);
    DLOG2(("ARM9 MSG: ARML_ARM9ChangeModeToARMMode:pulPCValue: 0x%08x",ulRegPC));     // Remove

    if ( (ulRegPC & 0x3) != 0 )
        {
        //If the PC is at a half word aligned address then it is 
        //changed to the previous word aligned address.
        ulRegPC -= 0x2;
        *pucOffsetNeeded = TRUE;
        }

    DLOG2(("ARM9 MSG: ARML_ARM9ChangeModeToARMMode:pulPCValue: 0x%08x",ulRegPC));     // Remove

    // Now we are in ARM mode we can restore the values of R0 and the PC.
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MOV_PC_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulReg0);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    return JtagScanDRMultiple(6, pulDRLength, pulDataToScan, pulDataFromScan);
}

/****************************************************************************
    Function: ARML_ARM9ChangeModeToARMMode
    Engineer: Shameerudheen PT.
       Input: unsigned char ucWatchPoint - input (how the core entered into debug mode)
              unsigned char * pucOffsetNeeded - Pointer to return whether the core stopped on a non word aligned address  
      Output: unsigned long - error code ERR_xxx
 Description: Change from Thumb mode to ARM mode
   Date        Initials    Description
01-Aug-2007    SPT         Initial
25-Mar-2008    JR          Included FPGA JTAG 16 Buffers feature
25-Mar-2008    SPT         Correction
****************************************************************************/
unsigned long ARML_ARM9ChangeModeToARMMode(unsigned char ucWatchPoint, unsigned char * pucOffsetNeeded)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulReg0;
    unsigned long ulRegPC;

    unsigned long ulBXValue = 0x0;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[3 * 16];
    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulTempTDOVal;

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;

    DLOG2(("ARML_ARM9ChangeModeToARMMode"));
    *pucOffsetNeeded = FALSE;
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Read R0 and store value.
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_STR_R0_R0;   

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    pulTempTDOVal += 3;


    // MOV R0, PC
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_MOV_R0_PC;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    // Read R0 (now equal to the value of PC) and store value.

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_STR_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    pulTempTDOVal +=3;

    //ulRegPC = pulTempTDOVal[3];
    //Put the value 0x0 into R0 so that we can BX to a word aligned address.

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareDataThumb(pulTempTDIVal,ulBXValue);
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    // Branch and exchange with the R0 (ie 0x0 ARM mode instructions have to be word
    // aligned). This changes back to ARM mode.

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_BX_R0;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulReg0 = pulTDOVal[0];
    ulRegPC = pulTDOVal[3];

    DLOG2(("ARM9 MSG: ARML_ARM9ChangeModeToARMMode:pulPCValue: 0x%08x",ulRegPC));     // Remove

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    // These NOP's are in thumb mode but really the pipeline is 
    // flushed when the BX is executed so it doesn't matter.
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine

    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //This offset accounts for the instructions taken since debug mode was entered,
    if ( ucWatchPoint )
        ulRegPC -= 0x28;
    else
        ulRegPC -= 0x2A;

    DLOG2(("ARM9 MSG: ARML_ARM9ChangeModeToARMMode:pulPCValue: 0x%08x",ulRegPC));     // Remove

    if ( (ulRegPC & 0x3) != 0 )
        {
        //If the PC is at a half word aligned address then it is 
        //changed to the previous word aligned address.
        ulRegPC -= 0x2;
        *pucOffsetNeeded = TRUE;
        }

    DLOG2(("ARM9 MSG: ARML_ARM9ChangeModeToARMMode:pulPCValue: 0x%08x",ulRegPC));     // Remove

    // Now we are in ARM mode we can restore the values of R0 and the PC.
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MOV_PC_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulReg0);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    return JtagScanDRMultiple(6, pulDRLength, pulDataToScan, pulDataFromScan);
}


/****************************************************************************
    Function: ARML_ARM9ChangeToThumbMode
    Engineer: Shameerudheen PT.
       Input: unsigned long ulReg0 - R0 register Value
              unsigned long ulRegPC - Program counter value
      Output: unsigned long - error code ERR_xxx  
 Description: Change from ARM mode to Thumb mode
   Date        Initials    Description
01-Aug-2007      SPT         Initial
****************************************************************************/
unsigned long ARML_ARM9ChangeToThumbMode(unsigned long ulReg0, unsigned long ulRegPC)
{
    unsigned long ulResult = ERR_NO_ERROR;
    //unsigned long pulTempVar[0x3] = {0x00000000, 0x00000000, 0x00000000}; //for scan chain 1, 67 bits long

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;

    DLOG2(("ARML_ARM9ChangeToThumbMode"));
    // These instructions are in ARM mode.
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal=pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_LDMIA_R0_R0,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL; 

    //ulResult = ARML_PerformARM9Nops(2);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    //ARM9ScanChain1PrepareData(pulTempVar,ulRegPC);
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulTempVar,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    //ulResult = ARML_PerformARM9Nops(2);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    //ulResult = ARML_PerformARM9Nops(2);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    // Branch and exchange with the R0.
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_BX_R0;

    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_ARM_BX_R0,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    // in Thumb mode.
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_LDMIA_R0_R0;
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_LDMIA_R0_R0,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareDataThumb(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    //ARM9ScanChain1PrepareDataThumb(pulTempVar,ulReg0);
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulTempVar,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;
    //return JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,NULL);
    return JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
}

/****************************************************************************
    Function: ARML_ARMReadWriteDCC
    Engineer: Shameerudheen PT.
       Input: unsigned char ucProcessor - Which Processor
              unsigned char *pucDataToWrite - Is there Data to Write
              unsigned long *pulWriteData - the Data to write
              unsigned char *pucDataWriteSuccessful - is Write successful
              unsigned char *pucDataReadFromCore - is there data from core
              unsigned long *pulReadData - Data from core
      Output: unsigned long - error code ERR_xxx
 Description: Change from Thumb mode to ARM mode
   Date        Initials    Description
10-Sep-2007      SPT         Initial
9-Aug-2009       ADK         Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMReadWriteDCC (unsigned char ucProcessor, 
                                    unsigned char *pucDataToWrite,
                                    unsigned long *pulWriteData,
                                    unsigned char *pucDataWriteSuccessful,
                                    unsigned char *pucDataReadFromCore,
                                    unsigned long *pulReadData)
{  
    DLOG2(("ARML_ARMReadWriteDCC"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7ReadWriteDCC (pucDataToWrite, pulWriteData, pucDataWriteSuccessful,
                                          pucDataReadFromCore, pulReadData);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9ReadWriteDCC (pucDataToWrite, pulWriteData, pucDataWriteSuccessful,
                                          pucDataReadFromCore, pulReadData);
            //break;
        case ARM11: // ARM11
            return ARML_ARM11ReadWriteDCC (pucDataToWrite, pulWriteData, pucDataWriteSuccessful,
                                           pucDataReadFromCore, pulReadData);
            //break;
        case CORTEXA: // CortexA
            return CortexA_ReadWriteDCC (pucDataToWrite, pulWriteData, pucDataWriteSuccessful,
                                          pucDataReadFromCore, pulReadData);
            //break;


        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
    Function: ARML_ARMChangeModeToARMMode
    Engineer: Shameerudheen PT.
       Input: unsigned char ucProcessor - on Which processor the command applied
              to, see each functions description for more details, This is a 
              wrapper function only
      Output: unsigned long - error code ERR_xxx  
 Description: Change from Thumb mode to ARM mode
   Date        Initials    Description
01-Aug-2007      SPT         Initial
****************************************************************************/
unsigned long  ARML_ARMChangeModeToARMMode (unsigned char ucProcessor, unsigned char ucWatchPoint,
                                            unsigned char ucUserHalt, unsigned char * pucOffsetNeeded)
{  
    DLOG2(("ARML_ARMChangeModeToARMMode"));
    switch ( ucProcessor )
        {
        case ARM7:
            return ARML_ARM7ChangeModeToARMMode(ucUserHalt,pucOffsetNeeded);
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7SChangeModeToARMMode(pucOffsetNeeded);
            //break;
        case ARM9: // ARM9
            if ( FEROCEON == ulTargetType )
                {
                return ARML_FeroceonChangeModeToARMMode(ucWatchPoint, pucOffsetNeeded);
                }
            else
                {
                return ARML_ARM9ChangeModeToARMMode(ucWatchPoint, pucOffsetNeeded);
                }
            //break;
        case ARM11: // ARM11
            return ARML_ARM11ChangeModeToARMMode(ucWatchPoint, pucOffsetNeeded);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMPerfromARMNops
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned long ulNumberOfnops - number of nops to call
       Output: unsigned long - error code ERR_xxx
  Description: Writes all the registers.
   Date        Initials    Description
18-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long  ARML_ARMPerfromARMNops (unsigned char ucProcessor, unsigned long ulNumberOfnops)
{
    //DLOG2(("ARML_ARMPerfromARMNops"));
    unsigned long ulResult = ERR_NO_ERROR;
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_PerformARM7Nops((unsigned char) ulNumberOfnops);
            //break;
        case ARM9: // ARM9
            ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_PerformARM9Nops((unsigned char) ulNumberOfnops);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMCheckDataAbort
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned char *pucReadData - pointer to return processor abort condition
       Output: unsigned long - error code ERR_xxx
  Description: Check data abort condition.
   Date        Initials    Description
13-Aug-2007      SPT       Initial
9-Aug-2009       ADK          Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMCheckDataAbort (unsigned char ucProcessor, unsigned char *pucReadData)
{
    DLOG2(("ARML_ARMCheckDataAbort"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7CheckDataAbort(pucReadData);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9CheckDataAbort(pucReadData);
            //break;
        case ARM11: // ARM9
            return ARML_ARM11CheckDataAbort(pucReadData);
            //break;
        case CORTEXM:
            return ARML_CortexMCheckDataAbort(pucReadData);
        case CORTEXA:
            return CortexA_CheckForAbort(pucReadData);
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMWriteWord
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned long *pulAddress - Address location,
               unsigned long *pulWriteData - Data to write in to the address
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to memory location.
   Date        Initials    Description
13-Aug-2007      SPT       Initial
9-Aug-2009       ADK          Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMWriteWord (unsigned char ucProcessor, unsigned long *pulAddress, unsigned long *pulWriteData)
{
    unsigned long ulResult;

    DLOG2(("ARML_ARMWriteWord"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            {
                ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM7WriteWord(*pulAddress, *pulWriteData);
            }
        case ARM9: // ARM9
            {
                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM9WriteWord(pulAddress, pulWriteData);
            }
        case ARM11: // ARM11
            {
                return ARML_ARM11WriteMemoryWord(*pulAddress,pulWriteData); 
            }

        case CORTEXM:
            {
                return ARML_CortexMWriteMemory(pulWriteData,*pulAddress, 1, ACCESS_SIZE_32);
            }
        case CORTEXA:
            {
                return CortexA_WriteMemoryWord(*pulAddress, pulWriteData);
                //return CortexA_WriteMemory(*pulAddress,1, ACCESS_SIZE_32, pulWriteData);
            }

        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMWriteMultipleWords
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned long *pulAddress - Address location,
               unsigned long *pulWriteData - Data to write in to the address
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to memory location.
   Date        Initials    Description
11-Feb-2008      SPT       Initial
9-Aug-2009       ADK          Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMWriteMultipleWords (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulWordCount, unsigned long *pulWriteData)
{
    unsigned long ulResult;
    //DLOG2(("ARML_ARMWriteMultipleWords"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            {
                ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM7WriteMultipleWord_ReallyFast(ulAddress, pulWriteData, ulWordCount);
            }
        case ARM9: // ARM9
            {
                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM9WriteMultipleWord_ReallyFast(&ulAddress, pulWriteData, ulWordCount);       
            }
        case ARM11: // ARM11
            {
                return ARML_ARM11WriteMultipleWord(ulAddress,ulWordCount,pulWriteData);
            }
        case CORTEXM:
            {
                return ARML_CortexMWriteMemory(pulWriteData,ulAddress, ulWordCount, ACCESS_SIZE_32);
            }
        case CORTEXA:
            {             
                return CortexA_WriteMemoryWordMultiple(ulAddress,ulWordCount,pulWriteData);
                //return CortexA_WriteMemory_MemAp(ulAddress,ulWordCount, ACCESS_SIZE_32, pulWriteData);
            }

        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMWriteBlock
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned long *pulAddress - Address location,
               unsigned long *pulWriteData - Data to write in to the address
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to a block of 14 consecutive memory location.
   Date        Initials    Description
27-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARMWriteBlock (unsigned char ucProcessor, unsigned long *pulAddress, unsigned long *pulWriteData)
{
    unsigned long ulResult;
    DLOG2(("ARML_ARMWriteBlock"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_ARM7WriteBlock(*pulAddress, pulWriteData); 
            //break;
        case ARM9: // ARM9
            ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            //return ARML_ARM9WriteBlock_ReallyFast(pulAddress, pulWriteData);
            return ARML_ARM9WriteBlock(pulAddress, pulWriteData);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}
/****************************************************************************
     Function: ARML_ARMWriteMultipleBlocks
     Engineer: Jeenus Chalattukunnath
        Input: unsigned char ucProcessor - Processor,
               unsigned long *pulAddress - Address location,
               unsigned long ulBlockCount - number of blocks to be written into memory
               unsigned long *pulWriteData - Data to write in to the address 
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to a (ulBlockCount X block of 14) consecutive memory location.
   Date        Initials    Description
11-Feb-2008      SPT       Initial
9-Aug-2009       ADK          Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMWriteMultipleBlocks (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulBlockCount, unsigned long *pulWriteData)
{
    unsigned long ulResult;
    DLOG2(("ARML_ARMWriteMultipleBlocks"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            {
                ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM7WriteMultipleBlock_ReallyFast(&ulAddress, ulBlockCount);
            }
        case ARM9: // ARM9
            {
                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM9WriteMultipleBlock_ReallyFast(&ulAddress, ulBlockCount);
            }
        case ARM11:
            {  /* This function works well,no speed Optimisation implemented,uses single FPGA buffer*/

                return ARML_ARM11WriteMultipleBlock(ulAddress,ulBlockCount,pulWriteData);
                //return ARML_ARM11WriteMultipleBlock_ReallyFast(&ulAddress,ulBlockCount,pulWriteData);
            }
        case CORTEXM:
            {
                return ARML_CortexMWriteMemory(pulWriteData,ulAddress, (ulBlockCount * 14), ACCESS_SIZE_32);
            }
        case CORTEXA:
            {
                return CortexA_WriteMemoryWordMultiple(ulAddress, (ulBlockCount * 14), pulWriteData);
                //return CortexA_WriteMemory_MemAp(ulAddress,ulBlockCount * 14, ACCESS_SIZE_32, pulWriteData);
            }
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************                                                                           
     Function: ARML_ARMExecuteProc
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned char *bThumbMode - Thumb mode or not,
               unsigned long *pulData - Data to write
       Output: unsigned long - error code ERR_xxx
  Description: Executes Program - used for single step, run etc.
   Date        Initials    Description
28-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARMExecuteProc (unsigned char ucProcessor, unsigned char *bThumbMode, unsigned long *pulData)
{
    DLOG2(("ARML_ARMExecuteProc"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7ExecuteProc(bThumbMode, pulData);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9ExecuteProc(bThumbMode, pulData);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMSetupExecuteProc
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned long *pulData - Data to write
       Output: unsigned long - error code ERR_xxx 
  Description: Setting up core before calling execute proc function
   Date        Initials    Description
27-Aug-2007      SPT       Initial
9-Aug-2009       ADK          Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMSetupExecuteProc (unsigned char ucProcessor, unsigned long *pulData)
{
    DLOG2(("ARML_ARMSetupExecuteProc"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7SetupExecuteProc(pulData);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9SetupExecuteProc(pulData);
            //break;
        case ARM11: // ARM11
            return ARML_ARM11SetupExecuteProc(pulData);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMChangeToThumbMode
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor, 
               unsigned long ulReg0 - Reg0 Value,
               unsigned long ulRegPC - RegPC Value
       Output: unsigned long - error code ERR_xxx
  Description: Change to Thumb mode from ARM mode
   Date        Initials    Description
27-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARMChangeToThumbMode (unsigned char ucProcessor, unsigned long ulReg0,
                                         unsigned long ulRegPC)
{
    DLOG2(("ARML_ARMChangeToThumbMode"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7ChangeToThumbMode(ulReg0,ulRegPC);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9ChangeToThumbMode(ulReg0,ulRegPC);
            //break;
        case ARM11: // ARM9
            return ARML_ARM11ChangeToThumbMode(ulReg0,ulRegPC);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMReadWord
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor, 
               unsigned long ulAddress - Address location, 
               unsigned long *pulReadData - Pointer for read data
       Output: unsigned long - error code ERR_xxx
  Description: read Data from memory location.
   Date        Initials    Description
13-Aug-2007      SPT       Initial
9-Aug-2009       ADK          Added ARM11 Support
****************************************************************************/
unsigned long ARML_ARMReadWord (unsigned char ucProcessor, unsigned long ulAddress, unsigned long *pulReadData)
{
    unsigned long ulResult;
    DLOG2(("ARML_ARMReadWord"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_ARM7ReadWord(ulAddress, pulReadData);

            //break;
        case ARM9: // ARM9
            ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_ARM9ReadWord(ulAddress, pulReadData);
            //break;
        case ARM11:
            return ARML_ARM11ReadMemoryWord(ulAddress,pulReadData);
            //break;
        case CORTEXM:
            return ARML_CortexMReadMemory(pulReadData, ulAddress, 1, ACCESS_SIZE_32);
        case CORTEXA:           
            return CortexA_ReadMemoryWord(ulAddress,pulReadData);
            //return CortexA_ReadMemory_MemAp(ulAddress, 1, ACCESS_SIZE_32, pulReadData); 
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMReadMultipleWords
     Engineer: Jeenus Chalattukunnath 
        Input: unsigned char ucProcessor - Processor, 
               unsigned long ulAddress - Address location, 
               unsigned long *pulReadData - Pointer for read data
       Output: unsigned long - error code ERR_xxx
  Description: read Data from memory location.
   Date        Initials    Description
11-Feb-2008      JCK       Initial
12-Feb-2008      SPT       Corrections
****************************************************************************/
unsigned long ARML_ARMReadMultipleWords (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulWordCount, unsigned long *pulReadData)
{
    unsigned long ulResult;
    //DLOG2(("ARML_ARMReadMultipleWords"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            {
                ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM7ReadMultipleWord_ReallyFast(ulAddress, pulReadData, ulWordCount);
                /*
                for (i = 0; i < ulWordCount; i++)
                   {
                   ulResult= ARML_ARM7ReadWord(ulAddress, pulReadData);
                   if (ulResult != ERR_NO_ERROR)
                      return ulResult;
                   ulAddress+=4;
                   pulReadData++;
                   }         
                return ulResult;
                */
            }
        case ARM9: // ARM9
            {
                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM9ReadMultipleWord_ReallyFast(ulAddress, pulReadData, ulWordCount);
                /*
                for (i = 0; i < ulWordCount; i++)
                   {
                   ulResult = ARML_ARM9ReadWord(ulAddress, pulReadData);
                   if (ulResult != ERR_NO_ERROR)
                      return ulResult;
                   ulAddress+=4;
                   pulReadData++;
                   }
                */
            }
        case ARM11: // ARM11
            {
                return ARML_ARM11ReadMultipleWord(ulAddress,ulWordCount,pulReadData);
            }
        case CORTEXM:
            {
                //UWORD i;
                ulResult =  ARML_CortexMReadMemory(pulReadData, ulAddress, ulWordCount, ACCESS_SIZE_32);

                /*for (i = 0; i < ulWordCount; i++)
                    {
                    DLOG2(("Data: 0x%08x",pulReadData[i]));
                    }*/
                return  ulResult;
            }
        case CORTEXA:
            {             
            return CortexA_ReadMemoryWordMultiple(ulAddress,ulWordCount,pulReadData);               
            //return CortexA_ReadMemory_MemAp(ulAddress, ulWordCount, ACCESS_SIZE_32, pulReadData);
            }

        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMReadBlock
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor, 
               unsigned long ulAddress - Address location, 
               unsigned long *pulReadData - pointer to read data
       Output: unsigned long - error code ERR_xxx
  Description: read Data from a block of 14 consecutive memory location.
   Date        Initials    Description
27-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARMReadBlock (unsigned char ucProcessor, unsigned long ulAddress, unsigned long *pulReadData)
{
    unsigned long ulResult;
    DLOG2(("ARML_ARMReadBlock"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_ARM7ReadBlock(ulAddress, pulReadData); 
            //break;
        case ARM9: // ARM9
            ulResult = ARML_ARM9SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            return ARML_ARM9ReadBlock(&ulAddress, pulReadData);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMReadMultipleBlocks
     Engineer: Jeenus Chalattukunnath
        Input: unsigned char ucProcessor - Processor, 
               unsigned long ulAddress - Address location, 
               unsigned long *pulReadData - pointer to read data
       Output: unsigned long - error code ERR_xxx
  Description: read Data from a block of 14 consecutive memory location.
   Date        Initials    Description
11-Feb-2008    JCK         Initial
9-Aug-2009     ADK         Added support for ARM11
****************************************************************************/
unsigned long ARML_ARMReadMultipleBlocks (unsigned char ucProcessor, unsigned long ulAddress, unsigned long ulWordCount, unsigned long *pulReadData)
{
    unsigned long ulResult;
    //unsigned long i;
    //DLOG2(("ARML_ARMReadMultipleBlocks"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            {
                ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM7ReadMultipleBlock_ReallyFast(&ulAddress, pulReadData, ulWordCount);
            }
        case ARM9: // ARM9
            {
                ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;
                return ARML_ARM9ReadMultipleBlock_ReallyFast(&ulAddress, pulReadData, ulWordCount);
            } 
        case ARM11:
            {  /* This function works,no optimisation done */
                return ARML_ARM11ReadMultipleBlock(ulAddress,ulWordCount,pulReadData);      
                //return ARML_ARM11ReadMultipleBlock_ReallyFast(&ulAddress, pulReadData, ulWordCount);
            }
        case CORTEXM:
            {
                return ARML_CortexMReadMemory(pulReadData,ulAddress, (ulWordCount * 14), ACCESS_SIZE_32);
            }
        case CORTEXA:
            {
                return CortexA_ReadMemoryWordMultiple(ulAddress, (ulWordCount * 14), pulReadData);
                //return CortexA_ReadMemory_MemAp(ulAddress, (ulWordCount * 14), ACCESS_SIZE_32, pulReadData);
            }
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMWriteRegisters
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor, 
               unsigned long *pulWriteData - Data to write in to registers
       Output: unsigned long - error code ERR_xxx
  Description: Writes all the registers.
   Date        Initials    Description
18-Jul-2007    SPT       Initial
9-Aug-2009     ADK         Added support for ARM11
12-Mar-2010    ADK         Added support for CortexA
****************************************************************************/
unsigned long  ARML_ARMWriteRegisters (unsigned char ucProcessor, unsigned long *pulWriteData)
{
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7WriteRegisters(pulWriteData);
        case ARM9: // ARM9
            return ARML_ARM9WriteRegisters(pulWriteData);
        case ARM11: // ARM11  
            return ARML_ARM11WriteRegisters(pulWriteData);
        case CORTEXM:
            return ARML_CortexMReadWriteRegisters(pulWriteData, WRITE);
        case CORTEXA:
            return CortexA_WriteRegisters(pulWriteData);
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMWriteByte
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor,
               unsigned long ulAddress - address,
               unsigned char ucData - data
       Output: unsigned long - error code ERR_xxx
  Description: Writes a byte to the register.
   Date        Initials    Description
18-Jul-2007    SPT          Initial
9-Aug-2009     ADK        Added support for ARM11
****************************************************************************/
unsigned long  ARML_ARMWriteByte (unsigned char ucProcessor, unsigned long ulAddress, unsigned char ucData)
{
    //DLOG2(("ARML_ARMWriteByte"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7WriteByte(ulAddress, ucData);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9WriteByte(ucData, ulAddress);
            //break;
        case ARM11: // ARM11
            return ARML_ARM11Write_MemoryByte(ulAddress,ucData);
            //break;
        case CORTEXM: /* CortexM */
            return ARML_CortexMWriteMemory((UWORD*)&ucData,ulAddress, 1, ACCESS_SIZE_8);
        case CORTEXA:
            return CortexA_WriteMemoryByte(ulAddress,ucData);
            //return CortexA_WriteMemory_MemAp(ulAddress,1, ACCESS_SIZE_8, (unsigned long*)&ucData);
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMReadRegisters
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - Processor, 
               unsigned long *pulResultData - Pointer to store result
       Output: unsigned long - error code ERR_xxx
  Description: Read All registers r0-r15 and spsr pcsr of all modes.
Date           Initials    Description
31-Jul-2007    SPT          Initial
19-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
9-Aug-2009     ADK          Added support for ARM11
12-Mar-2010    ADK          Added support for CortexA
****************************************************************************/
unsigned long ARML_ARMReadRegisters(unsigned char ucProcessor, unsigned long *pulResultData)
{
    //DLOG2(("ARML_ARMReadRegisters"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7ReadRegisters(pulResultData);
            //break;
        case ARM9: // ARM9
            return ARML_ARM9ReadRegisters(pulResultData);
            //break;
        case ARM11: // ARM11
            return ARML_ARM11ReadRegisters(pulResultData);
        case CORTEXM:
            return ARML_CortexMReadWriteRegisters(pulResultData,READ);
        case CORTEXA:
            return CortexA_ReadRegisters(pulResultData);
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
     Function: ARML_ARMStatusProc
     Engineer: Shameerudheen P T
        Input: unsigned char ucProcessor - ARM7 or ARM9
               unsigned char *pucSelectScanChain2 - Whether the core is already in scan chain 2,
               unsigned char *pucStartedExecution - Whether the core has started execution
               unsigned char *pucARM7StopProblem - Return ARM7 stop problem
               unsigned char *pucCoreExecuting - Return whether the core is still executing
               unsigned char *pucThumbMode - Return whether the core is in Thumb mode
               unsigned char *pucWatchPoint - Return whether the core stopped because of a watchpoint.
       Output: unsigned long - error code ERR_xxx
  Description: Checks the status of the processor
   Date        Initials    Description
31-Jul-2007    SPT         Initial 
9-Aug-2009     ADK         Added support for ARM11                     
****************************************************************************/
unsigned long ARML_ARMStatusProc(unsigned char ucProcessor, 
                                 unsigned char *pucSelectScanChain2,
                                 unsigned char *pucStartedExecution,
                                 unsigned char *pucARM7StopProblem,
                                 unsigned char *pucCoreExecuting,
                                 unsigned char *pucThumbMode,
                                 unsigned char *pucWatchPoint)

{
    //DLOG2(("ARML_ARMStatusProc"));
    switch ( ucProcessor )
        {
        case ARM7:
            return ARML_ARM7StatusProc (pucSelectScanChain2, pucStartedExecution,
                                        pucARM7StopProblem, pucCoreExecuting,
                                        pucThumbMode, pucWatchPoint);
            //break;
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7SStatusProc (pucSelectScanChain2, pucStartedExecution,
                                         pucARM7StopProblem, pucCoreExecuting,
                                         pucThumbMode, pucWatchPoint);
            //break;
        case ARM9: // ARM9
            *pucARM7StopProblem = 0x0;
            return ARML_ARM9StatusProc (pucSelectScanChain2, pucStartedExecution,
                                        pucCoreExecuting, pucThumbMode, pucWatchPoint);
            //break;
        case ARM11: // ARM11
            *pucARM7StopProblem = 0x0;
            return ARML_ARM11StatusProc (pucCoreExecuting,pucThumbMode,pucWatchPoint);

        case CORTEXM:
            return ARML_CortexMStatusProc(pucCoreExecuting,pucThumbMode,pucWatchPoint);

        case CORTEXA:
            return CortexA_StatusProc(pucCoreExecuting,pucThumbMode,pucWatchPoint);
            //break; 
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
    Function: ARML_SetSafeNonVectorAddress
    Engineer: Shameerudheen P T
       Input: unsigned char ucProcessor - Processor, 
              unsigned long *pulAddress - Address.
      Output: unsigned long - error code ERR_xxx
 Description: Set Safe non vector address.
   Date        Initials    Description
31-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_SetSafeNonVectorAddress(unsigned char ucProcessor, 
                                           unsigned long *pulAddress,
                                           unsigned char ucUseSafeNonVectorAddress)
{
    DLOG2(("ARML_SetSafeNonVectorAddress"));
    ucgUseSafeNonVectorAddress = ucUseSafeNonVectorAddress;

    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            return ARML_ARM7SetSNVectorAddress(pulAddress); 
            //break;
        case ARM9: // ARM9
            return ARML_ARM9SetSNVectorAddress(pulAddress);
        case ARM11: // ARM11
            //      return ARML_ARM11SetSNVectorAddress(pulAddress);
            //break;
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }
}

/****************************************************************************
    Function: ARML_SelectScanChain
    Engineer: Shameerudheen P T
       Input: unsigned char ucProcessor - Processor, 
              unsigned char ucScanChainNumber - Scan Chain Number, 
              unsigned char ucNoOfIntest - No of Intests.
      Output: unsigned long - error code ERR_xxx
 Description: Sleets a Scan Chain.
   Date        Initials    Description
31-Jul-2007      SPT       Initial
05-Aug-2009      ADK       Added Support for ARM11
****************************************************************************/
unsigned long ARML_SelectScanChain(unsigned char ucProcessor, 
                                   unsigned char ucScanChainNumber,
                                   unsigned char ucNoOfIntest)
{
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;
    DLOG2(("ARML_SelectScanChain"));
    switch ( ucProcessor )
        {
        case ARM7:
        case ARM7S: // ARM7 ARM7S
            ulResult = ARML_ARM7SelectScanChain((unsigned long)ucScanChainNumber);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            break;
        case ARM9: // ARM9
            ulResult = ARML_ARM9SelectScanChain((unsigned long)ucScanChainNumber);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            break;
            /*ARM11MODSTATRT*/
        case ARM11: // ARM11
            ulResult = ARML_ARM11SelectScanChain((unsigned long)ucScanChainNumber);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            break;
            /*ARM11MODEND*/
        default:
            return ERR_ARM_INVALID_PROCESSOR;
        }

    if ( ucNoOfIntest > 1 ) // one intest is done in the Select Scan Chain function remaing is doing here
        {
        for ( i = 1; i < ucNoOfIntest; i++ )
            {
            ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        }
    return ulResult;
}

/****************************************************************************
     Function: ARML_Read926CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - pointer for read data
       Output: unsigned long - error code ERR_xxx
  Description: Reads ARM926 Co-Processor registers from core
   Date        Initials    Description
26-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Read926CPReg (unsigned long *pulReadData)
{
    const unsigned long ARM926_CP_TEMP[15] = {ARM926_CP_REG_C0_ID_REG,
        ARM926_CP_REG_C0_CACHE_TYPE,
        ARM926_CP_REG_C0_TCM_STATUS,
        ARM926_CP_REG_C1_CONTROL,
        ARM926_CP_REG_C2_TTB,
        ARM926_CP_REG_C3_DAC,
        ARM926_CP_REG_C5_DFSR,
        ARM926_CP_REG_C5_IFSR,
        ARM926_CP_REG_C6_FAR,
        ARM926_CP_REG_C9_DCACHE_L_DOWN,
        ARM926_CP_REG_C9_ICACHE_L_DOWN,
        ARM926_CP_REG_C9_DCACHE_TCM,
        ARM926_CP_REG_C9_ICACHE_TCM,
        ARM926_CP_REG_C10_TLB,
        ARM926_CP_REG_C13_PROCESS_ID
    };
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 48 bits long for sending
    unsigned long pulLocalReceivedData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 48 bits long
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned short i;
    unsigned short usCount;

    //DLOG2(("ARML_Read926CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i < 15; i++ )
        {
        pulLocalData[0] = 0x0;
        pulLocalData[1] = ((ARM926_CP_TEMP[i] << 1) + ARM926_INITIATE_NEW_CP_ACCESS);
        ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        pulLocalData[0] = 0x0;
        pulLocalData[1] = 0x0;
        usCount = 0;
        do
            {
            ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            usCount++;
            if ( usCount >= MAX_WAIT_CYCLES )
                {
                DLOG2(("CP Count Reached"));
                return ERR_ARM_CP926_ACCESS_TIME_OUT;
                }
            }
        while ( (pulLocalReceivedData[1] & 0x1) != 0x1 );

        pulReadData[i] = pulLocalReceivedData[0];
        }

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}
/****************************************************************************
     Function: ARML_Read940_946CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - pointer to read data
       Output: unsigned long - error code ERR_xxx
  Description: Reads ARM940 / 946 Co-Processor registers from core
   Date        Initials    Description
14-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Read940_946CPReg (unsigned long *pulReadData)
{


    const unsigned long ARM940_946_CP_TEMP[19] = {
        ARM940_946_CP_REG_C0_ID_REG,
        ARM940_946_CP_REG_C0_CACHE_TYPE,
        ARM940_946_CP_REG_C0_TCM_SIZE,
        ARM940_946_CP_REG_C1_CONTROL,
        ARM940_946_CP_REG_C2_DCACHE_BITS,
        ARM940_946_CP_REG_C2_ICACHE_BITS,
        ARM940_946_CP_REG_C3_WRITEBUF_CTRL,
        ARM940_946_CP_REG_C5_DSPACE_PERMISSIONS,
        ARM940_946_CP_REG_C5_ISPACE_PERMISSIONS,
        ARM940_946_CP_REG_C6_REG0_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG1_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG2_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG3_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG4_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG5_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG6_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG7_MEMPROTECTION,
        ARM940_946_CP_REG_C9_DCACHE_LOCKDOWN,
        ARM940_946_CP_REG_C9_ICACHE_LOCKDOWN
    };
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];

    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    unsigned long pulTDOVal[2 * 16];
    unsigned long *pulTempTDOVal;

    //Read the Physical Access registers.
    //DLOG2(("ARML_Read940_946CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;
    for ( i=0; i < 8; i++ )
        {
        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulTempTDIVal[0] = 0x0;
        pulTempTDIVal[1] = ARM940_946_CP_TEMP[i];
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;   

        //ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;

        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        pulTempTDOVal += 2;

        //ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;
        //pulReadData[i] = pulLocalReceivedData[0];
        }
    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    for ( i=0; i < 8; i++ )
        {
        pulReadData[i] = pulTDOVal[i * 2];
        }

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    for ( i=8; i < 16; i++ )
        {
        pulTempTDIVal[0] = 0x0;
        pulTempTDIVal[1] = ARM940_946_CP_TEMP[i];
        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;   

        //ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;

        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        pulTempTDOVal += 2;

        //ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;
        //pulReadData[i] = pulLocalReceivedData[0];
        }
    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    for ( i=8; i < 16; i++ )
        {
        pulReadData[i] = pulTDOVal[(i-8) * 2];
        }
    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    for ( i=16; i < 19; i++ )
        {
        pulTempTDIVal[0] = 0x0;
        pulTempTDIVal[1] = ARM940_946_CP_TEMP[i];
        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;   

        //ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;

        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        pulTempTDOVal += 2;

        //ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;
        //pulReadData[i] = pulLocalReceivedData[0];
        }
    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    for ( i=16; i < 19; i++ )
        {
        pulReadData[i] = pulTDOVal[(i-16) * 2];
        }
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}
/*
unsigned long ARML_Read940_946CPReg (unsigned long *pulReadData)
{


   const unsigned long ARM940_946_CP_TEMP[19] = {
      ARM940_946_CP_REG_C0_ID_REG,
      ARM940_946_CP_REG_C0_CACHE_TYPE,
      ARM940_946_CP_REG_C0_TCM_SIZE,
      ARM940_946_CP_REG_C1_CONTROL,
      ARM940_946_CP_REG_C2_DCACHE_BITS,
      ARM940_946_CP_REG_C2_ICACHE_BITS,
      ARM940_946_CP_REG_C3_WRITEBUF_CTRL,
      ARM940_946_CP_REG_C5_DSPACE_PERMISSIONS,
      ARM940_946_CP_REG_C5_ISPACE_PERMISSIONS,
      ARM940_946_CP_REG_C6_REG0_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG1_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG2_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG3_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG4_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG5_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG6_MEMPROTECTION,
      ARM940_946_CP_REG_C6_REG7_MEMPROTECTION,
      ARM940_946_CP_REG_C9_DCACHE_LOCKDOWN,
      ARM940_946_CP_REG_C9_ICACHE_LOCKDOWN
   };
   unsigned char i;
   unsigned long pulLocalData[2];
   unsigned long pulLocalReceivedData[2];
   unsigned long ulResult = ERR_NO_ERROR;

   //Read the Physical Access registers.
   DLOG2(("ARML_Read940_946CPReg"));
   ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
   if (ulResult != ERR_NO_ERROR)
      return ulResult;

   for (i=0; i < 19; i++)
      {
      pulLocalData[0] = 0x0;
      pulLocalData[1] = ARM940_946_CP_TEMP[i];
      ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
      if (ulResult != ERR_NO_ERROR)
         return ulResult;
      ulResult = JtagScanDR(ARM940_946_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
      if (ulResult != ERR_NO_ERROR)
         return ulResult;
      pulReadData[i] = pulLocalReceivedData[0];
      }
   return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}
*/
/****************************************************************************
     Function: ARML_Read966CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - pointer to read data
       Output: unsigned long - error code ERR_xxx
  Description: Reads ARM966 Co-Processor registers from core
   Date        Initials    Description
20-Aug-2007      SPT       Initial
25-Mar-2008    SPT          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_Read966CPReg (unsigned long *pulReadData)
{


    const unsigned long ARM966_CP_TEMP[7] = {
        ARM966_CP_REG_C0_ID_REG,
        ARM966_CP_REG_C1,
        ARM966_CP_REG_C15_1,
        ARM966_CP_REG_C15_2,
        ARM966_CP_REG_C15_3,
        ARM966_CP_REG_C15_4,
        ARM966_CP_REG_C15_5
    };
    unsigned char i;
    unsigned long pulLocalData[2];
    unsigned long pulLocalReceivedData[2];
    unsigned long ulResult = ERR_NO_ERROR;

    //Read the Physical Access registers.
    //DLOG2(("ARML_Read966CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //C0 ID Register
    pulLocalData[0] = 0x0;
    pulLocalData[1] = ARM966_CP_TEMP[0];
    ulResult = JtagScanDR(ARM966_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM966_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulReadData[0] = pulLocalReceivedData[0];

    //C1 Control Register
    pulLocalData[0] = 0x0;
    pulLocalData[1] = ARM966_CP_TEMP[1];
    ulResult = JtagScanDR(ARM966_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM966_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulReadData[1] = pulLocalReceivedData[0];

    for ( i=2; i < 7; i++ )
        {
        pulLocalData[0] = ((ARM966_CP_TEMP[i] & 0x3) << 30);
        pulLocalData[1] = ((ARM966_CP_TEMP[i] & 0xFC) >> 2);
        ulResult = JtagScanDR(ARM966_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = JtagScanDR(ARM966_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        pulReadData[i] = (pulLocalReceivedData[0] & 0x00FFFFFF);
        }
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_Read968CPReg
     Engineer: Jeenus Challattu Kunnath
        Input: unsigned long *pulReadData - pointer to read data
       Output: unsigned long - error code ERR_xxx
  Description: Reads ARM968 Co-Processor registers from core
   Date        Initials    Description
24-Mar-2008      JCK       Initial
****************************************************************************/
unsigned long ARML_Read968CPReg (unsigned long *pulReadData)
{
    const unsigned long ARM968_CP_TEMP[2] = {
        ARM968_CP_REG_C0_ID_REG,
        ARM968_CP_REG_C1      
    };

    //unsigned long *pulDataToScan[16];
    //unsigned long *pulDataFromScan[16];

    //unsigned long pulDRLength[16];
    //unsigned char ucBuffercount = 0;

    //unsigned long pulTDOVal[3 * 16];
    //unsigned long *pulTempTDOVal;

    unsigned long pulLocalData[2];
    unsigned long pulLocalReceivedData[2];
    unsigned long ulResult = ERR_NO_ERROR;

    //Read the Physical Access registers.
    DLOG2(("ARML_Read968CPReg"));


    //pulTempTDIVal = pulTDIVal;
    //pulTempTDOVal = pulTDOVal;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //C0 ID Register

    pulLocalData[0] = 0x0;
    pulLocalData[1] = ARM968_CP_TEMP[0];

    //pulDRLength[ucBuffercount] = ARM968_SCAN_CHAIN_15_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulLocalData; 

    ulResult = JtagScanDR(ARM968_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //pulDRLength[ucBuffercount] = ARM968_SCAN_CHAIN_15_LENGTH;
    //pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    //pulDataToScan[ucBuffercount++] = pulLocalData;
    //pulTempTDOVal += 3;

    ulResult = JtagScanDR(ARM968_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulReadData[0] = pulLocalReceivedData[0];

    //C1 Control Register
    pulLocalData[0] = 0x0;
    pulLocalData[1] = ARM968_CP_TEMP[1];

    //pulDRLength[ucBuffercount] = ARM968_SCAN_CHAIN_15_LENGTH;
    //pulDataFromScan[ucBuffercount] = NULL;
    //pulDataToScan[ucBuffercount++] = pulLocalData;

    ulResult = JtagScanDR(ARM968_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //pulDRLength[ucBuffercount] = ARM968_SCAN_CHAIN_15_LENGTH;
    //pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    //pulDataToScan[ucBuffercount++] = pulLocalData;
    //pulTempTDOVal += 3;

    ulResult = JtagScanDR(ARM968_SCAN_CHAIN_15_LENGTH, pulLocalData, pulLocalReceivedData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulReadData[1] = pulLocalReceivedData[0];

    //ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;

    //pulReadData[0] = *pulDataFromScan[i+3];
    //pulReadData[1] = *pulDataFromScan[i+3];   

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_WriteARM940_946CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - Register values to be written
       Output: unsigned long - error code ERR_xxx
  Description: This function calling old ARML_Write940_946CPReg  
   Date        Initials    Description
29-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_WriteARM940_946CPReg (unsigned long *pulWriteData)
{
    unsigned long pulReOrOrderedData[16];

    pulReOrOrderedData[0] = pulWriteData[1]; //C2 D Cache Configuration Register value
    pulReOrOrderedData[1] = pulWriteData[2]; //C2 I Cache Configuration Register value
    pulReOrOrderedData[2] = pulWriteData[3]; //C3 Write Buffer Control Register value
    pulReOrOrderedData[3] = pulWriteData[4]; //C5 D Cache Access Permission Register value
    pulReOrOrderedData[4] = pulWriteData[5]; //C5 I Cache Access Permission Register value
    pulReOrOrderedData[5] = pulWriteData[6]; //C6 Protection Region0 Register value
    pulReOrOrderedData[6] = pulWriteData[7]; //C6 Protection Region1 Register value
    pulReOrOrderedData[7] = pulWriteData[8]; //C6 Protection Region2 Register value
    pulReOrOrderedData[8] = pulWriteData[9]; //C6 Protection Region3 Register value
    pulReOrOrderedData[9] = pulWriteData[10]; //C6 Protection Region4 Register value
    pulReOrOrderedData[10] = pulWriteData[11]; //C6 Protection Region5 Register value     
    pulReOrOrderedData[11] = pulWriteData[12]; //C6 Protection Region6 Register value     
    pulReOrOrderedData[12] = pulWriteData[13]; //C6 Protection Region7 Register value
    pulReOrOrderedData[13] = pulWriteData[14]; //C9 D Cache TLB Lock Down Register value
    pulReOrOrderedData[14] = pulWriteData[15]; //C9 I Cache TLB Lock Down Register value   
    pulReOrOrderedData[15] = pulWriteData[0]; //C1 Control Register value
    return ARML_Write940_946CPReg (pulReOrOrderedData);
}  

/****************************************************************************
     Function: ARML_Write940_946CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - Register values to be written
       Output: unsigned long - error code ERR_xxx
  Description: Writes ARM940 / 946 Co-Processor registers back to core
   Date        Initials    Description
15-Aug-2007    SPT       Initial
25-Mar-2008    SPT          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_Write940_946CPReg (unsigned long *pulWriteData)
{
    const unsigned long ARM940_946_CP_TEMP[16] = { ARM940_946_CP_REG_C2_DCACHE_BITS,
        ARM940_946_CP_REG_C2_ICACHE_BITS,
        ARM940_946_CP_REG_C3_WRITEBUF_CTRL,
        ARM940_946_CP_REG_C5_DSPACE_PERMISSIONS,
        ARM940_946_CP_REG_C5_ISPACE_PERMISSIONS,
        ARM940_946_CP_REG_C6_REG0_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG1_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG2_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG3_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG4_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG5_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG6_MEMPROTECTION,
        ARM940_946_CP_REG_C6_REG7_MEMPROTECTION,
        ARM940_946_CP_REG_C9_DCACHE_LOCKDOWN,
        ARM940_946_CP_REG_C9_ICACHE_LOCKDOWN,
        ARM940_946_CP_REG_C1_CONTROL
    };
    unsigned char ucIndex = 0;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;


    //DLOG2(("ARML_Write940_946CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulTempTDIVal = pulTDIVal;
    for ( ucIndex = 0; ucIndex < 16; ucIndex++ )
        {
        pulDRLength[ucBuffercount] = ARM940_946_SCAN_CHAIN_15_LENGTH;
        pulTempTDIVal[0] = pulWriteData[ucIndex];
        pulTempTDIVal[1] = (ARM940_946_CP_TEMP[ucIndex] + ARM940_946CP_WRITEMASK);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        }
    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_WriteARM966CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - Data to be written to CP Registers
       Output: unsigned long - error code ERR_xxx
  Description: Calling ARML_Write966CPReg, 
   Date        Initials    Description
03-Sep-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_WriteARM966CPReg (unsigned long *pulWriteData)
{
    unsigned long pulReOrOrderedData[6];
    pulReOrOrderedData[0] = pulWriteData[1]; // C15 BIST Control Register value
    pulReOrOrderedData[1] = pulWriteData[2]; // C15 BIST I Cache Address Register value
    pulReOrOrderedData[2] = pulWriteData[3]; // C15 BIST I Cache General Register value
    pulReOrOrderedData[3] = pulWriteData[4]; // C15 BIST D Cache Address Register value
    pulReOrOrderedData[4] = pulWriteData[5]; // C15 BIST D Cache General Register value
    pulReOrOrderedData[5] = pulWriteData[0]; // C1 Control Register value
    return ARML_Write966CPReg(pulReOrOrderedData);
}

/****************************************************************************
     Function: ARML_Write966CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - Data to be written
       Output: unsigned long - error code ERR_xxx
  Description: Writes ARM966 Co-Processor registers back to core
   Date        Initials    Description
20-Aug-2007      SPT       Initial
25-Mar-2008    SPT          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_Write966CPReg (unsigned long *pulWriteData)
{
    const unsigned long ARM966_CP_TEMP[6] = { 
        ARM966_CP_REG_C15_1,
        ARM966_CP_REG_C15_2,
        ARM966_CP_REG_C15_3,
        ARM966_CP_REG_C15_4,
        ARM966_CP_REG_C15_5,
        ARM966_CP_REG_C1
    };  
    unsigned char ucIndex = 0;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    DLOG2(("ARML_Write966CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulTempTDIVal = pulTDIVal;
    for ( ucIndex = 0; ucIndex < 6; ucIndex++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulTempTDIVal[0] = (((ARM966_CP_TEMP[ucIndex] & 0x3) << 30) + (pulWriteData[ucIndex] & 0xFFFFFF));
        pulTempTDIVal[1] = (((ARM966_CP_TEMP[ucIndex] & 0xFC) >> 2) + ARM966CP_WRITEMASK); 
        pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
        }
    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}
/****************************************************************************
     Function: ARML_Write968CPReg
     Engineer: Jeenus Challattu Kunnath 
        Input: unsigned long *pulWriteData - Data to be written
       Output: unsigned long - error code ERR_xxx
  Description: Writes ARM966 Co-Processor registers back to core
   Date        Initials    Description
25-Mar-2008    JCK         Initial
****************************************************************************/

unsigned long ARML_Write968CPReg (unsigned long *pulWriteData)
{
    const unsigned long ARM968_CP_TEMP[1] = {ARM968_CP_REG_C1};  
    unsigned long pulLocalData[2];
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_Write968CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulLocalData[0] = (((ARM968_CP_TEMP[0] & 0x3) << 30) + (pulWriteData[0] & 0xFFFFFF));
    pulLocalData[1] = (((ARM968_CP_TEMP[0] & 0xFC) >> 2) + ARM968_CP_WRITEMASK);

    ulResult = JtagScanDR(ARM968_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM920_922DisableCache
     Engineer: Shameerudheen P T
        Input: unsigned long *ulControlValueData - Control Register value.
       Output: unsigned long - error code ERR_xxx
  Description: Turns off the I and D caches on ARM920T / ARM922T
   Date        Initials    Description
26-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM920_922DisableCache (unsigned long *ulControlValueData)
{


    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulData;
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending

    DLOG2(("ARML_ARM920_922DisableCache"));

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Turn off the i and D caches.
    ulData = (ulControlValueData[0] << 1);
    pulLocalData[0] = ulData + ARM920_922CP_PHYSICAL_ACCESS;
    pulLocalData[1] = ARM920_922_CP_REG_C1_CONTROL + ARM920_922CP_WRITEMASK;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_WriteARM920_922CPReg
     Engineer: Shameerudheen P T
        Input: see description of ARML_Write920_922CPReg function
       Output: unsigned long - error code ERR_xxx
  Description: This is a wrapper function of old function ARML_Write920_922CPReg  
   Date        Initials    Description
28-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_WriteARM920_922CPReg (unsigned char *pucRestoreIModeRegisters,
                                         unsigned long *pulWriteData)
{
    unsigned long pulReOrderedWriteData[14];
    pulReOrderedWriteData[0] = pulWriteData[2]; // C2 I Cache Translation Table Base Register
    pulReOrderedWriteData[1] = pulWriteData[1]; //C2 D Cache Translation Table Base Register
    pulReOrderedWriteData[2] = pulWriteData[4]; //C3 I Cache Domain Access Control Register
    pulReOrderedWriteData[3] = pulWriteData[3]; //C3 D Cache Domain Access Control Register
    pulReOrderedWriteData[4] = pulWriteData[6]; //C5 I Cache Fault Status Register
    pulReOrderedWriteData[5] = pulWriteData[5]; //C5 D Cache Fault Status Register
    pulReOrderedWriteData[6] = pulWriteData[8]; //C6 I Cache Fault Address Register
    pulReOrderedWriteData[7] = pulWriteData[7]; //C6 D Cache Fault Address Register
    pulReOrderedWriteData[8] = pulWriteData[12]; //C10 I Cache TLB Lock Down Register
    pulReOrderedWriteData[9] = pulWriteData[11]; //C10 D Cache TLB Lock Down Register
    pulReOrderedWriteData[10] = pulWriteData[10]; //C9 I Cache Lock Down Register
    pulReOrderedWriteData[11] = pulWriteData[9]; //C9 D Cache Lock Down Register 
    pulReOrderedWriteData[12] = pulWriteData[13]; //C13 Process ID Register
    pulReOrderedWriteData[13] = pulWriteData[0]; //C1 Control Register
    return ARML_Write920_922CPReg(pucRestoreIModeRegisters, pulReOrderedWriteData);
}

/****************************************************************************
     Function: ARML_Write920_922CPReg
     Engineer: Shameerudheen P T
        Input: pucRestoreIModeRegisters 
               pulWriteData - data to write into ARM920,922 core CP Registers
       Output: unsigned long - error code ERR_xxx
  Description: Writes ARM920 Co-Processor registers back to core
   Date        Initials    Description
25-Jul-2007      SPT       Initial
02-Jan-2008      SPT       Did correction
****************************************************************************/
unsigned long ARML_Write920_922CPReg (unsigned char *pucRestoreIModeRegisters,
                                      unsigned long *pulWriteData)
{
    unsigned short ulIndex = 0;
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending
    unsigned short i;
    unsigned long ulResult = ERR_NO_ERROR;
    const unsigned long ARM920_922_CP_REG_TEST_MODE_CONSTANTS[10] = {ARM920_922_CP_REG_C2_WRITE_I_TTB,
        ARM920_922_CP_REG_C2_WRITE_D_TTB,
        ARM920_922_CP_REG_C3_WRITE_I_DAC,
        ARM920_922_CP_REG_C3_WRITE_D_DAC,
        ARM920_922_CP_REG_C5_WRITE_I_FSR,
        ARM920_922_CP_REG_C5_WRITE_D_FSR,
        ARM920_922_CP_REG_C6_WRITE_I_FAR,
        ARM920_922_CP_REG_C6_WRITE_D_FAR,
        ARM920_922_CP_REG_C10_WRITE_I_TLB_LOCKDOWN,
        ARM920_922_CP_REG_C10_WRITE_D_TLB_LOCKDOWN
    };

    unsigned long pulTDIVal[3 * 16];
    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulTempTDOVal;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    DLOG2(("ARML_Write920_922CPReg"));
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( *pucRestoreIModeRegisters == 1 )
        {
        //Read the C15 Test state register      

        pulTempTDOVal = pulTDOVal;
        pulTempTDIVal = pulTDIVal;
        ucBuffercount = 0;

        pulLocalData[0] = ARM920_922CP_PHYSICAL_ACCESS;
        pulLocalData[1] = ARM920_922_CP_REG_C15_TEST_STATE;

        pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulLocalData;

        pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulLocalData;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        //Set the interpreted mode bit.
        pulTDOVal[0] |= 0x02;
        pulTDOVal[1] |= 0x80;

        //Write the C15 Test state register back to the core.
        ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulTDOVal, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        //We are now in interpreted mode. Restore the registers now...
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        for ( i = 0; i < 10; i++ )
            {
            pulTempTDIVal = pulTDIVal;
            ucBuffercount = 0;
            //Send the read register command into the core. This transfers the value
            //of the register from the CP15 register into R0.
            //Load register value into R0.

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0R1;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;         

            ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulTempTDIVal;
            ulIndex++;

            //ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
            //if (ulResult != ERR_NO_ERROR)
            //    return ulResult;        

            //ucBuffercount = 0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            //Peform interpreted Write.
            ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            pulLocalData[0] = (ARM920_922_CP_REG_TEST_MODE_CONSTANTS[i] << 1); 
            pulLocalData[1] = (ARM920_922_CP_REG_TEST_MODE_CONSTANTS[i] >> 31);
            ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ucBuffercount = 0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

            ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            //Added as correction
            ulResult = JtagScanIR(&ulARM_JTAG_RESTART,NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = ARML_ARM9WaitForSystemSpeed();
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            //Addes as correction end

            ucBuffercount = 0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            /*ulResult = ARML_PerformARM9Nops(4);
            if (ulResult != ERR_NO_ERROR)
               return ulResult;*/
            }

        //Turn off Interpreted mode
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        //Clear the interpreted mode bit.
        pulTDOVal[0] &= 0xFFFFFFFD;

        //Write the C15 Test state register back to the core.
        ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulTDOVal, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        ulIndex = 10;

    //Restore the Physical Access registers.


    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
    pulTempTDIVal[0] = ((((unsigned long)pulWriteData[ulIndex]) << 1) + ARM920_922CP_PHYSICAL_ACCESS);
    pulTempTDIVal[1] = ((((unsigned long)pulWriteData[ulIndex]) >> 31) + ARM920_922CP_WRITEMASK + 
                        ARM920_922_CP_REG_C9_ICACHE_L_DOWN);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
    pulTempTDIVal[0] = ((((unsigned long)pulWriteData[ulIndex]) << 1) + ARM920_922CP_PHYSICAL_ACCESS);
    pulTempTDIVal[1] = ((((unsigned long)pulWriteData[ulIndex]) >> 31) + ARM920_922CP_WRITEMASK + 
                        ARM920_922_CP_REG_C9_DCACHE_L_DOWN);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
    pulTempTDIVal[0] = ((((unsigned long)pulWriteData[ulIndex]) << 1) + ARM920_922CP_PHYSICAL_ACCESS); 
    pulTempTDIVal[1] = ((((unsigned long)pulWriteData[ulIndex]) >> 31) + ARM920_922CP_WRITEMASK + 
                        ARM920_922_CP_REG_C13_PROCESS_ID);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
    pulTempTDIVal[0] = ((((unsigned long)pulWriteData[ulIndex]) << 1) + ARM920_922CP_PHYSICAL_ACCESS);
    pulTempTDIVal[1] = ((((unsigned long)pulWriteData[ulIndex]) >> 31) + ARM920_922CP_WRITEMASK + 
                        ARM920_922_CP_REG_C1_CONTROL);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ReadARM920_922CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - Data to be Written
       Output: unsigned long - error code ERR_xxx
  Description: This function calls ARML_Read920_922CPReg,
   Date        Initials    Description
05-Sep-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ReadARM920_922CPReg (unsigned long *pulReadData)
{
    unsigned long pulLocalArray[16];
    unsigned long ulResult;
    ulResult = ARML_Read920_922CPReg(pulLocalArray);
    if ( ulResult == ERR_NO_ERROR )
        {
        pulReadData[0] = pulLocalArray[0]; //C0 ID Register
        pulReadData[1] = pulLocalArray[1]; //C0 Cache Type Register
        pulReadData[2] = pulLocalArray[2]; //C1 Control Register
        pulReadData[3] = pulLocalArray[7]; //C2 D �TTB interpreted mode
        pulReadData[4] = pulLocalArray[6]; //C2 I TTB � interpreted mode
        pulReadData[5] = pulLocalArray[9]; //C3 D DAC� interpreted mode
        pulReadData[6] = pulLocalArray[8]; //C3 I DAC� interpreted mode
        pulReadData[7] = pulLocalArray[11]; //C5 D FSR� interpreted mode
        pulReadData[8] = pulLocalArray[10]; //C5 I FSR� interpreted mode
        pulReadData[9] = pulLocalArray[13]; //C6 D FAR� interpreted mode
        pulReadData[10] = pulLocalArray[12]; //C6 I FAR� interpreted mode
        pulReadData[11] = pulLocalArray[3]; //C9 D Cache LockDown Register
        pulReadData[12] = pulLocalArray[4]; //C9 I Cache LockDown Register
        pulReadData[13] = pulLocalArray[15]; //C10 D TLB LOCK DOWN� interpreted
        pulReadData[14] = pulLocalArray[14]; //C10 I TLB LOCK DOWN� interpreted
        pulReadData[15] = pulLocalArray[5]; //C13 Process ID  Register
        }
    return ulResult;   
}


/****************************************************************************
     Function: ARML_Read920_922CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - Data To be written
       Output: unsigned long - error code ERR_xxx
  Description: Reads ARM920 Co-Processor registers from core
   Date        Initials    Description
24-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Read920_922CPReg (unsigned long *pulReadData)
{
    unsigned short usIndex = 0;
    unsigned short i;
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long for sending
    unsigned long pulLocalReceivedData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long
    unsigned long ulResult = ERR_NO_ERROR;



    const unsigned long ARM920_922_CP_REG_CONSTANT[6] = {ARM920_922_CP_REG_C0_ID_REG,
        ARM920_922_CP_REG_C0_CACHE_TYPE,
        ARM920_922_CP_REG_C1_CONTROL,
        ARM920_922_CP_REG_C9_DCACHE_L_DOWN,
        ARM920_922_CP_REG_C9_ICACHE_L_DOWN,
        ARM920_922_CP_REG_C13_PROCESS_ID
    };


    const unsigned long ARM920_922_CP_REG_TEST_MODE_CONSTANTS[10] = {ARM920_922_CP_REG_C2_READ_I_TTB,
        ARM920_922_CP_REG_C2_READ_D_TTB,
        ARM920_922_CP_REG_C3_READ_I_DAC,
        ARM920_922_CP_REG_C3_READ_D_DAC,
        ARM920_922_CP_REG_C5_READ_I_FSR,
        ARM920_922_CP_REG_C5_READ_D_FSR,
        ARM920_922_CP_REG_C6_READ_I_FAR,
        ARM920_922_CP_REG_C6_READ_D_FAR,
        ARM920_922_CP_REG_C10_READ_I_TLB_LOCKDOWN,
        ARM920_922_CP_REG_C10_READ_D_TLB_LOCKDOWN
    };

    unsigned long pulTDIVal[2 * 16];
    unsigned long pulTDOVal[2 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulTempTDOVal;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;


    DLOG2(("ARML_Read920_922CPReg"));                                                              
    //Read the Physical Access registers.
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    usIndex = 0;

    // C0 ID Register
    // C0 Cache Type Register
    // C1 Control Register
    // C9 D Cache LockDown Register
    // C9 I Cache LockDown Register
    // C13 Process ID  Register

    pulTempTDOVal = pulTDOVal;   
    pulTempTDIVal = pulTDIVal;
    for ( i = 0; i < 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulTempTDIVal[0] = ARM920_922CP_PHYSICAL_ACCESS;
        pulTempTDIVal[1] = ARM920_922_CP_REG_CONSTANT[i];
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;

        pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDOVal += 2;
        pulTempTDIVal += 2;
        }

    //Read the C15 Test state register
    //preparing test state
    pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulTempTDIVal[0] = ARM920_922CP_PHYSICAL_ACCESS;
    pulTempTDIVal[1] = ARM920_922_CP_REG_C15_TEST_STATE;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;

    pulDRLength[ucBuffercount] = ARM920_922_SCAN_CHAIN_15_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 6; i++ )
        {
        pulTDOVal[i * 2] = pulTDOVal[i * 2] >> 1;
        pulReadData[usIndex] = pulTDOVal[i * 2] | ((pulTDOVal[(i * 2) + 1] & 0x00000001) << 31);
        usIndex++;
        }

    //Set the interpreted mode bit.
    pulTDOVal[12] |= 0x02;
    pulTDOVal[13] |= 0x80;

    pulLocalReceivedData[0] = pulTDOVal[12];
    pulLocalReceivedData[1] = pulTDOVal[13];

    //Write the C15 Test state register back to the core.
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, &pulTDOVal[12], NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //We are now in interpreted mode. Read the registers now...

    //Send the read register command into the core. This transfers the value
    //of the register from the CP15 register into R0.
    for ( i = 0; i < 10; i++ )
        {
        pulLocalData[0] = (ARM920_922_CP_REG_TEST_MODE_CONSTANTS[i] << 1); 
        pulLocalData[1] = (ARM920_922_CP_REG_TEST_MODE_CONSTANTS[i] >> 31);
        ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ucBuffercount = 0;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0; 

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK; 

        ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        //ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
        //if (ulResult != ERR_NO_ERROR)
        //      return ulResult;

        //newly added
        ulResult = ARML_ARM9WaitForSystemSpeed();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        //newly added

        pulTempTDOVal = pulTDOVal;
        ucBuffercount = 0;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL; 

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL; 

        ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulReadData[usIndex] = pulTDOVal[0];
        usIndex++;

        //ulResult = ARML_PerformARM9Nops(4);
        //if (ulResult != ERR_NO_ERROR)
        //   return ulResult;
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    //Clear the interpreted mode bit.
    pulLocalReceivedData[0] &= 0xFFFFFFFD;

    //Write the C15 Test state register back to the core.
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH, pulLocalReceivedData, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM9RunCacheClean
     Engineer: Shameerudheen P T
        Input: unsigned long *pulData[0] - Start Address of the Routine and 
               unsigned long *pulData[1] - End Address of the Routine
       Output: unsigned long - error code ERR_xxx
  Description: Runs the cache clean routine already loaded to target memory
   Date        Initials    Description
24-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM9RunCacheClean (unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[3 * 16]; //scan chain2 length is 38
    unsigned long *pulTempTDIVal;

    DLOG2(("ARML_ARM9RunCacheClean"));


    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    DLOG2(("End Address 0x%08x",pulData[0]));
    pulTempTDIVal[0] = pulData[0];
    pulTempTDIVal[1] = 0x28;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulTempTDIVal[0] = 0x00000000;
    pulTempTDIVal[1] = 0x29;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulTempTDIVal[0] = 0xFFFFFFFF;
    pulTempTDIVal[1] = 0x2A;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulTempTDIVal[0] = 0xFFFFFFFF;
    pulTempTDIVal[1] = 0x2B;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulTempTDIVal[0] = 0x00000103;
    pulTempTDIVal[1] = 0x2C;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulTempTDIVal[0] = 0x000000F3;
    pulTempTDIVal[1] = 0x2D;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulTempTDIVal[0] = 0x000000D3;
    pulTempTDIVal[1] = 0x000023C8;
    pulTempTDIVal[2] = 0x00;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_CPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_PC;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    DLOG2(("Start Address 0x%08x",pulData[1]));
    ARM9ScanChain1PrepareData(pulTempTDIVal,(pulData[1]+0x4));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_B_PC_4;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

    ulResult = JtagScanDRMultiple(7, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulTempTDIVal[0] = 0x00000000;
    pulTempTDIVal[1] = 0x2C; //WP0 Control Value
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulTempTDIVal[0] = 0x00000000;
    pulTempTDIVal[1] = 0x2D; //WP0 Control Value
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    return JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
}

/****************************************************************************
     Function: ARML_ARM920_922InvalidateICache
     Engineer: Shameerudheen P T
        Input: None
       Output: unsigned long - error code ERR_xxx
  Description: Invalidates the Instruction cache
   Date        Initials    Description
23-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM920_922InvalidateICache(void)
{
    unsigned long pulLocalData[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long
    unsigned long pulLocalDataOut[0x2] = {0x00000000, 0x00000000}; //for scan chain 15, 40 bits long
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_ARM920_922InvalidateICache"));

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_LDMIA_R0_R0R1,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = ARML_PerformARM9Nops(2);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_ARM_NOP_NORMAL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_ARM_NOP_NORMAL,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = ARML_PerformARM9Nops(4);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain (ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Read the current value
    pulLocalDataOut[0] = 0x00000001;
    pulLocalDataOut[1] = 0x0000003C;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalDataOut,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    pulLocalDataOut[0] = 0x00000001;
    pulLocalDataOut[1] = 0x0000003C;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalDataOut,pulLocalData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Set the interpreted bit.
    pulLocalData[0] = (pulLocalData[0] | 0x2);
    pulLocalData[1]  = 0xBC | (pulLocalData[1] & 0x00000001);

    //Write the new value back to the core
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalData,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //The core is now in interpreted mode. 
    //Write the invalidate cache instruction

    //Perform interpreted Write.
    //ulResult = ARML_ARM9SelectScanChain (ARM9_SCAN_CHAIN_CP15);
    //if (ulResult != ERR_NO_ERROR)
    //   return ulResult;
    // 
    pulLocalDataOut[0] = 0xDC0E1E2A;
    pulLocalDataOut[1] = 0x00000001;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalDataOut,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    //ulResult = ARML_PerformARM9Nops(2);
    //if (ulResult != ERR_NO_ERROR)
    //      return ulResult;

    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_STMIA_R0_R0,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_ARM_NOP_CLOCK,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    //ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
    //if (ulResult != ERR_NO_ERROR)
    //         return ulResult;

    //newly added 
    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    //newly added

    ulResult = ARML_PerformARM9Nops(4);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Turn off interpreted mode
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Read the current value
    pulLocalDataOut[0] = 0x00000001;
    pulLocalDataOut[1] = 0x0000003C;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalDataOut,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalDataOut,pulLocalData);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //Set the interpreted bit.
    pulLocalData[0] = (pulLocalData[0] & 0xFFFFFFFD);
    pulLocalData[1] = 0xBC;

    //Write the new value back to the core
    ulResult = JtagScanDR(ARM920_922_SCAN_CHAIN_15_LENGTH,pulLocalData,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_PerformARM9Nops(2);
}

/****************************************************************************
     Function: ARML_ARM9StatusProc
     Engineer: Shameerudheen P T
        Input: unsigned char *pucSelectScanChain2 - Whether the core is already in scan chain 2,
               unsigned char *pucStartedExecution - Whether the core has started execution
               unsigned char *pucCoreExecuting - Return whether the core is still executing
               unsigned char *pucThumbMode - Return whether the core is in Thumb mode
               unsigned char *pucWatchPoint - Return whether the core stopped because of a watchpoint.
       Output: unsigned long - error code ERR_xxx
  Description: Checks the status of the processor
   Date        Initials    Description
22-Jul-2007      SPT       Initial
27-Dec-2007      SPT       Changes made for Watchpoint detection
26-Mar-2008      SPT       Included FPGA JTAG 16 Buffers feature                       
****************************************************************************/
unsigned long ARML_ARM9StatusProc (unsigned char *pucSelectScanChain2,
                                   unsigned char *pucStartedExecution,
                                   unsigned char *pucCoreExecuting,
                                   unsigned char *pucThumbMode,
                                   unsigned char *pucWatchPoint)
{
    unsigned long pulLocalData[3];
    unsigned long ulStatusReg = 0x0;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];

    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;

    DLOG2(("ARML_ARM9StatusProc"));
    *pucThumbMode   = FALSE;
    *pucWatchPoint  = FALSE;

    //Check whether core is in debug mode.
    if ( *pucSelectScanChain2 == 1 )
        {
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    pulTempTDOVal = pulTDOVal;   
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS;   

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulALL_ZEROS;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulStatusReg = pulTDOVal[0];

    DLOG2(("ARML_ARM9StatusProc Status Register 0x%08x",ulStatusReg));

    if ( (ulStatusReg & ARM_DEBUG_MODE) == ARM_DEBUG_MODE )
        {
        *pucCoreExecuting = FALSE;
        //Disable EmbeddedICE watchpoints.   
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
        pulDataToScan[ucBuffercount++] = pulDISABLE_WP0;   

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
        pulDataToScan[ucBuffercount++] = pulDISABLE_WP1;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        *pucCoreExecuting = TRUE;

    if ( (*pucStartedExecution == TRUE) && (*pucCoreExecuting == FALSE) )
        {
        ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH,pulINTDIS_DBGAK_CTRL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( (ulStatusReg & THUMB_MODE_BIT) == THUMB_MODE_BIT )
            {
            *pucThumbMode = TRUE;

            //Check whether a watchpoint caused the core to stop.
            //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_CHECK_THUMB_WP,pulLocalData); //need to check
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D); 
            ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_THUMB_NOP_NORMAL,pulLocalData);
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D); 
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;


            DLOG2(("Watch Point 0x%08",pulLocalData[2]));

            if ( ((pulLocalData[1] & 0x4) == 0x4) &&
                 ((pulLocalData[1] & 0x2) != 0x2) )
                *pucWatchPoint = TRUE;

            }
        else
            {
            *pucThumbMode = FALSE;

            //Check whether a watchpoint caused the core to stop.
            //ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_CHECK_ARM_WP,pulLocalData); //need to check
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D); 
            ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_ARM_NOP_NORMAL,pulLocalData);
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D); 
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            DLOG2(("Watch Point 0x%08X",pulLocalData[2]));

            if ( ((pulLocalData[1] & 0x4) == 0x4) &&
                 ((pulLocalData[1] & 0x2) != 0x2) )
                *pucWatchPoint = TRUE;

            ucBuffercount = 0;
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;   

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;   

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        DLOG2(("Thumb Mode: %d",*pucThumbMode));
        DLOG2(("Watch Point: %d",*pucWatchPoint));
        }
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9ExecuteProc
     Engineer: Shameerudheen P T
        Input: unsigned char *bThumbMode - execute in thumb mode
               pulData - R0 value
       Output: unsigned long - error code ERR_xxx 
  Description: Execute from memory of the core. used in single step, run etc.
   Date        Initials    Description
19-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM9ExecuteProc (unsigned char *bThumbMode, unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;
    pulTempTDIVal = pulTDIVal;

    DLOG2(("ARML_ARM9ExecuteProc"));
    if ( *bThumbMode == 1 )
        {
        if ( FEROCEON == ulTargetType )
            {
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_E28F0001;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_BX_R0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_LDMIA_R0_R0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            ARM9ScanChain1PrepareDataThumb(pulTempTDIVal,*pulData);
            pulDataToScan[ucBuffercount++] = pulTempTDIVal;
            pulTempTDIVal += 3;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            //pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            //pulDataToScan[ucBuffercount++] = pulARM9_THUMB_B_PC_5;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_B_PC_5_F;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_CLOCK;

            //ulResult = JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
            ulResult = JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        else
            {
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_BX_R0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_LDMIA_R0_R0;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            ARM9ScanChain1PrepareDataThumb(pulTempTDIVal,*pulData);
            pulDataToScan[ucBuffercount++] = pulTempTDIVal;
            pulTempTDIVal += 3;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_B_PC_5;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_THUMB_NOP_CLOCK;

            ulResult = JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        }
    else
        {
        pulTempTDIVal = pulTDIVal;
        ucBuffercount = 0;

        if ( FEROCEON == ulTargetType )
            {
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_B_PC_F4;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

            ulResult = JtagScanDRMultiple(5, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        else
            {
            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_B_PC_3;

            pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

            ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        }

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE );
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    if ( FEROCEON == ulTargetType )
        {
        ulResult = JtagScanIR(&ulARM_JTAG_BYPASS, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return JtagScanIR(&ulARM_JTAG_INTEST, NULL);
}

/****************************************************************************
     Function: ARML_ARM9SetupExecuteProc
     Engineer: Shameerudheen P T
        Input: Array of Data - see Opella-XD SW USB Interface Description V 0.0.2 or later
       Output: unsigned long - error code ERR_xxx
  Description: Setting up Core before calling Execute Proc function
   Date        Initials    Description
18-Jul-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM9SetupExecuteProc (unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned short usIndex;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16]; //scan chain2 length is 38
    unsigned long *pulTempTDIVal;

    pulTempTDIVal = pulTDIVal;
    DLOG2(("ARML_ARM9SetupExecuteProc"));
    usIndex = 0;
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal[0] = 0x0; //(unsigned long)pulData[usIndex];
    pulTempTDIVal[1] = 0x20;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    DLOG2(("WP0 Address Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x28;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP0 Address Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x29;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP0 Data Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2A;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP0 Data Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2B;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP0 Ctrl Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2C;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP0 Ctrl Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2D;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP1 Address Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x30;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP1 Address Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x31;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP1 Data Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x32;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP1 Data Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x33;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP1 Ctrl Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x34;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG2(("WP1 Ctrl Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x35;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    ulResult = JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM9WriteRegisters
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - Data to write in to registers 
               for more details see Opella-XD SW USB Interface Description
               V 0.0.2 or later
       Output: unsigned long - error code ERR_xxx 
  Description: Writes all the registers.
   Date        Initials    Description
18-Jul-2007      SPT       Initial
20-Mar-2008      JR        Included FPGA JTAG 16 Buffers feature
24-Mar-2008      SPT       Correction
****************************************************************************/
unsigned long  ARML_ARM9WriteRegisters (unsigned long *pulWriteData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulIndex;
    unsigned long i;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;

    ulIndex = 0;

    DLOG2(("ARML_ARM9WriteRegisters"));

    pulTempTDIVal=pulTDIVal;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (FIQ)....
    ulResult = ARML_ARM9_ChangeMode(ARM9ModeFIQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL; 

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    DLOG2(("FIQ SPSR      = 0x%08x",pulWriteData[ulIndex]));
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_SPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(12, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    for ( i=0; i <= 6; i++ )
        {
        DLOG2(("FIQ R8..R14   = 0x%08x", pulWriteData[ulIndex]));
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]); 
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 3;
        ulIndex++;
        }
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    //cahnge mode (IRQ)......

    ulResult = ARML_ARM9_ChangeMode(ARM9ModeIRQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    DLOG2(("IRQ SPSR      = 0x%08x", pulWriteData[ulIndex]));
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_SPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    DLOG2(("IRQ R13       = 0x%08x", pulWriteData[ulIndex]));
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    DLOG2(("IRQ R14       = 0x%08x", pulWriteData[ulIndex]));
    ulIndex++;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, NULL);

    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

// Change mode (SVC)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM9_ChangeMode(ARM9ModeSupervisor);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    DLOG2(("SVC SPSR      = 0x%08x", pulWriteData[ulIndex]));
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_SPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    DLOG2(("SVC R13       = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    DLOG2(("SVC R14       = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (ABT)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM9_ChangeMode(ARM9ModeAbort);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    DLOG2(("ABT SPSR      = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_SPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    DLOG2(("ABT R13       = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    DLOG2(("ABT R14       = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (UND)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM9_ChangeMode(ARM9ModeUndefined);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    DLOG2(("UND SPSR      = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_SPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    DLOG2(("UND R13       = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    DLOG2(("UND R14       = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

// Change mode (USR)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM9_ChangeMode(ARM9ModeUser);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    for ( i=0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 3;
        DLOG2(("USR R8..R14   = 0x%08x", pulWriteData[ulIndex]));
        ulIndex++;
        }

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    DLOG2(("USR CPSR      = 0x%08x", pulWriteData[ulIndex]));
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_MSR_CPSR_csxf_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0toR7_PC;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    for ( i=0; i <= 8; i++ )
        {
        DLOG2(("USR R0..R8,PC = 0x%08x", pulWriteData[ulIndex]));
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 3;
        ulIndex++;
        }

    ulResult = JtagScanDRMultiple(15, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;


    return JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);

}
/****************************************************************************
     Function: ARML_ARM9ReadRegisters
     Engineer: Shameerudheen P T
        Input: unsigned long *pulResultData - Pointer to store result for more details see
               Opella-XD SW USB Interface Description V 0.0.2 or later
       Output: unsigned long - error code ERR_xxx 
  Description: Read All registers r0-r15 and spsr cpsr of all modes.
Date           Initials    Description
06-Jul-2007    SPT         Initial
15-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM9ReadRegisters(unsigned long *pulResultData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulIndex;
    unsigned long i;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];

    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;

    ulIndex = 0;

    DLOG2(("ARML_ARM9ReadRegisters"));

    pulTempTDOVal = pulTDOVal;   

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0toR7_PC;   

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;   

    for ( i = 0; i <= 8; i++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;
        }   
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_CPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulIndex = 0;

    for ( i = 0;i <= 8; i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        ulIndex++;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulResultData[ulIndex] = *pulDataFromScan[3]; 
    ulIndex += 1;

    // Change mode (USR)....
    ulResult = ARML_ARM9_ChangeMode(ARM9ModeUser);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    //reading r8 to r14 user mode registers
    for ( i = 0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;    
        }

    ulResult = JtagScanDRMultiple(10, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i <= 6; i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        ulIndex += 1;
        }   

    // Change mode (FIQ)....
    ulResult = ARML_ARM9_ChangeMode(ARM9ModeFIQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    //Reading FIQ mode Registers
    for ( i = 0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;
        }

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0;i <= 6;i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        //DLOG2(("read value FIQ R8..R14= 0x%08x", pulTempRetVar[0]));
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulResultData[ulIndex] = *pulDataFromScan[3];
    ulIndex += 1;

    // Change mode (IRQ)....
    ulResult = ARML_ARM9_ChangeMode(ARM9ModeIRQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0;i < 2; i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulResultData[ulIndex] = *pulDataFromScan[3];
    ulIndex += 1;

    ulResult = ARML_ARM9_ChangeMode (ARM9ModeSupervisor);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 2;  i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulResultData[ulIndex] = *pulDataFromScan[3];
    ulIndex += 1;

    // Change mode (Abort)....
    ulResult = ARML_ARM9_ChangeMode (ARM9ModeAbort);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 2;  i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulResultData[ulIndex] = *pulDataFromScan[3];
    ulIndex += 1;

    // Change mode (Undefined)....

    ulResult = ARML_ARM9_ChangeMode (ARM9ModeUndefined);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 2;  i++ )
        {
        pulResultData[ulIndex] = *pulDataFromScan[i+3];
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulResultData[ulIndex] = *pulDataFromScan[3];
    ulIndex += 1;

    // Change mode (Supervisor)....
    return ARML_ARM9_ChangeMode (ARM9ModeSupervisor); 
}

/****************************************************************************
     Function: ARM9_Set_SNVectorAddress
     Engineer: Shameerudheen PT
        Input: unsigned long *pulAddress - Value to be set
       Output: unsigned long - error code ERR_xxx
  Description: Sets the global Safe Non Vector address
   Date        Initials    Description
16-jul-2007      SPT       Initial
***************************************************************************/
unsigned long ARML_ARM9SetSNVectorAddress (unsigned long *pulAddress)
{
    DLOG2(("ARML_ARM9SetSNVectorAddress"));
    ulARM9_SET_SAFE_NON_VEC = *pulAddress;
    return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARML_ARM9CheckDataAbort
     Engineer: Shameerudheen P T
        Input: nsigned char *pucReadData - pointer to Return Status - 0 for 
               supervisor mode 1 otherwise
       Output: unsigned long - error code ERR_xxx
  Description: Checks the CPSR and returns FALSE if the mode is in
               Supervisor mode.
   Date        Initials    Description
13-Jul-2007      SPT       Initial
***************************************************************************/
unsigned long ARML_ARM9CheckDataAbort (unsigned char *pucReadData)
{
    unsigned long pulCPSR[3];
    unsigned long ulMode;

    unsigned long ulResult = ERR_NO_ERROR;

    //DLOG2(("ARML_ARM9CheckDataAbort"));

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9ReadCPSR(&pulCPSR[0]);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulMode = (pulCPSR[0] & 0x0000001F);
    if ( ulMode != 0x13 )
        {
        ulResult = ARML_ARM9_ChangeMode (ARM9ModeSupervisor);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        *pucReadData = 0x1;
        }
    else
        {
        *pucReadData = 0x0;
        }

    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9_Read_Word
     Engineer: Shameerudheen P T
        Input: unsigned long ulAddress - Address of memory location
               unsigned long *pulResultData - Pointer to store result
       Output: unsigned long - error code ERR_xxx
  Description: Read a word from a arm9 core processor memory area .
Date           Initials    Description
06-Jul-2007    SPT          Initial
19-Mar-2008    SPT          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM9ReadWord(unsigned long ulAddress, unsigned long *pulResultData)
{
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long *pulDataFromScan[16];
    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;

    //DLOG2(("ARML_ARM9ReadWord"));

    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddress); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;
    DLOG2(("Address 0x%08x",ulAddress));

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM9SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R1;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R1;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;
    //DLOG2(("Read Value3 0x%08x",pulTempRetVar[0]));

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    *pulResultData = pulTDOVal[0];
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9WriteWord
     Engineer: Shameerudheen P T
        Input: unsigned long *pulAddress - Address of memory location, 
               unsigned long *pulWriteData - Value to be written into that mem.
       Output: unsigned long - error code ERR_xxx 
  Description: Writes a single word to the target memory. set scan chain to 1 (Debug) before calling this function
   Date        Initials    Description
17-Jul-2007      SPT       Initial
18-Jan-2008      SPT       Performance Improvement
****************************************************************************/
unsigned long ARML_ARM9WriteWord(unsigned long *pulAddress,
                                 unsigned long *pulWriteData)
{
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    //DLOG2(("ARML_ARM9WriteWord"));

    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0R1;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,*pulAddress); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;
    DLOG2(("write Address 0x%08x",*pulAddress));

    // Setup the data we need to write...
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,*pulWriteData); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;
    //DLOG2(("write DATA 0x%08x",ulTempVar[0]));

    ulResult = JtagScanDRMultiple(5, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM9SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R1;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM9ReadBlock
     Engineer: Shameerudheen PT
        Input: unsigned long *pulAddress - Start address of block to be read, 
               unsigned long *pulReadData - Pointer to return data
               there are 14 words starting from the address
       Output: unsigned long - error code ERR_xxx
  Description: Reads 14 consecutive memory locations starting from pulAddress
   Date        Initials    Description
16-Jul-2007      SPT       Initial
14-Feb-2008      JCK       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM9ReadBlock(unsigned long *pulAddress, unsigned long *pulReadData)
{
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long *pulDataFromScan[16];
    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;


    //DLOG2(("ARML_ARM9ReadBlock"));
    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,*pulAddress); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM9SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R1toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM9_SCAN_CHAIN_1_LENGTH,pulARM9_STMIA_R0_R1toR14,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    for ( i = 0; i < 14; i++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;
        }

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 14; i++ )
        {
        pulReadData[i] = *pulDataFromScan[i + 2];
        }
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9WriteBlock
     Engineer: Shameerudheen P T
        Input: unsigned long *pulAddress - Block Start address, 
               unsigned long *pulWriteData - Data start pointer
       Output: unsigned long - error code ERR_xxx
  Description: Write 14 consecutive memory locations starting from pulAddress
   Date        Initials    Description
17-Jul-2007      SPT       Initial
18-Jan-2008      SPT       Performance Improvement
11-Feb-2008      SPT       Performance Improvement
14-Feb-2008      JCK       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM9WriteBlock(unsigned long *pulAddress,
                                  unsigned long *pulWriteData)
{
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    //DLOG2(("ARML_ARM9WriteBlock"));

    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,*pulAddress);                //doubt
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;


    DLOG2(("Write address = 0x%08x",*pulAddress));
    // Setup the data we need to write...

    for ( i = 0; i < 12; i++ )
        {
        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
        ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[i]); 
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;      
        pulTempTDIVal += 3;
        DLOG2(("Write Data %d = 0x%08x",i,pulWriteData[i]));
        }


    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[12]); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[13]); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 3;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM9SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount=0;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_STMIA_R0_R1toR14;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART,NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/*
unsigned long ARML_ARM9WriteBlock_ReallyFast(unsigned long *pulAddress,
                                             unsigned long *pulWriteData)
{
   unsigned long ulResult = ERR_NO_ERROR;

   unsigned long pulTDIVal[3 * 16];
   unsigned long *pulTempTDIVal;
   unsigned short usReg;
   unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulTmsTemp;

   DLOG2(("ARML_ARM9WriteBlock_ReallyFast"));

   pulTempTDIVal = pulTDIVal;

   //Re-writing ARML_ARM9WriteBlock not using function JtagScanDRMultiple. So directly setting FPGA registers

   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
  
   put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0toR14[0]);
   put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0toR14[1]);
   put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_LDMIA_R0_R0toR14[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

   put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); //Starts scan

   put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(2)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

   put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,*pulAddress); 
   put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
   pulTempTDIVal += 3;

   DLOG2(("Write address = 0x%08x",*pulAddress));
   // Setup the data we need to write...

   put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[0]); 
   put_wvalue(JTAG_TDI_BUF(4)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(4)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(4)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[1]); 
   put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(5)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[2]); 
   put_wvalue(JTAG_TDI_BUF(6)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(6)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(6)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[3]); 
   put_wvalue(JTAG_TDI_BUF(7)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(7)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(7)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(8), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[4]); 
   put_wvalue(JTAG_TDI_BUF(8)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(8)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(8)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(9), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[5]); 
   put_wvalue(JTAG_TDI_BUF(9)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(9)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(9)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(10), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(10), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[6]); 
   put_wvalue(JTAG_TDI_BUF(10)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(10)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(10)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(11), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(11), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[7]); 
   put_wvalue(JTAG_TDI_BUF(11)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(11)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(11)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(12), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(12), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[8]); 
   put_wvalue(JTAG_TDI_BUF(12)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(12)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(12)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(12));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(13), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(13), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[9]); 
   put_wvalue(JTAG_TDI_BUF(13)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(13)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(13)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(13));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(14), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(14), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[10]); 
   put_wvalue(JTAG_TDI_BUF(14)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(14)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(14)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(14));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_SPARAM_TMS(15), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(15), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
   ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[11]); 
   put_wvalue(JTAG_TDI_BUF(15)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(15)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(15)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(15));
   pulTempTDIVal += 3;
   
   while (get_hvalue(JTAG_JASR) &  0xffff){};  // waiting to sent all buffers
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
      return ERR_ADAPTIVE_CLOCK_TIMEOUT;

   pulTempTDIVal = pulTDIVal;

   ARM9ScanChain1PrepareData(pulTempTDIVal, pulWriteData[12]); 
   put_wvalue(JTAG_TDI_BUF(0)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(0)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(0)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   pulTempTDIVal += 3;

   ARM9ScanChain1PrepareData(pulTempTDIVal, pulWriteData[13]); 
   put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   pulTempTDIVal += 3;

   //no need to set again those values set above
   put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(2)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

   //Setting PC to Safe Address mode intead of ARML_ARM9SetPCtoSafeNonVectorAddress()
   put_wvalue(JTAG_TDI_BUF(3)+0, pulARM9_LDMIA_R0_PC[0]);
   put_wvalue(JTAG_TDI_BUF(3)+4, pulARM9_LDMIA_R0_PC[1]);
   put_wvalue(JTAG_TDI_BUF(3)+8, pulARM9_LDMIA_R0_PC[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

   put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

   put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

   ARM9ScanChain1PrepareData(pulTempTDIVal, ulARM9_SET_SAFE_NON_VEC); 
   put_wvalue(JTAG_TDI_BUF(6)+0, pulTempTDIVal[0]);
   put_wvalue(JTAG_TDI_BUF(6)+4, pulTempTDIVal[1]);
   put_wvalue(JTAG_TDI_BUF(6)+8, pulTempTDIVal[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
   pulTempTDIVal += 3;

   put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));

   put_wvalue(JTAG_TDI_BUF(8)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(8)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(8)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));

   put_wvalue(JTAG_TDI_BUF(9)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(9)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(9)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));

   put_wvalue(JTAG_TDI_BUF(10)+0, pulARM9_ARM_NOP_NORMAL[0]);
   put_wvalue(JTAG_TDI_BUF(10)+4, pulARM9_ARM_NOP_NORMAL[1]);
   put_wvalue(JTAG_TDI_BUF(10)+8, pulARM9_ARM_NOP_NORMAL[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));

   //end set safe address

   put_wvalue(JTAG_TDI_BUF(11)+0, pulARM9_STMIA_R0_R1toR14[0]);
   put_wvalue(JTAG_TDI_BUF(11)+4, pulARM9_STMIA_R0_R1toR14[1]);
   put_wvalue(JTAG_TDI_BUF(11)+8, pulARM9_STMIA_R0_R1toR14[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));

   put_wvalue(JTAG_TDI_BUF(12)+0, pulARM9_ARM_NOP_CLOCK[0]);
   put_wvalue(JTAG_TDI_BUF(12)+4, pulARM9_ARM_NOP_CLOCK[1]);
   put_wvalue(JTAG_TDI_BUF(12)+8, pulARM9_ARM_NOP_CLOCK[2]);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(12));

   put_wvalue(JTAG_SPARAM_TMS(13), tyJtagScanConfig.ulTmsForIR);
   put_wvalue(JTAG_SPARAM_CNT(13), (tyJtagScanConfig.ulIRLength));
   put_wvalue(JTAG_TDI_BUF(13)+0, ulARM_JTAG_RESTART);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(13));
   //end set safe address  

   while (get_hvalue(JTAG_JASR) &  0xffff){};  // waiting to sent all buffers
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
      return ERR_ADAPTIVE_CLOCK_TIMEOUT;
   put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

   
   //replacing ARML_ARM9WaitForSystemSpeed
      //select scan chain
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
   ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
   put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
   put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
   put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

   ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
   put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
   put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_ICE);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

   ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
   put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
   put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
   put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

   while (get_hvalue(JTAG_JASR) &  0xffff){};  // waiting to sent all buffers
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
      return ERR_ADAPTIVE_CLOCK_TIMEOUT;
   put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      //end select scan chain
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

   do
      {
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[0]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulREAD_DBG_STATUS[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

      while (get_hvalue(JTAG_JASR) &  0xffff){};  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;  

      ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
      usCount++;
      if (usCount >= MAX_WAIT_CYCLES)
         {
         return ERR_ARM_MAXIMUM_COUNT_REACHED;
         }

      }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 
   put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
   //end ARML_ARM9WaitForSystemSpeed

   

   //select scan chain
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
   ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
   put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
   put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
   put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

   ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
   put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
   put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

   ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
   put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
   put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
   put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

   while (get_hvalue(JTAG_JASR) &  0xffff){};  // waiting to sent all buffers
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
      return ERR_ADAPTIVE_CLOCK_TIMEOUT;
   put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
   //end select scan chain
   
   return ulResult;
}
*/

/****************************************************************************
     Function: ARML_DebugRequest
     Engineer: Shameerudheen PT.
        Input: unsigned char bDbgrq - 1 for asert bebug pin 0 for deasert
       Output: unsigned long - error code ERR_xxx
  Description: control DBGRQ pin of ARM processor
Date           Initials    Description
30-Jul-2007    SPT         Initial
****************************************************************************/
unsigned long ARML_DebugRequest(unsigned char bDbgrq)
{
    //DLOG2(("ARML_DebugRequest"));
    //DLOG2(("Read before %08x", get_wvalue(JTAG_TPOUT))); 
    // control DBGRQ
    if ( bDbgrq )
        put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | JTAG_TPA_DBGRQ);
    else
        put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~JTAG_TPA_DBGRQ);

    us_wait(5); 
    DLOG2(("Read after %08x", get_wvalue(JTAG_TPOUT))); // wait 5 us
    return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARML_GetDebugAcknowledge
     Engineer: Vitezslav Hola
        Input: unsigned char *pbDbgack - pointer to debug acknowledge status
       Output: unsigned long - error code ERR_xxx
  Description: read DBGACK pin of ARM processor
Date           Initials    Description
20-Nov-2007    VH          Initial
****************************************************************************/
unsigned long ARML_GetDebugAcknowledge(unsigned char *pbDbgack)
{
    unsigned char bLocStatus = 0;

    DLOG2(("ARML_GetDebugAcknowledge"));
    // read DBGACK pin
    if ( get_wvalue(JTAG_TPIN) & JTAG_TPA_DBGACK )
        bLocStatus = 1;
    DLOG2(("ARML_GetDebugAcknowledge: DBGACK status %d", bLocStatus));
    if ( pbDbgack != NULL )
        *pbDbgack = bLocStatus;
    return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARML_InitTarget
     Engineer: Roshan T.R
        Input: unsigned long ulTarget - Target type
       Output: unsigned long - error code ERR_xxx
  Description: Initialise the target type in diskwre
Date           Initials    Description
12-Mar-2010    RTR         Initial
****************************************************************************/
unsigned long ARML_InitTarget(unsigned long ulTarget)
{
    //DLOG2(("ARML_InitTarget"));

    if ( FEROCEON == ulTarget )
        {
        ulTargetType = FEROCEON;
        }
    else
        {
        ulTargetType = ISCORE;
        }
    return ERR_NO_ERROR;
}
/****************************************************************************
    Function: ARML_ARM9WriteByte
    Engineer: Shameerudheen P T
       Input: unsigned char ucWriteData - pointer to data to write
              unsigned long ulAddress - address to Write target memory 
      Output: unsigned long - error code ERR_xxx
 Description: Writes a byte to ARM9 target memory
   Date        Initials    Description
14-Aug-2007      SPT       Initial
18-Jan-2008      SPT       Performance Improvement
****************************************************************************/
unsigned long ARML_ARM9WriteByte(unsigned char ucWriteData, unsigned long ulAddress)
{
    unsigned long ulResult = ERR_NO_ERROR;
    //unsigned long ulTempvar[3];
    unsigned long ulData;

    unsigned long pulTDIVal[3 * 16];
    unsigned long *pulTempTDIVal;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    DLOG2(("ARML_ARM9WriteByte"));

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_LDMIA_R0_R0R1;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_NORMAL;

    // Setup the data we need to write...
    ulData = ucWriteData;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulData); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;

    // Setup the data we need to write...
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddress); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;

    ulResult = JtagScanDRMultiple(5, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM9SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_STRB_R0_R1;

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM9_ARM_NOP_CLOCK;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM9WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_WriteARM926CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - pointer to Write Data
       Output: unsigned long - error code ERR_xxx
  Description: Calls old ARML_W rite926CPReg function
   Date        Initials    Description
28-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_WriteARM926CPReg (unsigned long *pulWriteData)
{
    unsigned long pulReorderedWriteData[12];
    pulReorderedWriteData[0] = pulWriteData[1]; //C2 Translation Table Base Register value
    pulReorderedWriteData[1] = pulWriteData[2]; //C3 Domain Access Control Register value
    pulReorderedWriteData[2] = pulWriteData[3]; //C5 D Cache Fault Status Register value
    pulReorderedWriteData[3] = pulWriteData[4]; //C5 I Cache Fault Status Register value
    pulReorderedWriteData[4] = pulWriteData[5]; //C6 Fault Address Register value
    pulReorderedWriteData[5] = pulWriteData[6]; //C9 D Cache Lock Down Register value
    pulReorderedWriteData[6] = pulWriteData[7]; //C9 I Cache Lock Down Register value
    pulReorderedWriteData[7] = pulWriteData[8]; //C9 D Cache TCM Register value
    pulReorderedWriteData[8] = pulWriteData[9]; //C9 I Cache TCM Register value
    pulReorderedWriteData[9] = pulWriteData[10]; //C10 TLB Lock Down Register value  
    pulReorderedWriteData[10] = pulWriteData[11]; //C13 Process ID Register value 
    pulReorderedWriteData[11] = pulWriteData[0]; //C1 Control Register value

    return ARML_Write926CPReg (pulReorderedWriteData);
}

/****************************************************************************
     Function: ARML_Write926CPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - pointer to Write Data
       Output: unsigned long - error code ERR_xxx
  Description: Writes ARM926 Co-Processor registers
   Date        Initials    Description
14-Aug-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_Write926CPReg (unsigned long *pulWriteData)
{

    const unsigned long ARM926_CP_TEMP[12] = {
        ARM926_CP_REG_C2_TTB,
        ARM926_CP_REG_C3_DAC,
        ARM926_CP_REG_C5_DFSR,
        ARM926_CP_REG_C5_IFSR,
        ARM926_CP_REG_C6_FAR,
        ARM926_CP_REG_C9_DCACHE_L_DOWN,
        ARM926_CP_REG_C9_ICACHE_L_DOWN,
        ARM926_CP_REG_C9_DCACHE_TCM,
        ARM926_CP_REG_C9_ICACHE_TCM,
        ARM926_CP_REG_C10_TLB,
        ARM926_CP_REG_C13_PROCESS_ID,
        ARM926_CP_REG_C1_CONTROL
    };
    unsigned char ucIndex;
    unsigned long pulLocalData[2];
    unsigned long pulLocalReceivedData[2];
    unsigned long pucLocalZeros[0x8] = {0};
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned short usCount;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( ucIndex=0; ucIndex < 12; ucIndex ++ )
        {
        pulLocalData[0] = pulWriteData[ucIndex];
        pulLocalData[1] = ((ARM926_CP_TEMP[ucIndex] << 1) + 
                           ARM926_INITIATE_NEW_CP_ACCESS +
                           ARM926_CP_ACCESS_WRITE_MASK);
        ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pulLocalData, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        pulLocalData[0] = 0x0;
        pulLocalData[1] = 0x0;
        usCount = 0;
        do
            {
            ulResult = JtagScanDR(ARM926_SCAN_CHAIN_15_LENGTH, pucLocalZeros, pulLocalReceivedData);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            usCount++;
            if ( usCount >= MAX_WAIT_CYCLES )
                {
                DLOG2(("CP Count Reached"));
                return ERR_ARM_CP926_ACCESS_TIME_OUT;
                }
            }
        while ( (pulLocalReceivedData[1] & 0x1) != 0x1 );
        }

    return ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARM9ReadWriteDCC
     Engineer: Shameerudheen
        Input: unsigned char *pucDataToWrite - Is there Data to Write, 
               unsigned long *pulWriteData - the Data to write
               unsigned char *pucDataWriteSuccessful - Return is Write successful, 
               unsigned char *pucDataReadFromCore - return is there data from core, 
               unsigned long *pulReadData - Return Data from core
       Output: unsigned long - error code ERR_xxx
  Description: Read and Write to Debug communication Channel
   Date        Initials    Description
10-Sep-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_ARM9ReadWriteDCC (unsigned char *pucDataToWrite,
                                            unsigned long *pulWriteData,
                                            unsigned char *pucDataWriteSuccessful,
                                            unsigned char *pucDataReadFromCore,
                                            unsigned long *pulReadData)
{
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long pulTDOVal[2 * 16];
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDOVal;
    unsigned long *pulTempTDIVal;

    unsigned char ucDCCControlReg;
    unsigned long ulResult = ERR_NO_ERROR;

    pulTempTDOVal = pulTDOVal;
    pulTempTDIVal = pulTDIVal;

    *pucDataWriteSuccessful = FALSE;
    *pucDataReadFromCore    = FALSE;

    ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

//Read the DCC control register. 

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulREAD_DCC_CTRL; 

    pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
    pulTempTDOVal += 2;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ucDCCControlReg = (pulTDOVal[0] & 0x000000FF);

    if ( *pucDataToWrite == TRUE )
        {
        pulTempTDIVal = pulTDIVal;
        ucBuffercount = 0;
        if ( (ucDCCControlReg & DCC_WRITE_MASK) != DCC_WRITE_MASK )
            {
            pulTempTDIVal[0] = pulWriteData[0];
            pulTempTDIVal[1] = WRITE_TO_DCC_DATA_REG;
            ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulTempTDIVal, NULL);
            pulTempTDIVal += 2;
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            *pucDataWriteSuccessful = TRUE;
            }
        else
            *pucDataWriteSuccessful = FALSE;
        }

    if ( (ucDCCControlReg & DCC_READ_MASK) == DCC_READ_MASK )
        {
        pulTempTDOVal = pulTDOVal;
        ucBuffercount = 0;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DCC_DATA;

        pulDRLength[ucBuffercount] = ARM9_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
        pulTempTDOVal += 2;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulReadData[0] = pulTDOVal[0];
        *pucDataReadFromCore = TRUE;
        }
    return ulResult;
}
/*
static unsigned long ARML_ARM9ReadWriteDCC1 (unsigned char *pucDataToWrite,
                                            unsigned long *pulWriteData,
                                            unsigned char *pucDataWriteSuccessful,
                                            unsigned char *pucDataReadFromCore,
                                            unsigned long *pulReadData)
{

   unsigned long pulTempData[2]; 
   unsigned long pulTempReturnData[2]; 
   unsigned char ucDCCControlReg;
   unsigned long ulResult = ERR_NO_ERROR;

   *pucDataWriteSuccessful = FALSE;
   *pucDataReadFromCore    = FALSE;

   ulResult = ARML_ARM9SelectScanChain(ARM9_SCAN_CHAIN_ICE);
   if (ulResult != ERR_NO_ERROR)
      return ulResult;

   //Read the DCC control register. 
   ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulREAD_DCC_CTRL, NULL);
   if (ulResult != ERR_NO_ERROR)
      return ulResult;
   ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulALL_ZEROS, pulTempReturnData);
   if (ulResult != ERR_NO_ERROR)
      return ulResult;

   ucDCCControlReg = (pulTempReturnData[0] & 0x000000FF);

   if (*pucDataToWrite == TRUE)
      {
      if ((ucDCCControlReg & DCC_WRITE_MASK) != DCC_WRITE_MASK)
         {
         pulTempData[0] = pulWriteData[0];
         pulTempData[1] = WRITE_TO_DCC_DATA_REG;
         ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulTempData, NULL);
         if (ulResult != ERR_NO_ERROR)
            return ulResult;
         *pucDataWriteSuccessful = TRUE;
         }
      else
         *pucDataWriteSuccessful = FALSE;
      }

   if ((ucDCCControlReg & DCC_READ_MASK) == DCC_READ_MASK)
      {
      ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulREAD_DCC_DATA, NULL);   
      if (ulResult != ERR_NO_ERROR)
         return ulResult;
      ulResult = JtagScanDR(ARM9_SCAN_CHAIN_2_LENGTH, pulALL_ZEROS, pulTempReturnData);
      if (ulResult != ERR_NO_ERROR)
         return ulResult;
      pulReadData[0] = pulTempReturnData[0];
      *pucDataReadFromCore = TRUE;
      }
   return ulResult;
}
*/
//*********************** ARM7 Seciton **************************************

/****************************************************************************
    Function: ARML_PerformARM7Nops
    Engineer: Shameerudheen P T
       Input: unsigned char ucCount - number of NOPS to execute
      Output: unsigned long - error code ERR_xxx
 Description: Performs an ARM7 NOP
   Date        Initials    Description
21-Aug-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_PerformARM7Nops(unsigned char ucCount)
{
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;
    //DLOG2(("ARML_PerformARM7Nops"));

    for ( i=0; i < ucCount; i++ )
        {
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_NORMAL, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7SC1PrepareData
    Engineer: Shameerudheen P T
       Input: unsigned long *pulVar - pointer to prepared data, 
              unsigned long ulData - data to be prepared
      Output: None
 Description: Data is preparing for Scan chain 1 of ARM7.
   Date        Initials    Description
22-Aug-2007      SPT       Initial
****************************************************************************/

static void ARML_ARM7SC1PrepareData(unsigned long *pulVar, unsigned long ulData)
{
    unsigned char i;
    unsigned long ulReverse = 0;
    //DLOG2(("ARML_ARM7SC1PrepareData"));
    for ( i=0; i < 32; i++ )
        {
        ulReverse <<= 1;
        ulReverse  |= ulData & 1;;
        ulData >>= 1;
        }
    pulVar[0] = ulReverse << 1;
    pulVar[1] = ulReverse >> 31;
}

/****************************************************************************
    Function: ARML_ARM7SC1FilterData
    Engineer: Shameerudheen P T
       Input: unsigned long *pulData - pointer to data to be filtered
      Output: filtered data
 Description: Data is filtering from ARM7 Scan chain 1 Data out.
   Date        Initials    Description
22-Aug-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_ARM7SC1FilterData(unsigned long *pulData)
{
    unsigned char i;
    unsigned long ulReverse1;
    unsigned long ulReverse2 = 0;

    //DLOG2(("ARML_ARM7SC1FilterData"));

    ulReverse1 = pulData[0] >> 1;
    ulReverse1 = (ulReverse1 | ((pulData[1] & 0x1) << 31)); 
    for ( i=0; i < 32; i++ )
        {
        ulReverse2 <<= 1;
        ulReverse2  |= ulReverse1 & 1;;
        ulReverse1 >>= 1;
        }
    return ulReverse2;
}

/****************************************************************************
    Function: ARM7_WaitForSystemSpeed
    Engineer: Shameerudheen P T
       Input: None
      Output: unsigned long - error code ERR_xxx
 Description: Waits until the system speed operation completes.
   Date        Initials    Description
22-Aug-2007      SPT       Initial
14-Feb-2008      SPT       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
static unsigned long ARM7_WaitForSystemSpeed(void)
{
    unsigned long pulReadData[0x2];
    unsigned long usCount;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[2];
    unsigned long pulDRLength[2];
    unsigned char ucBuffercount = 0;
    unsigned long *pulDataFromScan[2];

    DLOG2(("ARM7_WaitForSystemSpeed"));
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    usCount = 0x0;

    do
        {
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulReadData;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        usCount++;
        DLOG2(("Count 0x%08x",usCount));
        if ( usCount >= MAX_WAIT_CYCLES )
            {
            DLOG2(("Count Reached"));
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
        }  
    while ( (pulReadData[0] & ARM_DEBUG_MODE) != ARM_DEBUG_MODE );
    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7SetPCtoSafeNonVectorAddress
    Engineer: Shameerudheen P T
       Input: None
      Output: unsigned long - error code ERR_xxx
 Description: Sets the PC to the safe non vector address
   Date        Initials    Description
24-Aug-2007      SPT       Initial
13-Feb-2008      SPT       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
static unsigned long ARML_ARM7SetPCtoSafeNonVectorAddress(void)
{
    unsigned long pulTDIVal[2];
    unsigned long *pulDataToScan[10];
    unsigned long pulDRLength[10];
    unsigned char ucBuffercount = 0; 
    unsigned char NoNops = 0;


    for ( NoNops = 0; NoNops < 2; NoNops++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        }


    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;   
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_PC;

    for ( NoNops = 0; NoNops < 2; NoNops++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        }

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
    pulDataToScan[ucBuffercount++] = pulTDIVal;

    for ( NoNops = 0; NoNops < 4; NoNops++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        }
    return JtagScanDRMultiple(10, pulDRLength, pulDataToScan, NULL);
}

/****************************************************************************
    Function: ARML_ARM7ReadWord
    Engineer: Shameerudheen P T
       Input: unsigned long ulAddress - address to read target memory from
              unsigned long *pulReadData - pointer to return data
      Output: unsigned long - error code ERR_xxx
 Description: Reads a word from ARM7 target memory
   Date        Initials    Description
21-Aug-2007      SPT       Initial
26-Mar-2008      JY        Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7ReadWord(unsigned long ulAddress, unsigned long *pulReadData)
{
    unsigned long ulResult;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[2 * 16];
    unsigned long *pulTempTDOVal;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    pulTempTDOVal = pulTDOVal;
    pulTempTDIVal = pulTDIVal;

    DLOG2(("ARML_ARM7ReadWord"));
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    DLOG2(("Address 0x%08x",ulAddress));
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddress);
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM7SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        {
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_CLOCK;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R1;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARM7_WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R1;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 2;

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG4(("Date read [1] = 0x%08x, [0] = 0x%08x", pulTDOVal[1], pulTDOVal[0]));
    *pulReadData = ARML_ARM7SC1FilterData(pulTDOVal);
    DLOG2(("Date read = 0x%08x", *pulReadData));
    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7ReadBlock
    Engineer: Shameerudheen P T
       Input: unsigned long ulAddress - address to read target memory from
              unsigned long *pulReadData - pointer to return data
      Output: unsigned long - error code ERR_xxx
 Description: Reads a Block of 14 words from ARM7 target memory
   Date        Initials    Description
24-Aug-2007      SPT       Initial
14-Feb-2008      SPT       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7ReadBlock(unsigned long ulAddress, unsigned long *pulReadData)
{
    unsigned long ulResult;
    unsigned char i;

    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long *pulDataFromScan[16];
    unsigned long pulTDOVal[2 * 16];
    unsigned long *pulTempTDOVal;


    DLOG2(("ARML_ARM7ReadBlock"));

    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddress);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;   
    pulTempTDIVal += 2;

    ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM7SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        {
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_CLOCK;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R1toR14;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARM7_WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_STMIA_R0_R1toR14, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;


    for ( i=0; i < 14; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        pulTempTDOVal += 2;
        }

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i<14; i++ )
        {
        pulReadData[i] = ARML_ARM7SC1FilterData(pulDataFromScan[i+2]);
        }

    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7WriteWord
    Engineer: Shameerudheen PT
       Input: unsigned long ulAddress - address to Write target memory 
              unsigned long ulWriteData - pointer to data to write
      Output: unsigned long - error code ERR_xxx
 Description: Writes a word from ARM7 target memory
   Date        Initials    Description
24-Aug-2007      SPT       Initial
18-Jan-2008      SPT       Performance Improvement
27-Mar-2008      JY       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7WriteWord(unsigned long ulAddress, unsigned long ulWriteData)
{
    unsigned long ulResult;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    pulTempTDIVal = pulTDIVal;

    DLOG2(("ARML_ARM7WriteWord"));

    /*ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if (ulResult != ERR_NO_ERROR)
        return ulResult;*/

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0R1;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    DLOG2(("Address 0x%08x",ulAddress));
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddress);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    DLOG2(("Write Data 0x%08x",ulWriteData));
    // Setup the data we need to write...
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulWriteData);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    ulResult = JtagScanDRMultiple(5, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM7SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        {
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }


    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_CLOCK;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R1;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARM7_WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ERR_NO_ERROR;

    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);

}

/****************************************************************************
    Function: ARML_ARM7WriteBlock
    Engineer: Shameerudheen P T
       Input: unsigned long ulAddress - address to Write target memory 
              unsigned long *pulWriteData - pointer to data to write
      Output: unsigned long - error code ERR_xxx
 Description: Writes a Block of 14 words from ARM7 target memory
   Date        Initials    Description
24-Aug-2007      SPT       Initial
11-Feb-2008      SPT       Modified for Performance improvement
13-Feb-2008      SPT       Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7WriteBlock(unsigned long ulAddress, unsigned long *pulWriteData)
{
    unsigned long ulResult;
    unsigned char i;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    DLOG2(("ARML_ARM7WriteBlock"));

    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddress);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;


    // Setup the data we need to write...
    for ( i=0; i < 12; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[i]);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        }
    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[12]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[13]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM7SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        {
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount=0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_CLOCK;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R1toR14;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARM7_WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM7ChangeMode
     Engineer: Shameerudheen P T
        Input: ARM7Mode tyARM7Mode - Mode ARM7ModeUser,ARM7ModeFIQ,ARM7ModeIRQ,ARM7ModeSupervisor,
               ARM7ModeAbort,ARM7ModeUndefined
       Output: unsigned long - error code ERR_xxx
  Description: Changes to the specified mode
   Date        Initials    Description
24-Aug-2007      SPT       Initial
****************************************************************************/

static unsigned long ARML_ARM7ChangeMode(ARM7Mode tyARM7Mode)
{

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;


    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    switch ( tyARM7Mode )
        {
        case ARM7ModeUser:
            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM7_MODE_USER;
            break;
        case ARM7ModeFIQ:
            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM7_MODE_FIQ;
            break;
        case ARM7ModeIRQ:
            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM7_MODE_IRQ;
            break;
        case ARM7ModeAbort:
            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM7_MODE_ABORT;
            break;
        case ARM7ModeSupervisor:
            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM7_MODE_SUPERVISOR;
            break;
        case ARM7ModeUndefined:
            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataToScan[ucBuffercount++] = pulARM7_MODE_UNDEFINED;
            break;
        }

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_CPSRc_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    return JtagScanDRMultiple(11, pulDRLength, pulDataToScan, NULL);   
}

/****************************************************************************
     Function: ARML_ARM7ReadRegisters
     Engineer: Shameerudhee P T
        Input: unsigned long *pulReadData - Pointer to return data for more 
               details of data structure Opella-XD SW USB Interface 
               Description V 0.0.2 or later
       Output: unsigned long - error code ERR_xxx
  Description: Reads all the registers from ARM7.
   Date        Initials    Description
24-Aug-2007    SPT       Initial
26-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7ReadRegisters (unsigned long *pulReadData)
{
    unsigned char i;
    unsigned long ulIndex;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];

    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;   

    DLOG2(("ARML_ARM7ReadRegisters"));

    pulTempTDOVal = pulTDOVal;

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0toR7_PC;   

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulIndex = 0;
    for ( i = 0; i <= 8; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;

        }

// read CPSR

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_CPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;  

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i<= 8; i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[3]);
    ulIndex++;

    // Change mode (USR)....
    ulResult = ARML_ARM7ChangeMode(ARM7ModeUser);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    for ( i=0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;
        }

    ulResult = JtagScanDRMultiple(10, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i<= 6; i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex ++;
        }


    // Change mode (FIQ)....
    ulResult = ARML_ARM7ChangeMode(ARM7ModeFIQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    for ( i=0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
        pulTempTDOVal += 3;
        }

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;   

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i<= 6; i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex ++;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[3]);
    ulIndex ++;

    // Change mode (IRQ)....
    ulResult = ARML_ARM7ChangeMode(ARM7ModeIRQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;


    for ( i = 0;i < 2; i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[3]);
    ulIndex += 1;
    // Change mode (SVC)....
    ulResult = ARML_ARM7ChangeMode(ARM7ModeSupervisor);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R13toR14;   

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 2;  i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex ++;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[3]);
    ulIndex += 1;

    // Change mode (ABO)....
    ulResult = ARML_ARM7ChangeMode(ARM7ModeAbort);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 2;  i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex ++;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[3]);
    ulIndex ++;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;   

    // Change mode (UND)....
    ulResult = ARML_ARM7ChangeMode(ARM7ModeUndefined);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_SPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;

    ulResult = JtagScanDRMultiple(9, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i = 0; i < 2;  i++ )
        {
        pulReadData[ulIndex] = ARML_ARM7SC1FilterData(pulDataFromScan[i+3]);
        ulIndex += 1;
        }

    pulTempTDOVal = pulTDOVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulReadData[ulIndex] =  ARML_ARM7SC1FilterData(pulDataFromScan[3]);
    ulIndex += 1;

    // Change mode (SVC)....
    return ARML_ARM7ChangeMode(ARM7ModeSupervisor);
}

/****************************************************************************
     Function: ARML_ARM7WriteRegisters
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - point to data to be written into 
               registers for more details see Opella-XD SW USB Interface 
               Description V 0.0.2 or later
       Output: unsigned long - error code ERR_xxx
  Description: Writes all the registers.
Date          Initials    Description
27-Aug-2007   SPT         Initial
26-Mar-2008   JY          Included FPGA JTAG 16 Buffers feature
27-Mar-2008   SPT         Correction
****************************************************************************/
unsigned long ARML_ARM7WriteRegisters (unsigned long *pulWriteData)
{
    unsigned char i;
    unsigned long ulIndex;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    ulIndex = 0;
    pulTempTDIVal = pulTDIVal;

    DLOG2(("ARML_ARM7WriteRegisters"));
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (FIQ)....

    ulResult = ARML_ARM7ChangeMode (ARM7ModeFIQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_SPSRcsxf_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    for ( i=0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        ulIndex++;
        }

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(11, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (IRQ)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM7ChangeMode (ARM7ModeIRQ);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_SPSRcsxf_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;


    // Change mode (SVC)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM7ChangeMode (ARM7ModeSupervisor);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_SPSRcsxf_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (ABT)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM7ChangeMode (ARM7ModeAbort);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_SPSRcsxf_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (UND)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM7ChangeMode (ARM7ModeUndefined);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_SPSRcsxf_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R13toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(6, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Change mode (USR)....

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    ulResult = ARML_ARM7ChangeMode (ARM7ModeUser);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R8toR14;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    for ( i=0; i <= 6; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        ulIndex++;
        }

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    ulIndex++;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MSR_CPSRcsxf_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0toR7_PC;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(14, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    for ( i=0; i <= 8; i++ )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulIndex]);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;
        ulIndex++;
        }

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    return  JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
}

/****************************************************************************
     Function: ARML_ARM7StatusProc
     Engineer: Shameerudheen P T
        Input: unsigned char *pucSelectScanChain2 - Whether the core is already in scan chain 2,
               unsigned char *pucStartedExecution - Whether the core has started execution
               unsigned char *pucStopProblem - return Stop Problem
               unsigned char *pucCoreExecuting - Return whether the core is still executing
               unsigned char *pucThumbMode - Return whether the core is in Thumb mode
               unsigned char *pucWatchPoint - Return whether the core stopped because of a watchpoint.
       Output: unsigned long - error code ERR_xxx
  Description: Checks status of processor
   Date        Initials    Description
27-Aug-2007    SPT       Initial
26-Mar-2008    JY        Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7StatusProc (unsigned char *pucSelectScanChain2, //input
                                   unsigned char *pucStartedExecution, //input
                                   unsigned char *pucStopProblem,
                                   unsigned char *pucCoreExecuting,
                                   unsigned char *pucThumbMode,
                                   unsigned char *pucWatchPoint)
{
    unsigned long pulLocalData[3];
    unsigned long ulStatusReg  = 0x0;
    unsigned long ulCount = 0x0;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal; 

    pulTempTDOVal = pulTDOVal;

    DLOG2(("ARML_ARM7StatusProc"));
    *pucStopProblem = FALSE;
    *pucThumbMode   = FALSE;
    *pucWatchPoint  = FALSE;

    //Check whether core is in debug mode.
    if ( *pucSelectScanChain2 == 1 )
        {
        ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ulCount = 0x0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS; 

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulStatusReg = pulTDOVal[0];
    DLOG2(("ARML_ARM7StatusProc Status Register %0x08x",ulStatusReg));


    //   if ((ulStatusReg & ARM_DBGACK_BIT) == ARM_DBGACK_BIT)
    if ( (ulStatusReg & ARM_DEBUG_MODE) == ARM_DEBUG_MODE )
        {
        ucBuffercount = 0;
        pulTempTDOVal = pulTDOVal;
        *pucCoreExecuting = FALSE;
        //Disable EmbeddedICE watchpoints.

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulDISABLE_WP0;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulDISABLE_WP1;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        *pucCoreExecuting = TRUE;

    if ( (*pucStartedExecution == TRUE) && (*pucCoreExecuting == FALSE) )
        {

        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_2_LENGTH,pulINTDIS_DBGAK_CTRL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( (ulStatusReg & THUMB_MODE_BIT) == THUMB_MODE_BIT )
            {

            DLOG2(("THUMB MODE"));
            *pucThumbMode = TRUE;

            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);
            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH,pulARM7_THUMB_NOP_NORMAL,pulLocalData);
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            DLOG2(("Watch Point 0x%08X",pulLocalData[0]));
            *pucWatchPoint = (pulLocalData[0] & 0x1);

            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH,pulARM7_THUMB_NOP_NORMAL,NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }

        else
            {
            DLOG2(("ARM MODE"));
            *pucThumbMode = FALSE;

            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);
            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH,pulARM7_ARM_NOP_NORMAL,pulLocalData);
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            DLOG2(("Watch Point 0x%08X",pulLocalData[0]));

            *pucWatchPoint = (pulLocalData[0] & 0x1);
            ulResult = ARML_PerformARM7Nops(1);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }

        DLOG2(("Thumb Mode: %d",*pucThumbMode));
        DLOG2(("Watch Point: %d",*pucWatchPoint));

        //Check if core has re-entered debug mode properly. 

        ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ucBuffercount = 0;
        pulTempTDOVal = pulTDOVal;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
        pulTempTDOVal += 3;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        ulStatusReg = pulTDOVal[0];


        if ( (ulStatusReg & ARM_DEBUG_MODE) != ARM_DEBUG_MODE )
            {
            DLOG3(("ARM 7 STOP Problem entered"));
            *pucStopProblem = TRUE;
            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_2_LENGTH,pulITDS_DBGAKRQ_CTRL,NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            ulResult = JtagScanIR(&ulARM_JTAG_INTEST, NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;

            //Wait for core to re-enter debug mode.
            do
                {
                ucBuffercount = 0;
                pulTempTDOVal = pulTDOVal;

                pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
                pulDataFromScan[ucBuffercount] = NULL;
                pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS;

                pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
                pulDataFromScan[ucBuffercount] = pulTempTDOVal;
                pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
                pulTempTDOVal += 3;
                ulCount++;

                ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
                if ( ulResult != ERR_NO_ERROR )
                    return ulResult;

                if ( ulCount >= MAX_WAIT_CYCLES )
                    return ERR_ARM_MAXIMUM_COUNT_REACHED;

                }  
            while ( (pulTDOVal[0] & ARM_DEBUG_MODE) != ARM_DEBUG_MODE );


            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_2_LENGTH,pulINTDIS_DBGAK_CTRL,NULL);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }

        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_2_LENGTH,pulINTDIS_DBGAK_CTRL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        if ( (ulStatusReg & THUMB_MODE_BIT) == THUMB_MODE_BIT )
            {
            *pucThumbMode = TRUE;
            }
        else
            {
            ucBuffercount = 0;
            pulTempTDOVal = pulTDOVal;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        }
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM7SStatusProc
     Engineer: Shameerudheen P T
        Input: unsigned char *pucSelectScanChain2 - Whether the core is already in scan chain 2,
               unsigned char *pucStartedExecution - Whether the core has started execution
               unsigned char *pucStopProblem - return Stop Problem
               unsigned char *pucCoreExecuting - Return whether the core is still executing
               unsigned char *pucThumbMode - Return whether the core is in Thumb mode
               unsigned char *pucWatchPoint - Return whether the core stopped because of a watchpoint.
       Output: unsigned long - error code ERR_xxx
  Description: Checks Status of the process for ARMS core
   Date        Initials    Description
03-Sep-2007    SPT       Initial
26-Mar-2008    JY        Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7SStatusProc (unsigned char *pucSelectScanChain2,
                                    unsigned char *pucStartedExecution,
                                    unsigned char *pucStopProblem,
                                    unsigned char *pucCoreExecuting,
                                    unsigned char *pucThumbMode,
                                    unsigned char *pucWatchPoint)
{

    unsigned char ucStatusReg  = 0x0;
    unsigned char ucStatusReg2  = 0x0; //extra  
    unsigned long ulCount = 0x0;
    unsigned long ulResult = ERR_NO_ERROR;

    unsigned long pulLocalData[3];
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[3 * 16];
    unsigned long *pulTempTDOVal;
    pulTempTDOVal = pulTDOVal;
    ulCount = 0x0;

    DLOG2(("ARML_ARM7StatusProc"));
    *pucStopProblem = FALSE;
    *pucThumbMode   = FALSE;
    *pucWatchPoint  = FALSE;

    //Check whether core is in debug mode.

    if ( *pucSelectScanChain2 == 1 )
        {
        ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    do
        {
        pulTempTDOVal = pulTDOVal;
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS; 

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
        pulTempTDOVal += 3;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DBG_STATUS; 

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
        pulTempTDOVal += 3;


        ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;


        if ( ulCount >= MAX_WAIT_CYCLES )
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
        //break;
        ulCount ++;

        ucStatusReg = (pulTDOVal[0] & 0x000000FF);
        ucStatusReg2 = (pulTDOVal[0] & 0x000000FF);
        }
    while ( ucStatusReg != ucStatusReg2 );

    if ( (ucStatusReg & ARM_DEBUG_MODE) == ARM_DEBUG_MODE )
        {

        ucBuffercount = 0;
        *pucCoreExecuting = FALSE;
        //Disable EmbeddedICE watchpoints.
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulDISABLE_WP0; 

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulDISABLE_WP1; 

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        }
    else
        *pucCoreExecuting = TRUE;

    if ( (*pucStartedExecution == TRUE) && (*pucCoreExecuting == FALSE) )
        {
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_2_LENGTH,pulINTDIS_DBGAK_CTRL,NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        //TBD This will not fit into EVBA7 memory --------
        //Check watchpoints for non -s cores.
        if ( (ucStatusReg & THUMB_MODE_BIT) == THUMB_MODE_BIT )
            {
            *pucThumbMode = TRUE;
            //Check whether a watchpoint caused the core to stop.
            //ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH,pulARM7_CHECK_THUMB_WP,pulLocalData);
            // if (ulResult != ERR_NO_ERROR)
            //return ulResult;
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);
            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_THUMB_NOP_NORMAL, pulLocalData);
            //pulTempTDOVal += 3;
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            *pucWatchPoint = (pulLocalData[0] & 0x1);
            }
        else
            {
            *pucThumbMode = FALSE;

            //Check whether a watchpoint caused the core to stop.
            //ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_CHECK_ARM_WP, pulLocalData);
            // if (ulResult != ERR_NO_ERROR)
            //return ulResult;
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);
            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_NORMAL, pulLocalData);
            //pulTempTDOVal += 3;
            tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            *pucWatchPoint = (pulLocalData[0] & 0x1);
            }

        DLOG3(("Thumb Mode: %d",*pucThumbMode));
        DLOG3(("Watch Point: %d",*pucWatchPoint));

        if ( (ucStatusReg & THUMB_MODE_BIT) == THUMB_MODE_BIT )
            {
            *pucThumbMode = TRUE;
            }
        else
            {
            ucBuffercount = 0;
            pulTempTDOVal = pulTDOVal;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
            pulDataFromScan[ucBuffercount] = NULL;
            pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

            ulResult = JtagScanDRMultiple(4, pulDRLength, pulDataToScan, pulDataFromScan);
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            }
        }
    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM7SetupExecuteProc
     Engineer: Shameerudheen P T
        Input: unsigned long *pulData - Pointer to data to use in this function for more details see
               Opella-XD SW USB Interface Description V 0.0.2 or later
       Output: unsigned long - error code ERR_xxx
  Description: Setup Processor before calling execute proc
   Date        Initials    Description
05-Sep-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM7SetupExecuteProc (unsigned long *pulData)
{

    unsigned long ulResult = ERR_NO_ERROR;
    unsigned short usIndex;

    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16]; //scan chain2 length is 38
    unsigned long *pulTempTDIVal;
    pulTempTDIVal = pulTDIVal;

    DLOG2(("ARML_ARM7SetupExecuteProc"));
    usIndex = 0;
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal[0] = 0x0; //(unsigned long)pulData[usIndex];
    pulTempTDIVal[1] = 0x20;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;


    DLOG3(("WP0 Address Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x28;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP0 Address Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x29;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP0 Data Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2A;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;


    DLOG3(("WP0 Data Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2B;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP0 Ctrl Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2C;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP0 Ctrl Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x2D;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP1 Address Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x30;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;


    DLOG3(("WP1 Address Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x31;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP1 Data Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x32;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;


    DLOG3(("WP1 Data Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x33;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP1 Ctrl Value 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x34;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    DLOG3(("WP1 Ctrl Mask 0x%08x",pulData[usIndex]));
    pulTempTDIVal[0] = pulData[usIndex];
    pulTempTDIVal[1] = 0x35;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;
    usIndex++;

    ulResult = JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM7ExecuteProc
     Engineer: Shameerudheen P T
        Input: unsigned char *bThumbMode - 1 -execute processor in thumb mode
               unsigned long *pulData - R0 value
       Output: unsigned long - error code ERR_xxx
  Description: Executing processor from its memory at system speed used for 
               single step, run etc.
   Date        Initials    Description
05-Sep-2007      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM7ExecuteProc (unsigned char *bThumbMode, unsigned long *pulData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    pulTempTDIVal = pulTDIVal;
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( *bThumbMode == 1 )
        {
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_BX_R0;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_LDMIA_R0_R0;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

        DLOG3(("Execute proc arm7 0x%08X",*pulData));
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        ARML_ARM7SC1PrepareData(pulTempTDIVal,*pulData);
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_CLOCK;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_THUMB_B_PC_5;

        ulResult = JtagScanDRMultiple(13, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    else
        {
        //pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        ARML_ARM7SC1PrepareData(pulTempTDIVal,0xEAFFFFFA);

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_CLOCK;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulTempTDIVal;
        pulTempTDIVal += 2;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    return JtagScanIR(&ulARM_JTAG_INTEST, NULL);
}

/****************************************************************************
    Function: ARML_ARM7WriteByte
    Engineer: Shameerudheen P T
       Input: unsigned long ulAddress - address to Write target memory 
              unsigned char ucWriteData - data to write
      Output: unsigned long - error code ERR_xxx
 Description: Writes a byte to ARM7 target memory
   Date        Initials    Description
06-Sep-2007      SPT       Initial.
18-Jan-2008      SPT       Performance Improvement
****************************************************************************/
unsigned long ARML_ARM7WriteByte(unsigned long ulAddress, unsigned char ucWriteData)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    pulTempTDIVal = pulTDIVal;

    DLOG2(("ARML_ARM7WriteByte"));

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0R1;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,(unsigned long)ucWriteData); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddress); 
    pulDataToScan[ucBuffercount++] = pulTempTDIVal; 
    pulTempTDIVal += 3;

    ulResult = JtagScanDRMultiple(5, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    if ( ucgUseSafeNonVectorAddress )
        {
        ulResult = ARML_ARM7SetPCtoSafeNonVectorAddress();
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }
    else
        {
        ucBuffercount = 0;
        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
        pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        }

    ucBuffercount = 0;
    pulTempTDIVal = pulTDIVal;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_CLOCK;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_STRB_R0_R1;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = JtagScanIR(&ulARM_JTAG_RESTART, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARM7_WaitForSystemSpeed();
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM7SetSNVectorAddress
     Engineer: Shameerudheen PT
        Input: unsigned long *pulAddress - Value to be set
       Output: unsigned long - error code ERR_xxx
  Description: Sets the global Safe Non Vector address
   Date        Initials    Description
06-Sep-2007      SPT       Initial
***************************************************************************/
unsigned long ARML_ARM7SetSNVectorAddress (unsigned long *pulAddress)
{
    DLOG2(("ARML_ARM7SetSNVectorAddress"));
    ulARM7_SAFE_NON_VEC_ADDRESS = *pulAddress;
    return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARML_ARM7ReadCPSR
     Engineer: Shameerudheen PT
        Input: unsigned long *pulCPSR - Pointer to CPSR read value
       Output: unsigned long - error code ERR_xxx
  Description: Reads the CPSR from the core
   Date        Initials    Description
06-Sep-2007      SPT       Initial
****************************************************************************/
static unsigned long ARML_ARM7ReadCPSR(unsigned long *pulCPSR)
{  
    unsigned long ulResult;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long *pulDataFromScan[16];
    unsigned long pulTDOVal[2 * 16];
    unsigned long *pulTempTDOVal;

    // deleted 4 NOPs for optimization...

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_MRS_R0_CPSR;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    // deleted 2 NOPs for optimization...

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_STMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulTempTDOVal = pulTDOVal;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;
    pulTempTDOVal += 3;

    ulResult = JtagScanDRMultiple(7, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    *pulCPSR = ARML_ARM7SC1FilterData(pulTDOVal);
    return ulResult;
    // deleted 4 NOPs for optimization...
}

/****************************************************************************
     Function: ARML_ARM7CheckDataAbort
     Engineer: Shameerudheen P T
        Input: unsigned char *pucReadData - pointer to return processor 
               status 0 if in Supervisor mode 1 other wise
       Output: unsigned long - error code ERR_xxx
  Description: Checks the CPSR and returns FALSE if the mode is in
               Supervisor mode. and also force processor to supervisor mode
   Date        Initials    Description
06-Sep-2007      SPT       Initial
***************************************************************************/
unsigned long ARML_ARM7CheckDataAbort (unsigned char *pucReadData)
{
    unsigned long ulCPSR;
    unsigned long ulMode;
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_ARM7CheckDataAbort"));

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulResult = ARML_ARM7ReadCPSR(&ulCPSR);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulMode = (ulCPSR & 0x0000001F);

    //This is 0x13 (Supervisor mode reversed)
    if ( ulMode != 0x13 )
        {
        // Change mode back to supervisor.
        ulResult = ARML_ARM7ChangeMode(ARM7ModeSupervisor);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        *pucReadData = 0x1;
        }
    else
        *pucReadData = 0x0;

    return ulResult;
}

/****************************************************************************
     Function: ARML_ARM720ReadCPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - Pointer to Read Data
       Output: unsigned long - error code ERR_xxx
  Description: read all cp registers of ARM720.
Date          Initials    Description
07-Sep-2007     SPT       Initial
****************************************************************************/
unsigned long ARML_ARM720ReadCPReg(unsigned long *pulReadData)
{


    const unsigned long ARM720_CP_REG_CONSTANTS[7] = {ARM720_REVERSED_CP_REG_C0_READ_ID_REG,
        ARM720_REVERSED_CP_REG_C1_READ_CONTROL_REG,
        ARM720_REVERSED_CP_REG_C2_READ_TTB,
        ARM720_REVERSED_CP_REG_C3_READ_DAC,
        ARM720_REVERSED_CP_REG_C5_READ_FSR,
        ARM720_REVERSED_CP_REG_C6_READ_FAR,
        ARM720_REVERSED_CP_REG_C13_READ_PID
    };
    unsigned long pulTempvar[2]; // to form arm7 scan chain 1 data
    unsigned long pulTempRecData[2]; //to receive data from arm core
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;


    DLOG2(("ARML_ARM720ReadCPReg"));
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i < 7; i++ )
        {
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_CLOCK, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        pulTempvar[0] = ARM720_CP_REG_CONSTANTS[i];
        pulTempvar[1] = 0x0;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulTempvar, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 7, 0x0D); // 2 extra clocks in RTI state of JTAG State Machine
        ulResult = ARML_PerformARM7Nops(1);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_NORMAL, pulTempRecData);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        pulReadData[i] = ARML_ARM7SC1FilterData(pulTempRecData);
        };

    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM720WriteCPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - pointer to data to be written 
               into ARM720 CP registers
       Output: unsigned long - error code ERR_xxx
  Description: Writes all cp registers of ARM 720.
Date          Initials    Description
10-Sep-2007     SPT       Initial
****************************************************************************/
unsigned long ARML_ARM720WriteCPReg(unsigned long *pulWriteData)
{


    const unsigned long ARM720_CP_REG_CONSTANTS[6] = {ARM720_REVERSED_CP_REG_C2_WRITE_TTB,
        ARM720_REVERSED_CP_REG_C3_WRITE_DAC,
        ARM720_REVERSED_CP_REG_C5_WRITE_FSR,
        ARM720_REVERSED_CP_REG_C6_WRITE_FAR,
        ARM720_REVERSED_CP_REG_C13_WRITE_PID,
        ARM720_REVERSED_CP_REG_C1_WRITE_CONTROL_REG
    };
    unsigned long pulTempvar[2]; // to form arm7 scan chain 1 data
    //unsigned long pulTempRecData[2]; //to receive data from arm core
    unsigned char i;
    unsigned long pulWriteData_local[6];
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_ARM720WriteCPReg"));

    pulWriteData_local[0] = pulWriteData[1]; //C2_TTB 
    pulWriteData_local[1] = pulWriteData[2]; //C3 DAC
    pulWriteData_local[2] = pulWriteData[3]; //C5 FSR                                            
    pulWriteData_local[3] = pulWriteData[4]; //C6 FAR
    pulWriteData_local[4] = pulWriteData[5]; //C13 PID
    pulWriteData_local[5] = pulWriteData[0]; //C1 Control

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i < 6; i++ )
        {
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_CLOCK, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulTempvar[0] = ARM720_CP_REG_CONSTANTS[i];
        pulTempvar[1] = 0x0;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulTempvar, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_NORMAL, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 7, 0x0D); // 2 extra clocks in RTI state of JTAG State Machine
        ARML_ARM7SC1PrepareData(pulTempvar,pulWriteData_local[i]);
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulTempvar, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
        };

    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM740ReadCPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulReadData - Pointer to data to read from 
               arm740 cP registers
       Output: unsigned long - error code ERR_xxx
  Description: Read CP the registers.
Date          Initials    Description
07-Sep-2007     SPT       Initial
****************************************************************************/
unsigned long ARML_ARM740ReadCPReg(unsigned long *pulReadData)
{


    const unsigned long ARM740_CP_REG_CONSTANTS[6] = {ARM740_REVERSED_CP_REG_C0_READ_ID_REG,
        ARM740_REVERSED_CP_REG_C1_READ_CONTROL_REG,
        ARM740_REVERSED_CP_REG_C2_READ_TTB,
        ARM740_REVERSED_CP_REG_C3_READ_DAC,
        ARM740_REVERSED_CP_REG_C5_READ_FSR,
        ARM740_REVERSED_CP_REG_C6_READ_FAR                                                     
    };
    unsigned long pulTempvar[2]; // to form arm7 scan chain 1 data
    unsigned long pulTempRecData[2]; //to receive data from arm core
    unsigned char i;
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_ARM740ReadCPReg"));

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i < 6; i++ )
        {
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_CLOCK, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulTempvar[0] = ARM740_CP_REG_CONSTANTS[i];
        pulTempvar[1] = 0x0;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulTempvar, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 7, 0x0D); // 2 extra clocks in RTI state of JTAG State Machine
        ulResult = ARML_PerformARM7Nops(1);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_NORMAL, pulTempRecData);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        pulReadData[i] = ARML_ARM7SC1FilterData(pulTempRecData);
        };

    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM740WriteCPReg
     Engineer: Shameerudheen P T
        Input: unsigned long *pulWriteData - Pointer to data to be written into ARM740 CP registers
       Output: unsigned long - error code ERR_xxx
  Description: Writes all CP registers.
Date          Initials    Description
10-Sep-2007     SPT       Initial
****************************************************************************/
unsigned long ARML_ARM740WriteCPReg(unsigned long *pulWriteData)
{

    const unsigned long ARM740_CP_REG_CONSTANTS[5] = {ARM740_REVERSED_CP_REG_C2_WRITE_TTB,
        ARM740_REVERSED_CP_REG_C3_WRITE_DAC,
        ARM740_REVERSED_CP_REG_C5_WRITE_FSR,
        ARM740_REVERSED_CP_REG_C6_WRITE_FAR,
        ARM740_REVERSED_CP_REG_C1_WRITE_CONTROL_REG
    };
    unsigned long pulTempvar[2]; // to form arm7 scan chain 1 data
    unsigned char i;
    unsigned long pulWriteData_local[6];
    unsigned long ulResult = ERR_NO_ERROR;

    DLOG2(("ARML_ARM740WriteCPReg"));

    pulWriteData_local[0] = pulWriteData[1]; //C2_TTB 
    pulWriteData_local[1] = pulWriteData[2]; //C3 DAC
    pulWriteData_local[2] = pulWriteData[3]; //C5 FSR                                            
    pulWriteData_local[3] = pulWriteData[4]; //C6 FAR
    pulWriteData_local[4] = pulWriteData[0]; //C1 Control

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_CP15);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    for ( i=0; i < 5; i++ )
        {
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_CLOCK, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulTempvar[0] = ARM740_CP_REG_CONSTANTS[i];
        pulTempvar[1] = 0x0;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulTempvar, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulARM7_ARM_NOP_NORMAL, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 7, 0x0D); // 2 extra clocks in RTI state of JTAG State Machine
        ARML_ARM7SC1PrepareData(pulTempvar,pulWriteData_local[i]);
        ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH, pulTempvar, NULL);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;
        tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
        };

    return ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
}

/****************************************************************************
     Function: ARML_ARM7ReadWriteDCC
     Engineer: Shameerudheen
        Input: unsigned char *pucDataToWrite - Is there Data to Write, 
               unsigned long *pulWriteData - the Data to write
               unsigned char *pucDataWriteSuccessful - is Write successful, 
               unsigned char *pucDataReadFromCore - is there data from core, 
               unsigned long *pulReadData - Data from core
       Output: unsigned long - error code ERR_xxx
  Description: Read and Write data to Debug communication channel of arm7
   Date        Initials    Description
10-Sep-2007    SPT       Initial
27-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
static unsigned long ARML_ARM7ReadWriteDCC (unsigned char *pucDataToWrite,
                                            unsigned long *pulWriteData,
                                            unsigned char *pucDataWriteSuccessful,
                                            unsigned char *pucDataReadFromCore,
                                            unsigned long *pulReadData)
{
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;

    unsigned long pulTDOVal[2 * 16];
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDOVal;
    unsigned long *pulTempTDIVal;

    unsigned char ucDCCControlReg;
    unsigned long ulResult = ERR_NO_ERROR;

    pulTempTDOVal = pulTDOVal;
    pulTempTDIVal = pulTDIVal;

    *pucDataWriteSuccessful = FALSE;
    *pucDataReadFromCore    = FALSE;

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_ICE);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

//Read the DCC control register. 

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulREAD_DCC_CTRL; 

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
    pulTempTDOVal += 2;

    ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ucDCCControlReg = (pulTDOVal[0] & 0x000000FF);

    if ( *pucDataToWrite == TRUE )
        {
        pulTempTDIVal = pulTDIVal;
        ucBuffercount = 0;
        if ( (ucDCCControlReg & DCC_WRITE_MASK) != DCC_WRITE_MASK )
            {
            pulTempTDIVal[0] = pulWriteData[0];
            pulTempTDIVal[1] = WRITE_TO_DCC_DATA_REG;
            ulResult = JtagScanDR(ARM7_SCAN_CHAIN_2_LENGTH, pulTempTDIVal, NULL);
            pulTempTDIVal += 2;
            if ( ulResult != ERR_NO_ERROR )
                return ulResult;
            *pucDataWriteSuccessful = TRUE;
            }
        else
            *pucDataWriteSuccessful = FALSE;
        }

    if ( (ucDCCControlReg & DCC_READ_MASK) == DCC_READ_MASK )
        {
        pulTempTDOVal = pulTDOVal;
        ucBuffercount = 0;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = NULL;
        pulDataToScan[ucBuffercount++] = pulREAD_DCC_DATA;

        pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_2_LENGTH;
        pulDataFromScan[ucBuffercount] = pulTempTDOVal;
        pulDataToScan[ucBuffercount++] = pulALL_ZEROS;
        pulTempTDOVal += 2;

        ulResult = JtagScanDRMultiple(2, pulDRLength, pulDataToScan, pulDataFromScan);
        if ( ulResult != ERR_NO_ERROR )
            return ulResult;

        pulReadData[0] = pulTDOVal[0];
        *pucDataReadFromCore = TRUE;
        }
    return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7ChangeModeToARMMode
    Engineer: Shameerudheen PT.
       Input: unsigned char ucUserHalt - inout whether user halted, 
              unsigned char * pucOffsetNeeded - Pointer to return whether the core stopped on a non word aligned address  
      Output: unsigned long - error code ERR_xxx
 Description: Change from Thumb mode to ARM mode for ARM7 core
   Date        Initials    Description
25-Sep-2007    SPT         Initial
27-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
27-Mar-2008    SPT          Correction
****************************************************************************/
unsigned long ARML_ARM7ChangeModeToARMMode(unsigned char ucUserHalt, unsigned char * pucOffsetNeeded)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulReg0;
    unsigned long ulRegPC;

    unsigned long ulBXValue = 0x0;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[2 * 16];
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulTempTDOVal;

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;

    DLOG2(("ARML_ARM7ChangeModeToARMMode"));
    *pucOffsetNeeded = FALSE;
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Read R0 and store value.
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_STR_R0_R0;   

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;
    pulTempTDOVal += 2;


    // MOV R0, PC
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_MOV_R0_PC;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    // Read R0 (now equal to the value of PC) and store value.

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_STR_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;
    pulTempTDOVal +=2;

    //Put the value 0x0 into R0 so that we can BX to a word aligned address.

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulBXValue);
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    // Branch and exchange with the R0 (ie 0x0 ARM mode instructions have to be word
    // aligned). This changes back to ARM mode.   

    ulResult = JtagScanDRMultiple(15, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    ulReg0 = ARML_ARM7SC1FilterData(&pulTDOVal[0]);
    ulRegPC = ARML_ARM7SC1FilterData(&pulTDOVal[2]);

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine
                                                                         //intead of ML_RunTestIdle()
    ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH,pulARM7_THUMB_BX_R0,NULL);
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;


    // These NOP's are in thumb mode but really the pipeline is 
    // flushed when the BX is executed so it doesn't matter.
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    //This offset accounts for the instructions taken since debug mode was entered,
    if ( ucUserHalt )
        ulRegPC -= 0x24;
    else
        ulRegPC -= 0x22;

    if ( (ulRegPC & 0x3) != 0 )
        {
        //If the PC is at a half word aligned address then it is 
        //changed to the previous word aligned address.
        ulRegPC -= 0x2;
        *pucOffsetNeeded = TRUE;
        }

    // Now we are in ARM mode we can restore the values of R0 and the PC.
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MOV_PC_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;


    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;   

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulReg0);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    return JtagScanDRMultiple(7, pulDRLength, pulDataToScan, pulDataFromScan);
}

/****************************************************************************
    Function: ARML_ARM7SChangeModeToARMMode
    Engineer: Shameerudheen PT.
       Input: unsigned char * pucOffsetNeeded - Pointer to return whether 
              the core stopped on a non word aligned address  
      Output: unsigned long - error code ERR_xxx  
 Description: Change from Thumb mode to ARM mode for ARM7S core
   Date        Initials    Description
25-Sep-2007    SPT         Initial
27-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7SChangeModeToARMMode(unsigned char * pucOffsetNeeded)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulReg0;
    unsigned long ulRegPC;

    unsigned long ulBXValue = 0x0;
    unsigned long *pulDataToScan[16];
    unsigned long *pulDataFromScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDOVal[2 * 16];
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;
    unsigned long *pulTempTDOVal;

    pulTempTDIVal = pulTDIVal;
    pulTempTDOVal = pulTDOVal;

    DLOG2(("ARML_ARM7ChangeModeToARMMode"));
    *pucOffsetNeeded = FALSE;
    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // Read R0 and store value.
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_STR_R0_R0;   

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;
    pulTempTDOVal += 2;


    // MOV R0, PC
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_MOV_R0_PC;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    // Read R0 (now equal to the value of PC) and store value.

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_STR_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = pulTempTDOVal;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;
    pulTempTDOVal +=2;

    //Put the value 0x0 into R0 so that we can BX to a word aligned address.

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulBXValue);
    pulDataFromScan[ucBuffercount] = NULL;
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;   

    ulResult = JtagScanDRMultiple(15, pulDRLength, pulDataToScan, pulDataFromScan);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;
    ulReg0 = ARML_ARM7SC1FilterData(&pulTDOVal[0]);
    ulRegPC = ARML_ARM7SC1FilterData(&pulTDOVal[2]);

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;

    // These NOP's are in thumb mode but really the pipeline is 
    // flushed when the BX is executed so it doesn't matter.
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 6, 0x0D); // 1 extra clocks in RTI state of JTAG State Machine
                                                                         //intead of ML_RunTestIdle()
    ulResult = JtagScanDR(ARM7_SCAN_CHAIN_1_LENGTH,pulARM7_THUMB_BX_R0,NULL);
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    // These NOP's are in thumb mode but really the pipeline is 
    // flushed when the BX is executed so it doesn't matter.
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    //This offset accounts for the instructions taken since debug mode was entered,
    ulRegPC -= 0x24;

    if ( (ulRegPC & 0x3) != 0 )
        {
        //If the PC is at a half word aligned address then it is 
        //changed to the previous word aligned address.
        ulRegPC -= 0x2;
        *pucOffsetNeeded = TRUE;
        }

    // Now we are in ARM mode we can restore the values of R0 and the PC.
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_MOV_PC_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL; 

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    ulResult = JtagScanDRMultiple(16, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;   

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulReg0);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal += 2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    return JtagScanDRMultiple(7, pulDRLength, pulDataToScan, pulDataFromScan);
}

/****************************************************************************
    Function: ARML_ARM7ChangeToThumbMode
    Engineer: Shameerudheen PT.
       Input: unsigned long ulReg0 - R0 register Value
              unsigned long ulRegPC - Program counter value
      Output: unsigned long - error code ERR_xxx  
 Description: Change from ARM mode to Thumb mode for ARM7 core
   Date        Initials    Description
14-Oct-2007    SPT         Initial
27-Mar-2008    JCK          Included FPGA JTAG 16 Buffers feature
****************************************************************************/
unsigned long ARML_ARM7ChangeToThumbMode(unsigned long ulReg0, unsigned long ulRegPC)
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long *pulDataToScan[16];
    unsigned long pulDRLength[16];
    unsigned char ucBuffercount = 0;
    unsigned long pulTDIVal[2 * 16];
    unsigned long *pulTempTDIVal;

    DLOG2(("ARML_ARM7ChangeToThumbMode"));
    // These instructions are in ARM mode.

    pulTempTDIVal = pulTDIVal;

    ulResult = ARML_ARM7SelectScanChain(ARM7_SCAN_CHAIN_DEBUG);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    ulResult = JtagScanDRMultiple(3, pulDRLength, pulDataToScan, NULL);
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    pulTempTDIVal = pulTDIVal;
    ucBuffercount = 0;
    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulRegPC);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal +=2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_ARM_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_BX_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_LDMIA_R0_R0;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    ARML_ARM7SC1PrepareData(pulTempTDIVal,ulReg0);
    pulDataToScan[ucBuffercount++] = pulTempTDIVal;
    pulTempTDIVal +=2;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    pulDRLength[ucBuffercount] = ARM7_SCAN_CHAIN_1_LENGTH;
    pulDataToScan[ucBuffercount++] = pulARM7_THUMB_NOP_NORMAL;

    return JtagScanDRMultiple(15, pulDRLength, pulDataToScan, NULL);
}

/****************************************************************************
     Function: ARML_ARMGetMultiCoreInfo
     Engineer: Shameerudheen P.T.
        Input: unsigned long * pulReturnData - pointer for return data, 
                            number of cores, IR length of each core
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: finding out number of cores in the Scan chain
               on particular core
Date           Initials    Description
09-Oct-2007    SPT         Initial
06-May-2010    JCK         Changed the method to find the total IR length and 
                           total number of cores.
note: Before entering to this function JTAG reset must be issued. 
****************************************************************************/

unsigned long ARML_ARMGetMultiCoreInfo(unsigned long * pulReturnData)
{
    unsigned long ulResult;
    unsigned long pulScanData[16] = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF,
        0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};

    unsigned long pulScanOutData[16];
    unsigned char ucFirstBit = 0;
    unsigned char ucSecondBit;
    unsigned char ucIRLengthArrayIndex = 0;
    unsigned char ucIrLength = 0;
    unsigned char i,j;
    unsigned long ulIRLengthBackup;

    DLOG2(("ARML_ARMGetMultiCoreInfo"));

    pulReturnData[0] = 0; //initializing number of cores into zero.

    ulIRLengthBackup = tyJtagScanConfig.ulIRLength;
    tyJtagScanConfig.ulIRLength = 512;
    ulResult = JtagScanIRLong(pulScanData,pulScanOutData);
    tyJtagScanConfig.ulIRLength = ulIRLengthBackup;
    if ( ulResult != ERR_NO_ERROR )
        return ulResult;

    DLOG2(("Scan Out Data 0x%08X", pulScanOutData));
    if ( (pulScanOutData[0] & 0x1) == 0x0 ) //first bit (LSB) should not be zero;
        return ERR_ARM_NO_CORES_FOUND;

    if ( (pulScanOutData[0] & 0x3) == 0x3 ) //fisr and second bits should not be one
        return ERR_ARM_NO_CORES_FOUND;

    for ( i = 0; i < 16; i++ )
        {
        for ( j = 0; j < 32; j++ )
            {
            ucSecondBit = (pulScanOutData[i] & 0x1);
            DLOG2(("second bit %d", ucSecondBit));
            pulScanOutData[i] = pulScanOutData[i] >> 1;
            if ( !(i == 0 && j ==0) )
                {
                if ( ucFirstBit == 1 && ucSecondBit == 1 )
                    {
                    return ERR_NO_ERROR;
                    }
                else if ( ucFirstBit == 0 && ucSecondBit == 1 )
                    {
                    pulReturnData[0] = pulReturnData[0] + 1; // increment core count by one
                    ucIRLengthArrayIndex++;
                    pulReturnData[ucIRLengthArrayIndex] = ucIrLength; //Storing current IRLength to return data
                    ucIrLength = 0; //Initializing IrLength
                    }
                }

            ucIrLength++;
            ucFirstBit = ucSecondBit;
            }
        }

    return ERR_ARM_MULTICORE_CALC_ERROR;

}

/****************************************************************************
     Function: ARML_InitilizeICePick
     Engineer: Jeenus C.K.
        Input: unsigned long ulSubPortNum - Subport number of the selected device.
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Inorder to Initialize the ICE-Pick
Date           Initials    Description
22-May-2010    SPT         Initial
****************************************************************************/
unsigned long ARML_InitilizeICePick( unsigned long ulSubPortNum )
{
    unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulDataToScan;
    unsigned long ulDataFromScan;
    unsigned long ulTemp[2];
    unsigned long ulIRLengthBackup;

    ProcessTpa();

    DLOG(("ulSubPortNum = 0x%08x ", ulSubPortNum));
    /* Reset the TAP using TMS Pattern  */
    ulResult = JtagResetTap ( 0 );
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;

    /* Set the IR Length o ICE-Pick */
    ulIRLengthBackup = tyJtagScanConfig.ulIRLength;
    tyJtagScanConfig.ulIRLength = ICEPICK_C_IR_LENGTH;
    
    /* # select router */
    /* Issue Connect instruction to ICE-Pick */
    ulDataToScan = ICEPICK_C_CONNECT;
    DLOG(("IRScan: ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanIR(&ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;

    /* Set the Connect pattern */
    ulDataToScan = ICEPICK_C_CONNECT_PATTERN;
    DLOG(("ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanDR(ICEPICK_C_DCON_REG_LENGTH, &ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;
    DLOG(("ulDataFromScan = 0x%08x ", ulDataFromScan));

    DLOG(("ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanDR(ICEPICK_C_DCON_REG_LENGTH, &ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;
    DLOG(("ulDataFromScan = 0x%08x ", ulDataFromScan));

    /* set Control */
    ulDataToScan = ICEPICK_C_ROUTER;
    DLOG(("IRScan: ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanIR(&ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;

    ulDataToScan = ARM_TAP_SELECT_PATTERN | ( ulSubPortNum << 24 );
    DLOG(("ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanDR(ICEPICK_C_SEC_TAP_REG_LENGTH, &ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND; 
    DLOG(("ulDataFromScan = 0x%08x ", ulDataFromScan));

    ulDataToScan = ARM_TAP_ADDRESS | ( ulSubPortNum << 24 );
    DLOG(("ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanDR(ICEPICK_C_SEC_TAP_REG_LENGTH, &ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;
    DLOG(("ulDataFromScan = 0x%08x ", ulDataFromScan));

    DLOG(("ulDataToScan = 0x%08x Length ", ulDataToScan));
    ulResult = JtagScanDR(ICEPICK_C_SEC_TAP_REG_LENGTH, &ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;
    DLOG(("ulDataFromScan = 0x%08x ", ulDataFromScan));

    /*******************************/
    ulDataToScan = 0x04E;
    tyJtagScanConfig.ulIRLength = 0xA;
    DLOG(("IRScan: ulDataToScan = 0x%08x ", ulDataToScan));
    ulResult = JtagScanIR(&ulDataToScan, &ulDataFromScan);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;

    ulDataToScan = 0x0;
    ulResult = JtagScanDR(64, &ulDataToScan, ulTemp);
    if ( ulResult!= ERR_NO_ERROR )
        return ERR_ARM_NO_CORES_FOUND;

    DLOG(( "IDCODE1 = 0x%08x, IDCODE2 = 0x%08x", ulTemp[0], ulTemp[1]));
    /*******************************/

    /*******************************/
    tyJtagScanConfig.ulIRLength = ulIRLengthBackup;
    return ulResult;
}
