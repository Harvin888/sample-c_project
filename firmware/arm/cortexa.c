/******************************************************************************
       Module: cortexa.c
     Engineer: Amerdas D K
  Description: Implementation of Cortex-A debug functionalities
  Date           Initials    Description
  22-03-2010     ADK         Initial   
******************************************************************************/
#include "../common/common.h"   /* Definition of debug message and internal I/O macros */
#include "cortexa.h"           /* Definition of CortexA specific registers. */
#include "export/api_err.h"     /* Definitions of Error returned by the Firmware*/
#include "coresight.h"  /* Definition of Coresight specific registers. */

extern TyCoreSightParam tyCoreSightParam;

Coprocessor Cp15ControlReg                      = { 0, 1, 0, 0};  
Coprocessor Cp15InvalidateInstMVAtoPoC          = { 0, 7, 5, 1 };
Coprocessor Cp15InvalidateDataMVAtoPoC          = { 0, 7, 6, 1 };
Coprocessor Cp15InvalidateAllInstCache          = { 0, 7, 5, 0 };
Coprocessor Cp15CacheSizeSelection              = { 2, 0, 0, 0 };
Coprocessor Cp15CacheSizeIdentifation           = { 1, 0, 0, 0 };
Coprocessor Cp15CleanandInvalidateDataSetWay    = { 0, 7, 14, 2 };

static unsigned long CortexA_InvalidateDataCache(void);

/****************************************************************************
Function    : CortexA_ReadDebugRegister
Engineer    : Amerdas D K
Input       : unsigned long ulRegAddr : Address of register to access
              unsigned long* pulData  : Pointer for result
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadDebugRegister(unsigned long ulRegAddr,
                                         unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;

    ulRegAddr = (tyCoreSightParam.ulDbgBaseAddr + ulRegAddr);

    /* Write the address of the register to be accessed in TAR */
    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulRegAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read the contents of the addressed register from DRW*/
    Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteDebugRegister
Engineer    : Amerdas D K
Input       : unsigned long ulRegAddr   : Address of register to access
              unsigned long ulData      : Data to write into addressd register
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteDebugRegister(unsigned long ulRegAddr,
                                          unsigned long ulData)
{
    unsigned long Result = ERR_NO_ERROR;

    ulRegAddr = (tyCoreSightParam.ulDbgBaseAddr + ulRegAddr);

    /* Write the address of the register to be accessed in TAR */
    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulRegAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write the contents to the addressed register by placing it in DRW*/
    Result = CoreSight_WriteDapReg(MEM_AP_DRW_REG, ulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;  
}
/****************************************************************************
Function    : CortexA_ExecuteInstruction
Engineer    : Amerdas D K
Input       : unsigned long ulInstruction   : Opcode of the instruction
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ExecuteInstruction(unsigned long ulInstruction)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    /* Read DSCR value and check whether previous execution is over 
       by checking the InstrCompl_l : bit 24 of DSCR
       0 = the processor is currently executing an instruction fetched from the ITR Register, reset value
       1 = the processor is not currently executing an instruction fetched from the ITR Register.
    */
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);// Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
    while (CORTEXA_DSCR_INSTR_COMP != (ulDSCR & CORTEXA_DSCR_INSTR_COMP));

    /* Write the opcode of the instruction in ITR */
    Result = CortexA_WriteDebugRegister(CORTEXA_ITR_OFSET, ulInstruction);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read DSCR value and check whether previous execution is over 
       by checking the InstrCompl_l : bit 24 of DSCR
        0 = the processor is currently executing an instruction fetched from the ITR Register, reset value
        1 = the processor is not currently executing an instruction fetched from the ITR Register.
    */
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
    while (CORTEXA_DSCR_INSTR_COMP != (ulDSCR & CORTEXA_DSCR_INSTR_COMP));

    return Result;  
}

/****************************************************************************
Function    : CortexA_ReadRegister
Engineer    : Amerdas D K
Input       : unsigned long ulRegNum  : Number of register to access
              unsigned long* pulData  : Pointer for result
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadRegister(unsigned long ulRegNum,
                                    unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;    

    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadRegister\n"));

    /* Execute the instruction: Rd -> DTRTX*/
    Result = CortexA_ExecuteInstruction(0xEE000E15 + (ulRegNum <<12));
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Check the DRTTX full flag untill set */
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
    while (0 == (ulDSCR & CORTEXA_DSCR_DTR_TX_FULL));

    /* Read the register value through DTRTX */
    Result = CortexA_ReadDebugRegister(CORTEXA_DTRTX_OFSET ,pulData);         // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadRegister\n"));

    return Result;  
}

/****************************************************************************
Function    : CortexA_WriteRegister
Engineer    : Amerdas D K
Input       : unsigned long ulRegNum  : Number of register to access
              unsigned long ulData    : Data to write into register
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteRegister(unsigned long ulRegNum,
                                     unsigned long ulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    /* Check the DRTRX full flag untill '0'*/
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
    while (CORTEXA_DSCR_DTR_RX_FULL == (ulDSCR & CORTEXA_DSCR_DTR_RX_FULL));

    /* Write the data in DTRRX */
    Result = CortexA_WriteDebugRegister(CORTEXA_DTRRX_OFSET,ulData);           // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction: DTRRX -> Rd*/
    Result = CortexA_ExecuteInstruction(0xEE100E15 + (ulRegNum <<12));
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadCPSR
Engineer    : Amerdas D K
Input       : unsigned long* pulData  : Pointer for result           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadCPSR(unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadCPSR"));

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Execute the instruction MRS R0,CPSR through the ITR */
    Result = CortexA_ExecuteInstruction(0xE10F0000);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read register R0;R0 = CPSR */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    *pulData = ulTemp;

    DLOG2(("    *pulData = 0x%08X",*pulData));

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadCPSR"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteCPSR
Engineer    : Amerdas D K
Input       : unsigned long ulData  : Data to write         
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteCPSR(unsigned long ulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_WriteCPSR"));    

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Write R0 with new CPSR value;R0 <- CPSR */
    Result = CortexA_WriteRegister(0,ulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction MSR CPSR,R0 through the ITR */
    Result = CortexA_ExecuteInstruction(0xE12FF000);        // Doubt Compiler
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute a PrefetchFlush instruction through the ITR */
    Result = CortexA_ExecuteInstruction(0xEE070F95);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteCPSR"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadSPSR
Engineer    : Amerdas D K
Input       : unsigned long* pulData  : Pointer for result           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadSPSR(unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadSPSR"));

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Execute the instruction MRS R0,SPSR through the ITR */
    Result = CortexA_ExecuteInstruction(0xE14F0000); 
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read register R0;R0 = SPSR */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    *pulData = ulTemp;

    DLOG2(("    *pulData = 0x%08X",*pulData));

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadSPSR"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteSPSR
Engineer    : Amerdas D K
Input       : unsigned long* pulData  : Pointer for result           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteSPSR(unsigned long ulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_WriteSPSR"));

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Write R0 with new SPSR value;R0 <- SPSR */
    Result = CortexA_WriteRegister(0,ulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction MSR SPSR,R0 through the ITR */
    Result = CortexA_ExecuteInstruction(0xE169F000);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute a PrefetchFlush instruction through the ITR */
    Result = CortexA_ExecuteInstruction(0xEE070F95);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteSPSR"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadPC
Engineer    : Amerdas D K
Input       : unsigned long* pulData  : Pointer for result           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadPC(unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadPC\n"));

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Execute the instruction MOV R0,PC through the ITR */
    Result = CortexA_ExecuteInstruction(0xE1A0000F);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read register R0;R0 = PC */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    *pulData = ulTemp;
    DLOG2(("    *pulData = 0x%08X\n",*pulData));

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadPC\n"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WritePC
Engineer    : Amerdas D K
Input       : unsigned long ulData  : Data to write         
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WritePC(unsigned long pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_WritePC\n"));

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Write R0 with new PC value;R0 <- PC */
    Result = CortexA_WriteRegister(0,pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction MOV PC,R0 through the ITR */
    Result = CortexA_ExecuteInstruction(0xE1A0F000);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_WritePC\n"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadCoProcessor
Engineer    : Amerdas D K
Input       : unsigned long CPnum   :Co-processor number
              unsigned long Opcode1 :Opcode 1 field 
              unsigned long CRn     :CRn field
              unsigned long CRm     :CRm field
              unsigned long Opcode2 :Opcode 1 field       
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadCoProcessor(unsigned long* pulInstr, 
                                       unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;


    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadCoProcessor"));


    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulR0Bakp = ulTemp;

    /* Execute instruction MRC p15,0,r0,c0,c1,0 through the ITR */
    /*ulInstr = (0xEE100010|(CRm)|((Opcode2)<<5)|((CPnum)<<8)|((CRn)<<16)|((Opcode1)<<21));*/
    DLOG(("ulInstr = 0x%8x", *pulInstr));

    Result = CortexA_ExecuteInstruction(*pulInstr);
    if (Result != ERR_NO_ERROR)
        {
        DLOG(("Error: CortexA_ExecuteInstruction"));
        return Result;
        }

    /* Read register R0;R0 = Co-processor */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        DLOG(("Error: CortexA_ReadRegister"));
        return Result;
        }

    *pulData = ulTemp;

    DLOG(("Read Data = 0x%08x", ulTemp));
    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadCoProcessor"));

    return Result;  
}

/****************************************************************************
Function    : CortexA_WriteCoProcessor
Engineer    : Amerdas D K
Input       : unsigned long ulData  : Data to write         
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteCoProcessor(unsigned long* pulInstr, 
                                        unsigned long  ulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;
    unsigned long ulR0Bakp;

    DLOG(("Enter:Cortex-A8 function:CortexA_WriteCoProcessor"));
    DLOG(("Instruction = 0x%08x, ulData = 0x%08x, 0x%08x", *pulInstr, ulData));

    /* Read register R0 and keep it: ulTemp -> ulR0Bakp */
    Result = CortexA_ReadRegister(0,&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    ulR0Bakp = ulTemp;

    /* Write R0 with new CoProcessor Value;R0 <- CoProcessor Value */
    Result = CortexA_WriteRegister(0,ulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute instruction MCR p15, 0, r0, c0, c1, 0 through the ITR */
    /* ulInstr = (0xee000010|(CRm)|((Opcode2)<< 5)|((CPnum)<<8)|((CRn)<<16)|((Opcode1)<<21)); */

    Result = CortexA_ExecuteInstruction(*pulInstr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Restore the value of R0 */
    Result = CortexA_WriteRegister(0,ulR0Bakp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("Exit:Cortex-A8 function:CortexA_WriteCoProcessor"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ChangeMode
Engineer    : Amerdas D K
Input       : CortexAMode tyCortexAMode  : Processor modes           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ChangeMode(CortexAMode tyCortexAMode)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulTemp;

    DLOG2(("Enter:Cortex-A8 function:CortexA_ChangeMode"));
    DLOG2(("    Parameter 1:tyCortexAMode = 0x%08X",tyCortexAMode));

    /* Read register CPSR and keep it: ulTemp -> ulCPSRBakp */
    Result = CortexA_ReadCPSR(&ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    ulCPSRBakp = 0x%08X",ulTemp));

    /* Prepare the data according to mode */
    switch (tyCortexAMode)
        {
        case CortexAModeUser:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_USR_MODE);       
            break;
        case CortexAModeFIQ:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_FIQ_MODE);       
            break;
        case CortexAModeIRQ:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_IRQ_MODE);
            break;
        case CortexAModeSupervisor:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_SVR_MODE);
            break;
        case CortexAModeAbort:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_ABT_MODE);
            break;
        case CortexAModeUndefined:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_UND_MODE);
            break;
        case CortexAModeSystem:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_SYS_MODE);
            break;
        case CortexAModeSecureMonitor:
            ulTemp = ((ulTemp & 0xFFFFFFE0) | CORTEXA_SMO_MODE);
            break;
        default:
            break;        
        }

    DLOG2(("    ulTemp(CPSR + new mode) = 0x%08X",ulTemp));

    Result = CortexA_WriteCPSR(ulTemp);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    if (CortexAModeSecureMonitor == tyCortexAMode)
        {
        /* Read register CPSR and keep it: ulTemp -> ulCPSRBakp */
        Result = CortexA_ReadCPSR(&ulTemp);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        if (CortexAModeSecureMonitor != ((CortexAMode)(ulTemp & 0xFFFFFFE0)))
            {
            Result = ERR_ARM_UNABLE_CHANGE_MON;
            }
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ChangeMode"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_LeaveDebugState
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
10-May-2010       JCK       Corrected the register R0 value problem in the 
                            thumb mode.  
****************************************************************************/
unsigned long CortexA_LeaveDebugState(unsigned char *bThumbMode, 
                                       unsigned long *pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;
    unsigned long ulDRCR;

    DLOG2(("Enter:Cortex-A8 function:CortexA_LeaveDebugState\n"));

    if (*bThumbMode)
        {
        Result = CortexA_WriteRegister( CORTEXA_REG_R0, *pulData );
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
#if 0

    /* Read DSCR;Disable Halting Debug Mode;Write back*/
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    ulDSCR &= (~CORTEXA_DSCR_HALT_DBG_MODE);

    DLOG2(("    ulDSCR Modified= 0x%08X",ulDSCR));

    Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Written Modified value:ulDSCR"));

#endif

    /* Read DSCR;Disable InstructionExecuteEnable bit and write back*/
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulDSCR &= (~CORTEXA_DSCR_EXT_INT_EN);

    Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulDRCR = (CORTEXA_DRCR_CORE_RESTART | CORTEXA_DRCR_CLEAR_STICKY_EXEPT);

    DLOG(("    ulDRCR Modified= 0x%08X",ulDRCR));

    Result = CortexA_WriteDebugRegister(CORTEXA_DRCR_OFSET,ulDRCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }


    /* Read DSCR;Check whether the core has left debug state */
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG(("    ulDSCR = 0x%08X",ulDSCR));
        }
    while (CORTEXA_DSCR_CORE_RESTARTED != (ulDSCR & CORTEXA_DSCR_CORE_RESTARTED));

    DLOG(("Exit:Cortex-A8 function:CortexA_LeaveDebugState\n"));

    return Result;  
}

/****************************************************************************
Function    : CortexA_ReadDSCR
Engineer    : Amerdas D K
Input       : None          
Output      : unsigned long - error code for commands (ERR_xxx)
Description : unsigned long* pulData;Pointer for result
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadDSCR(unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;


    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadDSCR\n"));

    Result = Coresight_SelectBank(AP_BANK_0, tyCoreSightParam.ulDbgAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write Lock Access Register ;Value 0xC5ACCE55 */
    Result = CortexA_WriteDebugRegister(CORTEXA_LOCKACCESS_OFSET,0xC5ACCE55);    
    if (Result != ERR_NO_ERROR)
        {
        /* Write Lock Access Register ;Value 0xC5ACCE55 */
        Result = CortexA_WriteDebugRegister(CORTEXA_LOCKACCESS_OFSET,0xC5ACCE55);    
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        return Result;
        }

    /* Read DSCR */
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);        // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    *pulData = ulDSCR;

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadDSCR\n"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_InstrExecEnable
Engineer    : Amerdas D K
Input       : None          
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_InstrExecEnable(void)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    DLOG2(("Exit:Cortex-A8 function:CortexA_InstrExecEnable"));

    Result = Coresight_SelectBank(AP_BANK_0, tyCoreSightParam.ulDbgAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write Lock Access Register ;Value 0xC5ACCE55 */
    Result = CortexA_WriteDebugRegister(CORTEXA_LOCKACCESS_OFSET,0xC5ACCE55);    
    if (Result != ERR_NO_ERROR)
        {
        /* Write Lock Access Register ;Value 0xC5ACCE55 */
        Result = CortexA_WriteDebugRegister(CORTEXA_LOCKACCESS_OFSET,0xC5ACCE55);    
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        return Result;
        }

    /* Read DSCR;Enable Instruction execution in debug state;Write back */
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);        // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    ulDSCR |= CORTEXA_DSCR_EXT_INT_EN;

    DLOG2(("    ulDSCR Modified= 0x%08X",ulDSCR));

    Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Written Modified value:ulDSCR"));

    DLOG2(("Exit:Cortex-A8 function:CortexA_InstrExecEnable"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_EnterDebugState
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_EnterDebugState(unsigned long* pulRecData,
                                       unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;
    unsigned long ulPRSR;
    unsigned long ulDIDR;
    unsigned long ulCount;

    DLOG(("Enter:Cortex-A8 function:CortexA_EnterDebugState\n"));
    DLOG2(("tyCoreSightParam.ulDbgBaseAddr = 0x%8x", tyCoreSightParam.ulDbgBaseAddr));

    Result = Coresight_SelectBank(AP_BANK_0, tyCoreSightParam.ulDbgAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write Lock Access Register ;Value 0xC5ACCE55 */
    Result = CortexA_WriteDebugRegister(CORTEXA_LOCKACCESS_OFSET,0xC5ACCE55);    
    if (Result != ERR_NO_ERROR)
        {
        /* Write Lock Access Register ;Value 0xC5ACCE55 */
        Result = CortexA_WriteDebugRegister(CORTEXA_LOCKACCESS_OFSET,0xC5ACCE55);    
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("    Written 0xC5ACCE55 to CORTEXA_LOCKACCESS_OFSET"));
        return Result;
        }

    /* Read PRSR; Clear Sticky Power Down status Bit in PRSR to enable access to
       the registers in the Core Power Domain */
    Result = CortexA_ReadDebugRegister(CORTEXA_PRSR_OFFSET,&ulPRSR);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* If TI core with DAP PC support needs to modify the DAP PC control 
    register */ 
    if ( TRUE == *pulRecData )
        {
        /* Write the address of the register to be accessed in TAR */
        Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, pulRecData[1]);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
    
        /* Write the contents to the addressed register by placing it in DRW*/
        Result = CoreSight_WriteDapReg(MEM_AP_DRW_REG, pulRecData[2]);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }

    /* Write Core halt bit in DRCR */
    Result = CortexA_WriteDebugRegister(CORTEXA_DRCR_OFSET,CORTEXA_DRCR_CORE_HALT);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Written CORTEXA_DRCR_CORE_HALT to ulDRCR"));

    /* Read DSCR;Enable Halting Debug Mode;Write back*/
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    ulDSCR |= CORTEXA_DSCR_HALT_DBG_MODE;

    DLOG2(("    ulDSCR Modified= 0x%08X",ulDSCR));

    Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Written Modified value:ulDSCR"));

    ulCount = 0;
    /* Read DSCR;Check whether the core has entered debug state */
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulCount++;
        if (ulCount > MAX_WAIT_CNT)
            {
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
        DLOG2(("Read Value :ulDSCR = 0x%x", ulDSCR));
        }
    while (CORTEXA_DSCR_CORE_HALTED != (ulDSCR & CORTEXA_DSCR_CORE_HALTED));

    DLOG2(("To check the cause of halt :ulDSCR = 0x%x", ulDSCR));

    /* Read DSCR;Enable Instruction execution in debug state;Write back */
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);        // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    ulDSCR |= CORTEXA_DSCR_EXT_INT_EN;

    DLOG2(("    ulDSCR Modified= 0x%08X",ulDSCR));

    Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Written Modified value:ulDSCR"));

    /* Read DSCR;Issue a Data Synchronization Barrier instruction if required;  */
    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);        // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    if ((ulDSCR & CORTEXA_DSCR_DIS_IMP_ABORT) != CORTEXA_DSCR_DIS_IMP_ABORT)
        {
        /* Execute the instruction MRS R0,CPSR through the ITR */
        Result = CortexA_ExecuteInstruction(0xEE070F9A);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        do
            {
            /* Poll the DSCR for DSCR[19] to be set to 1 */
            Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);        // Add base address//
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
            }
        while ((ulDSCR & CORTEXA_DSCR_DIS_IMP_ABORT) == CORTEXA_DSCR_DIS_IMP_ABORT);
        }


    Result = CortexA_ReadDebugRegister(CORTEXA_DIDR_OFSET,&ulDIDR);        // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        DLOG((" Error : Reading DIDR"));
        return Result;
        }

    *pulData = ulDIDR;


    DLOG(("    ulDIDR = 0x%08X",ulDIDR));    

    DLOG(("Exit:Cortex-A8 function:CortexA_EnterDebugState\n"));    
    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadRegisters
Engineer    : Amerdas D K
Input       : unsigned long *pulData :Pointer to result data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadRegisters(unsigned long *pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulLoopCnt;
    unsigned long ulCPSR;

    DLOG(("Enter:Cortex-A8 function:CortexA_ReadRegisters"));

    /* Read the Current CPSR */
    Result = CortexA_ReadCPSR(&ulCPSR);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulCPSR = 0x%08X",ulCPSR));

    /* Read unbanked registers R0 - R7*/
    for (ulLoopCnt=0;ulLoopCnt<=7;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    Unbanked register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Read the Current PC*/
    Result = CortexA_ReadPC(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    PC = 0x%08X",*pulData));

    pulData++;

    /* Read the Current CPSR */
    Result = CortexA_ReadCPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulCPSR = 0x%08X",*pulData));

    pulData++;

    /* Change mode to user */
    Result = CortexA_ChangeMode(CortexAModeUser);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: USER"));

    /* Read registers R8 to 14*/
    for (ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    USER Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Change mode to FIQ */
    Result = CortexA_ChangeMode(CortexAModeFIQ);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: FIQ"));

    /* Read registers FIQ : R8 to 14*/
    for (ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    FIQ Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    Result = CortexA_ReadSPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    FIQ SPSR = 0x%08X",*pulData));

    pulData++;

    /* Change mode to IRQ */
    Result = CortexA_ChangeMode(CortexAModeIRQ);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: IRQ"));

    /* Read registers IRQ : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    IRQ Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Read registers IRQ : SPSR */
    Result = CortexA_ReadSPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    IRQ SPSR = 0x%08X",*pulData));

    pulData++;

    /* Change mode to SUPERVISOR */
    Result = CortexA_ChangeMode(CortexAModeSupervisor);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: SUPERVISOR"));

    /* Read registers IRQ : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    IRQ Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Read registers SUPERVISOR : SPSR */
    Result = CortexA_ReadSPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    SUPERVISOR SPSR = 0x%08X",*pulData));

    pulData++;

    /* Change mode to ABORT */
    Result = CortexA_ChangeMode(CortexAModeAbort);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: ABORT"));

    /* Read registers IRQ : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    ABORT Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Read registers SUPERVISOR : SPSR */
    Result = CortexA_ReadSPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    ABORT SPSR = 0x%08X",*pulData));

    pulData++;

    /* Change mode to UNDEFINED */
    Result = CortexA_ChangeMode(CortexAModeUndefined);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("    Mode: UNDEFINED"));

    /* Read registers IRQ : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG(("    UNDEFINED Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Read registers UNDEFINED : SPSR */
    Result = CortexA_ReadSPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("    UNDEFINED SPSR = 0x%08X",*pulData));

    pulData++;

    /* Change mode to Secure monitor mode */
    Result = CortexA_ChangeMode(CortexAModeSecureMonitor);
    if (ERR_ARM_UNABLE_CHANGE_MON == Result)
        {
        /* Set the R13_mon, R14_mon and SPSR_mon as 0xFFFFFFFF */
        *pulData = 0xFFFFFFFF;
        pulData++;
        *pulData = 0xFFFFFFFF;
        pulData++;
        *pulData = 0xFFFFFFFF;        
        Result = CortexA_WriteCPSR(ulCPSR);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }        
        }
    else if (Result != ERR_NO_ERROR)
        {        
        return Result;
        }

    DLOG(("    Mode: Secure monitor"));

    /* Read registers Secure monitor : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_ReadRegister(ulLoopCnt,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG(("    Secure monitor Register = 0x%08X,0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Read registers Secure monitor : SPSR */
    Result = CortexA_ReadSPSR(pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("    Secure monitor SPSR = 0x%08X",*pulData));

    pulData++;

    Result = CortexA_WriteCPSR(ulCPSR);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Restorted CPSR"));

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadRegisters"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteRegisters
Engineer    : Amerdas D K
Input       : unsigned long *pulData :Pointer to result data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteRegisters(unsigned long *pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulLoopCnt;
    //unsigned long ulCPSR;

    unsigned long z;

    DLOG(("Enter:Cortex-A8 function:CortexA_WriteRegisters"));

    for (z=0;z<37;z++)
        {
        DLOG2(("    *pulData = 0x%08X",*(pulData + z)));
        }

    /* Change mode to FIQ */
    Result = CortexA_ChangeMode(CortexAModeFIQ);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: FIQ"));

    /* Write FIQ : SPSR */
    Result = CortexA_WriteSPSR(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    pulData++;

    /* Write registers FIQ : R8 to 14*/
    for (ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    Written FIQ Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Change mode to IRQ */
    Result = CortexA_ChangeMode(CortexAModeIRQ);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: IRQ"));

    /* Write registers IRQ : SPSR */
    Result = CortexA_WriteSPSR(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    pulData++;

    /* Write registers IRQ : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("    Written IRQ Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Change mode to SUPERVISOR */
    Result = CortexA_ChangeMode(CortexAModeSupervisor);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Mode: SUPERVISOR"));

    /* Write SUPERVISOR : SPSR */
    Result = CortexA_WriteSPSR(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    pulData++;

    /* Write registers SUPERVISOR : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    Written SUPERVISOR Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Change mode to ABORT */
    Result = CortexA_ChangeMode(CortexAModeAbort);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Mode: ABORT"));

    /* Write ABORT : SPSR */
    Result = CortexA_WriteSPSR(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    pulData++;

    /* Write registers ABORT : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("    Written ABORT Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Change mode to UNDEFINED */
    Result = CortexA_ChangeMode(CortexAModeUndefined);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: UNDEFINED"));

    /* Write UNDEFINED : SPSR */
    Result = CortexA_WriteSPSR(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    pulData++;

    /* Write registers UNDEFINED : R3,14*/
    for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("    Written UNDEFINED Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    /* Change mode to Monitor */
    Result = CortexA_ChangeMode(CortexAModeSecureMonitor);
    if (ERR_ARM_UNABLE_CHANGE_MON == Result)
        {
        /* Set the R13_mon, R14_mon and SPSR_mon as 0xFFFFFFFF */
        *pulData = 0xFFFFFFFF;
        pulData++;
        *pulData = 0xFFFFFFFF;
        pulData++;
        *pulData = 0xFFFFFFFF;   
        pulData++;
        }
    else if (Result != ERR_NO_ERROR)
        {        
        return Result;
        }
    else
        {
        DLOG(("    Mode: MONITOR"));

        DLOG(("    Written MONITOR SPSR Register 0x%08X",*pulData));
        /* Write MONITOR : SPSR */
        Result = CortexA_WriteSPSR(*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
    
        pulData++;
    
        /* Write registers MONITOR : R3,14*/
        for (ulLoopCnt=13;ulLoopCnt<=14;ulLoopCnt++)
            {
            Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
    
            DLOG(("    Written MONITOR Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
            pulData++;       
            }
        }

    /* Change mode to user */
    Result = CortexA_ChangeMode(CortexAModeUser);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    Mode: USER"));

    /* Write registers R8 to 14*/
    for (ulLoopCnt=8;ulLoopCnt<=14;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        DLOG2(("    Written USER Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    Result = CortexA_WriteCPSR(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    pulData++;
    DLOG2(("    Restorted CPSR"));

    /* Write registers R0 to R7*/
    for (ulLoopCnt=0;ulLoopCnt<=7;ulLoopCnt++)
        {
        Result = CortexA_WriteRegister(ulLoopCnt,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("    Written UNBANKED Register 0x%08X 0x%08X",ulLoopCnt,*pulData));
        pulData++;       
        }

    Result = CortexA_WritePC(*pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    Restorted PC"));

    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteRegisters"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_SetDTRAccessMode
Engineer    : Amerdas D K
Input       : u           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_SetDTRAccessMode(unsigned long ulDTRMode)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    DLOG2(("Enter:Cortex-A8 function:CortexA_SetDTRAccessMode"));

    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulDSCR = ((ulDSCR & (~CORTEXA_DSCR_DTR_ACC_MASK)) | ulDTRMode);

    DLOG2(("    CortexA_SetDTRAccessMode:ulDSCR = 0x%08X",ulDSCR));

    Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_SetDTRAccessMode"));

    return Result;  
}

/****************************************************************************
Function    : CortexA_CheckForAbort
Engineer    : Amerdas D K
Input       : void           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_CheckForAbort(unsigned char *pucReadData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    *pucReadData = 0;  
    DLOG2(("Enter:Cortex-A8 function:CortexA_CheckForAbort"));

    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    if (ulDSCR & (CORTEXA_DSCR_STICKY_PRE_ABORT | CORTEXA_DSCR_STICKY_IMPRE_ABORT))
        {
        *pucReadData = 1;
        Result = CortexA_WriteDebugRegister(CORTEXA_DRCR_OFSET,CORTEXA_DRCR_CLEAR_STICKY_EXEPT);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_CheckForAbort"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteMemoryByte
Engineer    : Amerdas D K
Input       : unsigned long ulAddr   : Address to write into
              unsigned long* pulData : Pointer to result data
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteMemoryByte(unsigned long ulAddr,
                                       unsigned char ucWriteData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("Enter:Cortex-A8 function:CortexA_WriteMemoryByte"));

    if (tyCoreSightParam.ulUseMemApMemAccess)
       {
       return CortexA_WriteMemory_MemAp(ulAddr,1, ACCESS_SIZE_8, (unsigned long*)&ucWriteData); 
       }

    /* Write R0 with address;R0 <- Memory Address */
    Result = CortexA_WriteRegister(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write R1 with data;R1 <- Data */
    Result = CortexA_WriteRegister(1,ucWriteData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction STRB R1,[R0] through the ITR */
    Result = CortexA_ExecuteInstruction(0xE5C01000);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    
    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteMemoryByte"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadMemoryByte
Engineer    : Amerdas D K
Input       : unsigned long ulAddr   : Address to read from
              unsigned long* pulData : Pointer to result data
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial

****************************************************************************/
unsigned long CortexA_ReadMemoryByte(unsigned long  ulAddr,
                                      unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("Enter:Cortex-A8 function:CortexA_ReadMemoryByte"));
    DLOG2(("    ulAddr = 0x%08X",ulAddr));


    /* Write R0 with address;R0 <- Memory Address */
    Result = CortexA_WriteRegister(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction LDRB R1,[R0] through the ITR */
    Result = CortexA_ExecuteInstruction(0xE5D01000);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read register R1: R1 = [Memory (byte)]*/
    Result = CortexA_ReadRegister(1,pulData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadMemoryByte"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteMemoryByteMultiple
Engineer    : Amerdas D K
Input       : unsigned long ulAddr   : Address to write into
              unsigned long ulCnt    : Number of bytes to read
              unsigned long* pulData : Pointer to result data
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteMemoryByteMultiple(unsigned long ulAddr,
                                               unsigned long ulCnt,
                                               unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("CortexA_WriteMemoryByteMultiple"));

    /* Write R0 with address;R0 <- Memory Address */
    Result = CortexA_WriteRegister(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    while (ulCnt)
        {
        /* Write R1 with data;R1 <- Data */
        Result = CortexA_WriteRegister(1,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Execute the instruction STRB R1,[R0],#1 through the ITR */
        Result = CortexA_ExecuteInstruction(0xE4C01001);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        pulData++;
        ulCnt--;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteMemoryByte"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadMemoryByteMultiple
Engineer    : Amerdas D K
Input       : unsigned long ulAddr   : Address to read from
              unsigned long ulCnt    : Number of bytes to read
              unsigned long* pulData : Pointer to result data
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadMemoryByteMultiple(unsigned long ulAddr,
                                              unsigned long ulCnt,
                                              unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("CortexA_ReadMemoryByteMultiple "));
    /* Write R0 with address;R0 <- Memory Address */
    Result = CortexA_WriteRegister(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    while (ulCnt)
        {
        /* Execute the instruction LDRB R1,[R0],#1 through the ITR */
        Result = CortexA_ExecuteInstruction(0xE4D01001);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Read register R1: R1 = [Memory (byte)]*/
        Result = CortexA_ReadRegister(1,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        pulData++;
        ulCnt--;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadMemoryByteMultiple"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteMemoryWord
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteMemoryWord(unsigned long ulAddr,
                                       unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    DLOG(("CortexA_WriteMemoryWord"));

    /* Write R0 with address;R0 <- Memory Address */
    Result = CortexA_WriteRegister(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Check the DRTRX full flag untill '0'*/
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
    while (CORTEXA_DSCR_DTR_RX_FULL == (ulDSCR & CORTEXA_DSCR_DTR_RX_FULL));

    /* Write the data in DTRRX */
    Result = CortexA_WriteDebugRegister(CORTEXA_DTRRX_OFSET,*pulData);           // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction STC p14,C5,[R0] through the ITR */
    Result = CortexA_ExecuteInstruction(0xED805E00);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteMemoryWord"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadMemoryWord
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadMemoryWord(unsigned long ulAddr,
                                      unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    DLOG(("CortexA_ReadMemoryWord "));

    if (tyCoreSightParam.ulUseMemApMemAccess)
       {
       return CortexA_ReadMemory_MemAp(ulAddr, 1, ACCESS_SIZE_32, pulData); 
       }

    /* Write R0 with address;R0 <- Memory Address */
    Result = CortexA_WriteRegister(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction LDC p14,C5,[R0] through the ITR */
    Result = CortexA_ExecuteInstruction(0xED905E00);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    /* Check the DRTTX full flag untill set */
    do
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }
    while (CORTEXA_DSCR_DTR_TX_FULL != (ulDSCR & CORTEXA_DSCR_DTR_TX_FULL));

    /* Read the register value through DTRTX */
    Result = CortexA_ReadDebugRegister(CORTEXA_DTRTX_OFSET ,pulData);         // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("    CortexA_ReadMemoryWord:pulData = 0x%08X",*pulData));

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadMemoryByte"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_WriteMemoryWordMultiple
Engineer    : Amerdas D K
Input       : unsigned long ulAddr   : Address to write into
              unsigned long ulCnt    : Number of bytes to read
              unsigned long* pulData : Pointer to result data            
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteMemoryWordMultiple(unsigned long ulAddr,
                                               unsigned long ulCnt,
                                               unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("CortexA_WriteMemoryWordMultiple"));

    if (tyCoreSightParam.ulUseMemApMemAccess)
       {
       return CortexA_WriteMemory_MemAp(ulAddr,ulCnt, ACCESS_SIZE_32, pulData); 
       }

    /* Write the value b10 to DSCR[21:20] for fast mode */
    Result = CortexA_SetDTRAccessMode(CORTEXA_DTR_FAST);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Execute the instruction MRC p14,0,r0,c5,c0 to the ITR */
    Result = CortexA_WriteDebugRegister(CORTEXA_ITR_OFSET,0xEE100E15);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write the address to read from to the DTRRX */
    Result = CortexA_WriteDebugRegister(CORTEXA_DTRRX_OFSET,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Write the opcode for STC p14,c5,[r0],4 to the ITR */
    Result = CortexA_WriteDebugRegister(CORTEXA_ITR_OFSET,0xECA05E01);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    while (ulCnt)
        {
        Result = CortexA_WriteDebugRegister(CORTEXA_DTRRX_OFSET,*pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        pulData++;
        ulCnt--;
        }

    /* Write the value 2'b00 to DSCR[21:20] for normal mode */
    Result = CortexA_SetDTRAccessMode(CORTEXA_DTR_NON_BLOCK);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("Exit:Cortex-A8 function:CortexA_WriteMemoryWordMultiple\n"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_ReadMemoryWordMultiple
Engineer    : Amerdas D K
Input       : unsigned long ulAddr   : Address to write into
              unsigned long ulCnt    : Number of bytes to read
              unsigned long* pulData : Pointer to result data           
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadMemoryWordMultiple(unsigned long ulAddr,
                                              unsigned long ulCnt,
                                              unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("CortexA_ReadMemoryWordMultiple "));

    if (tyCoreSightParam.ulUseMemApMemAccess)
       {
       return CortexA_ReadMemory_MemAp(ulAddr, ulCnt, ACCESS_SIZE_32, pulData); 
       }

    /* Write the value b01 to DSCR[21:20] for stall mode */
    Result = CortexA_SetDTRAccessMode(CORTEXA_DTR_STALL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    /* Write the address to read from to R0 */
    Result = CortexA_WriteRegisterStall(0,ulAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    /* Write the opcode for LDC p14,c5,[r0],4 to the ITR */
    Result = CortexA_WriteDebugRegister(CORTEXA_ITR_OFSET,0xECB05E01);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    /* Write the value b10 to DSCR[21:20] for fast mode */
    Result = CortexA_SetDTRAccessMode(CORTEXA_DTR_FAST);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    while (ulCnt)
        {
        Result = CortexA_ReadDebugRegister(CORTEXA_DTRTX_OFSET,pulData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("    *pulData = 0x%08X 0x%08X 0x%08X",ulCnt,pulData ,*pulData));

        pulData++;
        ulCnt--;
        }
    /* Write the value 2'b00 to DSCR[21:20] for normal mode */
    Result = CortexA_SetDTRAccessMode(CORTEXA_DTR_NON_BLOCK);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadMemoryWordMultiple\n"));

    return Result;  
}
/****************************************************************************
Function    : CortexA_StatusProc
Engineer    : Amerdas D K
Input       : unsigned char* pucCoreExecuting   : 
              unsigned char* pucThumbMode       :
              unsigned char* pucWatchPoint      :        
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_StatusProc(unsigned char* pucCoreExecuting,
                                  unsigned char* pucThumbMode,
                                  unsigned char* pucWatchPoint)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR = 0;
    unsigned long ulCPSR = 0;


    *pucCoreExecuting   = TRUE;
    *pucThumbMode       = FALSE;
    *pucWatchPoint      = FALSE;

    DLOG(("Enter:Cortex-A8 function:CortexA_StatusProc"));

    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDSCR = 0x%08X",ulDSCR));

    /* Check whether the core is Halted */
    if (ulDSCR & CORTEXA_DSCR_CORE_HALTED)
        {
        *pucCoreExecuting = FALSE;

        if ((ulDSCR & CORTEXA_DSCR_ENTRY_MASK)== CORTEXA_DSCR_ENTRY_WATCHPOINT)
            *pucWatchPoint = TRUE;

        ulDSCR |= CORTEXA_DSCR_EXT_INT_EN;
  
        Result = CortexA_WriteDebugRegister(CORTEXA_DSCR_OFSET,ulDSCR);    // Add base address//
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        Result = CortexA_ReadCPSR(&ulCPSR);         // Execute instruction in degub state required 
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        if (ulCPSR & CORTEXA_CPSR_T_BIT)
            *pucThumbMode = TRUE;
        }

    DLOG(("Exit:Cortex-A8 function:CortexA_StatusProc"));

    return Result; 
}
/****************************************************************************
Function    : CortexA_BreakPoint
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data      
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_BreakPoint(unsigned short usNumBreakPoint,unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long i;
    unsigned long ulBVRvalue;
    unsigned long ulBCRvalue;

    DLOG2(("Enter:Cortex-A8 function:CortexA_BreakPoint 0x%08x", usNumBreakPoint));

    for (i=0;i < (usNumBreakPoint * 4);i += 4)
        {
        /* Get the Address for watchpoint;mask the lower two bits */
        ulBVRvalue = (*pulData & 0xFFFFFFFC);

        /* Write address to the BVR, leaving the bottom 2 bits zero */
        Result = CortexA_WriteDebugRegister((CORTEXA_BVR_OFSET + i),ulBVRvalue);  
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Next is thecorresponding BCR value */
        pulData++;
        ulBCRvalue = *pulData++;


        /* Write the mask and control register to enable the breakpoint */
        Result = CortexA_WriteDebugRegister((CORTEXA_BCR_OFSET + i),ulBCRvalue);        
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_BreakPoint"));

    return Result; 
}
/****************************************************************************
Function    : CortexA_WatchPoint
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data      
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WatchPoint(unsigned short usNumWatchPoint,unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long i;
    unsigned long ulWVRvalue = 0x00000000;
    unsigned long ulWCRvalue = 0x00000000;;

    DLOG(("Enter:Cortex-A8 function:CortexA_WatchPoint0x%08x\n", usNumWatchPoint));

    for (i = 0;i < (usNumWatchPoint * 4);i += 4)
        {
        /* Get the Address for watchpoint;mask the lower two bits */
        ulWVRvalue = (*pulData & 0xFFFFFFFC);

        /* Write address to the WVR, leaving the bottom 2 bits zero. */
        DLOG(("Watchpoint Value : 0x%08x\n", ulWVRvalue));
        Result = CortexA_WriteDebugRegister((CORTEXA_WVR_OFSET + i),ulWVRvalue);  
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        /* Next is thecorresponding WCR value */
        pulData++;
        ulWCRvalue = *pulData++;

        DLOG(("Watchpoint Control : 0x%08x\n", ulWCRvalue));
        /* Write the mask and control register to enable the watchpoint */
        Result = CortexA_WriteDebugRegister((CORTEXA_WCR_OFSET + i),ulWCRvalue);        
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        }
    DLOG(("Exit:Cortex-A8 function:CortexA_WatchPoint\n"));

    return Result; 
}
/****************************************************************************
Function    : CortexA_VectorCatch
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data      
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
21-Apr-2010      ADK       Initial
****************************************************************************/
unsigned long CortexA_VectorCatch(unsigned long ulData)
{
    unsigned long Result = ERR_NO_ERROR;

    DLOG(("Enter:Cortex-A8 function:CortexA_VectorCatch\n"));

    /* Write address to the VCR */
    DLOG(("VCR Value : 0x%08x\n", ulData));

    Result = CortexA_WriteDebugRegister(CORTEXA_VCR_OFSET , ulData);  
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("Exit:Cortex-A8 function:CortexA_VectorCatch\n"));

    return Result; 
}
/****************************************************************************
Function    : CortexA_ReadDIDR
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data      
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
21-Apr-2010      ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadDIDR(unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDIDR;

    DLOG(("Enter:Cortex-A8 function:CortexA_ReadDIDR\n"));

    /* Read DIDR */
    Result = CortexA_ReadDebugRegister(CORTEXA_DIDR_OFSET,&ulDIDR);        // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    ulDIDR = 0x%08X",ulDIDR));

    *pulData = ulDIDR;

    return Result; 
}

/****************************************************************************
Function    : CortexA_WriteRegisterStall
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data      
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_WriteRegisterStall(unsigned long ulRegNum,
                                          unsigned long ulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulInst;

    DLOG2(("Enter:Cortex-A8 function:CortexA_WriteRegisterStall"));
    DLOG2(("    ulRegNum = 0x%08X",ulRegNum));
    DLOG2(("    ulData = 0x%08X",ulData));

    /* Write to DTRRX;the data to write into the register */
    Result = CortexA_WriteDebugRegister(CORTEXA_DTRRX_OFSET,ulData);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulInst = (0xEE100E15 + (ulRegNum << 12));

    /* Write the Opcode for MRC p14,0,<Rd>,c0,c5,0 to ITR */
    Result = CortexA_WriteDebugRegister(CORTEXA_ITR_OFSET,ulInst);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Exit:Cortex-A8 function:CortexA_WriteRegisterStall"));

    return Result; 
}
/****************************************************************************
Function    : CortexA_ReadRegisterStall
Engineer    : Amerdas D K
Input       : unsigned long* pulData : Pointer to result Data      
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadRegisterStall(unsigned long ulRegNum,
                                         unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulInst;

    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadRegisterStall"));
    DLOG2(("    ulRegNum = 0x%08X",ulRegNum));

    ulInst = (0xEE000E15 + (ulRegNum << 12));

    /* Write the Opcode for MCR p14,0,<Rd>,c0,c5,0 to ITR */
    Result = CortexA_WriteDebugRegister(CORTEXA_ITR_OFSET,ulInst);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read DTRTX;To get the value of addressed register */
    Result = CortexA_ReadDebugRegister(CORTEXA_DTRTX_OFSET,pulData);    // Add base address//
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG2(("    pulData = 0x%08X",*pulData));

    DLOG2(("Exit:Cortex-A8 function:CortexA_ReadRegisterStall"));

    return Result; 
}
/****************************************************************************
Function    : CortexA_ReadWriteDCC
Engineer    : Amerdas D K
Input       : unsigned char *pucDataToWrite - Is there Data to Write, 
              unsigned long *pulWriteData - the Data to write
              unsigned char *pucDataWriteSuccessful - Return is Write successful, 
              unsigned char *pucDataReadFromCore - return is there data from core, 
              unsigned long *pulReadData - Return Data from core  
Output      : unsigned long - error code for commands (ERR_xxx)
Description : 
Date            Initials    Description
12-Mar-2010       ADK       Initial
****************************************************************************/
unsigned long CortexA_ReadWriteDCC(unsigned char *pucDataToWrite,
                                    unsigned long *pulWriteData,
                                    unsigned char *pucDataWriteSuccessful,
                                    unsigned char *pucDataReadFromCore,
                                    unsigned long *pulReadData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulDSCR;

    DLOG2(("Enter:Cortex-A8 function:CortexA_ReadWriteDCC"));

    Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);   
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    DLOG(("ulDSCR 0x%08x", ulDSCR));
    /* Make the flags False */
    *pucDataWriteSuccessful = FALSE;
    *pucDataReadFromCore    = FALSE;

    /* Check whether to write : Write the data and make the flag true */
    if (*pucDataToWrite == TRUE)
        {
        /* Check the DRTRX full flag untill '0'*/
        do
            {
            Result = CortexA_ReadDebugRegister(CORTEXA_DSCR_OFSET,&ulDSCR);   
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
            }
        while (CORTEXA_DSCR_DTR_RX_FULL == (ulDSCR & CORTEXA_DSCR_DTR_RX_FULL));

        /* Write the data in DTRRX */
        Result = CortexA_WriteDebugRegister(CORTEXA_DTRRX_OFSET,*pulWriteData);          
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        *pucDataWriteSuccessful = TRUE;

        }
    else if (CORTEXA_DSCR_DTR_TX_FULL == (ulDSCR & CORTEXA_DSCR_DTR_TX_FULL))
        {
        /* Read the value from DTRTX */
        DLOG(("Read the Data from DTRTX"));
        Result = CortexA_ReadDebugRegister(CORTEXA_DTRTX_OFSET ,pulReadData);         
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        /* Make the flag True */
        *pucDataReadFromCore    = TRUE;
        }

    DLOG(("Exit:Cortex-A8 function:CortexA_ReadWriteDCC"));

    return Result; 
}

/****************************************************************************
Function    : CortexA_WriteMemory_MemAp
Engineer    : Jeenus C.K.
Input       : unsigned long ulStartAddr - Start address
            : unsigned long ulCount - Number of words to be written
            : unsigned long* pulData - Pointer to data.  
Output      : unsigned long - error code for commands (ERR_xxx)
Description : Inorder ot modify the memory.
Date            Initials    Description
12-Mar-2010       JCK       Initial
****************************************************************************/
unsigned long CortexA_WriteMemory_MemAp (unsigned long ulStartAddr,
                                   unsigned long ulCount,
                                   unsigned long ulAccessSize,
                                   unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulBlockSize = 0;
    unsigned long ulWordCount = 0;
    //unsigned char* pucData = (unsigned char*)pulData;

    DLOG(("CortexA_WriteMemory_MemAp"));

    Result = CoreSight_SetupAccessPort (ulAccessSize);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2 (("CortexA_WriteMemory 0x%08x, 0x%08x, 0x%08x", ulStartAddr, ulCount, ulAccessSize));
    while (ulCount)
        {
        CoreSight_MaxTARBlockSize ( CORTEXA_MAX_TAR_INC_SIZE, 
                                    ulStartAddr, 
                                    &ulBlockSize);
        
        if (ulCount < ulBlockSize)
            {
            ulBlockSize = ulCount;
            }

        if ( 0 == ulBlockSize)
            {
            ulBlockSize = 1;
            }

        //DLOG2(("BlockSize = 0x%08x, StartAddr = 0x%08x", ulBlockSize, ulStartAddr));
        Result = CoreSight_WriteDapReg (MEM_AP_TAR_REG, ulStartAddr);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulWordCount = 0;
        do
            {
            Result = CoreSight_WriteDapReg (MEM_AP_DRW_REG, *pulData);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }

            DLOG2(("Write data = 0x%08x", *pulData));
            pulData = pulData + 1;
            ulWordCount = ulWordCount + 1;
            }
        while ( ulBlockSize > ulWordCount);
        

        /*Result = CoreSight_WriteMultipleMem (ulBlockSize, pucData, ulAccessSize);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }*/

        
        ulCount = ulCount - ulBlockSize;
        ulStartAddr = ulStartAddr + ulBlockSize * 4;
        }

    Result = CoreSight_ClearStickyError ();
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = Coresight_SelectBank (AP_BANK_0 , tyCoreSightParam.ulDbgAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function    : CortexA_ReadMemory_MemAp
Engineer    : Jeenus C.K.
Input       : unsigned long ulStartAddr - Start address
            : unsigned long ulCount - Number of words to be written
            : unsigned long* pulData - Pointer to data.  
Output      : unsigned long - error code for commands (ERR_xxx)
Description : To read memory using mem-ap bus.
Date            Initials    Description
12-Mar-2010       JCK       Initial
****************************************************************************/
unsigned long CortexA_ReadMemory_MemAp (unsigned long ulStartAddr,
                                   unsigned long ulCount,
                                   unsigned long ulAccessSize,
                                   unsigned long* pulData)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulBlockSize = 0;
    unsigned long ulWordCount = 0;
    //unsigned char* pucData = (unsigned char*)pulData;

    DLOG(("CortexA_ReadMemory_MemAp"));

    Result = CoreSight_SetupAccessPort (ulAccessSize);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2 (("CortexA_ReadMemory_MemAp 0x%08x, 0x%08x, 0x%08x", ulStartAddr, ulCount, ulAccessSize));
    while (ulCount)
        {
        CoreSight_MaxTARBlockSize ( CORTEXA_MAX_TAR_INC_SIZE, 
                                    ulStartAddr, 
                                    &ulBlockSize);
        
        if (ulCount < ulBlockSize)
            {
            ulBlockSize = ulCount;
            }

        if ( 0 == ulBlockSize)
            {
            ulBlockSize = 1;
            }

        Result = CoreSight_WriteDapReg (MEM_AP_TAR_REG, ulStartAddr);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulWordCount = 0;
        do
            {
            Result = CoreSight_ReadDapReg (MEM_AP_DRW_REG, pulData);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }

            DLOG2(("Read Data = 0x%08x", *pulData));
            pulData = pulData + 1;
            ulWordCount = ulWordCount + 1;
            }
        while ( ulBlockSize > ulWordCount);
        

        /*Result = CoreSight_WriteMultipleMem (ulBlockSize, pucData, ulAccessSize);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }*/

        
        ulCount = ulCount - ulBlockSize;
        ulStartAddr = ulStartAddr + ulBlockSize * 4;
        }

    Result = CoreSight_ClearStickyError ();
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = Coresight_SelectBank (AP_BANK_0 , tyCoreSightParam.ulDbgAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function    : CortexA_InvalidateCache
Engineer    : Jeenus C.K.
Input       : none  
Output      : unsigned long - error code for commands (ERR_xxx)
Description : Inorder to Invalidate the Instruction cache and data cache
Date            Initials    Description
12-Mar-2010       JCK       Initial
****************************************************************************/
unsigned long CortexA_InvalidateCache(void)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulCoProcInst;
    unsigned long ulControlRegData;

    ulCoProcInst = ARMV4_5_MRC(CP15_COPROC, 
                               Cp15ControlReg.ulOp1, 
                               Cp15ControlReg.ulOp2, 
                               Cp15ControlReg.ulCRn, 
                               Cp15ControlReg.ulCRm);

    DLOG2(("ulCoProcInst for Cp15ControlReg == 0x%08x", ulCoProcInst));

    Result = CortexA_ReadCoProcessor( &ulCoProcInst, &ulControlRegData );
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("Cp15ControlReg value == 0x%08x", ulControlRegData));
    if (DATA_CACHE_ENABLED == (ulControlRegData & DATA_CACHE_ENABLED))
        {
        Result = CortexA_InvalidateDataCache();
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        }

    if (INSTRUCTION_CACHE_ENABLED == (ulControlRegData & INSTRUCTION_CACHE_ENABLED))
        {
        ulCoProcInst = ARMV4_5_MCR(CP15_COPROC, 
                               Cp15InvalidateAllInstCache.ulOp1, 
                               Cp15InvalidateAllInstCache.ulOp2, 
                               Cp15InvalidateAllInstCache.ulCRn, 
                               Cp15InvalidateAllInstCache.ulCRm);
        
        Result = CortexA_WriteCoProcessor( &ulCoProcInst, 0x0 );
        if (Result != ERR_NO_ERROR)
            {
            DLOG2(("CortexA_WriteCoProcessor; INSTRUCTION_CACHE_ENABLED: Error" ));
            return Result;
            }
        }

    return Result;
}

/****************************************************************************
Function    : CortexA_InvalidateDataCache
Engineer    : Jeenus C.K.
Input       : none  
Output      : unsigned long - error code for commands (ERR_xxx)
Description : To Invalidate the data cache
Date            Initials    Description
12-Mar-2010       JCK       Initial
****************************************************************************/
static unsigned long CortexA_InvalidateDataCache(void)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned long ulCoProcInst;
    unsigned long ulCacheSizeIdData; 
    unsigned long ulNumSets;
    unsigned long ulNumWays;
    unsigned long unNumSetIndex;
    unsigned long ulNumWayIndex;
    unsigned long ulSetWayRegData;

    /* Read the Cache Size Selection Register to select the Level 1 cache of data cache */
    ulCoProcInst = ARMV4_5_MRC(CP15_COPROC, 
                           Cp15CacheSizeSelection.ulOp1, 
                           Cp15CacheSizeSelection.ulOp2, 
                           Cp15CacheSizeSelection.ulCRn, 
                           Cp15CacheSizeSelection.ulCRm);
    Result = CortexA_WriteCoProcessor( &ulCoProcInst, CSSR_LEVEL_1_CACHE | CSSR_DATA_CACHE );
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read the Cache Size Identification Registers to identify the num of sets and ways. */
    ulCoProcInst = ARMV4_5_MRC(CP15_COPROC, 
                           Cp15CacheSizeIdentifation.ulOp1, 
                           Cp15CacheSizeIdentifation.ulOp2, 
                           Cp15CacheSizeIdentifation.ulCRn, 
                           Cp15CacheSizeIdentifation.ulCRm);
    Result = CortexA_ReadCoProcessor( &ulCoProcInst, &ulCacheSizeIdData );
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulNumSets = ( ulCacheSizeIdData & CSIR_NUMSETS_MASK ) >> CSIR_NUMSETS_SHIFT;
    ulNumWays = ( ulCacheSizeIdData & CSIR_NUMWAYS_MASK ) >> CSIR_NUMWAYS_SHIFT;

    ulCoProcInst = ARMV4_5_MRC(CP15_COPROC, 
                           Cp15CleanandInvalidateDataSetWay.ulOp1, 
                           Cp15CleanandInvalidateDataSetWay.ulOp2, 
                           Cp15CleanandInvalidateDataSetWay.ulCRn, 
                           Cp15CleanandInvalidateDataSetWay.ulCRm);

    for ( unNumSetIndex = 0; unNumSetIndex < ulNumSets; unNumSetIndex++ )
        {
        for (ulNumWayIndex = 0; ulNumWayIndex < ulNumWays; ulNumWayIndex++ )
            {
            ulSetWayRegData =   SETWAY_LEVEL1 |
                                ( unNumSetIndex << SETWAY_SET_SHIFT) | 
                                ( ulNumWayIndex << SETWAY_WAY_SHIFT); 
            Result = CortexA_WriteCoProcessor( &ulCoProcInst, ulSetWayRegData );
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
            }
        }

    return Result;
}

