/******************************************************************************
       Module: Cortex.h
     Engineer: Jeenus C.K.
  Description: Header for Cortex-M3 implementations
  Date           Initials    Description
  28-May-2009    JCK          initial  
******************************************************************************/
#ifndef _CORTEXM_H_
#define _CORTEXM_H_

#include "coresight.h"

#define DATA_READ_PREV      1
#define DATA_NOT_READ_PREV  0

/* Cortex M3 ROM table entry format dormat */
typedef struct
{
    UWORD ulNvicBaseAddr;
    UWORD ulDwtBaseAddr;
    UWORD ulFpbBaseAddr;
    UWORD ulItmBaseAddr;
    UWORD ulTpiuBaseAddr;
    UWORD ulEtmBaseAddr;
}CortexM;

/* Core debug registers */
typedef volatile struct 
{
    CM_REG CDR_DHCSR;  /* Debug Halting Control and Status Register */
    CM_REG CDR_DCRSR;  /* Debug Core Register Selector Register */
    CM_REG CDR_DCRDR;  /* Debug Core Register Data Register */
    CM_REG CDR_DEMCR;  /* Debug Exception and Monitor Control Register */
}CM_CDBGR, *pCM_CDBGR;

/* DHCSR : Debug Halting Control and Status Register: While Writing*/
#define DHCR_WR_DBGKEY         0xA05F0000       /* Debug Key */
#define DHCR_WR_CSNAPSTALL     (0x1     << 5)   /* Core is stalled and forced to complete */
#define DHCR_WR_CMASKINTS      (0x1     << 3)   /* Mask interrupts */
#define DHCR_WR_CSTEP          (0x1     << 2)   /* Steps the core in halted debug */
#define DHCR_WR_CHALT          (0x1     << 1)   /* Halts the core */
#define DHCR_WR_CDEBUGG_EN     (0x1     << 0)   /* Enables debug */

/* DHCSR : Debug Halting Control and Status Register: While Reading */
#define DHCSR_RD_SRESETST       (0x1 << 25)  /* Indicates that the core has been reset */
#define DHCSR_RD_SRETIREST      (0x1 << 24)  /* Indicates that an instruction has completed since last read */
#define DHCSR_RD_SLOCKUP        (0x1 << 19)  /* Reads as one if the core is running */
#define DHCSR_RD_SSLEEP         (0x1 << 18)  /* Indicates that the core is sleeping */
#define DHCSR_RD_SHALT          (0x1 << 17)  /* The core is in debug state */
#define DHCSR_RD_SREGRDY        (0x1 << 16)  /* Register Read/Write on the Debug Core Register 
                                                Selector register is available*/
#define DHCSR_RD_CSNAPSTALL     (0x1 << 5)   /* Core is stalled */
#define DHCSR_RD_CMASKINTS      (0x1 << 3)   /* Mask Interrupt status */
#define DHCSR_RD_CSTEP          (0x1 << 2)   /* Steps the core in halted debug */
#define DHCSR_RD_CHALT          (0x1 << 1)   /* Halts the core */
#define DHCSR_RD_CDEBUGGEN      (0x1 << 0)   /* Enables debug */

/* DCRSR: Debug Core Register Selector Register */
#define DCRSR_REG_WnR           (0x1 << 16)         /* Read or Write */
#define     DCRSR_REG_WnR_WRITE     (0x1 << 16)     /* Write to a register */
#define     DCRSR_REG_WnR_READ      (0x0 << 16)     /* Read from register */
#define DCRSR_REG_SEL           (0x1F << 0)         /* Select register */

typedef struct
{
    CM_REG FP_CTRL;    /* Flash Patch Control Register */
    CM_REG FP_REMAP;   /* Flash Patch Remapping Register */ 
    CM_REG FP_COMP0;   /* Flash Patch Comparator 0 Control Register */
}FPB_REG, *pFPB_REG;

#define DHCSR_ADDR              0xE000EDF0
#define DHCSR_HALT_BIT          (0x00020000)
#define DFSR_ADDR               (0xE000ED30)
#define DFSR_DWTTRAP_BIT        (0x00000004)
#define DFSR_BKPT_BIT           (0x00000002)
#define SELECT_WRITE_DCRSR      (0x00010000)
#define SELECT_READ_DCRSR       (0x00000000)
#define DCRSR_ADDR              (0xE000EDF4)
#define DHCSR_S_REGRDY_BIT      (0x00010000)
#define DHCSR_S_STEP_BIT        (0xA05F000D) // For enabling Single step.
#define DHCSR_MASK_INT          (DHCR_WR_DBGKEY | DHCR_WR_CMASKINTS | DHCR_WR_CHALT | DHCR_WR_CDEBUGG_EN)

/* Flash Patch Control Register Format */
#define FP_CTRL_ENABLE          (0x1 << 0)
#define FP_CTRL_KEY             (0x1 << 1)
#define FP_CTRL_NUM_CODE1       (0xF << 4)
#define FP_CTRL_NUM_LIT         (0xF << 4)
#define FP_CTRL_NUM_CODE2       (0xF << 4)
#define SET_TEST                (6)

/* Registers of Data WatchPoint and Trace Support */
    /* Register of Watchpoint */
#define NUM_WATCHPOINT_REG      0x3

/* The maximum increment boundary of the Auto-increment feature of the TAR register */
#define MAX_AUTO_INC_BOUNDARY   0x1000  /* 4Kb is the maximum address bounadry. Ref: Cortex-M3 r1p1 
                                           Control and Status Word Register format */

/* Access size */
#define ADDR_INC_SIZE_ACCESS_8   1
#define ADDR_INC_SIZE_ACCESS_16  2
#define ADDR_INC_SIZE_ACCESS_32  4

typedef struct
{
    CM_REG DWT_COMP;
    CM_REG DWT_MASK;
    CM_REG DWT_FUNCTION;
    CM_REG RESERVED;
}WATCHPOINT;

typedef struct
{
    CM_REG     DWT_CTRL;   /**/
    CM_REG     DWT_CYCCNT;
    CM_REG     DWT_CPICNT;
    CM_REG     DWT_EXCCNT; 
    CM_REG     DWT_SLEEPCNT;
    CM_REG     DWT_LSUCNT;
    CM_REG     DWT_FOLDCNT;
    CM_REG     DWT_PCSR;
    WATCHPOINT  DWT_WATCHPOINT;
}DWT_REG,*pDWT_REG;

typedef enum
{
    REG_START       = 0,
    REG_R0          = 0,    /* Register R0 */
    REG_R1          = 1,    /* Register R1 */
    REG_R2          = 2,    /* Register R2 */
    REG_R3          = 3,    /* Register R3 */
    REG_R4          = 4,    /* Register R4 */
    REG_R5          = 5,    /* Register R5 */
    REG_R6          = 6,    /* Register R6 */
    REG_R7          = 7,    /* Register R7 */
    REG_R8          = 8,    /* Register R8 */
    REG_R9          = 9,    /* Register R9 */
    REG_R10         = 10,   /* Register R10 */
    REG_R11         = 11,   /* Register R11 */
    REG_R12         = 12,   /* Register R12 */
    REG_CurrSP      = 13,   /* Register Current SP */
    REG_LR          = 14,   /* Register LR */
    REG_DbgRetAdd   = 15,   /* Register Debug return address */
    REG_xPSR        = 16,   /* Register xPSR */
    REG_MSP         = 17,   /* Register MSP */
    REG_PSP         = 18,   /* Register PSP */
    REG_CONTROL     = 20,   /* Register Control */
    REG_LAST        = 20
} Register; 

#define DEFAULT_CSW_VAL         0x23000012
#define CSW_VAL_INC_OFF         0x23000002
/* Address of Debug Exception and Monitor Control Register */
#define DEMCR_ADDR              0xE000EDFC
/* DEMCR: Debug Exception and Monitor Control Register */
#define DEMCR_TRCENA            (0x1 << 24)         /* */
#define DEMCR_MON_REQ           (0x1 << 19)
#define DEMCR_MON_STEP          (0x1 << 18)
#define DEMCR_MON_PEND          (0x1 << 17)
#define DEMCR_MON_EN            (0x1 << 16)
#define DEMCR_VC_HARDERR        (0x1 << 10)
#define DEMCR_VC_INTERR         (0x1 << 9)
#define DEMCR_VC_BUSERR         (0x1 << 8)
#define DEMCR_VC_STATERR        (0x1 << 7)
#define DEMCR_VC_CHKERR         (0x1 << 6)
#define DEMCR_VC_NOCPERR        (0x1 << 5)
#define DEMCR_VC_MMERR          (0x1 << 4)
#define DEMCR_VC_CORERESET      (0x1 << 0)

/* Configurable Fault Status Registers */
#define NUM_CONFIG_FAULT_STAT_REG   0x3

/* Bus fault status register format */
#define BFSR_IMPRECISERR        (0x1 << 2)
#define BFSR_PRECISERR          (0x1 << 1)
#define BFSR_IBUSERR            (0x1 << 0)

#define DATA_BUS_ABORT          1//(BFSR_IMPRECISERR | BFSR_PRECISERR | BFSR_IBUSERR)

/****************************************************************************
               BASE ADDRESS DEFINITIONS FOR CORTEX M
******************************************************************************/
#define CORTEXM_CDBGR_BASE               ((pCM_CDBGR)0xE000EDF0)
#define CORTEXM_FPB_BASE                 ((pFPB_REG)0xE0002000)
#define CORTEXM_DWT_BASE                 ((pDWT_REG)0xE0001000)
#define CORTEXM_CONFI_FAULT_STAT_REG     0xE000ED28
#define DEFAULT_CORTEXM_ROM_TABLE_BASE   0xE00FF000

unsigned long CortexM_EnterDebugState(unsigned long ulAddr);
unsigned long CortexM_LeaveDebugState(unsigned long ulAddr);
RESULT InitializeDAP(UWORD ulCore);
unsigned long ARML_CortexMReadWriteRegisters(unsigned long *pulData,
                                                unsigned char ucAccessType);
unsigned long ARML_CortexMSingleStep(void);
unsigned long ARML_CortexMStatusProc(unsigned char* pucCoreExecuting ,
                                      unsigned char* pucThumbMode,unsigned char* pucWatchPoint);
RESULT ARML_CortexMReadMemory(
    UWORD*  pulMemoryVal,
    UWORD   ulAddress,
    UWORD   ulNumMemLoc,
    UWORD   ulAccessType);

RESULT ARML_CortexMWriteMemory(
    UWORD*  pulDataToMem,
    UWORD   ulAddress,
    UWORD   ulNumMemLoc,
    UWORD   ulAccessSize);

RESULT ARML_CortexMCheckDataAbort(UBYTE* pucReadData);

RESULT ARML_CortexMWatchpoint(
    UBYTE  ulNumWatchPointUnits,
    UWORD* pulWatchPointData);

RESULT ARML_CortexMHWBreakpoint(
    UBYTE ucNumComparators,
    UWORD *pulComparatorData);

#endif // #define _CORTEX_H_
