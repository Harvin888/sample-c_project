/******************************************************************************
       Module: armcrit.c
     Engineer: Shameerudheen P.T.
  Description: ARM memory critical functions for Opella-XD firmware
  Date           Initials    Description
  02-Apr-2008    SPT          initial
******************************************************************************/


#include "common/comms.h"
#include "common/common.h"
#include "common/ml69q6203.h"
#include "armlayer.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "export/api_err.h"


#define ARM9ScanChain1PrepareData(var,dat) (var)[0] = (dat); (var)[1] = 0x00002C38;\
                                           (var)[2] = 0x0;

extern TyJtagScanConfig tyJtagScanConfig;  
extern PTyRxData ptyRxData;


extern unsigned long pulARM9_LDMIA_R0_R0toR14[3];
extern unsigned long pulARM9_ARM_NOP_NORMAL[3];
extern unsigned long pulARM9_STMIA_R0_R1toR14[3];
extern unsigned long pulARM9_ARM_NOP_CLOCK[3];
extern unsigned long ulARM_JTAG_RESTART;
extern unsigned long ulARM_JTAG_SCAN_N;
extern unsigned long ulARM_JTAG_INTEST;
extern unsigned long pulREAD_DBG_STATUS[2];
extern unsigned long pulALL_ZEROS[2];
extern unsigned long pulARM9_LDMIA_R0_R0R1[3];
extern unsigned long pulARM9_LDMIA_R0_R0toR2[3];
extern unsigned long pulARM9_STMIA_R0_R1[3];
extern unsigned long pulARM9_STMIA_R0_R1toR2[3];
extern unsigned long pulARM9_LDMIA_R0_R0[3];
extern unsigned long pulARM9_LDMIA_R0_R1toR14[3];
extern unsigned long pulARM9_LDMIA_R0_R1[3];

extern unsigned long pulARM7_ARM_NOP_CLOCK[2];
extern unsigned long pulARM7_LDMIA_R0_R0R1[2];
extern unsigned long pulARM7_ARM_NOP_NORMAL[2];
extern unsigned long pulARM7_STMIA_R0_R1[2];
extern unsigned long pulARM7_STMIA_R0_R1toR14[2];
extern unsigned long pulARM7_LDMIA_R0_R0toR2[2];
extern unsigned long pulARM7_STMIA_R0_R1toR2[2];
extern unsigned long pulARM7_LDMIA_R0_R0toR14[2];
extern unsigned long pulARM7_LDMIA_R0_R0[2];
extern unsigned long pulARM7_LDMIA_R0_R1[2];
extern unsigned long pulARM7_LDMIA_R0_R1toR14[2];
extern unsigned long pulARM9_LDMIA_R0_R0toR10[3];
extern unsigned long pulARM9_STMIA_R0_R1toR10[3];
extern unsigned long ulARM9_ARM_NOP_NORMAL;
extern unsigned char ucgUseSafeNonVectorAddress;
extern unsigned long pulARM7_LDMIA_R0_PC[2];
extern unsigned long ulARM7_SAFE_NON_VEC_ADDRESS;
extern unsigned long ulARM9_SET_SAFE_NON_VEC;
extern unsigned long pulARM9_LDMIA_R0_PC[2];

/****************************************************************************
     Function: ARML_ARM9WriteMultipleBlock_ReallyFast
     Engineer: Shameerudheen P T
        Input: unsigned long *pulAddress - Start address
               unsigned long ulBlockCount - number of blocks to be written into memory 
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to a (ulBlockCount X block of 14) consecutive memory location very fast.
   Date        Initials    Description
04-Mar-2008      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM9WriteMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long ulBlockCount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long pulTDIVal[3 * 16];
   register unsigned long *pulTempTDIVal;
   unsigned short usReg;
   register unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulTmsTemp;
   register unsigned long ulBlocksInPkt;

   unsigned long ulBlockCountTemp;
   register unsigned long ulAddressTemp;
   static unsigned char ucWordCount = 0;
   unsigned char ucBalWords = 0;
   unsigned char uci;

   ulAddressTemp = *pulAddress;
   ulBlockCountTemp = ulBlockCount;

   DLOG2(("ARML_ARM9WriteMultipleBlock_ReallyFast"));
  
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);


   while (ulBlockCountTemp)
      {
      while (!ptyRxData->bFastDataRecvFlag) ; // wait for data from USB (using fast transfers)
      if ((ptyRxData->ulFastDataSize & 0x03))
         {
         ulResult = ERR_ARM_DATA_NOT_WORD_ALIGNED;
         break;
         }
      ulBlocksInPkt = ptyRxData->ulFastDataSize / (14 * 4);
      if (ulBlockCountTemp < ulBlocksInPkt)
         ulBlocksInPkt = ulBlockCountTemp;      // do not use more blocks than required
      ulBlockCountTemp -= ulBlocksInPkt;

      while (ulBlocksInPkt--)
         {
         pulTempTDIVal = pulTDIVal;

         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0toR14[1]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0toR14[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
         put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);         
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddressTemp); 
         put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(2)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(4)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(4)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(4)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(5)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(6)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(6)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(6)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(7)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(7)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(7)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(8), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(8)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(8)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(8)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(9), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(9)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(9)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(9)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(10), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(10), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(10)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(10)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(10)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(11), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(11), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(11)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(11)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(11)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(12), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(12), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(12)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(12)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(12)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(12));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(13), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(13), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(13)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(13)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(13)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(13));
         pulTempTDIVal += 3;

         put_wvalue(JTAG_SPARAM_TMS(14), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(14), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal, get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(14)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(14)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(14)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(14));
         pulTempTDIVal += 3;

         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
         put_wvalue(JTAG_SPARAM_TMS(15), ulTmsTemp); 
         put_wvalue(JTAG_SPARAM_CNT(15), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal, get_wvalue(USBEPC_FIFO)); 
         put_wvalue(JTAG_TDI_BUF(15)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(15)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(15)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(15));
         pulTempTDIVal += 3;

         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         //set safe non vector address
         DLOG(("ARML_ARM9WriteBlock_ReallyFast"));
         //DLOG(("value 0X%08x",ucgUseSafeNonVectorAddress));
         
         if (ucgUseSafeNonVectorAddress)
         {
            pulTempTDIVal = pulTDIVal;
            //DLOG(("value "));
            
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            
            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
            
            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
            
            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
            put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
            
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
            put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
            put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
            put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
            put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
            pulTempTDIVal += 3;
            
            put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
            
            put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
            
            put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
            
            put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         }
         //end set safe non vector address

         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
     
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_STMIA_R0_R1toR14[1]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_STMIA_R0_R1toR14[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
         put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         
         ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
         put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         
         while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
         //filling first buffer inadvace to enhance speed
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);
         
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         
         //the set of block for do loop
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);

         usCount = 0;
         do
            {
            
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;

            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

            ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
            usCount++;
            if (usCount >= MAX_WAIT_CYCLES)
               {
               return ERR_ARM_MAXIMUM_COUNT_REACHED;
               }
            }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 

         //end ARML_ARM9WaitForSystemSpeed

         //select scan chain
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

         ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

         ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
         put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         //end select scan chain
         ulAddressTemp += (14 * 4);
         }
      //writing the remaining data to memory
      ucBalWords = (ptyRxData->ulFastDataSize % (14 * 4))/4;
      if (ucBalWords)
         {
         if (ucBalWords == 2)
            {
            pulTempTDIVal = pulTDIVal;    
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            
            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0toR2[1]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0toR2[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            //put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
            put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp); 
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddressTemp); 
            put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
            put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
            pulTempTDIVal += 3;

            // Setup the data we need to write...

            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
            put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
            put_wvalue(JTAG_TDI_BUF(2)+8, pulTempTDIVal[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
            pulTempTDIVal += 3;

            //put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
            put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp); 
            put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
            put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
            put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
            pulTempTDIVal += 3;

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

            //set safe non vector address

            if (ucgUseSafeNonVectorAddress)
            {
               pulTempTDIVal = pulTDIVal;
            
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
               
               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
               put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
               
               put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
               put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
               
               put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
               put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
               
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
               put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
               put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
               put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
               put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
               put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
               pulTempTDIVal += 3;
               
               put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
               put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
               
               put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
               put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
               
               put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
               put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
               
               put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
               put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));

               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

            }
            //end set safe non vector address

            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_STMIA_R0_R1toR2[1]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_STMIA_R0_R1toR2[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
            put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));  

            //replacing ARML_ARM9WaitForSystemSpeed
            //select scan chain

            ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

            ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

            ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
            put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

            while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
            //filling first buffer inadvace to enhance speed
            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
            //end select scan chain
            

            // The following two set of assignment is for do loop 
            
            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
            usCount = 0;
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            do
               {

               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

               ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
               usCount++;
               if (usCount >= MAX_WAIT_CYCLES)
                  {
                  return ERR_ARM_MAXIMUM_COUNT_REACHED;
                  }

               }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 
            //end ARML_ARM9WaitForSystemSpeed


            //select scan chain
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

            ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
            put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
            //end select scan chain

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
            ulAddressTemp += 8;
            }
         else
            {

            for (uci = 0; uci < ucBalWords; uci++)
               {

               pulTempTDIVal = pulTDIVal;    
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
               
               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0R1[1]);
               put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0R1[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
              
               //put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //For two NOPS after instruction
               put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp); 
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddressTemp); 
               put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
               put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
               pulTempTDIVal += 3;

               ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
               put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp); 
               put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               ARM9ScanChain1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO)); 
               put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
               put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
               put_wvalue(JTAG_TDI_BUF(2)+8, pulTempTDIVal[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
               pulTempTDIVal += 3;
               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

               //set safe non vector address

               if (ucgUseSafeNonVectorAddress)
               {
                  pulTempTDIVal = pulTDIVal;
            
                  usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
                  put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
                  
                  put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
                  put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
                  
                  put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
                  put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
                  
                  put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
                  put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
                  
                  ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
                  put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
                  put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
                  put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
                  put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
                  put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
                  pulTempTDIVal += 3;
                  
                  put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
                  put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
                  
                  put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
                  put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
                  
                  put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
                  put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
                  
                  put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
                  put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));

                  while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
                  if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                     return ERR_ADAPTIVE_CLOCK_TIMEOUT;
                  put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

               }
               //end set safe non vector address

               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_STMIA_R0_R1[1]);
               put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_STMIA_R0_R1[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

               put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

               put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
               put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2)); 

               //replacing ARML_ARM9WaitForSystemSpeed
               //select scan chain
               
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

               ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

               ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
               put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

                while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
               //filling first buffer inadvace to enhance speed
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
               //end select scan chain

               //The followinf two sets of code for do loop
               
               put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);

               usCount = 0;
               do
                  {
                  
                  put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
              
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

                  while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
                  if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                     return ERR_ADAPTIVE_CLOCK_TIMEOUT;
                  put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

                  ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
                  usCount++;
                  if (usCount >= MAX_WAIT_CYCLES)
                     {
                     return ERR_ARM_MAXIMUM_COUNT_REACHED;
                     }

                  }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 
               //end ARML_ARM9WaitForSystemSpeed

               //select scan chain
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

               ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

               ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
               put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
               //end select scan chain

               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
               ulAddressTemp += 4;
               }
            }
         ucWordCount += ucBalWords;
         if (ucWordCount >= 14)
            {
            ulBlockCountTemp--;
            ucWordCount -= 14;
            }
         }
      // now we should wait to finish last scan but we can afford to do other stuff in CPU (reenable USB) and check status later
      ptyRxData->bFastDataRecvFlag = 0;                                          // clear flag (ready for next packet)
      put_hvalue(USBEPC_STT, 0x0002);                                            // clear B_RxPktReady in EPC Stt register
      put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);                   // enable EPC event interrupt
      }
   return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9WriteMultipleWord_ReallyFast
     Engineer: Jayarani
        Input: unsigned long *pulAddress   - starting Address of memory location, 
               unsigned long *pulWriteData - starting address of values to be written in to consecutive memory locations
               unsigned long ulWordCount   - number of words to be written
       Output: unsigned long - error code ERR_xxx 
  Description: Writes a multiple words to the target memory. set scan chain to 1 (Debug) before calling this function, very fast
   Date        Initials    Description
04-Mar-2008    JY          Initial
05-Mar-2008    SPT         More Optimisation
****************************************************************************/
unsigned long ARML_ARM9WriteMultipleWord_ReallyFast(unsigned long *pulAddress, unsigned long *pulWriteData, unsigned long ulWordCount)
{
   unsigned long ulResult = ERR_NO_ERROR;

   unsigned long pulTDIVal[3 * 16];
   unsigned long *pulTempTDIVal;
   unsigned  short usReg;
   unsigned long ulTmsTemp;
   unsigned short usCount;
   unsigned long ulTDOVal;
   register unsigned long ulI = 0;
   register unsigned long ulAddressTemp;
   ulAddressTemp = *pulAddress;

   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

   DLOG2(("ARML_ARM9WriteMultipleWord_ReallyFast"));
   if(ulWordCount == 0xA)
      {         
      pulTempTDIVal = pulTDIVal;

      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0toR10[1]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0toR10[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //For two NOPS after instruction
      put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp); 
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddressTemp); 
      put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      pulTempTDIVal += 3;

      // Setup the data we need to write...

      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(2)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(4)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(4)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(4)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(5)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(6)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(6)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(6)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(7)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(7)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(7)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(8), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(8)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(8)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(8)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(9), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(9)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(9)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(9)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
      pulTempTDIVal += 3;

      put_wvalue(JTAG_SPARAM_TMS(10), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(10), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(10)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(10)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(10)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));
      pulTempTDIVal += 3;

      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03); //For one NOPS after this pay load
      put_wvalue(JTAG_SPARAM_TMS(11), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(11), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,pulWriteData[ulI++]); 
      put_wvalue(JTAG_TDI_BUF(11)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(11)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(11)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));
      pulTempTDIVal += 3;  

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      //set safe non vector address

      if (ucgUseSafeNonVectorAddress)
      {
         pulTempTDIVal = pulTDIVal;
         
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
         put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
         put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
         put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         pulTempTDIVal += 3;
         
         put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
         put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         
         put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
         
         put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
         
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      }
      //end set safe non vector address
      pulTempTDIVal = pulTDIVal;
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_STMIA_R0_R1toR10[1]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_STMIA_R0_R1toR10[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
      put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

      
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
      //filling first buffer inadvace to enhance speed
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);
      
      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;

      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      //the set of block for do loop
      
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);

      usCount = 0;
      do
         {
         
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

         
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;

         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
         usCount++;
         if (usCount >= MAX_WAIT_CYCLES)
            {
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
         }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 

      //end ARML_ARM9WaitForSystemSpeed
      }
   else
      {
      for (ulI =0; ulI < ulWordCount; ulI++)
         {
         pulTempTDIVal = pulTDIVal;    
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0R1[1]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0R1[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03);
         put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal, ulAddressTemp); 
         put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         pulTempTDIVal += 3;
      
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
         put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,*pulWriteData); 
         put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(2)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         pulTempTDIVal += 3;

         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         //set safe non vector address

         if (ucgUseSafeNonVectorAddress)
         {
            pulTempTDIVal = pulTDIVal;
         
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            
            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
            
            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
            
            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
            put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
            
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
            put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
            put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
            put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
            put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
            pulTempTDIVal += 3;
            
            put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
            
            put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
            
            put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
            
            put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
            put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
            
            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         }

         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_STMIA_R0_R1[1]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_STMIA_R0_R1[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
         put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2)); 
      
         //replacing ARML_ARM9WaitForSystemSpeed
         //select scan chain
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      
         ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
         put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

         while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
         //filling first buffer inadvace to enhance speed
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);
      
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         //end select scan chain
      
         //The following two sets of code for do loop
        
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
      
         usCount = 0;
         do
            {
            
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
        
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      
            ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
            usCount++;
            if (usCount >= MAX_WAIT_CYCLES)
               {
               return ERR_ARM_MAXIMUM_COUNT_REACHED;
               }
      
            }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 
         //end ARML_ARM9WaitForSystemSpeed
      
         //select scan chain
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
         ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
         put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         //end select scan chain
      
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      
         ulAddressTemp += 4;
         pulWriteData++;
         }
      }
   return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9ReadMultipleBlock_ReallyFast
     Engineer: Jayarani
        Input: unsigned long *pulAddress - Address of starting memory location, 
               unsigned long *pulReadData - Value to be read from that mem.
               unsigned long ulBlockcount - Number of blocks to be read
       Output: unsigned long - error code ERR_xxx 
  Description: Read multiple blocks from the target memory. set scan chain to 1 (Debug) before calling this function, very fast
   Date        Initials    Description
07-Apr-2008      JY        Initial
08-Apr-2008      SPT       More optimisation
****************************************************************************/
unsigned long ARML_ARM9ReadMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long *pulReadData,unsigned long ulBlockcount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned short usReg;
   unsigned long pulTDIVal[3 * 16];
   unsigned long *pulTempTDIVal;

   unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulTmsTemp;
   unsigned long ulI;
   unsigned long ulAddressTemp;

   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

   DLOG2(("ARML_ARM9ReadMultipleBlock_ReallyFast"));

   ulAddressTemp = *pulAddress;

   for (ulI =0; ulI < ulBlockcount; ulI++)
      {
      pulTempTDIVal = pulTDIVal;
   
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0[1]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03);//remove 2 NOPs for optimisation
      put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddressTemp); 
      put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
      put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      pulTempTDIVal += 3;

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      //set safe non vector address

      if (ucgUseSafeNonVectorAddress)
      {
         pulTempTDIVal = pulTDIVal;
         
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
         put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
         put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
         put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         pulTempTDIVal += 3;
         
         put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
         put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         
         put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
         
         put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
         
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      }
         //end set safe non vector address

      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R1toR14[1]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R1toR14[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
      put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
   
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
   
      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
   
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
      //filling first buffer inadvace to enhance speed
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         //end select scan chain

      
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
      usCount = 0;
      do
         {
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));   
         
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;  

         ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
         usCount++;
         if (usCount >= MAX_WAIT_CYCLES)
            {
             return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 
      
      //end ARML_ARM9WaitForSystemSpeed
   
      //select scan chain
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

      put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(3)+0, pulARM9_STMIA_R0_R1toR14[1]);
      put_wvalue(JTAG_TDI_BUF(3)+4, pulARM9_STMIA_R0_R1toR14[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
      put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[1]);
      put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

      put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(5)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

      put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(6)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
      
      put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(7)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
      
      put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(8), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(8)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
      
      put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(9), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(9)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
      
      put_wvalue(JTAG_SPARAM_TMS(10), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(10), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(10)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));
      
      put_wvalue(JTAG_SPARAM_TMS(11), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(11), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(11)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));
      
      put_wvalue(JTAG_SPARAM_TMS(12), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(12), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(12)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(12));
      
      put_wvalue(JTAG_SPARAM_TMS(13), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(13), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(13)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(13));
      
      put_wvalue(JTAG_SPARAM_TMS(14), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(14), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(14)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(14));
      
      put_wvalue(JTAG_SPARAM_TMS(15), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(15), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(15)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(15));

      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
      //filling first buffer inadvace to enhance speed
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, ulARM9_ARM_NOP_NORMAL);

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
          return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg);//disable scan

      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(5));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(6));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(7));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(8));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(9));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(10));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(11));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(12));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(13));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(14));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(15));

      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
          return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg);//disable scan

      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(0));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(1));
      *(pulReadData++) = get_wvalue(JTAG_TDO_BUF(2));
      ulAddressTemp += (14 * 4);
      }
   return ulResult;
}

/****************************************************************************
     Function: ARML_ARM9ReadMultipleWord_ReallyFast
     Engineer: Jayarani
        Input: unsigned long ulAddress - Address of memory location, 
               unsigned long *pulResultData - Value to be read into that mem.
               unsigned long ulWordcount - Number of to be read
       Output: unsigned long - error code ERR_xxx 
  Description: Read multiple words from the target memory. set scan chain to 1 (Debug) before calling this function, very fast
   Date        Initials    Description
07-Apr-2008    JY          Initial
07-Apr-2008    SPT         More Optimisation
****************************************************************************/
unsigned long ARML_ARM9ReadMultipleWord_ReallyFast(unsigned long ulAddress, unsigned long *pulResultData,unsigned long ulWordcount)
{
   unsigned long ulResult = ERR_NO_ERROR;

   unsigned long pulTDIVal[3 * 16];
   unsigned long *pulTempTDIVal;
   unsigned  short usReg;
   unsigned long ulTmsTemp;
   unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulI;

   DLOG2(("ARML_ARM9ReadMultipleWord_ReallyFast"));

   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

   for (ulI =0; ulI < ulWordcount; ulI++)
   {
       pulTempTDIVal = pulTDIVal;    
       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

       put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R0[1]);
       put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R0[2]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       ARM9ScanChain1PrepareData(pulTempTDIVal,ulAddress); 
       put_wvalue(JTAG_TDI_BUF(1)+0, pulTempTDIVal[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulTempTDIVal[1]);
       put_wvalue(JTAG_TDI_BUF(1)+8, pulTempTDIVal[2]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
       pulTempTDIVal += 3;

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

       //set safe non vector address

      if (ucgUseSafeNonVectorAddress)
      {
         pulTempTDIVal = pulTDIVal;
      
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(0)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(1)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(2)+0, pulARM9_LDMIA_R0_PC[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulARM9_LDMIA_R0_PC[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x03); //for two nops after instruction
         put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);         
         put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARM9ScanChain1PrepareData(pulTempTDIVal,ulARM9_SET_SAFE_NON_VEC);
         put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
         put_wvalue(JTAG_TDI_BUF(3)+8, pulTempTDIVal[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         pulTempTDIVal += 3;
         
         put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(4)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
         put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(5)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(5)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(5)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         
         put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(6), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(6)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(6)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(6)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
         
         put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(7), (ARM9_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(7)+0, pulARM9_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(7)+4, pulARM9_ARM_NOP_NORMAL[1]);
         put_wvalue(JTAG_TDI_BUF(7)+8, pulARM9_ARM_NOP_NORMAL[2]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
         
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      }
      //end set safe non vector address
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM9_LDMIA_R0_R1[1]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM9_LDMIA_R0_R1[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulARM9_ARM_NOP_CLOCK[1]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulARM9_ARM_NOP_CLOCK[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
      put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_RESTART);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2)); 
      
      //replacing ARML_ARM9WaitForSystemSpeed
      //select scan chain
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      
      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(4)+0, ARM9_SCAN_CHAIN_ICE);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(5), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(5)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
      
      //filling first buffer inadvace to enhance speed
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM9_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);
      
      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      
      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);

      usCount = 0;
      do
      {
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));   
         
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;  
         
         ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
         usCount++;
         if (usCount >= MAX_WAIT_CYCLES)
         {
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
         }
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 
      
      //end ARML_ARM9WaitForSystemSpeed
      //select scan chain
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM9_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, ARM9_SCAN_CHAIN_DEBUG);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
      
      put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(3), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(3)+0, pulARM9_STMIA_R0_R1[1]);
      put_wvalue(JTAG_TDI_BUF(3)+4, pulARM9_STMIA_R0_R1[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
      put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(4), (ARM9_SCAN_CHAIN_1_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(4)+0, pulARM9_ARM_NOP_NORMAL[1]);
      put_wvalue(JTAG_TDI_BUF(4)+4, pulARM9_ARM_NOP_NORMAL[2]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      
      put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(5), (ARM9_SCAN_CHAIN_1_DATA_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(5)+0, ulARM9_ARM_NOP_NORMAL);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
       
      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg);//disable scan
   
      *(pulResultData++) = get_wvalue(JTAG_TDO_BUF(5));

      ulAddress+=4;
   }
   return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7SC1PrepareData
    Engineer: Shameerudheen P T
       Input: unsigned long *pulVar - pointer to prepared data, 
              unsigned long ulData - data to be prepared
      Output: None
 Description: Data is preparing for Scan chain 1 of ARM7.
   Date        Initials    Description
04-Mar-2008     SPT       Initial
****************************************************************************/
static void ARML_ARM7SC1PrepareData(unsigned long *pulVar, unsigned long ulData)
{
   unsigned long ulReverse = 0;
   DLOG2(("ARML_ARM7SC1PrepareData"));
   ulReverse = ulData;
   //reverse word
   ulReverse = ((ulReverse >> 1) & 0x55555555) | ((ulReverse << 1) & 0xaaaaaaaa);
   ulReverse = ((ulReverse >> 2) & 0x33333333) | ((ulReverse << 2) & 0xcccccccc);
   ulReverse = ((ulReverse >> 4) & 0x0f0f0f0f) | ((ulReverse << 4) & 0xf0f0f0f0);
   ulReverse = ((ulReverse >> 8) & 0x00ff00ff) | ((ulReverse << 8) & 0xff00ff00);
   ulReverse = ((ulReverse >> 16) & 0x0000ffff) | ((ulReverse << 16) & 0xffff0000);

   pulVar[0] = ulReverse << 1;
   pulVar[1] = ulReverse >> 31;
}

/****************************************************************************
    Function: ARML_ARM7SC1FilterData
    Engineer: Shameerudheen P T
       Input: unsigned long pulData - data to be filtered from
      Output: None
 Description: Data from is Scan chain 1 of ARM7 is filtered to get actual data value.
   Date        Initials    Description
04-Mar-2008     SPT       Initial
****************************************************************************/
static unsigned long ARML_ARM7SC1FilterData(unsigned long *pulData)
{
   unsigned long ulReverse;

   DLOG2(("ARML_ARM7SC1FilterData"));

   ulReverse = pulData[0] >> 1;
   ulReverse = (ulReverse | ((pulData[1] & 0x1) << 31));
   //reverse word
   ulReverse = ((ulReverse >> 1) & 0x55555555) | ((ulReverse << 1) & 0xaaaaaaaa);
   ulReverse = ((ulReverse >> 2) & 0x33333333) | ((ulReverse << 2) & 0xcccccccc);
   ulReverse = ((ulReverse >> 4) & 0x0f0f0f0f) | ((ulReverse << 4) & 0xf0f0f0f0);
   ulReverse = ((ulReverse >> 8) & 0x00ff00ff) | ((ulReverse << 8) & 0xff00ff00);
   ulReverse = ((ulReverse >> 16) & 0x0000ffff) | ((ulReverse << 16) & 0xffff0000);
   return ulReverse;
}

/****************************************************************************
     Function: ARML_ARM7WriteMultipleBlock_ReallyFast
     Engineer: Shameerudheen P T
        Input: unsigned long *pulAddress - Address location,
               unsigned long ulBlockCount - number of blocks to be written into memory 
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to a (ulBlockCount X block of 14) consecutive memory location very fast.
   Date        Initials    Description
04-Mar-2008      SPT       Initial
****************************************************************************/
unsigned long ARML_ARM7WriteMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long ulBlockCount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long pulTDIVal[2 * 16];
   register unsigned long *pulTempTDIVal;
   unsigned short usReg;
   register unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulTmsTemp;
   register unsigned long ulBlocksInPkt;
  
   unsigned long ulBlockCountTemp;
   register unsigned long ulAddressTemp;
   static unsigned char ucWordCount = 0;
   unsigned char ucBalWords = 0;
   unsigned char uci;


   ulAddressTemp = *pulAddress;
   ulBlockCountTemp = ulBlockCount;

   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

   DLOG2(("ARML_ARM7WriteMultipleBlock_ReallyFast"));
   while (ulBlockCountTemp)
      {
      while (!ptyRxData->bFastDataRecvFlag) ; // wait for data from USB (using fast transfers)
      if ((ptyRxData->ulFastDataSize & 0x03))
         {
         ulResult = ERR_ARM_DATA_NOT_WORD_ALIGNED;
         break;
         }

      ulBlocksInPkt = ptyRxData->ulFastDataSize / (14 * 4);
      if (ulBlockCountTemp < ulBlocksInPkt)
         ulBlocksInPkt = ulBlockCountTemp; // do not use more blocks than required
      ulBlockCountTemp -= ulBlocksInPkt;
      while (ulBlocksInPkt--)
         {
         pulTempTDIVal = pulTDIVal;
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_LDMIA_R0_R0toR14[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_LDMIA_R0_R0toR14[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));         

         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
         put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000); 
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddressTemp);
         put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(4)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(4)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(6)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(6)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(7)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(7)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(8)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(8)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(9)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(9)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(10), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(10), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(10)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(10)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(11), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(11), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(11)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(11)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(12), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(12), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(12)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(12)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(12));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(13), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(13), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(13)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(13)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(13));
         pulTempTDIVal += 2;

         put_wvalue(JTAG_SPARAM_TMS(14), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(14), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(14)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(14)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(14));

         put_wvalue(JTAG_SPARAM_TMS(15), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(15), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(15)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(15)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(15));
         pulTempTDIVal += 2;

         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         pulTempTDIVal = pulTDIVal;
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
         put_wvalue(JTAG_TDI_BUF(0)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         pulTempTDIVal += 2;


         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan


         //set safe non vector address
         DLOG(("SNVA2 : 0X%08x",ucgUseSafeNonVectorAddress));

         if (ucgUseSafeNonVectorAddress)
         {
            pulTempTDIVal = pulTDIVal;
            DLOG(("SNVA3 : 0X%08x",ucgUseSafeNonVectorAddress));

            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_PC[0]);
            put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_PC[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

            put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

            put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

            put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARML_ARM7SC1PrepareData(pulTempTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
            put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
            pulTempTDIVal += 2;

            put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

            put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));

            put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));

            put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         }
         //end set safe non vector address

         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

         pulTempTDIVal = pulTDIVal;

         ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
         put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
                 
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_CLOCK[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_CLOCK[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_STMIA_R0_R1toR14[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_STMIA_R0_R1toR14[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

         put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForIR);
         put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_RESTART);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(4), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(4)+0, ulARM_JTAG_SCAN_N);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      
         ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(5)+0, ARM7_SCAN_CHAIN_ICE);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
      
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
         put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(6), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(6)+0, ulARM_JTAG_INTEST);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

		 //filling first buffer inadvace to enhance speed
         while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         //end select scan chain
         
         //The followinf two sets of code for do loop 
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
         usCount = 0;
         do
            {
            
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
        
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      
            ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
            usCount++;
            if (usCount >= MAX_WAIT_CYCLES)
               {
               return ERR_ARM_MAXIMUM_COUNT_REACHED;
               }
      
            }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE);
         //end ARML_ARM7WaitForSystemSpeed
               
         //select scan chain
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
         ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
         put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, ARM7_SCAN_CHAIN_DEBUG);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
         ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
         put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
         put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
         put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         //end select scan chain
              
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

         ulAddressTemp += (14 * 4);
         }

      //writing the remaining data to memory
      ucBalWords = (ptyRxData->ulFastDataSize % (14 * 4))/4;
      if (ucBalWords)
         {
         if (ucBalWords == 2)
            {
            pulTempTDIVal = pulTDIVal;    
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            
            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_LDMIA_R0_R0toR2[0]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_LDMIA_R0_R0toR2[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
            put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddressTemp);
            put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
            pulTempTDIVal += 2;

            put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
            put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
            pulTempTDIVal += 2;

            put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
            put_wvalue(JTAG_TDI_BUF(4)+0, pulTempTDIVal[0]);
            put_wvalue(JTAG_TDI_BUF(4)+4, pulTempTDIVal[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
            pulTempTDIVal += 2;

            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan


            //set safe non vector address

            if (ucgUseSafeNonVectorAddress)
            {
               pulTempTDIVal = pulTDIVal;

               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      
               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
               put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
               put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_PC[0]);
               put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_PC[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
      
               put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      
               put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      
               put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               ARML_ARM7SC1PrepareData(pulTempTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
               put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
               put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
               pulTempTDIVal += 2;
      
               put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
      
               put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
      
               put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
      
               put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
      
               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
            }
            //end set safe non vector address

            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

            //pulTempTDIVal = pulTDIVal;

            ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
            put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
            put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_CLOCK[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_CLOCK[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

            put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_STMIA_R0_R1toR2[0]);
            put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_STMIA_R0_R1toR2[1]);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

            put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForIR);
            put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_RESTART);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

            //replacing ARML_ARM7WaitForSystemSpeed
            //select scan chain
      
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(4), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(4)+0, ulARM_JTAG_SCAN_N);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
            ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(5)+0, ARM7_SCAN_CHAIN_ICE);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
            put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(6), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(6)+0, ulARM_JTAG_INTEST);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

            while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;//filling first buffer in advance
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);
         
            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
            //end select scan chain
         
            //The followinf two sets of code for do loop

            put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
            put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);

            usCount = 0;
            do
               {
               
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
           
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
         
               ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
               usCount++;
               if (usCount >= MAX_WAIT_CYCLES)
                  {
                  return ERR_ARM_MAXIMUM_COUNT_REACHED;
                  }
         
               }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE);
            //end ARML_ARM7WaitForSystemSpeed
         
            //select scan chain
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
            ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
            put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue(JTAG_TDI_BUF(1)+0, ARM7_SCAN_CHAIN_DEBUG);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
            ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
            put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
            put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
            put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
            //end select scan chain
         
            while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
            ulAddressTemp += 8;
            }
         else
            {
            
            for (uci = 0; uci < ucBalWords; uci++)
               {
               pulTempTDIVal = pulTDIVal;    
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
                                             
               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_LDMIA_R0_R0R1[0]);
               put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_LDMIA_R0_R0R1[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
            
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
               put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 
            
               put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddressTemp);
               put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
               put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
               pulTempTDIVal += 2;
            
               // Setup the data we need to write...
            
               put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               ARML_ARM7SC1PrepareData(pulTempTDIVal,get_wvalue(USBEPC_FIFO));
               put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
               put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
               pulTempTDIVal += 2;

               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

                //set safe non vector address

               if (ucgUseSafeNonVectorAddress)
               {
                  pulTempTDIVal = pulTDIVal;

                  usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
                  put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      
                  put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
      
                  put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
      
                  put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_PC[0]);
                  put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_PC[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
      
                  put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      
                  put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
      
                  put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  ARML_ARM7SC1PrepareData(pulTempTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
                  put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
                  put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
                  pulTempTDIVal += 2;
      
                  put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
      
                  put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
      
                  put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
      
                  put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
                  put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
                  put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
                  put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
      
                  while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
                  if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                     return ERR_ADAPTIVE_CLOCK_TIMEOUT;
                  put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
               }
               //end set safe non vector address

               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

               
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
               put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
               put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
            
               put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_CLOCK[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_CLOCK[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
            
               put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_STMIA_R0_R1[0]);
               put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_STMIA_R0_R1[1]);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2)); 
         
               put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForIR);
               put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_RESTART);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 
            
               //replacing ARML_ARM7WaitForSystemSpeed
               //select scan chain
               
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(4), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(4)+0, ulARM_JTAG_SCAN_N);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
            
               ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(5)+0, ARM7_SCAN_CHAIN_ICE);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
            
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
               put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(6), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(6)+0, ulARM_JTAG_INTEST);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

               //filling first buffer inadvace to enhance speed
               while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

               
               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
               //end select scan chain
            
               //The followinf two sets of code for do loop

               put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
               put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
               usCount = 0;
               do
                  {
                  
                  put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
              
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
            
                  put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
            
                  while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
                  if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                     return ERR_ADAPTIVE_CLOCK_TIMEOUT;
                  put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
            
                  ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
                  usCount++;
                  if (usCount >= MAX_WAIT_CYCLES)
                     {
                     return ERR_ARM_MAXIMUM_COUNT_REACHED;
                     }
            
                  }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE);
               //end ARML_ARM7WaitForSystemSpeed
            
               //select scan chain
               usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
               put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
            
               ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
               put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
               put_wvalue(JTAG_TDI_BUF(1)+0, ARM7_SCAN_CHAIN_DEBUG);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
            
               ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
               put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
               put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
               put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
               put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
               //end select scan chain
            
               while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
               if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
                  return ERR_ADAPTIVE_CLOCK_TIMEOUT;
               put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
               ulAddressTemp += 4;
               }
            }
         ucWordCount += ucBalWords;
         if (ucWordCount >= 14)
            {
            ulBlockCountTemp--;
            ucWordCount -= 14;
            }
         }
      // now we should wait to finish last scan but we can afford to do other stuff in CPU (reenable USB) and check status later
      ptyRxData->bFastDataRecvFlag = 0;                                          // clear flag (ready for next packet)
      put_hvalue(USBEPC_STT, 0x0002);                                            // clear B_RxPktReady in EPC Stt register
      put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);                   // enable EPC event interrupt
      }
   return ulResult;
}

/****************************************************************************
     Function: ARML_ARM7WriteMultipleWord_ReallyFast
     Engineer: Jayarani
        Input: unsigned long *pulAddress   - Address location,
               unsigned long *pulWriteData - Pointer Data to write
               unsigned long ulWordCount   - number of words to be written into memory 
       Output: unsigned long - error code ERR_xxx
  Description: Write multiple words to consecutive memory location very fast.
   Date        Initials    Description
04-Mar-2008    JY          Initial
05-Mar-2008    SPT         More Optimisation
****************************************************************************/
unsigned long ARML_ARM7WriteMultipleWord_ReallyFast(unsigned long ulAddress, unsigned long *pulWriteData, unsigned long ulWordCount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long pulTDIVal[2 * 16];
   unsigned long *pulTempTDIVal;
   unsigned  short usReg;
   unsigned long ulTmsTemp;
   unsigned short usCount;
   unsigned long ulTDOVal;
   unsigned long ulI; 
   unsigned long ulAddressTemp = ulAddress;   

   DLOG2(("ARML_ARM7WriteMultipleWord_ReallyFast"));
   
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   
   for(ulI = 0; ulI < ulWordCount; ulI++)
      {     
      pulTempTDIVal = pulTDIVal;    
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_LDMIA_R0_R0R1[0]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_LDMIA_R0_R0R1[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
      put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddressTemp);
      put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
      pulTempTDIVal += 2;
   
      // Setup the data we need to write...
   
      put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      ARML_ARM7SC1PrepareData(pulTempTDIVal,pulWriteData[ulI]);
      put_wvalue(JTAG_TDI_BUF(3)+0, pulTempTDIVal[0]);
      put_wvalue(JTAG_TDI_BUF(3)+4, pulTempTDIVal[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
      pulTempTDIVal += 2;

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      //set safe non vector address

      if (ucgUseSafeNonVectorAddress)
      {
         pulTempTDIVal = pulTDIVal;
      
         usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
         put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
         put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
         put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_PC[0]);
         put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_PC[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
         put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         
         put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
         put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         ARML_ARM7SC1PrepareData(pulTempTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
         put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
         put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
         pulTempTDIVal += 2;
         
         put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
         
         put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
         
         put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
         
         put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
         put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
         put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
         put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
   
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      }
      //end set safe non vector address

      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

      ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
      put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
      put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_CLOCK[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_CLOCK[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
      put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_STMIA_R0_R1[0]);
      put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_STMIA_R0_R1[1]);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2)); 

   
      put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForIR);
      put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_RESTART);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 
   
      //replacing ARML_ARM7WaitForSystemSpeed
      //select scan chain
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(4), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(4)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
   
      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(5)+0, ARM7_SCAN_CHAIN_ICE);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
   
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(6), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(6)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

      //filling first buffer inadvace to enhance speed
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);
   
      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      //end select scan chain
   
      //The followinf two sets of code for do loop

      put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
      put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
      usCount = 0;
      do
         {
         
         put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
     
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
         while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
         if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
         put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
   
         ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
         usCount++;
         if (usCount >= MAX_WAIT_CYCLES)
            {
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
   
         }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE);
      //end ARML_ARM7WaitForSystemSpeed
   
      //select scan chain
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
      ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
      put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
      put_wvalue(JTAG_TDI_BUF(1)+0, ARM7_SCAN_CHAIN_DEBUG);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
      ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
      put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
      put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
      put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
      //end select scan chain

      while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
         return ERR_ADAPTIVE_CLOCK_TIMEOUT;
      put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

      ulAddressTemp += 0x4;
      }
   return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7ReadMultipleWord_ReallyFast
    Engineer: Jayarani
       Input: unsigned long ulAddress   - Starting address of memmory location
              unsigned long *pulData    - pointer to store the data to be read
              unsigned long ulWordcount - number for words to be read
      Output: unsigned long - error code ERR_xxx
 Description: Reading multiple words of data from the memory very fast.
   Date        Initials    Description
10-Apr-2008    JY          Initial
10-Apr-2008    SPT         more optimisation
****************************************************************************/
unsigned long ARML_ARM7ReadMultipleWord_ReallyFast(unsigned long ulAddress, unsigned long *pulReadData,unsigned long ulWordcount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long pulTDIVal[2 * 16];
   unsigned long *pulTempTDIVal;
   unsigned  short usReg;
   unsigned long ulTmsTemp;
   unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulI;

   unsigned long pulTempVal[2 * 16];
   
   DLOG2(("ARML_ARM7ReadWord"));

   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

   
   for (ulI =0; ulI < ulWordcount; ulI++)
   {
       pulTempTDIVal = pulTDIVal;    
       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

       put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_LDMIA_R0_R0[0]);
       put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_LDMIA_R0_R0[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

       put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddress); 
       put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
       put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
       pulTempTDIVal += 2;

       while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
          return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

        //set safe non vector address

       if (ucgUseSafeNonVectorAddress)
       {
          pulTempTDIVal = pulTDIVal;
         
          usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
          put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
          put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
          put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
          put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_PC[0]);
          put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_PC[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
          put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         
          put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
          put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          ARML_ARM7SC1PrepareData(pulTempTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
          put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
          put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
          pulTempTDIVal += 2;
         
          put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
            
          put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
            
          put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
            
          put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
            
          while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
          if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
             return ERR_ADAPTIVE_CLOCK_TIMEOUT;
          put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

       }
      //end set safe non vector address

       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

       //removed NOP used for optimization
       put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_CLOCK[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_CLOCK[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

       put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_R1[0]);
       put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_R1[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

       put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForIR);
       put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_RESTART);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 

       //replacing ARML_ARM7WaitForSystemSpeed
         //select scan chain
       ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(4), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(4)+0, ulARM_JTAG_SCAN_N);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
   
       ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(5)+0, ARM7_SCAN_CHAIN_ICE);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
   
       ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
       put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(6), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(6)+0, ulARM_JTAG_INTEST);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

	   //filling first buffer inadvace to enhance speed
       while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

       while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
           return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan


       put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);
       usCount = 0;

       do
       {
           put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));//enable scan
           put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
           put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));   
         
           while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
           if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;  

           ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
           usCount++;
           if (usCount >= MAX_WAIT_CYCLES)
           {
               return ERR_ARM_MAXIMUM_COUNT_REACHED;
           }
           put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
       }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 

       //end ARML_ARM7WaitForSystemSpeed
       //select scan chain
       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
       ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
   
       ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, ARM7_SCAN_CHAIN_DEBUG);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
   
       ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
       put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
   
       put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_STMIA_R0_R1[0]);
       put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_STMIA_R0_R1[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

       put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(5)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(5)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

       while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
           return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg);//disable scan

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(5)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(5)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       ulAddress+=4;
   }
   return ulResult;
}

/****************************************************************************
    Function: ARML_ARM7ReadMultipleBlock_ReallyFast
    Engineer: Jayarani
       Input: unsigned long ulAddress - Starting address of memmory location
              unsigned long *pulData  - pointer to store the data to be read
              unsigned long ulBlockcount - number for blocks to be read
      Output: unsigned long - error code ERR_xxx
 Description: Reading multiple block of data from the memory very fast.
   Date        Initials    Description
11-Apr-2008    JY          Initial
12-Apr-2008    SPT         more optimisation
****************************************************************************/
unsigned long ARML_ARM7ReadMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long *pulReadData,unsigned long ulBlockcount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned short usReg;
   unsigned long pulTDIVal[2 * 16];
   unsigned long *pulTempTDIVal;

   unsigned short usCount = 0; 
   unsigned long ulTDOVal;
   unsigned long ulTmsTemp;
   unsigned long ulI;
   unsigned long ulAddressTemp;
   unsigned long pulTempVal[2 * 16];

   ulAddressTemp = *pulAddress;

   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

   DLOG2(("ARML_ARM7ReadMultipleBlock_ReallyFast"));

   for (ulI =0; ulI < ulBlockcount; ulI++)
   {
       
       pulTempTDIVal = pulTDIVal;

       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
       
       put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_LDMIA_R0_R0[0]);
       put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_LDMIA_R0_R0[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

       put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       ARML_ARM7SC1PrepareData(pulTempTDIVal,ulAddressTemp); 
       put_wvalue(JTAG_TDI_BUF(2)+0, pulTempTDIVal[0]);
       put_wvalue(JTAG_TDI_BUF(2)+4, pulTempTDIVal[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
       pulTempTDIVal += 2;

       while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
          return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

       //set safe non vector address

       if (ucgUseSafeNonVectorAddress)
       {
          pulTempTDIVal = pulTDIVal;
         
          usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
          put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
         
          put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
         
          put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));
         
          put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_PC[0]);
          put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_PC[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));
         
          put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));
         
          put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));
         
          put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          ARML_ARM7SC1PrepareData(pulTempTDIVal,ulARM7_SAFE_NON_VEC_ADDRESS);
          put_wvalue(JTAG_TDI_BUF(5)+0, pulTempTDIVal[0]);
          put_wvalue(JTAG_TDI_BUF(5)+4, pulTempTDIVal[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));
          pulTempTDIVal += 2;
         
          put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));
            
          put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));
            
          put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));
            
          put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
          put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
          put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
          put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
          put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));
            
          while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
          if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
             return ERR_ADAPTIVE_CLOCK_TIMEOUT;
          put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

       }
      //end set safe non vector address

       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));


       put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_CLOCK[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_CLOCK[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

       put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_LDMIA_R0_R1toR14[0]);
       put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_LDMIA_R0_R1toR14[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

       put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForIR);
       put_wvalue(JTAG_SPARAM_CNT(3), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(3)+0, ulARM_JTAG_RESTART);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(4), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(4)+0, ulARM_JTAG_SCAN_N);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

       ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(5)+0, ARM7_SCAN_CHAIN_ICE);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
       put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(6), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(6)+0, ulARM_JTAG_INTEST);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

	   //filling first buffer inadvace to enhance speed
       while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_2_INST_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulREAD_DBG_STATUS[1]);

      
       while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
           return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
      //end select scan chain

       put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_2_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulALL_ZEROS[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulALL_ZEROS[1]);

       usCount = 0;

       do
       {
           put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));
           put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));
           put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));   

           while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
           if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
               return ERR_ADAPTIVE_CLOCK_TIMEOUT;  

           ulTDOVal = get_wvalue(JTAG_TDO_BUF(1));
           usCount++;
           if (usCount >= MAX_WAIT_CYCLES)
           {
               return ERR_ARM_MAXIMUM_COUNT_REACHED;
           }
           put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan
       }while ((ulTDOVal & ARM_DEBUG_MODE) != ARM_DEBUG_MODE); 

       //select scan chain
       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
       ulTmsTemp = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x01); //4, 0x03, 5, 0x0D //4, 0x03, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(0), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(0)+0, ulARM_JTAG_SCAN_N);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

       ulTmsTemp =  JTAG_SPARAM_TMS_VAL(5, 0x07, 4, 0x01); //3, 0x01, 4, 0x01 //5, 0x07, 4, 0x01
       put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, ARM7_SCAN_CHAIN_DEBUG);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(6, 0x0F, 4, 0x0D); //6, 0x0F, 4, 0x0D
       put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(2), (tyJtagScanConfig.ulIRLength));
       put_wvalue(JTAG_TDI_BUF(2)+0, ulARM_JTAG_INTEST);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

       put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_STMIA_R0_R1toR14[0]);
       put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_STMIA_R0_R1toR14[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

       ulTmsTemp = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x03);
       put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
       put_wvalue(JTAG_SPARAM_CNT(4), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(4)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(4)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

       put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(5), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(5)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(5)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

       put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(6), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(6)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(6)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

       put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(7), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(7)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(7)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7));

       put_wvalue(JTAG_SPARAM_TMS(8), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(8), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(8)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(8)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(8));

       put_wvalue(JTAG_SPARAM_TMS(9), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(9), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(9)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(9)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(9));

       put_wvalue(JTAG_SPARAM_TMS(10), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(10), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(10)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(10)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(10));

       put_wvalue(JTAG_SPARAM_TMS(11), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(11), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(11)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(11)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(11));

       put_wvalue(JTAG_SPARAM_TMS(12), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(12), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(12)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(12)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(12));

       put_wvalue(JTAG_SPARAM_TMS(13), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(13), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(13)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(13)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(13));

       put_wvalue(JTAG_SPARAM_TMS(14), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(14), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(14)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(14)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(14));

	   //filling first buffer inadvace to enhance speed
       while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0)) ;
       put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(0), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(0)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(0)+4, pulARM7_ARM_NOP_NORMAL[1]);

       while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
           return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg);//disable scan

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(5)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(5)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(6)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(6)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);
       
       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(7)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(7)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(8)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(8)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(9)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(9)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(10)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(10)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(11)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(11)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(12)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(12)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(13)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(13)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(14)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(14)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
       put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

       put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(1), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(1)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(1)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

       put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(2), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(2)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(2)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

       put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(3), (ARM7_SCAN_CHAIN_1_LENGTH & 0x000007FF) | 0x00008000);
       put_wvalue(JTAG_TDI_BUF(3)+0, pulARM7_ARM_NOP_NORMAL[0]);
       put_wvalue(JTAG_TDI_BUF(3)+4, pulARM7_ARM_NOP_NORMAL[1]);
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));

       while (get_hvalue(JTAG_JASR)){};  // waiting to sent all buffers
       if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF) // check for adaptive clock timeout
           return ERR_ADAPTIVE_CLOCK_TIMEOUT;
       put_hvalue(JTAG_JSCTR, usReg);//disable scan


       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(0)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(0)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(1)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(1)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(2)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(2)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       pulTempVal[0] = get_wvalue(JTAG_TDO_BUF(3)+0);
       pulTempVal[1] = get_wvalue(JTAG_TDO_BUF(3)+4);
       *(pulReadData++) = ARML_ARM7SC1FilterData(pulTempVal);

       ulAddressTemp += (14 * 4);
   }
   return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11WriteMultipleBlock_ReallyFast
     Engineer: Amerdas D K
        Input: unsigned long *pulAddress - Start address
               unsigned long ulBlockCount - number of blocks to be written into memory 
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to a (ulBlockCount X block of 14) consecutive memory location very fast.
   Date        Initials    Description
04-Nov-2009      ADK      Initial
****************************************************************************/
unsigned long ARML_ARM11WriteMultipleBlock_ReallyFast(unsigned long *pulAddress, 
                                                      unsigned long  ulBlockcount,
                                                      unsigned long* ulData)
{
    unsigned long ulResult = ERR_NO_ERROR;    
    unsigned short usReg;

    register unsigned long ulBlocksInPkt;
    register unsigned long ucBalWords;
    register unsigned long ucBalAccu = 0;
    
    extern unsigned long pulARM11_ARM_MRC_DTR_Rd[2];    /* MRC P14,0,R0,c0,c5,0*/
    extern unsigned long pulARM11_ARM_STC_R0[2];        /* STC p14,c5,[R0],#4  */
    extern unsigned long pulARM11_ARM_NOP[2];           /* NOP */
    extern unsigned long ulARM_JTAG_ITRSEL;
    extern unsigned long ulARM_JTAG_EXTEST;

    unsigned long ulIndex;
    unsigned long ulBlockCountTemp;
    unsigned long ulAddress;
    unsigned long ulDataToWrite = 0;

    unsigned long ulTempIR;
    unsigned long ulTempDR;

    ulTempIR = tyJtagScanConfig.ulTmsForIR;
    ulTempDR = tyJtagScanConfig.ulTmsForDR;

    ulAddress = *pulAddress;
    ulBlockCountTemp = ulBlockcount;

    put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
    put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

    ptyRxData->bFastDataRecvFlag = 0;                                      /* Clear flag (ready for next packet) */
    put_hvalue(USBEPC_STT, 0x0002);                                        /* Clear B_RxPktReady in EPC Stt register */
    put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);               /* Enable EPC event interrupt */

/*------------------------------------------------------------------------*/
/*                      Memory Write Initial Sequence                     */                                  
/*------------------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /*           Place Memory Address -> R0 (as pointer)              */                                  
    /*----------------------------------------------------------------*/

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); 

    /* IR Scan:SCAN_N as Current Instruction -> SCREG between TDI & TDO*/
    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(0)+ 0), ulARM_JTAG_SCAN_N);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

    /* DR Scan:Scan Chain Number(DTR) -> SCREG */
    put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(1), (ARM11_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(1)+ 0), ARM11_SCAN_CHAIN_DTR);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(2), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(2)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* DR Scan:Scans in the DTR -> R0 instruction into ITR */ 
    put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(3), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(3)+ 0), pulARM11_ARM_MRC_DTR_Rd[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    /* IR Scan:EXTEST as Current Instruction  for Writing into DTR*/ 
    put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(4), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(4)+ 0), ulARM_JTAG_EXTEST);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

    /* DR Scan:Scans in Address -> DTR */   
    put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(5), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(5)+ 0), ulAddress);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(6), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(6)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* DR Scan:Scans in the NOP instruction into ITR */    
    put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(7), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(7)+ 0), pulARM11_ARM_NOP[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7)); 

    while (get_hvalue(JTAG_JASR)) ;                     /* Waiting to sent all buffers */
    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)      /* Check for adaptive clock timeout */
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
    put_hvalue(JTAG_JSCTR, usReg);                      /* Disable AutoScan */


    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    do
        {
        usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
        put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));

        put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
        put_wvalue(JTAG_SPARAM_CNT(0), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);

        put_wvalue((JTAG_TDI_BUF(0)+ 0), 0x00000000);
        put_wvalue((JTAG_TDI_BUF(0)+ 4), 0x00000000);
        put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

        while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
            ;                                               /* check buffer has already been sent */
        if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)      /* Check for adaptive clock timeout */
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
        put_hvalue(JTAG_JSCTR, usReg);                      /* Disable AutoScan */
        }
    while ((get_wvalue((JTAG_TDO_BUF(0) + 4)) & ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT);

    /*----------------------------------------------------------------*/
    /*   Select DTR for writing data; Place DTR -> [R0] Instruction   */                                  
    /*----------------------------------------------------------------*/
    /* Restore previous settings */
    tyJtagScanConfig.ulTmsForIR = ulTempIR;
    tyJtagScanConfig.ulTmsForDR = ulTempDR;

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); 

    /* IR Scan:SCAN_N as Current Instruction -> SCREG between TDI & TDO*/
    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(0)+ 0), ulARM_JTAG_SCAN_N);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

    /* DR Scan:Scan Chain Number(DTR) -> SCREG */
    put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(1), (ARM11_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(1)+ 0), ARM11_SCAN_CHAIN_DTR);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(2), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(2)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* DR Scan:Scans in the DTR -> R0 instruction into ITR */ 
    put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(3), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(3)+ 0), pulARM11_ARM_STC_R0[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    /* IR Scan:EXTEST as Current Instruction  for Writing into DTR*/ 
    put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(4), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(4)+ 0), ulARM_JTAG_EXTEST);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

    while (get_hvalue(JTAG_JASR)) ;                     /* Waiting to sent all buffers */
    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)      /* Check for adaptive clock timeout */
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
    put_hvalue(JTAG_JSCTR, usReg);                      /* Disable AutoScan */
    /*------------------------------------------------------------------------*/
    /*                          Memory Write                                  */
    /*------------------------------------------------------------------------*/
    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

    while (ulBlockCountTemp)
        {
        while (!ptyRxData->bFastDataRecvFlag) ;          /* wait for data from USB (using fast transfers)*/

        if ((ptyRxData->ulFastDataSize & 0x03))
            {
            ulResult = ERR_ARM_DATA_NOT_WORD_ALIGNED;
            break;
            }
        ulBlocksInPkt = ptyRxData->ulFastDataSize / (14 * 4); 
        ucBalWords = (ptyRxData->ulFastDataSize % (14 * 4))/4; 

        if (ulBlockCountTemp < ulBlocksInPkt)
            {
            ulBlocksInPkt = ulBlockCountTemp;      
            }
        ulBlockCountTemp -= ulBlocksInPkt;

        while (ulBlocksInPkt)
            {
            DLOG2(("A"));
            for (ulIndex=0; ulIndex<14; ulIndex++)
                {
                ulDataToWrite = get_wvalue(USBEPC_FIFO);               
                do
                    {
                    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
                    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));

                    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
                    put_wvalue(JTAG_SPARAM_CNT(0), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);
                    put_wvalue((JTAG_TDI_BUF(0)+ 0),ulDataToWrite );
                    put_wvalue((JTAG_TDI_BUF(0)+ 4),0x00000000 );
                    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0)); 
         
                    while (get_hvalue(JTAG_JASR)) ;                                     /* Waiting to sent all buffers */
                    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                      /* Check for adaptive clock timeout */
                        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
                    put_hvalue(JTAG_JSCTR, usReg);                                      /* Disable AutoScan */
                    }
                while ((get_wvalue((JTAG_TDO_BUF(0) + 4)) & ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT);
                }
            ulBlocksInPkt--;
            }       
        if (ucBalWords)
            {
            DLOG2(("B"));
            ucBalAccu += ucBalWords;

            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_ASE);         /* Enable AutoScan */

            for (ulIndex=0; ulIndex<ucBalWords; ulIndex++)
                {
                ulDataToWrite = get_wvalue(USBEPC_FIFO);
                do
                    {
                    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
                    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));

                    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
                    put_wvalue(JTAG_SPARAM_CNT(0), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);
                    put_wvalue((JTAG_TDI_BUF(0)+ 0),ulDataToWrite );
                    put_wvalue((JTAG_TDI_BUF(0)+ 4),0x00000000 );
                    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0)); 

                    while (get_hvalue(JTAG_JASR)) ;                                     /* Waiting to sent all buffers */
                    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                      /* Check for adaptive clock timeout */
                        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
                    put_hvalue(JTAG_JSCTR, usReg);  
                    }
                while ((get_wvalue((JTAG_TDO_BUF(0) + 4)) & ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT);
                }
            if (ucBalAccu >= 14)
                {
                ucBalAccu -= 14;
                ulBlockCountTemp -= 1;
                }

            ptyRxData->bFastDataRecvFlag = 0;                                      /* Clear flag (ready for next packet) */
            put_hvalue(USBEPC_STT, 0x0002);                                        /* Clear B_RxPktReady in EPC Stt register */
            put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);               /* Enable EPC event interrupt */
            }
        }
    /* NOP -> ITR */
    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); 

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(0)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

    /* DR Scan:Scans in the NOP instruction into ITR */    
    put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(1), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(1)+ 0), pulARM11_ARM_NOP[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

    while (get_hvalue(JTAG_JASR)) ;                 /* waiting to sent all buffers */
    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)  /* check for adaptive clock timeout */
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
    put_hvalue(JTAG_JSCTR, usReg);                  /* disable AutoScan */


    /* Restore previous settings */
    tyJtagScanConfig.ulTmsForIR = ulTempIR;
    tyJtagScanConfig.ulTmsForDR = ulTempDR;

    return ulResult;
}
/****************************************************************************
     Function: ARML_ARM11ReadMultipleBlock_ReallyFast
     Engineer: Amerdas D K
        Input: unsigned long *pulAddress - Start address
               unsigned long ulBlockCount - number of blocks to be written into memory 
       Output: unsigned long - error code ERR_xxx
  Description: Write Data to a (ulBlockCount X block of 14) consecutive memory location very fast.
   Date        Initials    Description
09-Nov-2009      ADK      Initial
****************************************************************************/
unsigned long ARML_ARM11ReadMultipleBlock_ReallyFast(unsigned long *pulAddress, unsigned long *pulReadData, unsigned long ulBlockcount)
{
    unsigned long ulResult = ERR_NO_ERROR;    
    unsigned short usReg;
    
    extern unsigned long pulARM11_ARM_MRC_DTR_Rd[2];    /* MRC P14,0,R0,c0,c5,0*/
    extern unsigned long pulARM11_ARM_LDC_R0[2];        /* LDC p14,c5,[R0],#4 */
    extern unsigned long pulARM11_ARM_NOP[2];           /* NOP */

    extern unsigned long ulARM_JTAG_ITRSEL;
    extern unsigned long ulARM_JTAG_EXTEST;
    extern unsigned long ulARM_JTAG_INTEST;

    unsigned long ulAddress = *pulAddress;
    unsigned long ulWordsToRead;
    
    unsigned long ulTempIR;
    unsigned long ulTempDR;

    DLOG2(("ARML_ARM11ReadMultipleBlock_ReallyFast()"));

    ulTempIR = tyJtagScanConfig.ulTmsForIR;
    ulTempDR = tyJtagScanConfig.ulTmsForDR;
   
    put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
    put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);

    ulWordsToRead = (ulBlockcount*14);
    /*---------------------------------------------------------------------------------------------*/
    /*                       Memory Read Initial sequence                                          */
    /*---------------------------------------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /*           Place Memory Address -> R0 (as pointer)              */                                  
    /*----------------------------------------------------------------*/

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); 

    /* IR Scan:SCAN_N as Current Instruction -> SCREG between TDI & TDO*/
    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(0)+ 0), ulARM_JTAG_SCAN_N);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

    /* DR Scan:Scan Chain Number(DTR) -> SCREG */
    put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(1), (ARM11_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(1)+ 0), ARM11_SCAN_CHAIN_DTR);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(2), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(2)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* DR Scan:Scans in the DTR -> R0 instruction into ITR */ 
    put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(3), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(3)+ 0), pulARM11_ARM_MRC_DTR_Rd[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    /* IR Scan:EXTEST as Current Instruction  for Writing into DTR*/ 
    put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(4), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(4)+ 0), ulARM_JTAG_EXTEST);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

    /* TMS patten adjusted so that TAP will go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 5, 0x0D);

    /* DR Scan:Scans in Address -> DTR */   
    put_wvalue(JTAG_SPARAM_TMS(5), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(5), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(5)+ 0), ulAddress);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(6), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(6), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(6)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    /* DR Scan:Scans in the NOP instruction into ITR */    
    put_wvalue(JTAG_SPARAM_TMS(7), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(7), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(7)+ 0), pulARM11_ARM_NOP[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(7)); 

    while (get_hvalue(JTAG_JASR)) ;                     /* Waiting to sent all buffers */
    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)      /* Check for adaptive clock timeout */
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
    put_hvalue(JTAG_JSCTR, usReg);                      /* Disable AutoScan */


    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);

    do
        {
        usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
        put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));

        put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
        put_wvalue(JTAG_SPARAM_CNT(0), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);

        put_wvalue((JTAG_TDI_BUF(0)+ 0), 0x00000000);
        put_wvalue((JTAG_TDI_BUF(0)+ 4), 0x00000000);
        put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

        while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
            ;                                               /* check buffer has already been sent */
        if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)      /* Check for adaptive clock timeout */
            return ERR_ADAPTIVE_CLOCK_TIMEOUT;
        put_hvalue(JTAG_JSCTR, usReg);                      /* Disable AutoScan */
        }
    while ((get_wvalue((JTAG_TDO_BUF(0) + 4)) & ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT);
    /*----------------------------------------------------------------*/
    /*   Select DTR for writing data; Place DTR <- [R0] Instruction   */                                  
    /*----------------------------------------------------------------*/
    /* Restore previous settings */
    tyJtagScanConfig.ulTmsForIR = ulTempIR;
    tyJtagScanConfig.ulTmsForDR = ulTempDR;

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); 

    /* IR Scan:SCAN_N as Current Instruction -> SCREG between TDI & TDO*/
    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(0)+ 0), ulARM_JTAG_SCAN_N);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

    /* DR Scan:Scan Chain Number(DTR) -> SCREG */
    put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(1), (ARM11_SCAN_CHAIN_SELECT_REG_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(1)+ 0), ARM11_SCAN_CHAIN_DTR);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(2), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(2), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(2)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));

    /* The TMS for DR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(3, 0x01, 4, 0x0D);
   
    /* DR Scan:Scans in the DTR -> R0 instruction into ITR */ 
    put_wvalue(JTAG_SPARAM_TMS(3), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(3), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(3)+ 0), pulARM11_ARM_LDC_R0[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3)); 

    /* The TMS for IR is Adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(4, 0x03, 4, 0x0D);

    /* IR Scan:EXTEST as Current Instruction  for Writing into DTR*/ 
    put_wvalue(JTAG_SPARAM_TMS(4), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(4), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(4)+ 0), ulARM_JTAG_INTEST);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));

    while (get_hvalue(JTAG_JASR)) ;                     /* Waiting to sent all buffers */
    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)      /* Check for adaptive clock timeout */
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
    put_hvalue(JTAG_JSCTR, usReg);                      /* Disable AutoScan */

    /*---------------------------------------------------------------------------------------------*/
    /*                                      Memory Read                                            */
    /*---------------------------------------------------------------------------------------------*/
    /* TMS patten adjusted so that TAP will not go to RTI */
    tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(4, 0x02, 4, 0x0D);
 
    while (ulWordsToRead)
        {
        do
            {
            usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
            put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));

            put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
            put_wvalue(JTAG_SPARAM_CNT(0), (ARM11_SCAN_CHAIN_DTR_LENGTH & 0x000007FF) | 0x00008000);
            put_wvalue((JTAG_TDI_BUF(0)+ 0),0x00000000 );
            put_wvalue((JTAG_TDI_BUF(0)+ 4),0x00000000 );
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0)); 

            while (get_hvalue(JTAG_JASR)) ;                                     /* Waiting to sent all buffers */
            if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                      /* Check for adaptive clock timeout */
                return ERR_ADAPTIVE_CLOCK_TIMEOUT;
            put_hvalue(JTAG_JSCTR, usReg);  
            }
        while ((get_wvalue((JTAG_TDO_BUF(0) + 4)) & ARM11_DTR_READY_BIT) != ARM11_DTR_READY_BIT);

        *pulReadData++ = get_wvalue(JTAG_TDO_BUF(0));
        ulWordsToRead--;
        }

    /* NOP -> ITR */
    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); 

    /* IR Scan:Scans in the ITRSEL instruction */   
    put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
    put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
    put_wvalue((JTAG_TDI_BUF(0)+ 0), ulARM_JTAG_ITRSEL);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));

    /* DR Scan:Scans in the NOP instruction into ITR */    
    put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForDR);
    put_wvalue(JTAG_SPARAM_CNT(1), (ARM11_SCAN_CHAIN_ITR_LENGTH & 0x000007FF) | 0x00008000);
    put_wvalue((JTAG_TDI_BUF(1)+ 0), pulARM11_ARM_NOP[0]);
    put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1)); 

    while (get_hvalue(JTAG_JASR)) ;                 /* waiting to sent all buffers */
    if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)  /* check for adaptive clock timeout */
        return ERR_ADAPTIVE_CLOCK_TIMEOUT;
    put_hvalue(JTAG_JSCTR, usReg);                  /* disable AutoScan */

    /* Restore previous settings */
    tyJtagScanConfig.ulTmsForIR = ulTempIR;
    tyJtagScanConfig.ulTmsForDR = ulTempDR;

    return ulResult;
}
