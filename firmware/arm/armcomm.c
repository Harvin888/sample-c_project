/******************************************************************************
       Module: armcomm.c
     Engineer: Vitezslav Hola, Shameerudheen PT
  Description: ARM commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial structure
  03-jul-2007    SPT         added ARM Diskware Commands
  25-sep-2009    ADK         Added ARM11 Commands
******************************************************************************/
#include "common/comms.h"
#include "arm/armcomm.h"
#include "arm/armlayer.h"
#include "common/fpga/jtag.h"
//#include "export/api_err.h"
#include "export/api_cmd.h"
#include "arm/cortexm.h"
#include "arm/cortexa.h"
#define PKT_WORD_LEN 4 

// global variable (referred from other modules in diskware)
unsigned short usCurrentCore = 0;
unsigned char ucPrevBigEndian; //used for keeping previous Endian sttings 1 for Big 0 for Little
                               //need to cahnge this variable later by arm configuration structure
                               // for each core the will be one set of structure data
// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;


/****************************************************************************
     Function: ProcessARMCommand
     Engineer: Vitezslav Hola
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incomming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing ARM commands
Date           Initials    Description
28-May-2007    VH          Initial
03-jul-2007    SPT         added ARM Diskware Commands
****************************************************************************/
int ProcessARMCommand(unsigned long ulCommandCode, unsigned long ulSize)
{
   int iReturnValue = 1;
  
   switch(ulCommandCode)
      {
      //ARM diskware commands
      case CMD_CODE_ARM_DEBUG_REQUEST:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ARML_DebugRequest(ptyRxData->pucDataBuf[0x06]);
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_DEBUG_ACKNOWLEDGE:
         {
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ARML_GetDebugAcknowledge(&(ptyTxData->pucDataBuf[0x04]));
         ptyTxData->ulDataSize = PKT_WORD_LEN + 1;                                     // result + 1 byte for DBGACK status
         }
         break;
	  case CMD_CODE_ARM_INIT_TARGET:
		 {
		 usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
		 JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
		 *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ARML_InitTarget(ptyRxData->pucDataBuf[0x06]);
		 ptyTxData->ulDataSize = PKT_WORD_LEN;
		 }
		 break;

      case CMD_CODE_ARM_SELECT_SCAN_CHAIN:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_SelectScanChain(ptyRxData->pucDataBuf[8], 
                                                       ptyRxData->pucDataBuf[6],
                                                       ptyRxData->pucDataBuf[7]);
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_RESET_PROC:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ResetProc(ptyRxData->pucDataBuf[6], ((unsigned char *)(ptyTxData->pucDataBuf + 0x04)));
         ptyTxData->pucDataBuf[5] = 0x0; //Previous states not implemented
         ptyTxData->ulDataSize = 6;
         }
         break;
      case CMD_CODE_ARM_SET_SAFE_NON_VECTOR_ADDRESS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_SetSafeNonVectorAddress(ptyRxData->pucDataBuf[8], 
                                   ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)), (*(unsigned char*)(ptyRxData->pucDataBuf + 0x9)));    
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_READ_ICE_BREAKER:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ReadICEBreaker(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)), 
                                   ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));
         ptyTxData->ulDataSize = 2 * PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ICE_BREAKER:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_WriteICEBreaker(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)), 
                                   ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_STATUS_PROC:
         {         
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMStatusProc ((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                          ((unsigned char *)(ptyRxData->pucDataBuf + 0x09)),
                          ((unsigned char *)(ptyRxData->pucDataBuf + 0x0A)),
                          ((unsigned char *)(ptyTxData->pucDataBuf + 0x04)),
                          ((unsigned char *)(ptyTxData->pucDataBuf + 0x05)),
                          ((unsigned char *)(ptyTxData->pucDataBuf + 0x06)),
                          ((unsigned char *)(ptyTxData->pucDataBuf + 0x07)));
         ptyTxData->ulDataSize = (2 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_REGISTERS:
         {
         // read ALL REGISTORS
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))    
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMReadRegisters((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                          ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));

         if(CORTEXA == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))    
             ptyTxData->ulDataSize = (41 * PKT_WORD_LEN); //152
         else
             ptyTxData->ulDataSize = (38 * PKT_WORD_LEN); //152
         
         }
         break;
      case CMD_CODE_ARM_WRITE_REGISTERS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMWriteRegisters((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                           ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_PERFORM_ARM_NOPS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMPerfromARMNops((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                           (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_CHANGE_TO_ARM_MODE:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMChangeModeToARMMode((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                                (*(unsigned char *)(ptyRxData->pucDataBuf + 0x09)),
                                                (*(unsigned char *)(ptyRxData->pucDataBuf + 0x0A)),
                                                ((unsigned char *)(ptyTxData->pucDataBuf + 0x04)));
         ptyTxData->ulDataSize = 5;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM940_946CP:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Write940_946CPSingleReg((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                                (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_RESTART:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))   
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_JtagRestart(); 
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI:
         { 
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))    
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_BringToRTIFromTLR(); 
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_WORD:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMWriteWord((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_READ_WORD:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMReadWord((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                     (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                     ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));
         ptyTxData->ulDataSize = (2 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_CHECK_FOR_DATA_ABORT:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMCheckDataAbort((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                     ((unsigned char *)(ptyTxData->pucDataBuf + 0x04)));
         ptyTxData->ulDataSize = 5;
         }
         break;
      case CMD_CODE_ARM_READ_MULTIPLE_WORDS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         
         ptyTxData->ulDataSize = 0;

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMReadMultipleWords((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                                                                 (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                                                                 (*(unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
                                                                                 ((unsigned long *)(ptyTxData->pucDataBuf + 0x4)));
          ptyTxData->ulDataSize = ((*(unsigned long *)(ptyRxData->pucDataBuf + 0x10))* PKT_WORD_LEN) + 0x4;
         }
         break;
      case CMD_CODE_ARM_WRITE_MULTIPLE_WORDS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMWriteMultipleWords((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                                                                  (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                                                                  (*(unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
                                                                                  ((unsigned long *)(ptyRxData->pucDataBuf + 0x14)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_BYTE:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08))) 
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMWriteByte((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                      (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                      (*(unsigned char *)(ptyRxData->pucDataBuf + 0x10)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_READ_MULTIPLE_BLOCKS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMReadMultipleBlocks((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                                                                  (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                                                                  (*(unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
                                                                                  (unsigned long *)(ptyTxData->pucDataBuf + 0x4));
         ptyTxData->ulDataSize = ((*(unsigned long *)(ptyRxData->pucDataBuf + 0x10)) * 14 * PKT_WORD_LEN) + 4;
         }
         break;
      case CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMWriteMultipleBlocks((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                                                                  (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                                                                  (*(unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
                                                                                  ((unsigned long *)(ptyRxData->pucDataBuf + 0x14)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_EXECUTE_PROC:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMExecuteProc((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                      ((unsigned char *)(ptyRxData->pucDataBuf + 0x0C)),
                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_SETUP_EXECUTE_PROC:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMSetupExecuteProc((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
        case CMD_CODE_ARM_SETUP_HARD_BREAK:
            {
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
                JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);
                if (CORTEXM == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_CortexMHWBreakpoint((*(unsigned char *)(ptyRxData->pucDataBuf + 0x0A)),
                                                                                        ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
                    }
                else if (CORTEXA == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_BreakPoint(*(unsigned short *)(ptyRxData->pucDataBuf + 0x0A),
                                                                                       (unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
                    }
                
                ptyTxData->ulDataSize = PKT_WORD_LEN;
            }         
            break;
        case CMD_CODE_ARM_SETUP_WATCH_POINT:
            {
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
                JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);
                if (CORTEXM == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_CortexMWatchpoint((*(unsigned char *)(ptyRxData->pucDataBuf + 0x0A)),
                                                                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));
                    }

                else if (CORTEXA == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_WatchPoint(*(unsigned short *)(ptyRxData->pucDataBuf + 0x0A),
                                                                                       (unsigned long *)(ptyRxData->pucDataBuf + 0x10));
                    }
                
                ptyTxData->ulDataSize = PKT_WORD_LEN;
            }         
            break;

      case CMD_CODE_ARM_CHANGE_TO_THUMB_MODE:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMChangeToThumbMode((*(unsigned char *)(ptyRxData->pucDataBuf + 0x08)),
                                              (*(unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
                                              (*(unsigned long *)(ptyRxData->pucDataBuf + 0x10)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM920_922CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_WriteARM920_922CPReg((unsigned char *)(ptyRxData->pucDataBuf + 0x08),
                              ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM926CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_WriteARM926CPReg(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM940_946CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_WriteARM940_946CPReg(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_READ_ARM940_946CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Read940_946CPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = ((19 * PKT_WORD_LEN) + PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_ARM9_RUN_CACHE_CLEAN:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM9RunCacheClean(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;   
      case CMD_CODE_ARM_WRITE_ARM966CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_WriteARM966CPReg(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM968CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Write968CPReg(((unsigned long *)(ptyRxData->pucDataBuf + 0x08)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM920_922CP:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Write920_922CPSingleReg((*(unsigned char*)(ptyRxData->pucDataBuf + 0x08)),
                                                 (*(unsigned long*)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM926CP:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Write926CPSingleReg((*(unsigned char*)(ptyRxData->pucDataBuf + 0x08)),
                                                 (*(unsigned long*)(ptyRxData->pucDataBuf + 0x0C)));
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_ARM920_922CP_INVALIDATE_ICACHE:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM920_922InvalidateICache();
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_READ_ARM920_922CP:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Read920_922CPSingleReg((*(unsigned char*)(ptyRxData->pucDataBuf + 0x08)),
                                                (unsigned long*)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (2 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM920_922CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ReadARM920_922CPReg ((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (17 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM926CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Read926CPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (16 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM966CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Read966CPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (8 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM968CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_Read968CPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (8 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_DCC_CHANNEL:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMReadWriteDCC(*(unsigned char *)(ptyRxData->pucDataBuf + 0x08), //ucProcessor
                                         (unsigned char *) (ptyRxData->pucDataBuf + 0x09), //pucDataToWrite
                                         (unsigned long *) (ptyRxData->pucDataBuf + 0x0C), //pulWriteData
                                         (unsigned char *) (ptyTxData->pucDataBuf + 0x04), //pucDataWriteSuccessful
                                         (unsigned char *) (ptyTxData->pucDataBuf + 0x05), //pucDataReadFromCore
                                         (unsigned long *) (ptyTxData->pucDataBuf + 0x08)  //pulReadData
                                         );
         ptyTxData->ulDataSize = (3 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM720CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM7_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM720WriteCPReg((unsigned long *)(ptyRxData->pucDataBuf + 0x08));
         ptyTxData->ulDataSize = (PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM740CP_REG:
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM7_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM740WriteCPReg((unsigned long *)(ptyRxData->pucDataBuf + 0x08));
         ptyTxData->ulDataSize = (PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM720CP_REG:
         { 
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM7_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM720ReadCPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (8 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM740CP_REG:
         { 
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM7_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM740ReadCPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (7 * PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_GET_MULTICORE_INFO:
         { 
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMGetMultiCoreInfo((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (8 + ((*(unsigned long *)(ptyTxData->pucDataBuf + 0x04)) * PKT_WORD_LEN));
         }
         break;
        case CMD_CODE_READ_DSCR:    
            {
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
                if (ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                {
                    JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11ReadDSCR((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
                }
                else if (CORTEXA == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_ReadDSCR((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
                    }

                ptyTxData->ulDataSize = (2*PKT_WORD_LEN);
            }
            break;
      case CMD_CODE_ARM11_READ_CPSR:    
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         else
             JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11ReadCPSR((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (2*PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_READ_ARM1136CP_REG:    
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11ReadCPReg((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (4*PKT_WORD_LEN);
         }
         break;
      case CMD_CODE_ARM_WRITE_ARM11_CP:    
         {
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11WriteCPReg((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
         ptyTxData->ulDataSize = (1*PKT_WORD_LEN);
         }
         break;
        
        case CMD_CODE_ENTER_DEBUG_STATE:
            {
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));       
                if (ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11EnterDebugState(*((unsigned char *)(ptyRxData->pucDataBuf + 0x09)),
                                                                                            (unsigned long *)(ptyTxData->pucDataBuf + 0x04));

                    ptyTxData->ulDataSize = (6 * PKT_WORD_LEN);
                    }
                else if (CORTEXM == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexM_EnterDebugState((unsigned long)(ptyTxData->pucDataBuf + 0x04));

                    ptyTxData->ulDataSize = (PKT_WORD_LEN);
                    }
                else if (CORTEXA == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_EnterDebugState((unsigned long *)(ptyRxData->pucDataBuf + 0x0C),
                                                                                            (unsigned long*)(ptyTxData->pucDataBuf + 0x04));

                    ptyTxData->ulDataSize = (2 * PKT_WORD_LEN);
                    }
                
                break;
            }
        case CMD_CODE_ARM11_LEAVE_DEBUG_STATE:    
            {
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
                if (ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);

                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11_LeaveDebugState((unsigned char *)(ptyRxData->pucDataBuf + 0x09),
                                                                                             *((unsigned char *)(ptyRxData->pucDataBuf + 0x0A)),
                                                                                             (unsigned long *)(ptyRxData->pucDataBuf + 0x0C));        
                    }
                else if (CORTEXM == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexM_LeaveDebugState((unsigned long)(ptyRxData->pucDataBuf + 0x08));
                    }

                else if (CORTEXA == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    {
                    ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_LeaveDebugState((unsigned char*)(ptyRxData->pucDataBuf + 0x09),
                                                                                           (unsigned long*)(ptyRxData->pucDataBuf + 0x0C));
                    }
                ptyTxData->ulDataSize = PKT_WORD_LEN;
            }
            break;
      case CMD_CODE_ARM_WRITE_CP14_REG:    
         {
          usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
          if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
              JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
             
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11_WriteSC7(*((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),(unsigned long *)(ptyRxData->pucDataBuf + 0x10));
                                
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
      case CMD_CODE_ARM_INST_EXE_ENABLE:    
         {
          usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
          if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);

          ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11ExeInstEnable((unsigned long *)(ptyTxData->pucDataBuf + 0x04));                              
          ptyTxData->ulDataSize = (2*PKT_WORD_LEN);
         }
         break;
       case CMD_CODE_CLEAN_ARM11_CACHES:
         {
          DLOG3(("ARM11 MSG: CMD_CODE_CLEAN_ARM11_CACHES"));
          usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
          if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11InvalidateCleanCache();
         ptyTxData->ulDataSize = PKT_WORD_LEN;
         }
         break;
       case CMD_CODE_ARM11_READ_CDCR:
         {
          usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
          if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
             JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARM11ReadCDCR(*((unsigned char *)(ptyRxData->pucDataBuf + 0x09)),
                                                                            (unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         ptyTxData->ulDataSize = (2*PKT_WORD_LEN);
         }
         break;
        case CMD_CODE_CONFIG_CORESIGHT:
            {
                unsigned long* pulTemp;
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
                pulTemp = ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
                JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);
                ((unsigned long *)ptyTxData->pucDataBuf)[0] = CoreSight_ConfigDebugPort(pulTemp);                              
                ptyTxData->ulDataSize = (PKT_WORD_LEN);
            }
            break;

        case CMD_CODE_READ_ROM_TABLE:
            {
                unsigned long ulCnt;

                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
                JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);
                ((unsigned long*)ptyTxData->pucDataBuf)[0] = ReadRomTableEntries(*(unsigned long *)(ptyRxData->pucDataBuf + 0x8), 
                                                                                 *(unsigned long *)(ptyRxData->pucDataBuf + 0x0C),
																				  /* Reply data count */
																				  (unsigned long *)(ptyTxData->pucDataBuf + 0x4),
                                                                                  /* Number of entries */
                                                                                  (unsigned long *)(ptyTxData->pucDataBuf + 0x8),
																				  /* Reply data */
				/* In long format */											  (unsigned long *)(ptyTxData->pucDataBuf + 0xC));
                ulCnt = *(unsigned long *)(ptyTxData->pucDataBuf + 0x4);

				ptyTxData->ulDataSize = (ulCnt * PKT_WORD_LEN);
            }

            break;
        case CMD_CODE_SINGLE_STEP:
           {
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
           if(CORTEXM == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
               JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);

          ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_CortexMSingleStep();                              
          ptyTxData->ulDataSize = (PKT_WORD_LEN);
           }
           break;

       case CMD_CODE_RESET_DAP:
           {
           JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);

           ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_DapResetProc();                              
          ptyTxData->ulDataSize = (PKT_WORD_LEN);
           }
           break;

        case CMD_CODE_CONFIG_VECTORCATCH:
            {
                JtagSelectCore(usCurrentCore, CORTEXM_IR_LENGTH);

                ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_VectorCatch(*(unsigned long *)(ptyRxData->pucDataBuf + 0xC));                              
                ptyTxData->ulDataSize = (PKT_WORD_LEN);
            }
            break;

        case CMD_CODE_READ_CP_REGISTER:
            {
                usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

                if (ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                    JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
                else
                    JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
                /* Processor Family */
                ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMReadSelectedCPReg(*(unsigned long *)(ptyRxData->pucDataBuf + 0x08),
                                                                                        (unsigned long *)(ptyRxData->pucDataBuf + 0x0C),
                                                                                        /* CP Reg:Opcode/Address */
                                                                                        (unsigned long *)(ptyRxData->pucDataBuf + 0x10),
                                                                                        /* Return: Read data */
                                                                                        (unsigned long *)(ptyTxData->pucDataBuf + 0x04));
                ptyTxData->ulDataSize = (2*PKT_WORD_LEN);
            }
            break;
       case CMD_CODE_WRITE_CP_REGISTER:
			 {
             usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
             
             if(ARM11 == *((unsigned char *)(ptyRxData->pucDataBuf + 0x08)))
                 JtagSelectCore(usCurrentCore, ARM11_IR_LENGTH);
             else
                 JtagSelectCore(usCurrentCore, ARM9_IR_LENGTH);
             /* Processor Family */
             ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_ARMWriteSelectedCPReg(*(unsigned long *)(ptyRxData->pucDataBuf + 0x08),
                                                            (unsigned long *)(ptyRxData->pucDataBuf + 0x0C),
                                                            /* CP Reg:Opcode/Address */
                                                            (unsigned long *)(ptyRxData->pucDataBuf + 0x10),
                                                            /* Data to Write */
                                                            (unsigned long *)(ptyRxData->pucDataBuf + 0x14));
             
             ptyTxData->ulDataSize = (PKT_WORD_LEN);
			 }
      break;

       case CMD_CODE_READ_DEBUG_REG:
           {
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

           JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);
           /* Processor Family */
           ((unsigned long *)ptyTxData->pucDataBuf)[0] = CoreSight_ReadDebugReg(*(unsigned long *)(ptyRxData->pucDataBuf + 0x08),
                                                                                *(unsigned long *)(ptyRxData->pucDataBuf + 0x0C),
                                                                                /* Return: Read data */
                                                                                 (unsigned long *)(ptyTxData->pucDataBuf + 0x04));
    
           ptyTxData->ulDataSize = (2 * PKT_WORD_LEN);

           }
           break;

       case CMD_CODE_READ_CORESIGHT_BANK15_REG:
           {
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

           JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);
           /* Processor Family */
           ((unsigned long *)ptyTxData->pucDataBuf)[0] = GetBank15RegValue(*(unsigned long *)(ptyRxData->pucDataBuf + 0x08),
                                                                            /* Return: Read data */
                                                                            (unsigned long *)(ptyTxData->pucDataBuf + 0x04));
    
           ptyTxData->ulDataSize = (4 * PKT_WORD_LEN);

           }
           break;

       case CMD_CODE_CORTEXA_CLEAN_CACHE:
           {
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

           JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);

           ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_InvalidateCache();

           ptyTxData->ulDataSize = (PKT_WORD_LEN);
           }
           break;

       case CMD_CODE_INIT_ICEPICK:
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

           JtagSelectCore(usCurrentCore, ICEPICK_IR_LENGTH);

           ((unsigned long *)ptyTxData->pucDataBuf)[0] = ARML_InitilizeICePick(*((unsigned short *)(ptyRxData->pucDataBuf + 0x08)));

           ptyTxData->ulDataSize = (PKT_WORD_LEN);
           break;

       case CMD_CODE_WRITE_MEM_AHB_AP:
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

           JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);

           ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_WriteMemory_MemAp(
               *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
               *((unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
               *((unsigned long *)(ptyRxData->pucDataBuf + 0x14)),
               ((unsigned long *)(ptyRxData->pucDataBuf + 0x18)));

           ptyTxData->ulDataSize = (PKT_WORD_LEN);
           break;

       case CMD_CODE_READ_MEM_AHB_AP:
           usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

           JtagSelectCore(usCurrentCore, DAP_IR_LENGTH);

           ((unsigned long *)ptyTxData->pucDataBuf)[0] = CortexA_ReadMemory_MemAp(
               *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),
               *((unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
               *((unsigned long *)(ptyRxData->pucDataBuf + 0x14)),
               ((unsigned long *)(ptyRxData->pucDataBuf + 0x18)));

           ptyTxData->ulDataSize = (PKT_WORD_LEN);
           break;
           
       default :      // unknown command code
         iReturnValue = 0;
         break;
      }
   return iReturnValue;
}
