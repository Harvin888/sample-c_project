/******************************************************************************
       Module: armcomm.h
     Engineer: Vitezslav Hola
  Description: Header for ARM commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/

#ifndef _ARMCOMM_H_
#define _ARMCOMM_H

// function prototype (API)
int ProcessARMCommand(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _ARMCOMM_H
