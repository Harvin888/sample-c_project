/******************************************************************************
       Module: Coresight.c
     Engineer: Jeenus Chalattu Kunnath
  Description: This file implements the basic functionalities of Coresight 
             : technology.
  Date           Initials    Description
  05-Mar-2010    JCK          initial
******************************************************************************/

#include "../common/common.h"   /* Definition of debug message and internal I/O macros */
#include "coresight.h"                /* Definition of Coresight specific registers. */
#include "common/fpga/jtag.h"   /* JTAG functions like JtagScanDR */
#include "common/fpga/fpga.h"   /* Definitions of JTAG registers and buffers in the FPGA */
#include "export/api_err.h"     /* Definitions of Error returned by the Firmware*/
#include "armlayer.h"           /* Defintion of IR and DR lengths corsight core type */

extern void PrepareDpaccApaccData(
                          unsigned long* pulTDIVal,
                          unsigned long  ulAddr,
                          unsigned long  ulData,
                          unsigned long  bAction
                          );

extern void FilterDpaccApaccData(
                         unsigned long* pulDataFromScan,
                         unsigned long* pulData,    
                         UBYTE* pucAck
                         );

/* Added for support */
unsigned long gulARM_JTAG_DPACC     = 0xA;
unsigned long gulARM_JTAG_APACC     = 0xB;
unsigned long gulARM_JTAG_ABORT     = 0x8;
unsigned long gulARM_JTAG_BYPASS    = 0xF;

unsigned long gulCswProtVal; /* Used to save the CSW value. */

/* Used to save the parameters of the Coresight configurations initially set 
by the user. */
TyCoreSightParam tyCoreSightParam;
/****************************************************************************
Function    : ScanDapData
Engineer    : Jeenus C.K.
Input       : unsigned long* pulDapData - The input data to be scanned through 
            :                           - TDI.
            : unsigned long* pulReadData - The data read through TDO. If no need
            :                            - to read the data set this parameter as 
            :                            - NULL. 
Output      : unsigned long - error code for commands (ERR_xxx)
Description : This function is used to scan the data through the data.
            : Also this function is used to read the data from the TDO.
            : The input data i.e pulDapData, must of the format of DPACC/APACC.
            : The read data i.e pulReadData is only data from TDO. Acknowledgment 
            : will not return.    
Date            Initials    Description
05-Mar-2010     JCK         Initial
****************************************************************************/
RESULT ScanDapData(
    unsigned long* pulDapData,
    unsigned long* pulReadData)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulWaitCount = 0;
    unsigned char ucAck = ACK_OK_FAULT;
    unsigned long ulTDOVal[2];
    unsigned long ulReadData;

    /* Scan the Input Data. Wait till the acknowledgment is OK/Fault.
    Also return the data read if necessary. */
    do
        {
        Result = JtagScanDR(CORTEX_DR_LENGTH, pulDapData, ulTDOVal);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG2(("TDO Value: 0x%08x : 0x%08x", ulTDOVal[0], ulTDOVal[1]));

        /* Filter the read data and acknowledgment from the TDO. */
        FilterDpaccApaccData(ulTDOVal, &ulReadData, &ucAck); 

        /* If acknowledgment is Wait for a maximum cout return the error. */
        ulWaitCount = ulWaitCount + 1;
        if (MAX_WAIT_CNT == ulWaitCount)
            {
            /* From a software perspective, this is a fatal operation.
            When a debugger has observed stalled target hardware for
            an extended period (Stalled target hardware is indicated by repeated
            WAIT responses), write to  Abort register with DAP_ABORT bit set.
            After a DAP abort is requested, new transactions can be accepted by
            the DP. Ref. IHI0031A ARM Debug Interface v5 Architecture Specification
            page number 111. */
            unsigned long ulTDIVal[2];

            Result = JtagScanIR(&gulARM_JTAG_ABORT,NULL);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }

            PrepareDpaccApaccData(ulTDIVal, JTAG_DP_ABORT_REG, DAP_ABORT, WRITE);
            Result = JtagScanDR(CORTEX_DR_LENGTH, ulTDIVal, NULL);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }
            return ERR_ARM_MAXIMUM_COUNT_REACHED;
            }
        }
    while (WAIT == ucAck);

    /* If pointer to return the read data is not NULL, return the read data. */
    if (pulReadData != NULL)
        {
        *pulReadData = ulReadData;
        }    

    return Result;
}

/****************************************************************************
Function    : CoreSight_ReadDapReg
Engineer    : Jeenus C.K.
Input       : unsigned long  ulRegAddr - The Address of the Debug port  
                                       - registers and Access port registers
              unsigned long* pulData - To return the value read from  
                                     - the DAP register.
Output      : unsigned long - error code for commands (ERR_xxx)
Description : This function is used to read DAP register value. This functon
            : can be used to read DPACC registers as well as APACC registers.
Date            Initials    Description
05-Mar-2010     JCK         Initial
****************************************************************************/
RESULT CoreSight_ReadDapReg(
    unsigned long ulRegAddr, 
    unsigned long* pulData)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulTDIVal[2];    
    unsigned long ulReadData;

    DLOG2(("CoreSight_ReadDapReg Reg: 0x%08x", ulRegAddr));

    /* Prepare the Data to scanned into the DPACC or APACC register.
    Since for read operation, Data scanned to the TDI will be ignored
    by the Core. So set the data to be scanned as 0x0.*/
    PrepareDpaccApaccData(ulTDIVal, ulRegAddr, 0x0, READ);
    Result = ScanDapData(ulTDIVal, NULL);    
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Perform the Scan IR operation to Select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC,NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read RDBUFF register. The value read is the actual output data
    of the DAP Register. Ref ARM IHI0031A document. */
    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_RDBUFF, 0x0, READ);
    Result = ScanDapData(ulTDIVal, &ulReadData);    
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG2(("CoreSight_ReadDapReg Read data: 0x%08x", ulReadData));
    *pulData = ulReadData;

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function    : CoreSight_WriteDapReg
Engineer    : Jeenus C.K.
Input       : unsigned long ulRegAddr - The Address of the Debug port  
                                     - registers and Access port registers
              unsigned long ulData   - The value to be written DAP register.
Output      : unsigned long - error code for commands (ERR_xxx)
Description : This function is used to write a value to the DAP register. 
            : The same function can be used to write to DPACC and APACC 
            : registers.
Date            Initials    Description
05-Mar-2010     JCK         Initial
****************************************************************************/
RESULT CoreSight_WriteDapReg(
    unsigned long ulRegAddr, 
    unsigned long ulData)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulTDIVal[2];

    DLOG2(("CoreSight_WriteDapReg Reg: 0x%08x Data : 0x%08x", ulRegAddr, ulData));

    /* Write the Data to the DAP register. */
    PrepareDpaccApaccData(ulTDIVal, ulRegAddr, ulData, WRITE);
    Result = ScanDapData(ulTDIVal, NULL);    
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function    : CoreSight_ReadMultipleMem
Engineer    : Jeenus C.K.
Input       : unsigned long  ulNumData - The number of data to be read.
            : unsigned long* pulData   - The return the Value read from DRW
                                       - register.
            : unsigned long ulAccessType - Word or half-word or Byte access. 
Output      : unsigned long - error code for commands (ERR_xxx)
Description : This function is used to read multiple memory locations.
            : Before calling this function, appropriate Access port and bank 0
            : should be selected using the AP Select register. Also maximum
            : number of data which depends on the TAR auto-increment limit,
            : must be considered. As per ARM debug architecture version, 
            : Automatic address increment is only guaranteed to operate on
            : the bottom 10-bits of the address held in the TAR. But this is
            : Core dependent. In case of Cortex-M3 auto-increment  limit is 
            : bottom 16 bits.    
Date            Initials    Description
05-Mar-2010     JCK         Initial
****************************************************************************/
RESULT CoreSight_ReadMultipleMem(
    unsigned long ulNumData,
    unsigned char* pucData,
    unsigned long ulAccessType)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulTDIVal[2]; 
    unsigned short* pusData = (unsigned short*)pucData;
    unsigned long*  pulData = (unsigned long*)pucData;
    unsigned long ulReadData;
    unsigned long ulCountIndex;

    /* Read the first data. This data is not valid. so ignore the data read.*/
    PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, 0x0, READ);
    Result = ScanDapData(ulTDIVal, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
   
    /* Since we had read the DRW register, and last memory data can be 
    obtained using the RDBUFF register limit the count index to (NumData -1)*/
    for (ulCountIndex = 0; ulCountIndex < (ulNumData - 1); ulCountIndex++)
        {
        /* Read the data using the DRW register. */
        PrepareDpaccApaccData(ulTDIVal, MEM_AP_DRW_REG, 0x0, READ);
        Result = ScanDapData(ulTDIVal, &ulReadData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        
        switch (ulAccessType)
            {
            case ACCESS_SIZE_32:
                *pulData = ulReadData;
                pulData = pulData + 1;
                break;
            case ACCESS_SIZE_16:
                *pusData = (unsigned short)(ulReadData & 0xFFFF);
                pusData = pusData + 1;
                break;
            case ACCESS_SIZE_8:
                *pucData = (unsigned char)(ulReadData & 0xFF);
                pucData = pucData + 1;
                break;
            default:
                return ERR_ARM_INVALID_PARM;
            }
        }

    /* Read the RDBUFF register to get the last read value.*/
    PrepareDpaccApaccData(ulTDIVal, JTAG_DP_RDBUFF, 0x0, READ);
    Result = ScanDapData(ulTDIVal, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    switch (ulAccessType)
        {
        case ACCESS_SIZE_32:
            *pulData = ulReadData;
            pulData = pulData + 1;
            break;
        case ACCESS_SIZE_16:
            *pusData = (unsigned short)(ulReadData & 0xFFFF);
            pusData = pusData + 1;
            break;
        case ACCESS_SIZE_8:
            *pucData = (unsigned char)(ulReadData & 0xFF);
            pucData = pucData + 1;
            break;
        default:
            return ERR_ARM_INVALID_PARM;
        }

    return Result;
    
}

/****************************************************************************
Function    : CoreSight_WriteMultipleMem
Engineer    : Jeenus C.K.
Input       : unsigned long  ulNumData - The number of data to be read.
              unsigned long* pulData   - The return the Value read from DRW
                                       - register.
            : unsigned long ulAccessType - Word or half-word or Byte access. 
Output      : unsigned long - error code for commands (ERR_xxx)
Description : This function is used to write data to multiple memory locations.
            : The conditions for Number of data and AP select which are 
            : explained for read memory are applicable for this function also.  
Date            Initials    Description
05-Mar-2010     JCK         Initial
****************************************************************************/
RESULT CoreSight_WriteMultipleMem(
    unsigned long ulNumData,
    unsigned char* pucData,
    unsigned long  ulAccessType)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulTDIVal[2];
    unsigned long ulCountIndex;
    unsigned long ulWriteData;
    unsigned short* pusData = (unsigned short*)pucData;
    unsigned long*  pulData = (unsigned long*)pucData;

    DLOG (("CoreSight_WriteMultipleMem : ulNumData = 0x%08x, ulAccessType = 0x%08x", ulNumData, ulAccessType));
    for (ulCountIndex = 0; ulCountIndex < ulNumData; ulCountIndex++)
        {
        switch (ulAccessType)
        {
        case ACCESS_SIZE_32:
            ulWriteData = *pulData;
            pulData = pulData + 1;
            break;
        case ACCESS_SIZE_16:
            ulWriteData = (unsigned long)(*pusData & 0xFFFF);
            pusData = pusData + 1;
            break;
        case ACCESS_SIZE_8:
            ulWriteData = (unsigned long)(*pucData & 0xFF);
            pucData = pucData + 1;
            break;
        default:
            return ERR_ARM_INVALID_PARM;
        }

        /* Write the data to DRW register. */
        PrepareDpaccApaccData(ulTDIVal,MEM_AP_DRW_REG, ulWriteData, WRITE);
        Result = ScanDapData(ulTDIVal, NULL);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }        
        }

    DLOG (("End of CoreSight_WriteMultipleMem"));
    return Result;
}

/****************************************************************************
Function:   Coresight_ResetProc
Engineer:   Jeenus C.K.
Input   :   void  
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: This function is used to reset the debug access port.
Date            Initials    Description
22-Oct-2009     JCK         Initial
****************************************************************************/
RESULT Coresight_ResetProc(void)
{
    RESULT   Result      = ERR_NO_ERROR;
    unsigned short usReg;
    unsigned long  ulRegData;
    unsigned long  ulTmsTemp;
    unsigned long  ulSavedMcDRCount;

    DLOG(("Coresight_ResetProc\r\n"));

    /* Save current multi core settings */
    ulSavedMcDRCount = get_wvalue(JTAG_MCDRC);
    put_wvalue(JTAG_MCDRC, 0);
    
    /* As per Coresight Component TRM, to ensure JTAG is in the reset state 
    we must send more than 50 TCK clock cycles with TMS HIGH. */
    ulTmsTemp = TMS_PATTERN_16_ONES; 

    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);

    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(1,0));    
    put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(1,0));

    put_wvalue(JTAG_TDI_BUF(0), 0x0);
    put_wvalue(JTAG_TDI_BUF(1), 0x0); 
    put_wvalue(JTAG_TDI_BUF(2), 0x0); 
    put_wvalue(JTAG_TDI_BUF(3), 0x0); 

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    put_hvalue(JTAG_JASR, 0xF);

    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    /* If the current state of the core is SWD change the state to JTAG. */
    ulTmsTemp = SWD_JTAG_SEQUENCE; 
    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_TDI_BUF(0), 0x0);

    put_hvalue(JTAG_JASR, 0x1);
    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers

    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    ulTmsTemp = TMS_PATTERN_16_ONES; // 16 consutive ones

    /* In order to ensure reset, issue minimum 50TCK with TMS = 1 */
    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);

    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(1,0));    
    put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(1,0));

    put_wvalue(JTAG_TDI_BUF(0), 0x0);
    put_wvalue(JTAG_TDI_BUF(1), 0x0); 
    put_wvalue(JTAG_TDI_BUF(2), 0x0); 
    put_wvalue(JTAG_TDI_BUF(3), 0x0); 

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    put_hvalue(JTAG_JASR, 0xF);

    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers

    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    /* Restore multi core settings */
    put_wvalue(JTAG_MCDRC, ulSavedMcDRCount);

    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = CoreSight_ReadDapReg(JTAG_DP_CTRL_STAT_REG, &ulRegData);    
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }
    
    ulRegData = (CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR | CDBG_RSTREQ); 
    Result = CoreSight_WriteDapReg(JTAG_DP_CTRL_STAT_REG, 0x0);
    if(Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function:   CoreSight_ConfigDebugPort
Engineer:   Jeenus C.K.
Input   :   unsigned long* pulDebugPortParam - Pointer to the debug port
                            - configuration parameters  
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: This function is used to configure the parameters set by the
           : user for accessing the debug features.
Date            Initials    Description
09-Mar-2010     JCK         Initial
09-Aug-2010     SJ          Corrected AP access method. Earlier DPACC scan was not done.
****************************************************************************/
RESULT CoreSight_ConfigDebugPort(    
    unsigned long* pulDebugPortParam)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData = 0;
    unsigned long ulDatatoScan;

    tyCoreSightParam.ulDbgAccessApIndex   = *pulDebugPortParam;
    pulDebugPortParam = pulDebugPortParam + 1;
    tyCoreSightParam.ulMemAccessApIndex   = *pulDebugPortParam;
    pulDebugPortParam = pulDebugPortParam + 1;
    tyCoreSightParam.ulDbgBaseAddr        = *pulDebugPortParam;
    pulDebugPortParam = pulDebugPortParam + 1;
    tyCoreSightParam.ulUseMemApMemAccess  = *pulDebugPortParam;

    DLOG(("CoreSight_ConfigDebugPort 0x%08x", tyCoreSightParam.ulDbgAccessApIndex));
    DLOG(("CoreSight_ConfigDebugPort 0x%08x", tyCoreSightParam.ulMemAccessApIndex));
    DLOG(("CoreSight_ConfigDebugPort 0x%08x", tyCoreSightParam.ulDbgBaseAddr));
    DLOG(("tyCoreSightParam.ulUseMemApMemAccess 0x%08x", tyCoreSightParam.ulUseMemApMemAccess));

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /*  Wite to DP Control/Status Register */
    ulDatatoScan = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR;
    Result = CoreSight_WriteDapReg(JTAG_DP_CTRL_STAT_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Now we need to write to read the AP CSW register */
    /*  Wite to DP AP Select Register */
    ulDatatoScan = APSEL_SHIFT(tyCoreSightParam.ulDbgAccessApIndex) + AP_BANK_0;
    Result = CoreSight_WriteDapReg(JTAG_DP_AP_SELECT_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = CoreSight_ReadDapReg(MEM_AP_CSW_REG, &ulReadData);    
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Save the value of CSW for further CSW modification. 
    Whenever CSW value has to be modified, PROT field of CSW should be same 
    as the reset value. */
    gulCswProtVal = ulReadData & CSW_PROT;

    /* Clear the sticky error flag. */
    ulDatatoScan = gulCswProtVal | DBG_SW_ENABLE | ACCESS_SIZE_32 | INC_SINGLE;
    Result = CoreSight_WriteDapReg(MEM_AP_CSW_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

#if 0
/****************************************************************************
Function:   ReadRomTableEntries
Engineer:   Jeenus C.K.
Input   :   unsigned long ulBaseAddr - Base address of the ROM table.
        :   unsigned long ulApIndex - Access port number.
        :   TyCoresightComp* ptyCoresightComp - To return the Coresight components.  
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: This function is used to read the ROM table enties. This function 
           : also returns IDR register, debug base register address and 
           : Configuration register values.  
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT ReadRomTableEntries(
    unsigned long ulBaseAddr,
    unsigned long ulApIndex,
    TyCoresightComp* ptyCoresightComp)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulDatatoScan;

    DLOG(("ReadRomTableEntries"));

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulDatatoScan = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR;
    Result = CoreSight_WriteDapReg(JTAG_DP_CTRL_STAT_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* First read Bank 15 registers whch include IDR register, Debug base register
    and Configuration register. */
    {
        unsigned long ulIdrRegValue;
        unsigned long ulDbgBaseRegValue;
        unsigned long ulConfigRegValue;        

        /* First read the IDR, BASE and Configuration registers. These registers are
        part of bank 15 of the MEM-AP registers. */
        Result = GetBank15RegValue(&ulIdrRegValue,
                            &ulDbgBaseRegValue,
                            &ulConfigRegValue,
                            ulApIndex);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        DLOG(("IDR Register value: 0x%08x", ulIdrRegValue));
        DLOG(("Debug Base addrees: 0x%08x", ulDbgBaseRegValue));
        DLOG(("Configuration Register: 0x%08x", ulConfigRegValue));

        /* Return the register values. */
        ptyCoresightComp->ulIdrRegValue = ulIdrRegValue;
        ptyCoresightComp->ulDbgBaseRegValue = (ulDbgBaseRegValue & BASEADDR_OFFSET_MASK);
        ptyCoresightComp->ulConfiRegValue = ulConfigRegValue;
    }

    {
        unsigned long ulRomTableEntry;      /* To read the ROM table entry value. */
        unsigned long ulCompIdReg[NUM_COMPO_ID_REG];  /* To save the Component ID value. */
        unsigned long ulPeriIdReg[NUM_PERI_ID_REG];   /* To save the peripheral ID value. */                
        unsigned long ulRomTableEntryAddr = ulBaseAddr;  /* To save the address of ROM table entries. */
        unsigned long ulCompIndex = 0;  /* Index of each ROM table components. */
        unsigned long ulNumEntries = 0;  /* To return the number of ROM table entries. */
        unsigned long ulCompBase;   /* The base addrees of the each component. */
        unsigned long ulDeviceTypeReg;  /* To save the Device type register value. */
        unsigned long ulWriteData;  /* To save the value to be written to DAp registers. */
        enum TyCompClass tyCompClass;

        TyRomTableEntry* ptyRomTableEntry = ptyCoresightComp->tyRomTableEntry; 

        DLOG(("ReadRomTableEntries Read"));

        /* Inorder to select DPACC registers. */
        Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
    
        ulDatatoScan = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR;
        Result = CoreSight_WriteDapReg(JTAG_DP_CTRL_STAT_REG, ulDatatoScan);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Inorder to select DPACC registers. */
        Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Select Bank 0 register. */
        ulWriteData = APSEL_SHIFT(ulApIndex) + AP_BANK_0;
        Result = CoreSight_WriteDapReg(JTAG_DP_AP_SELECT_REG, ulWriteData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }  

         /* Inorder to select APACC registers. */
        Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }


        {        
            unsigned long ulReadData;
            Result = CoreSight_ReadDapReg(MEM_AP_CSW_REG, &ulReadData);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }

            gulCswProtVal = ulReadData & CSW_PROT;
        }
        
        /* Inorder to select APACC registers. */
        Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }


        /* Write to the CSW register with CSW prot value, debug software enable,
        access size as 32 bit and auto increment single enabled. */       
        ulWriteData = ((gulCswProtVal & CSW_PROT) |
                                    DBG_SW_ENABLE |
                                   ACCESS_SIZE_32 |
                                    INC_SINGLE);
        Result = CoreSight_WriteDapReg(MEM_AP_CSW_REG, ulWriteData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Inorder to select APACC registers. */
        Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        /* Next check that base address which is input by the user is of ROM Table 
        base or not. 
        For this Read the component ID of the Base Address. Then check that
        Compnent ID class is ROM_TABLE_COMP or not. If not ROM Table return error.*/
        Result = GetCompIdValue(ulBaseAddr, ulCompIdReg);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }
        tyCompClass = (enum TyCompClass)((ulCompIdReg[COMP_ID_1] & CID1_COMPO_CLASS) >> 4);
        if (ROM_TABLE_COMP != tyCompClass)
            {
            return ERR_ARM_NOT_VALID_ROM_ADDR;
            }

        /* Now we can check the ROM Table Entries and there component types 
        and class. */        
        do
            { 
            /* First read the ROM table Entry. */
            Result = GetRomTableEntry(ulRomTableEntryAddr, &ulRomTableEntry);
            if (Result != ERR_NO_ERROR)
                {
                return Result;
                }

            /* Steps to be followed are,
            Step 1: Check the component is present or not.
            If Component is not present go to step 5
            Step 2: Read the component ID's using the Base address of the 
            ROM table and ROM table entry. Ref ARM IHI0031A page number 228.
            Step 3: From Component ID from which we can find Component ID class.
            Step 4: Read Peripheral ID from which we can find Component Type.
            Component Type ID given is using Cortex core specs, the CoreSight
            components TRM (ARM DDI 0314H), and ETM specs. Also from chip 
            observation (e.g. TI SDTI). 
            Step 5: Check that */
            if (ENTRY_PRESENT == (ulRomTableEntry & ENTRY_PRESENT))
                {
                ptyRomTableEntry->bEntryPresent  = (BOOL)DBG_ENTRY_PRESENT;                

                ulCompBase = (ulBaseAddr & BASEADDR_OFFSET_MASK) + 
                    (ulRomTableEntry & BASEADDR_OFFSET_MASK);

                ptyRomTableEntry->ulCompBaseAddr = ulCompBase;

                Result = GetCompIdValue(ulCompBase, ulCompIdReg);
                if (Result != ERR_NO_ERROR)
                    {
                    return Result;
                    }   

                tyCompClass = (enum TyCompClass)((ulCompIdReg[COMP_ID_1] & CID1_COMPO_CLASS) >> 4);
                ptyRomTableEntry->tyCompClass = tyCompClass;
                
                Result = GetPeriIdValue(ulCompBase, ulPeriIdReg);
                if (Result != ERR_NO_ERROR)
                    {
                    return Result;
                    }

                Result = GetDevTypeRegValue(ulCompBase, &ulDeviceTypeReg);
                if (Result != ERR_NO_ERROR)
                    {
                    return Result;
                    }

                ptyRomTableEntry->ulCompType = ulDeviceTypeReg;
                ulNumEntries = ulNumEntries + 1;
                }
            else if (END_OF_ROM_TABLE == ulRomTableEntry)
                {
                ptyCoresightComp->ulNumEntries = ulNumEntries;
               
                return Result;
                }
            else
                {
                ptyRomTableEntry->bEntryPresent  = (BOOL)DBG_ENTRY_NOT_PRESENT;
                ulNumEntries = ulNumEntries + 1;
                } 

            /* ROM Table entry format can be of two types. 32 bit or 8 bit.
            In an 32-bit ROM Table each entry is a single 32-bit word but
            in an 8-bit ROM Table each entry is four 8-bit words, stored LSB-first.
            So in the 8-bit format each entry is 4 times longer.
            Also in Maximum coresight components is defined for 32 bit format. In the
            8 bit format Maximum coresight components (MAX_CORESIGHT_COMP / 4). 
            So for each entry Compnent index (ulCompIndex) is increment by 1
            for 32 bit format and 4 for 8 bit format. */
            if (FORMAT_32_BIT == (ulRomTableEntry & ROM_TABLE_FORMAT))
                {
                DLOG(("32-Bit Format"));
                ulRomTableEntryAddr = ulRomTableEntryAddr + LENGTH_32_BIT_FORMAT;
                ulCompIndex = ulCompIndex + 1;
                }

            else
                {
                DLOG(("8-Bit Format"));
                ulRomTableEntryAddr = ulRomTableEntryAddr + LENGTH_8_BIT_FORMAT;
                ulCompIndex = ulCompIndex + 4;
                }      

            ptyRomTableEntry = ptyRomTableEntry + 1;
            }         
        while(ulCompIndex < MAX_CORESIGHT_COMP);
    }   
    
    return Result;    
}
#endif

/****************************************************************************
Function: GetBank15RegValue
Engineer: Amerdas D.K.
Input   : unsigned long  ulApIndex - Access port register index. 
Output  : unsigned long - error code for commands (ERR_xxx)
Description: This function is used to read the Bank 15 registers. Bank 15 registers
              : are IDR register, Debug base register and Configuration register.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT GetBank15RegValue(unsigned long ulApIndex,
                         unsigned long* pulData)
{

    unsigned long ulReadData;
    unsigned long ulWriteData;

    RESULT Result = ERR_NO_ERROR;

    TyBank15regs* ptyBank15regs;

    DLOG((""));
    DLOG(("Function:GetBank15RegValue:Entry"));
    DLOG((""));

    /* For replay data */
    ptyBank15regs = (TyBank15regs*)((char*)pulData);

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* First read the IDR, BASE and Configuration registers. These registers are
    part of bank 15 of the MEM-AP registers. */
    ulWriteData = APSEL_SHIFT(ulApIndex) + AP_BANK_15;
    Result = CoreSight_WriteDapReg(JTAG_DP_AP_SELECT_REG, ulWriteData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Read the IDR register value. */
    Result = CoreSight_ReadDapReg(MEM_AP_IDR_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    ptyBank15regs->ulIdrRegValue = ulReadData;

    DLOG(("	ptyBank15regs->ulIdrRegValue    : 0x%08x",ptyBank15regs->ulIdrRegValue));

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Read debug base register value. */
    Result = CoreSight_ReadDapReg(MEM_AP_DBG_BASE_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    ptyBank15regs->ulDbgBaseRegValue = ulReadData;

    DLOG(("	ptyBank15regs->ulDbgBaseRegValue: 0x%08x",ptyBank15regs->ulDbgBaseRegValue));

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Read configuration register value. */
    Result = CoreSight_ReadDapReg(MEM_AP_CFG_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    ptyBank15regs->ulConfiRegValue = ulReadData;

    DLOG(("	ptyBank15regs->ulConfiRegValue  : 0x%08x",ptyBank15regs->ulConfiRegValue));

    DLOG(("Function:GetBank15RegValue:Exit"));
    DLOG((""));

    return Result;
}

/****************************************************************************
Function:   ReadRomTableEntries
Engineer:   Jeenus C.K.
Input   :   unsigned long ulBaseAddr - Base address of the ROM table.
          :   unsigned long ulApIndex - Access port number.
          :   TyCoresightComp* ptyCoresightComp - To return the Coresight components.  
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: This function is used to read the ROM table enties. This function 
              : also returns IDR register, debug base register address and 
              : Configuration register values.  
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT ReadRomTableEntries(unsigned long ulBaseAddr,
                           unsigned long ulApIndex,
                           unsigned long* ulNumData,     /* Tx */
                           unsigned long* pulNumEntries, /* Tx */
                           unsigned long* pulData)       /* Tx */
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulDatatoScan;
    unsigned long ulWriteData;

    unsigned long ulEntryoffset;
    unsigned long ulSize;

    /* To Read Debug compnent */
    TyDebugComp  *ptyDebugComp;

    /* To Read ROM table Entries */
    TyROMTable tyROMTable;

    DLOG(("Function:ReadRomTableEntries"));
    DLOG((""));

    DLOG(("	ulBaseAddr  : 0x%08x",ulBaseAddr));
    DLOG(("	ulApIndex   : 0x%08x",ulApIndex));
    DLOG((""));

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    ulDatatoScan = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR;
    Result = CoreSight_WriteDapReg(JTAG_DP_CTRL_STAT_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Select Bank 0 register. */
    ulWriteData = APSEL_SHIFT(ulApIndex) + AP_BANK_0;
    Result = CoreSight_WriteDapReg(JTAG_DP_AP_SELECT_REG, ulWriteData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    {        
        unsigned long ulReadData;
        Result = CoreSight_ReadDapReg(MEM_AP_CSW_REG, &ulReadData);
        if (Result != ERR_NO_ERROR)
        {
            return Result;
        }

        gulCswProtVal = ulReadData & CSW_PROT;
    }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Write to the CSW register with CSW prot value, debug software enable,
    access size as 32 bit and auto increment single enabled. */
    ulWriteData = ((gulCswProtVal & CSW_PROT) |
                   DBG_SW_ENABLE  |
                   ACCESS_SIZE_32 |
                   INC_SINGLE);    

    Result = CoreSight_WriteDapReg(MEM_AP_CSW_REG, ulWriteData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    /* Take the base address */
    tyROMTable.ulbaseAddress = (ulBaseAddr & 0x7FFFF000);

    DLOG(("	tyROMTable.ulbaseAddress   : 0x%08x",tyROMTable.ulbaseAddress));

    /* Next check that base address  is of ROM Table base or not. 
    For this Read the component ID of the Base Address. Then check that
    Compnent ID class is ROM_TABLE_COMP or not. If not ROM Table return error.*/

    Result = GetCompIdValue( tyROMTable.ulbaseAddress,
                             COMPONENT_ID1_OFFSET,
                             &tyROMTable.tyCompIDVal[1].ulCompIDRegVal);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    if (ROM_TABLE != (tyROMTable.tyCompIDVal[1].ucVal >> 4))
    {
        return ERR_ARM_NOT_VALID_ROM_ADDR;
    }

    /* Proceed if a valid ROM table found */

    /* Reply buffer */
    ptyDebugComp = (TyDebugComp*)((char*)pulData);
    /* Size to increment reply buffer */
    ulSize = sizeof(TyDebugComp);
    /* In long format */
    ulSize = (ulSize/4);
    /* Reply data count initially '0' */
    *ulNumData     = 0x0;
    /* Num valid entries */
    *pulNumEntries = 0x0;
    /* Now read each entries */
    ulEntryoffset  = 0x0;

    do
    {
        /* 1. Read the ROM Table entry for the component, and extract the Address offset for the component. */
        /* 	The Address offset is bits [31:12] of the ROM Table entry.													*/
        Result = GetRomTableEntry((tyROMTable.ulbaseAddress | ulEntryoffset),
                                  &tyROMTable.tyEntry.ulTableEntry);
        if (Result != ERR_NO_ERROR)
        {
            return Result;
        }

        DLOG(("============================================================================"));
        DLOG(("TyROMTable.TyEntry[0x%08x]===========================>: 0x%08x",ulEntryoffset,tyROMTable.tyEntry.ulTableEntry));
        DLOG(("============================================================================"));
        DLOG((""));

        if (ENTRY_PRESENT == tyROMTable.tyEntry.tyEntryFormat.uiEntryPresent)
        {
            DLOG(("	tyROMTable.tyEntry.tyEntryFormat.uiEntryPresent: 0x%08x",tyROMTable.tyEntry.tyEntryFormat.uiEntryPresent));
            DLOG(("	tyROMTable.tyEntry.tyEntryFormat.uiFormat      : 0x%08x",tyROMTable.tyEntry.tyEntryFormat.uiFormat));
            DLOG(("	tyROMTable.tyEntry.tyEntryFormat.iAddOffset   : 0x%08x",tyROMTable.tyEntry.tyEntryFormat.iAddOffset));

            /*
            ptyDebugComp->ulCompBase =  (tyROMTable.ulbaseAddress +
                                        (tyROMTable.tyEntry.tyEntryFormat.iAddOffset << 12));
            */

            ptyDebugComp->ulCompBase =  (tyROMTable.ulbaseAddress +
                                         (int)(tyROMTable.tyEntry.ulTableEntry & 0xFFFFF000));


            /* ptyDebugComp->ulCompBase = (ptyDebugComp->ulCompBase & 0x7FFFFFFF); */

            /* 2. Use the Address offset, together with the base address of the ROM Table, to calculate the base address 		*/
            /* 	of the component. The component base address is:  															               */
            /* 	Component_Base_Address = ROM_Base_Address + (Address_Offset << 12) 											         */
            /* 	When performing this calculation, remember that the Address_Offset value might be a two�s 					   */
            /* 	complement negative value. 																					                  */
            /* 	Component_Base_Address is the start address of the final 4KB block of the address space for the 			   */
            /* 	component. 																									                        */


            Result = GetCompIdValue(ptyDebugComp->ulCompBase,
                                    COMPONENT_ID0_OFFSET,
                                    &ptyDebugComp->tyCompID[0].ulCompIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetCompIdValue(ptyDebugComp->ulCompBase,
                                    COMPONENT_ID1_OFFSET,
                                    &ptyDebugComp->tyCompID[1].ulCompIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetCompIdValue(ptyDebugComp->ulCompBase,
                                    COMPONENT_ID2_OFFSET,
                                    &ptyDebugComp->tyCompID[2].ulCompIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetCompIdValue(ptyDebugComp->ulCompBase,
                                    COMPONENT_ID3_OFFSET,
                                    &ptyDebugComp->tyCompID[3].ulCompIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            /* 3. Read the Peripheral ID4 Register for the component. The address of this register is:			*/
            /* 	Peripheral_ID4_address = Component_Base_Address + 0xFD0													*/

            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID0_OFFSET,
                                    &ptyDebugComp->tyPeriID[0].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID1_OFFSET,
                                    &ptyDebugComp->tyPeriID[1].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID2_OFFSET,
                                    &ptyDebugComp->tyPeriID[2].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID3_OFFSET,
                                    &ptyDebugComp->tyPeriID[3].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID4_OFFSET,
                                    &ptyDebugComp->tyPeriID[4].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }


            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID5_OFFSET,
                                    &ptyDebugComp->tyPeriID[5].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }


            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID6_OFFSET,
                                    &ptyDebugComp->tyPeriID[6].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }


            Result = GetPeriIdValue(ptyDebugComp->ulCompBase,
                                    PERIPHERAL_ID7_OFFSET,
                                    &ptyDebugComp->tyPeriID[7].ulperiIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            Result = GetDevTypeRegValue(ptyDebugComp->ulCompBase, 
                                        &ptyDebugComp->tyDevType.ulDeviceIDRegVal);
            if (Result != ERR_NO_ERROR)
            {
                return Result;
            }

            /* 4. 	Extract the 4KB count field, bits [7:4], from the value of the Peripheral ID4 Register.					*/
            /* 5. 	Use the 4KB count value to calculate the start address of the address space for the component.			*/
            /* 		If the 4KB count field is b0000, indicating a count value of 1, the address space for the component   */
            /* 		starts at the Component_Base_Address obtained at stage 2.												         */

            /* Component start address in reply packet */
            ptyDebugComp->ulCompStart = ptyDebugComp->ulCompBase - (0x1000*((ptyDebugComp->tyPeriID[4].ucVal & 0xF0) >> 4));

            /* Mask the MSB */
            /*ptyDebugComp->ulCompStart = (ptyDebugComp->ulCompStart & 0x7FFFFFFF);*/

            /* Number of 4KB blocks for the component */
            ptyDebugComp->ulcompSize = (((ptyDebugComp->tyPeriID[4].ucVal & 0xF0) >> 4) + 1);               /* Number of 4KB Blocks */

            switch ((ptyDebugComp->tyCompID[1].ulCompIDRegVal >> 4))
            {
            case GEN_VER_COMP:
                break;

            case ROM_TABLE:
                break;

            case DEBUG_COMP:
                /* Major type */
                ptyDebugComp->ulMajorType = (ptyDebugComp->tyDevType.ucVal & DEVTYPE_MAJOR_MASK);

                /* Sub type */
                ptyDebugComp->ulSubType   = ((ptyDebugComp->tyDevType.ucVal & DEVTYPE_SUBTYPE_MASK)>>DEVTYPE_SUBTYPE_SHIFT);
                break;

            case PERI_TEST_BLOCK:
                break;

            case DESS_COMP:
                break;

            case GEN_IP_COMP:
                break;

            case PRICEL_COMP:
                break;  

            default:
                break;
            }
            DLOG(("	tyDebugComp.ulCompStart: 0x%08x",ptyDebugComp->ulCompStart));
            DLOG(("	tyDebugComp.ulCompBase : 0x%08x",ptyDebugComp->ulCompBase));
            DLOG(("	tyDebugComp.ulcompSize : 0x%08x",ptyDebugComp->ulcompSize));
            DLOG((""));

            DLOG(("	ptyDebugComp->tyCompIDVal[0].ulCompIDRegVal: 0x%08x",ptyDebugComp->tyCompID[0].ulCompIDRegVal));
            DLOG(("	ptyDebugComp->tyCompIDVal[1].ulCompIDRegVal: 0x%08x",ptyDebugComp->tyCompID[1].ulCompIDRegVal));
            DLOG(("	ptyDebugComp->tyCompIDVal[2].ulCompIDRegVal: 0x%08x",ptyDebugComp->tyCompID[2].ulCompIDRegVal));
            DLOG(("	ptyDebugComp->tyCompIDVal[3].ulCompIDRegVal: 0x%08x",ptyDebugComp->tyCompID[3].ulCompIDRegVal));
            DLOG((""));

            DLOG(("	ptyDebugComp->tyPeriID[0].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[0].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[1].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[1].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[2].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[2].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[3].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[3].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[4].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[4].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[5].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[5].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[6].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[6].ulperiIDRegVal));
            DLOG(("	ptyDebugComp->tyPeriID[7].ulperiIDRegVal: 0x%08x",ptyDebugComp->tyPeriID[7].ulperiIDRegVal));
            DLOG((""));
            DLOG(("	ptyDebugComp->tyDevType   : 0x%08x",ptyDebugComp->tyDevType));
            DLOG((""));
            DLOG(("	ptyDebugComp->ulMajorType : 0x%08x",ptyDebugComp->ulMajorType));
            DLOG(("	ptyDebugComp->ulSubType   : 0x%08x",ptyDebugComp->ulSubType));

            /* Increment the data count */
            *ulNumData  = (*ulNumData  + ulSize);

            /* Point to Next Component in reply packet */
            ptyDebugComp = (ptyDebugComp + 1);

            /* Number of Entries */
            *pulNumEntries = (*pulNumEntries + 1);
        }

        /* ROM Table entry format can be of two types. 32 bit or 8 bit.
            In an 32-bit ROM Table each entry is a single 32-bit word but
            in an 8-bit ROM Table each entry is four 8-bit words, stored LSB-first.
            So in the 8-bit format each entry is 4 times longer.
            Also in Maximum coresight components is defined for 32 bit format. In the
            8 bit format Maximum coresight components (MAX_CORESIGHT_COMP / 4). 
            So for each entry Compnent index (ulCompIndex) is increment by 1
            for 32 bit format and 4 for 8 bit format. */

        if (1 == tyROMTable.tyEntry.tyEntryFormat.uiFormat)
        {
            ulEntryoffset = ulEntryoffset + 4;
        }

        else
        {
            ulEntryoffset = ulEntryoffset + 1;
        }      
    }
    while (tyROMTable.tyEntry.ulTableEntry > 0x0);


    DLOG(("Exit..."));

    return Result;    
}

#if 0
/****************************************************************************
Function: GetBank15RegValue
Engineer: Jeenus C.K.
Input   : unsigned long* pulIdrRegValue - To return the IDR register value. 
        : unsigned long* pulDbgBaseRegValue - To return the base address register 
        : unsigned long* pulConfigRegValue - To return the configuration register.
        : unsigned long  ulApIndex - Access port register index. 
Output  : unsigned long - error code for commands (ERR_xxx)
Description: This function is used to read the Bank 15 registers. Bank 15 registers
           : are IDR register, Debug base register and Configuration register.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT GetBank15RegValue(
    unsigned long* pulIdrRegValue,
    unsigned long* pulDbgBaseRegValue,
    unsigned long* pulConfigRegValue,
    unsigned long  ulApIndex)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData;
    unsigned long ulWriteData;

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* First read the IDR, BASE and Configuration registers. These registers are
    part of bank 15 of the MEM-AP registers. */
    ulWriteData = APSEL_SHIFT(ulApIndex) + AP_BANK_15;
    Result = CoreSight_WriteDapReg(JTAG_DP_AP_SELECT_REG, ulWriteData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read the IDR register value. */
    Result = CoreSight_ReadDapReg(MEM_AP_IDR_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    *pulIdrRegValue = ulReadData;

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read debug base register value. */
    Result = CoreSight_ReadDapReg(MEM_AP_DBG_BASE_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    *pulDbgBaseRegValue = ulReadData;

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Read configuration register value. */
    Result = CoreSight_ReadDapReg(MEM_AP_CFG_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }
    *pulConfigRegValue = ulReadData;

    return Result;

}
#endif
/****************************************************************************
Function: GetRomTableEntry
Engineer: Jeenus C.K.
Input   : unsigned long ulRomTableEntryAddr - The address of the ROM table entry.  
        : unsigned long* pulRomTableEntry  - To retiurn the value of ROM table 
        :                                  - entry. 
Output  : unsigned long - error code for commands (ERR_xxx)
Description: This function is used to read the ROM table entries.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT GetRomTableEntry(
    unsigned long ulRomTableEntryAddr,
    unsigned long* pulRomTableEntry)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData;

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Set the Rom table entry address */
    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulRomTableEntryAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* ROM table entry can be of Two types. 32-bit format and 8 bit format.
    In a 32-bit ROM Table an entry is a single 32-bit word but 
    in an 8-bit ROM Table an entry is four 8-bit words, stored LSB-first.
    When reading a location in an 8-bit format ROM Table, bits [31:8] of
    the access Read As Zero. Ref ARM IHI0031A page number 225.*/
    Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    if (FORMAT_8_BIT == (ulReadData & ROM_TABLE_FORMAT))
        {
        unsigned long ulReadDataTemp;

        DLOG(("GetRomTableEntry - 8-Bit Format"));
        ulReadDataTemp = ulReadData & 0xFF;

        Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulReadDataTemp |= ((ulReadData & 0xFF) << 8);

        Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulReadDataTemp |= ((ulReadData & 0xFF) << 16);

        Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
        if (Result != ERR_NO_ERROR)
            {
            return Result;
            }

        ulReadDataTemp |= ((ulReadData & 0xFF) << 24);
        ulReadData = ulReadDataTemp;
        }
    *pulRomTableEntry = ulReadData;

    return Result;

}

/****************************************************************************
Function: GetCompIdValue
Engineer: Jeenus C.K.
Input   : unsigned long ulCompBaseAddr - Component base address.
          : unsigned long* pulCompIdVal  - To return the component class.
Output  : unsigned long - error code for commands (ERR_xxx)
Description: Reads component ID's. From this component ID is identified and 
              : will return.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT GetCompIdValue(unsigned long ulCompBaseAddr,
                      unsigned long ulRegOffset,
                      unsigned long* pulCompIdVal)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData;
    unsigned long ulCompIdAddr;

    /* Set the Component ID Base address */
    ulCompIdAddr = (ulCompBaseAddr + ulRegOffset); 

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }
    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulCompIdAddr);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    *pulCompIdVal = ulReadData;

    //DLOG(("GetCompIdValue():0x%08x -> 0x%08x",ulCompIdAddr,*pulCompIdVal));

    return Result;
}

/****************************************************************************
Function: GetPeriIdValue
Engineer: Jeenus C.K.
Input   : unsigned long ulCompBaseAddr - The componentbase address.
          : unsigned long* pulPeriIdVal - To return the Peripheral type which
          :                             - is identified using the peripheral id
          :                             - values.    
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: Inorder to identify the peripheral id type.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT GetPeriIdValue( unsigned long ulCompBaseAddr,
                       unsigned long ulRegOffset,
                       unsigned long* pulPeriIdVal)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData;
    unsigned long ulPeriIdAddr;

    ulPeriIdAddr = (ulCompBaseAddr + ulRegOffset);

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulPeriIdAddr);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
    {
        return Result;
    }

    *pulPeriIdVal = ulReadData;

    //DLOG(("GetPeriIdValue():0x%08x -> 0x%08x",ulPeriIdAddr,*pulPeriIdVal));

    return Result;
}

/****************************************************************************
Function: GetDevTypeRegValue
Engineer: Jeenus C.K.
Input   : unsigned long ulCompBaseAddr - The componentbase address.
        : unsigned long* pulPeriIdVal - To return the Device Type Register value.    
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: Inorder to read the Device type register value.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT GetDevTypeRegValue(
    unsigned long ulCompBaseAddr,
    unsigned long* pulDevTypeRegVal)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData;
    unsigned long ulDevTypeAddr;    
    
    ulDevTypeAddr = ulCompBaseAddr + DEV_TYPE_ID_OFFSET;
    DLOG2(("Device Type Register address: 0x%08x", ulDevTypeAddr));    

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulDevTypeAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("Device Type Register : 0x%08x", ulReadData));
    *pulDevTypeRegVal = ulReadData;
            

    return Result;
}

/****************************************************************************
Function: Coresight_SelectBank
Engineer: Jeenus C.K.
Input   : unsigned long  ulBankNo - Bank to be selected  
        : unsigned long ulApindex - Access port to be selected. 
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: Inorder to change the bank.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT Coresight_SelectBank(unsigned long ulBankNo , 
                            unsigned long ulApindex)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulWriteData;    

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulWriteData = APSEL_SHIFT(ulApindex) + ulBankNo;
    Result = CoreSight_WriteDapReg(JTAG_DP_AP_SELECT_REG, ulWriteData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }    

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function: CoreSight_ReadDebugReg
Engineer: Jeenus C.K.
Input   : unsigned long ulMemApIndex     - The memory AP index.
        : unsigned long* pulDebugRegVal  - To return the debug register value.    
Output  :   unsigned long - error code for commands (ERR_xxx)
Description: Inorder to identify the peripheral id type.
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
RESULT CoreSight_ReadDebugReg(unsigned long ulMemApIndex,
                              unsigned long ulDebugBaseAddr,
                              unsigned long* pulDebugRegVal)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulReadData;

    DLOG(("CoreSight_ReadDebugReg"));
    DLOG(("ulDebugBaseAddr = 0x%08x", ulDebugBaseAddr));
    DLOG(("ulMemApIndex = 0x%08x", ulMemApIndex));

    Result = Coresight_SelectBank(AP_BANK_0, tyCoreSightParam.ulDbgAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Inorder to select APACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = CoreSight_WriteDapReg(MEM_AP_TAR_REG, ulDebugBaseAddr);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    Result = CoreSight_ReadDapReg(MEM_AP_DRW_REG, &ulReadData);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    DLOG(("ulReadData = 0x%08x", ulReadData));
    *pulDebugRegVal = ulReadData;

    return Result;
}

/****************************************************************************
Function    : CoreSight_MaxTARBlockSize
Engineer    : Jeenus C.K.
Input       : unsigned long ulMaxTarIncSize - Maximum increment size of the 
            :                               - TAR register.
            : unsigned long ulStartAddr - Start address of memory
Output      : unsigned long - error code for commands (ERR_xxx)
Description : Returns the largest block size starting at address ulStartAddr
            : that does not cross a TAR block size allignment boundary. 
Date            Initials    Description
10-Mar-2010     JCK         Initial
****************************************************************************/
void CoreSight_MaxTARBlockSize (
    unsigned long ulMaxTarIncSize,
    unsigned long ulStartAddr,
    unsigned long* pulMaxSize)
{
    /* As per ARM debug interface version 5, Automatic address increment is only 
    guaranteed to operate on the bottom 10-bits of the address held in the TAR. 
    Auto address incrementing of bit [10] and beyond is IMPLEMENTATION DEFINED.
    This means that auto address incrementing at a 1KB boundary is IMPLEMENTATION
    DEFINED. */
    /* For example, if TAR[31:0] is set to 0x14A4, and the access size is 
    word, successive accesses to the DRW increment TAR to 0x14A8, 0x14A, and so 
    on, up to the end of the 1KB range at 0x17FC. */
    /* Logic used here is, if  ulMaxTarIncSize is 1KB and start address is
    0x14A4 then TAR will automatically increment upto 0x17FC.
    So in case of Word access we have to limit the DRW access to count 0x358.
    Binary equlaent of 0x14A4 is 1 0100 1010 0100. Since ulMaxTarIncSize is 1KB
    10th bit '1' will not modify. */
    
    *pulMaxSize = (ulMaxTarIncSize - ((ulMaxTarIncSize - 1) & ulStartAddr )) >> 2;    
}

/****************************************************************************
Function    : CoreSight_ClearStickyError
Engineer    : Jeenus C.K.
Input       : void
Output      : unsigned long - error code for commands (ERR_xxx)
Description : To clear the sticky error flag.
Date            Initials    Description
01-June-2010       JCK       Initial
****************************************************************************/
RESULT CoreSight_ClearStickyError (void)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulDatatoScan;

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_DPACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulDatatoScan = CSYS_PWRUP_REQ | CDBG_PWRUP_REQ | STICKYERR;
    Result = CoreSight_WriteDapReg(JTAG_DP_CTRL_STAT_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    /* Inorder to select DPACC registers. */
    Result = JtagScanIR(&gulARM_JTAG_APACC, NULL);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function    : CoreSight_SetupAccessPort
Engineer    : Jeenus C.K.
Input       : unsigned long ulAccessSize - Access size of the memory.   
Output      : unsigned long - error code for commands (ERR_xxx)
Description : To initialize the CSW control register and to AP select register. 
Date            Initials    Description
01-June-2010       JCK       Initial
****************************************************************************/
RESULT CoreSight_SetupAccessPort (unsigned long ulAccessSize)
{
    RESULT Result = ERR_NO_ERROR;
    unsigned long ulDatatoScan;

    Result = Coresight_SelectBank (AP_BANK_0 , tyCoreSightParam.ulMemAccessApIndex);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    ulDatatoScan = gulCswProtVal | DBG_SW_ENABLE | ulAccessSize | INC_SINGLE;
    Result = CoreSight_WriteDapReg(MEM_AP_CSW_REG, ulDatatoScan);
    if (Result != ERR_NO_ERROR)
        {
        return Result;
        }

    return Result;
}

/****************************************************************************
Function    : OMAP_ActivateDebugPower
Engineer    : Jeenus C.K.
Input       : void   
Output      : unsigned long - error code for commands (ERR_xxx)
Description : In case of TI DAP support some TCK tycles are required to activate
            : the DEBUG power domain. 
Date            Initials    Description
01-June-2010       JCK       Initial
****************************************************************************/
unsigned long OMAP_ActivateDebugPower(void)
{
    unsigned long Result = ERR_NO_ERROR;
    unsigned short usReg;
    unsigned long  ulTmsTemp;
    unsigned long  ulSavedMcDRCount;

    DLOG2(("OMAP_ActivateDebugPower\r\n"));

    /* Save current multi core settings */
    ulSavedMcDRCount = get_wvalue(JTAG_MCDRC);
    put_wvalue(JTAG_MCDRC, 0);
    
    /* some TCK tycles are required to activate the DEBUG power domain. */
    ulTmsTemp = TMS_PATTERN_16_ZEROS; 

    put_wvalue(JTAG_SPARAM_TMS(0), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(1), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(2), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(3), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(4), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(5), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(6), ulTmsTemp);
    put_wvalue(JTAG_SPARAM_TMS(7), ulTmsTemp);

    put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(1,0));    
    put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(4), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(5), JTAG_SPARAM_CNT_DR(1,0));    
    put_wvalue(JTAG_SPARAM_CNT(6), JTAG_SPARAM_CNT_DR(1,0));
    put_wvalue(JTAG_SPARAM_CNT(7), JTAG_SPARAM_CNT_DR(1,0));

    put_wvalue(JTAG_TDI_BUF(0), 0x0);
    put_wvalue(JTAG_TDI_BUF(1), 0x0); 
    put_wvalue(JTAG_TDI_BUF(2), 0x0); 
    put_wvalue(JTAG_TDI_BUF(3), 0x0); 
    put_wvalue(JTAG_TDI_BUF(4), 0x0);
    put_wvalue(JTAG_TDI_BUF(5), 0x0); 
    put_wvalue(JTAG_TDI_BUF(6), 0x0); 
    put_wvalue(JTAG_TDI_BUF(7), 0x0); 

    usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
    put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE)); // enable AutoScan

    put_hvalue(JTAG_JASR, 0xFF);

    while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
    put_hvalue(JTAG_JSCTR, usReg); // disable AutoScan

    /* Restore multi core settings */
    put_wvalue(JTAG_MCDRC, ulSavedMcDRCount);

    return Result;
}
