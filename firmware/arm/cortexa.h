/******************************************************************************
       Module: CortexA.h
     Engineer: Amerdas D K
  Description: This file contains the defenitions for CortexA core 
  Date           Initials    Description
  22-03-2010     ADK         Initial   
******************************************************************************/
#ifndef CORTEXA_H
#define CORTEXA_H

#define CORTEXA_USR_MODE       0x10 /* User             */
#define CORTEXA_FIQ_MODE       0x11 /* FIQ              */
#define CORTEXA_IRQ_MODE       0x12 /* IRQ              */
#define CORTEXA_SVR_MODE       0x13 /* Supervisor       */
#define CORTEXA_ABT_MODE       0x17 /* Abort            */
#define CORTEXA_UND_MODE       0x1B /* Undefined        */
#define CORTEXA_SYS_MODE       0x1F /* System           */
#define CORTEXA_SMO_MODE       0x16 /* Secure Monitor   */


typedef enum
{
    CortexAModeUser,             
    CortexAModeFIQ,             
    CortexAModeIRQ,              
    CortexAModeSupervisor,      
    CortexAModeAbort,            
    CortexAModeUndefined,        
    CortexAModeSystem,           
    CortexAModeSecureMonitor    
}CortexAMode;

typedef struct
{
    unsigned long ulOp1;
    unsigned long ulCRn;
    unsigned long ulCRm;
    unsigned long ulOp2;
} Coprocessor;

#define CORTEXA_DTR_NON_BLOCK      (0x00000000 << 20)
#define CORTEXA_DTR_STALL          (0x00000001 << 20)
#define CORTEXA_DTR_FAST           (0x00000002 << 20)

#define CORTEXA_CPSR_T_BIT        0x00000020

/* Debug Memory-Mapped Registers of Cortex-A8 */
#define CORTEXA_DIDR_OFSET                      (0x00)              /* Cortex-A8 Debug Memory-Mapped Register DIDR  */
#define CORTEXA_WFAR_OFSET                      (0x18)              /* Cortex-A8 Debug Memory-Mapped Register WFAR  */
#define CORTEXA_VCR_OFSET                       (0x1C)              /* Cortex-A8 Debug Memory-Mapped Register VCR   */
#define CORTEXA_ECR_OFSET                       (0x24)              /* Cortex-A8 Debug Memory-Mapped Register ECR   */
#define CORTEXA_DSCCR_OFSET                     (0x28)              /* Cortex-A8 Debug Memory-Mapped Register DSCCR */
#define CORTEXA_DTRRX_OFSET                     (0x80)              /* Cortex-A8 Debug Memory-Mapped Register DTRRX */
#define CORTEXA_ITR_OFSET                       (0x84)              /* Cortex-A8 Debug Memory-Mapped Register ITR   */
#define CORTEXA_DSCR_OFSET                      (0x88)              /* Cortex-A8 Debug Memory-Mapped Register DSCR  */
#define CORTEXA_DTRTX_OFSET                     (0x8C)              /* Cortex-A8 Debug Memory-Mapped Register DTRTX */
#define CORTEXA_DRCR_OFSET                      (0x90)              /* Cortex-A8 Debug Memory-Mapped Register DSCR  */

#define CORTEXA_BVR_OFSET                       (0x100)             /* Cortex-A8 Debug Memory-Mapped Register BVR   */
#define CORTEXA_BCR_OFSET                       (0x140)             /* Cortex-A8 Debug Memory-Mapped Register BCR   */

#define CORTEXA_WVR_OFSET                       (0x180)             /* Cortex-A8 Debug Memory-Mapped Register WVR   */
#define CORTEXA_WCR_OFSET                       (0x1C0)             /* Cortex-A8 Debug Memory-Mapped Register BCR   */

/* Device Power Down and Reset Status Register */
#define CORTEXA_PRSR_OFFSET                     (0x314)  

/* Management registers Cortex-A8 */
#define CORTEXA_LOCKACCESS_OFSET                (0xFB0)              /* Cortex-A8 Lock Access Register */

/* Cortex-A8 DRCR bits */
#define CORTEXA_DRCR_CORE_HALT             (0x00000001 << 0)       /* Halt request */
#define CORTEXA_DRCR_CORE_RESTART          (0x00000001 << 1)       /* Restart request */
#define CORTEXA_DRCR_CLEAR_STICKY_EXEPT    (0x00000001 << 2)       /* Clear sticky exceptions. Writing a 1 to this bit clears DSCR[8:6] to b000 */
#define CORTEXA_DRCR_CLEAR_STICKY_PIPELINE (0x00000001 << 3)       /* Clear sticky pipeline advance. Writing a 1 to this bit clears DSCR[25] to 0 */

/* Cortex-A8 DSCR bits */
#define CORTEXA_DSCR_CORE_HALTED		    (0x00000001 << 0)       /* Core halted bit:
                                                                    0 = The processor is in normal state. This is the reset value.
                                                                    1 = The processor is in debug state.
                                                                    */
#define CORTEXA_DSCR_CORE_RESTARTED 	    (0x00000001 << 1)       /* Core restarted bit:
                                                                    0 = The processor is exiting debug state.
                                                                    1 = The processor has exited debug state. This is the reset value.
                                                                  */

#define CORTEXA_DSCR_ENTRY_MASK            (0x0000003C)            /* Method of entry bits */

#define CORTEXA_DSCR_ENTRY_HALTING              (0x0 << 2)      /* DRCR[0] halting debug event occurred, reset value */
#define CORTEXA_DSCR_ENTRY_BKPT                 (0x1 << 2)      /* Breakpoint occurred                               */
#define CORTEXA_DSCR_ENTRY_DBGRQ                (0x4 << 2)      /* DBGRQ halting debug event occurred                */
#define CORTEXA_DSCR_ENTRY_BKPT_INST            (0x3 << 2)      /* BKPT instruction occurred                         */
#define CORTEXA_DSCR_ENTRY_VECTOR_CATCH         (0x5 << 2)      /* Vector catch occurred                             */
#define CORTEXA_DSCR_ENTRY_OS_UNLOCK            (0x8 << 2)      /* OS unlock catch occurred                          */
#define CORTEXA_DSCR_ENTRY_WATCHPOINT           (0xA << 2)      /* Precise watchpoint occurred                       */

#define CORTEXA_DSCR_STICKY_PRE_ABORT      (0x00000001 << 6)       /* Sticky imprecise Data Abort bit:
                                                                    0 = no imprecise Data Aborts occurred since the last time this bit was cleared, reset value
                                                                    1 = an imprecise Data Abort occurred since the last time this bit was cleared.
                                                                    */
#define CORTEXA_DSCR_STICKY_IMPRE_ABORT    (0x00000001 << 7)       /* Sticky precise Data Abort bit:
                                                                    0 = no precise Data Abort occurred since the last time this bit was cleared, reset value
                                                                    1 = a precise Data Abort occurred since the last time this bit was cleared.
                                                                    */


#define CORTEXA_DSCR_EXT_INT_EN 		    (0x00000001 << 13)      /* Execute ARM instruction enable bit.
                                                                    0 = disabled
                                                                    1 = enabled
                                                                    */

#define CORTEXA_DSCR_HALT_DBG_MODE		    (0x00000001 << 14)      /* The Halting debug-mode enable bit
                                                                    0 = Halting debug-mode disabled
                                                                    1 = Halting debug-mode enabled
                                                                    */

#define CORTEXA_DSCR_MON_DBG_MODE 		    (0x00000001 << 15)      /* The Monitor debug-mode enable bit
                                                                    0 = Monitor debug-mode disabled
                                                                    1 = Monitor debug-mode enabled
                                                                    */

#define CORTEXA_DSCR_DIS_IMP_ABORT 		(0x00000001 << 19)      /* Discard imprecise abort
                                                                    0 = imprecise Data Aborts not discarded
                                                                    1 = imprecise Data Aborts discarded.
                                                                    */

#define CORTEXA_DSCR_DTR_ACC_MASK          (0x00000003 << 20)      /* DTR access mode [21:20] DTR access mode */


#define CORTEXA_DSCR_INSTR_COMP 		    (0x00000001 << 24)      /* The latched InstrCompl flag
                                                                    0 = the processor is currently executing an instruction fetched from the ITR Register
                                                                    1 = the processor is not currently executing an instruction fetched from the ITR Register
                                                                    */ 

#define CORTEXA_DSCR_DTR_TX_FULL 		    (0x00000001 << 29)      /* DTRTXfull The DTRTXfull flag:
                                                                    0 = DTRTX empty
                                                                    1 = DTRTX full
                                                                    */

#define CORTEXA_DSCR_DTR_RX_FULL 		    (0x00000001 << 30)      /* DTRRXfull The DTRRXfull flag:
                                                                    0 = DTRRX empty
                                                                    1 = DTRRX full
                                                                    */
#define DSCCR_NOT_WRITE_TRU           (1 << 2)

/* Cache size selection register definitions */
#define CSSR_DATA_CACHE              (0x00000000 << 0)
#define CSSR_INSTRUCTION_CACHE       (0x00000001 << 0)
#define CSSR_LEVEL_1_CACHE           (0x00000000 << 1)
#define CSSR_LEVEL_2_CACHE           (0x00000001 << 1)

/* Cache size identification register definitions */
#define CSIR_NUMSETS_SHIFT      13
#define CSIR_NUMSETS_MASK       ( 0x7FFF << CSIR_NUMSETS_SHIFT )
#define CSIR_NUMWAYS_SHIFT      3
#define CSIR_NUMWAYS_MASK       ( 0x3FF << CSIR_NUMWAYS_SHIFT )

/* Set Way format cache operation format */
#define SETWAY_LEVEL_SHIFT      1
#define SETWAY_LEVEL1           ( 0x0 << SETWAY_LEVEL_SHIFT )
#define SETWAY_LEVEL2           ( 0x1 << SETWAY_LEVEL_SHIFT )
/* The value of A and L for Level 1 cache */
#define SETWAY_VAL_A_LEVEL1     2
#define SETWAY_VAL_L_LEVEL1     6
#define SETWAY_SET_SHIFT        SETWAY_VAL_L_LEVEL1
#define SETWAY_WAY_SHIFT        ( 32 - SETWAY_VAL_A_LEVEL1 )

#define CORTEXA_REG_R0  0
/* Co-Processor Definitions */
#define CP15_COPROC     15

/* Move to ARM register from coprocessor
 * CP: Coprocessor number
 * op1: Coprocessor opcode
 * Rd: destination register
 * CRn: first coprocessor operand
 * CRm: second coprocessor operand
 * op2: Second coprocessor opcode
 */
#define ARMV4_5_MRC(CP, op1, op2, CRn, CRm) (0xee100010 | (CRm) | ((op2) << 5) | ((CP) << 8) | ((CRn) << 16) | ((op1) << 21))

/* Move to coprocessor from ARM register
 * CP: Coprocessor number
 * op1: Coprocessor opcode
 * Rd: destination register
 * CRn: first coprocessor operand
 * CRm: second coprocessor operand
 * op2: Second coprocessor opcode
 */
#define ARMV4_5_MCR(CP, op1, op2, CRn, CRm) (0xee000010 | (CRm) | ((op2) << 5) | ((CP) << 8) | ((CRn) << 16) | ((op1) << 21))

#define CACHE_FILL_LENGTH               64
#define ALLIGN_64                       0xFFFFFFC0
/* Control Register Format */
#define MMU_ENABLED                     (0x1 << 0)
#define DATA_CACHE_ENABLED              (0x1 << 2)
#define PROGRAM_PREDICT_ENABLED         (0x1 << 11)
#define INSTRUCTION_CACHE_ENABLED       (0x1 << 12)
#define HIGH_EXCEPTION_VECTOR_ENABLED   (0x1 << 13)

/* Maximum TAR Block size */
/* Maximum  10 bits*/
#define CORTEXA_MAX_TAR_INC_SIZE        (1 << 10)

unsigned long CortexA_ReadDebugRegister(unsigned long ulRegAddr,
                                         unsigned long* pulData);

unsigned long CortexA_WriteDebugRegister(unsigned long ulRegAddr,
                                          unsigned long ulData);

unsigned long CortexA_ExecuteInstruction(unsigned long ulInstruction);

unsigned long CortexA_ReadRegister(unsigned long ulRegNum,
                                    unsigned long* pulData);

unsigned long CortexA_WriteRegister(unsigned long ulRegNum,
                                     unsigned long ulData);

unsigned long CortexA_ReadPC(unsigned long* pulData);

unsigned long CortexA_WritePC(unsigned long pulData);

unsigned long CortexA_ReadCPSR(unsigned long* pulData);

unsigned long CortexA_WriteCPSR(unsigned long ulData);

unsigned long CortexA_ReadSPSR(unsigned long* pulData);

unsigned long CortexA_WriteSPSR(unsigned long ulData);

unsigned long CortexA_ReadCoProcessor(unsigned long* pulInstr, 
                                       unsigned long* pulData);

unsigned long CortexA_WriteCoProcessor(unsigned long* pulInstr, 
                                        unsigned long  ulData);

unsigned long CortexA_ChangeMode(CortexAMode tyCortexAMode);

unsigned long CortexA_LeaveDebugState(unsigned char *bThumbMode, 
                                       unsigned long *pulData);

unsigned long CortexA_EnterDebugState(unsigned long* pulRecData,
                                       unsigned long* pulData);

unsigned long CortexA_ReadRegisters(unsigned long *pulData);

unsigned long CortexA_WriteRegisters(unsigned long *pulData);

unsigned long CortexA_SetDTRAccessMode(unsigned long ulDTRMode);

unsigned long CortexA_CheckForAbort(unsigned char *pucReadData);

unsigned long CortexA_ReadMemoryByte(unsigned long ulAddr,
                                      unsigned long* pulData);

unsigned long CortexA_WriteMemoryByte(unsigned long ulAddr,
                                       unsigned char ucWriteData);

unsigned long CortexA_WriteMemoryByteMultiple(unsigned long ulAddr,
                                               unsigned long ulCnt,
                                               unsigned long* pulData);

unsigned long CortexA_ReadMemoryByteMultiple(unsigned long ulAddr,
                                              unsigned long ulCnt,
                                              unsigned long* pulData);

unsigned long CortexA_WriteMemoryWord(unsigned long ulAddr,
                                       unsigned long* pulData);

unsigned long CortexA_ReadMemoryWord(unsigned long ulAddr,
                                      unsigned long* pulData);

unsigned long CortexA_WriteMemoryWordMultiple(unsigned long ulAddr,
                                               unsigned long ulCnt,
                                               unsigned long* pulData);

unsigned long CortexA_ReadMemoryWordMultiple(unsigned long ulAddr,
                                              unsigned long ulCnt,
                                              unsigned long* pulData);

unsigned long CortexA_StatusProc(unsigned char* pucCoreExecuting,
                                  unsigned char* pucThumbMode,
                                  unsigned char* pucWatchPoint);

unsigned long CortexA_BreakPoint(unsigned short usNumBreakPoint,unsigned long* pulData);

unsigned long CortexA_WatchPoint(unsigned short usNumWatchPoint,unsigned long* pulData);

unsigned long CortexA_ReadRegisterStall(unsigned long ulRegNum,
                                         unsigned long* pulData);

unsigned long CortexA_WriteRegisterStall(unsigned long ulRegNum,
                                          unsigned long ulData);

unsigned long CortexA_InstrExecEnable(void);

unsigned long CortexA_ReadDSCR(unsigned long* pulData);

unsigned long CortexA_ReadWriteDCC(unsigned char *pucDataToWrite,
                                    unsigned long *pulWriteData,
                                    unsigned char *pucDataWriteSuccessful,
                                    unsigned char *pucDataReadFromCore,
                                    unsigned long *pulReadData);
unsigned long CortexA_VectorCatch(unsigned long ulData);

unsigned long CortexA_InvalidateCache(void);

unsigned long CortexA_WriteMemory_MemAp (unsigned long ulStartAddr,
                                   unsigned long ulCount,
                                   unsigned long ulAccessSize,
                                   unsigned long* pulData);

unsigned long CortexA_ReadMemory_MemAp (unsigned long ulStartAddr,
                                   unsigned long ulCount,
                                   unsigned long ulAccessSize,
                                   unsigned long* pulData);
#endif

