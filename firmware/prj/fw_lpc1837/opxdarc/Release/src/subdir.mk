################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Local_OpellaXD/firmware/arc/arccomm.c \
C:/Local_OpellaXD/firmware/arc/arcdw.c \
C:/Local_OpellaXD/firmware/arc/arclayer.c 

OBJS += \
./src/arccomm.o \
./src/arcdw.o \
./src/arclayer.o 

C_DEPS += \
./src/arccomm.d \
./src/arcdw.d \
./src/arclayer.d 


# Each subdirectory must supply rules for building sources it contributes
src/arccomm.o: C:/Local_OpellaXD/firmware/arc/arccomm.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DARCDW -DNDEBUG -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdarc/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/arcdw.o: C:/Local_OpellaXD/firmware/arc/arcdw.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DARCDW -DNDEBUG -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdarc/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/arclayer.o: C:/Local_OpellaXD/firmware/arc/arclayer.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DARCDW -DNDEBUG -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdarc/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


