################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Local_OpellaXD/firmware/common/lpc1837/adc_18xx.c \
C:/Local_OpellaXD/firmware/common/ad/adconv.c \
C:/Local_OpellaXD/firmware/common/lpc1837/board.c \
C:/Local_OpellaXD/firmware/common/fpga/cjtag.c \
C:/Local_OpellaXD/firmware/common/common.c \
C:/Local_OpellaXD/firmware/common/comms.c \
C:/Local_OpellaXD/firmware/common/device/config.c \
C:/Local_OpellaXD/firmware/common/lpc1837/cr_startup_lpc18xx43xx.c \
C:/Local_OpellaXD/firmware/common/diagn.c \
C:/Local_OpellaXD/firmware/fw/firmware.c \
C:/Local_OpellaXD/firmware/common/fpga/fpga.c \
C:/Local_OpellaXD/firmware/common/fw_api.c \
C:/Local_OpellaXD/firmware/common/lpc1837/gpdma.c \
C:/Local_OpellaXD/firmware/common/i2c/i2c.c \
C:/Local_OpellaXD/firmware/common/lpc1837/i2c_18xx.c \
C:/Local_OpellaXD/firmware/common/fpga/jtag.c \
C:/Local_OpellaXD/firmware/common/led/led.c \
C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev.c \
C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev_desc.c \
C:/Local_OpellaXD/firmware/common/sio/sio.c \
C:/Local_OpellaXD/firmware/common/lpc1837/sysinit.c \
C:/Local_OpellaXD/firmware/common/testcomm.c \
C:/Local_OpellaXD/firmware/common/lpc1837/testpins.c \
C:/Local_OpellaXD/firmware/common/timer.c \
C:/Local_OpellaXD/firmware/common/tpa/tpa.c 

OBJS += \
./src/adc_18xx.o \
./src/adconv.o \
./src/board.o \
./src/cjtag.o \
./src/common.o \
./src/comms.o \
./src/config.o \
./src/cr_startup_lpc18xx43xx.o \
./src/diagn.o \
./src/firmware.o \
./src/fpga.o \
./src/fw_api.o \
./src/gpdma.o \
./src/i2c.o \
./src/i2c_18xx.o \
./src/jtag.o \
./src/led.o \
./src/libusbdev.o \
./src/libusbdev_desc.o \
./src/sio.o \
./src/sysinit.o \
./src/testcomm.o \
./src/testpins.o \
./src/timer.o \
./src/tpa.o 

C_DEPS += \
./src/adc_18xx.d \
./src/adconv.d \
./src/board.d \
./src/cjtag.d \
./src/common.d \
./src/comms.d \
./src/config.d \
./src/cr_startup_lpc18xx43xx.d \
./src/diagn.d \
./src/firmware.d \
./src/fpga.d \
./src/fw_api.d \
./src/gpdma.d \
./src/i2c.d \
./src/i2c_18xx.d \
./src/jtag.d \
./src/led.d \
./src/libusbdev.d \
./src/libusbdev_desc.d \
./src/sio.d \
./src/sysinit.d \
./src/testcomm.d \
./src/testpins.d \
./src/timer.d \
./src/tpa.d 


# Each subdirectory must supply rules for building sources it contributes
src/adc_18xx.o: C:/Local_OpellaXD/firmware/common/lpc1837/adc_18xx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/adconv.o: C:/Local_OpellaXD/firmware/common/ad/adconv.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/board.o: C:/Local_OpellaXD/firmware/common/lpc1837/board.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cjtag.o: C:/Local_OpellaXD/firmware/common/fpga/cjtag.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/common.o: C:/Local_OpellaXD/firmware/common/common.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/comms.o: C:/Local_OpellaXD/firmware/common/comms.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/config.o: C:/Local_OpellaXD/firmware/common/device/config.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cr_startup_lpc18xx43xx.o: C:/Local_OpellaXD/firmware/common/lpc1837/cr_startup_lpc18xx43xx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/diagn.o: C:/Local_OpellaXD/firmware/common/diagn.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/firmware.o: C:/Local_OpellaXD/firmware/fw/firmware.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/fpga.o: C:/Local_OpellaXD/firmware/common/fpga/fpga.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/fw_api.o: C:/Local_OpellaXD/firmware/common/fw_api.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/gpdma.o: C:/Local_OpellaXD/firmware/common/lpc1837/gpdma.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/i2c.o: C:/Local_OpellaXD/firmware/common/i2c/i2c.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/i2c_18xx.o: C:/Local_OpellaXD/firmware/common/lpc1837/i2c_18xx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/jtag.o: C:/Local_OpellaXD/firmware/common/fpga/jtag.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/led.o: C:/Local_OpellaXD/firmware/common/led/led.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/libusbdev.o: C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/libusbdev_desc.o: C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev_desc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sio.o: C:/Local_OpellaXD/firmware/common/sio/sio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sysinit.o: C:/Local_OpellaXD/firmware/common/lpc1837/sysinit.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/testcomm.o: C:/Local_OpellaXD/firmware/common/testcomm.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/testpins.o: C:/Local_OpellaXD/firmware/common/lpc1837/testpins.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/timer.o: C:/Local_OpellaXD/firmware/common/timer.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/tpa.o: C:/Local_OpellaXD/firmware/common/tpa/tpa.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DWDC_ABS_MEMORY_BYTES_SUPPORT -DNDEBUG -D__CODE_RED -DLPC1837 -DCORE_M3 -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\firmware/../../.." -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O2 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


