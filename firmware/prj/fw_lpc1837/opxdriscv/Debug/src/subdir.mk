################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Local_OpellaXD/firmware/riscv/riscvadivega.c \
C:/Local_OpellaXD/firmware/riscv/riscvcomm.c \
C:/Local_OpellaXD/firmware/riscv/riscvdw.c \
C:/Local_OpellaXD/firmware/riscv/riscvlayer.c \
C:/Local_OpellaXD/firmware/riscv/riscvpulpinolayer.c 

OBJS += \
./src/riscvadivega.o \
./src/riscvcomm.o \
./src/riscvdw.o \
./src/riscvlayer.o \
./src/riscvpulpinolayer.o 

C_DEPS += \
./src/riscvadivega.d \
./src/riscvcomm.d \
./src/riscvdw.d \
./src/riscvlayer.d \
./src/riscvpulpinolayer.d 


# Each subdirectory must supply rules for building sources it contributes
src/riscvadivega.o: C:/Local_OpellaXD/firmware/riscv/riscvadivega.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DWDC_ABS_MEMORY_BYTES_SUPPORT -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DRISCVDW -DDEBUG -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdriscv/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/riscvcomm.o: C:/Local_OpellaXD/firmware/riscv/riscvcomm.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DWDC_ABS_MEMORY_BYTES_SUPPORT -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DRISCVDW -DDEBUG -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdriscv/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/riscvdw.o: C:/Local_OpellaXD/firmware/riscv/riscvdw.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DWDC_ABS_MEMORY_BYTES_SUPPORT -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DRISCVDW -DDEBUG -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdriscv/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/riscvlayer.o: C:/Local_OpellaXD/firmware/riscv/riscvlayer.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DWDC_ABS_MEMORY_BYTES_SUPPORT -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DRISCVDW -DDEBUG -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdriscv/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/riscvpulpinolayer.o: C:/Local_OpellaXD/firmware/riscv/riscvpulpinolayer.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DWDC_ABS_MEMORY_BYTES_SUPPORT -D__REDLIB__ -DCORE_M3 -DLPC1837 -DDISKWARE -DRISCVDW -DDEBUG -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/opxdriscv/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


