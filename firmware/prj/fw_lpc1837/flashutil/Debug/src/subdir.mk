################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Local_OpellaXD/firmware/common/device/config.c \
C:/Local_OpellaXD/firmware/common/lpc1837/flash.c \
C:/Local_OpellaXD/firmware/common/flashutil/flashutil.c \
C:/Local_OpellaXD/firmware/common/lpc1837/iap_18xx.c \
C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev.c \
C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev_desc.c 

OBJS += \
./src/config.o \
./src/flash.o \
./src/flashutil.o \
./src/iap_18xx.o \
./src/libusbdev.o \
./src/libusbdev_desc.o 

C_DEPS += \
./src/config.d \
./src/flash.d \
./src/flashutil.d \
./src/iap_18xx.d \
./src/libusbdev.d \
./src/libusbdev_desc.d 


# Each subdirectory must supply rules for building sources it contributes
src/config.o: C:/Local_OpellaXD/firmware/common/device/config.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DFLASH_UTIL -D__REDLIB__ -DCORE_M3 -DLPC1837 -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/flashutil/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/flash.o: C:/Local_OpellaXD/firmware/common/lpc1837/flash.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DFLASH_UTIL -D__REDLIB__ -DCORE_M3 -DLPC1837 -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/flashutil/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/flashutil.o: C:/Local_OpellaXD/firmware/common/flashutil/flashutil.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DFLASH_UTIL -D__REDLIB__ -DCORE_M3 -DLPC1837 -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/flashutil/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/iap_18xx.o: C:/Local_OpellaXD/firmware/common/lpc1837/iap_18xx.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DFLASH_UTIL -D__REDLIB__ -DCORE_M3 -DLPC1837 -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/flashutil/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/libusbdev.o: C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DFLASH_UTIL -D__REDLIB__ -DCORE_M3 -DLPC1837 -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/flashutil/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/libusbdev_desc.o: C:/Local_OpellaXD/firmware/common/lpc1837/libusbdev_desc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__CODE_RED -DFLASH_UTIL -D__REDLIB__ -DCORE_M3 -DLPC1837 -I"C:/Local_OpellaXD/firmware/prj/fw_lpc1837/flashutil/../../../" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc" -I"C:\Local_OpellaXD\firmware\prj\fw_lpc1837\lpc_chip_18xx\inc\usbd" -O0 -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


