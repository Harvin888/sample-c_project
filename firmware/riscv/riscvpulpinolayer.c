/******************************************************************************
 Module: riscvpulpinolayer.c
 Engineer: Sandeep V L
 Description: Routines for accessing debug registers in RISCV PULPino core
 Date           Initials    Description
 12-Nov-2018    SVL 		 Initial
 ******************************************************************************/
#include "export/api_err.h"
#include "riscv/riscvpulpinolayer.h"
#include "riscv/riscvadivega.h"
#include "riscv/riscvlayer.h"

/* Set and clear EBREAK exception in the Debug Interrupt Enable register*/
#define DBG_IE_SET_BP (1 << 3)
#define DBG_IE_CLEAR_BP (0 << 3)

/* Debug base addresses of RV32M1 cores */
volatile TyDbgUnitRegs * const ptyRV32M1DebugUnitRegs[NUM_OF_CORES_IN_RV32M1] =
		{ RI5CY_DBG_BASE_ADRS, ZERO_RISCY_DBG_BASE_ADRS };
/* RV32M1 Debug unit registers */
TyDbgUnitRegs tyDbgUnitRegs;
/* Event unit base addresses of RV32M1 cores */
unsigned long const ulRV32M1EventUnitRegs[NUM_OF_CORES_IN_RV32M1] = {
		RI5CY_EVENT_UNIT_BASE_ADRS, ZERO_RISCY_EVENT_UNIT_BASE_ADRS };

/****************************************************************************
 Function: RPL_HaltAndResume
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucHaltResume - Halt or resume the core
 Output:int - error code (ERR_xxx)
 Description: Function to Halt and resume the target
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_HaltAndResume(unsigned char ucCoreId,
		unsigned char ucHaltResume)
{
	TyDbgCtrl tyReg = { 0 };
	unsigned long ulResult = ERR_NO_ERROR;

	/* Set the Halt bit in the debug control register */
	if (ucHaltResume == RISCV_CORE_HALT)
	{
		tyReg.ucHaltBit = 1;
	}
	else
	{
		tyReg.ucHaltBit = 0;
		tyReg.ucSingleStepEnBit = 0;
	}

	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyDebugCtl), 1,
			(unsigned char*) &(tyReg.uiRegVal));
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_Step
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 Output:int - error code (ERR_xxx)
 Description: Enable/disable Step bit
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_Step(unsigned char ucCoreId)
{
	unsigned long ulResult = ERR_NO_ERROR;
	TyDbgCtrl tyReg = { 0 };

	tyReg.ucSingleStepEnBit = 0x1;
	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyDebugCtl), 1,
			(unsigned char*) &tyReg.uiRegVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_StatusProc
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char *pucStatus - Status whether the core is running (0)
 or halted (1)
 unsigned char *pucReasonCode - Reason code
 Output:int - error code (ERR_xxx)
 Description: Checks whether core is halted or not
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_StatusProc(unsigned char ucCoreId, unsigned char *pucStatus,
		unsigned char *pucReasonCode)
{
	unsigned long ulResult = ERR_NO_ERROR;

	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyDebugCtl), 1,
			(unsigned char*) pucStatus);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	if (*(unsigned long*) pucStatus & 0x10000)
	{
		/* core is halted */
		*(unsigned long*) pucStatus = 0x1;
	}
	else
	{
		/* core is running */
		*(unsigned long*) pucStatus = 0x0;
	}

	/* Read the cause register */
	/*TODO: Cause register always gives 0x1F. It seems unimplemented. Need to keep a
	 * track of reason code internally. */
	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyDbgCause), 1,
			(unsigned char*) pucReasonCode);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_AddHwBkpt
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucBkptIndex - Breakpoint register to be controlled
 unsigned int uiBkptAddress - Breakpoint Address
 Output:int - error code (ERR_xxx)
 Description: Function to Set hardware breakpoint
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_AddHwBkpt(unsigned char ucCoreId, unsigned char ucBkptIndex,
		unsigned int uiBkptAddress)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulEnableBP = 0x1;

	/* Enable the breakpoint control register */
	ulResult =
			ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyHwBkptReg[ucBkptIndex].tyHwBkptCtrl),
					1, (unsigned char*) &ulEnableBP);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}
	/* Write the breakpoint address to the data register */
	ulResult =
			ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyHwBkptReg[ucBkptIndex].tyHwBkptData),
					1, (unsigned char*) &uiBkptAddress);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_RemoveHwBkpt
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucBkptIndex - Breakpoint register to be controlled
 Output:int - error code (ERR_xxx)
 Description: Function to Set hardware breakpoint
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_RemoveHwBkpt(unsigned char ucCoreId, unsigned char ucBkptIndex)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0x0;

	/* Disable the breakpoint control register */
	ulResult =
			ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyHwBkptReg[ucBkptIndex].tyHwBkptCtrl),
					1, (unsigned char*) &ulData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadHwBkptCtrlReg
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucBkptIndex - Breakpoint register to be controlled
 unsigned int *puiBkptStatus - Breakpoint status (MSB is high when
 breakpoint is hit)
 Output:int - error code (ERR_xxx)
 Description: Function to read hardware breakpoint control register
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadHwBkptCtrlReg(unsigned char ucCoreId,
		unsigned char ucBkptIndex, unsigned int *puiBkptStatus)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/*NOTE: Reading this register always gives 0x0. */

	/* Enable the breakpoint control register */
	ulResult =
			ADI_BurstWrite(AXI4_OP_BURST_READ_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyHwBkptReg[ucBkptIndex].tyHwBkptCtrl),
					1, (unsigned char*) puiBkptStatus);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadNPC
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char *pucPCVal - PC value
 Output:int - error code (ERR_xxx)
 Description: Function to read NPC
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadNPC(unsigned char ucCoreId, unsigned char *pucPCVal)
{
	unsigned long ulResult = ERR_NO_ERROR;

	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiNPC), 1,
			(unsigned char*) pucPCVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadPPC
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char *pucPCVal - PC value
 Output:int - error code (ERR_xxx)
 Description: Function to read NPC
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadPPC(unsigned char ucCoreId, unsigned char *pucPCVal)
{
	unsigned long ulResult = ERR_NO_ERROR;

	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiPPC), 1,
			(unsigned char*) pucPCVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadCSR
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucRegIndex - Register to be read
 unsigned int *puiPCVal - PC value
 Output:int - error code (ERR_xxx)
 Description: Function to read a CSR
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadCSR(unsigned char ucCoreId, unsigned int uiRegIndex,
		unsigned char *pucRegVal)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Read a general purpose register */
	ulResult =
			ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiCSR[uiRegIndex]),
					1, pucRegVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadGPR
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucRegIndex - Register to be read
 unsigned int *puiPCVal - PC value
 Output:int - error code (ERR_xxx)
 Description: Function to read a GPR
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadGPR(unsigned char ucCoreId, unsigned char ucRegIndex,
		unsigned char *pucRegVal)
{
	unsigned long ulResult = ERR_NO_ERROR;

	if (ucRegIndex >= MAX_GPR_NUM)
	{
		return ERR_NO_ERROR;
	}

	/* Read a general purpose register */
	ulResult =
			ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiGPR[ucRegIndex]),
					1, pucRegVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadAllGPRs
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned int *puiPCVal - PC value
 Output:int - error code (ERR_xxx)
 Description: Function to read all GPRs
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadAllGPRs(unsigned char ucCoreId, unsigned char *pucRegVal) {
	int i = 0;
	unsigned long ulResult = ERR_NO_ERROR;

	/* Read all the general purpose registers */
	for (i = 0; i < MAX_GPR_NUM; i++)
	{
		ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
				(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiGPR[i]),
				1, (unsigned char*) (pucRegVal + i * 4));
		if (ulResult != ERR_NO_ERROR)
		{
			return ulResult;
		}
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WritePC
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucWhichPC - NPC/PPC
 unsigned long *pulRegVal - Register value to be written
 Output:int - error code (ERR_xxx)
 Description: Function to write to a GPR
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WritePC(unsigned char ucCoreId, unsigned char ucWhichPC,
		unsigned long *pulRegVal)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulPC =
			(ucWhichPC == REG_NPC) ?
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiNPC) :
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiPPC);

	/* Write to a general purpose registers */
	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD, ulPC, 1,
			(unsigned char*) pulRegVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WriteCSR
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucRegIndex - Register to be read
 unsigned int *pulRegVal - Reg value
 Output:int - error code (ERR_xxx)
 Description: Function to write a CSR
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteCSR(unsigned char ucCoreId, unsigned int uiRegIndex,
		unsigned long *pulRegVal)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Read a general purpose register */
	ulResult =
			ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiCSR[uiRegIndex]),
					1, (unsigned char*) pulRegVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WriteGPR
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucRegIndex - Register index
 unsigned long *pulRegVal - Register value to be written
 Output:int - error code (ERR_xxx)
 Description: Function to write to a GPR
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteGPR(unsigned char ucCoreId, unsigned char ucRegIndex,
		unsigned long *pulRegVal)
{
	unsigned long ulResult = ERR_NO_ERROR;

	if (ucRegIndex >= MAX_GPR_NUM)
	{
		return ERR_NO_ERROR;
	}

	/* Write to a general purpose registers */
	ulResult =
			ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
					(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiGPR[ucRegIndex]),
					1, (unsigned char*) pulRegVal);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WriteAllGPRs
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long *pulRegVal - Register values to be written. Values
 are assumed to be in sequence from X0 to X31.
 Output:int - error code (ERR_xxx)
 Description: Function to write all GPRs
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteAllGPRs(unsigned char ucCoreId, unsigned long *pulRegVal) {
	int i = 0;
	unsigned long ulResult = ERR_NO_ERROR;

	for (i = 0; i < MAX_GPR_NUM; i++)
	{
		/* Write to a general purpose registers */
		ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
				(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->uiGPR[i]),
				1, (unsigned char*) &pulRegVal[i]);
		if (ulResult != ERR_NO_ERROR)
		{
			return ulResult;
		}
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadMemoryByte
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be read
 unsigned char *pucData - Data to be written
 Output:int - error code (ERR_xxx)
 Description: Function to read memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadMemoryByte(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Write to a general purpose registers */
	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_8_BIT_WORD, ulAddress, uiCount,
			pucData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WriteMemoryByte
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be written
 unsigned char *pucData - Data to be written
 Output:int - error code (ERR_xxx)
 Description: Function to write memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteMemoryByte(unsigned char ucCoreId,
		unsigned long ulAddress, unsigned int uiCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Write to a general purpose registers */
	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_8_BIT_WORD, ulAddress,
			uiCount, pucData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadMemoryByte
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be read
 unsigned char *pucData - Data to be written
 Output:int - error code (ERR_xxx)
 Description: Function to read memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadMemoryHalfWord(unsigned char ucCoreId,
		unsigned long ulAddress, unsigned int uiCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Write to a general purpose registers */
	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_16_BIT_WORD, ulAddress, uiCount,
			pucData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WriteMemoryHalfWord
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be written
 unsigned char *pucData - Data to be written
 Output:int - error code (ERR_xxx)
 Description: Function to write memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteMemoryHalfWord(unsigned char ucCoreId,
		unsigned long ulAddress, unsigned int uiCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Write to a general purpose registers */
	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_16_BIT_WORD, ulAddress,
			uiCount, pucData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadMemoryWord
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be read
 unsigned char *pucData - Data to be read
 Output:int - error code (ERR_xxx)
 Description: Function to read memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadMemoryWord(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Write to a general purpose registers */
	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD, ulAddress, uiCount,
			pucData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_WriteMemoryWord
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be written
 unsigned char *pucData - Data to be written
 Output:int - error code (ERR_xxx)
 Description: Function to write memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteMemoryWord(unsigned char ucCoreId,
		unsigned long ulAddress, unsigned int uiCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	/* Write to a general purpose registers */
	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD, ulAddress,
			uiCount, pucData);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}

/****************************************************************************
 Function: RPL_ReadMemory
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be read
 unsigned long ulSize - Size of each word
 unsigned char *pucData - Data to be read
 Output:int - error code (ERR_xxx)
 Description: Function to read memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ReadMemory(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned long ulSize, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned int (*fpMemRead)(unsigned char, unsigned long, unsigned int,
			unsigned char*);
	unsigned int i = 0;

	switch (ulSize)
	{
	case ACCESS_WORD:
		fpMemRead = &RPL_ReadMemoryWord;
		break;
	case ACCESS_HALF:
		fpMemRead = &RPL_ReadMemoryHalfWord;
		break;
	case ACCESS_BYTE:
		fpMemRead = &RPL_ReadMemoryByte;
		break;
	default:
		fpMemRead = &RPL_ReadMemoryByte;
		break;
	}

	while (i < uiCount)
	{
		ulResult = fpMemRead(ucCoreId, ulAddress, 1, (pucData + i*ulSize));
		if (ulResult != ERR_NO_ERROR)
		{
			return ulResult;
		}
		ulAddress += ulSize;
		i++;
	}

	return ulResult;
}

/****************************************************************************
 Function: RPL_WriteMemory
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned long ulAddress - Memory address.
 unsigned int uiCount - Number of words to be written
 unsigned long ulSize - Size of each word
 unsigned char *pucData - Data to be written
 Output:int - error code (ERR_xxx)
 Description: Function to write block of memory
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_WriteMemory(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned long ulSize, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned int (*fpMemWrite)(unsigned char, unsigned long, unsigned int,
			unsigned char*);
	unsigned int i = 0;

	switch (ulSize)
	{
	case ACCESS_WORD:
		fpMemWrite = &RPL_WriteMemoryWord;
		break;
	case ACCESS_HALF:
		fpMemWrite = &RPL_WriteMemoryHalfWord;
		break;
	case ACCESS_BYTE:
		fpMemWrite = &RPL_WriteMemoryByte;
		break;
	default:
		fpMemWrite = RPL_WriteMemoryByte;
		break;
	}


	while (i < uiCount)
	{
		ulResult = fpMemWrite(ucCoreId, ulAddress, 1, (pucData + i*ulSize));
		if (ulResult != ERR_NO_ERROR)
		{
			return ulResult;
		}
		ulAddress += ulSize;
		i++;
	}

	return ulResult;
}

/****************************************************************************
 Function: RPL_CfgSwBkptException
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 unsigned char ucSet - Set or clear bkpt exception
 Output:int - error code (ERR_xxx)
 Description: Function to Set or clear the EBREAK exception
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_CfgSwBkptException(unsigned char ucCoreId, unsigned char ucSet)
{
	unsigned long ulResult = ERR_NO_ERROR, ulDbgIE = 0;
	unsigned char ucSetClear = (ucSet) ? DBG_IE_SET_BP : DBG_IE_CLEAR_BP;

	ulResult = ADI_BurstRead(AXI4_OP_BURST_READ_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyDbgIntEn), 1,
			(unsigned char*) &ulDbgIE);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}
	ulDbgIE = ulDbgIE | ucSetClear;
	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
			(unsigned long) &(ptyRV32M1DebugUnitRegs[ucCoreId]->tyDbgIntEn), 1,
			(unsigned char*) &ulDbgIE);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ulResult;
}

/****************************************************************************
 Function: RPL_ResetCore
 Engineer: Sandeep VL
 Input:unsigned char ucCoreId - Core to be debugged
 Output:int - error code (ERR_xxx)
 Description: Reset the specified core. After reset, core will halt at reset vector
 Date           Initials    Description
 14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int RPL_ResetCore(unsigned char ucCoreId)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulSysResReq = 0x1 << 31;

	ulResult = ADI_BurstWrite(AXI4_OP_BURST_WRITE_32_BIT_WORD,
			(ulRV32M1EventUnitRegs[ucCoreId] + SLEEP_CONTROL_REG_OFFSET), 1,
			(unsigned char*) &ulSysResReq);
	if (ulResult != ERR_NO_ERROR)
	{
		return ulResult;
	}

	return ERR_NO_ERROR;
}
