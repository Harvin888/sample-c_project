/******************************************************************************
       Module: riscvdw.c
     Engineer: Vitezslav Hola
  Description: Opella-XD RISCV Diskware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/
#ifdef LPC1837
#include "chip.h"
#include "common/timer.h"
#include "common/device/config.h"
#include "common/device/memmap.h"
#include "common/lpc1837/app_usbd_cfg.h"
#include "common/lpc1837/board.h"
#include "common/lpc1837/libusbdev.h"
#include "common/lpc1837/lpc1837.h"
#include "common/lpc1837/version.h"
#else
#include "common/ml69q6203.h"
#include "common/irq.h"
#endif

#include "common/led/led.h"

#ifdef LPC1837
#include "common/fw_api.h"
#endif

#include "common/common.h"
#include "common/comms.h"
#include "common/fpga/jtag.h"

#include "common/tpa/tpa.h"
#include "riscv/riscvcomm.h"
#include "riscv/riscvlayer.h"

// header specific for debug version
#ifdef DEBUG
#include "common/sio/sio.h"
#endif

#ifndef LPC1837
// external variables
extern unsigned char bLedUsbActivityFlag;
extern unsigned char bTimer100msElapsed;
#endif

#ifdef LPC1837
// function prototypes
int main(void);
int dw_main(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt);
int InitRiscvDW(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt);
int ProcessRiscvCommandDW(unsigned long ulCommandCode, unsigned long ulSize);

__attribute__ ((section(".data.$RISCV_DW_IDENT")))
volatile TyDW_Ident tyDW_Ident = {0, RISCV_DW_ID_CODE, RISCV_DW_SIZE, 0x0, RISCV_DW_VERSION_CONST, RISCV_DW_DATE_CONST, FW_VERSION_CONST};

PTyFwApiStruct ptyFwApi;
/****************************************************************************
     Function: InitRISCVDW
     Engineer: Rejeesh Shaji Babu
        Input: ptyFwApi -> pointer containing firmware function pointers which are required for diskware
       Output: return value
  Description: Initialize RISCV diskware (1st call to diskware)
Date           Initials    Description
11-June-2019   RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_RISCV_DW_INIT")))
__attribute__ ((optimize(0)))
int InitRiscvDW(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt)
{
	return dw_main(ptyFwApi, iOtherDwCnt);
}

/****************************************************************************
     Function: ProcessRiscvCommandDW
     Engineer: Rejeesh Shaji Babu
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incoming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: ProcessRiscvCommand wrapper called from firmware
Date           Initials    Description
29-May-2019    RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_RISCV_CMD_PROCESSING")))
__attribute__ ((optimize(0)))
int ProcessRiscvCommandDW(unsigned long ulCommandCode, unsigned long ulSize)
{
	return ProcessRiscvCommand(ulCommandCode,ulSize);
}
/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: none
       Output: return value
  Description: main program
Date           Initials    Description
24-Jul-2006    VH          Initial
****************************************************************************/
int dw_main(TyFwApiStruct *ptyFwApiFromFw, int *iOtherDwCnt)
{

   ptyFwApi = ptyFwApiFromFw;

#ifdef DEBUG
	//printf("\n\rStarting RISCV diskware\n\r");
#endif

   //Do JTAG initialization only if no other diskwares are running
   if(*iOtherDwCnt == 0)
   {
	   ptyFwApi->ptrInitializeJtagScanConfig();
   }
   RISCVL_InitializeJtagForRiscv(iOtherDwCnt);
   return 0;
}

__attribute__ ((optimize(0)))
int main()
{
	//These functions are never called, just keeping here so that linker doesn't remove these functions
	InitRiscvDW(NULL,0);
	ProcessRiscvCommandDW(0,0);
	return 0;
}
#else //R2
/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: none
       Output: return value
  Description: main program
Date           Initials    Description
24-Jul-2006    VH          Initial
****************************************************************************/
int main(int argc, char *argv[])
{
   unsigned char ucTerminateDiskware;
   int iOtherDwCnt = 0;

   // initialize interrupts and enabled them
   init_irq();
   (void)irq_en();

   // initialize LED and TPA management
   InitLed(LED_PWR_READY);
   InitTpa();

#ifdef DEBUG
	// initialize serial interface for debug messages
	(void)sio_init();
	//printf("\n\rStarting RISCV diskware\n\r");
#endif
   // initialize JTAG engine for ARC targets and data structures
     InitializeJtagScanConfig();
     RISCVL_InitializeJtagForRiscv(&iOtherDwCnt);

   // initialize command processing, if not possible, finish immediately
   if (InitCommandProcessing(NULL, NULL, 0, 0))
      {
      (void)irq_dis();
      return 1;
      }

   // main loop - get process command, send response, process led
   ucTerminateDiskware = 0;

   timer_100ms(TRUE);

   // main loop, process any pending commands
   do
   {
		if(ProcessCommand(&ucTerminateDiskware))
			bLedUsbActivityFlag = 1;
		if(ProcessResponse())
			bLedUsbActivityFlag = 1;
		timer_100ms(FALSE);       // check for 100 ms timer
		ProcessTpa();
		ProcessLedIndicators();
		bTimer100msElapsed = 0;
   }
   while(!ucTerminateDiskware);

#ifdef DEBUG
   sio_printf("Exiting ARC diskware\n\r");
#endif

   (void)irq_dis();      // disable interrupts
   return 0;
}

/****************************************************************************
     Function: timer_100ms
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: setting flag with 100 ms period
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void timer_100ms(unsigned char bInit)
{
   if (bInit)
      {
      bTimer100msElapsed = 0;
      // set timer 2 for interval about 100 ms, one shot mode
      put_wvalue(TIMESTAT2, TIMESTAT_STATUS);
      put_wvalue(TIMEBASE2, 0x0);
      put_wvalue(TIMECMP2, 0xB71A);
      put_wvalue(TIMECNTL2, TIMECNTL_CLK16 | TIMECNTL_OS | TIMECNTL_START);
      }
   else if (get_wvalue(TIMESTAT2) & TIMESTAT_STATUS)
      {  // timer elapsed
      bTimer100msElapsed = 1;
      put_wvalue(TIMESTAT2, TIMESTAT_STATUS);
      put_wvalue(TIMEBASE2, 0x0);
      put_wvalue(TIMECMP2, 0xB71A);
      put_wvalue(TIMECNTL2, TIMECNTL_CLK16 | TIMECNTL_OS | TIMECNTL_START);
      }
}
#endif
