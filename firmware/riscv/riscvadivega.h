/******************************************************************************
       Module: riscvadivega.h
     Engineer: Sandeep V L
  Description: Header for accessing Advanced Debug Interface in VEGA board
  Date           Initials    Description
  12-Nov-2018    SVL 		 Initial
 ******************************************************************************/
#ifndef __RISCV_ADI_VEGA_H
#define __RISCV_ADI_VEGA_H

/* Select the top module indicating the command is to be executed by top module */
#define SELECT_TOP_MODULE (0x1 << 4)
/* Only 1 bit is used to indicate whether the command is for top module or sub module */
#define MODULE_SELECT_BIT_LEN 0x1
/* AXI4 module number */
#define AXI4_MODULE_NUM 0x0
/* Command to select AXI4 sub module in ADI - 0x10 */
#define CMD_SELECT_AXI4 (SELECT_TOP_MODULE | AXI4_MODULE_NUM)
/* Length of ADI module select command */
#define SELECT_MODULE_CMD_LEN 0x5
/* Length of Select command */
#define INTERNAL_REG_SELECT_CMD_LEN 0x6
/* Length of internal register index */
#define REGISTER_INDEX_LEN 0x2
/* Length of AXI4 opcodes */
#define AXI4_OPCODE_LEN 0x4
/* Length of ADI module select command */
#define SELECT_INTERNAL_REG_LEN 0x7
/* Length (in bits) of Count field in BURST command */
#define BURST_CMD_WORD_COUNT_LEN 16
/* Length of BURST setup command in bits. */
#define BURST_SETUP_CMD_LEN 53
/* Start bit indicating valid start of a bit stream */
#define BURST_START_BIT 1
/* Polynomial used for CRC calculation in burst operations */
#define AXI4_CRC_POLYNOMIAL 0xEDB88320

/* Enumeration for AXI4 opcodes */
typedef enum _tyAXI4Opcode
{
	AXI4_OP_NOP = 0,
	AXI4_OP_BURST_WRITE_8_BIT_WORD,
	AXI4_OP_BURST_WRITE_16_BIT_WORD,
	AXI4_OP_BURST_WRITE_32_BIT_WORD,
	AXI4_OP_BURST_WRITE_64_BIT_WORD,
	AXI4_OP_BURST_READ_8_BIT_WORD,
	AXI4_OP_BURST_READ_16_BIT_WORD,
	AXI4_OP_BURST_READ_32_BIT_WORD,
	AXI4_OP_BURST_READ_64_BIT_WORD,
	AXI4_OP_INTERNAL_REG_WRITE,
	AXI4_OP_INTERNAL_REG_SEL = 0xD
}TyAXI4Opcode;

typedef enum _tyAXi4RegIndex
{
	AXI4_REG_ERROR = 0,
	AXI4_REG_DEBUG_PATH_SEL,
	AXI4_REG_POWER_UP
}TyAXI4RegIndex;

/* Set the JTAG instruction to Instruction register */
int ADI_SelectAXI4Module(unsigned long ulJTAGIRInstn);
/* Select an internal register */
int ADI_SelectAXI4InternalRegister(unsigned char ucRegIndex);
/* Select and write to internal register */
int ADI_WriteAXI4InternalRegister(unsigned char ucRegIndex, unsigned long *pulData,
		unsigned int uiDataLen);
/* Select and Read internal register */
int ADI_ReadAXI4InternalRegister(unsigned char ucRegIndex, unsigned long *pulData,
		unsigned int uiReadLen);
/* Setup burst command */
int ADI_BurstOperationSetupCommand(TyAXI4Opcode tyOpcode, unsigned long ulAddress,
		unsigned short usWordCount);
/* Burst write command */
int ADI_BurstWrite(TyAXI4Opcode tyOpcode, unsigned long ulAddress,
		unsigned short usWordCount, unsigned char *pulData);
/* Burst raed command */
int ADI_BurstRead(TyAXI4Opcode tyOpcode, unsigned long ulAddress,
		unsigned short usWordCount, unsigned char *pucData);
/* Enable RISCV Debug */
int ADI_EnableRISCVDebug();

#endif //__RISCV_ADI_VEGA_H
