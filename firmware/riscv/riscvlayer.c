/******************************************************************************
       Module: riscvlayer.c
     Engineer: 
  Description: RISCV layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
  09-Sep-2009    RSB         Removed TAP reset
 ******************************************************************************/

#ifdef LPC1837
#include "chip.h"
#else
#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/timer.h"
#include "riscv/riscvcomm.h"
#include "riscv/riscvlayer.h"
#include "riscv/riscvadivega.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "common/led/led.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/fpga/cjtag.h"

#ifdef LPC1837
#include "common/fw_api.h"
#endif

#define ABSTRACT_DATA0_CMD 0x4

#ifdef LPC1837
// external variables
extern PTyFwApiStruct ptyFwApi;
#else
extern PTyTpaInfo ptyTpaInfo;
#endif

//Global variables
TyJtagTmsSequence gtyTmsForIR, gtyTmsForDR;

extern TyDwRiscvTargetConfig gtyDwRiscvConfig;

/****************************************************************************
     Function: RISCVL_InitializeJtagForRiscv
     Engineer: CBR
        Input: None
       Output: None
  Description: Initialize the JTAG engine in Opella-XD
Date           Initials    Description
28-Sep-2018        HS          Initial
 ****************************************************************************/
void RISCVL_InitializeJtagForRiscv(int *iOtherDwCnt)
{
	volatile unsigned long ulValue;

	gtyDwRiscvConfig.bIsVegaBoard = 0;
	gtyDwRiscvConfig.ucAbsAutoExecSupported = 0;

	// Not sure if these are sensible default values
	gtyDwRiscvConfig.ulRegisterMech = REG_ACCESS_ABSTRCT_CMD;
	gtyDwRiscvConfig.ulMemoryMech = SYSTEM_BUS_ACCESS;
	gtyDwRiscvConfig.ulDirectAccessToCsr = ABS_REG_CSR_NOT_SUPPORTED;
	gtyDwRiscvConfig.ulDoublePrecision = 0;
	gtyDwRiscvConfig.ulFenceSupport = FENCE_UNSUPPORTED;

	gtyTmsForIR.usPreTmsCount = 5;          gtyTmsForIR.usPreTmsBits = 0x06;
	gtyTmsForIR.usPostTmsCount = 4;         gtyTmsForIR.usPostTmsBits = 0x03;
	gtyTmsForDR.usPreTmsCount = 4;          gtyTmsForDR.usPreTmsBits = 0x02;
	gtyTmsForDR.usPostTmsCount = 4;         gtyTmsForDR.usPostTmsBits = 0x03;

	if(*iOtherDwCnt == 0)
	{
		ulValue = get_wvalue(JTAG_IDENT);
		ulValue = get_wvalue(JTAG_VER);
		ulValue = get_wvalue(JTAG_VER_FPGAID);
		ulValue = get_wvalue(JTAG_DATE);

		put_wvalue(JTAG_MCIRC, 0x123456);
		ulValue = get_wvalue(JTAG_MCIRC);

		// DLOG(("Func:ARCL_InitializeJtagForArc"));
		(void)JtagSetMulticore(0, NULL, NULL, 0, NULL);                         // no MC support, just initialize structures
		// initialize TMS sequences for IR, DR and IRandDR scans

		(void)JtagSetTMS(&gtyTmsForIR, &gtyTmsForDR, &gtyTmsForIR, &gtyTmsForDR);
		// configure I/O for TPA in FPGA
		put_wvalue(JTAG_TPDIR, 0x0);                                            // disable all outputs (no hazards during mode change)
		us_wait(5);                                                             // wait 5 us
		// first select mode
		ulValue = 0;
		ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK | JTAG_TPA_TDO | JTAG_TPA_RSCK);
		ulValue |= JTAG_TPA_ENTRST | JTAG_TPA_TRST;
		put_wvalue(JTAG_TPMODE, ulValue);
		// now set output values (except output driver)
		ulValue = 0;
		ulValue |= JTAG_TPA_ENTRST | JTAG_TPA_RST | JTAG_TPA_TRST;
		put_wvalue(JTAG_TPOUT, ulValue);
		// enable output drivers
		ulValue = 0;
		ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK);
		ulValue |= (JTAG_TPA_DRIEN | JTAG_TPA_ENTRST | JTAG_TPA_DBGRQ | JTAG_TPA_TRST | JTAG_TPA_RST);
		put_wvalue(JTAG_TPDIR, ulValue);
	#ifdef LPC1837
		((PTyTpaInfo)(ptyFwApi->ptrTpaInfo))->ulSavedTPDIRValue = ulValue;
	#else
		ptyTpaInfo->ulSavedTPDIRValue = ulValue;
	#endif
		us_wait(5);
	}

	// wait 5 us
}
/****************************************************************************
     Function: RISCVL_SelectRiscvTarget
     Engineer: Vitezslav Hola
        Input: unsigned char ucRiscTargetType - RISCV target type
       Output: error code
  Description: Select RISCV target type (configure pins, etc.)
Date           Initials    Description
28-Mar-2019    RSB          Initial
****************************************************************************/
unsigned long RISCVL_SelectRiscvTarget(unsigned char ucRiscTargetType)
{
   unsigned long ulValue;
   unsigned long ulResult = ERR_NO_ERROR;
   switch (ucRiscTargetType)
      {
   	   case RISCV_TARGET_JTAG_TPA_R1:
         {
         // configure I/O for TPA in when using 20-way connector (ARM compatible)
         ulValue = get_wvalue(JTAG_TPOUT);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
         ulValue &= ~(JTAG_TPA_DBGRQ);
         put_wvalue(JTAG_TPOUT, ulValue);
         ulValue = get_wvalue(JTAG_TPMODE);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT) ;
         ulValue &= ~(JTAG_TPA_RST | JTAG_TPA_DBGRQ );
         put_wvalue(JTAG_TPMODE, ulValue);
         ulValue = get_wvalue(JTAG_TPDIR);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_DBGRQ);
         ulValue |= (JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
         put_wvalue(JTAG_TPDIR, ulValue);
         // configure reset detection
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) & ~JTAG_JSCTR_TRD);       // disable target reset detection in FPGA
         }
         break;
      default:    // unknown target type
         ulResult = ERR_PROTOCOL_INVALID;
         break;
      }

   return ulResult;
}

/****************************************************************************
     Function: RISCVL_TargetReset
     Engineer: Harvinder Singh
        Input: None
       Output: unsigned long *pulAbits - pointer to read  Abits.
               error code
  Description: Reset the RISCV target.
Date           Initials    Description
06-June-2019    HS          Initial
****************************************************************************/
unsigned long RISCVL_TargetReset(unsigned long *pulAbits)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulIrVal=0,ulData = 0;
	unsigned long ulJtagScanDROut = 0;

	ulIrVal = RISCV_DTM_DTMCS;

	//IR scan for DTM control and status register.
	ulResult = JtagScanIR(&ulIrVal,NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	//read DTM control and status register and store in ulJtagScanDROut.
	ulResult = JtagScanDR(RISCV_DTM_RISCV_DMI_DATA_LENGTH,&ulData,&ulJtagScanDROut);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	//store ulAbits
	*pulAbits = (ulJtagScanDROut & RISCV_DTM_DTMCS_ABITS) >> RISCV_DTM_DTMCS_ABITS_OFFSET;

	//IR scan for DMI
	ulIrVal = RISCV_DTM_DMI;
	ulResult = JtagScanIR(&ulIrVal,NULL);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Prepare and write to DM control register with NdmReset and Dmactive bit are set to 1 .
	ulResult = RISCVL_DmWrite(*pulAbits, RISCV_DMI_DMCONTROL, RISCV_DMI_DMCONTROL_NDMRESET | RISCV_DMI_DMCONTROL_DMACTIVE, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Prepare and write to DM control register with  NdmReset bit to 0 and Dmactive = 1.
	ulResult = RISCVL_DmWrite(*pulAbits, RISCV_DMI_DMCONTROL, RISCV_DMI_DMCONTROL_RESET_NDMRESET |  RISCV_DMI_DMCONTROL_DMACTIVE, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_DmiAccessDataIn
     Engineer: CBR
        Input: unsigned long *PulResult - pointer to store data
        	   unsigned long ulData - 32 bit data to be written
        	   unsigned long ulAdd - address for dmi
        	   unsigned long ulOp - operation for read and write
       Output: unsigned long - error code (ERR_xxx)
  Description: Program the DMI Access register
Date           Initials    Description
28-Sep-2018    CBR         Initial
****************************************************************************/
void RISCVL_DmiAccessDataIn(unsigned long ulAdd, unsigned long ulData, unsigned long ulOp, unsigned long *PulResult)
{
	unsigned long ulreset = 3;
	PulResult[0] = (ulData<<2) | (ulOp);
	PulResult[1] = (ulAdd<<2) | (ulData>>30);
	PulResult[2] = ulreset & (ulAdd>>30);
}

/****************************************************************************
     Function: RISCVL_DmRead
     Engineer: CBR
        Input: unsigned long ulAbits - abits size for Dmi
        	   unsigned long ulAddress - address for debug module register
        	   unsigned long *pulData - pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: read operation for Debug module
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_DmRead(unsigned long ulAbits, unsigned long ulAddress, unsigned long *pulData)
{
	unsigned long ulDmiAccessDataOut[3] = {0};
	unsigned long ulJtagScanDROut[3] = {0};
	unsigned long ulIrVal = 0;
	unsigned long ulIrData = 0;
	unsigned long ulResult = ERR_NO_ERROR;

	//printf("[RISCVL_DmRead]\n");
	//called for making jtag input data for JtagScanD
	RISCVL_DmiAccessDataIn(ulAddress, RISCV_DATA_NULL, RISCV_DTM_RISCV_DMI_OP_READ, ulDmiAccessDataOut);
	//passed the value to jatag scan dr and output is not required
	ulResult = JtagScanDR(ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH, ulDmiAccessDataOut, ulJtagScanDROut);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//called for making jtag input data for JtagScanDR, DMI_OP_NOP
	RISCVL_DmiAccessDataIn(ulAddress, RISCV_DATA_NULL, RISCV_DTM_RISCV_DMI_OP_NOP, ulDmiAccessDataOut);
	//passed the value to jatag scan dr and output is not required
	ulResult = JtagScanDR(ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH, ulDmiAccessDataOut, ulJtagScanDROut);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//condition to check JtagScanDR last to bit equal to value 3
	if( (ulJtagScanDROut[0] & RISCV_DTM_RISCV_DMI_OP) == 3)
	{
		//printf("[RISCVL_DmRead]fine tuning. Current tyTmsForDR.usPostTmsCount:%d\n",gtyTmsForDR.usPreTmsCount);
		gtyTmsForDR.usPreTmsCount += 1;          gtyTmsForDR.usPreTmsBits <<= 1;
		gtyTmsForDR.usPostTmsCount += 1;         gtyTmsForDR.usPostTmsBits = 0x03;

		(void)JtagSetTMS(&gtyTmsForIR, &gtyTmsForDR, &gtyTmsForIR, &gtyTmsForDR);

		ulIrVal = RISCV_DMI_DMCONTROL;
		//called for entering to JTAG DTM REGISTER, DTM CONTROL AND STATUS
		ulResult = JtagScanIR(&ulIrVal,NULL);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulIrData = RISCV_DTM_DTMCS_DMIRESET;
		//called for Dmireset in  DTM CONTROL AND STATUS
		ulResult = JtagScanDR(RISCV_DTM_RISCV_DMI_DATA_LENGTH, &ulIrData, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulIrVal = RISCV_DTM_DMI;
		//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
		ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		//called for making jtag input data for JtagScanDR, DMI_OP_NOP
		RISCVL_DmiAccessDataIn(ulAddress, RISCV_DATA_NULL, RISCV_DTM_RISCV_DMI_OP_NOP, ulDmiAccessDataOut);
		//making for JtagScanDR, DMI_OP_NOP
		ulResult = JtagScanDR(ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH, ulDmiAccessDataOut, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	*pulData = (((ulJtagScanDROut[0] >> RISCV_DTM_RISCV_DMI_OP_LENGTH) | (ulJtagScanDROut[1] << 30)) & 0xFFFFFFFF) ;
	
	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: RISCVL_DmWriteData0Multiple
     Engineer: Rejeesh S B
        Input: unsigned long ulDrLength - DR length
        	   unsigned long ulWords 	- No: of words to be written to scanchain
        	   unsigned long *pulData 	- pointer to data to be written
        	   unsigned long *pulDataRet- pointer to store return data (not reqd here)
       Output: unsigned long - error code (ERR_xxx)
  Description: Write to DATA0 register via DMI using 16 FPGA buffers
Date           Initials    Description
04-Jan-2019    RSB         Initial
 ****************************************************************************/
unsigned long RISCVL_DmWriteData0Multiple(unsigned long ulDMRegAddr, unsigned long ulDrLength, unsigned long ulWords,unsigned long *pulData, unsigned long *pulDataRet)
{
	unsigned long ulResult = ERR_NO_ERROR;

	unsigned short usReg, ulCount;
	register unsigned int uiWordCnt = 0;
	register unsigned short usBufCnt = 0;
	unsigned long ulDmiAccessData[3];
	unsigned long ulDmiAccessDataNOP[3] = {0};

	unsigned long ulDmiDataOut[3];
	unsigned long ulIrVal = 0;
	unsigned long ulIrData = 0;

	//printf("[RISCVL_DmWriteData0Multiple]\n");
	// enable AutoScan in FPGA
	usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
	put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));

	for(usBufCnt=0; usBufCnt<16; usBufCnt++)
	{
		put_wvalue(JTAG_SPARAM_CNT(usBufCnt), (ulDrLength & 0x000007FF) | 0x00008000);
		put_wvalue(JTAG_SPARAM_TMS(usBufCnt), JTAG_SPARAM_TMS_VAL(gtyTmsForDR.usPreTmsCount, gtyTmsForDR.usPreTmsBits, gtyTmsForDR.usPostTmsCount, gtyTmsForDR.usPostTmsBits));
	}

	while(uiWordCnt<ulWords)
	{
		for(usBufCnt=0; usBufCnt<16; usBufCnt++)
		{
			//while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usBufCnt)); // wait for buffer X to be free
			if(uiWordCnt<ulWords)
			{
				ulDmiAccessData[0] = (pulData[uiWordCnt]<<2) | (RISCV_DTM_RISCV_DMI_OP_WRITE);
				ulDmiAccessData[1] = (ulDMRegAddr<<2) | (pulData[uiWordCnt++]>>30);
				ulDmiAccessData[2] = 0x3 & (ABSTRACT_DATA0_CMD>>30);
			    for (ulCount=0; (ulCount*32) < ulDrLength; ulCount++)
			       put_wvalue(JTAG_TDI_BUF(usBufCnt) + (ulCount*4), ulDmiAccessData[ulCount]);
			}
			else
			{
				// TODO: Double check if this is the right way
			    for (ulCount=0; (ulCount*32) < ulDrLength; ulCount++)
			    	put_wvalue(JTAG_TDI_BUF(usBufCnt) + (ulCount*4), 0x00000000);
			}
			put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usBufCnt)); //Start buffer X scan

			//if we don't wait for each scan to complete,load fails when this code is run from Flash (no issue when run from ext SRAM)
			while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usBufCnt));

			//Get TDO output to check busy state
		    for (ulCount=0; (ulCount*32) < ulDrLength; ulCount++)
		       put_wvalue(JTAG_TDI_BUF(usBufCnt) + (ulCount*4), ulDmiAccessDataNOP[ulCount]);
			ulCount = 0;
			for ( ; (ulCount*32) < ulDrLength; ulCount++)
				ulDmiDataOut[ulCount] = get_wvalue(JTAG_TDO_BUF(usBufCnt) + (ulCount*4));
			// mask excessive bits in last words
			if (ulDrLength % 32)
				ulDmiDataOut[ulCount-1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulDrLength % 32)));
			if(( ulDmiDataOut[0] & RISCV_DTM_RISCV_DMI_OP ) == 3)
			{
				//printf("[RISCVL_DmWriteMultiple]fine tuning. Current tyTmsForDR.usPostTmsCount:%d\n",gtyTmsForDR.usPreTmsCount);
				gtyTmsForDR.usPreTmsCount += 1;          gtyTmsForDR.usPreTmsBits <<= 1;
				gtyTmsForDR.usPostTmsCount += 1;         gtyTmsForDR.usPostTmsBits = 0x03;
				(void)JtagSetTMS(&gtyTmsForIR, &gtyTmsForDR, &gtyTmsForIR, &gtyTmsForDR);

				ulIrVal = RISCV_DTM_DTMCS;
				//called for entering to JTAG DTM REGISTER, DTM CONTROL AND STATUS
				ulResult = JtagScanIR(&ulIrVal,NULL);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
				ulIrData = RISCV_DTM_DTMCS_DMIRESET;
				//called for Dmireset in  DTM CONTROL AND STATUS
				ulResult = JtagScanDR(RISCV_DTM_RISCV_DMI_DATA_LENGTH, &ulIrData, ulDmiDataOut);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
				ulIrVal = RISCV_DTM_DMI;
				//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
				ulResult = JtagScanIR(&ulIrVal, ulDmiDataOut);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
				//called for making jtag input data for JtagScanDR, DMI_OP_NOP
				//	RISCVL_DmiAccessDataIn(ulAddress, RISCV_DATA_NULL, RISCV_DTM_RISCV_DMI_OP_NOP, ulDmiAccessDataOut);
				//making for JtagScanDR, DMI_OP_NOP
				ulResult = JtagScanDR(ulDrLength, ulDmiAccessData, ulDmiDataOut);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
			}
		}
	}

	//lets wait for scan to finish
	while (get_hvalue(JTAG_JASR)) ;  // waiting to sent all buffers
	put_hvalue(JTAG_JSCTR, usReg);	// disable AutoScan

	*pulDataRet = *pulDataRet;
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_DmWrite
     Engineer: CBR
        Input: unsigned long ulAbits - abit size
        	   unsigned long ulAddress - address for debug module register
        	   unsigned long ulData - 32 bit data to be written in jtag
        	   unsigned long *pulData - pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: write operation for Debug module
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_DmWrite(unsigned long ulAbits, unsigned long ulAddress, unsigned long ulData, unsigned long *pulData)
{
	unsigned long ulDmiAccessDataOut[3] = {0};
	unsigned long ulJtagScanDROut[3] = {0};
	unsigned long ulIrVal = 0;
	unsigned long ulIrData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	//TyJtagTmsSequence tyTmsForIR, tyTmsForDR;

	//printf("[RISCVL_DmWrite]\n");
	//called for making jtag input data for JtagScanDR Write
	RISCVL_DmiAccessDataIn(ulAddress, ulData, RISCV_DTM_RISCV_DMI_OP_WRITE, ulDmiAccessDataOut);

	//passed the value to jtag scan dr and output is not required
	ulResult = JtagScanDR(ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH, ulDmiAccessDataOut, NULL);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//called for making jtag input data for JtagScanDR
	RISCVL_DmiAccessDataIn(ulAddress, RISCV_DATA_NULL, RISCV_DTM_RISCV_DMI_OP_NOP, ulDmiAccessDataOut);

	//passed the value to jtag scan dr
	ulResult = JtagScanDR(ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH, ulDmiAccessDataOut, ulJtagScanDROut);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//condition to check JtagScanDR last to bit equal to value 3
	if(( ulJtagScanDROut[0] & RISCV_DTM_RISCV_DMI_OP ) == 3)
	{
		//printf("[RISCVL_DmWrite]fine tuning. Current tyTmsForDR.usPostTmsCount:%d\n",gtyTmsForDR.usPreTmsCount);
		gtyTmsForDR.usPreTmsCount += 1;          gtyTmsForDR.usPreTmsBits <<= 1;
		gtyTmsForDR.usPostTmsCount += 1;         gtyTmsForDR.usPostTmsBits = 0x03;
		(void)JtagSetTMS(&gtyTmsForIR, &gtyTmsForDR, &gtyTmsForIR, &gtyTmsForDR);

		ulIrVal = RISCV_DTM_DTMCS;
		//called for entering to JTAG DTM REGISTER, DTM CONTROL AND STATUS
		ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulIrData = RISCV_DTM_DTMCS_DMIRESET;
		//called for Dmireset in  DTM CONTROL AND STATUS
		ulResult = JtagScanDR(RISCV_DTM_RISCV_DMI_DATA_LENGTH, &ulIrData, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulIrVal = RISCV_DTM_DMI;
		//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
		ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		//called for making jtag input data for JtagScanDR, DMI_OP_NOP
		RISCVL_DmiAccessDataIn(ulAddress, RISCV_DATA_NULL, RISCV_DTM_RISCV_DMI_OP_NOP, ulDmiAccessDataOut);
		//making for JtagScanDR, DMI_OP_NOP
		ulResult = JtagScanDR(ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH, ulDmiAccessDataOut, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	*pulData = (((ulJtagScanDROut[0] >> RISCV_DTM_RISCV_DMI_OP_LENGTH) | (ulJtagScanDROut[1] << 30)) & 0xFFFFFFFF) ;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_Halt
     Engineer: CBR
        Input: unsigned long ulHartid - hartid for a particular hart
        	   unsigned long ulAbits - size of abits
       Output: unsigned long - error code (ERR_xxx)
  Description: Riscv halt functions
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_Halt(unsigned long ulHartid, unsigned long ulAbits)
{
	unsigned long ulHartsello = 0;
	unsigned long ulHartselhi = 0;
	unsigned long ulData = 0;
	unsigned long ulJtagScanDROut[3] = {0};
	unsigned long ulIrVal = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	char bHalt = false;
	int i = 0;
	unsigned long ulTimer = 0;

	ulIrVal = RISCV_DTM_DMI;
	//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
	ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//making the hartsello and hartselhi to align in 20bits
	ulHartsello = (ulHartid & 0x3FF)<< RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET;
	ulHartselhi = ((ulHartid >> 10)& 0x3FF) << RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET;

	//select the current hart by writing hart id to hartsello and hartselhi fields
	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,ulHartsello | ulHartselhi | RISCV_DMI_DMCONTROL_DMACTIVE, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read the dmstatus to check if hart is halted
	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//if RISCV_DMI_DMSTATUS_ALLHALTED and RISCV_DMI_DMSTATUS_ALLHALTED are not true then go inside loop and Halt the hart otherwise do not halt just skip it .
	if(!(ulData & RISCV_DMI_DMSTATUS_ALLHALTED) && !(ulData & RISCV_DMI_DMSTATUS_ANYHALTED ))
	{
	do
	  {
		//write operation for Debug module control, haltreq and dmactive
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL, RISCV_DMI_DMCONTROL_HALTREQ  | RISCV_DMI_DMCONTROL_DMACTIVE, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//read the dmstatus to check the hart is halted
		for(i =0; i < MINIMUM_HALT_RESUME_COUNT; i++)
		{
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			if((ulData & RISCV_DMI_DMSTATUS_ALLHALTED) || ( ulData & RISCV_DMI_DMSTATUS_ANYHALTED ))
			{
				// making DMActive to set
				ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL, RISCV_DMI_DMCONTROL_DMACTIVE, &ulData);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;

				bHalt = true;
				break;
			}
		}
		if (bHalt == true) //target is halted, exit loop
		{
			break;
		}
		else //Halt operation failed, try again
		{
			timer_100ms(FALSE);
			ulTimer++;
		}
	}while(ulTimer <TIMERCOUNT_HALF_SECOND);

	if(ulTimer == TIMERCOUNT_HALF_SECOND)
	{
		return ERR_RISCV_ERROR_HALT_TIMEOUT;
	}

	}

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_MultipleHalt
     Engineer: CBR
        Input: unsigned long *pulRiscvHalt - pointer to store data
        	   unsigned long ulHart - Number of harts
        	   unsigned long ulAbits - size of abits
       Output: unsigned long - error code (ERR_xxx)
  Description: Riscv halt fucntions
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_MultipleHalt(unsigned long ulHart, unsigned long ulAbits, unsigned long *pulRiscvHalt)
{
	unsigned long ulData = 0;
	unsigned long ulJtagScanDROut[3] = {0};
	unsigned long ulIrVal = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulTimeOut = 0;
	unsigned long ulHaWindowSelValue = 0;
	unsigned long ulHaWindowValue = 0;

	ulHaWindowSelValue = ulHart / 32;
	ulHaWindowValue = ulHart % 32;
	do
	{
		ulIrVal = RISCV_DTM_DMI;
		//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
		ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_HAWINDOWSEL,ulHaWindowSelValue,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_HAWINDOW,ulHaWindowValue,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,RISCV_DMI_DMCONTROL_HALTREQ | RISCV_DMI_DMCONTROL_HASEL | RISCV_DMI_DMCONTROL_DMACTIVE ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		//read operation for Debug module status
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulTimeOut++;
	}
	while(!(( ulData & RISCV_DMI_DMSTATUS_ALLHALTED ) || ( ulData & RISCV_DMI_DMSTATUS_ANYHALTED )) && ( ulTimeOut < 10000 ));
	*pulRiscvHalt =  ulData ;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_Fence_i
     Engineer: CBR
        Input: unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - Arch size
        	   unsigned long *puldataOut - pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Fence_i function
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_Fence_i(unsigned long ulAbits, unsigned long ulArchSize, unsigned long *puldataOut)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;
	unsigned long ulArraySize = 0x2;

	//set array size for abstract command for 64 bits
	if(ulArchSize == 64)
	{
		ulArraySize = 0x3;
	}

	//write fence_i instruction  to program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_FENCE_i,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//execute fence_i instructions
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, RISCV_GPRS_S1_VALUE , RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	*puldataOut = ulData ;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_Execute_Fence_i
     Engineer: CBR
        Input: unsigned long ulFenceSupport - Fence supported or not
        	   unsigned long ulAbits - size of abits
               unsigned long ulArchSize - Arch size
        	   unsigned long ulDirectAccessToCsr - Weather direct access to Csr supported or not
       Output: unsigned long - error code (ERR_xxx)
  Description: Execute the instruction fence
Date           Initials    Description
06-Mar-2019    SPC         Initial
 ****************************************************************************/
unsigned long RISCVL_Execute_Fence_i(unsigned long ulFenceSupport, unsigned long ulAbits, unsigned long ulArchSize, unsigned long ulDirectAccessToCsr)
{
	unsigned long ulValue=1;
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;

	if (ulFenceSupport) {
		ulResult = RISCVL_Fence_i(ulAbits, ulArchSize, &ulData);
	} else {
		//NOTE: It is for WDC. WDC does not have program buffer
		//TODO: Check whether it is applicable for other targets
		ulResult = RISCVL_WriteSingleRegister(&ulValue, RISCV_WDC_DMST_CSR, ulArchSize, ulAbits, ulDirectAccessToCsr);
		//	ulResult = 	RISCVL_WriteCsr(ulAbits, ulArchSize, ulDirectAccessToCsr, RISCV_WDC_DMST_CSR,&ulValue);
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_Resume
     Engineer: CBR
        Input: unsigned long ulHartid - hartid for a particular hart
        	   unsigned long ulAbits - size of abits
        	   unsigned long *pulRiscvResume - pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Riscv resume function
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_Resume(unsigned long ulHartid,  unsigned long ulAbits,unsigned long ulFenceSupport,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned long *pulRiscvResume)
{
	unsigned long ulHartsello = 0;
	unsigned long ulHartselhi = 0;
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	char bResume = false;
	int i = 0;
	unsigned long ulTimer = 0;

	RISCVL_Execute_Fence_i(ulFenceSupport, ulAbits, ulArchSize, ulDirectAccessToCsr);

	//making the hartsello and hartselhi to align in 20bits
	ulHartsello = (ulHartid & 0x3FF)<< RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET;
	ulHartselhi = ((ulHartid >> 10)& 0x3FF) << RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET;
	do
	{
		//write operation for Debug module control, haltreq and dmactive
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL, RISCV_DMI_DMCONTROL_RESUMEREQ | ulHartsello | ulHartselhi | RISCV_DMI_DMCONTROL_DMACTIVE,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//read the dmstatus to check the hart is resume
		for(i =0; i<MINIMUM_HALT_RESUME_COUNT; i++)
		{
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			if(( ulData & RISCV_DMI_DMSTATUS_ALLRESUMEACK) || ( ulData & RISCV_DMI_DMSTATUS_ANYRESUMEACK ))
			{
				//if resume, make debug module to reset(implemented in Openocd)
				ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL, ulHartsello | ulHartselhi | RISCV_DMI_DMCONTROL_DMACTIVE,&ulData);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
				//true to come out of all loop
				bResume = true;
				break;
			}
		}
		if (bResume == true) //target is resumed, exit loop
		{
			break;
		}
		else //resume operation failed, try again
		{
			timer_100ms(FALSE);
			ulTimer++;
		}
	}while(ulTimer <TIMERCOUNT_HALF_SECOND);

	if(ulTimer == TIMERCOUNT_HALF_SECOND)
	{
		return ERR_RISCV_ERROR_RESUME_TIMEOUT;
	}
		*pulRiscvResume =  ulData ;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_MultipleResume
     Engineer: CBR
        Input: unsigned long *pulRiscvResume - pointer to store data
        	   unsigned long ulHart - Number of hart in debug module
        	   unsigned long ulAbits - size of abits
       Output: unsigned long - error code (ERR_xxx)
  Description: Riscv resume function
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_MultipleResume(unsigned long ulHart,  unsigned long ulAbits, unsigned long *pulRiscvResume)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulTimeOut = 0;
	unsigned long ulHaWindowSelValue = 0;
	unsigned long ulHaWindowValue = 0;

	ulHaWindowSelValue = ulHart / 32;
	ulHaWindowValue = ulHart % 32;

	do
	{
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_HAWINDOWSEL,ulHaWindowSelValue,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_HAWINDOW,ulHaWindowValue,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,RISCV_DMI_DMCONTROL_RESUMEREQ | RISCV_DMI_DMCONTROL_HASEL | RISCV_DMI_DMCONTROL_DMACTIVE ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		//read operation for Debug module status
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulTimeOut++;
	}
	while(!(( ulData & RISCV_DMI_DMSTATUS_ALLRESUMEACK) || ( ulData & RISCV_DMI_DMSTATUS_ANYRESUMEACK)) && (ulTimeOut < 10000 ));
	*pulRiscvResume =  ulData ;
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_StatusProc
     Engineer: CBR
        Input: unsigned long *pulRiscvStatusProc - pointer to store data
        	   unsigned long ulHartid - hartid for a particular hart
        	   unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: Riscv status proc
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_StatusProc(unsigned long ulHartid, unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr ,unsigned long *pulRiscvStatusProc,unsigned long *pulCause)
{
	unsigned long ulHartsello = 0;
	unsigned long ulHartselhi = 0;
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulHalted = 1;
	unsigned long ulOut[2] = {0};
	unsigned long ulAddress = 0;
	//	unsigned long ulWriteCsr[2] = {0};

	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMCONTROL, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//making the hartsello and hartselhi to align in 20bits
	ulHartsello = (ulHartid & 0x3FF)<< RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET;
	ulHartselhi = ((ulHartid >> 10)& 0x3FF) << RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET;
	//write operation for Debug module control, haltreq and dmactive
	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,ulHartsello | ulHartselhi | RISCV_DMI_DMCONTROL_DMACTIVE,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//read operation for Debug module status
	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	if ( ( ulData & RISCV_DMI_DMSTATUS_ALLHALTED ) || ( ulData & RISCV_DMI_DMSTATUS_ANYHALTED ) )
	{
		ulAddress = RISCV_CSR_DSCR;
		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulOut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		*pulRiscvStatusProc=ulHalted;
		*pulCause = ((ulOut[0] & RISCV_CSR_DCSR_CAUSE_CHECK) >> RISCV_CSR_DCSR_CAUSE_OFFSET);
	}
	else
	{
		*pulRiscvStatusProc = ! ulHalted ;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_GprsAndDpcRead
     Engineer: CBR
        Input: unsigned long *pulRiscvGprsAndDpcRead - pointer to store data
        	   unsigned long ulRegisterLenght - number of gprs and  one pc
        	   unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read gprs and dpc
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_GprsAndDpcRead(unsigned long ulRegisterLenght,
		unsigned long ulAbits, unsigned long ulArchSize,unsigned long ulDirectAccessToCsr, unsigned long *pulRiscvGprsAndDpcRead)
{
	unsigned long ulData[2] = {0};
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAddress = 0;
	//calling function for read gprs
	ulResult = RISCVL_RegisterAbsAccessRegGprsRead( ulRegisterLenght,ulAbits,ulArchSize,ulDirectAccessToCsr,pulRiscvGprsAndDpcRead);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulAddress = RISCV_CSR_DPC;
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	if( ulArchSize == 32)
	{
		pulRiscvGprsAndDpcRead[ulRegisterLenght - 1] = ulData[0];
	}
	else if(ulArchSize == 64)
	{
		pulRiscvGprsAndDpcRead[(ulRegisterLenght*2) - 2] = ulData[0];
		pulRiscvGprsAndDpcRead[(ulRegisterLenght*2) - 1] = ulData[1];
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_RegisterAbsAccessRegGprsRead
     Engineer: CBR
        Input: unsigned long *pulRiscvReadDataOut - pointer to store data
        	   unsigned long ulRegisterLenght - number of gprs
        	   unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command gprs read register
Date           Initials    Description
28-Sep-2018    CBR         Initial
 ****************************************************************************/
unsigned long RISCVL_RegisterAbsAccessRegGprsRead(unsigned long ulRegisterLenght,
		unsigned long ulAbits, unsigned long ulArchSize,unsigned long ulDirectAccessToCsr, unsigned long *pulRiscvReadDataOut)
{
	//	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	//	unsigned long ulArraySize = 0;
	unsigned long ulGprsAddress = RISCV_GPRS_START_ADDR;
	unsigned int j=0;
	unsigned int i;

	for ( i = 0; i < ulRegisterLenght - 1; i++ )
	{
		ulResult = RISCVL_ReadSingleRegister(ulGprsAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,pulRiscvReadDataOut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		if (ulArchSize == 32)
		{
			pulRiscvReadDataOut[i] = pulRiscvReadDataOut[0];
		}
		else if (ulArchSize == 64)
		{
			pulRiscvReadDataOut[j] = pulRiscvReadDataOut[0];
			j++;
			pulRiscvReadDataOut[j] = pulRiscvReadDataOut[1];
			j++;
		}
		ulGprsAddress++;
	}

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_RegisterAbsAccessRegGprsWrite
     Engineer: CBR
        Input: unsigned long *pulRiscvWriteDataIn - pointer to pass the data data
               unsigned long ulRegisterLenght - number of gprs
        	   unsigned long ulAbits - size of abits
       	       unsigned long ulArchSize - architecture bit size
       	       unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command gprs write registers
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_RegisterAbsAccessRegGprsWrite(unsigned long ulRegisterLenght, unsigned long *pulRiscvWriteDataIn, unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,
		unsigned long ulAbits)
{
	//	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	//	unsigned long ulArraySize = 0;
	unsigned long ulGprsAddress = RISCV_GPRS_START_ADDR;
	unsigned int i;
	unsigned int j = 0;
	unsigned long ulWriteDataIn[2] = {0};
	
	for ( i = 0; i < ulRegisterLenght - 1; i++ )
	{
		//		ulGprsAddress = RISCV_GPRS_START_ADDR + ulGprsValue;
		if(ulArchSize == 32)
		{
			ulWriteDataIn[0]	= pulRiscvWriteDataIn[i];
		}
		else if(ulArchSize == 64)
		{
			ulWriteDataIn[0]	= pulRiscvWriteDataIn[j];
			j++;
			ulWriteDataIn[1]	= pulRiscvWriteDataIn[j];
			j++;
		}
		//writing to particular register register
		ulResult = RISCVL_WriteSingleRegister(ulWriteDataIn,ulGprsAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulGprsAddress++;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_RegisterAbsAccessSingleRead
     Engineer: CBR
        Input: unsigned long *pulRiscvReadDataOut - pointer to store data
        	   unsigned long ulGprsValue - particular gprs value
        	   unsigned long ulAbits - lenght of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
        	   unsigned long ulDoublePrecision - double precision for float
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command Single read register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_RegisterAbsAccessSingleRead(unsigned long ulGprsValue,unsigned long ulArchSize,unsigned long ulAbits,unsigned long ulDirectAccessToCsr,unsigned long ulDoublePrecision,unsigned long *pulRiscvReadDataOut)
{
	unsigned long ulAddress = 0;
	unsigned long ulout[2] = {0};
	unsigned long ulResult = ERR_NO_ERROR;
	//gprs, 0-31
	if(ulGprsValue < RISCV_CSR_DPC_FROM_GDB)
	{
		ulAddress = RISCV_GPRS_START_ADDR + ulGprsValue;
		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,pulRiscvReadDataOut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	//dpc, 32
	else if(ulGprsValue == RISCV_CSR_DPC_FROM_GDB)
	{
		ulAddress = RISCV_CSR_DPC;
		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,pulRiscvReadDataOut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	//floating point, 33-64
	else if((ulGprsValue > RISCV_CSR_DPC_FROM_GDB) && (ulGprsValue < RISCV_GDB_CSR_START_ADDRESS))
	{
		//ulAddress = RISCV_FLOATING_POINT_REGISTER_START_Address + (ulGprsValue - 0x21);//0x21 = 33
		//ulAddress = RISCV_FLOATING_POINT_REGISTER_START_Address + (ulGprsValue - 0x21);//0x21 = 33(ulGprsValue - 0x21);//0x21 = 33
		ulAddress = ulGprsValue;
		ulResult = RISCVL_ReadSingleFloating(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulDoublePrecision,pulRiscvReadDataOut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	//dcsr, 32
	else if(ulGprsValue == RISCV_CSR_DCSR_FROM_GDB)
	{
		ulAddress = RISCV_CSR_DSCR;
		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulout);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		pulRiscvReadDataOut[0] = (ulout[0] & (RISCV_CSR_DCSR_PRV)) / ((ulout[0]) & ~((RISCV_CSR_DCSR_PRV) << 1));
		pulRiscvReadDataOut[1] = ulout[1];
	}
	//csr, starts with 65
	else if((ulGprsValue > RISCV_GDB_FLOAT_END_ADDRESS) && (ulGprsValue != RISCV_CSR_DCSR_FROM_GDB))
	{
		ulAddress = RISCV_CSR_START_ADDR + (ulGprsValue - 0x41);//0x41=65
		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,pulRiscvReadDataOut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_ReadSingleRegister
     Engineer: CBR
        Input: unsigned long *pulRiscvReadDataOut - pointer to store data
        	   unsigned long ulRegisterAddress - particular csr register address
        	   unsigned long ulAbits - lenght of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command gprs Single read register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadSingleRegister(unsigned long ulRegisterAddress,unsigned long ulArchSize,
		unsigned long ulAbits,unsigned long ulDirectAccessToCsr ,unsigned long *pulRiscvReadDataOut)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0;
	unsigned long j=0;
	unsigned long ulOut[2] = {0};
	unsigned long ulProgramBufferValue = RISCV_READ_INSTRUCTION_PROGRAM_BUFFER;
	switch (ulArchSize)
	{
	//32bit
	case 32:
		ulArraySize = 0x2 ;
		//GPRS
		if((ulRegisterAddress > RISCV_CSR_LAST_ADDR) && (ulRegisterAddress < RISCV_FLOATING_POINT_REGISTER_START_Address) )
		{
			//ulRegister = RISCV_GPRS_START_VALUE + RISCVL_AbstractDirectRegisterRead;
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress , RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//read the data0 register
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DATA0, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			*pulRiscvReadDataOut = ulData;
		}
		//CSR direct access
		else if((ulRegisterAddress < RISCV_GPRS_START_ADDR) && ulDirectAccessToCsr)
		{
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress , RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//read the data0 register
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DATA0, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			*pulRiscvReadDataOut = ulData;
		}
		//CSR access with program buffer
		else if( (ulRegisterAddress < RISCV_GPRS_START_ADDR) && (ulDirectAccessToCsr == 0) )
		{
			ulProgramBufferValue = ((ulRegisterAddress << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | ulProgramBufferValue);
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			*pulRiscvReadDataOut = ulData;
		}
	break;
	//64 bit
	case 64:
		ulArraySize = 0x3 ;
		//gprs
		if((ulRegisterAddress > RISCV_CSR_LAST_ADDR) && (ulRegisterAddress < RISCV_FLOATING_POINT_REGISTER_START_Address) )
		{
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress, RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//read the data1 register
			ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadDataOut[j] = ulData;
			j++;
			//read the data0 register
			ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA1,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadDataOut[j] = ulData;
		}
		//csr direct access
		else if((ulRegisterAddress < RISCV_GPRS_START_ADDR) && ulDirectAccessToCsr)
		{
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress, RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//read the data1 register
			ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadDataOut[j] = ulData;
			j++;
			//read the data0 register
			ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA1,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadDataOut[j] = ulData;
		}
		//csr access with program buffer
		else if( (ulRegisterAddress < RISCV_GPRS_START_ADDR) && (ulDirectAccessToCsr == 0) )
		{
			ulProgramBufferValue = ((ulRegisterAddress << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | ulProgramBufferValue);
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,ulOut);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadDataOut[j] = ulOut[0];
			j++;
			pulRiscvReadDataOut[j] = ulOut[1];
		}
	break;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_ReadSingleFloating
     Engineer: CBR
        Input: unsigned long *pulRiscvReadDataOut - pointer to store data
        	   unsigned long ulRegisterAddress - particular csr register address
        	   unsigned long ulAbits - lenght of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
        	   unsigned long ulDoublePrecision - say about double precision in floating point
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command float Single read register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadSingleFloating(unsigned long ulRegisterAddress,unsigned long ulArchSize,
		unsigned long ulAbits,unsigned long ulDirectAccessToCsr ,unsigned long ulDoublePrecision ,unsigned long *pulRiscvReadFloatDataOut)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0;
	//	unsigned long j=0;
	unsigned long ulOut[2] = {0};
	unsigned long ulProgramBufferValue = 0;
	unsigned long ulTempCsrFloat[2] = {0};
	unsigned long ulTempCsrFloatRestore = 0;

	//ulRegisterAddress = RISCV_FLOATING_POINT_REGISTER_START_Address + (ulRegisterAddress - 0x21);//0x21 = 33(ulGprsValue - 0x21);//0x21 = 33
	ulRegisterAddress = (ulRegisterAddress - RISCV_GDB_FLOAT_START_ADDRESS);//0x21 = 33
	switch (ulArchSize)
	{
	//32bit
	case 32:
		ulArraySize = 0x2 ;
		//Reading csr, MSTATUS
		if(ulDirectAccessToCsr)
		{
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, RISCV_CSR_MSTATUS , RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//read the data0 register
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DATA0, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			ulTempCsrFloat[0] = ulData;
			ulTempCsrFloatRestore = ulData;
		}
		else
		{
			ulProgramBufferValue = (( RISCV_CSR_MSTATUS << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | RISCV_READ_INSTRUCTION_PROGRAM_BUFFER);
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			ulTempCsrFloat[0] = ulData;
			ulTempCsrFloatRestore = ulData;
		}
		//enable FS in csr,MSTATUS
		if(((ulTempCsrFloat[0]) & MSTATUS_FS )==0)
		{
			ulTempCsrFloat[0] = (ulTempCsrFloat[0] | MSTATUS_FS);
			ulResult = RISCVL_WriteSingleRegister(ulTempCsrFloat,RISCV_CSR_MSTATUS,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		//32 bit double precision
		if(ulDoublePrecision)
		{
			//ulRegisterAddress = ulRegisterAddress - RISCV_FLOATING_POINT_REGISTER_START_Address;
			ulProgramBufferValue = ((ulRegisterAddress << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | RISCV_DOUBLE_PRECISION_32_READ );
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			*pulRiscvReadFloatDataOut = ulData;
		}
		//single precision float
		else
		{
			//ulRegisterAddress = ulRegisterAddress - RISCV_FLOATING_POINT_REGISTER_START_Address;
			ulProgramBufferValue = ((ulRegisterAddress << 15) | RISCV_FLOAT_READ );
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			*pulRiscvReadFloatDataOut = ulData;
		}
		//restore in csr,MSTATUS
		if(((ulTempCsrFloat[0]) & MSTATUS_FS )==0)
		{
			ulTempCsrFloat[0] = ulTempCsrFloatRestore;
			ulResult = RISCVL_WriteSingleRegister(ulTempCsrFloat,RISCV_CSR_MSTATUS,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
	break;
	//64 bit
	case 64:
		ulArraySize = 0x3 ;
		//Reading csr, MSTATUS
		if(ulDirectAccessToCsr)
		{
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, RISCV_CSR_MSTATUS , RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//read the data0 register
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DATA0, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			ulTempCsrFloat[0] = ulData;
			ulTempCsrFloatRestore = ulData;
			//read the data1
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DATA1, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			ulTempCsrFloat[1] = ulData;
		}
		else
		{
			ulProgramBufferValue = (( RISCV_CSR_MSTATUS << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | RISCV_READ_INSTRUCTION_PROGRAM_BUFFER);
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,ulOut);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			ulTempCsrFloat[0] = ulOut[0];
			ulTempCsrFloatRestore = ulOut[0];
			ulTempCsrFloat[1] = ulOut[1];
		}
		//enable FS in csr,MSTATUS
		if(((ulTempCsrFloat[0]) & MSTATUS_FS )==0)
		{
			ulTempCsrFloat[0] = (ulTempCsrFloat[0] | MSTATUS_FS);
			ulResult = RISCVL_WriteSingleRegister(ulTempCsrFloat,RISCV_CSR_MSTATUS,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		//64 bit double precision
		if(ulDoublePrecision)
		{
			//ulRegisterAddress = ulRegisterAddress - RISCV_FLOATING_POINT_REGISTER_START_Address;
			ulProgramBufferValue = ((ulRegisterAddress << 15) | RISCV_DOUBLE_PRECISION_64_READ );
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,ulOut);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadFloatDataOut[0] = ulOut[0];
			pulRiscvReadFloatDataOut[1] = ulOut[1];
		}
		//single precision float
		else
		{
			//ulRegisterAddress = ulRegisterAddress - RISCV_FLOATING_POINT_REGISTER_START_Address;
			ulProgramBufferValue = ((ulRegisterAddress << 15) | RISCV_FLOAT_READ );
			ulResult = RISCVL_ProgramBufferReadCsr(ulAbits,ulProgramBufferValue,ulArchSize,ulOut);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			pulRiscvReadFloatDataOut[0] = ulOut[0];
			pulRiscvReadFloatDataOut[1] = ulOut[1];
		}
		//restore in csr,MSTATUS
		if(((ulTempCsrFloat[0]) & MSTATUS_FS )==0)
		{
			ulTempCsrFloat[0] = ulTempCsrFloatRestore;
			ulResult = RISCVL_WriteSingleRegister(ulTempCsrFloat,RISCV_CSR_MSTATUS,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
	break;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_RegisterAbsAccessGprsAndFprsSingleWrite
     Engineer: CBR
        Input: unsigned long pulRiscvWriteDataIn - data to be written
        	   unsigned long ulGprsValue - particular gprs address
        	   unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command gprs ad floating point register write a single register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_RegisterAbsAccessGprsAndFprsSingleWrite(unsigned long *pulRiscvWriteDataIn,unsigned long ulGprsValue, unsigned long ulArchSize, unsigned long ulAbits,unsigned long ulDirectAccessToCsr)
{
	//	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	//	unsigned long ulArraySize = 0;
	unsigned long ulGprsAddress = 0;
	//ulArchSize = 32;
	//gprs
	if(ulGprsValue < RISCV_CSR_DPC_FROM_GDB)
	{
		ulGprsAddress = RISCV_GPRS_START_ADDR + ulGprsValue;
		//writing to particular register register
		ulResult = RISCVL_WriteSingleRegister(pulRiscvWriteDataIn,ulGprsAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	//Floating point
	else if((ulGprsValue > RISCV_CSR_DPC_FROM_GDB) && (ulGprsValue < RISCV_GDB_CSR_START_ADDRESS))
	{
		ulGprsAddress = RISCV_FLOATING_POINT_REGISTER_START_Address + (ulGprsValue - RISCV_GDB_FLOAT_START_ADDRESS);
		//writing to particular register register
		ulResult = RISCVL_WriteSingleRegister(pulRiscvWriteDataIn,ulGprsAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_WriteSingleRegister
     Engineer: CBR
        Input: unsigned long pulRiscvWriteDataIn - data to be written
        	   unsigned long ulRegisterAddress - particular register address
        	   unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: function for abstract access register using abstract command gprs write a single register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteSingleRegister(unsigned long *pulRiscvWriteDataIn,unsigned long ulRegisterAddress, unsigned long ulArchSize, unsigned long ulAbits,unsigned long ulDirectAccessToCsr)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0;
	unsigned long ulProgramBufferValue = RISCV_WRITE_INSTRUCTION_PROGRAM_BUFFER;
	//ulArchSize = 32;
	switch (ulArchSize)
	{
	//32bit
	case 32:
		ulArraySize = 0x2 ;
		//ulRiscvWriteDataIn[0] = pulRiscvWriteDataIn[0];
		//gprs
		if((ulRegisterAddress > RISCV_CSR_LAST_ADDR) && (ulRegisterAddress < RISCV_FLOATING_POINT_REGISTER_START_Address) )
		{
			//write on data0
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0,pulRiscvWriteDataIn[0],&ulData);// pulRiscvWriteDataIn[0],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//writing to particular register register
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress,RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER ,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		//csr direct access
		else if((ulRegisterAddress < RISCV_GPRS_START_ADDR) && ulDirectAccessToCsr)
		{
			//write on data0
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0,pulRiscvWriteDataIn[0],&ulData);// pulRiscvWriteDataIn[0],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//ulRegister = RISCV_CSR_DPC;
			//write program counter
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress,RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER ,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		//csr program buffer access
		else if( (ulRegisterAddress < RISCV_GPRS_START_ADDR) && (ulDirectAccessToCsr == 0) )
		{
			ulProgramBufferValue = ((ulRegisterAddress << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | ulProgramBufferValue);
			//write the csr with value to program buffer
			ulResult = RISCVL_ProgramBufferWriteCsr(ulAbits, ulProgramBufferValue , ulArchSize, pulRiscvWriteDataIn);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
	break;
	//64 bit
	case 64:
		ulArraySize = 0x3 ;
		//gprs
		if((ulRegisterAddress > RISCV_CSR_LAST_ADDR) && (ulRegisterAddress < RISCV_FLOATING_POINT_REGISTER_START_Address) )
		{
			//write on data1
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0, pulRiscvWriteDataIn[0],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//write on data0
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA1, pulRiscvWriteDataIn[1],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//writing to particular register register
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, ulRegisterAddress ,RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER ,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		//csr direct access
		else if((ulRegisterAddress < RISCV_GPRS_START_ADDR) && ulDirectAccessToCsr)
		{
			//write on data1
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0, pulRiscvWriteDataIn[0],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//write on data0
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA1, pulRiscvWriteDataIn[1],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//writing to particular register register
			ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,ulRegisterAddress,RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER ,ulArraySize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		//csr using program buffer
		else if( (ulRegisterAddress < RISCV_GPRS_START_ADDR) && (ulDirectAccessToCsr == 0) )
		{
			//write on data1
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0, pulRiscvWriteDataIn[0],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			//write on data0
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA1, pulRiscvWriteDataIn[1],&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			ulProgramBufferValue = ((ulRegisterAddress << RISCV_INSTRUCTION_TO_ADD_ADDRESS) | ulProgramBufferValue);
			//write the csr with value to program buffer
			ulResult = RISCVL_ProgramBufferWriteCsr(ulAbits, ulProgramBufferValue , ulArchSize, pulRiscvWriteDataIn);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
	break;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_SingleStep
     Engineer: CBR
        Input: unsigned long ulHartId - hart id for particular hart
        	   unsigned long ulAbits - size of abits
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: program for single step operation
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_SingleStep(unsigned long ulHartId ,unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulFenceSupport,unsigned long ulDirectAccessToCsr)
{
	unsigned long ulData[2] = {0};
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulWriteCsr[2] = {0};
	unsigned long ulOut = 0;
	unsigned long ulAddress = 0;
	char bHalt = false;
	int i = 0;
	unsigned long ulTimer = 0;

	ulAddress = RISCV_CSR_DSCR;
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulWriteCsr[0] = ( RISCV_CSR_DCSR_STEP | ulData[0]);
	if(ulArchSize == 64)
	{
		ulWriteCsr[1] = ulData[1];
	}
	//writing to particular register register
	ulResult = RISCVL_WriteSingleRegister(ulWriteCsr,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//resume the hart
	ulResult = RISCVL_Resume(ulHartId, ulAbits,ulFenceSupport,ulArchSize,ulDirectAccessToCsr, &ulOut );
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	do {
		//read the dmstatus to check the hart is halted
		for(i = 0; i < MINIMUM_HALT_RESUME_COUNT; i++)
		{
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS, &ulOut);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			if((ulOut & RISCV_DMI_DMSTATUS_ALLHALTED) || ( ulOut & RISCV_DMI_DMSTATUS_ANYHALTED ))
			{
				bHalt = true;
				break;
			}
		}
		if (bHalt == true) //target is halted, exit loop
		{
			break;
		}
		else //Halt operation failed, try again
		{
			timer_100ms(FALSE);
			ulTimer++;
		}
	}while(ulTimer < TIMERCOUNT_HALF_SECOND);

	if(ulTimer == TIMERCOUNT_HALF_SECOND)
	{
		return ERR_RISCV_ERROR_HALT_TIMEOUT;
	}

	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	ulData[0] = ulData[0] | 0x3 ;
	ulWriteCsr[0] = ( (RISCV_CSR_DCSR_STEP_RESET) & (ulData[0]));
	if(ulArchSize == 64)
	{
		ulWriteCsr[1] = ulData[1];
	}
	ulResult = RISCVL_WriteSingleRegister(ulWriteCsr,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_ProgramBufferReadCsr
     Engineer: CBR
        Input: unsigned long ulAbits - size of abits
        	   unsigned long ulProgramBufferValue - write the program buffer with csr operation macros
        	   unsigned long *ProgramBufferReadCsr - store the pointer data
        	   unsigned long ulArchSize - architecture bit size
       Output: unsigned long - error code (ERR_xxx)
  Description: read Csr
Date           Initials    Description
28--2007        VH          Initial
 *********************************************************************r*******/
unsigned long RISCVL_ProgramBufferReadCsr(unsigned long ulAbits,unsigned long ulProgramBufferValue, unsigned long ulArchSize,unsigned long *ProgramBufferReadCsr)
{
	unsigned long ulData1 = 0  ;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0x2;

	if (ulArchSize == 64)
	{
		ulArraySize = 0x3;
	}
	//write the prog buffer 0 with the respective operation
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_PROGBUF0, ulProgramBufferValue,&ulData1);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//write the ebrak instruction in program buffer 1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_PROGBUF1, RISCV_EBREAK_INSTRUCTION,&ulData1);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//operation of post execution for abstract access register
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, RISCV_GPRS_START_ADDR, RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData1);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//operation of transfer for abstract access register
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, RISCV_GPRS_S1_VALUE, RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData1);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	if (ulArchSize == 64)
	{
		ulResult = RISCVL_DmRead(ulAbits,RISCV_DMI_DATA0,&ulData1);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ProgramBufferReadCsr[0] = ulData1;
		//read the value for address used
		ulResult = RISCVL_DmRead(ulAbits,RISCV_DMI_DATA1,&ulData1);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ProgramBufferReadCsr[1] = ulData1;
	}
	else
	{
		ulResult = RISCVL_DmRead(ulAbits,RISCV_DMI_DATA0,&ulData1);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ProgramBufferReadCsr[0] = ulData1;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_ProgramBufferWriteCsr
     Engineer: CBR
        Input: unsigned long ulAbits - size of abits
        	   unsigned long ulProgramBufferValue - operation for program buffer
        	   unsigned long ulProgramBufferWriteCsr - value to be written in the particular csr register
        	   unsigned long ulArchSize - architecture bit size
       Output: unsigned long - error code (ERR_xxx)
  Description: write Csr
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ProgramBufferWriteCsr(unsigned long ulAbits, unsigned long ulProgramBufferValue, unsigned long ulArchSize, unsigned long *pulProgramBufferWriteCsr)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0x2;

	if (ulArchSize == 64)
	{
		ulArraySize = 0x3;
		//writing value for data1 register
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA1, pulProgramBufferWriteCsr[1],&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	//in data0 write the operation csr
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0, pulProgramBufferWriteCsr[0],&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//write program buffer0 with program buffer operation
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_PROGBUF0, ulProgramBufferValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//ebreak for program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_PROGBUF1, RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//post execution operation for access register
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits, RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_POSTEXEC , ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteCsr
     Engineer: CBR
        Input: unsigned long ulAbits - size of abits
        	   unsigned long ulProgramBufferWriteDpc - write the new value of dpc to move
        	   unsigned long ulArchSize - architecture bit size
        	   unsigned long ulDirectAccessToCsr - say about access to csr without program buffer
        	   unsigned long ulRegisterAddr - address for csr
       Output: unsigned long - error code (ERR_xxx)
  Description: program buffer to write the dpc
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteCsr(unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned long ulRegisterAddr,unsigned long *pulRiscvWriteDataIn)
{
	unsigned long ulResult = ERR_NO_ERROR;

	if(ulRegisterAddr == RISCV_CSR_DPC_FROM_GDB)
	{
		unsigned long ulAddress = RISCV_CSR_DPC;
		//writing to particular register register
		ulResult = RISCVL_WriteSingleRegister(pulRiscvWriteDataIn,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else
	{
		unsigned long ulAddress = RISCV_CSR_START_ADDR  + (ulRegisterAddr - RISCV_GDB_CSR_START_ADDRESS);
		//writing to particular register register
		ulResult = RISCVL_WriteSingleRegister(pulRiscvWriteDataIn,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_DtmDiscovery
     Engineer: HS
        Input: unsigned long *pulAbitsOut - pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Abits(The size of address in dmi register) for RISCV target.
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long  RISCVL_DtmDiscovery(unsigned long *pulAbitsOut,unsigned long *pulIdleOut)
{
	unsigned long ulIrVal;
	unsigned long ulDrData = 0;
	unsigned long ulJtagScanDROut;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulIdlecnt = 0;

	/*Reset TMS*/
	gtyTmsForIR.usPreTmsCount = 5;          gtyTmsForIR.usPreTmsBits = 0x06;
	gtyTmsForIR.usPostTmsCount = 4;         gtyTmsForIR.usPostTmsBits = 0x03;
	gtyTmsForDR.usPreTmsCount = 4;          gtyTmsForDR.usPreTmsBits = 0x02;
	gtyTmsForDR.usPostTmsCount = 4;         gtyTmsForDR.usPostTmsBits = 0x03;
	(void)JtagSetTMS(&gtyTmsForIR, &gtyTmsForDR, &gtyTmsForIR, &gtyTmsForDR);

	JtagResetTap(0);
	ulIrVal = RISCV_DTM_DTMCS;
	ulDrData = RISCV_DATA_NULL;
	//IR scan for DTM control and status register
	ulResult = JtagScanIR(&ulIrVal,NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	//DR scan with data 0 and get data in ulJtagScanDROut
	ulResult = JtagScanDR(RISCV_DTM_RISCV_DMI_DATA_LENGTH,&ulDrData,&ulJtagScanDROut);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	*pulAbitsOut = (ulJtagScanDROut & RISCV_DTM_DTMCS_ABITS) >> RISCV_DTM_DTMCS_ABITS_OFFSET;
	*pulIdleOut = (ulJtagScanDROut & RISCV_DTM_DTMCS_IDLE) >> RISCV_DTM_DTMCS_IDLE_OFFSET;

	//*pulVersionOut= (ulJtagScanDROut & RISCV_DTM_DTMCS_VERSION);
	ulIdlecnt = *pulIdleOut;

	//printf("[RISCVL_DtmDiscovery]fine tuning. Current tyTmsForDR.usPostTmsCount:%d\n",gtyTmsForDR.usPreTmsCount);
	gtyTmsForDR.usPreTmsCount += ulIdlecnt;          gtyTmsForDR.usPreTmsBits <<= ulIdlecnt;
	gtyTmsForDR.usPostTmsCount += ulIdlecnt;         gtyTmsForDR.usPostTmsBits = 0x03;
	(void)JtagSetTMS(&gtyTmsForIR, &gtyTmsForDR, &gtyTmsForIR, &gtyTmsForDR);

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_RegisterMechanism
     Engineer: HS
        Input: unsigned long ulAbits - Abits for target.
               unsigned long *pulAbsReg - pointer to store data
               unsigned long *pulProgramBuf-pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Returns the result for supported register mechanisms.
                1: Access register support is there
                2: Program buffer support is there

Date           Initials    Description
0-0-200        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_RegisterMechanism(unsigned long ulAbits,unsigned long *pulAbsReg,unsigned long *pulProgramBuf)
{
	unsigned long ulIrVal;
	unsigned long  ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;
	ulIrVal =RISCV_DTM_DMI;
	//IR scan for Dmi register
	ulResult =  JtagScanIR(&ulIrVal,NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	//Address for abstract control and status register
	ulResult = RISCVL_DmRead(ulAbits,RISCV_DMI_ABSTRACTCS,&ulData);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	if((ulData & RISCV_DMI_ABSTRACTCS_DATACOUNT) >> RISCV_DMI_ABSTRACTCS_DATACOUNT_OFFSET)  //for access register
	{
		*pulAbsReg = (ulData & RISCV_DMI_ABSTRACTCS_DATACOUNT) >> RISCV_DMI_ABSTRACTCS_DATACOUNT_OFFSET;
	}
	if((ulData & RISCV_DMI_ABSTRACTCS_PROGBUFSIZE) >> RISCV_DMI_ABSTRACTCS_PROGBUFSIZE_OFFSET) //for program buffer
	{
		*pulProgramBuf = (ulData & RISCV_DMI_ABSTRACTCS_PROGBUFSIZE) >> RISCV_DMI_ABSTRACTCS_PROGBUFSIZE_OFFSET;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_SystemBusAccess
     Engineer: Harvinder Singh
        Input: unsigned long ulAbits -Abits for riscv target.
               unsigned long *pulResult -returns the result.
       Output: unsigned long - error code (ERR_xxx)
  Description: if supported returns 1 else 0.
Date           Initials    Description
0-0-200        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_SystemBusAccess(unsigned long ulAbits,unsigned long *pulResult)
{
	unsigned long ulIrVal;
	unsigned long  ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;

	ulIrVal = RISCV_DTM_DMI;

	//IR scan for Dmi register
	ulResult =  JtagScanIR(&ulIrVal,NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	//Address for system bus control and status register
	RISCVL_DmRead(ulAbits,RISCV_DMI_SBCS,&ulData);
	//TODO: Check whether we need to return the SBASize.
	if(ulData & RISCV_DMI_SBCS_SBASIZE)
	{
		//set result to 1,as it supports SB access.
		*pulResult =SYSTEM_BUS_ACCESS;
		 //checking for sbsize and store in ulSystemBusSize.
		if((ulData & RISCV_DMI_SBCS_SBACCESS128))
		{
			gtyDwRiscvConfig.ulSystemBusSize = SYSTEMBUS128;
		}
		else if((ulData & RISCV_DMI_SBCS_SBACCESS64))
		{
			gtyDwRiscvConfig.ulSystemBusSize = SYSTEMBUS64;
		}
		else if((ulData & RISCV_DMI_SBCS_SBACCESS32))
		{
			gtyDwRiscvConfig.ulSystemBusSize = SYSTEMBUS32;

		}
		else if((ulData & RISCV_DMI_SBCS_SBACCESS16))
		{
			gtyDwRiscvConfig.ulSystemBusSize = SYSTEMBUS16;
		}
		else
		{
			gtyDwRiscvConfig.ulSystemBusSize = SYSTEMBUS8;
		}
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_MemoryMechanism
     Engineer: Harvinder Singh
        Input: unsigned long ulAbits- Abits for riscv target
               unsigned long ulDataCount-No. of data registers
               unsigned long ulProgramBufCount-No. of program buffers
       Output: unsigned long - error code (ERR_xxx)
  Description: unsigned long *pulMemMechanism - returns the memory mechanism
               1:  system bus supported.
               2:  program buffer supported
               4:  Abstract access memory supported
Date           Initials    Description
30-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_MemoryMechanism(unsigned long ulAbits,unsigned long ulDataCount,unsigned long ulProgramBufCount,unsigned long *pulMemMechanism)
{
	unsigned long ulResult = ERR_NO_ERROR;

	//check system bus access is supported.
	ulResult = RISCVL_SystemBusAccess(ulAbits,pulMemMechanism);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	//the prgoram buffer count should be greater than 2 for memory access using program buffer.
	if(ulProgramBufCount>2)
	{
		*pulMemMechanism =*pulMemMechanism | PROGRAM_BUFFER_ACCESS;
	}

	//the Data register count should be gretaer than equal 2 for abstract access memory support.
	if(ulDataCount>=2)
	{
		*pulMemMechanism =*pulMemMechanism | ABSTARCT_ACCESS_MEMORY;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_HartInformation
     Engineer: HS
        Input: pointer to
       Output:
  Description: Returns how many harts are there.

Date           Initials    Description
0-0-200        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_HartInformation(unsigned long ulAbits,unsigned long *pulVersion,unsigned long *pulFenceSupport,unsigned long *pulResult)
{
	unsigned long ulIrVal;
	unsigned long ulHartHiLength,ulFinalLength,ulHartLoLength;
	unsigned long ulData,i;
	unsigned long ulResult = ERR_NO_ERROR;

	ulIrVal = RISCV_DTM_DMI;
	//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
	ulResult = JtagScanIR(&ulIrVal,NULL);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
//Reset the Debug Module and read the version
	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,RISCV_DMI_DMCONTROL_DMACTIVE,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		ulResult = RISCVL_DmRead(ulAbits,RISCV_DMI_DMSTATUS,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
	 *pulVersion = (ulData & RISCV_DMI_DMSTATUS_VERSION);
	 *pulFenceSupport = (ulData & RISCV_DMI_DMSTATUS_IMPEBREAK);
	//check hart select length
	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,RISCV_DMI_DMCONTROL_HARTSELLO | RISCV_DMI_DMCONTROL_HARTSELHI | RISCV_DMI_DMCONTROL_DMACTIVE,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read operation for Debug module control,
	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMCONTROL,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//get high and low hart select length
	ulHartHiLength = ((ulData >> RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET) & 0x3FF);
	ulHartLoLength = ((ulData >> RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET) & 0x3FF);

	//get the final length
	ulFinalLength = (ulHartHiLength << 10 ) | ulHartLoLength;

	for(i=0;i<=ulFinalLength;i++)
	{
		ulHartLoLength = (i & 0x3FF)<< RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET;
		ulHartHiLength = ((i >> 10)& 0x3FF) << RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET;

		//check hart select length
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL,ulHartLoLength | ulHartHiLength | RISCV_DMI_DMCONTROL_DMACTIVE,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//read operation for Debug module control,
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMSTATUS,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		if((ulData & RISCV_DMI_DMSTATUS_ALLNONEXISTENT) || ( ulData & RISCV_DMI_DMSTATUS_ANYNONEXISTENT))
		{
			break;
		}
	}
	*pulResult = i;
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_ArchSupport
     Engineer:
        Input: pointer to
       Output:
  Description: Returns how many harts are there.

Date           Initials    Description
0-0-200        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ArchSupport(unsigned long ulAbits,unsigned long *pulResult)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArch=128;


	if(ERR_NO_ERROR == RISCVL_CheckArc(ulAbits,ARRAY_SIZE_128_BIT))
	{
		ulArch = ulArch;
	}
	else if(ERR_NO_ERROR == RISCVL_CheckArc(ulAbits,ARRAY_SIZE_64_BIT))
	{
		ulArch = ulArch/2;
	}
	else if (ERR_NO_ERROR == RISCVL_CheckArc(ulAbits,ARRAY_SIZE_32_BIT))
	{
		ulArch = ulArch/4;
	}
	else
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;

	*pulResult = ulArch;
	return ulResult;

}

/****************************************************************************
     Function: RISCVL_AbsCSRSupport
     Engineer:
        Input: unsigned long ulAbits -Abits for target
               unsigned long ulArraySize-architecture size (32,64 bit)
               unsigned long *pulResult-pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Returns true if CSRs can be read using Abstract Command.

Date           Initials    Description
0-0-200        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_AbsCSRSupport(unsigned long ulAbits,unsigned long ulArchSize,unsigned long *pulResult)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData,ulArraySize=0;
	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
	}

	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,0x1,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_COMMAND,((RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD) | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | (RISCV_ACCESS_REGISTER_TRANSFER) | (RISCV_CSR_DSCR )),&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_ABSTRACTCS, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	if (ulData & RISCV_DMI_ABSTRACTCS_CMDERR_7)
	{
		*pulResult=ABS_REG_CSR_NOT_SUPPORTED;
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_ABSTRACTCS, RISCV_DMI_ABSTRACTCS_CMDERR_7 ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else
	{
		*pulResult = ABS_REG_CSR_SUPPORTED;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_DiscoverDebugModules
     Engineer:
        Input: unsigned long ulAbits -Abits for target
               unsigned long ulArraySize-architecture size (32,64 bit)
               unsigned long *pulResult-pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Returns true if CSRs can be read using Abstract Command.

Date           Initials    Description
0-0-200        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_DiscoverDebugModules(unsigned long ulAbits,unsigned long *pulSize,unsigned long *pulResult)
{

	//TODO: this function is not tested
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulIrVal,i=0,ulData;
	unsigned long ulCounter = 0;
	ulIrVal = RISCV_DTM_DMI;
	//assuming maximum next debug module to 100
	do
	{
		//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
		ulResult = JtagScanIR(&ulIrVal, NULL);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//read operation for Debug module control,
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_NEXTDM, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		if(!ulData)
		{
			ulIrVal = ulData;
			pulResult[i]=ulData;
			i++;
		}
		else
		{
			break;
		}
		ulCounter++;
	}
	while(ulCounter <= MAX_NEXT_DEBUG_MODULE_100);

	if(ulCounter == MAX_NEXT_DEBUG_MODULE_100)
	{
		return ERR_RISCV_ERROR_NEXT_DEBUG_MODULE_TIMEOUT;
	}
	*pulSize=i;

	return ulResult;
}


/****************************************************************************
     Function: RISCVL_CheckAbstractAutoExec
     Engineer: Harvinder Singh
        Input: unsigned long ulAbits - size of DM registers.
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Discovery options for RISCV target.
Date           Initials    Description
24-05-2019       HS          Initial
 ****************************************************************************/
unsigned long RISCVL_CheckAbsAutoExecSupport(unsigned long ulAbits)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulValue = 0xFFFF0FFF;
	unsigned long ulData = 0;

	//Write ulValue to abstract auto exec register
	ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_ABSTRACTAUTO,ulValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	//set ulData to 0 to get new value
	ulData = 0;
	//read back abstract auto exec register
	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_ABSTRACTAUTO,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//if ulData is having value,it supports abstract auto exec  register
	if(ulData)
	{
		gtyDwRiscvConfig.ucAbsAutoExecSupported  = AUTO_EXEC_SUPPORT;
	}
	else
	{
		gtyDwRiscvConfig.ucAbsAutoExecSupported = AUTO_EXEC_NOT_SUPPORT;
	}

	//Reset abstract auto exec register
	ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_ABSTRACTAUTO,0x0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_DiscoveryOptions
     Engineer: HS
        Input:pointer to data buffer for outgoing data(response)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Discovery options for RISCV target.
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_DiscoveryOptions(unsigned long ulAbitsOut,unsigned long ulIdleOut,unsigned long ulVersionOut,unsigned long ulFenceSupport,unsigned char *pucDataBuf,unsigned long *ulDataSize)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulMemMechanism = 0;
	unsigned long ulAbsReg = 0,ulProgramBuf=0;//ulCsrOut;
	unsigned long ulArchOut,ulCsrout=0;

	//Idle Count(min cycles to wait in run/test idle cycle).
	*((unsigned long *)(pucDataBuf + 0x08)) = ulIdleOut;

	//version of riscv debug specification
	*((unsigned long *)(pucDataBuf + 0x0C)) = ulVersionOut;

	//register mechanism supported by target.
	ulResult = RISCVL_RegisterMechanism(ulAbitsOut,&ulAbsReg,&ulProgramBuf);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	*((unsigned long *)(pucDataBuf + 0x10)) = ulAbsReg;//No of  data registers
	*((unsigned long *)(pucDataBuf + 0x14)) = ulProgramBuf;//No. of program buffers
	if(ulProgramBuf + ulFenceSupport < 2)
	{
		*((unsigned long *)(pucDataBuf + 0x04)) = FENCE_UNSUPPORTED;
	}
	else
	{
		*((unsigned long *)(pucDataBuf + 0x04)) = FENCE_SUPPORTED;
	}
	//memory mechanism supported by target.
	ulResult = RISCVL_MemoryMechanism(ulAbitsOut,ulAbsReg,ulProgramBuf,&ulMemMechanism);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	*((unsigned long *)(pucDataBuf + 0x18)) = ulMemMechanism;//returns memory mechanism.

	//Architecture supported by target.
	ulResult = RISCVL_ArchSupport(ulAbitsOut,&ulArchOut);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	// check CSRs access supported by target.
	ulResult = RISCVL_AbsCSRSupport(ulAbitsOut,ulArchOut,&ulCsrout);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	*((unsigned long *)(pucDataBuf + 0x1C)) = ulArchOut;
	*((unsigned long *)(pucDataBuf + 0x20)) = ulCsrout;

   //No. of triggers supported by target.
	ulResult = RISCVL_TriggerInfo(ulAbitsOut,ulArchOut,((unsigned long *)(pucDataBuf+ 0x24)),ulCsrout);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read MISA register to find out floating point supported by target.
	ulResult = RISCVL_ReadSingleRegister(RISCV_CSR_MISA,ulArchOut,ulAbitsOut,ulCsrout,((unsigned long *)(pucDataBuf + 0x28)));
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

	//returns total size of buffer.
	*ulDataSize =0x30;

	//add Ebreak command.
	ulResult = RISCVL_Ebreaks(ulAbitsOut,ulArchOut,ulCsrout);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//check Abs Auto execution supported by target.
	ulResult = RISCVL_CheckAbsAutoExecSupport(ulAbitsOut);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_ReadDMIAccessReg
     Engineer: HS,CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - address value for debug module registers
               unsigned long *pulResult  - pointer to read data from register
       Output: unsigned long - error code (ERR_xxx)
  Description: Read DM register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadDMIAccessReg(unsigned long ulAbits,unsigned long ulAddress,unsigned long *pulResult)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;

	//clear any error before accessing DM register
	//(void)RISCVL_DmWrite(ulAbits, RISCV_DMI_ABSTRACTCS, RISCV_DMI_ABSTRACTCS_CMDERR_7 ,&ulData);

	// checking and clearing error before accessing DMI
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// actual DMI Access reg read after all error check
	ulResult = RISCVL_DmRead(ulAbits,ulAddress,pulResult);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	return	ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteDMIAccessReg
     Engineer: HS,CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - address value for debug module registers
               unsigned long ulProgramDataIn - value to write in register address
               unsigned long *pulResult  - pointer to read data
       Output: unsigned long - error code (ERR_xxx)
  Description: Write DM register
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteDMIAccessReg(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulProgramDataIn,unsigned long *pulResult)
{
	unsigned long ulResult = ERR_NO_ERROR;

	unsigned long ulData = 0;

	//clear any error before accessing DM register
	//(void)RISCVL_DmWrite(ulAbits, RISCV_DMI_ABSTRACTCS, RISCV_DMI_ABSTRACTCS_CMDERR_7 ,&ulData);

	// checking and clearing error before accessing DMI
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// actual DMI Access reg write after all error check
	ulResult = RISCVL_DmWrite(ulAbits,ulAddress,ulProgramDataIn,pulResult);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return	ulResult;
}

/****************************************************************************
     Function: RISCVL_AbsAccessRegOperations
     Engineer: HS,CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long ulGprsValue - gprs register number
               unsigned long ulOperation - operations in abstract command register(transfer,write,postexec )
               unsigned long *pulDataOut  - pointer to read data
       Output: unsigned long - error code (ERR_xxx)
  Description: Write data to access register abstract command depends upon operations.
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ExecAbstractCmdForRegOp(unsigned long ulAbits,unsigned long ulGprsValue,unsigned long ulOperation,unsigned long ulArraySize,unsigned long *pulDataOut)
{
	unsigned long ulData = 0;
	unsigned long ulDataTemp = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulCounter = 0;

	while(ulCounter <= ONE_ITERATION_COUNTER)
	{
		//read Debug module control register to conform haltreq, resumereq, and ackhavereset are all 0.
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMCONTROL, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		ulDataTemp = ulData;

		//check haltreq, resumereq, and ackhavereset are all 0
		if(ulData & HALTREQ_RESUMEREQ_ACKHAVERESET)
		{
			// check it and write all reset value only ones
			if(ulCounter == 0)
			{
			ulDataTemp = ulDataTemp & (~HALTREQ_RESUMEREQ_ACKHAVERESET);
			ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_DMCONTROL,ulDataTemp,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			}
			else
			{
				// still not able to make haltreq, resumereq, and ackhavereset are all 0. so returning error
				return ERR_RISCV_ERROR_DISALE_ABSTRACT_COMMAND;
			}
		}
		else
		{
			break;
		}
		ulCounter++;
	}

	if(ulOperation ==(RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_POSTEXEC ))
	{
		//enabling post execution, transfer and write
		ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | RISCV_ACCESS_REGISTER_POSTEXEC | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER | ulGprsValue ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else if(ulOperation ==(RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER))
	{
		//write and transfer enabled
		ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_TRANSFER | ulGprsValue ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else if(ulOperation == (RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_POSTEXEC))
	{
		//transfer and post execution
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | RISCV_ACCESS_REGISTER_POSTEXEC | RISCV_ACCESS_REGISTER_TRANSFER | ulGprsValue ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else if(ulOperation == RISCV_ACCESS_REGISTER_TRANSFER)
	{
		//transfer enable
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | RISCV_ACCESS_REGISTER_TRANSFER | ulGprsValue ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else if(ulOperation == RISCV_ACCESS_REGISTER_POSTEXEC)
	{
		//post execution enable
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | RISCV_ACCESS_REGISTER_POSTEXEC | ulGprsValue ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else
	{
		return ERR_RISCV_ERROR_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;
	}

	//cmd error return
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	*pulDataOut =ulData;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbsAccessRegCmdError
     Engineer: CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long *pulDataOut  - pointer to read data
       Output: unsigned long - error code (ERR_xxx)
  Description: checking for error case in Abstract register operations.
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_AbsAccessRegCmdError(unsigned long ulAbits ,unsigned long *pulDataOut)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulError = 0;
	unsigned long ulTimer= 0;

	do
	{
		//read the abstract control and status and see if its busy
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_ABSTRACTCS, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		if(!((ulData) & RISCV_DMI_ABSTRACTCS_BUSY ))
		{
			// break the condition when busy is zero
			break;
		}
		else
		{
			//check for busy after ever 100ms, when busy identified in previous read
			timer_100ms(FALSE);
			ulTimer++;
		}
	}while(ulTimer < TIMERCOUNT_ONE_SECOND);

	// if still busy, return error
	if(ulData & RISCV_DMI_ABSTRACTCS_BUSY)
	{
		//write the reset value for busy hang in debug module control, reset hart using hartreset and ndmreset
		(void)RISCVL_DmWrite(ulAbits,RISCV_DMI_DMCONTROL , RISCV_DMI_DMCONTROL_NDMRESET | RISCV_DMI_DMCONTROL_HARTRESET ,&ulData);

		// making DMActive to set
		(void)RISCVL_DmWrite(ulAbits, RISCV_DMI_DMCONTROL, RISCV_DMI_DMCONTROL_DMACTIVE, &ulData);
		
		return ERR_RISCV_ABSTRACT_REG_WRITE_BUSY;

	}
	else
	{
		//Check for command errors
		if ((ulData & RISCV_DMI_ABSTRACTCS_CMDERR_7))
		{
			ulError = ((ulData & RISCV_DMI_ABSTRACTCS_CMDERR_7) >> RISCV_DMI_ABSTRACTCS_CMDERR_OFFSET) ;

			//write the reset value for cmd err
			(void)RISCVL_DmWrite(ulAbits, RISCV_DMI_ABSTRACTCS, RISCV_DMI_ABSTRACTCS_CMDERR_7 ,&ulData);

			// switch to return type of cmd error produced
			switch(ulError)
			{
			case ERR_RISCV_ABSTRACT_REG_BUSY:
				ulResult = ERR_RISCV_ABSTRACT_REG_WRITE_BUSY;
				break;
			case ERR_RISCV_ABSTRACT_REG_NOT_SUPPORT:
				ulResult = ERR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED;
				break;
			case ERR_RISCV_ABSTRACT_REG_EXCEPTION:
				ulResult = ERR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION;
				break;
			case ERR_RISCV_ABSTRACT_REG_HART_STATE:
				ulResult = ERR_RISCV_ABSTRACT_REG_WRITE_HART_STATE;
				break;
			case ERR_RISCV_ABSTRACT_REG_BUS_EEROR:
				ulResult = ERR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR;
				break;
			case ERR_RISCV_ABSTRACT_REG_OTHERS:
			default:
				ulResult = ERR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON;
				break;
			}
		}
	}
	*pulDataOut =ulData;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_ReadMemoryWord
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long *pulAddressData  - pointer to read data
       Output: unsigned long - error code (ERR_xxx)
  Description: read a word to memory location
Date           Initials    Description
05-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadMemoryWord(unsigned long ulAbits,unsigned long ulAddress ,unsigned long ulArchSize,unsigned long *pulAddressData)
{
	unsigned long ulData = 0,ulArraySize=0;
	unsigned long ulResult = ERR_NO_ERROR;

	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}
	//write lw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_LOAD_S0_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_IMM_OPCODE | RISCV_EBREAK_INSTRUCTION_OPCODE, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Execute the program buffer with postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//move data from gprs s0 register to d0 data register with transfer 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read the d0 register
	ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,pulAddressData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteMemoryWord
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulValue  - value to be written
       Output: unsigned long - error code (ERR_xxx)
  Description: Write a word to memory location
Date           Initials    Description
05-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteMemoryWord(unsigned long ulAbits,unsigned long ulAddress, unsigned long ulArchSize,unsigned long ulValue)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0;

	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}

	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_STORE_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_IMM_OPCODE | RISCV_EBREAK_INSTRUCTION_OPCODE, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write value to  d0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s1 register with transfer 1,write 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Execute the program buffer with postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_ReadMemoryWordByte
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulWords   - number of words(1 word = 4 bytes)
              unsigned long *pulData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadMemoryWordByte(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulArraySize = 0;
	unsigned long ulResult = ERR_NO_ERROR;

	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}

	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_LOAD_BYTE_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer2
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1 and postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read value from gprs s1 register and execute program buffer with transfer 1, postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read the d0 register
	ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	pucData[0] = ulData & 0xFF;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_ReadMemoryWordHalf
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulWords   - number of words(1 word = 4 bytes)
               unsigned long *pulData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadMemoryWordHalf(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulArraySize = 0;
	unsigned long ulResult = ERR_NO_ERROR;

	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}
	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_LOAD_HALF_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer2
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1 and postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read value from gprs s1 register and execute program buffer with transfer 1, postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read the d0 register
	ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	pucData[0] = ulData & 0xFF;
	pucData[1] = (ulData >> 8) & 0xFF;

	return ulResult;
}


/****************************************************************************
     Function: RISCVL_ReadMemoryWordBlock
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulWords   - number of words(1 word = 4 bytes)
               unsigned long *pulData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadMemoryWordBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulArchSize,unsigned long ulFenceSupport,unsigned long *pulData)
{
	unsigned long ulData = 0,ulArraySize = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulOffset=0x0;
	// TODO:if requires inn future ulDirectAccessToCsr
	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}

	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_LOAD_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write Addi instruction  to program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_ADDI4_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer2
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF2,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	///till here
	//write the address from data0 register  to gprs s0 register with transfer 1,write 1 and postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read value from gprs s1 register and execute program buffer with transfer 1, postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write value to  autoexec command register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulWords = ulWords -1;
	while(ulWords--)
	{
		//write values to  d0 register
		ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		*(pulData + ulOffset) = ulData;
		ulOffset = ulOffset + 1;
	}
	//write value to  autoexec command register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,0x0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//cmd error return
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read last value after setting autoexec 0
	ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	*(pulData + ulOffset) = ulData;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteMemoryWordBlock
     Engineer: HS
        Input:unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulWords   - number of words(1 word = 4 bytes)
              unsigned long *pulData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteMemoryWordBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords ,unsigned long ulArchSize,unsigned long *pulData)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulArraySize = 0;
	unsigned long ulDrLength =ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH;
	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}

	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_STORE_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write Addi instruction  to program buffer1
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_ADDI4_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer2
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF2,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write value to  d0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,pulData[0],&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write first value  to gprs s1 register and execute program buffer with transfer 1,write 1 ,postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE |RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write value to  autoexec command register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	ulResult = RISCVL_DmWriteData0Multiple(RISCV_DMI_DATA0, ulDrLength,ulWords - 1,pulData + 1,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	/*while(ulWords--)
	{
		//write values to  d0 register
		//ulResult = RISCVL_WriteAbsDataRegOrProgramBuffer(ulAbits,RISCV_DMI_DATA0,pulData[i],&ulData);
		ulResult = RISCVL_MemoryOpt(ulDrLength,RISCV_DMI_DATA0,pulData[i],&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		i++;
	}*/
	//disabling the abstract_auto register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,0x0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	return ulResult;

	//check for error
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteMemoryWordByte
     Engineer: HS
        Input:unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulWords   - number of words(1 word = 4 bytes)
              unsigned long *pulData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteMemoryWordByte(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData)
{

	unsigned long ulData = 0,ulValue,ulArraySize=0;
	unsigned long ulResult = ERR_NO_ERROR;

	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}
	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_STORE_BYTE_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer2
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulValue = pucData[0];
	//write value to  d0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write first value  to gprs s1 register and execute program buffer with transfer 1,write 1 ,postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE |RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteMemoryWordHalf
     Engineer: HS
        Input:unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulWords   - number of words(1 word = 4 bytes)
              unsigned long *pulData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteMemoryWordHalf(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulValue,ulArraySize=0;
	unsigned long ulResult = ERR_NO_ERROR;


	switch(ulArchSize)
	{
	case 32:
		ulArraySize = 0x2;
	break;
	case 64:
		ulArraySize =0x3;
	break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;
		break;
	}
	//write sw instruction to program buffer0
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF0,RISCV_STORE_HALF_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write ebreak instruction  to program buffer2
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_PROGBUF1,RISCV_EBREAK_INSTRUCTION,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write address to data0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write the address from data0 register  to gprs s0 register with transfer 1,write 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S0_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulValue = pucData[0] | ((unsigned long)pucData[1]<< 8);
	//write value to  d0 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//write first value  to gprs s1 register and execute program buffer with transfer 1,write 1 ,postexec 1
	ulResult = RISCVL_ExecAbstractCmdForRegOp(ulAbits,RISCV_GPRS_S1_VALUE,RISCV_ACCESS_REGISTER_TRANSFER | RISCV_ACCESS_REGISTER_WRITE |RISCV_ACCESS_REGISTER_POSTEXEC,ulArraySize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_ReadMemoryProgBuf
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadMemoryProgBuf(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulFenceSupport,unsigned char *pucData)
{

	unsigned long ulResult = ERR_NO_ERROR;

	switch(ulSize)
	{
	case ACCESS_WORD:
		//read block of words(4 bytes)
		ulResult = RISCVL_ReadMemoryWordBlock(ulAbits,ulAddress,ulCount,ulArchSize,ulFenceSupport,(unsigned long *)pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	case ACCESS_HALF:
		//read 2 bytes of data
		ulResult = RISCVL_ReadMemoryWordHalf(ulAbits,ulAddress,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	case ACCESS_BYTE:
		//read 1 byte of data
		ulResult = RISCVL_ReadMemoryWordByte(ulAbits,ulAddress,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_MEMORY_ACCESS_SIZE;
		break;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteMemoryProgBuf
     Engineer: HS
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long *pucData  - pointer to data  buffer
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteMemoryProgBuf(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount ,unsigned long ulSize,unsigned long ulArchSize,unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;
	switch(ulSize)
	{
	case ACCESS_WORD:
		//write block of words(4 bytes)
		ulResult = RISCVL_WriteMemoryWordBlock(ulAbits,ulAddress,ulCount,ulArchSize,(unsigned long *)pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	case ACCESS_HALF:
		//write 2 bytes of data
		ulResult = RISCVL_WriteMemoryWordHalf(ulAbits,ulAddress,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	case ACCESS_BYTE:
		//write 1 byte of data
		ulResult = RISCVL_WriteMemoryWordByte(ulAbits,ulAddress,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_MEMORY_ACCESS_SIZE;
		break;
	}

	return ulResult;
}


/****************************************************************************
     Function: RISCVL_AddTrigger
     Engineer: Ashutosh Garg
        Input:unsigned long ulAbits - number of Abits
              unsigned long ulHartId - Current hart id
              unsigned long ulTriggerAddr - trigger address
              unsigned long ulTriggerNum  -trigger number
              unsigned long ulOperation -  operation to perform(hardware breakpoints or Watchpoints)
              unsigned long ulArchSize   - Architecture of target
              unsigned long ulDirectAccessToCsr- whether can access CSRs directly
		      unsigned long ulBitValue         - bit value to write in trigger register
       Output:unsigned long *pulTriggerOut   -   returns status of trigger
                                                 0: No error
                                                 2: trigger already exist
                                                 3: trigger does not exist
              unsigned long - error code (ERR_xxx)
  Description: Add trigger to target depends on Bit value.
Date           Initials    Description
08-10-2018        AG         Initial
03-05-2019        HS         Modified for Watchpoint support.
 ****************************************************************************/
unsigned long RISCVL_AddTrigger(unsigned long ulAbits, unsigned long ulHartId, unsigned long ulTriggerAddr, unsigned long ulTriggerNum,unsigned long ulOperation,unsigned long ulArchSize ,unsigned long ulDirectAccessToCsr,
		unsigned long *pulTriggerOut,unsigned long ulBitValue)
{
	unsigned long ulData[2] = {0};
	unsigned long ulTselectVal[2]={0};
	unsigned long ulTriggerNumber[2]={0};
	unsigned long ulTriggerAddress[2]={0};
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAddress = 0;
	unsigned long ulValue = 0;

	//prepare the ulValue depends on ulOperation and bit value .
	switch(ulOperation)
	{
	case RISCV_HARDWARE_BREAKPOINT:
		ulValue = TRIGGER_HARDWARE_BREAKPOINT | ulBitValue;
		break;
	case RISCV_WATCHPOINT_READ:
		ulValue = TRIGGER_READ_WATCHPOINT | ulBitValue;
		break;
	case RISCV_WATCHPOINT_WRITE:
		ulValue = TRIGGER_WRITE_WATCHPOINT | ulBitValue;
		break;
	case RISCV_WATCHPOINT_ACCESS:
		ulValue = TRIGGER_ACCESS_WATCHPOINT | ulBitValue;
		break;
	default:
		return ERR_RISCV_ERROR_INVALID_TRIGGER_OPERATION;
	}

	//Read Tselect register and store in ulTselectVal.
	ulAddress = RISCV_CSR_TSELECT;
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulTselectVal);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Store Trigger number.
	ulTriggerNumber[0]=ulTriggerNum;

	//write Tselect with trigger number.
	ulResult = RISCVL_WriteSingleRegister(ulTriggerNumber,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read back Tselect register.
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//ensure read value should be equal to written value to  Tselect  register.
	if(ulData[0] == ulTriggerNumber[0] && ulData[1] == ulTriggerNumber[1])
	{

		//read Tdata1 register(as Mcontrol register).
		ulAddress = RISCV_CSR_TDATA1;
		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//if load,store or execute bit in Mcontrol register is set to 1, means trigger is already in use.
		if((ulData[0] & RISCV_CSR_MCONTROL_LOAD) || (ulData[0] & RISCV_CSR_MCONTROL_STORE) || (ulData[0]& RISCV_CSR_MCONTROL_EXECUTE))
		{
			//Write back initial read value to tselect register.
			ulAddress = RISCV_CSR_TSELECT;
			ulResult = RISCVL_WriteSingleRegister(ulTselectVal,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//return status of trigger.
			*pulTriggerOut =  TRIGGER_ALREADY_EXIST;
			return ulResult;
		}
		else
		{
			//check architecture
			if(ulArchSize == 32)
			{
				//store ulValue to ulData.
				ulData[0] = ulValue;
			}
			else if(ulArchSize == 64)
			{
				// prepare (64 bit value)with required  bits to set and store  ulValue to ulData.
				ulData[1] = ulValue & 0XFFFF0000;
				ulData[0] = ulValue & 0x0000FFFF;
			}

			//write ulData to Tdata1(Mcontrol) register.
			ulAddress = RISCV_CSR_TDATA1;
			ulResult = RISCVL_WriteSingleRegister(ulData,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//store trigger address.
			ulTriggerAddress[0]=ulTriggerAddr;

			//write trigger address to Tdata2 register.
			ulAddress = RISCV_CSR_TDATA2;
			ulResult = RISCVL_WriteSingleRegister(ulTriggerAddress,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//Write back initial read value to tselect register.
			ulAddress = RISCV_CSR_TSELECT;
			ulResult = RISCVL_WriteSingleRegister(ulTselectVal,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//set trigger status as 0x0(no error).
			*pulTriggerOut = 0x0;
			return ulResult;
		}
	}
	else
	{
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		//set trigger status
		*pulTriggerOut =  TRIGGER_DOES_NOT_EXIST;
		return ulResult;
	}

}

/****************************************************************************
     Function: RISCVL_RemoveTrigger
     Engineer: Ashutosh Garg
        Input:unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulHartId - Current hart id
              unsigned long ulArchSize   - Architecture of target
              unsigned long ulTriggerNum  -trigger number
              unsigned long ulDirectAccessToCsr- whether can access CSRs directly
       Output:unsigned long - error code (ERR_xxx)
  Description: Remove Trigger from target
Date           Initials    Description
08-10-2018        AG          Initial
 ****************************************************************************/
unsigned long RISCVL_RemoveTrigger(unsigned long ulAbits, unsigned long ulHartId,unsigned long ulArchSize,unsigned long ulTriggerNum,unsigned long ulDirectAccessToCsr)
{
	unsigned long ulData[2] = {0};
	unsigned long ulTselectVal[2]={0};
	unsigned long ulTriggerNumber[2]={0};
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAddress = 0;

	//Read Tselect register and store in ulTselectVal.
	ulAddress = RISCV_CSR_TSELECT;
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulTselectVal);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Store Trigger number.
	ulTriggerNumber[0]= ulTriggerNum;
	ulTriggerNumber[1] = 0;

	////write Tselect with trigger number.
	ulResult = RISCVL_WriteSingleRegister(ulTriggerNumber,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

    //store 0 in ulData.
	ulData[0] = 0;
	ulData[1] = 0;

	//write ulData as 0 to Tdata1(Mcontrol) register to delete trigger.
	ulAddress = RISCV_CSR_TDATA1;
	ulResult = RISCVL_WriteSingleRegister(ulData,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Write back initial read value to tselect register.
	ulAddress = RISCV_CSR_TSELECT;
	ulResult = RISCVL_WriteSingleRegister(ulTselectVal,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_HandleRemoveTrigger
     Engineer: Harvinder Singh
        Input:unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulHartId - Current hart id
              unsigned long ulArchSize   - Architecture of target
              unsigned long ulTriggerNum  -trigger number
              unsigned long ulDirectAccessToCsr- whether can access CSRs directly
       Output:unsigned long - error code (ERR_xxx)
  Description: Handles the different cases to remove the trigger
Date           Initials    Description
03-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_HandleRemoveTrigger(unsigned long ulAbits, unsigned long ulHartId,unsigned long ulArchSize,unsigned long ulTriggerNum,unsigned long ulDirectAccessToCsr,unsigned long ulResUsed)
{
	unsigned long ulResult = ERR_NO_ERROR;

	//remove trigger depends on ulResUsed.
	//if ulResUsed is 1.
	if(ulResUsed == RESOURCE_1)
	{
		//remove trigger with given ulTriggerNum from target.
		ulResult = RISCVL_RemoveTrigger(ulAbits,ulHartId,ulArchSize,ulTriggerNum,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}

	//if ulResUsed is 2.
	else if(ulResUsed == RESOURCE_2)
	{
		//remove trigger with ulTriggerNum from target for resource 1.
		ulResult = RISCVL_RemoveTrigger(ulAbits,ulHartId,ulArchSize,ulTriggerNum,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//increment triggerNum by 1
		ulTriggerNum = ulTriggerNum +1;

		//remove trigger with ulTriggerNum from target for resource 2.
		ulResult = RISCVL_RemoveTrigger(ulAbits,ulHartId,ulArchSize,ulTriggerNum,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_HandleAddTrigger
     Engineer: Harvinder Singh
        Input:unsigned long ulAbits - number of Abits
              unsigned long ulHartId - Current hart id
              unsigned long ulTriggerAddr - trigger address
              unsigned long ulTriggerNum  -trigger number
              unsigned long ulOperation -  operation to perform(hardware breakpoints or Watchpoints)
              unsigned long ulArchSize   - Architecture of target
              unsigned long ulDirectAccessToCsr- whether can access CSRs directly
		      unsigned long ulLength         -   Length of particular breakpoint
		      unsigned long ulMatch          -   represent match bit for operation
		      unsigned long ulResrequired    -   No. of resource required for particular operation

       Output:unsigned long - error code (ERR_xxx)
              unsigned long *pulTriggerOut   - TO check trigger status
  Description: handles the different cases and  add trigger to target
Date           Initials    Description
03-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_HandleAddTrigger(unsigned long ulAbits, unsigned long ulHartId, unsigned long ulTriggerAddr, unsigned long ulTriggerNum,unsigned long ulOperation,unsigned long ulArchSize ,unsigned long ulDirectAccessToCsr,
		unsigned long *pulTriggerOut,unsigned long ulLength,unsigned long ulMatch,unsigned long ulResrequired)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulBitValue=0;
	unsigned long ulFinalAddress=0;

	//In Case when match for exact address is required.
	//All hardware breakpoint case and watchpoint with length = 1 will  be handled in this condition.
	if(ulMatch == MATCH_0 && ulResrequired == RESOURCE_1 )
	{
		//prepare ulBitValue to set Match bit to 0 in match control register.
		ulBitValue = MATCH_0 << RISCV_CSR_MCONTROL_MATCH_OFFSET;

		//function to add trigger.
		ulResult = RISCVL_AddTrigger(ulAbits,ulHartId,ulTriggerAddr,ulTriggerNum,ulOperation,ulArchSize,
				   ulDirectAccessToCsr,pulTriggerOut,ulBitValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}

	//In Case when  match = 1(for masking) and resource required is 1.
	//when we set WP on variable with length(2,4,bytes)  will be  handled in this condition.
	else if(ulMatch == MATCH_1 && ulResrequired == RESOURCE_1 )
	{
		//prepare ulBitValue to set Match bit to 1 in match control register.
		ulBitValue = MATCH_1 << RISCV_CSR_MCONTROL_MATCH_OFFSET;

		//function to add trigger.
		ulResult = RISCVL_AddTrigger(ulAbits,ulHartId,ulTriggerAddr,ulTriggerNum,ulOperation,ulArchSize,
				   ulDirectAccessToCsr,pulTriggerOut,ulBitValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}

	//In Case when match = 2 and  resources required is 2.
	//when we set WP on arrays of length greater than 4  in range of addresses will be  handled in this condition.
	else if(ulMatch == MATCH_2 && ulResrequired == RESOURCE_2)
	{
		//prepare ulBitValue to set Match bit to 2 and Chain bit as 1 in match control register.
		ulBitValue = (CHAIN<< RISCV_CSR_MCONTROL_CHAIN_OFFSET)| (MATCH_2 << RISCV_CSR_MCONTROL_MATCH_OFFSET);

		//function to add trigger.
		//Add 1st trigger on start address.
		ulResult = RISCVL_AddTrigger(ulAbits,ulHartId,ulTriggerAddr,ulTriggerNum,ulOperation,ulArchSize,
				   ulDirectAccessToCsr,pulTriggerOut,ulBitValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//reset bit value
		ulBitValue = 0;

		//Get final address to write in second trigger.
		ulFinalAddress = ulTriggerAddr + ulLength;

		//increment trigger number.
		ulTriggerNum = ulTriggerNum + 1;

		//prepare ulBitValue to set Match bit to 3 in Match Control register.
		ulBitValue = (MATCH_3 << RISCV_CSR_MCONTROL_MATCH_OFFSET);

		//function to add trigger.
		//Add 2nd trigger on final address.
		ulResult = RISCVL_AddTrigger(ulAbits,ulHartId,ulFinalAddress,ulTriggerNum,ulOperation,ulArchSize,
				   ulDirectAccessToCsr,pulTriggerOut,ulBitValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_TriggerInfo
     Engineer: Ashutosh Garg
        Input:unsigned long ulAbits - number of abits
              unsigned long ulAddress - starting address of memory location(RAM)
              unsigned long ulWords   - number of words(1 word = 4 bytes)
              unsigned long *pulData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        AG          Initial
 ****************************************************************************/
unsigned long RISCVL_TriggerInfo(unsigned long ulAbits,unsigned long ulArchSize, unsigned long *pulDataBuf,unsigned long ulDirectAccessToCsr)
{
	unsigned long ulTselectVal[2]={0};
	unsigned long ulTriggerCount[2] = {0};
	unsigned long ulCount = 0;
	unsigned long ulData[2]={0};
	unsigned long ulTriggerNum = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAddress = 0;
	unsigned long ulLoopCounter = 0;

	ulAddress = RISCV_CSR_TSELECT;
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulTselectVal);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//counter needs to come out of infinite loop, assumed with max value, future may to update
	while( ulLoopCounter < MAX_TRIGGER_COUNT_50)
	{

		ulResult = RISCVL_WriteSingleRegister(ulTriggerCount,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		if((ulTriggerCount[0] == ulData[0]) && (ulTriggerCount[1] == ulData[1]))
		{
			ulTriggerNum++;
			ulCount++;
			ulTriggerCount[0] = ulCount;
			ulTriggerCount[1] = 0;
		}
		else
		{
			ulResult = RISCVL_WriteSingleRegister(ulTselectVal,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			break;
		}
		ulLoopCounter++;
	}

	*pulDataBuf = ulTriggerNum;

	if(ulLoopCounter == MAX_TRIGGER_COUNT_50)
	{
		return ERR_RISCV_ERROR_TRIGGER_INFO_LOOP_TIMEOUT;
	}

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_SystemBusyErrorCheck
     Engineer: HS,CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - address value for debug module registers
               unsigned long *pulResult  - pointer to read data from register
       Output: unsigned long - error code (ERR_xxx)
  Description: read data from debug module registers and program buffer
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_SystemBusyErrorCheck(unsigned long ulAbits,unsigned long ulAddress,unsigned long *pulResult)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;
	unsigned long ulError = 0;
	unsigned long ulTimer = 0;

	//read the abstract command control and status
	ulResult = RISCVL_DmRead(ulAbits, ulAddress, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//to wait for busy to get over
	//TODO: read busy status within this loop and exit immediately when busy state is reset
	do
	{
		//checking busy after 100ms, if busy is enabled
		ulResult = RISCVL_DmRead(ulAbits, ulAddress, &ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		if(!(ulData & RISCV_DMI_SBCS_SBBUSY))
		{
			//break the loop when busy is zero
			break;
		}
		else
		{
			//checkfor busy, if busy wait 100ms
			timer_100ms(FALSE);
			ulTimer++;
		}
	}while(ulTimer < TWENTY_ITERATION_COUNTER);

	if(!(ulData & RISCV_DMI_SBCS_SBBUSY))
	{
		if(((ulData & RISCV_DMI_SBCS_SBBUSYERROR)))
		{
			ulResult = RISCVL_DmWrite(ulAbits, ulAddress, RISCV_DMI_SBCS_SBBUSYERROR ,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
		else if(((ulData & RISCV_DMI_SBCS_SBERROR)))
		{
			ulError = (ulData & RISCV_DMI_SBCS_SBERROR) >> RISCV_DMI_SBCS_SBERROR_OFFSET ;
			ulResult = RISCVL_DmWrite(ulAbits, ulAddress, RISCV_DMI_SBCS_SBERROR ,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			switch(ulError)
			{
			case ERR_RISCV_SYSTEM_BUS_TIMEOUT:
				ulResult = ERR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED;
				break;
			case ERR_RISCV_SYSTEM_BUS_BAD_ADDRESS:
				ulResult = ERR_RISCV_SYSTEM_BUS_ADDRESS_FAILED;
				break;
			case ERR_RISCV_SYSTEM_BUS_ALIGNMENT_ERR:
				ulResult = ERR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED;
				break;
			case ERR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE:
				ulResult = ERR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED;
				break;
			case ERR_RISCV_SYSTEM_BUS_OTHER:
			default:
				ulResult = ERR_RISCV_SYSTEM_BUS_OTHER_REASON;
				break;
			}
		}
	}
	else
	{
		//sb_busy still is high
		ulResult = ERR_RISCV_SYSTEM_BUS_MASTER_BUSY;
	}
	return	ulResult;
}


/****************************************************************************
     Function:  RISCVL_SystemBusReadMemory
     Engineer:
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long *pucData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_SystemBusReadMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount ,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned char *pucData)
{
	unsigned long ulData[2] = {0},ulSbAccessSize = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulOffset=0x0;
	//TODO:Implement 8,16,64,128 bit bus access.
	switch(ulSize)
	{
	//added case 1, for byte data
	case 1:
		ulSbAccessSize = 0x0;
		break;
	case 2:
		ulSbAccessSize = 0x1;
		break;
	case 4:
		ulSbAccessSize = 0x2;
		break;
	case 8:
		ulSbAccessSize = 0x3;
		break;
	default:
		return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
		break;
	}

	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_SBCS,(ulSbAccessSize << RISCV_DMI_SBCS_SBACCESS_OFFSET | RISCV_DMI_SBCS_SBREADONDATA | RISCV_DMI_SBCS_SBREADONADDR | RISCV_DMI_SBCS_SBAUTOINCREMENT),ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_SBADDRESS0,ulAddress,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	while(ulCount--)
	{
		//write values to  d0 register
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_SBDATA0, ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		*((unsigned long *)pucData + ulOffset) = ulData[0];
		ulOffset = ulOffset + 1;
	}
	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_SBCS,RISCV_DMI_SBCS_SBREADONDATA_RESET,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	if(ulCount!=0)
	{
		ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_SBDATA0,ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		*((unsigned long *)pucData + ulOffset) = ulData[0];
	}

	ulResult = RISCVL_SystemBusyErrorCheck(ulAbits,RISCV_DMI_SBCS,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
		
	return ulResult;
}

/****************************************************************************
     Function:  RISCVL_SystemBusWriteMemory
     Engineer:
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long *pucData  - pointer to data  buffer
       Output:unsigned long - error code (ERR_xxx)
  Description: Write a block of data to memory location
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_SystemBusWriteMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount ,unsigned long ulSize,unsigned char *pucData)
{
	//TODO:Implement 8,16,64,128 bit bus access.
	unsigned long ulData = 0,ulSbAccessSize = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulDrLength = ulAbits + RISCV_DTM_RISCV_DMI_DATA_LENGTH + RISCV_DTM_RISCV_DMI_OP_LENGTH;
	//	unsigned long ulOffset=0x0,i=0;
	//unsigned long i=0;
	unsigned long *pulData;
	switch(ulSize)
	{
	//added case 1, for byte data
	case 1:
		ulSbAccessSize = 0x0;
		break;
	case 2:
		ulSbAccessSize = 0x1;
		break;
	case 4:
		ulSbAccessSize = 0x2;
		break;
	case 8:
		ulSbAccessSize =0x3;
		break;
	default:
		return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
		break;
	}

	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_SBCS,(ulSbAccessSize << RISCV_DMI_SBCS_SBACCESS_OFFSET |  RISCV_DMI_SBCS_SBAUTOINCREMENT),&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_SBADDRESS0,ulAddress,&ulData);//currently there is no support for 64-bit address in openocd
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	pulData=(unsigned long*)pucData;

	ulResult = RISCVL_DmWriteData0Multiple(RISCV_DMI_SBDATA0, ulDrLength, ulCount, pulData, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	/*while(ulCount--)
	{
		//write values to  d0 register
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_SBDATA0,pulData[i],&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		i++;
	}*/

	ulResult = RISCVL_SystemBusyErrorCheck(ulAbits,RISCV_DMI_SBCS,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;

}

/****************************************************************************
     Function: RISCVL_AbsAccessMemoryOperations
     Engineer: HS,CBR
        Input: unsigned long ulAbits - number of abits.
               unsigned long ulOperation - operation(read=0 ,write=1).
               unsigned long ulAamSize   - specifies Access memory size.
                                           0: 8-bit
                                           1: 16-bit
                                           2: 32-bit
                                           3: 64-bit
                                           4: 128-bit
               unsigned long *pulDataOut  - returns status of abstract command.
       Output: unsigned long - error code (ERR_xxx)
  Description: Set up and execute Abstract Access Memory Command to read and write data.
Date           Initials    Description
21-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_ExecAbstractCmdForMemOp(unsigned long ulAbits,unsigned long ulOperation,unsigned long ulAamSize,unsigned long *pulDataOut)
{
	unsigned long ulData = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulCounter = 0;
	unsigned long ulDataTemp = 0;

	while(ulCounter <= ONE_ITERATION_COUNTER)
		{
			//read Debug module control register to conform haltreq, resumereq, and ackhavereset are all 0.
			ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_DMCONTROL, &ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
			ulDataTemp = ulData;

			//check haltreq, resumereq, and ackhavereset are all 0
			if(ulData & HALTREQ_RESUMEREQ_ACKHAVERESET)
			{
				// check it and write all reset value only ones
				if(ulCounter == 0)
				{
				ulDataTemp = ulDataTemp & (~HALTREQ_RESUMEREQ_ACKHAVERESET);
				ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_DMCONTROL,ulDataTemp,&ulData);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
				}
				else
				{
					// still not able to make haltreq, resumereq, and ackhavereset are all 0. so returning error
					return ERR_RISCV_ERROR_DISALE_ABSTRACT_COMMAND;
				}
			}
			else
			{
				break;
			}
			ulCounter++;
		}
	//Read operation
	if(ulOperation == ACCESS_MEMORY_READ)
	{
		// setup abstract command for read op: Abstract Cmd type = Abstract access memory, update mem size,ulOperationValue( to set autoincrement= 1 ) so that address stored in Data1 is incremented with every read operation.
		ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ABSTRACT_ACCESS | (ulAamSize << RISCV_ACCESS_MEMORY_AAMSIZE_OFFSET)| RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	//Write operation
	if(ulOperation == ACCESS_MEMORY_WRITE)
	{
		// setup abstract command for write op: Abstract Cmd type = Abstract access memory, update mem size,ulOperationValue( to set autoincrement= 1,write = 1 )so  that address stored in Data1 is incremented with every write operation.
		ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_COMMAND,RISCV_DMI_COMMAND_CMDTYPE_ABSTRACT_ACCESS | (ulAamSize << RISCV_ACCESS_MEMORY_AAMSIZE_OFFSET)| RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT | RISCV_ACCESS_MEMORY_WRITE  ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	
	//Check for any error
	ulResult = RISCVL_AbsAccessRegCmdError(ulAbits,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	*pulDataOut =ulData;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_SelectMemoryAccessSize
     Engineer: Harvinder Singh
       Input:  unsigned long ulSize - size of each word(1,2,4,8 bytes).
               unsigned long *pulAsize  - pointer to get Access Size.
       Output: unsigned long - error code (ERR_xxx)
  Description: select memory access size.
               1 byte : 0
               2 bytes: 1
               4 bytes: 2
Date           Initials    Description
24-05-2019        HS          Initial
*****************************************************************************/
unsigned long RISCVL_SelectMemoryAccessSize(unsigned long ulSize,unsigned long *pulAsize)
{
	unsigned long ulResult = ERR_NO_ERROR;

	//select access size for abstract access memory depends on ulSize(1,2,4,8 bytes).
	switch(ulSize)
	{
		// case 1, for byte data
		case 1:
			*pulAsize = 0x0;
			break;
		case 2:
			*pulAsize = 0x1;
			break;
		case 4:
			*pulAsize = 0x2;
			break;
		case 8:
			*pulAsize =0x3;
			break;
		default:
			return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
			break;
	}

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_StoreReadValueTOBuffer
     Engineer: Harvinder Singh
       Input:  unsigned char *pucData - pointer buffer to store data.
       	   	   unsigned long ulOffset  - offset from base address.
       	   	   unsigned long ulSize - size of each word(1,2,4 bytes).
               unsigned long ulValue  - value to store
       Output: unsigned long - error code (ERR_xxx)
  Description: store the value to buffer depends on ulSize(1,2,4 bytes).
Date           Initials    Description
24-05-2019        HS          Initial
*****************************************************************************/
unsigned long RISCVL_StoreReadValueToBuffer(unsigned char *pucData,unsigned long ulOffset,unsigned long ulSize,unsigned long ulValue)
{
	unsigned long ulResult = ERR_NO_ERROR;

#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	unsigned char* pucValue;
	//check ulSize (1: 1byte, 2: 2bytse, 4: 4bytes)
	switch(ulSize)
	{
	//store 1byte data
	case 1:
		pucValue = (unsigned char *)(&ulValue);
		*((unsigned char*)pucData)= *(pucValue + ulOffset);
		break;
		//2 bytes
	case 2:
		pucValue = (unsigned char *)(&ulValue);
		*((unsigned short*)pucData) = *((unsigned short *)(pucValue + ulOffset));
		break;
		//4 bytes
	case 4:
		*((unsigned long *)pucData) = *((unsigned long *)(&ulValue) + ulOffset);
		break;
	default:
		return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
		break;
	}
	return ulResult;
#else
	//TODO:need to test for 1,2 bytes if it supports.
	//Store value depends on ulSize
	switch(ulSize)
	{
		//store 1 byte data
		case 1:
			*(pucData) = *((unsigned char*)(&ulValue));
			break;
		//store  2 bytes data
		case 2:
			*((unsigned short *)pucData) = *((unsigned short *)(&ulValue));
			break;
			//store 4 byte data
		case 4:
			*((unsigned long *)pucData) = ulValue;
			break;
		default:
			return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
			break;
	}
	return ulResult;
#endif
}

/****************************************************************************
     Function: RISCVL_GetValueFromBuffer
     Engineer: Harvinder Singh
       Input:  unsigned char *pucData - pointer buffer to get data.
       	   	   unsigned long ulOffset  - offset value.
       	   	   unsigned long ulSize - size of each word(1,2,4 bytes).
               unsigned long *pulValue  - pointer  to store value
       Output: unsigned long - error code (ERR_xxx)
  Description: get value from buffer to pulValue depnds on ulSize(1,2,4 bytes).
Date           Initials    Description
24-05-2019        HS          Initial
*****************************************************************************/
unsigned long RISCVL_GetValueFromBuffer(unsigned char *pucData,unsigned long ulOffset,unsigned long ulSize,unsigned long *pulValue)
{
	unsigned long ulResult = ERR_NO_ERROR;

#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	//check ulSize (1: 1byte, 2: 2bytse, 4: 4bytes)
	switch(ulSize)
	{
	//store 1byte data
	case 1:
		*((unsigned char *)pulValue+ ulOffset) = *((unsigned char *)pucData);
		break;
		//2 bytes
	case 2:
		//TODO: change the logic as it is done in RISCVL_StoreReadValueToBuffer()
		if(ulOffset == 2)
		{
			*((unsigned short*)pulValue + (ulOffset-1)) = *((unsigned short *)pucData);
		}
		else
		{
			*((unsigned short*)pulValue + ulOffset) = *((unsigned short *)pucData);
		}
		break;
		//4 bytes
	case 4:
		*((unsigned long*)pulValue + ulOffset) = *((unsigned long *)pucData);
		break;
	default:
		return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
		break;
	}
	return ulResult;
#else
	//check ulSize (1: 1byte, 2: 2bytse, 4: 4bytes)

	//TODO:need to test for 1,2 bytes if it supports.
	switch(ulSize)
	{
		//store 1byte data
		case 1:
			*pulValue = *((unsigned char*)pucData);
			break;
		//2 bytes
		case 2:
			*pulValue = *((unsigned short *)pucData);
			break;
		//4 bytes
		case 4:
			*pulValue = *((unsigned long *)pucData);
			break;
		default:
			return ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD;
			break;
	}
   return ulResult;

#endif
}

/****************************************************************************
     Function: RISCVL_AbstractAccessReadMemoryWord
     Engineer: Harvinder Singh
       Input: unsigned long ulAbits -  DM(Debug Module) registers address Size.
               unsigned long ulAddress - starting address of memory location(RAM).
               unsigned long ulSize - size of each word(1,2,4 bytes).
               unsigned long *pucData  - pointer to data  buffer.
       Output: unsigned long - error code (ERR_xxx)
  Description: read a single word(1,2 and 4byte )of data from memory location.
Date           Initials    Description
24-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessReadMemoryWord(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulIndex = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAsize = 0;

//for WDC,get the index of byte ,Align the address and select access Size as 0x2(4 bytes).
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	//get the index where we have to write value in ulValue.
	ulIndex = ulAddress & 0x3;

	//for address alignment.
	ulAddress =ulAddress & 0xFFFFFFFC;

	//select access size as 0x2 for 4 bytes.
	//So pass 4 as ulSize.
	ulResult = RISCVL_SelectMemoryAccessSize(4,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
#else
	//select memory access size.
	ulResult = RISCVL_SelectMemoryAccessSize(ulSize,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

#endif

	// Store address to Data1 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// Setup  and execute abstract command for  read operation
	ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read value from Data0 register
	ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//store the ulData value to pucData buffer depends on ulSize(1,2 or 4 bytes).
	ulResult = RISCVL_StoreReadValueToBuffer(pucData,ulIndex,ulSize,ulData);
	if (ulResult != ERR_NO_ERROR)
	    return ulResult;

	//Check for any error after operation
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbstractAccessWriteMemoryWord
     Engineer: Harvinder Singh
       Input: unsigned long ulAbits - DM(Debug Module) registers address Size.
               unsigned long ulAddress - starting address of memory location(RAM).
               unsigned long ulSize - size of each word.
               unsigned long *pucData  - pointer to data  buffer.
       Output: unsigned long - error code (ERR_xxx)
  Description: Write a single word(1,2 and 4bytes)of data to memory location.
Date           Initials    Description
24-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessWriteMemoryWord(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulIndex = 0,ulStoreValue = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAsize =0;

	//for WDC,get the index of byte ,Align the address and select access Size as 0x2(4 bytes);
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT

	//get the index where we have to write value in ulValue.
	ulIndex = ulAddress & 0x3;

	//for address alignment.
	ulAddress =ulAddress & 0xFFFFFFFC;

	//select access size as 0x2 for 4 bytes.
	//So pass 4 as ulSize.
	ulResult = RISCVL_SelectMemoryAccessSize(4,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

#else

	//select memory access size.
	ulResult = RISCVL_SelectMemoryAccessSize(ulSize,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

#endif

	// Store address to Data1 register.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
	    return ulResult;

//for WDC,before writing a new value(1 or 2 bytes) firstly read previous value(32 bit) store it then write back updated value.
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT

	//ensure when ulSize is 1 or 2 bytes.
	if(ulSize != 4)
	{
		// Setup and execute abstract command for  read operation.
		ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//Read value from d0 register.
		ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulStoreValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
#endif

	//get value from puData buffer and store to ulStoreValue depends on ulSize(1,2 or 4 bytes).
	ulResult = RISCVL_GetValueFromBuffer(pucData,ulIndex,ulSize,&ulStoreValue);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// Store Value to Data0 register.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulStoreValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// Setup and execute abstract command for  Write operation.
	ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_WRITE,ulAsize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Check for any error after operation.
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbstractAccessReadMemoryBlock
     Engineer: Harvinder Singh
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulASize - size of each word
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
21-05-2019        HS          Initial
24=05-2019        HS          Added 1,2 bytes memory access
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessReadMemoryBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulIndex = 0;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulAsize =0;

//for WDC,get the index of byte ,Align the address and select access Size as 0x2(4 bytes).
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	//get the index where we have to write value in ulValue.
	ulIndex = ulAddress & 0x3;

	//for address alignment.
	ulAddress =ulAddress & 0xFFFFFFFC;

	//select access size as 0x2 for 4 bytes.
	//So pass 4 as ulSize.
	ulResult = RISCVL_SelectMemoryAccessSize(4,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
#else
	//select memory access size.
	ulResult = RISCVL_SelectMemoryAccessSize(ulSize,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
#endif
	// Enable autoexec in abstract auto register so that current command is executed whenever data0 is accessed
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
		
	// Store address to Data1 register
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// Setup  and execute abstract command for read operation
	ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//keep ulWords greater than 1 to read last value when autoexec register is disabled
	while(ulWords>1)
	{
		ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//store the ulData value to pucData buffer depends on ulSize(1,2 or 4 bytes).
		ulResult = RISCVL_StoreReadValueToBuffer(pucData,ulIndex,ulSize,ulData);
		if (ulResult != ERR_NO_ERROR)
		    return ulResult;

#ifndef ACCESS_MEMORY_AAMPOSTINCREMENT
		//aampostincrement is not supported by WD core.
	    //increment address by ulsize(1,2 or 4 bytes) and write to data1 register.
		ulAddress  = ulAddress + ulSize;

		// Store address to Data1
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
#endif
		pucData = pucData + ulSize;
		ulWords = ulWords - 1;
	}
	
  // Disable autoexec in abstract auto register to stop executing abstract command further
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,0x0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//read last value after setting autoexec 0
	ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//store the ulData value to pucData buffer depends on ulSize(1,2 or 4 bytes).
	ulResult = RISCVL_StoreReadValueToBuffer(pucData,ulIndex,ulSize,ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Check for any error
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/***************************************************************************
     Function: RISCVL_AbstractAccessReadMemoryBlockManual
     Engineer: HS
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulWords   - Count of no. of words
               unsigned long ulASize - size of each word
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location manually without Ammpostincrement bit.
Date           Initials    Description
21-05-2019        HS          Initial
24=05-2019        HS          Added 1,2 bytes memory access
 ***************************************************************************/
unsigned long RISCVL_AbstractAccessReadMemoryBlockManual(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulIndex = 0;
	unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulAsize = 0;

//for WDC,get the index of byte ,Align the address and select access Size as 0x2(4 bytes).
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	//get the index where we have to write value in ulValue.
	ulIndex = ulAddress & 0x3;

	//for address alignment.
	ulAddress =ulAddress & 0xFFFFFFFC;

	//select access size as 0x2 for 4 bytes.
	//So pass 4 as ulSize.
	ulResult = RISCVL_SelectMemoryAccessSize(4,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
#else
    //select memory access size.
    ulResult = RISCVL_SelectMemoryAccessSize(ulSize,&ulAsize);
    if (ulResult != ERR_NO_ERROR)
    	return ulResult;
#endif
	// Store address to Data1 register.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//ensure ulWords are greater than 0.
	while(ulWords>0)
	{
		// Setup and execute abstract command for read operation.
		ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//read value from Data0 register
		ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//store the ulData value to pucData buffer depends on ulSize(1,2 or 4 bytes).
		ulResult = RISCVL_StoreReadValueToBuffer(pucData,ulIndex,ulSize,ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//if  aampostincrement is not supported.
		//increment address by ulsize(1,2 or 4 bytes) and write to data1 register.
		#ifndef ACCESS_MEMORY_AAMPOSTINCREMENT
		    //increment address
			ulAddress  = ulAddress + ulSize;
			// Store address to Data1
			ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		#endif

		pucData = pucData + ulSize;
		ulWords = ulWords - 1;
	}

	//Check for any error during operation
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbstractAccessReadMemory
     Engineer: Harvinder Singh
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulWords   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
21-04-2019        HS          Initial
24=05-2019        HS          Added manual Read access depnds on Autoexec flag
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessReadMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulSize,unsigned long ulArchSize,unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	if(ulWords == 1)
	{
		//read single word(1,2 or 4 bytes)
		ulResult = RISCVL_AbstractAccessReadMemoryWord(ulAbits,ulAddress,ulSize,pucData);
		return ulResult;
	}

	//if tyDwRiscvConfig.ucAbsAutoExecSupported  is set that means it supports  auto exec register.
	//we can use this register for more efficient memory access.
	if(gtyDwRiscvConfig.ucAbsAutoExecSupported)
	{
		//read block of words(1,2 or 4 bytes)
		ulResult = RISCVL_AbstractAccessReadMemoryBlock(ulAbits,ulAddress,ulWords,ulSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else
	{
		//ensure this function is for read memory manually with no Abs autoExec Support.
		//read block of words(1,2 or 4 bytes)
		ulResult = RISCVL_AbstractAccessReadMemoryBlockManual(ulAbits,ulAddress,ulWords,ulSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbstractAccessWriteMemoryBlock
     Engineer: HS
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulASize - size of each word
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
21-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessWriteMemoryBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulIndex = 0,ulStoreValue = 0;
	unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulAsize = 0;

//for WDC,get the index of byte ,Align the address and select access Size as 0x2(4 bytes);
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	//get the index where we have to write value in ulValue.
	ulIndex = ulAddress & 0x3;

	//for address alignment.
	ulAddress =ulAddress & 0xFFFFFFFC;

	//select access size as 0x2 for 4 bytes.
	//So pass 4 as ulSize.
	ulResult = RISCVL_SelectMemoryAccessSize(4,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
#else
    //select memory access size.
    ulResult = RISCVL_SelectMemoryAccessSize(ulSize,&ulAsize);
    if (ulResult != ERR_NO_ERROR)
    	return ulResult;
#endif
    //write address to Data1 register.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

//for WDC,before writing a new value(1 or 2 bytes) firstly read previous value(32 bit) store it then write back updated value.
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT

	//ensure when ulSize is 1 or 2 bytes.
	if(ulSize != 4)
	{
		// Setup and execute abstract command for  read operation.
		ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//Read value from d0 register.
		ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulStoreValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	}
#endif

	//get value from puData buffer and store to ulStoreValue depends on ulSize(1,2 or 4 bytes).
	ulResult = RISCVL_GetValueFromBuffer(pucData,ulIndex,ulSize,&ulStoreValue);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//increment pucdata to get new value.
	pucData = pucData + ulSize;

	//write value to Data0 register.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulStoreValue,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// Setup  and execute abstract command for write operation.
	ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_WRITE,ulAsize,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	// Enable autoexec in abstract auto register so that current command is executed whenever data0 is accessed.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	ulWords = ulWords -1;

	while(ulWords>0)
	{
#ifndef ACCESS_MEMORY_AAMPOSTINCREMENT
		//aampostincrement is not supported by WD
		//increment address by ulsize(1,2 or 4 bytes) and write to data1 register.
		ulAddress  = ulAddress + ulSize;
		//write address to Data1 register.
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
#endif

//for WDC,before writing a new value(1 or 2 bytes) firstly read previous value(32 bit) store it then write back updated value.
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT

		//ensure when ulSize is 1 or 2 bytes.
		if(ulSize != 4)
		{
			// Setup and execute abstract command for  read operation.
			ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//Read value from d0 register.
			ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulStoreValue);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
#endif
		//get value from puData buffer and store to ulStoreValue depends on ulSize(1,2 or 4 bytes).
		ulResult = RISCVL_GetValueFromBuffer(pucData,ulIndex,ulSize,&ulStoreValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//write values to  d0 register.
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulStoreValue,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//increment pucData buffer by ulSize to get new value.
		pucData = pucData + ulSize;
		ulWords = ulWords - 1;
		ulStoreValue = 0;
	}

	// Disable autoexec in abstract auto register to stop executing abstract command further.
	ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_ABSTRACTAUTO,0x0,&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//Check for any error during operation.
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbstractAccessWriteMemoryBlockManual
     Engineer: Harvinder Singh
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulASize - size of each word
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
24-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessWriteMemoryBlockManual(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulSize,unsigned char *pucData)
{
	unsigned long ulData = 0,ulIndex = 0,ulStoreValue = 0;
	unsigned long ulResult = ERR_NO_ERROR;
    unsigned long ulAsize = 0;

//for WDC,get the index of byte ,Align the address and select access Size as 0x2(4 bytes);
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT
	//get the index where we have to write value in ulValue.
	ulIndex = ulAddress & 0x3;

	//for address alignment.
	ulAddress =ulAddress & 0xFFFFFFFC;

	//select access size as 0x2 for 4 bytes.
	//So pass 4 as ulSize.
	ulResult = RISCVL_SelectMemoryAccessSize(4,&ulAsize);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
#else
    //select memory access size.
    ulResult = RISCVL_SelectMemoryAccessSize(ulSize,&ulAsize);
    if (ulResult != ERR_NO_ERROR)
    	return ulResult;
#endif
    // Store address to Data1 register.
    ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
    if (ulResult != ERR_NO_ERROR)
    	return ulResult;

    while(ulWords>0)
	{

//for WDC,before writing a new value(1 or 2 bytes) firstly read previous value(32 bit) store it then write back updated value.
#ifdef WDC_ABS_MEMORY_BYTES_SUPPORT

		//ensure when ulSize is 1 or 2 bytes.
		if(ulSize != 4)
		{
			// Setup and execute abstract command for  read operation.
			ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_READ,ulAsize,&ulData);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;

			//Read value from d0 register.
			ulResult = RISCVL_ReadDMIAccessReg(ulAbits,RISCV_DMI_DATA0,&ulStoreValue);
			if (ulResult != ERR_NO_ERROR)
				return ulResult;
		}
#endif

		//get value from puData buffer and store to ulStoreValue depends on ulSize(1,2 or 4 bytes).
		ulResult = RISCVL_GetValueFromBuffer(pucData,ulIndex,ulSize,&ulStoreValue);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		//write value to  d0 register.
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA0,ulStoreValue,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		// Setup and execute abstract command for write operation.
		ulResult = RISCVL_ExecAbstractCmdForMemOp(ulAbits,ACCESS_MEMORY_WRITE,ulAsize,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

//if aampostincrement is not supported.
//increment address by ulsize(1,2 or 4 bytes) and write to data1 register.
#ifndef ACCESS_MEMORY_AAMPOSTINCREMENT
		ulAddress  = ulAddress + ulSize;
		//write address to Data1
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits,RISCV_DMI_DATA1,ulAddress,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
#endif

		//increment pucData buffer by ulsize to get new value.
		pucData = pucData + ulSize;
		ulWords= ulWords-1;
		ulStoreValue = 0;
	}

	//Check for any error during operation
	ulResult = RISCVL_AbsAccessRegCmdError( ulAbits, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ulResult;
}
/****************************************************************************
     Function: RISCVL_AbstractAccessWriteMemory
     Engineer: Harvinder Singh
       Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulWords   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
24-05-2019        HS          Initial
 ****************************************************************************/
unsigned long RISCVL_AbstractAccessWriteMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulSize,unsigned long ulArchSize,unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;

	//Write single word(1,2 or 4 bytes).
	if(ulWords == 1)
	{
		ulResult = RISCVL_AbstractAccessWriteMemoryWord(ulAbits,ulAddress,ulSize,pucData);
		return ulResult;
	}

	//if tyDwRiscvConfig.ucAbsAutoExecSupported  is set that means it supports  auto exec register.
	//we can use this register for more efficient  memory access.
	if(gtyDwRiscvConfig.ucAbsAutoExecSupported)
	{
		//Write a block(1,2 or 4 bytes) of memory.
		ulResult = RISCVL_AbstractAccessWriteMemoryBlock(ulAbits,ulAddress,ulWords,ulSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
    }
	else
	{
		//ensure this function for write memory manually with not AbsautoExec support.
		//Write block of memory(1,2 or 4 bytes).
		ulResult = RISCVL_AbstractAccessWriteMemoryBlockManual(ulAbits,ulAddress,ulWords,ulSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_ReadMemory
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long ulMemMechanism- which memory mechanism to use
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_ReadMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulMemMechanism,unsigned long ulDirectAccessToCsr,unsigned long ulFenceSupport,unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulJtagScanDROut[3] = {0};
	unsigned long ulIrVal = 0;

	//writing abstract data register to zero
	ulResult=RISCVL_AbstractDataResest(ulAbits,ulArchSize);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	if(ulMemMechanism == SYSTEM_BUS_ACCESS)
	{
		//need to check, now added only for system bus
		ulIrVal = RISCV_DTM_DMI;
		//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
		ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		ulResult=RISCVL_SystemBusReadMemory(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,ulDirectAccessToCsr,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else if(ulMemMechanism ==PROGRAM_BUFFER_ACCESS)
	{
		ulResult = RISCVL_ReadMemoryProgBuf(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,ulFenceSupport,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else
	{
		ulResult = RISCVL_AbstractAccessReadMemory(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_WriteMemory
     Engineer: HS
        Input: unsigned long ulAbits - number of abits
               unsigned long ulAddress - starting address of memory location(RAM)
               unsigned long ulCount   - Count of no. of words
               unsigned long ulSize - size of each word
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long ulMemMechanism- which memory mechanism to use
               unsigned long *pucData  - pointer to data  buffer
       Output: unsigned long - error code (ERR_xxx)
  Description: read a block of data from memory location.
Date           Initials    Description
08-10-2018        VH          Initial
 ****************************************************************************/
unsigned long RISCVL_WriteMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulMemMechanism,unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulJtagScanDROut[3] = {0};
	unsigned long ulIrVal = 0;

	if(ulMemMechanism == SYSTEM_BUS_ACCESS)
	{
		//need to check, now added only for system bus
		ulIrVal = RISCV_DTM_DMI;
		//called for entering to JTAG DTM REGISTER, DEBUG MODULE INTERFACE ACCESS
		ulResult = JtagScanIR(&ulIrVal, ulJtagScanDROut);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;

		ulResult=RISCVL_SystemBusWriteMemory(ulAbits,ulAddress,ulCount,ulSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else if(ulMemMechanism == PROGRAM_BUFFER_ACCESS)
	{
		ulResult = RISCVL_WriteMemoryProgBuf(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	else
	{
		ulResult = RISCVL_AbstractAccessWriteMemory(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,pucData);
		if(ulResult != ERR_NO_ERROR)
			return ulResult;
	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_Ebreaks
     Engineer: CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long ulArchSize- architecture size (32,64 bit)
               unsigned long ulDirectAccessToCsr - dscr can be accessed directly or with GPRS
       Output: unsigned long - error code (ERR_xxx)
  Description: Enabling EBREAK instructions such as Machine Mode,Supervisor Mode,User/Application mode.
Date           Initials    Description
08-10-2018        CBR          Initial
 ****************************************************************************/
unsigned long RISCVL_Ebreaks(unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr)
{
	unsigned long ulAddress = 0;
	unsigned long ulWriteCsr[2] = {0};
	unsigned long ulResult = ERR_NO_ERROR;

	//read csr for debugcsr register
	ulAddress = RISCV_CSR_DSCR;
	ulResult = RISCVL_ReadSingleRegister(ulAddress,ulArchSize,ulAbits,ulDirectAccessToCsr,ulWriteCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	//enable all the Ebreak in the array
	ulWriteCsr[0] = ( RISCV_CSR_DCSR_EBREAKM | RISCV_CSR_DCSR_EBREAKS | RISCV_CSR_DCSR_EBREAKU | ulWriteCsr[0] | 0x03);

	//writing to EBREAK to enable in DCSR register
	ulResult = RISCVL_WriteSingleRegister(ulWriteCsr,ulAddress,ulArchSize ,ulAbits,ulDirectAccessToCsr);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_CheckArc
     Engineer: CBR
        Input: unsigned long ulAbits -Abits for target
               unsigned long ulArraySize-architecture size (32,64 bit)
       Output: unsigned long - error code (ERR_xxx)
  Description: Returns error value, if failed for particular architecture .

Date           Initials    Description
13-5-2019        CBR          Initial
 ****************************************************************************/
unsigned long RISCVL_CheckArc(unsigned long ulAbits, unsigned long ulArraySize)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;

	ulResult = RISCVL_DmWrite(ulAbits,RISCV_DMI_COMMAND,((RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD) | (ulArraySize << RISCV_ACCESS_REGISTER_AARSIZE_OFFSET) | (RISCV_ACCESS_REGISTER_TRANSFER) | (RISCV_GPRS_S0_VALUE )),&ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	ulResult = RISCVL_DmRead(ulAbits, RISCV_DMI_ABSTRACTCS, &ulData);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;
	if (ulData & RISCV_DMI_ABSTRACTCS_CMDERR_7)
	{
		ulResult = RISCVL_DmWrite(ulAbits, RISCV_DMI_ABSTRACTCS, RISCV_DMI_ABSTRACTCS_CMDERR_7 ,&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		return ERR_RISCV_ERROR_UNSUPPORT_ABSTRACT_ACCESS_OPERATION;
	}
	return ulResult;
}
/****************************************************************************
     Function: RISCVL_Dtm
     Engineer: CBR
        Input: unsigned long *pulAbitsOut - pointer to store data
        	   unsigned long *pulIdleOut - pointer to store data
        	   unsigned long *pulversionOut - pointer to store data
       Output: unsigned long - error code (ERR_xxx)
  Description: Get Abit, Idle count and Version
Date           Initials    Description
28--2007        VH          Initial
 ****************************************************************************/
unsigned long  RISCVL_Dtm(unsigned long *pulAbitsOut,unsigned long *pulIdleOut,unsigned long *pulversionOut)
{
	unsigned long ulIrVal;
	unsigned long ulDrData = 0;
	unsigned long ulJtagScanDROut;
	unsigned long ulResult = ERR_NO_ERROR;

	JtagResetTap(0);
	ulIrVal = RISCV_DTM_DTMCS;
	ulDrData = RISCV_DATA_NULL;
	//IR scan for DTM control and status register
	ulResult = JtagScanIR(&ulIrVal,NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	//DR scan with data 0 and get data in ulJtagScanDROut
	ulResult = JtagScanDR(RISCV_DTM_RISCV_DMI_DATA_LENGTH,&ulDrData,&ulJtagScanDROut);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;
	*pulAbitsOut = (ulJtagScanDROut & RISCV_DTM_DTMCS_ABITS) >> RISCV_DTM_DTMCS_ABITS_OFFSET;
	*pulIdleOut = (ulJtagScanDROut & RISCV_DTM_DTMCS_IDLE) >> RISCV_DTM_DTMCS_IDLE_OFFSET;
	*pulversionOut = (ulJtagScanDROut & RISCV_DTM_DTMCS_VERSION);
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_AbstractDataResest
     Engineer: CBR
        Input: unsigned long ulAbits - number of abits
               unsigned long ulArchSize- architecture size (32,64 bit)
       Output: unsigned long - error code (ERR_xxx)
  Description: Writing Zero to Abstract data registers.
Date           Initials    Description
19-06-2019       CBR          Initial
 ****************************************************************************/
unsigned long RISCVL_AbstractDataResest(unsigned long ulAbits,unsigned long ulArchSize)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData = 0;
	unsigned long ulRiscvWriteDataIn[2] = {0};

	switch(ulArchSize)
	{
	case 64:
		//write on data1
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA1, ulRiscvWriteDataIn[1],&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
	case 32:
		//write on data0
		ulResult = RISCVL_WriteDMIAccessReg(ulAbits, RISCV_DMI_DATA0, ulRiscvWriteDataIn[0],&ulData);
		if (ulResult != ERR_NO_ERROR)
			return ulResult;
		break;
	default:
		return ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE;

	}
	return ulResult;
}

/****************************************************************************
     Function: RISCVL_SelectDMIAcessRegInIR
     Engineer: Rejeesh Shaji Babu
        Input: void
       Output: void
  Description: Select DMI Access reg (0x11) in IR register
  Date           Initials    Description
  18-07-2019      RSB          Initial
 ****************************************************************************/
void RISCVL_SelectDMIAcessRegInIR(void)
{
	unsigned long ulIrVal = RISCV_DTM_DMI;
	(void)JtagScanIR(&ulIrVal, NULL);
}
